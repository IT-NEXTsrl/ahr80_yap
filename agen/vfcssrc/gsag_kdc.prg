* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kdc                                                        *
*              Rinnovo elementi contratto                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-17                                                      *
* Last revis.: 2015-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kdc",oParentObject))

* --- Class definition
define class tgsag_kdc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 816
  Height = 562+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-12"
  HelpContextID=82454377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=81

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  DES_DIVE_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  CAUMATTI_IDX = 0
  ART_ICOL_IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsag_kdc"
  cComment = "Rinnovo elementi contratto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  w_ORIGINE = space(1)
  w_OLDCOMP = space(10)
  w_IMMODATR = space(20)
  w_DATAINIZIO1 = ctod('  /  /  ')
  w_DATAINIZIO2 = ctod('  /  /  ')
  w_DATAFINE1 = ctod('  /  /  ')
  w_DATAFINE2 = ctod('  /  /  ')
  w_FLTIPCON = space(1)
  o_FLTIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_ATCODNOM = space(15)
  w_ATCODSED = space(5)
  w_ELCONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELCODCLI = space(15)
  w_MODESCRI = space(50)
  w_ANDESCRI = space(50)
  w_CODESCON_ZOOM = space(50)
  w_IMDESCRI_ZOOM = space(50)
  w_IMDESCON = space(50)
  w_CODNOM = space(15)
  o_CODNOM = space(15)
  w_CODICE = space(15)
  w_DESCRI = space(60)
  w_CODSED = space(5)
  o_CODSED = space(5)
  w_DDNOMDES = space(40)
  w_CODCONTR = space(10)
  o_CODCONTR = space(10)
  w_CODCONTR2 = space(10)
  w_IMPIANTO = space(10)
  o_IMPIANTO = space(10)
  w_IMDESCRI = space(50)
  w_COMPIMP = space(50)
  o_COMPIMP = space(50)
  w_MODELLO = space(10)
  o_MODELLO = space(10)
  w_CODSERVL = space(20)
  w_MOTIPCON = space(1)
  w_COD_SERV = space(20)
  w_COMPIMPKEY = 0
  o_COMPIMPKEY = 0
  w_COD_SERV = space(20)
  w_COMPIMPKEYREAD = 0
  w_FLCONDQT = space(2)
  o_FLCONDQT = space(2)
  w_FLQTATOT = 0
  w_FLCONDQC = space(2)
  o_FLCONDQC = space(2)
  w_FLQTACON = 0
  w_FLCONDQR = space(2)
  o_FLCONDQR = space(2)
  w_FLQTARES = 0
  w_DATASTIPULA1 = ctod('  /  /  ')
  w_DATASTIPULA2 = ctod('  /  /  ')
  w_ELEMENTI = space(1)
  w_CODESCON = space(50)
  w_CODESCON2 = space(50)
  w_DESELE = space(60)
  w_DTPROFAT = space(1)
  w_DATFAT1 = ctod('  /  /  ')
  w_DATFAT2 = ctod('  /  /  ')
  w_DTPROATT = space(1)
  w_DATATT1 = ctod('  /  /  ')
  w_DATATT2 = ctod('  /  /  ')
  w_RICPE1 = 0
  w_RICVA1 = 0
  w_ARROT1 = 0
  w_VALORIN = 0
  w_ARROT2 = 0
  w_VALOR2IN = 0
  w_ARROT3 = 0
  w_VALOR3IN = 0
  w_ARROT4 = 0
  w_VALOFIN = 0
  w_ELRINNOV = 0
  w_RINCON = space(1)
  w_DATDIS = ctod('  /  /  ')
  w_EMPTYDATDIS = space(1)
  w_RINNCONT = space(1)
  w_ARDESART = space(40)
  w_ARUNIMIS = space(3)
  w_ARTIPART = space(2)
  w_MOFLATT = space(1)
  w_MOFLATTI = space(1)
  w_MOTIPATT = space(20)
  w_CAFLNSAP = space(1)
  w_FLESCRIN = space(1)
  w_FLDATFIS = space(1)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kdcPag1","gsag_kdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Elementi contratto")
      .Pages(2).addobject("oPag","tgsag_kdcPag2","gsag_kdc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATAINIZIO1_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='IMP_MAST'
    this.cWorkTables[4]='IMP_DETT'
    this.cWorkTables[5]='CON_TRAS'
    this.cWorkTables[6]='MOD_ELEM'
    this.cWorkTables[7]='CAUMATTI'
    this.cWorkTables[8]='ART_ICOL'
    this.cWorkTables[9]='OFF_NOMI'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_ORIGINE=space(1)
      .w_OLDCOMP=space(10)
      .w_IMMODATR=space(20)
      .w_DATAINIZIO1=ctod("  /  /  ")
      .w_DATAINIZIO2=ctod("  /  /  ")
      .w_DATAFINE1=ctod("  /  /  ")
      .w_DATAFINE2=ctod("  /  /  ")
      .w_FLTIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_ATCODNOM=space(15)
      .w_ATCODSED=space(5)
      .w_ELCONTRA=space(10)
      .w_ELCODMOD=space(10)
      .w_ELCODIMP=space(10)
      .w_ELCODCOM=0
      .w_ELCODCLI=space(15)
      .w_MODESCRI=space(50)
      .w_ANDESCRI=space(50)
      .w_CODESCON_ZOOM=space(50)
      .w_IMDESCRI_ZOOM=space(50)
      .w_IMDESCON=space(50)
      .w_CODNOM=space(15)
      .w_CODICE=space(15)
      .w_DESCRI=space(60)
      .w_CODSED=space(5)
      .w_DDNOMDES=space(40)
      .w_CODCONTR=space(10)
      .w_CODCONTR2=space(10)
      .w_IMPIANTO=space(10)
      .w_IMDESCRI=space(50)
      .w_COMPIMP=space(50)
      .w_MODELLO=space(10)
      .w_CODSERVL=space(20)
      .w_MOTIPCON=space(1)
      .w_COD_SERV=space(20)
      .w_COMPIMPKEY=0
      .w_COD_SERV=space(20)
      .w_COMPIMPKEYREAD=0
      .w_FLCONDQT=space(2)
      .w_FLQTATOT=0
      .w_FLCONDQC=space(2)
      .w_FLQTACON=0
      .w_FLCONDQR=space(2)
      .w_FLQTARES=0
      .w_DATASTIPULA1=ctod("  /  /  ")
      .w_DATASTIPULA2=ctod("  /  /  ")
      .w_ELEMENTI=space(1)
      .w_CODESCON=space(50)
      .w_CODESCON2=space(50)
      .w_DESELE=space(60)
      .w_DTPROFAT=space(1)
      .w_DATFAT1=ctod("  /  /  ")
      .w_DATFAT2=ctod("  /  /  ")
      .w_DTPROATT=space(1)
      .w_DATATT1=ctod("  /  /  ")
      .w_DATATT2=ctod("  /  /  ")
      .w_RICPE1=0
      .w_RICVA1=0
      .w_ARROT1=0
      .w_VALORIN=0
      .w_ARROT2=0
      .w_VALOR2IN=0
      .w_ARROT3=0
      .w_VALOR3IN=0
      .w_ARROT4=0
      .w_VALOFIN=0
      .w_ELRINNOV=0
      .w_RINCON=space(1)
      .w_DATDIS=ctod("  /  /  ")
      .w_EMPTYDATDIS=space(1)
      .w_RINNCONT=space(1)
      .w_ARDESART=space(40)
      .w_ARUNIMIS=space(3)
      .w_ARTIPART=space(2)
      .w_MOFLATT=space(1)
      .w_MOFLATTI=space(1)
      .w_MOTIPATT=space(20)
      .w_CAFLNSAP=space(1)
      .w_FLESCRIN=space(1)
      .w_FLDATFIS=space(1)
        .w_TIPCON = 'C'
        .w_ORIGINE = 'R'
          .DoRTCalc(3,8,.f.)
        .w_FLTIPCON = 'T'
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .w_OBTEST = i_DATSYS
        .w_ATCODNOM = .w_CODNOM
        .w_ATCODSED = .w_CODSED
        .w_ELCONTRA = .w_ZOOM.GetVar("ELCONTRA")
        .w_ELCODMOD = .w_ZOOM.GetVar("ELCODMOD")
        .w_ELCODIMP = .w_ZOOM.GetVar("ELCODIMP")
        .w_ELCODCOM = .w_ZOOM.GetVar("ELCODCOM")
        .w_ELCODCLI = .w_CODNOM
        .w_MODESCRI = .w_ZOOM.GETVAR("MODESCRI")
        .w_ANDESCRI = .w_ZOOM.GETVAR("ANDESCRI")
        .w_CODESCON_ZOOM = .w_ZOOM.GETVAR("CODESCON")
        .w_IMDESCRI_ZOOM = .w_ZOOM.GETVAR("IMDESCRI")
        .w_IMDESCON = .w_ZOOM.GETVAR("IMDESCON")
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_CODNOM))
          .link_2_1('Full')
        endif
        .w_CODICE = .w_CODNOM
          .DoRTCalc(25,25,.f.)
        .w_CODSED = SPACE(5)
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CODSED))
          .link_2_5('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_CODCONTR))
          .link_2_8('Full')
        endif
        .w_CODCONTR2 = .w_CODCONTR2
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_CODCONTR2))
          .link_2_9('Full')
        endif
        .w_IMPIANTO = IIF(EMPTY(.w_CODSED), space(10), .w_IMPIANTO)
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_IMPIANTO))
          .link_2_10('Full')
        endif
        .DoRTCalc(31,33,.f.)
        if not(empty(.w_MODELLO))
          .link_2_14('Full')
        endif
          .DoRTCalc(34,35,.f.)
        .w_COD_SERV = IIF(EMPTY(.w_MODELLO), SPACE(20), IIF(EMPTY(.w_CODSERVL), .w_COD_SERV, .w_CODSERVL) )
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_COD_SERV))
          .link_2_17('Full')
        endif
        .w_COMPIMPKEY = 0
        .w_COD_SERV = IIF(EMPTY(.w_MODELLO), SPACE(20), IIF(EMPTY(.w_CODSERVL), .w_COD_SERV, .w_CODSERVL) )
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_COD_SERV))
          .link_2_19('Full')
        endif
        .w_COMPIMPKEYREAD = .w_COMPIMPKEY
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_COMPIMPKEYREAD))
          .link_2_20('Full')
        endif
        .w_FLCONDQT = IIF(EMPTY(.w_FLCONDQT) OR .w_FLTIPCON='C' OR !EMPTY(.w_MODELLO) OR .w_MOTIPCON<>'P', 'NU', .w_FLCONDQT)
        .w_FLQTATOT = IIF(.w_FLCONDQT='NU', 0, .w_FLQTATOT)
        .w_FLCONDQC = IIF(EMPTY(.w_FLCONDQC) OR .w_FLTIPCON='C' OR !EMPTY(.w_MODELLO) OR .w_MOTIPCON<>'P', 'NU', .w_FLCONDQC)
        .w_FLQTACON = IIF(.w_FLCONDQC='NU', 0, .w_FLQTACON)
        .w_FLCONDQR = IIF(EMPTY(.w_FLCONDQR) OR .w_FLTIPCON='C' OR !EMPTY(.w_MODELLO) OR .w_MOTIPCON<>'P', 'NU', .w_FLCONDQR)
        .w_FLQTARES = IIF(.w_FLCONDQR='NU', 0, .w_FLQTARES)
          .DoRTCalc(46,47,.f.)
        .w_ELEMENTI = "E"
          .DoRTCalc(49,51,.f.)
        .w_DTPROFAT = "T"
          .DoRTCalc(53,54,.f.)
        .w_DTPROATT = "T"
          .DoRTCalc(56,66,.f.)
        .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
        .w_ELRINNOV = .w_ZOOM.GetVar("ELRINNOV")
        .w_RINCON = "T"
        .w_DATDIS = i_DATSYS
        .w_EMPTYDATDIS = IIF( EMPTY( .w_DATDIS ), "S", "N")
        .w_RINNCONT = 'U'
          .DoRTCalc(73,77,.f.)
        .w_MOTIPATT = .w_MOTIPATT
        .DoRTCalc(78,78,.f.)
        if not(empty(.w_MOTIPATT))
          .link_2_69('Full')
        endif
          .DoRTCalc(79,79,.f.)
        .w_FLESCRIN = 'S'
        .w_FLDATFIS = .w_ZOOM.GetVar("MODATFIS")
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_52.enabled = this.oPgFrm.Page2.oPag.oBtn_2_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .DoRTCalc(1,10,.t.)
            .w_ATCODNOM = .w_CODNOM
            .w_ATCODSED = .w_CODSED
            .w_ELCONTRA = .w_ZOOM.GetVar("ELCONTRA")
            .w_ELCODMOD = .w_ZOOM.GetVar("ELCODMOD")
            .w_ELCODIMP = .w_ZOOM.GetVar("ELCODIMP")
            .w_ELCODCOM = .w_ZOOM.GetVar("ELCODCOM")
            .w_ELCODCLI = .w_CODNOM
            .w_MODESCRI = .w_ZOOM.GETVAR("MODESCRI")
            .w_ANDESCRI = .w_ZOOM.GETVAR("ANDESCRI")
            .w_CODESCON_ZOOM = .w_ZOOM.GETVAR("CODESCON")
            .w_IMDESCRI_ZOOM = .w_ZOOM.GETVAR("IMDESCRI")
            .w_IMDESCON = .w_ZOOM.GETVAR("IMDESCON")
        .DoRTCalc(23,23,.t.)
            .w_CODICE = .w_CODNOM
        .DoRTCalc(25,25,.t.)
        if .o_CODNOM<>.w_CODNOM
            .w_CODSED = SPACE(5)
          .link_2_5('Full')
        endif
        .DoRTCalc(27,28,.t.)
        if .o_CODCONTR<>.w_CODCONTR
            .w_CODCONTR2 = .w_CODCONTR2
          .link_2_9('Full')
        endif
        if .o_CODSED<>.w_CODSED
            .w_IMPIANTO = IIF(EMPTY(.w_CODSED), space(10), .w_IMPIANTO)
          .link_2_10('Full')
        endif
        .DoRTCalc(31,35,.t.)
        if .o_MODELLO<>.w_MODELLO.or. .o_FLTIPCON<>.w_FLTIPCON
            .w_COD_SERV = IIF(EMPTY(.w_MODELLO), SPACE(20), IIF(EMPTY(.w_CODSERVL), .w_COD_SERV, .w_CODSERVL) )
          .link_2_17('Full')
        endif
        .DoRTCalc(37,37,.t.)
        if .o_MODELLO<>.w_MODELLO.or. .o_FLTIPCON<>.w_FLTIPCON
            .w_COD_SERV = IIF(EMPTY(.w_MODELLO), SPACE(20), IIF(EMPTY(.w_CODSERVL), .w_COD_SERV, .w_CODSERVL) )
          .link_2_19('Full')
        endif
        if .o_IMPIANTO<>.w_IMPIANTO.or. .o_COMPIMPKEY<>.w_COMPIMPKEY
            .w_COMPIMPKEYREAD = .w_COMPIMPKEY
          .link_2_20('Full')
        endif
        if .o_FLTIPCON<>.w_FLTIPCON.or. .o_MODELLO<>.w_MODELLO
            .w_FLCONDQT = IIF(EMPTY(.w_FLCONDQT) OR .w_FLTIPCON='C' OR !EMPTY(.w_MODELLO) OR .w_MOTIPCON<>'P', 'NU', .w_FLCONDQT)
        endif
        if .o_FLTIPCON<>.w_FLTIPCON.or. .o_FLCONDQT<>.w_FLCONDQT.or. .o_MODELLO<>.w_MODELLO
            .w_FLQTATOT = IIF(.w_FLCONDQT='NU', 0, .w_FLQTATOT)
        endif
        if .o_FLTIPCON<>.w_FLTIPCON.or. .o_MODELLO<>.w_MODELLO
            .w_FLCONDQC = IIF(EMPTY(.w_FLCONDQC) OR .w_FLTIPCON='C' OR !EMPTY(.w_MODELLO) OR .w_MOTIPCON<>'P', 'NU', .w_FLCONDQC)
        endif
        if .o_FLTIPCON<>.w_FLTIPCON.or. .o_FLCONDQC<>.w_FLCONDQC.or. .o_MODELLO<>.w_MODELLO
            .w_FLQTACON = IIF(.w_FLCONDQC='NU', 0, .w_FLQTACON)
        endif
        if .o_FLTIPCON<>.w_FLTIPCON.or. .o_MODELLO<>.w_MODELLO
            .w_FLCONDQR = IIF(EMPTY(.w_FLCONDQR) OR .w_FLTIPCON='C' OR !EMPTY(.w_MODELLO) OR .w_MOTIPCON<>'P', 'NU', .w_FLCONDQR)
        endif
        if .o_FLTIPCON<>.w_FLTIPCON.or. .o_FLCONDQR<>.w_FLCONDQR.or. .o_MODELLO<>.w_MODELLO
            .w_FLQTARES = IIF(.w_FLCONDQR='NU', 0, .w_FLQTARES)
        endif
        .DoRTCalc(46,66,.t.)
            .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
            .w_ELRINNOV = .w_ZOOM.GetVar("ELRINNOV")
        .DoRTCalc(69,70,.t.)
            .w_EMPTYDATDIS = IIF( EMPTY( .w_DATDIS ), "S", "N")
        .DoRTCalc(72,77,.t.)
            .w_MOTIPATT = .w_MOTIPATT
          .link_2_69('Full')
        if .o_FLTIPCON<>.w_FLTIPCON
          .Calculate_KROGUBZYVL()
        endif
        .DoRTCalc(79,80,.t.)
            .w_FLDATFIS = .w_ZOOM.GetVar("MODATFIS")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
    endwith
  return

  proc Calculate_BWBOIOUNMQ()
    with this
          * --- Sbianca contratto e impianto
          .w_CODCONTR = SPACE( 15 )
          .w_CODCONTR2 = SPACE(15)
          .w_IMPIANTO = SPACE( 10 )
          .w_COMPIMP = SPACE( 50 )
          .w_CODESCON = SPACE( 50 )
          .w_IMDESCRI = SPACE( 50 )
          .w_COMPIMPKEY = 0
          .w_CODESCON2 = SPACE(50)
    endwith
  endproc
  proc Calculate_KROGUBZYVL()
    with this
          * --- Sbianca modello, servizio, campi quantit� e relative combo
          .w_MODELLO = SPACE(10)
          .link_2_14('Full')
          .w_COD_SERV = SPACE(20)
          .link_2_17('Full')
          .link_2_19('Full')
          .w_FLCONDQT = "NU"
          .w_FLCONDQC = "NU"
          .w_FLCONDQR = "NU"
          .w_FLQTATOT = 0
          .w_FLQTACON = 0
          .w_FLQTARES = 0
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCOMPIMP_2_13.enabled = this.oPgFrm.Page2.oPag.oCOMPIMP_2_13.mCond()
    this.oPgFrm.Page2.oPag.oCOD_SERV_2_17.enabled = this.oPgFrm.Page2.oPag.oCOD_SERV_2_17.mCond()
    this.oPgFrm.Page2.oPag.oFLCONDQT_2_21.enabled = this.oPgFrm.Page2.oPag.oFLCONDQT_2_21.mCond()
    this.oPgFrm.Page2.oPag.oFLQTATOT_2_22.enabled = this.oPgFrm.Page2.oPag.oFLQTATOT_2_22.mCond()
    this.oPgFrm.Page2.oPag.oFLCONDQC_2_23.enabled = this.oPgFrm.Page2.oPag.oFLCONDQC_2_23.mCond()
    this.oPgFrm.Page2.oPag.oFLQTACON_2_24.enabled = this.oPgFrm.Page2.oPag.oFLQTACON_2_24.mCond()
    this.oPgFrm.Page2.oPag.oFLCONDQR_2_25.enabled = this.oPgFrm.Page2.oPag.oFLCONDQR_2_25.mCond()
    this.oPgFrm.Page2.oPag.oFLQTARES_2_26.enabled = this.oPgFrm.Page2.oPag.oFLQTARES_2_26.mCond()
    this.oPgFrm.Page1.oPag.oRICPE1_1_35.enabled = this.oPgFrm.Page1.oPag.oRICPE1_1_35.mCond()
    this.oPgFrm.Page1.oPag.oRICVA1_1_36.enabled = this.oPgFrm.Page1.oPag.oRICVA1_1_36.mCond()
    this.oPgFrm.Page1.oPag.oARROT2_1_39.enabled = this.oPgFrm.Page1.oPag.oARROT2_1_39.mCond()
    this.oPgFrm.Page1.oPag.oVALOR2IN_1_40.enabled = this.oPgFrm.Page1.oPag.oVALOR2IN_1_40.mCond()
    this.oPgFrm.Page1.oPag.oARROT3_1_41.enabled = this.oPgFrm.Page1.oPag.oARROT3_1_41.mCond()
    this.oPgFrm.Page1.oPag.oVALOR3IN_1_42.enabled = this.oPgFrm.Page1.oPag.oVALOR3IN_1_42.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page2.oPag.oCODSED_2_5.visible=!this.oPgFrm.Page2.oPag.oCODSED_2_5.mHide()
    this.oPgFrm.Page2.oPag.oDDNOMDES_2_6.visible=!this.oPgFrm.Page2.oPag.oDDNOMDES_2_6.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_7.visible=!this.oPgFrm.Page2.oPag.oStr_2_7.mHide()
    this.oPgFrm.Page2.oPag.oIMPIANTO_2_10.visible=!this.oPgFrm.Page2.oPag.oIMPIANTO_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    this.oPgFrm.Page2.oPag.oCOMPIMP_2_13.visible=!this.oPgFrm.Page2.oPag.oCOMPIMP_2_13.mHide()
    this.oPgFrm.Page2.oPag.oCOD_SERV_2_17.visible=!this.oPgFrm.Page2.oPag.oCOD_SERV_2_17.mHide()
    this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_18.visible=!this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_18.mHide()
    this.oPgFrm.Page2.oPag.oCOD_SERV_2_19.visible=!this.oPgFrm.Page2.oPag.oCOD_SERV_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_30.visible=!this.oPgFrm.Page2.oPag.oStr_2_30.mHide()
    this.oPgFrm.Page2.oPag.oDATFAT1_2_42.visible=!this.oPgFrm.Page2.oPag.oDATFAT1_2_42.mHide()
    this.oPgFrm.Page2.oPag.oDATFAT2_2_43.visible=!this.oPgFrm.Page2.oPag.oDATFAT2_2_43.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_44.visible=!this.oPgFrm.Page2.oPag.oStr_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_46.visible=!this.oPgFrm.Page2.oPag.oStr_2_46.mHide()
    this.oPgFrm.Page2.oPag.oDATATT1_2_47.visible=!this.oPgFrm.Page2.oPag.oDATATT1_2_47.mHide()
    this.oPgFrm.Page2.oPag.oDATATT2_2_48.visible=!this.oPgFrm.Page2.oPag.oDATATT2_2_48.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_49.visible=!this.oPgFrm.Page2.oPag.oStr_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page1.oPag.oRICPE1_1_35.visible=!this.oPgFrm.Page1.oPag.oRICPE1_1_35.mHide()
    this.oPgFrm.Page1.oPag.oRICVA1_1_36.visible=!this.oPgFrm.Page1.oPag.oRICVA1_1_36.mHide()
    this.oPgFrm.Page1.oPag.oARROT1_1_37.visible=!this.oPgFrm.Page1.oPag.oARROT1_1_37.mHide()
    this.oPgFrm.Page1.oPag.oVALORIN_1_38.visible=!this.oPgFrm.Page1.oPag.oVALORIN_1_38.mHide()
    this.oPgFrm.Page1.oPag.oARROT2_1_39.visible=!this.oPgFrm.Page1.oPag.oARROT2_1_39.mHide()
    this.oPgFrm.Page1.oPag.oVALOR2IN_1_40.visible=!this.oPgFrm.Page1.oPag.oVALOR2IN_1_40.mHide()
    this.oPgFrm.Page1.oPag.oARROT3_1_41.visible=!this.oPgFrm.Page1.oPag.oARROT3_1_41.mHide()
    this.oPgFrm.Page1.oPag.oVALOR3IN_1_42.visible=!this.oPgFrm.Page1.oPag.oVALOR3IN_1_42.mHide()
    this.oPgFrm.Page1.oPag.oARROT4_1_43.visible=!this.oPgFrm.Page1.oPag.oARROT4_1_43.mHide()
    this.oPgFrm.Page1.oPag.oVALOFIN_1_44.visible=!this.oPgFrm.Page1.oPag.oVALOFIN_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_55.visible=!this.oPgFrm.Page2.oPag.oStr_2_55.mHide()
    this.oPgFrm.Page2.oPag.oDATDIS_2_56.visible=!this.oPgFrm.Page2.oPag.oDATDIS_2_56.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_72.visible=!this.oPgFrm.Page2.oPag.oBtn_2_72.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_CODNOM Changed")
          .Calculate_BWBOIOUNMQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kdc
    if lower(cEvent)='w_zoom selected'
      opengest("A","gsag_ael","ELCODMOD", this.w_ELCODMOD,"ELCONTRA", this.w_ELCONTRA,"ELCODIMP", this.w_ELCODIMP,"ELCODCOM", this.w_ELCODCOM, "ELRINNOV", this.w_ELRINNOV)
    endif
    if cEvent='Search'
      this.oPgfrm.ActivePage=1
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODNOM
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODNOM))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODNOM_2_1'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODNOM)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_DESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSED
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODSED)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODNOM);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPCON;
                     ,'DDCODICE',this.w_CODNOM;
                     ,'DDCODDES',trim(this.w_CODSED))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSED)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSED) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODSED_2_5'),i_cWhere,'',"Sedi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);
           .or. this.w_CODNOM<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPCON;
                       ,'DDCODICE',this.w_CODNOM;
                       ,'DDCODDES',this.w_CODSED)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSED = NVL(_Link_.DDCODDES,space(5))
      this.w_DDNOMDES = NVL(_Link_.DDNOMDES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODSED = space(5)
      endif
      this.w_DDNOMDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONTR
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COSERIAL like "+cp_ToStrODBC(trim(this.w_CODCONTR)+"%");

          i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COSERIAL',trim(this.w_CODCONTR))
          select COSERIAL,CODESCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCONTR)==trim(_Link_.COSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCONTR) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAS','*','COSERIAL',cp_AbsName(oSource.parent,'oCODCONTR_2_8'),i_cWhere,'',"Contratti d'assistenza",'GSAG_AEL.CON_TRAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',oSource.xKey(1))
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_CODCONTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_CODCONTR)
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONTR = NVL(_Link_.COSERIAL,space(10))
      this.w_CODESCON = NVL(_Link_.CODESCON,space(50))
      this.w_CODCONTR2 = NVL(_Link_.COSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONTR = space(10)
      endif
      this.w_CODESCON = space(50)
      this.w_CODCONTR2 = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONTR2
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONTR2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COSERIAL like "+cp_ToStrODBC(trim(this.w_CODCONTR2)+"%");

          i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COSERIAL',trim(this.w_CODCONTR2))
          select COSERIAL,CODESCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCONTR2)==trim(_Link_.COSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCONTR2) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAS','*','COSERIAL',cp_AbsName(oSource.parent,'oCODCONTR2_2_9'),i_cWhere,'',"Contratti d'assistenza",'GSAG_AEL.CON_TRAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',oSource.xKey(1))
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONTR2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_CODCONTR2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_CODCONTR2)
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONTR2 = NVL(_Link_.COSERIAL,space(10))
      this.w_CODESCON2 = NVL(_Link_.CODESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONTR2 = space(10)
      endif
      this.w_CODESCON2 = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCONTR2  >= .w_CODCONTR OR EMPTY( .w_CODCONTR2 )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCONTR2 = space(10)
        this.w_CODESCON2 = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONTR2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIM',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_2_10'),i_cWhere,'GSAG_MIM',"Impianto",'GSAG_AE3.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODELLO
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODELLO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MOD_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_MODELLO)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON,MOFLFATT,MOFLATTI,MOTIPATT,MOCODSER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_MODELLO))
          select MOCODICE,MODESCRI,MOTIPCON,MOFLFATT,MOFLATTI,MOTIPATT,MOCODSER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODELLO)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStrODBC(trim(this.w_MODELLO)+"%");

            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON,MOFLFATT,MOFLATTI,MOTIPATT,MOCODSER";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStr(trim(this.w_MODELLO)+"%");

            select MOCODICE,MODESCRI,MOTIPCON,MOFLFATT,MOFLATTI,MOTIPATT,MOCODSER;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MODELLO) and !this.bDontReportError
            deferred_cp_zoom('MOD_ELEM','*','MOCODICE',cp_AbsName(oSource.parent,'oMODELLO_2_14'),i_cWhere,'',"Modelli",'GSAG_KZM.MOD_ELEM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON,MOFLFATT,MOFLATTI,MOTIPATT,MOCODSER";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI,MOTIPCON,MOFLFATT,MOFLATTI,MOTIPATT,MOCODSER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODELLO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON,MOFLFATT,MOFLATTI,MOTIPATT,MOCODSER";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_MODELLO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_MODELLO)
            select MOCODICE,MODESCRI,MOTIPCON,MOFLFATT,MOFLATTI,MOTIPATT,MOCODSER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODELLO = NVL(_Link_.MOCODICE,space(10))
      this.w_DESELE = NVL(_Link_.MODESCRI,space(60))
      this.w_MOTIPCON = NVL(_Link_.MOTIPCON,space(1))
      this.w_MOFLATT = NVL(_Link_.MOFLFATT,space(1))
      this.w_MOFLATTI = NVL(_Link_.MOFLATTI,space(1))
      this.w_MOTIPATT = NVL(_Link_.MOTIPATT,space(20))
      this.w_CODSERVL = NVL(_Link_.MOCODSER,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MODELLO = space(10)
      endif
      this.w_DESELE = space(60)
      this.w_MOTIPCON = space(1)
      this.w_MOFLATT = space(1)
      this.w_MOFLATTI = space(1)
      this.w_MOTIPATT = space(20)
      this.w_CODSERVL = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLTIPCON='T' OR .w_MOTIPCON=.w_FLTIPCON
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Modello inesistente o con tipologia errata")
        endif
        this.w_MODELLO = space(10)
        this.w_DESELE = space(60)
        this.w_MOTIPCON = space(1)
        this.w_MOFLATT = space(1)
        this.w_MOFLATTI = space(1)
        this.w_MOTIPATT = space(20)
        this.w_CODSERVL = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODELLO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_SERV
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_SERV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_COD_SERV)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_COD_SERV))
          select ARCODART,ARDESART,ARTIPART,ARUNMIS1;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COD_SERV)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_COD_SERV)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_COD_SERV)+"%");

            select ARCODART,ARDESART,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COD_SERV) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCOD_SERV_2_17'),i_cWhere,'GSMA_AAS',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_SERV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_COD_SERV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_COD_SERV)
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_SERV = NVL(_Link_.ARCODART,space(20))
      this.w_ARDESART = NVL(_Link_.ARDESART,space(40))
      this.w_ARTIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_ARUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_COD_SERV = space(20)
      endif
      this.w_ARDESART = space(40)
      this.w_ARTIPART = space(2)
      this.w_ARUNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTIPART='FM' OR .w_ARTIPART='FO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COD_SERV = space(20)
        this.w_ARDESART = space(40)
        this.w_ARTIPART = space(2)
        this.w_ARUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_SERV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_SERV
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_SERV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_COD_SERV)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_COD_SERV))
          select ARCODART,ARDESART,ARTIPART,ARUNMIS1;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COD_SERV)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_COD_SERV)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_COD_SERV)+"%");

            select ARCODART,ARDESART,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COD_SERV) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCOD_SERV_2_19'),i_cWhere,'GSMA_AAS',"Servizi",'GSMA3AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_SERV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_COD_SERV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_COD_SERV)
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_SERV = NVL(_Link_.ARCODART,space(20))
      this.w_ARDESART = NVL(_Link_.ARDESART,space(40))
      this.w_ARTIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_ARUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_COD_SERV = space(20)
      endif
      this.w_ARDESART = space(40)
      this.w_ARTIPART = space(2)
      this.w_ARUNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTIPART='FM' OR .w_MOTIPCON='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COD_SERV = space(20)
        this.w_ARDESART = space(40)
        this.w_ARTIPART = space(2)
        this.w_ARUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_SERV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPIMPKEYREAD
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPIMPKEYREAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPIMPKEYREAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COMPIMPKEYREAD);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO;
                       ,'CPROWNUM',this.w_COMPIMPKEYREAD)
            select IMCODICE,CPROWNUM,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPIMPKEYREAD = NVL(_Link_.CPROWNUM,0)
      this.w_COMPIMP = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COMPIMPKEYREAD = 0
      endif
      this.w_COMPIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPIMPKEYREAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOTIPATT
  func Link_2_69(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOTIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOTIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CAFLNSAP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MOTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MOTIPATT)
            select CACODICE,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOTIPATT = NVL(_Link_.CACODICE,space(20))
      this.w_CAFLNSAP = NVL(_Link_.CAFLNSAP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOTIPATT = space(20)
      endif
      this.w_CAFLNSAP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOTIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATAINIZIO1_1_5.value==this.w_DATAINIZIO1)
      this.oPgFrm.Page1.oPag.oDATAINIZIO1_1_5.value=this.w_DATAINIZIO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINIZIO2_1_6.value==this.w_DATAINIZIO2)
      this.oPgFrm.Page1.oPag.oDATAINIZIO2_1_6.value=this.w_DATAINIZIO2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFINE1_1_7.value==this.w_DATAFINE1)
      this.oPgFrm.Page1.oPag.oDATAFINE1_1_7.value=this.w_DATAFINE1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFINE2_1_8.value==this.w_DATAFINE2)
      this.oPgFrm.Page1.oPag.oDATAFINE2_1_8.value=this.w_DATAFINE2
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTIPCON_1_9.RadioValue()==this.w_FLTIPCON)
      this.oPgFrm.Page1.oPag.oFLTIPCON_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_25.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_25.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_26.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_26.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_ZOOM_1_29.value==this.w_CODESCON_ZOOM)
      this.oPgFrm.Page1.oPag.oCODESCON_ZOOM_1_29.value=this.w_CODESCON_ZOOM
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_ZOOM_1_30.value==this.w_IMDESCRI_ZOOM)
      this.oPgFrm.Page1.oPag.oIMDESCRI_ZOOM_1_30.value=this.w_IMDESCRI_ZOOM
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCON_1_32.value==this.w_IMDESCON)
      this.oPgFrm.Page1.oPag.oIMDESCON_1_32.value=this.w_IMDESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCODNOM_2_1.value==this.w_CODNOM)
      this.oPgFrm.Page2.oPag.oCODNOM_2_1.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRI_2_4.value==this.w_DESCRI)
      this.oPgFrm.Page2.oPag.oDESCRI_2_4.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODSED_2_5.value==this.w_CODSED)
      this.oPgFrm.Page2.oPag.oCODSED_2_5.value=this.w_CODSED
    endif
    if not(this.oPgFrm.Page2.oPag.oDDNOMDES_2_6.value==this.w_DDNOMDES)
      this.oPgFrm.Page2.oPag.oDDNOMDES_2_6.value=this.w_DDNOMDES
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCONTR_2_8.value==this.w_CODCONTR)
      this.oPgFrm.Page2.oPag.oCODCONTR_2_8.value=this.w_CODCONTR
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCONTR2_2_9.value==this.w_CODCONTR2)
      this.oPgFrm.Page2.oPag.oCODCONTR2_2_9.value=this.w_CODCONTR2
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPIANTO_2_10.value==this.w_IMPIANTO)
      this.oPgFrm.Page2.oPag.oIMPIANTO_2_10.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page2.oPag.oIMDESCRI_2_12.value==this.w_IMDESCRI)
      this.oPgFrm.Page2.oPag.oIMDESCRI_2_12.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMPIMP_2_13.value==this.w_COMPIMP)
      this.oPgFrm.Page2.oPag.oCOMPIMP_2_13.value=this.w_COMPIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oMODELLO_2_14.value==this.w_MODELLO)
      this.oPgFrm.Page2.oPag.oMODELLO_2_14.value=this.w_MODELLO
    endif
    if not(this.oPgFrm.Page2.oPag.oCOD_SERV_2_17.value==this.w_COD_SERV)
      this.oPgFrm.Page2.oPag.oCOD_SERV_2_17.value=this.w_COD_SERV
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_18.value==this.w_COMPIMPKEY)
      this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_18.value=this.w_COMPIMPKEY
    endif
    if not(this.oPgFrm.Page2.oPag.oCOD_SERV_2_19.value==this.w_COD_SERV)
      this.oPgFrm.Page2.oPag.oCOD_SERV_2_19.value=this.w_COD_SERV
    endif
    if not(this.oPgFrm.Page2.oPag.oFLCONDQT_2_21.RadioValue()==this.w_FLCONDQT)
      this.oPgFrm.Page2.oPag.oFLCONDQT_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLQTATOT_2_22.value==this.w_FLQTATOT)
      this.oPgFrm.Page2.oPag.oFLQTATOT_2_22.value=this.w_FLQTATOT
    endif
    if not(this.oPgFrm.Page2.oPag.oFLCONDQC_2_23.RadioValue()==this.w_FLCONDQC)
      this.oPgFrm.Page2.oPag.oFLCONDQC_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLQTACON_2_24.value==this.w_FLQTACON)
      this.oPgFrm.Page2.oPag.oFLQTACON_2_24.value=this.w_FLQTACON
    endif
    if not(this.oPgFrm.Page2.oPag.oFLCONDQR_2_25.RadioValue()==this.w_FLCONDQR)
      this.oPgFrm.Page2.oPag.oFLCONDQR_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLQTARES_2_26.value==this.w_FLQTARES)
      this.oPgFrm.Page2.oPag.oFLQTARES_2_26.value=this.w_FLQTARES
    endif
    if not(this.oPgFrm.Page2.oPag.oDATASTIPULA1_2_27.value==this.w_DATASTIPULA1)
      this.oPgFrm.Page2.oPag.oDATASTIPULA1_2_27.value=this.w_DATASTIPULA1
    endif
    if not(this.oPgFrm.Page2.oPag.oDATASTIPULA2_2_28.value==this.w_DATASTIPULA2)
      this.oPgFrm.Page2.oPag.oDATASTIPULA2_2_28.value=this.w_DATASTIPULA2
    endif
    if not(this.oPgFrm.Page2.oPag.oELEMENTI_2_29.RadioValue()==this.w_ELEMENTI)
      this.oPgFrm.Page2.oPag.oELEMENTI_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODESCON_2_31.value==this.w_CODESCON)
      this.oPgFrm.Page2.oPag.oCODESCON_2_31.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCODESCON2_2_33.value==this.w_CODESCON2)
      this.oPgFrm.Page2.oPag.oCODESCON2_2_33.value=this.w_CODESCON2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESELE_2_36.value==this.w_DESELE)
      this.oPgFrm.Page2.oPag.oDESELE_2_36.value=this.w_DESELE
    endif
    if not(this.oPgFrm.Page2.oPag.oDTPROFAT_2_41.RadioValue()==this.w_DTPROFAT)
      this.oPgFrm.Page2.oPag.oDTPROFAT_2_41.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFAT1_2_42.value==this.w_DATFAT1)
      this.oPgFrm.Page2.oPag.oDATFAT1_2_42.value=this.w_DATFAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFAT2_2_43.value==this.w_DATFAT2)
      this.oPgFrm.Page2.oPag.oDATFAT2_2_43.value=this.w_DATFAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDTPROATT_2_45.RadioValue()==this.w_DTPROATT)
      this.oPgFrm.Page2.oPag.oDTPROATT_2_45.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATATT1_2_47.value==this.w_DATATT1)
      this.oPgFrm.Page2.oPag.oDATATT1_2_47.value=this.w_DATATT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDATATT2_2_48.value==this.w_DATATT2)
      this.oPgFrm.Page2.oPag.oDATATT2_2_48.value=this.w_DATATT2
    endif
    if not(this.oPgFrm.Page1.oPag.oRICPE1_1_35.value==this.w_RICPE1)
      this.oPgFrm.Page1.oPag.oRICPE1_1_35.value=this.w_RICPE1
    endif
    if not(this.oPgFrm.Page1.oPag.oRICVA1_1_36.value==this.w_RICVA1)
      this.oPgFrm.Page1.oPag.oRICVA1_1_36.value=this.w_RICVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT1_1_37.value==this.w_ARROT1)
      this.oPgFrm.Page1.oPag.oARROT1_1_37.value=this.w_ARROT1
    endif
    if not(this.oPgFrm.Page1.oPag.oVALORIN_1_38.value==this.w_VALORIN)
      this.oPgFrm.Page1.oPag.oVALORIN_1_38.value=this.w_VALORIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT2_1_39.value==this.w_ARROT2)
      this.oPgFrm.Page1.oPag.oARROT2_1_39.value=this.w_ARROT2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR2IN_1_40.value==this.w_VALOR2IN)
      this.oPgFrm.Page1.oPag.oVALOR2IN_1_40.value=this.w_VALOR2IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT3_1_41.value==this.w_ARROT3)
      this.oPgFrm.Page1.oPag.oARROT3_1_41.value=this.w_ARROT3
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR3IN_1_42.value==this.w_VALOR3IN)
      this.oPgFrm.Page1.oPag.oVALOR3IN_1_42.value=this.w_VALOR3IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT4_1_43.value==this.w_ARROT4)
      this.oPgFrm.Page1.oPag.oARROT4_1_43.value=this.w_ARROT4
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOFIN_1_44.value==this.w_VALOFIN)
      this.oPgFrm.Page1.oPag.oVALOFIN_1_44.value=this.w_VALOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oRINCON_2_54.RadioValue()==this.w_RINCON)
      this.oPgFrm.Page2.oPag.oRINCON_2_54.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATDIS_2_56.value==this.w_DATDIS)
      this.oPgFrm.Page2.oPag.oDATDIS_2_56.value=this.w_DATDIS
    endif
    if not(this.oPgFrm.Page2.oPag.oARDESART_2_59.value==this.w_ARDESART)
      this.oPgFrm.Page2.oPag.oARDESART_2_59.value=this.w_ARDESART
    endif
    if not(this.oPgFrm.Page2.oPag.oARUNIMIS_2_60.value==this.w_ARUNIMIS)
      this.oPgFrm.Page2.oPag.oARUNIMIS_2_60.value=this.w_ARUNIMIS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATAINIZIO1 <= .w_DATAINIZIO2 OR EMPTY( .w_DATAINIZIO2 ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINIZIO1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_DATAINIZIO1 <= .w_DATAINIZIO2 OR EMPTY( .w_DATAINIZIO2 ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINIZIO2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_DATAFINE1 <= .w_DATAFINE2 OR EMPTY( .w_DATAFINE2 ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFINE1_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_DATAFINE1 <= .w_DATAFINE2 OR EMPTY( .w_DATAFINE2 ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFINE2_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_CODCONTR2  >= .w_CODCONTR OR EMPTY( .w_CODCONTR2 ))  and not(empty(.w_CODCONTR2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCONTR2_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FLTIPCON='T' OR .w_MOTIPCON=.w_FLTIPCON)  and not(empty(.w_MODELLO))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMODELLO_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Modello inesistente o con tipologia errata")
          case   not(.w_ARTIPART='FM' OR .w_ARTIPART='FO')  and not(.w_FLTIPCON<>'C' and .w_MOTIPCON<>'C' and not EMPTY(.w_MODELLO) )  and ((.w_FLTIPCON='C' and EMPTY(.w_MODELLO)) OR ((.w_MOFLATT='S' OR (.w_MOFLATTI='S'  AND .w_CAFLNSAP='N')) AND .w_MOTIPCON='C'))  and not(empty(.w_COD_SERV))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOD_SERV_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ARTIPART='FM' OR .w_MOTIPCON='C')  and not(.w_FLTIPCON<>'P' and .w_MOTIPCON<>'P')  and not(empty(.w_COD_SERV))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOD_SERV_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATASTIPULA1 <= .w_DATASTIPULA2 OR EMPTY( .w_DATASTIPULA2 ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATASTIPULA1_2_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_DATASTIPULA1 <= .w_DATASTIPULA2 OR EMPTY( .w_DATASTIPULA2 ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATASTIPULA2_2_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_DATFAT1 <= .w_DATFAT2 OR EMPTY( .w_DATFAT2 ))  and not(.w_DTPROFAT <> "S")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATFAT1_2_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_DATFAT1 <= .w_DATFAT2 OR EMPTY( .w_DATFAT2 ))  and not(.w_DTPROFAT <> "S")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATFAT2_2_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_DATATT1 <= .w_DATATT2 OR EMPTY( .w_DATATT2 ))  and not(.w_DTPROATT <> "S")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATATT1_2_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_DATATT1 <= .w_DATATT2 OR EMPTY( .w_DATATT2 ))  and not(.w_DTPROATT <> "S")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATATT2_2_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale precedente a data iniziale")
          case   not(.w_VALOR2IN>.w_VALORIN OR .w_VALOR2IN=0)  and not(g_APPLICATION <>"ADHOC REVOLUTION")  and (.w_VALORIN<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR2IN_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN) OR .w_VALOR3IN=0)  and not(g_APPLICATION <>"ADHOC REVOLUTION")  and (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR3IN_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLTIPCON = this.w_FLTIPCON
    this.o_CODNOM = this.w_CODNOM
    this.o_CODSED = this.w_CODSED
    this.o_CODCONTR = this.w_CODCONTR
    this.o_IMPIANTO = this.w_IMPIANTO
    this.o_COMPIMP = this.w_COMPIMP
    this.o_MODELLO = this.w_MODELLO
    this.o_COMPIMPKEY = this.w_COMPIMPKEY
    this.o_FLCONDQT = this.w_FLCONDQT
    this.o_FLCONDQC = this.w_FLCONDQC
    this.o_FLCONDQR = this.w_FLCONDQR
    return

enddefine

* --- Define pages as container
define class tgsag_kdcPag1 as StdContainer
  Width  = 904
  height = 562
  stdWidth  = 904
  stdheight = 562
  resizeXpos=531
  resizeYpos=136
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATAINIZIO1_1_5 as StdField with uid="BSFLRHLAVC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATAINIZIO1", cQueryName = "DATAINIZIO1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "Da data inizio validit�",;
    HelpContextID = 233816608,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=146, Top=5

  func oDATAINIZIO1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAINIZIO1 <= .w_DATAINIZIO2 OR EMPTY( .w_DATAINIZIO2 ))
    endwith
    return bRes
  endfunc

  add object oDATAINIZIO2_1_6 as StdField with uid="VXGENLVVEB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATAINIZIO2", cQueryName = "DATAINIZIO2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "A data inizio validit�",;
    HelpContextID = 233820704,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=370, Top=5

  func oDATAINIZIO2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAINIZIO1 <= .w_DATAINIZIO2 OR EMPTY( .w_DATAINIZIO2 ))
    endwith
    return bRes
  endfunc

  add object oDATAFINE1_1_7 as StdField with uid="NGZTRXDZBE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATAFINE1", cQueryName = "DATAFINE1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "Da data fine validit�",;
    HelpContextID = 121871989,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=146, Top=30

  func oDATAFINE1_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAFINE1 <= .w_DATAFINE2 OR EMPTY( .w_DATAFINE2 ))
    endwith
    return bRes
  endfunc

  add object oDATAFINE2_1_8 as StdField with uid="TPYRSSNFVT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATAFINE2", cQueryName = "DATAFINE2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "A data fine validit�",;
    HelpContextID = 121871973,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=370, Top=30

  func oDATAFINE2_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAFINE1 <= .w_DATAFINE2 OR EMPTY( .w_DATAFINE2 ))
    endwith
    return bRes
  endfunc


  add object oFLTIPCON_1_9 as StdCombo with uid="RXCBFDEGBK",rtseq=9,rtrep=.f.,left=576,top=5,width=146,height=21;
    , ToolTipText = "Tipo contratto";
    , HelpContextID = 211523164;
    , cFormVar="w_FLTIPCON",RowSource=""+"Canone,"+"Pacchetto,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLTIPCON_1_9.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'P',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLTIPCON_1_9.GetRadio()
    this.Parent.oContained.w_FLTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oFLTIPCON_1_9.SetRadio()
    this.Parent.oContained.w_FLTIPCON=trim(this.Parent.oContained.w_FLTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_FLTIPCON=='C',1,;
      iif(this.Parent.oContained.w_FLTIPCON=='P',2,;
      iif(this.Parent.oContained.w_FLTIPCON=='T',3,;
      0)))
  endfunc


  add object oBtn_1_10 as StdButton with uid="PPWFAOCXOY",left=760, top=10, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la ricerca";
    , HelpContextID = 94467862;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      this.parent.oContained.NotifyEvent("Search")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOM as cp_szoombox with uid="VZMOVSYNOQ",left=-2, top=57, width=818,height=251,;
    caption='ZOOM',;
   bGlobalFont=.t.,;
    cTable="ELE_CONT",bRetriveAllRows=.t.,cZoomFile="gsag_kvc",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Search",;
    nPag=1;
    , HelpContextID = 77062762


  add object oBtn_1_12 as StdButton with uid="WOOVXXVTWQ",left=7, top=311, width=48,height=45,;
    CpPicture="BMP\Check.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti gli elementi contratto";
    , HelpContextID = 21722074;
    , Caption='\<Sel. tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSAG_BDU(this.Parent.oContained,"SELEZ_NOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="NWXMVNCBXX",left=56, top=311, width=48,height=45,;
    CpPicture="BMP\UnCheck.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti gli elementi contratti";
    , HelpContextID = 256692742;
    , Caption='\<Desel. tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSAG_BDU(this.Parent.oContained,"DESEL_NOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="ZPJFCAWGCO",left=105, top=311, width=48,height=45,;
    CpPicture="BMP\InvCheck.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione degli elementi contratti";
    , HelpContextID = 5180482;
    , Caption='\<Inv. selez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSAG_BDU(this.Parent.oContained,"INVER_NOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc

  add object oMODESCRI_1_25 as StdField with uid="YUTQSCCACI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 59731215,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=284, Top=313, InputMask=replicate('X',50)

  add object oANDESCRI_1_26 as StdField with uid="ACNTZAUZOM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 59730767,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=284, Top=335, InputMask=replicate('X',50)

  add object oCODESCON_ZOOM_1_29 as StdField with uid="EWFOXIKFWQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODESCON_ZOOM", cQueryName = "CODESCON_ZOOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 122438556,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=284, Top=357, InputMask=replicate('X',50)

  add object oIMDESCRI_ZOOM_1_30 as StdField with uid="RWUOGMWYUL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_IMDESCRI_ZOOM", cQueryName = "IMDESCRI_ZOOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 145996479,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=284, Top=379, InputMask=replicate('X',50)

  add object oIMDESCON_1_32 as StdField with uid="FYHRCQDSWS",rtseq=22,rtrep=.f.,;
    cFormVar = "w_IMDESCON", cQueryName = "IMDESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208704812,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=284, Top=401, InputMask=replicate('X',50)

  add object oRICPE1_1_35 as StdField with uid="JCWLSRROBT",rtseq=58,rtrep=.f.,;
    cFormVar = "w_RICPE1", cQueryName = "RICPE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ricalcolo del contratto",;
    HelpContextID = 256223466,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=130, Top=461, cSayPict='"999.99"', cGetPict='"999.99"'

  func oRICPE1_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICVA1=0)
    endwith
   endif
  endfunc

  func oRICPE1_1_35.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oRICVA1_1_36 as StdField with uid="RAFGTLVTMZ",rtseq=59,rtrep=.f.,;
    cFormVar = "w_RICVA1", cQueryName = "RICVA1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ricalcolo in valore del contratto",;
    HelpContextID = 260024554,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=130, Top=486, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oRICVA1_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICPE1=0)
    endwith
   endif
  endfunc

  func oRICVA1_1_36.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oARROT1_1_37 as StdField with uid="RVCKWZFISF",rtseq=60,rtrep=.f.,;
    cFormVar = "w_ARROT1", cQueryName = "ARROT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento al valore monetario desiderato (decine, centinaia, ecc., 0=nessun arrotondamento)",;
    HelpContextID = 240496890,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=268, Top=461, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT1_1_37.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oVALORIN_1_38 as StdField with uid="MHNGXGXYBX",rtseq=61,rtrep=.f.,;
    cFormVar = "w_VALORIN", cQueryName = "VALORIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fino al quale si vuole arrotondare",;
    HelpContextID = 108404906,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=400, Top=461, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALORIN_1_38.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oARROT2_1_39 as StdField with uid="RHGJBSFYAZ",rtseq=62,rtrep=.f.,;
    cFormVar = "w_ARROT2", cQueryName = "ARROT2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento al valore monetario desiderato (decine, centinaia, ecc., 0=nessun arrotondamento)",;
    HelpContextID = 223719674,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=268, Top=486, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT2_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0)
    endwith
   endif
  endfunc

  func oARROT2_1_39.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oVALOR2IN_1_40 as StdField with uid="NMSIQUJYRO",rtseq=63,rtrep=.f.,;
    cFormVar = "w_VALOR2IN", cQueryName = "VALOR2IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fino al quale si vuole arrotondare",;
    HelpContextID = 42590116,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=400, Top=486, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALOR2IN_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0)
    endwith
   endif
  endfunc

  func oVALOR2IN_1_40.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oVALOR2IN_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VALOR2IN>.w_VALORIN OR .w_VALOR2IN=0)
    endwith
    return bRes
  endfunc

  add object oARROT3_1_41 as StdField with uid="HXTPQSAAPP",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ARROT3", cQueryName = "ARROT3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento al valore monetario desiderato (decine, centinaia, ecc., 0=nessun arrotondamento)",;
    HelpContextID = 206942458,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=268, Top=511, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT3_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
    endwith
   endif
  endfunc

  func oARROT3_1_41.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oVALOR3IN_1_42 as StdField with uid="GVJHJMJPXN",rtseq=65,rtrep=.f.,;
    cFormVar = "w_VALOR3IN", cQueryName = "VALOR3IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fino al quale si vuole arrotondare",;
    HelpContextID = 59367332,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=400, Top=511, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALOR3IN_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
    endwith
   endif
  endfunc

  func oVALOR3IN_1_42.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oVALOR3IN_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN) OR .w_VALOR3IN=0)
    endwith
    return bRes
  endfunc

  add object oARROT4_1_43 as StdField with uid="RPOAXMKGRV",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ARROT4", cQueryName = "ARROT4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento per importi diversi dai precedenti (0 = nessun arrotondamento)",;
    HelpContextID = 190165242,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=268, Top=536, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT4_1_43.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oVALOFIN_1_44 as StdField with uid="ZZMRLWXAIF",rtseq=67,rtrep=.f.,;
    cFormVar = "w_VALOFIN", cQueryName = "VALOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 120987818,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=536, Top=536, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALOFIN_1_44.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc


  add object oBtn_1_57 as StdButton with uid="PGFMVLGCKD",left=661, top=313, width=48,height=45,;
    CpPicture="BMP\ELABORA.BMP", caption="", nPag=1;
    , ToolTipText = "Rinnova gli elementi contratto selezionati";
    , HelpContextID = 142288662;
    , Caption='Ri\<nnova';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      with this.Parent.oContained
        GSAG_BDU(this.Parent.oContained,"DUPLICA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_58 as StdButton with uid="OXDOEQRTST",left=711, top=313, width=48,height=45,;
    CpPicture="BMP\ELIMINA.BMP", caption="", nPag=1;
    , ToolTipText = "Elimina gli elementi contratto selezionati";
    , HelpContextID = 1694022;
    , Caption='El\<imina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_58.Click()
      with this.Parent.oContained
        GSAG_BDU(this.Parent.oContained,"ELIMINA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_59 as StdButton with uid="RDXFXQLGGN",left=760, top=313, width=48,height=45,;
    CpPicture="BMP\visuali.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire l'elemento contratto selezionato";
    , HelpContextID = 75076346;
    , Caption='A\<pri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      this.parent.oContained.NotifyEvent("w_zoom selected")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_60 as StdButton with uid="KOTWJAFPMC",left=760, top=513, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75136954;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_23 as StdString with uid="JMNZWRVMBC",Visible=.t., Left=162, Top=313,;
    Alignment=1, Width=121, Height=18,;
    Caption="Descrizione modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ZPSOZWYUGS",Visible=.t., Left=166, Top=335,;
    Alignment=1, Width=117, Height=18,;
    Caption="Descrizione cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="EGSAJTEXEB",Visible=.t., Left=159, Top=357,;
    Alignment=1, Width=124, Height=18,;
    Caption="Descrizione contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="QCJADCOLDU",Visible=.t., Left=159, Top=379,;
    Alignment=1, Width=124, Height=18,;
    Caption="Descrizione impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HLGFFRTGDE",Visible=.t., Left=141, Top=401,;
    Alignment=1, Width=142, Height=18,;
    Caption="Descrizione componente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="LVHQKDPFPW",Visible=.t., Left=12, Top=30,;
    Alignment=1, Width=133, Height=18,;
    Caption="Da data fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="KAPMBOIKZD",Visible=.t., Left=239, Top=30,;
    Alignment=1, Width=130, Height=18,;
    Caption="A data fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="GOPKQRRHJT",Visible=.t., Left=3, Top=442,;
    Alignment=0, Width=252, Height=15,;
    Caption="Parametri di calcolo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="NYHQXBQHJL",Visible=.t., Left=0, Top=461,;
    Alignment=1, Width=128, Height=15,;
    Caption="Percentuale ricalcolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="SVPRRTBRIR",Visible=.t., Left=0, Top=486,;
    Alignment=1, Width=128, Height=15,;
    Caption="Ricalcolo in valore:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="JEFECLEULP",Visible=.t., Left=270, Top=442,;
    Alignment=0, Width=104, Height=15,;
    Caption="Arrotondamenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="MHEWZFMOQE",Visible=.t., Left=403, Top=443,;
    Alignment=0, Width=146, Height=15,;
    Caption="Per importi fino a"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="HFEZTPBADH",Visible=.t., Left=398, Top=536,;
    Alignment=1, Width=137, Height=15,;
    Caption="Per importi superiori a:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="LOGACFNMVI",Visible=.t., Left=12, Top=5,;
    Alignment=1, Width=133, Height=18,;
    Caption="Da data inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="FZLUJVDXVM",Visible=.t., Left=239, Top=5,;
    Alignment=1, Width=130, Height=18,;
    Caption="A data inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="EQHUPAEOEY",Visible=.t., Left=463, Top=5,;
    Alignment=1, Width=110, Height=18,;
    Caption="Tipo contratto:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsag_kdcPag2 as StdContainer
  Width  = 904
  height = 562
  stdWidth  = 904
  stdheight = 562
  resizeXpos=709
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODNOM_2_1 as StdField with uid="LIAOAVOJOL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Intestatario dell'elemento contratto",;
    HelpContextID = 44536794,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=137, Top=10, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODNOM"

  func oCODNOM_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
      if .not. empty(.w_CODSED)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODNOM_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODNOM_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCODNOM_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc

  add object oDESCRI_2_4 as StdField with uid="HJWQLVWINK",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 109161930,;
   bGlobalFont=.t.,;
    Height=21, Width=472, Left=259, Top=10, InputMask=replicate('X',60)

  add object oCODSED_2_5 as StdField with uid="PJTHIPRNQB",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODSED", cQueryName = "CODSED",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Sede dell'intestatario",;
    HelpContextID = 205689818,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=137, Top=35, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="DDCODICE", oKey_2_2="this.w_CODNOM", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODSED"

  func oCODSED_2_5.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  func oCODSED_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSED_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSED_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODNOM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODNOM)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODSED_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sedi",'',this.parent.oContained
  endproc

  add object oDDNOMDES_2_6 as StdField with uid="HBJELHDXJT",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DDNOMDES", cQueryName = "DDNOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 70910345,;
   bGlobalFont=.t.,;
    Height=21, Width=529, Left=202, Top=35, InputMask=replicate('X',40)

  func oDDNOMDES_2_6.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oCODCONTR_2_8 as StdField with uid="KKDTXAZCSX",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CODCONTR", cQueryName = "CODCONTR",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Contratto",;
    HelpContextID = 239955064,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=137, Top=60, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CON_TRAS", oKey_1_1="COSERIAL", oKey_1_2="this.w_CODCONTR"

  func oCODCONTR_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCONTR_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCONTR_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAS','*','COSERIAL',cp_AbsName(this.parent,'oCODCONTR_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Contratti d'assistenza",'GSAG_AEL.CON_TRAS_VZM',this.parent.oContained
  endproc

  add object oCODCONTR2_2_9 as StdField with uid="VDGOSEFGOP",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODCONTR2", cQueryName = "CODCONTR2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Contratto",;
    HelpContextID = 239955864,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=137, Top=86, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CON_TRAS", oKey_1_1="COSERIAL", oKey_1_2="this.w_CODCONTR2"

  func oCODCONTR2_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCONTR2_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCONTR2_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAS','*','COSERIAL',cp_AbsName(this.parent,'oCODCONTR2_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Contratti d'assistenza",'GSAG_AEL.CON_TRAS_VZM',this.parent.oContained
  endproc

  add object oIMPIANTO_2_10 as StdField with uid="PQQYIMXCVC",rtseq=30,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Impianto",;
    HelpContextID = 225716949,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=137, Top=112, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="GSAG_MIM", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_2_10.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  func oIMPIANTO_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
      if .not. empty(.w_COMPIMPKEYREAD)
        bRes2=.link_2_20('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIM',"Impianto",'GSAG_AE3.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oIMPIANTO_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_IMPIANTO
     i_obj.ecpSave()
  endproc

  add object oIMDESCRI_2_12 as StdField with uid="WZXLGTMRDA",rtseq=31,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 59730639,;
   bGlobalFont=.t.,;
    Height=21, Width=467, Left=265, Top=113, InputMask=replicate('X',50)

  add object oCOMPIMP_2_13 as StdField with uid="AKXQSRDOOH",rtseq=32,rtrep=.f.,;
    cFormVar = "w_COMPIMP", cQueryName = "COMPIMP",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Componente dell'impianto",;
    HelpContextID = 50660314,;
   bGlobalFont=.t.,;
    Height=21, Width=470, Left=137, Top=138, InputMask=replicate('X',50), bHasZoom = .t. 

  func oCOMPIMP_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_IMPIANTO))
    endwith
   endif
  endfunc

  func oCOMPIMP_2_13.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  proc oCOMPIMP_2_13.mBefore
    with this.Parent.oContained
      .w_OLDCOMP=.w_COMPIMP
    endwith
  endproc

  proc oCOMPIMP_2_13.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M")
      endwith
  endproc

  proc oCOMPIMP_2_13.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMODELLO_2_14 as StdField with uid="IQMNHREPEO",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MODELLO", cQueryName = "MODELLO",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Modello inesistente o con tipologia errata",;
    ToolTipText = "Modello elementi",;
    HelpContextID = 65049402,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=137, Top=164, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_ELEM", oKey_1_1="MOCODICE", oKey_1_2="this.w_MODELLO"

  func oMODELLO_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODELLO_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODELLO_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_ELEM','*','MOCODICE',cp_AbsName(this.parent,'oMODELLO_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Modelli",'GSAG_KZM.MOD_ELEM_VZM',this.parent.oContained
  endproc

  add object oCOD_SERV_2_17 as StdField with uid="MHCDHUMPOS",rtseq=36,rtrep=.f.,;
    cFormVar = "w_COD_SERV", cQueryName = "COD_SERV",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Servizio",;
    HelpContextID = 94989436,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=137, Top=192, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_COD_SERV"

  func oCOD_SERV_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLTIPCON='C' and EMPTY(.w_MODELLO)) OR ((.w_MOFLATT='S' OR (.w_MOFLATTI='S'  AND .w_CAFLNSAP='N')) AND .w_MOTIPCON='C'))
    endwith
   endif
  endfunc

  func oCOD_SERV_2_17.mHide()
    with this.Parent.oContained
      return (.w_FLTIPCON<>'C' and .w_MOTIPCON<>'C' and not EMPTY(.w_MODELLO) )
    endwith
  endfunc

  func oCOD_SERV_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOD_SERV_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOD_SERV_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCOD_SERV_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCOD_SERV_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_COD_SERV
     i_obj.ecpSave()
  endproc

  add object oCOMPIMPKEY_2_18 as StdField with uid="SBSCAGJTGI",rtseq=37,rtrep=.f.,;
    cFormVar = "w_COMPIMPKEY", cQueryName = "COMPIMPKEY",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50636351,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=805, Top=-40

  func oCOMPIMPKEY_2_18.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oCOD_SERV_2_19 as StdField with uid="APDMWUSPFI",rtseq=38,rtrep=.f.,;
    cFormVar = "w_COD_SERV", cQueryName = "COD_SERV",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 94989436,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=137, Top=192, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_COD_SERV"

  func oCOD_SERV_2_19.mHide()
    with this.Parent.oContained
      return (.w_FLTIPCON<>'P' and .w_MOTIPCON<>'P')
    endwith
  endfunc

  func oCOD_SERV_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOD_SERV_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOD_SERV_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCOD_SERV_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSMA3AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCOD_SERV_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_COD_SERV
     i_obj.ecpSave()
  endproc


  add object oFLCONDQT_2_21 as StdCombo with uid="UTHDLCYUQJ",rtseq=40,rtrep=.f.,left=323,top=218,width=124,height=21;
    , ToolTipText = "Quantit� totale";
    , HelpContextID = 196519510;
    , cFormVar="w_FLCONDQT",RowSource=""+"Non utilizzato,"+"Maggiore di,"+"Maggiore o uguale a,"+"Uguale a,"+"Minore o uguale a ,"+"Minore di", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLCONDQT_2_21.RadioValue()
    return(iif(this.value =1,'NU',;
    iif(this.value =2,'>',;
    iif(this.value =3,'>=',;
    iif(this.value =4,'=',;
    iif(this.value =5,'<=',;
    iif(this.value =6,'<',;
    space(2))))))))
  endfunc
  func oFLCONDQT_2_21.GetRadio()
    this.Parent.oContained.w_FLCONDQT = this.RadioValue()
    return .t.
  endfunc

  func oFLCONDQT_2_21.SetRadio()
    this.Parent.oContained.w_FLCONDQT=trim(this.Parent.oContained.w_FLCONDQT)
    this.value = ;
      iif(this.Parent.oContained.w_FLCONDQT=='NU',1,;
      iif(this.Parent.oContained.w_FLCONDQT=='>',2,;
      iif(this.Parent.oContained.w_FLCONDQT=='>=',3,;
      iif(this.Parent.oContained.w_FLCONDQT=='=',4,;
      iif(this.Parent.oContained.w_FLCONDQT=='<=',5,;
      iif(this.Parent.oContained.w_FLCONDQT=='<',6,;
      0))))))
  endfunc

  func oFLCONDQT_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLTIPCON='P' AND EMPTY(.w_MODELLO) OR .w_MOTIPCON='P'))
    endwith
   endif
  endfunc

  add object oFLQTATOT_2_22 as StdField with uid="KVWJPOJAXL",rtseq=41,rtrep=.f.,;
    cFormVar = "w_FLQTATOT", cQueryName = "FLQTATOT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� totale",;
    HelpContextID = 209765974,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=452, Top=218, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  func oFLQTATOT_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCONDQT<>'NU' AND (.w_FLTIPCON='P' AND EMPTY(.w_MODELLO) OR .w_MOTIPCON='P'))
    endwith
   endif
  endfunc


  add object oFLCONDQC_2_23 as StdCombo with uid="GLRXRWZHQF",rtseq=42,rtrep=.f.,left=323,top=244,width=124,height=21;
    , ToolTipText = "Quantit� consumata";
    , HelpContextID = 196519527;
    , cFormVar="w_FLCONDQC",RowSource=""+"Non utilizzato,"+"Maggiore di,"+"Maggiore o uguale a,"+"Uguale a,"+"Minore o uguale a ,"+"Minore di", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLCONDQC_2_23.RadioValue()
    return(iif(this.value =1,'NU',;
    iif(this.value =2,'>',;
    iif(this.value =3,'>=',;
    iif(this.value =4,'=',;
    iif(this.value =5,'<=',;
    iif(this.value =6,'<',;
    space(2))))))))
  endfunc
  func oFLCONDQC_2_23.GetRadio()
    this.Parent.oContained.w_FLCONDQC = this.RadioValue()
    return .t.
  endfunc

  func oFLCONDQC_2_23.SetRadio()
    this.Parent.oContained.w_FLCONDQC=trim(this.Parent.oContained.w_FLCONDQC)
    this.value = ;
      iif(this.Parent.oContained.w_FLCONDQC=='NU',1,;
      iif(this.Parent.oContained.w_FLCONDQC=='>',2,;
      iif(this.Parent.oContained.w_FLCONDQC=='>=',3,;
      iif(this.Parent.oContained.w_FLCONDQC=='=',4,;
      iif(this.Parent.oContained.w_FLCONDQC=='<=',5,;
      iif(this.Parent.oContained.w_FLCONDQC=='<',6,;
      0))))))
  endfunc

  func oFLCONDQC_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLTIPCON='P' AND EMPTY(.w_MODELLO) OR .w_MOTIPCON='P'))
    endwith
   endif
  endfunc

  add object oFLQTACON_2_24 as StdField with uid="POBFYYVMBC",rtseq=43,rtrep=.f.,;
    cFormVar = "w_FLQTACON", cQueryName = "FLQTACON",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� consumata",;
    HelpContextID = 226543196,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=452, Top=244, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  func oFLQTACON_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCONDQC<>'NU' AND (.w_FLTIPCON='P' AND EMPTY(.w_MODELLO) OR .w_MOTIPCON='P'))
    endwith
   endif
  endfunc


  add object oFLCONDQR_2_25 as StdCombo with uid="POSSCOCFAF",rtseq=44,rtrep=.f.,left=323,top=270,width=124,height=21;
    , ToolTipText = "Quantit� residua";
    , HelpContextID = 196519512;
    , cFormVar="w_FLCONDQR",RowSource=""+"Non utilizzato,"+"Maggiore di,"+"Maggiore o uguale a,"+"Uguale a,"+"Minore o uguale a ,"+"Minore di", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLCONDQR_2_25.RadioValue()
    return(iif(this.value =1,'NU',;
    iif(this.value =2,'>',;
    iif(this.value =3,'>=',;
    iif(this.value =4,'=',;
    iif(this.value =5,'<=',;
    iif(this.value =6,'<',;
    space(2))))))))
  endfunc
  func oFLCONDQR_2_25.GetRadio()
    this.Parent.oContained.w_FLCONDQR = this.RadioValue()
    return .t.
  endfunc

  func oFLCONDQR_2_25.SetRadio()
    this.Parent.oContained.w_FLCONDQR=trim(this.Parent.oContained.w_FLCONDQR)
    this.value = ;
      iif(this.Parent.oContained.w_FLCONDQR=='NU',1,;
      iif(this.Parent.oContained.w_FLCONDQR=='>',2,;
      iif(this.Parent.oContained.w_FLCONDQR=='>=',3,;
      iif(this.Parent.oContained.w_FLCONDQR=='=',4,;
      iif(this.Parent.oContained.w_FLCONDQR=='<=',5,;
      iif(this.Parent.oContained.w_FLCONDQR=='<',6,;
      0))))))
  endfunc

  func oFLCONDQR_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLTIPCON='P' AND EMPTY(.w_MODELLO) OR .w_MOTIPCON='P'))
    endwith
   endif
  endfunc

  add object oFLQTARES_2_26 as StdField with uid="QXVJFLRUJF",rtseq=45,rtrep=.f.,;
    cFormVar = "w_FLQTARES", cQueryName = "FLQTARES",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� residua",;
    HelpContextID = 25115049,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=452, Top=270, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  func oFLQTARES_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCONDQR<>'NU' AND (.w_FLTIPCON='P' AND EMPTY(.w_MODELLO) OR .w_MOTIPCON='P'))
    endwith
   endif
  endfunc

  add object oDATASTIPULA1_2_27 as StdField with uid="QCISDZKHXV",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DATASTIPULA1", cQueryName = "DATASTIPULA1",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "Da data stipula",;
    HelpContextID = 188629034,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=137, Top=303

  func oDATASTIPULA1_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATASTIPULA1 <= .w_DATASTIPULA2 OR EMPTY( .w_DATASTIPULA2 ))
    endwith
    return bRes
  endfunc

  add object oDATASTIPULA2_2_28 as StdField with uid="JPSETZYKDZ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DATASTIPULA2", cQueryName = "DATASTIPULA2",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "A data stipula",;
    HelpContextID = 188563498,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=323, Top=302

  func oDATASTIPULA2_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATASTIPULA1 <= .w_DATASTIPULA2 OR EMPTY( .w_DATASTIPULA2 ))
    endwith
    return bRes
  endfunc


  add object oELEMENTI_2_29 as StdCombo with uid="UUHOQWQHAY",rtseq=48,rtrep=.f.,left=137,top=332,width=146,height=21;
    , ToolTipText = "Permette la visualizzazione dei soli elementi contratto attivi, non attivi (con data prossimo documento e data prossima attivit� vuote) oppure tutti";
    , HelpContextID = 230128015;
    , cFormVar="w_ELEMENTI",RowSource=""+"Attivi,"+"Non attivi,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oELEMENTI_2_29.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oELEMENTI_2_29.GetRadio()
    this.Parent.oContained.w_ELEMENTI = this.RadioValue()
    return .t.
  endfunc

  func oELEMENTI_2_29.SetRadio()
    this.Parent.oContained.w_ELEMENTI=trim(this.Parent.oContained.w_ELEMENTI)
    this.value = ;
      iif(this.Parent.oContained.w_ELEMENTI=='A',1,;
      iif(this.Parent.oContained.w_ELEMENTI=='N',2,;
      iif(this.Parent.oContained.w_ELEMENTI=='E',3,;
      0)))
  endfunc

  add object oCODESCON_2_31 as StdField with uid="HCMULJMVEV",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208704396,;
   bGlobalFont=.t.,;
    Height=21, Width=493, Left=238, Top=60, InputMask=replicate('X',50)

  add object oCODESCON2_2_33 as StdField with uid="HXBNKQFSGX",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CODESCON2", cQueryName = "CODESCON2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208703596,;
   bGlobalFont=.t.,;
    Height=21, Width=494, Left=238, Top=86, InputMask=replicate('X',50)

  add object oDESELE_2_36 as StdField with uid="VZTKENKUDW",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESELE", cQueryName = "DESELE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 182431178,;
   bGlobalFont=.t.,;
    Height=21, Width=493, Left=238, Top=164, InputMask=replicate('X',60)


  add object oDTPROFAT_2_41 as StdCombo with uid="PIMFBDUMOD",rtseq=52,rtrep=.f.,left=137,top=358,width=146,height=21;
    , ToolTipText = "Filtro su data prossimo documento";
    , HelpContextID = 161664630;
    , cFormVar="w_DTPROFAT",RowSource=""+"Tutti,"+"Vuota,"+"Valorizzata", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDTPROFAT_2_41.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oDTPROFAT_2_41.GetRadio()
    this.Parent.oContained.w_DTPROFAT = this.RadioValue()
    return .t.
  endfunc

  func oDTPROFAT_2_41.SetRadio()
    this.Parent.oContained.w_DTPROFAT=trim(this.Parent.oContained.w_DTPROFAT)
    this.value = ;
      iif(this.Parent.oContained.w_DTPROFAT=='T',1,;
      iif(this.Parent.oContained.w_DTPROFAT=='N',2,;
      iif(this.Parent.oContained.w_DTPROFAT=='S',3,;
      0)))
  endfunc

  add object oDATFAT1_2_42 as StdField with uid="KVVIBWZFPE",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DATFAT1", cQueryName = "DATFAT1",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "Da data di prossimo documento",;
    HelpContextID = 57761334,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=427, Top=357

  func oDATFAT1_2_42.mHide()
    with this.Parent.oContained
      return (.w_DTPROFAT <> "S")
    endwith
  endfunc

  func oDATFAT1_2_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFAT1 <= .w_DATFAT2 OR EMPTY( .w_DATFAT2 ))
    endwith
    return bRes
  endfunc

  add object oDATFAT2_2_43 as StdField with uid="TQGDVWOMGX",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DATFAT2", cQueryName = "DATFAT2",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "A data di prossimo documento",;
    HelpContextID = 57761334,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=620, Top=356

  func oDATFAT2_2_43.mHide()
    with this.Parent.oContained
      return (.w_DTPROFAT <> "S")
    endwith
  endfunc

  func oDATFAT2_2_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFAT1 <= .w_DATFAT2 OR EMPTY( .w_DATFAT2 ))
    endwith
    return bRes
  endfunc


  add object oDTPROATT_2_45 as StdCombo with uid="MXZPVMEJLC",rtseq=55,rtrep=.f.,left=137,top=384,width=146,height=21;
    , ToolTipText = "Filtro su data prossima attivit�";
    , HelpContextID = 22884746;
    , cFormVar="w_DTPROATT",RowSource=""+"Tutti,"+"Vuota,"+"Valorizzata", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDTPROATT_2_45.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oDTPROATT_2_45.GetRadio()
    this.Parent.oContained.w_DTPROATT = this.RadioValue()
    return .t.
  endfunc

  func oDTPROATT_2_45.SetRadio()
    this.Parent.oContained.w_DTPROATT=trim(this.Parent.oContained.w_DTPROATT)
    this.value = ;
      iif(this.Parent.oContained.w_DTPROATT=='T',1,;
      iif(this.Parent.oContained.w_DTPROATT=='N',2,;
      iif(this.Parent.oContained.w_DTPROATT=='S',3,;
      0)))
  endfunc

  add object oDATATT1_2_47 as StdField with uid="TVFKQALMGQ",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DATATT1", cQueryName = "DATATT1",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "Da data di prossima attivit�",;
    HelpContextID = 77356598,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=427, Top=384

  func oDATATT1_2_47.mHide()
    with this.Parent.oContained
      return (.w_DTPROATT <> "S")
    endwith
  endfunc

  func oDATATT1_2_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATATT1 <= .w_DATATT2 OR EMPTY( .w_DATATT2 ))
    endwith
    return bRes
  endfunc

  add object oDATATT2_2_48 as StdField with uid="EWKECICIMG",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DATATT2", cQueryName = "DATATT2",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale precedente a data iniziale",;
    ToolTipText = "A data di prossima attivit�",;
    HelpContextID = 77356598,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=620, Top=383

  func oDATATT2_2_48.mHide()
    with this.Parent.oContained
      return (.w_DTPROATT <> "S")
    endwith
  endfunc

  func oDATATT2_2_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATATT1 <= .w_DATATT2 OR EMPTY( .w_DATATT2 ))
    endwith
    return bRes
  endfunc


  add object oBtn_2_52 as StdButton with uid="WXFGUHODNH",left=734, top=10, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per avviare la ricerca";
    , HelpContextID = 94467862;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_52.Click()
      this.parent.oContained.NotifyEvent("Search")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oRINCON_2_54 as StdCombo with uid="YOKBGWFKCL",rtseq=69,rtrep=.f.,left=137,top=410,width=146,height=21;
    , ToolTipText = "Filtro su flag di rinnovo tacito";
    , HelpContextID = 28440810;
    , cFormVar="w_RINCON",RowSource=""+"Tutti,"+"S�,"+"No", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRINCON_2_54.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oRINCON_2_54.GetRadio()
    this.Parent.oContained.w_RINCON = this.RadioValue()
    return .t.
  endfunc

  func oRINCON_2_54.SetRadio()
    this.Parent.oContained.w_RINCON=trim(this.Parent.oContained.w_RINCON)
    this.value = ;
      iif(this.Parent.oContained.w_RINCON=='T',1,;
      iif(this.Parent.oContained.w_RINCON=='S',2,;
      iif(this.Parent.oContained.w_RINCON=='N',3,;
      0)))
  endfunc

  add object oDATDIS_2_56 as StdField with uid="GZXYVBBPVY",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DATDIS", cQueryName = "DATDIS",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data limite disdetta",;
    HelpContextID = 219193802,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=427, Top=410

  func oDATDIS_2_56.mHide()
    with this.Parent.oContained
      return (.w_RINCON <> "S")
    endwith
  endfunc

  add object oARDESART_2_59 as StdField with uid="LRFGHBDEAP",rtseq=73,rtrep=.f.,;
    cFormVar = "w_ARDESART", cQueryName = "ARDESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26177370,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=323, Top=192, InputMask=replicate('X',40)

  add object oARUNIMIS_2_60 as StdField with uid="YFCJHHIQRQ",rtseq=74,rtrep=.f.,;
    cFormVar = "w_ARUNIMIS", cQueryName = "ARUNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "1^ Unit� di misura",;
    HelpContextID = 217677657,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=137, Top=218, InputMask=replicate('X',3)


  add object oBtn_2_72 as StdButton with uid="MNVZAMDXTB",left=239, top=113, width=22,height=21,;
    CpPicture="bmp\ZOOM.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per ricercare per attributo";
    , HelpContextID = 82253354;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_2_72.Click()
      do gsag_kir with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_72.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsALT())
     endwith
    endif
  endfunc

  add object oStr_2_3 as StdString with uid="DYFIOYDNWL",Visible=.t., Left=4, Top=12,;
    Alignment=1, Width=129, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="KMREVKLXHS",Visible=.t., Left=4, Top=36,;
    Alignment=1, Width=129, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  func oStr_2_7.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="AFCZQNHPWC",Visible=.t., Left=4, Top=114,;
    Alignment=1, Width=129, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_2_30 as StdString with uid="AURUTIHBUT",Visible=.t., Left=4, Top=139,;
    Alignment=1, Width=129, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_2_30.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="HTHJPRBQMP",Visible=.t., Left=4, Top=62,;
    Alignment=1, Width=129, Height=18,;
    Caption="Da contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="XSNAXSWHXP",Visible=.t., Left=4, Top=88,;
    Alignment=1, Width=129, Height=18,;
    Caption="A contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="FAIWABLLUD",Visible=.t., Left=4, Top=164,;
    Alignment=1, Width=129, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="MUQLXUVPXG",Visible=.t., Left=4, Top=304,;
    Alignment=1, Width=129, Height=18,;
    Caption="Da data stipula:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="EIIBWGGMIT",Visible=.t., Left=218, Top=304,;
    Alignment=1, Width=102, Height=18,;
    Caption="A data stipula:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="ZZGTACWPWG",Visible=.t., Left=4, Top=359,;
    Alignment=1, Width=129, Height=18,;
    Caption="Prossimo doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="IEJTZCQICI",Visible=.t., Left=4, Top=386,;
    Alignment=1, Width=129, Height=18,;
    Caption="Prossima attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="TAXMXKZREW",Visible=.t., Left=321, Top=358,;
    Alignment=1, Width=102, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  func oStr_2_44.mHide()
    with this.Parent.oContained
      return (.w_DTPROFAT <> "S")
    endwith
  endfunc

  add object oStr_2_46 as StdString with uid="KLQBDBNCXS",Visible=.t., Left=514, Top=358,;
    Alignment=1, Width=102, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  func oStr_2_46.mHide()
    with this.Parent.oContained
      return (.w_DTPROFAT <> "S")
    endwith
  endfunc

  add object oStr_2_49 as StdString with uid="FLZMCXAGUF",Visible=.t., Left=321, Top=385,;
    Alignment=1, Width=102, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  func oStr_2_49.mHide()
    with this.Parent.oContained
      return (.w_DTPROATT <> "S")
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="RIFXIFOQPZ",Visible=.t., Left=514, Top=385,;
    Alignment=1, Width=102, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (.w_DTPROATT <> "S")
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="DUWSNRKNWA",Visible=.t., Left=4, Top=333,;
    Alignment=1, Width=129, Height=18,;
    Caption="Elementi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_53 as StdString with uid="GYMADPHXZP",Visible=.t., Left=4, Top=411,;
    Alignment=1, Width=129, Height=18,;
    Caption="Rinnovo tacito:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="UWOPUQAZWV",Visible=.t., Left=292, Top=414,;
    Alignment=1, Width=131, Height=18,;
    Caption="Data limite disdetta:"  ;
  , bGlobalFont=.t.

  func oStr_2_55.mHide()
    with this.Parent.oContained
      return (.w_RINCON <> "S")
    endwith
  endfunc

  add object oStr_2_61 as StdString with uid="FZNBHXHGZN",Visible=.t., Left=0, Top=191,;
    Alignment=1, Width=133, Height=18,;
    Caption="Servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_62 as StdString with uid="NCESOVBDJW",Visible=.t., Left=0, Top=218,;
    Alignment=1, Width=133, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_63 as StdString with uid="LBJKEQCMWA",Visible=.t., Left=178, Top=221,;
    Alignment=1, Width=142, Height=18,;
    Caption="Quantit� totale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_64 as StdString with uid="ACBKDFRERX",Visible=.t., Left=178, Top=246,;
    Alignment=1, Width=142, Height=18,;
    Caption="Quantit� consumata:"  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="FZGWRCEVSB",Visible=.t., Left=178, Top=271,;
    Alignment=1, Width=142, Height=18,;
    Caption="Quantit� residua:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kdc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
