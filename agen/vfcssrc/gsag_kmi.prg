* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kmi                                                        *
*              Inserimento multiplo impianti/componenti                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-12                                                      *
* Last revis.: 2015-07-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kmi",oParentObject))

* --- Class definition
define class tgsag_kmi as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 899
  Height = 586
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-20"
  HelpContextID=68540567
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  IMP_MAST_IDX = 0
  OFF_NOMI_IDX = 0
  DES_DIVE_IDX = 0
  CONTI_IDX = 0
  IMP_DETT_IDX = 0
  FAM_ATTR_IDX = 0
  cPrg = "gsag_kmi"
  cComment = "Inserimento multiplo impianti/componenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_TIPCON = space(1)
  w_ATCODNOM = space(15)
  w_CODCLI = space(15)
  o_CODCLI = space(15)
  w_ATCODSED = space(5)
  w_DesNomin = space(60)
  w_NOMDES = space(40)
  w_TIPRIF = space(2)
  w_FLSELE = 0
  w_IMPIANTO = space(10)
  o_IMPIANTO = space(10)
  w_DESCRI = space(50)
  w_RESCHK = 0
  w_COMPIMP = space(50)
  o_COMPIMP = space(50)
  w_DATFIN = ctod('  /  /  ')
  w_COMPIMPKEYREAD = 0
  w_COMPIMPKEY = 0
  o_COMPIMPKEY = 0
  w_DESIMP = space(50)
  w_OLDCOMP = space(10)
  w_RICERCA = space(50)
  w_ATTRIBUTO = space(10)
  w_FRDESCRI = space(40)
  w_IMDESCRI = space(50)
  w_ANDESCRI = space(30)
  w_DESCRIMODELLO = space(40)
  w_DESCRGRUPPO = space(40)
  w_DESCRFAMIGLIA = space(40)
  w_zoom_imp = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kmiPag1","gsag_kmi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCLI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_zoom_imp = this.oPgFrm.Pages(1).oPag.zoom_imp
    DoDefault()
    proc Destroy()
      this.w_zoom_imp = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='IMP_MAST'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='DES_DIVE'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='IMP_DETT'
    this.cWorkTables[6]='FAM_ATTR'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_ATCODNOM=space(15)
      .w_CODCLI=space(15)
      .w_ATCODSED=space(5)
      .w_DesNomin=space(60)
      .w_NOMDES=space(40)
      .w_TIPRIF=space(2)
      .w_FLSELE=0
      .w_IMPIANTO=space(10)
      .w_DESCRI=space(50)
      .w_RESCHK=0
      .w_COMPIMP=space(50)
      .w_DATFIN=ctod("  /  /  ")
      .w_COMPIMPKEYREAD=0
      .w_COMPIMPKEY=0
      .w_DESIMP=space(50)
      .w_OLDCOMP=space(10)
      .w_RICERCA=space(50)
      .w_ATTRIBUTO=space(10)
      .w_FRDESCRI=space(40)
      .w_IMDESCRI=space(50)
      .w_ANDESCRI=space(30)
      .w_DESCRIMODELLO=space(40)
      .w_DESCRGRUPPO=space(40)
      .w_DESCRFAMIGLIA=space(40)
      .w_ATCODNOM=oParentObject.w_ATCODNOM
      .w_ATCODSED=oParentObject.w_ATCODSED
      .w_NOMDES=oParentObject.w_NOMDES
        .w_OBTEST = i_DATSYS
        .w_TIPCON = 'C'
          .DoRTCalc(3,3,.f.)
        .w_CODCLI = this.oparentobject.w_CODCLI
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODCLI))
          .link_1_5('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_ATCODSED))
          .link_1_6('Full')
        endif
      .oPgFrm.Page1.oPag.zoom_imp.Calculate()
        .w_DesNomin = this.oparentobject.w_DESNOMIN
          .DoRTCalc(7,8,.f.)
        .w_FLSELE = 0
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_IMPIANTO))
          .link_1_16('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_RESCHK = 0
          .DoRTCalc(13,13,.f.)
        .w_DATFIN = THIS.OPARENTOBJECT.w_DATFIN
        .w_COMPIMPKEYREAD = .w_COMPIMPKEY
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_COMPIMPKEYREAD))
          .link_1_24('Full')
        endif
        .w_COMPIMPKEY = 0
        .w_DESIMP = .w_IMDESCRI
        .DoRTCalc(18,20,.f.)
        if not(empty(.w_ATTRIBUTO))
          .link_1_31('Full')
        endif
          .DoRTCalc(21,21,.f.)
        .w_IMDESCRI = .w_zoom_imp.GETVAR('IMDESCRI')
        .w_ANDESCRI = .w_zoom_imp.GETVAR('ANDESCRI')
        .w_DESCRIMODELLO = .w_zoom_imp.GETVAR('MADESCRI')
        .w_DESCRGRUPPO = .w_zoom_imp.GETVAR('GRDESCRI')
        .w_DESCRFAMIGLIA = .w_zoom_imp.GETVAR('FRDESCRI')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ATCODNOM=.w_ATCODNOM
      .oParentObject.w_ATCODSED=.w_ATCODSED
      .oParentObject.w_NOMDES=.w_NOMDES
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_TIPCON = 'C'
        .DoRTCalc(3,4,.t.)
        if .o_CODCLI<>.w_CODCLI
            .w_ATCODSED = IIF(NOT EMPTY(.w_CODCLI),.w_ATCODSED,'')
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.zoom_imp.Calculate()
        .DoRTCalc(6,13,.t.)
            .w_DATFIN = THIS.OPARENTOBJECT.w_DATFIN
        if .o_IMPIANTO<>.w_IMPIANTO.or. .o_COMPIMPKEY<>.w_COMPIMPKEY
            .w_COMPIMPKEYREAD = .w_COMPIMPKEY
          .link_1_24('Full')
        endif
        if .o_COMPIMP<>.w_COMPIMP.or. .o_IMPIANTO<>.w_IMPIANTO
            .w_COMPIMPKEY = 0
        endif
            .w_DESIMP = .w_IMDESCRI
        .DoRTCalc(18,21,.t.)
            .w_IMDESCRI = .w_zoom_imp.GETVAR('IMDESCRI')
            .w_ANDESCRI = .w_zoom_imp.GETVAR('ANDESCRI')
            .w_DESCRIMODELLO = .w_zoom_imp.GETVAR('MADESCRI')
            .w_DESCRGRUPPO = .w_zoom_imp.GETVAR('GRDESCRI')
            .w_DESCRFAMIGLIA = .w_zoom_imp.GETVAR('FRDESCRI')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.zoom_imp.Calculate()
    endwith
  return

  proc Calculate_QTYFFRAAXR()
    with this
          * --- Inserimento impianti multiplo
          GSAG_BIC(this;
             )
    endwith
  endproc
  proc Calculate_BWBOIOUNMQ()
    with this
          * --- Sbianca impianto
          .w_IMPIANTO = SPACE( 10 )
          .w_COMPIMP = SPACE( 50 )
          .w_IMDESCRI = SPACE( 50 )
          .w_COMPIMPKEY = 0
          .w_RICERCA = ''
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATCODSED_1_6.enabled = this.oPgFrm.Page1.oPag.oATCODSED_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_25.visible=!this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_25.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.zoom_imp.Event(cEvent)
        if lower(cEvent)==lower("InsImpianti")
          .Calculate_QTYFFRAAXR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CODCLI Changed")
          .Calculate_BWBOIOUNMQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCLI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLI_1_5'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DesNomin = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_DesNomin = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODSED
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_ATCODSED)+"%");
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);

          i_ret=cp_SQL(i_nConn,"select DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDCODICE',this.w_CODCLI;
                     ,'DDCODDES',trim(this.w_ATCODSED))
          select DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODSED)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODSED) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oATCODSED_1_6'),i_cWhere,'',"Sedi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDCODICE',oSource.xKey(1);
                       ,'DDCODDES',oSource.xKey(2))
            select DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_ATCODSED);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDCODICE',this.w_CODCLI;
                       ,'DDCODDES',this.w_ATCODSED)
            select DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODSED = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(40))
      this.w_TIPRIF = NVL(_Link_.DDTIPRIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODSED = space(5)
      endif
      this.w_NOMDES = space(40)
      this.w_TIPRIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIM',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_1_16'),i_cWhere,'GSAG_MIM',"Impianti",'GSAG1KMI.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_DESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_DESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPIMPKEYREAD
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPIMPKEYREAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPIMPKEYREAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COMPIMPKEYREAD);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO;
                       ,'CPROWNUM',this.w_COMPIMPKEYREAD)
            select IMCODICE,CPROWNUM,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPIMPKEYREAD = NVL(_Link_.CPROWNUM,0)
      this.w_COMPIMP = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COMPIMPKEYREAD = 0
      endif
      this.w_COMPIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPIMPKEYREAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTRIBUTO
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_lTable = "FAM_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2], .t., this.FAM_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTRIBUTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AAT',True,'FAM_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FRCODICE like "+cp_ToStrODBC(trim(this.w_ATTRIBUTO)+"%");

          i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FRCODICE',trim(this.w_ATTRIBUTO))
          select FRCODICE,FRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTRIBUTO)==trim(_Link_.FRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FRDESCRI like "+cp_ToStrODBC(trim(this.w_ATTRIBUTO)+"%");

            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FRDESCRI like "+cp_ToStr(trim(this.w_ATTRIBUTO)+"%");

            select FRCODICE,FRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATTRIBUTO) and !this.bDontReportError
            deferred_cp_zoom('FAM_ATTR','*','FRCODICE',cp_AbsName(oSource.parent,'oATTRIBUTO_1_31'),i_cWhere,'GSAR_AAT',"Attributi",'GSAG1KIR.FAM_ATTR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',oSource.xKey(1))
            select FRCODICE,FRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTRIBUTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(this.w_ATTRIBUTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',this.w_ATTRIBUTO)
            select FRCODICE,FRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTRIBUTO = NVL(_Link_.FRCODICE,space(10))
      this.w_FRDESCRI = NVL(_Link_.FRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ATTRIBUTO = space(10)
      endif
      this.w_FRDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.FRCODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTRIBUTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCLI_1_5.value==this.w_CODCLI)
      this.oPgFrm.Page1.oPag.oCODCLI_1_5.value=this.w_CODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODSED_1_6.value==this.w_ATCODSED)
      this.oPgFrm.Page1.oPag.oATCODSED_1_6.value=this.w_ATCODSED
    endif
    if not(this.oPgFrm.Page1.oPag.oDesNomin_1_9.value==this.w_DesNomin)
      this.oPgFrm.Page1.oPag.oDesNomin_1_9.value=this.w_DesNomin
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMDES_1_10.value==this.w_NOMDES)
      this.oPgFrm.Page1.oPag.oNOMDES_1_10.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPIANTO_1_16.value==this.w_IMPIANTO)
      this.oPgFrm.Page1.oPag.oIMPIANTO_1_16.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_17.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_17.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_25.value==this.w_COMPIMPKEY)
      this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_25.value=this.w_COMPIMPKEY
    endif
    if not(this.oPgFrm.Page1.oPag.oRICERCA_1_29.value==this.w_RICERCA)
      this.oPgFrm.Page1.oPag.oRICERCA_1_29.value=this.w_RICERCA
    endif
    if not(this.oPgFrm.Page1.oPag.oATTRIBUTO_1_31.value==this.w_ATTRIBUTO)
      this.oPgFrm.Page1.oPag.oATTRIBUTO_1_31.value=this.w_ATTRIBUTO
    endif
    if not(this.oPgFrm.Page1.oPag.oFRDESCRI_1_33.value==this.w_FRDESCRI)
      this.oPgFrm.Page1.oPag.oFRDESCRI_1_33.value=this.w_FRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_1_34.value==this.w_IMDESCRI)
      this.oPgFrm.Page1.oPag.oIMDESCRI_1_34.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_35.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_35.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIMODELLO_1_38.value==this.w_DESCRIMODELLO)
      this.oPgFrm.Page1.oPag.oDESCRIMODELLO_1_38.value=this.w_DESCRIMODELLO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRGRUPPO_1_40.value==this.w_DESCRGRUPPO)
      this.oPgFrm.Page1.oPag.oDESCRGRUPPO_1_40.value=this.w_DESCRGRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRFAMIGLIA_1_42.value==this.w_DESCRFAMIGLIA)
      this.oPgFrm.Page1.oPag.oDESCRFAMIGLIA_1_42.value=this.w_DESCRFAMIGLIA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsag_kmi
      IF i_bRes=.t.
         IF i_bRes
           .w_RESCHK=0 
           .NotifyEvent('InsImpianti')
           IF .w_RESCHK<>0
             i_bRes=.f.
           ENDIF
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODCLI = this.w_CODCLI
    this.o_IMPIANTO = this.w_IMPIANTO
    this.o_COMPIMP = this.w_COMPIMP
    this.o_COMPIMPKEY = this.w_COMPIMPKEY
    return

enddefine

* --- Define pages as container
define class tgsag_kmiPag1 as StdContainer
  Width  = 895
  height = 586
  stdWidth  = 895
  stdheight = 586
  resizeXpos=497
  resizeYpos=307
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCLI_1_5 as StdField with uid="RZGFAZDAQK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODCLI", cQueryName = "CODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 35482662,;
   bGlobalFont=.t.,;
    Height=21, Width=163, Left=206, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLI"

  func oCODCLI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
      if .not. empty(.w_ATCODSED)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCLI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCODCLI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLI
     i_obj.ecpSave()
  endproc

  add object oATCODSED_1_6 as StdField with uid="CCIDQETGZW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ATCODSED", cQueryName = "ATCODSED",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 72785590,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=206, Top=37, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDCODICE", oKey_1_2="this.w_CODCLI", oKey_2_1="DDCODDES", oKey_2_2="this.w_ATCODSED"

  func oATCODSED_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODCLI))
    endwith
   endif
  endfunc

  func oATCODSED_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODSED_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODSED_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODCLI)
    endif
    do cp_zoom with 'DES_DIVE','*','DDCODICE,DDCODDES',cp_AbsName(this.parent,'oATCODSED_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sedi",'',this.parent.oContained
  endproc


  add object zoom_imp as cp_szoombox with uid="IAFNEIEWLE",left=10, top=149, width=878,height=247,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSAG_KMI",bOptions=.f.,cTable="IMP_DETT",cZoomOnZoom="",bQueryOnLoad=.t.,bReadOnly=.t.,cMenuFile="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Ricerca,Init,w_CODCLI Changed",;
    nPag=1;
    , HelpContextID = 246538214

  add object oDesNomin_1_9 as StdField with uid="IMBHILJNMB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DesNomin", cQueryName = "DesNomin",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 140210852,;
   bGlobalFont=.t.,;
    Height=21, Width=455, Left=372, Top=9, InputMask=replicate('X',60)

  add object oNOMDES_1_10 as StdField with uid="BVECHYSAMP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 196017366,;
   bGlobalFont=.t.,;
    Height=21, Width=550, Left=278, Top=37, InputMask=replicate('X',40)


  add object oBtn_1_13 as StdButton with uid="ERHOXVEEGI",left=788, top=533, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 68569318;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="TKCMXZXILU",left=839, top=533, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75857990;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oIMPIANTO_1_16 as StdField with uid="AUISPJHOOI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice impianto di inizio selezione",;
    HelpContextID = 160159019,;
   bGlobalFont=.t.,;
    Height=21, Width=112, Left=206, Top=65, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="GSAG_MIM", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
      if .not. empty(.w_COMPIMPKEYREAD)
        bRes2=.link_1_24('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIM',"Impianti",'GSAG1KMI.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oIMPIANTO_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_IMPIANTO
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_17 as StdField with uid="MZTTQOQOOZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 41833014,;
   bGlobalFont=.t.,;
    Height=21, Width=508, Left=320, Top=65, InputMask=replicate('X',50)


  add object oBtn_1_18 as StdButton with uid="PHEWMSSLEL",left=839, top=9, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 245462806;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOMPIMPKEY_1_25 as StdField with uid="SBSCAGJTGI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COMPIMPKEY", cQueryName = "COMPIMPKEY",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 100358593,;
   bGlobalFont=.t.,;
    Height=22, Width=124, Left=10, Top=400

  func oCOMPIMPKEY_1_25.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oRICERCA_1_29 as AH_SEARCHFLD with uid="LMOCOKSDXU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_RICERCA", cQueryName = "RICERCA",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Valore da ricercare in impianti/componenti",;
    HelpContextID = 209671958,;
   bGlobalFont=.t.,;
    Height=21, Width=622, Left=206, Top=121, InputMask=replicate('X',50)

  add object oATTRIBUTO_1_31 as StdField with uid="ZXCOETZTBC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ATTRIBUTO", cQueryName = "ATTRIBUTO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Attributo da ricercare in impianti/componenti",;
    HelpContextID = 84052406,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=206, Top=93, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="FAM_ATTR", cZoomOnZoom="GSAR_AAT", oKey_1_1="FRCODICE", oKey_1_2="this.w_ATTRIBUTO"

  func oATTRIBUTO_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTRIBUTO_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTRIBUTO_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ATTR','*','FRCODICE',cp_AbsName(this.parent,'oATTRIBUTO_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AAT',"Attributi",'GSAG1KIR.FAM_ATTR_VZM',this.parent.oContained
  endproc
  proc oATTRIBUTO_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AAT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FRCODICE=this.parent.oContained.w_ATTRIBUTO
     i_obj.ecpSave()
  endproc

  add object oFRDESCRI_1_33 as StdField with uid="OJPSNSLUJQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FRDESCRI", cQueryName = "FRDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210726815,;
   bGlobalFont=.t.,;
    Height=21, Width=507, Left=321, Top=93, InputMask=replicate('X',40)

  add object oIMDESCRI_1_34 as StdField with uid="EWZQEETYGI",rtseq=22,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 210725583,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=481, Top=429, InputMask=replicate('X',50)

  add object oANDESCRI_1_35 as StdField with uid="JCWBFAPTCY",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 210725711,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=481, Top=403, InputMask=replicate('X',30)

  add object oDESCRIMODELLO_1_38 as StdField with uid="PZWEPDMHUT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCRIMODELLO", cQueryName = "DESCRIMODELLO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 129981381,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=481, Top=455, InputMask=replicate('X',40)

  add object oDESCRGRUPPO_1_40 as StdField with uid="ZPJOTTFOTG",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCRGRUPPO", cQueryName = "DESCRGRUPPO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 8624011,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=481, Top=481, InputMask=replicate('X',40)

  add object oDESCRFAMIGLIA_1_42 as StdField with uid="PCNMZAMYKK",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCRFAMIGLIA", cQueryName = "DESCRFAMIGLIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 64773651,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=481, Top=507, InputMask=replicate('X',40)

  add object oStr_1_2 as StdString with uid="WPPHXIZKOW",Visible=.t., Left=136, Top=9,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="VLCUXYDVQK",Visible=.t., Left=169, Top=37,;
    Alignment=1, Width=32, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ICGZJRMBFC",Visible=.t., Left=150, Top=65,;
    Alignment=1, Width=51, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="EZGROXWKTJ",Visible=.t., Left=95, Top=121,;
    Alignment=1, Width=106, Height=18,;
    Caption="Valore da ricercare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="FJQEOWSVZU",Visible=.t., Left=154, Top=93,;
    Alignment=1, Width=47, Height=18,;
    Caption="Attributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="YLGQODOLTR",Visible=.t., Left=358, Top=429,;
    Alignment=1, Width=119, Height=18,;
    Caption="Descrizione impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="AHWYEENTXP",Visible=.t., Left=345, Top=403,;
    Alignment=1, Width=132, Height=18,;
    Caption="Descrizione intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="AHQVYYKGXH",Visible=.t., Left=345, Top=455,;
    Alignment=1, Width=132, Height=18,;
    Caption="Descrizione modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="TDFYIRSJRG",Visible=.t., Left=345, Top=481,;
    Alignment=1, Width=132, Height=18,;
    Caption="Descrizione gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="VPTNGYWUZI",Visible=.t., Left=345, Top=507,;
    Alignment=1, Width=132, Height=18,;
    Caption="Descrizione famiglia:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kmi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
