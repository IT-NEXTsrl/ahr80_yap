* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bab                                                        *
*              Abbina documenti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-02-11                                                      *
* Last revis.: 2013-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bab",oParentObject,m.pEXEC)
return(i_retval)

define class tgsag_bab as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_PADRE = .NULL.
  w_OK = .f.
  w_ADSERDOC = space(10)
  w_ATSERIAL = space(20)
  w_ADTIPDOC = space(1)
  w_CPROWORD = 0
  w_OGGATT = .NULL.
  w_ATCAUATT = space(20)
  w_GSVE_MDV = .NULL.
  w_CAUABB = space(5)
  w_FLINTE = space(1)
  w_CLADOC = space(2)
  w_RICNOM = space(1)
  w_FLVEAC = space(1)
  w_OBJ = .NULL.
  w_TIPCLI = space(1)
  w_CODCLI = space(15)
  * --- WorkFile variables
  DOC_MAST_idx=0
  VASTRUTT_idx=0
  TIP_DOCU_idx=0
  CONTI_idx=0
  ATT_DCOL_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- A abbina documenti
    *     N nuovo documento
    *     L  esegue abbinamento tra documento e attivit� dai documenti
    *     S  seleziona
    *     D deseleziona
    *     I   inverti selezione
    this.w_PADRE = This.oParentobject.oParentobject
    do case
      case this.pEXEC $ "L-A"
        this.w_OGGATT = iif(this.pEXEC="L",this.w_PADRE.w_OGGATT,this.w_PADRE.oParentobject)
        this.w_ATSERIAL = this.w_OGGATT.w_ATSERIAL
        * --- Select from ATT_DCOL
        i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2],.t.,this.ATT_DCOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ADSERIAL,MAX(CPROWORD) AS CPROWORD  from "+i_cTable+" ATT_DCOL ";
              +" where ADSERIAL="+cp_ToStrODBC(this.w_ATSERIAL)+"";
              +" group by ADSERIAL";
               ,"_Curs_ATT_DCOL")
        else
          select ADSERIAL,MAX(CPROWORD) AS CPROWORD from (i_cTable);
           where ADSERIAL=this.w_ATSERIAL;
           group by ADSERIAL;
            into cursor _Curs_ATT_DCOL
        endif
        if used('_Curs_ATT_DCOL')
          select _Curs_ATT_DCOL
          locate for 1=1
          do while not(eof())
          this.w_CPROWORD = Nvl(_Curs_ATT_DCOL.CPROWORD,0)
          exit
            select _Curs_ATT_DCOL
            continue
          enddo
          use
        endif
        if this.pEXEC="A"
          * --- Cicla sui record Selezionati
           
 NC =this.oParentObject.w_Zoomdoc.cCursor
          this.w_OK = .F.
          SELECT (NC)
          GO TOP
          SCAN FOR XCHK<>0 
          this.w_ADSERDOC = MVSERIAL
          this.w_ADTIPDOC = "D"
          this.w_CPROWORD = this.w_CPROWORD + 10
          * --- Carica il Temporaneo dei Dati e skippa al record successivo
          this.w_OK = .T.
          SELECT (NC)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ENDSCAN
          if !this.w_OK
            ah_ErrorMsg("Non ci sono documenti da abbinare",,"")
          else
            This.oparentobject.EcpQuit()
          endif
        else
          this.w_ADSERDOC = this.w_PADRE.w_MVSERIAL
          this.w_ADTIPDOC = "D"
          this.w_CPROWORD = this.w_CPROWORD + 10
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Chiudo la Maschera
        this.w_OGGATT.GSAG_MDD.Ecpsave()     
        this.w_OGGATT.loadrec()     
      case this.pEXEC="N"
        this.w_ATCAUATT = this.w_PADRE.w_ATCAUATT
        this.w_CAUABB = IIF(Not Empty(this.w_PADRE.w_CAUABB),this.w_PADRE.w_CAUABB,this.w_PADRE.w_PCAUABB)
        if Not Empty(this.w_CAUABB)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDCATDOC,TDRICNOM,TDFLVEAC,TDFLINTE"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUABB);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDCATDOC,TDRICNOM,TDFLVEAC,TDFLINTE;
              from (i_cTable) where;
                  TDTIPDOC = this.w_CAUABB;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            this.w_RICNOM = NVL(cp_ToDate(_read_.TDRICNOM),cp_NullValue(_read_.TDRICNOM))
            this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
            this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_GSVE_MDV = icase(IsAhe(),GSVE_MDV(this.w_FLVEAC+this.w_CLADOC),this.w_CLADOC="OR",GSOR_MDV(this.w_FLVEAC),this.w_FLVEAC="A",GSAC_MDV(this.w_CLADOC),GSVE_MDV(this.w_CLADOC))
          p_OLDCAU=this.w_CAUABB
          this.w_GSVE_MDV.Ecpload()     
          this.w_GSVE_MDV.w_MVFLINTE = this.w_FLINTE
          this.w_GSVE_MDV.w_MVTIPCON = IIF(this.w_FLINTE $ "CF", this.w_FLINTE, " ")
          this.w_GSVE_MDV.w_MVTIPDOC = this.w_CAUABB
          this.w_OBJ = this.w_GSVE_MDV.getctrl("w_MVCODCON")
          this.w_OBJ.bOBBL = .f.
          this.w_OBJ = Null
          = setvaluelinked ( "M" , this.w_GSVE_MDV, "w_MVTIPDOC" , this.w_CAUABB)
          if this.w_RICNOM="A"
            if ! IsAhe()
              this.w_GSVE_MDV.w_NOCODICE = this.w_PADRE.w_ATCODNOM
              GSUT_BVP(this,this.w_GSVE_MDV, "w_NOCODICE", "["+this.w_PADRE.w_ATCODNOM+"]")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_GSVE_MDV.w_FLEDIT = "S"
            endif
          else
            * --- Read from OFF_NOMI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NOTIPCLI,NOCODCLI"+;
                " from "+i_cTable+" OFF_NOMI where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_PADRE.w_ATCODNOM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NOTIPCLI,NOCODCLI;
                from (i_cTable) where;
                    NOCODICE = this.w_PADRE.w_ATCODNOM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TIPCLI = NVL(cp_ToDate(_read_.NOTIPCLI),cp_NullValue(_read_.NOTIPCLI))
              this.w_CODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Not Empty(this.w_CODCLI) AND this.w_FLINTE $ "C-F"
              this.w_GSVE_MDV.w_MVCODCON = this.w_CODCLI
              this.w_GSVE_MDV.w_MVTIPCON = this.w_TIPCLI
              GSUT_BVP(this,this.w_GSVE_MDV, "w_MVCODCON", "["+this.w_CODCLI+"]")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if !IsAhe()
              this.w_GSVE_MDV.w_FLEDIT = "S"
            endif
          endif
          this.w_GSVE_MDV.w_OGGATT = this.w_PADRE
        else
          ah_ErrorMsg("Nessuna causale documento abbinata. Impossibile proseguire")
        endif
        this.w_GSVE_MDV = .null.
      otherwise
        ND=this.oParentObject.w_ZOOMDOC.cCursor
        UPDATE (ND) SET XCHK=IIF(this.pEXEC="S",1, 0)
    endcase
    this.w_OGGATT = .Null.
    this.w_PADRE = .Null.
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_02607BC0
    bErr_02607BC0=bTrsErr
    this.Try_02607BC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      ah_ErrorMsg("Errore durante l'aggiornamento; Operazione abbandonata",,"")
    endif
    bTrsErr=bTrsErr or bErr_02607BC0
    * --- End
  endproc
  proc Try_02607BC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATT_DCOL
    i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATT_DCOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ADSERIAL"+",ADSERDOC"+",ADTIPDOC"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ATSERIAL),'ATT_DCOL','ADSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ADSERDOC),'ATT_DCOL','ADSERDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ADTIPDOC),'ATT_DCOL','ADTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'ATT_DCOL','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ADSERIAL',this.w_ATSERIAL,'ADSERDOC',this.w_ADSERDOC,'ADTIPDOC',this.w_ADTIPDOC,'CPROWORD',this.w_CPROWORD)
      insert into (i_cTable) (ADSERIAL,ADSERDOC,ADTIPDOC,CPROWORD &i_ccchkf. );
         values (;
           this.w_ATSERIAL;
           ,this.w_ADSERDOC;
           ,this.w_ADTIPDOC;
           ,this.w_CPROWORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='ATT_DCOL'
    this.cWorkTables[6]='OFF_NOMI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_ATT_DCOL')
      use in _Curs_ATT_DCOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
