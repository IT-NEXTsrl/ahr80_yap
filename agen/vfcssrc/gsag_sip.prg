* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_sip                                                        *
*              Stampa raggruppamenti di attivit�                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-08-01                                                      *
* Last revis.: 2010-04-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_sip",oParentObject))

* --- Class definition
define class tgsag_sip as StdForm
  Top    = 12
  Left   = 88

  * --- Standard Properties
  Width  = 501
  Height = 152
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-20"
  HelpContextID=9820311
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  VOC_COST_IDX = 0
  OUT_PUTS_IDX = 0
  PRO_ITER_IDX = 0
  cPrg = "gsag_sip"
  cComment = "Stampa raggruppamenti di attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(10)
  w_CODFIN = space(10)
  w_DESCRITER = space(40)
  w_DESCRITER2 = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_sipPag1","gsag_sip",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VOC_COST'
    this.cWorkTables[2]='OUT_PUTS'
    this.cWorkTables[3]='PRO_ITER'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsag_sip
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(10)
      .w_CODFIN=space(10)
      .w_DESCRITER=space(40)
      .w_DESCRITER2=space(40)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODFIN))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
    this.DoRTCalc(3,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODINI_1_1.visible=!this.oPgFrm.Page1.oPag.oCODINI_1_1.mHide()
    this.oPgFrm.Page1.oPag.oCODFIN_1_2.visible=!this.oPgFrm.Page1.oPag.oCODFIN_1_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDESCRITER_1_9.visible=!this.oPgFrm.Page1.oPag.oDESCRITER_1_9.mHide()
    this.oPgFrm.Page1.oPag.oDESCRITER2_1_10.visible=!this.oPgFrm.Page1.oPag.oDESCRITER2_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
    i_lTable = "PRO_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2], .t., this.PRO_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIP',True,'PRO_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IPCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IPCODICE',trim(this.w_CODINI))
          select IPCODICE,IPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.IPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IPDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IPDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('PRO_ITER','*','IPCODICE',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'GSAG_MIP',"Raggruppamenti di attivit�",'GSAG_MIP.PRO_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IPCODICE',oSource.xKey(1))
            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IPCODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IPCODICE',this.w_CODINI)
            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.IPCODICE,space(10))
      this.w_DESCRITER = NVL(_Link_.IPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(10)
      endif
      this.w_DESCRITER = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI <= .w_CODFIN OR EMPTY(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODINI = space(10)
        this.w_DESCRITER = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])+'\'+cp_ToStr(_Link_.IPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRO_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
    i_lTable = "PRO_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2], .t., this.PRO_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIP',True,'PRO_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IPCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IPCODICE',trim(this.w_CODFIN))
          select IPCODICE,IPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.IPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IPDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IPDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('PRO_ITER','*','IPCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_2'),i_cWhere,'GSAG_MIP',"Raggruppamenti di attivit�",'GSAG_MIP.PRO_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IPCODICE',oSource.xKey(1))
            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IPCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IPCODICE',this.w_CODFIN)
            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.IPCODICE,space(10))
      this.w_DESCRITER2 = NVL(_Link_.IPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(10)
      endif
      this.w_DESCRITER2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI <= .w_CODFIN OR EMPTY(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFIN = space(10)
        this.w_DESCRITER2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])+'\'+cp_ToStr(_Link_.IPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRO_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_2.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_2.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRITER_1_9.value==this.w_DESCRITER)
      this.oPgFrm.Page1.oPag.oDESCRITER_1_9.value=this.w_DESCRITER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRITER2_1_10.value==this.w_DESCRITER2)
      this.oPgFrm.Page1.oPag.oDESCRITER2_1_10.value=this.w_DESCRITER2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODINI <= .w_CODFIN OR EMPTY(.w_CODFIN))  and not(IsAlt())  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODINI <= .w_CODFIN OR EMPTY(.w_CODFIN))  and not(IsAlt())  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_sipPag1 as StdContainer
  Width  = 497
  height = 152
  stdWidth  = 497
  stdheight = 152
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="LUIWKXTMHR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 20747226,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=105, Top=14, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRO_ITER", cZoomOnZoom="GSAG_MIP", oKey_1_1="IPCODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRO_ITER','*','IPCODICE',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIP',"Raggruppamenti di attivit�",'GSAG_MIP.PRO_ITER_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IPCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_2 as StdField with uid="XYBZFHDLGN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 210736090,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=105, Top=38, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRO_ITER", cZoomOnZoom="GSAG_MIP", oKey_1_1="IPCODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_2.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCODFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRO_ITER','*','IPCODICE',cp_AbsName(this.parent,'oCODFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIP',"Raggruppamenti di attivit�",'GSAG_MIP.PRO_ITER_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IPCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc


  add object oObj_1_3 as cp_outputCombo with uid="ZLERXIVPWT",left=105, top=67, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 80617498


  add object oBtn_1_4 as StdButton with uid="FFZUPFQMOE",left=382, top=101, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 116825562;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="DIOMPKAHZX",left=437, top=101, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 17137734;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRITER_1_9 as StdField with uid="HIHAVWGZOZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCRITER", cQueryName = "DESCRITER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 16885861,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=193, Top=14, InputMask=replicate('X',40)

  func oDESCRITER_1_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDESCRITER2_1_10 as StdField with uid="FYWXOPBPSY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRITER2", cQueryName = "DESCRITER2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 16873061,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=193, Top=38, InputMask=replicate('X',40)

  func oDESCRITER2_1_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="HVQDALZAWB",Visible=.t., Left=2, Top=67,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ZYYLDVTSYC",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="GORDTTCVUF",Visible=.t., Left=2, Top=37,;
    Alignment=1, Width=99, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_sip','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
