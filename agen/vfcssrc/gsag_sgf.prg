* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_sgf                                                        *
*              Variazione di data                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-04-30                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_sgf",oParentObject))

* --- Class definition
define class tgsag_sgf as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 776
  Height = 142
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=23734121
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_sgf"
  cComment = "Variazione di data"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_ORAINI = space(2)
  w_MININI = space(2)
  w_DATACAL = ctot('')
  w_OGGETTO = space(254)
  w_ATDATINI = ctot('')
  o_ATDATINI = ctot('')
  w_NOTEERRORE = space(254)
  w_DAY1 = space(10)
  w_DAY2 = space(10)
  w_ORAINIIN = space(2)
  w_MININIESC = space(2)
  w_DATCAL = ctod('  /  /  ')
  w_PACHKFES = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_sgfPag1","gsag_sgf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATINI=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_DATACAL=ctot("")
      .w_OGGETTO=space(254)
      .w_ATDATINI=ctot("")
      .w_NOTEERRORE=space(254)
      .w_DAY1=space(10)
      .w_DAY2=space(10)
      .w_ORAINIIN=space(2)
      .w_MININIESC=space(2)
      .w_DATCAL=ctod("  /  /  ")
      .w_PACHKFES=space(1)
      .w_DATACAL=oParentObject.w_DATACAL
      .w_OGGETTO=oParentObject.w_OGGETTO
      .w_ATDATINI=oParentObject.w_ATDATINI
      .w_NOTEERRORE=oParentObject.w_NOTEERRORE
      .w_PACHKFES=oParentObject.w_PACHKFES
        .w_DATINI = ttod( .w_DATACAL )
        .w_ORAINI = Padl(alltrim(str(HOUR( .w_DATACAL ),2,0)),2,'0')
        .w_MININI = Padl(alltrim(str(minute( .w_DATACAL ),2,0)),2,'0')
          .DoRTCalc(4,7,.f.)
        .w_DAY1 = IIF(NOT EMPTY(.w_ATDATINI),LEFT(g_GIORNO[DOW(TTOD(.w_ATDATINI))],3), '   ')
        .w_DAY2 = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        .w_ORAINIIN = Padl(alltrim(str(HOUR( .w_ATDATINI ),2,0)),2,'0')
        .w_MININIESC = Padl(alltrim(str(minute( .w_ATDATINI ),2,0)),2,'0')
        .w_DATCAL = ttod( .w_ATDATINI)
    endwith
    this.DoRTCalc(13,13,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATACAL=.w_DATACAL
      .oParentObject.w_OGGETTO=.w_OGGETTO
      .oParentObject.w_ATDATINI=.w_ATDATINI
      .oParentObject.w_NOTEERRORE=.w_NOTEERRORE
      .oParentObject.w_PACHKFES=.w_PACHKFES
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DAY2 = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_PKMHFGFLHY()
      endwith
      this.DoRTCalc(10,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_PKMHFGFLHY()
    with this
          * --- calcolo la data inizio
          .w_DATACAL = cp_CharToDatetime(DTOC(.w_DATINI)+' '+PADL(alltrim(.w_ORAINI),2,'0')+':'+PADL(alltrim(.w_MININI),2,'0')+':00')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_1.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_2.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_2.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_3.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_3.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oOGGETTO_1_7.value==this.w_OGGETTO)
      this.oPgFrm.Page1.oPag.oOGGETTO_1_7.value=this.w_OGGETTO
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTEERRORE_1_11.value==this.w_NOTEERRORE)
      this.oPgFrm.Page1.oPag.oNOTEERRORE_1_11.value=this.w_NOTEERRORE
    endif
    if not(this.oPgFrm.Page1.oPag.oDAY1_1_13.value==this.w_DAY1)
      this.oPgFrm.Page1.oPag.oDAY1_1_13.value=this.w_DAY1
    endif
    if not(this.oPgFrm.Page1.oPag.oDAY2_1_14.value==this.w_DAY2)
      this.oPgFrm.Page1.oPag.oDAY2_1_14.value=this.w_DAY2
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINIIN_1_18.value==this.w_ORAINIIN)
      this.oPgFrm.Page1.oPag.oORAINIIN_1_18.value=this.w_ORAINIIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMININIESC_1_19.value==this.w_MININIESC)
      this.oPgFrm.Page1.oPag.oMININIESC_1_19.value=this.w_MININIESC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCAL_1_20.value==this.w_DATCAL)
      this.oPgFrm.Page1.oPag.oDATCAL_1_20.value=this.w_DATCAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ORAINI)) or not(VAL(.w_ORAINI) < 24))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_2.SetFocus()
            i_bnoObbl = !empty(.w_ORAINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MININI)) or not(VAL(.w_MININI) < 60))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_MININI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_DATACAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATACAL_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATACAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATCAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCAL_1_20.SetFocus()
            i_bnoObbl = !empty(.w_DATCAL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATINI = this.w_DATINI
    this.o_ATDATINI = this.w_ATDATINI
    return

enddefine

* --- Define pages as container
define class tgsag_sgfPag1 as StdContainer
  Width  = 772
  height = 142
  stdWidth  = 772
  stdheight = 142
  resizeXpos=287
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_1 as StdField with uid="QXSPXYXHLS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit�",;
    HelpContextID = 214195766,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=194, Top=84

  func oDATINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_PACHKFES='N' OR Chkfestivi(.w_DATINI,.w_PACHKFES))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAINI_1_2 as StdField with uid="IFZETBJSDN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 214122470,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=324, Top=84, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_3 as StdField with uid="BMAZHAYBOV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 214173382,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=359, Top=84, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc


  add object oBtn_1_4 as StdButton with uid="ERHOXVEEGI",left=714, top=86, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 23705370;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oOGGETTO_1_7 as StdField with uid="HVVLWEALIF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_OGGETTO", cQueryName = "OGGETTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 136287462,;
   bGlobalFont=.t.,;
    Height=21, Width=631, Left=132, Top=7, InputMask=replicate('X',254)

  add object oNOTEERRORE_1_11 as StdField with uid="LRVBIBUBPF",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NOTEERRORE", cQueryName = "NOTEERRORE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 87078725,;
   bGlobalFont=.t.,;
    Height=21, Width=631, Left=132, Top=60, InputMask=replicate('X',254)

  add object oDAY1_1_13 as StdField with uid="GMFJMSDVQP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DAY1", cQueryName = "DAY1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 20140490,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=132, Top=34, InputMask=replicate('X',10)

  add object oDAY2_1_14 as StdField with uid="JBPDSTDGRU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DAY2", cQueryName = "DAY2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 20074954,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=132, Top=84, InputMask=replicate('X',10)

  add object oORAINIIN_1_18 as StdField with uid="ZEVDCNYAEZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ORAINIIN", cQueryName = "ORAINIIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 54312908,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=324, Top=34, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  add object oMININIESC_1_19 as StdField with uid="ERRBUEPMQI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MININIESC", cQueryName = "MININIESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 214174537,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=359, Top=34, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  add object oDATCAL_1_20 as StdField with uid="QZLSKFWZRW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATCAL", cQueryName = "DATCAL",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit�",;
    HelpContextID = 250502710,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=194, Top=34

  add object oStr_1_6 as StdString with uid="GDWVHFWLRW",Visible=.t., Left=83, Top=7,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="QXAGYMIDYT",Visible=.t., Left=4, Top=35,;
    Alignment=1, Width=125, Height=18,;
    Caption="Data di inizio calcolata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="DFLZUSCFTD",Visible=.t., Left=53, Top=60,;
    Alignment=1, Width=77, Height=18,;
    Caption="Incongruenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="EBEVKGRFFE",Visible=.t., Left=12, Top=84,;
    Alignment=1, Width=117, Height=18,;
    Caption="Data di inizio corretta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="DWXUKAMXAQ",Visible=.t., Left=354, Top=84,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="LRDPZVZQBN",Visible=.t., Left=278, Top=84,;
    Alignment=0, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="VISSWERGTQ",Visible=.t., Left=354, Top=34,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="UQEMVCDORY",Visible=.t., Left=278, Top=34,;
    Alignment=0, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_sgf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
