* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mpe                                                        *
*              Dettaglio esportazione                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-09                                                      *
* Last revis.: 2011-06-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsag_mpe")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsag_mpe")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsag_mpe")
  return

* --- Class definition
define class tgsag_mpe as StdPCForm
  Width  = 621
  Height = 401
  Top    = 10
  Left   = 10
  cComment = "Dettaglio esportazione"
  cPrg = "gsag_mpe"
  HelpContextID=147466089
  add object cnt as tcgsag_mpe
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsag_mpe as PCContext
  w_COSERPRO = space(10)
  w_COSERATT = space(20)
  w_COSEREVE = space(150)
  w_COFLGIGN = space(1)
  w_CODATESP = space(14)
  w_CO__NOTE = space(10)
  proc Save(i_oFrom)
    this.w_COSERPRO = i_oFrom.w_COSERPRO
    this.w_COSERATT = i_oFrom.w_COSERATT
    this.w_COSEREVE = i_oFrom.w_COSEREVE
    this.w_COFLGIGN = i_oFrom.w_COFLGIGN
    this.w_CODATESP = i_oFrom.w_CODATESP
    this.w_CO__NOTE = i_oFrom.w_CO__NOTE
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_COSERPRO = this.w_COSERPRO
    i_oTo.w_COSERATT = this.w_COSERATT
    i_oTo.w_COSEREVE = this.w_COSEREVE
    i_oTo.w_COFLGIGN = this.w_COFLGIGN
    i_oTo.w_CODATESP = this.w_CODATESP
    i_oTo.w_CO__NOTE = this.w_CO__NOTE
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsag_mpe as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 621
  Height = 401
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-06-28"
  HelpContextID=147466089
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DET_EXPO_IDX = 0
  cFile = "DET_EXPO"
  cKeySelect = "COSERPRO"
  cKeyWhere  = "COSERPRO=this.w_COSERPRO"
  cKeyDetail  = "COSERPRO=this.w_COSERPRO and COSERATT=this.w_COSERATT and COSEREVE=this.w_COSEREVE"
  cKeyWhereODBC = '"COSERPRO="+cp_ToStrODBC(this.w_COSERPRO)';

  cKeyDetailWhereODBC = '"COSERPRO="+cp_ToStrODBC(this.w_COSERPRO)';
      +'+" and COSERATT="+cp_ToStrODBC(this.w_COSERATT)';
      +'+" and COSEREVE="+cp_ToStrODBC(this.w_COSEREVE)';

  cKeyWhereODBCqualified = '"DET_EXPO.COSERPRO="+cp_ToStrODBC(this.w_COSERPRO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsag_mpe"
  cComment = "Dettaglio esportazione"
  i_nRowNum = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COSERPRO = space(10)
  w_COSERATT = space(20)
  w_COSEREVE = space(150)
  w_COFLGIGN = space(1)
  w_CODATESP = ctot('')
  w_CO__NOTE = space(0)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mpePag1","gsag_mpe",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DET_EXPO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DET_EXPO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DET_EXPO_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsag_mpe'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DET_EXPO where COSERPRO=KeySet.COSERPRO
    *                            and COSERATT=KeySet.COSERATT
    *                            and COSEREVE=KeySet.COSEREVE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DET_EXPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_EXPO_IDX,2],this.bLoadRecFilter,this.DET_EXPO_IDX,"gsag_mpe")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DET_EXPO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DET_EXPO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DET_EXPO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COSERPRO',this.w_COSERPRO  )
      select * from (i_cTable) DET_EXPO where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_COSERPRO = NVL(COSERPRO,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DET_EXPO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_COSERATT = NVL(COSERATT,space(20))
          .w_COSEREVE = NVL(COSEREVE,space(150))
          .w_COFLGIGN = NVL(COFLGIGN,space(1))
          .w_CODATESP = NVL(CODATESP,ctot(""))
          .w_CO__NOTE = NVL(CO__NOTE,space(0))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace COSERATT with .w_COSERATT
          replace COSEREVE with .w_COSEREVE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_3.enabled = .oPgFrm.Page1.oPag.oBtn_1_3.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_COSERPRO=space(10)
      .w_COSERATT=space(20)
      .w_COSEREVE=space(150)
      .w_COFLGIGN=space(1)
      .w_CODATESP=ctot("")
      .w_CO__NOTE=space(0)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DET_EXPO')
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCO__NOTE_2_5.enabled = i_bVal
      .Page1.oPag.oBtn_1_3.enabled = .Page1.oPag.oBtn_1_3.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DET_EXPO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsag_mpe
    * Abilito il campo note sul dettaglio per scorrere eventuali log lunghi
    Local L_Ctrl
    L_Ctrl=This.GetCtrl('w_CO__NOTE')
    L_Ctrl.Enabled=.t.
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DET_EXPO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSERPRO,"COSERPRO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_COSERATT C(20);
      ,t_COSEREVE C(150);
      ,t_COFLGIGN N(3);
      ,t_CODATESP T(14);
      ,t_CO__NOTE M(10);
      ,COSERATT C(20);
      ,COSEREVE C(150);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mpebodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOSERATT_2_1.controlsource=this.cTrsName+'.t_COSERATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOSEREVE_2_2.controlsource=this.cTrsName+'.t_COSEREVE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOFLGIGN_2_3.controlsource=this.cTrsName+'.t_COFLGIGN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCODATESP_2_4.controlsource=this.cTrsName+'.t_CODATESP'
    this.oPgFRm.Page1.oPag.oCO__NOTE_2_5.controlsource=this.cTrsName+'.t_CO__NOTE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(120)
    this.AddVLine(274)
    this.AddVLine(575)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSERATT_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DET_EXPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_EXPO_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DET_EXPO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_EXPO_IDX,2])
      *
      * insert into DET_EXPO
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DET_EXPO')
        i_extval=cp_InsertValODBCExtFlds(this,'DET_EXPO')
        i_cFldBody=" "+;
                  "(COSERPRO,COSERATT,COSEREVE,COFLGIGN,CODATESP"+;
                  ",CO__NOTE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_COSERPRO)+","+cp_ToStrODBC(this.w_COSERATT)+","+cp_ToStrODBC(this.w_COSEREVE)+","+cp_ToStrODBC(this.w_COFLGIGN)+","+cp_ToStrODBC(this.w_CODATESP)+;
             ","+cp_ToStrODBC(this.w_CO__NOTE)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DET_EXPO')
        i_extval=cp_InsertValVFPExtFlds(this,'DET_EXPO')
        cp_CheckDeletedKey(i_cTable,0,'COSERPRO',this.w_COSERPRO,'COSERATT',this.w_COSERATT,'COSEREVE',this.w_COSEREVE)
        INSERT INTO (i_cTable) (;
                   COSERPRO;
                  ,COSERATT;
                  ,COSEREVE;
                  ,COFLGIGN;
                  ,CODATESP;
                  ,CO__NOTE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_COSERPRO;
                  ,this.w_COSERATT;
                  ,this.w_COSEREVE;
                  ,this.w_COFLGIGN;
                  ,this.w_CODATESP;
                  ,this.w_CO__NOTE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DET_EXPO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_EXPO_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_COSERATT))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DET_EXPO')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and COSERATT="+cp_ToStrODBC(&i_TN.->COSERATT)+;
                 " and COSEREVE="+cp_ToStrODBC(&i_TN.->COSEREVE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DET_EXPO')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and COSERATT=&i_TN.->COSERATT;
                      and COSEREVE=&i_TN.->COSEREVE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_COSERATT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and COSERATT="+cp_ToStrODBC(&i_TN.->COSERATT)+;
                            " and COSEREVE="+cp_ToStrODBC(&i_TN.->COSEREVE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and COSERATT=&i_TN.->COSERATT;
                            and COSEREVE=&i_TN.->COSEREVE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace COSERATT with this.w_COSERATT
              replace COSEREVE with this.w_COSEREVE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DET_EXPO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DET_EXPO')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " COFLGIGN="+cp_ToStrODBC(this.w_COFLGIGN)+;
                     ",CODATESP="+cp_ToStrODBC(this.w_CODATESP)+;
                     ",CO__NOTE="+cp_ToStrODBC(this.w_CO__NOTE)+;
                     ",COSERATT="+cp_ToStrODBC(this.w_COSERATT)+;
                     ",COSEREVE="+cp_ToStrODBC(this.w_COSEREVE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and COSERATT="+cp_ToStrODBC(COSERATT)+;
                             " and COSEREVE="+cp_ToStrODBC(COSEREVE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DET_EXPO')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      COFLGIGN=this.w_COFLGIGN;
                     ,CODATESP=this.w_CODATESP;
                     ,CO__NOTE=this.w_CO__NOTE;
                     ,COSERATT=this.w_COSERATT;
                     ,COSEREVE=this.w_COSEREVE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and COSERATT=&i_TN.->COSERATT;
                                      and COSEREVE=&i_TN.->COSEREVE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DET_EXPO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_EXPO_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_COSERATT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DET_EXPO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and COSERATT="+cp_ToStrODBC(&i_TN.->COSERATT)+;
                            " and COSEREVE="+cp_ToStrODBC(&i_TN.->COSEREVE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and COSERATT=&i_TN.->COSERATT;
                              and COSEREVE=&i_TN.->COSEREVE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_COSERATT))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DET_EXPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_EXPO_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCO__NOTE_2_5.value==this.w_CO__NOTE)
      this.oPgFrm.Page1.oPag.oCO__NOTE_2_5.value=this.w_CO__NOTE
      replace t_CO__NOTE with this.oPgFrm.Page1.oPag.oCO__NOTE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSERATT_2_1.value==this.w_COSERATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSERATT_2_1.value=this.w_COSERATT
      replace t_COSERATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSERATT_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSEREVE_2_2.value==this.w_COSEREVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSEREVE_2_2.value=this.w_COSEREVE
      replace t_COSEREVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOSEREVE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOFLGIGN_2_3.RadioValue()==this.w_COFLGIGN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOFLGIGN_2_3.SetRadio()
      replace t_COFLGIGN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOFLGIGN_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODATESP_2_4.value==this.w_CODATESP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODATESP_2_4.value=this.w_CODATESP
      replace t_CODATESP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODATESP_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'DET_EXPO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_COSERATT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_COSERATT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_COSERATT=space(20)
      .w_COSEREVE=space(150)
      .w_COFLGIGN=space(1)
      .w_CODATESP=ctot("")
      .w_CO__NOTE=space(0)
    endwith
    this.DoRTCalc(1,6,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_COSERATT = t_COSERATT
    this.w_COSEREVE = t_COSEREVE
    this.w_COFLGIGN = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOFLGIGN_2_3.RadioValue(.t.)
    this.w_CODATESP = t_CODATESP
    this.w_CO__NOTE = t_CO__NOTE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_COSERATT with this.w_COSERATT
    replace t_COSEREVE with this.w_COSEREVE
    replace t_COFLGIGN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOFLGIGN_2_3.ToRadio()
    replace t_CODATESP with this.w_CODATESP
    replace t_CO__NOTE with this.w_CO__NOTE
    if i_srv='A'
      replace COSERATT with this.w_COSERATT
      replace COSEREVE with this.w_COSEREVE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mpePag1 as StdContainer
  Width  = 617
  height = 401
  stdWidth  = 617
  stdheight = 401
  resizeXpos=510
  resizeYpos=111
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=16, width=601,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CODATESP",Label1="Data ultima operaz.",Field2="COSERATT",Label2="Attivit�",Field3="COSEREVE",Label3="UID",Field4="COFLGIGN",Label4="Ignora",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168624762


  add object oBtn_1_3 as StdButton with uid="HEACLVWGWF",left=555, top=212, width=48,height=45,;
    CpPicture="bmp\DM_agenda.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare\inserire attivit�";
    , HelpContextID = 134648294;
    , Caption='\<Attivit�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        =opengest('A','GSAG_AAT','ATSERIAL',.w_COSERATT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_COSERATT))
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=35,;
    width=597+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=36,width=596+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCO__NOTE_2_5.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCO__NOTE_2_5 as StdTrsMemo with uid="NHKIYIJXSW",rtseq=6,rtrep=.t.,;
    cFormVar="w_CO__NOTE",value=space(0),;
    HelpContextID = 192617579,;
    cTotal="", bFixedPos=.t., cQueryName = "CO__NOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=181, Width=537, Left=9, Top=212, tabstop = .f., readonly = .t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mpeBodyRow as CPBodyRowCnt
  Width=587
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCOSERATT_2_1 as StdTrsField with uid="WKEPQJLHXA",rtseq=2,rtrep=.t.,;
    cFormVar="w_COSERATT",value=space(20),isprimarykey=.t.,;
    HelpContextID = 228613242,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=111, Top=0, InputMask=replicate('X',20), bHasZoom = .t. 

  proc oCOSERATT_2_1.mZoom
    a=opengest("A","GSAG_AAT","ATSERIAL",this.parent.oContained.w_COSERATT)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCOSEREVE_2_2 as StdTrsField with uid="NSGIUOLHWS",rtseq=3,rtrep=.t.,;
    cFormVar="w_COSEREVE",value=space(150),isprimarykey=.t.,;
    HelpContextID = 27286635,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=299, Left=265, Top=0, InputMask=replicate('X',150)

  add object oCOFLGIGN_2_3 as StdTrsCheck with uid="KRDGWMDLLB",rtrep=.t.,;
    cFormVar="w_COFLGIGN",  caption=" ",;
    ToolTipText = "Se attivo associazione ignorata",;
    HelpContextID = 185168780,;
    Left=566, Top=0, Width=16,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCOFLGIGN_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLGIGN,&i_cF..t_COFLGIGN),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCOFLGIGN_2_3.GetRadio()
    this.Parent.oContained.w_COFLGIGN = this.RadioValue()
    return .t.
  endfunc

  func oCOFLGIGN_2_3.ToRadio()
    this.Parent.oContained.w_COFLGIGN=trim(this.Parent.oContained.w_COFLGIGN)
    return(;
      iif(this.Parent.oContained.w_COFLGIGN=='S',1,;
      0))
  endfunc

  func oCOFLGIGN_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCODATESP_2_4 as StdTrsField with uid="GHZGSSSMCI",rtseq=5,rtrep=.t.,;
    cFormVar="w_CODATESP",value=ctot(""),;
    HelpContextID = 29060214,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=-2, Top=0
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCOSERATT_2_1.When()
    return(.t.)
  proc oCOSERATT_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCOSERATT_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mpe','DET_EXPO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COSERPRO=DET_EXPO.COSERPRO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
