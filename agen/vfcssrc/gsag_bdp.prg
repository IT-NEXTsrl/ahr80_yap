* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bdp                                                        *
*              Disponibilit� partecipanti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-29                                                      *
* Last revis.: 2008-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bdp",oParentObject,m.pAZIONE)
return(i_retval)

define class tgsag_bdp as StdBatch
  * --- Local variables
  pAZIONE = space(10)
  w_nBaseCal = 0
  ind = 0
  w_ListCount = 0
  w_CntDip = 0
  w_CodDip = space(5)
  w_DPCOGNOM = space(40)
  w_DPNOME = space(40)
  w_DPDESCRI = space(40)
  w_DPTIPRIS = space(1)
  w_TxtItem = space(100)
  w_StatusItem = 0
  w_DatIn = ctod("  /  /  ")
  w_DatFi = ctod("  /  /  ")
  w_OraIn = 0
  w_OraFi = 0
  w_Obj = .NULL.
  w_PATIPRIS = space(1)
  w_CURDIR = space(254)
  w_Title = space(254)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_nBaseCal = VAL(SYS( 11 , cp_CharToDate("01-01-1900", "dd-mm-yyyy") ))
    do case
      case this.pAZIONE=="INIT"
        * --- Inizializziamo i campi ctList
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAZIONE=="AGGIORNASCHEDULE"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAZIONE=="STAMPA"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="CHECKATTIVO" OR this.pAzione=="CHECKDISATTIVO"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="IMPOSTAORA"
        * --- Nell'ambito dello stesso giorno imposta l''ora d'inizio con l'ora inizio del calendario
        if this.oParentObject.w_DatIni=this.oParentObject.w_DatFin AND VAL(this.oParentObject.w_OREINI)=0 AND VAL(this.oParentObject.w_MININI)=0 AND g_OraLavIni*60<( VAL(this.oParentObject.w_OREFIN)*60+VAL(this.oParentObject.w_MINFIN) )
          this.w_Obj = this.oParentObject
          this.oParentObject.w_OREINI = PADL( ALLTRIM( STR( INT(g_OraLavIni) ) ), 2, "0")
          this.oParentObject.w_MININI = PADL( ALLTRIM( STR( ( g_OraLavIni-INT(g_OraLavIni))*60 ) ), 2, "0")
          this.w_Obj.mCalc(.T.)     
        endif
      case this.pAzione=="SELEZIONA" OR this.pAzione=="DESELEZIONA"
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="MODIFICA"
        * --- Doppio click su riga di partecipante
        this.Pag8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAZIONE=="INGRANDISCI"
        this.oParentObject.w_DimScheda = 0
        this.oParentObject.w_ctSchedParte.Top = this.oParentObject.w_ctSchedParte.Top-55
        this.oParentObject.w_ctSchedParte.Height = this.oParentObject.w_ctSchedParte.Height+55
      case this.pAZIONE=="RIDUCI"
        this.oParentObject.w_DimScheda = 1
        this.oParentObject.w_ctSchedParte.Top = this.oParentObject.w_ctSchedParte.Top+55
        this.oParentObject.w_ctSchedParte.Height = this.oParentObject.w_ctSchedParte.Height-55
      case this.pAZIONE=="-1"
        * --- Toglie un giorno
        this.oParentObject.w_DATFIN = this.oParentObject.w_DATFIN - 1
        this.oParentObject.w_DATINI = this.oParentObject.w_DATINI - 1
        * --- Gestiamo o_DATINI e o_DATFIN in modo che il calcolo di w_DATA_INI e w_DATA_FIN sia corretto
         
 this.oParentObject.o_DATINI=CTOD("  -  -  ") 
 this.oParentObject.o_DATFIN=CTOD("  -  -  ")
      case this.pAZIONE=="+1"
        * --- Aggiunge un giorno
        this.oParentObject.w_DATFIN = this.oParentObject.w_DATFIN + 1
        this.oParentObject.w_DATINI = this.oParentObject.w_DATINI + 1
        * --- Gestiamo o_DATINI e o_DATFIN in modo che il calcolo di w_DATA_INI e w_DATA_FIN sia corretto
         
 this.oParentObject.o_DATINI=CTOD("  -  -  ") 
 this.oParentObject.o_DATFIN=CTOD("  -  -  ")
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializziamo i campi CtList aggiungendo le colonne con i vari campi
    *     I parametri sono cos� determinati
    *     
    *     1 - titolo colonna, 2 - larghezza,
    *     3 - tipo della colonna (0 - Text, 1 - Integer, 2 - Real, 3 - Date / Time)
    *     4 - tipo del check box, (0 - nessuno, 1 - bidimensionale, 2 - tridimensionale, 3 - User Defined)
    *     5 - Lock larghezza colonna, 6 - Sort abilitato T/F
    *     7 - Allineamento del testo (0 - Left Justified, 1 - Right Justified, 2 - Centered, 3 - Default)
    * --- ctList_Parte
    this.oParentObject.w_ctList_Parte.SetVisible(.F.)     
    * --- Check
    this.oParentObject.w_ClnCheck = this.oParentObject.w_ctList_Parte.AddColumn(" ",20,0,2,.F.,.F.,2)
    * --- Descrizione risorsa
    this.oParentObject.w_ClnDesRis = this.oParentObject.w_ctList_Parte.AddColumn(AH_MSGFORMAT("Persone"),200,0,0,.F.,.F.,0)
    * --- Decodifichiamo l'utente attivo come risorsa interna
    this.w_CodDip = ReadDipend(i_codUte, "C")
    * --- Inseriamo le persone in ctList_Parte
    if this.oParentObject.w_ClnCheck>0 and this.oParentObject.w_ClnDesRis>0
      * --- Se tutti i valori sono maggiori di zero, � segno che tutte le colonne sono state correttamente inserite
      this.oParentObject.w_ctList_Parte.SetVisible(.T.)     
      this.oParentObject.w_ctList_Parte.ClearList()     
      Dimension aText(2) 
 store "" to aText(1),aText(2)
      * --- Nella query GSAG_MPA estraiamo le persone
      this.w_PATIPRIS = "P"
      * --- Select from ..\AGEN\EXE\QUERY\GSAG_MPA
      do vq_exec with '..\AGEN\EXE\QUERY\GSAG_MPA',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA','',.f.,.t.
      if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA')
        select _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
        locate for 1=1
        do while not(eof())
        * --- Inseriamo ogni elemento in CtList
        * --- Check
        aText( this.oParentObject.w_ClnCheck ) = " "
        * --- Oggetto
        aText( this.oParentObject.w_ClnDesRis ) = ALLTRIM( DESCRIZIONE )
        * --- Componiamo il testo dell'item
        this.w_TxtItem = aText(1)+CHR(10)+aText(2)
        this.ind = this.oParentObject.w_ctList_Parte.AddItem(this.w_TxtItem, DPCODICE)
        this.w_StatusItem = 0
        if DPCODICE=this.w_CodDip
          * --- L'utente attivo � l'unico selezionato
          this.w_StatusItem = 1
        endif
        * --- Avvaloriamo il check
        this.oParentObject.w_ctList_Parte.CellCheck(this.Ind, this.oParentObject.w_ClnCheck, this.w_StatusItem)     
          select _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
          continue
        enddo
        use
      endif
    endif
    * --- ctList_Risorse
    this.oParentObject.w_ctList_Risorse.SetVisible(.F.)     
    * --- Check
    this.oParentObject.w_ClnCheck_Ris = this.oParentObject.w_ctList_Risorse.AddColumn(" ",20,0,2,.F.,.F.,2)
    * --- Descrizione risorsa
    this.oParentObject.w_ClnDesRis_Ris = this.oParentObject.w_ctList_Risorse.AddColumn(AH_MSGFORMAT("Risorse"),180,0,0,.F.,.F.,0)
    * --- Inseriamo le risorse in ctList_Risorse
    if this.oParentObject.w_ClnCheck_Ris>0 and this.oParentObject.w_ClnDesRis_Ris>0
      * --- Se tutti i valori sono maggiori di zero, � segno che tutte le colonne sono state correttamente inserite
      this.oParentObject.w_ctList_Risorse.SetVisible(.T.)     
      this.oParentObject.w_ctList_Risorse.ClearList()     
      Dimension bText(2) 
 store "" to bText(1),bText(2)
      * --- Nella query GSAG_MPA estraiamo le risorse
      this.w_PATIPRIS = "R"
      * --- Select from ..\AGEN\EXE\QUERY\GSAG_MPA
      do vq_exec with '..\AGEN\EXE\QUERY\GSAG_MPA',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA','',.f.,.t.
      if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA')
        select _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
        locate for 1=1
        do while not(eof())
        * --- Inseriamo ogni elemento in CtList
        * --- Check
        bText( this.oParentObject.w_ClnCheck_Ris ) = " "
        * --- Oggetto
        bText( this.oParentObject.w_ClnDesRis_Ris ) = ALLTRIM( DESCRIZIONE )
        * --- Componiamo il testo dell'item
        this.w_TxtItem = bText(1)+CHR(10)+bText(2)
        this.ind = this.oParentObject.w_ctList_Risorse.AddItem(this.w_TxtItem, DPCODICE)
        this.w_StatusItem = 0
        * --- Avvaloriamo il check
        this.oParentObject.w_ctList_Risorse.CellCheck(this.Ind, this.oParentObject.w_ClnCheck_Ris, this.w_StatusItem)     
          select _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Popoliamo CtSchedule estraendo solo i dipendenti che in ctList hanno il check attivo
    this.oParentObject.w_ctSchedParte.SetVisible(.F.)     
    this.oParentObject.w_ctSchedParte.ClearSchedule()     
    this.oParentObject.w_ctSchedParte.SetDate(TTOD(this.oParentObject.w_Data_Ini), TTOD(this.oParentObject.w_Data_Fin), HOUR(this.oParentObject.w_Data_Ini)*60+MINUTE(this.oParentObject.w_Data_Ini), HOUR(this.oParentObject.w_Data_Fin)*60+MINUTE(this.oParentObject.w_Data_Fin))     
    * --- In ctList gli elementi partono da 0
    this.w_ListCount = this.oParentObject.w_ctList_Parte.ListCount()
    this.w_CntDip = 0
    do while this.w_CntDip <= this.w_ListCount
      * --- Cicliamo su tutti gli elementi di ctList_Parte e per ciascuno controlliamo il valore del check.
      *     La procedura CheckOn ritorna il codice della risorsa nel caso in cui il check sia attivo, vuoto altrimenti
      this.w_CodDip = this.oParentObject.w_ctList_Parte.CheckOn(this.w_CntDip, this.oParentObject.w_ClnCheck)
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CntDip = this.w_CntDip + 1
    enddo
    * --- ctList_Risorse
    this.w_ListCount = this.oParentObject.w_ctList_Risorse.ListCount()
    this.w_CntDip = 0
    do while this.w_CntDip <= this.w_ListCount
      * --- Cicliamo su tutti gli elementi di ctList_Risorse e per ciascuno controlliamo il valore del check.
      *     La procedura CheckOn ritorna il codice della risorsa nel caso in cui il check sia attivo, vuoto altrimenti
      this.w_CodDip = this.oParentObject.w_ctList_Risorse.CheckOn(this.w_CntDip, this.oParentObject.w_ClnCheck_Ris)
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CntDip = this.w_CntDip + 1
    enddo
    this.oParentObject.w_ctSchedParte.SetVisible(.T.)     
    if !oParentObject.oPgFrm.ActivePage = 1
      oParentObject.oPgFrm.ActivePage = 1
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Deve aggiornare lo schedule della risorsa il cui seriale � passato come parametro
    if !EMPTY(this.oParentObject.w_DipCodice)
      * --- Modifichiamo il valore di SELEZION in TMP_DISP
      if this.pAzione=="CHECKDISATTIVO"
        * --- Dobbiamo cancellare l'item e tutte le sue task bar dallo scheduler
        this.w_ListCount = this.oParentObject.w_ctSchedParte.ListCount()
        this.w_CntDip = 1
        do while this.w_CntDip <= this.w_ListCount
          * --- Cicliamo su tutti gli elementi di ctSchedParte fino a che non troviamo l'item relativo al dipendente selezionato
          *     Quando lo troveremo ci limiteremo a cancellare la riga
          this.w_CodDip = this.oParentObject.w_ctSchedParte.ItemCargo(this.w_CntDip)
          if this.w_CodDip=this.oParentObject.w_DipCodice
            this.oParentObject.w_ctSchedParte.delItem(this.w_CntDip)     
            EXIT
          else
            this.w_CntDip = this.w_CntDip + 1
          endif
        enddo
      else
        * --- pAzione=='CHECKATTIVO'
        *     Aggiungiamo l'item con le relative attivit�
        this.w_CodDip = this.oParentObject.w_DipCodice
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestisce la risorsa inserendo una time bar per ciascuna attivit�
    if !EMPTY(this.w_CodDip)
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPCOGNOM,DPNOME,DPTIPRIS,DPDESCRI"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(this.w_CodDip);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPCOGNOM,DPNOME,DPTIPRIS,DPDESCRI;
          from (i_cTable) where;
              DPCODICE = this.w_CodDip;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DPCOGNOM = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
        this.w_DPNOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
        this.w_DPTIPRIS = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
        this.w_DPDESCRI = NVL(cp_ToDate(_read_.DPDESCRI),cp_NullValue(_read_.DPDESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.ind = this.oParentObject.w_ctSchedParte.AddItem( IIF( this.w_DPTIPRIS=="P", ALLTRIM(this.w_DPCOGNOM)+" "+ALLTRIM(this.w_DPNOME), ALLTRIM(this.w_DPDESCRI)), this.w_CodDip)
      * --- Select from QUERY\GSAG_BCP
      do vq_exec with 'QUERY\GSAG_BCP',this,'_Curs_QUERY_GSAG_BCP','',.f.,.t.
      if used('_Curs_QUERY_GSAG_BCP')
        select _Curs_QUERY_GSAG_BCP
        locate for 1=1
        do while not(eof())
        * --- Gestione data attivit�
        if !ISNULL(ATDATINI) AND ATDATINI >= this.oParentObject.w_Data_Ini
          * --- L'appuntamento inizia nell'intervallo considerato
          this.w_DatIn = VAL(SYS(11,ATDATINI)) - this.w_nBaseCal
          this.w_OraIn = HOUR(ATDATINI)*60+MINUTE(ATDATINI)
        else
          * --- L'appuntamento inizia prima della selezione
          this.w_DatIn = VAL(SYS(11,this.oParentObject.w_Data_Ini)) - this.w_nBaseCal
          this.w_OraIn = HOUR(this.oParentObject.w_Data_Ini)*60+MINUTE(this.oParentObject.w_Data_Ini)
        endif
        if !ISNULL(ATDATFIN) AND ATDATFIN<= this.oParentObject.w_Data_Fin
          this.w_DatFi = VAL(SYS(11,ATDATFIN)) - this.w_nBaseCal
          this.w_OraFi = HOUR(ATDATFIN)*60+MINUTE(ATDATFIN)
        else
          this.w_DatFi = TTOD(this.oParentObject.w_DATA_FIN)
          this.w_OraFi = HOUR(this.oParentObject.w_Data_Fin)*60+MINUTE(this.oParentObject.w_Data_Fin)
        endif
        * --- AddColorBar
        this.oParentObject.w_ctSchedParte.AddTimeBar(this.w_OraIn, this.w_OraFi, this.w_DatIn, this.w_DatFi, DISPOCOL, this.ind)     
          select _Curs_QUERY_GSAG_BCP
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa
    this.w_CURDIR = SYS(5)+CURDIR()
    if TTOD(this.oParentObject.w_Data_Ini)=TTOD(this.oParentObject.w_DATA_FIN)
      this.w_Title = Ah_MsgFormat("Di")+" "+ALLTRIM(g_GIORNO[DOW(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Ini)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Ini)))
    else
      this.w_Title = Ah_MsgFormat("Da")+" "+ALLTRIM(g_GIORNO[DOW(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Ini)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Ini)))+" "+Ah_MsgFormat("a")+" "+ALLTRIM(g_GIORNO[DOW(this.oParentObject.w_Data_Fin)])+" "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Fin)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Fin)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Fin)))
    endif
    this.oParentObject.w_ctSchedParte.PrintTitles(Ah_MsgFormat("Stampa disponibilit� delle persone"), this.w_Title)     
    this.oParentObject.w_ctSchedParte.PrintSchedule()     
    cd (this.w_CURDIR)
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona/deseleziona tutti
    this.w_ListCount = this.oParentObject.w_ctList_Parte.ListCount()
    this.w_CntDip = 0
    if this.pAzione=="SELEZIONA"
      * --- Check attivo
      this.w_StatusItem = 1
    else
      * --- Check disattivo
      this.w_StatusItem = 0
    endif
    do while this.w_CntDip <= this.w_ListCount-1
      * --- Cicliamo su tutti gli elementi di ctSchedParte ed impostiamo il check
      this.oParentObject.w_ctList_Parte.CellCheck(this.w_CntDip, this.oParentObject.w_ClnCheck, this.w_StatusItem)     
      this.w_CntDip = this.w_CntDip + 1
    enddo
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_DatIni=this.oParentObject.w_DatFin AND !EMPTY(this.oParentObject.w_DipendSel)
      this.w_CodDip = this.oParentObject.w_ctSchedParte.ItemCargo(this.oParentObject.w_DipendSel)
      * --- Deve modificare l'attivit� il cui seriale � passato come parametro
      * --- Instanzio l'anagrafica
      this.w_OBJECT = GSAG_KAG()
      * --- Controllo se ha passato il test di accesso
      if !(this.w_OBJECT.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_OBJECT.w_CODPART = this.w_CodDip
      * --- Impostando o_CODPART a spazi ne forziamo la rilettura ed aggiorniamo la caption del form
      this.w_OBJECT.o_CODPART = ""
      this.w_OBJECT.w_UDIENZE = this.oParentObject.w_UDIENZE
      this.w_OBJECT.w_ASSENZE = this.oParentObject.w_ASSENZE
      this.w_OBJECT.w_ATTFUOSTU = this.oParentObject.w_ATTFUOSTU
      this.w_OBJECT.w_ATTGENER = this.oParentObject.w_ATTGENER
      this.w_OBJECT.w_ATTSTU = this.oParentObject.w_ATTSTU
      this.w_OBJECT.w_DAFARE = this.oParentObject.w_DAFARE
      this.w_OBJECT.w_MEMO = this.oParentObject.w_MEMO
      this.w_OBJECT.w_SESSTEL = this.oParentObject.w_SESSTEL
      this.w_OBJECT.w_DataSel = TTOD(this.oParentObject.w_Data_Ini)
      this.w_OBJECT.w_calAttivo = 1
      this.w_OBJECT.NotifyEvent("ChangeDate")     
    endif
  endproc


  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
    endif
    if used('_Curs_QUERY_GSAG_BCP')
      use in _Curs_QUERY_GSAG_BCP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
