* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bgm                                                        *
*              Gestione menu in elenco attivita                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-27                                                      *
* Last revis.: 2016-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bgm",oParentObject,m.pCursore)
return(i_retval)

define class tgsag_bgm as StdBatch
  * --- Local variables
  pCursore = space(10)
  w_RETVAL = 0
  w_PADRE = .NULL.
  w_CursorZoomMemo = space(10)
  w_CursorZoomAtt = space(10)
  w_PREAVVISO = .f.
  w_CURSOR = space(10)
  w_RETAGGATT = space(254)
  w_ResMsg = .NULL.
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_MESBLOK = space(0)
  w_READAZI = space(5)
  w_PACHKCAR = space(1)
  w_SERATT = space(20)
  w_MESS = space(254)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  DET_GEN_idx=0
  PAR_AGEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestisce men� dal bottone Elabora in Elenco Attivit�
    * --- Zoom delle Cose da fare, Note, e Preavvisi
    * --- Zoom delle Attivit�
    this.w_PADRE = this.oParentObject
    this.w_RETVAL = 0
    if EMPTY(this.pCURSORE) OR (!USED(this.pCURSORE) AND UPPER(this.w_PADRE.Class)=="TGSAG_KRA")
      this.pCursore = SYS(2015)
      SELECT ATSERIAL FROM (this.w_PADRE.w_AGKRA_ZOOM.cCursor) WHERE XCHK=1 UNION ALL SELECT ATSERIAL FROM (this.w_PADRE.w_AGKRA_ZOOM3.cCursor) WHERE XCHK=1 INTO CURSOR (this.pCursore)
    endif
    if !USED(this.pCURSORE) OR RECCOUNT(this.pCursore)<=0
      ah_ErrorMsg("Non ci sono attivit� selezionate")
      USE IN SELECT(this.pCURSORE)
      i_retcode = 'stop'
      return
    endif
    if i_VisualTheme = -1
      L_RETVAL=0
      DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT
      DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Evadi")
      DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Completa")
      DEFINE BAR 3 OF popCmd PROMPT ah_MsgFormat("Rinvio") SKIP FOR !IsAlt()
      DEFINE BAR 4 OF popCmd PROMPT ah_MsgFormat("Sposta")
      DEFINE BAR 5 OF popCmd PROMPT ah_MsgFormat("Elimina")
      ON SELECTION BAR 1 OF popCmd L_RETVAL = 3
      ON SELECTION BAR 2 OF popCmd L_RETVAL = 4
      ON SELECTION BAR 3 OF popCmd L_RETVAL = 1
      ON SELECTION BAR 4 OF popCmd L_RETVAL = 2
      ON SELECTION BAR 5 OF popCmd L_RETVAL = 5
      ACTIVATE POPUP popCmd
      DEACTIVATE POPUP popCmd
      RELEASE POPUPS popCmd
      this.w_RETVAL = L_RETVAL
      Release L_RETVAL
    else
      local oBtnMenu, oMenuItem
      oBtnMenu = CreateObject("cbPopupMenu")
      oBtnMenu.oParentObject = This
      oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
      oMenuItem = oBtnMenu.AddMenuItem()
      oMenuItem.Caption = Ah_Msgformat("Evadi")
      oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=3"
      oMenuItem.Visible=.T.
      oMenuItem = oBtnMenu.AddMenuItem()
      oMenuItem.Caption = Ah_Msgformat("Completa")
      oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=4"
      oMenuItem.Visible=.T.
      if IsAlt()
        oMenuItem = oBtnMenu.AddMenuItem()
        oMenuItem.Caption = Ah_Msgformat("Rinvio")
        oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=1"
        oMenuItem.Visible=.T.
      endif
      oMenuItem = oBtnMenu.AddMenuItem()
      oMenuItem.Caption = Ah_Msgformat("Sposta")
      oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=2"
      oMenuItem.Visible=.T.
      oMenuItem = oBtnMenu.AddMenuItem()
      oMenuItem.Caption = Ah_Msgformat("Elimina")
      oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=5"
      oMenuItem.Visible=.T.
      oBtnMenu.InitMenu()
      oBtnMenu.ShowMenu()
      oBtnMenu = .NULL.
    endif
    this.w_CursorZoomMemo = this.w_PADRE.w_AGKRA_ZOOM3.ccursor
    this.w_CursorZoomAtt = this.w_PADRE.w_AGKRA_ZOOM.ccursor
    do case
      case this.w_RETVAL = 1
        * --- Rinvio Attivit�
        this.w_CURSOR = SYS(2015)
        SELECT ATSERIAL FROM (this.w_CursorZoomMemo) WHERE XCHK=1 UNION SELECT ATSERIAL FROM (this.w_CursorZoomAtt) WHERE XCHK=1 INTO CURSOR (this.w_CURSOR)
        this.w_RETAGGATT = GSAG_BEL(this, "RINVIO", this.w_CURSOR, .T.)
        if !EMPTY(this.w_RETAGGATT)
          ah_ErrorMsg("%1",48,"",this.w_RETAGGATT)
        endif
      case this.w_RETVAL = 2
        * --- Spostamento date Attivit�
        this.w_MESS = ah_Msgformat("Attenzione non sar� possibile spostare automaticamente le attivit� collegate a quelle selezionate , %0Confermi comunque l'elaborazione?")
        if ah_YesNo(this.w_MESS)
          * --- Nello sposatmento massivo non � previsto il controlllo data iniziale
          this.w_PACHKCAR = "N"
          this.w_CURSOR = SYS(2015)
          SELECT ATSERIAL FROM (this.w_CursorZoomMemo) WHERE XCHK=1 UNION SELECT ATSERIAL FROM (this.w_CursorZoomAtt) WHERE XCHK=1 INTO CURSOR (this.w_CURSOR)
          if !GSAG_BEL(this, "SPOSTA", this.w_CURSOR, .T.)
            this.w_MESBLOK = this.w_ResMsg.ComposeMessage()
            this.w_RESOCON1 = this.w_ResMsg.ComposeMessage()
            do GSUT_KLG with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_ResMsg = .NULL.
        endif
      case this.w_RETVAL = 3
        * --- Evasione Attivit�
        this.w_PREAVVISO = .F.
        * --- Dobbiamo cercare eventuali preavvisi: se ci sono, chiediamo se evaderli
        SELECT (this.w_CursorZoomMemo)
        GO TOP
        LOCATE FOR XCHK=1 AND ATDATORD>this.oParentObject.w_DATA_FIN
        this.w_PREAVVISO = FOUND() AND ah_YesNo("Sono state selezionate attivit� con preavviso non comprese %0nell'arco di data selezionato. Confermi l'evasione?", "")
        this.w_CURSOR = SYS(2015)
        SELECT ATSERIAL FROM (this.w_CursorZoomMemo) WHERE XCHK=1 AND !(ATDATORD > this.oParentObject.w_DATA_FIN AND !this.w_PREAVVISO) UNION SELECT ATSERIAL FROM (this.w_CursorZoomAtt) WHERE XCHK=1 AND !(ATSTATUS $ "FP") INTO CURSOR (this.w_CURSOR)
        this.w_RETAGGATT = GSAG_BEM(this, this.w_CURSOR, .T.)
        * --- Viene simulata la ricerca presente in maschera
        this.w_PADRE.NotifyEvent("Ricerca")     
      case this.w_RETVAL = 4
        * --- Completa Attivit�
        this.w_PREAVVISO = .F.
        * --- Dobbiamo cercare eventuali preavvisi: se ci sono, chiediamo se evaderli
        SELECT (this.w_CursorZoomMemo)
        GO TOP
        LOCATE FOR XCHK=1 AND ATDATORD>this.oParentObject.w_DATA_FIN AND ATSTATUS<>"P"
        this.w_PREAVVISO = FOUND() AND ah_YesNo("Sono state selezionate attivit� con preavviso non comprese %0nell'arco di data selezionato. Confermi il completamento?", "")
        this.w_CURSOR = SYS(2015)
        SELECT ATSERIAL FROM (this.w_CursorZoomMemo) WHERE XCHK=1 AND ATSTATUS<>"P" AND !(ATDATORD > this.oParentObject.w_DATA_FIN AND !this.w_PREAVVISO) UNION SELECT ATSERIAL FROM (this.w_CursorZoomAtt) WHERE XCHK=1 AND ATSTATUS <> "P" INTO CURSOR (this.w_CURSOR)
        this.w_RETAGGATT = GSAG_BEL(this, "COMPLETA", this.w_CURSOR, .T.)
        if !EMPTY(this.w_RETAGGATT)
          ah_ErrorMsg("%1",48,"",this.w_RETAGGATT)
        endif
        this.w_PADRE.NotifyEvent("Ricerca")     
      case this.w_RETVAL = 5
        * --- Read from DET_GEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DET_GEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2],.t.,this.DET_GEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MDSERIAL"+;
            " from "+i_cTable+" DET_GEN where ";
                +"MDSERATT = "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MDSERIAL;
            from (i_cTable) where;
                MDSERATT = this.oParentObject.w_ATSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERATT = NVL(cp_ToDate(_read_.MDSERIAL),cp_NullValue(_read_.MDSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if I_ROWS<>0
          this.w_MESS = ah_Msgformat("Attenzione: attivit� generata da piano di generazione attivit� (%1)%0La cancellazione manuale di attivit� non comporta gli aggiornamenti automatici delle date prossima attivit� sugli elementi contratto%0Confermi ugualmente?",this.w_SERATT)
        else
          this.w_MESS = ah_Msgformat("Attenzione non sar� possibile eliminare automaticamente le attivit� collegate a quelle selezionate %0Confermi comunque l'elaborazione?")
        endif
        if ah_YesNo(this.w_MESS)
          this.w_CURSOR = SYS(2015)
          SELECT ATSERIAL FROM (this.w_CursorZoomMemo) WHERE XCHK=1 UNION SELECT ATSERIAL FROM (this.w_CursorZoomAtt) WHERE XCHK=1 INTO CURSOR (this.w_CURSOR)
          this.w_RETAGGATT = GSAG_BEL(this, "ELIMINA", this.w_CURSOR, .T.)
          if !EMPTY(this.w_RETAGGATT)
            ah_ErrorMsg("%1",48,"",this.w_RETAGGATT)
          endif
        else
          ah_ErrorMsg("Operazione interrotta come richiesto",64)
        endif
    endcase
    USE IN SELECT(this.pCURSORE)
    this.w_PADRE.NotifyEvent("Ricerca")     
  endproc


  proc Init(oParentObject,pCursore)
    this.pCursore=pCursore
    this.w_ResMsg=createobject("Ah_Message")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='DET_GEN'
    this.cWorkTables[3]='PAR_AGEN'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore"
endproc
