* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_klc                                                        *
*              Dettagli listini                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-22                                                      *
* Last revis.: 2012-04-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_klc",oParentObject))

* --- Class definition
define class tgsag_klc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 669
  Height = 96
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-18"
  HelpContextID=216672105
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=44

  * --- Constant Properties
  _IDX = 0
  LISTINI_IDX = 0
  TIP_DOCU_IDX = 0
  VOC_COST_IDX = 0
  CENCOST_IDX = 0
  ATTIVITA_IDX = 0
  CAN_TIER_IDX = 0
  cPrg = "gsag_klc"
  cComment = "Dettagli listini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_MVCODVAL = space(3)
  w_MVFLVEAC = space(10)
  w_ELTCOLIS = space(5)
  w_DATALIST = ctod('  /  /  ')
  w_CAUDOC = space(5)
  w_ELSCOLIS = space(5)
  w_ELPROLIS = space(5)
  w_ELPROSCO = space(5)
  w_DESLIS = space(40)
  w_DESLIS2 = space(40)
  w_VALLIS = space(3)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_VALLIS2 = space(3)
  w_INILIS2 = ctod('  /  /  ')
  w_FINLIS2 = ctod('  /  /  ')
  w_FINLIS3 = ctod('  /  /  ')
  w_INILIS3 = ctod('  /  /  ')
  w_VALLIS3 = space(3)
  w_FINLIS4 = ctod('  /  /  ')
  w_INILIS4 = ctod('  /  /  ')
  w_VALLIS4 = space(3)
  w_TIPSER = space(1)
  w_IVALIS = space(1)
  w_FLGCIC = space(1)
  w_FLGCIC2 = space(1)
  w_FLGCIC3 = space(1)
  w_FLGCIC4 = space(1)
  w_TIPLIS4 = space(1)
  o_TIPLIS4 = space(1)
  w_TIPLIS3 = space(1)
  o_TIPLIS3 = space(1)
  w_TIPLIS = space(1)
  w_TIPLIS2 = space(1)
  w_FLSTAT = space(1)
  w_F2STAT = space(1)
  w_F3STAT = space(1)
  w_F4STAT = space(1)
  w_FLSCOR = space(1)
  w_MVTSCLIS = space(5)
  w_MVTCOLIS = space(5)
  w_IVALISP = space(1)
  w_ELSCONT1 = 0
  w_ELSCONT2 = 0
  w_ELSCONT3 = 0
  w_ELSCONT4 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_klcPag1","gsag_klc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati di riga")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELTCOLIS_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='VOC_COST'
    this.cWorkTables[4]='CENCOST'
    this.cWorkTables[5]='ATTIVITA'
    this.cWorkTables[6]='CAN_TIER'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVCODVAL=space(3)
      .w_MVFLVEAC=space(10)
      .w_ELTCOLIS=space(5)
      .w_DATALIST=ctod("  /  /  ")
      .w_CAUDOC=space(5)
      .w_ELSCOLIS=space(5)
      .w_ELPROLIS=space(5)
      .w_ELPROSCO=space(5)
      .w_DESLIS=space(40)
      .w_DESLIS2=space(40)
      .w_VALLIS=space(3)
      .w_INILIS=ctod("  /  /  ")
      .w_FINLIS=ctod("  /  /  ")
      .w_VALLIS2=space(3)
      .w_INILIS2=ctod("  /  /  ")
      .w_FINLIS2=ctod("  /  /  ")
      .w_FINLIS3=ctod("  /  /  ")
      .w_INILIS3=ctod("  /  /  ")
      .w_VALLIS3=space(3)
      .w_FINLIS4=ctod("  /  /  ")
      .w_INILIS4=ctod("  /  /  ")
      .w_VALLIS4=space(3)
      .w_TIPSER=space(1)
      .w_IVALIS=space(1)
      .w_FLGCIC=space(1)
      .w_FLGCIC2=space(1)
      .w_FLGCIC3=space(1)
      .w_FLGCIC4=space(1)
      .w_TIPLIS4=space(1)
      .w_TIPLIS3=space(1)
      .w_TIPLIS=space(1)
      .w_TIPLIS2=space(1)
      .w_FLSTAT=space(1)
      .w_F2STAT=space(1)
      .w_F3STAT=space(1)
      .w_F4STAT=space(1)
      .w_FLSCOR=space(1)
      .w_MVTSCLIS=space(5)
      .w_MVTCOLIS=space(5)
      .w_IVALISP=space(1)
      .w_ELSCONT1=0
      .w_ELSCONT2=0
      .w_ELSCONT3=0
      .w_ELSCONT4=0
      .w_MVCODVAL=oParentObject.w_MVCODVAL
      .w_ELTCOLIS=oParentObject.w_ELTCOLIS
      .w_DATALIST=oParentObject.w_DATALIST
      .w_ELSCOLIS=oParentObject.w_ELSCOLIS
      .w_ELPROLIS=oParentObject.w_ELPROLIS
      .w_ELPROSCO=oParentObject.w_ELPROSCO
      .w_TIPSER=oParentObject.w_TIPSER
      .w_ELSCONT1=oParentObject.w_ELSCONT1
      .w_ELSCONT2=oParentObject.w_ELSCONT2
      .w_ELSCONT3=oParentObject.w_ELSCONT3
      .w_ELSCONT4=oParentObject.w_ELSCONT4
          .DoRTCalc(1,1,.f.)
        .w_MVFLVEAC = 'V'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ELTCOLIS))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,6,.f.)
        if not(empty(.w_ELSCOLIS))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_ELPROLIS))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_ELPROSCO))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,36,.f.)
        .w_FLSCOR = this.oparentobject.w_FLSCOR
    endwith
    this.DoRTCalc(38,44,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oELTCOLIS_1_3.enabled = i_bVal
      .Page1.oPag.oELSCOLIS_1_6.enabled = i_bVal
      .Page1.oPag.oELPROLIS_1_7.enabled = i_bVal
      .Page1.oPag.oELPROSCO_1_8.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MVCODVAL=.w_MVCODVAL
      .oParentObject.w_ELTCOLIS=.w_ELTCOLIS
      .oParentObject.w_DATALIST=.w_DATALIST
      .oParentObject.w_ELSCOLIS=.w_ELSCOLIS
      .oParentObject.w_ELPROLIS=.w_ELPROLIS
      .oParentObject.w_ELPROSCO=.w_ELPROSCO
      .oParentObject.w_TIPSER=.w_TIPSER
      .oParentObject.w_ELSCONT1=.w_ELSCONT1
      .oParentObject.w_ELSCONT2=.w_ELSCONT2
      .oParentObject.w_ELSCONT3=.w_ELSCONT3
      .oParentObject.w_ELSCONT4=.w_ELSCONT4
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_MVFLVEAC = 'V'
        .DoRTCalc(3,6,.t.)
        if .o_TIPLIS4<>.w_TIPLIS4
            .w_ELPROLIS = iif(.w_TIPLIS4='E' And Empty(.w_ELPROLIS), .w_ELPROSCO, .w_ELPROLIS)
          .link_1_7('Full')
        endif
        if .o_TIPLIS3<>.w_TIPLIS3
            .w_ELPROSCO = iif(.w_TIPLIS3='E' And Empty(.w_ELPROSCO), .w_ELPROLIS, .w_ELPROSCO)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,36,.t.)
            .w_FLSCOR = this.oparentobject.w_FLSCOR
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(38,44,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oELTCOLIS_1_3.enabled = this.oPgFrm.Page1.oPag.oELTCOLIS_1_3.mCond()
    this.oPgFrm.Page1.oPag.oELSCOLIS_1_6.enabled = this.oPgFrm.Page1.oPag.oELSCOLIS_1_6.mCond()
    this.oPgFrm.Page1.oPag.oELPROLIS_1_7.enabled = this.oPgFrm.Page1.oPag.oELPROLIS_1_7.mCond()
    this.oPgFrm.Page1.oPag.oELPROSCO_1_8.enabled = this.oPgFrm.Page1.oPag.oELPROSCO_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_klc
    If  cEvent='w_ELPROLIS Changed' or cEvent='w_ELPROSCO Changed' or cEvent='w_ELTCOLIS Changed' or cEvent='w_ELSCOLIS Changed'
        this.oparentobject.w_ELPROLIS=This.w_ELPROLIS
        this.oparentobject.w_ELPROSCO=This.w_ELPROSCO
        this.oparentobject.w_ELTCOLIS=This.w_ELTCOLIS
        this.oparentobject.w_ELSCOLIS=This.w_ELSCOLIS
        this.oparentobject.w_ELPREZZO=0
        This.oparentobject.w_ELSCONT1=0
        This.oparentobject.w_ELSCONT2=0
        This.oparentobject.w_ELSCONT3=0
        This.oparentobject.w_ELSCONT4=0
        IF Not Empty(Alltrim(This.w_ELPROLIS+This.w_ELPROSCO+This.w_ELTCOLIS+This.w_ELSCOLIS))
          this.oparentobject.NotifyEvent('LisRig')
        else
         This.oparentobject.mcalc(.t.)
        endif
    endif
    
    If (cEvent='w_ELPROSCO Changed' OR cEvent='w_ELSCOLIS Changed')
        this.w_ELSCONT1=This.oparentobject.w_ELSCONT1
        this.w_ELSCONT2=This.oparentobject.w_ELSCONT2
        this.w_ELSCONT3=This.oparentobject.w_ELSCONT3
        this.w_ELSCONT4=This.oparentobject.w_ELSCONT4
    Endif
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ELTCOLIS
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELTCOLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ELTCOLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ELTCOLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELTCOLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_ELTCOLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_ELTCOLIS)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELTCOLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oELTCOLIS_1_3'),i_cWhere,'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELTCOLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ELTCOLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ELTCOLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELTCOLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC = NVL(_Link_.LSFLGCIC,space(1))
      this.w_FLSTAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELTCOLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
      this.w_VALLIS = space(3)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_TIPLIS = space(1)
      this.w_FLGCIC = space(1)
      this.w_FLSTAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKLISD(.w_ELTCOLIS, .w_IVALIS, .w_VALLIS, .w_INILIS, .w_FINLIS,.w_TIPLIS,'P',.w_FLGCIC, .w_FLSTAT,.w_FLSCOR, .w_MVCODVAL, .w_DATALIST,'V')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELTCOLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_IVALIS = space(1)
        this.w_VALLIS = space(3)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_TIPLIS = space(1)
        this.w_FLGCIC = space(1)
        this.w_FLSTAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELTCOLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELSCOLIS
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELSCOLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ELSCOLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ELSCOLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELSCOLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_ELSCOLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_ELSCOLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELSCOLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oELSCOLIS_1_6'),i_cWhere,'',"ELENCO LISTINI",'listdocu2.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELSCOLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ELSCOLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ELSCOLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELSCOLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS2 = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS2 = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS2 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS2 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS2 = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC2 = NVL(_Link_.LSFLGCIC,space(1))
      this.w_F2STAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELSCOLIS = space(5)
      endif
      this.w_DESLIS2 = space(40)
      this.w_VALLIS2 = space(3)
      this.w_INILIS2 = ctod("  /  /  ")
      this.w_FINLIS2 = ctod("  /  /  ")
      this.w_TIPLIS2 = space(1)
      this.w_FLGCIC2 = space(1)
      this.w_F2STAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKLISD(.w_ELSCOLIS, 'L', .w_VALLIS2, .w_INILIS2, .w_FINLIS2,.w_TIPLIS2,'S',.w_FLGCIC2, .w_F2STAT,.w_FLSCOR, .w_MVCODVAL, .w_DATALIST,'V')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELSCOLIS = space(5)
        this.w_DESLIS2 = space(40)
        this.w_VALLIS2 = space(3)
        this.w_INILIS2 = ctod("  /  /  ")
        this.w_FINLIS2 = ctod("  /  /  ")
        this.w_TIPLIS2 = space(1)
        this.w_FLGCIC2 = space(1)
        this.w_F2STAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELSCOLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELPROLIS
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELPROLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ELPROLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ELPROLIS))
          select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELPROLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELPROLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oELPROLIS_1_7'),i_cWhere,'',"ELENCO PROMOZIONI",'prodocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELPROLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ELPROLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ELPROLIS)
            select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELPROLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_IVALISP = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALLIS3 = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS3 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS3 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS3 = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC3 = NVL(_Link_.LSFLGCIC,space(1))
      this.w_F3STAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELPROLIS = space(5)
      endif
      this.w_IVALISP = space(1)
      this.w_VALLIS3 = space(3)
      this.w_INILIS3 = ctod("  /  /  ")
      this.w_FINLIS3 = ctod("  /  /  ")
      this.w_TIPLIS3 = space(1)
      this.w_FLGCIC3 = space(1)
      this.w_F3STAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKLISD(.w_ELPROLIS, .w_IVALISP, .w_VALLIS3, .w_INILIS3, .w_FINLIS3,.w_TIPLIS3,'P',.w_FLGCIC3, .w_F3STAT,.w_FLSCOR, .w_MVCODVAL,.w_DATALIST,'V',.w_ELTCOLIS,.w_ELSCOLIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELPROLIS = space(5)
        this.w_IVALISP = space(1)
        this.w_VALLIS3 = space(3)
        this.w_INILIS3 = ctod("  /  /  ")
        this.w_FINLIS3 = ctod("  /  /  ")
        this.w_TIPLIS3 = space(1)
        this.w_FLGCIC3 = space(1)
        this.w_F3STAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELPROLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELPROSCO
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELPROSCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ELPROSCO)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ELPROSCO))
          select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELPROSCO)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELPROSCO) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oELPROSCO_1_8'),i_cWhere,'',"ELENCO PROMOZIONI",'prodocu2.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELPROSCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ELPROSCO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ELPROSCO)
            select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELPROSCO = NVL(_Link_.LSCODLIS,space(5))
      this.w_VALLIS4 = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS4 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS4 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS4 = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC4 = NVL(_Link_.LSFLGCIC,space(1))
      this.w_F4STAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELPROSCO = space(5)
      endif
      this.w_VALLIS4 = space(3)
      this.w_INILIS4 = ctod("  /  /  ")
      this.w_FINLIS4 = ctod("  /  /  ")
      this.w_TIPLIS4 = space(1)
      this.w_FLGCIC4 = space(1)
      this.w_F4STAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKLISD(.w_ELPROSCO, 'L', .w_VALLIS4, .w_INILIS4, .w_FINLIS4,.w_TIPLIS4,'S',.w_FLGCIC4, .w_F4STAT,.w_FLSCOR, .w_MVCODVAL, .w_DATALIST,'V', .w_ELTCOLIS,.w_ELSCOLIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELPROSCO = space(5)
        this.w_VALLIS4 = space(3)
        this.w_INILIS4 = ctod("  /  /  ")
        this.w_FINLIS4 = ctod("  /  /  ")
        this.w_TIPLIS4 = space(1)
        this.w_FLGCIC4 = space(1)
        this.w_F4STAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELPROSCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELTCOLIS_1_3.value==this.w_ELTCOLIS)
      this.oPgFrm.Page1.oPag.oELTCOLIS_1_3.value=this.w_ELTCOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oELSCOLIS_1_6.value==this.w_ELSCOLIS)
      this.oPgFrm.Page1.oPag.oELSCOLIS_1_6.value=this.w_ELSCOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oELPROLIS_1_7.value==this.w_ELPROLIS)
      this.oPgFrm.Page1.oPag.oELPROLIS_1_7.value=this.w_ELPROLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oELPROSCO_1_8.value==this.w_ELPROSCO)
      this.oPgFrm.Page1.oPag.oELPROSCO_1_8.value=this.w_ELPROSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_9.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_9.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS2_1_10.value==this.w_DESLIS2)
      this.oPgFrm.Page1.oPag.oDESLIS2_1_10.value=this.w_DESLIS2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKLISD(.w_ELTCOLIS, .w_IVALIS, .w_VALLIS, .w_INILIS, .w_FINLIS,.w_TIPLIS,'P',.w_FLGCIC, .w_FLSTAT,.w_FLSCOR, .w_MVCODVAL, .w_DATALIST,'V'))  and (.w_TIPSER <> 'D'  And Empty(.w_ELPROLIS+.w_ELPROSCO))  and not(empty(.w_ELTCOLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELTCOLIS_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKLISD(.w_ELSCOLIS, 'L', .w_VALLIS2, .w_INILIS2, .w_FINLIS2,.w_TIPLIS2,'S',.w_FLGCIC2, .w_F2STAT,.w_FLSCOR, .w_MVCODVAL, .w_DATALIST,'V'))  and (.w_TIPSER <> 'D'  And Empty(.w_ELPROLIS+.w_ELPROSCO))  and not(empty(.w_ELSCOLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELSCOLIS_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKLISD(.w_ELPROLIS, .w_IVALISP, .w_VALLIS3, .w_INILIS3, .w_FINLIS3,.w_TIPLIS3,'P',.w_FLGCIC3, .w_F3STAT,.w_FLSCOR, .w_MVCODVAL,.w_DATALIST,'V',.w_ELTCOLIS,.w_ELSCOLIS))  and (.w_TIPSER <> 'D' And !Empty(.w_ELTCOLIS+.w_ELSCOLIS))  and not(empty(.w_ELPROLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELPROLIS_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKLISD(.w_ELPROSCO, 'L', .w_VALLIS4, .w_INILIS4, .w_FINLIS4,.w_TIPLIS4,'S',.w_FLGCIC4, .w_F4STAT,.w_FLSCOR, .w_MVCODVAL, .w_DATALIST,'V', .w_ELTCOLIS,.w_ELSCOLIS))  and (.w_TIPSER <> 'D' And !Empty(.w_ELTCOLIS+.w_ELSCOLIS))  and not(empty(.w_ELPROSCO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELPROSCO_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPLIS4 = this.w_TIPLIS4
    this.o_TIPLIS3 = this.w_TIPLIS3
    return

enddefine

* --- Define pages as container
define class tgsag_klcPag1 as StdContainer
  Width  = 665
  height = 96
  stdWidth  = 665
  stdheight = 96
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELTCOLIS_1_3 as StdField with uid="ICMRWOPFLQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ELTCOLIS", cQueryName = "ELTCOLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino utilizzato per la determinazione del prezzo",;
    HelpContextID = 196187751,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=141, Top=39, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ELTCOLIS"

  func oELTCOLIS_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSER <> 'D'  And Empty(.w_ELPROLIS+.w_ELPROSCO))
    endwith
   endif
  endfunc

  proc oELTCOLIS_1_3.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oELTCOLIS_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oELTCOLIS_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELTCOLIS_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oELTCOLIS_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oELSCOLIS_1_6 as StdField with uid="ZBIWFUOLGF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ELSCOLIS", cQueryName = "ELSCOLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino utilizzato per la determinazione degli sconti/maggiorazioni",;
    HelpContextID = 196191847,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=141, Top=64, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ELSCOLIS"

  func oELSCOLIS_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSER <> 'D'  And Empty(.w_ELPROLIS+.w_ELPROSCO))
    endwith
   endif
  endfunc

  proc oELSCOLIS_1_6.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oELSCOLIS_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oELSCOLIS_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELSCOLIS_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oELSCOLIS_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO LISTINI",'listdocu2.LISTINI_VZM',this.parent.oContained
  endproc

  add object oELPROLIS_1_7 as StdField with uid="VRIDBZETVO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ELPROLIS", cQueryName = "ELPROLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice promozione utilizzato per la determinazione del prezzo",;
    HelpContextID = 195221095,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=589, Top=39, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ELPROLIS"

  func oELPROLIS_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSER <> 'D' And !Empty(.w_ELTCOLIS+.w_ELSCOLIS))
    endwith
   endif
  endfunc

  proc oELPROLIS_1_7.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oELPROLIS_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oELPROLIS_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELPROLIS_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oELPROLIS_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO PROMOZIONI",'prodocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oELPROSCO_1_8 as StdField with uid="DXITMYNBNF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ELPROSCO", cQueryName = "ELPROSCO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice promozione utilizzato per la determinazione degli sconti/maggiorazioni",;
    HelpContextID = 190654869,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=589, Top=64, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ELPROSCO"

  func oELPROSCO_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSER <> 'D' And !Empty(.w_ELTCOLIS+.w_ELSCOLIS))
    endwith
   endif
  endfunc

  proc oELPROSCO_1_8.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oELPROSCO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oELPROSCO_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELPROSCO_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oELPROSCO_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO PROMOZIONI",'prodocu2.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDESLIS_1_9 as StdField with uid="JVXIADCBCJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 84454858,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=209, Top=39, InputMask=replicate('X',40)

  add object oDESLIS2_1_10 as StdField with uid="CFNLLJPOMY",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESLIS2", cQueryName = "DESLIS2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 183980598,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=209, Top=64, InputMask=replicate('X',40)

  add object oStr_1_12 as StdString with uid="FARPKBIGXI",Visible=.t., Left=17, Top=19,;
    Alignment=0, Width=180, Height=18,;
    Caption="Listini/promozioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="KOHASJEEDX",Visible=.t., Left=495, Top=39,;
    Alignment=1, Width=90, Height=18,;
    Caption="Promoz.prezzi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="MRGVFXRUTH",Visible=.t., Left=472, Top=64,;
    Alignment=1, Width=113, Height=18,;
    Caption="Promoz.sc/mag:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="UHOSUHUQRB",Visible=.t., Left=20, Top=64,;
    Alignment=1, Width=115, Height=18,;
    Caption="Listino sc/mag:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="OVCEGGNPWE",Visible=.t., Left=58, Top=39,;
    Alignment=1, Width=77, Height=18,;
    Caption="Listino prezzi:"  ;
  , bGlobalFont=.t.

  add object oBox_1_11 as StdBox with uid="YFOEMMRHOF",left=4, top=35, width=651,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_klc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
