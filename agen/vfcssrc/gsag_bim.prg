* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bim                                                        *
*              LANCIO ZOOM                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-03                                                      *
* Last revis.: 2008-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bim",oParentObject)
return(i_retval)

define class tgsag_bim as StdBatch
  * --- Local variables
  w_NUMRIG = 0
  w_ROWORD = 0
  w_DESCON = space(50)
  w_LIVELLO1 = 0
  w_MODATR = space(20)
  w_LEGAME = 0
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Popolo attraverso una Exec_select del transitorio uno zoom.
    *     Tale zoom verr� lanciato all'interno di una maschera 
    this.w_PADRE = this.OparentObject
    this.w_PADRE.MarkPos()     
    this.w_PADRE.Exec_Select("Livello","t_CPROWORD as ROWORD, t_IMDESCON AS DESCON,t_LIVELLO AS LIVELLO1, t_IMMODATR AS MODATR, t_IMLEGAME AS LEGAME, CPROWNUM AS NUMRIG","CPROWNUM <> "+ cp_ToStr(this.oParentObject.w_CPROWNUM),"","","")
    * --- Riverso il cursore temporaneo fox in una tabella temporanea lato server
    CURTOTAB("Livello", "TMP_PART")
    vx_exec("QUERY\GSAG_BIM.VZM",this)
    this.oParentObject.w_LIVELLO = this.w_ROWORD
    this.oParentObject.w_IMLEGAME = this.w_NUMRIG
    this.w_PADRE.SaveRow()     
    this.w_PADRE.RePos()     
    * --- Drop temporary table TMP_PART
    i_nIdx=cp_GetTableDefIdx('TMP_PART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
