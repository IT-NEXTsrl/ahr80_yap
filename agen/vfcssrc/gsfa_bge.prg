* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bge                                                        *
*              Creazione evento da documento                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-27                                                      *
* Last revis.: 2014-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSerDoc
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bge",oParentObject,m.pSerDoc)
return(i_retval)

define class tgsfa_bge as StdBatch
  * --- Local variables
  pSerDoc = space(10)
  w_CHKCREAEVENTO = space(1)
  w_TIPOEVENTO = space(10)
  w_ANNO_EV = space(4)
  w_MVTIPDOC = space(5)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVTIPCON = space(1)
  w_MVNUMEST = 0
  w_MVALFEST = space(10)
  w_MVDATEST = ctod("  /  /  ")
  w_MV__NOTE = space(0)
  w_ERROR = .f.
  w_EVCURNAM = space(10)
  w_MVCODCON = space(15)
  w_CODPERS = space(15)
  w_CODGRUP = space(5)
  w_DESCAU = space(35)
  w_RAGSOC = space(60)
  w_TETIPDIR = space(1)
  w_CODNOMIN = space(15)
  * --- WorkFile variables
  CAU_DOCU_idx=0
  ANEVENTI_idx=0
  DOC_MAST_idx=0
  DIPENDEN_idx=0
  TIP_DOCU_idx=0
  CONTI_idx=0
  TIPEVENT_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                                 Creazione evento da documento
    this.w_ERROR = .T.
    if ISAHE()
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODCON,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPCON,MVNUMEST,MVALFEST,MVDATEST,MVNOTEPN"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.pSerDoc);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODCON,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPCON,MVNUMEST,MVALFEST,MVDATEST,MVNOTEPN;
          from (i_cTable) where;
              MVSERIAL = this.pSerDoc;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        this.w_MVNUMEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
        this.w_MVALFEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
        this.w_MVDATEST = NVL(cp_ToDate(_read_.MVDATEST),cp_NullValue(_read_.MVDATEST))
        this.w_MV__NOTE = NVL(cp_ToDate(_read_.MVNOTEPN),cp_NullValue(_read_.MVNOTEPN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODCON,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPCON,MVNUMEST,MVALFEST,MVDATEST,MV__NOTE"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.pSerDoc);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODCON,MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPCON,MVNUMEST,MVALFEST,MVDATEST,MV__NOTE;
          from (i_cTable) where;
              MVSERIAL = this.pSerDoc;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        this.w_MVNUMEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
        this.w_MVALFEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
        this.w_MVDATEST = NVL(cp_ToDate(_read_.MVDATEST),cp_NullValue(_read_.MVDATEST))
        this.w_MV__NOTE = NVL(cp_ToDate(_read_.MV__NOTE),cp_NullValue(_read_.MV__NOTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Verifica se la causale del docum. � presente nelle Causali documenti (in Parametri agenda)
    * --- Read from CAU_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_DOCU_idx,2],.t.,this.CAU_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CDFLCREV,CDTIPEVE"+;
        " from "+i_cTable+" CAU_DOCU where ";
            +"CDCODAZI = "+cp_ToStrODBC(i_codazi);
            +" and CDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CDFLCREV,CDTIPEVE;
        from (i_cTable) where;
            CDCODAZI = i_codazi;
            and CDTIPDOC = this.w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CHKCREAEVENTO = NVL(cp_ToDate(_read_.CDFLCREV),cp_NullValue(_read_.CDFLCREV))
      this.w_TIPOEVENTO = NVL(cp_ToDate(_read_.CDTIPEVE),cp_NullValue(_read_.CDTIPEVE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se la causale del docum. viene trovata
    if NOT EMPTY(this.w_TIPOEVENTO)
      * --- Legge se esiste un evento associato al documento (se siamo in presenza di un nuovo documento sicuramente no)
      * --- Read from ANEVENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ANEVENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2],.t.,this.ANEVENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "EV__ANNO"+;
          " from "+i_cTable+" ANEVENTI where ";
              +"EVSERDOC = "+cp_ToStrODBC(this.pSerDoc);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          EV__ANNO;
          from (i_cTable) where;
              EVSERDOC = this.pSerDoc;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANNO_EV = NVL(cp_ToDate(_read_.EV__ANNO),cp_NullValue(_read_.EV__ANNO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se non esiste
      if EMPTY(this.w_ANNO_EV)
        if this.w_CHKCREAEVENTO="S" OR (this.w_CHKCREAEVENTO="C" AND AH_YESNO("Creare un nuovo evento per il documento appena salvato?"))
          this.w_EVCURNAM = SYS(2015)
          * --- Viene creata la struttura del cursore
          vq_exec("..\agen\exe\QUERY\GSFA_BIM.VQR",this,this.w_EVCURNAM)
          * --- Calcola il codice persona
          this.w_CODPERS = Readdipend(i_codute, "C")
          * --- Legge il gruppo associato alla persona
          * --- Read from DIPENDEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIPENDEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPGRUPRE"+;
              " from "+i_cTable+" DIPENDEN where ";
                  +"DPCODICE = "+cp_ToStrODBC(this.w_CODPERS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPGRUPRE;
              from (i_cTable) where;
                  DPCODICE = this.w_CODPERS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODGRUP = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Legge descrizione causale documento
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDDESDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDDESDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.w_MVTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCAU = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Legge ragione sociale cliente
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANDESCRI"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANDESCRI;
              from (i_cTable) where;
                  ANTIPCON = this.w_MVTIPCON;
                  and ANCODICE = this.w_MVCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RAGSOC = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Legge il codice nominativo associato al cliente
          * --- Read from OFF_NOMI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NOCODICE"+;
              " from "+i_cTable+" OFF_NOMI where ";
                  +"NOCODCLI = "+cp_ToStrODBC(this.w_MVCODCON);
                  +" and NOTIPCLI = "+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NOCODICE;
              from (i_cTable) where;
                  NOCODCLI = this.w_MVCODCON;
                  and NOTIPCLI = this.w_MVTIPCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODNOMIN = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from TIPEVENT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIPEVENT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIPEVENT_idx,2],.t.,this.TIPEVENT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TETIPDIR"+;
              " from "+i_cTable+" TIPEVENT where ";
                  +"TETIPEVE = "+cp_ToStrODBC(this.w_TIPOEVENTO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TETIPDIR;
              from (i_cTable) where;
                  TETIPEVE = this.w_TIPOEVENTO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TETIPDIR = NVL(cp_ToDate(_read_.TETIPDIR),cp_NullValue(_read_.TETIPDIR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Inserisce una riga nel cursore
          INSERT INTO ( this.w_EVCURNAM ) (EVDIREVE, EVTIPEVE, EVSERDOC, EVNOMINA, EVPERSON, EVGRUPPO, EVDATINI, EVDATFIN, EVOGGETT, EV__NOTE) ; 
 VALUES (this.w_TETIPDIR, this.w_TIPOEVENTO, this.pSerDoc, this.w_CODNOMIN, this.w_CODPERS, this.w_CODGRUP, datetime(), datetime(), ; 
 ah_msgformat("Doc. %1 num. %2 del %3",alltrim(this.w_MVTIPDOC),alltrim(str(this.w_MVNUMDOC))+iif(!empty(this.w_MVALFDOC), "/", "")+alltrim(this.w_MVALFDOC),dtoc(this.w_MVDATDOC) ), ; 
 ah_msgformat("Doc. %1 %0intestato a %2",alltrim(this.w_DESCAU),alltrim(this.w_MVCODCON)+" - "+alltrim(this.w_RAGSOC) )+chr(13)+chr(10)+; 
 iif(empty(this.w_MVNUMEST) or empty(this.w_MVDATEST),"", ah_msgformat("Riferimento esterno: %1 del %2",alltrim(str(this.w_MVNUMEST))+iif(!empty(this.w_MVALFEST),"/", "")+alltrim(this.w_MVALFEST),dtoc(this.w_MVDATEST)))+chr(13)+chr(10)+; 
 iif(empty(this.w_MV__NOTE), "", ah_msgformat("Note documento: %1",alltrim(this.w_MV__NOTE))) )
          * --- Creazione evento
          this.w_ERROR = GSFA_BEV(this, this.w_EVCURNAM , "", "", "A")
          if NOT this.w_ERROR
            AH_ErrorMsg("Attenzione: creazione evento non avvenuta")
          endif
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pSerDoc)
    this.pSerDoc=pSerDoc
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='CAU_DOCU'
    this.cWorkTables[2]='ANEVENTI'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='DIPENDEN'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='TIPEVENT'
    this.cWorkTables[8]='OFF_NOMI'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSerDoc"
endproc
