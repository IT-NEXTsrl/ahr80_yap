* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_spp                                                        *
*              Stampa prestazioni provvisorie                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-18                                                      *
* Last revis.: 2012-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_spp",oParentObject))

* --- Class definition
define class tgsag_spp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 559
  Height = 267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-14"
  HelpContextID=127260823
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=33

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  KEY_ARTI_IDX = 0
  CAN_TIER_IDX = 0
  OFF_NOMI_IDX = 0
  CENCOST_IDX = 0
  cPrg = "gsag_spp"
  cComment = "Stampa prestazioni provvisorie"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TipoRisorsa = space(1)
  w_CODRESP = space(5)
  o_CODRESP = space(5)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODSER2 = space(41)
  w_CODSER = space(20)
  w_DesSer = space(40)
  w_CODPRA = space(15)
  w_OnoCiv = space(1)
  w_OnoPen = space(1)
  w_OnoStrag = space(1)
  w_DirCiv = space(1)
  w_DirStrag = space(1)
  w_PreGen = space(1)
  w_PreTempo = space(1)
  w_Spese = space(1)
  w_Anticip = space(1)
  w_COGNOME = space(40)
  o_COGNOME = space(40)
  w_NOME = space(40)
  o_NOME = space(40)
  w_DESCRIZ = space(80)
  w_OB_TEST = ctod('  /  /  ')
  w_COMODO = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_DESPRA = space(100)
  w_ISALT = space(1)
  w_CODNOM = space(20)
  w_DESNOM = space(40)
  w_OFDATDOC = ctod('  /  /  ')
  w_TIPANA = space(1)
  w_CODCOMM = space(15)
  w_CC_CONTO = space(15)
  w_DESCOMM = space(100)
  w_CCDESPIA = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_sppPag1","gsag_spp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODRESP_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_spp
    Local nCtrl
    For nCtrl=1 to this.page1.Controls(1).ControlCount
       If UPPER(this.page1.Controls(1).Controls(nCtrl).Class)=='STDBOX'
        this.page1.Controls(1).Controls(nCtrl).Visible=IsAlt()
       EndIf
    Next
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='OFF_NOMI'
    this.cWorkTables[5]='CENCOST'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TipoRisorsa=space(1)
      .w_CODRESP=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CODSER2=space(41)
      .w_CODSER=space(20)
      .w_DesSer=space(40)
      .w_CODPRA=space(15)
      .w_OnoCiv=space(1)
      .w_OnoPen=space(1)
      .w_OnoStrag=space(1)
      .w_DirCiv=space(1)
      .w_DirStrag=space(1)
      .w_PreGen=space(1)
      .w_PreTempo=space(1)
      .w_Spese=space(1)
      .w_Anticip=space(1)
      .w_COGNOME=space(40)
      .w_NOME=space(40)
      .w_DESCRIZ=space(80)
      .w_OB_TEST=ctod("  /  /  ")
      .w_COMODO=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESPRA=space(100)
      .w_ISALT=space(1)
      .w_CODNOM=space(20)
      .w_DESNOM=space(40)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_TIPANA=space(1)
      .w_CODCOMM=space(15)
      .w_CC_CONTO=space(15)
      .w_DESCOMM=space(100)
      .w_CCDESPIA=space(40)
        .w_TipoRisorsa = 'P'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODRESP))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_DATFIN = i_datSys
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODSER2))
          .link_1_6('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODSER))
          .link_1_7('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_CODPRA))
          .link_1_9('Full')
        endif
        .w_OnoCiv = 'I'
        .w_OnoPen = 'E'
        .w_OnoStrag = 'T'
        .w_DirCiv = 'C'
        .w_DirStrag = 'R'
        .w_PreGen = 'G'
        .w_PreTempo = 'P'
        .w_Spese = 'S'
        .w_Anticip = 'A'
          .DoRTCalc(18,19,.f.)
        .w_DESCRIZ = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate(IIF(IsAlt(), "Codice responsabile", "Codice partecipante"))
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(22,22,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(24,24,.f.)
        .w_ISALT = IIF(IsAlt(), 'S', 'N')
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CODNOM))
          .link_1_38('Full')
        endif
          .DoRTCalc(27,27,.f.)
        .w_OFDATDOC = i_datsys
        .w_TIPANA = 'C'
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_CODCOMM))
          .link_1_43('Full')
        endif
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_CC_CONTO))
          .link_1_44('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
    endwith
    this.DoRTCalc(32,33,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,19,.t.)
        if .o_CODRESP<>.w_CODRESP.or. .o_COGNOME<>.w_COGNOME.or. .o_NOME<>.w_NOME
            .w_DESCRIZ = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(IIF(IsAlt(), "Codice responsabile", "Codice partecipante"))
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,33,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(IIF(IsAlt(), "Codice responsabile", "Codice partecipante"))
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCODSER2_1_6.visible=!this.oPgFrm.Page1.oPag.oCODSER2_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCODSER_1_7.visible=!this.oPgFrm.Page1.oPag.oCODSER_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCODPRA_1_9.visible=!this.oPgFrm.Page1.oPag.oCODPRA_1_9.mHide()
    this.oPgFrm.Page1.oPag.oOnoCiv_1_10.visible=!this.oPgFrm.Page1.oPag.oOnoCiv_1_10.mHide()
    this.oPgFrm.Page1.oPag.oOnoPen_1_11.visible=!this.oPgFrm.Page1.oPag.oOnoPen_1_11.mHide()
    this.oPgFrm.Page1.oPag.oOnoStrag_1_12.visible=!this.oPgFrm.Page1.oPag.oOnoStrag_1_12.mHide()
    this.oPgFrm.Page1.oPag.oDirCiv_1_13.visible=!this.oPgFrm.Page1.oPag.oDirCiv_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDirStrag_1_14.visible=!this.oPgFrm.Page1.oPag.oDirStrag_1_14.mHide()
    this.oPgFrm.Page1.oPag.oPreGen_1_15.visible=!this.oPgFrm.Page1.oPag.oPreGen_1_15.mHide()
    this.oPgFrm.Page1.oPag.oPreTempo_1_16.visible=!this.oPgFrm.Page1.oPag.oPreTempo_1_16.mHide()
    this.oPgFrm.Page1.oPag.oSpese_1_17.visible=!this.oPgFrm.Page1.oPag.oSpese_1_17.mHide()
    this.oPgFrm.Page1.oPag.oAnticip_1_18.visible=!this.oPgFrm.Page1.oPag.oAnticip_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDESPRA_1_34.visible=!this.oPgFrm.Page1.oPag.oDESPRA_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oCODNOM_1_38.visible=!this.oPgFrm.Page1.oPag.oCODNOM_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDESNOM_1_39.visible=!this.oPgFrm.Page1.oPag.oDESNOM_1_39.mHide()
    this.oPgFrm.Page1.oPag.oTIPANA_1_41.visible=!this.oPgFrm.Page1.oPag.oTIPANA_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oCODCOMM_1_43.visible=!this.oPgFrm.Page1.oPag.oCODCOMM_1_43.mHide()
    this.oPgFrm.Page1.oPag.oCC_CONTO_1_44.visible=!this.oPgFrm.Page1.oPag.oCC_CONTO_1_44.mHide()
    this.oPgFrm.Page1.oPag.oDESCOMM_1_45.visible=!this.oPgFrm.Page1.oPag.oDESCOMM_1_45.mHide()
    this.oPgFrm.Page1.oPag.oCCDESPIA_1_46.visible=!this.oPgFrm.Page1.oPag.oCCDESPIA_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODRESP
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRESP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_CODRESP))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODRESP)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODRESP) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCODRESP_1_3'),i_cWhere,'GSAR_BDZ',""+iif(isalt (), "Responsabili", "Partecipanti") +"",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRESP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODRESP);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CODRESP)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRESP = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODRESP = space(5)
      endif
      this.w_COGNOME = space(40)
      this.w_NOME = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRESP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSER2
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSER2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODSER2)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODSER2))
          select CACODICE,CADESART,CADESSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSER2)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODSER2)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODSER2)+"%");

            select CACODICE,CADESART,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CODSER2)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CODSER2)+"%");

            select CACODICE,CADESART,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODSER2) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODSER2_1_6'),i_cWhere,'',"Prestazioni",'GSAGZKPM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSER2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODSER2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODSER2)
            select CACODICE,CADESART,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSER2 = NVL(_Link_.CACODICE,space(41))
      this.w_comodo = NVL(_Link_.CADESART,space(30))
      this.w_comodo = NVL(_Link_.CADESSUP,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODSER2 = space(41)
      endif
      this.w_comodo = space(30)
      this.w_comodo = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSER2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSER
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODSER))
          select CACODICE,CADESART,CADESSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSER)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODSER) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODSER_1_7'),i_cWhere,'',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Default")+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODSER)
            select CACODICE,CADESART,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSER = NVL(_Link_.CACODICE,space(20))
      this.w_comodo = NVL(_Link_.CADESART,space(30))
      this.w_comodo = NVL(_Link_.CADESSUP,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODSER = space(20)
      endif
      this.w_comodo = space(30)
      this.w_comodo = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_1_9'),i_cWhere,'',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(100)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_38'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(20)
      endif
      this.w_DESNOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOMM
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOMM))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOMM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOMM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOMM)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODCOMM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODCOMM)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODCOMM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODCOMM)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOMM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOMM_1_43'),i_cWhere,'',"Commesse",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOMM)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOMM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMM = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOMM = space(15)
      endif
      this.w_DESCOMM = space(100)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CC_CONTO
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CC_CONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CC_CONTO)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CC_CONTO))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CC_CONTO)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CC_CONTO) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCC_CONTO_1_44'),i_cWhere,'',"Centri di costo/Ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CC_CONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CC_CONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CC_CONTO)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CC_CONTO = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CC_CONTO = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CC_CONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODRESP_1_3.value==this.w_CODRESP)
      this.oPgFrm.Page1.oPag.oCODRESP_1_3.value=this.w_CODRESP
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_4.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_4.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_5.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_5.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSER2_1_6.value==this.w_CODSER2)
      this.oPgFrm.Page1.oPag.oCODSER2_1_6.value=this.w_CODSER2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSER_1_7.value==this.w_CODSER)
      this.oPgFrm.Page1.oPag.oCODSER_1_7.value=this.w_CODSER
    endif
    if not(this.oPgFrm.Page1.oPag.oDesSer_1_8.value==this.w_DesSer)
      this.oPgFrm.Page1.oPag.oDesSer_1_8.value=this.w_DesSer
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRA_1_9.value==this.w_CODPRA)
      this.oPgFrm.Page1.oPag.oCODPRA_1_9.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oOnoCiv_1_10.RadioValue()==this.w_OnoCiv)
      this.oPgFrm.Page1.oPag.oOnoCiv_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOnoPen_1_11.RadioValue()==this.w_OnoPen)
      this.oPgFrm.Page1.oPag.oOnoPen_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOnoStrag_1_12.RadioValue()==this.w_OnoStrag)
      this.oPgFrm.Page1.oPag.oOnoStrag_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDirCiv_1_13.RadioValue()==this.w_DirCiv)
      this.oPgFrm.Page1.oPag.oDirCiv_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDirStrag_1_14.RadioValue()==this.w_DirStrag)
      this.oPgFrm.Page1.oPag.oDirStrag_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPreGen_1_15.RadioValue()==this.w_PreGen)
      this.oPgFrm.Page1.oPag.oPreGen_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPreTempo_1_16.RadioValue()==this.w_PreTempo)
      this.oPgFrm.Page1.oPag.oPreTempo_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSpese_1_17.RadioValue()==this.w_Spese)
      this.oPgFrm.Page1.oPag.oSpese_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAnticip_1_18.RadioValue()==this.w_Anticip)
      this.oPgFrm.Page1.oPag.oAnticip_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIZ_1_21.value==this.w_DESCRIZ)
      this.oPgFrm.Page1.oPag.oDESCRIZ_1_21.value=this.w_DESCRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRA_1_34.value==this.w_DESPRA)
      this.oPgFrm.Page1.oPag.oDESPRA_1_34.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_38.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_38.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_39.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_39.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPANA_1_41.RadioValue()==this.w_TIPANA)
      this.oPgFrm.Page1.oPag.oTIPANA_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOMM_1_43.value==this.w_CODCOMM)
      this.oPgFrm.Page1.oPag.oCODCOMM_1_43.value=this.w_CODCOMM
    endif
    if not(this.oPgFrm.Page1.oPag.oCC_CONTO_1_44.value==this.w_CC_CONTO)
      this.oPgFrm.Page1.oPag.oCC_CONTO_1_44.value=this.w_CC_CONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMM_1_45.value==this.w_DESCOMM)
      this.oPgFrm.Page1.oPag.oDESCOMM_1_45.value=this.w_DESCOMM
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_46.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_46.value=this.w_CCDESPIA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore a quella finale")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore a quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODRESP = this.w_CODRESP
    this.o_COGNOME = this.w_COGNOME
    this.o_NOME = this.w_NOME
    return

enddefine

* --- Define pages as container
define class tgsag_sppPag1 as StdContainer
  Width  = 555
  height = 267
  stdWidth  = 555
  stdheight = 267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODRESP_1_3 as StdField with uid="HIIAOTSMWB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODRESP", cQueryName = "CODRESP",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 12817370,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=100, Top=10, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_CODRESP"

  func oCODRESP_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODRESP_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODRESP_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCODRESP_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',""+iif(isalt (), "Responsabili", "Partecipanti") +"",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oCODRESP_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TipoRisorsa
     i_obj.w_DPCODICE=this.parent.oContained.w_CODRESP
     i_obj.ecpSave()
  endproc

  add object oDATINI_1_4 as StdField with uid="LOBOVQPRWD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore a quella finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 171680202,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=100, Top=35

  func oDATINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_5 as StdField with uid="OWDJHUXKLB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore a quella finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 93233610,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=217, Top=35

  func oDATFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oCODSER2_1_6 as StdField with uid="SPIZUMVYUU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODSER2", cQueryName = "CODSER2",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 29529050,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=100, Top=60, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODSER2"

  func oCODSER2_1_6.mHide()
    with this.Parent.oContained
      return (NOT ISAHE())
    endwith
  endfunc

  func oCODSER2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSER2_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSER2_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODSER2_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Prestazioni",'GSAGZKPM.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oCODSER_1_7 as StdField with uid="XLNRNSVNEW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODSER", cQueryName = "CODSER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 29529050,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=100, Top=60, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODSER"

  func oCODSER_1_7.mHide()
    with this.Parent.oContained
      return (ISAHE())
    endwith
  endfunc

  func oCODSER_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSER_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSER_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODSER_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Default")+'.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDesSer_1_8 as AH_SEARCHFLD with uid="HAEQFJXMXN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DesSer", cQueryName = "DesSer",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prestazione o parte di essa",;
    HelpContextID = 264211914,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=346, Top=60, InputMask=replicate('X',40)

  add object oCODPRA_1_9 as StdField with uid="HTRKXPXTPK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice pratica",;
    HelpContextID = 32871386,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=100, Top=85, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_1_9.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oCODPRA_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oOnoCiv_1_10 as StdCheck with uid="ELVFIVSHDY",rtseq=9,rtrep=.f.,left=116, top=120, caption="Onorario civile",;
    ToolTipText = "Se attivo, stampa gli onorari civili",;
    HelpContextID = 193971226,;
    cFormVar="w_OnoCiv", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOnoCiv_1_10.RadioValue()
    return(iif(this.value =1,'I',;
    space(1)))
  endfunc
  func oOnoCiv_1_10.GetRadio()
    this.Parent.oContained.w_OnoCiv = this.RadioValue()
    return .t.
  endfunc

  func oOnoCiv_1_10.SetRadio()
    this.Parent.oContained.w_OnoCiv=trim(this.Parent.oContained.w_OnoCiv)
    this.value = ;
      iif(this.Parent.oContained.w_OnoCiv=='I',1,;
      0)
  endfunc

  func oOnoCiv_1_10.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oOnoPen_1_11 as StdCheck with uid="FATEDFEVGH",rtseq=10,rtrep=.f.,left=258, top=120, caption="Onorario penale",;
    ToolTipText = "Se attivo, stampa gli onorari penali",;
    HelpContextID = 63095834,;
    cFormVar="w_OnoPen", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOnoPen_1_11.RadioValue()
    return(iif(this.value =1,'E',;
    space(1)))
  endfunc
  func oOnoPen_1_11.GetRadio()
    this.Parent.oContained.w_OnoPen = this.RadioValue()
    return .t.
  endfunc

  func oOnoPen_1_11.SetRadio()
    this.Parent.oContained.w_OnoPen=trim(this.Parent.oContained.w_OnoPen)
    this.value = ;
      iif(this.Parent.oContained.w_OnoPen=='E',1,;
      0)
  endfunc

  func oOnoPen_1_11.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oOnoStrag_1_12 as StdCheck with uid="JNXMKTIAUU",rtseq=11,rtrep=.f.,left=394, top=120, caption="Onorario stragiudiziale",;
    ToolTipText = "Se attivo, stampa gli onorari stragiudiziali",;
    HelpContextID = 19938381,;
    cFormVar="w_OnoStrag", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOnoStrag_1_12.RadioValue()
    return(iif(this.value =1,'T',;
    space(1)))
  endfunc
  func oOnoStrag_1_12.GetRadio()
    this.Parent.oContained.w_OnoStrag = this.RadioValue()
    return .t.
  endfunc

  func oOnoStrag_1_12.SetRadio()
    this.Parent.oContained.w_OnoStrag=trim(this.Parent.oContained.w_OnoStrag)
    this.value = ;
      iif(this.Parent.oContained.w_OnoStrag=='T',1,;
      0)
  endfunc

  func oOnoStrag_1_12.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDirCiv_1_13 as StdCheck with uid="KWCAIAQPMY",rtseq=12,rtrep=.f.,left=116, top=139, caption="Diritto civile",;
    ToolTipText = "Se attivo, stampa i diritti civili",;
    HelpContextID = 193960394,;
    cFormVar="w_DirCiv", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDirCiv_1_13.RadioValue()
    return(iif(this.value =1,'C',;
    space(1)))
  endfunc
  func oDirCiv_1_13.GetRadio()
    this.Parent.oContained.w_DirCiv = this.RadioValue()
    return .t.
  endfunc

  func oDirCiv_1_13.SetRadio()
    this.Parent.oContained.w_DirCiv=trim(this.Parent.oContained.w_DirCiv)
    this.value = ;
      iif(this.Parent.oContained.w_DirCiv=='C',1,;
      0)
  endfunc

  func oDirCiv_1_13.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDirStrag_1_14 as StdCheck with uid="YPYEWIJVVO",rtseq=13,rtrep=.f.,left=258, top=139, caption="Diritto stragiudiziale",;
    ToolTipText = "Se attivo, stampa i diritti stragiudiziali",;
    HelpContextID = 19949213,;
    cFormVar="w_DirStrag", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDirStrag_1_14.RadioValue()
    return(iif(this.value =1,'R',;
    space(1)))
  endfunc
  func oDirStrag_1_14.GetRadio()
    this.Parent.oContained.w_DirStrag = this.RadioValue()
    return .t.
  endfunc

  func oDirStrag_1_14.SetRadio()
    this.Parent.oContained.w_DirStrag=trim(this.Parent.oContained.w_DirStrag)
    this.value = ;
      iif(this.Parent.oContained.w_DirStrag=='R',1,;
      0)
  endfunc

  func oDirStrag_1_14.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oPreGen_1_15 as StdCheck with uid="TCWVESISVS",rtseq=14,rtrep=.f.,left=394, top=139, caption="Prestazione generica",;
    ToolTipText = "Se attivo, stampa le tariffe di prestazione generica",;
    HelpContextID = 63725578,;
    cFormVar="w_PreGen", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPreGen_1_15.RadioValue()
    return(iif(this.value =1,'G',;
    space(1)))
  endfunc
  func oPreGen_1_15.GetRadio()
    this.Parent.oContained.w_PreGen = this.RadioValue()
    return .t.
  endfunc

  func oPreGen_1_15.SetRadio()
    this.Parent.oContained.w_PreGen=trim(this.Parent.oContained.w_PreGen)
    this.value = ;
      iif(this.Parent.oContained.w_PreGen=='G',1,;
      0)
  endfunc

  func oPreGen_1_15.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oPreTempo_1_16 as StdCheck with uid="ZQYMXNWOEB",rtseq=15,rtrep=.f.,left=116, top=158, caption="Prestazione a tempo",;
    ToolTipText = "Se attivo, stampa le tariffe di prestazione a tempo",;
    HelpContextID = 79650715,;
    cFormVar="w_PreTempo", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPreTempo_1_16.RadioValue()
    return(iif(this.value =1,'P',;
    space(1)))
  endfunc
  func oPreTempo_1_16.GetRadio()
    this.Parent.oContained.w_PreTempo = this.RadioValue()
    return .t.
  endfunc

  func oPreTempo_1_16.SetRadio()
    this.Parent.oContained.w_PreTempo=trim(this.Parent.oContained.w_PreTempo)
    this.value = ;
      iif(this.Parent.oContained.w_PreTempo=='P',1,;
      0)
  endfunc

  func oPreTempo_1_16.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oSpese_1_17 as StdCheck with uid="GJLHVDCIUE",rtseq=16,rtrep=.f.,left=258, top=158, caption="Spesa",;
    ToolTipText = "Se attivo, stampa le spese",;
    HelpContextID = 241147430,;
    cFormVar="w_Spese", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSpese_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSpese_1_17.GetRadio()
    this.Parent.oContained.w_Spese = this.RadioValue()
    return .t.
  endfunc

  func oSpese_1_17.SetRadio()
    this.Parent.oContained.w_Spese=trim(this.Parent.oContained.w_Spese)
    this.value = ;
      iif(this.Parent.oContained.w_Spese=='S',1,;
      0)
  endfunc

  func oSpese_1_17.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oAnticip_1_18 as StdCheck with uid="IBXMGDLBBY",rtseq=17,rtrep=.f.,left=394, top=158, caption="Anticipazione",;
    ToolTipText = "Se attivo, stampa le anticipazioni",;
    HelpContextID = 147420410,;
    cFormVar="w_Anticip", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAnticip_1_18.RadioValue()
    return(iif(this.value =1,'A',;
    space(1)))
  endfunc
  func oAnticip_1_18.GetRadio()
    this.Parent.oContained.w_Anticip = this.RadioValue()
    return .t.
  endfunc

  func oAnticip_1_18.SetRadio()
    this.Parent.oContained.w_Anticip=trim(this.Parent.oContained.w_Anticip)
    this.value = ;
      iif(this.Parent.oContained.w_Anticip=='A',1,;
      0)
  endfunc

  func oAnticip_1_18.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDESCRIZ_1_21 as StdField with uid="KMZNHYKWWA",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCRIZ", cQueryName = "DESCRIZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 100553270,;
   bGlobalFont=.t.,;
    Height=21, Width=381, Left=167, Top=10, InputMask=replicate('X',80)


  add object oObj_1_24 as cp_setobjprop with uid="VBHIYEPEDD",left=316, top=358, width=191,height=22,;
    caption='ToolTip di w_CODRESP',;
   bGlobalFont=.t.,;
    cObj="w_CODRESP",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 534266

  add object oDESPRA_1_34 as StdField with uid="OZIZCGPPPT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 32812490,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=238, Top=85, InputMask=replicate('X',100)

  func oDESPRA_1_34.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oCODNOM_1_38 as StdField with uid="XRMWQEMWPW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 103257050,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=100, Top=85, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_38.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oCODNOM_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc

  add object oDESNOM_1_39 as StdField with uid="QOGFAOHDZJ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 103198154,;
   bGlobalFont=.t.,;
    Height=21, Width=311, Left=237, Top=85, InputMask=replicate('X',40)

  func oDESNOM_1_39.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc


  add object oTIPANA_1_41 as StdCombo with uid="OOYOOVQSKI",rtseq=29,rtrep=.f.,left=100,top=112,width=132,height=21;
    , HelpContextID = 38000842;
    , cFormVar="w_TIPANA",RowSource=""+"Costo,"+"Ricavo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPANA_1_41.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPANA_1_41.GetRadio()
    this.Parent.oContained.w_TIPANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPANA_1_41.SetRadio()
    this.Parent.oContained.w_TIPANA=trim(this.Parent.oContained.w_TIPANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPANA=='C',1,;
      iif(this.Parent.oContained.w_TIPANA=='R',2,;
      0))
  endfunc

  func oTIPANA_1_41.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCODCOMM_1_43 as StdField with uid="INOKKLNQEA",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODCOMM", cQueryName = "CODCOMM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 103977946,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=100, Top=137, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOMM"

  func oCODCOMM_1_43.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oCODCOMM_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOMM_1_43.ecpDrop(oSource)
    this.Parent.oContained.link_1_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOMM_1_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOMM_1_43'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oCC_CONTO_1_44 as StdField with uid="PALBMHFMEH",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CC_CONTO", cQueryName = "CC_CONTO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 181342325,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=100, Top=163, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CC_CONTO"

  func oCC_CONTO_1_44.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR ISALT())
    endwith
  endfunc

  func oCC_CONTO_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oCC_CONTO_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCC_CONTO_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCC_CONTO_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Centri di costo/Ricavo",'',this.parent.oContained
  endproc

  add object oDESCOMM_1_45 as StdField with uid="LJOPFBHRPX",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCOMM", cQueryName = "DESCOMM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 103919050,;
   bGlobalFont=.t.,;
    Height=21, Width=311, Left=237, Top=138, InputMask=replicate('X',100)

  func oDESCOMM_1_45.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oCCDESPIA_1_46 as StdField with uid="KDRZJDGARK",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219111527,;
   bGlobalFont=.t.,;
    Height=21, Width=311, Left=237, Top=162, InputMask=replicate('X',40)

  func oCCDESPIA_1_46.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR ISALT())
    endwith
  endfunc


  add object oObj_1_48 as cp_outputCombo with uid="ZLERXIVPWT",left=100, top=193, width=304,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 231612442


  add object oBtn_1_50 as StdButton with uid="FFZUPFQMOE",left=446, top=216, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 267820506;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_50.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_50.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_51 as StdButton with uid="DIOMPKAHZX",left=500, top=216, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 134578246;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="GLFTJMROPX",Visible=.t., Left=11, Top=11,;
    Alignment=1, Width=81, Height=18,;
    Caption="Responsabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="HVQDALZAWB",Visible=.t., Left=7, Top=193,;
    Alignment=1, Width=85, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="XFIFQSEACQ",Visible=.t., Left=20, Top=11,;
    Alignment=1, Width=72, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="JQRNPBGBON",Visible=.t., Left=19, Top=37,;
    Alignment=1, Width=73, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="IZQIMNOCEG",Visible=.t., Left=189, Top=37,;
    Alignment=1, Width=20, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="HEERJRJVVJ",Visible=.t., Left=41, Top=62,;
    Alignment=1, Width=51, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="EXFPHNQNMI",Visible=.t., Left=247, Top=63,;
    Alignment=1, Width=96, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="AUEJGKPLZP",Visible=.t., Left=19, Top=62,;
    Alignment=1, Width=73, Height=18,;
    Caption="Prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="AUMKWLZTYQ",Visible=.t., Left=51, Top=85,;
    Alignment=1, Width=41, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="KSBGPQZUEI",Visible=.t., Left=11, Top=85,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="FJAAOETXLD",Visible=.t., Left=11, Top=112,;
    Alignment=1, Width=81, Height=18,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="EBUADEBZKR",Visible=.t., Left=19, Top=166,;
    Alignment=1, Width=73, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR ISALT())
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="VGACWRSQQW",Visible=.t., Left=19, Top=139,;
    Alignment=1, Width=73, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oBox_1_35 as StdBox with uid="GUAITALLMO",left=101, top=115, width=447,height=66
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_spp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
