* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mec                                                        *
*              Elementi contratto impianto                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-08-06                                                      *
* Last revis.: 2015-07-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsag_mec")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsag_mec")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsag_mec")
  return

* --- Class definition
define class tgsag_mec as StdPCForm
  Width  = 711
  Height = 607
  Top    = 1
  Left   = 10
  cComment = "Elementi contratto impianto"
  cPrg = "gsag_mec"
  HelpContextID=63580009
  add object cnt as tcgsag_mec
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsag_mec as PCContext
  w_ELCODMOD = space(10)
  w_CODART = space(20)
  w_CODART = space(20)
  w_ELCODCLI = space(15)
  w_CODICE = space(15)
  w_ELCONTRA = space(10)
  w_CODCLI = space(15)
  w_ELCODART = space(20)
  w_ELUNIMIS = space(3)
  w_ELQTAMOV = 0
  w_TIPART = space(2)
  w_TIPO = space(1)
  w_ELCODVAL = space(3)
  w_ELCODIMP = space(10)
  w_TIPCON = space(1)
  w_ELCODCOM = 0
  w_PERFAT = space(3)
  w_CATCON = space(3)
  w_GESRAT = space(1)
  w_UNIMIS = space(5)
  w_TIPMOD = space(1)
  w_PERFAT = space(3)
  w_PERATT = space(3)
  w_PRIDAT = space(1)
  w_PRIDOC = space(1)
  w_COPRIDAT = space(1)
  w_COPRIDOC = space(1)
  w_PRIMADATA = space(1)
  w_PRIMADATADOC = space(1)
  w_DATINI = space(8)
  w_DATFIN = space(8)
  w_PATIPRIS = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_QTAMOV = 0
  w_CODVAL = space(3)
  w_FLSERG = space(1)
  w_CARAGGST = space(10)
  w_ELGESRAT = space(1)
  w_FLATT = space(1)
  w_FLATTI = space(1)
  w_CODPAG = space(5)
  w_DESPER = space(50)
  w_DESCAT = space(60)
  w_ELPREZZO = 0
  w_ELSCONT1 = 0
  w_ELCONCOD = space(15)
  w_ELSCONT2 = 0
  w_ELSCONT3 = 0
  w_ELSCONT4 = 0
  w_DESCRIAT = space(50)
  w_ELDATINI = space(8)
  w_ELDATFIN = space(8)
  w_ELCATCON = space(5)
  w_FLNSAP = space(1)
  w_ELCODLIS = space(5)
  w_ELTIPATT = space(20)
  w_ELGRUPAR = space(5)
  w_ELDATDOC = space(8)
  w_ELPERATT = space(3)
  w_ELDATATT = space(8)
  w_ELCAUDOC = space(5)
  w_ELGIOATT = 0
  w_ELDATGAT = space(8)
  w_ELCODPAG = space(5)
  w_DESLIS = space(40)
  w_ELPERFAT = space(3)
  w_PREZUM = space(1)
  w_ELDATFAT = space(8)
  w_OPERAT = space(1)
  w_ELGIODOC = 0
  w_MOLTIP = 0
  w_CATCLI = space(5)
  w_CATART = space(5)
  w_SCOLIS = space(1)
  w_DECUNI = 0
  w_CACODART = space(20)
  w_MOLTI3 = 0
  w_OPERA3 = space(1)
  w_FLUSEP = space(1)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(2)
  w_TIPATT = space(20)
  w_TIPDOC = space(5)
  w_GRUPAR = space(5)
  w_MCAUDOC = space(5)
  w_MTIPATT = space(20)
  w_MGRUPAR = space(5)
  w_VALLIS = space(3)
  w_CATDOC = space(2)
  w_TIPRIS = space(1)
  w_CICLO = space(1)
  w_LORNET = space(1)
  w_FLSCOR = space(1)
  w_DESPAG = space(30)
  w_INILIS = space(8)
  w_FINLIS = space(8)
  w_QUALIS = space(1)
  w_NUMLIS = space(5)
  w_IVACLI = space(1)
  w_OB_TEST = space(8)
  w_DATOBSO = space(8)
  w_LIPREZZO = 0
  w_TIPSER = space(1)
  w_ELPROLIS = space(5)
  w_ELPROSCO = space(5)
  w_FLGLIS = space(1)
  w_FLINTE = space(1)
  w_ARCODART = space(20)
  w_NULLA = space(1)
  w_DATALIST = space(8)
  w_ELSCOLIS = space(5)
  w_ELTCOLIS = space(5)
  w_KEYLISTB = space(10)
  w_MVCODVAL = space(5)
  w_FLVEAC = space(1)
  w_FLRIPS = space(10)
  w_GESTGUID = space(10)
  w_ARUNMIS1 = space(3)
  w_ARUNMIS2 = space(3)
  w_MVFLVEAC = space(1)
  w_CCODLIS = space(5)
  w_MCODLIS = space(5)
  w_ELRINNOV = 0
  w_ELESCRIN = space(1)
  w_ELPERCON = space(3)
  w_ELRINCON = space(1)
  w_ELNUMGIO = 0
  w_ELDATDIS = space(8)
  w_DESATT = space(254)
  w_DESDOC = space(35)
  w_DESCTR = space(35)
  w_ELVOCCEN = space(15)
  w_ELCODCEN = space(15)
  w_ELCOCOMM = space(15)
  w_ELCODATT = space(15)
  w_MCODCEN = space(15)
  w_MCOCOMM = space(15)
  w_CCOCOMM = space(15)
  w_CCODCEN = space(15)
  w_MOTIPCON = space(1)
  w_MD__FREQ = space(1)
  w_DPDESCRI = space(60)
  w_ATTEXIST = space(1)
  w_DOCEXIST = space(1)
  w_FLSCOR = space(1)
  w_CATCOM = space(3)
  w_CT = space(1)
  w_CC = space(15)
  w_CM = space(3)
  w_CI = space(8)
  w_CF = space(8)
  w_CV = space(3)
  w_QUACON = space(1)
  w_IVACON = space(1)
  w_MDDESCRI = space(50)
  w_ELINICOA = space(8)
  w_ELFINCOA = space(8)
  w_ELINICOD = space(8)
  w_ELFINCOD = space(8)
  w_DTBSO_CAUMATTI = space(8)
  w_ELQTACON = 0
  w_QTA__RES = 0
  w_PREZZTOT = 0
  w_OBTEST = space(8)
  w_RESCHK = 0
  proc Save(i_oFrom)
    this.w_ELCODMOD = i_oFrom.w_ELCODMOD
    this.w_CODART = i_oFrom.w_CODART
    this.w_CODART = i_oFrom.w_CODART
    this.w_ELCODCLI = i_oFrom.w_ELCODCLI
    this.w_CODICE = i_oFrom.w_CODICE
    this.w_ELCONTRA = i_oFrom.w_ELCONTRA
    this.w_CODCLI = i_oFrom.w_CODCLI
    this.w_ELCODART = i_oFrom.w_ELCODART
    this.w_ELUNIMIS = i_oFrom.w_ELUNIMIS
    this.w_ELQTAMOV = i_oFrom.w_ELQTAMOV
    this.w_TIPART = i_oFrom.w_TIPART
    this.w_TIPO = i_oFrom.w_TIPO
    this.w_ELCODVAL = i_oFrom.w_ELCODVAL
    this.w_ELCODIMP = i_oFrom.w_ELCODIMP
    this.w_TIPCON = i_oFrom.w_TIPCON
    this.w_ELCODCOM = i_oFrom.w_ELCODCOM
    this.w_PERFAT = i_oFrom.w_PERFAT
    this.w_CATCON = i_oFrom.w_CATCON
    this.w_GESRAT = i_oFrom.w_GESRAT
    this.w_UNIMIS = i_oFrom.w_UNIMIS
    this.w_TIPMOD = i_oFrom.w_TIPMOD
    this.w_PERFAT = i_oFrom.w_PERFAT
    this.w_PERATT = i_oFrom.w_PERATT
    this.w_PRIDAT = i_oFrom.w_PRIDAT
    this.w_PRIDOC = i_oFrom.w_PRIDOC
    this.w_COPRIDAT = i_oFrom.w_COPRIDAT
    this.w_COPRIDOC = i_oFrom.w_COPRIDOC
    this.w_PRIMADATA = i_oFrom.w_PRIMADATA
    this.w_PRIMADATADOC = i_oFrom.w_PRIMADATADOC
    this.w_DATINI = i_oFrom.w_DATINI
    this.w_DATFIN = i_oFrom.w_DATFIN
    this.w_PATIPRIS = i_oFrom.w_PATIPRIS
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_QTAMOV = i_oFrom.w_QTAMOV
    this.w_CODVAL = i_oFrom.w_CODVAL
    this.w_FLSERG = i_oFrom.w_FLSERG
    this.w_CARAGGST = i_oFrom.w_CARAGGST
    this.w_ELGESRAT = i_oFrom.w_ELGESRAT
    this.w_FLATT = i_oFrom.w_FLATT
    this.w_FLATTI = i_oFrom.w_FLATTI
    this.w_CODPAG = i_oFrom.w_CODPAG
    this.w_DESPER = i_oFrom.w_DESPER
    this.w_DESCAT = i_oFrom.w_DESCAT
    this.w_ELPREZZO = i_oFrom.w_ELPREZZO
    this.w_ELSCONT1 = i_oFrom.w_ELSCONT1
    this.w_ELCONCOD = i_oFrom.w_ELCONCOD
    this.w_ELSCONT2 = i_oFrom.w_ELSCONT2
    this.w_ELSCONT3 = i_oFrom.w_ELSCONT3
    this.w_ELSCONT4 = i_oFrom.w_ELSCONT4
    this.w_DESCRIAT = i_oFrom.w_DESCRIAT
    this.w_ELDATINI = i_oFrom.w_ELDATINI
    this.w_ELDATFIN = i_oFrom.w_ELDATFIN
    this.w_ELCATCON = i_oFrom.w_ELCATCON
    this.w_FLNSAP = i_oFrom.w_FLNSAP
    this.w_ELCODLIS = i_oFrom.w_ELCODLIS
    this.w_ELTIPATT = i_oFrom.w_ELTIPATT
    this.w_ELGRUPAR = i_oFrom.w_ELGRUPAR
    this.w_ELDATDOC = i_oFrom.w_ELDATDOC
    this.w_ELPERATT = i_oFrom.w_ELPERATT
    this.w_ELDATATT = i_oFrom.w_ELDATATT
    this.w_ELCAUDOC = i_oFrom.w_ELCAUDOC
    this.w_ELGIOATT = i_oFrom.w_ELGIOATT
    this.w_ELDATGAT = i_oFrom.w_ELDATGAT
    this.w_ELCODPAG = i_oFrom.w_ELCODPAG
    this.w_DESLIS = i_oFrom.w_DESLIS
    this.w_ELPERFAT = i_oFrom.w_ELPERFAT
    this.w_PREZUM = i_oFrom.w_PREZUM
    this.w_ELDATFAT = i_oFrom.w_ELDATFAT
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_ELGIODOC = i_oFrom.w_ELGIODOC
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_CATCLI = i_oFrom.w_CATCLI
    this.w_CATART = i_oFrom.w_CATART
    this.w_SCOLIS = i_oFrom.w_SCOLIS
    this.w_DECUNI = i_oFrom.w_DECUNI
    this.w_CACODART = i_oFrom.w_CACODART
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_FLUSEP = i_oFrom.w_FLUSEP
    this.w_FLFRAZ1 = i_oFrom.w_FLFRAZ1
    this.w_MODUM2 = i_oFrom.w_MODUM2
    this.w_TIPATT = i_oFrom.w_TIPATT
    this.w_TIPDOC = i_oFrom.w_TIPDOC
    this.w_GRUPAR = i_oFrom.w_GRUPAR
    this.w_MCAUDOC = i_oFrom.w_MCAUDOC
    this.w_MTIPATT = i_oFrom.w_MTIPATT
    this.w_MGRUPAR = i_oFrom.w_MGRUPAR
    this.w_VALLIS = i_oFrom.w_VALLIS
    this.w_CATDOC = i_oFrom.w_CATDOC
    this.w_TIPRIS = i_oFrom.w_TIPRIS
    this.w_CICLO = i_oFrom.w_CICLO
    this.w_LORNET = i_oFrom.w_LORNET
    this.w_FLSCOR = i_oFrom.w_FLSCOR
    this.w_DESPAG = i_oFrom.w_DESPAG
    this.w_INILIS = i_oFrom.w_INILIS
    this.w_FINLIS = i_oFrom.w_FINLIS
    this.w_QUALIS = i_oFrom.w_QUALIS
    this.w_NUMLIS = i_oFrom.w_NUMLIS
    this.w_IVACLI = i_oFrom.w_IVACLI
    this.w_OB_TEST = i_oFrom.w_OB_TEST
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_LIPREZZO = i_oFrom.w_LIPREZZO
    this.w_TIPSER = i_oFrom.w_TIPSER
    this.w_ELPROLIS = i_oFrom.w_ELPROLIS
    this.w_ELPROSCO = i_oFrom.w_ELPROSCO
    this.w_FLGLIS = i_oFrom.w_FLGLIS
    this.w_FLINTE = i_oFrom.w_FLINTE
    this.w_ARCODART = i_oFrom.w_ARCODART
    this.w_NULLA = i_oFrom.w_NULLA
    this.w_DATALIST = i_oFrom.w_DATALIST
    this.w_ELSCOLIS = i_oFrom.w_ELSCOLIS
    this.w_ELTCOLIS = i_oFrom.w_ELTCOLIS
    this.w_KEYLISTB = i_oFrom.w_KEYLISTB
    this.w_MVCODVAL = i_oFrom.w_MVCODVAL
    this.w_FLVEAC = i_oFrom.w_FLVEAC
    this.w_FLRIPS = i_oFrom.w_FLRIPS
    this.w_GESTGUID = i_oFrom.w_GESTGUID
    this.w_ARUNMIS1 = i_oFrom.w_ARUNMIS1
    this.w_ARUNMIS2 = i_oFrom.w_ARUNMIS2
    this.w_MVFLVEAC = i_oFrom.w_MVFLVEAC
    this.w_CCODLIS = i_oFrom.w_CCODLIS
    this.w_MCODLIS = i_oFrom.w_MCODLIS
    this.w_ELRINNOV = i_oFrom.w_ELRINNOV
    this.w_ELESCRIN = i_oFrom.w_ELESCRIN
    this.w_ELPERCON = i_oFrom.w_ELPERCON
    this.w_ELRINCON = i_oFrom.w_ELRINCON
    this.w_ELNUMGIO = i_oFrom.w_ELNUMGIO
    this.w_ELDATDIS = i_oFrom.w_ELDATDIS
    this.w_DESATT = i_oFrom.w_DESATT
    this.w_DESDOC = i_oFrom.w_DESDOC
    this.w_DESCTR = i_oFrom.w_DESCTR
    this.w_ELVOCCEN = i_oFrom.w_ELVOCCEN
    this.w_ELCODCEN = i_oFrom.w_ELCODCEN
    this.w_ELCOCOMM = i_oFrom.w_ELCOCOMM
    this.w_ELCODATT = i_oFrom.w_ELCODATT
    this.w_MCODCEN = i_oFrom.w_MCODCEN
    this.w_MCOCOMM = i_oFrom.w_MCOCOMM
    this.w_CCOCOMM = i_oFrom.w_CCOCOMM
    this.w_CCODCEN = i_oFrom.w_CCODCEN
    this.w_MOTIPCON = i_oFrom.w_MOTIPCON
    this.w_MD__FREQ = i_oFrom.w_MD__FREQ
    this.w_DPDESCRI = i_oFrom.w_DPDESCRI
    this.w_ATTEXIST = i_oFrom.w_ATTEXIST
    this.w_DOCEXIST = i_oFrom.w_DOCEXIST
    this.w_FLSCOR = i_oFrom.w_FLSCOR
    this.w_CATCOM = i_oFrom.w_CATCOM
    this.w_CT = i_oFrom.w_CT
    this.w_CC = i_oFrom.w_CC
    this.w_CM = i_oFrom.w_CM
    this.w_CI = i_oFrom.w_CI
    this.w_CF = i_oFrom.w_CF
    this.w_CV = i_oFrom.w_CV
    this.w_QUACON = i_oFrom.w_QUACON
    this.w_IVACON = i_oFrom.w_IVACON
    this.w_MDDESCRI = i_oFrom.w_MDDESCRI
    this.w_ELINICOA = i_oFrom.w_ELINICOA
    this.w_ELFINCOA = i_oFrom.w_ELFINCOA
    this.w_ELINICOD = i_oFrom.w_ELINICOD
    this.w_ELFINCOD = i_oFrom.w_ELFINCOD
    this.w_DTBSO_CAUMATTI = i_oFrom.w_DTBSO_CAUMATTI
    this.w_ELQTACON = i_oFrom.w_ELQTACON
    this.w_QTA__RES = i_oFrom.w_QTA__RES
    this.w_PREZZTOT = i_oFrom.w_PREZZTOT
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_RESCHK = i_oFrom.w_RESCHK
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ELCODMOD = this.w_ELCODMOD
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_ELCODCLI = this.w_ELCODCLI
    i_oTo.w_CODICE = this.w_CODICE
    i_oTo.w_ELCONTRA = this.w_ELCONTRA
    i_oTo.w_CODCLI = this.w_CODCLI
    i_oTo.w_ELCODART = this.w_ELCODART
    i_oTo.w_ELUNIMIS = this.w_ELUNIMIS
    i_oTo.w_ELQTAMOV = this.w_ELQTAMOV
    i_oTo.w_TIPART = this.w_TIPART
    i_oTo.w_TIPO = this.w_TIPO
    i_oTo.w_ELCODVAL = this.w_ELCODVAL
    i_oTo.w_ELCODIMP = this.w_ELCODIMP
    i_oTo.w_TIPCON = this.w_TIPCON
    i_oTo.w_ELCODCOM = this.w_ELCODCOM
    i_oTo.w_PERFAT = this.w_PERFAT
    i_oTo.w_CATCON = this.w_CATCON
    i_oTo.w_GESRAT = this.w_GESRAT
    i_oTo.w_UNIMIS = this.w_UNIMIS
    i_oTo.w_TIPMOD = this.w_TIPMOD
    i_oTo.w_PERFAT = this.w_PERFAT
    i_oTo.w_PERATT = this.w_PERATT
    i_oTo.w_PRIDAT = this.w_PRIDAT
    i_oTo.w_PRIDOC = this.w_PRIDOC
    i_oTo.w_COPRIDAT = this.w_COPRIDAT
    i_oTo.w_COPRIDOC = this.w_COPRIDOC
    i_oTo.w_PRIMADATA = this.w_PRIMADATA
    i_oTo.w_PRIMADATADOC = this.w_PRIMADATADOC
    i_oTo.w_DATINI = this.w_DATINI
    i_oTo.w_DATFIN = this.w_DATFIN
    i_oTo.w_PATIPRIS = this.w_PATIPRIS
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_QTAMOV = this.w_QTAMOV
    i_oTo.w_CODVAL = this.w_CODVAL
    i_oTo.w_FLSERG = this.w_FLSERG
    i_oTo.w_CARAGGST = this.w_CARAGGST
    i_oTo.w_ELGESRAT = this.w_ELGESRAT
    i_oTo.w_FLATT = this.w_FLATT
    i_oTo.w_FLATTI = this.w_FLATTI
    i_oTo.w_CODPAG = this.w_CODPAG
    i_oTo.w_DESPER = this.w_DESPER
    i_oTo.w_DESCAT = this.w_DESCAT
    i_oTo.w_ELPREZZO = this.w_ELPREZZO
    i_oTo.w_ELSCONT1 = this.w_ELSCONT1
    i_oTo.w_ELCONCOD = this.w_ELCONCOD
    i_oTo.w_ELSCONT2 = this.w_ELSCONT2
    i_oTo.w_ELSCONT3 = this.w_ELSCONT3
    i_oTo.w_ELSCONT4 = this.w_ELSCONT4
    i_oTo.w_DESCRIAT = this.w_DESCRIAT
    i_oTo.w_ELDATINI = this.w_ELDATINI
    i_oTo.w_ELDATFIN = this.w_ELDATFIN
    i_oTo.w_ELCATCON = this.w_ELCATCON
    i_oTo.w_FLNSAP = this.w_FLNSAP
    i_oTo.w_ELCODLIS = this.w_ELCODLIS
    i_oTo.w_ELTIPATT = this.w_ELTIPATT
    i_oTo.w_ELGRUPAR = this.w_ELGRUPAR
    i_oTo.w_ELDATDOC = this.w_ELDATDOC
    i_oTo.w_ELPERATT = this.w_ELPERATT
    i_oTo.w_ELDATATT = this.w_ELDATATT
    i_oTo.w_ELCAUDOC = this.w_ELCAUDOC
    i_oTo.w_ELGIOATT = this.w_ELGIOATT
    i_oTo.w_ELDATGAT = this.w_ELDATGAT
    i_oTo.w_ELCODPAG = this.w_ELCODPAG
    i_oTo.w_DESLIS = this.w_DESLIS
    i_oTo.w_ELPERFAT = this.w_ELPERFAT
    i_oTo.w_PREZUM = this.w_PREZUM
    i_oTo.w_ELDATFAT = this.w_ELDATFAT
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_ELGIODOC = this.w_ELGIODOC
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_CATCLI = this.w_CATCLI
    i_oTo.w_CATART = this.w_CATART
    i_oTo.w_SCOLIS = this.w_SCOLIS
    i_oTo.w_DECUNI = this.w_DECUNI
    i_oTo.w_CACODART = this.w_CACODART
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_FLUSEP = this.w_FLUSEP
    i_oTo.w_FLFRAZ1 = this.w_FLFRAZ1
    i_oTo.w_MODUM2 = this.w_MODUM2
    i_oTo.w_TIPATT = this.w_TIPATT
    i_oTo.w_TIPDOC = this.w_TIPDOC
    i_oTo.w_GRUPAR = this.w_GRUPAR
    i_oTo.w_MCAUDOC = this.w_MCAUDOC
    i_oTo.w_MTIPATT = this.w_MTIPATT
    i_oTo.w_MGRUPAR = this.w_MGRUPAR
    i_oTo.w_VALLIS = this.w_VALLIS
    i_oTo.w_CATDOC = this.w_CATDOC
    i_oTo.w_TIPRIS = this.w_TIPRIS
    i_oTo.w_CICLO = this.w_CICLO
    i_oTo.w_LORNET = this.w_LORNET
    i_oTo.w_FLSCOR = this.w_FLSCOR
    i_oTo.w_DESPAG = this.w_DESPAG
    i_oTo.w_INILIS = this.w_INILIS
    i_oTo.w_FINLIS = this.w_FINLIS
    i_oTo.w_QUALIS = this.w_QUALIS
    i_oTo.w_NUMLIS = this.w_NUMLIS
    i_oTo.w_IVACLI = this.w_IVACLI
    i_oTo.w_OB_TEST = this.w_OB_TEST
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_LIPREZZO = this.w_LIPREZZO
    i_oTo.w_TIPSER = this.w_TIPSER
    i_oTo.w_ELPROLIS = this.w_ELPROLIS
    i_oTo.w_ELPROSCO = this.w_ELPROSCO
    i_oTo.w_FLGLIS = this.w_FLGLIS
    i_oTo.w_FLINTE = this.w_FLINTE
    i_oTo.w_ARCODART = this.w_ARCODART
    i_oTo.w_NULLA = this.w_NULLA
    i_oTo.w_DATALIST = this.w_DATALIST
    i_oTo.w_ELSCOLIS = this.w_ELSCOLIS
    i_oTo.w_ELTCOLIS = this.w_ELTCOLIS
    i_oTo.w_KEYLISTB = this.w_KEYLISTB
    i_oTo.w_MVCODVAL = this.w_MVCODVAL
    i_oTo.w_FLVEAC = this.w_FLVEAC
    i_oTo.w_FLRIPS = this.w_FLRIPS
    i_oTo.w_GESTGUID = this.w_GESTGUID
    i_oTo.w_ARUNMIS1 = this.w_ARUNMIS1
    i_oTo.w_ARUNMIS2 = this.w_ARUNMIS2
    i_oTo.w_MVFLVEAC = this.w_MVFLVEAC
    i_oTo.w_CCODLIS = this.w_CCODLIS
    i_oTo.w_MCODLIS = this.w_MCODLIS
    i_oTo.w_ELRINNOV = this.w_ELRINNOV
    i_oTo.w_ELESCRIN = this.w_ELESCRIN
    i_oTo.w_ELPERCON = this.w_ELPERCON
    i_oTo.w_ELRINCON = this.w_ELRINCON
    i_oTo.w_ELNUMGIO = this.w_ELNUMGIO
    i_oTo.w_ELDATDIS = this.w_ELDATDIS
    i_oTo.w_DESATT = this.w_DESATT
    i_oTo.w_DESDOC = this.w_DESDOC
    i_oTo.w_DESCTR = this.w_DESCTR
    i_oTo.w_ELVOCCEN = this.w_ELVOCCEN
    i_oTo.w_ELCODCEN = this.w_ELCODCEN
    i_oTo.w_ELCOCOMM = this.w_ELCOCOMM
    i_oTo.w_ELCODATT = this.w_ELCODATT
    i_oTo.w_MCODCEN = this.w_MCODCEN
    i_oTo.w_MCOCOMM = this.w_MCOCOMM
    i_oTo.w_CCOCOMM = this.w_CCOCOMM
    i_oTo.w_CCODCEN = this.w_CCODCEN
    i_oTo.w_MOTIPCON = this.w_MOTIPCON
    i_oTo.w_MD__FREQ = this.w_MD__FREQ
    i_oTo.w_DPDESCRI = this.w_DPDESCRI
    i_oTo.w_ATTEXIST = this.w_ATTEXIST
    i_oTo.w_DOCEXIST = this.w_DOCEXIST
    i_oTo.w_FLSCOR = this.w_FLSCOR
    i_oTo.w_CATCOM = this.w_CATCOM
    i_oTo.w_CT = this.w_CT
    i_oTo.w_CC = this.w_CC
    i_oTo.w_CM = this.w_CM
    i_oTo.w_CI = this.w_CI
    i_oTo.w_CF = this.w_CF
    i_oTo.w_CV = this.w_CV
    i_oTo.w_QUACON = this.w_QUACON
    i_oTo.w_IVACON = this.w_IVACON
    i_oTo.w_MDDESCRI = this.w_MDDESCRI
    i_oTo.w_ELINICOA = this.w_ELINICOA
    i_oTo.w_ELFINCOA = this.w_ELFINCOA
    i_oTo.w_ELINICOD = this.w_ELINICOD
    i_oTo.w_ELFINCOD = this.w_ELFINCOD
    i_oTo.w_DTBSO_CAUMATTI = this.w_DTBSO_CAUMATTI
    i_oTo.w_ELQTACON = this.w_ELQTACON
    i_oTo.w_QTA__RES = this.w_QTA__RES
    i_oTo.w_PREZZTOT = this.w_PREZZTOT
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_RESCHK = this.w_RESCHK
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsag_mec as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 711
  Height = 607
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-24"
  HelpContextID=63580009
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=167

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ELE_CONT_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  CAT_ELEM_IDX = 0
  DIPENDEN_IDX = 0
  TIP_DOCU_IDX = 0
  CAUMATTI_IDX = 0
  MODCLDAT_IDX = 0
  LISTINI_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  KEY_ARTI_IDX = 0
  PAG_AMEN_IDX = 0
  CON_TRAM_IDX = 0
  cFile = "ELE_CONT"
  cKeySelect = "ELCODIMP,ELCODCOM"
  cKeyWhere  = "ELCODIMP=this.w_ELCODIMP and ELCODCOM=this.w_ELCODCOM"
  cKeyDetail  = "ELCODMOD=this.w_ELCODMOD and ELCONTRA=this.w_ELCONTRA and ELCODIMP=this.w_ELCODIMP and ELCODCOM=this.w_ELCODCOM and ELRINNOV=this.w_ELRINNOV"
  cKeyWhereODBC = '"ELCODIMP="+cp_ToStrODBC(this.w_ELCODIMP)';
      +'+" and ELCODCOM="+cp_ToStrODBC(this.w_ELCODCOM)';

  cKeyDetailWhereODBC = '"ELCODMOD="+cp_ToStrODBC(this.w_ELCODMOD)';
      +'+" and ELCONTRA="+cp_ToStrODBC(this.w_ELCONTRA)';
      +'+" and ELCODIMP="+cp_ToStrODBC(this.w_ELCODIMP)';
      +'+" and ELCODCOM="+cp_ToStrODBC(this.w_ELCODCOM)';
      +'+" and ELRINNOV="+cp_ToStrODBC(this.w_ELRINNOV)';

  cKeyWhereODBCqualified = '"ELE_CONT.ELCODIMP="+cp_ToStrODBC(this.w_ELCODIMP)';
      +'+" and ELE_CONT.ELCODCOM="+cp_ToStrODBC(this.w_ELCODCOM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ELE_CONT.ELCODMOD,ELE_CONT.ELCONTRA,ELE_CONT.ELRINNOV DESC'
  cPrg = "gsag_mec"
  cComment = "Elementi contratto impianto"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ELCODMOD = space(10)
  o_ELCODMOD = space(10)
  w_CODART = space(20)
  w_CODART = space(20)
  w_ELCODCLI = space(15)
  o_ELCODCLI = space(15)
  w_CODICE = space(15)
  w_ELCONTRA = space(10)
  o_ELCONTRA = space(10)
  w_CODCLI = space(15)
  w_ELCODART = space(20)
  o_ELCODART = space(20)
  w_ELUNIMIS = space(3)
  w_ELQTAMOV = 0
  o_ELQTAMOV = 0
  w_TIPART = space(2)
  o_TIPART = space(2)
  w_TIPO = space(1)
  w_ELCODVAL = space(3)
  o_ELCODVAL = space(3)
  w_ELCODIMP = space(10)
  w_TIPCON = space(1)
  w_ELCODCOM = 0
  w_PERFAT = space(3)
  w_CATCON = space(3)
  w_GESRAT = space(1)
  w_UNIMIS = space(5)
  w_TIPMOD = space(1)
  w_PERFAT = space(3)
  w_PERATT = space(3)
  w_PRIDAT = space(1)
  w_PRIDOC = space(1)
  w_COPRIDAT = space(1)
  w_COPRIDOC = space(1)
  w_PRIMADATA = space(1)
  o_PRIMADATA = space(1)
  w_PRIMADATADOC = space(1)
  o_PRIMADATADOC = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_PATIPRIS = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_QTAMOV = 0
  w_CODVAL = space(3)
  w_FLSERG = space(1)
  w_CARAGGST = space(10)
  w_ELGESRAT = space(1)
  w_FLATT = space(1)
  o_FLATT = space(1)
  w_FLATTI = space(1)
  o_FLATTI = space(1)
  w_CODPAG = space(5)
  w_DESPER = space(50)
  w_DESCAT = space(60)
  w_ELPREZZO = 0
  o_ELPREZZO = 0
  w_ELSCONT1 = 0
  o_ELSCONT1 = 0
  w_ELCONCOD = space(15)
  w_ELSCONT2 = 0
  o_ELSCONT2 = 0
  w_ELSCONT3 = 0
  o_ELSCONT3 = 0
  w_ELSCONT4 = 0
  o_ELSCONT4 = 0
  w_DESCRIAT = space(50)
  w_ELDATINI = ctod('  /  /  ')
  o_ELDATINI = ctod('  /  /  ')
  w_ELDATFIN = ctod('  /  /  ')
  o_ELDATFIN = ctod('  /  /  ')
  w_ELCATCON = space(5)
  w_FLNSAP = space(1)
  w_ELCODLIS = space(5)
  w_ELTIPATT = space(20)
  w_ELGRUPAR = space(5)
  w_ELDATDOC = ctod('  /  /  ')
  w_ELPERATT = space(3)
  o_ELPERATT = space(3)
  w_ELDATATT = ctod('  /  /  ')
  o_ELDATATT = ctod('  /  /  ')
  w_ELCAUDOC = space(5)
  o_ELCAUDOC = space(5)
  w_ELGIOATT = 0
  o_ELGIOATT = 0
  w_ELDATGAT = ctod('  /  /  ')
  w_ELCODPAG = space(5)
  w_DESLIS = space(40)
  w_ELPERFAT = space(3)
  o_ELPERFAT = space(3)
  w_PREZUM = space(1)
  w_ELDATFAT = ctod('  /  /  ')
  o_ELDATFAT = ctod('  /  /  ')
  w_OPERAT = space(1)
  w_ELGIODOC = 0
  o_ELGIODOC = 0
  w_MOLTIP = 0
  w_CATCLI = space(5)
  w_CATART = space(5)
  w_SCOLIS = space(1)
  w_DECUNI = 0
  w_CACODART = space(20)
  w_MOLTI3 = 0
  w_OPERA3 = space(1)
  w_FLUSEP = space(1)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(2)
  w_TIPATT = space(20)
  w_TIPDOC = space(5)
  w_GRUPAR = space(5)
  w_MCAUDOC = space(5)
  w_MTIPATT = space(20)
  w_MGRUPAR = space(5)
  w_VALLIS = space(3)
  w_CATDOC = space(2)
  w_TIPRIS = space(1)
  w_CICLO = space(1)
  w_LORNET = space(1)
  w_FLSCOR = space(1)
  w_DESPAG = space(30)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_QUALIS = space(1)
  w_NUMLIS = space(5)
  w_IVACLI = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_LIPREZZO = 0
  w_TIPSER = space(1)
  w_ELPROLIS = space(5)
  w_ELPROSCO = space(5)
  w_FLGLIS = space(1)
  w_FLINTE = space(1)
  w_ARCODART = space(20)
  w_NULLA = space(1)
  w_DATALIST = ctod('  /  /  ')
  w_ELSCOLIS = space(5)
  w_ELTCOLIS = space(5)
  w_KEYLISTB = space(10)
  w_MVCODVAL = space(5)
  w_FLVEAC = space(1)
  w_FLRIPS = space(10)
  w_GESTGUID = space(10)
  w_ARUNMIS1 = space(3)
  w_ARUNMIS2 = space(3)
  w_MVFLVEAC = space(1)
  w_CCODLIS = space(5)
  w_MCODLIS = space(5)
  w_ELRINNOV = 0
  w_ELESCRIN = space(1)
  o_ELESCRIN = space(1)
  w_ELPERCON = space(3)
  w_ELRINCON = space(1)
  w_ELNUMGIO = 0
  w_ELDATDIS = ctod('  /  /  ')
  w_DESATT = space(254)
  w_DESDOC = space(35)
  w_DESCTR = space(35)
  w_ELVOCCEN = space(15)
  w_ELCODCEN = space(15)
  w_ELCOCOMM = space(15)
  w_ELCODATT = space(15)
  w_MCODCEN = space(15)
  w_MCOCOMM = space(15)
  w_CCOCOMM = space(15)
  w_CCODCEN = space(15)
  w_MOTIPCON = space(1)
  w_MD__FREQ = space(1)
  w_DPDESCRI = space(60)
  w_ATTEXIST = .F.
  w_DOCEXIST = .F.
  w_FLSCOR = space(1)
  w_CATCOM = space(3)
  w_CT = space(1)
  w_CC = space(15)
  w_CM = space(3)
  w_CI = ctod('  /  /  ')
  w_CF = ctod('  /  /  ')
  w_CV = space(3)
  w_QUACON = space(1)
  w_IVACON = space(1)
  w_MDDESCRI = space(50)
  w_ELINICOA = ctod('  /  /  ')
  w_ELFINCOA = ctod('  /  /  ')
  w_ELINICOD = ctod('  /  /  ')
  w_ELFINCOD = ctod('  /  /  ')
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_ELQTACON = 0
  w_QTA__RES = 0
  w_PREZZTOT = 0
  w_OBTEST = ctod('  /  /  ')
  w_RESCHK = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_mec
  proc F6()
    if this.RowStatus() <> "A"
      if ah_yesno("Attenzione: l'elemento contratto abbinato sar� eliminato dal relativo archivio.%0Si desidera proseguire?")
       dodefault()
      endif
    else
      dodefault()
    endif
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mecPag1","gsag_mec",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Elementi contratto")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[16]
    this.cWorkTables[1]='CON_TRAS'
    this.cWorkTables[2]='MOD_ELEM'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='CAT_ELEM'
    this.cWorkTables[6]='DIPENDEN'
    this.cWorkTables[7]='TIP_DOCU'
    this.cWorkTables[8]='CAUMATTI'
    this.cWorkTables[9]='MODCLDAT'
    this.cWorkTables[10]='LISTINI'
    this.cWorkTables[11]='CONTI'
    this.cWorkTables[12]='VALUTE'
    this.cWorkTables[13]='KEY_ARTI'
    this.cWorkTables[14]='PAG_AMEN'
    this.cWorkTables[15]='CON_TRAM'
    this.cWorkTables[16]='ELE_CONT'
    * --- Area Manuale = Open Work Table
    * --- gsag_mec
    This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
    "IIF(Empty(t_ELDATFAT)AND Empty(t_ELDATATT) and t_QTA__RES=0, 12566463, 16777215)"
    * --- Fine Area Manuale
  return(this.OpenAllTables(16))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ELE_CONT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ELE_CONT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsag_mec'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_2_52_joined
    link_2_52_joined=.f.
    local link_2_54_joined
    link_2_54_joined=.f.
    local link_2_55_joined
    link_2_55_joined=.f.
    local link_2_56_joined
    link_2_56_joined=.f.
    local link_2_58_joined
    link_2_58_joined=.f.
    local link_2_60_joined
    link_2_60_joined=.f.
    local link_2_63_joined
    link_2_63_joined=.f.
    local link_2_65_joined
    link_2_65_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ELE_CONT where ELCODMOD=KeySet.ELCODMOD
    *                            and ELCONTRA=KeySet.ELCONTRA
    *                            and ELCODIMP=KeySet.ELCODIMP
    *                            and ELCODCOM=KeySet.ELCODCOM
    *                            and ELRINNOV=KeySet.ELRINNOV
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2],this.bLoadRecFilter,this.ELE_CONT_IDX,"gsag_mec")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ELE_CONT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ELE_CONT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ELE_CONT '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_52_joined=this.AddJoinedLink_2_52(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_54_joined=this.AddJoinedLink_2_54(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_55_joined=this.AddJoinedLink_2_55(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_56_joined=this.AddJoinedLink_2_56(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_58_joined=this.AddJoinedLink_2_58(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_60_joined=this.AddJoinedLink_2_60(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_63_joined=this.AddJoinedLink_2_63(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_65_joined=this.AddJoinedLink_2_65(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ELCODIMP',this.w_ELCODIMP  ,'ELCODCOM',this.w_ELCODCOM  )
      select * from (i_cTable) ELE_CONT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ELCODCLI = space(15)
        .w_TIPO = space(1)
        .w_PERFAT = space(3)
        .w_CATCON = space(3)
        .w_GESRAT = space(1)
        .w_PERFAT = space(3)
        .w_PERATT = space(3)
        .w_OPERAT = space(1)
        .w_CATDOC = space(2)
        .w_LORNET = space(1)
        .w_FLSCOR = space(1)
        .w_NUMLIS = space(5)
        .w_IVACLI = space(1)
        .w_FLINTE = space(1)
        .w_KEYLISTB = Sys(2015)
        .w_FLSCOR = space(1)
        .w_CATCOM = space(3)
        .w_CT = space(1)
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_RESCHK = 0
          .link_2_4('Load')
        .w_ELCODIMP = NVL(ELCODIMP,space(10))
        .w_TIPCON = 'C'
        .w_ELCODCOM = NVL(ELCODCOM,0)
        .w_ELGESRAT = NVL(ELGESRAT,space(1))
          .link_1_17('Load')
        .w_ELPROLIS = NVL(ELPROLIS,space(5))
        .w_ELPROSCO = NVL(ELPROSCO,space(5))
        .w_FLGLIS = 'N'
        .w_NULLA = 'X'
        .w_ELSCOLIS = NVL(ELSCOLIS,space(5))
        .w_ELTCOLIS = NVL(ELTCOLIS,space(5))
        .w_MVCODVAL = .w_ELCODVAL
        .w_MVFLVEAC = 'V'
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        cp_LoadRecExtFlds(this,'ELE_CONT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CODART = space(20)
          .w_CODART = space(20)
          .w_CODCLI = space(15)
          .w_TIPART = space(2)
          .w_UNIMIS = space(5)
          .w_TIPMOD = space(1)
          .w_PRIDAT = space(1)
          .w_PRIDOC = space(1)
          .w_COPRIDAT = space(1)
          .w_COPRIDOC = space(1)
          .w_DATINI = ctod("  /  /  ")
          .w_DATFIN = ctod("  /  /  ")
        .w_PATIPRIS = 'G'
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_UNMIS3 = space(3)
          .w_QTAMOV = 0
          .w_CODVAL = space(3)
          .w_FLSERG = space(1)
          .w_CARAGGST = space(10)
          .w_FLATT = space(1)
          .w_FLATTI = space(1)
          .w_CODPAG = space(5)
          .w_DESPER = space(50)
          .w_DESCAT = space(60)
          .w_DESCRIAT = space(50)
          .w_FLNSAP = space(1)
          .w_DESLIS = space(40)
          .w_PREZUM = space(1)
          .w_MOLTIP = 0
          .w_CATCLI = space(5)
          .w_CATART = space(5)
          .w_SCOLIS = space(1)
          .w_DECUNI = 0
          .w_MOLTI3 = 0
          .w_OPERA3 = space(1)
          .w_FLUSEP = space(1)
          .w_FLFRAZ1 = space(1)
          .w_MODUM2 = space(2)
          .w_TIPATT = space(20)
          .w_TIPDOC = space(5)
          .w_GRUPAR = space(5)
          .w_MCAUDOC = space(5)
          .w_MTIPATT = space(20)
          .w_MGRUPAR = space(5)
          .w_VALLIS = space(3)
          .w_TIPRIS = space(1)
        .w_CICLO = 'E'
          .w_DESPAG = space(30)
          .w_INILIS = ctod("  /  /  ")
          .w_FINLIS = ctod("  /  /  ")
          .w_QUALIS = space(1)
          .w_DATOBSO = ctod("  /  /  ")
          .w_LIPREZZO = 0
          .w_TIPSER = space(1)
          .w_FLVEAC = space(1)
          .w_FLRIPS = space(10)
          .w_GESTGUID = space(10)
          .w_CCODLIS = space(5)
          .w_MCODLIS = space(5)
          .w_DESATT = space(254)
          .w_DESDOC = space(35)
          .w_DESCTR = space(35)
          .w_MCODCEN = space(15)
          .w_MCOCOMM = space(15)
          .w_CCOCOMM = space(15)
          .w_CCODCEN = space(15)
          .w_MOTIPCON = space(1)
          .w_MD__FREQ = space(1)
          .w_DPDESCRI = space(60)
          .w_ATTEXIST = .f.
          .w_DOCEXIST = .f.
          .w_CC = space(15)
          .w_CM = space(3)
          .w_CI = ctod("  /  /  ")
          .w_CF = ctod("  /  /  ")
          .w_CV = space(3)
          .w_QUACON = space(1)
          .w_IVACON = space(1)
          .w_MDDESCRI = space(50)
          .w_ELCODMOD = NVL(ELCODMOD,space(10))
          if link_2_1_joined
            this.w_ELCODMOD = NVL(MOCODICE201,NVL(this.w_ELCODMOD,space(10)))
            this.w_ELCATCON = NVL(MOCATELE201,space(5))
            this.w_PERFAT = NVL(MOPERFAT201,space(3))
            this.w_ELGESRAT = NVL(MOGESRAT201,space(1))
            this.w_TIPMOD = NVL(MOTIPAPL201,space(1))
            this.w_CODART = NVL(MOCODSER201,space(20))
            this.w_ELUNIMIS = NVL(MOUNIMIS201,space(3))
            this.w_ELQTAMOV = NVL(MOQTAMOV201,0)
            this.w_ELCODVAL = NVL(MOCODVAL201,space(3))
            this.w_PERATT = NVL(MOPERATT201,space(3))
            this.w_MTIPATT = NVL(MOTIPATT201,space(20))
            this.w_MCAUDOC = NVL(MOCAUDOC201,space(5))
            this.w_MGRUPAR = NVL(MOGRUPAR201,space(5))
            this.w_FLATT = NVL(MOFLFATT201,space(1))
            this.w_FLATTI = NVL(MOFLATTI201,space(1))
            this.w_ELGIODOC = NVL(MOGIODOC201,0)
            this.w_ELGIOATT = NVL(MOGIOATT201,0)
            this.w_MCODLIS = NVL(MOCODLIS201,space(5))
            this.w_ELVOCCEN = NVL(MOVOCCEN201,space(15))
            this.w_MCODCEN = NVL(MOCODCEN201,space(15))
            this.w_MCOCOMM = NVL(MOCOCOMM201,space(15))
            this.w_ELCODATT = NVL(MOCODATT201,space(15))
            this.w_MOTIPCON = NVL(MOTIPCON201,space(1))
            this.w_PRIDAT = NVL(MOPRIDAT201,space(1))
            this.w_PRIDOC = NVL(MOPRIDOC201,space(1))
          else
          .link_2_1('Load')
          endif
        .w_CODICE = .w_ELCODCLI
          .w_ELCONTRA = NVL(ELCONTRA,space(10))
          if link_2_6_joined
            this.w_ELCONTRA = NVL(COSERIAL206,NVL(this.w_ELCONTRA,space(10)))
            this.w_CODCLI = NVL(COCODCON206,space(15))
            this.w_ELDATINI = NVL(cp_ToDate(CODATINI206),ctod("  /  /  "))
            this.w_ELDATFIN = NVL(cp_ToDate(CODATFIN206),ctod("  /  /  "))
            this.w_DATINI = NVL(cp_ToDate(CODATINI206),ctod("  /  /  "))
            this.w_DATFIN = NVL(cp_ToDate(CODATFIN206),ctod("  /  /  "))
            this.w_TIPDOC = NVL(COTIPDOC206,space(5))
            this.w_TIPATT = NVL(COTIPATT206,space(20))
            this.w_GRUPAR = NVL(COGRUPAR206,space(5))
            this.w_CODPAG = NVL(COCODPAG206,space(5))
            this.w_CCODLIS = NVL(COCODLIS206,space(5))
            this.w_CCODCEN = NVL(COCODCEN206,space(15))
            this.w_CCOCOMM = NVL(COCOCOMM206,space(15))
            this.w_COPRIDAT = NVL(COPRIDAT206,space(1))
            this.w_COPRIDOC = NVL(COPRIDOC206,space(1))
          else
          .link_2_6('Load')
          endif
          .w_ELCODART = NVL(ELCODART,space(20))
          if link_2_8_joined
            this.w_ELCODART = NVL(ARCODART208,NVL(this.w_ELCODART,space(20)))
            this.w_TIPART = NVL(ARTIPART208,space(2))
            this.w_UNMIS1 = NVL(ARUNMIS1208,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2208,space(3))
            this.w_FLSERG = NVL(ARFLSERG208,space(1))
            this.w_PREZUM = NVL(ARPREZUM208,space(1))
            this.w_OPERAT = NVL(AROPERAT208,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP208,0)
            this.w_CATART = NVL(ARCATSCM208,space(5))
            this.w_FLUSEP = NVL(ARFLUSEP208,space(1))
            this.w_ELUNIMIS = NVL(ARUNMIS1208,space(3))
          else
          .link_2_8('Load')
          endif
          .w_ELUNIMIS = NVL(ELUNIMIS,space(3))
          * evitabile
          *.link_2_9('Load')
          .w_ELQTAMOV = NVL(ELQTAMOV,0)
          .w_ELCODVAL = NVL(ELCODVAL,space(3))
          if link_1_2_joined
            this.w_ELCODVAL = NVL(VACODVAL102,NVL(this.w_ELCODVAL,space(3)))
            this.w_DECUNI = NVL(VADECUNI102,0)
          else
          .link_1_2('Load')
          endif
        .w_PRIMADATA = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'', IIF(.w_FLATTI<>'S','',IIF(.w_COPRIDAT='M',.w_PRIDAT,.w_COPRIDAT)))
        .w_PRIMADATADOC = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'',IIF(.w_FLATT<>'S','',IIF(.w_COPRIDOC='M',.w_PRIDOC,.w_COPRIDOC)))
          .link_2_31('Load')
          .w_ELPREZZO = NVL(ELPREZZO,0)
          .w_ELSCONT1 = NVL(ELSCONT1,0)
          .w_ELCONCOD = NVL(ELCONCOD,space(15))
          .link_2_45('Load')
          .w_ELSCONT2 = NVL(ELSCONT2,0)
          .w_ELSCONT3 = NVL(ELSCONT3,0)
          .w_ELSCONT4 = NVL(ELSCONT4,0)
          .w_ELDATINI = NVL(cp_ToDate(ELDATINI),ctod("  /  /  "))
          .w_ELDATFIN = NVL(cp_ToDate(ELDATFIN),ctod("  /  /  "))
          .w_ELCATCON = NVL(ELCATCON,space(5))
          if link_2_52_joined
            this.w_ELCATCON = NVL(CECODELE252,NVL(this.w_ELCATCON,space(5)))
            this.w_DESCAT = NVL(CEDESELE252,space(60))
          else
          .link_2_52('Load')
          endif
          .w_ELCODLIS = NVL(ELCODLIS,space(5))
          if link_2_54_joined
            this.w_ELCODLIS = NVL(LSCODLIS254,NVL(this.w_ELCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS254,space(40))
            this.w_SCOLIS = NVL(LSFLSCON254,space(1))
            this.w_VALLIS = NVL(LSVALLIS254,space(3))
            this.w_LORNET = NVL(LSIVALIS254,space(1))
            this.w_INILIS = NVL(cp_ToDate(LSDTINVA254),ctod("  /  /  "))
            this.w_FINLIS = NVL(cp_ToDate(LSDTOBSO254),ctod("  /  /  "))
            this.w_QUALIS = NVL(LSQUANTI254,space(1))
          else
          .link_2_54('Load')
          endif
          .w_ELTIPATT = NVL(ELTIPATT,space(20))
          if link_2_55_joined
            this.w_ELTIPATT = NVL(CACODICE255,NVL(this.w_ELTIPATT,space(20)))
            this.w_DESATT = NVL(CADESCRI255,space(254))
            this.w_FLNSAP = NVL(CAFLNSAP255,space(1))
          else
          .link_2_55('Load')
          endif
          .w_ELGRUPAR = NVL(ELGRUPAR,space(5))
          if link_2_56_joined
            this.w_ELGRUPAR = NVL(DPCODICE256,NVL(this.w_ELGRUPAR,space(5)))
            this.w_TIPRIS = NVL(DPTIPRIS256,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(DPDTOBSO256),ctod("  /  /  "))
            this.w_DPDESCRI = NVL(DPDESCRI256,space(60))
          else
          .link_2_56('Load')
          endif
          .w_ELDATDOC = NVL(cp_ToDate(ELDATDOC),ctod("  /  /  "))
          .w_ELPERATT = NVL(ELPERATT,space(3))
          if link_2_58_joined
            this.w_ELPERATT = NVL(MDCODICE258,NVL(this.w_ELPERATT,space(3)))
            this.w_DESCRIAT = NVL(MDDESCRI258,space(50))
          else
          .link_2_58('Load')
          endif
          .w_ELDATATT = NVL(cp_ToDate(ELDATATT),ctod("  /  /  "))
          .w_ELCAUDOC = NVL(ELCAUDOC,space(5))
          if link_2_60_joined
            this.w_ELCAUDOC = NVL(TDTIPDOC260,NVL(this.w_ELCAUDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC260,space(35))
            this.w_CATDOC = NVL(TDCATDOC260,space(2))
            this.w_FLVEAC = NVL(TDFLVEAC260,space(1))
            this.w_FLINTE = NVL(TDFLINTE260,space(1))
            this.w_FLRIPS = NVL(TDFLRIPS260,space(10))
          else
          .link_2_60('Load')
          endif
          .w_ELGIOATT = NVL(ELGIOATT,0)
          .w_ELDATGAT = NVL(cp_ToDate(ELDATGAT),ctod("  /  /  "))
          .w_ELCODPAG = NVL(ELCODPAG,space(5))
          if link_2_63_joined
            this.w_ELCODPAG = NVL(PACODICE263,NVL(this.w_ELCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI263,space(30))
          else
          .link_2_63('Load')
          endif
          .w_ELPERFAT = NVL(ELPERFAT,space(3))
          if link_2_65_joined
            this.w_ELPERFAT = NVL(MDCODICE265,NVL(this.w_ELPERFAT,space(3)))
            this.w_DESPER = NVL(MDDESCRI265,space(50))
            this.w_MD__FREQ = NVL(MD__FREQ265,space(1))
          else
          .link_2_65('Load')
          endif
          .w_ELDATFAT = NVL(cp_ToDate(ELDATFAT),ctod("  /  /  "))
          .w_ELGIODOC = NVL(ELGIODOC,0)
        .w_CACODART = .w_ELCODART
          .link_2_75('Load')
        .w_OB_TEST = IIF(EMPTY(.w_ELDATATT), i_INIDAT, .w_ELDATATT)
        .w_ARCODART = .w_ELCODART
        .w_DATALIST = IIF(Not empty(.w_ELDATFAT),.w_ELDATFAT,IIF(Empty(.w_ELDATATT),.w_ELDATINI,.w_ELDATATT))
        .w_ARUNMIS1 = .w_UNMIS1
        .w_ARUNMIS2 = .w_UNMIS2
          .w_ELRINNOV = NVL(ELRINNOV,0)
          .w_ELESCRIN = NVL(ELESCRIN,space(1))
          .w_ELPERCON = NVL(ELPERCON,space(3))
          .w_ELRINCON = NVL(ELRINCON,space(1))
          .w_ELNUMGIO = NVL(ELNUMGIO,0)
          .w_ELDATDIS = NVL(cp_ToDate(ELDATDIS),ctod("  /  /  "))
          .w_ELVOCCEN = NVL(ELVOCCEN,space(15))
          .w_ELCODCEN = NVL(ELCODCEN,space(15))
          .w_ELCOCOMM = NVL(ELCOCOMM,space(15))
          .w_ELCODATT = NVL(ELCODATT,space(15))
          .w_ELINICOA = NVL(cp_ToDate(ELINICOA),ctod("  /  /  "))
          .w_ELFINCOA = NVL(cp_ToDate(ELFINCOA),ctod("  /  /  "))
          .w_ELINICOD = NVL(cp_ToDate(ELINICOD),ctod("  /  /  "))
          .w_ELFINCOD = NVL(cp_ToDate(ELFINCOD),ctod("  /  /  "))
          .w_ELQTACON = NVL(ELQTACON,0)
        .w_QTA__RES = .w_ELQTAMOV - .w_ELQTACON
        .w_PREZZTOT = cp_round(.w_ELQTAMOV * (cp_round(.w_ELPREZZO *(100+.w_ELSCONT1)*(100+.w_ELSCONT2)*(100+.w_ELSCONT3)*(100+.w_ELSCONT4)/100000000  ,5) ),g_PERPVL)
        .w_OBTEST = I_DATSYS
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace ELCODMOD with .w_ELCODMOD
          replace ELCONTRA with .w_ELCONTRA
          replace ELRINNOV with .w_ELRINNOV
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_MVCODVAL = .w_ELCODVAL
        .w_MVFLVEAC = 'V'
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_110.enabled = .oPgFrm.Page1.oPag.oBtn_2_110.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_112.enabled = .oPgFrm.Page1.oPag.oBtn_2_112.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_116.enabled = .oPgFrm.Page1.oPag.oBtn_2_116.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_117.enabled = .oPgFrm.Page1.oPag.oBtn_2_117.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_121.enabled = .oPgFrm.Page1.oPag.oBtn_2_121.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_141.enabled = .oPgFrm.Page1.oPag.oBtn_2_141.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_142.enabled = .oPgFrm.Page1.oPag.oBtn_2_142.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ELCODMOD=space(10)
      .w_CODART=space(20)
      .w_CODART=space(20)
      .w_ELCODCLI=space(15)
      .w_CODICE=space(15)
      .w_ELCONTRA=space(10)
      .w_CODCLI=space(15)
      .w_ELCODART=space(20)
      .w_ELUNIMIS=space(3)
      .w_ELQTAMOV=0
      .w_TIPART=space(2)
      .w_TIPO=space(1)
      .w_ELCODVAL=space(3)
      .w_ELCODIMP=space(10)
      .w_TIPCON=space(1)
      .w_ELCODCOM=0
      .w_PERFAT=space(3)
      .w_CATCON=space(3)
      .w_GESRAT=space(1)
      .w_UNIMIS=space(5)
      .w_TIPMOD=space(1)
      .w_PERFAT=space(3)
      .w_PERATT=space(3)
      .w_PRIDAT=space(1)
      .w_PRIDOC=space(1)
      .w_COPRIDAT=space(1)
      .w_COPRIDOC=space(1)
      .w_PRIMADATA=space(1)
      .w_PRIMADATADOC=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_PATIPRIS=space(1)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_QTAMOV=0
      .w_CODVAL=space(3)
      .w_FLSERG=space(1)
      .w_CARAGGST=space(10)
      .w_ELGESRAT=space(1)
      .w_FLATT=space(1)
      .w_FLATTI=space(1)
      .w_CODPAG=space(5)
      .w_DESPER=space(50)
      .w_DESCAT=space(60)
      .w_ELPREZZO=0
      .w_ELSCONT1=0
      .w_ELCONCOD=space(15)
      .w_ELSCONT2=0
      .w_ELSCONT3=0
      .w_ELSCONT4=0
      .w_DESCRIAT=space(50)
      .w_ELDATINI=ctod("  /  /  ")
      .w_ELDATFIN=ctod("  /  /  ")
      .w_ELCATCON=space(5)
      .w_FLNSAP=space(1)
      .w_ELCODLIS=space(5)
      .w_ELTIPATT=space(20)
      .w_ELGRUPAR=space(5)
      .w_ELDATDOC=ctod("  /  /  ")
      .w_ELPERATT=space(3)
      .w_ELDATATT=ctod("  /  /  ")
      .w_ELCAUDOC=space(5)
      .w_ELGIOATT=0
      .w_ELDATGAT=ctod("  /  /  ")
      .w_ELCODPAG=space(5)
      .w_DESLIS=space(40)
      .w_ELPERFAT=space(3)
      .w_PREZUM=space(1)
      .w_ELDATFAT=ctod("  /  /  ")
      .w_OPERAT=space(1)
      .w_ELGIODOC=0
      .w_MOLTIP=0
      .w_CATCLI=space(5)
      .w_CATART=space(5)
      .w_SCOLIS=space(1)
      .w_DECUNI=0
      .w_CACODART=space(20)
      .w_MOLTI3=0
      .w_OPERA3=space(1)
      .w_FLUSEP=space(1)
      .w_FLFRAZ1=space(1)
      .w_MODUM2=space(2)
      .w_TIPATT=space(20)
      .w_TIPDOC=space(5)
      .w_GRUPAR=space(5)
      .w_MCAUDOC=space(5)
      .w_MTIPATT=space(20)
      .w_MGRUPAR=space(5)
      .w_VALLIS=space(3)
      .w_CATDOC=space(2)
      .w_TIPRIS=space(1)
      .w_CICLO=space(1)
      .w_LORNET=space(1)
      .w_FLSCOR=space(1)
      .w_DESPAG=space(30)
      .w_INILIS=ctod("  /  /  ")
      .w_FINLIS=ctod("  /  /  ")
      .w_QUALIS=space(1)
      .w_NUMLIS=space(5)
      .w_IVACLI=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_LIPREZZO=0
      .w_TIPSER=space(1)
      .w_ELPROLIS=space(5)
      .w_ELPROSCO=space(5)
      .w_FLGLIS=space(1)
      .w_FLINTE=space(1)
      .w_ARCODART=space(20)
      .w_NULLA=space(1)
      .w_DATALIST=ctod("  /  /  ")
      .w_ELSCOLIS=space(5)
      .w_ELTCOLIS=space(5)
      .w_KEYLISTB=space(10)
      .w_MVCODVAL=space(5)
      .w_FLVEAC=space(1)
      .w_FLRIPS=space(10)
      .w_GESTGUID=space(10)
      .w_ARUNMIS1=space(3)
      .w_ARUNMIS2=space(3)
      .w_MVFLVEAC=space(1)
      .w_CCODLIS=space(5)
      .w_MCODLIS=space(5)
      .w_ELRINNOV=0
      .w_ELESCRIN=space(1)
      .w_ELPERCON=space(3)
      .w_ELRINCON=space(1)
      .w_ELNUMGIO=0
      .w_ELDATDIS=ctod("  /  /  ")
      .w_DESATT=space(254)
      .w_DESDOC=space(35)
      .w_DESCTR=space(35)
      .w_ELVOCCEN=space(15)
      .w_ELCODCEN=space(15)
      .w_ELCOCOMM=space(15)
      .w_ELCODATT=space(15)
      .w_MCODCEN=space(15)
      .w_MCOCOMM=space(15)
      .w_CCOCOMM=space(15)
      .w_CCODCEN=space(15)
      .w_MOTIPCON=space(1)
      .w_MD__FREQ=space(1)
      .w_DPDESCRI=space(60)
      .w_ATTEXIST=.f.
      .w_DOCEXIST=.f.
      .w_FLSCOR=space(1)
      .w_CATCOM=space(3)
      .w_CT=space(1)
      .w_CC=space(15)
      .w_CM=space(3)
      .w_CI=ctod("  /  /  ")
      .w_CF=ctod("  /  /  ")
      .w_CV=space(3)
      .w_QUACON=space(1)
      .w_IVACON=space(1)
      .w_MDDESCRI=space(50)
      .w_ELINICOA=ctod("  /  /  ")
      .w_ELFINCOA=ctod("  /  /  ")
      .w_ELINICOD=ctod("  /  /  ")
      .w_ELFINCOD=ctod("  /  /  ")
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_ELQTACON=0
      .w_QTA__RES=0
      .w_PREZZTOT=0
      .w_OBTEST=ctod("  /  /  ")
      .w_RESCHK=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ELCODMOD))
         .link_2_1('Full')
        endif
        .DoRTCalc(2,4,.f.)
        if not(empty(.w_ELCODCLI))
         .link_2_4('Full')
        endif
        .w_CODICE = .w_ELCODCLI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ELCONTRA))
         .link_2_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        .w_ELCODART = .w_CODART
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_ELCODART))
         .link_2_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_ELUNIMIS))
         .link_2_9('Full')
        endif
        .w_ELQTAMOV = IIF(EMPTY(NVL(.w_ELCODART,'')) OR .w_TIPART='FO',1, .w_ELQTAMOV)
        .DoRTCalc(11,12,.f.)
        .w_ELCODVAL = g_PERVAL
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_ELCODVAL))
         .link_1_2('Full')
        endif
        .DoRTCalc(14,14,.f.)
        .w_TIPCON = 'C'
        .w_ELCODCOM = 0
        .DoRTCalc(17,27,.f.)
        .w_PRIMADATA = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'', IIF(.w_FLATTI<>'S','',IIF(.w_COPRIDAT='M',.w_PRIDAT,.w_COPRIDAT)))
        .w_PRIMADATADOC = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'',IIF(.w_FLATT<>'S','',IIF(.w_COPRIDOC='M',.w_PRIDOC,.w_COPRIDOC)))
        .DoRTCalc(30,31,.f.)
        .w_PATIPRIS = 'G'
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_31('Full')
        endif
        .DoRTCalc(34,48,.f.)
        if not(empty(.w_ELCONCOD))
         .link_2_45('Full')
        endif
        .DoRTCalc(49,55,.f.)
        if not(empty(.w_ELCATCON))
         .link_2_52('Full')
        endif
        .DoRTCalc(56,56,.f.)
        .w_ELCODLIS = ICASE(!EMPTY(NVL(.w_CCODLIS,'')),.w_CCODLIS,!EMPTY(NVL(.w_MCODLIS,'')),.w_MCODLIS,.w_ELCODLIS)
        .DoRTCalc(57,57,.f.)
        if not(empty(.w_ELCODLIS))
         .link_2_54('Full')
        endif
        .w_ELTIPATT = IIF(.w_FLATTI<>'S', SPACE(20), IIF(NOT EMPTY(.w_TIPATT), .w_TIPATT, .w_MTIPATT))
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_ELTIPATT))
         .link_2_55('Full')
        endif
        .w_ELGRUPAR = IIF(.w_FLATTI<>'S', SPACE(5), IIF(NOT EMPTY(.w_GRUPAR), .w_GRUPAR, .w_MGRUPAR))
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_ELGRUPAR))
         .link_2_56('Full')
        endif
        .w_ELDATDOC = IIF(.w_FLATT='S',.w_ELDATFIN+.w_ELGIODOC, CP_CHARTODATE('  -  -  '))
        .w_ELPERATT = IIF(.w_FLATTI<>'S' OR EMPTY(.w_ELTIPATT), SPACE(3), .w_PERATT)
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_ELPERATT))
         .link_2_58('Full')
        endif
        .w_ELDATATT = IIF(EMPTY(.w_ELTIPATT),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATA='I',.w_ELDATINI,IIF(.w_PRIMADATA='P' AND NOT EMPTY(.w_ELPERATT),NEXTTIME( .w_ELPERATT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATA='F',.w_ELDATFIN,.w_ELDATATT))))
        .w_ELCAUDOC = IIF(.w_FLATT<>'S',SPACE(5), IIF(NOT EMPTY(.w_TIPDOC), .w_TIPDOC, .w_MCAUDOC))
        .DoRTCalc(63,63,.f.)
        if not(empty(.w_ELCAUDOC))
         .link_2_60('Full')
        endif
        .DoRTCalc(64,64,.f.)
        .w_ELDATGAT = IIF(.w_FLATTI='S',.w_ELDATFIN+.w_ELGIOATT,CP_CHARTODATE('  -  -  '))
        .w_ELCODPAG = iif(.w_FLATT='S',IIF(NOT EMPTY(.w_CODPAG), .w_CODPAG, .w_ELCODPAG),Space(5))
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_ELCODPAG))
         .link_2_63('Full')
        endif
        .DoRTCalc(67,67,.f.)
        .w_ELPERFAT = IIF(.w_FLATT<>'S' or empty(.w_ELCAUDOC), SPACE(3), .w_PERFAT)
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_ELPERFAT))
         .link_2_65('Full')
        endif
        .DoRTCalc(69,69,.f.)
        .w_ELDATFAT = IIF(EMPTY(.w_ELCAUDOC),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATADOC='I',.w_ELDATINI,IIF(.w_PRIMADATADOC='P' AND NOT EMPTY(.w_ELPERFAT),NEXTTIME( .w_ELPERFAT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATADOC='F',.w_ELDATFIN,.w_ELDATFAT))))
        .DoRTCalc(71,77,.f.)
        .w_CACODART = .w_ELCODART
        .DoRTCalc(78,78,.f.)
        if not(empty(.w_CACODART))
         .link_2_75('Full')
        endif
        .DoRTCalc(79,92,.f.)
        .w_CICLO = 'E'
        .DoRTCalc(94,100,.f.)
        if not(empty(.w_NUMLIS))
         .link_1_17('Full')
        endif
        .DoRTCalc(101,101,.f.)
        .w_OB_TEST = IIF(EMPTY(.w_ELDATATT), i_INIDAT, .w_ELDATATT)
        .DoRTCalc(103,107,.f.)
        .w_FLGLIS = 'N'
        .DoRTCalc(109,109,.f.)
        .w_ARCODART = .w_ELCODART
        .w_NULLA = 'X'
        .w_DATALIST = IIF(Not empty(.w_ELDATFAT),.w_ELDATFAT,IIF(Empty(.w_ELDATATT),.w_ELDATINI,.w_ELDATATT))
        .DoRTCalc(113,114,.f.)
        .w_KEYLISTB = Sys(2015)
        .w_MVCODVAL = .w_ELCODVAL
        .DoRTCalc(117,119,.f.)
        .w_ARUNMIS1 = .w_UNMIS1
        .w_ARUNMIS2 = .w_UNMIS2
        .w_MVFLVEAC = 'V'
        .DoRTCalc(123,134,.f.)
        .w_ELCODCEN = IIF(NOT EMPTY(.w_CCODCEN), .w_CCODCEN, .w_MCODCEN)
        .w_ELCOCOMM = IIF(NOT EMPTY(.w_CCOCOMM), .w_CCOCOMM, .w_MCOCOMM)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(137,161,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
        .DoRTCalc(163,163,.f.)
        .w_QTA__RES = .w_ELQTAMOV - .w_ELQTACON
        .w_PREZZTOT = cp_round(.w_ELQTAMOV * (cp_round(.w_ELPREZZO *(100+.w_ELSCONT1)*(100+.w_ELSCONT2)*(100+.w_ELSCONT3)*(100+.w_ELSCONT4)/100000000  ,5) ),g_PERPVL)
        .w_OBTEST = I_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ELE_CONT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_110.enabled = this.oPgFrm.Page1.oPag.oBtn_2_110.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_112.enabled = this.oPgFrm.Page1.oPag.oBtn_2_112.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_116.enabled = this.oPgFrm.Page1.oPag.oBtn_2_116.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_117.enabled = this.oPgFrm.Page1.oPag.oBtn_2_117.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_121.enabled = this.oPgFrm.Page1.oPag.oBtn_2_121.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_141.enabled = this.oPgFrm.Page1.oPag.oBtn_2_141.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_142.enabled = this.oPgFrm.Page1.oPag.oBtn_2_142.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oELSCONT1_2_44.enabled = i_bVal
      .Page1.oPag.oELCONCOD_2_45.enabled = i_bVal
      .Page1.oPag.oELSCONT2_2_46.enabled = i_bVal
      .Page1.oPag.oELSCONT3_2_47.enabled = i_bVal
      .Page1.oPag.oELSCONT4_2_48.enabled = i_bVal
      .Page1.oPag.oELDATINI_2_50.enabled = i_bVal
      .Page1.oPag.oELDATFIN_2_51.enabled = i_bVal
      .Page1.oPag.oELCATCON_2_52.enabled = i_bVal
      .Page1.oPag.oELCODLIS_2_54.enabled = i_bVal
      .Page1.oPag.oELTIPATT_2_55.enabled = i_bVal
      .Page1.oPag.oELGRUPAR_2_56.enabled = i_bVal
      .Page1.oPag.oELPERATT_2_58.enabled = i_bVal
      .Page1.oPag.oELDATATT_2_59.enabled = i_bVal
      .Page1.oPag.oELCAUDOC_2_60.enabled = i_bVal
      .Page1.oPag.oELGIOATT_2_61.enabled = i_bVal
      .Page1.oPag.oELCODPAG_2_63.enabled = i_bVal
      .Page1.oPag.oELPERFAT_2_65.enabled = i_bVal
      .Page1.oPag.oELDATFAT_2_67.enabled = i_bVal
      .Page1.oPag.oELGIODOC_2_69.enabled = i_bVal
      .Page1.oPag.oPREZZTOT_2_162.enabled = i_bVal
      .Page1.oPag.oBtn_2_110.enabled = .Page1.oPag.oBtn_2_110.mCond()
      .Page1.oPag.oBtn_2_112.enabled = .Page1.oPag.oBtn_2_112.mCond()
      .Page1.oPag.oBtn_2_116.enabled = .Page1.oPag.oBtn_2_116.mCond()
      .Page1.oPag.oBtn_2_117.enabled = .Page1.oPag.oBtn_2_117.mCond()
      .Page1.oPag.oBtn_2_121.enabled = .Page1.oPag.oBtn_2_121.mCond()
      .Page1.oPag.oBtn_2_141.enabled = .Page1.oPag.oBtn_2_141.mCond()
      .Page1.oPag.oBtn_2_142.enabled = .Page1.oPag.oBtn_2_142.mCond()
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oObj_1_42.enabled = i_bVal
      .Page1.oPag.oObj_1_43.enabled = i_bVal
      .Page1.oPag.oObj_1_44.enabled = i_bVal
      .Page1.oPag.oObj_1_46.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ELE_CONT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODIMP,"ELCODIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODCOM,"ELCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELGESRAT,"ELGESRAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELPROLIS,"ELPROLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELPROSCO,"ELPROSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCOLIS,"ELSCOLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELTCOLIS,"ELTCOLIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ELCODMOD C(10);
      ,t_ELCONTRA C(10);
      ,t_ELCODART C(20);
      ,t_ELUNIMIS C(3);
      ,t_ELQTAMOV N(12,3);
      ,t_DESPER C(50);
      ,t_DESCAT C(60);
      ,t_ELPREZZO N(18,5);
      ,t_ELSCONT1 N(6,2);
      ,t_ELCONCOD C(15);
      ,t_ELSCONT2 N(6,2);
      ,t_ELSCONT3 N(6,2);
      ,t_ELSCONT4 N(6,2);
      ,t_DESCRIAT C(50);
      ,t_ELDATINI D(8);
      ,t_ELDATFIN D(8);
      ,t_ELCATCON C(5);
      ,t_ELCODLIS C(5);
      ,t_ELTIPATT C(20);
      ,t_ELGRUPAR C(5);
      ,t_ELDATDOC D(8);
      ,t_ELPERATT C(3);
      ,t_ELDATATT D(8);
      ,t_ELCAUDOC C(5);
      ,t_ELGIOATT N(4);
      ,t_ELDATGAT D(8);
      ,t_ELCODPAG C(5);
      ,t_DESLIS C(40);
      ,t_ELPERFAT C(3);
      ,t_ELDATFAT D(8);
      ,t_ELGIODOC N(4);
      ,t_DESPAG C(30);
      ,t_ELRINNOV N(6);
      ,t_DESATT C(254);
      ,t_DESDOC C(35);
      ,t_DESCTR C(35);
      ,t_DPDESCRI C(60);
      ,t_ELQTACON N(12,3);
      ,t_QTA__RES N(12,3);
      ,t_PREZZTOT N(18,5);
      ,ELCODMOD C(10);
      ,ELCONTRA C(10);
      ,ELRINNOV N(6);
      ,t_CODART C(20);
      ,t_CODICE C(15);
      ,t_CODCLI C(15);
      ,t_TIPART C(2);
      ,t_ELCODVAL C(3);
      ,t_UNIMIS C(5);
      ,t_TIPMOD C(1);
      ,t_PRIDAT C(1);
      ,t_PRIDOC C(1);
      ,t_COPRIDAT C(1);
      ,t_COPRIDOC C(1);
      ,t_PRIMADATA C(1);
      ,t_PRIMADATADOC C(1);
      ,t_DATINI D(8);
      ,t_DATFIN D(8);
      ,t_PATIPRIS C(1);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_UNMIS3 C(3);
      ,t_QTAMOV N(12,3);
      ,t_CODVAL C(3);
      ,t_FLSERG C(1);
      ,t_CARAGGST C(10);
      ,t_FLATT C(1);
      ,t_FLATTI C(1);
      ,t_CODPAG C(5);
      ,t_FLNSAP C(1);
      ,t_PREZUM C(1);
      ,t_MOLTIP N(10,4);
      ,t_CATCLI C(5);
      ,t_CATART C(5);
      ,t_SCOLIS C(1);
      ,t_DECUNI N(1);
      ,t_CACODART C(20);
      ,t_MOLTI3 N(10,4);
      ,t_OPERA3 C(1);
      ,t_FLUSEP C(1);
      ,t_FLFRAZ1 C(1);
      ,t_MODUM2 C(2);
      ,t_TIPATT C(20);
      ,t_TIPDOC C(5);
      ,t_GRUPAR C(5);
      ,t_MCAUDOC C(5);
      ,t_MTIPATT C(20);
      ,t_MGRUPAR C(5);
      ,t_VALLIS C(3);
      ,t_TIPRIS C(1);
      ,t_CICLO C(1);
      ,t_INILIS D(8);
      ,t_FINLIS D(8);
      ,t_QUALIS C(1);
      ,t_OB_TEST D(8);
      ,t_DATOBSO D(8);
      ,t_LIPREZZO N(18,5);
      ,t_TIPSER C(1);
      ,t_ARCODART C(20);
      ,t_DATALIST D(8);
      ,t_FLVEAC C(1);
      ,t_FLRIPS C(10);
      ,t_GESTGUID C(10);
      ,t_ARUNMIS1 C(3);
      ,t_ARUNMIS2 C(3);
      ,t_CCODLIS C(5);
      ,t_MCODLIS C(5);
      ,t_ELESCRIN C(1);
      ,t_ELPERCON C(3);
      ,t_ELRINCON C(1);
      ,t_ELNUMGIO N(3);
      ,t_ELDATDIS D(8);
      ,t_ELVOCCEN C(15);
      ,t_ELCODCEN C(15);
      ,t_ELCOCOMM C(15);
      ,t_ELCODATT C(15);
      ,t_MCODCEN C(15);
      ,t_MCOCOMM C(15);
      ,t_CCOCOMM C(15);
      ,t_CCODCEN C(15);
      ,t_MOTIPCON C(1);
      ,t_MD__FREQ C(1);
      ,t_ATTEXIST L(1);
      ,t_DOCEXIST L(1);
      ,t_CC C(15);
      ,t_CM C(3);
      ,t_CI D(8);
      ,t_CF D(8);
      ,t_CV C(3);
      ,t_QUACON C(1);
      ,t_IVACON C(1);
      ,t_MDDESCRI C(50);
      ,t_ELINICOA D(8);
      ,t_ELFINCOA D(8);
      ,t_ELINICOD D(8);
      ,t_ELFINCOD D(8);
      ,t_OBTEST D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mecbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oELCODMOD_2_1.controlsource=this.cTrsName+'.t_ELCODMOD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oELCONTRA_2_6.controlsource=this.cTrsName+'.t_ELCONTRA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oELCODART_2_8.controlsource=this.cTrsName+'.t_ELCODART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oELUNIMIS_2_9.controlsource=this.cTrsName+'.t_ELUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oELQTAMOV_2_10.controlsource=this.cTrsName+'.t_ELQTAMOV'
    this.oPgFRm.Page1.oPag.oDESPER_2_41.controlsource=this.cTrsName+'.t_DESPER'
    this.oPgFRm.Page1.oPag.oDESCAT_2_42.controlsource=this.cTrsName+'.t_DESCAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oELPREZZO_2_43.controlsource=this.cTrsName+'.t_ELPREZZO'
    this.oPgFRm.Page1.oPag.oELSCONT1_2_44.controlsource=this.cTrsName+'.t_ELSCONT1'
    this.oPgFRm.Page1.oPag.oELCONCOD_2_45.controlsource=this.cTrsName+'.t_ELCONCOD'
    this.oPgFRm.Page1.oPag.oELSCONT2_2_46.controlsource=this.cTrsName+'.t_ELSCONT2'
    this.oPgFRm.Page1.oPag.oELSCONT3_2_47.controlsource=this.cTrsName+'.t_ELSCONT3'
    this.oPgFRm.Page1.oPag.oELSCONT4_2_48.controlsource=this.cTrsName+'.t_ELSCONT4'
    this.oPgFRm.Page1.oPag.oDESCRIAT_2_49.controlsource=this.cTrsName+'.t_DESCRIAT'
    this.oPgFRm.Page1.oPag.oELDATINI_2_50.controlsource=this.cTrsName+'.t_ELDATINI'
    this.oPgFRm.Page1.oPag.oELDATFIN_2_51.controlsource=this.cTrsName+'.t_ELDATFIN'
    this.oPgFRm.Page1.oPag.oELCATCON_2_52.controlsource=this.cTrsName+'.t_ELCATCON'
    this.oPgFRm.Page1.oPag.oELCODLIS_2_54.controlsource=this.cTrsName+'.t_ELCODLIS'
    this.oPgFRm.Page1.oPag.oELTIPATT_2_55.controlsource=this.cTrsName+'.t_ELTIPATT'
    this.oPgFRm.Page1.oPag.oELGRUPAR_2_56.controlsource=this.cTrsName+'.t_ELGRUPAR'
    this.oPgFRm.Page1.oPag.oELDATDOC_2_57.controlsource=this.cTrsName+'.t_ELDATDOC'
    this.oPgFRm.Page1.oPag.oELPERATT_2_58.controlsource=this.cTrsName+'.t_ELPERATT'
    this.oPgFRm.Page1.oPag.oELDATATT_2_59.controlsource=this.cTrsName+'.t_ELDATATT'
    this.oPgFRm.Page1.oPag.oELCAUDOC_2_60.controlsource=this.cTrsName+'.t_ELCAUDOC'
    this.oPgFRm.Page1.oPag.oELGIOATT_2_61.controlsource=this.cTrsName+'.t_ELGIOATT'
    this.oPgFRm.Page1.oPag.oELDATGAT_2_62.controlsource=this.cTrsName+'.t_ELDATGAT'
    this.oPgFRm.Page1.oPag.oELCODPAG_2_63.controlsource=this.cTrsName+'.t_ELCODPAG'
    this.oPgFRm.Page1.oPag.oDESLIS_2_64.controlsource=this.cTrsName+'.t_DESLIS'
    this.oPgFRm.Page1.oPag.oELPERFAT_2_65.controlsource=this.cTrsName+'.t_ELPERFAT'
    this.oPgFRm.Page1.oPag.oELDATFAT_2_67.controlsource=this.cTrsName+'.t_ELDATFAT'
    this.oPgFRm.Page1.oPag.oELGIODOC_2_69.controlsource=this.cTrsName+'.t_ELGIODOC'
    this.oPgFRm.Page1.oPag.oDESPAG_2_92.controlsource=this.cTrsName+'.t_DESPAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oELRINNOV_2_120.controlsource=this.cTrsName+'.t_ELRINNOV'
    this.oPgFRm.Page1.oPag.oDESATT_2_127.controlsource=this.cTrsName+'.t_DESATT'
    this.oPgFRm.Page1.oPag.oDESDOC_2_128.controlsource=this.cTrsName+'.t_DESDOC'
    this.oPgFRm.Page1.oPag.oDESCTR_2_129.controlsource=this.cTrsName+'.t_DESCTR'
    this.oPgFRm.Page1.oPag.oDPDESCRI_2_140.controlsource=this.cTrsName+'.t_DPDESCRI'
    this.oPgFRm.Page1.oPag.oELQTACON_2_160.controlsource=this.cTrsName+'.t_ELQTACON'
    this.oPgFRm.Page1.oPag.oQTA__RES_2_161.controlsource=this.cTrsName+'.t_QTA__RES'
    this.oPgFRm.Page1.oPag.oPREZZTOT_2_162.controlsource=this.cTrsName+'.t_PREZZTOT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(92)
    this.AddVLine(184)
    this.AddVLine(337)
    this.AddVLine(386)
    this.AddVLine(485)
    this.AddVLine(627)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCODMOD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
      *
      * insert into ELE_CONT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ELE_CONT')
        i_extval=cp_InsertValODBCExtFlds(this,'ELE_CONT')
        i_cFldBody=" "+;
                  "(ELCODMOD,ELCONTRA,ELCODART,ELUNIMIS,ELQTAMOV"+;
                  ",ELCODVAL,ELCODIMP,ELCODCOM,ELGESRAT,ELPREZZO"+;
                  ",ELSCONT1,ELCONCOD,ELSCONT2,ELSCONT3,ELSCONT4"+;
                  ",ELDATINI,ELDATFIN,ELCATCON,ELCODLIS,ELTIPATT"+;
                  ",ELGRUPAR,ELDATDOC,ELPERATT,ELDATATT,ELCAUDOC"+;
                  ",ELGIOATT,ELDATGAT,ELCODPAG,ELPERFAT,ELDATFAT"+;
                  ",ELGIODOC,ELPROLIS,ELPROSCO,ELSCOLIS,ELTCOLIS"+;
                  ",ELRINNOV,ELESCRIN,ELPERCON,ELRINCON,ELNUMGIO"+;
                  ",ELDATDIS,ELVOCCEN,ELCODCEN,ELCOCOMM,ELCODATT"+;
                  ",ELINICOA,ELFINCOA,ELINICOD,ELFINCOD,ELQTACON,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_ELCODMOD)+","+cp_ToStrODBCNull(this.w_ELCONTRA)+","+cp_ToStrODBCNull(this.w_ELCODART)+","+cp_ToStrODBCNull(this.w_ELUNIMIS)+","+cp_ToStrODBC(this.w_ELQTAMOV)+;
             ","+cp_ToStrODBCNull(this.w_ELCODVAL)+","+cp_ToStrODBC(this.w_ELCODIMP)+","+cp_ToStrODBC(this.w_ELCODCOM)+","+cp_ToStrODBC(this.w_ELGESRAT)+","+cp_ToStrODBC(this.w_ELPREZZO)+;
             ","+cp_ToStrODBC(this.w_ELSCONT1)+","+cp_ToStrODBCNull(this.w_ELCONCOD)+","+cp_ToStrODBC(this.w_ELSCONT2)+","+cp_ToStrODBC(this.w_ELSCONT3)+","+cp_ToStrODBC(this.w_ELSCONT4)+;
             ","+cp_ToStrODBC(this.w_ELDATINI)+","+cp_ToStrODBC(this.w_ELDATFIN)+","+cp_ToStrODBCNull(this.w_ELCATCON)+","+cp_ToStrODBCNull(this.w_ELCODLIS)+","+cp_ToStrODBCNull(this.w_ELTIPATT)+;
             ","+cp_ToStrODBCNull(this.w_ELGRUPAR)+","+cp_ToStrODBC(this.w_ELDATDOC)+","+cp_ToStrODBCNull(this.w_ELPERATT)+","+cp_ToStrODBC(this.w_ELDATATT)+","+cp_ToStrODBCNull(this.w_ELCAUDOC)+;
             ","+cp_ToStrODBC(this.w_ELGIOATT)+","+cp_ToStrODBC(this.w_ELDATGAT)+","+cp_ToStrODBCNull(this.w_ELCODPAG)+","+cp_ToStrODBCNull(this.w_ELPERFAT)+","+cp_ToStrODBC(this.w_ELDATFAT)+;
             ","+cp_ToStrODBC(this.w_ELGIODOC)+","+cp_ToStrODBC(this.w_ELPROLIS)+","+cp_ToStrODBC(this.w_ELPROSCO)+","+cp_ToStrODBC(this.w_ELSCOLIS)+","+cp_ToStrODBC(this.w_ELTCOLIS)+;
             ","+cp_ToStrODBC(this.w_ELRINNOV)+","+cp_ToStrODBC(this.w_ELESCRIN)+","+cp_ToStrODBC(this.w_ELPERCON)+","+cp_ToStrODBC(this.w_ELRINCON)+","+cp_ToStrODBC(this.w_ELNUMGIO)+;
             ","+cp_ToStrODBC(this.w_ELDATDIS)+","+cp_ToStrODBC(this.w_ELVOCCEN)+","+cp_ToStrODBC(this.w_ELCODCEN)+","+cp_ToStrODBC(this.w_ELCOCOMM)+","+cp_ToStrODBC(this.w_ELCODATT)+;
             ","+cp_ToStrODBC(this.w_ELINICOA)+","+cp_ToStrODBC(this.w_ELFINCOA)+","+cp_ToStrODBC(this.w_ELINICOD)+","+cp_ToStrODBC(this.w_ELFINCOD)+","+cp_ToStrODBC(this.w_ELQTACON)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ELE_CONT')
        i_extval=cp_InsertValVFPExtFlds(this,'ELE_CONT')
        cp_CheckDeletedKey(i_cTable,0,'ELCODMOD',this.w_ELCODMOD,'ELCONTRA',this.w_ELCONTRA,'ELCODIMP',this.w_ELCODIMP,'ELCODCOM',this.w_ELCODCOM,'ELRINNOV',this.w_ELRINNOV)
        INSERT INTO (i_cTable) (;
                   ELCODMOD;
                  ,ELCONTRA;
                  ,ELCODART;
                  ,ELUNIMIS;
                  ,ELQTAMOV;
                  ,ELCODVAL;
                  ,ELCODIMP;
                  ,ELCODCOM;
                  ,ELGESRAT;
                  ,ELPREZZO;
                  ,ELSCONT1;
                  ,ELCONCOD;
                  ,ELSCONT2;
                  ,ELSCONT3;
                  ,ELSCONT4;
                  ,ELDATINI;
                  ,ELDATFIN;
                  ,ELCATCON;
                  ,ELCODLIS;
                  ,ELTIPATT;
                  ,ELGRUPAR;
                  ,ELDATDOC;
                  ,ELPERATT;
                  ,ELDATATT;
                  ,ELCAUDOC;
                  ,ELGIOATT;
                  ,ELDATGAT;
                  ,ELCODPAG;
                  ,ELPERFAT;
                  ,ELDATFAT;
                  ,ELGIODOC;
                  ,ELPROLIS;
                  ,ELPROSCO;
                  ,ELSCOLIS;
                  ,ELTCOLIS;
                  ,ELRINNOV;
                  ,ELESCRIN;
                  ,ELPERCON;
                  ,ELRINCON;
                  ,ELNUMGIO;
                  ,ELDATDIS;
                  ,ELVOCCEN;
                  ,ELCODCEN;
                  ,ELCOCOMM;
                  ,ELCODATT;
                  ,ELINICOA;
                  ,ELFINCOA;
                  ,ELINICOD;
                  ,ELFINCOD;
                  ,ELQTACON;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ELCODMOD;
                  ,this.w_ELCONTRA;
                  ,this.w_ELCODART;
                  ,this.w_ELUNIMIS;
                  ,this.w_ELQTAMOV;
                  ,this.w_ELCODVAL;
                  ,this.w_ELCODIMP;
                  ,this.w_ELCODCOM;
                  ,this.w_ELGESRAT;
                  ,this.w_ELPREZZO;
                  ,this.w_ELSCONT1;
                  ,this.w_ELCONCOD;
                  ,this.w_ELSCONT2;
                  ,this.w_ELSCONT3;
                  ,this.w_ELSCONT4;
                  ,this.w_ELDATINI;
                  ,this.w_ELDATFIN;
                  ,this.w_ELCATCON;
                  ,this.w_ELCODLIS;
                  ,this.w_ELTIPATT;
                  ,this.w_ELGRUPAR;
                  ,this.w_ELDATDOC;
                  ,this.w_ELPERATT;
                  ,this.w_ELDATATT;
                  ,this.w_ELCAUDOC;
                  ,this.w_ELGIOATT;
                  ,this.w_ELDATGAT;
                  ,this.w_ELCODPAG;
                  ,this.w_ELPERFAT;
                  ,this.w_ELDATFAT;
                  ,this.w_ELGIODOC;
                  ,this.w_ELPROLIS;
                  ,this.w_ELPROSCO;
                  ,this.w_ELSCOLIS;
                  ,this.w_ELTCOLIS;
                  ,this.w_ELRINNOV;
                  ,this.w_ELESCRIN;
                  ,this.w_ELPERCON;
                  ,this.w_ELRINCON;
                  ,this.w_ELNUMGIO;
                  ,this.w_ELDATDIS;
                  ,this.w_ELVOCCEN;
                  ,this.w_ELCODCEN;
                  ,this.w_ELCOCOMM;
                  ,this.w_ELCODATT;
                  ,this.w_ELINICOA;
                  ,this.w_ELFINCOA;
                  ,this.w_ELINICOD;
                  ,this.w_ELFINCOD;
                  ,this.w_ELQTACON;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_ELCODVAL<>t_ELCODVAL
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not empty(t_ELCONTRA) AND NOT EMPTY(t_ELCODMOD)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ELE_CONT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " ELGESRAT="+cp_ToStrODBC(this.w_ELGESRAT)+;
                 ",ELPROLIS="+cp_ToStrODBC(this.w_ELPROLIS)+;
                 ",ELPROSCO="+cp_ToStrODBC(this.w_ELPROSCO)+;
                 ",ELSCOLIS="+cp_ToStrODBC(this.w_ELSCOLIS)+;
                 ",ELTCOLIS="+cp_ToStrODBC(this.w_ELTCOLIS)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and ELCODMOD="+cp_ToStrODBC(&i_TN.->ELCODMOD)+;
                 " and ELCONTRA="+cp_ToStrODBC(&i_TN.->ELCONTRA)+;
                 " and ELRINNOV="+cp_ToStrODBC(&i_TN.->ELRINNOV)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ELE_CONT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  ELGESRAT=this.w_ELGESRAT;
                 ,ELPROLIS=this.w_ELPROLIS;
                 ,ELPROSCO=this.w_ELPROSCO;
                 ,ELSCOLIS=this.w_ELSCOLIS;
                 ,ELTCOLIS=this.w_ELTCOLIS;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and ELCODMOD=&i_TN.->ELCODMOD;
                      and ELCONTRA=&i_TN.->ELCONTRA;
                      and ELRINNOV=&i_TN.->ELRINNOV;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_ELCODVAL<>t_ELCODVAL
            i_bUpdAll = .t.
          endif
        endif
        scan for (not empty(t_ELCONTRA) AND NOT EMPTY(t_ELCODMOD)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and ELCODMOD="+cp_ToStrODBC(&i_TN.->ELCODMOD)+;
                            " and ELCONTRA="+cp_ToStrODBC(&i_TN.->ELCONTRA)+;
                            " and ELRINNOV="+cp_ToStrODBC(&i_TN.->ELRINNOV)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and ELCODMOD=&i_TN.->ELCODMOD;
                            and ELCONTRA=&i_TN.->ELCONTRA;
                            and ELRINNOV=&i_TN.->ELRINNOV;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace ELCODMOD with this.w_ELCODMOD
              replace ELCONTRA with this.w_ELCONTRA
              replace ELRINNOV with this.w_ELRINNOV
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ELE_CONT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ELE_CONT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ELCODART="+cp_ToStrODBCNull(this.w_ELCODART)+;
                     ",ELUNIMIS="+cp_ToStrODBCNull(this.w_ELUNIMIS)+;
                     ",ELQTAMOV="+cp_ToStrODBC(this.w_ELQTAMOV)+;
                     ",ELCODVAL="+cp_ToStrODBCNull(this.w_ELCODVAL)+;
                     ",ELGESRAT="+cp_ToStrODBC(this.w_ELGESRAT)+;
                     ",ELPREZZO="+cp_ToStrODBC(this.w_ELPREZZO)+;
                     ",ELSCONT1="+cp_ToStrODBC(this.w_ELSCONT1)+;
                     ",ELCONCOD="+cp_ToStrODBCNull(this.w_ELCONCOD)+;
                     ",ELSCONT2="+cp_ToStrODBC(this.w_ELSCONT2)+;
                     ",ELSCONT3="+cp_ToStrODBC(this.w_ELSCONT3)+;
                     ",ELSCONT4="+cp_ToStrODBC(this.w_ELSCONT4)+;
                     ",ELDATINI="+cp_ToStrODBC(this.w_ELDATINI)+;
                     ",ELDATFIN="+cp_ToStrODBC(this.w_ELDATFIN)+;
                     ",ELCATCON="+cp_ToStrODBCNull(this.w_ELCATCON)+;
                     ",ELCODLIS="+cp_ToStrODBCNull(this.w_ELCODLIS)+;
                     ",ELTIPATT="+cp_ToStrODBCNull(this.w_ELTIPATT)+;
                     ",ELGRUPAR="+cp_ToStrODBCNull(this.w_ELGRUPAR)+;
                     ",ELDATDOC="+cp_ToStrODBC(this.w_ELDATDOC)+;
                     ",ELPERATT="+cp_ToStrODBCNull(this.w_ELPERATT)+;
                     ",ELDATATT="+cp_ToStrODBC(this.w_ELDATATT)+;
                     ",ELCAUDOC="+cp_ToStrODBCNull(this.w_ELCAUDOC)+;
                     ",ELGIOATT="+cp_ToStrODBC(this.w_ELGIOATT)+;
                     ",ELDATGAT="+cp_ToStrODBC(this.w_ELDATGAT)+;
                     ",ELCODPAG="+cp_ToStrODBCNull(this.w_ELCODPAG)+;
                     ",ELPERFAT="+cp_ToStrODBCNull(this.w_ELPERFAT)+;
                     ",ELDATFAT="+cp_ToStrODBC(this.w_ELDATFAT)+;
                     ",ELGIODOC="+cp_ToStrODBC(this.w_ELGIODOC)+;
                     ",ELPROLIS="+cp_ToStrODBC(this.w_ELPROLIS)+;
                     ",ELPROSCO="+cp_ToStrODBC(this.w_ELPROSCO)+;
                     ",ELSCOLIS="+cp_ToStrODBC(this.w_ELSCOLIS)+;
                     ",ELTCOLIS="+cp_ToStrODBC(this.w_ELTCOLIS)+;
                     ",ELESCRIN="+cp_ToStrODBC(this.w_ELESCRIN)+;
                     ",ELPERCON="+cp_ToStrODBC(this.w_ELPERCON)+;
                     ",ELRINCON="+cp_ToStrODBC(this.w_ELRINCON)+;
                     ",ELNUMGIO="+cp_ToStrODBC(this.w_ELNUMGIO)+;
                     ",ELDATDIS="+cp_ToStrODBC(this.w_ELDATDIS)+;
                     ",ELVOCCEN="+cp_ToStrODBC(this.w_ELVOCCEN)+;
                     ",ELCODCEN="+cp_ToStrODBC(this.w_ELCODCEN)+;
                     ",ELCOCOMM="+cp_ToStrODBC(this.w_ELCOCOMM)+;
                     ",ELCODATT="+cp_ToStrODBC(this.w_ELCODATT)+;
                     ",ELINICOA="+cp_ToStrODBC(this.w_ELINICOA)+;
                     ",ELFINCOA="+cp_ToStrODBC(this.w_ELFINCOA)+;
                     ",ELINICOD="+cp_ToStrODBC(this.w_ELINICOD)+;
                     ",ELFINCOD="+cp_ToStrODBC(this.w_ELFINCOD)+;
                     ",ELQTACON="+cp_ToStrODBC(this.w_ELQTACON)+;
                     ",ELCODMOD="+cp_ToStrODBC(this.w_ELCODMOD)+;
                     ",ELCONTRA="+cp_ToStrODBC(this.w_ELCONTRA)+;
                     ",ELRINNOV="+cp_ToStrODBC(this.w_ELRINNOV)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and ELCODMOD="+cp_ToStrODBC(ELCODMOD)+;
                             " and ELCONTRA="+cp_ToStrODBC(ELCONTRA)+;
                             " and ELRINNOV="+cp_ToStrODBC(ELRINNOV)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ELE_CONT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ELCODART=this.w_ELCODART;
                     ,ELUNIMIS=this.w_ELUNIMIS;
                     ,ELQTAMOV=this.w_ELQTAMOV;
                     ,ELCODVAL=this.w_ELCODVAL;
                     ,ELGESRAT=this.w_ELGESRAT;
                     ,ELPREZZO=this.w_ELPREZZO;
                     ,ELSCONT1=this.w_ELSCONT1;
                     ,ELCONCOD=this.w_ELCONCOD;
                     ,ELSCONT2=this.w_ELSCONT2;
                     ,ELSCONT3=this.w_ELSCONT3;
                     ,ELSCONT4=this.w_ELSCONT4;
                     ,ELDATINI=this.w_ELDATINI;
                     ,ELDATFIN=this.w_ELDATFIN;
                     ,ELCATCON=this.w_ELCATCON;
                     ,ELCODLIS=this.w_ELCODLIS;
                     ,ELTIPATT=this.w_ELTIPATT;
                     ,ELGRUPAR=this.w_ELGRUPAR;
                     ,ELDATDOC=this.w_ELDATDOC;
                     ,ELPERATT=this.w_ELPERATT;
                     ,ELDATATT=this.w_ELDATATT;
                     ,ELCAUDOC=this.w_ELCAUDOC;
                     ,ELGIOATT=this.w_ELGIOATT;
                     ,ELDATGAT=this.w_ELDATGAT;
                     ,ELCODPAG=this.w_ELCODPAG;
                     ,ELPERFAT=this.w_ELPERFAT;
                     ,ELDATFAT=this.w_ELDATFAT;
                     ,ELGIODOC=this.w_ELGIODOC;
                     ,ELPROLIS=this.w_ELPROLIS;
                     ,ELPROSCO=this.w_ELPROSCO;
                     ,ELSCOLIS=this.w_ELSCOLIS;
                     ,ELTCOLIS=this.w_ELTCOLIS;
                     ,ELESCRIN=this.w_ELESCRIN;
                     ,ELPERCON=this.w_ELPERCON;
                     ,ELRINCON=this.w_ELRINCON;
                     ,ELNUMGIO=this.w_ELNUMGIO;
                     ,ELDATDIS=this.w_ELDATDIS;
                     ,ELVOCCEN=this.w_ELVOCCEN;
                     ,ELCODCEN=this.w_ELCODCEN;
                     ,ELCOCOMM=this.w_ELCOCOMM;
                     ,ELCODATT=this.w_ELCODATT;
                     ,ELINICOA=this.w_ELINICOA;
                     ,ELFINCOA=this.w_ELFINCOA;
                     ,ELINICOD=this.w_ELINICOD;
                     ,ELFINCOD=this.w_ELFINCOD;
                     ,ELQTACON=this.w_ELQTACON;
                     ,ELCODMOD=this.w_ELCODMOD;
                     ,ELCONTRA=this.w_ELCONTRA;
                     ,ELRINNOV=this.w_ELRINNOV;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and ELCODMOD=&i_TN.->ELCODMOD;
                                      and ELCONTRA=&i_TN.->ELCONTRA;
                                      and ELRINNOV=&i_TN.->ELRINNOV;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not empty(t_ELCONTRA) AND NOT EMPTY(t_ELCODMOD)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ELE_CONT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and ELCODMOD="+cp_ToStrODBC(&i_TN.->ELCODMOD)+;
                            " and ELCONTRA="+cp_ToStrODBC(&i_TN.->ELCONTRA)+;
                            " and ELRINNOV="+cp_ToStrODBC(&i_TN.->ELRINNOV)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and ELCODMOD=&i_TN.->ELCODMOD;
                              and ELCONTRA=&i_TN.->ELCONTRA;
                              and ELRINNOV=&i_TN.->ELRINNOV;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not empty(t_ELCONTRA) AND NOT EMPTY(t_ELCODMOD)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_ELCONTRA<>.w_ELCONTRA
          .link_2_4('Full')
        endif
          .w_CODICE = .w_ELCODCLI
        .DoRTCalc(6,7,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD
          .w_ELCODART = .w_CODART
          .link_2_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
        if .o_ELCODART<>.w_ELCODART.or. .o_TIPART<>.w_TIPART
          .w_ELQTAMOV = IIF(EMPTY(NVL(.w_ELCODART,'')) OR .w_TIPART='FO',1, .w_ELQTAMOV)
        endif
        .DoRTCalc(11,12,.t.)
          .link_1_2('Full')
        .DoRTCalc(14,14,.t.)
          .w_TIPCON = 'C'
        .DoRTCalc(16,27,.t.)
          .w_PRIMADATA = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'', IIF(.w_FLATTI<>'S','',IIF(.w_COPRIDAT='M',.w_PRIDAT,.w_COPRIDAT)))
          .w_PRIMADATADOC = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'',IIF(.w_FLATT<>'S','',IIF(.w_COPRIDOC='M',.w_PRIDOC,.w_COPRIDOC)))
        .DoRTCalc(30,32,.t.)
          .link_2_31('Full')
        .DoRTCalc(34,54,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD
          .link_2_52('Full')
        endif
        .DoRTCalc(56,56,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
          .w_ELCODLIS = ICASE(!EMPTY(NVL(.w_CCODLIS,'')),.w_CCODLIS,!EMPTY(NVL(.w_MCODLIS,'')),.w_MCODLIS,.w_ELCODLIS)
          .link_2_54('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
          .w_ELTIPATT = IIF(.w_FLATTI<>'S', SPACE(20), IIF(NOT EMPTY(.w_TIPATT), .w_TIPATT, .w_MTIPATT))
          .link_2_55('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
          .w_ELGRUPAR = IIF(.w_FLATTI<>'S', SPACE(5), IIF(NOT EMPTY(.w_GRUPAR), .w_GRUPAR, .w_MGRUPAR))
          .link_2_56('Full')
        endif
        if .o_ELGIODOC<>.w_ELGIODOC.or. .o_ELDATFIN<>.w_ELDATFIN
          .w_ELDATDOC = IIF(.w_FLATT='S',.w_ELDATFIN+.w_ELGIODOC, CP_CHARTODATE('  -  -  '))
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_FLATTI<>.w_FLATTI
          .w_ELPERATT = IIF(.w_FLATTI<>'S' OR EMPTY(.w_ELTIPATT), SPACE(3), .w_PERATT)
          .link_2_58('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_FLATTI<>.w_FLATTI.or. .o_ELPERATT<>.w_ELPERATT.or. .o_ELDATINI<>.w_ELDATINI.or. .o_ELDATFIN<>.w_ELDATFIN.or. .o_PRIMADATA<>.w_PRIMADATA
          .w_ELDATATT = IIF(EMPTY(.w_ELTIPATT),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATA='I',.w_ELDATINI,IIF(.w_PRIMADATA='P' AND NOT EMPTY(.w_ELPERATT),NEXTTIME( .w_ELPERATT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATA='F',.w_ELDATFIN,.w_ELDATATT))))
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
          .w_ELCAUDOC = IIF(.w_FLATT<>'S',SPACE(5), IIF(NOT EMPTY(.w_TIPDOC), .w_TIPDOC, .w_MCAUDOC))
          .link_2_60('Full')
        endif
        .DoRTCalc(64,64,.t.)
        if .o_ELGIOATT<>.w_ELGIOATT.or. .o_ELDATFIN<>.w_ELDATFIN
          .w_ELDATGAT = IIF(.w_FLATTI='S',.w_ELDATFIN+.w_ELGIOATT,CP_CHARTODATE('  -  -  '))
        endif
        if .o_ELCONTRA<>.w_ELCONTRA
          .w_ELCODPAG = iif(.w_FLATT='S',IIF(NOT EMPTY(.w_CODPAG), .w_CODPAG, .w_ELCODPAG),Space(5))
          .link_2_63('Full')
        endif
        .DoRTCalc(67,67,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_FLATT<>.w_FLATT
          .w_ELPERFAT = IIF(.w_FLATT<>'S' or empty(.w_ELCAUDOC), SPACE(3), .w_PERFAT)
          .link_2_65('Full')
        endif
        .DoRTCalc(69,69,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_FLATT<>.w_FLATT.or. .o_ELPERFAT<>.w_ELPERFAT.or. .o_ELDATINI<>.w_ELDATINI.or. .o_ELDATFIN<>.w_ELDATFIN.or. .o_PRIMADATADOC<>.w_PRIMADATADOC
          .w_ELDATFAT = IIF(EMPTY(.w_ELCAUDOC),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATADOC='I',.w_ELDATINI,IIF(.w_PRIMADATADOC='P' AND NOT EMPTY(.w_ELPERFAT),NEXTTIME( .w_ELPERFAT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATADOC='F',.w_ELDATFIN,.w_ELDATFAT))))
        endif
        .DoRTCalc(71,77,.t.)
          .w_CACODART = .w_ELCODART
          .link_2_75('Full')
        .DoRTCalc(79,99,.t.)
          .link_1_17('Full')
        .DoRTCalc(101,101,.t.)
        if .o_ELDATATT<>.w_ELDATATT
          .w_OB_TEST = IIF(EMPTY(.w_ELDATATT), i_INIDAT, .w_ELDATATT)
        endif
        .DoRTCalc(103,107,.t.)
          .w_FLGLIS = 'N'
        .DoRTCalc(109,109,.t.)
        if .o_ELCODART<>.w_ELCODART
          .w_ARCODART = .w_ELCODART
        endif
        if .o_ELCAUDOC<>.w_ELCAUDOC.or. .o_ELCODVAL<>.w_ELCODVAL.or. .o_ELCODCLI<>.w_ELCODCLI.or. .o_ELDATINI<>.w_ELDATINI
          .w_NULLA = 'X'
        endif
        if .o_ELDATFAT<>.w_ELDATFAT.or. .o_ELDATATT<>.w_ELDATATT
          .w_DATALIST = IIF(Not empty(.w_ELDATFAT),.w_ELDATFAT,IIF(Empty(.w_ELDATATT),.w_ELDATINI,.w_ELDATATT))
        endif
        .DoRTCalc(113,115,.t.)
        if .o_ELCODVAL<>.w_ELCODVAL
          .w_MVCODVAL = .w_ELCODVAL
        endif
        .DoRTCalc(117,119,.t.)
        if .o_ELCODART<>.w_ELCODART
          .w_ARUNMIS1 = .w_UNMIS1
        endif
        if .o_ELCODART<>.w_ELCODART
          .w_ARUNMIS2 = .w_UNMIS2
        endif
          .w_MVFLVEAC = 'V'
        .DoRTCalc(123,134,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
          .w_ELCODCEN = IIF(NOT EMPTY(.w_CCODCEN), .w_CCODCEN, .w_MCODCEN)
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
          .w_ELCOCOMM = IIF(NOT EMPTY(.w_CCOCOMM), .w_CCOCOMM, .w_MCOCOMM)
        endif
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(137,163,.t.)
          .w_QTA__RES = .w_ELQTAMOV - .w_ELQTACON
        if .o_ELQTAMOV<>.w_ELQTAMOV.or. .o_ELPREZZO<>.w_ELPREZZO.or. .o_ELSCONT1<>.w_ELSCONT1.or. .o_ELSCONT2<>.w_ELSCONT2.or. .o_ELSCONT3<>.w_ELSCONT3.or. .o_ELSCONT4<>.w_ELSCONT4
          .w_PREZZTOT = cp_round(.w_ELQTAMOV * (cp_round(.w_ELPREZZO *(100+.w_ELSCONT1)*(100+.w_ELSCONT2)*(100+.w_ELSCONT3)*(100+.w_ELSCONT4)/100000000  ,5) ),g_PERPVL)
        endif
          .w_OBTEST = I_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(167,167,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CODART with this.w_CODART
      replace t_CODART with this.w_CODART
      replace t_CODICE with this.w_CODICE
      replace t_CODCLI with this.w_CODCLI
      replace t_TIPART with this.w_TIPART
      replace t_ELCODVAL with this.w_ELCODVAL
      replace t_UNIMIS with this.w_UNIMIS
      replace t_TIPMOD with this.w_TIPMOD
      replace t_PRIDAT with this.w_PRIDAT
      replace t_PRIDOC with this.w_PRIDOC
      replace t_COPRIDAT with this.w_COPRIDAT
      replace t_COPRIDOC with this.w_COPRIDOC
      replace t_PRIMADATA with this.w_PRIMADATA
      replace t_PRIMADATADOC with this.w_PRIMADATADOC
      replace t_DATINI with this.w_DATINI
      replace t_DATFIN with this.w_DATFIN
      replace t_PATIPRIS with this.w_PATIPRIS
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_QTAMOV with this.w_QTAMOV
      replace t_CODVAL with this.w_CODVAL
      replace t_FLSERG with this.w_FLSERG
      replace t_CARAGGST with this.w_CARAGGST
      replace t_FLATT with this.w_FLATT
      replace t_FLATTI with this.w_FLATTI
      replace t_CODPAG with this.w_CODPAG
      replace t_FLNSAP with this.w_FLNSAP
      replace t_PREZUM with this.w_PREZUM
      replace t_MOLTIP with this.w_MOLTIP
      replace t_CATCLI with this.w_CATCLI
      replace t_CATART with this.w_CATART
      replace t_SCOLIS with this.w_SCOLIS
      replace t_DECUNI with this.w_DECUNI
      replace t_CACODART with this.w_CACODART
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_OPERA3 with this.w_OPERA3
      replace t_FLUSEP with this.w_FLUSEP
      replace t_FLFRAZ1 with this.w_FLFRAZ1
      replace t_MODUM2 with this.w_MODUM2
      replace t_TIPATT with this.w_TIPATT
      replace t_TIPDOC with this.w_TIPDOC
      replace t_GRUPAR with this.w_GRUPAR
      replace t_MCAUDOC with this.w_MCAUDOC
      replace t_MTIPATT with this.w_MTIPATT
      replace t_MGRUPAR with this.w_MGRUPAR
      replace t_VALLIS with this.w_VALLIS
      replace t_TIPRIS with this.w_TIPRIS
      replace t_CICLO with this.w_CICLO
      replace t_INILIS with this.w_INILIS
      replace t_FINLIS with this.w_FINLIS
      replace t_QUALIS with this.w_QUALIS
      replace t_OB_TEST with this.w_OB_TEST
      replace t_DATOBSO with this.w_DATOBSO
      replace t_LIPREZZO with this.w_LIPREZZO
      replace t_TIPSER with this.w_TIPSER
      replace t_ARCODART with this.w_ARCODART
      replace t_DATALIST with this.w_DATALIST
      replace t_FLVEAC with this.w_FLVEAC
      replace t_FLRIPS with this.w_FLRIPS
      replace t_GESTGUID with this.w_GESTGUID
      replace t_ARUNMIS1 with this.w_ARUNMIS1
      replace t_ARUNMIS2 with this.w_ARUNMIS2
      replace t_CCODLIS with this.w_CCODLIS
      replace t_MCODLIS with this.w_MCODLIS
      replace t_ELESCRIN with this.w_ELESCRIN
      replace t_ELPERCON with this.w_ELPERCON
      replace t_ELRINCON with this.w_ELRINCON
      replace t_ELNUMGIO with this.w_ELNUMGIO
      replace t_ELDATDIS with this.w_ELDATDIS
      replace t_ELVOCCEN with this.w_ELVOCCEN
      replace t_ELCODCEN with this.w_ELCODCEN
      replace t_ELCOCOMM with this.w_ELCOCOMM
      replace t_ELCODATT with this.w_ELCODATT
      replace t_MCODCEN with this.w_MCODCEN
      replace t_MCOCOMM with this.w_MCOCOMM
      replace t_CCOCOMM with this.w_CCOCOMM
      replace t_CCODCEN with this.w_CCODCEN
      replace t_MOTIPCON with this.w_MOTIPCON
      replace t_MD__FREQ with this.w_MD__FREQ
      replace t_ATTEXIST with this.w_ATTEXIST
      replace t_DOCEXIST with this.w_DOCEXIST
      replace t_CC with this.w_CC
      replace t_CM with this.w_CM
      replace t_CI with this.w_CI
      replace t_CF with this.w_CF
      replace t_CV with this.w_CV
      replace t_QUACON with this.w_QUACON
      replace t_IVACON with this.w_IVACON
      replace t_MDDESCRI with this.w_MDDESCRI
      replace t_ELINICOA with this.w_ELINICOA
      replace t_ELFINCOA with this.w_ELFINCOA
      replace t_ELINICOD with this.w_ELINICOD
      replace t_ELFINCOD with this.w_ELFINCOD
      replace t_OBTEST with this.w_OBTEST
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_MSKJAIWDSN()
    with this
          * --- Azzeramento sconti maggiorazioni al cambio prezzo totale
          .w_ELSCONT1 = 0
          .w_ELSCONT2 = 0
          .w_ELSCONT3 = 0
          .w_ELSCONT4 = 0
          .w_ELPREZZO = IIF(.w_ELQTAMOV<>0, .w_PREZZTOT / .w_ELQTAMOV, 0 )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oELCODART_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oELCODART_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oELUNIMIS_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oELUNIMIS_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oELQTAMOV_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oELQTAMOV_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oELPREZZO_2_43.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oELPREZZO_2_43.mCond()
    this.oPgFrm.Page1.oPag.oELSCONT1_2_44.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELSCONT1_2_44.mCond()
    this.oPgFrm.Page1.oPag.oELCONCOD_2_45.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELCONCOD_2_45.mCond()
    this.oPgFrm.Page1.oPag.oELSCONT2_2_46.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELSCONT2_2_46.mCond()
    this.oPgFrm.Page1.oPag.oELSCONT3_2_47.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELSCONT3_2_47.mCond()
    this.oPgFrm.Page1.oPag.oELSCONT4_2_48.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELSCONT4_2_48.mCond()
    this.oPgFrm.Page1.oPag.oELTIPATT_2_55.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELTIPATT_2_55.mCond()
    this.oPgFrm.Page1.oPag.oELGRUPAR_2_56.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELGRUPAR_2_56.mCond()
    this.oPgFrm.Page1.oPag.oELPERATT_2_58.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELPERATT_2_58.mCond()
    this.oPgFrm.Page1.oPag.oELDATATT_2_59.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELDATATT_2_59.mCond()
    this.oPgFrm.Page1.oPag.oELCAUDOC_2_60.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELCAUDOC_2_60.mCond()
    this.oPgFrm.Page1.oPag.oELGIOATT_2_61.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELGIOATT_2_61.mCond()
    this.oPgFrm.Page1.oPag.oELCODPAG_2_63.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELCODPAG_2_63.mCond()
    this.oPgFrm.Page1.oPag.oELPERFAT_2_65.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELPERFAT_2_65.mCond()
    this.oPgFrm.Page1.oPag.oELDATFAT_2_67.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELDATFAT_2_67.mCond()
    this.oPgFrm.Page1.oPag.oELGIODOC_2_69.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oELGIODOC_2_69.mCond()
    this.oPgFrm.Page1.oPag.oPREZZTOT_2_162.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPREZZTOT_2_162.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_112.enabled =this.oPgFrm.Page1.oPag.oBtn_2_112.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_116.enabled =this.oPgFrm.Page1.oPag.oBtn_2_116.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_141.enabled =this.oPgFrm.Page1.oPag.oBtn_2_141.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_142.enabled =this.oPgFrm.Page1.oPag.oBtn_2_142.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oELSCONT1_2_44.visible=!this.oPgFrm.Page1.oPag.oELSCONT1_2_44.mHide()
    this.oPgFrm.Page1.oPag.oELCONCOD_2_45.visible=!this.oPgFrm.Page1.oPag.oELCONCOD_2_45.mHide()
    this.oPgFrm.Page1.oPag.oELSCONT2_2_46.visible=!this.oPgFrm.Page1.oPag.oELSCONT2_2_46.mHide()
    this.oPgFrm.Page1.oPag.oELSCONT3_2_47.visible=!this.oPgFrm.Page1.oPag.oELSCONT3_2_47.mHide()
    this.oPgFrm.Page1.oPag.oELSCONT4_2_48.visible=!this.oPgFrm.Page1.oPag.oELSCONT4_2_48.mHide()
    this.oPgFrm.Page1.oPag.oELCODLIS_2_54.visible=!this.oPgFrm.Page1.oPag.oELCODLIS_2_54.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_2_64.visible=!this.oPgFrm.Page1.oPag.oDESLIS_2_64.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_110.visible=!this.oPgFrm.Page1.oPag.oBtn_2_110.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_112.visible=!this.oPgFrm.Page1.oPag.oBtn_2_112.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_116.visible=!this.oPgFrm.Page1.oPag.oBtn_2_116.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_117.visible=!this.oPgFrm.Page1.oPag.oBtn_2_117.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_121.visible=!this.oPgFrm.Page1.oPag.oBtn_2_121.mHide()
    this.oPgFrm.Page1.oPag.oDESCTR_2_129.visible=!this.oPgFrm.Page1.oPag.oDESCTR_2_129.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_141.visible=!this.oPgFrm.Page1.oPag.oBtn_2_141.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_142.visible=!this.oPgFrm.Page1.oPag.oBtn_2_142.mHide()
    this.oPgFrm.Page1.oPag.oELQTACON_2_160.visible=!this.oPgFrm.Page1.oPag.oELQTACON_2_160.mHide()
    this.oPgFrm.Page1.oPag.oQTA__RES_2_161.visible=!this.oPgFrm.Page1.oPag.oQTA__RES_2_161.mHide()
    this.oPgFrm.Page1.oPag.oPREZZTOT_2_162.visible=!this.oPgFrm.Page1.oPag.oPREZZTOT_2_162.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_mec
    If CEVENT='Aggio'
       This.w_ELTCOLIS=Space(5)
       This.w_ELSCOLIS=Space(5)
       This.w_ELPROLIS=Space(5)
       This.w_ELPROSCO=Space(5)
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Azzera")
          .Calculate_MSKJAIWDSN()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ELCODMOD
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AME',True,'MOD_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_ELCODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOUNIMIS,MOQTAMOV,MOCODVAL,MOPERATT,MOTIPATT,MOCAUDOC,MOGRUPAR,MOFLFATT,MOFLATTI,MOGIODOC,MOGIOATT,MOCODLIS,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOPRIDAT,MOPRIDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_ELCODMOD))
          select MOCODICE,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOUNIMIS,MOQTAMOV,MOCODVAL,MOPERATT,MOTIPATT,MOCAUDOC,MOGRUPAR,MOFLFATT,MOFLATTI,MOGIODOC,MOGIOATT,MOCODLIS,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOPRIDAT,MOPRIDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MOCATELE like "+cp_ToStrODBC(trim(this.w_ELCODMOD)+"%");

            i_ret=cp_SQL(i_nConn,"select MOCODICE,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOUNIMIS,MOQTAMOV,MOCODVAL,MOPERATT,MOTIPATT,MOCAUDOC,MOGRUPAR,MOFLFATT,MOFLATTI,MOGIODOC,MOGIOATT,MOCODLIS,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOPRIDAT,MOPRIDOC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MOCATELE like "+cp_ToStr(trim(this.w_ELCODMOD)+"%");

            select MOCODICE,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOUNIMIS,MOQTAMOV,MOCODVAL,MOPERATT,MOTIPATT,MOCAUDOC,MOGRUPAR,MOFLFATT,MOFLATTI,MOGIODOC,MOGIOATT,MOCODLIS,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOPRIDAT,MOPRIDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODMOD) and !this.bDontReportError
            deferred_cp_zoom('MOD_ELEM','*','MOCODICE',cp_AbsName(oSource.parent,'oELCODMOD_2_1'),i_cWhere,'GSAG_AME',"Modelli",'GSAG_KV3.MOD_ELEM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOUNIMIS,MOQTAMOV,MOCODVAL,MOPERATT,MOTIPATT,MOCAUDOC,MOGRUPAR,MOFLFATT,MOFLATTI,MOGIODOC,MOGIOATT,MOCODLIS,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOPRIDAT,MOPRIDOC";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOUNIMIS,MOQTAMOV,MOCODVAL,MOPERATT,MOTIPATT,MOCAUDOC,MOGRUPAR,MOFLFATT,MOFLATTI,MOGIODOC,MOGIOATT,MOCODLIS,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOPRIDAT,MOPRIDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOUNIMIS,MOQTAMOV,MOCODVAL,MOPERATT,MOTIPATT,MOCAUDOC,MOGRUPAR,MOFLFATT,MOFLATTI,MOGIODOC,MOGIOATT,MOCODLIS,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOPRIDAT,MOPRIDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_ELCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_ELCODMOD)
            select MOCODICE,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOUNIMIS,MOQTAMOV,MOCODVAL,MOPERATT,MOTIPATT,MOCAUDOC,MOGRUPAR,MOFLFATT,MOFLATTI,MOGIODOC,MOGIOATT,MOCODLIS,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOPRIDAT,MOPRIDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODMOD = NVL(_Link_.MOCODICE,space(10))
      this.w_ELCATCON = NVL(_Link_.MOCATELE,space(5))
      this.w_PERFAT = NVL(_Link_.MOPERFAT,space(3))
      this.w_ELGESRAT = NVL(_Link_.MOGESRAT,space(1))
      this.w_TIPMOD = NVL(_Link_.MOTIPAPL,space(1))
      this.w_CODART = NVL(_Link_.MOCODSER,space(20))
      this.w_ELUNIMIS = NVL(_Link_.MOUNIMIS,space(3))
      this.w_ELQTAMOV = NVL(_Link_.MOQTAMOV,0)
      this.w_ELCODVAL = NVL(_Link_.MOCODVAL,space(3))
      this.w_PERATT = NVL(_Link_.MOPERATT,space(3))
      this.w_MTIPATT = NVL(_Link_.MOTIPATT,space(20))
      this.w_MCAUDOC = NVL(_Link_.MOCAUDOC,space(5))
      this.w_MGRUPAR = NVL(_Link_.MOGRUPAR,space(5))
      this.w_FLATT = NVL(_Link_.MOFLFATT,space(1))
      this.w_FLATTI = NVL(_Link_.MOFLATTI,space(1))
      this.w_ELGIODOC = NVL(_Link_.MOGIODOC,0)
      this.w_ELGIOATT = NVL(_Link_.MOGIOATT,0)
      this.w_MCODLIS = NVL(_Link_.MOCODLIS,space(5))
      this.w_ELVOCCEN = NVL(_Link_.MOVOCCEN,space(15))
      this.w_MCODCEN = NVL(_Link_.MOCODCEN,space(15))
      this.w_MCOCOMM = NVL(_Link_.MOCOCOMM,space(15))
      this.w_ELCODATT = NVL(_Link_.MOCODATT,space(15))
      this.w_MOTIPCON = NVL(_Link_.MOTIPCON,space(1))
      this.w_PRIDAT = NVL(_Link_.MOPRIDAT,space(1))
      this.w_PRIDOC = NVL(_Link_.MOPRIDOC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODMOD = space(10)
      endif
      this.w_ELCATCON = space(5)
      this.w_PERFAT = space(3)
      this.w_ELGESRAT = space(1)
      this.w_TIPMOD = space(1)
      this.w_CODART = space(20)
      this.w_ELUNIMIS = space(3)
      this.w_ELQTAMOV = 0
      this.w_ELCODVAL = space(3)
      this.w_PERATT = space(3)
      this.w_MTIPATT = space(20)
      this.w_MCAUDOC = space(5)
      this.w_MGRUPAR = space(5)
      this.w_FLATT = space(1)
      this.w_FLATTI = space(1)
      this.w_ELGIODOC = 0
      this.w_ELGIOATT = 0
      this.w_MCODLIS = space(5)
      this.w_ELVOCCEN = space(15)
      this.w_MCODCEN = space(15)
      this.w_MCOCOMM = space(15)
      this.w_ELCODATT = space(15)
      this.w_MOTIPCON = space(1)
      this.w_PRIDAT = space(1)
      this.w_PRIDOC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 25 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MOD_ELEM_IDX,3] and i_nFlds+25<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.MOCODICE as MOCODICE201"+ ",link_2_1.MOCATELE as MOCATELE201"+ ",link_2_1.MOPERFAT as MOPERFAT201"+ ",link_2_1.MOGESRAT as MOGESRAT201"+ ",link_2_1.MOTIPAPL as MOTIPAPL201"+ ",link_2_1.MOCODSER as MOCODSER201"+ ",link_2_1.MOUNIMIS as MOUNIMIS201"+ ",link_2_1.MOQTAMOV as MOQTAMOV201"+ ",link_2_1.MOCODVAL as MOCODVAL201"+ ",link_2_1.MOPERATT as MOPERATT201"+ ",link_2_1.MOTIPATT as MOTIPATT201"+ ",link_2_1.MOCAUDOC as MOCAUDOC201"+ ",link_2_1.MOGRUPAR as MOGRUPAR201"+ ",link_2_1.MOFLFATT as MOFLFATT201"+ ",link_2_1.MOFLATTI as MOFLATTI201"+ ",link_2_1.MOGIODOC as MOGIODOC201"+ ",link_2_1.MOGIOATT as MOGIOATT201"+ ",link_2_1.MOCODLIS as MOCODLIS201"+ ",link_2_1.MOVOCCEN as MOVOCCEN201"+ ",link_2_1.MOCODCEN as MOCODCEN201"+ ",link_2_1.MOCOCOMM as MOCOCOMM201"+ ",link_2_1.MOCODATT as MOCODATT201"+ ",link_2_1.MOTIPCON as MOTIPCON201"+ ",link_2_1.MOPRIDAT as MOPRIDAT201"+ ",link_2_1.MOPRIDOC as MOPRIDOC201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on ELE_CONT.ELCODMOD=link_2_1.MOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+25
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODMOD=link_2_1.MOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+25
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODCLI
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCATCOM,ANSCORPO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ELCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_ELCODCLI)
            select ANTIPCON,ANCODICE,ANCATCOM,ANSCORPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_CATCOM = NVL(_Link_.ANCATCOM,space(3))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODCLI = space(15)
      endif
      this.w_CATCOM = space(3)
      this.w_FLSCOR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCONTRA
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACA',True,'CON_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COSERIAL like "+cp_ToStrODBC(trim(this.w_ELCONTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select COSERIAL,COCODCON,CODATINI,CODATFIN,COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COSERIAL',trim(this.w_ELCONTRA))
          select COSERIAL,COCODCON,CODATINI,CODATFIN,COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCONTRA)==trim(_Link_.COSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" COCODCON like "+cp_ToStrODBC(trim(this.w_ELCONTRA)+"%");

            i_ret=cp_SQL(i_nConn,"select COSERIAL,COCODCON,CODATINI,CODATFIN,COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" COCODCON like "+cp_ToStr(trim(this.w_ELCONTRA)+"%");

            select COSERIAL,COCODCON,CODATINI,CODATFIN,COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCONTRA) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAS','*','COSERIAL',cp_AbsName(oSource.parent,'oELCONTRA_2_6'),i_cWhere,'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,COCODCON,CODATINI,CODATFIN,COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC";
                     +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',oSource.xKey(1))
            select COSERIAL,COCODCON,CODATINI,CODATFIN,COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,COCODCON,CODATINI,CODATFIN,COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_ELCONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_ELCONTRA)
            select COSERIAL,COCODCON,CODATINI,CODATFIN,COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCONTRA = NVL(_Link_.COSERIAL,space(10))
      this.w_CODCLI = NVL(_Link_.COCODCON,space(15))
      this.w_ELDATINI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_ELDATFIN = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_DATINI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_TIPDOC = NVL(_Link_.COTIPDOC,space(5))
      this.w_TIPATT = NVL(_Link_.COTIPATT,space(20))
      this.w_GRUPAR = NVL(_Link_.COGRUPAR,space(5))
      this.w_CODPAG = NVL(_Link_.COCODPAG,space(5))
      this.w_CCODLIS = NVL(_Link_.COCODLIS,space(5))
      this.w_CCODCEN = NVL(_Link_.COCODCEN,space(15))
      this.w_CCOCOMM = NVL(_Link_.COCOCOMM,space(15))
      this.w_COPRIDAT = NVL(_Link_.COPRIDAT,space(1))
      this.w_COPRIDOC = NVL(_Link_.COPRIDOC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCONTRA = space(10)
      endif
      this.w_CODCLI = space(15)
      this.w_ELDATINI = ctod("  /  /  ")
      this.w_ELDATFIN = ctod("  /  /  ")
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
      this.w_TIPDOC = space(5)
      this.w_TIPATT = space(20)
      this.w_GRUPAR = space(5)
      this.w_CODPAG = space(5)
      this.w_CCODLIS = space(5)
      this.w_CCODCEN = space(15)
      this.w_CCOCOMM = space(15)
      this.w_COPRIDAT = space(1)
      this.w_COPRIDOC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=this.oparentobject.w_IMCODCON=.w_ELCODCLI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione: Intestatario del contratto incongruente!")
        endif
        this.w_ELCONTRA = space(10)
        this.w_CODCLI = space(15)
        this.w_ELDATINI = ctod("  /  /  ")
        this.w_ELDATFIN = ctod("  /  /  ")
        this.w_DATINI = ctod("  /  /  ")
        this.w_DATFIN = ctod("  /  /  ")
        this.w_TIPDOC = space(5)
        this.w_TIPATT = space(20)
        this.w_GRUPAR = space(5)
        this.w_CODPAG = space(5)
        this.w_CCODLIS = space(5)
        this.w_CCODCEN = space(15)
        this.w_CCOCOMM = space(15)
        this.w_COPRIDAT = space(1)
        this.w_COPRIDOC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 15 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CON_TRAS_IDX,3] and i_nFlds+15<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.COSERIAL as COSERIAL206"+ ",link_2_6.COCODCON as COCODCON206"+ ",link_2_6.CODATINI as CODATINI206"+ ",link_2_6.CODATFIN as CODATFIN206"+ ",link_2_6.CODATINI as CODATINI206"+ ",link_2_6.CODATFIN as CODATFIN206"+ ",link_2_6.COTIPDOC as COTIPDOC206"+ ",link_2_6.COTIPATT as COTIPATT206"+ ",link_2_6.COGRUPAR as COGRUPAR206"+ ",link_2_6.COCODPAG as COCODPAG206"+ ",link_2_6.COCODLIS as COCODLIS206"+ ",link_2_6.COCODCEN as COCODCEN206"+ ",link_2_6.COCOCOMM as COCOCOMM206"+ ",link_2_6.COPRIDAT as COPRIDAT206"+ ",link_2_6.COPRIDOC as COPRIDOC206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on ELE_CONT.ELCONTRA=link_2_6.COSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCONTRA=link_2_6.COSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODART
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ELCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ELCODART))
          select ARCODART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oELCODART_2_8'),i_cWhere,'GSMA_AAS',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ELCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ELCODART)
            select ARCODART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODART = NVL(_Link_.ARCODART,space(20))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_PREZUM = NVL(_Link_.ARPREZUM,space(1))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_CATART = NVL(_Link_.ARCATSCM,space(5))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_ELUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODART = space(20)
      endif
      this.w_TIPART = space(2)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_PREZUM = space(1)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_CATART = space(5)
      this.w_FLUSEP = space(1)
      this.w_ELUNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPART='FM' OR .w_TIPART='FO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELCODART = space(20)
        this.w_TIPART = space(2)
        this.w_UNMIS1 = space(3)
        this.w_UNMIS2 = space(3)
        this.w_FLSERG = space(1)
        this.w_PREZUM = space(1)
        this.w_OPERAT = space(1)
        this.w_MOLTIP = 0
        this.w_CATART = space(5)
        this.w_FLUSEP = space(1)
        this.w_ELUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 11 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+11<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.ARCODART as ARCODART208"+ ",link_2_8.ARTIPART as ARTIPART208"+ ",link_2_8.ARUNMIS1 as ARUNMIS1208"+ ",link_2_8.ARUNMIS2 as ARUNMIS2208"+ ",link_2_8.ARFLSERG as ARFLSERG208"+ ",link_2_8.ARPREZUM as ARPREZUM208"+ ",link_2_8.AROPERAT as AROPERAT208"+ ",link_2_8.ARMOLTIP as ARMOLTIP208"+ ",link_2_8.ARCATSCM as ARCATSCM208"+ ",link_2_8.ARFLUSEP as ARFLUSEP208"+ ",link_2_8.ARUNMIS1 as ARUNMIS1208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on ELE_CONT.ELCODART=link_2_8.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODART=link_2_8.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELUNIMIS
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ELUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ELUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oELUNIMIS_2_9'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ELUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ELUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ELUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_ELUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_ELUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODVAL
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_ELCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_ELCODVAL)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_ELCODVAL = space(3)
      endif
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.VACODVAL as VACODVAL102"+ ",link_1_2.VADECUNI as VADECUNI102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on ELE_CONT.ELCODVAL=link_1_2.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODVAL=link_1_2.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_FLFRAZ1 = space(1)
      this.w_MODUM2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCONCOD
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCONCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BCZ',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_ELCONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_TIPCON;
                     ,'CONUMERO',trim(this.w_ELCONCOD))
          select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCONCOD)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStrODBC(trim(this.w_ELCONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStr(trim(this.w_ELCONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStr(this.w_TIPCON);

            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCONCOD) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oELCONCOD_2_45'),i_cWhere,'GSAR_BCZ',"Elenco contratti",'GSAG_AEL.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCONCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_ELCONCOD);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_TIPCON;
                       ,'CONUMERO',this.w_ELCONCOD)
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCONCOD = NVL(_Link_.CONUMERO,space(15))
      this.w_DESCTR = NVL(_Link_.CODESCON,space(35))
      this.w_CT = NVL(_Link_.COTIPCLF,space(1))
      this.w_CC = NVL(_Link_.COCODCLF,space(15))
      this.w_CM = NVL(_Link_.COCATCOM,space(3))
      this.w_CI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CF = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_CV = NVL(_Link_.COCODVAL,space(3))
      this.w_QUACON = NVL(_Link_.COQUANTI,space(1))
      this.w_IVACON = NVL(_Link_.COIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCONCOD = space(15)
      endif
      this.w_DESCTR = space(35)
      this.w_CT = space(1)
      this.w_CC = space(15)
      this.w_CM = space(3)
      this.w_CI = ctod("  /  /  ")
      this.w_CF = ctod("  /  /  ")
      this.w_CV = space(3)
      this.w_QUACON = space(1)
      this.w_IVACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCONTR(.w_ELCONCOD,.w_TIPCON,.w_ELCODCLI,.w_CATCOM,.w_FLSCOR,.w_ELCODVAL, .w_DATALIST,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELCONCOD = space(15)
        this.w_DESCTR = space(35)
        this.w_CT = space(1)
        this.w_CC = space(15)
        this.w_CM = space(3)
        this.w_CI = ctod("  /  /  ")
        this.w_CF = ctod("  /  /  ")
        this.w_CV = space(3)
        this.w_QUACON = space(1)
        this.w_IVACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCONCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCATCON
  func Link_2_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ELEM_IDX,3]
    i_lTable = "CAT_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2], .t., this.CAT_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACE',True,'CAT_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODELE like "+cp_ToStrODBC(trim(this.w_ELCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODELE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODELE',trim(this.w_ELCATCON))
          select CECODELE,CEDESELE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODELE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCATCON)==trim(_Link_.CECODELE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESELE like "+cp_ToStrODBC(trim(this.w_ELCATCON)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESELE like "+cp_ToStr(trim(this.w_ELCATCON)+"%");

            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCATCON) and !this.bDontReportError
            deferred_cp_zoom('CAT_ELEM','*','CECODELE',cp_AbsName(oSource.parent,'oELCATCON_2_52'),i_cWhere,'GSAG_ACE',"Categorie contratti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                     +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',oSource.xKey(1))
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(this.w_ELCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',this.w_ELCATCON)
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCATCON = NVL(_Link_.CECODELE,space(5))
      this.w_DESCAT = NVL(_Link_.CEDESELE,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ELCATCON = space(5)
      endif
      this.w_DESCAT = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.CECODELE,1)
      cp_ShowWarn(i_cKey,this.CAT_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_52(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_ELEM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_52.CECODELE as CECODELE252"+ ",link_2_52.CEDESELE as CEDESELE252"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_52 on ELE_CONT.ELCATCON=link_2_52.CECODELE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_52"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCATCON=link_2_52.CECODELE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODLIS
  func Link_2_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ELCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ELCODLIS))
          select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oELCODLIS_2_54'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ELCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ELCODLIS)
            select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_SCOLIS = NVL(_Link_.LSFLSCON,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_LORNET = NVL(_Link_.LSIVALIS,space(1))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_QUALIS = NVL(_Link_.LSQUANTI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_SCOLIS = space(1)
      this.w_VALLIS = space(3)
      this.w_LORNET = space(1)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_QUALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( EMPTY(.w_ELCODMOD) OR EMPTY(.w_ELCONTRA), .T., CHKLISD(.w_ELCODLIS,.w_LORNET,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ELCODVAL, CTOD('  -  -  ')) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_SCOLIS = space(1)
        this.w_VALLIS = space(3)
        this.w_LORNET = space(1)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_QUALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_54(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_54.LSCODLIS as LSCODLIS254"+ ",link_2_54.LSDESLIS as LSDESLIS254"+ ",link_2_54.LSFLSCON as LSFLSCON254"+ ",link_2_54.LSVALLIS as LSVALLIS254"+ ",link_2_54.LSIVALIS as LSIVALIS254"+ ",link_2_54.LSDTINVA as LSDTINVA254"+ ",link_2_54.LSDTOBSO as LSDTOBSO254"+ ",link_2_54.LSQUANTI as LSQUANTI254"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_54 on ELE_CONT.ELCODLIS=link_2_54.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_54"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODLIS=link_2_54.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELTIPATT
  func Link_2_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELTIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ELTIPATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLNSAP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ELTIPATT))
          select CACODICE,CADESCRI,CAFLNSAP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELTIPATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELTIPATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oELTIPATT_2_55'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLNSAP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELTIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAFLNSAP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ELTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ELTIPATT)
            select CACODICE,CADESCRI,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELTIPATT = NVL(_Link_.CACODICE,space(20))
      this.w_DESATT = NVL(_Link_.CADESCRI,space(254))
      this.w_FLNSAP = NVL(_Link_.CAFLNSAP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELTIPATT = space(20)
      endif
      this.w_DESATT = space(254)
      this.w_FLNSAP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELTIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_55(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_55.CACODICE as CACODICE255"+ ",link_2_55.CADESCRI as CADESCRI255"+ ",link_2_55.CAFLNSAP as CAFLNSAP255"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_55 on ELE_CONT.ELTIPATT=link_2_55.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_55"
          i_cKey=i_cKey+'+" and ELE_CONT.ELTIPATT=link_2_55.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELGRUPAR
  func Link_2_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELGRUPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_ELGRUPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_ELGRUPAR))
          select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELGRUPAR)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELGRUPAR) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oELGRUPAR_2_56'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELGRUPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_ELGRUPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_ELGRUPAR)
            select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELGRUPAR = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
      this.w_DPDESCRI = NVL(_Link_.DPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ELGRUPAR = space(5)
      endif
      this.w_TIPRIS = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DPDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIS='G' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore incongruente o obsoleto")
        endif
        this.w_ELGRUPAR = space(5)
        this.w_TIPRIS = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DPDESCRI = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELGRUPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_56(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_56.DPCODICE as DPCODICE256"+ ",link_2_56.DPTIPRIS as DPTIPRIS256"+ ",link_2_56.DPDTOBSO as DPDTOBSO256"+ ",link_2_56.DPDESCRI as DPDESCRI256"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_56 on ELE_CONT.ELGRUPAR=link_2_56.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_56"
          i_cKey=i_cKey+'+" and ELE_CONT.ELGRUPAR=link_2_56.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELPERATT
  func Link_2_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELPERATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_ELPERATT)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_ELPERATT))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELPERATT)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELPERATT) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oELPERATT_2_58'),i_cWhere,'GSAR_AMD',"Metodi di calcolo periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELPERATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_ELPERATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_ELPERATT)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELPERATT = NVL(_Link_.MDCODICE,space(3))
      this.w_DESCRIAT = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ELPERATT = space(3)
      endif
      this.w_DESCRIAT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELPERATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_58(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_58.MDCODICE as MDCODICE258"+ ",link_2_58.MDDESCRI as MDDESCRI258"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_58 on ELE_CONT.ELPERATT=link_2_58.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_58"
          i_cKey=i_cKey+'+" and ELE_CONT.ELPERATT=link_2_58.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCAUDOC
  func Link_2_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_ELCAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_ELCAUDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oELCAUDOC_2_60'),i_cWhere,'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_ELCAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_ELCAUDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLRIPS = NVL(_Link_.TDFLRIPS,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ELCAUDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
      this.w_FLINTE = space(1)
      this.w_FLRIPS = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOC<>'OR' AND .w_FLVEAC='V' and (! Isahe() or .w_FLRIPS='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo")
        endif
        this.w_ELCAUDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
        this.w_FLINTE = space(1)
        this.w_FLRIPS = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_60(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_60.TDTIPDOC as TDTIPDOC260"+ ",link_2_60.TDDESDOC as TDDESDOC260"+ ",link_2_60.TDCATDOC as TDCATDOC260"+ ",link_2_60.TDFLVEAC as TDFLVEAC260"+ ",link_2_60.TDFLINTE as TDFLINTE260"+ ",link_2_60.TDFLRIPS as TDFLRIPS260"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_60 on ELE_CONT.ELCAUDOC=link_2_60.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_60"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCAUDOC=link_2_60.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODPAG
  func Link_2_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_ELCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_ELCODPAG))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oELCODPAG_2_63'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ELCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ELCODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(_Link_.PADESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_63(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_63.PACODICE as PACODICE263"+ ",link_2_63.PADESCRI as PADESCRI263"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_63 on ELE_CONT.ELCODPAG=link_2_63.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_63"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODPAG=link_2_63.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELPERFAT
  func Link_2_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELPERFAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_ELPERFAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_ELPERFAT))
          select MDCODICE,MDDESCRI,MD__FREQ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELPERFAT)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELPERFAT) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oELPERFAT_2_65'),i_cWhere,'GSAR_AMD',"Metodi di calcolo periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI,MD__FREQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELPERFAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_ELPERFAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_ELPERFAT)
            select MDCODICE,MDDESCRI,MD__FREQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELPERFAT = NVL(_Link_.MDCODICE,space(3))
      this.w_DESPER = NVL(_Link_.MDDESCRI,space(50))
      this.w_MD__FREQ = NVL(_Link_.MD__FREQ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELPERFAT = space(3)
      endif
      this.w_DESPER = space(50)
      this.w_MD__FREQ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELPERFAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_65(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_65.MDCODICE as MDCODICE265"+ ",link_2_65.MDDESCRI as MDDESCRI265"+ ",link_2_65.MD__FREQ as MD__FREQ265"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_65 on ELE_CONT.ELPERFAT=link_2_65.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_65"
          i_cKey=i_cKey+'+" and ELE_CONT.ELPERFAT=link_2_65.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODART
  func Link_2_75(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CAOPERAT,CAMOLTIP,CAUNIMIS,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODART)
            select CACODICE,CAOPERAT,CAMOLTIP,CAUNIMIS,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODART = NVL(_Link_.CACODICE,space(20))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_TIPSER = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODART = space(20)
      endif
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_UNMIS3 = space(3)
      this.w_TIPSER = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMLIS
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_NUMLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_NUMLIS)
            select LSCODLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_IVACLI = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NUMLIS = space(5)
      endif
      this.w_IVACLI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESPER_2_41.value==this.w_DESPER)
      this.oPgFrm.Page1.oPag.oDESPER_2_41.value=this.w_DESPER
      replace t_DESPER with this.oPgFrm.Page1.oPag.oDESPER_2_41.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_2_42.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_2_42.value=this.w_DESCAT
      replace t_DESCAT with this.oPgFrm.Page1.oPag.oDESCAT_2_42.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELSCONT1_2_44.value==this.w_ELSCONT1)
      this.oPgFrm.Page1.oPag.oELSCONT1_2_44.value=this.w_ELSCONT1
      replace t_ELSCONT1 with this.oPgFrm.Page1.oPag.oELSCONT1_2_44.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELCONCOD_2_45.value==this.w_ELCONCOD)
      this.oPgFrm.Page1.oPag.oELCONCOD_2_45.value=this.w_ELCONCOD
      replace t_ELCONCOD with this.oPgFrm.Page1.oPag.oELCONCOD_2_45.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELSCONT2_2_46.value==this.w_ELSCONT2)
      this.oPgFrm.Page1.oPag.oELSCONT2_2_46.value=this.w_ELSCONT2
      replace t_ELSCONT2 with this.oPgFrm.Page1.oPag.oELSCONT2_2_46.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELSCONT3_2_47.value==this.w_ELSCONT3)
      this.oPgFrm.Page1.oPag.oELSCONT3_2_47.value=this.w_ELSCONT3
      replace t_ELSCONT3 with this.oPgFrm.Page1.oPag.oELSCONT3_2_47.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELSCONT4_2_48.value==this.w_ELSCONT4)
      this.oPgFrm.Page1.oPag.oELSCONT4_2_48.value=this.w_ELSCONT4
      replace t_ELSCONT4 with this.oPgFrm.Page1.oPag.oELSCONT4_2_48.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIAT_2_49.value==this.w_DESCRIAT)
      this.oPgFrm.Page1.oPag.oDESCRIAT_2_49.value=this.w_DESCRIAT
      replace t_DESCRIAT with this.oPgFrm.Page1.oPag.oDESCRIAT_2_49.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATINI_2_50.value==this.w_ELDATINI)
      this.oPgFrm.Page1.oPag.oELDATINI_2_50.value=this.w_ELDATINI
      replace t_ELDATINI with this.oPgFrm.Page1.oPag.oELDATINI_2_50.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATFIN_2_51.value==this.w_ELDATFIN)
      this.oPgFrm.Page1.oPag.oELDATFIN_2_51.value=this.w_ELDATFIN
      replace t_ELDATFIN with this.oPgFrm.Page1.oPag.oELDATFIN_2_51.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELCATCON_2_52.value==this.w_ELCATCON)
      this.oPgFrm.Page1.oPag.oELCATCON_2_52.value=this.w_ELCATCON
      replace t_ELCATCON with this.oPgFrm.Page1.oPag.oELCATCON_2_52.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODLIS_2_54.value==this.w_ELCODLIS)
      this.oPgFrm.Page1.oPag.oELCODLIS_2_54.value=this.w_ELCODLIS
      replace t_ELCODLIS with this.oPgFrm.Page1.oPag.oELCODLIS_2_54.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELTIPATT_2_55.value==this.w_ELTIPATT)
      this.oPgFrm.Page1.oPag.oELTIPATT_2_55.value=this.w_ELTIPATT
      replace t_ELTIPATT with this.oPgFrm.Page1.oPag.oELTIPATT_2_55.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELGRUPAR_2_56.value==this.w_ELGRUPAR)
      this.oPgFrm.Page1.oPag.oELGRUPAR_2_56.value=this.w_ELGRUPAR
      replace t_ELGRUPAR with this.oPgFrm.Page1.oPag.oELGRUPAR_2_56.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATDOC_2_57.value==this.w_ELDATDOC)
      this.oPgFrm.Page1.oPag.oELDATDOC_2_57.value=this.w_ELDATDOC
      replace t_ELDATDOC with this.oPgFrm.Page1.oPag.oELDATDOC_2_57.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELPERATT_2_58.value==this.w_ELPERATT)
      this.oPgFrm.Page1.oPag.oELPERATT_2_58.value=this.w_ELPERATT
      replace t_ELPERATT with this.oPgFrm.Page1.oPag.oELPERATT_2_58.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATATT_2_59.value==this.w_ELDATATT)
      this.oPgFrm.Page1.oPag.oELDATATT_2_59.value=this.w_ELDATATT
      replace t_ELDATATT with this.oPgFrm.Page1.oPag.oELDATATT_2_59.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELCAUDOC_2_60.value==this.w_ELCAUDOC)
      this.oPgFrm.Page1.oPag.oELCAUDOC_2_60.value=this.w_ELCAUDOC
      replace t_ELCAUDOC with this.oPgFrm.Page1.oPag.oELCAUDOC_2_60.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELGIOATT_2_61.value==this.w_ELGIOATT)
      this.oPgFrm.Page1.oPag.oELGIOATT_2_61.value=this.w_ELGIOATT
      replace t_ELGIOATT with this.oPgFrm.Page1.oPag.oELGIOATT_2_61.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATGAT_2_62.value==this.w_ELDATGAT)
      this.oPgFrm.Page1.oPag.oELDATGAT_2_62.value=this.w_ELDATGAT
      replace t_ELDATGAT with this.oPgFrm.Page1.oPag.oELDATGAT_2_62.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODPAG_2_63.value==this.w_ELCODPAG)
      this.oPgFrm.Page1.oPag.oELCODPAG_2_63.value=this.w_ELCODPAG
      replace t_ELCODPAG with this.oPgFrm.Page1.oPag.oELCODPAG_2_63.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_2_64.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_2_64.value=this.w_DESLIS
      replace t_DESLIS with this.oPgFrm.Page1.oPag.oDESLIS_2_64.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELPERFAT_2_65.value==this.w_ELPERFAT)
      this.oPgFrm.Page1.oPag.oELPERFAT_2_65.value=this.w_ELPERFAT
      replace t_ELPERFAT with this.oPgFrm.Page1.oPag.oELPERFAT_2_65.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATFAT_2_67.value==this.w_ELDATFAT)
      this.oPgFrm.Page1.oPag.oELDATFAT_2_67.value=this.w_ELDATFAT
      replace t_ELDATFAT with this.oPgFrm.Page1.oPag.oELDATFAT_2_67.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELGIODOC_2_69.value==this.w_ELGIODOC)
      this.oPgFrm.Page1.oPag.oELGIODOC_2_69.value=this.w_ELGIODOC
      replace t_ELGIODOC with this.oPgFrm.Page1.oPag.oELGIODOC_2_69.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_2_92.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_2_92.value=this.w_DESPAG
      replace t_DESPAG with this.oPgFrm.Page1.oPag.oDESPAG_2_92.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_2_127.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_2_127.value=this.w_DESATT
      replace t_DESATT with this.oPgFrm.Page1.oPag.oDESATT_2_127.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_2_128.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_2_128.value=this.w_DESDOC
      replace t_DESDOC with this.oPgFrm.Page1.oPag.oDESDOC_2_128.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCTR_2_129.value==this.w_DESCTR)
      this.oPgFrm.Page1.oPag.oDESCTR_2_129.value=this.w_DESCTR
      replace t_DESCTR with this.oPgFrm.Page1.oPag.oDESCTR_2_129.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESCRI_2_140.value==this.w_DPDESCRI)
      this.oPgFrm.Page1.oPag.oDPDESCRI_2_140.value=this.w_DPDESCRI
      replace t_DPDESCRI with this.oPgFrm.Page1.oPag.oDPDESCRI_2_140.value
    endif
    if not(this.oPgFrm.Page1.oPag.oELQTACON_2_160.value==this.w_ELQTACON)
      this.oPgFrm.Page1.oPag.oELQTACON_2_160.value=this.w_ELQTACON
      replace t_ELQTACON with this.oPgFrm.Page1.oPag.oELQTACON_2_160.value
    endif
    if not(this.oPgFrm.Page1.oPag.oQTA__RES_2_161.value==this.w_QTA__RES)
      this.oPgFrm.Page1.oPag.oQTA__RES_2_161.value=this.w_QTA__RES
      replace t_QTA__RES with this.oPgFrm.Page1.oPag.oQTA__RES_2_161.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPREZZTOT_2_162.value==this.w_PREZZTOT)
      this.oPgFrm.Page1.oPag.oPREZZTOT_2_162.value=this.w_PREZZTOT
      replace t_PREZZTOT with this.oPgFrm.Page1.oPag.oPREZZTOT_2_162.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCODMOD_2_1.value==this.w_ELCODMOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCODMOD_2_1.value=this.w_ELCODMOD
      replace t_ELCODMOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCODMOD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCONTRA_2_6.value==this.w_ELCONTRA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCONTRA_2_6.value=this.w_ELCONTRA
      replace t_ELCONTRA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCONTRA_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCODART_2_8.value==this.w_ELCODART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCODART_2_8.value=this.w_ELCODART
      replace t_ELCODART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCODART_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELUNIMIS_2_9.value==this.w_ELUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELUNIMIS_2_9.value=this.w_ELUNIMIS
      replace t_ELUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELUNIMIS_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELQTAMOV_2_10.value==this.w_ELQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELQTAMOV_2_10.value=this.w_ELQTAMOV
      replace t_ELQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELQTAMOV_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELPREZZO_2_43.value==this.w_ELPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELPREZZO_2_43.value=this.w_ELPREZZO
      replace t_ELPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELPREZZO_2_43.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELRINNOV_2_120.value==this.w_ELRINNOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELRINNOV_2_120.value=this.w_ELRINNOV
      replace t_ELRINNOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELRINNOV_2_120.value
    endif
    cp_SetControlsValueExtFlds(this,'ELE_CONT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_mec
      IF i_bRes
         IF (this.w_FLSCOR='S' AND this.w_LORNET='N') 
           Ah_Errormsg('Attenzione.Intestatario con scorporo piede fattura, inserire listino di tipo "Iva inclusa (lordo)"',48,'')
           i_bRes=.f.
         ENDIF
      ENDIF
      * --- Controllo validit� data prossimo documento
      IF i_bRes
         IF .w_FLATT='S' 
           if EMPTY(.w_ELDATFAT)
           i_bRes=AH_YESNO("Attenzione: affinch� l'elemento sia considerato dal piano di generazione documenti%0occorre valorizzare la data prossimo documento!%0Confermare comunque?")
           else
              if .w_ELDATFAT>(.w_ELDATFIN + .w_ELGIODOC)
               i_bRes = .f.
               i_bnoChk = .f.
      	       i_cErrorMsg =Ah_MsgFormat("Data prossimo documento non valida")
              Endif
              if .w_ELDATFAT<.w_ELDATINI AND i_bRes = .t.
               i_bRes=AH_YESNO("Attenzione: la data prossimo documento � inferiore alla data inizio validit� dell'elemento contratto!%0Verranno generati documenti in base alla periodicit� %1 fino alla data %2!%0Procedo ugualmente?",,ALLTRIM(.w_DESPER),DTOC(.w_ELDATDOC))
              endif 
           Endif
         Endif 
      endif
      
      * --- Controllo validit� data prossima attivit�
      IF i_bRes
         IF .w_FLATTI='S'
           if EMPTY(.w_ELDATATT)
           i_bRes=AH_YESNO("Attenzione: affinch� l'elemento sia considerato dal piano di generazione attivit�%0occorre valorizzare la data prossima attivit�!%0Confermare comunque?")
           else
              if .w_ELDATATT>(.w_ELDATFIN + .w_ELGIOATT)
               i_bRes = .f.
               i_bnoChk = .f.
      	       i_cErrorMsg =Ah_MsgFormat("Data prossima attivit� non valida")
              Endif
              if .w_ELDATATT<.w_ELDATINI AND i_bRes = .t.
               i_bRes=AH_YESNO("Attenzione: la data prossima attivit� � inferiore alla data inizio validit� dell'elemento contratto!%0Verranno generate attivit� in base alla periodicit� %1 fino alla data %2!%0Procedo ugualmente?",,ALLTRIM(.w_DESCRIAT),DTOC(.w_ELDATGAT))
              endif 
           Endif
         Endif
      endif
      
      * --- Controllo validit� data limite disdetta
      IF i_bRes
        if .w_ELRINCON='S' and !(.w_ELDATDIS>.w_ELDATINI AND .w_ELDATDIS<=.w_ELDATFIN)
          i_bRes = .f.
          i_bnoChk = .f.
      	  i_cErrorMsg =Ah_MsgFormat("Data limite disdetta non compresa nell'intervallo di validit�")
        Endif
      Endif
      * --- Controllo congruita' date competenza
      IF i_bRes
        if ((.w_ELINICOA>.w_ELFINCOA)  or (empty(.w_ELINICOA) AND NOT empty(.w_ELFINCOA)) OR (NOT EMPTY(.w_ELINICOA) AND EMPTY(.w_ELFINCOA)))
          i_bRes = .f.
          i_bnoChk = .f.
      	  i_cErrorMsg =Ah_MsgFormat("Data inizio competenza attivit� maggiore della data finale o vuota")
        Endif
      Endif
      
      IF i_bRes
        if ((.w_ELINICOD>.w_ELFINCOD)  or (empty(.w_ELINICOD) AND NOT empty(.w_ELFINCOD)) OR (NOT EMPTY(.w_ELINICOD) AND EMPTY(.w_ELFINCOD)))
          i_bRes = .f.
          i_bnoChk = .f.
      	  i_cErrorMsg =Ah_MsgFormat("Data inizio competenza documenti maggiore della data finale o vuota")
        Endif
      Endif
      
      if i_bRes
         .w_RESCHK=0
           .NotifyEvent('CalcoliFinali')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(this.oparentobject.w_IMCODCON=.w_ELCODCLI) and not(empty(.w_ELCONTRA)) and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCONTRA_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione: Intestatario del contratto incongruente!")
        case   (empty(.w_ELCODART) or not(.w_TIPART='FM' OR .w_TIPART='FO')) and (.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N')) and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELCODART_2_8
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_ELUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_ELUNIMIS)) and ((.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N')) and .w_MOTIPCON<>'P') and not(empty(.w_ELUNIMIS)) and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELUNIMIS_2_9
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_ELQTAMOV>0) and (.w_TIPART<>'FO') and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oELQTAMOV_2_10
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire quantit� maggiore di zero")
        case   not(CHKCONTR(.w_ELCONCOD,.w_TIPCON,.w_ELCODCLI,.w_CATCOM,.w_FLSCOR,.w_ELCODVAL, .w_DATALIST,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON)) and (.w_TIPMOD<>'F' ) and not(empty(.w_ELCONCOD)) and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oELCONCOD_2_45
          i_bRes = .f.
          i_bnoChk = .f.
        case   (empty(.w_ELDATINI) or not(.w_ELDATINI>=.w_DATINI AND .w_ELDATINI<=.w_DATFIN)) and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oELDATINI_2_50
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Date non comprese nell'intervallo specificato in anagrafica contratti!")
        case   (empty(.w_ELDATFIN) or not((.w_ELDATFIN>=.w_DATINI AND .w_ELDATFIN<=.w_DATFIN) AND (.w_ELDATINI<=.w_ELDATFIN) or (empty(.w_ELDATINI)))) and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oELDATFIN_2_51
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Date non comprese nell'intervallo specificato in anagrafica contratti o data iniziale maggiore della data finale")
        case   not(IIF( EMPTY(.w_ELCODMOD) OR EMPTY(.w_ELCONTRA), .T., CHKLISD(.w_ELCODLIS,.w_LORNET,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ELCODVAL, CTOD('  -  -  ')) )) and not(empty(.w_ELCODLIS)) and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oELCODLIS_2_54
          i_bRes = .f.
          i_bnoChk = .f.
        case   empty(.w_ELTIPATT) and (.w_FLATTI='S' and .w_MOTIPCON<>'P') and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oELTIPATT_2_55
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_ELGRUPAR) or not(.w_TIPRIS='G' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST))) and (.w_FLATTI='S' and .w_MOTIPCON<>'P') and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oELGRUPAR_2_56
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore incongruente o obsoleto")
        case   empty(.w_ELPERATT) and (.w_FLATTI='S' and .w_MOTIPCON<>'P') and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oELPERATT_2_58
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_ELCAUDOC) or not(.w_CATDOC<>'OR' AND .w_FLVEAC='V' and (! Isahe() or .w_FLRIPS='S'))) and (.w_FLATT='S') and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oELCAUDOC_2_60
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo")
        case   empty(.w_ELPERFAT) and (.w_FLATT='S') and (not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD))
          .oNewFocus=.oPgFrm.Page1.oPag.oELPERFAT_2_65
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not empty(.w_ELCONTRA) AND NOT EMPTY(.w_ELCODMOD)
        * --- Area Manuale = Check Row
        * --- gsag_mec
        * --- Controllo congruenza intestatario impianto con elemento contratto
        IF i_bRes
           if .w_CODCLI<>.w_ELCODCLI
             i_bRes=AH_YESNO("Esistono uno o pi� elementi contratto che fanno riferimento a un intestatario diverso!%0Confermare comunque?")
           endif
        endif
        
        * --- Controllo validit� data prossimo documento
        IF i_bRes
           IF .w_FLATT='S' 
             if EMPTY(.w_ELDATFAT)
             i_bRes=AH_YESNO("Attenzione: affinch� l'elemento sia considerato dal piano di generazione documenti%0occorre valorizzare la data prossimo documento!%0Confermare comunque?")
             else
                if .w_ELDATFAT>(.w_ELDATFIN + .w_ELGIODOC)
                 i_bRes = .f.
                 i_bnoChk = .f.
        	       i_cErrorMsg =Ah_MsgFormat("Data prossimo documento non valida")
                Endif
             Endif
           Endif 
        endif
        
        * --- Controllo validit� data prossima attivit�
        IF i_bRes
           IF .w_FLATTI='S'
             if EMPTY(.w_ELDATATT)
             i_bRes=AH_YESNO("Attenzione: affinch� l'elemento sia considerato dal piano di generazione attivit�%0occorre valorizzare la data prossima attivit�!%0Confermare comunque?")
             else
                if .w_ELDATATT>(.w_ELDATFIN + .w_ELGIOATT)
                 i_bRes = .f.
                 i_bnoChk = .f.
        	       i_cErrorMsg =Ah_MsgFormat("Data prossima attivit� non valida")
                Endif 
             Endif
           Endif
        endif
        
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ELCODMOD = this.w_ELCODMOD
    this.o_ELCODCLI = this.w_ELCODCLI
    this.o_ELCONTRA = this.w_ELCONTRA
    this.o_ELCODART = this.w_ELCODART
    this.o_ELQTAMOV = this.w_ELQTAMOV
    this.o_TIPART = this.w_TIPART
    this.o_ELCODVAL = this.w_ELCODVAL
    this.o_PRIMADATA = this.w_PRIMADATA
    this.o_PRIMADATADOC = this.w_PRIMADATADOC
    this.o_FLATT = this.w_FLATT
    this.o_FLATTI = this.w_FLATTI
    this.o_ELPREZZO = this.w_ELPREZZO
    this.o_ELSCONT1 = this.w_ELSCONT1
    this.o_ELSCONT2 = this.w_ELSCONT2
    this.o_ELSCONT3 = this.w_ELSCONT3
    this.o_ELSCONT4 = this.w_ELSCONT4
    this.o_ELDATINI = this.w_ELDATINI
    this.o_ELDATFIN = this.w_ELDATFIN
    this.o_ELPERATT = this.w_ELPERATT
    this.o_ELDATATT = this.w_ELDATATT
    this.o_ELCAUDOC = this.w_ELCAUDOC
    this.o_ELGIOATT = this.w_ELGIOATT
    this.o_ELPERFAT = this.w_ELPERFAT
    this.o_ELDATFAT = this.w_ELDATFAT
    this.o_ELGIODOC = this.w_ELGIODOC
    this.o_ELESCRIN = this.w_ELESCRIN
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not empty(t_ELCONTRA) AND NOT EMPTY(t_ELCODMOD))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ELCODMOD=space(10)
      .w_CODART=space(20)
      .w_CODART=space(20)
      .w_CODICE=space(15)
      .w_ELCONTRA=space(10)
      .w_CODCLI=space(15)
      .w_ELCODART=space(20)
      .w_ELUNIMIS=space(3)
      .w_ELQTAMOV=0
      .w_TIPART=space(2)
      .w_ELCODVAL=space(3)
      .w_UNIMIS=space(5)
      .w_TIPMOD=space(1)
      .w_PRIDAT=space(1)
      .w_PRIDOC=space(1)
      .w_COPRIDAT=space(1)
      .w_COPRIDOC=space(1)
      .w_PRIMADATA=space(1)
      .w_PRIMADATADOC=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_PATIPRIS=space(1)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_QTAMOV=0
      .w_CODVAL=space(3)
      .w_FLSERG=space(1)
      .w_CARAGGST=space(10)
      .w_FLATT=space(1)
      .w_FLATTI=space(1)
      .w_CODPAG=space(5)
      .w_DESPER=space(50)
      .w_DESCAT=space(60)
      .w_ELPREZZO=0
      .w_ELSCONT1=0
      .w_ELCONCOD=space(15)
      .w_ELSCONT2=0
      .w_ELSCONT3=0
      .w_ELSCONT4=0
      .w_DESCRIAT=space(50)
      .w_ELDATINI=ctod("  /  /  ")
      .w_ELDATFIN=ctod("  /  /  ")
      .w_ELCATCON=space(5)
      .w_FLNSAP=space(1)
      .w_ELCODLIS=space(5)
      .w_ELTIPATT=space(20)
      .w_ELGRUPAR=space(5)
      .w_ELDATDOC=ctod("  /  /  ")
      .w_ELPERATT=space(3)
      .w_ELDATATT=ctod("  /  /  ")
      .w_ELCAUDOC=space(5)
      .w_ELGIOATT=0
      .w_ELDATGAT=ctod("  /  /  ")
      .w_ELCODPAG=space(5)
      .w_DESLIS=space(40)
      .w_ELPERFAT=space(3)
      .w_PREZUM=space(1)
      .w_ELDATFAT=ctod("  /  /  ")
      .w_ELGIODOC=0
      .w_MOLTIP=0
      .w_CATCLI=space(5)
      .w_CATART=space(5)
      .w_SCOLIS=space(1)
      .w_DECUNI=0
      .w_CACODART=space(20)
      .w_MOLTI3=0
      .w_OPERA3=space(1)
      .w_FLUSEP=space(1)
      .w_FLFRAZ1=space(1)
      .w_MODUM2=space(2)
      .w_TIPATT=space(20)
      .w_TIPDOC=space(5)
      .w_GRUPAR=space(5)
      .w_MCAUDOC=space(5)
      .w_MTIPATT=space(20)
      .w_MGRUPAR=space(5)
      .w_VALLIS=space(3)
      .w_TIPRIS=space(1)
      .w_CICLO=space(1)
      .w_DESPAG=space(30)
      .w_INILIS=ctod("  /  /  ")
      .w_FINLIS=ctod("  /  /  ")
      .w_QUALIS=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_LIPREZZO=0
      .w_TIPSER=space(1)
      .w_ARCODART=space(20)
      .w_DATALIST=ctod("  /  /  ")
      .w_FLVEAC=space(1)
      .w_FLRIPS=space(10)
      .w_GESTGUID=space(10)
      .w_ARUNMIS1=space(3)
      .w_ARUNMIS2=space(3)
      .w_CCODLIS=space(5)
      .w_MCODLIS=space(5)
      .w_ELRINNOV=0
      .w_ELESCRIN=space(1)
      .w_ELPERCON=space(3)
      .w_ELRINCON=space(1)
      .w_ELNUMGIO=0
      .w_ELDATDIS=ctod("  /  /  ")
      .w_DESATT=space(254)
      .w_DESDOC=space(35)
      .w_DESCTR=space(35)
      .w_ELVOCCEN=space(15)
      .w_ELCODCEN=space(15)
      .w_ELCOCOMM=space(15)
      .w_ELCODATT=space(15)
      .w_MCODCEN=space(15)
      .w_MCOCOMM=space(15)
      .w_CCOCOMM=space(15)
      .w_CCODCEN=space(15)
      .w_MOTIPCON=space(1)
      .w_MD__FREQ=space(1)
      .w_DPDESCRI=space(60)
      .w_ATTEXIST=.f.
      .w_DOCEXIST=.f.
      .w_CC=space(15)
      .w_CM=space(3)
      .w_CI=ctod("  /  /  ")
      .w_CF=ctod("  /  /  ")
      .w_CV=space(3)
      .w_QUACON=space(1)
      .w_IVACON=space(1)
      .w_MDDESCRI=space(50)
      .w_ELINICOA=ctod("  /  /  ")
      .w_ELFINCOA=ctod("  /  /  ")
      .w_ELINICOD=ctod("  /  /  ")
      .w_ELFINCOD=ctod("  /  /  ")
      .w_ELQTACON=0
      .w_QTA__RES=0
      .w_PREZZTOT=0
      .w_OBTEST=ctod("  /  /  ")
      .DoRTCalc(1,1,.f.)
      if not(empty(.w_ELCODMOD))
        .link_2_1('Full')
      endif
      .DoRTCalc(2,4,.f.)
        .w_CODICE = .w_ELCODCLI
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_ELCONTRA))
        .link_2_6('Full')
      endif
      .DoRTCalc(7,7,.f.)
        .w_ELCODART = .w_CODART
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_ELCODART))
        .link_2_8('Full')
      endif
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_ELUNIMIS))
        .link_2_9('Full')
      endif
        .w_ELQTAMOV = IIF(EMPTY(NVL(.w_ELCODART,'')) OR .w_TIPART='FO',1, .w_ELQTAMOV)
      .DoRTCalc(11,12,.f.)
        .w_ELCODVAL = g_PERVAL
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_ELCODVAL))
        .link_1_2('Full')
      endif
      .DoRTCalc(14,27,.f.)
        .w_PRIMADATA = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'', IIF(.w_FLATTI<>'S','',IIF(.w_COPRIDAT='M',.w_PRIDAT,.w_COPRIDAT)))
        .w_PRIMADATADOC = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'',IIF(.w_FLATT<>'S','',IIF(.w_COPRIDOC='M',.w_PRIDOC,.w_COPRIDOC)))
      .DoRTCalc(30,31,.f.)
        .w_PATIPRIS = 'G'
      .DoRTCalc(33,33,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_31('Full')
      endif
      .DoRTCalc(34,48,.f.)
      if not(empty(.w_ELCONCOD))
        .link_2_45('Full')
      endif
      .DoRTCalc(49,55,.f.)
      if not(empty(.w_ELCATCON))
        .link_2_52('Full')
      endif
      .DoRTCalc(56,56,.f.)
        .w_ELCODLIS = ICASE(!EMPTY(NVL(.w_CCODLIS,'')),.w_CCODLIS,!EMPTY(NVL(.w_MCODLIS,'')),.w_MCODLIS,.w_ELCODLIS)
      .DoRTCalc(57,57,.f.)
      if not(empty(.w_ELCODLIS))
        .link_2_54('Full')
      endif
        .w_ELTIPATT = IIF(.w_FLATTI<>'S', SPACE(20), IIF(NOT EMPTY(.w_TIPATT), .w_TIPATT, .w_MTIPATT))
      .DoRTCalc(58,58,.f.)
      if not(empty(.w_ELTIPATT))
        .link_2_55('Full')
      endif
        .w_ELGRUPAR = IIF(.w_FLATTI<>'S', SPACE(5), IIF(NOT EMPTY(.w_GRUPAR), .w_GRUPAR, .w_MGRUPAR))
      .DoRTCalc(59,59,.f.)
      if not(empty(.w_ELGRUPAR))
        .link_2_56('Full')
      endif
        .w_ELDATDOC = IIF(.w_FLATT='S',.w_ELDATFIN+.w_ELGIODOC, CP_CHARTODATE('  -  -  '))
        .w_ELPERATT = IIF(.w_FLATTI<>'S' OR EMPTY(.w_ELTIPATT), SPACE(3), .w_PERATT)
      .DoRTCalc(61,61,.f.)
      if not(empty(.w_ELPERATT))
        .link_2_58('Full')
      endif
        .w_ELDATATT = IIF(EMPTY(.w_ELTIPATT),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATA='I',.w_ELDATINI,IIF(.w_PRIMADATA='P' AND NOT EMPTY(.w_ELPERATT),NEXTTIME( .w_ELPERATT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATA='F',.w_ELDATFIN,.w_ELDATATT))))
        .w_ELCAUDOC = IIF(.w_FLATT<>'S',SPACE(5), IIF(NOT EMPTY(.w_TIPDOC), .w_TIPDOC, .w_MCAUDOC))
      .DoRTCalc(63,63,.f.)
      if not(empty(.w_ELCAUDOC))
        .link_2_60('Full')
      endif
      .DoRTCalc(64,64,.f.)
        .w_ELDATGAT = IIF(.w_FLATTI='S',.w_ELDATFIN+.w_ELGIOATT,CP_CHARTODATE('  -  -  '))
        .w_ELCODPAG = iif(.w_FLATT='S',IIF(NOT EMPTY(.w_CODPAG), .w_CODPAG, .w_ELCODPAG),Space(5))
      .DoRTCalc(66,66,.f.)
      if not(empty(.w_ELCODPAG))
        .link_2_63('Full')
      endif
      .DoRTCalc(67,67,.f.)
        .w_ELPERFAT = IIF(.w_FLATT<>'S' or empty(.w_ELCAUDOC), SPACE(3), .w_PERFAT)
      .DoRTCalc(68,68,.f.)
      if not(empty(.w_ELPERFAT))
        .link_2_65('Full')
      endif
      .DoRTCalc(69,69,.f.)
        .w_ELDATFAT = IIF(EMPTY(.w_ELCAUDOC),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATADOC='I',.w_ELDATINI,IIF(.w_PRIMADATADOC='P' AND NOT EMPTY(.w_ELPERFAT),NEXTTIME( .w_ELPERFAT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATADOC='F',.w_ELDATFIN,.w_ELDATFAT))))
      .DoRTCalc(71,77,.f.)
        .w_CACODART = .w_ELCODART
      .DoRTCalc(78,78,.f.)
      if not(empty(.w_CACODART))
        .link_2_75('Full')
      endif
      .DoRTCalc(79,92,.f.)
        .w_CICLO = 'E'
      .DoRTCalc(94,101,.f.)
        .w_OB_TEST = IIF(EMPTY(.w_ELDATATT), i_INIDAT, .w_ELDATATT)
      .DoRTCalc(103,109,.f.)
        .w_ARCODART = .w_ELCODART
      .DoRTCalc(111,111,.f.)
        .w_DATALIST = IIF(Not empty(.w_ELDATFAT),.w_ELDATFAT,IIF(Empty(.w_ELDATATT),.w_ELDATINI,.w_ELDATATT))
      .DoRTCalc(113,119,.f.)
        .w_ARUNMIS1 = .w_UNMIS1
        .w_ARUNMIS2 = .w_UNMIS2
      .DoRTCalc(122,134,.f.)
        .w_ELCODCEN = IIF(NOT EMPTY(.w_CCODCEN), .w_CCODCEN, .w_MCODCEN)
        .w_ELCOCOMM = IIF(NOT EMPTY(.w_CCOCOMM), .w_CCOCOMM, .w_MCOCOMM)
      .DoRTCalc(137,163,.f.)
        .w_QTA__RES = .w_ELQTAMOV - .w_ELQTACON
        .w_PREZZTOT = cp_round(.w_ELQTAMOV * (cp_round(.w_ELPREZZO *(100+.w_ELSCONT1)*(100+.w_ELSCONT2)*(100+.w_ELSCONT3)*(100+.w_ELSCONT4)/100000000  ,5) ),g_PERPVL)
        .w_OBTEST = I_DATSYS
    endwith
    this.DoRTCalc(167,167,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ELCODMOD = t_ELCODMOD
    this.w_CODART = t_CODART
    this.w_CODART = t_CODART
    this.w_CODICE = t_CODICE
    this.w_ELCONTRA = t_ELCONTRA
    this.w_CODCLI = t_CODCLI
    this.w_ELCODART = t_ELCODART
    this.w_ELUNIMIS = t_ELUNIMIS
    this.w_ELQTAMOV = t_ELQTAMOV
    this.w_TIPART = t_TIPART
    this.w_ELCODVAL = t_ELCODVAL
    this.w_UNIMIS = t_UNIMIS
    this.w_TIPMOD = t_TIPMOD
    this.w_PRIDAT = t_PRIDAT
    this.w_PRIDOC = t_PRIDOC
    this.w_COPRIDAT = t_COPRIDAT
    this.w_COPRIDOC = t_COPRIDOC
    this.w_PRIMADATA = t_PRIMADATA
    this.w_PRIMADATADOC = t_PRIMADATADOC
    this.w_DATINI = t_DATINI
    this.w_DATFIN = t_DATFIN
    this.w_PATIPRIS = t_PATIPRIS
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_UNMIS3 = t_UNMIS3
    this.w_QTAMOV = t_QTAMOV
    this.w_CODVAL = t_CODVAL
    this.w_FLSERG = t_FLSERG
    this.w_CARAGGST = t_CARAGGST
    this.w_FLATT = t_FLATT
    this.w_FLATTI = t_FLATTI
    this.w_CODPAG = t_CODPAG
    this.w_DESPER = t_DESPER
    this.w_DESCAT = t_DESCAT
    this.w_ELPREZZO = t_ELPREZZO
    this.w_ELSCONT1 = t_ELSCONT1
    this.w_ELCONCOD = t_ELCONCOD
    this.w_ELSCONT2 = t_ELSCONT2
    this.w_ELSCONT3 = t_ELSCONT3
    this.w_ELSCONT4 = t_ELSCONT4
    this.w_DESCRIAT = t_DESCRIAT
    this.w_ELDATINI = t_ELDATINI
    this.w_ELDATFIN = t_ELDATFIN
    this.w_ELCATCON = t_ELCATCON
    this.w_FLNSAP = t_FLNSAP
    this.w_ELCODLIS = t_ELCODLIS
    this.w_ELTIPATT = t_ELTIPATT
    this.w_ELGRUPAR = t_ELGRUPAR
    this.w_ELDATDOC = t_ELDATDOC
    this.w_ELPERATT = t_ELPERATT
    this.w_ELDATATT = t_ELDATATT
    this.w_ELCAUDOC = t_ELCAUDOC
    this.w_ELGIOATT = t_ELGIOATT
    this.w_ELDATGAT = t_ELDATGAT
    this.w_ELCODPAG = t_ELCODPAG
    this.w_DESLIS = t_DESLIS
    this.w_ELPERFAT = t_ELPERFAT
    this.w_PREZUM = t_PREZUM
    this.w_ELDATFAT = t_ELDATFAT
    this.w_ELGIODOC = t_ELGIODOC
    this.w_MOLTIP = t_MOLTIP
    this.w_CATCLI = t_CATCLI
    this.w_CATART = t_CATART
    this.w_SCOLIS = t_SCOLIS
    this.w_DECUNI = t_DECUNI
    this.w_CACODART = t_CACODART
    this.w_MOLTI3 = t_MOLTI3
    this.w_OPERA3 = t_OPERA3
    this.w_FLUSEP = t_FLUSEP
    this.w_FLFRAZ1 = t_FLFRAZ1
    this.w_MODUM2 = t_MODUM2
    this.w_TIPATT = t_TIPATT
    this.w_TIPDOC = t_TIPDOC
    this.w_GRUPAR = t_GRUPAR
    this.w_MCAUDOC = t_MCAUDOC
    this.w_MTIPATT = t_MTIPATT
    this.w_MGRUPAR = t_MGRUPAR
    this.w_VALLIS = t_VALLIS
    this.w_TIPRIS = t_TIPRIS
    this.w_CICLO = t_CICLO
    this.w_DESPAG = t_DESPAG
    this.w_INILIS = t_INILIS
    this.w_FINLIS = t_FINLIS
    this.w_QUALIS = t_QUALIS
    this.w_OB_TEST = t_OB_TEST
    this.w_DATOBSO = t_DATOBSO
    this.w_LIPREZZO = t_LIPREZZO
    this.w_TIPSER = t_TIPSER
    this.w_ARCODART = t_ARCODART
    this.w_DATALIST = t_DATALIST
    this.w_FLVEAC = t_FLVEAC
    this.w_FLRIPS = t_FLRIPS
    this.w_GESTGUID = t_GESTGUID
    this.w_ARUNMIS1 = t_ARUNMIS1
    this.w_ARUNMIS2 = t_ARUNMIS2
    this.w_CCODLIS = t_CCODLIS
    this.w_MCODLIS = t_MCODLIS
    this.w_ELRINNOV = t_ELRINNOV
    this.w_ELESCRIN = t_ELESCRIN
    this.w_ELPERCON = t_ELPERCON
    this.w_ELRINCON = t_ELRINCON
    this.w_ELNUMGIO = t_ELNUMGIO
    this.w_ELDATDIS = t_ELDATDIS
    this.w_DESATT = t_DESATT
    this.w_DESDOC = t_DESDOC
    this.w_DESCTR = t_DESCTR
    this.w_ELVOCCEN = t_ELVOCCEN
    this.w_ELCODCEN = t_ELCODCEN
    this.w_ELCOCOMM = t_ELCOCOMM
    this.w_ELCODATT = t_ELCODATT
    this.w_MCODCEN = t_MCODCEN
    this.w_MCOCOMM = t_MCOCOMM
    this.w_CCOCOMM = t_CCOCOMM
    this.w_CCODCEN = t_CCODCEN
    this.w_MOTIPCON = t_MOTIPCON
    this.w_MD__FREQ = t_MD__FREQ
    this.w_DPDESCRI = t_DPDESCRI
    this.w_ATTEXIST = t_ATTEXIST
    this.w_DOCEXIST = t_DOCEXIST
    this.w_CC = t_CC
    this.w_CM = t_CM
    this.w_CI = t_CI
    this.w_CF = t_CF
    this.w_CV = t_CV
    this.w_QUACON = t_QUACON
    this.w_IVACON = t_IVACON
    this.w_MDDESCRI = t_MDDESCRI
    this.w_ELINICOA = t_ELINICOA
    this.w_ELFINCOA = t_ELFINCOA
    this.w_ELINICOD = t_ELINICOD
    this.w_ELFINCOD = t_ELFINCOD
    this.w_ELQTACON = t_ELQTACON
    this.w_QTA__RES = t_QTA__RES
    this.w_PREZZTOT = t_PREZZTOT
    this.w_OBTEST = t_OBTEST
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ELCODMOD with this.w_ELCODMOD
    replace t_CODART with this.w_CODART
    replace t_CODART with this.w_CODART
    replace t_CODICE with this.w_CODICE
    replace t_ELCONTRA with this.w_ELCONTRA
    replace t_CODCLI with this.w_CODCLI
    replace t_ELCODART with this.w_ELCODART
    replace t_ELUNIMIS with this.w_ELUNIMIS
    replace t_ELQTAMOV with this.w_ELQTAMOV
    replace t_TIPART with this.w_TIPART
    replace t_ELCODVAL with this.w_ELCODVAL
    replace t_UNIMIS with this.w_UNIMIS
    replace t_TIPMOD with this.w_TIPMOD
    replace t_PRIDAT with this.w_PRIDAT
    replace t_PRIDOC with this.w_PRIDOC
    replace t_COPRIDAT with this.w_COPRIDAT
    replace t_COPRIDOC with this.w_COPRIDOC
    replace t_PRIMADATA with this.w_PRIMADATA
    replace t_PRIMADATADOC with this.w_PRIMADATADOC
    replace t_DATINI with this.w_DATINI
    replace t_DATFIN with this.w_DATFIN
    replace t_PATIPRIS with this.w_PATIPRIS
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_QTAMOV with this.w_QTAMOV
    replace t_CODVAL with this.w_CODVAL
    replace t_FLSERG with this.w_FLSERG
    replace t_CARAGGST with this.w_CARAGGST
    replace t_FLATT with this.w_FLATT
    replace t_FLATTI with this.w_FLATTI
    replace t_CODPAG with this.w_CODPAG
    replace t_DESPER with this.w_DESPER
    replace t_DESCAT with this.w_DESCAT
    replace t_ELPREZZO with this.w_ELPREZZO
    replace t_ELSCONT1 with this.w_ELSCONT1
    replace t_ELCONCOD with this.w_ELCONCOD
    replace t_ELSCONT2 with this.w_ELSCONT2
    replace t_ELSCONT3 with this.w_ELSCONT3
    replace t_ELSCONT4 with this.w_ELSCONT4
    replace t_DESCRIAT with this.w_DESCRIAT
    replace t_ELDATINI with this.w_ELDATINI
    replace t_ELDATFIN with this.w_ELDATFIN
    replace t_ELCATCON with this.w_ELCATCON
    replace t_FLNSAP with this.w_FLNSAP
    replace t_ELCODLIS with this.w_ELCODLIS
    replace t_ELTIPATT with this.w_ELTIPATT
    replace t_ELGRUPAR with this.w_ELGRUPAR
    replace t_ELDATDOC with this.w_ELDATDOC
    replace t_ELPERATT with this.w_ELPERATT
    replace t_ELDATATT with this.w_ELDATATT
    replace t_ELCAUDOC with this.w_ELCAUDOC
    replace t_ELGIOATT with this.w_ELGIOATT
    replace t_ELDATGAT with this.w_ELDATGAT
    replace t_ELCODPAG with this.w_ELCODPAG
    replace t_DESLIS with this.w_DESLIS
    replace t_ELPERFAT with this.w_ELPERFAT
    replace t_PREZUM with this.w_PREZUM
    replace t_ELDATFAT with this.w_ELDATFAT
    replace t_ELGIODOC with this.w_ELGIODOC
    replace t_MOLTIP with this.w_MOLTIP
    replace t_CATCLI with this.w_CATCLI
    replace t_CATART with this.w_CATART
    replace t_SCOLIS with this.w_SCOLIS
    replace t_DECUNI with this.w_DECUNI
    replace t_CACODART with this.w_CACODART
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_OPERA3 with this.w_OPERA3
    replace t_FLUSEP with this.w_FLUSEP
    replace t_FLFRAZ1 with this.w_FLFRAZ1
    replace t_MODUM2 with this.w_MODUM2
    replace t_TIPATT with this.w_TIPATT
    replace t_TIPDOC with this.w_TIPDOC
    replace t_GRUPAR with this.w_GRUPAR
    replace t_MCAUDOC with this.w_MCAUDOC
    replace t_MTIPATT with this.w_MTIPATT
    replace t_MGRUPAR with this.w_MGRUPAR
    replace t_VALLIS with this.w_VALLIS
    replace t_TIPRIS with this.w_TIPRIS
    replace t_CICLO with this.w_CICLO
    replace t_DESPAG with this.w_DESPAG
    replace t_INILIS with this.w_INILIS
    replace t_FINLIS with this.w_FINLIS
    replace t_QUALIS with this.w_QUALIS
    replace t_OB_TEST with this.w_OB_TEST
    replace t_DATOBSO with this.w_DATOBSO
    replace t_LIPREZZO with this.w_LIPREZZO
    replace t_TIPSER with this.w_TIPSER
    replace t_ARCODART with this.w_ARCODART
    replace t_DATALIST with this.w_DATALIST
    replace t_FLVEAC with this.w_FLVEAC
    replace t_FLRIPS with this.w_FLRIPS
    replace t_GESTGUID with this.w_GESTGUID
    replace t_ARUNMIS1 with this.w_ARUNMIS1
    replace t_ARUNMIS2 with this.w_ARUNMIS2
    replace t_CCODLIS with this.w_CCODLIS
    replace t_MCODLIS with this.w_MCODLIS
    replace t_ELRINNOV with this.w_ELRINNOV
    replace t_ELESCRIN with this.w_ELESCRIN
    replace t_ELPERCON with this.w_ELPERCON
    replace t_ELRINCON with this.w_ELRINCON
    replace t_ELNUMGIO with this.w_ELNUMGIO
    replace t_ELDATDIS with this.w_ELDATDIS
    replace t_DESATT with this.w_DESATT
    replace t_DESDOC with this.w_DESDOC
    replace t_DESCTR with this.w_DESCTR
    replace t_ELVOCCEN with this.w_ELVOCCEN
    replace t_ELCODCEN with this.w_ELCODCEN
    replace t_ELCOCOMM with this.w_ELCOCOMM
    replace t_ELCODATT with this.w_ELCODATT
    replace t_MCODCEN with this.w_MCODCEN
    replace t_MCOCOMM with this.w_MCOCOMM
    replace t_CCOCOMM with this.w_CCOCOMM
    replace t_CCODCEN with this.w_CCODCEN
    replace t_MOTIPCON with this.w_MOTIPCON
    replace t_MD__FREQ with this.w_MD__FREQ
    replace t_DPDESCRI with this.w_DPDESCRI
    replace t_ATTEXIST with this.w_ATTEXIST
    replace t_DOCEXIST with this.w_DOCEXIST
    replace t_CC with this.w_CC
    replace t_CM with this.w_CM
    replace t_CI with this.w_CI
    replace t_CF with this.w_CF
    replace t_CV with this.w_CV
    replace t_QUACON with this.w_QUACON
    replace t_IVACON with this.w_IVACON
    replace t_MDDESCRI with this.w_MDDESCRI
    replace t_ELINICOA with this.w_ELINICOA
    replace t_ELFINCOA with this.w_ELFINCOA
    replace t_ELINICOD with this.w_ELINICOD
    replace t_ELFINCOD with this.w_ELFINCOD
    replace t_ELQTACON with this.w_ELQTACON
    replace t_QTA__RES with this.w_QTA__RES
    replace t_PREZZTOT with this.w_PREZZTOT
    replace t_OBTEST with this.w_OBTEST
    if i_srv='A'
      replace ELCODMOD with this.w_ELCODMOD
      replace ELCONTRA with this.w_ELCONTRA
      replace ELRINNOV with this.w_ELRINNOV
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mecPag1 as StdContainer
  Width  = 707
  height = 607
  stdWidth  = 707
  stdheight = 607
  resizeXpos=355
  resizeYpos=142
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_34 as cp_runprogram with uid="CNRVUIWANT",left=436, top=619, width=198,height=24,;
    caption='GSAG2BZM(XR)',;
   bGlobalFont=.t.,;
    prg="GSAG2BZM('XR')",;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 238044365


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=2, width=691,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="ELCODMOD",Label1="Modello",Field2="ELCONTRA",Label2="Contratto",Field3="ELCODART",Label3="Servizio",Field4="ELUNIMIS",Label4="U.M.",Field5="ELQTAMOV",Label5="Quantit�",Field6="ELPREZZO",Label6="Prezzo",Field7="ELRINNOV",Label7="Rinnovi",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 252510842


  add object oObj_1_42 as cp_runprogram with uid="JFPEHYJVFL",left=2, top=656, width=330,height=21,;
    caption='GSAG_BCL',;
   bGlobalFont=.t.,;
    prg="GSAG_BCL('R')",;
    cEvent = "w_ELCONTRA Changed,w_ELCODMOD Changed",;
    nPag=1;
    , HelpContextID = 74530994


  add object oObj_1_43 as cp_runprogram with uid="ELDBQWGEIG",left=2, top=678, width=223,height=19,;
    caption='GSAG_BCL(Z)',;
   bGlobalFont=.t.,;
    prg="GSAG_BCL('Z')",;
    cEvent = "w_ELCONCOD Changed",;
    nPag=1;
    , HelpContextID = 74722610


  add object oObj_1_44 as cp_runprogram with uid="ZXQEZMLEQN",left=2, top=698, width=223,height=19,;
    caption='GSAG_BCL(D)',;
   bGlobalFont=.t.,;
    prg="GSAG_BCL('D')",;
    cEvent = "CalcoliFinali",;
    nPag=1;
    , HelpContextID = 74716978


  add object oObj_1_46 as cp_runprogram with uid="QCDFOTEQFC",left=2, top=723, width=754,height=19,;
    caption='GSAG_BCL(C)',;
   bGlobalFont=.t.,;
    prg="GSAG_BCL('C')",;
    cEvent = "w_ELCODART Changed,w_ELQTAMOV Changed, w_ELUNIMIS Changed, w_ELCODLIS Changed,w_ELCONTRA Changed,LisRig, w_ELCODMOD Changed, w_ELCODCLI Changed, w_ELDATATT Changed, w_ELDATFAT Changed,Aggio",;
    nPag=1;
    , HelpContextID = 74716722

  add object oStr_1_3 as StdString with uid="TREAKEDOYE",Visible=.t., Left=71, Top=321,;
    Alignment=1, Width=62, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="RECURRHNPY",Visible=.t., Left=33, Top=547,;
    Alignment=1, Width=100, Height=18,;
    Caption="Periodicit� doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="GRYGCNVWHA",Visible=.t., Left=35, Top=433,;
    Alignment=1, Width=98, Height=18,;
    Caption="Periodicit� att.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="JJOGNADGXI",Visible=.t., Left=99, Top=296,;
    Alignment=1, Width=34, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="REXXEPTVJA",Visible=.t., Left=279, Top=296,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="XOIAGJIEZP",Visible=.t., Left=47, Top=573,;
    Alignment=1, Width=86, Height=18,;
    Caption="Prossimo doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="IQXWPGYCMQ",Visible=.t., Left=32, Top=458,;
    Alignment=1, Width=101, Height=18,;
    Caption="Prossima attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="EZUZGFJVBZ",Visible=.t., Left=220, Top=573,;
    Alignment=1, Width=185, Height=18,;
    Caption="Giorni di tolleranza per gen. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="ZVBJJYPOSX",Visible=.t., Left=224, Top=458,;
    Alignment=1, Width=181, Height=18,;
    Caption="Giorni di tolleranza per gen. att.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NIPJNHNPVP",Visible=.t., Left=457, Top=573,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="STSMTSPMRI",Visible=.t., Left=457, Top=458,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KNTMQONIRV",Visible=.t., Left=52, Top=346,;
    Alignment=1, Width=81, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="YBRCUUTOWA",Visible=.t., Left=66, Top=521,;
    Alignment=1, Width=67, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="SAFTLWSVCH",Visible=.t., Left=58, Top=246,;
    Alignment=1, Width=75, Height=18,;
    Caption="Sconti/magg.:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="BTXCUYMVGK",Visible=.t., Left=195, Top=249,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="ZHJAWJZPTZ",Visible=.t., Left=264, Top=249,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="NKPLWNOYFT",Visible=.t., Left=333, Top=249,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="ULPNZMOWRH",Visible=.t., Left=25, Top=274,;
    Alignment=1, Width=108, Height=18,;
    Caption="Contratto di vendita:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (!IsAhr())
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="MSBVDPRUIW",Visible=.t., Left=18, Top=496,;
    Alignment=1, Width=115, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="XXTYMZANRA",Visible=.t., Left=53, Top=383,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="JUMMMMPTTP",Visible=.t., Left=89, Top=408,;
    Alignment=1, Width=44, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="YZVVOCOKFH",Visible=.t., Left=7, Top=364,;
    Alignment=0, Width=66, Height=14,;
    Caption="Attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="PEVHHINHPJ",Visible=.t., Left=7, Top=478,;
    Alignment=0, Width=66, Height=18,;
    Caption="Documento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="ZNGEITTXVV",Visible=.t., Left=18, Top=222,;
    Alignment=1, Width=115, Height=18,;
    Caption="Quantit� consumata:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="PRKGDDZXWO",Visible=.t., Left=238, Top=222,;
    Alignment=1, Width=100, Height=18,;
    Caption="Quantit� residua:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="NPRJDERTUV",Visible=.t., Left=442, Top=222,;
    Alignment=1, Width=77, Height=18,;
    Caption="Prezzo totale:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oBox_1_30 as StdBox with uid="LIPBORAQCJ",left=3, top=378, width=631,height=2

  add object oBox_1_32 as StdBox with uid="NGRTBZQPQI",left=3, top=492, width=631,height=2

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=21,;
    width=687+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=22,width=686+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MOD_ELEM|CON_TRAS|ART_ICOL|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESPER_2_41.Refresh()
      this.Parent.oDESCAT_2_42.Refresh()
      this.Parent.oELSCONT1_2_44.Refresh()
      this.Parent.oELCONCOD_2_45.Refresh()
      this.Parent.oELSCONT2_2_46.Refresh()
      this.Parent.oELSCONT3_2_47.Refresh()
      this.Parent.oELSCONT4_2_48.Refresh()
      this.Parent.oDESCRIAT_2_49.Refresh()
      this.Parent.oELDATINI_2_50.Refresh()
      this.Parent.oELDATFIN_2_51.Refresh()
      this.Parent.oELCATCON_2_52.Refresh()
      this.Parent.oELCODLIS_2_54.Refresh()
      this.Parent.oELTIPATT_2_55.Refresh()
      this.Parent.oELGRUPAR_2_56.Refresh()
      this.Parent.oELDATDOC_2_57.Refresh()
      this.Parent.oELPERATT_2_58.Refresh()
      this.Parent.oELDATATT_2_59.Refresh()
      this.Parent.oELCAUDOC_2_60.Refresh()
      this.Parent.oELGIOATT_2_61.Refresh()
      this.Parent.oELDATGAT_2_62.Refresh()
      this.Parent.oELCODPAG_2_63.Refresh()
      this.Parent.oDESLIS_2_64.Refresh()
      this.Parent.oELPERFAT_2_65.Refresh()
      this.Parent.oELDATFAT_2_67.Refresh()
      this.Parent.oELGIODOC_2_69.Refresh()
      this.Parent.oDESPAG_2_92.Refresh()
      this.Parent.oDESATT_2_127.Refresh()
      this.Parent.oDESDOC_2_128.Refresh()
      this.Parent.oDESCTR_2_129.Refresh()
      this.Parent.oDPDESCRI_2_140.Refresh()
      this.Parent.oELQTACON_2_160.Refresh()
      this.Parent.oQTA__RES_2_161.Refresh()
      this.Parent.oPREZZTOT_2_162.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MOD_ELEM'
        oDropInto=this.oBodyCol.oRow.oELCODMOD_2_1
      case cFile='CON_TRAS'
        oDropInto=this.oBodyCol.oRow.oELCONTRA_2_6
      case cFile='ART_ICOL'
        oDropInto=this.oBodyCol.oRow.oELCODART_2_8
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oELUNIMIS_2_9
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESPER_2_41 as StdTrsField with uid="FLNNGAKPXT",rtseq=44,rtrep=.t.,;
    cFormVar="w_DESPER",value=space(50),enabled=.f.,;
    HelpContextID = 220507594,;
    cTotal="", bFixedPos=.t., cQueryName = "DESPER",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=371, Left=194, Top=547, InputMask=replicate('X',50)

  add object oDESCAT_2_42 as StdTrsField with uid="UUJMDVZLQG",rtseq=45,rtrep=.t.,;
    cFormVar="w_DESCAT",value=space(60),enabled=.f.,;
    HelpContextID = 191999434,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=371, Left=194, Top=320, InputMask=replicate('X',60)

  add object oELSCONT1_2_44 as StdTrsField with uid="XXMPTXVADK",rtseq=47,rtrep=.t.,;
    cFormVar="w_ELSCONT1",value=0,;
    ToolTipText = "1^Sconto",;
    HelpContextID = 258890103,;
    cTotal="", bFixedPos=.t., cQueryName = "ELSCONT1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=138, Top=245, cSayPict=["999.99"], cGetPict=["999.99"]

  func oELSCONT1_2_44.mCond()
    with this.Parent.oContained
      return (.w_TIPMOD='C' and !Isahe())
    endwith
  endfunc

  func oELSCONT1_2_44.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  add object oELCONCOD_2_45 as StdTrsField with uid="NIDLRVGNTI",rtseq=48,rtrep=.t.,;
    cFormVar="w_ELCONCOD",value=space(15),;
    ToolTipText = "Codice contratto di vendita associato",;
    HelpContextID = 194422390,;
    cTotal="", bFixedPos=.t., cQueryName = "ELCONCOD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=138, Top=270, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CON_TRAM", cZoomOnZoom="GSAR_BCZ", oKey_1_1="COTIPCLF", oKey_1_2="this.w_TIPCON", oKey_2_1="CONUMERO", oKey_2_2="this.w_ELCONCOD"

  func oELCONCOD_2_45.mCond()
    with this.Parent.oContained
      return (.w_TIPMOD<>'F' )
    endwith
  endfunc

  func oELCONCOD_2_45.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAhr())
    endwith
    endif
  endfunc

  func oELCONCOD_2_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCONCOD_2_45.ecpDrop(oSource)
    this.Parent.oContained.link_2_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oELCONCOD_2_45.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oELCONCOD_2_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BCZ',"Elenco contratti",'GSAG_AEL.CON_TRAM_VZM',this.parent.oContained
  endproc
  proc oELCONCOD_2_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BCZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.COTIPCLF=w_TIPCON
     i_obj.w_CONUMERO=this.parent.oContained.w_ELCONCOD
    i_obj.ecpSave()
  endproc

  add object oELSCONT2_2_46 as StdTrsField with uid="JWYDOBVQUO",rtseq=49,rtrep=.t.,;
    cFormVar="w_ELSCONT2",value=0,;
    ToolTipText = "2^Sconto",;
    HelpContextID = 258890104,;
    cTotal="", bFixedPos=.t., cQueryName = "ELSCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=207, Top=245, cSayPict=["999.99"], cGetPict=["999.99"]

  func oELSCONT2_2_46.mCond()
    with this.Parent.oContained
      return (.w_ELSCONT1<>0 and .w_TIPMOD='C' and !Isahe())
    endwith
  endfunc

  func oELSCONT2_2_46.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  add object oELSCONT3_2_47 as StdTrsField with uid="NNFRICYISQ",rtseq=50,rtrep=.t.,;
    cFormVar="w_ELSCONT3",value=0,;
    ToolTipText = "3^Sconto",;
    HelpContextID = 258890105,;
    cTotal="", bFixedPos=.t., cQueryName = "ELSCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=275, Top=245, cSayPict=["999.99"], cGetPict=["999.99"]

  func oELSCONT3_2_47.mCond()
    with this.Parent.oContained
      return (.w_ELSCONT2<>0 AND .w_TIPMOD='C' and !Isahe())
    endwith
  endfunc

  func oELSCONT3_2_47.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  add object oELSCONT4_2_48 as StdTrsField with uid="XPRSGEGUZE",rtseq=51,rtrep=.t.,;
    cFormVar="w_ELSCONT4",value=0,;
    ToolTipText = "4^Sconto",;
    HelpContextID = 258890106,;
    cTotal="", bFixedPos=.t., cQueryName = "ELSCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=343, Top=245, cSayPict=["999.99"], cGetPict=["999.99"]

  func oELSCONT4_2_48.mCond()
    with this.Parent.oContained
      return (.w_ELSCONT3<>0 and .w_TIPMOD='C' and !Isahe())
    endwith
  endfunc

  func oELSCONT4_2_48.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
    endif
  endfunc

  add object oDESCRIAT_2_49 as StdTrsField with uid="URXPABGLKR",rtseq=52,rtrep=.t.,;
    cFormVar="w_DESCRIAT",value=space(50),enabled=.f.,;
    HelpContextID = 90287478,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCRIAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=371, Left=194, Top=432, InputMask=replicate('X',50)

  add object oELDATINI_2_50 as StdTrsField with uid="AJKXPHHGVX",rtseq=53,rtrep=.t.,;
    cFormVar="w_ELDATINI",value=ctod("  /  /  "),;
    ToolTipText = "Data inizio applicazione contratto",;
    HelpContextID = 88381041,;
    cTotal="", bFixedPos=.t., cQueryName = "ELDATINI",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Date non comprese nell'intervallo specificato in anagrafica contratti!",;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=138, Top=295

  func oELDATINI_2_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ELDATINI>=.w_DATINI AND .w_ELDATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oELDATFIN_2_51 as StdTrsField with uid="JCSMJCZMBG",rtseq=54,rtrep=.t.,;
    cFormVar="w_ELDATFIN",value=ctod("  /  /  "),;
    ToolTipText = "Data fine applicazione contratto",;
    HelpContextID = 129722772,;
    cTotal="", bFixedPos=.t., cQueryName = "ELDATFIN",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Date non comprese nell'intervallo specificato in anagrafica contratti o data iniziale maggiore della data finale",;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=306, Top=295

  func oELDATFIN_2_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELDATFIN>=.w_DATINI AND .w_ELDATFIN<=.w_DATFIN) AND (.w_ELDATINI<=.w_ELDATFIN) or (empty(.w_ELDATINI)))
    endwith
    return bRes
  endfunc

  add object oELCATCON_2_52 as StdTrsField with uid="KQEOVMLXFF",rtseq=55,rtrep=.t.,;
    cFormVar="w_ELCATCON",value=space(5),;
    ToolTipText = "Categoria contratto",;
    HelpContextID = 189048428,;
    cTotal="", bFixedPos=.t., cQueryName = "ELCATCON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=138, Top=320, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_ELEM", cZoomOnZoom="GSAG_ACE", oKey_1_1="CECODELE", oKey_1_2="this.w_ELCATCON"

  func oELCATCON_2_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCATCON_2_52.ecpDrop(oSource)
    this.Parent.oContained.link_2_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oELCATCON_2_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ELEM','*','CECODELE',cp_AbsName(this.parent,'oELCATCON_2_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACE',"Categorie contratti",'',this.parent.oContained
  endproc
  proc oELCATCON_2_52.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODELE=this.parent.oContained.w_ELCATCON
    i_obj.ecpSave()
  endproc

  add object oELCODLIS_2_54 as StdTrsField with uid="XGWRVDFFGC",rtseq=57,rtrep=.t.,;
    cFormVar="w_ELCODLIS",value=space(5),;
    ToolTipText = "Codice listino",;
    HelpContextID = 214522265,;
    cTotal="", bFixedPos=.t., cQueryName = "ELCODLIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=138, Top=345, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ELCODLIS"

  func oELCODLIS_2_54.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Isahe())
    endwith
    endif
  endfunc

  func oELCODLIS_2_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_54('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODLIS_2_54.ecpDrop(oSource)
    this.Parent.oContained.link_2_54('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oELCODLIS_2_54.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oELCODLIS_2_54'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oELCODLIS_2_54.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_ELCODLIS
    i_obj.ecpSave()
  endproc

  add object oELTIPATT_2_55 as StdTrsField with uid="SFIFNHEATF",rtseq=58,rtrep=.t.,;
    cFormVar="w_ELTIPATT",value=space(20),;
    HelpContextID = 42232218,;
    cTotal="", bFixedPos=.t., cQueryName = "ELTIPATT",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=138, Top=383, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_ELTIPATT"

  func oELTIPATT_2_55.mCond()
    with this.Parent.oContained
      return (.w_FLATTI='S' and .w_MOTIPCON<>'P')
    endwith
  endfunc

  func oELTIPATT_2_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_55('Part',this)
    endwith
    return bRes
  endfunc

  proc oELTIPATT_2_55.ecpDrop(oSource)
    this.Parent.oContained.link_2_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oELTIPATT_2_55.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oELTIPATT_2_55'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oELTIPATT_2_55.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ELTIPATT
    i_obj.ecpSave()
  endproc

  add object oELGRUPAR_2_56 as StdTrsField with uid="XSAVVZNWKG",rtseq=59,rtrep=.t.,;
    cFormVar="w_ELGRUPAR",value=space(5),;
    ToolTipText = "Gruppo partecipante attivit�",;
    HelpContextID = 31234456,;
    cTotal="", bFixedPos=.t., cQueryName = "ELGRUPAR",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore incongruente o obsoleto",;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=138, Top=408, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_ELGRUPAR"

  func oELGRUPAR_2_56.mCond()
    with this.Parent.oContained
      return (.w_FLATTI='S' and .w_MOTIPCON<>'P')
    endwith
  endfunc

  func oELGRUPAR_2_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_56('Part',this)
    endwith
    return bRes
  endfunc

  proc oELGRUPAR_2_56.ecpDrop(oSource)
    this.Parent.oContained.link_2_56('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oELGRUPAR_2_56.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oELGRUPAR_2_56'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oELGRUPAR_2_56.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_ELGRUPAR
    i_obj.ecpSave()
  endproc

  add object oELDATDOC_2_57 as StdTrsField with uid="XGKBJRDNON",rtseq=60,rtrep=.t.,;
    cFormVar="w_ELDATDOC",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data fino alla quale l'elemento potr� generare documenti",;
    HelpContextID = 172267127,;
    cTotal="", bFixedPos=.t., cQueryName = "ELDATDOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=504, Top=572

  add object oELPERATT_2_58 as StdTrsField with uid="ZDRBTDNVNL",rtseq=61,rtrep=.t.,;
    cFormVar="w_ELPERATT",value=space(3),;
    ToolTipText = "Periodicit� attivit�",;
    HelpContextID = 44050842,;
    cTotal="", bFixedPos=.t., cQueryName = "ELPERATT",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=138, Top=432, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_ELPERATT"

  func oELPERATT_2_58.mCond()
    with this.Parent.oContained
      return (.w_FLATTI='S' and .w_MOTIPCON<>'P')
    endwith
  endfunc

  func oELPERATT_2_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_58('Part',this)
    endwith
    return bRes
  endfunc

  proc oELPERATT_2_58.ecpDrop(oSource)
    this.Parent.oContained.link_2_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oELPERATT_2_58.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oELPERATT_2_58'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo periodicit�",'',this.parent.oContained
  endproc
  proc oELPERATT_2_58.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_ELPERATT
    i_obj.ecpSave()
  endproc

  add object oELDATATT_2_59 as StdTrsField with uid="MKQNCZTGZU",rtseq=62,rtrep=.t.,;
    cFormVar="w_ELDATATT",value=ctod("  /  /  "),;
    ToolTipText = "Data prossima attivit�",;
    HelpContextID = 45836698,;
    cTotal="", bFixedPos=.t., cQueryName = "ELDATATT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=138, Top=457

  func oELDATATT_2_59.mCond()
    with this.Parent.oContained
      return (.w_FLATTI='S' and .w_MOTIPCON<>'P')
    endwith
  endfunc

  func oELDATATT_2_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_ELDATATT>=.w_ELDATINI)
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione: la data prossima attivit� � inferiore alla data inizio validit� dell'elemento contratto! Verranno generate attivit� in base alla periodicit� definita fino alla data fine validit� pi� giorni di tolleranza")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oELCAUDOC_2_60 as StdTrsField with uid="UQVSEYLYCZ",rtseq=63,rtrep=.t.,;
    cFormVar="w_ELCAUDOC",value=space(5),;
    HelpContextID = 171222647,;
    cTotal="", bFixedPos=.t., cQueryName = "ELCAUDOC",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo",;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=138, Top=496, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_ELCAUDOC"

  func oELCAUDOC_2_60.mCond()
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
  endfunc

  func oELCAUDOC_2_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_60('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCAUDOC_2_60.ecpDrop(oSource)
    this.Parent.oContained.link_2_60('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oELCAUDOC_2_60.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oELCAUDOC_2_60'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oELCAUDOC_2_60.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_ELCAUDOC
    i_obj.ecpSave()
  endproc

  add object oELGIOATT_2_61 as StdTrsField with uid="CIFPROMYSS",rtseq=64,rtrep=.t.,;
    cFormVar="w_ELGIOATT",value=0,;
    ToolTipText = "Giorni di tolleranza per gen. att.",;
    HelpContextID = 41130394,;
    cTotal="", bFixedPos=.t., cQueryName = "ELGIOATT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=409, Top=457

  func oELGIOATT_2_61.mCond()
    with this.Parent.oContained
      return (.w_FLATTI='S' and .w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oELDATGAT_2_62 as StdTrsField with uid="KEFGAQKQFS",rtseq=65,rtrep=.t.,;
    cFormVar="w_ELDATGAT",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data fino alla quale l'elemento potr� generare attivit�",;
    HelpContextID = 121935462,;
    cTotal="", bFixedPos=.t., cQueryName = "ELDATGAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=504, Top=457

  add object oELCODPAG_2_63 as StdTrsField with uid="ASKOFPGSXQ",rtseq=66,rtrep=.t.,;
    cFormVar="w_ELCODPAG",value=space(5),;
    HelpContextID = 13195661,;
    cTotal="", bFixedPos=.t., cQueryName = "ELCODPAG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=138, Top=521, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_ELCODPAG"

  func oELCODPAG_2_63.mCond()
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
  endfunc

  func oELCODPAG_2_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_63('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODPAG_2_63.ecpDrop(oSource)
    this.Parent.oContained.link_2_63('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oELCODPAG_2_63.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oELCODPAG_2_63'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oELCODPAG_2_63.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_ELCODPAG
    i_obj.ecpSave()
  endproc

  add object oDESLIS_2_64 as StdTrsField with uid="SWKCQHAJKZ",rtseq=67,rtrep=.t.,;
    cFormVar="w_DESLIS",value=space(40),enabled=.f.,;
    HelpContextID = 199798218,;
    cTotal="", bFixedPos=.t., cQueryName = "DESLIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=202, Top=345, InputMask=replicate('X',40)

  func oDESLIS_2_64.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Isahe())
    endwith
    endif
  endfunc

  add object oELPERFAT_2_65 as StdTrsField with uid="OIFIBVFPTX",rtseq=68,rtrep=.t.,;
    cFormVar="w_ELPERFAT",value=space(3),;
    ToolTipText = "Periodicit� documento",;
    HelpContextID = 140498534,;
    cTotal="", bFixedPos=.t., cQueryName = "ELPERFAT",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=138, Top=547, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_ELPERFAT"

  func oELPERFAT_2_65.mCond()
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
  endfunc

  func oELPERFAT_2_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_65('Part',this)
    endwith
    return bRes
  endfunc

  proc oELPERFAT_2_65.ecpDrop(oSource)
    this.Parent.oContained.link_2_65('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oELPERFAT_2_65.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oELPERFAT_2_65'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo periodicit�",'',this.parent.oContained
  endproc
  proc oELPERFAT_2_65.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_ELPERFAT
    i_obj.ecpSave()
  endproc

  add object oELDATFAT_2_67 as StdTrsField with uid="JJMKJZCEVA",rtseq=70,rtrep=.t.,;
    cFormVar="w_ELDATFAT",value=ctod("  /  /  "),;
    ToolTipText = "Data prossimo documento",;
    HelpContextID = 138712678,;
    cTotal="", bFixedPos=.t., cQueryName = "ELDATFAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=138, Top=572

  func oELDATFAT_2_67.mCond()
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
  endfunc

  func oELDATFAT_2_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_ELDATFAT>=.w_ELDATINI)
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione: la data prossimo documento � inferiore alla data inizio validit� dell'elemento contratto! Verranno generati documenti in base alla periodicit� definita fino alla data fine validit� pi� giorni di tolleranza")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oELGIODOC_2_69 as StdTrsField with uid="SADNDRPEIC",rtseq=72,rtrep=.t.,;
    cFormVar="w_ELGIODOC",value=0,;
    ToolTipText = "gg di tolleranza per gen. doc.",;
    HelpContextID = 176973431,;
    cTotal="", bFixedPos=.t., cQueryName = "ELGIODOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=409, Top=572

  func oELGIODOC_2_69.mCond()
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
  endfunc

  add object oDESPAG_2_92 as StdTrsField with uid="CEKXLHALVI",rtseq=96,rtrep=.t.,;
    cFormVar="w_DESPAG",value=space(30),enabled=.f.,;
    HelpContextID = 140815818,;
    cTotal="", bFixedPos=.t., cQueryName = "DESPAG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=362, Left=203, Top=521, InputMask=replicate('X',30)

  add object oBtn_2_110 as StdButton with uid="FLVSTLMVZH",width=48,height=45,;
   left=644, top=387,;
    CpPicture="bmp\carica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati di riga";
    , HelpContextID = 247368010;
    , TabStop=.f.,Caption='\<Listini';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_110.Click()
      do GSAG_KLC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_110.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!isahe())
    endwith
   endif
  endfunc

  add object oBtn_2_112 as StdButton with uid="DVFYFXDEXI",width=48,height=45,;
   left=644, top=433,;
    CpPicture="BMP\INVENTAR.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al dettaglio listini articolo di vendita/entrambi";
    , HelpContextID = 74153316;
    , tabstop=.f., caption='Lis.ve\<nd.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_112.Click()
      with this.Parent.oContained
        GSVE_BLI(this.Parent.oContained,"LISV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_112.mCond()
    with this.Parent.oContained
      return (isahe() and (NOT EMPTY(.w_ARCODART) and !(g_CVEN <> 'S' And g_CACQ = 'S'  ) ))
    endwith
  endfunc

  func oBtn_2_112.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!isahe())
    endwith
   endif
  endfunc

  add object oBtn_2_116 as StdButton with uid="DGTXJYSTJS",width=48,height=45,;
   left=644, top=341,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=2;
    , ToolTipText = "Apre la stampa verifica listini applicabili e applicati";
    , HelpContextID = 201745335;
    , caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_116.Click()
      do GSVE_SVL with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_116.mCond()
    with this.Parent.oContained
      return ( !Empty(.w_ELTCOLIS+.w_ELSCOLIS))
    endwith
  endfunc

  func oBtn_2_116.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! Isahe())
    endwith
   endif
  endfunc

  add object oBtn_2_117 as StdButton with uid="TWKEMSZZJT",width=48,height=45,;
   left=644, top=293,;
    CpPicture="BMP\APPLICA.BMP", caption="", nPag=2;
    , ToolTipText = "Apre la stampa verifica listini applicabili e applicati";
    , HelpContextID = 174738329;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_117.Click()
      with this.Parent.oContained
        .Notifyevent('LisRig')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_117.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! Isahe())
    endwith
   endif
  endfunc

  add object oBtn_2_121 as StdButton with uid="XZUXHRYMMI",width=48,height=45,;
   left=644, top=246,;
    CpPicture="BMP\CONCLUSI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati aggiuntivi";
    , HelpContextID = 56197578;
    , caption='\<Altri dati';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_121.Click()
      do GSAG_KDG with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_121.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  add object oDESATT_2_127 as StdTrsField with uid="LVCSAEKOST",rtseq=131,rtrep=.t.,;
    cFormVar="w_DESATT",value=space(254),enabled=.f.,;
    HelpContextID = 172207562,;
    cTotal="", bFixedPos=.t., cQueryName = "DESATT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=296, Top=383, InputMask=replicate('X',254)

  add object oDESDOC_2_128 as StdTrsField with uid="LIHTDOVJGR",rtseq=132,rtrep=.t.,;
    cFormVar="w_DESDOC",value=space(35),enabled=.f.,;
    HelpContextID = 194031050,;
    cTotal="", bFixedPos=.t., cQueryName = "DESDOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=197, Top=496, InputMask=replicate('X',35)

  add object oDESCTR_2_129 as StdTrsField with uid="OFAWAUPOGQ",rtseq=133,rtrep=.t.,;
    cFormVar="w_DESCTR",value=space(35),enabled=.f.,;
    HelpContextID = 205630922,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCTR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=340, Left=258, Top=270, InputMask=replicate('X',35)

  func oDESCTR_2_129.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAhr())
    endwith
    endif
  endfunc

  add object oDPDESCRI_2_140 as StdTrsField with uid="QEQALOSWZO",rtseq=144,rtrep=.t.,;
    cFormVar="w_DPDESCRI",value=space(60),enabled=.f.,;
    HelpContextID = 78605695,;
    cTotal="", bFixedPos=.t., cQueryName = "DPDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=197, Top=408, InputMask=replicate('X',60)

  add object oBtn_2_141 as StdButton with uid="NHPYNCGGKY",width=48,height=45,;
   left=590, top=433,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla visualizzazione delle attivit� generate";
    , HelpContextID = 258701994;
    , Caption='\<Vis. Att.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_141.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"AO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_141.mCond()
    with this.Parent.oContained
      return (.cFunction='Query' and !empty(.w_ELCONTRA))
    endwith
  endfunc

  func oBtn_2_141.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ATTEXIST)
    endwith
   endif
  endfunc

  add object oBtn_2_142 as StdButton with uid="YJIXATOSCL",width=48,height=45,;
   left=590, top=521,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla visualizzazione dei documenti generati";
    , HelpContextID = 227837287;
    , Caption='\<Vis. Doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_142.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"DO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_142.mCond()
    with this.Parent.oContained
      return (.cFunction='Query' and !empty(.w_ELCONTRA))
    endwith
  endfunc

  func oBtn_2_142.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_DOCEXIST)
    endwith
   endif
  endfunc

  add object oELQTACON_2_160 as StdTrsField with uid="YWQFQQMGRQ",rtseq=163,rtrep=.t.,;
    cFormVar="w_ELQTACON",value=0,enabled=.f.,;
    HelpContextID = 207668844,;
    cTotal="", bFixedPos=.t., cQueryName = "ELQTACON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=138, Top=220, cSayPict=["99999999.999"], cGetPict=["99999999.999"]

  func oELQTACON_2_160.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
    endif
  endfunc

  add object oQTA__RES_2_161 as StdTrsField with uid="OYFEXYAPJW",rtseq=164,rtrep=.t.,;
    cFormVar="w_QTA__RES",value=0,enabled=.f.,;
    HelpContextID = 76104281,;
    cTotal="", bFixedPos=.t., cQueryName = "QTA__RES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=343, Top=220, cSayPict=["99999999.999"], cGetPict=["99999999.999"]

  func oQTA__RES_2_161.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
    endif
  endfunc

  add object oPREZZTOT_2_162 as StdTrsField with uid="ZZQBBYYDYH",rtseq=165,rtrep=.t.,;
    cFormVar="w_PREZZTOT",value=0,;
    HelpContextID = 164331446,;
    cTotal="", bFixedPos=.t., cQueryName = "PREZZTOT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=103, Left=523, Top=220, cSayPict=[v_PU(38+VVL)], cGetPict=[v_GU(38+VVL)]

  func oPREZZTOT_2_162.mCond()
    with this.Parent.oContained
      return (.w_MOTIPCON='P' AND .w_TIPMOD<>'F' and !isahe())
    endwith
  endfunc

  func oPREZZTOT_2_162.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
    endif
  endfunc

  proc oPREZZTOT_2_162.mAfter
    with this.Parent.oContained
      .Notifyevent('Azzera')
    endwith
  endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mecBodyRow as CPBodyRowCnt
  Width=677
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oELCODMOD_2_1 as StdTrsField with uid="JIBYUMKBKV",rtseq=1,rtrep=.t.,;
    cFormVar="w_ELCODMOD",value=space(10),isprimarykey=.t.,;
    ToolTipText = "Modello elementi",;
    HelpContextID = 37135990,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=80, Left=-2, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_ELEM", cZoomOnZoom="GSAG_AME", oKey_1_1="MOCODICE", oKey_1_2="this.w_ELCODMOD"

  func oELCODMOD_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODMOD_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oELCODMOD_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oELCODMOD_2_1.readonly and this.parent.oELCODMOD_2_1.isprimarykey)
    do cp_zoom with 'MOD_ELEM','*','MOCODICE',cp_AbsName(this.parent,'oELCODMOD_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_AME',"Modelli",'GSAG_KV3.MOD_ELEM_VZM',this.parent.oContained
   endif
  endproc
  proc oELCODMOD_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAG_AME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_ELCODMOD
    i_obj.ecpSave()
  endproc

  add object oELCONTRA_2_6 as StdTrsField with uid="BVSYEDGMSM",rtseq=6,rtrep=.t.,;
    cFormVar="w_ELCONTRA",value=space(10),nZero=10,isprimarykey=.t.,;
    ToolTipText = "Codice contratto",;
    HelpContextID = 90790279,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione: Intestatario del contratto incongruente!",;
   bGlobalFont=.t.,;
    Height=17, Width=89, Left=82, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CON_TRAS", cZoomOnZoom="GSAG_ACA", oKey_1_1="COSERIAL", oKey_1_2="this.w_ELCONTRA"

  func oELCONTRA_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCONTRA_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oELCONTRA_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oELCONTRA_2_6.readonly and this.parent.oELCONTRA_2_6.isprimarykey)
    do cp_zoom with 'CON_TRAS','*','COSERIAL',cp_AbsName(this.parent,'oELCONTRA_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this.parent.oContained
   endif
  endproc
  proc oELCONTRA_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_COSERIAL=this.parent.oContained.w_ELCONTRA
    i_obj.ecpSave()
  endproc

  add object oELCODART_2_8 as StdTrsField with uid="ORXGOFZIXD",rtseq=8,rtrep=.t.,;
    cFormVar="w_ELCODART",value=space(20),;
    ToolTipText = "Codice servizio",;
    HelpContextID = 29972890,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=151, Left=173, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_ELCODART"

  func oELCODART_2_8.mCond()
    with this.Parent.oContained
      return (.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N'))
    endwith
  endfunc

  func oELCODART_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODART_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oELCODART_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oELCODART_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oELCODART_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ELCODART
    i_obj.ecpSave()
  endproc

  add object oELUNIMIS_2_9 as StdTrsField with uid="RKDSMQHGJV",rtseq=9,rtrep=.t.,;
    cFormVar="w_ELUNIMIS",value=space(3),;
    HelpContextID = 236550553,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=326, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ELUNIMIS"

  func oELUNIMIS_2_9.mCond()
    with this.Parent.oContained
      return ((.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N')) and .w_MOTIPCON<>'P')
    endwith
  endfunc

  func oELUNIMIS_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oELUNIMIS_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oELUNIMIS_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oELUNIMIS_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oELUNIMIS_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ELUNIMIS
    i_obj.ecpSave()
  endproc

  add object oELQTAMOV_2_10 as StdTrsField with uid="HFZYBNAOZV",rtseq=10,rtrep=.t.,;
    cFormVar="w_ELQTAMOV",value=0,;
    HelpContextID = 39896676,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire quantit� maggiore di zero",;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=375, Top=0

  func oELQTAMOV_2_10.mCond()
    with this.Parent.oContained
      return (.w_TIPART<>'FO')
    endwith
  endfunc

  func oELQTAMOV_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ELQTAMOV>0)
    endwith
    return bRes
  endfunc

  add object oELPREZZO_2_43 as StdTrsField with uid="TZCOFENQAZ",rtseq=46,rtrep=.t.,;
    cFormVar="w_ELPREZZO",value=0,;
    HelpContextID = 86169195,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=475, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)]

  func oELPREZZO_2_43.mCond()
    with this.Parent.oContained
      return (.w_TIPMOD='C' and !Isahe())
    endwith
  endfunc

  add object oELRINNOV_2_120 as StdTrsField with uid="VCEMTLAKAJ",rtseq=125,rtrep=.t.,;
    cFormVar="w_ELRINNOV",value=0,enabled=.f.,isprimarykey=.t.,;
    HelpContextID = 10204772,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=617, Top=0
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oELCODMOD_2_1.When()
    return(.t.)
  proc oELCODMOD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oELCODMOD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mec','ELE_CONT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ELCODIMP=ELE_CONT.ELCODIMP";
  +" and "+i_cAliasName2+".ELCODCOM=ELE_CONT.ELCODCOM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
