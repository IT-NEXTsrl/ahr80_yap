* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ba6                                                        *
*              Allineamento dell'elemento contratto al modello corrispondente  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-18                                                      *
* Last revis.: 2011-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pELCONTRA,pELCODMOD,pELCODIMP,pELCODCOM,pELRINNOV
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_ba6",oParentObject,m.pELCONTRA,m.pELCODMOD,m.pELCODIMP,m.pELCODCOM,m.pELRINNOV)
return(i_retval)

define class tgsag_ba6 as StdBatch
  * --- Local variables
  pELCONTRA = space(10)
  pELCODMOD = space(10)
  pELCODIMP = space(10)
  pELCODCOM = 0
  pELRINNOV = 0
  w_MOTIPCON = space(1)
  w_FLATTI = space(1)
  w_FLATT = space(1)
  w_COTIPDOC = space(5)
  w_MOCAUDOC = space(5)
  w_ELCAUDOC = space(5)
  w_COCODPAG = space(5)
  w_ELCODPAG = space(5)
  w_COTIPATT = space(20)
  w_MOTIPATT = space(20)
  w_ELTIPATT = space(20)
  w_COGRUPAR = space(5)
  w_MOGRUPAR = space(5)
  w_ELGRUPAR = space(5)
  w_COCODLIS = space(5)
  w_MOCODLIS = space(5)
  w_ELCODLIS = space(5)
  w_COESCRIN = space(1)
  w_MOESCRIN = space(1)
  w_ELESCRIN = space(1)
  w_COPERCON = space(3)
  w_MOPERCON = space(3)
  w_ELPERCON = space(3)
  w_CORINCON = space(1)
  w_MORINCON = space(1)
  w_ELRINCON = space(1)
  w_CONUMGIO = 0
  w_MONUMGIO = 0
  w_ELNUMGIO = 0
  w_ELRINCON_OLD = space(1)
  w_ELDATDIS = ctod("  /  /  ")
  w_ELDATFIN = ctod("  /  /  ")
  w_COCODCEN = space(15)
  w_MOCODCEN = space(15)
  w_ELCODCEN = space(15)
  w_COCOCOMM = space(15)
  w_MOCOCOMM = space(15)
  w_ELCOCOMM = space(15)
  w_ELCOCOMM_1 = space(15)
  w_MOVOCCEN = space(15)
  w_ELVOCCEN = space(15)
  w_MOCODATT = space(15)
  w_ELCODATT = space(15)
  * --- WorkFile variables
  MOD_ELEM_idx=0
  CON_TRAS_idx=0
  ELE_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Allineamento dell'elemento contratto al modello corrispondente
    * --- Legge i dati dal contratto
    * --- Read from CON_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CON_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAS_idx,2],.t.,this.CON_TRAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COTIPDOC,COTIPATT,COGRUPAR,COCODLIS,COESCRIN,COPERCON,CORINCON,CONUMGIO,COCODCEN,COCOCOMM,COCODPAG"+;
        " from "+i_cTable+" CON_TRAS where ";
            +"COSERIAL = "+cp_ToStrODBC(this.pELCONTRA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COTIPDOC,COTIPATT,COGRUPAR,COCODLIS,COESCRIN,COPERCON,CORINCON,CONUMGIO,COCODCEN,COCOCOMM,COCODPAG;
        from (i_cTable) where;
            COSERIAL = this.pELCONTRA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COTIPDOC = NVL(cp_ToDate(_read_.COTIPDOC),cp_NullValue(_read_.COTIPDOC))
      this.w_COTIPATT = NVL(cp_ToDate(_read_.COTIPATT),cp_NullValue(_read_.COTIPATT))
      this.w_COGRUPAR = NVL(cp_ToDate(_read_.COGRUPAR),cp_NullValue(_read_.COGRUPAR))
      this.w_COCODLIS = NVL(cp_ToDate(_read_.COCODLIS),cp_NullValue(_read_.COCODLIS))
      this.w_COESCRIN = NVL(cp_ToDate(_read_.COESCRIN),cp_NullValue(_read_.COESCRIN))
      this.w_COPERCON = NVL(cp_ToDate(_read_.COPERCON),cp_NullValue(_read_.COPERCON))
      this.w_CORINCON = NVL(cp_ToDate(_read_.CORINCON),cp_NullValue(_read_.CORINCON))
      this.w_CONUMGIO = NVL(cp_ToDate(_read_.CONUMGIO),cp_NullValue(_read_.CONUMGIO))
      this.w_COCODCEN = NVL(cp_ToDate(_read_.COCODCEN),cp_NullValue(_read_.COCODCEN))
      this.w_COCOCOMM = NVL(cp_ToDate(_read_.COCOCOMM),cp_NullValue(_read_.COCOCOMM))
      this.w_COCODPAG = NVL(cp_ToDate(_read_.COCODPAG),cp_NullValue(_read_.COCODPAG))
      this.w_COCODLIS = NVL(cp_ToDate(_read_.COCODLIS),cp_NullValue(_read_.COCODLIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Legge i dati dal modello
    * --- Read from MOD_ELEM
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_ELEM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_ELEM_idx,2],.t.,this.MOD_ELEM_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MOCAUDOC,MOTIPATT,MOGRUPAR,MOCODLIS,MOESCRIN,MOPERCON,MORINCON,MONUMGIO,MOCODCEN,MOCOCOMM,MOVOCCEN,MOCODATT,MOTIPCON,MOFLATTI,MOFLFATT"+;
        " from "+i_cTable+" MOD_ELEM where ";
            +"MOCODICE = "+cp_ToStrODBC(this.pELCODMOD);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MOCAUDOC,MOTIPATT,MOGRUPAR,MOCODLIS,MOESCRIN,MOPERCON,MORINCON,MONUMGIO,MOCODCEN,MOCOCOMM,MOVOCCEN,MOCODATT,MOTIPCON,MOFLATTI,MOFLFATT;
        from (i_cTable) where;
            MOCODICE = this.pELCODMOD;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MOCAUDOC = NVL(cp_ToDate(_read_.MOCAUDOC),cp_NullValue(_read_.MOCAUDOC))
      this.w_MOTIPATT = NVL(cp_ToDate(_read_.MOTIPATT),cp_NullValue(_read_.MOTIPATT))
      this.w_MOGRUPAR = NVL(cp_ToDate(_read_.MOGRUPAR),cp_NullValue(_read_.MOGRUPAR))
      this.w_MOCODLIS = NVL(cp_ToDate(_read_.MOCODLIS),cp_NullValue(_read_.MOCODLIS))
      this.w_MOESCRIN = NVL(cp_ToDate(_read_.MOESCRIN),cp_NullValue(_read_.MOESCRIN))
      this.w_MOPERCON = NVL(cp_ToDate(_read_.MOPERCON),cp_NullValue(_read_.MOPERCON))
      this.w_MORINCON = NVL(cp_ToDate(_read_.MORINCON),cp_NullValue(_read_.MORINCON))
      this.w_MONUMGIO = NVL(cp_ToDate(_read_.MONUMGIO),cp_NullValue(_read_.MONUMGIO))
      this.w_MOCODCEN = NVL(cp_ToDate(_read_.MOCODCEN),cp_NullValue(_read_.MOCODCEN))
      this.w_MOCOCOMM = NVL(cp_ToDate(_read_.MOCOCOMM),cp_NullValue(_read_.MOCOCOMM))
      this.w_MOVOCCEN = NVL(cp_ToDate(_read_.MOVOCCEN),cp_NullValue(_read_.MOVOCCEN))
      this.w_MOCODATT = NVL(cp_ToDate(_read_.MOCODATT),cp_NullValue(_read_.MOCODATT))
      this.w_MOTIPCON = NVL(cp_ToDate(_read_.MOTIPCON),cp_NullValue(_read_.MOTIPCON))
      this.w_FLATTI = NVL(cp_ToDate(_read_.MOFLATTI),cp_NullValue(_read_.MOFLATTI))
      this.w_FLATT = NVL(cp_ToDate(_read_.MOFLFATT),cp_NullValue(_read_.MOFLFATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Legge i dati dall'elemento da aggiornare, in modo da mantenere il valore corrente se non specificato nel contratto o nel modello
    * --- Read from ELE_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2],.t.,this.ELE_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ELCAUDOC,ELTIPATT,ELGRUPAR,ELCODLIS,ELESCRIN,ELPERCON,ELRINCON,ELNUMGIO,ELCODCEN,ELCOCOMM,ELVOCCEN,ELCODATT,ELDATDIS,ELDATFIN,ELCODPAG"+;
        " from "+i_cTable+" ELE_CONT where ";
            +"ELCONTRA = "+cp_ToStrODBC(this.pELCONTRA);
            +" and ELCODMOD = "+cp_ToStrODBC(this.pELCODMOD);
            +" and ELCODIMP = "+cp_ToStrODBC(this.pELCODIMP);
            +" and ELCODCOM = "+cp_ToStrODBC(this.pELCODCOM);
            +" and ELRINNOV = "+cp_ToStrODBC(this.pELRINNOV);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ELCAUDOC,ELTIPATT,ELGRUPAR,ELCODLIS,ELESCRIN,ELPERCON,ELRINCON,ELNUMGIO,ELCODCEN,ELCOCOMM,ELVOCCEN,ELCODATT,ELDATDIS,ELDATFIN,ELCODPAG;
        from (i_cTable) where;
            ELCONTRA = this.pELCONTRA;
            and ELCODMOD = this.pELCODMOD;
            and ELCODIMP = this.pELCODIMP;
            and ELCODCOM = this.pELCODCOM;
            and ELRINNOV = this.pELRINNOV;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ELCAUDOC = NVL(cp_ToDate(_read_.ELCAUDOC),cp_NullValue(_read_.ELCAUDOC))
      this.w_ELTIPATT = NVL(cp_ToDate(_read_.ELTIPATT),cp_NullValue(_read_.ELTIPATT))
      this.w_ELGRUPAR = NVL(cp_ToDate(_read_.ELGRUPAR),cp_NullValue(_read_.ELGRUPAR))
      this.w_ELCODLIS = NVL(cp_ToDate(_read_.ELCODLIS),cp_NullValue(_read_.ELCODLIS))
      this.w_ELESCRIN = NVL(cp_ToDate(_read_.ELESCRIN),cp_NullValue(_read_.ELESCRIN))
      this.w_ELPERCON = NVL(cp_ToDate(_read_.ELPERCON),cp_NullValue(_read_.ELPERCON))
      this.w_ELRINCON_OLD = NVL(cp_ToDate(_read_.ELRINCON),cp_NullValue(_read_.ELRINCON))
      this.w_ELNUMGIO = NVL(cp_ToDate(_read_.ELNUMGIO),cp_NullValue(_read_.ELNUMGIO))
      this.w_ELCODCEN = NVL(cp_ToDate(_read_.ELCODCEN),cp_NullValue(_read_.ELCODCEN))
      this.w_ELCOCOMM = NVL(cp_ToDate(_read_.ELCOCOMM),cp_NullValue(_read_.ELCOCOMM))
      this.w_ELVOCCEN = NVL(cp_ToDate(_read_.ELVOCCEN),cp_NullValue(_read_.ELVOCCEN))
      this.w_ELCODATT = NVL(cp_ToDate(_read_.ELCODATT),cp_NullValue(_read_.ELCODATT))
      this.w_ELDATDIS = NVL(cp_ToDate(_read_.ELDATDIS),cp_NullValue(_read_.ELDATDIS))
      this.w_ELDATFIN = NVL(cp_ToDate(_read_.ELDATFIN),cp_NullValue(_read_.ELDATFIN))
      this.w_ELCODPAG = NVL(cp_ToDate(_read_.ELCODPAG),cp_NullValue(_read_.ELCODPAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Utilizza il dato nel contratto se presente , altrimenti usa il dato nel modello
    * --- Causale documento
    if this.w_FLATT="S"
      if NOT EMPTY( NVL( this.w_COTIPDOC, "" ) )
        * --- Utilizza il dato nel contratto
        this.w_ELCAUDOC = this.w_COTIPDOC
      else
        if NOT EMPTY( NVL( this.w_MOCAUDOC, "" ) )
          * --- Utilizza il dato del modello
          this.w_ELCAUDOC = NVL( this.w_MOCAUDOC, "     " )
        else
          * --- Mantiene il dato corrente
          this.w_ELCAUDOC = NVL( this.w_ELCAUDOC, "     " )
        endif
      endif
      * --- Codice del pagamento
      if NOT EMPTY( NVL( this.w_COCODPAG, "" ) )
        * --- Utilizza il dato nel contratto
        this.w_ELCODPAG = NVL( this.w_COCODPAG, SPACE( 5 ) )
      else
        * --- Mantiene il dato corrente
        this.w_ELCODPAG = NVL( this.w_ELCODPAG, SPACE( 5 ) )
      endif
    endif
    * --- Tipo attivit�
    if this.w_FLATTI="S"
      if NOT EMPTY( NVL( this.w_COTIPATT, "" ) )
        * --- Utilizza il dato nel contratto
        this.w_ELTIPATT = this.w_COTIPATT
      else
        if NOT EMPTY( NVL( this.w_MOTIPATT, "" ) )
          * --- Utilizza il dato del modello
          this.w_ELTIPATT = NVL( this.w_MOTIPATT, "     " )
        else
          * --- Mantiene il dato corrente
          this.w_ELTIPATT = NVL( this.w_ELTIPATT, "     " )
        endif
      endif
      * --- Gruppo
      if NOT EMPTY( NVL( this.w_COGRUPAR, "" ) )
        * --- Utilizza il dato nel contratto
        this.w_ELGRUPAR = this.w_COGRUPAR
      else
        if NOT EMPTY( NVL( this.w_MOGRUPAR, "" ) )
          * --- Utilizza il dato del modello
          this.w_ELGRUPAR = NVL( this.w_MOGRUPAR, "     " )
        else
          * --- Mantiene il dato corrente
          this.w_ELGRUPAR = NVL( this.w_ELGRUPAR, "     " )
        endif
      endif
    endif
    * --- Listino
    if NOT EMPTY( NVL( this.w_COCODLIS, "" ) )
      * --- Utilizza il dato nel contratto
      this.w_ELCODLIS = this.w_COCODLIS
    else
      if NOT EMPTY( NVL( this.w_MOCODLIS, "" ) )
        * --- Utilizza il dato del modello
        this.w_ELCODLIS = NVL( this.w_MOCODLIS, "     " )
      else
        * --- Mantiene il dato corrente
        this.w_ELCODLIS = NVL( this.w_ELCODLIS, "     " )
      endif
    endif
    * --- Esclude rinnovo
    if NVL( this.w_COESCRIN, "M" ) <> "M"
      * --- Utilizza il dato nel contratto
      this.w_ELESCRIN = this.w_COESCRIN
    else
      * --- w_COESCRIN = "M" : da modello
      * --- Utilizza il dato del modello
      this.w_ELESCRIN = NVL( this.w_MOESCRIN, "N" )
    endif
    * --- Periodicit� rinnovi
    if NOT EMPTY( NVL( this.w_COPERCON, "" ) )
      * --- Utilizza il dato nel contratto
      this.w_ELPERCON = TRIM( NVL( this.w_COPERCON, "   " ) )
    else
      if NOT EMPTY( NVL( this.w_MOPERCON, "" ) )
        * --- Utilizza il dato del modello
        this.w_ELPERCON = TRIM( NVL( this.w_MOPERCON, "   " ) )
      else
        * --- Mantiene il dato corrente
        this.w_ELPERCON = TRIM( NVL( this.w_ELPERCON, "   " ) )
      endif
    endif
    * --- Rinnovo tacito
    if NVL( this.w_CORINCON, "M" ) <> "M"
      * --- Utilizza il dato nel contratto
      this.w_ELRINCON = this.w_CORINCON
    else
      * --- w_COESCRIN = "M" : da modello
      * --- Utilizza il dato del modello
      this.w_ELRINCON = NVL( this.w_MORINCON, "N" )
    endif
    * --- Giorni di preavviso disdetta
    if NVL(this.w_CONUMGIO, 0 ) <> 0
      * --- Utilizza il dato nel contratto
      this.w_ELNUMGIO = this.w_CONUMGIO
    else
      if NVL(this.w_MONUMGIO, 0 ) <> 0
        * --- Utilizza il dato del modello
        this.w_ELNUMGIO = NVL( this.w_MONUMGIO, 0 )
      else
        * --- Mantiene il dato corrente
        this.w_ELNUMGIO = NVL( this.w_ELNUMGIO, 0 )
      endif
    endif
    if this.w_ELESCRIN = "S"
      * --- Se il flag di esclusione rinnovo � disattivato i dati relativi al rinnovo devono essere sbiancati
      this.w_ELPERCON = "   "
      this.w_ELRINCON = "N"
      this.w_ELNUMGIO = 0
    endif
    if this.w_ELRINCON = "S"
      * --- Se viene attivato il check Rinnovo tacito occorre calcolare la data limite disdetta in base ai gg di preavviso (che possono rimanere anche a zero)
      *     w_ELRINCON_OLD
      *     w_ELDATDIS
      *     w_ELDATFIN
      *     sono letti all'inizio della routine dal record da aggiornare
      if this.w_ELRINCON_OLD <> "S"
        this.w_ELDATDIS = this.w_ELDATFIN - this.w_ELNUMGIO
      else
        this.w_ELDATDIS = NVL( this.w_ELDATDIS, CP_CHARTODATE("  -  -  ") )
      endif
    else
      * --- Il rinnovo tacito non � attivo, i giorni di preavviso della disdetta e la data limite devono essere sbiancati
      this.w_ELNUMGIO = 0
      this.w_ELDATDIS = CP_CHARTODATE("  -  -  ")
    endif
    * --- Centro di costo
    if NOT EMPTY( NVL( this.w_COCODCEN, "" ) )
      * --- Utilizza il dato nel contratto
      this.w_ELCODCEN = NVL( this.w_COCODCEN, SPACE( 15 ) )
    else
      if NOT EMPTY( NVL( this.w_MOCODCEN, "" ) )
        * --- Utilizza il dato del modello
        this.w_ELCODCEN = NVL( this.w_MOCODCEN, SPACE( 15 ) )
      else
        * --- Mantiene il dato corrente
        this.w_ELCODCEN = NVL( this.w_ELCODCEN, SPACE( 15 ) )
      endif
    endif
    * --- Commessa
    * --- w_ELCOCOMM: commessa gi� presente sull'archivio
    *     w_ELCOCOMM_1: commessa da scrivere
    if NOT EMPTY( NVL( this.w_COCOCOMM, "" ) )
      * --- Utilizza il dato nel contratto
      this.w_ELCOCOMM_1 = NVL( this.w_COCOCOMM, SPACE( 15 ) )
    else
      if NOT EMPTY( NVL( this.w_MOCOCOMM, "" ) )
        * --- Utilizza il dato del modello
        this.w_ELCOCOMM_1 = NVL( this.w_MOCOCOMM, SPACE( 15 ) )
      else
        * --- Mantiene il dato corrente
        this.w_ELCOCOMM_1 = NVL( this.w_ELCOCOMM, SPACE( 15 ) )
      endif
    endif
    * --- Voce di costo
    if NOT EMPTY( NVL( this.w_MOVOCCEN, "" ) )
      * --- Utilizza il dato del modello
      this.w_ELVOCCEN = NVL( this.w_MOVOCCEN, SPACE( 15 ) )
    else
      * --- Mantiene il dato corrente
      this.w_ELVOCCEN = NVL( this.w_ELVOCCEN, SPACE( 15 ) )
    endif
    * --- Attivit� di commessa
    if NOT EMPTY( NVL( this.w_MOCODATT, "" ) )
      * --- Utilizza il dato del modello
      this.w_ELCODATT = NVL( this.w_MOCODATT, SPACE( 15 ) )
    else
      * --- Mantiene il dato corrente
      this.w_ELCODATT = NVL( this.w_ELCODATT, SPACE( 15 ) )
    endif
    * --- Se la commessa �stata cambiata bisogna cancellare l'attivit�
    if this.w_ELCOCOMM_1 <> NVL( this.w_ELCOCOMM, SPACE( 15 ) )
      this.w_ELCODATT = SPACE( 15 )
    endif
    * --- Aggiorna i dati dell'elemento contratto
    * --- Write into ELE_CONT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ELCAUDOC ="+cp_NullLink(cp_ToStrODBC(this.w_ELCAUDOC),'ELE_CONT','ELCAUDOC');
      +",ELTIPATT ="+cp_NullLink(cp_ToStrODBC(IIF(this.w_MOTIPCON<>"P", this.w_ELTIPATT, SPACE(20) )),'ELE_CONT','ELTIPATT');
      +",ELGRUPAR ="+cp_NullLink(cp_ToStrODBC(IIF(this.w_MOTIPCON<>"P", this.w_ELGRUPAR, SPACE(5) )),'ELE_CONT','ELGRUPAR');
      +",ELCODLIS ="+cp_NullLink(cp_ToStrODBC(this.w_ELCODLIS),'ELE_CONT','ELCODLIS');
      +",ELESCRIN ="+cp_NullLink(cp_ToStrODBC(this.w_ELESCRIN),'ELE_CONT','ELESCRIN');
      +",ELPERCON ="+cp_NullLink(cp_ToStrODBC(this.w_ELPERCON),'ELE_CONT','ELPERCON');
      +",ELRINCON ="+cp_NullLink(cp_ToStrODBC(this.w_ELRINCON),'ELE_CONT','ELRINCON');
      +",ELNUMGIO ="+cp_NullLink(cp_ToStrODBC(this.w_ELNUMGIO),'ELE_CONT','ELNUMGIO');
      +",ELVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_ELVOCCEN),'ELE_CONT','ELVOCCEN');
      +",ELCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_ELCODCEN),'ELE_CONT','ELCODCEN');
      +",ELCOCOMM ="+cp_NullLink(cp_ToStrODBC(this.w_ELCOCOMM_1),'ELE_CONT','ELCOCOMM');
      +",ELCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_ELCODATT),'ELE_CONT','ELCODATT');
      +",ELDATDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ELDATDIS),'ELE_CONT','ELDATDIS');
      +",ELCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_ELCODPAG),'ELE_CONT','ELCODPAG');
          +i_ccchkf ;
      +" where ";
          +"ELCONTRA = "+cp_ToStrODBC(this.pELCONTRA);
          +" and ELCODMOD = "+cp_ToStrODBC(this.pELCODMOD);
          +" and ELCODIMP = "+cp_ToStrODBC(this.pELCODIMP);
          +" and ELCODCOM = "+cp_ToStrODBC(this.pELCODCOM);
          +" and ELRINNOV = "+cp_ToStrODBC(this.pELRINNOV);
             )
    else
      update (i_cTable) set;
          ELCAUDOC = this.w_ELCAUDOC;
          ,ELTIPATT = IIF(this.w_MOTIPCON<>"P", this.w_ELTIPATT, SPACE(20) );
          ,ELGRUPAR = IIF(this.w_MOTIPCON<>"P", this.w_ELGRUPAR, SPACE(5) );
          ,ELCODLIS = this.w_ELCODLIS;
          ,ELESCRIN = this.w_ELESCRIN;
          ,ELPERCON = this.w_ELPERCON;
          ,ELRINCON = this.w_ELRINCON;
          ,ELNUMGIO = this.w_ELNUMGIO;
          ,ELVOCCEN = this.w_ELVOCCEN;
          ,ELCODCEN = this.w_ELCODCEN;
          ,ELCOCOMM = this.w_ELCOCOMM_1;
          ,ELCODATT = this.w_ELCODATT;
          ,ELDATDIS = this.w_ELDATDIS;
          ,ELCODPAG = this.w_ELCODPAG;
          &i_ccchkf. ;
       where;
          ELCONTRA = this.pELCONTRA;
          and ELCODMOD = this.pELCODMOD;
          and ELCODIMP = this.pELCODIMP;
          and ELCODCOM = this.pELCODCOM;
          and ELRINNOV = this.pELRINNOV;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,pELCONTRA,pELCODMOD,pELCODIMP,pELCODCOM,pELRINNOV)
    this.pELCONTRA=pELCONTRA
    this.pELCODMOD=pELCODMOD
    this.pELCODIMP=pELCODIMP
    this.pELCODCOM=pELCODCOM
    this.pELRINNOV=pELRINNOV
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MOD_ELEM'
    this.cWorkTables[2]='CON_TRAS'
    this.cWorkTables[3]='ELE_CONT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pELCONTRA,pELCODMOD,pELCODIMP,pELCODCOM,pELRINNOV"
endproc
