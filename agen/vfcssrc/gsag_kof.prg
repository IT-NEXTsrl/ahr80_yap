* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kof                                                        *
*              Dati Offerte                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-12                                                      *
* Last revis.: 2014-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kof",oParentObject))

* --- Class definition
define class tgsag_kof as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 590
  Height = 352
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-19"
  HelpContextID=166340457
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  OFF_ESIA_IDX = 0
  OFF_ERTE_IDX = 0
  cpusers_IDX = 0
  CPUSERS_IDX = 0
  NOM_CONT_IDX = 0
  cPrg = "gsag_kof"
  cComment = "Dati Offerte"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ATCODNOM = space(15)
  w_OFCODNOM      = space(15)
  w_ATOPERAT = 0
  w_ATNUMPRI = 0
  w_OFSTATUS = space(1)
  w_ATCONTAT = space(5)
  w_OFNUMDOC = 0
  w_OFDATDOC = ctod('  /  /  ')
  w_OFSERDOC = space(10)
  w_ATOPPOFF = space(10)
  w_AT_ESITO = space(0)
  w_ATCODESI = space(10)
  w_NAME = space(20)
  w_NCPERSON = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kofPag1","gsag_kof",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATOPERAT_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='OFF_ESIA'
    this.cWorkTables[2]='OFF_ERTE'
    this.cWorkTables[3]='cpusers'
    this.cWorkTables[4]='CPUSERS'
    this.cWorkTables[5]='NOM_CONT'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ATCODNOM=space(15)
      .w_OFCODNOM     =space(15)
      .w_ATOPERAT=0
      .w_ATNUMPRI=0
      .w_OFSTATUS=space(1)
      .w_ATCONTAT=space(5)
      .w_OFNUMDOC=0
      .w_OFDATDOC=ctod("  /  /  ")
      .w_OFSERDOC=space(10)
      .w_ATOPPOFF=space(10)
      .w_AT_ESITO=space(0)
      .w_ATCODESI=space(10)
      .w_NAME=space(20)
      .w_NCPERSON=space(40)
      .w_ATCODNOM=oParentObject.w_ATCODNOM
      .w_ATOPERAT=oParentObject.w_ATOPERAT
      .w_ATNUMPRI=oParentObject.w_ATNUMPRI
      .w_ATCONTAT=oParentObject.w_ATCONTAT
      .w_ATOPPOFF=oParentObject.w_ATOPPOFF
      .w_AT_ESITO=oParentObject.w_AT_ESITO
      .w_ATCODESI=oParentObject.w_ATCODESI
          .DoRTCalc(1,1,.f.)
        .w_OFCODNOM      = .w_ATCODNOM
        .w_ATOPERAT = i_CODUTE
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ATOPERAT))
          .link_1_5('Full')
        endif
        .DoRTCalc(4,6,.f.)
        if not(empty(.w_ATCONTAT))
          .link_1_10('Full')
        endif
        .DoRTCalc(7,10,.f.)
        if not(empty(.w_ATOPPOFF))
          .link_1_19('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_ATCODESI))
          .link_1_21('Full')
        endif
    endwith
    this.DoRTCalc(13,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oATOPERAT_1_5.enabled = i_bVal
      .Page1.oPag.oATCONTAT_1_10.enabled = i_bVal
      .Page1.oPag.oATOPPOFF_1_19.enabled = i_bVal
      .Page1.oPag.oAT_ESITO_1_20.enabled = i_bVal
      .Page1.oPag.oATCODESI_1_21.enabled = i_bVal
      .Page1.oPag.oBtn_1_3.enabled = .Page1.oPag.oBtn_1_3.mCond()
      .Page1.oPag.oBtn_1_4.enabled = .Page1.oPag.oBtn_1_4.mCond()
      .Page1.oPag.oBtn_1_8.enabled = i_bVal
      .Page1.oPag.oBtn_1_28.enabled = .Page1.oPag.oBtn_1_28.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ATCODNOM=.w_ATCODNOM
      .oParentObject.w_ATOPERAT=.w_ATOPERAT
      .oParentObject.w_ATNUMPRI=.w_ATNUMPRI
      .oParentObject.w_ATCONTAT=.w_ATCONTAT
      .oParentObject.w_ATOPPOFF=.w_ATOPPOFF
      .oParentObject.w_AT_ESITO=.w_AT_ESITO
      .oParentObject.w_ATCODESI=.w_ATCODESI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_OFCODNOM      = .w_ATCODNOM
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .cComment = IIF(Isalt(),AH_MSGFORMAT("Dati preventivo"),AH_MSGFORMAT("Dati offerte"))
          .Caption = .cComment
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATOPERAT_1_5.enabled = this.oPgFrm.Page1.oPag.oATOPERAT_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_3.visible=!this.oPgFrm.Page1.oPag.oBtn_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oATCONTAT_1_10.visible=!this.oPgFrm.Page1.oPag.oATCONTAT_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oATOPPOFF_1_19.visible=!this.oPgFrm.Page1.oPag.oATOPPOFF_1_19.mHide()
    this.oPgFrm.Page1.oPag.oAT_ESITO_1_20.visible=!this.oPgFrm.Page1.oPag.oAT_ESITO_1_20.mHide()
    this.oPgFrm.Page1.oPag.oATCODESI_1_21.visible=!this.oPgFrm.Page1.oPag.oATCODESI_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oNAME_1_23.visible=!this.oPgFrm.Page1.oPag.oNAME_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oNCPERSON_1_25.visible=!this.oPgFrm.Page1.oPag.oNCPERSON_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_28.visible=!this.oPgFrm.Page1.oPag.oBtn_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ATOPERAT
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATOPERAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_ATOPERAT);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_ATOPERAT)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_ATOPERAT) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oATOPERAT_1_5'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATOPERAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_ATOPERAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_ATOPERAT)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATOPERAT = NVL(_Link_.CODE,0)
      this.w_NAME = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_ATOPERAT = 0
      endif
      this.w_NAME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATOPERAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCONTAT
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCONTAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_ATCODNOM;
                     ,'NCCODCON',trim(this.w_ATCONTAT))
          select NCCODICE,NCCODCON,NCPERSON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCONTAT)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCONTAT) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oATCONTAT_1_10'),i_cWhere,'',"Contatti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODNOM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCONTAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_ATCONTAT);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_ATCODNOM;
                       ,'NCCODCON',this.w_ATCONTAT)
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCONTAT = NVL(_Link_.NCCODCON,space(5))
      this.w_NCPERSON = NVL(_Link_.NCPERSON,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ATCONTAT = space(5)
      endif
      this.w_NCPERSON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCONTAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATOPPOFF
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    i_lTable = "OFF_ERTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2], .t., this.OFF_ERTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATOPPOFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AOF',True,'OFF_ERTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OFSERIAL like "+cp_ToStrODBC(trim(this.w_ATOPPOFF)+"%");
                   +" and OFCODNOM="+cp_ToStrODBC(this.w_ATCODNOM);

          i_ret=cp_SQL(i_nConn,"select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OFCODNOM,OFSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OFCODNOM',this.w_ATCODNOM;
                     ,'OFSERIAL',trim(this.w_ATOPPOFF))
          select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OFCODNOM,OFSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATOPPOFF)==trim(_Link_.OFSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATOPPOFF) and !this.bDontReportError
            deferred_cp_zoom('OFF_ERTE','*','OFCODNOM,OFSERIAL',cp_AbsName(oSource.parent,'oATOPPOFF_1_19'),i_cWhere,'GSOF_AOF',"Opportunita/offerte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODNOM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                     +" from "+i_cTable+" "+i_lTable+" where OFSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and OFCODNOM="+cp_ToStrODBC(this.w_ATCODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OFCODNOM',oSource.xKey(1);
                       ,'OFSERIAL',oSource.xKey(2))
            select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATOPPOFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                   +" from "+i_cTable+" "+i_lTable+" where OFSERIAL="+cp_ToStrODBC(this.w_ATOPPOFF);
                   +" and OFCODNOM="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OFCODNOM',this.w_ATCODNOM;
                       ,'OFSERIAL',this.w_ATOPPOFF)
            select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATOPPOFF = NVL(_Link_.OFSERIAL,space(10))
      this.w_OFNUMDOC = NVL(_Link_.OFNUMDOC,0)
      this.w_OFDATDOC = NVL(cp_ToDate(_Link_.OFDATDOC),ctod("  /  /  "))
      this.w_OFSERDOC = NVL(_Link_.OFSERDOC,space(10))
      this.w_OFSTATUS = NVL(_Link_.OFSTATUS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATOPPOFF = space(10)
      endif
      this.w_OFNUMDOC = 0
      this.w_OFDATDOC = ctod("  /  /  ")
      this.w_OFSERDOC = space(10)
      this.w_OFSTATUS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])+'\'+cp_ToStr(_Link_.OFCODNOM,1)+'\'+cp_ToStr(_Link_.OFSERIAL,1)
      cp_ShowWarn(i_cKey,this.OFF_ERTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATOPPOFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODESI
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_ESIA_IDX,3]
    i_lTable = "OFF_ESIA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ESIA_IDX,2], .t., this.OFF_ESIA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ESIA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODESI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AES',True,'OFF_ESIA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESI like "+cp_ToStrODBC(trim(this.w_ATCODESI)+"%");

          i_ret=cp_SQL(i_nConn,"select ESCODESI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODESI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODESI',trim(this.w_ATCODESI))
          select ESCODESI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODESI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODESI)==trim(_Link_.ESCODESI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODESI) and !this.bDontReportError
            deferred_cp_zoom('OFF_ESIA','*','ESCODESI',cp_AbsName(oSource.parent,'oATCODESI_1_21'),i_cWhere,'GSOF_AES',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODESI";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODESI',oSource.xKey(1))
            select ESCODESI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODESI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODESI";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESI="+cp_ToStrODBC(this.w_ATCODESI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODESI',this.w_ATCODESI)
            select ESCODESI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODESI = NVL(_Link_.ESCODESI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODESI = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_ESIA_IDX,2])+'\'+cp_ToStr(_Link_.ESCODESI,1)
      cp_ShowWarn(i_cKey,this.OFF_ESIA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODESI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATOPERAT_1_5.value==this.w_ATOPERAT)
      this.oPgFrm.Page1.oPag.oATOPERAT_1_5.value=this.w_ATOPERAT
    endif
    if not(this.oPgFrm.Page1.oPag.oOFSTATUS_1_9.RadioValue()==this.w_OFSTATUS)
      this.oPgFrm.Page1.oPag.oOFSTATUS_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCONTAT_1_10.value==this.w_ATCONTAT)
      this.oPgFrm.Page1.oPag.oATCONTAT_1_10.value=this.w_ATCONTAT
    endif
    if not(this.oPgFrm.Page1.oPag.oOFNUMDOC_1_11.value==this.w_OFNUMDOC)
      this.oPgFrm.Page1.oPag.oOFNUMDOC_1_11.value=this.w_OFNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDATDOC_1_12.value==this.w_OFDATDOC)
      this.oPgFrm.Page1.oPag.oOFDATDOC_1_12.value=this.w_OFDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFSERDOC_1_13.value==this.w_OFSERDOC)
      this.oPgFrm.Page1.oPag.oOFSERDOC_1_13.value=this.w_OFSERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oATOPPOFF_1_19.value==this.w_ATOPPOFF)
      this.oPgFrm.Page1.oPag.oATOPPOFF_1_19.value=this.w_ATOPPOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oAT_ESITO_1_20.value==this.w_AT_ESITO)
      this.oPgFrm.Page1.oPag.oAT_ESITO_1_20.value=this.w_AT_ESITO
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODESI_1_21.RadioValue()==this.w_ATCODESI)
      this.oPgFrm.Page1.oPag.oATCODESI_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNAME_1_23.value==this.w_NAME)
      this.oPgFrm.Page1.oPag.oNAME_1_23.value=this.w_NAME
    endif
    if not(this.oPgFrm.Page1.oPag.oNCPERSON_1_25.value==this.w_NCPERSON)
      this.oPgFrm.Page1.oPag.oNCPERSON_1_25.value=this.w_NCPERSON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_kofPag1 as StdContainer
  Width  = 819
  height = 352
  stdWidth  = 819
  stdheight = 352
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_3 as StdButton with uid="PVRNPREXQE",left=477, top=302, width=48,height=45,;
    CpPicture="bmp\AltreOff.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire l'offerta collegata";
    , HelpContextID = 241064986;
    , caption='\<Offerte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        OpenGest('A', iif(Isalt(),'GSPR_AOF','GSOF_AOF'), 'OFSERIAL', .w_ATOPPOFF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_ATOPPOFF))
      endwith
    endif
  endfunc

  func oBtn_1_3.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ATOPPOFF)  or Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_4 as StdButton with uid="AXAXNIQDQZ",left=529, top=302, width=48,height=45,;
    CpPicture="bmp\ctegorie.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il nominativo collegato";
    , HelpContextID = 241601951;
    , caption='\<Nominat.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSAR_ANO', 'NOCODICE', .w_ATCODNOM)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_ATCODNOM))
      endwith
    endif
  endfunc

  add object oATOPERAT_1_5 as StdField with uid="KYMLENJJDT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ATOPERAT", cQueryName = "ATOPERAT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente/operatore",;
    HelpContextID = 213590362,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=119, Top=18, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_ATOPERAT"

  func oATOPERAT_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (CP_ISADMINISTRATOR())
    endwith
   endif
  endfunc

  func oATOPERAT_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oATOPERAT_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATOPERAT_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oATOPERAT_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oBtn_1_8 as StdButton with uid="CXRFQHUCZY",left=494, top=47, width=22,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare gestione da collegare";
    , HelpContextID = 166139434;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        LinkOfferte(This.Parent.oContained)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oOFSTATUS_1_9 as StdCombo with uid="WJAVJWDSCL",rtseq=5,rtrep=.f.,left=120,top=79,width=115,height=21, enabled=.f.;
    , ToolTipText = "Stato ";
    , HelpContextID = 243225657;
    , cFormVar="w_OFSTATUS",RowSource=""+"In corso,"+"Inviata,"+"Confermata,"+"Versione chiusa,"+"Rifiutata,"+"Sospesa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFSTATUS_1_9.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'V',;
    iif(this.value =5,'R',;
    iif(this.value =6,'S',;
    space(1))))))))
  endfunc
  func oOFSTATUS_1_9.GetRadio()
    this.Parent.oContained.w_OFSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oOFSTATUS_1_9.SetRadio()
    this.Parent.oContained.w_OFSTATUS=trim(this.Parent.oContained.w_OFSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_OFSTATUS=='I',1,;
      iif(this.Parent.oContained.w_OFSTATUS=='A',2,;
      iif(this.Parent.oContained.w_OFSTATUS=='C',3,;
      iif(this.Parent.oContained.w_OFSTATUS=='V',4,;
      iif(this.Parent.oContained.w_OFSTATUS=='R',5,;
      iif(this.Parent.oContained.w_OFSTATUS=='S',6,;
      0))))))
  endfunc

  add object oATCONTAT_1_10 as StdField with uid="EFJRRWMVHE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ATCONTAT", cQueryName = "ATCONTAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice contatto",;
    HelpContextID = 256467290,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=120, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCCODICE", oKey_1_2="this.w_ATCODNOM", oKey_2_1="NCCODCON", oKey_2_2="this.w_ATCONTAT"

  func oATCONTAT_1_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oATCONTAT_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCONTAT_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCONTAT_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.NOM_CONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_ATCODNOM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStr(this.Parent.oContained.w_ATCODNOM)
    endif
    do cp_zoom with 'NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(this.parent,'oATCONTAT_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Contatti",'',this.parent.oContained
  endproc

  add object oOFNUMDOC_1_11 as StdField with uid="LQFYCLYDMU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_OFNUMDOC", cQueryName = "OFNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 255853609,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=120, Top=47

  add object oOFDATDOC_1_12 as StdField with uid="SCFATDDIMX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OFDATDOC", cQueryName = "OFDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 261841961,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=415, Top=47

  add object oOFSERDOC_1_13 as StdField with uid="LRTHJETKKI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_OFSERDOC", cQueryName = "OFSERDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 260068393,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=265, Top=47, InputMask=replicate('X',10)

  add object oATOPPOFF_1_19 as StdField with uid="ZXXXPVPGLR",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ATOPPOFF", cQueryName = "ATOPPOFF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 93642420,;
   bGlobalFont=.t.,;
    Height=21, Width=101, Left=718, Top=9, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="OFF_ERTE", cZoomOnZoom="GSOF_AOF", oKey_1_1="OFCODNOM", oKey_1_2="this.w_ATCODNOM", oKey_2_1="OFSERIAL", oKey_2_2="this.w_ATOPPOFF"

  func oATOPPOFF_1_19.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oATOPPOFF_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oATOPPOFF_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATOPPOFF_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.OFF_ERTE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OFCODNOM="+cp_ToStrODBC(this.Parent.oContained.w_ATCODNOM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OFCODNOM="+cp_ToStr(this.Parent.oContained.w_ATCODNOM)
    endif
    do cp_zoom with 'OFF_ERTE','*','OFCODNOM,OFSERIAL',cp_AbsName(this.parent,'oATOPPOFF_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AOF',"Opportunita/offerte",'',this.parent.oContained
  endproc
  proc oATOPPOFF_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AOF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.OFCODNOM=w_ATCODNOM
     i_obj.w_OFSERIAL=this.parent.oContained.w_ATOPPOFF
     i_obj.ecpSave()
  endproc

  add object oAT_ESITO_1_20 as StdMemo with uid="LNLABEIHEB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_AT_ESITO", cQueryName = "AT_ESITO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 76620117,;
   bGlobalFont=.t.,;
    Height=124, Width=563, Left=14, Top=165

  func oAT_ESITO_1_20.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oATCODESI_1_21 as StdTableCombo with uid="LOHEAKGLII",rtseq=12,rtrep=.f.,left=415,top=79,width=162,height=21;
    , ToolTipText = "Esito attivit�";
    , HelpContextID = 262758735;
    , cFormVar="w_ATCODESI",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="OFF_ESIA";
    , cTable='OFF_ESIA',cKey='ESCODESI',cValue='ESDESCRI',cOrderBy='ESCODESI',xDefault=space(10);
  , bGlobalFont=.t.


  func oATCODESI_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oATCODESI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODESI_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oNAME_1_23 as StdField with uid="OMTZYHNIFK",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NAME", cQueryName = "NAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente/operatore",;
    HelpContextID = 161485098,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=200, Top=18, InputMask=replicate('X',20)

  func oNAME_1_23.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oNCPERSON_1_25 as StdField with uid="EKYIMCOMHX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NCPERSON", cQueryName = "NCPERSON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contatto",;
    HelpContextID = 243278116,;
   bGlobalFont=.t.,;
    Height=21, Width=390, Left=187, Top=108, InputMask=replicate('X',40)

  func oNCPERSON_1_25.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oBtn_1_28 as StdButton with uid="UJKLGNCQLT",left=477, top=302, width=48,height=45,;
    CpPicture="bmp\AltreOff.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire preventivo collegato";
    , HelpContextID = 85782337;
    , caption='\<Prev.i';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        OpenGest('A', iif(Isalt(),'GSPR_AOF','GSOF_AOF'), 'OFSERIAL', .w_ATOPPOFF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_ATOPPOFF))
      endwith
    endif
  endfunc

  func oBtn_1_28.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ATOPPOFF) or !Isalt())
     endwith
    endif
  endfunc

  add object oStr_1_6 as StdString with uid="EHJVTZDRGG",Visible=.t., Left=7, Top=18,;
    Alignment=1, Width=109, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="PDAGFAJEHK",Visible=.t., Left=241, Top=47,;
    Alignment=2, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="MSWKBXMGNU",Visible=.t., Left=7, Top=79,;
    Alignment=1, Width=109, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="RHWKDBVNLE",Visible=.t., Left=354, Top=47,;
    Alignment=1, Width=58, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="RJNIIFDFWJ",Visible=.t., Left=7, Top=47,;
    Alignment=1, Width=109, Height=18,;
    Caption="Opportunit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_OFSTATUS<>'I' or Isalt())
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="VINNSDQUWW",Visible=.t., Left=7, Top=48,;
    Alignment=1, Width=109, Height=18,;
    Caption="Offerta:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_OFSTATUS='I' or Isalt())
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="LCLBOVZIPX",Visible=.t., Left=266, Top=80,;
    Alignment=1, Width=146, Height=18,;
    Caption="Esito attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="MUMEBTGAUR",Visible=.t., Left=67, Top=108,;
    Alignment=0, Width=49, Height=18,;
    Caption="Contatto:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="OEAEDHCCAV",Visible=.t., Left=17, Top=146,;
    Alignment=0, Width=271, Height=18,;
    Caption="Note esito"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="CAQRFHNXFO",Visible=.t., Left=7, Top=47,;
    Alignment=1, Width=109, Height=18,;
    Caption="Preventivo:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return ( ! Isalt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kof','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_kof
*--- Effettua il link sulle offerte, lanciato dal bottone ...
Proc LinkOfferte(oParent)
    l_obj = oParent.GetCtrl('w_ATOPPOFF')
    oParent.bDontReportError=.t.
    public i_lastindirectaction
    i_lastindirectaction='ecpZoom'	
    l_obj.setFocus()
	  l_obj.mzoom()
EndFunc
Proc AbilitaBottone(oParent)
    obj = oParent.GetCtrl(Cp_Translate('\<Offerte'))
    obj.enabled=not empty(oParent.w_ATOPPOFF)
    obj = oParent.GetCtrl(Cp_Translate('\<Nominat.'))
    obj.enabled=not empty(oParent.w_ATCODNOM)
endproc
* --- Fine Area Manuale
