* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bzm                                                        *
*              Lancia visualizza documenti dalla generazione                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-10-01                                                      *
* Last revis.: 2009-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bzm",oParentObject)
return(i_retval)

define class tgsag_bzm as StdBatch
  * --- Local variables
  w_PROG = .NULL.
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apre masch.VISUALIZZA DOCUMENTI DI VENDITA
    this.w_PROG = GSVE_SZM()
    if Isahe()
      this.w_PROG.w_ZoomDoc.cCpQueryName = "QUERY\GSAGESZM"
    else
      this.w_PROG.w_ZoomDoc.cCpQueryName = "GSAG_SZM"
    endif
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      * --- Messaggio di errore
      Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
      i_retcode = 'stop'
      return
    endif
    if Isahr()
      this.w_PROG.w_DESAPP = ""
    endif
    this.w_PROG.w_SERDOCU = this.oParentObject.w_DTSERIAL
    this.w_PROG.w_CODMAG = ""
    this.w_PROG.w_DOCINI = this.oParentObject.w_DTDATINI
    this.w_PROG.w_DOCFIN = this.oParentObject.w_DTDATFIN
    this.w_PROG.w_DATINI = this.oParentObject.w_DTDATINI
    this.w_PROG.w_DATFIN = this.oParentObject.w_DTDATFIN
    this.w_PROG.NotifyEvent("Lanciattivity")     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
