* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bch                                                        *
*              Controlla profili esportazione/importazione                     *
*                                                                              *
*      Author: Zucchetti TAM S.p.a.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_66]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-17                                                      *
* Last revis.: 2012-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bch",oParentObject,m.pEXEC)
return(i_retval)

define class tgsag_bch as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_OK = .f.
  w_SERIAL = space(10)
  w_PADRE = .NULL.
  w_GSAG_MPC = .NULL.
  w_GSAG_MPK = .NULL.
  w_FLDEL = .f.
  w_OKDEL = .f.
  * --- WorkFile variables
  PRO_EXPO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla profili esportazione/importazione (lanciato alla checkform di GSAG_APE)
    * --- C check form
    *     R replace end
    this.w_OK = .T.
    this.w_SERIAL = ""
    if this.pEXEC="C"
      * --- Controllo esistenza di almeno una categoria attivit�
      if this.oParentObject.w_PRAPPUNT=" " and this.oParentObject.w_PRDAFARE=" " and this.oParentObject.w_PRSESTEL=" " and this.oParentObject.w_PRCATNOT=" " and this.oParentObject.w_PRATTGEN=" " and this.oParentObject.w_PRASSENZ=" " and this.oParentObject.w_PRUDIENZ=" " and this.oParentObject.w_PRINSPRE=" "
        ah_ErrorMsg("E' necessario selezionare almeno una categoria attivit�")
        this.w_OK = .F.
        this.oParentObject.w_RESCHK = -1
      endif
      * --- Controllo esistenza di un unico profilo per persona
      if this.w_OK and This.oparentobject.Cfunction="Load"
        * --- Select from PRO_EXPO
        i_nConn=i_TableProp[this.PRO_EXPO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRO_EXPO_idx,2],.t.,this.PRO_EXPO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PRSERIAL  from "+i_cTable+" PRO_EXPO ";
              +" where PRCODUTE="+cp_ToStrODBC(this.oParentObject.w_PRCODUTE)+"";
               ,"_Curs_PRO_EXPO")
        else
          select PRSERIAL from (i_cTable);
           where PRCODUTE=this.oParentObject.w_PRCODUTE;
            into cursor _Curs_PRO_EXPO
        endif
        if used('_Curs_PRO_EXPO')
          select _Curs_PRO_EXPO
          locate for 1=1
          do while not(eof())
          this.w_SERIAL = NVL(_Curs_PRO_EXPO.PRSERIAL, SPACE(10))
          if this.w_SERIAL<>this.oParentObject.w_PRSERIAL
            this.w_OK = .F.
            exit
          endif
            select _Curs_PRO_EXPO
            continue
          enddo
          use
        endif
        if NOT this.w_OK
          ah_ErrorMsg("Non � possibile salvare pi� di un profilo per la stessa persona")
          this.oParentObject.w_RESCHK = -1
        endif
      endif
      * --- Controllo univocit� categorie per il profilo
      if VARTYPE(this.oParentObject.GSAG_MPC.cnt)="O"
        this.w_GSAG_MPC = this.oParentObject.GSAG_MPC.cnt
        if Isalt()
          this.w_GSAG_MPC.Exec_Select("_TMP_MPC","t_PCCODCA2" + " As CodCat, Count(t_PCCATEGO) as Conta","","","t_PCCODCA2","Count(t_PCCATEGO)>1")     
        else
          this.w_GSAG_MPC.Exec_Select("_TMP_MPC","t_PCCODCAT" + " As CodCat, Count(t_PCCATEGO) as Conta","","","t_PCCODCAT","Count(t_PCCATEGO)>1")     
        endif
        if RECCOUNT()>0
          * --- Ci sono categorie non univoche per il profilo
          ah_ErrorMsg("Non � possibile salvare una categoria in export non univoca per il profilo")
          this.oParentObject.w_RESCHK = -1
        endif
        USE IN SELECT("_TMP_MPC")
      endif
      if this.oParentObject.w_RESCHK<>-1 AND VARTYPE(this.oParentObject.GSAG_MPK.cnt)="O"
        this.w_GSAG_MPK = this.oParentObject.GSAG_MPK.cnt
        if Isalt()
          this.w_GSAG_MPK.Exec_Select("_TMP_MPC","t_PCCATEGO" + " As CodCat, Count(t_PCCODCA2) as Conta","","","t_PCCATEGO","Count(t_PCCODCA2)>1")     
        else
          this.w_GSAG_MPK.Exec_Select("_TMP_MPC","t_PCCATEGO" + " As CodCat, Count(t_PCCODCAT) as Conta","","","t_PCCATEGO","Count(t_PCCODCAT)>1")     
        endif
        if RECCOUNT()>0
          * --- Ci sono categorie non univoche per il profilo
          ah_ErrorMsg("Non � possibile salvare una categoria in import non univoca per il profilo")
          this.oParentObject.w_RESCHK = -1
        endif
        USE IN SELECT("_TMP_MPC")
      endif
    else
      this.w_OKDEL = .T.
      this.w_PADRE = This.oparentobject.GSAG_MPE
      this.w_FLDEL = This.oparentobject.cFunction="Query"
      if TYPE("this.w_PADRE.ctrsname")="C"
        this.w_PADRE.FirstRowDel()     
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
          *     se non cancellate
          if this.w_PADRE.FullRow() And ( this.w_PADRE.RowStatus() = "D" Or this.w_FLDEL) and Not EMpty(this.w_PADRE.w_COSERATT)
            if this.w_OKDEL
              if !Ah_Yesno("Attenzione! Cancellare anche attivit� collegate agli eventi ICS eliminati?")
                Exit
              endif
              this.w_OKDEL = .F.
            endif
            this.w_OK = Empty(gsag_bda(This,this.w_PADRE.w_COSERATT))
          endif
          * --- Se tutto ok passo alla prossima riga altrimenti esco...
          if this.w_OK
            this.w_PADRE.NextRowDel()     
          else
            Exit
          endif
        enddo
      endif
      if NOT this.w_OK
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=Ah_msgformat("Errore eliminazione attivit� collegate")
      endif
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PRO_EXPO'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PRO_EXPO')
      use in _Curs_PRO_EXPO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
