* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mdg                                                        *
*              Dettaglio Generazione                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-11                                                      *
* Last revis.: 2011-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsag_mdg")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsag_mdg")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsag_mdg")
  return

* --- Class definition
define class tgsag_mdg as StdPCForm
  Width  = 533
  Height = 336
  Top    = 12
  Left   = 10
  cComment = "Dettaglio Generazione"
  cPrg = "gsag_mdg"
  HelpContextID=80357225
  add object cnt as tcgsag_mdg
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsag_mdg as PCContext
  w_MDSERIAL = space(10)
  w_MDSERATT = space(20)
  w_MDCONTRA = space(10)
  w_MDCODMOD = space(10)
  w_MDCODIMP = space(10)
  w_MDCOMPON = 0
  w_ATSERIAL = space(20)
  w_MDOLDDAT = space(8)
  w_CAUATT = space(20)
  w_OGGETT = space(254)
  w_PERSON = space(60)
  w_DATINI = space(14)
  w_DATFIN = space(14)
  w_MDDATGAT = space(8)
  w_DESIMP = space(50)
  w_MDINICOM = space(8)
  w_MDFINCOM = space(8)
  proc Save(i_oFrom)
    this.w_MDSERIAL = i_oFrom.w_MDSERIAL
    this.w_MDSERATT = i_oFrom.w_MDSERATT
    this.w_MDCONTRA = i_oFrom.w_MDCONTRA
    this.w_MDCODMOD = i_oFrom.w_MDCODMOD
    this.w_MDCODIMP = i_oFrom.w_MDCODIMP
    this.w_MDCOMPON = i_oFrom.w_MDCOMPON
    this.w_ATSERIAL = i_oFrom.w_ATSERIAL
    this.w_MDOLDDAT = i_oFrom.w_MDOLDDAT
    this.w_CAUATT = i_oFrom.w_CAUATT
    this.w_OGGETT = i_oFrom.w_OGGETT
    this.w_PERSON = i_oFrom.w_PERSON
    this.w_DATINI = i_oFrom.w_DATINI
    this.w_DATFIN = i_oFrom.w_DATFIN
    this.w_MDDATGAT = i_oFrom.w_MDDATGAT
    this.w_DESIMP = i_oFrom.w_DESIMP
    this.w_MDINICOM = i_oFrom.w_MDINICOM
    this.w_MDFINCOM = i_oFrom.w_MDFINCOM
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MDSERIAL = this.w_MDSERIAL
    i_oTo.w_MDSERATT = this.w_MDSERATT
    i_oTo.w_MDCONTRA = this.w_MDCONTRA
    i_oTo.w_MDCODMOD = this.w_MDCODMOD
    i_oTo.w_MDCODIMP = this.w_MDCODIMP
    i_oTo.w_MDCOMPON = this.w_MDCOMPON
    i_oTo.w_ATSERIAL = this.w_ATSERIAL
    i_oTo.w_MDOLDDAT = this.w_MDOLDDAT
    i_oTo.w_CAUATT = this.w_CAUATT
    i_oTo.w_OGGETT = this.w_OGGETT
    i_oTo.w_PERSON = this.w_PERSON
    i_oTo.w_DATINI = this.w_DATINI
    i_oTo.w_DATFIN = this.w_DATFIN
    i_oTo.w_MDDATGAT = this.w_MDDATGAT
    i_oTo.w_DESIMP = this.w_DESIMP
    i_oTo.w_MDINICOM = this.w_MDINICOM
    i_oTo.w_MDFINCOM = this.w_MDFINCOM
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsag_mdg as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 533
  Height = 336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-11-17"
  HelpContextID=80357225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DET_GEN_IDX = 0
  MOD_ELEM_IDX = 0
  CON_TRAS_IDX = 0
  IMP_MAST_IDX = 0
  OFF_ATTI_IDX = 0
  cFile = "DET_GEN"
  cKeySelect = "MDSERIAL"
  cKeyWhere  = "MDSERIAL=this.w_MDSERIAL"
  cKeyDetail  = "MDSERIAL=this.w_MDSERIAL"
  cKeyWhereODBC = '"MDSERIAL="+cp_ToStrODBC(this.w_MDSERIAL)';

  cKeyDetailWhereODBC = '"MDSERIAL="+cp_ToStrODBC(this.w_MDSERIAL)';

  cKeyWhereODBCqualified = '"DET_GEN.MDSERIAL="+cp_ToStrODBC(this.w_MDSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsag_mdg"
  cComment = "Dettaglio Generazione"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MDSERIAL = space(10)
  w_MDSERATT = space(20)
  w_MDCONTRA = space(10)
  w_MDCODMOD = space(10)
  w_MDCODIMP = space(10)
  w_MDCOMPON = 0
  w_ATSERIAL = space(20)
  w_MDOLDDAT = ctod('  /  /  ')
  w_CAUATT = space(20)
  w_OGGETT = space(254)
  w_PERSON = space(60)
  w_DATINI = ctot('')
  w_DATFIN = ctot('')
  w_MDDATGAT = ctod('  /  /  ')
  w_DESIMP = space(50)
  w_MDINICOM = ctod('  /  /  ')
  w_MDFINCOM = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mdgPag1","gsag_mdg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dettagli")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='MOD_ELEM'
    this.cWorkTables[2]='CON_TRAS'
    this.cWorkTables[3]='IMP_MAST'
    this.cWorkTables[4]='OFF_ATTI'
    this.cWorkTables[5]='DET_GEN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DET_GEN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DET_GEN_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsag_mdg'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DET_GEN where MDSERIAL=KeySet.MDSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DET_GEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_GEN_IDX,2],this.bLoadRecFilter,this.DET_GEN_IDX,"gsag_mdg")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DET_GEN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DET_GEN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DET_GEN '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MDSERIAL',this.w_MDSERIAL  )
      select * from (i_cTable) DET_GEN where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MDSERIAL = NVL(MDSERIAL,space(10))
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .w_ATSERIAL = THIS.OPARENTOBJECT.w_ATSERIAL
        .w_MDINICOM = NVL(cp_ToDate(MDINICOM),ctod("  /  /  "))
        .w_MDFINCOM = NVL(cp_ToDate(MDFINCOM),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DET_GEN')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CAUATT = space(20)
          .w_OGGETT = space(254)
          .w_PERSON = space(60)
          .w_DATINI = ctot("")
          .w_DATFIN = ctot("")
          .w_DESIMP = space(50)
          .w_MDSERATT = NVL(MDSERATT,space(20))
          if link_2_1_joined
            this.w_MDSERATT = NVL(ATSERIAL201,NVL(this.w_MDSERATT,space(20)))
            this.w_CAUATT = NVL(ATCAUATT201,space(20))
            this.w_OGGETT = NVL(ATOGGETT201,space(254))
            this.w_PERSON = NVL(ATPERSON201,space(60))
            this.w_DATINI = NVL(ATDATINI201,ctot(""))
            this.w_DATFIN = NVL(ATDATFIN201,ctot(""))
          else
          .link_2_1('Load')
          endif
          .w_MDCONTRA = NVL(MDCONTRA,space(10))
          * evitabile
          *.link_2_2('Load')
          .w_MDCODMOD = NVL(MDCODMOD,space(10))
          * evitabile
          *.link_2_3('Load')
          .w_MDCODIMP = NVL(MDCODIMP,space(10))
          if link_2_4_joined
            this.w_MDCODIMP = NVL(IMCODICE204,NVL(this.w_MDCODIMP,space(10)))
            this.w_DESIMP = NVL(IMDESCRI204,space(50))
          else
          .link_2_4('Load')
          endif
          .w_MDCOMPON = NVL(MDCOMPON,0)
          .w_MDOLDDAT = NVL(cp_ToDate(MDOLDDAT),ctod("  /  /  "))
          .w_MDDATGAT = NVL(cp_ToDate(MDDATGAT),ctod("  /  /  "))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .w_ATSERIAL = THIS.OPARENTOBJECT.w_ATSERIAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_14.enabled = .oPgFrm.Page1.oPag.oBtn_2_14.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MDSERIAL=space(10)
      .w_MDSERATT=space(20)
      .w_MDCONTRA=space(10)
      .w_MDCODMOD=space(10)
      .w_MDCODIMP=space(10)
      .w_MDCOMPON=0
      .w_ATSERIAL=space(20)
      .w_MDOLDDAT=ctod("  /  /  ")
      .w_CAUATT=space(20)
      .w_OGGETT=space(254)
      .w_PERSON=space(60)
      .w_DATINI=ctot("")
      .w_DATFIN=ctot("")
      .w_MDDATGAT=ctod("  /  /  ")
      .w_DESIMP=space(50)
      .w_MDINICOM=ctod("  /  /  ")
      .w_MDFINCOM=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_MDSERATT))
         .link_2_1('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MDCONTRA))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_MDCODMOD))
         .link_2_3('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_MDCODIMP))
         .link_2_4('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .DoRTCalc(6,6,.f.)
        .w_ATSERIAL = THIS.OPARENTOBJECT.w_ATSERIAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DET_GEN')
    this.DoRTCalc(8,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_14.enabled = this.oPgFrm.Page1.oPag.oBtn_2_14.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_2_14.enabled = .Page1.oPag.oBtn_2_14.mCond()
      .Page1.oPag.oObj_1_3.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DET_GEN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DET_GEN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDSERIAL,"MDSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDINICOM,"MDINICOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDFINCOM,"MDFINCOM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MDSERATT C(20);
      ,t_MDCONTRA C(10);
      ,t_MDCODMOD C(10);
      ,t_MDCODIMP C(10);
      ,t_CAUATT C(20);
      ,t_OGGETT C(254);
      ,t_PERSON C(60);
      ,t_DATINI T(14);
      ,t_DATFIN T(14);
      ,t_DESIMP C(50);
      ,t_MDCOMPON N(4);
      ,t_MDOLDDAT D(8);
      ,t_MDDATGAT D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mdgbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDSERATT_2_1.controlsource=this.cTrsName+'.t_MDSERATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDCONTRA_2_2.controlsource=this.cTrsName+'.t_MDCONTRA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODMOD_2_3.controlsource=this.cTrsName+'.t_MDCODMOD'
    this.oPgFRm.Page1.oPag.oMDCODIMP_2_4.controlsource=this.cTrsName+'.t_MDCODIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAUATT_2_7.controlsource=this.cTrsName+'.t_CAUATT'
    this.oPgFRm.Page1.oPag.oOGGETT_2_8.controlsource=this.cTrsName+'.t_OGGETT'
    this.oPgFRm.Page1.oPag.oPERSON_2_9.controlsource=this.cTrsName+'.t_PERSON'
    this.oPgFRm.Page1.oPag.oDATINI_2_10.controlsource=this.cTrsName+'.t_DATINI'
    this.oPgFRm.Page1.oPag.oDATFIN_2_11.controlsource=this.cTrsName+'.t_DATFIN'
    this.oPgFRm.Page1.oPag.oDESIMP_2_13.controlsource=this.cTrsName+'.t_DESIMP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(172)
    this.AddVLine(326)
    this.AddVLine(411)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDSERATT_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DET_GEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_GEN_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DET_GEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_GEN_IDX,2])
      *
      * insert into DET_GEN
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DET_GEN')
        i_extval=cp_InsertValODBCExtFlds(this,'DET_GEN')
        i_cFldBody=" "+;
                  "(MDSERIAL,MDSERATT,MDCONTRA,MDCODMOD,MDCODIMP"+;
                  ",MDCOMPON,MDOLDDAT,MDDATGAT,MDINICOM,MDFINCOM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MDSERIAL)+","+cp_ToStrODBCNull(this.w_MDSERATT)+","+cp_ToStrODBCNull(this.w_MDCONTRA)+","+cp_ToStrODBCNull(this.w_MDCODMOD)+","+cp_ToStrODBCNull(this.w_MDCODIMP)+;
             ","+cp_ToStrODBC(this.w_MDCOMPON)+","+cp_ToStrODBC(this.w_MDOLDDAT)+","+cp_ToStrODBC(this.w_MDDATGAT)+","+cp_ToStrODBC(this.w_MDINICOM)+","+cp_ToStrODBC(this.w_MDFINCOM)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DET_GEN')
        i_extval=cp_InsertValVFPExtFlds(this,'DET_GEN')
        cp_CheckDeletedKey(i_cTable,0,'MDSERIAL',this.w_MDSERIAL)
        INSERT INTO (i_cTable) (;
                   MDSERIAL;
                  ,MDSERATT;
                  ,MDCONTRA;
                  ,MDCODMOD;
                  ,MDCODIMP;
                  ,MDCOMPON;
                  ,MDOLDDAT;
                  ,MDDATGAT;
                  ,MDINICOM;
                  ,MDFINCOM;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MDSERIAL;
                  ,this.w_MDSERATT;
                  ,this.w_MDCONTRA;
                  ,this.w_MDCODMOD;
                  ,this.w_MDCODIMP;
                  ,this.w_MDCOMPON;
                  ,this.w_MDOLDDAT;
                  ,this.w_MDDATGAT;
                  ,this.w_MDINICOM;
                  ,this.w_MDFINCOM;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DET_GEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_GEN_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_MDSERATT))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DET_GEN')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " MDINICOM="+cp_ToStrODBC(this.w_MDINICOM)+;
                 ",MDFINCOM="+cp_ToStrODBC(this.w_MDFINCOM)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DET_GEN')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  MDINICOM=this.w_MDINICOM;
                 ,MDFINCOM=this.w_MDFINCOM;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_MDSERATT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DET_GEN
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DET_GEN')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MDSERATT="+cp_ToStrODBCNull(this.w_MDSERATT)+;
                     ",MDCONTRA="+cp_ToStrODBCNull(this.w_MDCONTRA)+;
                     ",MDCODMOD="+cp_ToStrODBCNull(this.w_MDCODMOD)+;
                     ",MDCODIMP="+cp_ToStrODBCNull(this.w_MDCODIMP)+;
                     ",MDCOMPON="+cp_ToStrODBC(this.w_MDCOMPON)+;
                     ",MDOLDDAT="+cp_ToStrODBC(this.w_MDOLDDAT)+;
                     ",MDDATGAT="+cp_ToStrODBC(this.w_MDDATGAT)+;
                     ",MDINICOM="+cp_ToStrODBC(this.w_MDINICOM)+;
                     ",MDFINCOM="+cp_ToStrODBC(this.w_MDFINCOM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DET_GEN')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MDSERATT=this.w_MDSERATT;
                     ,MDCONTRA=this.w_MDCONTRA;
                     ,MDCODMOD=this.w_MDCODMOD;
                     ,MDCODIMP=this.w_MDCODIMP;
                     ,MDCOMPON=this.w_MDCOMPON;
                     ,MDOLDDAT=this.w_MDOLDDAT;
                     ,MDDATGAT=this.w_MDDATGAT;
                     ,MDINICOM=this.w_MDINICOM;
                     ,MDFINCOM=this.w_MDFINCOM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DET_GEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_GEN_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MDSERATT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DET_GEN
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MDSERATT))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DET_GEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_GEN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_2_2('Full')
          .link_2_3('Full')
          .link_2_4('Full')
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .DoRTCalc(6,6,.t.)
          .w_ATSERIAL = THIS.OPARENTOBJECT.w_ATSERIAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MDCOMPON with this.w_MDCOMPON
      replace t_MDOLDDAT with this.w_MDOLDDAT
      replace t_MDDATGAT with this.w_MDDATGAT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBtn_2_14.visible=!this.oPgFrm.Page1.oPag.oBtn_2_14.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MDSERATT
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_lTable = "OFF_ATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2], .t., this.OFF_ATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDSERATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AAT',True,'OFF_ATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATSERIAL like "+cp_ToStrODBC(trim(this.w_MDSERATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATSERIAL,ATCAUATT,ATOGGETT,ATPERSON,ATDATINI,ATDATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATSERIAL',trim(this.w_MDSERATT))
          select ATSERIAL,ATCAUATT,ATOGGETT,ATPERSON,ATDATINI,ATDATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDSERATT)==trim(_Link_.ATSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MDSERATT) and !this.bDontReportError
            deferred_cp_zoom('OFF_ATTI','*','ATSERIAL',cp_AbsName(oSource.parent,'oMDSERATT_2_1'),i_cWhere,'GSAG_AAT',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATSERIAL,ATCAUATT,ATOGGETT,ATPERSON,ATDATINI,ATDATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where ATSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATSERIAL',oSource.xKey(1))
            select ATSERIAL,ATCAUATT,ATOGGETT,ATPERSON,ATDATINI,ATDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDSERATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATSERIAL,ATCAUATT,ATOGGETT,ATPERSON,ATDATINI,ATDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where ATSERIAL="+cp_ToStrODBC(this.w_MDSERATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATSERIAL',this.w_MDSERATT)
            select ATSERIAL,ATCAUATT,ATOGGETT,ATPERSON,ATDATINI,ATDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDSERATT = NVL(_Link_.ATSERIAL,space(20))
      this.w_CAUATT = NVL(_Link_.ATCAUATT,space(20))
      this.w_OGGETT = NVL(_Link_.ATOGGETT,space(254))
      this.w_PERSON = NVL(_Link_.ATPERSON,space(60))
      this.w_DATINI = NVL(_Link_.ATDATINI,ctot(""))
      this.w_DATFIN = NVL(_Link_.ATDATFIN,ctot(""))
    else
      if i_cCtrl<>'Load'
        this.w_MDSERATT = space(20)
      endif
      this.w_CAUATT = space(20)
      this.w_OGGETT = space(254)
      this.w_PERSON = space(60)
      this.w_DATINI = ctot("")
      this.w_DATFIN = ctot("")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])+'\'+cp_ToStr(_Link_.ATSERIAL,1)
      cp_ShowWarn(i_cKey,this.OFF_ATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDSERATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_ATTI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.ATSERIAL as ATSERIAL201"+ ",link_2_1.ATCAUATT as ATCAUATT201"+ ",link_2_1.ATOGGETT as ATOGGETT201"+ ",link_2_1.ATPERSON as ATPERSON201"+ ",link_2_1.ATDATINI as ATDATINI201"+ ",link_2_1.ATDATFIN as ATDATFIN201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on DET_GEN.MDSERATT=link_2_1.ATSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and DET_GEN.MDSERATT=link_2_1.ATSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MDCONTRA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_MDCONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_MDCONTRA)
            select COSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCONTRA = NVL(_Link_.COSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MDCONTRA = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODMOD
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_MDCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_MDCODMOD)
            select MOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODMOD = NVL(_Link_.MOCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODMOD = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCODIMP
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_MDCODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_MDCODIMP)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCODIMP = NVL(_Link_.IMCODICE,space(10))
      this.w_DESIMP = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_MDCODIMP = space(10)
      endif
      this.w_DESIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IMP_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.IMCODICE as IMCODICE204"+ ",link_2_4.IMDESCRI as IMDESCRI204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on DET_GEN.MDCODIMP=link_2_4.IMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and DET_GEN.MDCODIMP=link_2_4.IMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMDCODIMP_2_4.value==this.w_MDCODIMP)
      this.oPgFrm.Page1.oPag.oMDCODIMP_2_4.value=this.w_MDCODIMP
      replace t_MDCODIMP with this.oPgFrm.Page1.oPag.oMDCODIMP_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOGGETT_2_8.value==this.w_OGGETT)
      this.oPgFrm.Page1.oPag.oOGGETT_2_8.value=this.w_OGGETT
      replace t_OGGETT with this.oPgFrm.Page1.oPag.oOGGETT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPERSON_2_9.value==this.w_PERSON)
      this.oPgFrm.Page1.oPag.oPERSON_2_9.value=this.w_PERSON
      replace t_PERSON with this.oPgFrm.Page1.oPag.oPERSON_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_2_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_2_10.value=this.w_DATINI
      replace t_DATINI with this.oPgFrm.Page1.oPag.oDATINI_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_2_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_2_11.value=this.w_DATFIN
      replace t_DATFIN with this.oPgFrm.Page1.oPag.oDATFIN_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_2_13.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_2_13.value=this.w_DESIMP
      replace t_DESIMP with this.oPgFrm.Page1.oPag.oDESIMP_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDSERATT_2_1.value==this.w_MDSERATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDSERATT_2_1.value=this.w_MDSERATT
      replace t_MDSERATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDSERATT_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCONTRA_2_2.value==this.w_MDCONTRA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCONTRA_2_2.value=this.w_MDCONTRA
      replace t_MDCONTRA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCONTRA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODMOD_2_3.value==this.w_MDCODMOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODMOD_2_3.value=this.w_MDCODMOD
      replace t_MDCODMOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMDCODMOD_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAUATT_2_7.value==this.w_CAUATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAUATT_2_7.value=this.w_CAUATT
      replace t_CAUATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAUATT_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'DET_GEN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_MDSERATT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MDSERATT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MDSERATT=space(20)
      .w_MDCONTRA=space(10)
      .w_MDCODMOD=space(10)
      .w_MDCODIMP=space(10)
      .w_MDCOMPON=0
      .w_MDOLDDAT=ctod("  /  /  ")
      .w_CAUATT=space(20)
      .w_OGGETT=space(254)
      .w_PERSON=space(60)
      .w_DATINI=ctot("")
      .w_DATFIN=ctot("")
      .w_MDDATGAT=ctod("  /  /  ")
      .w_DESIMP=space(50)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_MDSERATT))
        .link_2_1('Full')
      endif
      .DoRTCalc(3,3,.f.)
      if not(empty(.w_MDCONTRA))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_MDCODMOD))
        .link_2_3('Full')
      endif
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_MDCODIMP))
        .link_2_4('Full')
      endif
    endwith
    this.DoRTCalc(6,17,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MDSERATT = t_MDSERATT
    this.w_MDCONTRA = t_MDCONTRA
    this.w_MDCODMOD = t_MDCODMOD
    this.w_MDCODIMP = t_MDCODIMP
    this.w_MDCOMPON = t_MDCOMPON
    this.w_MDOLDDAT = t_MDOLDDAT
    this.w_CAUATT = t_CAUATT
    this.w_OGGETT = t_OGGETT
    this.w_PERSON = t_PERSON
    this.w_DATINI = t_DATINI
    this.w_DATFIN = t_DATFIN
    this.w_MDDATGAT = t_MDDATGAT
    this.w_DESIMP = t_DESIMP
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MDSERATT with this.w_MDSERATT
    replace t_MDCONTRA with this.w_MDCONTRA
    replace t_MDCODMOD with this.w_MDCODMOD
    replace t_MDCODIMP with this.w_MDCODIMP
    replace t_MDCOMPON with this.w_MDCOMPON
    replace t_MDOLDDAT with this.w_MDOLDDAT
    replace t_CAUATT with this.w_CAUATT
    replace t_OGGETT with this.w_OGGETT
    replace t_PERSON with this.w_PERSON
    replace t_DATINI with this.w_DATINI
    replace t_DATFIN with this.w_DATFIN
    replace t_MDDATGAT with this.w_MDDATGAT
    replace t_DESIMP with this.w_DESIMP
    if i_srv='A'
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mdgPag1 as StdContainer
  Width  = 529
  height = 336
  stdWidth  = 529
  stdheight = 336
  resizeXpos=297
  resizeYpos=142
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_3 as cp_runprogram with uid="TGKSFYEEWX",left=-4, top=412, width=168,height=19,;
    caption='GSAG_BCF(B)',;
   bGlobalFont=.t.,;
    prg="GSAG_BCF('B')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 57939244


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=18, top=7, width=495,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="MDSERATT",Label1="Seriale attivit�",Field2="CAUATT",Label2="Tipo attivit�",Field3="MDCODMOD",Label3="Modello",Field4="MDCONTRA",Label4="Contratto",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235733626

  add object oStr_1_2 as StdString with uid="HJXVNARIYB",Visible=.t., Left=12, Top=304,;
    Alignment=1, Width=59, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="RZQZIVTZVL",Visible=.t., Left=25, Top=250,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="MYBRYCKZXF",Visible=.t., Left=6, Top=277,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="MMDHGYWUPW",Visible=.t., Left=26, Top=223,;
    Alignment=1, Width=45, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="JAYRPKTFLY",Visible=.t., Left=343, Top=223,;
    Alignment=1, Width=40, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=8,top=26,;
    width=491+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=9,top=27,width=490+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='OFF_ATTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oMDCODIMP_2_4.Refresh()
      this.Parent.oOGGETT_2_8.Refresh()
      this.Parent.oPERSON_2_9.Refresh()
      this.Parent.oDATINI_2_10.Refresh()
      this.Parent.oDATFIN_2_11.Refresh()
      this.Parent.oDESIMP_2_13.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='OFF_ATTI'
        oDropInto=this.oBodyCol.oRow.oMDSERATT_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oMDCODIMP_2_4 as StdTrsField with uid="QQFBMVJRRW",rtseq=5,rtrep=.t.,;
    cFormVar="w_MDCODIMP",value=space(10),enabled=.f.,;
    HelpContextID = 121023978,;
    cTotal="", bFixedPos=.t., cQueryName = "MDCODIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=77, Top=304, InputMask=replicate('X',10), cLinkFile="IMP_MAST", oKey_1_1="IMCODICE", oKey_1_2="this.w_MDCODIMP"

  func oMDCODIMP_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oOGGETT_2_8 as StdTrsField with uid="KWGFYAUKQD",rtseq=10,rtrep=.t.,;
    cFormVar="w_OGGETT",value=space(254),enabled=.f.,;
    HelpContextID = 79664358,;
    cTotal="", bFixedPos=.t., cQueryName = "OGGETT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=435, Left=78, Top=250, InputMask=replicate('X',254)

  add object oPERSON_2_9 as StdTrsField with uid="ETBVKLERBI",rtseq=11,rtrep=.t.,;
    cFormVar="w_PERSON",value=space(60),enabled=.f.,;
    HelpContextID = 243155702,;
    cTotal="", bFixedPos=.t., cQueryName = "PERSON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=374, Left=77, Top=277, InputMask=replicate('X',60)

  add object oDATINI_2_10 as StdTrsField with uid="SPSBYSUUTY",rtseq=12,rtrep=.t.,;
    cFormVar="w_DATINI",value=ctot(""),enabled=.f.,;
    HelpContextID = 157572662,;
    cTotal="", bFixedPos=.t., cQueryName = "DATINI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=77, Top=223

  add object oDATFIN_2_11 as StdTrsField with uid="PNKECXIBYB",rtseq=13,rtrep=.t.,;
    cFormVar="w_DATFIN",value=ctot(""),enabled=.f.,;
    HelpContextID = 236019254,;
    cTotal="", bFixedPos=.t., cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=388, Top=223

  add object oDESIMP_2_13 as StdTrsField with uid="ZWWWMMFMBH",rtseq=15,rtrep=.t.,;
    cFormVar="w_DESIMP",value=space(50),enabled=.f.,;
    HelpContextID = 5526070,;
    cTotal="", bFixedPos=.t., cQueryName = "DESIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=164, Top=304, InputMask=replicate('X',50)

  add object oBtn_2_14 as StdButton with uid="BIGHRONQVF",width=48,height=45,;
   left=463, top=279,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedre all'attivit� selezionata";
    , HelpContextID = 146899615;
    , caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_14.Click()
      with this.Parent.oContained
        GSAG_BZA(this.Parent.oContained,.w_MDSERATT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MDSERATT))
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mdgBodyRow as CPBodyRowCnt
  Width=481
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMDSERATT_2_1 as StdTrsField with uid="POBQQORDSN",rtseq=2,rtrep=.t.,;
    cFormVar="w_MDSERATT",value=space(20),;
    HelpContextID = 27283994,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=-2, Top=0, InputMask=replicate('X',20), cLinkFile="OFF_ATTI", cZoomOnZoom="GSAG_AAT", oKey_1_1="ATSERIAL", oKey_1_2="this.w_MDSERATT"

  func oMDSERATT_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDSERATT_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  add object oMDCONTRA_2_2 as StdTrsField with uid="MZWOUKWCER",rtseq=3,rtrep=.t.,;
    cFormVar="w_MDCONTRA",value=space(10),enabled=.f.,;
    HelpContextID = 74011143,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=393, Top=0, InputMask=replicate('X',10), cLinkFile="CON_TRAS", cZoomOnZoom="GSAG_ACA", oKey_1_1="COSERIAL", oKey_1_2="this.w_MDCONTRA"

  func oMDCONTRA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMDCODMOD_2_3 as StdTrsField with uid="SCRKSSYPXW",rtseq=4,rtrep=.t.,;
    cFormVar="w_MDCODMOD",value=space(10),enabled=.f.,;
    HelpContextID = 214520330,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=307, Top=0, InputMask=replicate('X',10), cLinkFile="MOD_ELEM", cZoomOnZoom="GSAG_AME", oKey_1_1="MOCODICE", oKey_1_2="this.w_MDCODMOD"

  func oMDCODMOD_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCAUATT_2_7 as StdTrsField with uid="HGQLXCESKJ",rtseq=9,rtrep=.t.,;
    cFormVar="w_CAUATT",value=space(20),enabled=.f.,;
    HelpContextID = 79457830,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=153, Top=0, InputMask=replicate('X',20)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oMDSERATT_2_1.When()
    return(.t.)
  proc oMDSERATT_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMDSERATT_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mdg','DET_GEN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MDSERIAL=DET_GEN.MDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
