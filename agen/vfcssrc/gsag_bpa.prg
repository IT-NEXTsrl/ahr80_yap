* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bpa                                                        *
*              Parametri agenda                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-27                                                      *
* Last revis.: 2012-04-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bpa",oParentObject,m.Azione)
return(i_retval)

define class tgsag_bpa as StdBatch
  * --- Local variables
  Azione = space(1)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_TIPAGG = space(1)
  w_oERRORLOG = .NULL.
  w_PRIMA = .f.
  * --- WorkFile variables
  PAR_AGEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione parametri agenda (GSAG_KPA)
    * --- Azione==A  eseguo aggiornamento struttura aziendale
    *     Azione==C  eseguo controlli sulle attivit� al cambio di impostazione
    do case
      case this.Azione=="A"
        this.oParentObject.w_CHECK = .T.
        if this.oParentObject.w_OLDFLVISI="L" and this.oParentObject.w_PAFLVISI="S" 
          if Ah_YesNo("Occorre aggiornare il gruppo nel dettaglio partecipanti delle attivit� e dei tipi attivit� presenti in archivio.%0Si desidera proseguire ugualmente?")
            this.w_DATINI = i_inidat
            this.w_DATFIN = i_findat
            this.w_TIPAGG = "E"
            do GSAG_BAA with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            this.oParentObject.w_PAFLVISI = "L"
            this.oParentObject.w_CHECK = .F.
          endif
        endif
      case this.Azione=="C" or this.Azione=="R"
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        if this.oParentObject.w_PAGESRIS="S" and this.Azione=="R"
          this.w_PRIMA = .t.
          * --- Select from gsag_bpa
          do vq_exec with 'gsag_bpa',this,'_Curs_gsag_bpa','',.f.,.t.
          if used('_Curs_gsag_bpa')
            select _Curs_gsag_bpa
            locate for 1=1
            do while not(eof())
            if this.w_PRIMA
              this.w_oERRORLOG.AddMsgLog("Elenco attivit� con solo risorse/gruppi come partecipanti")     
              this.w_PRIMA = .f.
            endif
            this.w_oERRORLOG.AddMsgLog("%4 - %1 - con data inizio %2 e data fine %3", Alltrim(_Curs_GSAG_BPA.atoggett),DTOC(TTOD(_Curs_GSAG_BPA.atdatini)) , DTOC(TTOD(_Curs_GSAG_BPA.atdatfin)),Alltrim(_Curs_GSAG_BPA.atserial))     
              select _Curs_gsag_bpa
              continue
            enddo
            use
          endif
        endif
        if this.oParentObject.w_PAFLVISI="S" and this.Azione=="C"
          this.w_PRIMA = .t.
          * --- Select from gsag1bpa
          do vq_exec with 'gsag1bpa',this,'_Curs_gsag1bpa','',.f.,.t.
          if used('_Curs_gsag1bpa')
            select _Curs_gsag1bpa
            locate for 1=1
            do while not(eof())
            if this.w_PRIMA
              this.w_oERRORLOG.AddMsgLog("Elenco partecipanti senza gruppo predefinito:")     
              this.w_PRIMA = .f.
            endif
            this.w_oERRORLOG.AddMsgLog("%1 - %2", Alltrim(_Curs_GSAG1BPA.dpcodice),Alltrim(_Curs_GSAG1BPA.dpcognom)+" "+Alltrim(_Curs_GSAG1BPA.dpnome))     
              select _Curs_gsag1bpa
              continue
            enddo
            use
          endif
        endif
        if this.w_oErrorLog.IsFullLog()
          if  this.Azione=="R"
            this.oParentObject.w_PAGESRIS = " "
          else
            this.oParentObject.w_PAFLVISI = "L"
          endif
          Ah_errormsg("Attenzione, esistono incongruenze che non permettono la modifica della modalit� di visualizzazione")
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Elenco incongruenze riscontrate")     
        endif
        this.w_oERRORLOG = .Null.
    endcase
  endproc


  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_AGEN'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_gsag_bpa')
      use in _Curs_gsag_bpa
    endif
    if used('_Curs_gsag1bpa')
      use in _Curs_gsag1bpa
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
