* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bif                                                        *
*              Imposta font                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-16                                                      *
* Last revis.: 2011-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bif",oParentObject)
return(i_retval)

define class tgsag_bif as StdBatch
  * --- Local variables
  w_GSAG_KRA = .NULL.
  w_GRD0 = .NULL.
  w_TMPN = 0
  w_GSAG_KRM = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPOSTA IL FONT WINGDINGS
    this.w_GSAG_KRA = this.oParentObject
    this.w_GSAG_KRA.w_AGKRA_ZOOM.cCPQUERYNAME = iif(isalt(), "QUERY\GSAG_KRAA", "QUERY\GSAG_KRA")
    this.w_GSAG_KRM = this.oParentObject
    this.w_GRD0 = this.w_GSAG_KRM.w_AGKRA_ZOOM3.GRD
    this.w_TMPN = 1
    do while this.w_TMPN <= this.w_GRD0.ColumnCount
      LInd = alltrim(str(this.w_TMPN,5,0))
      do case
        case this.w_GRD0.Column&LInd..ControlSource = "ATCODPRA" AND Not Isalt()
          this.w_GRD0.Column&LInd..HDR.caption = "Commessa"
        case this.w_GRD0.Column&LInd..ControlSource = "DESPRAT" AND Not Isalt()
          this.w_GRD0.Column&LInd..HDR.caption = "Descrizione commessa"
      endcase
      this.w_TMPN = this.w_TMPN+1
    enddo
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
