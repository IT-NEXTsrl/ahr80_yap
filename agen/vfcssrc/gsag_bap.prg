* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bap                                                        *
*              Carica prestazioni in attivita                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-18                                                      *
* Last revis.: 2015-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame,pParameSeriale,pNoAskMail
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bap",oParentObject,m.pParame,m.pParameSeriale,m.pNoAskMail)
return(i_retval)

define class tgsag_bap as StdBatch
  * --- Local variables
  pParame = space(1)
  pParameSeriale = space(20)
  pNoAskMail = .f.
  ContaRisPrat = 0
  w_NUMRIG = 0
  w_CAUATT = space(20)
  w_CODPRA = space(15)
  w_T_OREEFF = 0
  w_T_MINEFF = 0
  w_COSORA = 0
  w_COSINT = 0
  w_STATUS = space(1)
  w_ObjMDA = .NULL.
  w_TOTALE = 0
  w_INSPRE = .f.
  w_OSOURCE = .NULL.
  w_OBJCTRL = .NULL.
  w_NUMMARK = 0
  w_DPGRUPRE = space(5)
  w_DatIniAttivita = ctot("")
  w_DatFinAttivita = ctot("")
  w_NoteAttivita = space(0)
  w_OggeAttivita = space(10)
  w_StatoAttivita = 0
  w_CodPraAttivita = space(15)
  w_DesPraAttivita = space(10)
  w_TipoAttivita = space(20)
  w_DescTipologia = space(50)
  w_PersonaAttivita = space(10)
  w_Cellulare = space(18)
  w_Telefono = space(18)
  w_TxtPostIn = space(100)
  w_DataIni = ctod("  /  /  ")
  w_DataFin = ctod("  /  /  ")
  w_OraIni = space(5)
  w_orafine = space(5)
  w_RitornoCarrello = space(2)
  w_DataOraEla = ctot("")
  w_DataEla = ctod("  /  /  ")
  w_OraEla = space(5)
  w_CodEntePra = space(10)
  w_CodUffiPra = space(10)
  w_NRuoloPra = space(30)
  w_DesEntePra = space(60)
  w_DesUffiPra = space(60)
  w_Giudice = space(100)
  w_Textapp = space(254)
  w_Udienza = space(100)
  w_CODCEN_CauDoc = space(15)
  w_CODSER = space(20)
  w_CODART = space(20)
  w_DESATT = space(40)
  w_DESAGG = space(0)
  w_CODRES = space(5)
  w_CODOPE = 0
  w_UNIMIS = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_FLSERG = space(1)
  w_DESUNI = space(35)
  w_DATMOD = ctod("  /  /  ")
  w_FLRESP = space(1)
  w_FLDEFF = space(1)
  w_OREEFF = 0
  w_MINEFF = 0
  w_TIPART = space(2)
  w_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  w_FLTEMP = space(1)
  w_DURORE = 0
  w_CADTOBSO = ctod("  /  /  ")
  w_FL_FRAZ = space(1)
  w_TIPO = space(1)
  w_PRIPAR = .f.
  w_PRIMOPAR = .f.
  w_PATIPRIS = space(1)
  w_ObjMPA = .NULL.
  w_CodDip = space(4)
  w_CODRIS = space(5)
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_TIPRIS = space(40)
  w_DESCRI = space(40)
  w_SERPER = space(41)
  w_TextPart = space(254)
  w_TextRis = space(254)
  w_TextGru = space(254)
  w_TextMail = space(254)
  w_PartSerial = space(20)
  w_CntDest = 0
  w_CntDestPst = 0
  w_Ret = .f.
  w_DatIniAttivita = ctot("")
  w_DatFinAttivita = ctot("")
  w_OggeMail = space(10)
  w_IndDestMail_VCS = space(254)
  w_IndDestMail_ICS = space(254)
  w_IndDestMail_ICS_VCS = space(254)
  w_IndDestMail = space(254)
  w_LuogoAttivita = space(30)
  w_CodiceNominativo = space(15)
  w_DescrizNominativo = space(60)
  w_StatoAtti = space(1)
  w_PathVCS = space(10)
  w_DatIniVCS = ctot("")
  w_DatFinVCS = ctot("")
  w_NoteAppVCS = space(10)
  w_DTStartVCS = space(10)
  w_DTEndVCS = space(10)
  w_CategoriaVCS = space(1)
  w_TipoCategVCS = space(1)
  w_StatoAttivita = 0
  w_PRIEML = space(1)
  w_OLDPRIEML = space(1)
  w_COLPOS = 0
  w_OLDCOL = 0
  w_DPVCSICS = space(1)
  w_RICALCOLA = .f.
  w_ATCODLIS = space(20)
  w_ISALT = .f.
  w_ISAHE = .f.
  w_ISAHR = .f.
  w_ATCODUID = space(254)
  w_CALCPICU = 0
  w_CALCPICT = 0
  w_CODSES = space(15)
  w_ATSERIAL = space(10)
  w_CONTRA = space(10)
  w_TIPONOMINA = space(1)
  w_TIPOCLI = space(1)
  w_CODICECLI = space(15)
  w_Erecapito = space(0)
  w_ATEVANNO = space(4)
  w_ATSEREVE = space(4)
  w_Eidrich = space(15)
  w_ATCODSED = space(5)
  w_Nindiri = space(35)
  w_Ncodcap = space(9)
  w_Nlocali = space(30)
  w_Nprovi = space(2)
  w_Cindiri = space(35)
  w_Ccodcap = space(9)
  w_Clocali = space(30)
  w_Cprovi = space(2)
  w_Dindiri = space(35)
  w_Dcodcap = space(8)
  w_Dlocali = space(30)
  w_Dprovi = space(2)
  w_ATCOMRIC = space(15)
  w_DESCODRIC = space(100)
  w_ATNUMPRI = 0
  w_nomecompleto = space(200)
  w_CACODIMP = space(10)
  w_IMPIANTI = space(100)
  w_NoteCodiceImp = space(10)
  w_NoteDescrImp = space(10)
  w_NoteCodiceCmp = 0
  w_NoteDescrCmp = space(10)
  w_OK = .f.
  w_CNCODCAN = space(15)
  w_GIORNO = space(3)
  w_DATAUDIENZA = ctod("  /  /  ")
  w_OGGETTOUDIENZA = space(254)
  w_UDIENZASERIAL = space(10)
  w_ORAUDI = space(2)
  w_MINUDI = space(2)
  w_UDIENZATIPRES = space(1)
  w_BOTTONE = space(250)
  w_GESTIONE = .NULL.
  w_SCOLIC = space(1)
  w_CODLIS = space(5)
  w_CODCEN = space(15)
  w_OPERA3 = space(1)
  w_TIPSER = space(1)
  w_DTIPRIG = space(1)
  w_ARPRESTA = space(1)
  w_FLUNIV = space(1)
  w_FLSPAN = space(1)
  w_CALCONC = 0
  w_DESCOD = space(40)
  w_DESSUP = space(20)
  w_CODICE = space(41)
  w_TIPRIG = space(1)
  w_TIPRI2 = space(1)
  w_VOCRIC = space(15)
  w_VOCCOS = space(15)
  w_DESCOD = space(40)
  w_DESSUP = space(20)
  w_ROW = 0
  w_DATOBSO = ctod("  /  /  ")
  w_ATLISACQ = space(5)
  w_CALCPREZ = .f.
  w_MESS = space(200)
  w_HandleVCS = 0
  w_HandleICS = 0
  w_PathICS = space(10)
  w_DTStampVCS = space(0)
  w_RESULT = space(0)
  w_ATCAUATT = space(10)
  w_CODUTE = 0
  * --- WorkFile variables
  CAU_ATTI_idx=0
  RIS_INTE_idx=0
  DIPENDEN_idx=0
  ART_ICOL_idx=0
  UNIMIS_idx=0
  KEY_ARTI_idx=0
  OFF_ATTI_idx=0
  CAN_TIER_idx=0
  PAR_AGEN_idx=0
  OFFTIPAT_idx=0
  RIS_ESTR_idx=0
  COM_ATTI_idx=0
  CAUMATTI_idx=0
  CONTI_idx=0
  OFF_NOMI_idx=0
  DEF_PART_idx=0
  PRA_ENTI_idx=0
  PRA_UFFI_idx=0
  ANEVENTI_idx=0
  DES_DIVE_idx=0
  IMP_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica temporaneo Prestazioni dai Tipi Attivita (da GSAG_MDA)
    * --- pPARAME da gsag_aat
    *     ==============
    *     1= ActivatePage 2 
    *     2=w_ATCODPRA Changed  
    *     3=w_ATCODLIS Changed
    *     4=w_AT__ENTE Changed
    *     9=Partecip
    *     A=w_ATCODNOM Changed
    *     B= w_CACHKOBB depend on  AggLink
    *     C=invio mail massivo senza avviso  GSAG_BAI-GSAG_BAR-classe DocumWriter
    *     D=invio mail in fase di cancellazione da GSAG_BEL
    *     E=w_ATLISACQ Changed
    *     V=w_ATCODVAL Changed
    *     M=invio email   da GSAG_BAD-GSAG_BAG
    *     P=ricalcola i partecipanti al cambiare del tipo attivit�
    * --- Numero riga
    * --- Tipo Attivit�
    * --- Pratica
    * --- Categoria
    * --- Durata effettiva (testata)
    * --- Costo orario risorsa interna (partecipante)
    * --- Costo interno
    * --- Stato Attivit�
    * --- Prestazioni - Dettaglio Attivit�
    * --- Totale Prestazioni
    * --- Inserimento Prestazione - per controllo congruenza tipologia prestazione con il tipo ente
    * --- Centro di costo associato alla causale documento
    * --- Primo partecipante
    * --- Tipo risorsa
    * --- Partecipanti
    * --- Destinatari per solo allegato VCS
    * --- Destinatari per solo allegato ICS
    * --- Destinatari per entrambi gli allegati
    * --- Destinatari senza allegati
    this.w_PRIPAR = .T.
    this.w_ISALT = isalt()
    this.w_ISAHE = isahe()
    this.w_ISAHR = isahr()
    if this.pParame=="1" or this.pParame=="2" or this.pParame=="9" or this.pParame=="4" or this.pParame=="R"
      this.w_CALCPICU = DEFPIU(this.oParentObject.w_DECUNI)
      this.w_CALCPICT = DEFPIC(this.oParentObject.w_DECTOT)
      * --- Potremmo non provenire da GSAG_AAT, dobbiamo solo inviare la mail
      this.w_ObjMDA = this.oParentObject.GSAG_MDA
      this.w_CAUATT = this.oParentObject.w_ATCAUATT
      this.w_CODPRA = this.oParentObject.w_ATCODPRA
      this.w_ATCODLIS = this.oParentObject.w_ATCODLIS
      * --- Da Abilitare solo in Caricamento ...
      if this.pParame=="2" and this.w_ISALT
        * --- Read from RIS_ESTR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SRCODSES"+;
            " from "+i_cTable+" RIS_ESTR where ";
                +"SRCODPRA = "+cp_ToStrODBC(this.oParentObject.o_ATCODPRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SRCODSES;
            from (i_cTable) where;
                SRCODPRA = this.oParentObject.o_ATCODPRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODSES = NVL(cp_ToDate(_read_.SRCODSES),cp_NullValue(_read_.SRCODSES))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_STATUS = this.oParentObject.w_ATSTATUS
        if Not Empty(this.w_CODSES) and this.w_ObjMDA.cFunction="Edit" and this.w_STATUS="P"
          ah_ErrorMsg("Attenzione pratica soggetta a ripartizione impossibile modificare ",64,"")
          this.oParentObject.w_ATCODPRA = this.oParentObject.o_ATCODPRA
          i_retcode = 'stop'
          return
        endif
      endif
      if this.w_ObjMDA.cFunction="Load"
        this.w_ObjMPA = this.oParentObject.GSAG_MPA
        do case
          case this.pParame=="1" and this.w_ObjMDA.NumRow() = 0 and ! this.oParentObject.w_NOAUTOM
            * --- Prestazioni - Caricamento da Tipo Attivit�
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Prestazioni - Esplosione per i Partecipanti
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Calcolo Prezzi Prestazioni
            this.Page_8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.pParame=="2" and this.w_ISALT
            * --- Partecipanti - Caricamento da Pratica
            this.w_NUMRIG = 0
            if this.w_ObjMPA.Numrow()>0
              this.Page_7()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if NOT EMPTY(NVL(this.w_CAUATT, SPACE(20))) and this.w_ObjMDA.Numrow()>0
              * --- Prestazioni - Caricamento da Tipo Attivit�
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Prestazioni - Esplosione per i Partecipanti
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if EMPTY(NVL( this.w_ATCODLIS,""))
                * --- calcolo prezzi, se il listino non � presente al cambio della pratica non 
                *     ricalcolo i prezzi, lascio le prestazioni con prezzo a 0 determinate a pag 2/3
                ah_ErrorMsg("Per determinare le tariffe � necessario selezionare un listino",64,"")
              else
                this.Page_8()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
        endcase
      endif
      if this.pParame=="1" and this.w_ObjMDA.cFunction ="Load" and this.w_ObjMDA.NumRow() = 0 
        this.w_ObjMDA.w_DACODRES = InitResp( this.w_ObjMDA )
         
 SETVALUELINKED("D", this.w_ObjMDA, "w_DACODRES", this.w_ObjMDA.w_DACODRES)
      endif
    endif
    this.w_ATSERIAL = this.pParameSeriale
    do case
      case this.pParame=="3" or this.pParame=="E" or this.pParame=="V"
        * --- Calcolo Prezzi Prestazioni se Modificato il listino (o la valuta).
        *     Se cambio il listino ricalcolo sempre i prezzi, se cambio la valuta pure.
        *     ATTENZIONE alcune variabili caller sono definite anche locali per
        *     cui occorer utilizzare la loro estenzione completa e non definre una caller.
        *     Ad esempio w_ATCODLIS � LOCAL!
        this.w_ATCODLIS = this.oParentObject.w_ATCODLIS
        if this.oParentObject.w_ATCODVAL<>this.oParentObject.o_ATCODVAL
          * --- Se viene modificata la valuta e la valuta non � di conto sbianco il listino, parte il ricalcolo del listino
          if this.oParentObject.w_ATCODVAL=g_PERVAL
            this.w_ATCODLIS = this.w_CODLIS
          else
            this.w_ATCODLIS = ""
          endif
        endif
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParame=="4"
        * --- Modificata l'Autorit�, solo per isalt, lancio il calcolo prezzi prestazioni
        if this.w_ISALT
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pParame=="5" 
        * --- Modificate Ore Durata Effettiva
        this.w_T_OREEFF = this.oParentObject.w_ATOREEFF
        this.w_OREEFF = NVL(this.w_T_OREEFF, 0)
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParame=="6" 
        * --- Modificati Minuti Durata Effettiva
        this.w_T_MINEFF = this.oParentObject.w_ATMINEFF
        this.w_MINEFF = NVL(this.w_T_MINEFF, 0)
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParame=="7"
        this.w_STATUS = this.oParentObject.w_ATSTATUS
        if this.w_STATUS="P" OR this.w_STATUS="F"
          * --- Ore durata effettiva
          this.w_T_OREEFF = this.oParentObject.w_ATOREEFF
          this.w_OREEFF = NVL(this.w_T_OREEFF, 0)
          * --- Minuti durata effettiva
          this.w_T_MINEFF = this.oParentObject.w_ATMINEFF
          this.w_MINEFF = NVL(this.w_T_MINEFF, 0)
        else
          * --- Sbiancamento ore durata effettiva
          this.w_OREEFF = 0
          * --- Sbiancamento minuti durata effettiva
          this.w_MINEFF = 0
        endif
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParame=="8" or this.pParame=="P"
        * --- Se � vuoto il codice pratica dell'attivit� caricata al passo precedente
        if this.pParame=="P" and this.oParentObject.w_FLNSAP="S"
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_ObjMPA = this.oParentObject.GSAG_MPA
        if ((EMPTY(this.oParentObject.SalvaPrat) and Empty(this.oParentObject.w_ATCODPRA)) or not this.w_ISALT) 
          this.w_NUMRIG = 0
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_MODAPP<>"D"
            this.Page_11()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          this.w_CODPRA = iif(Empty(this.oParentObject.w_ATCODPRA),this.oParentObject.SalvaPrat,this.oParentObject.w_ATCODPRA)
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_MODAPP $ "D-E"
          this.Page_10()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pParame=="9" OR this.pParame=="R"
        * --- '9' Modifica/Inserimento/Cancellazione manuale di un Partecipante
        *     'R' bottone rigenera
        this.w_CAUATT = this.oParentObject.w_ATCAUATT
        if NOT EMPTY(NVL(this.w_CAUATT, SPACE(20)))
          this.w_ObjMDA = this.oParentObject.GSAG_MDA
          this.w_ObjMPA = this.oParentObject.GSAG_MPA
          this.w_ObjMDA = this.oParentObject.GSAG_MDA
          if (((this.w_ObjMDA.NumRow() > 0 and this.w_ObjMPA.w_PATIPRIS="P") or this.pParame=="R") and ah_YesNo("Si desidera rigenerare dettaglio prestazioni?"))
            * --- Prestazioni - Caricamento da Tipo Attivit�
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Prestazioni - Esplosione per i Partecipanti
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_ISALT
              if EMPTY(NVL( this.w_ATCODLIS,""))
                * --- calcolo prezzi, se il listino non � presente al cambio della pratica non 
                *     ricalcolo i prezzi, lascio le prestazioni con prezzo a 0 determinate a pag 2/3
                ah_ErrorMsg("Per determinare le tariffe � necessario selezionare un listino",64,"")
              else
                this.Page_8()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              if NOT EMPTY(NVL( this.w_ATCODLIS,""))
                * --- Prestazioni - Calcolo prezzi se presente un listino
                this.Page_5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
        endif
        this.oParentObject.o_DATFIN = this.oParentObject.w_DATFIN
        this.oParentObject.o_ORAFIN = this.oParentObject.w_ORAFIN
        this.oParentObject.o_MINFIN = this.oParentObject.w_MINFIN
        this.oParentObject.o_CA_TIMER = this.oParentObject.w_CA_TIMER
      case this.pParame=="B" and this.w_ISALT
        this.w_ObjMPA = this.oParentObject.GSAG_MPA
        if EMPTY(this.oParentObject.w_CACHKOBB) AND NOT EMPTY(this.oParentObject.w_ATCAUATT)
          * --- Sbiancata la pratica se nel tipo attivit� la pratica non � gestita
          this.w_NUMRIG = 0
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pParame=="T"
        * --- E' stata fatta una SAVE: mette a .T. la variabile AggiornaCalend
        this.oParentObject.w_AggiornaCalend = .T.
        this.oParentObject.w_AggiornaEleAtt = .T.
      case this.pParame=="N"
        * --- Notifica: se esiste la maschera del calendario, deve notificare il ChangeDate in modo da aggiornare la visualizzazione attiva
        do case
          case VARTYPE(this.oParentObject.w_MaskCalend)="O" AND this.oParentObject.w_AggiornaCalend
            * --- Notifica l'evento "ChangeDate" alla maschera GSAG_KAG (Calendario)
            this.oParentObject.w_MaskCalend.NotifyEvent("ChangeDate")     
          case VARTYPE(this.oParentObject.w_MaskEleAtt)="O" AND this.oParentObject.w_AggiornaEleAtt
            * --- Notifica l'evento "Ricerca" alla maschera GSAG_KRA (Elenco Attivit�)
            this.oParentObject.w_MaskEleAtt.NotifyEvent("Ricerca")     
        endcase
      case this.pParame=="M" OR this.pParame=="C" OR this.pParame=="D"
        * --- Invia la mail ai partecipanti.
        *     Possiamo scorrere OFF_PART perch� il salvataggio � gi� andato a buon fine oppure prima della cancellazione. 
        *     Appoggiandoci semplicemente a pParameSeriale possiamo utilizzare la procedura anche da GSAG_BAG.
        * --- Evito l'aggiornamento della gestione chiamante
        *     (in questo caso l'oggetto che contiene le attivit� da creare
        *     le cui variabili non necessitano di aggiornamenti al termine della routine)
        this.bUpdateParentObject = .F.
        if VARTYPE(this.pParameSeriale)="C" AND this.oParentObject.w_ATFLNOTI<>"N" AND (UPPER(this.oParentObject.class)=="ACTIVITYWRITER" OR this.pParame=="D" OR this.oParentObject.w_DataCambiata OR (type("this.oParentObject.cFunction")="C" And this.oParentObject.cFunction="Load"))
          * --- Utilizziamo w_PartSerial perch� modificando la data del calendario
          *     w_ATSERIAL non sarebbe pi� visibile nella query (gsag_aat->gsag_bag->gsag_bap->query)
          this.w_PartSerial = this.pParameSeriale
          this.w_CntDest = 0
          this.w_CntDestPst = 0
          this.w_TextPart = ""
          this.w_TextRis = ""
          this.w_TextGru = ""
          this.w_IndDestMail = ""
          this.w_IndDestMail_VCS = ""
          this.w_IndDestMail_ICS = ""
          this.w_IndDestMail_ICS_VCS = ""
          this.w_StatoAtti = ""
          this.w_LuogoAttivita = ""
          this.w_DescrizNominativo = ""
          this.w_CodiceNominativo = ""
          this.w_PersonaAttivita = ""
          this.w_Cellulare = ""
          this.w_Telefono = ""
          this.w_CodEntePra = ""
          this.w_CodUffiPra = ""
          this.w_NRuoloPra = ""
          this.w_DesEntePra = ""
          this.w_DesUffiPra = ""
          this.w_Giudice = ""
          this.w_Udienza = ""
          this.w_Eidrich = ""
          * --- Leggiamo i dati dell'attivit�: date, note e oggetto
          this.w_OLDCOL = g_PostInColor
          this.w_OLDPRIEML = g_EmailPrior
           
 g_PostInColor=this.w_COLPOS 
 g_EmailPrior=this.w_PRIEML
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNDESCAN"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.w_ATCOMRIC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNDESCAN;
              from (i_cTable) where;
                  CNCODCAN = this.w_ATCOMRIC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCODRIC = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from OFF_ATTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ATDATINI,ATDATFIN,ATNOTPIA,ATOGGETT,ATCODPRA,ATCAUATT,ATSTATUS,ATCODATT,ATLOCALI,ATPERSON,ATCODNOM,ATCELLUL,ATTELEFO,ATEVANNO,ATCODSED,ATCODUID,ATCOMRIC,ATSEREVE,ATNUMPRI"+;
              " from "+i_cTable+" OFF_ATTI where ";
                  +"ATSERIAL = "+cp_ToStrODBC(this.pParameSeriale);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ATDATINI,ATDATFIN,ATNOTPIA,ATOGGETT,ATCODPRA,ATCAUATT,ATSTATUS,ATCODATT,ATLOCALI,ATPERSON,ATCODNOM,ATCELLUL,ATTELEFO,ATEVANNO,ATCODSED,ATCODUID,ATCOMRIC,ATSEREVE,ATNUMPRI;
              from (i_cTable) where;
                  ATSERIAL = this.pParameSeriale;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DatIniAttivita = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
            this.w_DatFinAttivita = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
            this.w_NoteAttivita = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
            this.w_OggeAttivita = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
            this.w_CodPraAttivita = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
            this.w_CAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
            this.w_StatoAtti = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
            this.w_TipoAttivita = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
            this.w_LuogoAttivita = NVL(cp_ToDate(_read_.ATLOCALI),cp_NullValue(_read_.ATLOCALI))
            this.w_PersonaAttivita = NVL(cp_ToDate(_read_.ATPERSON),cp_NullValue(_read_.ATPERSON))
            this.w_CodiceNominativo = NVL(cp_ToDate(_read_.ATCODNOM),cp_NullValue(_read_.ATCODNOM))
            this.w_Cellulare = NVL(cp_ToDate(_read_.ATCELLUL),cp_NullValue(_read_.ATCELLUL))
            this.w_Telefono = NVL(cp_ToDate(_read_.ATTELEFO),cp_NullValue(_read_.ATTELEFO))
            this.w_ATEVANNO = NVL(cp_ToDate(_read_.ATEVANNO),cp_NullValue(_read_.ATEVANNO))
            this.w_ATCODSED = NVL(cp_ToDate(_read_.ATCODSED),cp_NullValue(_read_.ATCODSED))
            this.w_ATCODUID = NVL(cp_ToDate(_read_.ATCODUID),cp_NullValue(_read_.ATCODUID))
            this.w_ATCOMRIC = NVL(cp_ToDate(_read_.ATCOMRIC),cp_NullValue(_read_.ATCOMRIC))
            this.w_ATSEREVE = NVL(cp_ToDate(_read_.ATSEREVE),cp_NullValue(_read_.ATSEREVE))
            this.w_ATNUMPRI = NVL(cp_ToDate(_read_.ATNUMPRI),cp_NullValue(_read_.ATNUMPRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_ATNUMPRI = icase(Nvl(this.w_ATNUMPRI,0)=1,1,Nvl(this.w_ATNUMPRI,0)=3 ,5,9)
          * --- Leggiamo la descrizione della tipologia
          this.w_DescTipologia = ""
          * --- Lettura descrizione nominativo
          if !EMPTY(this.w_CodiceNominativo)
            * --- Read from OFF_NOMI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODCLI,NOTIPCLI,NOTIPNOM,NODESCRI"+;
                " from "+i_cTable+" OFF_NOMI where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_CodiceNominativo);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODCLI,NOTIPCLI,NOTIPNOM,NODESCRI;
                from (i_cTable) where;
                    NOCODICE = this.w_CodiceNominativo;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_Nindiri = NVL(cp_ToDate(_read_.NOINDIRI),cp_NullValue(_read_.NOINDIRI))
              this.w_Ncodcap = NVL(cp_ToDate(_read_.NO___CAP),cp_NullValue(_read_.NO___CAP))
              this.w_Nlocali = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
              this.w_Nprovi = NVL(cp_ToDate(_read_.NOPROVIN),cp_NullValue(_read_.NOPROVIN))
              this.w_CODICECLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
              this.w_TIPOCLI = NVL(cp_ToDate(_read_.NOTIPCLI),cp_NullValue(_read_.NOTIPCLI))
              this.w_TIPONOMINA = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
              this.w_DescrizNominativo = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_nomecompleto = alltrim(this.w_CodiceNominativo)+" "+this.w_DescrizNominativo
          endif
          * --- Read from OFFTIPAT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFFTIPAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFFTIPAT_idx,2],.t.,this.OFFTIPAT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TADESCRI"+;
              " from "+i_cTable+" OFFTIPAT where ";
                  +"TACODTIP = "+cp_ToStrODBC(this.w_TipoAttivita);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TADESCRI;
              from (i_cTable) where;
                  TACODTIP = this.w_TipoAttivita;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DescTipologia = NVL(cp_ToDate(_read_.TADESCRI),cp_NullValue(_read_.TADESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ANEVENTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ANEVENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2],.t.,this.ANEVENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "EVIDRICH"+;
              " from "+i_cTable+" ANEVENTI where ";
                  +"EV__ANNO = "+cp_ToStrODBC(this.w_ATEVANNO);
                  +" and EVSERIAL = "+cp_ToStrODBC(this.w_ATSEREVE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              EVIDRICH;
              from (i_cTable) where;
                  EV__ANNO = this.w_ATEVANNO;
                  and EVSERIAL = this.w_ATSEREVE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_Eidrich = NVL(cp_ToDate(_read_.EVIDRICH),cp_NullValue(_read_.EVIDRICH))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CAUMATTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAUMATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CARAGGST,CACOLPOS,CAPRIEML"+;
              " from "+i_cTable+" CAUMATTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CAUATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CARAGGST,CACOLPOS,CAPRIEML;
              from (i_cTable) where;
                  CACODICE = this.w_CAUATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TipoCategVCS = NVL(cp_ToDate(_read_.CARAGGST),cp_NullValue(_read_.CARAGGST))
            this.w_COLPOS = NVL(cp_ToDate(_read_.CACOLPOS),cp_NullValue(_read_.CACOLPOS))
            this.w_PRIEML = NVL(cp_ToDate(_read_.CAPRIEML),cp_NullValue(_read_.CAPRIEML))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from COM_ATTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COM_ATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COM_ATTI_idx,2],.t.,this.COM_ATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CACODIMP"+;
              " from "+i_cTable+" COM_ATTI where ";
                  +"CASERIAL = "+cp_ToStrODBC(this.pParameSeriale);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CACODIMP;
              from (i_cTable) where;
                  CASERIAL = this.pParameSeriale;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CACODIMP = NVL(cp_ToDate(_read_.CACODIMP),cp_NullValue(_read_.CACODIMP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_IMPIANTI = " "
          if !EMPTY(this.w_CACODIMP)
            * --- Select from QUERY\GSAG3BEX.VQR
            do vq_exec with 'QUERY\GSAG3BEX.VQR',this,'_Curs_QUERY_GSAG3BEX_d_VQR','',.f.,.t.
            if used('_Curs_QUERY_GSAG3BEX_d_VQR')
              select _Curs_QUERY_GSAG3BEX_d_VQR
              locate for 1=1
              do while not(eof())
              this.w_NoteCodiceImp = NVL(CACODIMP,"")
              this.w_NoteDescrImp = NVL(IMDESCRI,"")
              this.w_NoteCodiceCmp = NVL(CACODCOM,0)
              this.w_NoteDescrCmp = NVL(DESCON,"")
              * --- " - "+Ah_MsgFormat("Componente:")+SPACE(1)+ALLTRIM(GSAG3BEX->DESCON),"")+CHR(13)+CHR(10)
              this.w_Impianti = this.w_Impianti+Ah_MsgFormat("Impianto:")+SPACE(1)+ALLTRIM(this.w_NoteCodiceImp)+SPACE(1)+ALLTRIM(this.w_NoteDescrImp)+IIF(this.w_NoteCodiceCmp>0," - "+Ah_MsgFormat("Componente:")+SPACE(1)+ALLTRIM(this.w_NoteDescrCmp),"")+" <RITCAR>"
                select _Curs_QUERY_GSAG3BEX_d_VQR
                continue
              enddo
              use
            endif
          endif
          do case
            case this.w_TIPONOMINA<>"C"
              this.w_Erecapito = ALLTRIM(this.w_Nindiri)+" "+ALLTRIM(this.w_Ncodcap)+" "+ALLTRIM(this.w_Nlocali)+" "+ALLTRIM(this.w_Nprovi)
            case (this.w_TIPONOMINA="C" AND EMPTY(this.w_ATCODSED)) OR this.w_TIPONOMINA="F"
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPOCLI);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICECLI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPOCLI;
                      and ANCODICE = this.w_CODICECLI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_Cindiri = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
                this.w_Ccodcap = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
                this.w_Clocali = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
                this.w_Cprovi = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_Erecapito = ALLTRIM(this.w_Cindiri)+" "+ALLTRIM(this.w_Ccodcap)+" "+ALLTRIM(this.w_Clocali)+" "+ALLTRIM(this.w_Cprovi)
            case this.w_TIPONOMINA="C" AND NOT EMPTY(this.w_ATCODSED)
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPOCLI);
                      +" and DDCODICE = "+cp_ToStrODBC(this.w_CODICECLI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN;
                  from (i_cTable) where;
                      DDTIPCON = this.w_TIPOCLI;
                      and DDCODICE = this.w_CODICECLI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_Dindiri = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
                this.w_Dcodcap = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
                this.w_Dlocali = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
                this.w_Dprovi = NVL(cp_ToDate(_read_.DDPROVIN),cp_NullValue(_read_.DDPROVIN))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_Erecapito = ALLTRIM(this.w_Dindiri)+" "+ALLTRIM(this.w_Dcodcap)+" "+ALLTRIM(this.w_Dlocali)+" "+ALLTRIM(this.w_Dprovi)
          endcase
          * --- Chiede "Si desidera inviare gli avvisi" se NON � AE oppure se NON proveniamo da "Nuove attivit� collegate"
          if ISALT()
            this.w_OK = !this.w_StatoAtti=="P" 
          else
            this.w_OK = !this.w_StatoAtti=="F" AND !this.w_StatoAtti=="P" AND (this.pNoAskMail OR Ah_YesNo("Si desidera inviare gli avvisi?"))
          endif
          if this.w_OK
            * --- Pratica e descrizione
            * --- Procede con l'invio dei messaggi solo se l'attivit� non � stata evaas o completata
            if !EMPTY(this.w_CodPraAttivita)
              * --- Pratica e descrizione
              * --- Read from CAN_TIER
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAN_TIER_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CNDESCAN,CN__ENTE,CNUFFICI,CNRIFGIU"+;
                  " from "+i_cTable+" CAN_TIER where ";
                      +"CNCODCAN = "+cp_ToStrODBC(this.w_CodPraAttivita);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CNDESCAN,CN__ENTE,CNUFFICI,CNRIFGIU;
                  from (i_cTable) where;
                      CNCODCAN = this.w_CodPraAttivita;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DesPraAttivita = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
                this.w_CodEntePra = NVL(cp_ToDate(_read_.CN__ENTE),cp_NullValue(_read_.CN__ENTE))
                this.w_CodUffiPra = NVL(cp_ToDate(_read_.CNUFFICI),cp_NullValue(_read_.CNUFFICI))
                this.w_NRuoloPra = NVL(cp_ToDate(_read_.CNRIFGIU),cp_NullValue(_read_.CNRIFGIU))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if IsAlt()
                if !EMPTY(this.w_CodEntePra)
                  * --- Lettura ente
                  * --- Read from PRA_ENTI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2],.t.,this.PRA_ENTI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "EPDESCRI"+;
                      " from "+i_cTable+" PRA_ENTI where ";
                          +"EPCODICE = "+cp_ToStrODBC(this.w_CodEntePra);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      EPDESCRI;
                      from (i_cTable) where;
                          EPCODICE = this.w_CodEntePra;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_DesEntePra = NVL(cp_ToDate(_read_.EPDESCRI),cp_NullValue(_read_.EPDESCRI))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                if !EMPTY(this.w_CodUffiPra)
                  * --- Lettura ufficio
                  * --- Read from PRA_UFFI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PRA_UFFI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRA_UFFI_idx,2],.t.,this.PRA_UFFI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "UFDESCRI"+;
                      " from "+i_cTable+" PRA_UFFI where ";
                          +"UFCODICE = "+cp_ToStrODBC(this.w_CodUffiPra);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      UFDESCRI;
                      from (i_cTable) where;
                          UFCODICE = this.w_CodUffiPra;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_DesUffiPra = NVL(cp_ToDate(_read_.UFDESCRI),cp_NullValue(_read_.UFDESCRI))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                * --- Lettura della prossima udienza demandata da GSPR_BCO
                this.w_Giudice = GetGiudici(this.w_CodPraAttivita, "", this.w_DatFinAttivita, 100, "")
                this.w_CNCODCAN = this.w_CodPraAttivita
                GSPR_BCO(this,"UDIENZA_MSG")
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if !EMPTY(this.w_UDIENZASERIAL) AND this.w_UDIENZASERIAL<>this.pParameSeriale
                  * --- Abbiamo una data di prossima udienza per la pratica
                  this.w_Udienza = ALLTRIM(DTOC(this.w_DATAUDIENZA)+" "+this.w_ORAUDI+":"+this.w_MINUDI+" "+LEFT(this.w_OGGETTOUDIENZA,80))
                endif
              endif
            endif
            this.w_DataIni = TTOD(this.w_DatIniAttivita)
            this.w_DataFin = TTOD(this.w_DatFinAttivita)
            this.w_OraIni = PADL(ALLTRIM(STR(HOUR(this.w_DatIniAttivita))),2,"0")+":"++PADL(ALLTRIM(STR(MINUTE(this.w_DatIniAttivita))),2,"0")
            this.w_orafine = PADL(ALLTRIM(STR(HOUR(this.w_DatFinAttivita))),2,"0")+":"++PADL(ALLTRIM(STR(MINUTE(this.w_DatFinAttivita))),2,"0")
            this.w_RitornoCarrello = CHR(13)+CHR(10)
            * --- Leggiamo da PAR_AGEN le var. per la composizione del messaggio
            this.w_OggeMail = ""
            this.w_TxtPostIn = ""
            this.w_TextMail = ""
            do case
              case this.pParame=="C"
                * --- Read from PAR_AGEN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PATXTCRE,PAOGGCRE,PAPSTCRE"+;
                    " from "+i_cTable+" PAR_AGEN where ";
                        +"PACODAZI = "+cp_ToStrODBC(i_CodAzi);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PATXTCRE,PAOGGCRE,PAPSTCRE;
                    from (i_cTable) where;
                        PACODAZI = i_CodAzi;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TextMail = NVL(cp_ToDate(_read_.PATXTCRE),cp_NullValue(_read_.PATXTCRE))
                  this.w_OggeMail = NVL(cp_ToDate(_read_.PAOGGCRE),cp_NullValue(_read_.PAOGGCRE))
                  this.w_TxtPostIn = NVL(cp_ToDate(_read_.PAPSTCRE),cp_NullValue(_read_.PAPSTCRE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              case this.pParame=="M"
                * --- Read from PAR_AGEN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PATXTMOD,PAOGGMOD,PAPSTMOD"+;
                    " from "+i_cTable+" PAR_AGEN where ";
                        +"PACODAZI = "+cp_ToStrODBC(i_CodAzi);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PATXTMOD,PAOGGMOD,PAPSTMOD;
                    from (i_cTable) where;
                        PACODAZI = i_CodAzi;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TextMail = NVL(cp_ToDate(_read_.PATXTMOD),cp_NullValue(_read_.PATXTMOD))
                  this.w_OggeMail = NVL(cp_ToDate(_read_.PAOGGMOD),cp_NullValue(_read_.PAOGGMOD))
                  this.w_TxtPostIn = NVL(cp_ToDate(_read_.PAPSTMOD),cp_NullValue(_read_.PAPSTMOD))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              case this.pParame=="D"
                * --- Read from PAR_AGEN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PATXTDEL,PAOGGDEL,PAPSTDEL"+;
                    " from "+i_cTable+" PAR_AGEN where ";
                        +"PACODAZI = "+cp_ToStrODBC(i_CodAzi);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PATXTDEL,PAOGGDEL,PAPSTDEL;
                    from (i_cTable) where;
                        PACODAZI = i_CodAzi;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TextMail = NVL(cp_ToDate(_read_.PATXTDEL),cp_NullValue(_read_.PATXTDEL))
                  this.w_OggeMail = NVL(cp_ToDate(_read_.PAOGGDEL),cp_NullValue(_read_.PAOGGDEL))
                  this.w_TxtPostIn = NVL(cp_ToDate(_read_.PAPSTDEL),cp_NullValue(_read_.PAPSTDEL))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
            endcase
            if EMPTY(this.w_TextMail)
              * --- Utilizziamo il messaggio di default
              this.w_TextMail = "La informiamo che � stata effettuata la seguente operazione:<RITCAR>"
              do case
                case this.pParame=="C"
                  this.w_TextMail = this.w_TextMail+"Nuova attivit�<RITCAR>"
                case this.pParame=="M"
                  this.w_TextMail = this.w_TextMail+"Modifica attivit�<RITCAR>"
                case this.pParame=="D"
                  this.w_TextMail = this.w_TextMail+"Cancellazione attivit�<RITCAR>"
              endcase
              this.w_TextMail = this.w_TextMail+"Oggetto: <OGGETT><RITCAR>"
              this.w_TextMail = this.w_TextMail+"Data e ora inizio: <DATAINI> <ORAINI><RITCAR>"
              this.w_TextMail = this.w_TextMail+"Data e ora fine: <DATAFIN> <ORAFIN><RITCAR>"
              this.w_TextMail = this.w_TextMail+IIF(this.w_ISALT,"Pratica:","Commessa:")+"<CODPRA> <DESPRA><RITCAR>"
              this.w_TextMail = this.w_TextMail+"Partecipanti: <PERSONE><RITCAR>"
              this.w_TextMail = this.w_TextMail+"Risorse: <RISORSE><RITCAR>"
              this.w_TextMail = this.w_TextMail+"Note: <NOTPIA><RITCAR>"
              this.w_TextMail = this.w_TextMail+"Cordiali saluti. <DESUTE><RITCAR>"
            endif
            if EMPTY(this.w_OggeMail)
              * --- Oggetto e-mail di default
              do case
                case this.pParame=="C"
                  this.w_OggeMail = "Nuova attivit�:"
                case this.pParame=="M"
                  this.w_OggeMail = "Modifica attivit�:"
                case this.pParame=="D"
                  this.w_OggeMail = "Cancellazione attivit�:"
              endcase
              this.w_OggeMail = this.w_OggeMail+" <OGGETT>"
            endif
            if EMPTY(this.w_TxtPostIn)
              * --- Messaggio post-in di default
              do case
                case this.pParame=="C"
                  this.w_TxtPostIn = "Nuova attivit�: "
                case this.pParame=="M"
                  this.w_TxtPostIn = "Modifica attivit�: "
                case this.pParame=="D"
                  this.w_TxtPostIn = "Cancellazione attivit�: "
              endcase
              this.w_TxtPostIn = this.w_TxtPostIn+"<OGGETT> <RITCAR>"
              this.w_TxtPostIn = this.w_TxtPostIn+"Tipo: <CAUATT><RITCAR>"
              this.w_TxtPostIn = this.w_TxtPostIn+"Dal: <DATAINI> ore <ORAINI><RITCAR>"
              this.w_TxtPostIn = this.w_TxtPostIn+"Al: <DATAFIN> ore <ORAFIN><RITCAR>"
              this.w_TxtPostIn = this.w_TxtPostIn+IIF(this.w_ISALT,"Pratica:","Commessa:")+"<CODPRA> <DESPRA><RITCAR>"
              this.w_TxtPostIn = this.w_TxtPostIn+"Partecipanti: <PERSONE><RITCAR>"
              this.w_TxtPostIn = this.w_TxtPostIn+"Risorse: <RISORSE><RITCAR>"
              this.w_TxtPostIn = this.w_TxtPostIn+"Note: <NOTPIA><RITCAR>"
              this.w_TxtPostIn = this.w_TxtPostIn+"Cordiali saluti. <DESUTE><RITCAR>"
            endif
            this.w_CodRis = readdipend(i_CodUte, "D")
            * --- Creazione cursore per preparazione messaggio per invio mail e post-in
            CREATE CURSOR APPOMESS ;
            (DATAINI D(8), ORAINI C(5), DATAFIN D(8), ORAFIN C(5), OGGETT C(254), TIPATT C(50), DESUTE C(100), NOTPIA M(10), CODPRA C(15), DESPRA C(100), PERSONE M(10), RISORSE M(10), RITCAR C(2), CAUATT C(20), DATAELA D(8),; 
 ORAELA C(5), CODNOM C(15), DESNOM C(60), PERSONA C(60), TELEFO C(18), CELLUL C(18), LOCALI C(30), RUOLOPRA C(30), ENTEPRA C(60), UFFIPRA C(60), GIUDICE C(100), UDIENZA C(100),INDIRI C(100),IDRICH C(15),COMCOS C(15),DESCCO C(100),COMRIC C(15),DECMRI C(100),DETIMP C(254))
            * --- Inserimento nel cursore di tutti i dati necessari
            INSERT INTO APPOMESS (DATAINI, ORAINI, DATAFIN, ORAFIN, OGGETT, TIPATT, DESUTE, NOTPIA, CODPRA, DESPRA, PERSONE, RISORSE, RITCAR, CAUATT, DATAELA, ORAELA, CODNOM, DESNOM, PERSONA, TELEFO, CELLUL, LOCALI, RUOLOPRA, ENTEPRA, UFFIPRA, GIUDICE, UDIENZA,INDIRI,IDRICH,COMCOS,DESCCO,COMRIC,DECMRI,DETIMP) VALUES;
            (this.w_DATAINI, this.w_ORAINI, this.w_DATAFIN, this.w_orafine, this.w_OggeAttivita, this.w_DescTipologia, this.w_CodRis, this.w_NoteAttivita, this.w_CodPraAttivita, this.w_DesPraAttivita, this.w_TextPart, this.w_TextRis, this.w_RitornoCarrello, this.w_CAUATT, this.w_DataEla, this.w_OraEla, ; 
 this.w_CodiceNominativo, this.w_DescrizNominativo, this.w_PersonaAttivita, this.w_Telefono, this.w_Cellulare, this.w_LuogoAttivita, this.w_NRuoloPra, this.w_DesEntePra, this.w_DesUffiPra, this.w_Giudice, this.w_Udienza,this.w_Erecapito,this.w_Eidrich, this.w_CodPraAttivita, this.w_DesPraAttivita,this.w_ATCOMRIC,this.w_DESCODRIC,this.w_IMPIANTI)
            * --- Select from QUERY\GSAG_BAP.VQR
            do vq_exec with 'QUERY\GSAG_BAP.VQR',this,'_Curs_QUERY_GSAG_BAP_d_VQR','',.f.,.t.
            if used('_Curs_QUERY_GSAG_BAP_d_VQR')
              select _Curs_QUERY_GSAG_BAP_d_VQR
              locate for 1=1
              do while not(eof())
              this.w_DPVCSICS = NVL(DPVCSICS,"A")
              do case
                case DPTIPRIS="P"
                  this.w_TextPart = this.w_TextPart+ ALLTRIM( ALLTRIM(NVL(DPCOGNOM,""))+" "+ALLTRIM(NVL(DPNOME,"")) )+","+SPACE(1)
                case DPTIPRIS="R"
                  this.w_TextRis = this.w_TextRis+ALLTRIM(DPDESCRI)+","+SPACE(1)
                case DPTIPRIS="G"
                  this.w_TextGru = this.w_TextGru+ALLTRIM(DPDESCRI)+","+SPACE(1)
              endcase
              if !EMPTY(NVL(DPINDMAI,"")) AND ( NVL(DPFLAVVI,"")=="M" OR NVL(DPFLAVVI,"")=="E")
                * --- C'� un indirizzo da gestire, quindi certamente � una persona
                this.w_CntDest = this.w_CntDest + 1
                * --- A secondo della scelta dell'utente dobbiamo inviare VCS, ICS o entrambi
                * --- Valori del campo DPVCSICS in GSAR_ADP: A - Da attivit�, N - Nessun allegato, V - VCS, I - ICS, E- ICS + VCS
                * --- Valori del campo ATFLNOTI: N - Nessun avviso, S- Avviso, C - Avviso con VCS, I - Avviso con ICS, E - Avviso ICS e VCS
                do case
                  case (this.w_DPVCSICS="A" AND this.oParentObject.w_ATFLNOTI="S") OR this.w_DPVCSICS="N"
                    * --- Utente da attivit� e attivit� senza allegati oppure utente senza allegati
                    this.w_IndDestMail = this.w_IndDestMail + ALLTRIM(DPINDMAI) + "; "
                  case (this.w_DPVCSICS="A" AND this.oParentObject.w_ATFLNOTI="C") OR this.w_DPVCSICS="V"
                    * --- Utente da attivit� e attivit� con VCS oppure utente con solo allegato VCS
                    this.w_IndDestMail_VCS = this.w_IndDestMail_VCS + ALLTRIM(DPINDMAI) + "; "
                  case (this.w_DPVCSICS="A" AND this.oParentObject.w_ATFLNOTI="I") OR this.w_DPVCSICS="I"
                    * --- Utente da attivit� e attivit� con ICS oppure utente con solo allegato ICS
                    this.w_IndDestMail_ICS = this.w_IndDestMail_ICS + ALLTRIM(DPINDMAI) + "; "
                  case (this.w_DPVCSICS="A" AND this.oParentObject.w_ATFLNOTI="E") OR this.w_DPVCSICS="E"
                    * --- Utente da attivit� e attivit� con VCS + ICS oppure utente con entrambi gli allegati
                    this.w_IndDestMail_ICS_VCS = this.w_IndDestMail_ICS_VCS + ALLTRIM(DPINDMAI) + "; "
                  otherwise
                    * --- Combinazione non gestita
                    this.w_CntDest = this.w_CntDest - 1
                endcase
              endif
              if !EMPTY(NVL(DPCODUTE,0)) AND ( NVL(DPFLAVVI,"")=="P" OR NVL(DPFLAVVI,"")=="E")
                * --- Destinatario per PostIN
                this.w_CntDestPst = this.w_CntDestPst + 1
              endif
                select _Curs_QUERY_GSAG_BAP_d_VQR
                continue
              enddo
              use
            endif
            * --- Tolgo virgola finale
            this.w_TextPart = left(this.w_TextPart,len(this.w_TextPart)-2)
            this.w_TextRis = left(this.w_TextRis,len(this.w_TextRis)-2)
            this.w_TextGru = left(this.w_TextGru,len(this.w_TextGru)-2)
            * --- Data/ora elaborazione: per evitare incongurenze di elaborazione a cavallo della mezzanotte, utilizzo una variabile di appoggio
            this.w_DataOraEla = DateTime()
            this.w_DataEla = TTOD(this.w_DataOraEla)
            this.w_OraEla = PADL(ALLTRIM(STR(HOUR(this.w_DataOraEla))),2,"0")+":"++PADL(ALLTRIM(STR(MINUTE(this.w_DataOraEla))),2,"0")
            * --- Inserisce nel cursore i partecipanti
            UPDATE APPOMESS SET PERSONE=this.w_TextPart , RISORSE=this.w_TextRis, DATAELA=this.w_DataEla, ORAELA=this.w_OraEla
            if this.w_CntDestPst>0
              * --- Invia i PostIn
              this.w_TxtPostIn = CUR_TO_MSG( "APPOMESS" , "" , this.w_TxtPostIn , "" )
              this.w_BOTTONE = " "
              if this.pParame<>"D"
                this.w_BOTTONE = GSUT_BMB("GSAG_AAT","Attivit�",i_codazi,this.pParameSeriale)
              endif
              * --- Select from QUERY\GSAG_BAP.VQR
              do vq_exec with 'QUERY\GSAG_BAP.VQR',this,'_Curs_QUERY_GSAG_BAP_d_VQR','',.f.,.t.
              if used('_Curs_QUERY_GSAG_BAP_d_VQR')
                select _Curs_QUERY_GSAG_BAP_d_VQR
                locate for 1=1
                do while not(eof())
                if !EMPTY(NVL(DPCODUTE,0)) AND ( NVL(DPFLAVVI,"")=="P" OR NVL(DPFLAVVI,"")=="E")
                  * --- Invia un Post-in, abbiamo gi� il testo del messaggio
                  GSUT_BIP(this, DPCODUTE , this.w_TxtPostIn , "",this.w_BOTTONE )
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                  select _Curs_QUERY_GSAG_BAP_d_VQR
                  continue
                enddo
                use
              endif
            endif
            if this.w_CntDest > 0
              g_EmailPrior=this.w_PRIEML
              * --- Ci sono partecipanti, invia la mail
              this.w_TextPart = SUBSTR(this.w_TextPart,1, LEN(this.w_TextPart)-2 )
              this.w_TextRis = SUBSTR(this.w_TextRis,1, LEN(this.w_TextRis)-2 )
              this.w_TextGru = SUBSTR(this.w_TextGru,1, LEN(this.w_TextGru)-2 )
              * --- Componiamo il messaggio
              this.w_TextMail = CUR_TO_MSG( "APPOMESS" , "" , this.w_TextMail, "" )
              this.w_OggeMail = CUR_TO_MSG( "APPOMESS" , "" , this.w_OggeMail, "" )
              this.w_PathVCS = ""
              this.w_PathICS = ""
              this.Page_9()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.pParame=="D"
                if EMPTY(this.w_IndDestMail_ICS)
                  this.w_IndDestMail_ICS = this.w_IndDestMail_ICS_VCS
                  this.w_IndDestMail_ICS_VCS = ""
                endif
                DELETE FILE (this.w_PathVCS)
              endif
              if LEN(this.w_IndDestMail_VCS) > 2
                * --- Solo allegato VCS
                GestMail(this,this.w_PathVCS,g_UEDIAL,,SUBSTR(this.w_IndDestMail_VCS,1, LEN(this.w_IndDestMail_VCS)-2 ),this.w_OggeMail,this.w_TextMail)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              if LEN(this.w_IndDestMail_ICS) > 2
                * --- Solo allegato ICS
                GestMail(this,this.w_PathICS,g_UEDIAL,,SUBSTR(this.w_IndDestMail_ICS,1, LEN(this.w_IndDestMail_ICS)-2 ),this.w_OggeMail,this.w_TextMail)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              if LEN(this.w_IndDestMail_ICS_VCS) > 2
                * --- Entrambi gli allegati
                Dimension aText(2) 
 store "" to aText(1),aText(2)
                DIMENSION ics_vcs(2)
                ics_vcs(1)=this.w_PathVCS
                ics_vcs(2)=this.w_PathICS
                GestMail(this,@ics_vcs,g_UEDIAL,,SUBSTR(this.w_IndDestMail_ICS_VCS,1, LEN(this.w_IndDestMail_ICS_VCS)-2 ),this.w_OggeMail,this.w_TextMail)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              if LEN(this.w_IndDestMail) > 2
                * --- Nessun allegato
                GestMail(this,"",g_UEDIAL,,SUBSTR(this.w_IndDestMail,1, LEN(this.w_IndDestMail)-2 ),this.w_OggeMail,this.w_TextMail)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              * --- Eventuale cancellazione degli allegati non inviati
              if EMPTY(this.w_IndDestMail_ICS) AND EMPTY(this.w_IndDestMail_ICS_VCS) AND FILE(this.w_PathICS)
                * --- Cancellazione ICS
                DELETE FILE (this.w_PathICS)
              endif
              if EMPTY(this.w_IndDestMail_VCS) AND EMPTY(this.w_IndDestMail_ICS_VCS) AND FILE(this.w_PathVCS)
                * --- Cancellazione VCS
                DELETE FILE (this.w_PathVCS)
              endif
            endif
            USE IN SELECT("APPOMESS")
          endif
           
 g_PostInColor=this.w_OLDCOL 
 g_EmailPrior=this.w_OLDPRIEML
        endif
      case this.pParame=="A" 
        if upper(g_APPLICATION) = "ADHOC REVOLUTION"
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANFLGCON"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_ATTIPCLI);
                  +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCLI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANFLGCON;
              from (i_cTable) where;
                  ANTIPCON = this.oParentObject.w_ATTIPCLI;
                  and ANCODICE = this.oParentObject.w_CODCLI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_FLGCON = NVL(cp_ToDate(_read_.ANFLGCON),cp_NullValue(_read_.ANFLGCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NVL(this.oParentObject.w_FLGCON," ")="S"
            if !AH_YesNo("Attenzione: sul cliente � attivo il blocco per contenzioso %0Procedere comunque con la selezione del nominativo?")
              SETVALUELINKED("M", this.oparentobject, "w_ATCODNOM",SPACE(15))
              THIS.OPARENTOBJECT.w_ATLOCALI=IIF(this.w_ISALT, THIS.OPARENTOBJECT.w_LOC_PRA, THIS.OPARENTOBJECT.w_LOC_NOM)
              i_retcode = 'stop'
              return
            endif
          endif
        else
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANFLBLVE"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_ATTIPCLI);
                  +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCLI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANFLBLVE;
              from (i_cTable) where;
                  ANTIPCON = this.oParentObject.w_ATTIPCLI;
                  and ANCODICE = this.oParentObject.w_CODCLI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_FLGCON = NVL(cp_ToDate(_read_.ANFLBLVE),cp_NullValue(_read_.ANFLBLVE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NVL(this.oParentObject.w_FLGCON," ")="S"
            if !AH_YesNo("Attenzione: sul cliente � attivo il blocco vendite %0Procedere comunque con la selezione del nominativo?")
              SETVALUELINKED("M", this.oparentobject, "w_ATCODNOM",SPACE(15))
              THIS.OPARENTOBJECT.w_ATLOCALI= THIS.OPARENTOBJECT.w_LOC_NOM
              i_retcode = 'stop'
              return
            endif
          endif
        endif
        * --- Rilancio ricalcolo prezzi al cambiare del nominativo
        this.w_GESTIONE = this.oParentObject
        this.w_CODLIS = this.w_GESTIONE.w_ATCODLIS
        this.w_SCOLIC = this.w_GESTIONE.w_SCOLIC
        this.w_GESTIONE.w_ATCODLIS = IIF(EMPTY(this.w_GESTIONE.w_NUMLIS), IIF(Not Empty(this.w_GESTIONE.w_NCODLIS),this.w_GESTIONE.w_NCODLIS,this.w_GESTIONE.w_CODLIS), this.w_GESTIONE.w_NUMLIS)
        this.w_GESTIONE.NotifyEvent("AggList")     
        if (this.w_GESTIONE.w_ATCODLIS<>this.w_GESTIONE.o_ATCODLIS and !this.oParentObject.w_GENDAEV) OR !isalt()
          * --- se il listino clienti � pieno e senza sconti su listini lo assegno
          this.w_ObjMDA = this.oParentObject.GSAG_MDA
          if this.w_ObjMDA.Numrow()>0 
            if Ah_yesno("Si desidera aggiornare il prezzo su riga?")
              this.w_RICALCOLA = .t.
              this.Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- ho risposto di no devo ripristinare il listino
              this.w_GESTIONE.w_ATCODLIS = this.w_GESTIONE.o_ATCODLIS
              this.w_GESTIONE.o_ATCODNOM = this.w_GESTIONE.w_ATCODNOM
            endif
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prestazioni - Caricamento da Tipo Attivit�
    this.w_ObjMDA = this.oParentObject.GSAG_MDA
    if this.w_ObjMDA.NumRow()>0
      * --- Azzera Righe prestazioni
      this.w_ObjMDA.FirstRow()     
      do while ! this.w_ObjMDA.Eof_Trs()
        this.w_ObjMDA.DeleteRow()     
        this.w_ObjMDA.NextRow()     
      enddo
      * --- Se elimino tutto ricreo un record vuoto e mi ci posiziono...
      this.w_ObjMDA.AddRow()     
    endif
    if NOT EMPTY(NVL(this.w_CAUATT, SPACE(20)))
      this.w_CODRES = SPACE(5)
      this.w_COGNOME = SPACE(40)
      this.w_NOME = SPACE(40)
      * --- Memorizzo il primo partecipante
      this.w_ObjMPA = this.oParentObject.GSAG_MPA
      this.w_ObjMPA.MarkPos()     
      this.w_ObjMPA.FirstRow()     
      * --- w_PRIMOPAR indica se ho trovato il primo partecipante con tipologia persona
      this.w_PRIMOPAR = .F.
      do while NOT this.w_ObjMPA.Eof_Trs() AND NOT this.w_PRIMOPAR
        this.w_ObjMPA.SetRow()     
        if NVL(this.w_ObjMPA.w_PATIPRIS, "Z")="P"
          * --- Trovato il primo partecipante con tipologia persona
          this.w_PRIMOPAR = .T.
          this.w_CODRES = NVL(this.w_ObjMPA.w_PACODRIS, SPACE(5))
          this.w_COGNOME = NVL(this.w_ObjMPA.w_COGNOME, SPACE(40))
          this.w_NOME = NVL(this.w_ObjMPA.w_NOME, SPACE(40))
        else
          this.w_ObjMPA.NextRow()     
        endif
      enddo
      this.w_ObjMPA.RePos()     
      this.w_NUMRIG = 0
      * --- Procede solo se ha trovato il partecipante
      if !EMPTY(this.w_CODRES)
        * --- Lettura costo orario del partecipante all'attivit�
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPCOSORA,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPTIPRIG"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_CODRES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPCOSORA,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPTIPRIG;
            from (i_cTable) where;
                DPCODICE = this.w_CODRES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COSORA = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
          this.w_COGNOME = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
          this.w_NOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
          this.w_CODCEN = NVL(cp_ToDate(_read_.DPCODCEN),cp_NullValue(_read_.DPCODCEN))
          this.w_TIPRIS = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
          this.w_DTIPRIG = NVL(cp_ToDate(_read_.DPTIPRIG),cp_NullValue(_read_.DPTIPRIG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_COSORA = NVL(this.w_COSORA,0)
        * --- Select from CAU_ATTI
        i_nConn=i_TableProp[this.CAU_ATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_ATTI_idx,2],.t.,this.CAU_ATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CPROWORD,CACODSER,CADESSER,CADESAGG,CAFLRESP,CAFLDEFF,CATIPRIS,CATIPRIG,CAKEYART,CATIPRI2  from "+i_cTable+" CAU_ATTI ";
              +" where CACODICE = "+cp_ToStrODBC(this.w_CAUATT)+"";
              +" order by CPROWORD";
               ,"_Curs_CAU_ATTI")
        else
          select CPROWORD,CACODSER,CADESSER,CADESAGG,CAFLRESP,CAFLDEFF,CATIPRIS,CATIPRIG,CAKEYART,CATIPRI2 from (i_cTable);
           where CACODICE = this.w_CAUATT;
           order by CPROWORD;
            into cursor _Curs_CAU_ATTI
        endif
        if used('_Curs_CAU_ATTI')
          select _Curs_CAU_ATTI
          locate for 1=1
          do while not(eof())
          * --- Controllo congruenza tipologia prestazione con il tipo ente
          this.w_INSPRE = .T.
          if this.w_ISAHE
            this.w_CODSER = NVL(_Curs_CAU_ATTI.CAKEYART, SPACE(41))
          else
            this.w_CODSER = NVL(_Curs_CAU_ATTI.CACODSER, SPACE(20))
          endif
          if NOT EMPTY(NVL(this.w_CODPRA, SPACE(15))) and this.w_ISALT
            this.w_INSPRE = chktipen(this.w_CODSER,this.w_CODPRA," ")
          endif
          if this.w_INSPRE
            * --- Nuova Riga del Temporaneo
            this.w_ObjMDA.AddRow()     
            this.w_NUMRIG = this.w_NUMRIG+10
            this.w_ObjMDA.w_CPROWORD = NVL(_Curs_CAU_ATTI.CPROWORD, 0)
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CACODART,CAUNIMIS,CADTOBSO,CAMOLTIP,CA__TIPO,CAOPERAT"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_CODSER);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CACODART,CAUNIMIS,CADTOBSO,CAMOLTIP,CA__TIPO,CAOPERAT;
                from (i_cTable) where;
                    CACODICE = this.w_CODSER;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
              this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
              this.w_CADTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
              w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
              this.w_TIPSER = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
              this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
              this.w_TIPO = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_ISAHE
              this.w_CODSER = NVL(_Curs_CAU_ATTI.CAKEYART, SPACE(41))
              this.w_ObjMDA.w_DACODICE = this.w_CODSER
              this.w_ObjMDA.w_ECOD1ATT = this.w_CODART
              this.w_ObjMDA.w_ETIPSER = this.w_TIPO
            else
              this.w_CODSER = NVL(_Curs_CAU_ATTI.CACODSER, SPACE(20))
              this.w_ObjMDA.w_DACODATT = this.w_CODSER
              this.w_ObjMDA.w_RCOD1ATT = this.w_CODART
              this.w_ObjMDA.w_RTIPSER = this.w_TIPO
            endif
            this.w_ObjMDA.w_TIPSER = this.w_TIPO
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARFLSPAN,ARTIPRIG,ARTIPRI2,ARPRESTA,ARFLUNIV"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARFLSPAN,ARTIPRIG,ARTIPRI2,ARPRESTA,ARFLUNIV;
                from (i_cTable) where;
                    ARCODART = this.w_CODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
              this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
              this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
              this.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
              this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
              this.w_FLSPAN = NVL(cp_ToDate(_read_.ARFLSPAN),cp_NullValue(_read_.ARFLSPAN))
              this.w_ARTIPRIG = NVL(cp_ToDate(_read_.ARTIPRIG),cp_NullValue(_read_.ARTIPRIG))
              this.w_ARTIPRI2 = NVL(cp_ToDate(_read_.ARTIPRI2),cp_NullValue(_read_.ARTIPRI2))
              this.w_ARPRESTA = NVL(cp_ToDate(_read_.ARPRESTA),cp_NullValue(_read_.ARPRESTA))
              this.w_FLUNIV = NVL(cp_ToDate(_read_.ARFLUNIV),cp_NullValue(_read_.ARFLUNIV))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_ObjMDA.w_FLSPAN = this.w_FLSPAN
            this.w_ObjMDA.w_DACODATT = this.w_CODSER
            this.w_ObjMDA.w_SERPER = this.w_ObjMDA.w_DACODATT
            this.w_ObjMDA.w_COD1ATT = this.w_CODART
            this.w_UNIMIS = NVL(this.w_UNIMIS, SPACE(3))
            * --- Se sulla pratica � definita Tariffa concordata e se siamo in presenza di una tariffa a tempo
            if ISALT() AND this.oParentObject.w_ATTARTEM="C" AND this.w_ARPRESTA="P"
              * --- Dichiarazione array tariffe a tempo concordate
              DECLARE ARRTARCON (3,1)
              * --- Azzero l'array che verr� riempito dalla funzione Caltarcon()
              ARRTARCON(1)=0 
 ARRTARCON(2)= " " 
 ARRTARCON(3)=0
              this.w_CALCONC = CALTARCON(this.oParentObject.w_ATCODPRA, this.w_CODRES, this.w_CODSER, @ARRTARCON)
              * --- Tariffa concordata
              this.oParentObject.w_ATTARCON = ARRTARCON(1)
              this.w_UNIMIS = IIF(Not Empty(ARRTARCON(2)), ARRTARCON(2), this.w_UNIMIS)
              this.w_UNMIS1 = IIF(Not Empty(ARRTARCON(2)), ARRTARCON(2), this.w_UNMIS1)
              this.w_ObjMDA.w_DAQTAMOV = IIF(Not Empty(ARRTARCON(3)), ARRTARCON(3), this.w_ObjMDA.w_DAQTAMOV)
            endif
            this.w_ObjMDA.w_DAUNIMIS = this.w_UNIMIS
            * --- Read from UNIMIS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.UNIMIS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ"+;
                " from "+i_cTable+" UNIMIS where ";
                    +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ;
                from (i_cTable) where;
                    UMCODICE = this.w_UNIMIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESUNI = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
              this.w_FLTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
              this.w_DURORE = NVL(cp_ToDate(_read_.UMDURORE),cp_NullValue(_read_.UMDURORE))
              this.w_FL_FRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_FLTEMP = NVL(this.w_FLTEMP, SPACE(1))
            this.w_DURORE = NVL(this.w_DURORE, 0)
            this.w_ObjMDA.w_FL_FRAZ = NVL(this.w_FL_FRAZ, SPACE(1))
            this.w_ObjMDA.w_DESUNI = NVL(this.w_DESUNI, SPACE(35))
            this.w_ObjMDA.w_UNMIS1 = NVL(this.w_UNMIS1, SPACE(3))
            this.w_ObjMDA.w_UNMIS2 = NVL(this.w_UNMIS2, SPACE(3))
            this.w_ObjMDA.w_UNMIS3 = NVL(this.w_UNMIS3, SPACE(3))
            this.w_ObjMDA.w_OPERA3 = this.w_OPERA3
            this.w_ObjMDA.w_TIPSER = this.w_TIPSER
            this.w_ObjMDA.w_FLSERG = NVL(this.w_FLSERG, " ")
            this.w_ObjMDA.w_DADATMOD = i_datsys
            this.w_ObjMDA.w_ARPRESTA = this.w_ARPRESTA
            this.w_ObjMDA.w_FLUNIV = this.w_FLUNIV
            * --- Traduzione in Lingua della Descrizione Articoli 
            this.w_ObjMDA.w_DADESATT = NVL(_Curs_CAU_ATTI.CADESSER, SPACE(40))
            this.w_ObjMDA.w_DADESAGG = NVL(_Curs_CAU_ATTI.CADESAGG, SPACE(256))
            if NOT EMPTY(this.oParentObject.w_CODLIN) AND this.oParentObject.w_CODLIN<>g_CODLIN
              * --- Legge la Traduzione
              DECLARE ARRRIS (2) 
 a=Tradlin(this.w_ObjMDA.w_DACODATT,this.oParentObject.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
              if Not Empty(this.w_DESCOD)
                this.w_ObjMDA.w_DADESATT = this.w_DESCOD
                this.w_ObjMDA.w_DADESAGG = this.w_DESSUP
              endif
            endif
            this.w_ObjMDA.w_FLRESP = NVL(_Curs_CAU_ATTI.CAFLRESP, SPACE(1))
            this.w_FLDEFF = NVL(_Curs_CAU_ATTI.CAFLDEFF, SPACE(1))
            this.w_ObjMDA.w_DAFLDEFF = this.w_FLDEFF
            this.w_ObjMDA.w_DUR_ORE = this.w_DURORE
            this.w_ObjMDA.w_CHKTEMP = this.w_FLTEMP
            this.w_ObjMDA.w_TIPRIS = NVL(_Curs_CAU_ATTI.CATIPRIS, SPACE(1))
            if this.w_FLTEMP="S"
              * --- La quantit� della prestazione � 1
              this.w_ObjMDA.w_DAOREEFF = INT(this.w_DURORE)
              this.w_ObjMDA.w_DAMINEFF = INT((this.w_DURORE - INT(this.w_DURORE)) * 60)
            else
              this.w_ObjMDA.w_DAOREEFF = 0
              this.w_ObjMDA.w_DAMINEFF = 0
            endif
            * --- Responsabile valorizzato con il primo partecipante
            this.w_ObjMDA.w_DACODRES = this.w_CODRES
            this.w_ObjMDA.w_COST_ORA = this.w_COSORA
            this.w_ObjMDA.w_DACOSUNI = this.w_COSORA
            this.w_ObjMDA.w_DACOSINT = (this.w_ObjMDA.w_DAOREEFF+(this.w_ObjMDA.w_DAMINEFF/60))*this.w_COSORA
            this.w_ObjMDA.w_COGNOME = this.w_COGNOME
            this.w_ObjMDA.w_NOME = this.w_NOME
            this.w_ObjMDA.w_DENOM_RESP = alltrim(NVL(this.w_COGNOME, ""))+" "+alltrim(NVL(this.w_NOME, ""))
            this.w_ObjMDA.w_TIPART = this.w_TIPART
            this.w_ObjMDA.w_CACODART = this.w_CODART
            if this.w_ISAHE
              this.w_ObjMDA.w_ECACODART = this.w_CODART
              this.w_ObjMDA.w_ECADTOBSO = this.w_CADTOBSO
              this.w_ObjMDA.w_EDESAGG = this.w_ObjMDA.w_DADESAGG
              this.w_ObjMDA.w_EDESATT = this.w_ObjMDA.w_DADESATT
            else
              this.w_ObjMDA.w_RCACODART = this.w_CODART
              this.w_ObjMDA.w_RCADTOBSO = this.w_CADTOBSO
              this.w_ObjMDA.w_RDESAGG = this.w_ObjMDA.w_DADESAGG
              this.w_ObjMDA.w_RDESATT = this.w_ObjMDA.w_DADESATT
            endif
            this.w_ObjMDA.w_CADTOBSO = this.w_CADTOBSO
            this.w_ObjMDA.w_ARTIPRIG = this.w_ARTIPRIG
            this.w_ObjMDA.w_ARTIPRI2 = this.w_ARTIPRI2
            this.w_ObjMDA.w_TIPRIS = this.w_TIPRIS
            this.w_ObjMDA.w_CATIPRIG = NVL(_Curs_CAU_ATTI.CATIPRIG, SPACE(1))
            this.w_ObjMDA.w_CATIPRI2 = NVL(_Curs_CAU_ATTI.CATIPRI2, SPACE(1))
            this.w_ObjMDA.w_ARTIPRIG = this.w_ARTIPRIG
            this.w_ObjMDA.w_ARTIPRI2 = this.w_ARTIPRI2
            if this.w_ISALT
              this.w_ObjMDA.w_DTIPRIG = this.w_DTIPRIG
              this.w_ObjMDA.w_DATIPRIG = IIF(this.w_DTIPRIG="N","N",NVL(_Curs_CAU_ATTI.CATIPRIG, this.w_ARTIPRIG))
              this.w_ObjMDA.w_DATIPRI2 = NVL(_Curs_CAU_ATTI.CATIPRI2, this.w_ARTIPRI2)
              this.w_ObjMDA.w_ATIPRIG = IIF(this.w_DTIPRIG="N","N",NVL(_Curs_CAU_ATTI.CATIPRIG, this.w_ARTIPRIG))
              this.w_ObjMDA.w_ATIPRI2 = NVL(_Curs_CAU_ATTI.CATIPRI2, this.w_ARTIPRI2)
            else
              this.w_ObjMDA.w_DATIPRIG = NVL(_Curs_CAU_ATTI.CATIPRIG, SPACE(1))
              this.w_ObjMDA.w_ATIPRIG = NVL(_Curs_CAU_ATTI.CATIPRIG, SPACE(1))
            endif
            this.w_ObjMDA.w_ETIPRIG = NVL(_Curs_CAU_ATTI.CATIPRIG, SPACE(1))
            this.w_ObjMDA.w_ETIPRI2 = NVL(_Curs_CAU_ATTI.CATIPRI2, SPACE(1))
            this.w_ObjMDA.w_RTIPRIG = NVL(_Curs_CAU_ATTI.CATIPRIG, SPACE(1))
            this.w_ObjMDA.w_RTIPRI2 = NVL(_Curs_CAU_ATTI.CATIPRI2, SPACE(1))
            this.w_ObjMDA.w_DATCOINI = this.oparentobject.w_ATINIRIC
            this.w_ObjMDA.w_DATCOFIN = this.oparentobject.w_ATFINRIC
            this.w_ObjMDA.w_DATRIINI = this.oparentobject.w_ATINICOS
            this.w_ObjMDA.w_DATRIFIN = this.oparentobject.w_ATFINCOS
            if g_COAN="S" and Not empty(this.w_CODART)
              * --- La voce di ricavo � condizionata al flag analitica della causale documento
              this.w_ObjMDA.w_DAVOCRIC = SearchVoc(this,"R",this.w_CODART,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_CAUDOC,this.oparentobject.w_ATCODNOM)
              * --- La voce di costo � condizionata al flag movimento di analitica del tipo attivit�
              this.w_ObjMDA.w_DAVOCCOS = SearchVoc(this,"C",this.w_CODART,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_CAUDOC,this.oparentobject.w_ATCODNOM)
              this.w_ObjMDA.w_DCODCEN = this.w_CODCEN
              this.w_ObjMDA.w_DACENCOS = IIF(NOT EMPTY(NVL(this.w_CODCEN, SPACE(15))), NVL(this.w_CODCEN, SPACE(15)), This.oparentobject.w_ATCENCOS)
              this.w_ObjMDA.o_DACODRES = this.w_ObjMDA.w_DACODRES
              this.w_ObjMDA.w_DACENRIC = IIF(NOT EMPTY(NVL(this.w_CODCEN, SPACE(15))), NVL(this.w_CODCEN, SPACE(15)), This.oparentobject.w_ATCENRIC)
            endif
            if ! this.w_ISALT
              * --- La voce di costo � condizionata al flag movimento di analitica del tipo attivit�
              this.w_ObjMDA.w_DACOMRIC = this.oparentobject.w_ATCOMRIC
              this.w_ObjMDA.w_DAATTRIC = this.oparentobject.w_ATATTRIC
            endif
            * --- La voce di costo � condizionata al flag movimento di analitica del tipo attivit�
            this.w_ObjMDA.w_DACODCOM = this.oparentobject.w_ATCODPRA
            this.w_ObjMDA.w_DAATTIVI = this.oparentobject.w_ATATTCOS
            this.w_ObjMDA.SaveRow()     
          endif
            select _Curs_CAU_ATTI
            continue
          enddo
          use
        endif
      endif
      * --- Nel caso in cui non abbia trovato neanche un partecipante, inserisce una riga vuota
      if this.w_NUMRIG=0
        this.w_ObjMDA.AddRow()     
      endif
    else
      this.w_ObjMDA.AddRow()     
    endif
    this.w_NUMRIG = NVL(this.w_ObjMDA.w_CPROWORD, 0)
    this.w_ObjMDA.FirstRow()     
    this.w_ObjMDA.SetRow()     
    this.w_ObjMDA.Refresh()     
    this.w_ObjMDA.bUpdated = .T.
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prestazioni - Esplosione per i Partecipanti
    * --- Caricamento Pratica seguito da caricamento Tipo
    this.w_ObjMPA = this.oParentObject.GSAG_MPA
    this.w_ObjMPA.MarkPos()     
    this.w_ObjMPA.FirstRow()     
    do while ! this.w_ObjMPA.Eof_Trs()
      this.w_ObjMPA.SetRow()     
      if Not Deleted() and not Empty(this.w_ObjMPA.w_PACODRIS) and this.w_ObjMPA.w_PATIPRIS="P"
        this.w_CODRIS = this.w_ObjMPA.w_PACODRIS
        if this.w_ObjMPA.NumRow()>1
          * --- Esplode righe prestazioni per i partecipanti
          if NOT this.w_PRIPAR
            * --- Il primo partecipante � gi� stato inserito nel campo codice responsabile di ogni riga (Prestazione)
            this.w_ObjMDA.MarkPos()     
            this.w_ObjMDA.FirstRow()     
            do while ! this.w_ObjMDA.Eof_Trs()
              this.w_ObjMDA.SetRow()     
              if this.w_ObjMDA.w_FLRESP="S"
                this.w_CODSER = this.w_ObjMDA.w_DACODATT
                this.w_CODICE = this.w_ObjMDA.w_DACODICE
                this.w_DESATT = this.w_ObjMDA.w_DADESATT
                this.w_DESAGG = this.w_ObjMDA.w_DADESAGG
                this.w_CODART = this.w_ObjMDA.w_COD1ATT
                * --- Lettura costo orario del partecipante all'attivit�
                * --- Read from DIPENDEN
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DIPENDEN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DPCOSORA,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPTIPRIG"+;
                    " from "+i_cTable+" DIPENDEN where ";
                        +"DPCODICE = "+cp_ToStrODBC(this.w_CODRIS);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DPCOSORA,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPTIPRIG;
                    from (i_cTable) where;
                        DPCODICE = this.w_CODRIS;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_COSORA = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
                  this.w_COGNOME = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
                  this.w_NOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
                  this.w_CODCEN = NVL(cp_ToDate(_read_.DPCODCEN),cp_NullValue(_read_.DPCODCEN))
                  this.w_TIPRIS = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
                  this.w_DTIPRIG = NVL(cp_ToDate(_read_.DPTIPRIG),cp_NullValue(_read_.DPTIPRIG))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_COSORA = NVL(this.w_COSORA,0)
                this.w_CODOPE = this.w_ObjMDA.w_DACODOPE
                this.w_UNIMIS = this.w_ObjMDA.w_DAUNIMIS
                this.w_DESUNI = this.w_ObjMDA.w_DESUNI
                this.w_UNMIS1 = this.w_ObjMDA.w_UNMIS1
                this.w_UNMIS2 = this.w_ObjMDA.w_UNMIS2
                this.w_UNMIS3 = this.w_ObjMDA.w_UNMIS3
                this.w_FLSERG = this.w_ObjMDA.w_FLSERG
                this.w_DATMOD = this.w_ObjMDA.w_DADATMOD
                this.w_FLRESP = this.w_ObjMDA.w_FLRESP
                this.w_FLDEFF = this.w_ObjMDA.w_DAFLDEFF
                this.w_OREEFF = this.w_ObjMDA.w_DAOREEFF
                this.w_MINEFF = this.w_ObjMDA.w_DAMINEFF
                this.w_TIPART = this.w_ObjMDA.w_TIPART
                this.w_DURORE = this.w_ObjMDA.w_DUR_ORE
                this.w_FLTEMP = this.w_ObjMDA.w_CHKTEMP
                this.w_CADTOBSO = this.w_ObjMDA.w_CADTOBSO
                this.w_TIPO = this.w_ObjMDA.w_TIPSER
                this.w_FL_FRAZ = this.w_ObjMDA.w_FL_FRAZ
                this.w_TIPRIG = iCASE(Not empty(this.w_ObjMDA.w_DATIPRIG),this.w_ObjMDA.w_DATIPRIG,icase(!EMPTY(this.w_ObjMDA.w_CATIPRIG),this.w_ObjMDA.w_CATIPRIG, Not empty(this.w_ObjMDA.w_CAUACQ) and Not empty(this.w_ObjMDA.w_CAUDOC),"E",Not empty(this.w_ObjMDA.w_CAUACQ),"A","D"))
                this.w_TIPRI2 = this.w_ObjMDA.w_DATIPRI2
                * --- Inserimento nel cursore per esplosione 
                 
 INSERT INTO APPODETT VALUES; 
 (this.w_CODICE,this.w_CODSER,this.w_CODART,this.w_DESATT,this.w_DESAGG,this.w_CODRIS,this.w_CODOPE,this.w_UNIMIS,this.w_UNMIS1,this.w_UNMIS2,this.w_UNMIS3, ; 
 this.w_FLSERG,this.w_DESUNI,this.w_DATMOD,this.w_FLRESP,this.w_FLDEFF,this.w_OREEFF,this.w_MINEFF,this.w_COSORA, ; 
 this.w_TIPART,this.w_COGNOME,this.w_NOME,this.w_DURORE,this.w_FLTEMP,this.w_CADTOBSO,this.w_CODCEN,this.w_TIPRIS,this.w_TIPO,this.w_FL_FRAZ,this.w_TIPRIG,this.w_TIPRI2, this.w_DTIPRIG)
              endif
              this.w_ObjMDA.NextRow()     
            enddo
            this.w_ObjMDA.RePos()     
          else
            this.w_PRIPAR = .F.
            * --- Creazione cursore per esplosione 
             
 CREATE CURSOR APPODETT ; 
 (DACODICE C(41),DACODATT C(20),COD1ATT C(20),DADESATT C(40),DADESAGG M(10),DACODRES C(5),DACODOPE N(4,0),DAUNIMIS C(3),UNMIS1 C(3),UNMIS2 C(3),UNMIS3 C(3), ; 
 FLSERG C(1),DESUNI C(35),DADATMOD D(8),FLRESP C(1),DAFLDEFF C(1),DAOREEFF N(3,0),DAMINEFF N(2,0),COST_ORA N(18,4), ; 
 TIPART C(2),COGNOME C(50),NOME C(50),DURORE N(6,2),FLTEMP C(1),CADTOBSO D(8),DACENCOS C(15),TIPRIS C(1),TIPSER C(1),FL_FRAZ C(1),DATIPRIG C(1),DATIPRI2 C(1),DTIPRIG C(1))
          endif
        endif
      endif
      this.w_ObjMPA.NextRow()     
    enddo
    this.w_ObjMPA.RePos()     
    if USED("APPODETT")
      SELECT APPODETT
      GO TOP
      SCAN
      this.w_CODSER = APPODETT.DACODATT
      this.w_CODICE = APPODETT.DACODICE
      this.w_CODART = APPODETT.COD1ATT
      this.w_DESATT = APPODETT.DADESATT
      this.w_DESAGG = APPODETT.DADESAGG
      this.w_CODRES = APPODETT.DACODRES
      this.w_COSORA = APPODETT.COST_ORA
      this.w_CODOPE = APPODETT.DACODOPE
      this.w_UNIMIS = APPODETT.DAUNIMIS
      this.w_DESUNI = APPODETT.DESUNI
      this.w_UNMIS1 = APPODETT.UNMIS1
      this.w_UNMIS2 = APPODETT.UNMIS2
      this.w_UNMIS3 = APPODETT.UNMIS3
      this.w_FLSERG = APPODETT.FLSERG
      this.w_DATMOD = APPODETT.DADATMOD
      this.w_FLRESP = APPODETT.FLRESP
      this.w_FLDEFF = APPODETT.DAFLDEFF
      this.w_OREEFF = APPODETT.DAOREEFF
      this.w_MINEFF = APPODETT.DAMINEFF
      this.w_NUMRIG = this.w_NUMRIG+10
      this.w_TIPART = APPODETT.TIPART
      this.w_COGNOME = APPODETT.COGNOME
      this.w_NOME = APPODETT.NOME
      this.w_FLTEMP = APPODETT.FLTEMP
      this.w_DURORE = APPODETT.DURORE
      this.w_CADTOBSO = APPODETT.CADTOBSO
      this.w_TIPO = APPODETT.TIPSER
      this.w_FL_FRAZ = APPODETT.FL_FRAZ
      this.w_TIPRIG = APPODETT.DATIPRIG
      this.w_TIPRI2 = APPODETT.DATIPRI2
      this.w_DTIPRIG = APPODETT.DTIPRIG
      this.w_CODCEN = APPODETT.DACENCOS
      this.w_ObjMDA.AddRow()     
      this.w_ObjMDA.w_CPROWORD = this.w_NUMRIG
      this.w_ObjMDA.w_DACODATT = this.w_CODSER
      this.w_ObjMDA.w_DACODICE = this.w_CODICE
      this.w_ObjMDA.w_COD1ATT = this.w_CODART
      this.w_ObjMDA.w_DAUNIMIS = this.w_UNIMIS
      this.w_ObjMDA.w_DESUNI = this.w_DESUNI
      this.w_ObjMDA.w_UNMIS1 = this.w_UNMIS1
      this.w_ObjMDA.w_UNMIS2 = this.w_UNMIS2
      this.w_ObjMDA.w_UNMIS3 = this.w_UNMIS3
      this.w_ObjMDA.w_FLSERG = this.w_FLSERG
      this.w_ObjMDA.w_DADATMOD = this.w_DATMOD
      * --- Traduzione in Lingua della Descrizione Articoli 
      this.w_ObjMDA.w_DADESATT = this.w_DESATT
      this.w_ObjMDA.w_DADESAGG = this.w_DESAGG
      if NOT EMPTY(this.oParentObject.w_CODLIN) AND this.oParentObject.w_CODLIN<>g_CODLIN
        * --- Legge la Traduzione
        DECLARE ARRRIS (2) 
 a=Tradlin(this.w_ObjMDA.w_DACODATT,this.oParentObject.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
        if Not Empty(this.w_DESCOD)
          this.w_ObjMDA.w_DADESATT = this.w_DESCOD
          this.w_ObjMDA.w_DADESAGG = this.w_DESSUP
        endif
      endif
      this.w_ObjMDA.w_FLRESP = this.w_FLRESP
      this.w_ObjMDA.w_DAFLDEFF = this.w_FLDEFF
      this.w_ObjMDA.w_DAOREEFF = this.w_OREEFF
      this.w_ObjMDA.w_DAMINEFF = this.w_MINEFF
      this.w_ObjMDA.w_DACODRES = this.w_CODRES
      this.w_ObjMDA.w_TIPRIS = this.w_TIPRIS
      this.w_ObjMDA.w_COST_ORA = this.w_COSORA
      this.w_ObjMDA.w_TIPART = this.w_TIPART
      this.w_ObjMDA.w_COGNOME = this.w_COGNOME
      this.w_ObjMDA.w_NOME = this.w_NOME
      this.w_ObjMDA.w_DUR_ORE = this.w_DURORE
      this.w_ObjMDA.w_CHKTEMP = this.w_FLTEMP
      this.w_ObjMDA.w_DENOM_RESP = alltrim(NVL(this.w_COGNOME, ""))+" "+alltrim(NVL(this.w_NOME, ""))
      this.w_ObjMDA.w_CACODART = this.w_CODART
      this.w_ObjMDA.w_DATIPRIG = IIF(this.w_DTIPRIG="N","N",this.w_TIPRIG)
      this.w_ObjMDA.w_CATIPRIG = IIF(this.w_DTIPRIG="N","N",this.w_TIPRIG)
      this.w_ObjMDA.w_DATIPRI2 = this.w_TIPRI2
      this.w_ObjMDA.w_ETIPRIG = this.w_TIPRIG
      this.w_ObjMDA.w_ETIPRI2 = this.w_TIPRI2
      this.w_ObjMDA.w_ATIPRIG = IIF(this.w_DTIPRIG="N","N",this.w_TIPRIG)
      this.w_ObjMDA.w_ATIPRI2 = this.w_TIPRI2
      this.w_ObjMDA.w_RTIPRIG = this.w_TIPRIG
      this.w_ObjMDA.w_RTIPRI2 = this.w_TIPRI2
      this.w_ObjMDA.w_ARTIPRIG = this.w_TIPRIG
      this.w_ObjMDA.w_ARTIPRI2 = this.w_TIPRI2
      this.w_ObjMDA.w_DATCOINI = this.oparentobject.w_ATINIRIC
      this.w_ObjMDA.w_DATCOFIN = this.oparentobject.w_ATFINRIC
      this.w_ObjMDA.w_DATRIINI = this.oparentobject.w_ATINICOS
      this.w_ObjMDA.w_DATRIFIN = this.oparentobject.w_ATFINCOS
      if this.w_ISAHE
        this.w_ObjMDA.w_ECACODART = this.w_CODART
        this.w_ObjMDA.w_ECADTOBSO = this.w_CADTOBSO
        this.w_ObjMDA.w_ETIPRIG = this.w_TIPRIG
        this.w_ObjMDA.w_ETIPRI2 = this.w_TIPRI2
        this.w_ObjMDA.w_ATIPRIG = this.w_TIPRIG
        this.w_ObjMDA.w_ATIPRI2 = this.w_TIPRI2
        this.w_ObjMDA.w_RTIPRIG = this.w_TIPRIG
        this.w_ObjMDA.w_RTIPRI2 = this.w_TIPRI2
        this.w_ObjMDA.w_ECOD1ATT = this.w_CODART
        if !EMPTY(NVL(DACENCOS,SPACE(15)))
          this.w_ObjMDA.w_DACENRIC = DACENCOS
        else
          * --- Nel caso in cui sia nullo, deve attingere dal centro di costo dell'attivit�
          this.w_ObjMDA.w_DACENRIC = This.oparentobject.w_ATCENRIC
        endif
        this.w_ObjMDA.w_EDESAGG = this.w_DESAGG
        this.w_ObjMDA.w_EDESATT = this.w_DESATT
        this.w_ObjMDA.w_DACOMRIC = this.oparentobject.w_ATCOMRIC
        this.w_ObjMDA.w_DAATTRIC = this.oparentobject.w_ATATTRIC
        this.w_ObjMDA.w_DACODCOM = this.oparentobject.w_ATCODPRA
        this.w_ObjMDA.w_DAATTIVI = this.oparentobject.w_ATATTCOS
        this.w_ObjMDA.w_DAVOCCOS = SearchVoc(this,"C",this.w_CODART,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_CAUDOC,this.oparentobject.w_ATCODNOM)
        if !EMPTY(NVL(DACENCOS,SPACE(15)))
          this.w_ObjMDA.w_DACENCOS = DACENCOS
        else
          * --- Nel caso in cui sia nullo, deve attingere dal centro di costo dell'attivit�
          this.w_ObjMDA.w_DACENCOS = This.oparentobject.w_ATCENCOS
        endif
      else
        this.w_ObjMDA.w_RCOD1ATT = this.w_CODART
        this.w_ObjMDA.w_RCACODART = this.w_CODART
        this.w_ObjMDA.w_RCADTOBSO = this.w_CADTOBSO
        this.w_ObjMDA.w_RDESAGG = this.w_DESAGG
        this.w_ObjMDA.w_RDESATT = this.w_DESATT
        this.w_ObjMDA.w_DAVOCCOS = SearchVoc(this,"C",this.w_CODART,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_CAUDOC,this.oparentobject.w_ATCODNOM)
        this.w_ObjMDA.w_DACENCOS = IIF(NOT EMPTY(NVL(this.w_CODCEN, SPACE(15))), NVL(this.w_CODCEN, SPACE(15)), This.oparentobject.w_ATCENCOS)
      endif
      this.w_ObjMDA.w_DACENRIC = IIF(NOT EMPTY(NVL(this.w_CODCEN, SPACE(15))), NVL(this.w_CODCEN, SPACE(15)), This.oparentobject.w_ATCENRIC)
      if g_COAN="S"
        this.w_ObjMDA.w_DAVOCRIC = SearchVoc(this,"R",this.w_CODART,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_CAUDOC,this.oparentobject.w_ATCODNOM) 
      endif
      * --- La voce di costo � condizionata al flag movimento di analitica del tipo attivit�
      this.w_ObjMDA.w_DACODCOM = this.oparentobject.w_ATCODPRA
      this.w_ObjMDA.w_DAATTIVI = this.oparentobject.w_ATATTCOS
      this.w_ObjMDA.w_CADTOBSO = this.w_CADTOBSO
      this.w_ObjMDA.w_TIPSER = this.w_TIPO
      this.w_ObjMDA.w_FL_FRAZ = this.w_FL_FRAZ
      this.w_ObjMDA.w_DACOSUNI = this.w_COSORA
      this.w_ObjMDA.w_DACOSINT = (this.w_ObjMDA.w_DAOREEFF+(this.w_ObjMDA.w_DAMINEFF/60))*this.w_COSORA
      this.w_ObjMDA.o_DACODRES = this.w_ObjMDA.w_DACODRES
      this.w_ObjMDA.SaveRow()     
      SELECT APPODETT
      ENDSCAN
      this.w_ObjMDA.FirstRow()     
      this.w_ObjMDA.SetRow()     
      this.w_ObjMDA.Refresh()     
      this.w_ObjMDA.bUpdated = .T.
      USE IN SELECT("APPODETT")
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Propaga Ore e minuti durata effettiva sul dettaglio Prestazioni
    this.w_ObjMDA = this.oParentObject.GSAG_MDA
    this.w_ObjMDA.MarkPos()     
    this.w_ObjMDA.FirstRow()     
    do while ! this.w_ObjMDA.Eof_Trs()
      this.w_ObjMDA.SetRow()     
      if NVL(this.w_ObjMDA.w_DAFLDEFF," ")="S"
        do case
          case this.pParame=="5" or this.pParame=="7"
            this.w_ObjMDA.Set("w_DAOREEFF" , this.w_OREEFF)     
        endcase
        do case
          case this.pParame=="6" or this.pParame=="7"
            this.w_ObjMDA.Set("w_DAMINEFF" , this.w_MINEFF)     
        endcase
        * --- Valorizzazione del partecipante per successiva lettura del suo costo orario
        this.w_CODRES = NVL(this.w_ObjMDA.w_DACODRES,SPACE(5))
        * --- Lettura costo orario del partecipante all'attivit�
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPCOSORA"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_CODRES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPCOSORA;
            from (i_cTable) where;
                DPCODICE = this.w_CODRES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COSORA = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_COSORA = NVL(this.w_COSORA,0)
        * --- Calcolo del costo interno
        this.w_COSINT = (this.w_OREEFF+(this.w_MINEFF/60))*this.w_COSORA
        * --- Scrittura del costo interno
        this.w_ObjMDA.Set("w_DACOSUNI" , this.w_COSORA)     
        this.w_ObjMDA.Set("w_DACOSINT" , this.w_COSINT)     
      endif
      this.w_ObjMDA.SetUpdateRow()     
      this.w_ObjMDA.NextRow()     
    enddo
    this.w_ObjMDA.RePos()     
    this.w_ObjMDA.bUpdated = .T.
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prestazioni - Calcolo prezzi da Tariffario
    this.w_ObjMDA = this.oParentObject.GSAG_MDA
    this.w_GESTIONE = this.oParentObject
    if this.w_RICALCOLA and !this.w_ISALT
      do case
        case this.pParame=="3"
          if this.w_ISAHE
            this.w_RICALCOLA = ah_YesNo("Si desidera aggiornare prezzi e voci di analitica?")
          else
            this.w_RICALCOLA = ah_YesNo("Si desidera aggiornare i prezzi?")
          endif
        case this.pParame=="E"
          this.w_RICALCOLA = ah_YesNo("Si desidera aggiornare i costi?")
        case this.pParame=="V"
          this.w_RICALCOLA = ah_YesNo("Si desidera aggiornare prezzi e costi?")
      endcase
      if ! this.w_RICALCOLA
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_ObjMDA.MarkPos()     
    this.w_ObjMDA.FirstRow()     
    do while ! this.w_ObjMDA.Eof_Trs()
      this.w_ObjMDA.SetRow()     
      this.w_ObjMDA.o_DACODRES = this.w_ObjMDA.w_DACODRES
      * --- Evito di rileggere i dati dall'articolo...
      this.w_ObjMDA.o_SERPER = this.w_ObjMDA.w_SERPER
      do case
        case this.w_RICALCOLA
          this.oParentObject.GSAG_MDA.NotifyEvent("RicAna")
          this.w_ObjMDA.w_DAVOCRIC = SearchVoc(this,"R",this.w_ObjMDA.w_COD1ATT,this.w_GESTIONE.w_ATTIPCLI,this.w_GESTIONE.w_CODCLI,this.w_GESTIONE.w_CAUDOC,this.w_GESTIONE.w_ATCODNOM)
          this.w_ObjMDA.w_DAVOCCOS = SearchVoc(this,"C",this.w_ObjMDA.w_COD1ATT,this.w_GESTIONE.w_ATTIPCLI,this.w_GESTIONE.w_CODCLI,this.w_GESTIONE.w_CAUACQ,this.w_GESTIONE.w_ATCODNOM)
          this.oParentObject.GSAG_MDA.NotifyEvent("Aggcosto")
        case this.pParame=="3" or this.pParame=="V" or this.pParame=="E"
          do case
            case this.pParame=="3"
              if this.w_ISAHE
                this.oParentObject.GSAG_MDA.NotifyEvent("Caltes")
              else
                this.oParentObject.GSAG_MDA.NotifyEvent("Agglis")
              endif
            case this.pParame=="E"
              this.oParentObject.GSAG_MDA.NotifyEvent("LisAcq")
              this.oParentObject.GSAG_MDA.NotifyEvent("Aggcosto")
            case this.pParame=="V"
              this.oParentObject.GSAG_MDA.NotifyEvent("Caltes")
          endcase
        otherwise
          if this.w_ObjMDA.w_DARIFPRE=0
            this.oParentObject.GSAG_MDA.NotifyEvent("Calcola")
            this.oParentObject.GSAG_MDA.NotifyEvent("Aggcosto")
            if this.w_ObjMDA.w_FLSPAN="S"
              this.w_ROW = this.w_ObjMDA.Rowindex()
              this.oParentObject.GSAG_MDA.NotifyEvent("CalcPrest")
              this.w_ObjMDA.SetRow(this.w_ROW)     
            endif
          endif
      endcase
      this.oParentObject.GSAG_MDA.NotifyEvent("Asstrad")
      this.w_ObjMDA.SaveRow()     
      this.w_ObjMDA.NextRow()     
      this.w_ObjMDA.Refresh()     
    enddo
    this.w_ObjMDA.RePos()     
    this.w_ObjMDA.bUpdated = .T.
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo se esiste almeno una riga di prestazione con importo diverso da zero
    this.w_TOTALE = 0
    this.w_ObjMDA = this.oParentObject.GSAG_MDA
    this.w_ObjMDA.MarkPos()     
    this.w_ObjMDA.FirstRow()     
    do while ! this.w_ObjMDA.Eof_Trs()
      if this.w_ObjMDA.FullRow()
        this.w_TOTALE = this.w_TOTALE + NVL( this.w_ObjMDA.Get("w_DAVALRIG") , 0)
      endif
      this.w_ObjMDA.NextRow()     
    enddo
    this.w_ObjMDA.RePos()     
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se viene cambiata la pratica presente nell'attivit� viene eseguito il seguente controllo:
    *     se la pratica � priva di partecipanti i dati vengono lasciati invariati a meno che non si tratti della prima valorizzazione del campo pratica
    *     Viceversa, se la pratica scelta � priva di partecipanti ma nell'attivit� era gi� stato inserito il codice di una pratica, 
    *     allora vengono azzerati i partecipanti e cancellati i responsabili dalle prestazioni.
    *     Ovviamente, se la pratica inserita nell'attivit� ha almeno un partecipante, allora i dati partecipanti e responsabili delle prastazioni vengono modificati.
    this.ContaRisPrat = 0
    if this.w_ISALT
      * --- Select from RIS_INTE
      i_nConn=i_TableProp[this.RIS_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIS_INTE_idx,2],.t.,this.RIS_INTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select RICODRIS  from "+i_cTable+" RIS_INTE ";
            +" where RICODPRA = "+cp_ToStrODBC(this.w_CODPRA)+" AND RIFLATTI='S'";
             ,"_Curs_RIS_INTE")
      else
        select RICODRIS from (i_cTable);
         where RICODPRA = this.w_CODPRA AND RIFLATTI="S";
          into cursor _Curs_RIS_INTE
      endif
      if used('_Curs_RIS_INTE')
        select _Curs_RIS_INTE
        locate for 1=1
        do while not(eof())
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPDTOBSO"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(_Curs_RIS_INTE.RICODRIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPDTOBSO;
            from (i_cTable) where;
                DPCODICE = _Curs_RIS_INTE.RICODRIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.DPDTOBSO),cp_NullValue(_read_.DPDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_DATOBSO) OR this.w_DATOBSO>this.oParentObject.w_DATINI
          this.ContaRisPrat = this.ContaRisPrat + 1
        endif
          select _Curs_RIS_INTE
          continue
        enddo
        use
      endif
    endif
    if this.oParentObject.w_MODAPP<>"D"
      if this.ContaRisPrat > 0 OR NOT EMPTY( this.oParentObject.o_ATCODPRA )
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if NOT EMPTY(NVL(this.w_CODPRA, SPACE(15)))
          * --- Select from RIS_INTE
          i_nConn=i_TableProp[this.RIS_INTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIS_INTE_idx,2],.t.,this.RIS_INTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select RICODRIS  from "+i_cTable+" RIS_INTE ";
                +" where RICODPRA = "+cp_ToStrODBC(this.w_CODPRA)+" AND RIFLATTI='S'";
                +" order by CPROWORD";
                 ,"_Curs_RIS_INTE")
          else
            select RICODRIS from (i_cTable);
             where RICODPRA = this.w_CODPRA AND RIFLATTI="S";
             order by CPROWORD;
              into cursor _Curs_RIS_INTE
          endif
          if used('_Curs_RIS_INTE')
            select _Curs_RIS_INTE
            locate for 1=1
            do while not(eof())
            this.w_CODRIS = NVL(_Curs_RIS_INTE.RICODRIS, SPACE(5))
            * --- Read from DIPENDEN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIPENDEN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DPDTOBSO"+;
                " from "+i_cTable+" DIPENDEN where ";
                    +"DPCODICE = "+cp_ToStrODBC(this.w_CODRIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DPDTOBSO;
                from (i_cTable) where;
                    DPCODICE = this.w_CODRIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DATOBSO = NVL(cp_ToDate(_read_.DPDTOBSO),cp_NullValue(_read_.DPDTOBSO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Se nel temporaneo dei Partecipanti non � gi� presente la risorsa in esame e se questa non � obsoleta
            if this.w_ObjMPA.Search("NVL(t_PACODRIS, SPACE(5))= " + cp_ToStrODBC(this.w_CODRIS) + " AND NOT DELETED()") = -1 AND (EMPTY(this.w_DATOBSO) OR this.w_DATOBSO>this.oParentObject.w_DATFIN)
              * --- Nuova Riga del Temporaneo
              this.w_NUMRIG = this.w_NUMRIG+10
              this.w_ObjMPA.AddRow()     
              this.w_ObjMPA.w_CPROWORD = this.w_NUMRIG
              * --- Memorizzo il primo partecipante
              if this.w_NUMRIG=10
                this.w_CODRES = this.w_CODRIS
              endif
              * --- Read from DIPENDEN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DIPENDEN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DPCOGNOM,DPNOME,DPGRUPRE,DPCODUTE"+;
                  " from "+i_cTable+" DIPENDEN where ";
                      +"DPCODICE = "+cp_ToStrODBC(this.w_CODRIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DPCOGNOM,DPNOME,DPGRUPRE,DPCODUTE;
                  from (i_cTable) where;
                      DPCODICE = this.w_CODRIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_COGNOME = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
                this.w_NOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
                this.w_DPGRUPRE = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
                this.w_CODUTE = NVL(cp_ToDate(_read_.DPCODUTE),cp_NullValue(_read_.DPCODUTE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_ObjMPA.w_PACODRIS = this.w_CODRIS
              this.w_ObjMPA.w_CODUTE = this.w_CODUTE
              this.w_ObjMPA.w_COGNOME = NVL(this.w_COGNOME, SPACE(40))
              this.w_ObjMPA.w_NOME = NVL(this.w_NOME, SPACE(40))
              this.w_ObjMPA.w_DENOM = alltrim(NVL(this.w_COGNOME, ""))+" "+alltrim(NVL(this.w_NOME, ""))
              this.w_ObjMPA.w_DESCRI = alltrim(NVL(this.w_COGNOME, ""))+" "+alltrim(NVL(this.w_NOME, ""))
              this.w_ObjMPA.w_PAGRURIS = this.w_DPGRUPRE
              this.w_ObjMPA.SaveRow()     
            endif
              select _Curs_RIS_INTE
              continue
            enddo
            use
          endif
          if this.w_NUMRIG=0
            this.w_ObjMPA.AddRow()     
          endif
        else
          this.w_ObjMPA.AddRow()     
        endif
        this.w_ObjMPA.FirstRow()     
        this.w_ObjMPA.SetRow()     
        this.w_ObjMPA.Refresh()     
        this.w_ObjMPA.bUpdated = .T.
      else
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if (this.oParentObject.w_MODAPP$ "E-P" and ! this.w_ISALT) or ( this.ContaRisPrat=0 and this.w_ISALT)
        * --- Se modalit� Predefinito + dettaglio partecipanti devo prima aggiungere partecipante predefinito
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.Page_10()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.pParame<>"P"
        if NOT EMPTY(this.oParentObject.w_COVAPR)
          this.oParentObject.w_ATCODVAL=this.oParentObject.w_COVAPR
        endif
        * --- Valorizza il codice listino dell'attivit� con quello della pratica (solo se quest'ultimo non � vuoto)
        if NOT EMPTY(this.oParentObject.w_COLIPR)
          this.oParentObject.w_ATCODLIS=this.oParentObject.w_COLIPR
        endif
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola prezzi
    this.w_ATLISACQ = this.oParentObject.w_ATLISACQ
    this.w_CAUATT = this.oParentObject.w_ATCAUATT
    this.w_CODPRA = this.oParentObject.w_ATCODPRA
    if (NOT EMPTY(NVL(this.w_CAUATT, SPACE(20))) AND NOT EMPTY(NVL(this.w_CODPRA, SPACE(15))) AND this.w_ISALT) OR (Not this.w_ISALT and (Not empty(this.w_ATCODLIS) or Not empty(this.w_ATLISACQ)) or this.w_ISAHE)
      this.w_CALCPREZ = .F.
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_TOTALE>0 and this.pParame<>"1"
        * --- Esiste almeno una riga di prestazione valorizzata
        do case
          case this.pParame=="3" or this.pParame=="4"
            this.w_MESS = iif(this.w_ISALT,"Vuoi calcolare le tariffe delle prestazioni?", "Aggiorno i prezzi sulle righe in base al listino impostato?")
          case this.pParame=="E"
            this.w_MESS = "Aggiorno i costi sulle righe in base al listino impostato?"
          case this.pParame=="V"
            this.w_MESS = iif(this.w_ISALT,"Vuoi calcolare le tariffe delle prestazioni?", "Aggiorno costi e prezzi sulle righe in base al listino impostato?")
        endcase
        if ah_YesNo(Alltrim(this.w_MESS))
          this.w_CALCPREZ = .T.
        endif
      else
        this.w_CALCPREZ = .T.
        if ! this.pParame $ "1-2"
          this.w_RICALCOLA = .t.
        endif
      endif
      if this.w_CALCPREZ 
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if EMPTY(NVL( this.w_ATCODLIS,"")) and this.w_ISALT
          ah_ErrorMsg("Per determinare le tariffe � necessario selezionare un listino",64,"")
        endif
      endif
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione allegato VCS/ICS
    *     Inseriamo progressivamente le righe in entrambi poich� non � possibile copiare l'uno nell'altro a causa della differenza del formato note.
    this.w_PathVCS = ADDBS(TEMPADHOC())+"SCAD"+SYS(2015)+".VCS"
    * --- Da JUSTFNAME togliamo i 4 caratteri a destra, che sono .VCS che abbiamo impostato noi
    this.w_PathICS = ADDBS(JUSTPATH(this.w_PathVCS))+LEFT(JUSTFNAME(this.w_PathVCS), LEN(JUSTFNAME(this.w_PathVCS))-4)+".ICS"
    this.w_HandleVCS = 0
    this.w_HandleICS = 0
    * --- Gestione VCS
    this.w_HandleVCS = FCREATE(this.w_PathVCS,0)
    = FCLOSE(this.w_HandleVCS)
    this.w_HandleVCS = FOPEN(this.w_PathVCS,1)
    * --- Gestione ICS
    this.w_HandleICS = FCREATE(this.w_PathICS,0)
    = FCLOSE(this.w_HandleICS)
    this.w_HandleICS = FOPEN(this.w_PathICS,1)
    if this.w_HandleVCS < 0 OR this.w_HandleICS < 0
      * --- Errore, non procede
      i_retcode = 'stop'
      return
    endif
    * --- Gestione VCS
    = FPUTS(this.w_HandleVCS,"BEGIN:VCALENDAR")
    = FPUTS(this.w_HandleVCS,"PRODID:-//Zucchetti SPA//"+ALLTRIM(i_MsgTitle)+"//EN")
    = FPUTS(this.w_HandleVCS,"VERSION:1.0")
    * --- Gestione ICS
    = FPUTS(this.w_HandleICS,"BEGIN:VCALENDAR")
    = FPUTS(this.w_HandleICS,"PRODID:-//Zucchetti SPA//"+ALLTRIM(i_MsgTitle)+"//IT")
    = FPUTS(this.w_HandleICS,"VERSION:2.0")
    = FPUTS(this.w_HandleICS,"CONTENT-TYPE:text/calendar;charset=UTF-8")
    * --- Gestione singolo evento
    * --- Leggiamo la categoria
    this.w_TipoCategVCS = ""
    this.w_CategoriaVCS = ""
    do case
      case this.w_TipoCategVCS="G"
        * --- Generica
        this.w_CategoriaVCS = Ah_MsgFormat("Generica")
      case this.w_TipoCategVCS="U"
        * --- Udienza
        this.w_CategoriaVCS = Ah_MsgFormat("Udienza")
      case this.w_TipoCategVCS="S"
        * --- Appuntamento
        this.w_CategoriaVCS = Ah_MsgFormat("Appuntamento")
      case this.w_TipoCategVCS="D"
        * --- Cosa da fare
        this.w_CategoriaVCS = Ah_MsgFormat("Cosa da fare")
      case this.w_TipoCategVCS="M"
        * --- Nota
        this.w_CategoriaVCS = Ah_MsgFormat("Nota")
      case this.w_TipoCategVCS="T"
        * --- Sessione telefonica
        this.w_CategoriaVCS = Ah_MsgFormat("Sessione telefonica")
      case this.w_TipoCategVCS="A"
        * --- Assenza
        this.w_CategoriaVCS = Ah_MsgFormat("Assenza")
      case this.w_TipoCategVCS="Z"
        * --- Da inserimento prestazioni
        this.w_CategoriaVCS = Ah_MsgFormat("Da inserimento prestazioni")
    endcase
    * --- Allegando il VCS alla e-mail di notifica, a pag.1 sono avvalorate le seguenti variabili:
    *     w_DatIniAttivita, w_DatFinAttivita, w_NoteAttivita, w_OggeAttivita, w_CodPraAttivita, w_CAUATT, w_StatoAtti, w_TipoAttivita, w_DescTipologia
    *     w_DataIni, w_DataFin, w_OraIni, w_orafine, w_StatoAttivita
    this.w_DatIniVCS = this.w_DatIniAttivita
    this.w_DatFinVCS = this.w_DatFinAttivita
    * --- Dobbiamo sottrarre 1 ora (impostando l'ora di Greenwich)
    this.w_DatIniVCS = this.w_DatIniAttivita - 3600
    this.w_DatFinVCS = this.w_DatFinAttivita - 3600
    if OraLegale(this.w_DatIniAttivita)
      * --- Ora legale, dobbiamo detrarre un'ora, ossia 3600 secondi 
      this.w_DatIniVCS = this.w_DatIniVCS - 3600
      this.w_DatFinVCS = this.w_DatFinVCS - 3600
    endif
    * --- La data deve essere nel formato AAAA MM GG mentre l'ora sar� HHMMSS
    this.w_DTStartVCS = ALLTRIM(STR(YEAR(this.w_DatIniVCS)))+PADL(ALLTRIM(STR(MONTH(this.w_DatIniVCS))),2,"0")+PADL(ALLTRIM(STR(DAY(this.w_DatIniVCS))),2,"0")+"T"+PADL(ALLTRIM(STR(HOUR(this.w_DatIniVCS))),2,"0")+PADL(ALLTRIM(STR(MINUTE(this.w_DatIniVCS))),2,"0")+PADL(ALLTRIM(STR(SEC(this.w_DatIniVCS))),2,"0")+"Z"
    this.w_DTEndVCS = ALLTRIM(STR(YEAR(this.w_DatFinVCS)))+PADL(ALLTRIM(STR(MONTH(this.w_DatFinVCS))),2,"0")+PADL(ALLTRIM(STR(DAY(this.w_DatFinVCS))),2,"0")+"T"+PADL(ALLTRIM(STR(HOUR(this.w_DatFinVCS))),2,"0")+PADL(ALLTRIM(STR(MINUTE(this.w_DatFinVCS))),2,"0")+PADL(ALLTRIM(STR(SEC(this.w_DatFinVCS))),2,"0")+"Z"
    this.w_DTStampVCS = ALLTRIM(STR(YEAR(i_datsys)))+PADL(ALLTRIM(STR(MONTH(i_datsys))),2,"0")+PADL(ALLTRIM(STR(DAY(i_datsys))),2,"0")+"T"+PADL(ALLTRIM(STR(HOUR(DATETIME()))),2,"0")+PADL(ALLTRIM(STR(MINUTE(DATETIME()))),2,"0")+PADL(ALLTRIM(STR(SEC(DATETIME()))),2,"0")+"Z"
    * --- w_DTStartVCS
    * --- Stato attivit�:
    *     1 - libero, Free
    *     2 - Occupato, Busy
    *     3 - Per urgenze - Occupatp, Busy
    *     4 - Fuori sede, non disponibile
    * --- Gestione VCS
    = FPUTS(this.w_HandleVCS,"BEGIN:VEVENT")
    = FPUTS(this.w_HandleVCS,"DTSTART:"+this.w_DTStartVCS)
    = FPUTS(this.w_HandleVCS,"DTEND:"+this.w_DTEndVCS)
    = FPUTS(this.w_HandleVCS,"FREEBUSY;FBTYPE="+ICASE(this.w_StatoAttivita=1, "FREE",this.w_StatoAttivita=4,"BUSY-UNAVAILABLE","BUSY")+":"+this.w_DTStartVCS+"/"+this.w_DTEndVCS)
    * --- Gestione ICS
    = FPUTS(this.w_HandleICS,"BEGIN:VEVENT")
    = FPUTS(this.w_HandleICS,"UID:"+alltrim(this.w_ATCODUID))
    if this.pParame=="D"
      = FPUTS(this.w_HandleICS,"METHOD:CANCEL")
      = FPUTS(this.w_HandleICS,"STATUS:CANCELLED")
    endif
    = FPUTS(this.w_HandleICS,"DTSTART:"+this.w_DTStartVCS)
    = FPUTS(this.w_HandleICS,"DTEND:"+this.w_DTEndVCS)
    = FPUTS(this.w_HandleICS,"DTSTAMP:"+this.w_DTStampVCS)
    = FPUTS(this.w_HandleICS,"SUMMARY:"+ALLTRIM(this.w_OggeAttivita))
    * --- Composizione oggetto e note
    *     Prima per VCS e poi per ICS: avendo un formato diverso dobbiamo gestirlo in modo opportuno
    * --- Gestione VCS
    this.w_NoteAppVCS = GSAG_BEX(this,"COMPONINOTE",this.w_PartSerial,this.w_CodPraAttivita,this.w_DatIniVCS, this.w_DatFinVCS, this.w_OggeAttivita, this.w_CAUATT,this.w_nomecompleto, this.w_NoteAttivita,"V")
    this.w_Textapp = SUBSTR(this.w_NoteAppVCS,1, LEN(this.w_NoteAppVCS)-2 )
    = FPUTS(this.w_HandleVCS,"DESCRIPTION;ENCODING=QUOTED-PRINTABLE:"+this.w_Textapp)
    * --- Gestione ICS
    this.w_NoteAppVCS = GSAG_BEX(this,"COMPONINOTE",this.w_PartSerial,this.w_CodPraAttivita,this.w_DatIniVCS, this.w_DatFinVCS, this.w_OggeAttivita, this.w_CAUATT,this.w_nomecompleto, this.w_NoteAttivita,"I")
    this.w_Textapp = SUBSTR(this.w_NoteAppVCS,1, LEN(this.w_NoteAppVCS)-2 )
    this.w_NoteAppVCS = "DESCRIPTION:"+this.w_Textapp
    this.w_RESULT = ""
    do while LEN(this.w_NoteAppVCS)>75
      if empty(this.w_RESULT)
        this.w_RESULT = this.w_RESULT+ Substr(this.w_NoteAppVCS, 1, 75) + CHR(13)+CHR(10) + Space(1)
        this.w_NoteAppVCS = Substr(this.w_NoteAppVCS, Min(76, Len(this.w_NoteAppVCS)))
      else
        this.w_RESULT = this.w_RESULT+ Substr(this.w_NoteAppVCS, 1, 74) + CHR(13)+CHR(10) + Space(1)
        this.w_NoteAppVCS = Substr(this.w_NoteAppVCS, Min(75, Len(this.w_NoteAppVCS)))
      endif
    enddo
    this.w_RESULT = this.w_RESULT + this.w_NoteAppVCS
    this.w_RESULT = STRCONV(STRCONV(this.w_RESULT,1),9)
    this.w_NoteAppVCS = alltrim(this.w_RESULT)
    = FPUTS(this.w_HandleICS,""+this.w_NoteAppVCS)
    if !EMPTY(this.w_LuogoAttivita)
      this.w_LuogoAttivita = ALLTRIM(this.w_LuogoAttivita)
      * --- Gestione VCS
      = FPUTS(this.w_HandleVCS,"LOCATION;ENCODING=QUOTED-PRINTABLE:"+this.w_LuogoAttivita)
      * --- Gestione ICS
      = FPUTS(this.w_HandleICS,"LOCATION:"+this.w_LuogoAttivita)
    endif
    * --- Gestione VCS
    = FPUTS(this.w_HandleVCS,"CATEGORIES:"+this.w_CategoriaVCS)
    = FPUTS(this.w_HandleVCS,"SUMMARY;ENCODING=QUOTED-PRINTABLE:"+ALLTRIM(this.w_OggeAttivita))
    = FPUTS(this.w_HandleVCS,"PRIORITY:"+alltrim(str(this.w_ATNUMPRI)))
    = FPUTS(this.w_HandleVCS,"END:VEVENT")
    * --- Gestione ICS
    = FPUTS(this.w_HandleICS,"CATEGORIES:"+this.w_CategoriaVCS)
    = FPUTS(this.w_HandleICS,"PRIORITY:"+alltrim(str(this.w_ATNUMPRI)))
    = FPUTS(this.w_HandleICS,"FREEBUSY;FBTYPE="+ICASE(this.w_StatoAttivita=1, "FREE",this.w_StatoAttivita=4,"BUSY-UNAVAILABLE","BUSY")+":"+this.w_DTStartVCS+"/"+this.w_DTEndVCS)
    = FPUTS(this.w_HandleICS,"END:VEVENT")
    * --- Gestione VCS
    = FPUTS(this.w_HandleVCS,"END:VCALENDAR")
    = FCLOSE(this.w_HandleVCS)
    * --- Gestione ICS
    = FPUTS(this.w_HandleICS,"END:VCALENDAR")
    = FCLOSE(this.w_HandleICS)
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_MODAPP<>"P"
      if this.oParentObject.w_MODAPP="D"
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_ATCAUATT = this.oParentObject.w_ATCAUATT
      * --- Select from GSAGPBAP
      do vq_exec with 'GSAGPBAP',this,'_Curs_GSAGPBAP','',.f.,.t.
      if used('_Curs_GSAGPBAP')
        select _Curs_GSAGPBAP
        locate for 1=1
        do while not(eof())
        this.w_CODRIS = Nvl(_Curs_GSAGPBAP.DFCODRIS,Space(5))
        if this.w_ObjMPA.Search("NVL(t_PACODRIS, SPACE(5))= " + cp_ToStrODBC(this.w_CODRIS) + " AND NOT DELETED()") = -1 
          this.w_ObjMPA.AddRow()     
          this.w_NUMRIG = this.w_NUMRIG +10
          this.w_ObjMPA.w_CPROWORD = this.w_NUMRIG
          * --- Memorizzo il primo partecipante
          this.w_ObjMPA.w_PATIPRIS = Nvl(_Curs_GSAGPBAP.DFTIPRIS,"P")
          this.w_ObjMPA.w_PACODRIS = this.w_CODRIS
          this.w_ObjMPA.w_CODUTE = Nvl(_Curs_GSAGPBAP.DPCODUTE,0)
          this.w_ObjMPA.w_TipoRisorsa = Nvl(_Curs_GSAGPBAP.DFTIPRIS,"P")
          this.w_ObjMPA.w_COGNOME = NVL(_Curs_GSAGPBAP.DPCOGNOM, SPACE(40))
          this.w_ObjMPA.w_NOME = NVL(_Curs_GSAGPBAP.DP__NOME, SPACE(40))
          this.w_ObjMPA.w_DESCRI = NVL(_Curs_GSAGPBAP.DPDESCRI, SPACE(40))
          this.w_ObjMPA.w_DENOM = IIF(this.w_ObjMPA.w_PATIPRIS="P",alltrim(this.w_ObjMPA.w_COGNOME)+" "+alltrim(this.w_ObjMPA.w_NOME),alltrim(this.w_ObjMPA.w_DESCRI))
          this.w_ObjMPA.w_PAGRURIS = Nvl(_Curs_GSAGPBAP.DFGRURIS," ")
          this.w_ObjMPA.SaveRow()     
          this.w_ObjMPA.FirstRow()     
          this.w_ObjMPA.SetRow()     
          this.w_ObjMPA.Refresh()     
          this.w_ObjMPA.bUpdated = .T.
        endif
          select _Curs_GSAGPBAP
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento dell'operatore in qualit� di partecipante.
    if EMPTY(this.oParentObject.w_CODPART)
      * --- Caricamento da ricerca attivit� senza pratica
      * --- Cerchiamo codice, nome e cognome da inserire nel temporaneo.
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPCOGNOM,DPNOME,DPCODICE,DPTIPRIS,DPDESCRI,DPGRUPRE,DPDTOBSO"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODUTE = "+cp_ToStrODBC(i_CodUte);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPCOGNOM,DPNOME,DPCODICE,DPTIPRIS,DPDESCRI,DPGRUPRE,DPDTOBSO;
          from (i_cTable) where;
              DPCODUTE = i_CodUte;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COGNOME = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
        this.w_NOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
        this.w_CodDip = NVL(cp_ToDate(_read_.DPCODICE),cp_NullValue(_read_.DPCODICE))
        this.w_TIPRIS = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
        this.w_DESCRI = NVL(cp_ToDate(_read_.DPDESCRI),cp_NullValue(_read_.DPDESCRI))
        this.w_DPGRUPRE = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
        this.w_DATOBSO = NVL(cp_ToDate(_read_.DPDTOBSO),cp_NullValue(_read_.DPDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CODUTE = i_CODUTE
    else
      * --- Caricamento da calendario con filtro su partecipante
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPCOGNOM,DPNOME,DPCODICE,DPTIPRIS,DPGRUPRE,DPCODUTE,DPDTOBSO"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(this.oParentObject.w_CodPart);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPCOGNOM,DPNOME,DPCODICE,DPTIPRIS,DPGRUPRE,DPCODUTE,DPDTOBSO;
          from (i_cTable) where;
              DPCODICE = this.oParentObject.w_CodPart;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COGNOME = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
        this.w_NOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
        this.w_CodDip = NVL(cp_ToDate(_read_.DPCODICE),cp_NullValue(_read_.DPCODICE))
        this.w_TIPRIS = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
        this.w_DESCRI = NVL(cp_ToDate(_read_.DPCODICE),cp_NullValue(_read_.DPCODICE))
        this.w_DPGRUPRE = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
        this.w_CODUTE = NVL(cp_ToDate(_read_.DPCODUTE),cp_NullValue(_read_.DPCODUTE))
        this.w_DATOBSO = NVL(cp_ToDate(_read_.DPDTOBSO),cp_NullValue(_read_.DPDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if ! EMPTY(this.w_CodDip)
      * --- E' caricato in DIPENDEN, lo inseriamo nella movimentazione che � certamente vuota.
      SELECT (this.w_ObjMPA.cTrsName)
      this.w_ObjMPA.AddRow()     
      this.w_NUMRIG = this.w_NUMRIG +10
      this.w_ObjMPA.w_CPROWORD = this.w_NUMRIG
      * --- Memorizzo il primo partecipante
      this.w_CODRES = this.w_CODDIP
      this.w_ObjMPA.w_PATIPRIS = this.w_TIPRIS
      this.w_ObjMPA.w_PACODRIS = this.w_CODRES
      this.w_ObjMPA.o_PACODRIS = this.w_CODRES
      this.w_ObjMPA.w_CODUTE = this.w_CODUTE
      this.w_ObjMPA.w_COGNOME = NVL(this.w_COGNOME, SPACE(40))
      this.w_ObjMPA.w_NOME = NVL(this.w_NOME, SPACE(40))
      this.w_ObjMPA.w_DESCRI = NVL(this.w_DESCRI, SPACE(40))
      this.w_ObjMPA.w_DENOM = IIF(this.w_TIPRIS="P",alltrim(this.w_COGNOME)+" "+alltrim(this.w_NOME),alltrim(this.w_DESCRI))
      this.w_ObjMPA.w_PAGRURIS = this.w_DPGRUPRE
      this.w_ObjMPA.w_DATOBSO = this.w_DATOBSO
      this.w_ObjMPA.SaveRow()     
      this.w_ObjMPA.FirstRow()     
      this.w_ObjMPA.SetRow()     
      this.w_ObjMPA.Refresh()     
      this.w_ObjMPA.bUpdated = .T.
    endif
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_ObjMPA.NumRow()>0
      * --- Azzera Righe partecipanti
      this.w_ObjMPA.MarkPos()     
      this.w_ObjMPA.FirstRow()     
      do while ! this.w_ObjMPA.Eof_Trs()
        this.w_ObjMPA.DeleteRow()     
        this.w_ObjMPA.NextRow()     
      enddo
      this.w_ObjMPA.RePos()     
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParame,pParameSeriale,pNoAskMail)
    this.pParame=pParame
    this.pParameSeriale=pParameSeriale
    this.pNoAskMail=pNoAskMail
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,21)]
    this.cWorkTables[1]='CAU_ATTI'
    this.cWorkTables[2]='RIS_INTE'
    this.cWorkTables[3]='DIPENDEN'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='UNIMIS'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='OFF_ATTI'
    this.cWorkTables[8]='CAN_TIER'
    this.cWorkTables[9]='PAR_AGEN'
    this.cWorkTables[10]='OFFTIPAT'
    this.cWorkTables[11]='RIS_ESTR'
    this.cWorkTables[12]='COM_ATTI'
    this.cWorkTables[13]='CAUMATTI'
    this.cWorkTables[14]='CONTI'
    this.cWorkTables[15]='OFF_NOMI'
    this.cWorkTables[16]='DEF_PART'
    this.cWorkTables[17]='PRA_ENTI'
    this.cWorkTables[18]='PRA_UFFI'
    this.cWorkTables[19]='ANEVENTI'
    this.cWorkTables[20]='DES_DIVE'
    this.cWorkTables[21]='IMP_MAST'
    return(this.OpenAllTables(21))

  proc CloseCursors()
    if used('_Curs_QUERY_GSAG3BEX_d_VQR')
      use in _Curs_QUERY_GSAG3BEX_d_VQR
    endif
    if used('_Curs_QUERY_GSAG_BAP_d_VQR')
      use in _Curs_QUERY_GSAG_BAP_d_VQR
    endif
    if used('_Curs_QUERY_GSAG_BAP_d_VQR')
      use in _Curs_QUERY_GSAG_BAP_d_VQR
    endif
    if used('_Curs_CAU_ATTI')
      use in _Curs_CAU_ATTI
    endif
    if used('_Curs_RIS_INTE')
      use in _Curs_RIS_INTE
    endif
    if used('_Curs_RIS_INTE')
      use in _Curs_RIS_INTE
    endif
    if used('_Curs_GSAGPBAP')
      use in _Curs_GSAGPBAP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame,pParameSeriale,pNoAskMail"
endproc
