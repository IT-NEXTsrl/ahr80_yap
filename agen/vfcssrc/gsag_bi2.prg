* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bi2                                                        *
*              Eventi da importa componenti                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-20                                                      *
* Last revis.: 2008-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bi2",oParentObject,m.pPARAM)
return(i_retval)

define class tgsag_bi2 as StdBatch
  * --- Local variables
  pPARAM = space(3)
  w_GSAG_KIM = .NULL.
  w_CODICE = space(10)
  w_NUMRIG = 0
  w_ROWORD = 0
  w_DESCON = space(50)
  w_MODATTR = space(10)
  w_LEGAME = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GSAG_KIM = this.oParentObject
    ND = this.w_GSAG_KIM.w_ZoomDett.cCursor
    NM = this.w_GSAG_KIM.w_ZoomMast.cCursor
    this.w_CODICE = this.w_GSAG_KIM.w_ZoomDett.GetVar("IMCODICE")
    this.w_NUMRIG = this.w_GSAG_KIM.w_ZoomDett.GetVar("NUMRIG")
    this.w_ROWORD = this.w_GSAG_KIM.w_ZoomDett.GetVar("ROWORD")
    this.w_DESCON = this.w_GSAG_KIM.w_ZoomDett.GetVar("IMDESCON")
    this.w_MODATTR = this.w_GSAG_KIM.w_ZoomDett.GetVar("IMMODATR")
    this.w_LEGAME = this.w_GSAG_KIM.w_ZoomDett.GetVar("IMLEGAME")
    do case
      case this.pPARAM="DRC" 
        if USED(this.w_GSAG_KIM.w_ZoomDett.cCursor) 
          INSERT INTO GRUPPI (IMCODICE, NUMRIG, ROWORD, IMDESCON, IMMODATR, IMLEGAME) VALUES ( this.w_CODICE, this.w_NUMRIG, this.w_ROWORD, this.w_DESCON, this.w_MODATTR, this.w_LEGAME)
        endif
      case this.pPARAM="DRU" 
        DELETE FROM GRUPPI WHERE ALLTRIM(GRUPPI.IMCODICE)= ALLTRIM(this.w_CODICE) AND GRUPPI.NUMRIG = this.w_NUMRIG
      case this.pPARAM="DAQ"
        if USED(this.w_GSAG_KIM.w_ZoomDett.cCursor) AND USED("GRUPPI")
           
 UPDATE (ND) SET XCHK = 1 WHERE ALLTRIM(&ND..IMCODICE) + TRANSFORM(&ND..NUMRIG) IN (SELECT ALLTRIM(IMCODICE)+TRANSFORM(NUMRIG) FROM GRUPPI)
          this.oParentObject.w_ZoomDett.REFRESH()     
        endif
      case this.pPARAM="SEL"
        this.oParentObject.w_FLSELE = IIF ( this.oParentObject.w_SELEZI="S" ,1, 0 )
        UPDATE ( ND ) SET XCHK = this.oParentObject.w_FLSELE
        if this.oParentObject.w_FLSELE=1
          USE IN GRUPPI
          SELECT * FROM (ND) INTO CURSOR GRUPPI
          wrcursor("GRUPPI")
        else
          DELETE FROM GRUPPI
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
