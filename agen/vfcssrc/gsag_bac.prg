* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bac                                                        *
*              Generazione attivit�                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-31                                                      *
* Last revis.: 2015-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPADRE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bac",oParentObject,m.pPADRE)
return(i_retval)

define class tgsag_bac as StdBatch
  * --- Local variables
  pPADRE = .NULL.
  w_PADRE = .NULL.
  w_SELECTCURS = space(10)
  w_OLDAREA = space(10)
  w_CAFLRESP = space(1)
  w_RETVAL = space(254)
  w_bUnderTran = .f.
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_TOTPART = 0
  w_CA__NOTE = space(0)
  w_GRAPAT = space(1)
  w_ALFDO1 = space(2)
  w_ALFDOC = space(10)
  w_ALFRI1 = space(2)
  w_ALFRIF = space(2)
  w_APPOAZI = space(5)
  w_ATFLRICO = space(1)
  w_ATCALDIR = space(1)
  w_ATCOECAL = 0
  w_ATDURMIN = 0
  w_ATDURORE = 0
  w_ATFLAPON = space(1)
  w_ATFLVALO = space(1)
  w_ATIMPORT = 0
  w_ATTIPCLI = space(1)
  w_ATTPROSC = space(5)
  w_ATTPROLI = space(5)
  w_ATTSCLIS = space(5)
  w_ATTCOLIS = space(5)
  w_ATCENRIC = space(15)
  w_ATCOMRIC = space(15)
  w_ATATTRIC = space(15)
  w_ATATTCOS = space(15)
  w_ATCODUID = space(254)
  w_ATCODBUN = space(3)
  w_ATDATDOC = ctod("  /  /  ")
  w_ATLISACQ = space(5)
  w_ATEVANNO = space(4)
  w_CACHKNOM = space(1)
  w_CACHKOBB = space(1)
  w_CAFLPROM = space(1)
  w_CADTIN = ctot("")
  w_CAMINPRE = 0
  w_CAORASYS = space(1)
  w_CARAGGST = space(10)
  w_CAUDOC = space(5)
  w_CAUDOCA = space(5)
  w_CCDESPIA = space(40)
  w_CENOBSO = ctod("  /  /  ")
  w_CFUNC = space(10)
  w_CLADOC = space(2)
  w_CODCLI = space(15)
  w_CODLIS = space(5)
  w_codpra = space(15)
  w_codpra = space(15)
  w_COLIPR = space(5)
  w_COMODO = space(10)
  w_COVAPR = space(3)
  w_DataCambiata = .f.
  w_DATAPRO = ctod("  /  /  ")
  w_DATDO1 = ctod("  /  /  ")
  w_DATDOC = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_DATINI_CHANGED = space(1)
  w_DATOBSO = ctod("  /  /  ")
  w_DATOBSONOM = ctod("  /  /  ")
  w_datpre = ctod("  /  /  ")
  w_datpre = ctod("  /  /  ")
  w_DATPRV = ctod("  /  /  ")
  w_DATRI1 = ctod("  /  /  ")
  w_DATRIF = ctod("  /  /  ")
  w_DATRIN = ctod("  /  /  ")
  w_DESCAN = space(100)
  w_DESLIS = space(40)
  w_despra = space(100)
  w_despra = space(100)
  w_DESTIPOL = space(50)
  w_DESVAL = space(35)
  w_DOCESP = space(1)
  w_ENTE = space(10)
  w_FLANAL = space(1)
  w_FLDANA = space(1)
  w_FLGCOM = space(1)
  w_FLNSAP = space(1)
  w_FLRINV = space(1)
  w_FLTRIS = space(1)
  w_FLVEAC = space(1)
  w_GIOFIN = space(10)
  w_GIOINI = space(10)
  w_GIOPRM = space(10)
  w_GIOPRV = space(10)
  w_GIORIN = space(10)
  w_LEGIND = space(1)
  w_LetturaParAgen = space(5)
  w_MATETIPOL = space(1)
  w_MINFIN = space(2)
  w_MININI = space(2)
  w_MINPRO = space(2)
  w_MINPRV = space(2)
  w_MINRIN = space(2)
  w_MVSERIAL = space(10)
  w_NODESCRI = space(40)
  w_NOMDES = space(40)
  w_NUMDO1 = 0
  w_NUMDOC = 0
  w_NUMRI1 = 0
  w_NUMRIF = 0
  w_OBTEST = ctod("  /  /  ")
  w_OFCODNOM = space(15)
  w_OFDATDOC = ctod("  /  /  ")
  w_ORAFIN = space(2)
  w_ORAINI = space(2)
  w_ORAPRO = space(2)
  w_ORAPRV = space(2)
  w_ORARIN = space(2)
  w_PACHKCAR = space(1)
  w_PACHKFES = space(1)
  w_PAFLVISI = space(1)
  w_PARAME = space(2)
  w_PARASS = 0
  w_FLAMPA = space(1)
  w_PRENTTIP = space(1)
  w_RESCHK = 0
  w_RIGA = 0
  w_ROWNUM = 0
  w_SERIAL = space(10)
  w_SERIAL1 = space(10)
  w_SERIAL2 = space(10)
  w_SERIALE = space(10)
  w_SERTRA = space(10)
  w_STATOATT = space(1)
  w_TIPENT = space(1)
  w_TIPNOM = space(1)
  w_TIPOENTE = space(1)
  w_TipoRiso = space(1)
  w_USCITA = space(1)
  w_VALLIS = space(3)
  w_VisTracciab = space(1)
  w_VSRIF = space(2)
  w_ATSERIAL = space(20)
  w_ATCAUATT = space(20)
  w_ATOGGETT = space(254)
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_ATOPPOFF = space(10)
  w_ATSTATUS = space(1)
  w_ATCODNOM = space(15)
  w_ATSTATUS = space(1)
  w_ATNUMPRI = 0
  w_ATPERCOM = 0
  w_ATFLNOTI = space(1)
  w_ATGGPREA = 0
  w_ATFLPROM = space(1)
  w_ATNOTPIA = space(0)
  w_ATSTAATT = 0
  w_ATCODATT = space(5)
  w_ATCONTAT = space(5)
  w_ATPROMEM = ctot("")
  w_ATOPERAT = 0
  w_ATDATRIN = ctot("")
  w_ATTIPRIS = space(1)
  w_AT_ESITO = space(0)
  w_ATCODESI = space(5)
  w_ATCODPRA = space(15)
  w_ATCODPRA = space(15)
  w_ATCENCOS = space(15)
  w_ATDATPRO = ctot("")
  w_ATPERSON = space(60)
  w_ATTELEFO = space(18)
  w_ATCODSED = space(5)
  w_ATCELLUL = space(18)
  w_AT_EMAIL = space(0)
  w_AT_EMPEC = space(0)
  w_ATDATOUT = ctot("")
  w_ATPUBWEB = space(1)
  w_UTDC = ctod("  /  /  ")
  w_UTCV = 0
  w_UTDV = ctod("  /  /  ")
  w_ATCAITER = space(20)
  w_ATKEYOUT = 0
  w_ATDATOUT = ctot("")
  w_UTCC = 0
  w_ATRIFSER = space(20)
  w_ATNUMGIO = 0
  w_ATRIFDOC = space(10)
  w_AT___FAX = space(18)
  w_ATLOCALI = space(30)
  w_ATOREEFF = 0
  w_ATMINEFF = 0
  w_ATFLATRI = space(1)
  w_AT__ENTE = space(10)
  w_ATUFFICI = space(10)
  w_ATCODVAL = space(3)
  w_ATCODLIS = space(5)
  w_ATSTATUS = space(1)
  w_ATSTATUS = space(1)
  w_ATRIFMOV = space(10)
  w_ATSEREVE = space(10)
  w_ATCAUDOC = space(5)
  w_ATCAUACQ = space(5)
  w_ATNUMDOC = 0
  w_ATALFDOC = space(4)
  w_ATANNDOC = space(4)
  w_FLPDOC = space(1)
  w_COGNOME = space(40)
  w_DENOM = space(72)
  w_DESCRI = space(40)
  w_NOME = space(40)
  w_PAFLVISI = space(1)
  w_TIPOGRUP = space(1)
  w_TipoRisorsa = space(1)
  w_PASERIAL = space(20)
  w_PACODRIS = space(5)
  w_GSAG_MPA_CPROWORD = 0
  w_PATIPRIS = space(1)
  w_PAGRURIS = space(5)
  w_DPGRUPRE = space(5)
  w_OLDCOMP = space(10)
  w_CASERIAL = space(20)
  w_GSAG_MCP_CPROWORD = 0
  w_CACODIMP = space(10)
  w_CACODCOM = 0
  w_CACODART = space(20)
  w_CADTOBSO = ctod("  /  /  ")
  w_CAUATT = space(20)
  w_CENCOS = space(15)
  w_CHKTEMP = space(1)
  w_COD1ATT = space(20)
  w_CODCOM = space(15)
  w_COST_ORA = 0
  w_DCODCEN = space(15)
  w_DENOM_RESP = space(39)
  w_DESUNI = space(35)
  w_DUR_ORE = 0
  w_FLDANA = space(1)
  w_FLGCOM = space(1)
  w_FLRESP = space(1)
  w_FLSERG = space(1)
  w_NOTIFICA = space(1)
  w_OLDCEN = space(15)
  w_OLDCOM = space(15)
  w_SERPER = space(41)
  w_TIPART = space(2)
  w_TipoRisorsa = space(1)
  w_TIPRIS = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_VOCCOS = space(15)
  w_VOCRIC = space(15)
  w_DASERIAL = space(20)
  w_DACODRES = space(5)
  w_GSAG_MDA_CPROWORD = 0
  w_DACODATT = space(20)
  w_DACODICE = space(41)
  w_DADESATT = space(40)
  w_DAUNIMIS = space(3)
  w_DAQTAMOV = 0
  w_DAPREZZO = 0
  w_DADESAGG = space(0)
  w_DACODOPE = 0
  w_DADATMOD = ctod("  /  /  ")
  w_DAVALRIG = 0
  w_DAOREEFF = 0
  w_DAMINEFF = 0
  w_DACOSINT = 0
  w_DAFLDEFF = space(1)
  w_DAPREMIN = 0
  w_DAPREMAX = 0
  w_DAGAZUFF = space(6)
  w_DARIFRIG = 0
  w_DACODCOM = space(15)
  w_DAVOCRIC = space(15)
  w_DAVOCCOS = space(15)
  w_DAINICOM = ctod("  /  /  ")
  w_DAFINCOM = ctod("  /  /  ")
  w_DA_SEGNO = space(1)
  w_DACENCOS = space(15)
  w_DAATTIVI = space(15)
  w_DATIPRIG = space(1)
  w_DATIPRI2 = space(1)
  w_TATIPPRE = space(1)
  w_ONORARBI = space(1)
  w_FLPRPE = space(1)
  w_MANSION = space(5)
  w_TAIMPORT = 0
  w_PREZZO_MIN = 0
  w_PREZZO_MAX = 0
  w_PREZZO_UN = 0
  w_CNFLAPON = space(1)
  w_LISPRA = space(5)
  w_ENTE_CAL = space(10)
  w_CNFLVALO = space(1)
  w_CNFLVALO1 = space(1)
  w_CNIMPORT = 0
  w_CNCALDIR = space(1)
  w_CNCOECAL = 0
  w_PARASS = 0
  w_FLAMPA = space(1)
  w_COST_ORA = 0
  w_UMFLTEMP = space(1)
  w_UMDURORE = 0
  w_CNTARTEM = space(1)
  w_CNTARCON = 0
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_DARIFPRE = 0
  w_DARIGPRE = space(1)
  w_UM2 = space(3)
  w_OPER = space(1)
  w_MOLTIP = 0
  w_QTAUM1 = 0
  w_FLUSEP = space(1)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_TIPOPRAT = space(10)
  w_FLGIUD = space(1)
  w_CNMATOBB = space(1)
  w_CNASSCTP = space(1)
  w_CNCOMPLX = space(1)
  w_CNPROFMT = space(1)
  w_CNESIPOS = space(1)
  w_CNPERPLX = 0
  w_CNPERPOS = 0
  w_CNFLVMLQ = space(1)
  w_TAPARAM = space(1)
  w_VOCECR = space(1)
  w_CAUMAG = space(5)
  w_FLINTE = space(1)
  w_FLGLIS = space(1)
  w_FLGLIS = space(1)
  w_NCODLIS = space(5)
  w_NUMLIS = space(5)
  w_FLSCOR = space(1)
  w_PALISACQ = space(5)
  w_FLPRES = space(1)
  w_DATALIST = ctod("  /  /  ")
  w_KEYLISTB = space(10)
  w_FLACQ = space(1)
  w_FLACOMAQ = space(1)
  w_CATIPRIG = space(1)
  w_CATIPRI2 = space(1)
  w_DACENCOS = space(15)
  w_DPCENCOS = space(15)
  w_OK = .f.
  w_VALATT = space(3)
  w_CALPRZ = 0
  w_CALCONC = 0
  w_COSTO = 0
  w_DACOSUNI = 0
  w_DACOSINT = 0
  w_CALPRZ = 0
  w_DACODLIS = space(5)
  w_DAPROLIS = space(5)
  w_DASCOLIS = space(5)
  w_DAPROSCO = space(5)
  w_DASCONT1 = 0
  w_DASCONT2 = 0
  w_DASCONT2 = 0
  w_DASCONT4 = 0
  w_DPCENRIC = space(15)
  w_TIPSER = space(1)
  w_CODICE = space(20)
  w_ETIPRIG = space(1)
  w_RTIPRIG = space(1)
  w_ATIPRIG = space(1)
  w_DTIPRIG = space(1)
  w_ETIPRI2 = space(1)
  w_RTIPRI2 = space(1)
  w_ATIPRI2 = space(1)
  w_CAUACQ = space(5)
  w_CHKDATAPRE = space(1)
  w_GG_PRE = 0
  w_GG_SUC = 0
  w_PROPINS = 0
  w_PROPATT = 0
  w_NUMPROP = 0
  w_bSaveCurRec = .f.
  w_CODSES = space(15)
  w_FLRESTO = .f.
  w_LetturaParAlte = space(5)
  w_GENPRE = space(1)
  w_CAUNOT = space(5)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  VALUTE_idx=0
  OFFDATTI_idx=0
  KEY_ARTI_idx=0
  RIS_ESTR_idx=0
  PAR_PRAT_idx=0
  LISTINI_idx=0
  PAR_ALTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione attivit� da classe
    this.w_RETVAL = ""
    this.w_PADRE = IIF(VARTYPE(This.oParentObject)="O", This.oParentObject, this.pPADRE)
    * --- Evito l'aggiornamento della gestione chiamante
    *     (in questo caso l'oggetto che contiene le attivit� da creare
    *     le cui variabili non necessitano di aggiornamenti al termine della routine)
    this.bUpdateParentObject = .F.
    * --- Valorizzo variabili agenda
    * --- Valorizzazione variabili locali attivit�
    this.w_PADRE.SetLocalValues(This)     
    if !EMPTY(NVL( this.w_ATCAUATT, " "))
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "CACODICE"
      ArrWhere(1,2) = this.w_ATCAUATT
      this.w_ATOGGETT = IIF(EMPTY(this.w_ATOGGETT), this.w_PADRE.ReadTable( "CAUMATTI" , "CADESCRI" , @ArrWhere ), this.w_ATOGGETT)
      this.w_ATDURORE = this.w_PADRE.ReadTable( "CAUMATTI" , "CADURORE" , @ArrWhere )
      this.w_ATDURMIN = this.w_PADRE.ReadTable( "CAUMATTI" , "CADURMIN" , @ArrWhere )
      this.w_CADTIN = this.w_PADRE.ReadTable( "CAUMATTI" , "CADATINI" , @ArrWhere )
      this.w_ATGGPREA = IIF(EMPTY(this.w_ATGGPREA), this.w_PADRE.ReadTable( "CAUMATTI" , "CAGGPREA" , @ArrWhere ), this.w_ATGGPREA)
      this.w_ATSTATUS = IIF(EMPTY(this.w_ATSTATUS), this.w_PADRE.ReadTable( "CAUMATTI" , "CASTAATT" , @ArrWhere ), this.w_ATSTATUS)
      this.w_ATNUMPRI = IIF(EMPTY(this.w_ATNUMPRI), this.w_PADRE.ReadTable( "CAUMATTI" , "CAPRIORI" , @ArrWhere ), this.w_ATNUMPRI)
      this.w_ATCODATT = IIF(EMPTY(this.w_ATCODATT), this.w_PADRE.ReadTable( "CAUMATTI" , "CATIPATT" , @ArrWhere ), this.w_ATCODATT)
      this.w_FLNSAP = this.w_PADRE.ReadTable( "CAUMATTI" , "CAFLNSAP" , @ArrWhere )
      this.w_CARAGGST = this.w_PADRE.ReadTable( "CAUMATTI" , "CARAGGST" , @ArrWhere )
      this.w_ATSTAATT = IIF(EMPTY(this.w_ATSTAATT), this.w_PADRE.ReadTable( "CAUMATTI" , "CADISPON" , @ArrWhere ), this.w_ATSTAATT)
      this.w_FLTRIS = this.w_PADRE.ReadTable( "CAUMATTI" , "CAFLTRIS" , @ArrWhere )
      this.w_FLRINV = this.w_PADRE.ReadTable( "CAUMATTI" , "CAFLRINV" , @ArrWhere )
      this.w_ATFLNOTI = iif(Empty(Nvl(this.w_ATFLNOTI,"N")),this.w_PADRE.ReadTable( "CAUMATTI" , "CAFLNOTI" , @ArrWhere ), this.w_ATFLNOTI)
      this.w_CACHKNOM = this.w_PADRE.ReadTable( "CAUMATTI" , "CACHKNOM" , @ArrWhere )
      this.w_CAMINPRE = this.w_PADRE.ReadTable( "CAUMATTI" , "CAMINPRE" , @ArrWhere )
      this.w_CAORASYS = this.w_PADRE.ReadTable( "CAUMATTI" , "CAORASYS" , @ArrWhere )
      this.w_CAFLPROM = this.w_PADRE.ReadTable( "CAUMATTI" , "CAFLPREA" , @ArrWhere )
      this.w_FLANAL = this.w_PADRE.ReadTable( "CAUMATTI" , "CAFLANAL" , @ArrWhere )
      this.w_ATPUBWEB = IIF(EMPTY(this.w_ATPUBWEB), this.w_PADRE.ReadTable( "CAUMATTI" , "CAPUBWEB" , @ArrWhere ), this.w_ATPUBWEB)
      this.w_CAUACQ = this.w_PADRE.ReadTable( "CAUMATTI" , "CACAUACQ" , @ArrWhere )
      this.w_CAUDOC = this.w_PADRE.ReadTable( "CAUMATTI" , "CACAUDOC" , @ArrWhere )
      this.w_CA__NOTE = this.w_PADRE.ReadTable( "CAUMATTI" , "CA__NOTE" , @ArrWhere )
      this.w_ATNOTPIA = IIF(Not Empty(Nvl(this.w_ATNOTPIA," ")),this.w_ATNOTPIA,this.w_CA__NOTE)
      this.w_ATFLATRI = IIF(EMPTY( this.w_ATFLATRI ), this.w_PADRE.ReadTable( "CAUMATTI" , "CAFLATRI" , @ArrWhere ), this.w_ATFLATRI )
      this.w_FLPDOC = this.w_PADRE.ReadTable( "CAUMATTI" , "CAFLPDOC" , @ArrWhere )
      this.w_ATALFDOC = this.w_PADRE.ReadTable( "CAUMATTI" , "CASERDOC" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_ATCAUDOC = IIF(NOT EMPTY(this.w_ATCAUDOC),this.w_ATCAUDOC,this.w_CAUDOC)
    this.w_CAUDOC = this.w_ATCAUDOC
    this.w_ATCAUACQ = IIF(NOT EMPTY(this.w_ATCAUACQ),this.w_ATCAUACQ,this.w_CAUACQ)
    this.w_CAUACQ = this.w_ATCAUACQ
    if !EMPTY(NVL( this.w_CAUDOC, " "))
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "TDTIPDOC"
      ArrWhere(1,2) = this.w_CAUDOC
      if ! Isahe()
        this.w_VOCECR = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDVOCECR" , @ArrWhere )
        this.w_FLDANA = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLANAL" , @ArrWhere )
        this.w_FLGCOM = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLCOMM" , @ArrWhere )
      else
        this.w_FLGLIS = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLGLIS" , @ArrWhere )
        this.w_FLVEAC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLVEAC" , @ArrWhere )
      endif
      this.w_CAUMAG = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCAUMAG" , @ArrWhere )
      this.w_FLINTE = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLINTE" , @ArrWhere )
      Release ArrWhere
    endif
    if !EMPTY(NVL( this.w_CAUACQ, " "))
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "TDTIPDOC"
      ArrWhere(1,2) = this.w_CAUACQ
      if ! Isahe()
        this.w_VOCECR = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDVOCECR" , @ArrWhere )
        this.w_FLDANA = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLANAL" , @ArrWhere )
        this.w_FLGCOM = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLCOMM" , @ArrWhere )
      else
        this.w_FLGLIS = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLGLIS" , @ArrWhere )
        this.w_FLVEAC = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLVEAC" , @ArrWhere )
      endif
      this.w_CAUMAG = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDCAUMAG" , @ArrWhere )
      this.w_FLINTE = this.w_PADRE.ReadTable( "TIP_DOCU" , "TDFLINTE" , @ArrWhere )
      Release ArrWhere
    endif
    if Isahe()
      if !EMPTY(NVL( this.w_CAUMAG, " "))
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "CMCODICE"
        ArrWhere(1,2) = this.w_CAUMAG
        this.w_FLDANA = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLELAN" , @ArrWhere )
        this.w_FLGCOM = this.w_PADRE.ReadTable( "CAM_AGAZ" , "CMFLCOMM" , @ArrWhere )
        Release ArrWhere
      endif
    endif
    * --- Se la causale non lo prevede non seve essere possibile introdurre:
    *     - Nominativo e relativi dati
    *     - Componenti dell'attivit�
    this.w_ATCODNOM = IIF(EMPTY(this.w_CACHKNOM), SPACE(15), this.w_ATCODNOM)
    if !EMPTY(NVL( this.w_ATCODNOM, " "))
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "NOCODICE"
      ArrWhere(1,2) = this.w_ATCODNOM
      this.w_TIPNOM = this.w_PADRE.ReadTable( "OFF_NOMI" , "NOTIPNOM" , @ArrWhere )
      * --- Eliminata lettura di ATPERSON da OFF_NOMI, che adesso � gestito da ATCONTAT
      this.w_AT_EMAIL = IIF(EMPTY(NVL(this.w_AT_EMAIL," ")), this.w_PADRE.ReadTable( "OFF_NOMI" , "NO_EMAIL" , @ArrWhere ), this.w_AT_EMAIL)
      this.w_AT_EMPEC = IIF(EMPTY(NVL(this.w_AT_EMPEC," ")), this.w_PADRE.ReadTable( "OFF_NOMI" , "NO_EMPEC" , @ArrWhere ), this.w_AT_EMPEC)
      this.w_ATTELEFO = IIF(EMPTY(NVL(this.w_ATTELEFO," ")), this.w_PADRE.ReadTable( "OFF_NOMI" , "NOTELEFO" , @ArrWhere ), this.w_ATTELEFO)
      this.w_CODCLI = this.w_PADRE.ReadTable( "OFF_NOMI" , "NOCODCLI" , @ArrWhere )
      this.w_ATTIPCLI = this.w_PADRE.ReadTable( "OFF_NOMI" , "NOTIPCLI" , @ArrWhere )
      if !IsAlt()
        this.w_ATLOCALI = IIF(EMPTY(NVL(this.w_ATLOCALI," ")), this.w_PADRE.ReadTable( "OFF_NOMI" , "NOLOCALI" , @ArrWhere ), this.w_ATLOCALI)
      endif
      this.w_ATCELLUL = IIF(EMPTY(NVL(this.w_ATCELLUL," ")), this.w_PADRE.ReadTable( "OFF_NOMI" , "NONUMCEL" , @ArrWhere ), this.w_ATCELLUL)
      this.w_NCODLIS = this.w_PADRE.ReadTable( "OFF_NOMI" , "NONUMLIS" , @ArrWhere )
      Release ArrWhere
      if Not Empty(this.w_CODCLI) AND this.w_ATTIPCLI="C"
        * --- Leggo il listino del cliente
        DIMENSION ArrWhere(2,2)
        ArrWhere(1,1) = "ANTIPCON"
        ArrWhere(1,2) = "C"
        ArrWhere(2,1) = "ANCODICE"
        ArrWhere(2,2) = this.w_CODCLI
        this.w_NUMLIS = this.w_PADRE.ReadTable( "CONTI" , "ANNUMLIS" , @ArrWhere )
        if Isahe()
          this.w_FLSCOR = this.w_PADRE.ReadTable( "CONTI" , "ANSCORPO" , @ArrWhere )
        else
          this.w_FLSCOR = this.w_PADRE.ReadTable( "CONTI" , "ANFLSCOR" , @ArrWhere )
        endif
        Release ArrWhere
      endif
    else
      this.w_TIPNOM = ""
    endif
    this.w_DATINI = IIF(Empty(this.w_DATINI) , IIF( EMPTY(this.w_ATDATINI), i_datsys , TTOD(this.w_ATDATINI)) , this.w_DATINI)
    this.w_ORAINI = IIF(EMPTY(this.w_ORAINI), PADL(ALLTRIM(STR(HOUR( IIF(EMPTY(this.w_ATDATINI), IIF(this.w_CAORASYS="S",DATETIME(),this.w_CADTIN) ,this.w_ATDATINI)))),2,"0") , this.w_ORAINI)
    this.w_MININI = IIF(EMPTY(this.w_ORAINI), PADL(ALLTRIM(STR(MINUTE( IIF(EMPTY(this.w_ATDATINI), IIF(this.w_CAORASYS="S",DATETIME(),this.w_CADTIN) ,this.w_ATDATINI)))),2,"0") , this.w_MININI)
    this.w_DATFIN = IIF(Empty(this.w_DATFIN) , IIF( EMPTY(this.w_ATDATFIN), this.w_DATINI+INT( (VAL(this.w_ORAINI)+this.w_ATDURORE+INT((VAL(this.w_MININI)+this.w_ATDURMIN)/60)) / 24) , TTOD(this.w_ATDATFIN)) , this.w_DATFIN)
    this.w_ORAFIN = IIF(EMPTY(this.w_ORAFIN), RIGHT("00"+ALLTRIM(IIF(EMPTY(this.w_ATDATFIN), STR(MOD( (VAL(this.w_ORAINI)+this.w_ATDURORE+INT((VAL(this.w_MININI)+this.w_ATDURMIN)/60)), 24)) ,STR(HOUR(this.w_ATDATFIN),2))) ,2) ,this.w_ORAFIN)
    this.w_MINFIN = IIF(EMPTY(this.w_MINFIN), RIGHT("00"+ALLTRIM(IIF(EMPTY(this.w_ATDATFIN), STR(MOD( (VAL(this.w_MININI)+this.w_ATDURMIN), 60),2) ,STR(MINUTE(this.w_ATDATFIN),2))) ,2) ,this.w_MINFIN)
    if Isalt()
      this.w_ATTIPRIS = IIF(EMPTY(this.w_ATTIPRIS), IIF(this.w_ATSTATUS $ "FP",this.w_ATTIPRIS,.NULL.) , this.w_ATTIPRIS)
    else
      this.w_ATTIPRIS = .NULL.
    endif
    if EMPTY(this.w_ATCODLIS) or empty(this.w_ATLISACQ) or empty(this.w_ATTCOLIS)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "PACODAZI"
      ArrWhere(1,2) = i_CODAZI
      this.w_CODLIS = this.w_PADRE.ReadTable( "PAR_AGEN" , "PACODLIS" , @ArrWhere )
      this.w_PAFLVISI = this.w_PADRE.ReadTable( "PAR_AGEN" , "PAFLVISI" , @ArrWhere )
      this.w_PALISACQ = this.w_PADRE.ReadTable( "PAR_AGEN" , "PALISACQ" , @ArrWhere )
      this.w_FLPRES = this.w_PADRE.ReadTable( "PAR_AGEN" , "PAFLPRES" , @ArrWhere )
      Release ArrWhere
      if !Isahe()
        if EMPTY(this.w_ATCODLIS)
          this.w_ATCODLIS = IIF(EMPTY(this.w_NUMLIS), IIF(Not Empty(this.w_NCODLIS),this.w_NCODLIS,this.w_CODLIS), this.w_NUMLIS)
        endif
      else
        if EMPTY(this.w_ATTCOLIS) AND this.w_FLGLIS="S"
          this.w_ATTCOLIS = IIF(EMPTY(this.w_NUMLIS), IIF(Not Empty(this.w_NCODLIS),this.w_NCODLIS,this.w_CODLIS), this.w_NUMLIS)
        endif
      endif
      if EMPTY(this.w_ATLISACQ)
        this.w_ATLISACQ = this.w_PALISACQ
      endif
    endif
    if !EMPTY(this.w_ATCODPRA)
      if IsAlt()
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "CNCODCAN"
        ArrWhere(1,2) = this.w_ATCODPRA
        this.w_COVAPR = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCODVAL" , @ArrWhere )
        this.w_ATLOCALI = IIF(EMPTY(NVL(this.w_ATLOCALI," ")), this.w_PADRE.ReadTable( "CAN_TIER" , "CNLOCALI" , @ArrWhere ), this.w_ATLOCALI)
        this.w_AT__ENTE = IIF(EMPTY(NVL(this.w_AT__ENTE," ")), this.w_PADRE.ReadTable( "CAN_TIER" , "CN__ENTE" , @ArrWhere ), this.w_AT__ENTE)
        this.w_ATUFFICI = IIF(EMPTY(NVL(this.w_ATUFFICI," ")), this.w_PADRE.ReadTable( "CAN_TIER" , "CNUFFICI" , @ArrWhere ), this.w_ATUFFICI)
        this.w_CNFLAPON = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLAPON" , @ArrWhere )
        this.w_LISPRA = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCODLIS" , @ArrWhere )
        this.w_CNFLVALO = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLVALO" , @ArrWhere )
        this.w_CNFLVALO1 = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLDIND" , @ArrWhere )
        this.w_CNIMPORT = this.w_PADRE.ReadTable( "CAN_TIER" , "CNIMPORT" , @ArrWhere )
        this.w_CNCALDIR = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCALDIR" , @ArrWhere )
        this.w_CNCOECAL = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCOECAL" , @ArrWhere )
        this.w_PARASS = this.w_PADRE.ReadTable( "CAN_TIER" , "CNPARASS" , @ArrWhere )
        this.w_FLAMPA = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLAMPA" , @ArrWhere )
        this.w_CNTARTEM = this.w_PADRE.ReadTable( "CAN_TIER" , "CNTARTEM" , @ArrWhere )
        this.w_CNTARCON = this.w_PADRE.ReadTable( "CAN_TIER" , "CNTARCON" , @ArrWhere )
        this.w_CONTRACN = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCONTRA" , @ArrWhere )
        this.w_TIPOPRAT = this.w_PADRE.ReadTable( "CAN_TIER" , "CNTIPPRA" , @ArrWhere )
        this.w_CNMATOBB = this.w_PADRE.ReadTable( "CAN_TIER" , "CNMATOBB" , @ArrWhere )
        this.w_CNASSCTP = this.w_PADRE.ReadTable( "CAN_TIER" , "CNASSCTP" , @ArrWhere )
        this.w_CNCOMPLX = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCOMPLX" , @ArrWhere )
        this.w_CNPROFMT = this.w_PADRE.ReadTable( "CAN_TIER" , "CNPROFMT" , @ArrWhere )
        this.w_CNESIPOS = this.w_PADRE.ReadTable( "CAN_TIER" , "CNESIPOS" , @ArrWhere )
        this.w_CNPERPLX = this.w_PADRE.ReadTable( "CAN_TIER" , "CNPERPLX" , @ArrWhere )
        this.w_CNPERPOS = this.w_PADRE.ReadTable( "CAN_TIER" , "CNPERPOS" , @ArrWhere )
        this.w_CNFLVMLQ = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLVMLQ" , @ArrWhere )
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "EPCODICE"
        ArrWhere(1,2) = this.w_PADRE.w_AT__ENTE
        this.w_ENTE_CAL = IIF(EMPTY(NVL(this.w_AT__ENTE," ")), this.w_PADRE.ReadTable( "PRA_ENTI" , "EPCODICE" , @ArrWhere ), this.w_AT__ENTE)
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "CPCODCON"
        ArrWhere(1,2) = this.w_CONTRACN
        this.w_TIPCONCO = this.w_PADRE.ReadTable( "PRA_CONT" , "CPTIPCON" , @ArrWhere )
        this.w_LISCOLCO = this.w_PADRE.ReadTable( "PRA_CONT" , "CPLISCOL" , @ArrWhere )
        this.w_STATUSCO = this.w_PADRE.ReadTable( "PRA_CONT" , "CPSTATUS" , @ArrWhere )
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "TPCODICE"
        ArrWhere(1,2) = this.w_TIPOPRAT
        this.w_FLGIUD = this.w_PADRE.ReadTable( "PRA_TIPI" , "TPFLGIUD" , @ArrWhere )
      endif
      Release ArrWhere
    endif
    this.w_ATCODBUN = iif(Not Empty(this.w_ATCODBUN),this.w_ATCODBUN,g_CODBUN)
    this.w_ATDATDOC = iif(Not Empty(this.w_ATDATDOC),this.w_ATDATDOC,IIF(this.w_FLNSAP<>"S",this.w_ATDATFIN,cp_Chartodate("  -  -    ")))
    this.w_ATCODVAL = IIF(EMPTY(this.w_ATCODVAL), IIF(EMPTY(this.w_COVAPR), g_PERVAL, this.w_COVAPR) , this.w_ATCODVAL)
    if Isahe()
      this.w_KEYLISTB = SYS(2015)
      DECLARE ARRCALC (4,1)
      this.w_DATALIST = IIF(Not Empty(this.w_ATDATDOC),this.w_ATDATDOC,this.w_DATFIN)
      this.w_ATTCOLIS = Space(5)
      this.w_ATTSCLIS = Space(5)
      this.w_ATTPROLI = Space(5)
      this.w_ATTPROSC = Space(5)
      w_RESULT=CALCLIST("CREA",@ARRCALC,this.w_FLVEAC,this.w_FLGLIS,this.w_ATCODVAL,this.w_ATTIPCLI,this.w_CODCLI,this.w_DATALIST," "," ",this.w_KEYLISTB,-20,this.w_FLSCOR," ",this.w_CAUDOC)
      this.w_ATTCOLIS = ARRCALC(1)
      this.w_ATTSCLIS = ARRCALC(2)
      this.w_ATTPROLI = ARRCALC(3)
      this.w_ATTPROSC = ARRCALC(4)
    endif
    this.w_ATDATOUT = IIF(EMPTY(this.w_ATDATOUT), DATETIME(), this.w_ATDATOUT)
    this.w_ATFLATRI = IIF(EMPTY(this.w_ATFLATRI), "N", this.w_ATFLATRI)
    * --- Se non � impostato, utilizza quello della causale
    this.w_ATFLPROM = IIF(EMPTY(this.w_ATFLPROM), this.w_CAFLPROM, this.w_ATFLPROM)
    this.w_ATPROMEM = IIF(this.w_ATFLPROM="S", IIF(EMPTY(this.w_ATPROMEM), this.w_ATDATINI-IIF(this.w_CAMINPRE>0,this.w_CAMINPRE*60,0), this.w_ATPROMEM) , cp_CharToDateTime("  -  -       :  :  ") )
    * --- Valorizzazione variabili oggetto attivit� da variabili locali
    this.w_ATFLRICO = IIF(Empty(Nvl(this.w_ATFLRICO,"N")), "N", this.w_ATFLRICO)
    this.w_ATSEREVE = IIF(Empty(Nvl(this.w_ATSEREVE," ")), Space(10), this.w_ATSEREVE)
    this.w_ATEVANNO = IIF(Empty(Nvl(this.w_ATEVANNO," ")), Space(4), this.w_ATEVANNO)
    this.w_PADRE.SetObjectValues(This)     
    this.w_PADRE.SyncDate()     
    if EMPTY(this.w_ATDATDOC) AND this.w_FLNSAP<>"S"
      this.w_PADRE.w_ATDATDOC = this.w_PADRE.w_ATDATFIN
    endif
    this.w_PADRE.w_OFF_PART.GoTop()     
    this.w_PADRE.w_OFF_PART.ReadCurrentRecord()     
    this.w_TOTPART = this.w_PADRE.w_OFF_PART.RecCount()
    if this.w_TOTPART = 0
      DIMENSION ArrWhere(1,2)
      * --- Inserimento partecipante collegato a risorsa corrente
      ArrWhere(1,1) = "DPCODUTE"
      ArrWhere(1,2) = i_CODUTE
      this.w_DESCRI = this.w_PADRE.ReadTable( "DIPENDEN" , "DPDESCRI" , @ArrWhere )
      this.w_PACODRIS = this.w_PADRE.ReadTable( "DIPENDEN" , "DPCODICE" , @ArrWhere )
      this.w_TIPRIS = "P"
      this.w_DPGRUPRE = this.w_PADRE.ReadTable( "DIPENDEN" , "DPGRUPRE" , @ArrWhere )
      this.w_MANSION = this.w_PADRE.ReadTable( "DIPENDEN" , "DPMANSION" , @ArrWhere )
      this.w_COST_ORA = this.w_PADRE.ReadTable( "DIPENDEN" , "DPCOSORA" , @ArrWhere )
      this.w_DATOBSO = this.w_PADRE.ReadTable( "DIPENDEN" , "DPDTOBSO" , @ArrWhere )
      this.w_PADRE.AddPart(0, 0, this.w_TIPRIS, this.w_PACODRIS, IIF(this.w_TIPRIS="P", this.w_DPGRUPRE, ""))     
      this.w_TOTPART = 1
      if this.w_DATOBSO<=this.w_DATFIN AND not empty(this.w_DATOBSO)
        this.w_RETVAL = ah_Msgformat("Il partecipante %1 � obsoleto",ALLTRIM(this.w_DESCRI))
      endif
    else
      this.w_SELECTCURS = SYS(2015)
      this.w_PADRE.w_OFF_PART.Exec_MCSelect(this.w_SELECTCURS , "MIN(PATIPRIS) AS TIPRIS", "" , "" , "" , "")     
      if TIPRIS="G" AND this.w_FLNSAP<>"S"
        this.w_PADRE.w_ATSTATUS = "T"
      endif
      USE IN SELECT(this.w_SELECTCURS)
      DIMENSION ArrWhere(1,2)
      this.w_PADRE.w_OFF_PART.GoTop()     
      do while !this.w_PADRE.w_OFF_PART.Eof()
        this.w_PADRE.w_OFF_PART.ReadCurrentRecord()     
        ArrWhere(1,1) = "DPCODICE"
        ArrWhere(1,2) = this.w_PADRE.w_OFF_PART.PACODRIS
        this.w_DESCRI = this.w_PADRE.ReadTable( "DIPENDEN" , "DPDESCRI" , @ArrWhere )
        this.w_DATOBSO = this.w_PADRE.ReadTable( "DIPENDEN" , "DPDTOBSO" , @ArrWhere )
        if this.w_DATOBSO<=this.w_DATFIN AND not empty(this.w_DATOBSO)
          this.w_RETVAL = ah_Msgformat("Il partecipante %1 � obsoleto",ALLTRIM(this.w_DESCRI))
          exit
        endif
        this.w_PADRE.w_OFF_PART.Next()     
      enddo
    endif
    Release ArrWhere
    if Not empty(this.w_CAUACQ)
      this.w_FLACQ = Docgesana(this.w_CAUACQ,"A")
      this.w_FLACOMAQ = Docgesana(this.w_CAUACQ,"C")
    endif
    this.w_PADRE.w_OFFDATTI.GoTop()     
    if this.w_PADRE.w_OFFDATTI.RecCount() = 0 and Empty(this.w_RETVAL)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "CACODICE"
      ArrWhere(1,2) = this.w_ATCAUATT
      this.w_SELECTCURS = this.w_PADRE.SelectTable("CAU_ATTI", "*", @ArrWhere )
      if !EMPTY(this.w_SELECTCURS)
        this.w_OLDAREA = SELECT()
        SELECT ( this.w_SELECTCURS )
        GO TOP
        SCAN
        this.w_CAFLRESP = NVL(CAFLRESP, "N")
        this.w_CATIPRIG = NVL(CATIPRIG, "D")
        this.w_CATIPRI2 = NVL(CATIPRI2, "D")
        if Isahe()
          this.w_DACODICE = CAKEYART
        else
          this.w_DACODATT = CACODSER
        endif
        if !EMPTY(NVL(this.w_DACODATT, " ")) OR !EMPTY(NVL(this.w_DACODICE, " "))
          this.w_DADESATT = CADESSER
          this.w_DAFLDEFF = NVL(CAFLDEFF, SPACE(1))
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "CACODICE"
          ArrWhere(1,2) = IIF(Isahe(),this.w_DACODICE,this.w_DACODATT)
          this.w_COD1ATT = this.w_PADRE.ReadTable( "KEY_ARTI" , "CACODART" , @ArrWhere )
          if EMPTY(NVL(this.w_DADESATT, " "))
            this.w_DADESATT = this.w_PADRE.ReadTable( "KEY_ARTI" , "CADESART" , @ArrWhere )
          endif
          this.w_DADESAGG = this.w_PADRE.ReadTable( "KEY_ARTI" , "CADESSUP" , @ArrWhere )
          this.w_UNMIS3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CAUNIMIS" , @ArrWhere )
          this.w_OPERA3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CAOPERAT" , @ArrWhere )
          this.w_MOLTI3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CAMOLTIP" , @ArrWhere )
          Release ArrWhere
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "ARCODART"
          ArrWhere(1,2) = this.w_COD1ATT
          this.w_DAUNIMIS = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS1" , @ArrWhere )
          this.w_TIPART = this.w_PADRE.ReadTable( "ART_ICOL" , "ARTIPART" , @ArrWhere )
          this.w_DAVOCRIC = SearchVoc(this,"R",this.w_COD1ATT, this.w_ATTIPCLI, this.w_CODCLI, this.w_CAUDOC, this.w_ATCODNOM)
          this.w_DAVOCCOS = SearchVoc(this,"C",this.w_COD1ATT, this.w_ATTIPCLI, this.w_CODCLI, this.w_CAUDOC, this.w_ATCODNOM)
          this.w_TATIPPRE = this.w_PADRE.ReadTable( "ART_ICOL" , "ARPRESTA" , @ArrWhere )
          this.w_ONORARBI = this.w_PADRE.ReadTable( "ART_ICOL" , "ARSTASUP" , @ArrWhere )
          this.w_FLPRPE = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLPRPE" , @ArrWhere )
          this.w_NOTIFICA = this.w_PADRE.ReadTable( "ART_ICOL" , "ARSTACOD" , @ArrWhere )
          this.w_DESAGG = this.w_PADRE.ReadTable( "ART_ICOL" , "ARDESSUP" , @ArrWhere )
          this.w_UM2 = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS2" , @ArrWhere )
          this.w_OPER = this.w_PADRE.ReadTable( "ART_ICOL" , "AROPERAT" , @ArrWhere )
          this.w_MOLTIP = this.w_PADRE.ReadTable( "ART_ICOL" , "ARMOLTIP" , @ArrWhere )
          this.w_FLUSEP = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLUSEP" , @ArrWhere )
          this.w_TAPARAM = this.w_PADRE.ReadTable( "ART_ICOL" , "ARPARAME" , @ArrWhere )
          Release ArrWhere
          this.w_DAQTAMOV = IIF(this.w_TIPART="DE", 0, 1)
          this.w_PADRE.w_OFF_PART.GoTop()     
          * --- Controllo di essere entrato almeno una volta nel ciclo
          this.w_OK = .T.
          this.w_PADRE.w_OFF_PART.GoTop()     
          do while (this.w_CAFLRESP="S" OR this.w_TOTPART=1 OR this.w_OK) And !this.w_PADRE.w_OFF_PART.Eof()
            this.w_OK = .F.
            this.w_PADRE.w_OFF_PART.ReadCurrentRecord()     
            this.w_PADRE.w_OFF_PART.SetLocalValues(this)     
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "DPCODICE"
            ArrWhere(1,2) = this.w_PADRE.w_OFF_PART.PACODRIS
            this.w_DPCENCOS = this.w_PADRE.ReadTable( "DIPENDEN" , "DPCODCEN" , @ArrWhere )
            this.w_DACENCOS = IIF(Not Empty(Nvl(this.w_DPCENCOS," ")),this.w_DPCENCOS,NVL(this.w_PADRE.w_ATCENCOS,SPACE(15)))
            this.w_COST_ORA = this.w_PADRE.ReadTable( "DIPENDEN" , "DPCOSORA" , @ArrWhere )
            Release ArrWhere
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "UMCODICE"
            ArrWhere(1,2) = this.w_DAUNIMIS
            this.w_UMFLTEMP = this.w_PADRE.ReadTable( "UNIMIS" , "UMFLTEMP" , @ArrWhere )
            this.w_UMDURORE = this.w_PADRE.ReadTable( "UNIMIS" , "UMDURORE" , @ArrWhere )
            this.w_FLFRAZ1 = this.w_PADRE.ReadTable( "UNIMIS" , "UMFLFRAZ" , @ArrWhere )
            this.w_MODUM2 = this.w_PADRE.ReadTable( "UNIMIS" , "UMMODUM2" , @ArrWhere )
            Release ArrWhere
            this.w_FLCCRAUTO = Docgesana(this.w_CAUDOC,"R")
            this.w_FLCCRAUTC = Docgesana(this.w_CAUACQ,"R")
            this.w_CEN_CauDoc = IIF(IsAhe() AND (this.w_FLCCRAUTO="S" or this.w_FLCCRAUTC="S"),SearchVoc(this,"T",this.w_DACODICE,"","","",""),SPACE(15))
            if this.w_PADRE.w_OFF_PART.PATIPRIS="P"
              this.w_PADRE.AddDetail(0, 0, this.w_PADRE.w_OFF_PART.PACODRIS, IIF(ISAHE(),this.w_DACODICE,this.w_DACODATT), this.w_DADESATT, this.w_DAUNIMIS, this.w_DAQTAMOV, 0 , "D", i_codute, i_datsys, this.w_DAFLDEFF, "D")     
              this.w_PADRE.w_OFFDATTI.DADESAGG = this.w_DADESAGG
              this.w_PADRE.w_OFFDATTI.DAVOCCOS = this.w_DAVOCCOS
              this.w_PADRE.w_OFFDATTI.DAVOCRIC = this.w_DAVOCRIC
              this.w_PADRE.w_OFFDATTI.DACENCOS = IIF(NOT EMPTY(NVL(this.w_PADRE.w_OFFDATTI.DACENCOS, SPACE(15))), NVL(this.w_PADRE.w_OFFDATTI.DACENCOS, SPACE(15)), this.w_DACENCOS)
              this.w_DAOREEFF = IIF(Nvl(this.w_UMFLTEMP,"N")="S", INT(Nvl(this.w_UMDURORE,0) * this.w_PADRE.w_OFFDATTI.DAQTAMOV), IIF(this.w_TIPART="DE", 0, 1))
              this.w_DAMINEFF = IIF(Nvl(this.w_UMFLTEMP,"N")="S", INT((Nvl(this.w_UMDURORE,0) * this.w_PADRE.w_OFFDATTI.DAQTAMOV- INT(Nvl(this.w_UMDURORE,0) * this.w_PADRE.w_OFFDATTI.DAQTAMOV)) * 60), 0)
              do case
                case IsAlt()
                  * --- Dichiarazione array prezzi
                  DECLARE ARRPRE (4,1)
                  * --- Azzero l'Array che verr� riempito dalla Funzione
                  ARRPRE(1)=0
                  * --- Se � un diritto con valore pratica indeterminabile viene considerato il check
                  *     "Calcolo dei diritti per valore indeterminabile" presente sulla pratica.
                  if this.w_TATIPPRE $ "RC" AND this.w_CNFLVALO $ "IPS"
                    this.w_CNFLVALO = this.w_CNFLVALO1
                    this.w_CNCALDIR = "M"
                    this.w_CNCOECAL = 50
                  endif
                  * --- Valore Pratica
                  do case
                    case this.w_CNFLVALO="D"
                      this.w_TAIMPORT = this.w_CNIMPORT
                    case this.w_CNFLVALO="I"
                      this.w_TAIMPORT = -1
                    case this.w_CNFLVALO$"PS"
                      this.w_TAIMPORT = -2
                  endcase
                  * --- Legge il numero di decimali per valori unitari
                  * --- Read from VALUTE
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.VALUTE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "VADECUNI"+;
                      " from "+i_cTable+" VALUTE where ";
                          +"VACODVAL = "+cp_ToStrODBC(this.w_VALATT);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      VADECUNI;
                      from (i_cTable) where;
                          VACODVAL = this.w_VALATT;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    w_NUMDECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  * --- Se sulla pratica � definita Tariffa concordata e se siamo in presenza di una tariffa a tempo
                  if this.w_CNTARTEM="C" AND this.w_TATIPPRE="P"
                    * --- Dichiarazione array tariffe a tempo concordate
                    DECLARE ARRTARCON (3,1)
                    * --- Azzero l'array che verr� riempito dalla funzione Caltarcon()
                    ARRTARCON(1)=0 
 ARRTARCON(2)= " " 
 ARRTARCON(3)=0
                    this.w_CALCONC = CALTARCON(this.w_ATCODPRA, this.w_PADRE.w_OFFDATTI.DACODRES, this.w_PADRE.w_OFFDATTI.DACODATT, @ARRTARCON)
                    this.w_CNTARCON = ARRTARCON(1)
                    this.w_DAQTAMOV = IIF(Not Empty(ARRTARCON(3)), ARRTARCON(3), this.w_DAQTAMOV)
                  endif
                  this.w_QTAUM1 = CALQTA(this.w_DAQTAMOV,this.w_DAUNIMIS,this.w_UM2,this.w_OPER, this.w_MOLTIP, this.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, " ", this.w_UNMIS3, this.w_OPERA3, this.w_MOLTI3)
                  do case
                    case this.w_TATIPPRE="P" AND (this.w_CNTARTEM="C" OR (this.w_CNTARTEM="D" AND this.w_TIPCONCO<>"L" AND this.w_STATUSCO="I"))
                      * --- Se Prestazione a tempo e (Tipo di tariffazione a tempo uguale a Tariffa Concordata o 
                      *     (Tipo di tariffazione a tempo uguale a Tariffa Da Contratto e Tipo condizioni del contratto diverso da monte ore/listino collegato)):
                      *     applicazione tariffa concordata
                      this.w_PREZZO_UN = this.w_CNTARCON
                      this.w_PREZZO_MIN = this.w_CNTARCON
                      this.w_PREZZO_MAX = this.w_CNTARCON
                      this.w_DAGAZUFF = ""
                    case this.w_TATIPPRE="P" AND this.w_CNTARTEM="D" AND this.w_TIPCONCO="L" AND this.w_STATUSCO="I"
                      * --- Se Prestazione a tempo e Tipo di tariffazione a tempo uguale a Tariffa Da Contratto e Tipo condizioni del contratto uguale a Da monte ore/listino collegato
                      *     applicazione listino collegato (da contratto)
                      this.w_GRAPAT = "N"
                      this.w_CALPRZ = CALPRAT(this.w_COD1ATT, this.w_TATIPPRE, this.w_ONORARBI, this.w_FLPRPE, this.w_NOTIFICA, this.w_CNFLAPON, this.w_LISCOLCO, this.w_GRAPAT, this.w_ENTE_CAL, this.w_PACODRIS, this.w_MANSION, IIF(Vartype(this.w_ATDATINI)="T",this.w_ATDATINI,ttod(this.w_ATDATINI)), this.w_CNFLVALO, this.w_TAIMPORT, this.w_CNCALDIR, this.w_CNCOECAL, this.w_PARASS, this.w_FLAMPA, @ARRPRE, w_NUMDECUNI, .F., "*NNNNN"+this.w_CNFLVMLQ+this.w_TAPARAM, 0, 0 )
                      this.w_PREZZO_UN = ARRPRE(1)
                      this.w_PREZZO_MIN = cp_Round(CALMMPZ(ARRPRE(2), this.w_DAQTAMOV, this.w_QTAUM1, "N", 0, g_perpul),g_perpul)
                      this.w_PREZZO_MAX = cp_Round(CALMMPZ(ARRPRE(3), this.w_DAQTAMOV, this.w_QTAUM1, "N", 0, g_perpul),g_perpul)
                      this.w_DAGAZUFF = ARRPRE(4)
                      * --- cp_Round(CALMMPZ(ARRPRE(2), w_TAQTAMOV, w_TAQTAUM1, 'N', 0, g_perpul),g_perpul)
                    otherwise
                      * --- Caso normale
                      *     applicazione listino (della pratica)
                      * --- Read from LISTINI
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.LISTINI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "LSGRAPAT"+;
                          " from "+i_cTable+" LISTINI where ";
                              +"LSCODLIS = "+cp_ToStrODBC(this.w_ATCODLIS);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          LSGRAPAT;
                          from (i_cTable) where;
                              LSCODLIS = this.w_ATCODLIS;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_GRAPAT = NVL(cp_ToDate(_read_.LSGRAPAT),cp_NullValue(_read_.LSGRAPAT))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      this.w_CALPRZ = CALPRAT(this.w_COD1ATT, this.w_TATIPPRE, this.w_ONORARBI, this.w_FLPRPE, this.w_NOTIFICA, this.w_CNFLAPON, this.w_ATCODLIS, this.w_GRAPAT, this.w_ENTE_CAL, this.w_PACODRIS, this.w_MANSION, IIF(Vartype(this.w_ATDATINI)="T",this.w_ATDATINI,ttod(this.w_ATDATINI)), this.w_CNFLVALO, this.w_TAIMPORT, this.w_CNCALDIR, this.w_CNCOECAL, this.w_PARASS, this.w_FLAMPA, @ARRPRE, w_NUMDECUNI, .F., this.w_FLGIUD+this.w_CNMATOBB+this.w_CNASSCTP+this.w_CNCOMPLX+this.w_CNPROFMT+this.w_CNESIPOS+this.w_CNFLVMLQ+this.w_TAPARAM, this.w_CNPERPLX,this.w_CNPERPOS)
                      this.w_PREZZO_UN = ARRPRE(1)
                      this.w_PREZZO_MIN = cp_Round(CALMMPZ(ARRPRE(2), this.w_DAQTAMOV, this.w_QTAUM1, "N", 0, g_perpul),g_perpul)
                      this.w_PREZZO_MAX = cp_Round(CALMMPZ(ARRPRE(3), this.w_DAQTAMOV, this.w_QTAUM1, "N", 0, g_perpul),g_perpul)
                      this.w_DAGAZUFF = ARRPRE(4)
                  endcase
                case IsAhr()
                  * --- Prezzo
                  this.w_PREZZO_UN = gsar_bpz(this,this.w_COD1ATT,this.w_CODCLI,this.w_ATTIPCLI,this.w_ATCODLIS,this.w_DAQTAMOV,this.w_ATCODVAL,IIF(Vartype(this.w_ATDATINI)="T",ttod(this.w_ATDATINI),this.w_ATDATINI),this.w_DAUNIMIS)
                  this.w_DAGAZUFF = ""
                  * --- Costo
                  this.w_COSTO = 0
                  if this.w_PADRE.w_OFF_PART.PATIPRIS$ "P-R"
                    if Not empty(this.w_ATLISACQ) and this.w_COST_ORA=0
                      DECLARE L_ArrRet (16,1)
                      this.w_COSTO = gsar_bpz(this,this.w_DACODATT,this.w_CODCLI,"C",this.w_ATLISACQ,this.w_DAQTAMOV,this.w_ATCODVAL,IIF(Vartype(this.w_ATDATINI)="T",ttod(this.w_ATDATINI),this.w_ATDATINI),this.w_DAUNIMIS, REPL("X",16))
                      RELEASE L_ArrRet
                    endif
                    this.w_DAOREEFF = Min(IIF(this.w_UMFLTEMP="S", INT(this.w_UMDURORE * this.w_DAQTAMOV), this.w_DAOREEFF),999)
                    this.w_DAMINEFF = IIF(this.w_UMFLTEMP="S", INT((this.w_UMDURORE * this.w_DAQTAMOV - INT(this.w_UMDURORE * this.w_DAQTAMOV)) * 60), this.w_DAMINEFF)
                    this.w_DACOSUNI = IIF(this.w_COSTO>0 OR (this.w_COST_ORA=0 and this.w_UMFLTEMP<>"S" and this.w_TIPRIS$ "R-P"),this.w_COSTO,this.w_COST_ORA)
                    this.w_PADRE.w_OFFDATTI.DACOSUNI = this.w_DACOSUNI
                    this.w_PADRE.w_OFFDATTI.DACOSINT = IIF(this.w_COSTO>0 and this.w_COST_ORA=0 ,this.w_DACOSUNI*this.w_DAQTAMOV,(this.w_DAOREEFF+(this.w_DAMINEFF/60))*this.w_DACOSUNI)
                  endif
                case Isahe()
                  this.w_COSTO = 0
                  DECLARE ARRET (10,1) 
 ARRET[1]= "" 
 ARRET[2]= "" 
 ARRET[3]= "" 
 ARRET[4]= "" 
 ARRET[5]=0 
 ARRET[6]=0 
 ARRET[7]=0 
 ARRET[8]=0 
 ARRET[9]=0 
 ARRET[10]=0
                  this.w_CALPRZ = gsar_bpz(this.w_PADRE,this.w_ATTCOLIS,this.w_ATTPROLI,this.w_ATTSCLIS,this.w_ATTPROSC,this.w_DACODICE,this.w_ATTIPCLI,this.w_CODCLI,this.w_DAQTAMOV,this.w_ATCODVAL,IIF(Vartype(this.w_ATDATINI)="T",ttod(this.w_ATDATINI),this.w_ATDATINI),this.w_DAUNIMIS,this.w_CAUDOC,this.w_FLGLIS,this.w_KEYLISTB,@ARRET)
                  if this.w_CALPRZ>0
                    this.w_DACODLIS = ARRET[1]
                    this.w_PADRE.w_OFFDATTI.DACODLIS = this.w_DACODLIS
                    this.w_DAPROLIS = ARRET[2]
                    this.w_PADRE.w_OFFDATTI.DAPROLIS = this.w_DAPROLIS
                    this.w_DASCOLIS = ARRET[3]
                    this.w_PADRE.w_OFFDATTI.DASCOLIS = this.w_DASCOLIS
                    this.w_DAPROSCO = ARRET[4]
                    this.w_PADRE.w_OFFDATTI.DAPROSCO = this.w_DAPROSCO
                    this.w_PREZZO_UN = ARRET[5]
                    this.w_DASCONT1 = ARRET[6]
                    this.w_PADRE.w_OFFDATTI.DASCONT1 = this.w_DASCONT1
                    this.w_DASCONT2 = ARRET[7]
                    this.w_PADRE.w_OFFDATTI.DASCONT2 = this.w_DASCONT2
                    this.w_DASCONT3 = ARRET[8]
                    this.w_PADRE.w_OFFDATTI.DASCONT3 = this.w_DASCONT3
                    this.w_DASCONT4 = ARRET[8]
                    this.w_PADRE.w_OFFDATTI.DASCONT4 = this.w_DASCONT4
                  endif
                  if this.w_PADRE.w_OFF_PART.PATIPRIS$ "P-R"
                    if Not empty(this.w_ATLISACQ) and this.w_COST_ORA=0
                      DECLARE ARRET (10,1) 
 ARRET[1]= "" 
 ARRET[2]= "" 
 ARRET[3]= "" 
 ARRET[4]= "" 
 ARRET[5]=0 
 ARRET[6]=0 
 ARRET[7]=0 
 ARRET[8]=0 
 ARRET[9]=0 
 ARRET[10]=0
                      * --- Calcolo costo interno in base al listino impostato
                      * --- Aggiungo listino acquisti
                      gsag_bdl(this,"I",this.w_KEYLISTB,this,this.w_ATLISACQ)
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                      this.w_CALPRZ = gsar_bpz(this.w_PADRE,this.w_ATLISACQ,Space(5),Space(5),Space(5),this.w_DACODICE," ",Space(15),this.w_DAQTAMOV,this.w_ATCODVAL,IIF(Vartype(this.w_ATDATINI)="T",ttod(this.w_ATDATINI),this.w_ATDATINI),this.w_DAUNIMIS,this.w_CAUACQ,this.w_FLGLIS,this.w_KEYLISTB,@ARRET)
                      this.w_COSTO = ARRET[5]
                      * --- Elimino listino acquisti
                      gsag_bdl(this,"E",this.w_KEYLISTB,this,this.w_ATLISACQ)
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                    this.w_DACOSUNI = IIF(this.w_COSTO>0 OR (this.w_COST_ORA=0 and this.w_UMFLTEMP<>"S" and this.w_TIPRIS$ "R-P"), this.w_COSTO,this.w_COST_ORA)
                    this.w_PADRE.w_OFFDATTI.DACOSUNI = this.w_DACOSUNI
                    this.w_PADRE.w_OFFDATTI.DACOSINT = IIF(this.w_COSTO>0 and this.w_COST_ORA=0 ,this.w_DACOSUNI*this.w_DAQTAMOV,(this.w_DAOREEFF+(this.w_DAMINEFF/60))*this.w_DACOSUNI)
                    this.w_DAOREEFF = Min(IIF(this.w_UMFLTEMP="S", INT(this.w_UMDURORE * this.w_DAQTAMOV), this.w_DAOREEFF),999)
                    this.w_DAMINEFF = IIF(this.w_UMFLTEMP="S", INT((this.w_UMDURORE * this.w_DAQTAMOV - INT(this.w_UMDURORE * this.w_DAQTAMOV)) * 60), this.w_DAMINEFF)
                  endif
                otherwise
              endcase
              this.w_PADRE.w_OFFDATTI.DACODCOM = this.w_PADRE.w_ATCODPRA
              if Not empty(this.w_PADRE.w_ATATTCOS)
                this.w_PADRE.w_OFFDATTI.DAATTIVI = this.w_PADRE.w_ATATTCOS
              endif
              this.w_DPCENRIC = IIF(Not Empty(Nvl(this.w_DPCENCOS," ")),this.w_DPCENCOS,NVL(this.w_PADRE.w_ATCENRIC,SPACE(15)))
              this.w_PADRE.w_OFFDATTI.DACENRIC = IIF(NOT EMPTY(NVL(this.w_PADRE.w_OFFDATTI.DACENRIC, SPACE(15))), NVL(this.w_PADRE.w_OFFDATTI.DACENRIC, SPACE(15)), this.w_DPCENRIC)
              if Not empty(this.w_PADRE.w_ATCOMRIC)
                this.w_PADRE.w_OFFDATTI.DACOMRIC = this.w_PADRE.w_ATCOMRIC
              endif
              if Not empty(this.w_PADRE.w_ATATTRIC)
                this.w_PADRE.w_OFFDATTI.DAATTRIC = this.w_PADRE.w_ATATTRIC
              endif
              this.w_PADRE.w_OFFDATTI.DA_SEGNO = "D"
              this.w_PADRE.w_OFFDATTI.DAPREZZO = this.w_PREZZO_UN
              this.w_PADRE.w_OFFDATTI.DAGAZUFF = this.w_DAGAZUFF
              this.w_PADRE.w_OFFDATTI.DAOREEFF = this.w_DAOREEFF
              this.w_PADRE.w_OFFDATTI.DAMINEFF = this.w_DAMINEFF
              this.w_PADRE.w_OFFDATTI.DATIPRIG = this.w_CATIPRIG
              this.w_PADRE.w_OFFDATTI.DATIPRI2 = this.w_CATIPRI2
              if IsAlt()
                this.w_PADRE.w_OFFDATTI.DACOSUNI = this.w_COST_ORA
                this.w_PADRE.w_OFFDATTI.DACOSINT = (this.w_DAOREEFF+(this.w_DAMINEFF/60))*this.w_COST_ORA
                this.w_PADRE.w_OFFDATTI.DAPREMIN = this.w_PREZZO_MIN
                this.w_PADRE.w_OFFDATTI.DAPREMAX = this.w_PREZZO_MAX
                if this.w_CNTARTEM="C" AND this.w_TATIPPRE="P"
                  this.w_PADRE.w_OFFDATTI.DAQTAMOV = this.w_DAQTAMOV
                endif
              endif
              * --- Abbinamento automatico elementi contratto
              this.w_PADRE.w_OFFDATTI.SaveCurrentRecord()     
            endif
            this.w_PADRE.w_OFF_PART.Next()     
          enddo
        endif
        SELECT ( this.w_SELECTCURS )
        ENDSCAN
        SELECT ( this.w_OLDAREA )
        USE IN SELECT(this.w_SELECTCURS)
        this.w_OLDAREA = ""
      endif
    endif
    this.w_PADRE.w_OFFDATTI.GoTop()     
    if this.w_PADRE.w_OFFDATTI.RecCount() <> 0 and this.w_ATSTATUS="P" and Empty(this.w_RETVAL)
      do while !this.w_PADRE.w_OFFDATTI.Eof()
        this.w_PADRE.w_OFFDATTI.ReadCurrentRecord()     
        if Isahe() 
          this.w_CODICE = this.w_PADRE.w_OFFDATTI.DACODICE
        else
          this.w_CODICE = this.w_PADRE.w_OFFDATTI.DACODATT
        endif
        this.w_DADESATT = this.w_PADRE.w_OFFDATTI.DADESATT
        if empty(this.w_PADRE.w_OFFDATTI.DATIPRIG)
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "DPCODICE"
          ArrWhere(1,2) = this.w_PADRE.w_OFFDATTI.DACODRES
          this.w_DTIPRIG = NVL(this.w_PADRE.ReadTable( "DIPENDEN" , "DPTIPRIG" , @ArrWhere ),"N")
          Release ArrWhere
          this.w_ETIPRIG = iif(Not empty(this.w_CAUACQ) and Not empty(this.w_CAUDOC),"E",iif(Not empty(this.w_CAUACQ),"A","D"))
          this.w_RTIPRIG = iif(Not empty(this.w_CAUACQ) and Not empty(this.w_CAUDOC),"E",iif(Not empty(this.w_CAUACQ),"A","D"))
          this.w_ATIPRIG = IIF(this.w_TIPART="FO" and this.w_PADRE.w_OFFDATTI.DAPREZZO=0, "N", this.w_DTIPRIG)
          this.w_PADRE.w_OFFDATTI.DATIPRIG = ICASE(Isahe(),this.w_ETIPRIG,Isahr(),this.w_RTIPRIG,this.w_ATIPRIG)
        endif
        if empty(this.w_PADRE.w_OFFDATTI.DATIPRI2)
          this.w_ETIPRI2 = iif(Not empty(this.w_CAUACQ) and Not empty(this.w_CAUDOC),"E",iif(Not empty(this.w_CAUACQ),"A","D"))
          this.w_RTIPRI2 = iif(Not empty(this.w_CAUACQ) and Not empty(this.w_CAUDOC),"E",iif(Not empty(this.w_CAUACQ),"A","D"))
          this.w_ATIPRI2 = IIF(this.w_TIPART="FO" and this.w_PADRE.w_OFFDATTI.DAPREZZO=0, "N", "D")
          this.w_PADRE.w_OFFDATTI.DATIPRI2 = ICASE(Isahe(),this.w_ETIPRI2,Isahr(),this.w_RTIPRI2,this.w_ATIPRI2)
        endif
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CA__TIPO"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_CODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CA__TIPO;
            from (i_cTable) where;
                CACODICE = this.w_CODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPSER = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !ISAHE
          if (Not Empty(NVL(this.w_CODICE, "")) AND (Nvl(this.w_PADRE.w_OFFDATTI.DATIPRIG," ") $ "D-E") OR ((Nvl(this.w_PADRE.w_OFFDATTI.DATIPRI2," ") $ "D-E") and Isalt()))
            if Empty(this.w_ATCAUDOC)
              this.w_RETVAL = this.w_RETVAL+ah_Msgformat("Attenzione: per generare il documento di vendita � necessario indicare la causale specifica nei dati di testata per la riga con i seguenti riferimenti:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+ chr(10)+chr(13)
            endif
          endif
          if (Not Empty(NVL(this.w_CODICE, "")) AND (Nvl(this.w_PADRE.w_OFFDATTI.DATIPRIG," ") $ "A-E") OR ((Nvl(this.w_PADRE.w_OFFDATTI.DATIPRI2," ") $ "A-E") and Isalt()))
            if this.w_FLANAL="S"
              this.w_RETVAL = this.w_RETVAL+ah_Msgformat("Attenzione: il tipo attivit� gestisce il movimento di analitica, impossibile generare il documento di acquisto per la riga con i seguenti riferimenti:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+ chr(10)+chr(13)
            endif
          endif
        endif
        if this.w_TIPSER<>"D"
          do case
            case (this.w_PADRE.w_OFFDATTI.DATIPRIG $ "D-A-E" OR (this.w_PADRE.w_OFFDATTI.DATIPRI2 $ "D-A-E" and Isalt()) )
              if !Isahe()
                * --- Controllo dati analitici vendite
                if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DAVOCRIC,Space(15))) and (this.w_FLDANA="S" or this.w_FLGCOM="S")
                  this.w_RETVAL = this.w_RETVAL + ah_Msgformat("La riga con i seguenti riferimenti � priva di voce di ricavo:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+chr(10)+chr(13)
                endif
                if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DACENRIC,Space(15))) and this.w_FLDANA="S"
                  this.w_RETVAL = this.w_RETVAL+ah_Msgformat("La riga con i seguenti riferimenti � priva di centro di ricavo:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+ chr(10)+chr(13)
                endif
                if Not Empty(this.w_ATCAUDOC) and ((this.w_PADRE.w_OFFDATTI.DATIPRIG $ "D-E" OR ((this.w_PADRE.w_OFFDATTI.DATIPRI2 $ "D-E" and Isalt())))) and this.w_FLGCOM ="S"
                  if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DACOMRIC,Space(15))) 
                    this.w_RETVAL = this.w_RETVAL+ah_Msgformat("La riga con i seguenti riferimenti � priva di commessa per i ricavi:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+chr(10)+chr(13)
                  endif
                  if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DAATTRIC,Space(15))) and !isAhe()
                    this.w_RETVAL = this.w_RETVAL+ah_Msgformat("La riga con i seguenti riferimenti � priva di attivit� per i ricavi:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT)) + chr(10)+chr(13)
                  endif
                endif
                * --- Controllo dati analitici acquisti
                if g_PERCCR="S" and (this.w_PADRE.w_OFFDATTI.DATIPRIG $ "A-E" OR (this.w_PADRE.w_OFFDATTI.DATIPRI2 $ "A-E" and Isalt()) ) or (this.w_FLANAL="S" and Nvl(this.w_PADRE.w_OFFDATTI.DACOSINT,0)>0)
                  if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DAVOCCOS,Space(15))) AND ((this.w_FLANAL="S" or (this.w_VOCECR="C" AND (this.w_FLACQ="S" or this.w_FLACOMAQ="S")))) 
                    this.w_RETVAL = this.w_RETVAL+ah_Msgformat("La riga con i seguenti riferimenti � priva di voce di costo:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+ chr(10)+chr(13)
                  endif
                  if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DACENCOS,Space(15))) AND ((this.w_FLACQ="S" or this.w_FLANAL="S")) 
                    this.w_RETVAL = this.w_RETVAL+ah_Msgformat("La riga con i seguenti riferimenti � priva di centro di costo:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+ chr(10)+chr(13)
                  endif
                endif
                if NOT EMPTY(this.w_ATCAUACQ) and ((this.w_PADRE.w_OFFDATTI.DATIPRIG $ "A-E" OR ((this.w_PADRE.w_OFFDATTI.DATIPRI2 $ "A-E" and Isalt())))) and this.w_FLGCOM ="S"
                  if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DACODCOM,Space(15))) 
                    this.w_RETVAL = this.w_RETVAL+ah_Msgformat("La riga con i seguenti riferimenti � priva di commessa per i costi:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT)) + chr(10)+chr(13)
                  endif
                  if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DAATTIVI,Space(15)))
                    this.w_RETVAL = this.w_RETVAL+ah_Msgformat("La riga con i seguenti riferimenti � priva di attivit� per i costi:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+ chr(10)+chr(13)
                  endif
                endif
              else
                if this.w_PADRE.w_OFFDATTI.DATIPRIG $ "A-E" or this.w_PADRE.w_OFFDATTI.DATIPRI2 $ "A-E" 
                  do case
                    case Empty(Nvl(this.w_PADRE.w_OFFDATTI.DAVOCCOS,Space(15))) and this.w_FLACQ $ "S-I"
                      this.w_RETVAL = ah_Msgformat("La riga con i seguenti riferimenti � priva di voce di costo:%0Data:%1%0Commessa:%2%0Codice:%3%0Descrizione:%4", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_PADRE.w_OFFDATTI.DACODCOM),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))
                    case Empty(Nvl(this.w_PADRE.w_OFFDATTI.DACENCOS,Space(15))) and this.w_FLACQ $ "S-I"
                      this.w_RETVAL = ah_Msgformat("La riga con i seguenti riferimenti � priva di centro di costo:%0Data:%1%0Commessa:%2%0Codice:%3%0Descrizione:%4", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_PADRE.w_OFFDATTI.DACODCOM),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))
                  endcase
                endif
                if g_PERCAN="S" and empty(this.w_RETVAL) and (this.w_PADRE.w_OFFDATTI.DATIPRIG $ "A-E" or this.w_PADRE.w_OFFDATTI.DATIPRI2 $ "A-E") and this.w_FLACQ $ "S-I"
                  if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DACODCOM,Space(15))) 
                    this.w_RETVAL = ah_Msgformat("La riga con i seguenti riferimenti � priva di commessa per i costi:%0Data:%1%0Commessa:%2%0Codice:%3%0Descrizione:%4", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_PADRE.w_OFFDATTI.DACODCOM),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))
                  endif
                endif
                if empty(this.w_RETVAL) and ((this.w_PADRE.w_OFFDATTI.DATIPRIG $ "D-E" ) or (this.w_PADRE.w_OFFDATTI.DATIPRI2 $ "D-E" )) and this.w_FLDANA $ "S-I"
                  * --- Controllo dati analitici ricavi
                  do case
                    case Empty(Nvl(this.w_PADRE.w_OFFDATTI.DAVOCRIC,Space(15))) 
                      this.w_RETVAL = ah_Msgformat("La riga con i seguenti riferimenti � priva di voce di ricavo:%0Data:%1%0Commessa:%2%0Codice:%3%0Descrizione:%4", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_PADRE.w_OFFDATTI.DACODCOM),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))
                    case Empty(Nvl(this.w_PADRE.w_OFFDATTI.DACENRIC,Space(15))) 
                      this.w_RETVAL = ah_Msgformat("La riga con i seguenti riferimenti � priva di centro di ricavo:%0Data:%1%0Commessa:%2%0Codice:%3%0Descrizione:%4", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_PADRE.w_OFFDATTI.DACODCOM),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))
                  endcase
                endif
                if Not Empty(this.w_ATCAUDOC) and (w_GSAG_MPA.w_DATIPRIG $ "D-E") and this.w_RESCHK <> -1 AND (((this.w_FLGCOM <> "N" and Isahe() and g_CACQ="S" and !(w_BUFLANAL<>"S" and not empty(g_CODBUN)))))
                  * --- Controllo dati analitici vendite
                  do case
                    case Empty(Nvl(this.w_PADRE.w_OFFDATTI.DACOMRIC,Space(15))) 
                      ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza commessa ricavi.","!","", Alltrim(STR(this.w_PADRE.w_OFFDATTI.CPROWORD)))
                      this.w_RESCHK = -1
                    case Empty(Nvl(this.w_PADRE.w_OFFDATTI.DAATTRIC,Space(15))) and !isAhe()
                      ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza attivit� ricavi.","!","", Alltrim(STR(this.w_PADRE.w_OFFDATTI.CPROWORD)))
                      this.w_RESCHK = -1
                  endcase
                endif
              endif
            case (this.w_PADRE.w_OFFDATTI.DATIPRIG="N" OR (this.w_PADRE.w_OFFDATTI.DATIPRI2 ="N" and Isalt()) )
              if !Isahe()
                if this.w_FLANAL="S" and Nvl(this.w_PADRE.w_OFFDATTI.DACOSINT,0)>0
                  if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DAVOCCOS,Space(15)))
                    this.w_RETVAL = this.w_RETVAL+ah_Msgformat("La riga con i seguenti riferimenti � priva di voce di costo:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+ chr(10)+chr(13)
                  endif
                  if Empty(Nvl(this.w_PADRE.w_OFFDATTI.DACENCOS,Space(15)))
                    this.w_RETVAL = this.w_RETVAL+ah_Msgformat("La riga con i seguenti riferimenti � priva di centro di costo:%0Data: %1%0Codice: %2%0Descrizione: %3", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT))+ chr(10)+chr(13)
                  endif
                endif
              endif
          endcase
        endif
        * --- Se � attivo il check Controllo su ins./variaz. prestazioni nei Parametri attivit� e se l'utente non � amministratore
        if empty(this.w_RETVAL) AND IsAlt() AND this.w_FLPRES="S" AND NOT Cp_IsAdministrator()
          DIMENSION ArrWhere(1,2)
          ArrWhere(1,1) = "DPCODICE"
          ArrWhere(1,2) = this.w_PADRE.w_OFFDATTI.DACODRES
          this.w_CHKDATAPRE = NVL(this.w_PADRE.ReadTable( "DIPENDEN" , "DPCTRPRE" , @ArrWhere ),"N")
          this.w_GG_PRE = NVL(this.w_PADRE.ReadTable( "DIPENDEN" , "DPGG_PRE" , @ArrWhere ),"N")
          this.w_GG_SUC = NVL(this.w_PADRE.ReadTable( "DIPENDEN" , "DPGG_SUC" , @ArrWhere ),"N")
          Release ArrWhere
          * --- Se attivo il check Controllo su prestazioni (nelle Persone) e se la data della prestazione � al di fuori dell'intervallo definito per il relativo responsabile
          if this.w_CHKDATAPRE="S" AND (this.w_DATINI < date() - this.w_GG_PRE OR this.w_DATINI > date() + this.w_GG_SUC)
            this.w_RETVAL = ah_Msgformat("Nella riga con i seguenti riferimenti la data non � consentita dai controlli relativi al responsabile:%0Data:%1%0Attivit�:%2%0Codice:%3%0Descrizione:%4%0Responsabile:%5", Alltrim(DTOC(this.w_DATINI)),ALLTRIM(this.w_ATOGGETT),ALLTRIM(this.w_CODICE),ALLTRIM(this.w_DADESATT),ALLTRIM(this.w_PADRE.w_OFFDATTI.DACODRES))
          endif
        endif
        if Not Empty(this.w_RETVAL)
          exit
        endif
        this.w_PADRE.w_OFFDATTI.Next()     
      enddo
    endif
    if this.w_PADRE.bCheckBeforeCreation
    endif
    this.w_ATDATOUT = DateTime()
    if Empty(this.w_RETVAL)
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Isahe()
        gsag_bdl(this,"D",this.w_KEYLISTB,.Null.,Space(5))
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    if this.w_bUnderTran
      * --- begin transaction
      cp_BeginTrs()
    endif
    if EMPTY(this.w_RETVAL)
      if EMPTY(this.w_PADRE.w_ATSERIAL)
        i_Conn=i_TableProp[this.OFF_ATTI_IDX, 3]
        cp_NextTableProg(this.w_PADRE, i_Conn, "SEATT", "i_CODAZI,w_ATSERIAL", .T. )
        if EMPTY(this.w_PADRE.w_ATCODUID)
          this.w_PADRE.w_ATCODUID = this.w_PADRE.w_ATSERIAL
        endif
      endif
      this.w_ATANNDOC = STR(YEAR(CP_TODATE(this.w_DATFIN)), 4, 0)
      this.w_PADRE.w_ATANNDOC = this.w_ATANNDOC
      this.w_PADRE.w_ATALFDOC = this.w_ATALFDOC
      this.w_ATNUMDOC = 0
      this.w_PADRE.w_ATNUMDOC = this.w_ATNUMDOC
      if !ISALT() AND this.w_FLPDOC="D"
        cp_NextTableProg(this.w_PADRE, i_Conn, "NUATT", "i_CODAZI,w_ATANNDOC,w_ATALFDOC,w_ATNUMDOC", .T. )
      endif
      Local cMacro
      this.w_PROPINS = 0
      this.w_PROPATT = 1
      this.w_NUMPROP = ALEN(this.w_PADRE.PropList, 1)
      do while this.w_PROPATT<=this.w_NUMPROP
        if this.w_PADRE.PropList(this.w_PROPATT,3)="F"
          this.w_PROPINS = this.w_PROPINS + 1
          DIMENSION ArrInsert( this.w_PROPINS ,2)
          cMacro = UPPER( ALLTRIM( this.w_PADRE.PropList(this.w_PROPATT,1) ) )
          ArrInsert( this.w_PROPINS ,1) = STRTRAN(m.cMacro, "W_", "")
          cMacro = "this.w_PADRE."+m.cMacro
          ArrInsert( this.w_PROPINS ,2) = &cMacro
        endif
        this.w_PROPATT = this.w_PROPATT + 1
      enddo
      release cMacro
      * --- Aggiungo un elemento all'array per ospitare il cpcchk
      DIMENSION ArrInsert( ALEN(ArrInsert,1)+1 ,2)
      ArrInsert( ALEN(ArrInsert,1) , 1) = "cpccchk"
      ArrInsert( ALEN(ArrInsert,1) , 2) = cp_NewCCChk()
      * --- Try
      local bErr_041291D8
      bErr_041291D8=bTrsErr
      this.Try_041291D8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_RETVAL = ah_MsgFormat("Errore inserimento attivit�.%0%1", MESSAGE())
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_RETVAL
      endif
      bTrsErr=bTrsErr or bErr_041291D8
      * --- End
      Release ArrInsert
    endif
    if EMPTY(this.w_RETVAL)
      this.w_PADRE.w_OFF_PART.GoTop()     
      this.w_SELECTCURS = SYS(2015)
      this.w_PADRE.w_OFF_PART.Exec_MCSelect(this.w_SELECTCURS , "MAX(CPROWNUM) AS CPROWNUM", "" , "" , "" , "")     
      if USED(this.w_SELECTCURS)
        SELECT (this.w_SELECTCURS)
        GO TOP
        this.w_CPROWNUM = MAX(NVL(CPROWNUM, 0), 0)
        USE IN SELECT(this.w_SELECTCURS)
      endif
      this.w_PADRE.w_OFF_PART.GoTop()     
      do while !this.w_PADRE.w_OFF_PART.Eof()
        this.w_PADRE.w_OFF_PART.ReadCurrentRecord()     
        this.w_bSaveCurRec = .F.
        if this.w_PADRE.w_OFF_PART.CPROWNUM = 0
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          this.w_PADRE.w_OFF_PART.CPROWNUM = this.w_CPROWNUM
          this.w_bSaveCurRec = .T.
        endif
        if this.w_PADRE.w_OFF_PART.CPROWORD = 0
          this.w_PADRE.w_OFF_PART.CPROWORD = this.w_CPROWNUM * 10
          this.w_bSaveCurRec = .T.
        endif
        if this.w_bSaveCurRec
          this.w_PADRE.w_OFF_PART.SaveCurrentRecord()     
        endif
        DIMENSION ArrInsert( 1 ,2)
        * --- Popolo l'array con i dati del partecipante corrente
        if this.w_PAFLVISI="S" and Empty(Nvl(this.w_PADRE.w_OFF_PART.PAGRURIS," ")) and Nvl(this.w_PADRE.w_OFF_PART.PATIPRIS," ")="P"
          this.w_RETVAL = ah_Msgformat("Impossibile creare l'attivit�:%0partecipante %1 senza gruppo predefinito.", Alltrim(Nvl(this.w_PADRE.w_OFF_PART.PACODRIS," ")))
          exit
        endif
        this.w_PADRE.w_OFF_PART.CurrentRecordToArray(@ArrInsert)     
        * --- Aggiungo un elemento all'array per ospitare la chiave primaria
        DIMENSION ArrInsert( ALEN(ArrInsert,1)+1 ,2)
        ArrInsert( ALEN(ArrInsert,1) , 1) = "PASERIAL"
        ArrInsert( ALEN(ArrInsert,1) , 2) = this.w_PADRE.w_ATSERIAL
        * --- Aggiungo un elemento all'array per ospitare il cpcchk
        DIMENSION ArrInsert( ALEN(ArrInsert,1)+1 ,2)
        ArrInsert( ALEN(ArrInsert,1) , 1) = "cpccchk"
        ArrInsert( ALEN(ArrInsert,1) , 2) = cp_NewCCChk()
        * --- Inserimento record partecipante attuale
        * --- Try
        local bErr_0412B998
        bErr_0412B998=bTrsErr
        this.Try_0412B998()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_RETVAL = ah_MsgFormat("Errore inserimento partecipante.%0%1", MESSAGE())
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_RETVAL
        endif
        bTrsErr=bTrsErr or bErr_0412B998
        * --- End
        Release ArrInsert
        this.w_PADRE.w_OFF_PART.Next()     
      enddo
    endif
    if EMPTY(this.w_RETVAL)
      if !EMPTY(this.w_ATCODNOM) AND this.w_TIPNOM="C"
        * --- La scrittura del figlio integrato degli impianti � vincolata alla presenza del nominativo
        this.w_PADRE.w_COM_ATTI.ReadCurrentRecord()     
        this.w_PADRE.w_COM_ATTI.GoTop()     
        do while !this.w_PADRE.w_COM_ATTI.Eof()
          this.w_PADRE.w_COM_ATTI.ReadCurrentRecord()     
          this.w_bSaveCurRec = .F.
          if this.w_PADRE.w_COM_ATTI.CPROWORD = 0
            this.w_PADRE.w_COM_ATTI.CPROWORD = this.w_PADRE.w_COM_ATTI.recno() * 10
            this.w_bSaveCurRec = .T.
          endif
          if this.w_bSaveCurRec
            this.w_PADRE.w_COM_ATTI.SaveCurrentRecord()     
          endif
          DIMENSION ArrInsert( 1 ,2)
          * --- Popolo l'array con i dati del componente corrente
          this.w_PADRE.w_COM_ATTI.CurrentRecordToArray(@ArrInsert)     
          * --- Aggiungo un elemento all'array per ospitare la chiave primaria
          DIMENSION ArrInsert( ALEN(ArrInsert,1)+1 ,2)
          ArrInsert( ALEN(ArrInsert,1) , 1) = "CASERIAL"
          ArrInsert( ALEN(ArrInsert,1) , 2) = this.w_PADRE.w_ATSERIAL
          * --- Aggiungo un elemento all'array per ospitare il cpcchk
          DIMENSION ArrInsert( ALEN(ArrInsert,1)+1 ,2)
          ArrInsert( ALEN(ArrInsert,1) , 1) = "cpccchk"
          ArrInsert( ALEN(ArrInsert,1) , 2) = cp_NewCCChk()
          * --- Inserimento record componente attuale
          * --- Try
          local bErr_04117078
          bErr_04117078=bTrsErr
          this.Try_04117078()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_RETVAL = ah_MsgFormat("Errore inserimento impianto.%0%1", MESSAGE())
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_RETVAL
          endif
          bTrsErr=bTrsErr or bErr_04117078
          * --- End
          Release ArrInsert
          this.w_PADRE.w_COM_ATTI.Next()     
        enddo
      endif
    endif
    if EMPTY(this.w_RETVAL)
      if this.w_FLNSAP="N"
        this.w_PADRE.w_OFFDATTI.GoTop()     
        this.w_CPROWNUM = 0
        this.w_SELECTCURS = SYS(2015)
        this.w_PADRE.w_OFFDATTI.Exec_MCSelect(this.w_SELECTCURS , "MAX(CPROWNUM) AS CPROWNUM", "" , "" , "" , "")     
        if USED(this.w_SELECTCURS)
          SELECT (this.w_SELECTCURS)
          GO TOP
          this.w_CPROWNUM = MAX(NVL(CPROWNUM, 0), 0)
          USE IN SELECT(this.w_SELECTCURS)
        endif
        this.w_PADRE.w_OFFDATTI.GoTop()     
        do while !this.w_PADRE.w_OFFDATTI.Eof()
          this.w_PADRE.w_OFFDATTI.ReadCurrentRecord()     
          if this.w_PADRE.w_OFFDATTI.CPROWNUM = 0
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_PADRE.w_OFFDATTI.CPROWNUM = this.w_CPROWNUM
          endif
          if this.w_PADRE.w_OFFDATTI.CPROWORD = 0
            this.w_PADRE.w_OFFDATTI.CPROWORD = this.w_CPROWNUM * 10
          endif
          this.w_PADRE.w_OFFDATTI.DAVALRIG = cp_ROUND(this.w_PADRE.w_OFFDATTI.DAQTAMOV*this.w_PADRE.w_OFFDATTI.DAPREZZO,IIF(this.w_NOTIFICA="S",0,g_PERPVL))
          this.w_PADRE.w_OFFDATTI.SaveCurrentRecord()     
          DIMENSION ArrInsert( 1 ,2)
          * --- Popolo l'array con i dati del componente corrente
          this.w_PADRE.w_OFFDATTI.CurrentRecordToArray(@ArrInsert)     
          * --- Aggiungo un elemento all'array per ospitare la chiave primaria
          DIMENSION ArrInsert( ALEN(ArrInsert,1)+1 ,2)
          ArrInsert( ALEN(ArrInsert,1) , 1) = "DASERIAL"
          ArrInsert( ALEN(ArrInsert,1) , 2) = this.w_PADRE.w_ATSERIAL
          * --- Aggiungo un elemento all'array per ospitare il cpcchk
          DIMENSION ArrInsert( ALEN(ArrInsert,1)+1 ,2)
          ArrInsert( ALEN(ArrInsert,1) , 1) = "cpccchk"
          ArrInsert( ALEN(ArrInsert,1) , 2) = cp_NewCCChk()
          * --- Inserimento record dettaglio attuale
          * --- Try
          local bErr_040F2C78
          bErr_040F2C78=bTrsErr
          this.Try_040F2C78()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_RETVAL = ah_MsgFormat("Errore inserimento prestazione.%0%1", MESSAGE())
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_RETVAL
          endif
          bTrsErr=bTrsErr or bErr_040F2C78
          * --- End
          Release ArrInsert
          this.w_PADRE.w_OFFDATTI.Next()     
        enddo
      endif
      * --- Elimino Prestazioni servizi a valore con prezzo a zero e costo interno a zero
      if this.w_PADRE.w_ATSTATUS="P" 
        this.w_ATSERIAL = this.w_PADRE.w_ATSERIAL
      endif
    endif
    * --- Eseguo generazione documento se prevista
    if EMPTY(this.w_RETVAL)
      this.w_LetturaParAlte = i_CODAZI
      * --- Read from PAR_ALTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAGENPRE"+;
          " from "+i_cTable+" PAR_ALTE where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_LetturaParAlte);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAGENPRE;
          from (i_cTable) where;
              PACODAZI = this.w_LetturaParAlte;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GENPRE = NVL(cp_ToDate(_read_.PAGENPRE),cp_NullValue(_read_.PAGENPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_GENPRE="S"
        this.w_RETVAL = GSAG_BGE(this,this.w_PADRE.w_ATSERIAL,space(10),space(15),0,.f.,"D", .F.)
      else
        * --- Read from RIS_ESTR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SRCODSES"+;
            " from "+i_cTable+" RIS_ESTR where ";
                +"SRCODPRA = "+cp_ToStrODBC(this.w_ATCODPRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SRCODSES;
            from (i_cTable) where;
                SRCODPRA = this.w_ATCODPRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODSES = NVL(cp_ToDate(_read_.SRCODSES),cp_NullValue(_read_.SRCODSES))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Isalt() and Not Empty(this.w_CODSES) and Not Empty(this.w_ATCODPRA) 
          * --- Ho ripartizione nelle pratiche
          this.w_FLRESTO = .t.
          * --- Select from RIS_ESTR
          i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SRCODPRA,SRCODSES,SRPARAME  from "+i_cTable+" RIS_ESTR ";
                +" where SRCODPRA="+cp_ToStrODBC(this.w_ATCODPRA)+"";
                +" order by SRPARAME Desc";
                 ,"_Curs_RIS_ESTR")
          else
            select SRCODPRA,SRCODSES,SRPARAME from (i_cTable);
             where SRCODPRA=this.w_ATCODPRA;
             order by SRPARAME Desc;
              into cursor _Curs_RIS_ESTR
          endif
          if used('_Curs_RIS_ESTR')
            select _Curs_RIS_ESTR
            locate for 1=1
            do while not(eof())
            this.w_RETVAL = GSAG_BGE(.null.,this.w_PADRE.w_ATSERIAL,space(10),_Curs_RIS_ESTR.SRCODSES,_Curs_RIS_ESTR.SRPARAME,this.w_FLRESTO,"R", .F.)
            this.w_FLRESTO = .f.
            if Not Empty(this.w_RETVAL)
              exit
            endif
              select _Curs_RIS_ESTR
              continue
            enddo
            use
          endif
          if Empty(this.w_RETVAL)
            * --- Genero documento pre nota spese
            this.w_RETVAL = GSAG_BGE(this,this.w_PADRE.w_ATSERIAL,space(10),space(15),0,.f.,"N", .F.)
          endif
        else
          this.w_RETVAL = GSAG_BGE(this,this.w_PADRE.w_ATSERIAL,space(10),space(15),0,.f.,"D", .F.)
          if Isalt()
            * --- Read from PAR_PRAT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_PRAT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_PRAT_idx,2],.t.,this.PAR_PRAT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PACAUNOT"+;
                " from "+i_cTable+" PAR_PRAT where ";
                    +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PACAUNOT;
                from (i_cTable) where;
                    PACODAZI = i_codazi;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CAUNOT = NVL(cp_ToDate(_read_.PACAUNOT),cp_NullValue(_read_.PACAUNOT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Not Empty(this.w_CAUNOT) and Empty(this.w_RETVAL)
              * --- Genero documento pre nota spese
              this.w_RETVAL = GSAG_BGE(this,this.w_PADRE.w_ATSERIAL,space(10),space(15),0,.f.,"N", .F.)
            endif
          endif
        endif
      endif
    endif
    if this.w_bUnderTran
      if EMPTY(this.w_RETVAL)
        * --- commit
        cp_EndTrs(.t.)
      else
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
    endif
    if w_ERROR>0
      AH_ERRORMSG(w_RESULT)
    endif
    * --- Esegue invio messaggio se previsto.
    *     Non ha nessuna influenza ai fini della conculsione della transazione
    if this.w_PADRE.bSendMSG and Empty(this.w_RETVAL)
      Local cMacro
      cMacro = this.w_PADRE.cSendMSGRoutine
      this.w_RETVAL = &cMacro
      Release cMacro
    endif
  endproc
  proc Try_041291D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("OFF_ATTI", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento attivit�.%0%1", this.w_RETVAL)
    endif
    return
  proc Try_0412B998()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("OFF_PART", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento partecipante.%0%1", this.w_RETVAL)
    endif
    return
  proc Try_04117078()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("COM_ATTI", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento impianto.%0%1", this.w_RETVAL)
    endif
    return
  proc Try_040F2C78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("OFFDATTI", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento prestazione.%0%1", this.w_RETVAL)
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPADRE)
    this.pPADRE=pPADRE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='OFFDATTI'
    this.cWorkTables[4]='KEY_ARTI'
    this.cWorkTables[5]='RIS_ESTR'
    this.cWorkTables[6]='PAR_PRAT'
    this.cWorkTables[7]='LISTINI'
    this.cWorkTables[8]='PAR_ALTE'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_RIS_ESTR')
      use in _Curs_RIS_ESTR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPADRE"
endproc
