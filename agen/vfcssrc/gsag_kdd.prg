* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kdd                                                        *
*              Abbina documenti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_46]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-06                                                      *
* Last revis.: 2013-10-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kdd",oParentObject))

* --- Class definition
define class tgsag_kdd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 687
  Height = 541
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-10-24"
  HelpContextID=82454377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Constant Properties
  _IDX = 0
  VASTRUTT_IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsag_kdd"
  cComment = "Abbina documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_CODCLI = space(15)
  w_ACQU = space(1)
  w_CATDO1 = space(2)
  o_CATDO1 = space(2)
  w_CATDOC = space(2)
  w_CATDO2 = space(2)
  o_CATDO2 = space(2)
  w_FILDOC = space(2)
  w_TIPDOC = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DOCINI = ctod('  /  /  ')
  w_NUMINI = 0
  w_DOCFIN = ctod('  /  /  ')
  w_NUMFIN = 0
  w_ALFDOC = space(10)
  w_STRUTT = space(10)
  w_FLSEND1 = space(10)
  o_FLSEND1 = space(10)
  w_DESDOC = space(35)
  w_TIPOCF = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_FLVEAC1 = space(1)
  w_FLSEND = space(1)
  w_TIPDIS = space(1)
  w_SELEZI = space(1)
  w_TDFLINTE = space(1)
  w_CAT_DOC = space(2)
  w_Zoomdoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kddPag1","gsag_kdd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATDO1_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoomdoc = this.oPgFrm.Pages(1).oPag.Zoomdoc
    DoDefault()
    proc Destroy()
      this.w_Zoomdoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='CONTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gsar_bdf(this,"A")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC=space(1)
      .w_CODCLI=space(15)
      .w_ACQU=space(1)
      .w_CATDO1=space(2)
      .w_CATDOC=space(2)
      .w_CATDO2=space(2)
      .w_FILDOC=space(2)
      .w_TIPDOC=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DOCINI=ctod("  /  /  ")
      .w_NUMINI=0
      .w_DOCFIN=ctod("  /  /  ")
      .w_NUMFIN=0
      .w_ALFDOC=space(10)
      .w_STRUTT=space(10)
      .w_FLSEND1=space(10)
      .w_DESDOC=space(35)
      .w_TIPOCF=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLVEAC1=space(1)
      .w_FLSEND=space(1)
      .w_TIPDIS=space(1)
      .w_SELEZI=space(1)
      .w_TDFLINTE=space(1)
      .w_CAT_DOC=space(2)
      .w_CODCLI=oParentObject.w_CODCLI
        .w_FLVEAC = 'V'
          .DoRTCalc(2,2,.f.)
        .w_ACQU = iif(g_APPLICATION='ad hoc ENTERPRISE',g_CACQ,g_ACQU)
        .w_CATDO1 = 'XX'
        .w_CATDOC = iif(.w_CATDO1='XX','  ',.w_CATDO1)
        .w_CATDO2 = 'XX'
        .w_FILDOC = .w_CATDOC
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_8('Full')
        endif
        .w_OBTEST = i_DATSYS
        .w_DOCINI = g_INIESE
          .DoRTCalc(11,11,.f.)
        .w_DOCFIN = g_FINESE
        .DoRTCalc(13,15,.f.)
        if not(empty(.w_STRUTT))
          .link_1_15('Full')
        endif
        .w_FLSEND1 = 'N'
      .oPgFrm.Page1.oPag.Zoomdoc.Calculate()
          .DoRTCalc(17,17,.f.)
        .w_TIPOCF = 'C'
          .DoRTCalc(19,19,.f.)
        .w_FLVEAC1 = IIF(.w_FLVEAC='T',' ',.w_FLVEAC)
        .w_FLSEND = IIF(.w_FLSEND1='S','N',' ')
        .w_TIPDIS = 'N'
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
          .DoRTCalc(24,24,.f.)
        .w_CAT_DOC = IIF(ISALT(), 'DI', SPACE(2) )
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODCLI=.w_CODCLI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_ACQU = iif(g_APPLICATION='ad hoc ENTERPRISE',g_CACQ,g_ACQU)
        .DoRTCalc(4,4,.t.)
        if .o_CATDO1<>.w_CATDO1.or. .o_CATDO2<>.w_CATDO2
            .w_CATDOC = iif(.w_CATDO1='XX','  ',.w_CATDO1)
        endif
        .DoRTCalc(6,6,.t.)
        if .o_CATDO1<>.w_CATDO1
            .w_FILDOC = .w_CATDOC
        endif
        .DoRTCalc(8,8,.t.)
            .w_OBTEST = i_DATSYS
        .DoRTCalc(10,14,.t.)
          .link_1_15('Full')
        .oPgFrm.Page1.oPag.Zoomdoc.Calculate()
        .DoRTCalc(16,17,.t.)
            .w_TIPOCF = 'C'
        .DoRTCalc(19,19,.t.)
        if .o_FLVEAC<>.w_FLVEAC
            .w_FLVEAC1 = IIF(.w_FLVEAC='T',' ',.w_FLVEAC)
        endif
        if .o_FLSEND1<>.w_FLSEND1
            .w_FLSEND = IIF(.w_FLSEND1='S','N',' ')
        endif
            .w_TIPDIS = 'N'
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoomdoc.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoomdoc.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPDOC
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BLT',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_8'),i_cWhere,'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDFLINTE = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_CATDOC = space(2)
      this.w_TDFLINTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDFLINTE<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale incongruente")
        endif
        this.w_TIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_CATDOC = space(2)
        this.w_TDFLINTE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTT
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_STRUTT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_STRUTT)
            select STCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTT = NVL(_Link_.STCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTT = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATDO1_1_4.RadioValue()==this.w_CATDO1)
      this.oPgFrm.Page1.oPag.oCATDO1_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_8.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_8.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_10.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_10.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_11.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_11.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_12.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_12.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_13.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_13.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_14.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_14.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_19.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_19.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_34.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_34.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TDFLINTE<>'F')  and not(empty(.w_TIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPDOC_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale incongruente")
          case   ((empty(.w_DOCINI)) or not(.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DOCINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DOCFIN)) or not(.w_DOCINI<=.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DOCFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC = this.w_FLVEAC
    this.o_CATDO1 = this.w_CATDO1
    this.o_CATDO2 = this.w_CATDO2
    this.o_FLSEND1 = this.w_FLSEND1
    return

enddefine

* --- Define pages as container
define class tgsag_kddPag1 as StdContainer
  Width  = 683
  height = 541
  stdWidth  = 683
  stdheight = 541
  resizeXpos=281
  resizeYpos=221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCATDO1_1_4 as StdCombo with uid="QDJUHAEQBR",rtseq=4,rtrep=.f.,left=119,top=10,width=135,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 246456794;
    , cFormVar="w_CATDO1",RowSource=""+"Tutti i documenti,"+"Ordini,"+"Doc.interni,"+"Doc.di trasporto,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO1_1_4.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'OR',;
    iif(this.value =3,'DI',;
    iif(this.value =4,'DT',;
    iif(this.value =5,'FA',;
    iif(this.value =6,'NC',;
    space(2))))))))
  endfunc
  func oCATDO1_1_4.GetRadio()
    this.Parent.oContained.w_CATDO1 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO1_1_4.SetRadio()
    this.Parent.oContained.w_CATDO1=trim(this.Parent.oContained.w_CATDO1)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO1=='XX',1,;
      iif(this.Parent.oContained.w_CATDO1=='OR',2,;
      iif(this.Parent.oContained.w_CATDO1=='DI',3,;
      iif(this.Parent.oContained.w_CATDO1=='DT',4,;
      iif(this.Parent.oContained.w_CATDO1=='FA',5,;
      iif(this.Parent.oContained.w_CATDO1=='NC',6,;
      0))))))
  endfunc

  add object oTIPDOC_1_8 as StdField with uid="SOMWIURCTL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale incongruente",;
    ToolTipText = "Codice tipo documento di selezione (spazio = no selezione)",;
    HelpContextID = 212916426,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=347, Top=10, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAR_BLT", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPDOC_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BLT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc

  add object oDOCINI_1_10 as StdField with uid="ZGFEWOOULR",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 113025994,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=119, Top=36

  func oDOCINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_11 as StdField with uid="MUNXKFVXTI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento iniziale",;
    HelpContextID = 112983338,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=119, Top=63, cSayPict="'999999999999999'", cGetPict="'999999999999999'"

  add object oDOCFIN_1_12 as StdField with uid="MSJYJXJTGQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 34579402,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=346, Top=36

  func oDOCFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_13 as StdField with uid="LTEGZCEXHB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento finale",;
    HelpContextID = 34536746,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=346, Top=63, cSayPict="'999999999999999'", cGetPict="'999999999999999'"

  add object oALFDOC_1_14 as StdField with uid="PGASOKMZAJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 212956922,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=535, Top=63, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)


  add object Zoomdoc as cp_szoombox with uid="MRJMLVEPWI",left=20, top=101, width=638,height=376,;
    caption='ZOOMDOC',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSAG_KDD",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Init,Esegui",;
    nPag=1;
    , HelpContextID = 245898646


  add object oBtn_1_18 as StdButton with uid="RLBYLLHJDM",left=573, top=487, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 82433818;
    , caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        gsag_bab(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESDOC_1_19 as StdField with uid="OADXZRBATB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 212905418,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=409, Top=10, InputMask=replicate('X',35)


  add object oBtn_1_23 as StdButton with uid="EITOYBANNL",left=623, top=487, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 240265990;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="KMGHCQHBEF",left=626, top=50, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , HelpContextID = 94467862;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        .Notifyevent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_34 as StdRadio with uid="RMUJOFEZEL",rtseq=23,rtrep=.f.,left=26, top=493, width=136,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_34.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 100670682
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 100670682
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_34.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_34.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_35 as cp_runprogram with uid="HWMKCXUGHJ",left=6, top=543, width=193,height=27,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsag_bab(w_SELEZI)",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 95543270

  add object oStr_1_20 as StdString with uid="WPBOLNVROY",Visible=.t., Left=275, Top=10,;
    Alignment=1, Width=68, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="OOQMYWSQRY",Visible=.t., Left=6, Top=40,;
    Alignment=1, Width=108, Height=18,;
    Caption="Doc. dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="KTZRCRKILE",Visible=.t., Left=254, Top=40,;
    Alignment=1, Width=89, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="HLEBWDKZSD",Visible=.t., Left=468, Top=65,;
    Alignment=1, Width=65, Height=18,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="LYWXYRNSYZ",Visible=.t., Left=6, Top=65,;
    Alignment=1, Width=108, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="SSXMIEIOAX",Visible=.t., Left=242, Top=65,;
    Alignment=1, Width=101, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="DSORJRRLOZ",Visible=.t., Left=13, Top=13,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tipo doc.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kdd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
