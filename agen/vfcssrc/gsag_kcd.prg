* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kcd                                                        *
*              Composizione descrizione attivit�                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-31                                                      *
* Last revis.: 2012-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kcd",oParentObject))

* --- Class definition
define class tgsag_kcd as StdForm
  Top    = 11
  Left   = 2

  * --- Standard Properties
  Width  = 758
  Height = 102
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-18"
  HelpContextID=99231593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kcd"
  cComment = "Composizione descrizione attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CAFLATTI = space(10)
  o_CAFLATTI = space(10)
  w_CAFLATTN = 0
  w_CAFLPART = space(1)
  o_CAFLPART = space(1)
  w_CAFLPARN = 0
  w_CAFLPRAT = space(1)
  o_CAFLPRAT = space(1)
  w_CAFLPRAN = 0
  w_CAFLSOGG = space(1)
  o_CAFLSOGG = space(1)
  w_CAFLSOGN = 0
  w_CAFLGIUD = space(1)
  o_CAFLGIUD = space(1)
  w_CAFLGIUN = 0
  w_CAFLNOMI = space(1)
  o_CAFLNOMI = space(1)
  w_CAFLNOMN = 0
  w_CAFLDINI = space(1)
  o_CAFLDINI = space(1)
  w_CAFLDINN = 0
  w_CAFLDFIN = space(1)
  o_CAFLDFIN = space(1)
  w_CAFLDFNN = 0
  w_CAFLPARN = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kcdPag1","gsag_kcd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCAFLATTI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CAFLATTI=space(10)
      .w_CAFLATTN=0
      .w_CAFLPART=space(1)
      .w_CAFLPARN=0
      .w_CAFLPRAT=space(1)
      .w_CAFLPRAN=0
      .w_CAFLSOGG=space(1)
      .w_CAFLSOGN=0
      .w_CAFLGIUD=space(1)
      .w_CAFLGIUN=0
      .w_CAFLNOMI=space(1)
      .w_CAFLNOMN=0
      .w_CAFLDINI=space(1)
      .w_CAFLDINN=0
      .w_CAFLDFIN=space(1)
      .w_CAFLDFNN=0
      .w_CAFLPARN=space(1)
      .w_CAFLATTI=oParentObject.w_CAFLATTI
      .w_CAFLATTN=oParentObject.w_CAFLATTN
      .w_CAFLPART=oParentObject.w_CAFLPART
      .w_CAFLPARN=oParentObject.w_CAFLPARN
      .w_CAFLPRAT=oParentObject.w_CAFLPRAT
      .w_CAFLPRAN=oParentObject.w_CAFLPRAN
      .w_CAFLSOGG=oParentObject.w_CAFLSOGG
      .w_CAFLSOGN=oParentObject.w_CAFLSOGN
      .w_CAFLGIUD=oParentObject.w_CAFLGIUD
      .w_CAFLGIUN=oParentObject.w_CAFLGIUN
      .w_CAFLNOMI=oParentObject.w_CAFLNOMI
      .w_CAFLNOMN=oParentObject.w_CAFLNOMN
      .w_CAFLDINI=oParentObject.w_CAFLDINI
      .w_CAFLDINN=oParentObject.w_CAFLDINN
      .w_CAFLDFIN=oParentObject.w_CAFLDFIN
      .w_CAFLDFNN=oParentObject.w_CAFLDFNN
      .w_CAFLPARN=oParentObject.w_CAFLPARN
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate(IIF(IsAlt(), "Se attivo, viene inserito il nome della pratica in agenda", "Se attivo, viene inserito il nome della commessa in agenda"))
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate(IIF(IsAlt(), "Inserire l'ordine di visualizzazione del nome pratica in agenda", "Inserire l'ordine di visualizzazione del nome commessa in agenda"))
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate(IIF(IsAlt(), "Pratica", "Commessa"))
    endwith
    this.DoRTCalc(1,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCAFLATTI_1_3.enabled = i_bVal
      .Page1.oPag.oCAFLATTN_1_4.enabled = i_bVal
      .Page1.oPag.oCAFLPART_1_5.enabled = i_bVal
      .Page1.oPag.oCAFLPARN_1_6.enabled = i_bVal
      .Page1.oPag.oCAFLPRAT_1_7.enabled = i_bVal
      .Page1.oPag.oCAFLPRAN_1_8.enabled = i_bVal
      .Page1.oPag.oCAFLSOGG_1_9.enabled = i_bVal
      .Page1.oPag.oCAFLSOGN_1_10.enabled = i_bVal
      .Page1.oPag.oCAFLGIUD_1_11.enabled = i_bVal
      .Page1.oPag.oCAFLGIUN_1_12.enabled = i_bVal
      .Page1.oPag.oCAFLNOMI_1_13.enabled = i_bVal
      .Page1.oPag.oCAFLNOMN_1_14.enabled = i_bVal
      .Page1.oPag.oCAFLDINI_1_15.enabled = i_bVal
      .Page1.oPag.oCAFLDINN_1_16.enabled = i_bVal
      .Page1.oPag.oCAFLDFIN_1_17.enabled = i_bVal
      .Page1.oPag.oCAFLDFNN_1_18.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CAFLATTI=.w_CAFLATTI
      .oParentObject.w_CAFLATTN=.w_CAFLATTN
      .oParentObject.w_CAFLPART=.w_CAFLPART
      .oParentObject.w_CAFLPARN=.w_CAFLPARN
      .oParentObject.w_CAFLPRAT=.w_CAFLPRAT
      .oParentObject.w_CAFLPRAN=.w_CAFLPRAN
      .oParentObject.w_CAFLSOGG=.w_CAFLSOGG
      .oParentObject.w_CAFLSOGN=.w_CAFLSOGN
      .oParentObject.w_CAFLGIUD=.w_CAFLGIUD
      .oParentObject.w_CAFLGIUN=.w_CAFLGIUN
      .oParentObject.w_CAFLNOMI=.w_CAFLNOMI
      .oParentObject.w_CAFLNOMN=.w_CAFLNOMN
      .oParentObject.w_CAFLDINI=.w_CAFLDINI
      .oParentObject.w_CAFLDINN=.w_CAFLDINN
      .oParentObject.w_CAFLDFIN=.w_CAFLDFIN
      .oParentObject.w_CAFLDFNN=.w_CAFLDFNN
      .oParentObject.w_CAFLPARN=.w_CAFLPARN
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CAFLATTI<>.w_CAFLATTI
          .Calculate_RSIEOUGEBJ()
        endif
        if .o_CAFLPART<>.w_CAFLPART
          .Calculate_KSDWORKUPK()
        endif
        if .o_CAFLPRAT<>.w_CAFLPRAT
          .Calculate_FASXRWKQVM()
        endif
        if .o_CAFLSOGG<>.w_CAFLSOGG
          .Calculate_DISVFZXBPK()
        endif
        if .o_CAFLGIUD<>.w_CAFLGIUD
          .Calculate_PKPJFAIVFZ()
        endif
        if .o_CAFLNOMI<>.w_CAFLNOMI
          .Calculate_RIFHQORAII()
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(IIF(IsAlt(), "Se attivo, viene inserito il nome della pratica in agenda", "Se attivo, viene inserito il nome della commessa in agenda"))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(IIF(IsAlt(), "Inserire l'ordine di visualizzazione del nome pratica in agenda", "Inserire l'ordine di visualizzazione del nome commessa in agenda"))
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(IIF(IsAlt(), "Pratica", "Commessa"))
        if .o_CAFLDINI<>.w_CAFLDINI
          .Calculate_DIHBYEANXR()
        endif
        if .o_CAFLDFIN<>.w_CAFLDFIN
          .Calculate_MOQVZQXBIY()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate(IIF(IsAlt(), "Se attivo, viene inserito il nome della pratica in agenda", "Se attivo, viene inserito il nome della commessa in agenda"))
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate(IIF(IsAlt(), "Inserire l'ordine di visualizzazione del nome pratica in agenda", "Inserire l'ordine di visualizzazione del nome commessa in agenda"))
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(IIF(IsAlt(), "Pratica", "Commessa"))
    endwith
  return

  proc Calculate_RSIEOUGEBJ()
    with this
          * --- Azzeramento w_CAFLATTN
          .w_CAFLATTN = IIF(.w_CAFLATTI='N', 0, .w_CAFLATTN)
    endwith
  endproc
  proc Calculate_KSDWORKUPK()
    with this
          * --- Azzeramento w_CAFLPARN
          .w_CAFLPARN = IIF(.w_CAFLPART='N', 0, .w_CAFLPARN)
    endwith
  endproc
  proc Calculate_FASXRWKQVM()
    with this
          * --- Azzeramento w_CAFLPRAN
          .w_CAFLPRAN = IIF(.w_CAFLPRAT='N', 0, .w_CAFLPRAN)
    endwith
  endproc
  proc Calculate_DISVFZXBPK()
    with this
          * --- Azzeramento w_CAFLSOGN
          .w_CAFLSOGN = IIF(.w_CAFLSOGG='N', 0, .w_CAFLSOGN)
    endwith
  endproc
  proc Calculate_PKPJFAIVFZ()
    with this
          * --- Azzeramento w_CAFLGIUN
          .w_CAFLGIUN = IIF(.w_CAFLGIUD='N', 0, .w_CAFLGIUN)
    endwith
  endproc
  proc Calculate_RIFHQORAII()
    with this
          * --- Azzeramento w_CAFLNOMN
          .w_CAFLNOMN = IIF(.w_CAFLNOMI='N', 0, .w_CAFLNOMN)
    endwith
  endproc
  proc Calculate_DIHBYEANXR()
    with this
          * --- Azzeramento w_CAFLDINN
          .w_CAFLDINN = IIF(.w_CAFLDINI='N', 0, .w_CAFLDINN)
    endwith
  endproc
  proc Calculate_MOQVZQXBIY()
    with this
          * --- Azzeramento w_CAFLDFNN
          .w_CAFLDFNN = IIF(.w_CAFLDFIN='N', 0, .w_CAFLDFNN)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCAFLATTN_1_4.enabled = this.oPgFrm.Page1.oPag.oCAFLATTN_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCAFLPARN_1_6.enabled = this.oPgFrm.Page1.oPag.oCAFLPARN_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCAFLPRAN_1_8.enabled = this.oPgFrm.Page1.oPag.oCAFLPRAN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCAFLSOGN_1_10.enabled = this.oPgFrm.Page1.oPag.oCAFLSOGN_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCAFLGIUN_1_12.enabled = this.oPgFrm.Page1.oPag.oCAFLGIUN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCAFLNOMN_1_14.enabled = this.oPgFrm.Page1.oPag.oCAFLNOMN_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCAFLDINN_1_16.enabled = this.oPgFrm.Page1.oPag.oCAFLDINN_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCAFLDFNN_1_18.enabled = this.oPgFrm.Page1.oPag.oCAFLDFNN_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAFLSOGG_1_9.visible=!this.oPgFrm.Page1.oPag.oCAFLSOGG_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCAFLSOGN_1_10.visible=!this.oPgFrm.Page1.oPag.oCAFLSOGN_1_10.mHide()
    this.oPgFrm.Page1.oPag.oCAFLGIUD_1_11.visible=!this.oPgFrm.Page1.oPag.oCAFLGIUD_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCAFLGIUN_1_12.visible=!this.oPgFrm.Page1.oPag.oCAFLGIUN_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCAFLNOMI_1_13.visible=!this.oPgFrm.Page1.oPag.oCAFLNOMI_1_13.mHide()
    this.oPgFrm.Page1.oPag.oCAFLNOMN_1_14.visible=!this.oPgFrm.Page1.oPag.oCAFLNOMN_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCAFLATTI_1_3.RadioValue()==this.w_CAFLATTI)
      this.oPgFrm.Page1.oPag.oCAFLATTI_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLATTN_1_4.value==this.w_CAFLATTN)
      this.oPgFrm.Page1.oPag.oCAFLATTN_1_4.value=this.w_CAFLATTN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLPART_1_5.RadioValue()==this.w_CAFLPART)
      this.oPgFrm.Page1.oPag.oCAFLPART_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLPARN_1_6.value==this.w_CAFLPARN)
      this.oPgFrm.Page1.oPag.oCAFLPARN_1_6.value=this.w_CAFLPARN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLPRAT_1_7.RadioValue()==this.w_CAFLPRAT)
      this.oPgFrm.Page1.oPag.oCAFLPRAT_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLPRAN_1_8.value==this.w_CAFLPRAN)
      this.oPgFrm.Page1.oPag.oCAFLPRAN_1_8.value=this.w_CAFLPRAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLSOGG_1_9.RadioValue()==this.w_CAFLSOGG)
      this.oPgFrm.Page1.oPag.oCAFLSOGG_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLSOGN_1_10.value==this.w_CAFLSOGN)
      this.oPgFrm.Page1.oPag.oCAFLSOGN_1_10.value=this.w_CAFLSOGN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLGIUD_1_11.RadioValue()==this.w_CAFLGIUD)
      this.oPgFrm.Page1.oPag.oCAFLGIUD_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLGIUN_1_12.value==this.w_CAFLGIUN)
      this.oPgFrm.Page1.oPag.oCAFLGIUN_1_12.value=this.w_CAFLGIUN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLNOMI_1_13.RadioValue()==this.w_CAFLNOMI)
      this.oPgFrm.Page1.oPag.oCAFLNOMI_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLNOMN_1_14.value==this.w_CAFLNOMN)
      this.oPgFrm.Page1.oPag.oCAFLNOMN_1_14.value=this.w_CAFLNOMN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLDINI_1_15.RadioValue()==this.w_CAFLDINI)
      this.oPgFrm.Page1.oPag.oCAFLDINI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLDINN_1_16.value==this.w_CAFLDINN)
      this.oPgFrm.Page1.oPag.oCAFLDINN_1_16.value=this.w_CAFLDINN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLDFIN_1_17.RadioValue()==this.w_CAFLDFIN)
      this.oPgFrm.Page1.oPag.oCAFLDFIN_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLDFNN_1_18.value==this.w_CAFLDFNN)
      this.oPgFrm.Page1.oPag.oCAFLDFNN_1_18.value=this.w_CAFLDFNN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CAFLATTN>=0)  and (.w_CAFLATTI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAFLATTN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAFLPARN>=0)  and (.w_CAFLPART='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAFLPARN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAFLPRAN>=0)  and (.w_CAFLPRAT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAFLPRAN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAFLSOGN>=0)  and not(NOT ISALT())  and (.w_CAFLSOGG='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAFLSOGN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAFLGIUN>=0)  and not(NOT ISALT())  and (.w_CAFLGIUD='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAFLGIUN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAFLNOMN>=0)  and not(ISALT())  and (.w_CAFLNOMI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAFLNOMN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAFLDINN>=0)  and (.w_CAFLDINI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAFLDINN_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAFLDFNN>=0)  and (.w_CAFLDFIN='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAFLDFNN_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CAFLATTI = this.w_CAFLATTI
    this.o_CAFLPART = this.w_CAFLPART
    this.o_CAFLPRAT = this.w_CAFLPRAT
    this.o_CAFLSOGG = this.w_CAFLSOGG
    this.o_CAFLGIUD = this.w_CAFLGIUD
    this.o_CAFLNOMI = this.w_CAFLNOMI
    this.o_CAFLDINI = this.w_CAFLDINI
    this.o_CAFLDFIN = this.w_CAFLDFIN
    return

enddefine

* --- Define pages as container
define class tgsag_kcdPag1 as StdContainer
  Width  = 754
  height = 102
  stdWidth  = 754
  stdheight = 102
  resizeXpos=739
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCAFLATTI_1_3 as StdCheck with uid="JSQRXDITZV",rtseq=1,rtrep=.f.,left=15, top=42, caption="Oggetto attivit�",;
    ToolTipText = "Se attivo, viene inserito l'oggetto dell'attivit� in agenda",;
    HelpContextID = 41320047,;
    cFormVar="w_CAFLATTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLATTI_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLATTI_1_3.GetRadio()
    this.Parent.oContained.w_CAFLATTI = this.RadioValue()
    return .t.
  endfunc

  func oCAFLATTI_1_3.SetRadio()
    this.Parent.oContained.w_CAFLATTI=trim(this.Parent.oContained.w_CAFLATTI)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLATTI=='S',1,;
      0)
  endfunc

  add object oCAFLATTN_1_4 as StdField with uid="CKVBNRVDXL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CAFLATTN", cQueryName = "CAFLATTN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 41320052,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=12, Top=67

  func oCAFLATTN_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLATTI='S')
    endwith
   endif
  endfunc

  func oCAFLATTN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAFLATTN>=0)
    endwith
    return bRes
  endfunc

  add object oCAFLPART_1_5 as StdCheck with uid="FDNPOTNAPH",rtseq=3,rtrep=.f.,left=137, top=42, caption="Partecipanti attivit�",;
    ToolTipText = "Se attivo, vengono inseriti i partecipanti dell'attivit� in agenda",;
    HelpContextID = 6717050,;
    cFormVar="w_CAFLPART", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLPART_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLPART_1_5.GetRadio()
    this.Parent.oContained.w_CAFLPART = this.RadioValue()
    return .t.
  endfunc

  func oCAFLPART_1_5.SetRadio()
    this.Parent.oContained.w_CAFLPART=trim(this.Parent.oContained.w_CAFLPART)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLPART=='S',1,;
      0)
  endfunc

  add object oCAFLPARN_1_6 as StdField with uid="FADQNEAKPC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CAFLPARN", cQueryName = "CAFLPARN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 6717044,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=134, Top=67

  func oCAFLPARN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLPART='S')
    endwith
   endif
  endfunc

  func oCAFLPARN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAFLPARN>=0)
    endwith
    return bRes
  endfunc

  add object oCAFLPRAT_1_7 as StdCheck with uid="UUZPTZIPEU",rtseq=5,rtrep=.f.,left=274, top=42, caption="Pratica",;
    ToolTipText = "Se attivo, vengono inseriti i partecipanti dell'attivit� in agenda",;
    HelpContextID = 23494266,;
    cFormVar="w_CAFLPRAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLPRAT_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLPRAT_1_7.GetRadio()
    this.Parent.oContained.w_CAFLPRAT = this.RadioValue()
    return .t.
  endfunc

  func oCAFLPRAT_1_7.SetRadio()
    this.Parent.oContained.w_CAFLPRAT=trim(this.Parent.oContained.w_CAFLPRAT)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLPRAT=='S',1,;
      0)
  endfunc

  add object oCAFLPRAN_1_8 as StdField with uid="CSAQQJXEAX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CAFLPRAN", cQueryName = "CAFLPRAN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 23494260,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=271, Top=67

  func oCAFLPRAN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLPRAT='S')
    endwith
   endif
  endfunc

  func oCAFLPRAN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAFLPRAN>=0)
    endwith
    return bRes
  endfunc

  add object oCAFLSOGG_1_9 as StdCheck with uid="SUDCTEDEDG",rtseq=7,rtrep=.f.,left=350, top=42, caption="Soggetti esterni pratica",;
    ToolTipText = "Se attivo, vengono inseriti i soggetti esterni della pratica in agenda",;
    HelpContextID = 244743789,;
    cFormVar="w_CAFLSOGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLSOGG_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLSOGG_1_9.GetRadio()
    this.Parent.oContained.w_CAFLSOGG = this.RadioValue()
    return .t.
  endfunc

  func oCAFLSOGG_1_9.SetRadio()
    this.Parent.oContained.w_CAFLSOGG=trim(this.Parent.oContained.w_CAFLSOGG)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLSOGG=='S',1,;
      0)
  endfunc

  func oCAFLSOGG_1_9.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oCAFLSOGN_1_10 as StdField with uid="ZZUQNOTULV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CAFLSOGN", cQueryName = "CAFLSOGN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Inserire l'ordine di visualizzazione dei soggetti esterni della pratica in agenda",;
    HelpContextID = 244743796,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=347, Top=67

  func oCAFLSOGN_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLSOGG='S')
    endwith
   endif
  endfunc

  func oCAFLSOGN_1_10.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oCAFLSOGN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAFLSOGN>=0)
    endwith
    return bRes
  endfunc

  add object oCAFLGIUD_1_11 as StdCheck with uid="INHHJWLHUE",rtseq=9,rtrep=.f.,left=507, top=42, caption="Giudice",;
    ToolTipText = "Se attivo, viene inserito il giudice dell'attivit� in agenda",;
    HelpContextID = 131497578,;
    cFormVar="w_CAFLGIUD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLGIUD_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLGIUD_1_11.GetRadio()
    this.Parent.oContained.w_CAFLGIUD = this.RadioValue()
    return .t.
  endfunc

  func oCAFLGIUD_1_11.SetRadio()
    this.Parent.oContained.w_CAFLGIUD=trim(this.Parent.oContained.w_CAFLGIUD)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLGIUD=='S',1,;
      0)
  endfunc

  func oCAFLGIUD_1_11.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oCAFLGIUN_1_12 as StdField with uid="YGEVZKHUDL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CAFLGIUN", cQueryName = "CAFLGIUN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 131497588,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=504, Top=67

  func oCAFLGIUN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLGIUD='S')
    endwith
   endif
  endfunc

  func oCAFLGIUN_1_12.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oCAFLGIUN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAFLGIUN>=0)
    endwith
    return bRes
  endfunc

  add object oCAFLNOMI_1_13 as StdCheck with uid="UQUIXWIKJH",rtseq=11,rtrep=.f.,left=377, top=42, caption="Nominativo",;
    ToolTipText = "Se attivo, viene inserito il nominativo in agenda",;
    HelpContextID = 28934545,;
    cFormVar="w_CAFLNOMI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLNOMI_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLNOMI_1_13.GetRadio()
    this.Parent.oContained.w_CAFLNOMI = this.RadioValue()
    return .t.
  endfunc

  func oCAFLNOMI_1_13.SetRadio()
    this.Parent.oContained.w_CAFLNOMI=trim(this.Parent.oContained.w_CAFLNOMI)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLNOMI=='S',1,;
      0)
  endfunc

  func oCAFLNOMI_1_13.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oCAFLNOMN_1_14 as StdField with uid="WWBQNGJWCR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CAFLNOMN", cQueryName = "CAFLNOMN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Inserire l'ordine di visualizzazione del nominativo in agenda",;
    HelpContextID = 28934540,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=374, Top=67

  func oCAFLNOMN_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNOMI='S')
    endwith
   endif
  endfunc

  func oCAFLNOMN_1_14.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oCAFLNOMN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAFLNOMN>=0)
    endwith
    return bRes
  endfunc

  add object oCAFLDINI_1_15 as StdCheck with uid="DCFJZXIJTH",rtseq=13,rtrep=.f.,left=584, top=42, caption="Ora inizio",;
    ToolTipText = "Se attivo, viene inserita l'ora d'inizio dell'attivit� in agenda",;
    HelpContextID = 140083601,;
    cFormVar="w_CAFLDINI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLDINI_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLDINI_1_15.GetRadio()
    this.Parent.oContained.w_CAFLDINI = this.RadioValue()
    return .t.
  endfunc

  func oCAFLDINI_1_15.SetRadio()
    this.Parent.oContained.w_CAFLDINI=trim(this.Parent.oContained.w_CAFLDINI)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLDINI=='S',1,;
      0)
  endfunc

  add object oCAFLDINN_1_16 as StdField with uid="LRVVUXLGQJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CAFLDINN", cQueryName = "CAFLDINN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 140083596,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=584, Top=67

  func oCAFLDINN_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLDINI='S')
    endwith
   endif
  endfunc

  func oCAFLDINN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAFLDINN>=0)
    endwith
    return bRes
  endfunc

  add object oCAFLDFIN_1_17 as StdCheck with uid="NUYUVZOCCB",rtseq=15,rtrep=.f.,left=667, top=42, caption="Ora fine",;
    ToolTipText = "Se attivo, viene inserita l'ora di fine dell'attivit� in agenda",;
    HelpContextID = 190415244,;
    cFormVar="w_CAFLDFIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLDFIN_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAFLDFIN_1_17.GetRadio()
    this.Parent.oContained.w_CAFLDFIN = this.RadioValue()
    return .t.
  endfunc

  func oCAFLDFIN_1_17.SetRadio()
    this.Parent.oContained.w_CAFLDFIN=trim(this.Parent.oContained.w_CAFLDFIN)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLDFIN=='S',1,;
      0)
  endfunc

  add object oCAFLDFNN_1_18 as StdField with uid="UEPOPFWECR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CAFLDFNN", cQueryName = "CAFLDFNN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 190415244,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=667, Top=67

  func oCAFLDFNN_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLDFIN='S')
    endwith
   endif
  endfunc

  func oCAFLDFNN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAFLDFNN>=0)
    endwith
    return bRes
  endfunc


  add object oObj_1_25 as cp_setobjprop with uid="AKLRRWQFYW",left=7, top=125, width=191,height=19,;
    caption='Tooltip di w_CAFLPRAT',;
   bGlobalFont=.t.,;
    cObj="w_CAFLPRAT",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 163127708


  add object oObj_1_26 as cp_setobjprop with uid="PLPHOODGRJ",left=7, top=146, width=191,height=19,;
    caption='Tooltip di w_CAFLPRAN',;
   bGlobalFont=.t.,;
    cObj="w_CAFLPRAN",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 156836252


  add object oObj_1_27 as cp_setobjprop with uid="GWDPNDGWLT",left=7, top=167, width=191,height=19,;
    caption='Caption di w_CAFLPRAT',;
   bGlobalFont=.t.,;
    cObj="w_CAFLPRAT",cProp="Caption",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 252781196

  add object oStr_1_2 as StdString with uid="OASLJWNNDZ",Visible=.t., Left=13, Top=14,;
    Alignment=0, Width=241, Height=18,;
    Caption="Composizione descrizione attivit� in agenda"  ;
  , bGlobalFont=.t.

  add object oBox_1_1 as StdBox with uid="WYMQDWWGBL",left=3, top=31, width=739,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kcd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
