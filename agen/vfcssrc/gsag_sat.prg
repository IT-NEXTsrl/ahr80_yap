* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_sat                                                        *
*              Stampa attivit�                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-09                                                      *
* Last revis.: 2014-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_sat",oParentObject))

* --- Class definition
define class tgsag_sat as StdForm
  Top    = 4
  Left   = 14

  * --- Standard Properties
  Width  = 825
  Height = 562+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-03"
  HelpContextID=124397417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=143

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  CPUSERS_IDX = 0
  OFF_NOMI_IDX = 0
  CAT_SOGG_IDX = 0
  OFFTIPAT_IDX = 0
  CENCOST_IDX = 0
  GEST_ATTI_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  DES_DIVE_IDX = 0
  TIP_RISE_IDX = 0
  PAR_AGEN_IDX = 0
  CAUMATTI_IDX = 0
  RUO_SOGI_IDX = 0
  cPrg = "gsag_sat"
  cComment = "Stampa attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TipoRisorsa = space(1)
  w_READAZI = space(5)
  o_READAZI = space(5)
  w_NOMPRG = space(10)
  w_FLVISI = space(1)
  w_FLTPART = space(5)
  o_FLTPART = space(5)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_PATIPRIS = space(1)
  o_PATIPRIS = space(1)
  w_PATIPRIS = space(1)
  w_CODPART = space(5)
  o_CODPART = space(5)
  w_STATO = space(1)
  o_STATO = space(1)
  w_RISERVE = space(1)
  w_FLESGR = space(1)
  w_GRUPART = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_CODPRA = space(15)
  w_PRIORITA = 0
  w_CACODICE = space(20)
  w_CADESCRI = space(254)
  w_CODNOM = space(20)
  o_CODNOM = space(20)
  w_CODCAT = space(10)
  w_CodRis = space(5)
  o_CodRis = space(5)
  w_RuolRis = space(5)
  w_ATTSTU = space(1)
  w_ATTGENER = space(1)
  w_UDIENZE = space(1)
  w_SESSTEL = space(1)
  w_ASSENZE = space(1)
  w_DAINSPRE = space(1)
  w_MEMO = space(1)
  w_DAFARE = space(1)
  w_FiltroWeb = space(1)
  o_FiltroWeb = space(1)
  w_DISPONIB = 0
  w_FiltPerson = space(60)
  w_CODTIPOL = space(5)
  w_NOTE = space(0)
  w_ATLOCALI = space(30)
  w_ENTE = space(10)
  w_CODPRA = space(15)
  w_TIPANA = space(1)
  w_CENCOS = space(15)
  w_GIUDICE = space(15)
  w_CODSED = space(5)
  o_CODSED = space(5)
  w_UFFICIO = space(10)
  w_CodUte = 0
  w_ChkProm = space(1)
  o_ChkProm = space(1)
  w_DATA_PROM = ctod('  /  /  ')
  o_DATA_PROM = ctod('  /  /  ')
  w_ORA_PROM = space(2)
  o_ORA_PROM = space(2)
  w_MIN_PROM = space(2)
  o_MIN_PROM = space(2)
  w_TIPORIS = space(1)
  w_SERPIAN = space(20)
  w_Annotazioni = space(1)
  w_Descagg = space(1)
  w_DETTIMP = space(1)
  w_DatiPratica = space(1)
  o_DatiPratica = space(1)
  w_ELCONTR = space(1)
  w_AccodaPreavvisi = space(1)
  w_AccodaNote = space(1)
  w_AccodaDaFare = space(1)
  w_VISUDI = space(1)
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_COD_UTE = space(5)
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_VARLOG = .F.
  w_OBTEST = ctod('  /  /  ')
  w_COMODO = space(30)
  w_CODCLI = space(15)
  w_ATTFUOSTU = space(1)
  w_ATCODSED = space(10)
  w_CACODIMP = space(10)
  w_ATCODNOM = space(15)
  w_TIPNOM = space(1)
  w_DESCAT = space(30)
  w_TIPCAT = space(1)
  w_OFDATDOC = ctod('  /  /  ')
  w_DESCRPART = space(40)
  w_DPTIPRIS = space(1)
  w_FILTPROM = ctot('')
  o_FILTPROM = ctot('')
  w_FiltPubWeb = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_COMPIMPKEY = 0
  o_COMPIMPKEY = 0
  w_COMPIMPKEYREAD = 0
  w_COMPIMP = space(50)
  w_OLDCOMP = space(50)
  w_IMPIANTO = space(10)
  o_IMPIANTO = space(10)
  w_TGIUDICE = space(15)
  w_DATOBSONOM = ctod('  /  /  ')
  w_CAPNOM = space(9)
  w_INDNOM = space(35)
  w_COMODOGIU = space(60)
  w_OB_TEST = ctod('  /  /  ')
  w_TIPOGRUP = space(1)
  w_COCODCON = space(15)
  w_CONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  o_ELCODIMP = space(10)
  w_ELRINNOV = 0
  w_ELTIPCON = space(1)
  o_ELTIPCON = space(1)
  w_COMPIMPR = space(50)
  w_ELCODCOM = 0
  o_ELCODCOM = 0
  w_PATIPRIS = space(1)
  w_PATIPRIS = space(1)
  w_CODPART = space(5)
  w_GRUPART = space(5)
  w_FLESGR = space(1)
  w_DENOM_PART = space(48)
  w_CURPART = space(10)
  w_DENOM_PART = space(48)
  w_DESPRA = space(75)
  w_DesEnte = space(60)
  w_DESUTE = space(20)
  w_DESNOM = space(40)
  w_DESTIPOL = space(50)
  w_DESCEN = space(40)
  w_DESCRI = space(40)
  w_DesUffi = space(60)
  w_DESNOMGIU = space(30)
  w_DESCRI = space(60)
  w_DESPRA = space(75)
  w_STATO = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_DATFIN_Cose = ctod('  /  /  ')
  o_DATFIN_Cose = ctod('  /  /  ')
  w_DATINI_Cose = ctod('  /  /  ')
  o_DATINI_Cose = ctod('  /  /  ')
  w_OREINI_Cose = space(2)
  o_OREINI_Cose = space(2)
  w_MININI_Cose = space(2)
  o_MININI_Cose = space(2)
  w_OREFIN_Cose = space(2)
  o_OREFIN_Cose = space(2)
  w_MINFIN_Cose = space(2)
  o_MINFIN_Cose = space(2)
  w_DATA_INI_Cose = ctot('')
  w_DATA_FIN_Cose = ctot('')
  w_STADETTDOC = space(1)
  w_IMDESCRI = space(50)
  w_COGN_CODRIS = space(40)
  w_NOME_CODRIS = space(40)
  w_DESRIS = space(80)
  w_DATINIZ = ctod('  /  /  ')
  o_DATINIZ = ctod('  /  /  ')
  w_obsodat1 = space(1)
  o_obsodat1 = space(1)
  w_PART_ZOOM = .NULL.
  w_OUT = .NULL.
  w_OUT1 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_sat
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_satPag1","gsag_sat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsag_satPag2","gsag_sat",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Elenco partecipanti")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_13
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_PART_ZOOM = this.oPgFrm.Pages(2).oPag.PART_ZOOM
    this.w_OUT = this.oPgFrm.Pages(1).oPag.OUT
    this.w_OUT1 = this.oPgFrm.Pages(1).oPag.OUT1
    DoDefault()
    proc Destroy()
      this.w_PART_ZOOM = .NULL.
      this.w_OUT = .NULL.
      this.w_OUT1 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[17]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='PRA_ENTI'
    this.cWorkTables[4]='PRA_UFFI'
    this.cWorkTables[5]='CPUSERS'
    this.cWorkTables[6]='OFF_NOMI'
    this.cWorkTables[7]='CAT_SOGG'
    this.cWorkTables[8]='OFFTIPAT'
    this.cWorkTables[9]='CENCOST'
    this.cWorkTables[10]='GEST_ATTI'
    this.cWorkTables[11]='IMP_MAST'
    this.cWorkTables[12]='IMP_DETT'
    this.cWorkTables[13]='DES_DIVE'
    this.cWorkTables[14]='TIP_RISE'
    this.cWorkTables[15]='PAR_AGEN'
    this.cWorkTables[16]='CAUMATTI'
    this.cWorkTables[17]='RUO_SOGI'
    return(this.OpenAllTables(17))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TipoRisorsa=space(1)
      .w_READAZI=space(5)
      .w_NOMPRG=space(10)
      .w_FLVISI=space(1)
      .w_FLTPART=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_PATIPRIS=space(1)
      .w_PATIPRIS=space(1)
      .w_CODPART=space(5)
      .w_STATO=space(1)
      .w_RISERVE=space(1)
      .w_FLESGR=space(1)
      .w_GRUPART=space(5)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_CODPRA=space(15)
      .w_PRIORITA=0
      .w_CACODICE=space(20)
      .w_CADESCRI=space(254)
      .w_CODNOM=space(20)
      .w_CODCAT=space(10)
      .w_CodRis=space(5)
      .w_RuolRis=space(5)
      .w_ATTSTU=space(1)
      .w_ATTGENER=space(1)
      .w_UDIENZE=space(1)
      .w_SESSTEL=space(1)
      .w_ASSENZE=space(1)
      .w_DAINSPRE=space(1)
      .w_MEMO=space(1)
      .w_DAFARE=space(1)
      .w_FiltroWeb=space(1)
      .w_DISPONIB=0
      .w_FiltPerson=space(60)
      .w_CODTIPOL=space(5)
      .w_NOTE=space(0)
      .w_ATLOCALI=space(30)
      .w_ENTE=space(10)
      .w_CODPRA=space(15)
      .w_TIPANA=space(1)
      .w_CENCOS=space(15)
      .w_GIUDICE=space(15)
      .w_CODSED=space(5)
      .w_UFFICIO=space(10)
      .w_CodUte=0
      .w_ChkProm=space(1)
      .w_DATA_PROM=ctod("  /  /  ")
      .w_ORA_PROM=space(2)
      .w_MIN_PROM=space(2)
      .w_TIPORIS=space(1)
      .w_SERPIAN=space(20)
      .w_Annotazioni=space(1)
      .w_Descagg=space(1)
      .w_DETTIMP=space(1)
      .w_DatiPratica=space(1)
      .w_ELCONTR=space(1)
      .w_AccodaPreavvisi=space(1)
      .w_AccodaNote=space(1)
      .w_AccodaDaFare=space(1)
      .w_VISUDI=space(1)
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_COD_UTE=space(5)
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_VARLOG=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_COMODO=space(30)
      .w_CODCLI=space(15)
      .w_ATTFUOSTU=space(1)
      .w_ATCODSED=space(10)
      .w_CACODIMP=space(10)
      .w_ATCODNOM=space(15)
      .w_TIPNOM=space(1)
      .w_DESCAT=space(30)
      .w_TIPCAT=space(1)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_DESCRPART=space(40)
      .w_DPTIPRIS=space(1)
      .w_FILTPROM=ctot("")
      .w_FiltPubWeb=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_COMPIMPKEY=0
      .w_COMPIMPKEYREAD=0
      .w_COMPIMP=space(50)
      .w_OLDCOMP=space(50)
      .w_IMPIANTO=space(10)
      .w_TGIUDICE=space(15)
      .w_DATOBSONOM=ctod("  /  /  ")
      .w_CAPNOM=space(9)
      .w_INDNOM=space(35)
      .w_COMODOGIU=space(60)
      .w_OB_TEST=ctod("  /  /  ")
      .w_TIPOGRUP=space(1)
      .w_COCODCON=space(15)
      .w_CONTRA=space(10)
      .w_ELCODMOD=space(10)
      .w_ELCODIMP=space(10)
      .w_ELRINNOV=0
      .w_ELTIPCON=space(1)
      .w_COMPIMPR=space(50)
      .w_ELCODCOM=0
      .w_PATIPRIS=space(1)
      .w_PATIPRIS=space(1)
      .w_CODPART=space(5)
      .w_GRUPART=space(5)
      .w_FLESGR=space(1)
      .w_DENOM_PART=space(48)
      .w_CURPART=space(10)
      .w_DENOM_PART=space(48)
      .w_DESPRA=space(75)
      .w_DesEnte=space(60)
      .w_DESUTE=space(20)
      .w_DESNOM=space(40)
      .w_DESTIPOL=space(50)
      .w_DESCEN=space(40)
      .w_DESCRI=space(40)
      .w_DesUffi=space(60)
      .w_DESNOMGIU=space(30)
      .w_DESCRI=space(60)
      .w_DESPRA=space(75)
      .w_STATO=space(1)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_DATFIN_Cose=ctod("  /  /  ")
      .w_DATINI_Cose=ctod("  /  /  ")
      .w_OREINI_Cose=space(2)
      .w_MININI_Cose=space(2)
      .w_OREFIN_Cose=space(2)
      .w_MINFIN_Cose=space(2)
      .w_DATA_INI_Cose=ctot("")
      .w_DATA_FIN_Cose=ctot("")
      .w_STADETTDOC=space(1)
      .w_IMDESCRI=space(50)
      .w_COGN_CODRIS=space(40)
      .w_NOME_CODRIS=space(40)
      .w_DESRIS=space(80)
      .w_DATINIZ=ctod("  /  /  ")
      .w_obsodat1=space(1)
        .w_TipoRisorsa = 'P'
        .w_READAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READAZI))
          .link_1_3('Full')
        endif
        .w_NOMPRG = iif(!isalt(),'GSAG1SAT','GSAG_SAT')
          .DoRTCalc(4,4,.f.)
        .w_FLTPART = readdipend( i_CodUte, 'C')
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
          .DoRTCalc(6,7,.f.)
        .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        .w_PATIPRIS = ''
        .w_PATIPRIS = ''
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODPART))
          .link_1_22('Full')
        endif
        .w_STATO = IIF(NOT ISALT(), 'N', InitStato())
        .w_RISERVE = 'L'
        .w_FLESGR = 'N'
        .w_GRUPART = Space(5)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_GRUPART))
          .link_1_27('Full')
        endif
        .DoRTCalc(19,21,.f.)
        if not(empty(.w_CODPRA))
          .link_1_30('Full')
        endif
        .w_PRIORITA = 0
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_CACODICE))
          .link_1_32('Full')
        endif
        .DoRTCalc(24,25,.f.)
        if not(empty(.w_CODNOM))
          .link_1_34('Full')
        endif
        .w_CODCAT = ''
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CODCAT))
          .link_1_35('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CodRis))
          .link_1_36('Full')
        endif
        .w_RuolRis = ''
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_RuolRis))
          .link_1_37('Full')
        endif
          .DoRTCalc(29,36,.f.)
        .w_FiltroWeb = 'T'
        .w_DISPONIB = 0
        .DoRTCalc(39,40,.f.)
        if not(empty(.w_CODTIPOL))
          .link_1_49('Full')
        endif
        .DoRTCalc(41,43,.f.)
        if not(empty(.w_ENTE))
          .link_1_52('Full')
        endif
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_CODPRA))
          .link_1_53('Full')
        endif
        .w_TIPANA = 'C'
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_CENCOS))
          .link_1_55('Full')
        endif
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_GIUDICE))
          .link_1_56('Full')
        endif
        .w_CODSED = SPACE(5)
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_CODSED))
          .link_1_57('Full')
        endif
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_UFFICIO))
          .link_1_58('Full')
        endif
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_CodUte))
          .link_1_59('Full')
        endif
        .w_ChkProm = ' '
          .DoRTCalc(52,52,.f.)
        .w_ORA_PROM = PADL(ALLTRIM(STR(VAL(.w_ORA_PROM))),2,'0')
        .w_MIN_PROM = PADL(ALLTRIM(STR(VAL(.w_MIN_PROM))),2,'0')
        .DoRTCalc(55,56,.f.)
        if not(empty(.w_SERPIAN))
          .link_1_65('Full')
        endif
        .w_Annotazioni = 'S'
        .w_Descagg = 'N'
          .DoRTCalc(59,59,.f.)
        .w_DatiPratica = 'S'
          .DoRTCalc(61,61,.f.)
        .w_AccodaPreavvisi = 'N'
        .w_AccodaNote = 'N'
        .w_AccodaDaFare = 'N'
        .w_VISUDI = 'N'
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
          .DoRTCalc(69,71,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(73,74,.f.)
        .w_ATTFUOSTU = ' '
        .w_ATCODSED = .w_CODSED
        .w_CACODIMP = .w_IMPIANTO
        .w_ATCODNOM = .w_CODNOM
          .DoRTCalc(79,81,.f.)
        .w_OFDATDOC = i_datsys
          .DoRTCalc(83,84,.f.)
        .w_FILTPROM = IIF(.w_ChkProm='S' AND !EMPTY(.w_DATA_PROM),cp_CharToDatetime(ALLTR(STR(DAY(.w_DATA_PROM)))+'-'+ALLTR(STR(MONTH(.w_DATA_PROM)))+'-'+ALLTR(STR(YEAR(.w_DATA_PROM)))+' '+.w_ORA_PROM+':'+.w_MIN_PROM+':00', 'dd-mm-yyyy hh:nn:ss'),DTOT(CTOD('  -  -  ')))
        .w_FiltPubWeb = IIF(.w_FiltroWeb='T','',.w_FiltroWeb)
          .DoRTCalc(87,87,.f.)
        .w_COMPIMPKEY = 0
        .w_COMPIMPKEYREAD = .w_COMPIMPKEY
        .DoRTCalc(89,89,.f.)
        if not(empty(.w_COMPIMPKEYREAD))
          .link_1_101('Full')
        endif
          .DoRTCalc(90,91,.f.)
        .w_IMPIANTO = IIF(EMPTY(.w_CODSED), space(10), .w_IMPIANTO)
        .DoRTCalc(92,92,.f.)
        if not(empty(.w_IMPIANTO))
          .link_1_104('Full')
        endif
          .DoRTCalc(93,97,.f.)
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(99,104,.f.)
        .w_ELTIPCON = 'T'
          .DoRTCalc(106,107,.f.)
        .w_PATIPRIS = ''
        .w_PATIPRIS = ''
        .DoRTCalc(110,110,.f.)
        if not(empty(.w_CODPART))
          .link_2_3('Full')
        endif
        .w_GRUPART = Space(5)
        .DoRTCalc(111,111,.f.)
        if not(empty(.w_GRUPART))
          .link_2_4('Full')
        endif
        .w_FLESGR = 'N'
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
      .oPgFrm.Page2.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
        .w_CURPART = .w_PART_ZOOM.cCursor
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
      .oPgFrm.Page1.oPag.OUT.Calculate()
      .oPgFrm.Page1.oPag.OUT1.Calculate()
          .DoRTCalc(116,126,.f.)
        .w_STATO = InitStato()
        .w_DTBSO_CAUMATTI = i_DatSys
          .DoRTCalc(129,130,.f.)
        .w_OREINI_Cose = RIGHT('00'+IIF(EMPTY(.w_OREINI_Cose),'00',.w_OREINI_Cose),2)
        .w_MININI_Cose = RIGHT('00'+IIF(empty(.w_MININI_Cose),'00',.w_MININI_Cose),2)
        .w_OREFIN_Cose = RIGHT('00'+IIF(empty(.w_OREFIN_Cose) OR .w_OREFIN_Cose="00", '23', .w_OREFIN_Cose),2)
        .w_MINFIN_Cose = RIGHT('00'+IIF(empty(.w_MINFIN_Cose) OR (empty(.w_DATFIN_Cose) AND .w_OREFIN_Cose="23" AND .w_MINFIN_Cose="00"), '59', .w_MINFIN_Cose),2)
        .w_DATA_INI_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI_Cose),  ALLTR(STR(DAY(.w_DATINI_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATINI_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATINI_Cose)))+' '+.w_OREINI_Cose+':'+.w_MININI_Cose+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN_Cose),  ALLTR(STR(DAY(.w_DATFIN_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATFIN_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATFIN_Cose)))+' '+.w_OREFIN_Cose+':'+.w_MINFIN_Cose+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        .w_STADETTDOC = IIF(.w_STATO='P','S','N')
          .DoRTCalc(138,140,.f.)
        .w_DESRIS = ALLTRIM(.w_COGN_CODRIS)+' '+ALLTRIM(.w_NOME_CODRIS)
        .w_DATINIZ = i_datsys
        .w_obsodat1 = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_142.enabled = this.oPgFrm.Page1.oPag.oBtn_1_142.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_143.enabled = this.oPgFrm.Page1.oPag.oBtn_1_143.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_sat
    this.w_OUT.visible=iif(IsAlt(),.T.,.F.)
    this.w_OUT1.visible=iif(IsAlt(),.F.,.T.)
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_READAZI<>.w_READAZI
            .w_READAZI = i_CODAZI
          .link_1_3('Full')
        endif
        .DoRTCalc(3,4,.t.)
        if .o_FLTPART<>.w_FLTPART
            .w_FLTPART = readdipend( i_CodUte, 'C')
        endif
        if .o_OREINI<>.w_OREINI
          .Calculate_MVFLEXPGGM()
        endif
        if .o_MININI<>.w_MININI
          .Calculate_SUGRFFLBCH()
        endif
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        if .o_OREFIN<>.w_OREFIN
          .Calculate_FPJEAUJXWE()
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_KTQUMLVLEF()
        endif
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .DoRTCalc(6,7,.t.)
        if .o_OREINI<>.w_OREINI
            .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        endif
        if .o_MININI<>.w_MININI
            .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        endif
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN
            .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN
            .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        endif
        .DoRTCalc(12,13,.t.)
        if .o_PATIPRIS<>.w_PATIPRIS
          .link_1_22('Full')
        endif
        .DoRTCalc(15,16,.t.)
        if .o_CODPART<>.w_CODPART
            .w_FLESGR = 'N'
        endif
        if .o_CODPART<>.w_CODPART
            .w_GRUPART = Space(5)
          .link_1_27('Full')
        endif
        .DoRTCalc(19,25,.t.)
        if .o_CODNOM<>.w_CODNOM
            .w_CODCAT = ''
          .link_1_35('Full')
        endif
        .DoRTCalc(27,27,.t.)
        if .o_CodRis<>.w_CodRis
            .w_RuolRis = ''
          .link_1_37('Full')
        endif
        .DoRTCalc(29,47,.t.)
        if .o_CODNOM<>.w_CODNOM
            .w_CODSED = SPACE(5)
          .link_1_57('Full')
        endif
        .DoRTCalc(49,52,.t.)
        if .o_ORA_PROM<>.w_ORA_PROM
            .w_ORA_PROM = PADL(ALLTRIM(STR(VAL(.w_ORA_PROM))),2,'0')
        endif
        if .o_MIN_PROM<>.w_MIN_PROM
            .w_MIN_PROM = PADL(ALLTRIM(STR(VAL(.w_MIN_PROM))),2,'0')
        endif
        .DoRTCalc(55,65,.t.)
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_CODPART<>.w_CODPART
            .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
        endif
        .DoRTCalc(69,75,.t.)
            .w_ATCODSED = .w_CODSED
            .w_CACODIMP = .w_IMPIANTO
            .w_ATCODNOM = .w_CODNOM
        if .o_ChkProm<>.w_ChkProm
          .Calculate_WWJSDCQGIA()
        endif
        .DoRTCalc(79,84,.t.)
        if .o_DATA_PROM<>.w_DATA_PROM.or. .o_MIN_PROM<>.w_MIN_PROM.or. .o_ORA_PROM<>.w_ORA_PROM.or. .o_FILTPROM<>.w_FILTPROM.or. .o_ChkProm<>.w_ChkProm
            .w_FILTPROM = IIF(.w_ChkProm='S' AND !EMPTY(.w_DATA_PROM),cp_CharToDatetime(ALLTR(STR(DAY(.w_DATA_PROM)))+'-'+ALLTR(STR(MONTH(.w_DATA_PROM)))+'-'+ALLTR(STR(YEAR(.w_DATA_PROM)))+' '+.w_ORA_PROM+':'+.w_MIN_PROM+':00', 'dd-mm-yyyy hh:nn:ss'),DTOT(CTOD('  -  -  ')))
        endif
        if .o_FiltroWeb<>.w_FiltroWeb
            .w_FiltPubWeb = IIF(.w_FiltroWeb='T','',.w_FiltroWeb)
        endif
        .DoRTCalc(87,87,.t.)
        if .o_IMPIANTO<>.w_IMPIANTO
            .w_COMPIMPKEY = 0
        endif
        if .o_IMPIANTO<>.w_IMPIANTO.or. .o_COMPIMPKEY<>.w_COMPIMPKEY
            .w_COMPIMPKEYREAD = .w_COMPIMPKEY
          .link_1_101('Full')
        endif
        .DoRTCalc(90,91,.t.)
        if .o_CODSED<>.w_CODSED
            .w_IMPIANTO = IIF(EMPTY(.w_CODSED), space(10), .w_IMPIANTO)
          .link_1_104('Full')
        endif
        .DoRTCalc(93,109,.t.)
        if .o_PATIPRIS<>.w_PATIPRIS
          .link_2_3('Full')
        endif
        if .o_CODPART<>.w_CODPART
            .w_GRUPART = Space(5)
          .link_2_4('Full')
        endif
        if .o_CODPART<>.w_CODPART
            .w_FLESGR = 'N'
        endif
        if .o_CODPART<>.w_CODPART
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        .oPgFrm.Page2.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
        if .o_DATINIZ<>.w_DATINIZ.or. .o_obsodat1<>.w_obsodat1
          .Calculate_JQCKGKZTVV()
        endif
        if .o_PATIPRIS<>.w_PATIPRIS.or. .o_DATINIZ<>.w_DATINIZ.or. .o_obsodat1<>.w_obsodat1
          .Calculate_BLTFQIRAHO()
        endif
        if .o_CODPART<>.w_CODPART.or. .o_DATINIZ<>.w_DATINIZ.or. .o_obsodat1<>.w_obsodat1
          .Calculate_FWWDAFBQBE()
        endif
        if .o_DatiPratica<>.w_DatiPratica
          .Calculate_DIDIGCQSMK()
        endif
        .DoRTCalc(114,114,.t.)
        if .o_CODPART<>.w_CODPART
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        .oPgFrm.Page1.oPag.OUT.Calculate()
        .oPgFrm.Page1.oPag.OUT1.Calculate()
        .DoRTCalc(116,130,.t.)
        if .o_OREINI_Cose<>.w_OREINI_Cose
            .w_OREINI_Cose = RIGHT('00'+IIF(EMPTY(.w_OREINI_Cose),'00',.w_OREINI_Cose),2)
        endif
        if .o_MININI_Cose<>.w_MININI_Cose
            .w_MININI_Cose = RIGHT('00'+IIF(empty(.w_MININI_Cose),'00',.w_MININI_Cose),2)
        endif
        if .o_OREFIN_Cose<>.w_OREFIN_Cose.or. .o_DATFIN_Cose<>.w_DATFIN_Cose
            .w_OREFIN_Cose = RIGHT('00'+IIF(empty(.w_OREFIN_Cose) OR .w_OREFIN_Cose="00", '23', .w_OREFIN_Cose),2)
        endif
        if .o_MINFIN_Cose<>.w_MINFIN_Cose.or. .o_DATFIN_Cose<>.w_DATFIN_Cose
            .w_MINFIN_Cose = RIGHT('00'+IIF(empty(.w_MINFIN_Cose) OR (empty(.w_DATFIN_Cose) AND .w_OREFIN_Cose="23" AND .w_MINFIN_Cose="00"), '59', .w_MINFIN_Cose),2)
        endif
        if .o_DATINI_Cose<>.w_DATINI_Cose
          .Calculate_HWTLQYNXHT()
        endif
        if .o_DATFIN_Cose<>.w_DATFIN_Cose
          .Calculate_JBMQEMONXV()
        endif
        if .o_DATINI_Cose<>.w_DATINI_Cose.or. .o_OREINI_Cose<>.w_OREINI_Cose.or. .o_MININI_Cose<>.w_MININI_Cose
            .w_DATA_INI_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI_Cose),  ALLTR(STR(DAY(.w_DATINI_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATINI_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATINI_Cose)))+' '+.w_OREINI_Cose+':'+.w_MININI_Cose+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN_Cose<>.w_DATFIN_Cose.or. .o_OREFIN_Cose<>.w_OREFIN_Cose.or. .o_MINFIN_Cose<>.w_MINFIN_Cose
            .w_DATA_FIN_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN_Cose),  ALLTR(STR(DAY(.w_DATFIN_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATFIN_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATFIN_Cose)))+' '+.w_OREFIN_Cose+':'+.w_MINFIN_Cose+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_STATO<>.w_STATO
            .w_STADETTDOC = IIF(.w_STATO='P','S','N')
        endif
        .DoRTCalc(138,140,.t.)
        if .o_CodRis<>.w_CodRis
            .w_DESRIS = ALLTRIM(.w_COGN_CODRIS)+' '+ALLTRIM(.w_NOME_CODRIS)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(142,143,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page2.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
        .oPgFrm.Page1.oPag.OUT.Calculate()
        .oPgFrm.Page1.oPag.OUT1.Calculate()
    endwith
  return

  proc Calculate_NRTSRHMIVM()
    with this
          * --- Inizializza date
          gsag_ba3(this;
              ,'AGEN';
             )
    endwith
  endproc
  proc Calculate_MVFLEXPGGM()
    with this
          * --- Resetto w_oreini
          .w_OREINI = .ZeroFill(.w_OREINI)
    endwith
  endproc
  proc Calculate_SUGRFFLBCH()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_FPJEAUJXWE()
    with this
          * --- Resetto w_orefin
          .w_OREFIN = .ZeroFill(.w_OREFIN)
    endwith
  endproc
  proc Calculate_KTQUMLVLEF()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_LYBISUKBKM()
    with this
          * --- Imposta data odierna
          .w_DATINI = i_DatSys
          .w_DATFIN = i_DatSys
          .w_OREINI = '00'
          .w_MININI = '00'
          .w_OREFIN = '23'
          .w_MINFIN = '59'
          .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'))
          .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'))
    endwith
  endproc
  proc Calculate_WWJSDCQGIA()
    with this
          * --- Imposta data/ora filtro promemoria
          .w_FILTPROM = IIF(.w_ChkProm='S',DATETIME(),DTOT(CTOD('  -  -  ')))
          .w_DATA_PROM = IIF(.w_ChkProm='S',TTOD(.w_FILTPROM),CTOD('  -  -  '))
          .w_ORA_PROM = IIF(.w_ChkProm='S',PADL(ALLTRIM(STR(HOUR(.w_FILTPROM))),2,'0'),'00')
          .w_MIN_PROM = IIF(.w_ChkProm='S',PADL(ALLTRIM(STR(MINUTE(.w_FILTPROM))),2,'0'),'00')
    endwith
  endproc
  proc Calculate_JQCKGKZTVV()
    with this
          * --- Azzera w_CODPART
          .w_CODPART = space(5)
          .link_1_22('Full')
          .link_2_3('Full')
          .w_DENOM_PART = space(48)
    endwith
  endproc
  proc Calculate_BLTFQIRAHO()
    with this
          * --- Attiva zoom selezionando le righe
          GSAG_BPP(this;
              ,'ATTIVAZOOM';
              ,.w_PART_ZOOM;
              ,.w_CODPART;
              ,this;
             )
    endwith
  endproc
  proc Calculate_FWWDAFBQBE()
    with this
          * --- Seleziona righe
          GSAG_BPP(this;
              ,'SELRIGA';
              ,.w_PART_ZOOM;
              ,.w_CODPART;
              ,this;
             )
    endwith
  endproc
  proc Calculate_DIDIGCQSMK()
    with this
          * --- Azzera w_VISUDI
          .w_VISUDI = IIF(.w_DatiPratica<>'S', 'N', .w_VISUDI)
    endwith
  endproc
  proc Calculate_HWTLQYNXHT()
    with this
          * --- Azzera ore- minuti inizio
          .w_OREINI_Cose = IIF(EMPTY(.w_DATINI_Cose),'00',.w_OREINI_Cose)
          .w_MININI_Cose = IIF(EMPTY(.w_DATINI_Cose),'00',.w_MININI_Cose)
    endwith
  endproc
  proc Calculate_JBMQEMONXV()
    with this
          * --- Azzera ore- minuti fine
          .w_OREFIN_Cose = IIF(EMPTY(.w_DATFIN_Cose),'00',.w_OREFIN_Cose)
          .w_MINFIN_Cose = IIF(EMPTY(.w_DATFIN_Cose),'00',.w_MINFIN_Cose)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOREINI_1_15.enabled = this.oPgFrm.Page1.oPag.oOREINI_1_15.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_16.enabled = this.oPgFrm.Page1.oPag.oMININI_1_16.mCond()
    this.oPgFrm.Page1.oPag.oOREFIN_1_17.enabled = this.oPgFrm.Page1.oPag.oOREFIN_1_17.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_18.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_18.mCond()
    this.oPgFrm.Page1.oPag.oPATIPRIS_1_20.enabled = this.oPgFrm.Page1.oPag.oPATIPRIS_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPATIPRIS_1_21.enabled = this.oPgFrm.Page1.oPag.oPATIPRIS_1_21.mCond()
    this.oPgFrm.Page1.oPag.oGRUPART_1_27.enabled = this.oPgFrm.Page1.oPag.oGRUPART_1_27.mCond()
    this.oPgFrm.Page1.oPag.oCODNOM_1_34.enabled = this.oPgFrm.Page1.oPag.oCODNOM_1_34.mCond()
    this.oPgFrm.Page1.oPag.oCODCAT_1_35.enabled = this.oPgFrm.Page1.oPag.oCODCAT_1_35.mCond()
    this.oPgFrm.Page1.oPag.oRuolRis_1_37.enabled = this.oPgFrm.Page1.oPag.oRuolRis_1_37.mCond()
    this.oPgFrm.Page1.oPag.oGIUDICE_1_56.enabled = this.oPgFrm.Page1.oPag.oGIUDICE_1_56.mCond()
    this.oPgFrm.Page1.oPag.oCODSED_1_57.enabled = this.oPgFrm.Page1.oPag.oCODSED_1_57.mCond()
    this.oPgFrm.Page1.oPag.oDATA_PROM_1_61.enabled = this.oPgFrm.Page1.oPag.oDATA_PROM_1_61.mCond()
    this.oPgFrm.Page1.oPag.oORA_PROM_1_62.enabled = this.oPgFrm.Page1.oPag.oORA_PROM_1_62.mCond()
    this.oPgFrm.Page1.oPag.oMIN_PROM_1_63.enabled = this.oPgFrm.Page1.oPag.oMIN_PROM_1_63.mCond()
    this.oPgFrm.Page1.oPag.oAccodaNote_1_72.enabled = this.oPgFrm.Page1.oPag.oAccodaNote_1_72.mCond()
    this.oPgFrm.Page1.oPag.oAccodaDaFare_1_73.enabled = this.oPgFrm.Page1.oPag.oAccodaDaFare_1_73.mCond()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_102.enabled = this.oPgFrm.Page1.oPag.oCOMPIMP_1_102.mCond()
    this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.enabled = this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.mCond()
    this.oPgFrm.Page2.oPag.oPATIPRIS_2_2.enabled = this.oPgFrm.Page2.oPag.oPATIPRIS_2_2.mCond()
    this.oPgFrm.Page2.oPag.oGRUPART_2_4.enabled = this.oPgFrm.Page2.oPag.oGRUPART_2_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_142.enabled = this.oPgFrm.Page1.oPag.oBtn_1_142.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPATIPRIS_1_20.visible=!this.oPgFrm.Page1.oPag.oPATIPRIS_1_20.mHide()
    this.oPgFrm.Page1.oPag.oPATIPRIS_1_21.visible=!this.oPgFrm.Page1.oPag.oPATIPRIS_1_21.mHide()
    this.oPgFrm.Page1.oPag.oSTATO_1_24.visible=!this.oPgFrm.Page1.oPag.oSTATO_1_24.mHide()
    this.oPgFrm.Page1.oPag.oRISERVE_1_25.visible=!this.oPgFrm.Page1.oPag.oRISERVE_1_25.mHide()
    this.oPgFrm.Page1.oPag.oFLESGR_1_26.visible=!this.oPgFrm.Page1.oPag.oFLESGR_1_26.mHide()
    this.oPgFrm.Page1.oPag.oGRUPART_1_27.visible=!this.oPgFrm.Page1.oPag.oGRUPART_1_27.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_28.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_1_28.mHide()
    this.oPgFrm.Page1.oPag.oALFDOC_1_29.visible=!this.oPgFrm.Page1.oPag.oALFDOC_1_29.mHide()
    this.oPgFrm.Page1.oPag.oCODPRA_1_30.visible=!this.oPgFrm.Page1.oPag.oCODPRA_1_30.mHide()
    this.oPgFrm.Page1.oPag.oCODCAT_1_35.visible=!this.oPgFrm.Page1.oPag.oCODCAT_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCodRis_1_36.visible=!this.oPgFrm.Page1.oPag.oCodRis_1_36.mHide()
    this.oPgFrm.Page1.oPag.oRuolRis_1_37.visible=!this.oPgFrm.Page1.oPag.oRuolRis_1_37.mHide()
    this.oPgFrm.Page1.oPag.oUDIENZE_1_40.visible=!this.oPgFrm.Page1.oPag.oUDIENZE_1_40.mHide()
    this.oPgFrm.Page1.oPag.oFiltroWeb_1_46.visible=!this.oPgFrm.Page1.oPag.oFiltroWeb_1_46.mHide()
    this.oPgFrm.Page1.oPag.oENTE_1_52.visible=!this.oPgFrm.Page1.oPag.oENTE_1_52.mHide()
    this.oPgFrm.Page1.oPag.oCODPRA_1_53.visible=!this.oPgFrm.Page1.oPag.oCODPRA_1_53.mHide()
    this.oPgFrm.Page1.oPag.oTIPANA_1_54.visible=!this.oPgFrm.Page1.oPag.oTIPANA_1_54.mHide()
    this.oPgFrm.Page1.oPag.oGIUDICE_1_56.visible=!this.oPgFrm.Page1.oPag.oGIUDICE_1_56.mHide()
    this.oPgFrm.Page1.oPag.oCODSED_1_57.visible=!this.oPgFrm.Page1.oPag.oCODSED_1_57.mHide()
    this.oPgFrm.Page1.oPag.oUFFICIO_1_58.visible=!this.oPgFrm.Page1.oPag.oUFFICIO_1_58.mHide()
    this.oPgFrm.Page1.oPag.oTIPORIS_1_64.visible=!this.oPgFrm.Page1.oPag.oTIPORIS_1_64.mHide()
    this.oPgFrm.Page1.oPag.oSERPIAN_1_65.visible=!this.oPgFrm.Page1.oPag.oSERPIAN_1_65.mHide()
    this.oPgFrm.Page1.oPag.oDETTIMP_1_68.visible=!this.oPgFrm.Page1.oPag.oDETTIMP_1_68.mHide()
    this.oPgFrm.Page1.oPag.oDatiPratica_1_69.visible=!this.oPgFrm.Page1.oPag.oDatiPratica_1_69.mHide()
    this.oPgFrm.Page1.oPag.oELCONTR_1_70.visible=!this.oPgFrm.Page1.oPag.oELCONTR_1_70.mHide()
    this.oPgFrm.Page1.oPag.oAccodaNote_1_72.visible=!this.oPgFrm.Page1.oPag.oAccodaNote_1_72.mHide()
    this.oPgFrm.Page1.oPag.oAccodaDaFare_1_73.visible=!this.oPgFrm.Page1.oPag.oAccodaDaFare_1_73.mHide()
    this.oPgFrm.Page1.oPag.oVISUDI_1_74.visible=!this.oPgFrm.Page1.oPag.oVISUDI_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_100.visible=!this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_100.mHide()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_102.visible=!this.oPgFrm.Page1.oPag.oCOMPIMP_1_102.mHide()
    this.oPgFrm.Page1.oPag.oIMPIANTO_1_104.visible=!this.oPgFrm.Page1.oPag.oIMPIANTO_1_104.mHide()
    this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.visible=!this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.mHide()
    this.oPgFrm.Page2.oPag.oPATIPRIS_2_2.visible=!this.oPgFrm.Page2.oPag.oPATIPRIS_2_2.mHide()
    this.oPgFrm.Page2.oPag.oGRUPART_2_4.visible=!this.oPgFrm.Page2.oPag.oGRUPART_2_4.mHide()
    this.oPgFrm.Page2.oPag.oFLESGR_2_5.visible=!this.oPgFrm.Page2.oPag.oFLESGR_2_5.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_8.visible=!this.oPgFrm.Page2.oPag.oStr_2_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_130.visible=!this.oPgFrm.Page1.oPag.oStr_1_130.mHide()
    this.oPgFrm.Page1.oPag.oDESPRA_1_131.visible=!this.oPgFrm.Page1.oPag.oDESPRA_1_131.mHide()
    this.oPgFrm.Page1.oPag.oDesEnte_1_136.visible=!this.oPgFrm.Page1.oPag.oDesEnte_1_136.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_137.visible=!this.oPgFrm.Page1.oPag.oStr_1_137.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_141.visible=!this.oPgFrm.Page1.oPag.oStr_1_141.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_151.visible=!this.oPgFrm.Page1.oPag.oStr_1_151.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_156.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_156.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_157.visible=!this.oPgFrm.Page1.oPag.oStr_1_157.mHide()
    this.oPgFrm.Page1.oPag.oDesUffi_1_158.visible=!this.oPgFrm.Page1.oPag.oDesUffi_1_158.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_159.visible=!this.oPgFrm.Page1.oPag.oStr_1_159.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_160.visible=!this.oPgFrm.Page1.oPag.oStr_1_160.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_161.visible=!this.oPgFrm.Page1.oPag.oStr_1_161.mHide()
    this.oPgFrm.Page1.oPag.oDESNOMGIU_1_162.visible=!this.oPgFrm.Page1.oPag.oDESNOMGIU_1_162.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_163.visible=!this.oPgFrm.Page1.oPag.oStr_1_163.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_164.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_164.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_165.visible=!this.oPgFrm.Page1.oPag.oStr_1_165.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_166.visible=!this.oPgFrm.Page1.oPag.oStr_1_166.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_167.visible=!this.oPgFrm.Page1.oPag.oStr_1_167.mHide()
    this.oPgFrm.Page1.oPag.oDESPRA_1_170.visible=!this.oPgFrm.Page1.oPag.oDESPRA_1_170.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_171.visible=!this.oPgFrm.Page1.oPag.oStr_1_171.mHide()
    this.oPgFrm.Page1.oPag.oSTATO_1_172.visible=!this.oPgFrm.Page1.oPag.oSTATO_1_172.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_174.visible=!this.oPgFrm.Page1.oPag.oStr_1_174.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_175.visible=!this.oPgFrm.Page1.oPag.oStr_1_175.mHide()
    this.oPgFrm.Page1.oPag.oDATFIN_Cose_1_176.visible=!this.oPgFrm.Page1.oPag.oDATFIN_Cose_1_176.mHide()
    this.oPgFrm.Page1.oPag.oDATINI_Cose_1_177.visible=!this.oPgFrm.Page1.oPag.oDATINI_Cose_1_177.mHide()
    this.oPgFrm.Page1.oPag.oSTADETTDOC_1_186.visible=!this.oPgFrm.Page1.oPag.oSTADETTDOC_1_186.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_187.visible=!this.oPgFrm.Page1.oPag.oBtn_1_187.mHide()
    this.oPgFrm.Page1.oPag.oDESRIS_1_191.visible=!this.oPgFrm.Page1.oPag.oDESRIS_1_191.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_192.visible=!this.oPgFrm.Page1.oPag.oStr_1_192.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_193.visible=!this.oPgFrm.Page1.oPag.oStr_1_193.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_194.visible=!this.oPgFrm.Page1.oPag.oStr_1_194.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_195.visible=!this.oPgFrm.Page1.oPag.oStr_1_195.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NRTSRHMIVM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
        if lower(cEvent)==lower("ImpostaDataOdierna")
          .Calculate_LYBISUKBKM()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.PART_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_PART_ZOOM after query")
          .Calculate_BLTFQIRAHO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.OUT.Event(cEvent)
      .oPgFrm.Page1.oPag.OUT1.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_sat
    * --- Al checked/unchecked della riga sullo zoom non � immediatamente visualizzato
    * --- o nascosto il bottone completa (la HideControls scatta solo al cambio
    * --- di riga dello zoom, per cui forzo la chiamata).
    If upper(cEvent)='W_AGKRA_ZOOM ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM ROW UNCHECKED' OR upper(cEvent)='W_AGKRA_ZOOM3 ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM3 ROW UNCHECKED'
        this.mHideControls()
    endif
    
    
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLVISI,PAFLAPP3,PAFLATG3,PAFLUDI3,PAFLSES3,PAFLASS3,PAFLINP3,PAFLNOT3,PAFLDAF3";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PAFLVISI,PAFLAPP3,PAFLATG3,PAFLUDI3,PAFLSES3,PAFLASS3,PAFLINP3,PAFLNOT3,PAFLDAF3;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_FLVISI = NVL(_Link_.PAFLVISI,space(1))
      this.w_ATTSTU = NVL(_Link_.PAFLAPP3,space(1))
      this.w_ATTGENER = NVL(_Link_.PAFLATG3,space(1))
      this.w_UDIENZE = NVL(_Link_.PAFLUDI3,space(1))
      this.w_SESSTEL = NVL(_Link_.PAFLSES3,space(1))
      this.w_ASSENZE = NVL(_Link_.PAFLASS3,space(1))
      this.w_DAINSPRE = NVL(_Link_.PAFLINP3,space(1))
      this.w_MEMO = NVL(_Link_.PAFLNOT3,space(1))
      this.w_DAFARE = NVL(_Link_.PAFLDAF3,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_FLVISI = space(1)
      this.w_ATTSTU = space(1)
      this.w_ATTGENER = space(1)
      this.w_UDIENZE = space(1)
      this.w_SESSTEL = space(1)
      this.w_ASSENZE = space(1)
      this.w_DAINSPRE = space(1)
      this.w_MEMO = space(1)
      this.w_DAFARE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPART
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODPART))
          select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCODPART_1_22'),i_cWhere,'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPART)
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPART = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPART = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPART = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPART
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPART))
          select DPCODICE,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPART_1_27'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPART)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPART = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPART = space(5)
      endif
      this.w_TIPOGRUP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
        endif
        this.w_GRUPART = space(5)
        this.w_TIPOGRUP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZZ',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_1_30'),i_cWhere,'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(75))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(75)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODICE
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CACODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CACODICE))
          select CACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODICE) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oCACODICE_1_32'),i_cWhere,'',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODICE)
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODICE = NVL(_Link_.CACODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CACODICE = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_34'),i_cWhere,'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(30))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(20)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(30)
      this.w_TIPNOM = space(1)
      this.w_CODCLI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(20)
        this.w_DESNOM = space(40)
        this.w_COMODO = space(30)
        this.w_TIPNOM = space(1)
        this.w_CODCLI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SOGG_IDX,3]
    i_lTable = "CAT_SOGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2], .t., this.CAT_SOGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACS',True,'CAT_SOGG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCAT like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODCAT',trim(this.w_CODCAT))
          select CSCODCAT,CSDESCAT,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.CSCODCAT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStr(trim(this.w_CODCAT)+"%");

            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CAT_SOGG','*','CSCODCAT',cp_AbsName(oSource.parent,'oCODCAT_1_35'),i_cWhere,'GSPR_ACS',"Categorie soggetti esterni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',oSource.xKey(1))
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',this.w_CODCAT)
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.CSCODCAT,space(10))
      this.w_DESCAT = NVL(_Link_.CSDESCAT,space(30))
      this.w_TIPCAT = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(10)
      endif
      this.w_DESCAT = space(30)
      this.w_TIPCAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCAT,1)
      cp_ShowWarn(i_cKey,this.CAT_SOGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodRis
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodRis) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_CodRis))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CodRis)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CodRis) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCodRis_1_36'),i_cWhere,'',"Risorse interne",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodRis)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CodRis);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CodRis)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodRis = NVL(_Link_.DPCODICE,space(5))
      this.w_COGN_CODRIS = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME_CODRIS = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CodRis = space(5)
      endif
      this.w_COGN_CODRIS = space(40)
      this.w_NOME_CODRIS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodRis Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RuolRis
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RUO_SOGI_IDX,3]
    i_lTable = "RUO_SOGI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2], .t., this.RUO_SOGI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RuolRis) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ARI',True,'RUO_SOGI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RSCODRUO like "+cp_ToStrODBC(trim(this.w_RuolRis)+"%");

          i_ret=cp_SQL(i_nConn,"select RSCODRUO,RSDESRUO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RSCODRUO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RSCODRUO',trim(this.w_RuolRis))
          select RSCODRUO,RSDESRUO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RSCODRUO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RuolRis)==trim(_Link_.RSCODRUO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RSDESRUO like "+cp_ToStrODBC(trim(this.w_RuolRis)+"%");

            i_ret=cp_SQL(i_nConn,"select RSCODRUO,RSDESRUO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RSDESRUO like "+cp_ToStr(trim(this.w_RuolRis)+"%");

            select RSCODRUO,RSDESRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RuolRis) and !this.bDontReportError
            deferred_cp_zoom('RUO_SOGI','*','RSCODRUO',cp_AbsName(oSource.parent,'oRuolRis_1_37'),i_cWhere,'GSPR_ARI',"Ruoli soggetti interni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RSCODRUO,RSDESRUO";
                     +" from "+i_cTable+" "+i_lTable+" where RSCODRUO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RSCODRUO',oSource.xKey(1))
            select RSCODRUO,RSDESRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RuolRis)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RSCODRUO,RSDESRUO";
                   +" from "+i_cTable+" "+i_lTable+" where RSCODRUO="+cp_ToStrODBC(this.w_RuolRis);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RSCODRUO',this.w_RuolRis)
            select RSCODRUO,RSDESRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RuolRis = NVL(_Link_.RSCODRUO,space(5))
      this.w_COMODO = NVL(_Link_.RSDESRUO,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_RuolRis = space(5)
      endif
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2])+'\'+cp_ToStr(_Link_.RSCODRUO,1)
      cp_ShowWarn(i_cKey,this.RUO_SOGI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RuolRis Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODTIPOL
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODTIPOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_CODTIPOL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_CODTIPOL))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODTIPOL)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODTIPOL) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oCODTIPOL_1_49'),i_cWhere,'',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODTIPOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_CODTIPOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_CODTIPOL)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODTIPOL = NVL(_Link_.TACODTIP,space(5))
      this.w_DESTIPOL = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODTIPOL = space(5)
      endif
      this.w_DESTIPOL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODTIPOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ENTE
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRA_ENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EPCODICE like "+cp_ToStrODBC(trim(this.w_ENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EPCODICE',trim(this.w_ENTE))
          select EPCODICE,EPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ENTE)==trim(_Link_.EPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" EPDESCRI like "+cp_ToStrODBC(trim(this.w_ENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" EPDESCRI like "+cp_ToStr(trim(this.w_ENTE)+"%");

            select EPCODICE,EPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ENTE) and !this.bDontReportError
            deferred_cp_zoom('PRA_ENTI','*','EPCODICE',cp_AbsName(oSource.parent,'oENTE_1_52'),i_cWhere,'',"Enti pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',oSource.xKey(1))
            select EPCODICE,EPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_ENTE)
            select EPCODICE,EPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENTE = NVL(_Link_.EPCODICE,space(10))
      this.w_DesEnte = NVL(_Link_.EPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ENTE = space(10)
      endif
      this.w_DesEnte = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZZ',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_1_53'),i_cWhere,'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(75))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(75)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENCOS
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_CENCOS)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_CENCOS)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCENCOS_1_55'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CENCOS = space(15)
      endif
      this.w_DESCEN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice centro di costo  inesistente oppure obsoleto")
        endif
        this.w_CENCOS = space(15)
        this.w_DESCEN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GIUDICE
  func Link_1_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GIUDICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_GIUDICE))
          select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GIUDICE)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_GIUDICE)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_GIUDICE)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NOINDIRI like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NOINDIRI like "+cp_ToStr(trim(this.w_GIUDICE)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GIUDICE) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oGIUDICE_1_56'),i_cWhere,'GSAR_ANO',"Giudici",'GSPR_ZGI.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GIUDICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_GIUDICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_GIUDICE)
            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GIUDICE = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOMGIU = NVL(_Link_.NODESCRI,space(30))
      this.w_COMODOGIU = NVL(_Link_.NODESCR2,space(60))
      this.w_INDNOM = NVL(_Link_.NOINDIRI,space(35))
      this.w_CAPNOM = NVL(_Link_.NO___CAP,space(9))
      this.w_DATOBSONOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GIUDICE = space(15)
      endif
      this.w_DESNOMGIU = space(30)
      this.w_COMODOGIU = space(60)
      this.w_INDNOM = space(35)
      this.w_CAPNOM = space(9)
      this.w_DATOBSONOM = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAlt() OR (.w_DATOBSONOM>i_datsys OR EMPTY(.w_DATOBSONOM) ) And CHKGIUDI( .w_GIUDICE )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica")
        endif
        this.w_GIUDICE = space(15)
        this.w_DESNOMGIU = space(30)
        this.w_COMODOGIU = space(60)
        this.w_INDNOM = space(35)
        this.w_CAPNOM = space(9)
        this.w_DATOBSONOM = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GIUDICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSED
  func Link_1_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MDD',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODSED)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPNOM);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPNOM;
                     ,'DDCODICE',this.w_CODCLI;
                     ,'DDCODDES',trim(this.w_CODSED))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSED)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSED) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODSED_1_57'),i_cWhere,'GSAR_MDD',"Sedi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPNOM<>oSource.xKey(1);
           .or. this.w_CODCLI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPNOM);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPNOM);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPNOM;
                       ,'DDCODICE',this.w_CODCLI;
                       ,'DDCODDES',this.w_CODSED)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSED = NVL(_Link_.DDCODDES,space(5))
      this.w_DESCRI = NVL(_Link_.DDNOMDES,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODSED = space(5)
      endif
      this.w_DESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UFFICIO
  func Link_1_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_UFFI_IDX,3]
    i_lTable = "PRA_UFFI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2], .t., this.PRA_UFFI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UFFICIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRA_UFFI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_UFFICIO)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_UFFICIO))
          select UFCODICE,UFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UFFICIO)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UFDESCRI like "+cp_ToStrODBC(trim(this.w_UFFICIO)+"%");

            i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UFDESCRI like "+cp_ToStr(trim(this.w_UFFICIO)+"%");

            select UFCODICE,UFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_UFFICIO) and !this.bDontReportError
            deferred_cp_zoom('PRA_UFFI','*','UFCODICE',cp_AbsName(oSource.parent,'oUFFICIO_1_58'),i_cWhere,'',"Uffici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE,UFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UFFICIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_UFFICIO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_UFFICIO)
            select UFCODICE,UFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UFFICIO = NVL(_Link_.UFCODICE,space(10))
      this.w_DESUFFI = NVL(_Link_.UFDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_UFFICIO = space(10)
      endif
      this.w_DESUFFI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_UFFI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UFFICIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodUte
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodUte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CodUte);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CodUte)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CodUte) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCodUte_1_59'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodUte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CodUte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CodUte)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodUte = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CodUte = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodUte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERPIAN
  func Link_1_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
    i_lTable = "GEST_ATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2], .t., this.GEST_ATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERPIAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AGA',True,'GEST_ATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATSERIAL like "+cp_ToStrODBC(trim(this.w_SERPIAN)+"%");

          i_ret=cp_SQL(i_nConn,"select ATSERIAL,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATSERIAL',trim(this.w_SERPIAN))
          select ATSERIAL,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERPIAN)==trim(_Link_.ATSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SERPIAN) and !this.bDontReportError
            deferred_cp_zoom('GEST_ATTI','*','ATSERIAL',cp_AbsName(oSource.parent,'oSERPIAN_1_65'),i_cWhere,'GSAG_AGA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATSERIAL,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATSERIAL',oSource.xKey(1))
            select ATSERIAL,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERPIAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATSERIAL,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATSERIAL="+cp_ToStrODBC(this.w_SERPIAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATSERIAL',this.w_SERPIAN)
            select ATSERIAL,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERPIAN = NVL(_Link_.ATSERIAL,space(20))
      this.w_DESCRI = NVL(_Link_.ATDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_SERPIAN = space(20)
      endif
      this.w_DESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])+'\'+cp_ToStr(_Link_.ATSERIAL,1)
      cp_ShowWarn(i_cKey,this.GEST_ATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERPIAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPIMPKEYREAD
  func Link_1_101(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPIMPKEYREAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPIMPKEYREAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COMPIMPKEYREAD);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO;
                       ,'CPROWNUM',this.w_COMPIMPKEYREAD)
            select IMCODICE,CPROWNUM,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPIMPKEYREAD = NVL(_Link_.CPROWNUM,0)
      this.w_COMPIMP = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COMPIMPKEYREAD = 0
      endif
      this.w_COMPIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPIMPKEYREAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_1_104(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIM',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_1_104'),i_cWhere,'GSAG_MIM',"Impianto",'GSAG_AAT.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODPART))
          select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCODPART_2_3'),i_cWhere,'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPART)
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPART = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPART = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPART = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPART
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPART))
          select DPCODICE,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPART_2_4'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPART)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPART = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPART = space(5)
      endif
      this.w_TIPOGRUP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
        endif
        this.w_GRUPART = space(5)
        this.w_TIPOGRUP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_13.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_13.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_14.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_14.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_15.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_15.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_16.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_16.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_17.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_17.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_18.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_18.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPRIS_1_20.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page1.oPag.oPATIPRIS_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPRIS_1_21.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page1.oPag.oPATIPRIS_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPART_1_22.value==this.w_CODPART)
      this.oPgFrm.Page1.oPag.oCODPART_1_22.value=this.w_CODPART
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_24.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRISERVE_1_25.RadioValue()==this.w_RISERVE)
      this.oPgFrm.Page1.oPag.oRISERVE_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLESGR_1_26.RadioValue()==this.w_FLESGR)
      this.oPgFrm.Page1.oPag.oFLESGR_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPART_1_27.value==this.w_GRUPART)
      this.oPgFrm.Page1.oPag.oGRUPART_1_27.value=this.w_GRUPART
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_28.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_28.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_29.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_29.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRA_1_30.value==this.w_CODPRA)
      this.oPgFrm.Page1.oPag.oCODPRA_1_30.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIORITA_1_31.RadioValue()==this.w_PRIORITA)
      this.oPgFrm.Page1.oPag.oPRIORITA_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODICE_1_32.value==this.w_CACODICE)
      this.oPgFrm.Page1.oPag.oCACODICE_1_32.value=this.w_CACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_1_33.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_1_33.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_34.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_34.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_35.RadioValue()==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCodRis_1_36.value==this.w_CodRis)
      this.oPgFrm.Page1.oPag.oCodRis_1_36.value=this.w_CodRis
    endif
    if not(this.oPgFrm.Page1.oPag.oRuolRis_1_37.RadioValue()==this.w_RuolRis)
      this.oPgFrm.Page1.oPag.oRuolRis_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATTSTU_1_38.RadioValue()==this.w_ATTSTU)
      this.oPgFrm.Page1.oPag.oATTSTU_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATTGENER_1_39.RadioValue()==this.w_ATTGENER)
      this.oPgFrm.Page1.oPag.oATTGENER_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUDIENZE_1_40.RadioValue()==this.w_UDIENZE)
      this.oPgFrm.Page1.oPag.oUDIENZE_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSESSTEL_1_41.RadioValue()==this.w_SESSTEL)
      this.oPgFrm.Page1.oPag.oSESSTEL_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASSENZE_1_42.RadioValue()==this.w_ASSENZE)
      this.oPgFrm.Page1.oPag.oASSENZE_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAINSPRE_1_43.RadioValue()==this.w_DAINSPRE)
      this.oPgFrm.Page1.oPag.oDAINSPRE_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMEMO_1_44.RadioValue()==this.w_MEMO)
      this.oPgFrm.Page1.oPag.oMEMO_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAFARE_1_45.RadioValue()==this.w_DAFARE)
      this.oPgFrm.Page1.oPag.oDAFARE_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltroWeb_1_46.RadioValue()==this.w_FiltroWeb)
      this.oPgFrm.Page1.oPag.oFiltroWeb_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPONIB_1_47.RadioValue()==this.w_DISPONIB)
      this.oPgFrm.Page1.oPag.oDISPONIB_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltPerson_1_48.value==this.w_FiltPerson)
      this.oPgFrm.Page1.oPag.oFiltPerson_1_48.value=this.w_FiltPerson
    endif
    if not(this.oPgFrm.Page1.oPag.oCODTIPOL_1_49.value==this.w_CODTIPOL)
      this.oPgFrm.Page1.oPag.oCODTIPOL_1_49.value=this.w_CODTIPOL
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_50.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_50.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oATLOCALI_1_51.value==this.w_ATLOCALI)
      this.oPgFrm.Page1.oPag.oATLOCALI_1_51.value=this.w_ATLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oENTE_1_52.value==this.w_ENTE)
      this.oPgFrm.Page1.oPag.oENTE_1_52.value=this.w_ENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRA_1_53.value==this.w_CODPRA)
      this.oPgFrm.Page1.oPag.oCODPRA_1_53.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPANA_1_54.RadioValue()==this.w_TIPANA)
      this.oPgFrm.Page1.oPag.oTIPANA_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCENCOS_1_55.value==this.w_CENCOS)
      this.oPgFrm.Page1.oPag.oCENCOS_1_55.value=this.w_CENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oGIUDICE_1_56.value==this.w_GIUDICE)
      this.oPgFrm.Page1.oPag.oGIUDICE_1_56.value=this.w_GIUDICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSED_1_57.value==this.w_CODSED)
      this.oPgFrm.Page1.oPag.oCODSED_1_57.value=this.w_CODSED
    endif
    if not(this.oPgFrm.Page1.oPag.oUFFICIO_1_58.value==this.w_UFFICIO)
      this.oPgFrm.Page1.oPag.oUFFICIO_1_58.value=this.w_UFFICIO
    endif
    if not(this.oPgFrm.Page1.oPag.oCodUte_1_59.value==this.w_CodUte)
      this.oPgFrm.Page1.oPag.oCodUte_1_59.value=this.w_CodUte
    endif
    if not(this.oPgFrm.Page1.oPag.oChkProm_1_60.RadioValue()==this.w_ChkProm)
      this.oPgFrm.Page1.oPag.oChkProm_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA_PROM_1_61.value==this.w_DATA_PROM)
      this.oPgFrm.Page1.oPag.oDATA_PROM_1_61.value=this.w_DATA_PROM
    endif
    if not(this.oPgFrm.Page1.oPag.oORA_PROM_1_62.value==this.w_ORA_PROM)
      this.oPgFrm.Page1.oPag.oORA_PROM_1_62.value=this.w_ORA_PROM
    endif
    if not(this.oPgFrm.Page1.oPag.oMIN_PROM_1_63.value==this.w_MIN_PROM)
      this.oPgFrm.Page1.oPag.oMIN_PROM_1_63.value=this.w_MIN_PROM
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPORIS_1_64.RadioValue()==this.w_TIPORIS)
      this.oPgFrm.Page1.oPag.oTIPORIS_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSERPIAN_1_65.value==this.w_SERPIAN)
      this.oPgFrm.Page1.oPag.oSERPIAN_1_65.value=this.w_SERPIAN
    endif
    if not(this.oPgFrm.Page1.oPag.oAnnotazioni_1_66.RadioValue()==this.w_Annotazioni)
      this.oPgFrm.Page1.oPag.oAnnotazioni_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDescagg_1_67.RadioValue()==this.w_Descagg)
      this.oPgFrm.Page1.oPag.oDescagg_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETTIMP_1_68.RadioValue()==this.w_DETTIMP)
      this.oPgFrm.Page1.oPag.oDETTIMP_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDatiPratica_1_69.RadioValue()==this.w_DatiPratica)
      this.oPgFrm.Page1.oPag.oDatiPratica_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELCONTR_1_70.RadioValue()==this.w_ELCONTR)
      this.oPgFrm.Page1.oPag.oELCONTR_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAccodaPreavvisi_1_71.RadioValue()==this.w_AccodaPreavvisi)
      this.oPgFrm.Page1.oPag.oAccodaPreavvisi_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAccodaNote_1_72.RadioValue()==this.w_AccodaNote)
      this.oPgFrm.Page1.oPag.oAccodaNote_1_72.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAccodaDaFare_1_73.RadioValue()==this.w_AccodaDaFare)
      this.oPgFrm.Page1.oPag.oAccodaDaFare_1_73.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVISUDI_1_74.RadioValue()==this.w_VISUDI)
      this.oPgFrm.Page1.oPag.oVISUDI_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_100.value==this.w_COMPIMPKEY)
      this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_100.value=this.w_COMPIMPKEY
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMP_1_102.value==this.w_COMPIMP)
      this.oPgFrm.Page1.oPag.oCOMPIMP_1_102.value=this.w_COMPIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPIANTO_1_104.value==this.w_IMPIANTO)
      this.oPgFrm.Page1.oPag.oIMPIANTO_1_104.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPATIPRIS_2_2.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page2.oPag.oPATIPRIS_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPART_2_3.value==this.w_CODPART)
      this.oPgFrm.Page2.oPag.oCODPART_2_3.value=this.w_CODPART
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUPART_2_4.value==this.w_GRUPART)
      this.oPgFrm.Page2.oPag.oGRUPART_2_4.value=this.w_GRUPART
    endif
    if not(this.oPgFrm.Page2.oPag.oFLESGR_2_5.RadioValue()==this.w_FLESGR)
      this.oPgFrm.Page2.oPag.oFLESGR_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDENOM_PART_2_7.value==this.w_DENOM_PART)
      this.oPgFrm.Page2.oPag.oDENOM_PART_2_7.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_PART_1_129.value==this.w_DENOM_PART)
      this.oPgFrm.Page1.oPag.oDENOM_PART_1_129.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRA_1_131.value==this.w_DESPRA)
      this.oPgFrm.Page1.oPag.oDESPRA_1_131.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDesEnte_1_136.value==this.w_DesEnte)
      this.oPgFrm.Page1.oPag.oDesEnte_1_136.value=this.w_DesEnte
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_138.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_138.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_147.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_147.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPOL_1_150.value==this.w_DESTIPOL)
      this.oPgFrm.Page1.oPag.oDESTIPOL_1_150.value=this.w_DESTIPOL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCEN_1_154.value==this.w_DESCEN)
      this.oPgFrm.Page1.oPag.oDESCEN_1_154.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_156.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_156.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDesUffi_1_158.value==this.w_DesUffi)
      this.oPgFrm.Page1.oPag.oDesUffi_1_158.value=this.w_DesUffi
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOMGIU_1_162.value==this.w_DESNOMGIU)
      this.oPgFrm.Page1.oPag.oDESNOMGIU_1_162.value=this.w_DESNOMGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_164.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_164.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRA_1_170.value==this.w_DESPRA)
      this.oPgFrm.Page1.oPag.oDESPRA_1_170.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_172.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_172.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_Cose_1_176.value==this.w_DATFIN_Cose)
      this.oPgFrm.Page1.oPag.oDATFIN_Cose_1_176.value=this.w_DATFIN_Cose
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_Cose_1_177.value==this.w_DATINI_Cose)
      this.oPgFrm.Page1.oPag.oDATINI_Cose_1_177.value=this.w_DATINI_Cose
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADETTDOC_1_186.RadioValue()==this.w_STADETTDOC)
      this.oPgFrm.Page1.oPag.oSTADETTDOC_1_186.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIS_1_191.value==this.w_DESRIS)
      this.oPgFrm.Page1.oPag.oDESRIS_1_191.value=this.w_DESRIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINIZ_2_17.value==this.w_DATINIZ)
      this.oPgFrm.Page2.oPag.oDATINIZ_2_17.value=this.w_DATINIZ
    endif
    if not(this.oPgFrm.Page2.oPag.oobsodat1_2_18.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page2.oPag.oobsodat1_2_18.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(VAL(.w_OREINI) < 24)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_OREFIN) < 24)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not((EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S')))  and not(empty(.w_CODPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPART_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART))  and not(.w_DPTIPRIS <> 'P' )  and (.w_DPTIPRIS='P' AND Not Empty(.w_CODPART) )  and not(empty(.w_GRUPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUPART_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and (Empty( .w_GIUDICE ))  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODNOM_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CENCOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCENCOS_1_55.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice centro di costo  inesistente oppure obsoleto")
          case   not(!IsAlt() OR (.w_DATOBSONOM>i_datsys OR EMPTY(.w_DATOBSONOM) ) And CHKGIUDI( .w_GIUDICE ))  and not(! ISALT())  and (Empty( .w_CODNOM ))  and not(empty(.w_GIUDICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGIUDICE_1_56.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica")
          case   not(VAL(.w_ORA_PROM) < 24)  and (.w_ChkProm='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORA_PROM_1_62.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MIN_PROM) < 60)  and (.w_ChkProm='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMIN_PROM_1_63.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not((EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S')))  and not(empty(.w_CODPART))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODPART_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART))  and not(.w_DPTIPRIS <> 'P' )  and (.w_DPTIPRIS='P' AND Not Empty(.w_CODPART) )  and not(empty(.w_GRUPART))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUPART_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
          case   not(.w_DATINI_Cose<=.w_DATFIN_Cose OR EMPTY(.w_DATFIN_Cose))  and not(.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_Cose_1_176.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   (empty(.w_DATINIZ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATINIZ_2_17.SetFocus()
            i_bnoObbl = !empty(.w_DATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_READAZI = this.w_READAZI
    this.o_FLTPART = this.w_FLTPART
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_PATIPRIS = this.w_PATIPRIS
    this.o_CODPART = this.w_CODPART
    this.o_STATO = this.w_STATO
    this.o_CODNOM = this.w_CODNOM
    this.o_CodRis = this.w_CodRis
    this.o_FiltroWeb = this.w_FiltroWeb
    this.o_CODSED = this.w_CODSED
    this.o_ChkProm = this.w_ChkProm
    this.o_DATA_PROM = this.w_DATA_PROM
    this.o_ORA_PROM = this.w_ORA_PROM
    this.o_MIN_PROM = this.w_MIN_PROM
    this.o_DatiPratica = this.w_DatiPratica
    this.o_FILTPROM = this.w_FILTPROM
    this.o_COMPIMPKEY = this.w_COMPIMPKEY
    this.o_IMPIANTO = this.w_IMPIANTO
    this.o_ELCODIMP = this.w_ELCODIMP
    this.o_ELTIPCON = this.w_ELTIPCON
    this.o_ELCODCOM = this.w_ELCODCOM
    this.o_DATFIN_Cose = this.w_DATFIN_Cose
    this.o_DATINI_Cose = this.w_DATINI_Cose
    this.o_OREINI_Cose = this.w_OREINI_Cose
    this.o_MININI_Cose = this.w_MININI_Cose
    this.o_OREFIN_Cose = this.w_OREFIN_Cose
    this.o_MINFIN_Cose = this.w_MINFIN_Cose
    this.o_DATINIZ = this.w_DATINIZ
    this.o_obsodat1 = this.w_obsodat1
    return

enddefine

* --- Define pages as container
define class tgsag_satPag1 as StdContainer
  Width  = 942
  height = 563
  stdWidth  = 942
  stdheight = 563
  resizeXpos=583
  resizeYpos=397
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_9 as cp_runprogram with uid="VSYPGKICOH",left=14, top=611, width=211,height=26,;
    caption='Azzera ore e min iniziali',;
   bGlobalFont=.t.,;
    prg="gsag_ba2('1')",;
    cEvent = "w_DATINI Changed",;
    nPag=1;
    , HelpContextID = 241519105


  add object oObj_1_12 as cp_runprogram with uid="GTGWZWHBZD",left=14, top=638, width=211,height=26,;
    caption='Azzera ore e min finali',;
   bGlobalFont=.t.,;
    prg="gsag_ba2('2')",;
    cEvent = "w_DATFIN Changed",;
    nPag=1;
    , HelpContextID = 51338243

  add object oDATINI_1_13 as StdField with uid="VAJTUNAFFS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 154902986,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=99, Top=8

  func oDATINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_14 as StdField with uid="VRHGJGBHQK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 76456394,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=99, Top=32

  func oDATFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oOREINI_1_15 as StdField with uid="WOKDXXPUIP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio selezione",;
    HelpContextID = 154959898,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=227, Top=8, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oOREINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_16 as StdField with uid="QMRKNBACQP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio selezione",;
    HelpContextID = 154925370,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=265, Top=8, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oMININI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oOREFIN_1_17 as StdField with uid="VKJCJSQZFK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine selezione",;
    HelpContextID = 76513306,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=227, Top=32, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oOREFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_18 as StdField with uid="GIWPXXGMLC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine selezione",;
    HelpContextID = 76478778,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=265, Top=32, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc


  add object oBtn_1_19 as StdButton with uid="BQJHHOQZPU",left=296, top=8, width=48,height=45,;
    CpPicture="bmp\date-time.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per impostare la data odierna";
    , HelpContextID = 117066522;
    , caption='\<Oggi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      this.parent.oContained.NotifyEvent("ImpostaDataOdierna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oPATIPRIS_1_20 as StdCombo with uid="DGFKWOOHPQ",value=3,rtseq=12,rtrep=.f.,left=430,top=8,width=76,height=21;
    , HelpContextID = 1810615;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPRIS_1_20.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'R',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oPATIPRIS_1_20.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_1_20.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='',3,;
      0)))
  endfunc

  func oPATIPRIS_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (isAlt())
    endwith
   endif
  endfunc

  func oPATIPRIS_1_20.mHide()
    with this.Parent.oContained
      return (!isAlt())
    endwith
  endfunc


  add object oPATIPRIS_1_21 as StdCombo with uid="FEFTOWFWCM",value=4,rtseq=13,rtrep=.f.,left=427,top=8,width=76,height=21;
    , HelpContextID = 1810615;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPRIS_1_21.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_1_21.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_1_21.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc

  func oPATIPRIS_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!isAlt())
    endwith
   endif
  endfunc

  func oPATIPRIS_1_21.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oCODPART_1_22 as StdField with uid="QJYWRUDDRU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODPART", cQueryName = "CODPART",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 251292710,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=507, Top=8, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPART"

  func oCODPART_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPART_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPART_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCODPART_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this.parent.oContained
  endproc


  add object oSTATO_1_24 as StdCombo with uid="MIGALQMTMD",rtseq=15,rtrep=.f.,left=397,top=35,width=183,height=21;
    , ToolTipText = "Stato";
    , HelpContextID = 35765722;
    , cFormVar="w_STATO",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Evasa (da completare),"+"Completata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_24.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'B',;
    iif(this.value =6,'F',;
    iif(this.value =7,'P',;
    iif(this.value =8,'K',;
    space(1))))))))))
  endfunc
  func oSTATO_1_24.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_24.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='N',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='I',3,;
      iif(this.Parent.oContained.w_STATO=='T',4,;
      iif(this.Parent.oContained.w_STATO=='B',5,;
      iif(this.Parent.oContained.w_STATO=='F',6,;
      iif(this.Parent.oContained.w_STATO=='P',7,;
      iif(this.Parent.oContained.w_STATO=='K',8,;
      0))))))))
  endfunc

  func oSTATO_1_24.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oRISERVE_1_25 as StdCombo with uid="MIFARZNGXZ",rtseq=16,rtrep=.f.,left=580,top=35,width=106,height=21;
    , ToolTipText = "Considera o meno tutte le attivit� in riserva a prescindere dai filtri sullo stato e sulle date";
    , HelpContextID = 67131158;
    , cFormVar="w_RISERVE",RowSource=""+"Escludi riserve,"+"Aggiungi riserve,"+"No filtro riserve,"+"Solo riserve", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRISERVE_1_25.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'A',;
    iif(this.value =3,'L',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oRISERVE_1_25.GetRadio()
    this.Parent.oContained.w_RISERVE = this.RadioValue()
    return .t.
  endfunc

  func oRISERVE_1_25.SetRadio()
    this.Parent.oContained.w_RISERVE=trim(this.Parent.oContained.w_RISERVE)
    this.value = ;
      iif(this.Parent.oContained.w_RISERVE=='E',1,;
      iif(this.Parent.oContained.w_RISERVE=='A',2,;
      iif(this.Parent.oContained.w_RISERVE=='L',3,;
      iif(this.Parent.oContained.w_RISERVE=='S',4,;
      0))))
  endfunc

  func oRISERVE_1_25.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oFLESGR_1_26 as StdCheck with uid="AYKWNFTPEF",rtseq=17,rtrep=.f.,left=691, top=34, caption="Esplodi gruppo",;
    ToolTipText = "Se attivo visualizza anche appuntamenti delle persone del gruppo",;
    HelpContextID = 10651306,;
    cFormVar="w_FLESGR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLESGR_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLESGR_1_26.GetRadio()
    this.Parent.oContained.w_FLESGR = this.RadioValue()
    return .t.
  endfunc

  func oFLESGR_1_26.SetRadio()
    this.Parent.oContained.w_FLESGR=trim(this.Parent.oContained.w_FLESGR)
    this.value = ;
      iif(this.Parent.oContained.w_FLESGR=='S',1,;
      0)
  endfunc

  func oFLESGR_1_26.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'G' or Empty(.w_CODPART) )
    endwith
  endfunc

  add object oGRUPART_1_27 as StdField with uid="AYUXRJLLLH",rtseq=18,rtrep=.f.,;
    cFormVar = "w_GRUPART", cQueryName = "GRUPART",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo inesistente o non associato al partecipante",;
    HelpContextID = 251363174,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=750, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPART"

  func oGRUPART_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPTIPRIS='P' AND Not Empty(.w_CODPART) )
    endwith
   endif
  endfunc

  func oGRUPART_1_27.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  func oGRUPART_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPART_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPART_1_27.mZoom
      with this.Parent.oContained
        GSAR1BGP(this.Parent.oContained,  "", .w_CODPART, "", "GRUPART" )
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNUMDOC_1_28 as StdField with uid="ALSVTWVYIC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'attivit�",;
    HelpContextID = 254868778,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=100, Top=58, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMDOC_1_28.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oALFDOC_1_29 as StdField with uid="TNHYATACTB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie dell'attivit�",;
    HelpContextID = 254899962,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=226, Top=58, InputMask=replicate('X',10)

  func oALFDOC_1_29.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oCODPRA_1_30 as StdField with uid="IYNQTLTSOS",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 16094170,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=99, Top=58, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_BZZ", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_1_30.mHide()
    with this.Parent.oContained
      return (Isahe() or Isahr())
    endwith
  endfunc

  func oCODPRA_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRA_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRA
     i_obj.ecpSave()
  endproc


  add object oPRIORITA_1_31 as StdCombo with uid="TPBXXBSCEW",value=4,rtseq=22,rtrep=.f.,left=694,top=58,width=118,height=21;
    , ToolTipText = "Priorit�";
    , HelpContextID = 118079543;
    , cFormVar="w_PRIORITA",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRIORITA_1_31.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,3,;
    iif(this.value =3,4,;
    iif(this.value =4,0,;
    0)))))
  endfunc
  func oPRIORITA_1_31.GetRadio()
    this.Parent.oContained.w_PRIORITA = this.RadioValue()
    return .t.
  endfunc

  func oPRIORITA_1_31.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PRIORITA==1,1,;
      iif(this.Parent.oContained.w_PRIORITA==3,2,;
      iif(this.Parent.oContained.w_PRIORITA==4,3,;
      iif(this.Parent.oContained.w_PRIORITA==0,4,;
      0))))
  endfunc

  add object oCACODICE_1_32 as StdField with uid="MBGICDMGLD",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CACODICE", cQueryName = "CACODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 103370347,;
   bGlobalFont=.t.,;
    Height=21, Width=171, Left=99, Top=82, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODICE"

  func oCACODICE_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODICE_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODICE_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oCACODICE_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc

  add object oCADESCRI_1_33 as AH_SEARCHFLD with uid="AVSKMMIFVK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione oggetto o parte di essa",;
    HelpContextID = 17784431,;
   bGlobalFont=.t.,;
    Height=21, Width=413, Left=399, Top=82, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oCADESCRI_1_33.mZoom
    vx_exec("query\GSAG1KRA.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCODNOM_1_34 as StdField with uid="LDORSNJAGM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo o soggetto esterno della pratica",;
    HelpContextID = 86479834,;
   bGlobalFont=.t.,;
    Height=21, Width=147, Left=99, Top=107, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_GIUDICE ))
    endwith
   endif
  endfunc

  func oCODNOM_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc


  add object oCODCAT_1_35 as StdTableCombo with uid="AXUHUESMBU",rtseq=26,rtrep=.f.,left=678,top=107,width=134,height=21;
    , ToolTipText = "Ruolo del nominativo (soggetti esterni)";
    , HelpContextID = 15559718;
    , cFormVar="w_CODCAT",tablefilter="", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Codice categoria soggetti inesistente o di tipo cliente";
    , cLinkFile="CAT_SOGG";
    , cTable='CAT_SOGG',cKey='CSCODCAT',cValue='CSDESCAT',cOrderBy='CSDESCAT',xDefault=space(10);
  , bGlobalFont=.t.


  func oCODCAT_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODNOM))
    endwith
   endif
  endfunc

  func oCODCAT_1_35.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oCODCAT_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCodRis_1_36 as StdField with uid="DOBBSYFCFJ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CodRis", cQueryName = "CodRis",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Soggetto interno",;
    HelpContextID = 41847846,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=99, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_CodRis"

  func oCodRis_1_36.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oCodRis_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodRis_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodRis_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCodRis_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse interne",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc


  add object oRuolRis_1_37 as StdTableCombo with uid="HKRTBWAVCA",rtseq=28,rtrep=.f.,left=678,top=132,width=134,height=21;
    , ToolTipText = "Ruolo risorsa interna";
    , HelpContextID = 120144662;
    , cFormVar="w_RuolRis",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="RUO_SOGI";
    , cTable='RUO_SOGI',cKey='RSCODRUO',cValue='RSDESRUO',cOrderBy='RSDESRUO',xDefault=space(5);
  , bGlobalFont=.t.


  func oRuolRis_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CodRis))
    endwith
   endif
  endfunc

  func oRuolRis_1_37.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oRuolRis_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oRuolRis_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oATTSTU_1_38 as StdCheck with uid="FUHPBAIQSC",rtseq=29,rtrep=.f.,left=101, top=154, caption="Appuntamenti",;
    ToolTipText = "Se attivo: visualizza gli appuntamenti",;
    HelpContextID = 53375238,;
    cFormVar="w_ATTSTU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTSTU_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oATTSTU_1_38.GetRadio()
    this.Parent.oContained.w_ATTSTU = this.RadioValue()
    return .t.
  endfunc

  func oATTSTU_1_38.SetRadio()
    this.Parent.oContained.w_ATTSTU=trim(this.Parent.oContained.w_ATTSTU)
    this.value = ;
      iif(this.Parent.oContained.w_ATTSTU=='S',1,;
      0)
  endfunc

  add object oATTGENER_1_39 as StdCheck with uid="KLBNRLNCJC",rtseq=30,rtrep=.f.,left=101, top=172, caption="Attivit� generiche",;
    ToolTipText = "Se attivo: visualizza le attivit� generiche",;
    HelpContextID = 187855192,;
    cFormVar="w_ATTGENER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTGENER_1_39.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oATTGENER_1_39.GetRadio()
    this.Parent.oContained.w_ATTGENER = this.RadioValue()
    return .t.
  endfunc

  func oATTGENER_1_39.SetRadio()
    this.Parent.oContained.w_ATTGENER=trim(this.Parent.oContained.w_ATTGENER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTGENER=='G',1,;
      0)
  endfunc

  add object oUDIENZE_1_40 as StdCheck with uid="HBQKVBXHMX",rtseq=31,rtrep=.f.,left=101, top=190, caption="Udienze",;
    ToolTipText = "Se attivo: visualizza le udienze",;
    HelpContextID = 130003526,;
    cFormVar="w_UDIENZE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUDIENZE_1_40.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oUDIENZE_1_40.GetRadio()
    this.Parent.oContained.w_UDIENZE = this.RadioValue()
    return .t.
  endfunc

  func oUDIENZE_1_40.SetRadio()
    this.Parent.oContained.w_UDIENZE=trim(this.Parent.oContained.w_UDIENZE)
    this.value = ;
      iif(this.Parent.oContained.w_UDIENZE=='U',1,;
      0)
  endfunc

  func oUDIENZE_1_40.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oSESSTEL_1_41 as StdCheck with uid="KYMNVXZYIW",rtseq=32,rtrep=.f.,left=272, top=154, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo: visualizza le sessioni telefoniche",;
    HelpContextID = 215067866,;
    cFormVar="w_SESSTEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSESSTEL_1_41.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oSESSTEL_1_41.GetRadio()
    this.Parent.oContained.w_SESSTEL = this.RadioValue()
    return .t.
  endfunc

  func oSESSTEL_1_41.SetRadio()
    this.Parent.oContained.w_SESSTEL=trim(this.Parent.oContained.w_SESSTEL)
    this.value = ;
      iif(this.Parent.oContained.w_SESSTEL=='T',1,;
      0)
  endfunc

  add object oASSENZE_1_42 as StdCheck with uid="RPXCBGRCCL",rtseq=33,rtrep=.f.,left=272, top=172, caption="Assenze",;
    ToolTipText = "Se attivo: visualizza le assenze",;
    HelpContextID = 130048006,;
    cFormVar="w_ASSENZE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASSENZE_1_42.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oASSENZE_1_42.GetRadio()
    this.Parent.oContained.w_ASSENZE = this.RadioValue()
    return .t.
  endfunc

  func oASSENZE_1_42.SetRadio()
    this.Parent.oContained.w_ASSENZE=trim(this.Parent.oContained.w_ASSENZE)
    this.value = ;
      iif(this.Parent.oContained.w_ASSENZE=='A',1,;
      0)
  endfunc

  add object oDAINSPRE_1_43 as StdCheck with uid="AMMYJSOHVW",rtseq=34,rtrep=.f.,left=272, top=190, caption="Da inserimento prestazioni",;
    ToolTipText = "Se attivo: visualizza le attivit� generate dall'inserimento delle prestazioni",;
    HelpContextID = 236498555,;
    cFormVar="w_DAINSPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAINSPRE_1_43.RadioValue()
    return(iif(this.value =1,'Z',;
    ' '))
  endfunc
  func oDAINSPRE_1_43.GetRadio()
    this.Parent.oContained.w_DAINSPRE = this.RadioValue()
    return .t.
  endfunc

  func oDAINSPRE_1_43.SetRadio()
    this.Parent.oContained.w_DAINSPRE=trim(this.Parent.oContained.w_DAINSPRE)
    this.value = ;
      iif(this.Parent.oContained.w_DAINSPRE=='Z',1,;
      0)
  endfunc

  add object oMEMO_1_44 as StdCheck with uid="ELRSHFYRWO",rtseq=35,rtrep=.f.,left=479, top=154, caption="Note",;
    ToolTipText = "Se attivo: visualizza le note",;
    HelpContextID = 118885690,;
    cFormVar="w_MEMO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMEMO_1_44.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oMEMO_1_44.GetRadio()
    this.Parent.oContained.w_MEMO = this.RadioValue()
    return .t.
  endfunc

  func oMEMO_1_44.SetRadio()
    this.Parent.oContained.w_MEMO=trim(this.Parent.oContained.w_MEMO)
    this.value = ;
      iif(this.Parent.oContained.w_MEMO=='M',1,;
      0)
  endfunc

  proc oMEMO_1_44.mAfter
    with this.Parent.oContained
      .w_AccodaNote='N'
    endwith
  endproc

  add object oDAFARE_1_45 as StdCheck with uid="JYTLCSHGGN",rtseq=36,rtrep=.f.,left=479, top=172, caption="Cose da fare",;
    ToolTipText = "Se attivo: visualizza le cose da fare",;
    HelpContextID = 218399178,;
    cFormVar="w_DAFARE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAFARE_1_45.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oDAFARE_1_45.GetRadio()
    this.Parent.oContained.w_DAFARE = this.RadioValue()
    return .t.
  endfunc

  func oDAFARE_1_45.SetRadio()
    this.Parent.oContained.w_DAFARE=trim(this.Parent.oContained.w_DAFARE)
    this.value = ;
      iif(this.Parent.oContained.w_DAFARE=='D',1,;
      0)
  endfunc

  proc oDAFARE_1_45.mAfter
    with this.Parent.oContained
      .w_AccodaDaFare='N'
    endwith
  endproc

  add object oFiltroWeb_1_46 as StdRadio with uid="NOISYYKDWQ",rtseq=37,rtrep=.f.,left=605, top=155, width=147,height=49;
    , ToolTipText = "Filtro su flag di pubblicazione su Web";
    , cFormVar="w_FiltroWeb", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oFiltroWeb_1_46.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo pubblicabili"
      this.Buttons(1).HelpContextID = 13562661
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo non pubblicabili"
      this.Buttons(2).HelpContextID = 13562661
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 13562661
      this.Buttons(3).Top=30
      this.SetAll("Width",145)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Filtro su flag di pubblicazione su Web")
      StdRadio::init()
    endproc

  func oFiltroWeb_1_46.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFiltroWeb_1_46.GetRadio()
    this.Parent.oContained.w_FiltroWeb = this.RadioValue()
    return .t.
  endfunc

  func oFiltroWeb_1_46.SetRadio()
    this.Parent.oContained.w_FiltroWeb=trim(this.Parent.oContained.w_FiltroWeb)
    this.value = ;
      iif(this.Parent.oContained.w_FiltroWeb=='S',1,;
      iif(this.Parent.oContained.w_FiltroWeb=='N',2,;
      iif(this.Parent.oContained.w_FiltroWeb=='T',3,;
      0)))
  endfunc

  func oFiltroWeb_1_46.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oDISPONIB_1_47 as StdCombo with uid="MRAFDOVVBK",value=5,rtseq=38,rtrep=.f.,left=99,top=212,width=118,height=21;
    , ToolTipText = "Disponibilit�";
    , HelpContextID = 69511560;
    , cFormVar="w_DISPONIB",RowSource=""+"Occupato,"+"Per urgenze,"+"Libero,"+"Fuori sede,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDISPONIB_1_47.RadioValue()
    return(iif(this.value =1,2,;
    iif(this.value =2,3,;
    iif(this.value =3,1,;
    iif(this.value =4,4,;
    iif(this.value =5,0,;
    0))))))
  endfunc
  func oDISPONIB_1_47.GetRadio()
    this.Parent.oContained.w_DISPONIB = this.RadioValue()
    return .t.
  endfunc

  func oDISPONIB_1_47.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DISPONIB==2,1,;
      iif(this.Parent.oContained.w_DISPONIB==3,2,;
      iif(this.Parent.oContained.w_DISPONIB==1,3,;
      iif(this.Parent.oContained.w_DISPONIB==4,4,;
      iif(this.Parent.oContained.w_DISPONIB==0,5,;
      0)))))
  endfunc

  add object oFiltPerson_1_48 as AH_SEARCHFLD with uid="MYRJGKWLFV",rtseq=39,rtrep=.f.,;
    cFormVar = "w_FiltPerson", cQueryName = "FiltPerson",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Persona o parte di essa",;
    HelpContextID = 51477433,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=361, Top=211, InputMask=replicate('X',60)

  add object oCODTIPOL_1_49 as StdField with uid="JGICKRMXCV",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CODTIPOL", cQueryName = "CODTIPOL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia",;
    HelpContextID = 42046350,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=99, Top=234, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", oKey_1_1="TACODTIP", oKey_1_2="this.w_CODTIPOL"

  func oCODTIPOL_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODTIPOL_1_49.ecpDrop(oSource)
    this.Parent.oContained.link_1_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODTIPOL_1_49.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oCODTIPOL_1_49'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipologie attivit�",'',this.parent.oContained
  endproc

  add object oNOTE_1_50 as AH_SEARCHMEMO with uid="YFIDICHIXZ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 119509802,;
   bGlobalFont=.t.,;
    Height=21, Width=695, Left=99, Top=259

  add object oATLOCALI_1_51 as AH_SEARCHFLD with uid="KNSKLUNXGJ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_ATLOCALI", cQueryName = "ATLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� o parte di essa",;
    HelpContextID = 31854257,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=99, Top=284, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oATLOCALI_1_51.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_ATLOCALI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oENTE_1_52 as StdField with uid="NHZLOLJQPN",rtseq=43,rtrep=.f.,;
    cFormVar = "w_ENTE", cQueryName = "ENTE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente/ufficio giudiziario",;
    HelpContextID = 119510202,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=99, Top=311, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRA_ENTI", oKey_1_1="EPCODICE", oKey_1_2="this.w_ENTE"

  func oENTE_1_52.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  func oENTE_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oENTE_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oENTE_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRA_ENTI','*','EPCODICE',cp_AbsName(this.parent,'oENTE_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Enti pratica",'',this.parent.oContained
  endproc

  add object oCODPRA_1_53 as StdField with uid="LNZCMMZIKS",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 16094170,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=99, Top=311, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_BZZ", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_1_53.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCODPRA_1_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_53('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_1_53.ecpDrop(oSource)
    this.Parent.oContained.link_1_53('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_1_53.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_1_53'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRA_1_53.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRA
     i_obj.ecpSave()
  endproc


  add object oTIPANA_1_54 as StdCombo with uid="OOYOOVQSKI",rtseq=45,rtrep=.f.,left=100,top=338,width=146,height=21;
    , HelpContextID = 21223626;
    , cFormVar="w_TIPANA",RowSource=""+"Costo,"+"Ricavo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPANA_1_54.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPANA_1_54.GetRadio()
    this.Parent.oContained.w_TIPANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPANA_1_54.SetRadio()
    this.Parent.oContained.w_TIPANA=trim(this.Parent.oContained.w_TIPANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPANA=='C',1,;
      iif(this.Parent.oContained.w_TIPANA=='R',2,;
      0))
  endfunc

  func oTIPANA_1_54.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCENCOS_1_55 as StdField with uid="YIACDIYSPI",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CENCOS", cQueryName = "CENCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice centro di costo  inesistente oppure obsoleto",;
    ToolTipText = "Centro di costo/ricavo di fine selezione",;
    HelpContextID = 13500966,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=343, Top=336, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENCOS"

  func oCENCOS_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_55('Part',this)
    endwith
    return bRes
  endfunc

  proc oCENCOS_1_55.ecpDrop(oSource)
    this.Parent.oContained.link_1_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oGIUDICE_1_56 as StdField with uid="FJVJFDFGGP",rtseq=47,rtrep=.f.,;
    cFormVar = "w_GIUDICE", cQueryName = "GIUDICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica",;
    ToolTipText = "Se specificato filtra le attivit� associate a pratiche con data fine compresa all'interno dell'intervallo di validiit� del giudice",;
    HelpContextID = 7304806,;
   bGlobalFont=.t.,;
    Height=21, Width=138, Left=99, Top=361, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_GIUDICE"

  func oGIUDICE_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_CODNOM ))
    endwith
   endif
  endfunc

  func oGIUDICE_1_56.mHide()
    with this.Parent.oContained
      return (! ISALT())
    endwith
  endfunc

  func oGIUDICE_1_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_56('Part',this)
    endwith
    return bRes
  endfunc

  proc oGIUDICE_1_56.ecpDrop(oSource)
    this.Parent.oContained.link_1_56('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGIUDICE_1_56.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oGIUDICE_1_56'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Giudici",'GSPR_ZGI.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oGIUDICE_1_56.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_GIUDICE
     i_obj.ecpSave()
  endproc

  add object oCODSED_1_57 as StdField with uid="DGJQCXNDEJ",rtseq=48,rtrep=.f.,;
    cFormVar = "w_CODSED", cQueryName = "CODSED",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 247632858,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=99, Top=361, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", cZoomOnZoom="GSAR_MDD", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPNOM", oKey_2_1="DDCODICE", oKey_2_2="this.w_CODCLI", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODSED"

  func oCODSED_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPNOM='C')
    endwith
   endif
  endfunc

  func oCODSED_1_57.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  func oCODSED_1_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_57('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSED_1_57.ecpDrop(oSource)
    this.Parent.oContained.link_1_57('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSED_1_57.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPNOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPNOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODCLI)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODSED_1_57'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MDD',"Sedi",'',this.parent.oContained
  endproc
  proc oCODSED_1_57.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MDD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DDTIPCON=w_TIPNOM
    i_obj.DDCODICE=w_CODCLI
     i_obj.w_DDCODDES=this.parent.oContained.w_CODSED
     i_obj.ecpSave()
  endproc

  add object oUFFICIO_1_58 as StdField with uid="WWRVAFBIIP",rtseq=49,rtrep=.f.,;
    cFormVar = "w_UFFICIO", cQueryName = "UFFICIO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice ufficio/sezione",;
    HelpContextID = 166493114,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=98, Top=388, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRA_UFFI", oKey_1_1="UFCODICE", oKey_1_2="this.w_UFFICIO"

  func oUFFICIO_1_58.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  func oUFFICIO_1_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_58('Part',this)
    endwith
    return bRes
  endfunc

  proc oUFFICIO_1_58.ecpDrop(oSource)
    this.Parent.oContained.link_1_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUFFICIO_1_58.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRA_UFFI','*','UFCODICE',cp_AbsName(this.parent,'oUFFICIO_1_58'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Uffici",'',this.parent.oContained
  endproc

  add object oCodUte_1_59 as StdField with uid="PIUVUYSBHD",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CodUte", cQueryName = "CodUte",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente ultima modifica",;
    HelpContextID = 87133222,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=98, Top=415, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CodUte"

  func oCodUte_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_59('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodUte_1_59.ecpDrop(oSource)
    this.Parent.oContained.link_1_59('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodUte_1_59.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCodUte_1_59'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oChkProm_1_60 as StdCheck with uid="XNJEQKNWQH",rtseq=51,rtrep=.f.,left=98, top=438, caption="Filtra le attivit� con promemoria inferiore a",;
    ToolTipText = "Se attivo: seleziona le attivit� con promemoria inferiore alla data selezionata",;
    HelpContextID = 15928026,;
    cFormVar="w_ChkProm", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oChkProm_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oChkProm_1_60.GetRadio()
    this.Parent.oContained.w_ChkProm = this.RadioValue()
    return .t.
  endfunc

  func oChkProm_1_60.SetRadio()
    this.Parent.oContained.w_ChkProm=trim(this.Parent.oContained.w_ChkProm)
    this.value = ;
      iif(this.Parent.oContained.w_ChkProm=='S',1,;
      0)
  endfunc

  add object oDATA_PROM_1_61 as StdField with uid="SWIUVBMUDH",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DATA_PROM", cQueryName = "DATA_PROM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data promemoria",;
    HelpContextID = 248275797,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=373, Top=439

  func oDATA_PROM_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ChkProm='S')
    endwith
   endif
  endfunc

  add object oORA_PROM_1_62 as StdField with uid="QRHLNNKGBQ",rtseq=53,rtrep=.f.,;
    cFormVar = "w_ORA_PROM", cQueryName = "ORA_PROM",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora promemoria",;
    HelpContextID = 442317,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=516, Top=439, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORA_PROM_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ChkProm='S')
    endwith
   endif
  endfunc

  func oORA_PROM_1_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORA_PROM) < 24)
    endwith
    return bRes
  endfunc

  add object oMIN_PROM_1_63 as StdField with uid="BUHARTDGYH",rtseq=54,rtrep=.f.,;
    cFormVar = "w_MIN_PROM", cQueryName = "MIN_PROM",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti promemoria",;
    HelpContextID = 391405,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=552, Top=439, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMIN_PROM_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ChkProm='S')
    endwith
   endif
  endfunc

  func oMIN_PROM_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MIN_PROM) < 60)
    endwith
    return bRes
  endfunc


  add object oTIPORIS_1_64 as StdTableCombo with uid="XMEKXWDJPL",rtseq=55,rtrep=.f.,left=98,top=461,width=123,height=21;
    , HelpContextID = 118105910;
    , cFormVar="w_TIPORIS",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='TIP_RISE',cKey='TRCODICE',cValue='TRDESTIP',cOrderBy='TRDESTIP',xDefault=space(1);
  , bGlobalFont=.t.


  func oTIPORIS_1_64.mHide()
    with this.Parent.oContained
      return (.w_RISERVE<> 'S' or not IsAlt())
    endwith
  endfunc

  add object oSERPIAN_1_65 as StdField with uid="DXJGXWZCBD",rtseq=56,rtrep=.f.,;
    cFormVar = "w_SERPIAN", cQueryName = "SERPIAN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Piano di generazione attivit�",;
    HelpContextID = 25476314,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=98, Top=461, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="GEST_ATTI", cZoomOnZoom="GSAG_AGA", oKey_1_1="ATSERIAL", oKey_1_2="this.w_SERPIAN"

  func oSERPIAN_1_65.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oSERPIAN_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_65('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERPIAN_1_65.ecpDrop(oSource)
    this.Parent.oContained.link_1_65('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERPIAN_1_65.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GEST_ATTI','*','ATSERIAL',cp_AbsName(this.parent,'oSERPIAN_1_65'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_AGA',"",'',this.parent.oContained
  endproc
  proc oSERPIAN_1_65.mZoomOnZoom
    local i_obj
    i_obj=GSAG_AGA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATSERIAL=this.parent.oContained.w_SERPIAN
     i_obj.ecpSave()
  endproc

  add object oAnnotazioni_1_66 as StdCheck with uid="BOCSCACCBJ",rtseq=57,rtrep=.f.,left=11, top=490, caption="Note attivit�",;
    ToolTipText = "Se attivo, stampa le note attivit�",;
    HelpContextID = 246206369,;
    cFormVar="w_Annotazioni", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAnnotazioni_1_66.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAnnotazioni_1_66.GetRadio()
    this.Parent.oContained.w_Annotazioni = this.RadioValue()
    return .t.
  endfunc

  func oAnnotazioni_1_66.SetRadio()
    this.Parent.oContained.w_Annotazioni=trim(this.Parent.oContained.w_Annotazioni)
    this.value = ;
      iif(this.Parent.oContained.w_Annotazioni=='S',1,;
      0)
  endfunc

  add object oDescagg_1_67 as StdCheck with uid="FSYHMFZRNQ",rtseq=58,rtrep=.f.,left=11, top=513, caption="Descrizione aggiuntiva",;
    ToolTipText = "Se attivo, stampa la descrizione aggiuntiva",;
    HelpContextID = 166694346,;
    cFormVar="w_Descagg", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDescagg_1_67.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDescagg_1_67.GetRadio()
    this.Parent.oContained.w_Descagg = this.RadioValue()
    return .t.
  endfunc

  func oDescagg_1_67.SetRadio()
    this.Parent.oContained.w_Descagg=trim(this.Parent.oContained.w_Descagg)
    this.value = ;
      iif(this.Parent.oContained.w_Descagg=='S',1,;
      0)
  endfunc

  add object oDETTIMP_1_68 as StdCheck with uid="UINFRLTFXU",rtseq=59,rtrep=.f.,left=184, top=490, caption="Dettaglio attributi",;
    ToolTipText = "Se attivo, stampa il dettaglio attributi degli impianti",;
    HelpContextID = 92315082,;
    cFormVar="w_DETTIMP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDETTIMP_1_68.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDETTIMP_1_68.GetRadio()
    this.Parent.oContained.w_DETTIMP = this.RadioValue()
    return .t.
  endfunc

  func oDETTIMP_1_68.SetRadio()
    this.Parent.oContained.w_DETTIMP=trim(this.Parent.oContained.w_DETTIMP)
    this.value = ;
      iif(this.Parent.oContained.w_DETTIMP=='S',1,;
      0)
  endfunc

  func oDETTIMP_1_68.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDatiPratica_1_69 as StdCheck with uid="MTQOGAYNLY",rtseq=60,rtrep=.f.,left=184, top=490, caption="Dati pratica",;
    ToolTipText = "Se attivo, stampa i dati della pratica",;
    HelpContextID = 849978,;
    cFormVar="w_DatiPratica", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDatiPratica_1_69.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDatiPratica_1_69.GetRadio()
    this.Parent.oContained.w_DatiPratica = this.RadioValue()
    return .t.
  endfunc

  func oDatiPratica_1_69.SetRadio()
    this.Parent.oContained.w_DatiPratica=trim(this.Parent.oContained.w_DatiPratica)
    this.value = ;
      iif(this.Parent.oContained.w_DatiPratica=='S',1,;
      0)
  endfunc

  func oDatiPratica_1_69.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oELCONTR_1_70 as StdCheck with uid="RBDYNNUKWL",rtseq=61,rtrep=.f.,left=184, top=513, caption="Elemento contratto",;
    ToolTipText = "Se attivo, stampa l'elemento contratto del dettaglio prestazioni",;
    HelpContextID = 29972806,;
    cFormVar="w_ELCONTR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELCONTR_1_70.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oELCONTR_1_70.GetRadio()
    this.Parent.oContained.w_ELCONTR = this.RadioValue()
    return .t.
  endfunc

  func oELCONTR_1_70.SetRadio()
    this.Parent.oContained.w_ELCONTR=trim(this.Parent.oContained.w_ELCONTR)
    this.value = ;
      iif(this.Parent.oContained.w_ELCONTR=='S',1,;
      0)
  endfunc

  func oELCONTR_1_70.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAccodaPreavvisi_1_71 as StdCheck with uid="ZFZLPXQWWQ",rtseq=62,rtrep=.f.,left=320, top=489, caption="Accoda preavvisi",;
    ToolTipText = "Se attivo, accoda i preavvisi",;
    HelpContextID = 173619144,;
    cFormVar="w_AccodaPreavvisi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAccodaPreavvisi_1_71.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAccodaPreavvisi_1_71.GetRadio()
    this.Parent.oContained.w_AccodaPreavvisi = this.RadioValue()
    return .t.
  endfunc

  func oAccodaPreavvisi_1_71.SetRadio()
    this.Parent.oContained.w_AccodaPreavvisi=trim(this.Parent.oContained.w_AccodaPreavvisi)
    this.value = ;
      iif(this.Parent.oContained.w_AccodaPreavvisi=='S',1,;
      0)
  endfunc

  add object oAccodaNote_1_72 as StdCheck with uid="WKLVDZRVGV",rtseq=63,rtrep=.f.,left=320, top=512, caption="Accoda le note",;
    ToolTipText = "Se attivo, accoda le note",;
    HelpContextID = 263463755,;
    cFormVar="w_AccodaNote", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAccodaNote_1_72.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAccodaNote_1_72.GetRadio()
    this.Parent.oContained.w_AccodaNote = this.RadioValue()
    return .t.
  endfunc

  func oAccodaNote_1_72.SetRadio()
    this.Parent.oContained.w_AccodaNote=trim(this.Parent.oContained.w_AccodaNote)
    this.value = ;
      iif(this.Parent.oContained.w_AccodaNote=='S',1,;
      0)
  endfunc

  func oAccodaNote_1_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_MEMO='M')
    endwith
   endif
  endfunc

  func oAccodaNote_1_72.mHide()
    with this.Parent.oContained
      return (.w_MEMO='M')
    endwith
  endfunc

  add object oAccodaDaFare_1_73 as StdCheck with uid="ZOGZDBZCXV",rtseq=64,rtrep=.f.,left=455, top=489, caption="Accoda le cose da fare",;
    ToolTipText = "Se attivo, accoda le cose da fare",;
    HelpContextID = 12056007,;
    cFormVar="w_AccodaDaFare", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAccodaDaFare_1_73.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAccodaDaFare_1_73.GetRadio()
    this.Parent.oContained.w_AccodaDaFare = this.RadioValue()
    return .t.
  endfunc

  func oAccodaDaFare_1_73.SetRadio()
    this.Parent.oContained.w_AccodaDaFare=trim(this.Parent.oContained.w_AccodaDaFare)
    this.value = ;
      iif(this.Parent.oContained.w_AccodaDaFare=='S',1,;
      0)
  endfunc

  func oAccodaDaFare_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_DAFARE='D')
    endwith
   endif
  endfunc

  func oAccodaDaFare_1_73.mHide()
    with this.Parent.oContained
      return (.w_DAFARE='D')
    endwith
  endfunc

  add object oVISUDI_1_74 as StdCheck with uid="QNKFDXRENN",rtseq=65,rtrep=.f.,left=455, top=512, caption="Dati udienza",;
    ToolTipText = "Se attivo, per ogni attivit� non di tipo Udienza/Da inser.prestaz./Assenza/Nota vengono riportate in stampa le date della prossima udienza da evadere e quella dell'ultima udienza evasa.",;
    HelpContextID = 164604074,;
    cFormVar="w_VISUDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVISUDI_1_74.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVISUDI_1_74.GetRadio()
    this.Parent.oContained.w_VISUDI = this.RadioValue()
    return .t.
  endfunc

  func oVISUDI_1_74.SetRadio()
    this.Parent.oContained.w_VISUDI=trim(this.Parent.oContained.w_VISUDI)
    this.value = ;
      iif(this.Parent.oContained.w_VISUDI=='S',1,;
      0)
  endfunc

  func oVISUDI_1_74.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_DatiPratica<>'S')
    endwith
  endfunc

  add object oCOMPIMPKEY_1_100 as StdField with uid="JVDRLEZYLN",rtseq=88,rtrep=.f.,;
    cFormVar = "w_COMPIMPKEY", cQueryName = "COMPIMPKEY",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 92579391,;
   bGlobalFont=.t.,;
    Height=22, Width=94, Left=848, Top=450

  func oCOMPIMPKEY_1_100.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oCOMPIMP_1_102 as StdField with uid="IZBYMBNXJI",rtseq=90,rtrep=.f.,;
    cFormVar = "w_COMPIMP", cQueryName = "COMPIMP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Componente",;
    HelpContextID = 92603354,;
   bGlobalFont=.t.,;
    Height=21, Width=486, Left=304, Top=388, InputMask=replicate('X',50), bHasZoom = .t. 

  func oCOMPIMP_1_102.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_IMPIANTO))
    endwith
   endif
  endfunc

  func oCOMPIMP_1_102.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  proc oCOMPIMP_1_102.mBefore
    with this.Parent.oContained
      .w_OLDCOMP=.w_COMPIMP
    endwith
  endproc

  proc oCOMPIMP_1_102.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M")
      endwith
  endproc

  proc oCOMPIMP_1_102.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oIMPIANTO_1_104 as StdField with uid="NSXUUBVBOI",rtseq=92,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice impianto",;
    HelpContextID = 183773909,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=99, Top=388, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="GSAG_MIM", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_1_104.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  func oIMPIANTO_1_104.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_104('Part',this)
      if .not. empty(.w_COMPIMPKEYREAD)
        bRes2=.link_1_101('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_1_104.ecpDrop(oSource)
    this.Parent.oContained.link_1_104('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_1_104.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_1_104'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIM',"Impianto",'GSAG_AAT.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oIMPIANTO_1_104.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_IMPIANTO
     i_obj.ecpSave()
  endproc

  add object oDENOM_PART_1_129 as StdField with uid="SKXCKTCAOH",rtseq=115,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(48), bMultilanguage =  .f.,;
    HelpContextID = 213539735,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=576, Top=8, InputMask=replicate('X',48)

  add object oDESPRA_1_131 as StdField with uid="DHZPCSQKFH",rtseq=116,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(75), bMultilanguage =  .f.,;
    HelpContextID = 16035274,;
   bGlobalFont=.t.,;
    Height=21, Width=535, Left=238, Top=311, InputMask=replicate('X',75)

  func oDESPRA_1_131.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object OUT as cp_outputCombo with uid="ZLERXIVPWT",left=167, top=541, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 53600230


  add object OUT1 as cp_outputCombo with uid="GROQGTHGYA",left=167, top=541, width=276,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 53600230

  add object oDesEnte_1_136 as StdField with uid="LTVJZFGKNZ",rtseq=117,rtrep=.f.,;
    cFormVar = "w_DesEnte", cQueryName = "DesEnte",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 63074870,;
   bGlobalFont=.t.,;
    Height=21, Width=582, Left=209, Top=311, InputMask=replicate('X',60)

  func oDesEnte_1_136.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oDESUTE_1_138 as StdField with uid="JXGTZBFHCK",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 214937034,;
   bGlobalFont=.t.,;
    Height=21, Width=630, Left=159, Top=415, InputMask=replicate('X',20)


  add object oBtn_1_142 as StdButton with uid="MBXAIGGBZE",left=719, top=518, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 17392166;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_142.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_142.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_143 as StdButton with uid="ISDICPRYHO",left=769, top=518, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 117079994;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_143.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESNOM_1_147 as StdField with uid="YBKTMCLSXL",rtseq=119,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 86420938,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=248, Top=107, InputMask=replicate('X',40)

  add object oDESTIPOL_1_150 as StdField with uid="XDLBTHYTYA",rtseq=120,rtrep=.f.,;
    cFormVar = "w_DESTIPOL", cQueryName = "DESTIPOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 41987454,;
   bGlobalFont=.t.,;
    Height=21, Width=620, Left=174, Top=234, InputMask=replicate('X',50)

  add object oDESCEN_1_154 as StdField with uid="OZEXVCNRGI",rtseq=121,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 80850378,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=481, Top=336, InputMask=replicate('X',40)

  add object oDESCRI_1_156 as StdField with uid="FFAMWOLEDT",rtseq=122,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 151104970,;
   bGlobalFont=.t.,;
    Height=21, Width=530, Left=165, Top=361, InputMask=replicate('X',40)

  func oDESCRI_1_156.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oDesUffi_1_158 as StdField with uid="NTCDFXGZMA",rtseq=123,rtrep=.f.,;
    cFormVar = "w_DesUffi", cQueryName = "DesUffi",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 179146186,;
   bGlobalFont=.t.,;
    Height=21, Width=582, Left=208, Top=388, InputMask=replicate('X',60)

  func oDesUffi_1_158.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oDESNOMGIU_1_162 as StdField with uid="LIIOHDEHWV",rtseq=124,rtrep=.f.,;
    cFormVar = "w_DESNOMGIU", cQueryName = "DESNOMGIU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 182015951,;
   bGlobalFont=.t.,;
    Height=21, Width=547, Left=243, Top=361, InputMask=replicate('X',30)

  func oDESNOMGIU_1_162.mHide()
    with this.Parent.oContained
      return (! ISALT())
    endwith
  endfunc

  add object oDESCRI_1_164 as StdField with uid="XTEJBUNQYP",rtseq=125,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 151104970,;
   bGlobalFont=.t.,;
    Height=21, Width=340, Left=258, Top=461, InputMask=replicate('X',60)

  func oDESCRI_1_164.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oDESPRA_1_170 as StdField with uid="BFDBYHAVEX",rtseq=126,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(75), bMultilanguage =  .f.,;
    HelpContextID = 16035274,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=237, Top=58, InputMask=replicate('X',75)

  func oDESPRA_1_170.mHide()
    with this.Parent.oContained
      return (Isahe() or Isahr())
    endwith
  endfunc


  add object oSTATO_1_172 as StdCombo with uid="HROEOHMDQV",rtseq=127,rtrep=.f.,left=397,top=35,width=183,height=21;
    , ToolTipText = "Stato";
    , HelpContextID = 35765722;
    , cFormVar="w_STATO",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Completata,"+"Evasa o completata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_172.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'P',;
    iif(this.value =7,'B',;
    iif(this.value =8,'K',;
    space(1))))))))))
  endfunc
  func oSTATO_1_172.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_172.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='N',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='I',3,;
      iif(this.Parent.oContained.w_STATO=='T',4,;
      iif(this.Parent.oContained.w_STATO=='F',5,;
      iif(this.Parent.oContained.w_STATO=='P',6,;
      iif(this.Parent.oContained.w_STATO=='B',7,;
      iif(this.Parent.oContained.w_STATO=='K',8,;
      0))))))))
  endfunc

  func oSTATO_1_172.mHide()
    with this.Parent.oContained
      return (!IsAlt() or .w_RISERVE='S')
    endwith
  endfunc

  add object oDATFIN_Cose_1_176 as StdField with uid="DETMKSVCXW",rtseq=129,rtrep=.f.,;
    cFormVar = "w_DATFIN_Cose", cQueryName = "DATFIN_Cose",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione per note e cose da fare",;
    HelpContextID = 76011415,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=733, Top=489

  func oDATFIN_Cose_1_176.mHide()
    with this.Parent.oContained
      return (.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
    endwith
  endfunc

  func oDATFIN_Cose_1_176.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI_Cose<=.w_DATFIN_Cose OR EMPTY(.w_DATFIN_Cose))
    endwith
    return bRes
  endfunc

  add object oDATINI_Cose_1_177 as StdField with uid="QWTXKWCFBP",rtseq=130,rtrep=.f.,;
    cFormVar = "w_DATINI_Cose", cQueryName = "DATINI_Cose",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione per note e cose da fare",;
    HelpContextID = 154458007,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=639, Top=489

  func oDATINI_Cose_1_177.mHide()
    with this.Parent.oContained
      return (.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
    endwith
  endfunc

  add object oSTADETTDOC_1_186 as StdCheck with uid="XGFLBDWCLX",rtseq=137,rtrep=.f.,left=455, top=512, caption="Dettaglio documenti associati",;
    ToolTipText = "Dettaglio documenti associati",;
    HelpContextID = 19827290,;
    cFormVar="w_STADETTDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTADETTDOC_1_186.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTADETTDOC_1_186.GetRadio()
    this.Parent.oContained.w_STADETTDOC = this.RadioValue()
    return .t.
  endfunc

  func oSTADETTDOC_1_186.SetRadio()
    this.Parent.oContained.w_STADETTDOC=trim(this.Parent.oContained.w_STADETTDOC)
    this.value = ;
      iif(this.Parent.oContained.w_STADETTDOC=='S',1,;
      0)
  endfunc

  func oSTADETTDOC_1_186.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oBtn_1_187 as StdButton with uid="MNVZAMDXTB",left=199, top=389, width=22,height=21,;
    CpPicture="bmp\ZOOM.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare per attributo";
    , HelpContextID = 124196394;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_1_187.Click()
      do gsag_kir with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_187.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsALT())
     endwith
    endif
  endfunc

  add object oDESRIS_1_191 as StdField with uid="WDOPCYWMCC",rtseq=141,rtrep=.f.,;
    cFormVar = "w_DESRIS", cQueryName = "DESRIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 8213046,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=248, Top=131, InputMask=replicate('X',80)

  func oDESRIS_1_191.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=12, Top=12,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="UASYQLKIAH",Visible=.t., Left=-1, Top=579,;
    Alignment=0, Width=862, Height=18,;
    Caption="Attenzione! la seq. della calculation deve essere inferiore a FiltProm in modo che questo ricalcoli dopo l'assegnamento, cancellando i secondi!!!"  ;
  , bGlobalFont=.t.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_121 as StdString with uid="JAHWAWKLCQ",Visible=.t., Left=12, Top=36,;
    Alignment=1, Width=83, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_122 as StdString with uid="JGUELPQNWG",Visible=.t., Left=345, Top=36,;
    Alignment=1, Width=47, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_123 as StdString with uid="UQSAZJMDFM",Visible=.t., Left=632, Top=60,;
    Alignment=1, Width=59, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_124 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=178, Top=12,;
    Alignment=1, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_125 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=178, Top=36,;
    Alignment=1, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_126 as StdString with uid="DAAABZBARA",Visible=.t., Left=254, Top=12,;
    Alignment=1, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_127 as StdString with uid="BTYOAEACQT",Visible=.t., Left=254, Top=36,;
    Alignment=1, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_128 as StdString with uid="ZGENJNNRPH",Visible=.t., Left=350, Top=8,;
    Alignment=1, Width=72, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_130 as StdString with uid="HZALIZNENK",Visible=.t., Left=12, Top=59,;
    Alignment=1, Width=83, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_130.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oStr_1_132 as StdString with uid="GXOWXVAARV",Visible=.t., Left=313, Top=85,;
    Alignment=1, Width=83, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_135 as StdString with uid="BSGKMHWZHM",Visible=.t., Left=12, Top=263,;
    Alignment=1, Width=83, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_137 as StdString with uid="QSNQCLUHKK",Visible=.t., Left=5, Top=463,;
    Alignment=1, Width=90, Height=18,;
    Caption="Tipo riserva:"  ;
  , bGlobalFont=.t.

  func oStr_1_137.mHide()
    with this.Parent.oContained
      return (.w_RISERVE<> 'S' or not IsAlt())
    endwith
  endfunc

  add object oStr_1_139 as StdString with uid="EIENBMKRMG",Visible=.t., Left=12, Top=418,;
    Alignment=1, Width=83, Height=16,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_140 as StdString with uid="GWTPKWGUGW",Visible=.t., Left=12, Top=286,;
    Alignment=1, Width=83, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_141 as StdString with uid="ITQYQODJLV",Visible=.t., Left=12, Top=315,;
    Alignment=1, Width=83, Height=18,;
    Caption="Ente/uff. giud.:"  ;
  , bGlobalFont=.t.

  func oStr_1_141.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oStr_1_144 as StdString with uid="HVQDALZAWB",Visible=.t., Left=19, Top=541,;
    Alignment=1, Width=143, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_146 as StdString with uid="RCYDFBJYOO",Visible=.t., Left=12, Top=111,;
    Alignment=1, Width=83, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_148 as StdString with uid="LBTIYCWQWU",Visible=.t., Left=12, Top=213,;
    Alignment=1, Width=83, Height=18,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_149 as StdString with uid="YJIZXRPWAZ",Visible=.t., Left=12, Top=238,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_151 as StdString with uid="JREGTPETYS",Visible=.t., Left=12, Top=315,;
    Alignment=1, Width=83, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_151.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_152 as StdString with uid="MMVSQAYOEN",Visible=.t., Left=543, Top=441,;
    Alignment=1, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_153 as StdString with uid="NUKLUCBHUI",Visible=.t., Left=462, Top=443,;
    Alignment=1, Width=51, Height=18,;
    Caption="Ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_155 as StdString with uid="OMPWFUNHHF",Visible=.t., Left=257, Top=340,;
    Alignment=1, Width=82, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_157 as StdString with uid="CKQRVEYIHP",Visible=.t., Left=52, Top=365,;
    Alignment=1, Width=43, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  func oStr_1_157.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_159 as StdString with uid="WPJSUGQTKU",Visible=.t., Left=3, Top=392,;
    Alignment=1, Width=92, Height=18,;
    Caption="Ufficio/sezione:"  ;
  , bGlobalFont=.t.

  func oStr_1_159.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oStr_1_160 as StdString with uid="TUXQEFOWXW",Visible=.t., Left=3, Top=392,;
    Alignment=1, Width=92, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  func oStr_1_160.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_1_161 as StdString with uid="ZJKSIFGZBN",Visible=.t., Left=228, Top=388,;
    Alignment=1, Width=75, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_1_161.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_1_163 as StdString with uid="LXDDNGIUPI",Visible=.t., Left=60, Top=463,;
    Alignment=1, Width=35, Height=18,;
    Caption="Piano:"  ;
  , bGlobalFont=.t.

  func oStr_1_163.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_165 as StdString with uid="YAKKYAVGYE",Visible=.t., Left=50, Top=365,;
    Alignment=1, Width=45, Height=18,;
    Caption="Giudice:"  ;
  , bGlobalFont=.t.

  func oStr_1_165.mHide()
    with this.Parent.oContained
      return (! ISALT())
    endwith
  endfunc

  add object oStr_1_166 as StdString with uid="KSBGPQZUEI",Visible=.t., Left=13, Top=337,;
    Alignment=1, Width=81, Height=18,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  func oStr_1_166.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_167 as StdString with uid="VRCOAPPYPP",Visible=.t., Left=691, Top=36,;
    Alignment=1, Width=57, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_167.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  add object oStr_1_168 as StdString with uid="QVTFQRDCTZ",Visible=.t., Left=12, Top=84,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_169 as StdString with uid="DNFJJECVYE",Visible=.t., Left=234, Top=212,;
    Alignment=1, Width=125, Height=18,;
    Caption="Riferimento persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_171 as StdString with uid="GOADFKEWFS",Visible=.t., Left=631, Top=107,;
    Alignment=1, Width=45, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_171.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_174 as StdString with uid="KMXRIRFDUM",Visible=.t., Left=698, Top=489,;
    Alignment=1, Width=31, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  func oStr_1_174.mHide()
    with this.Parent.oContained
      return (.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
    endwith
  endfunc

  add object oStr_1_175 as StdString with uid="DACIAVJCND",Visible=.t., Left=604, Top=489,;
    Alignment=1, Width=31, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  func oStr_1_175.mHide()
    with this.Parent.oContained
      return (.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
    endwith
  endfunc

  add object oStr_1_192 as StdString with uid="HRAEFMYVHV",Visible=.t., Left=-19, Top=133,;
    Alignment=1, Width=114, Height=18,;
    Caption="Soggetto interno:"  ;
  , bGlobalFont=.t.

  func oStr_1_192.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_193 as StdString with uid="TGWAUCUBXZ",Visible=.t., Left=626, Top=133,;
    Alignment=1, Width=50, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_193.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_194 as StdString with uid="LIJLPIXOSP",Visible=.t., Left=55, Top=59,;
    Alignment=1, Width=40, Height=18,;
    Caption="Num.:"  ;
  , bGlobalFont=.t.

  func oStr_1_194.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_195 as StdString with uid="JMLLMGFFZD",Visible=.t., Left=217, Top=60,;
    Alignment=1, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_195.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oBox_1_145 as StdBox with uid="EGABWCSCSO",left=7, top=486, width=304,height=49
enddefine
define class tgsag_satPag2 as StdContainer
  Width  = 942
  height = 563
  stdWidth  = 942
  stdheight = 563
  resizeXpos=429
  resizeYpos=185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPATIPRIS_2_1 as StdCombo with uid="TRCNSMSKIW",value=4,rtseq=108,rtrep=.f.,left=128,top=37,width=76,height=21;
    , HelpContextID = 1810615;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPATIPRIS_2_1.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_2_1.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_2_1.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc

  func oPATIPRIS_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!isAlt())
    endwith
   endif
  endfunc

  func oPATIPRIS_2_1.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc


  add object oPATIPRIS_2_2 as StdCombo with uid="OWYFYNUFGT",value=3,rtseq=109,rtrep=.f.,left=128,top=37,width=76,height=21;
    , HelpContextID = 1810615;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPATIPRIS_2_2.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'R',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oPATIPRIS_2_2.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_2_2.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='',3,;
      0)))
  endfunc

  func oPATIPRIS_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (isAlt())
    endwith
   endif
  endfunc

  func oPATIPRIS_2_2.mHide()
    with this.Parent.oContained
      return (!isAlt())
    endwith
  endfunc

  add object oCODPART_2_3 as StdField with uid="HQCEOQCDYV",rtseq=110,rtrep=.f.,;
    cFormVar = "w_CODPART", cQueryName = "CODPART",nZero=5,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 251292710,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=208, Top=37, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPART"

  func oCODPART_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPART_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPART_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCODPART_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oGRUPART_2_4 as StdField with uid="WIRZQPFOUM",rtseq=111,rtrep=.f.,;
    cFormVar = "w_GRUPART", cQueryName = "GRUPART",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo inesistente o non associato al partecipante",;
    HelpContextID = 251363174,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=277, Top=64, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPART"

  func oGRUPART_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPTIPRIS='P' AND Not Empty(.w_CODPART) )
    endwith
   endif
  endfunc

  func oGRUPART_2_4.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  func oGRUPART_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPART_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPART_2_4.mZoom
      with this.Parent.oContained
        GSAR1BGP(this.Parent.oContained,  "", .w_CODPART, "", "GRUPART" )
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFLESGR_2_5 as StdCheck with uid="SNJJPXEYPH",rtseq=112,rtrep=.f.,left=277, top=65, caption="Esplodi gruppo",;
    ToolTipText = "Se attivo visualizza anche appuntamenti delle persone del gruppo",;
    HelpContextID = 10651306,;
    cFormVar="w_FLESGR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLESGR_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLESGR_2_5.GetRadio()
    this.Parent.oContained.w_FLESGR = this.RadioValue()
    return .t.
  endfunc

  func oFLESGR_2_5.SetRadio()
    this.Parent.oContained.w_FLESGR=trim(this.Parent.oContained.w_FLESGR)
    this.value = ;
      iif(this.Parent.oContained.w_FLESGR=='S',1,;
      0)
  endfunc

  func oFLESGR_2_5.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'G' or Empty(.w_CODPART) )
    endwith
  endfunc

  add object oDENOM_PART_2_7 as StdField with uid="SGXVKIVHDX",rtseq=113,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(48), bMultilanguage =  .f.,;
    HelpContextID = 213539735,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=277, Top=37, InputMask=replicate('X',48)


  add object oBtn_2_9 as StdButton with uid="IVNIYPCRKD",left=680, top=500, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 17392166;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_2_10 as StdButton with uid="DBRVUGYYTH",left=730, top=500, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 117079994;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object PART_ZOOM as cp_szoombox with uid="WODZOSNAJX",left=46, top=93, width=731,height=398,;
    caption='PART_ZOOM',;
   bGlobalFont=.t.,;
    cTable="DIPENDEN",cZoomFile="GSAG2MPP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Init,w_DATINIZ Changed,w_obsodat1 Changed",;
    nPag=2;
    , HelpContextID = 119585771

  add object oDATINIZ_2_17 as StdField with uid="KXZLKOTRZF",rtseq=142,rtrep=.f.,;
    cFormVar = "w_DATINIZ", cQueryName = "DATINIZ",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 154902986,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=561, Top=65

  add object oobsodat1_2_18 as StdCheck with uid="TANZHKUOWP",rtseq=143,rtrep=.f.,left=673, top=65, caption="Solo obsoleti",;
    ToolTipText = "Se attivo, visualizza solo i partecipanti obsoleti",;
    HelpContextID = 5009943,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oobsodat1_2_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_2_18.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_2_18.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc

  add object oStr_2_6 as StdString with uid="ZRYUFIMMYG",Visible=.t., Left=51, Top=37,;
    Alignment=1, Width=72, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="PNAWUEUQMA",Visible=.t., Left=140, Top=65,;
    Alignment=1, Width=132, Height=18,;
    Caption="Gruppo partecipante:"  ;
  , bGlobalFont=.t.

  func oStr_2_8.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  add object oStr_2_16 as StdString with uid="XVUBYBVEQE",Visible=.t., Left=464, Top=65,;
    Alignment=1, Width=92, Height=18,;
    Caption="Data di controllo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_sat','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
