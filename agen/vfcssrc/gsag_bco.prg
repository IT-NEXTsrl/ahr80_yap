* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bco                                                        *
*              Controlla tipi e attivit�                                       *
*                                                                              *
*      Author: Zucchetti TAM S.p.a.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_66]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-01                                                      *
* Last revis.: 2016-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bco",oParentObject,m.pParam)
return(i_retval)

define class tgsag_bco as StdBatch
  * --- Local variables
  pParam = space(1)
  w_OK = .f.
  w_OBJ = .NULL.
  w_CODRIS = space(5)
  w_CODGRU = space(5)
  w_CODCAL = space(5)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_ORAINI = space(5)
  w_ORAFIN = space(5)
  w_RETVAL = 0
  w_GSAG_MPA = .NULL.
  w_ResMsg = .NULL.
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_ANNULLA = .f.
  w_GSAG_MDA = .NULL.
  w_RicalcolaPrz = space(1)
  w_PrzCalcolato = 0
  w_MsgWarning = .f.
  w_MAXCODCONT = space(5)
  w_MAXROWORD = 0
  w_MESS = space(0)
  w_GSAG_MDP = .NULL.
  w_GSAG_MDP = .NULL.
  w_ROWCAU = 0
  w_SIDOC = .f.
  w_oMESS = .NULL.
  w_COD_RESP = space(5)
  w_GSAG_MDA = .NULL.
  w_SIDOC = .f.
  w_SERDOC = space(10)
  w_oMESS = .NULL.
  w_CODRES = space(5)
  w_ROWORD = 0
  w_MINUTI = 0
  w_GSAG_MDA = .NULL.
  w_OKLOG = .f.
  w_NOCODCLI = space(15)
  w_NOTIPO = space(1)
  w_GSAG_MDD = .NULL.
  w_CODAZI = space(5)
  w_CAUOFFE = space(5)
  w_Err_Log = .NULL.
  w_CodRis = space(5)
  w_GSAG_MPR = .NULL.
  w_STATO = space(1)
  w_TELINT = space(18)
  w_EVSERIAL = space(10)
  w_IMDATINI = ctod("  /  /  ")
  w_IMDATFIN = ctod("  /  /  ")
  w_DTOBSO = ctod("  /  /  ")
  w_CACODIMP = space(15)
  w_DESCOM = space(50)
  w_DTOINI = ctod("  /  /  ")
  w_oERRORLOG = .NULL.
  w_GSAG_MCP = .NULL.
  w_RIGA = 0
  w_CONPRA = space(15)
  w_NOMCUR = space(10)
  w_CAMPO = space(10)
  w_CODPRA = space(15)
  w_DESPRA = space(100)
  w_CODART = space(20)
  w_DESATT = space(40)
  w_ORDINA = space(1)
  w_CODATT = space(20)
  w_FLUNIV = space(1)
  w_NUMPRE = 0
  w_DATINI = ctod("  /  /  ")
  w_OCCORR = 0
  w_OLDPRA = space(15)
  w_CONFERMA = .f.
  w_MSG = space(0)
  w_ANNDOC = space(4)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_CAUATT = space(20)
  * --- WorkFile variables
  OFFDATTI_idx=0
  PAR_AGEN_idx=0
  OFF_NOMI_idx=0
  ATT_DCOL_idx=0
  TIP_DOCU_idx=0
  PAR_OFFE_idx=0
  CAU_ATTI_idx=0
  ANEVENTI_idx=0
  OFF_ATTI_idx=0
  OFF_PART_idx=0
  DIPENDEN_idx=0
  NOM_CONT_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pParam = C :   Tipi attivit�
    *     pParam = A :   Attivit� checkform
    *     pParam = K :   Replace end
    *     pParam = W :   Checkform da GSAG_KWA wizard attivit�
    this.w_OK = .T.
    do case
      case this.pParam="C"
        * --- TIPI ATTIVITA'
        * --- Stato COMPLETATA e Pratica NON obbligatoria
        if this.oParentObject.w_RESCHK <> -1 AND ((this.oParentObject.w_CASTAATT = "P" AND this.oParentObject.w_CACHKOBB="N") or (this.oParentObject.w_FLPRAT="S" and this.oParentObject.w_CACHKOBB=" ")) AND Isalt()
          if this.oParentObject.w_FLPRAT="S" 
            ah_ErrorMsg("Causale documento con dati pratica, la pratica � obbligatoria")
          else
            ah_ErrorMsg("La pratica � obbligatoria")
          endif
          this.oParentObject.w_RESCHK = -1
          this.oParentObject.SetControlsValue()
          this.w_OBJ = this.oParentObject.Getctrl("w_CACHKOBB")
          if vartype(this.w_OBJ)="O"
            * --- Viene posizionato il cursore sul campo CACHKOBB di GSAG_MCA (Tipi Attivit�)
            this.w_OBJ.Setfocus()     
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CAFLNSAP="N" AND empty(this.oParentObject.w_CACAUDOC) AND empty(this.oParentObject.w_CACAUACQ) and ! Isahe()
          * --- Non � stata impostata nessuna causale
          ah_ErrorMsg("Inserire il tipo del documento interno")
          this.oParentObject.w_RESCHK = -1
        endif
        * --- Controllo sulla composizione della descrizione attivit� in agenda
        if this.oParentObject.w_RESCHK <> -1 AND OCCURS("S", this.oParentObject.w_CAFLATTI+this.oParentObject.w_CAFLPART+this.oParentObject.w_CAFLPRAT+this.oParentObject.w_CAFLSOGG+this.oParentObject.w_CAFLGIUD+this.oParentObject.w_CAFLNOMI) > 2
          if NOT ah_YesNo("Attenzione! La combinazione scelta potrebbe determinare una descrizione dell'attivit� troppo lunga ed � possibile che sia troncata in alcune visualizzazioni dell'agenda. Si vuole procedere comunque?")
            this.oParentObject.w_RESCHK = -1
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CAFLATTI="S" AND OCCURS(ALLTRIM(STR(this.oParentObject.w_CAFLATTN)), ALLTRIM(STR(this.oParentObject.w_CAFLATTN))+ALLTRIM(STR(this.oParentObject.w_CAFLPARN))+ALLTRIM(STR(this.oParentObject.w_CAFLPRAN))+ALLTRIM(STR(this.oParentObject.w_CAFLSOGN))+ALLTRIM(STR(this.oParentObject.w_CAFLGIUN))+ALLTRIM(STR(this.oParentObject.w_CAFLNOMN))) > 1
          ah_ErrorMsg("Nella composizione della descrizione attivit� in agenda non possono esserci due o pi� opzioni con il medesimo ordinamento!")
          this.oParentObject.w_RESCHK = -1
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CAFLPART="S" AND OCCURS(ALLTRIM(STR(this.oParentObject.w_CAFLPARN)), ALLTRIM(STR(this.oParentObject.w_CAFLATTN))+ALLTRIM(STR(this.oParentObject.w_CAFLPARN))+ALLTRIM(STR(this.oParentObject.w_CAFLPRAN))+ALLTRIM(STR(this.oParentObject.w_CAFLSOGN))+ALLTRIM(STR(this.oParentObject.w_CAFLGIUN))+ALLTRIM(STR(this.oParentObject.w_CAFLNOMN))) > 1
          ah_ErrorMsg("Nella composizione della descrizione attivit� in agenda non possono esserci due o pi� opzioni con il medesimo ordinamento!")
          this.oParentObject.w_RESCHK = -1
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CAFLPRAT="S" AND OCCURS(ALLTRIM(STR(this.oParentObject.w_CAFLPRAN)), ALLTRIM(STR(this.oParentObject.w_CAFLATTN))+ALLTRIM(STR(this.oParentObject.w_CAFLPARN))+ALLTRIM(STR(this.oParentObject.w_CAFLPRAN))+ALLTRIM(STR(this.oParentObject.w_CAFLSOGN))+ALLTRIM(STR(this.oParentObject.w_CAFLGIUN))+ALLTRIM(STR(this.oParentObject.w_CAFLNOMN))) > 1
          ah_ErrorMsg("Nella composizione della descrizione attivit� in agenda non possono esserci due o pi� opzioni con il medesimo ordinamento!")
          this.oParentObject.w_RESCHK = -1
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CAFLSOGG="S" AND OCCURS(ALLTRIM(STR(this.oParentObject.w_CAFLSOGN)), ALLTRIM(STR(this.oParentObject.w_CAFLATTN))+ALLTRIM(STR(this.oParentObject.w_CAFLPARN))+ALLTRIM(STR(this.oParentObject.w_CAFLPRAN))+ALLTRIM(STR(this.oParentObject.w_CAFLSOGN))+ALLTRIM(STR(this.oParentObject.w_CAFLGIUN))+ALLTRIM(STR(this.oParentObject.w_CAFLNOMN))) > 1
          ah_ErrorMsg("Nella composizione della descrizione attivit� in agenda non possono esserci due o pi� opzioni con il medesimo ordinamento!")
          this.oParentObject.w_RESCHK = -1
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CAFLGIUD="S" AND OCCURS(ALLTRIM(STR(this.oParentObject.w_CAFLGIUN)), ALLTRIM(STR(this.oParentObject.w_CAFLATTN))+ALLTRIM(STR(this.oParentObject.w_CAFLPARN))+ALLTRIM(STR(this.oParentObject.w_CAFLPRAN))+ALLTRIM(STR(this.oParentObject.w_CAFLSOGN))+ALLTRIM(STR(this.oParentObject.w_CAFLGIUN))+ALLTRIM(STR(this.oParentObject.w_CAFLNOMN))) > 1
          ah_ErrorMsg("Nella composizione della descrizione attivit� in agenda non possono esserci due o pi� opzioni con il medesimo ordinamento!")
          this.oParentObject.w_RESCHK = -1
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CAFLNOMI="S" AND OCCURS(ALLTRIM(STR(this.oParentObject.w_CAFLNOMN)), ALLTRIM(STR(this.oParentObject.w_CAFLATTN))+ALLTRIM(STR(this.oParentObject.w_CAFLPARN))+ALLTRIM(STR(this.oParentObject.w_CAFLPRAN))+ALLTRIM(STR(this.oParentObject.w_CAFLSOGN))+ALLTRIM(STR(this.oParentObject.w_CAFLGIUN))+ALLTRIM(STR(this.oParentObject.w_CAFLNOMN))) > 1
          ah_ErrorMsg("Nella composizione della descrizione attivit� in agenda non possono esserci due o pi� opzioni con il medesimo ordinamento!")
          this.oParentObject.w_RESCHK = -1
        endif
        if this.oParentObject.w_RESCHK <> -1 
          this.w_GSAG_MDP = this.oParentObject.GSAG_MDP
          this.w_GSAG_MDP.Exec_Select("_TMP_MDP","COUNT(*) AS CONTA,t_DFCODRIS AS DFCODRIS","Not Empty(NVL(t_DFCODRIS,' '))","t_DFCODRIS","t_DFCODRIS","CONTA>1")     
          if Reccount("_TMP_MDP")>0
            ah_ErrorMsg("Attenzione, partecipante %1 indicato pi� volte !","!","",DFCODRIS)
            this.oParentObject.w_RESCHK = -1
          endif
          Use in _TMP_MDP
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CAFLNSAP="N" AND EMPTY(this.oParentObject.w_CACAUACQ) 
          * --- Dobbiamo verificare che non ci siano prestazioni con documento di vendita o entrambi
          this.w_OBJ = this.oParentObject
          this.w_OBJ.Exec_Select("CTRL_RIGHE","MIN(t_CPROWORD) AS ROWORD","NVL(t_CATIPRIG,' ')='A' OR NVL(t_CATIPRIG,' ')='E' ","","","")     
          if Reccount("CTRL_RIGHE")>0
            ah_ErrorMsg("Attenzione, ci sono prestazioni per cui � necessario specificare una causale documento d'acquisto! Inserire la causale oppure correggere le righe","!","",)
            this.oParentObject.w_RESCHK = -1
          endif
          USE IN SELECT("CTRL_RIGHE")
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CAFLNSAP="N" AND EMPTY(this.oParentObject.w_CACAUDOC)
          * --- Dobbiamo verificare che non ci siano prestazioni con documento di vendita o entrambi
          this.w_OBJ = this.oParentObject
          this.w_OBJ.Exec_Select("CTRL_RIGHE","MIN(t_CPROWORD) AS ROWORD","NVL(t_CATIPRIG,' ')='D' OR NVL(t_CATIPRIG,' ')='E' ","","","")     
          if Reccount("CTRL_RIGHE")>0
            ah_ErrorMsg("Attenzione, ci sono prestazioni per cui � necessario specificare una causale documento di vendita! Inserire la causale oppure correggere le righe","!","",)
            this.oParentObject.w_RESCHK = -1
          endif
          USE IN SELECT("CTRL_RIGHE")
        endif
        if this.oParentObject.w_PAFLVISI="S"
          this.w_GSAG_MDP = this.oParentObject.GSAG_MDP
          if this.oParentObject.w_PAFLVISI = "S" and this.w_GSAG_MDP.Search("NVL(t_DFTIPRIS, 0)= 1 AND NOT DELETED() AND Empty(Nvl(t_DFGRURIS,' ')) and Not Empty(Nvl(t_DFCODRIS,' '))") <> -1
            ah_ErrorMsg("Attenzione! Per una o pi� persone occorre indicare il gruppo di riferimento!")
            this.oParentObject.w_RESCHK = -1
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND NOT EMPTY(this.oParentObject.w_CADTOBSO)
          * --- Controllo su data obsolescenza
          * --- Select from OFF_ATTI
          i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ATSERIAL,ATDATFIN,ATCAUATT  from "+i_cTable+" OFF_ATTI ";
                +" where "+cp_ToStrODBC(this.oParentObject.w_CADTOBSO)+"<ATDATFIN AND "+cp_ToStrODBC(this.oParentObject.w_CACODICE)+"=ATCAUATT";
                 ,"_Curs_OFF_ATTI")
          else
            select ATSERIAL,ATDATFIN,ATCAUATT from (i_cTable);
             where this.oParentObject.w_CADTOBSO<ATDATFIN AND this.oParentObject.w_CACODICE=ATCAUATT;
              into cursor _Curs_OFF_ATTI
          endif
          if used('_Curs_OFF_ATTI')
            select _Curs_OFF_ATTI
            locate for 1=1
            do while not(eof())
            ah_ErrorMsg("Attenzione, esistono una o pi� attivit� con data fine superiore alla data indicata!")
            this.oParentObject.w_CADTOBSO = CP_CHARTODATE("  -  -    ")
            this.oParentObject.w_RESCHK = -1
            exit
              select _Curs_OFF_ATTI
              continue
            enddo
            use
          endif
        endif
      case this.pParam="A" OR this.pParam="W"
        * --- ATTIVITA'
        if this.pParam="A"
          * --- Controllo su dettaglio prestazioni solo per GSAG_AAT
          this.w_GSAG_MDA = this.oParentObject.GSAG_MDA
          if lower(this.oParentObject.cFunction)="load" and this.w_GSAG_MDA.Numrow() =0 AND this.oParentObject.w_FLNSAP<>"S" 
            * --- Read from CAU_ATTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAU_ATTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAU_ATTI_idx,2],.t.,this.CAU_ATTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CPROWNUM"+;
                " from "+i_cTable+" CAU_ATTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_ATCAUATT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CPROWNUM;
                from (i_cTable) where;
                    CACODICE = this.oParentObject.w_ATCAUATT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ROWCAU = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_ROWCAU>0
              this.w_GSAG_MDA.FirstRowDel(.t.)     
              if this.w_GSAG_MDA.RowStatus()<>"D" or Ah_yesno("Attenzione dettaglio prestazioni non compilato, si desidera applicare modello previsto nella tipologia attivit�?")
                * --- Se non valorizzato potrebbe essere perch� l'utente non � entrato in pagina 2
                 
 this.oParentObject.NotifyEvent("ActivatePage 2") 
 This.oParentObject.oPgFrm.ActivePage=1 
              endif
            endif
          endif
          if this.oParentObject.w_FLNSAP<>"S" AND !Isalt()
            this.oParentObject.NotifyEvent("Abbinacontratti")
          endif
          * --- Controllo su dettaglio partecipanti per GSAG_AAT
          this.w_GSAG_MPA = this.oParentObject.GSAG_MPA
          if this.oParentObject.w_PAFLVISI = "S" and this.w_GSAG_MPA.Search("NVL(t_PATIPRIS, 0)= 1 AND NOT DELETED() AND Empty(Nvl(t_PAGRURIS,' ')) and Not Empty(Nvl(t_PACODRIS,' '))") <> -1
            ah_ErrorMsg("Attenzione, visualizzazione da struttura aziendale attiva: impossibile salvare attivit� con partecipanti di tipo persona senza gruppo di riferimento. ","!","")
            this.oParentObject.w_RESCHK = -1
          endif
          if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_FLNSAP<>"S"
            this.w_GSAG_MPA = this.oParentObject.GSAG_MPA
            this.w_GSAG_MPA.Exec_Select("PARTECIP","t_PACODRIS as PACODRIS","","","","")     
            this.w_GSAG_MDA = this.oParentObject.GSAG_MDA
            this.w_GSAG_MDA.Exec_Select("PRESTAZ","t_DACODRES AS DACODRES","t_DACODRES NOT IN (SELECT PACODRIS FROM PARTECIP) AND not(Empty(T_CPROWORD)) and (not(Empty(T_DACODATT)) or not(Empty(T_DACODICE)))","","","")     
            if RECCOUNT("PRESTAZ")>0
              if NOT ah_YesNo("Attenzione! Nel dettaglio prestazioni sono state inserite persone/risorse non presenti nel dettaglio partecipanti! Proseguire ugualmente?")
                this.oParentObject.w_RESCHK = -1
              endif
            endif
            Use in PARTECIP
            Use in PRESTAZ
          endif
        else
          * --- Da GSAG_KWA - Controllo su dettaglio partecipanti per GSAG_AAT
          if this.oParentObject.w_PAFLVISI = "S"
            SELECT * FROM (this.oParentObject.w_ZOOMPART.ccursor) WHERE DPTIPRIS="P" AND EMPTY(DPGRUPRE) AND XCHK=1 INTO CURSOR TMP_PART 
            if Used("TMP_PART") AND RECCOUNT("TMP_PART")>0
              ah_ErrorMsg("Attenzione, visualizzazione da struttura aziendale attiva: impossibile salvare attivit� con partecipanti di tipo persona senza gruppo di riferimento. ","!","")
              this.oParentObject.w_RESCHK = -1
            endif
            USE IN SELECT("TMP_PART")
          endif
        endif
        if this.oParentObject.w_ATSTATUS = "P" AND this.pParam="A"
          * --- Stato PRESTAZIONE e nessuna prestazione
          * --- Verifico partecipanti
          this.oParentObject.w_okgen = .T.
          if lower(this.oParentObject.cFunction) = "edit" and this.oParentObject.w_FLNSAP<>"S"
            * --- Select from ATT_DCOL
            i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2],.t.,this.ATT_DCOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATT_DCOL ";
                  +" where ADSERIAL= "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL)+" and (ADTIPDOC= 'A' or ADTIPDOC='V' )";
                   ,"_Curs_ATT_DCOL")
            else
              select * from (i_cTable);
               where ADSERIAL= this.oParentObject.w_ATSERIAL and (ADTIPDOC= "A" or ADTIPDOC="V" );
                into cursor _Curs_ATT_DCOL
            endif
            if used('_Curs_ATT_DCOL')
              select _Curs_ATT_DCOL
              locate for 1=1
              do while not(eof())
              this.w_SIDOC = .t.
              exit
                select _Curs_ATT_DCOL
                continue
              enddo
              use
            endif
            if this.w_SIDOC
              this.w_SIDOC = .f.
              this.w_oMESS=createobject("ah_message")
              if !Isalt()
                this.w_oMESS.AddMsgPartNL("Attivit� completata soggetta a prestazione.%0 I documenti generati in precedenza verranno cancellati.%0 Si desidera rigenerare i documenti associati?")     
              else
                this.w_oMESS.AddMsgPartNL("Procedo ad aggiornare le prestazioni?%0(Se le modifiche riguardano solo l'attivit� si puo rispondere di No)")     
              endif
              if !this.oParentObject.w_NOMSGEN and ! this.w_oMESS.ah_YesNo()
                this.oParentObject.w_okgen = .f.
              endif
              if VARTYPE(g_SCHEDULER)="C" and g_SCHEDULER="S"
                g_MSG=" "
              endif
            endif
          endif
          * --- Se � attivo il check Controllo su ins./variaz. prestazioni nei Parametri attivit� e se l'utente non � amministratore
          if this.oParentObject.w_RESCHK <> -1 AND ISALT() AND this.oParentObject.w_FLPRES="S" AND NOT Cp_IsAdministrator()
            this.w_GSAG_MDA.Firstrow()     
            do while Not this.w_GSAG_MDA.Eof_Trs()
              this.w_GSAG_MDA.SetRow()     
              * --- Se attivo il check Controllo su prestazioni (nelle Persone) e se la data della prestazione � al di fuori dell'intervallo definito per il relativo responsabile
              if this.w_GSAG_MDA.w_CHKDATAPRE="S" AND (this.oparentobject.w_DATINI < date() - this.w_GSAG_MDA.w_GG_PRE OR this.oparentobject.w_DATINI > date() + this.w_GSAG_MDA.w_GG_SUC)
                ah_ErrorMsg("Data non consentita dai controlli relativi al responsabile")
                this.oParentObject.w_RESCHK = -1
                exit
              endif
              this.w_GSAG_MDA.NextRow()     
            enddo
          endif
          if this.oParentObject.w_RESCHK <> -1
            if this.w_GSAG_MDA.Search("(Not Empty(NVL(t_DACODATT, SPACE(20))) or Not Empty(NVL(t_DACODICE, SPACE(41)))) AND NOT DELETED()") = -1
              ah_ErrorMsg("Lo stato non pu� essere 'Completata' se non � presente almeno una prestazione")
              this.oParentObject.w_RESCHK = -1
              this.w_OBJ = this.oParentObject.GSAG_MDA.Getbodyctrl("w_DACODRES")
              if vartype(this.w_OBJ)="O"
                * --- Viene attivata la pag. 2 di GSAG_AAT (Attivit�)
                this.oParentObject.oPgFrm.ActivePage=2
                * --- Viene posizionato il cursore sul campo DACODRES del figlio GSAG_MDA che ha seq.=1 (in caso contrario non funziona)
                this.w_OBJ.Setfocus()     
              endif
            endif
            if !Isalt()
              this.w_GSAG_MDA = this.oParentObject.GSAG_MDA
              this.w_GSAG_MDA.Exec_Select("CONTROLLO","t_CPROWORD",(" (Not Empty(NVL(t_DACODICE, SPACE(41))) or Not Empty(NVL(t_DACODATT, SPACE(20)))) AND NOT DELETED() and (Nvl(t_DATIPRIG,' ') $ 'D-E' OR (Nvl(t_DATIPRI2,' ') $ 'D-E') and Isalt())") ,"","","")     
              if Reccount("CONTROLLO") >0 and this.oParentObject.w_RESCHK<> -1 and Empty(this.oParentObject.w_ATCAUDOC)
                ah_ErrorMsg("Attenzione: per generare il documento di vendita � necessario indicare la causale specifica nei dati di testata per la riga %1","!","", t_CPROWORD)
                this.oParentObject.w_RESCHK = -1
              endif
              USE IN SELECT("CONTROLLO")
              this.w_GSAG_MDA.Exec_Select("ANALITICA","t_CPROWORD",(" (Not Empty(NVL(t_DACODICE, SPACE(41))) OR Not Empty(NVL(t_DACODATT, SPACE(20)))  ) AND NOT DELETED() and (Nvl(t_DATIPRIG,' ') $ 'A-E' OR (Nvl(t_DATIPRI2,' ') $ 'A-E') and Isalt())") ,"","","")     
              if Reccount("ANALITICA") >0 and this.oParentObject.w_RESCHK <> -1 and ((Empty(this.oParentObject.w_ATCAUACQ) and this.oParentObject.w_FLANAL<>"S") or this.oParentObject.w_FLANAL="S")
                if this.oParentObject.w_FLANAL="S"
                  ah_ErrorMsg("Attenzione: il tipo attivit� gestisce il movimento di analitica, impossibile generare il documento di acquisto per la riga %1","!","", t_CPROWORD)
                  this.oParentObject.w_RESCHK = -1
                else
                  ah_ErrorMsg("Attenzione: per generare il documento di acquisto � necessario indicare la causale specifica nei dati di testata per la riga %1","!","", t_CPROWORD)
                  this.oParentObject.w_RESCHK = -1
                endif
              endif
              USE IN SELECT("ANALITICA")
            else
              if lower(this.oParentObject.cFunction) = "edit" and this.oParentObject.w_FLNSAP<>"S"
                this.w_SERDOC = this.oParentObject.w_ATRIFDOC
                * --- Select from GSAR_BDK
                do vq_exec with 'GSAR_BDK',this,'_Curs_GSAR_BDK','',.f.,.t.
                if used('_Curs_GSAR_BDK')
                  select _Curs_GSAR_BDK
                  locate for 1=1
                  do while not(eof())
                  this.w_SIDOC = .t.
                  exit
                    select _Curs_GSAR_BDK
                    continue
                  enddo
                  use
                endif
                if this.w_SIDOC
                  this.w_oMESS=createobject("ah_message")
                  this.w_oMESS.AddMsgPartNL("L'attivit� ha generato un documento nel quale � stata evasa almeno una riga di un altro documento! Procedere con la modifica riaprendo le righe precedentemente evase?")     
                  if ! this.w_oMESS.ah_YesNo()
                    this.oParentObject.w_RESCHK = -1
                  endif
                endif
              endif
            endif
          endif
          if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CHKPRE="S" AND this.oParentObject.w_FLNSAP<>"S"
            this.w_GSAG_MDA = this.oParentObject.GSAG_MDA
            this.w_MINUTI = cp_ROUND(((this.oParentObject.w_ATDATFIN-this.oParentObject.w_ATDATINI)/60),0)
            this.w_GSAG_MDA.Exec_Select("_TMP_MDA","SUM((T_DAOREEFF*60)+t_DAMINEFF) AS MINUTI, T_CPROWORD AS ROWORD, t_DACODRES AS DACODRES","NOT EMPTY(NVL(t_DACODRES,'')) AND (NOT EMPTY(NVL(t_DACODATT,'')) OR NOT EMPTY(NVL(t_DACODICE,'')))","DACODRES","DACODRES","")     
             
 select _TMP_MDA 
 Go Top 
 Scan
            if (_TMP_MDA.MINUTI)<>this.w_MINUTI
              this.w_CODRES = _TMP_MDA.DACODRES
              this.w_ROWORD = _TMP_MDA.ROWORD
              this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Rilevata incongruenza tra la durata effettiva delle prestazioni e la durata dell'attivit� per il partecipante %1", ALLTRIM(this.w_CODRES)))     
              this.w_RESOCON1 = this.w_RESOCON1 + this.w_ResMsg.ComposeMessage()
              this.w_OKLOG = .T.
            endif
            Endscan
            this.w_ResMsg.AddMsgPartNL("Procedere con il salvataggio dell'attivit� comunque?")     
            this.w_RESOCON1 = this.w_RESOCON1 + this.w_ResMsg.ComposeMessage()
            if this.w_OKLOG and !(VARTYPE(g_SCHEDULER)="C" and g_SCHEDULER="S")
              do GSUT_KLG with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              this.w_ANNULLA = .T.
            endif
             
 USE IN _TMP_MDA
            this.oParentObject.w_RESCHK = IIF(Not this.w_ANNULLA, -1, 0)
          endif
        endif
        this.w_DATINI = TTOD(this.oParentObject.w_ATDATINI)
        this.w_DATFIN = TTOD(this.oParentObject.w_ATDATFIN)
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_ATFLRICO="S" AND this.w_DATINI<>this.w_DATFIN
          ah_ErrorMsg("Impossibile confermare attivit� ricorrente ,%0data iniziale e data finale non corrispondono! ")
          this.oParentObject.w_RESCHK = -1
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_ATSTATUS <> "T" AND this.oParentObject.w_FLNSAP<>"S"
          * --- Verifico partecipanti
          if this.pParam="A"
            this.w_GSAG_MPA = this.oParentObject.GSAG_MPA
            this.w_GSAG_MPA.MarkPos()     
            * --- t_PATIPRIS � codificato con un numerico mentre TipoRisorsa � un carattere: filtriamo cercando TipoRisorsa='G'
            *     poich� la condizione per ciascuna riga � appunto che PATIPRIS=TipoRisorsa
            if this.w_GSAG_MPA.Search("NVL(t_TipoRisorsa, SPACE(1))= 'G' AND NOT DELETED()") <> -1
              ah_ErrorMsg("Impossibile confermare attivit� in stato non provvisorio se sono presenti uno o pi� gruppi fra i partecipanti")
              this.oParentObject.w_RESCHK = -1
            endif
            this.w_GSAG_MPA.RePos()     
          else
            SELECT * FROM (this.oParentObject.w_ZOOMPART.ccursor) WHERE DPTIPRIS="G" AND XCHK=1 INTO CURSOR TMP_PART 
            if Used("TMP_PART") AND RECCOUNT("TMP_PART")>0
              ah_ErrorMsg("Impossibile confermare attivit� in stato non provvisorio se sono presenti uno o pi� gruppi fra i partecipanti")
              this.oParentObject.w_RESCHK = -1
            endif
            USE IN SELECT("TMP_PART")
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_ATFLATRI = "S"
          * --- a) non deve essere provvisoria (non deve avere gruppi tra i partecipanti)
          if this.oParentObject.w_ATSTATUS = "T"
            ah_ErrorMsg("Impossibile confermare attivit� riservate in stato provvisorio ")
            this.oParentObject.w_RESCHK = -1
          else
            * --- b) deve esistere tra i partecipanti almeno una risorsa che ha associato un utente applicativo (i_codute)
            if this.pParam="A"
              this.w_GSAG_MPA = this.oParentObject.GSAG_MPA
              this.w_GSAG_MPA.MarkPos()     
              if this.w_GSAG_MPA.Search("NVL(t_CODUTE, 0)> 0 AND NOT DELETED()") = -1
                ah_ErrorMsg("Impossibile confermare attivit� riservata senza definire neppure un partecipante al quale � stato associato un utente")
                this.oParentObject.w_RESCHK = -1
              endif
              this.w_GSAG_MPA.RePos()     
            else
              SELECT * FROM (this.oParentObject.w_ZOOMPART.ccursor) WHERE DPCODUTE>0 AND XCHK=1 INTO CURSOR TMP_PART 
              if Used("TMP_PART") AND RECCOUNT("TMP_PART")>0
                ah_ErrorMsg("Attenzione, visualizzazione da struttura aziendale attiva: impossibile salvare attivit� con partecipanti di tipo persona senza gruppo di riferimento. ","!","")
                this.oParentObject.w_RESCHK = -1
              endif
              USE IN SELECT("TMP_PART")
            endif
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_ATFLATRI = "H"
          * --- a) non deve essere provvisoria (non deve avere gruppi tra i partecipanti)
          if this.oParentObject.w_ATSTATUS = "T"
            ah_ErrorMsg("Impossibile confermare attivit� nascoste in stato provvisorio ")
            this.oParentObject.w_RESCHK = -1
          else
            * --- b) deve esistere tra i partecipanti almeno una risorsa che ha associato un utente applicativo (i_codute)
            if this.pParam="A"
              this.w_GSAG_MPA = this.oParentObject.GSAG_MPA
              this.w_GSAG_MPA.MarkPos()     
              if this.w_GSAG_MPA.Search("NVL(t_CODUTE, 0)> 0 AND NOT DELETED()") = -1
                ah_ErrorMsg("Impossibile confermare attivit� nascosta senza definire neppure un partecipante al quale � stato associato un utente")
                this.oParentObject.w_RESCHK = -1
              endif
              this.w_GSAG_MPA.RePos()     
            else
              SELECT * FROM (this.oParentObject.w_ZOOMPART.ccursor) WHERE DPCODUTE>0 AND XCHK=1 INTO CURSOR TMP_PART 
              if Used("TMP_PART") AND RECCOUNT("TMP_PART")>0
                ah_ErrorMsg("Impossibile confermare attivit� nascosta senza definire neppure un partecipante al quale � stato associato un utente")
                this.oParentObject.w_RESCHK = -1
              endif
              USE IN SELECT("TMP_PART")
            endif
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND (EMPTY(this.oParentObject.w_ATCODPRA) AND (this.oParentObject.w_CACHKOBB="P" OR (this.oParentObject.w_CACHKOBB="N" AND this.oParentObject.w_ATSTATUS = "P" and Isalt())))
          * --- Stato PRESTAZIONE con Pratica Obbligatoria e non � stata specificata la pratica 
          if Isalt()
            ah_ErrorMsg("La pratica � obbligatoria")
          else
            ah_ErrorMsg("La commessa � obbligatoria")
          endif
          this.oParentObject.w_RESCHK = -1
          this.w_OBJ = this.oParentObject.Getctrl("w_ATCODPRA")
          if vartype(this.w_OBJ)="O"
            * --- Viene attivata la pag. 1 di GSAG_AAT (Attivit�)
            this.oParentObject.oPgFrm.ActivePage=1
            * --- Viene posizionato il cursore sul campo ATCODPRA di GSAG_AAT (Attivit�)
            this.w_OBJ.Setfocus()     
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_CACHKNOM="P" AND EMPTY(this.oParentObject.w_ATCODNOM)
          * --- Nominativo obbligatorio
          ah_ErrorMsg("Il nominativo � obbligatorio")
          this.oParentObject.w_RESCHK = -1
          this.w_OBJ = this.oParentObject.Getctrl("w_ATCODNOM")
          if vartype(this.w_OBJ)="O"
            * --- Viene attivata la pag. 1 di GSAG_AAT (Attivit�)
            this.oParentObject.oPgFrm.ActivePage=1
            * --- Viene posizionato il cursore sul campo ATCODPRA di GSAG_AAT (Attivit�)
            this.w_OBJ.Setfocus()     
          endif
        endif
        * --- Controlla il tipo documento solo se soggetta a prestazione
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_ATSTATUS="P" AND (((this.oParentObject.w_FLINTE <> "N" AND this.oParentObject.w_PARAME<>"RF") OR (this.oParentObject.w_FLCOSE = "S" AND this.oParentObject.w_PARAME="RF") ) AND this.oParentObject.w_CACHKNOM<>"P" AND EMPTY(this.oParentObject.w_ATCODNOM) and Not Empty(this.oParentObject.w_ATCAUDOC))
          ah_ErrorMsg("Attenzione: il documento da generare prevede l'intestatario. Occorre indicare un nominativo!")
          this.oParentObject.w_RESCHK = -1
        endif
        if this.oParentObject.w_RESCHK<>-1 AND this.oParentObject.w_ATSTATUS = "P" AND !EMPTY(this.oParentObject.w_ATCODNOM)
          * --- Read from OFF_NOMI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NOCODCLI,NOTIPNOM"+;
              " from "+i_cTable+" OFF_NOMI where ";
                  +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_ATCODNOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NOCODCLI,NOTIPNOM;
              from (i_cTable) where;
                  NOCODICE = this.oParentObject.w_ATCODNOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NOCODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
            this.w_NOTIPO = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_NOCODCLI = NVL( this.w_NOCODCLI , SPACE( 15 ) )
          if EMPTY( this.w_NOCODCLI ) and ((this.oParentObject.w_FLINTE <> "N" AND this.oParentObject.w_PARAME<>"RF") OR (this.oParentObject.w_FLCOSE = "S" AND this.oParentObject.w_PARAME="RF") )
            if AH_YESNO( "Il nominativo selezionato non � cliente%0Si desidera trasformarlo?" )
              * --- Write into OFF_NOMI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC("C"),'OFF_NOMI','NOTIPNOM');
                    +i_ccchkf ;
                +" where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_NOCODCLI);
                       )
              else
                update (i_cTable) set;
                    NOTIPNOM = "C";
                    &i_ccchkf. ;
                 where;
                    NOCODICE = this.w_NOCODCLI;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.oParentObject.w_CODCLI = GSAR_BO1( THIS , this.oParentObject.w_ATCODNOM , this.w_NOTIPO )
              if empty(this.oParentObject.w_CODCLI)
                this.oParentObject.w_RESCHK = -1
                ah_ErrorMsg("Impossibile confermare attivit� con nominativo non trasformato in cliente")
              endif
            else
              this.oParentObject.w_RESCHK = -1
              ah_ErrorMsg("Impossibile confermare attivit� con nominativo non trasformato in cliente")
            endif
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_ATSTATUS = "P" AND this.pParam="A"
          * --- Stato PRESTAZIONE: controlliamo che le prestazioni abbiano un responsabile
          this.w_GSAG_MPA = this.oParentObject.GSAG_MDA
          this.w_GSAG_MPA.MarkPos()     
          this.w_GSAG_MPA.FirstRow()     
          do while !this.w_GSAG_MPA.Eof_Trs()
            this.w_GSAG_MPA.SetRow()     
            if this.w_GSAG_MPA.FullRow()
              if EMPTY(NVL(this.w_GSAG_MPA.w_DACODRES, " "))
                ah_ErrorMsg("Attenzione: inserire il codice partecipante sulle righe prestazioni")
                this.oParentObject.w_RESCHK = -1
                exit
              endif
            endif
            this.w_GSAG_MPA.NextRow()     
          enddo
          this.w_GSAG_MPA.RePos(.T.)     
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_ATDATINI > this.oParentObject.w_ATDATFIN
          * --- Controllo data inizio attivit� non inferiore a data fine attivit�
          ah_ErrorMsg("La data di fine attivit� non deve essere minore della data di inizio attivit�")
          this.oParentObject.w_RESCHK = -1
          this.w_OBJ = this.oParentObject.Getctrl("w_DATFIN")
          if vartype(this.w_OBJ)="O"
            * --- Viene attivata la pag. 1 di GSAG_AAT (Attivit�)
            this.oParentObject.oPgFrm.ActivePage=1
            * --- Viene posizionato il cursore sulla variabile DATFIN di GSAG_AAT (Attivit�)
            this.w_OBJ.Setfocus()     
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_ATSTATUS$"T-D-I" AND this.w_DATFIN<I_DATSYS and (this.pParam="W" OR lower(this.oParentObject.cFunction) = "load")
          if !Ah_yesno( "Inserita data antecedente alla data odierna! Continuare?")
            this.oParentObject.w_RESCHK = -1
            this.w_OBJ = this.oParentObject.Getctrl("w_DATFIN")
            if vartype(this.w_OBJ)="O"
              * --- Viene attivata la pag. 1 di GSAG_AAT (Attivit�)
              this.oParentObject.oPgFrm.ActivePage=1
              * --- Viene posizionato il cursore sulla variabile DATFIN di GSAG_AAT (Attivit�)
              this.w_OBJ.Setfocus()     
            endif
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND NOT EMPTY(this.oParentObject.w_PACHKATT) AND this.oParentObject.w_ATSTATUS$"T-D-I" AND this.w_DATINI > (I_DATSYS+this.oParentObject.w_PACHKATT) and (this.pParam="W" OR lower(this.oParentObject.cFunction) = "load")
          if !Ah_yesno( "Inserita data superiore alla data odierna di oltre %1 giorni. Continuare?", "", alltrim(str(this.oParentObject.w_PACHKATT)))
            this.oParentObject.w_RESCHK = -1
            this.w_OBJ = this.oParentObject.Getctrl("w_DATINI")
            if vartype(this.w_OBJ)="O"
              * --- Viene attivata la pag. 1 di GSAG_AAT (Attivit�)
              this.oParentObject.oPgFrm.ActivePage=1
              * --- Viene posizionato il cursore sulla variabile DATINI di GSAG_AAT (Attivit�)
              this.w_OBJ.Setfocus()     
            endif
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_PACHKCAR<>"N" AND !this.oParentObject.w_ATSTATUS=="F" AND !this.oParentObject.w_ATSTATUS=="P"
          * --- Verifica congruenza data attivit� con calendario partecipanti
          this.oParentObject.w_RESCHK = ChkDatCal(This.oparentobject,This,.f.)
        endif
        if this.pParam="A"
          * --- Controlli per GSAG_AAT
          if this.oParentObject.w_RESCHK <> -1 and this.oParentObject.w_ATSTATUS = "P" 
            * --- Verifiche analitica
            this.w_GSAG_MPA = this.oParentObject.GSAG_MDA
            this.w_GSAG_MPA.MarkPos()     
            this.w_GSAG_MPA.FirstRow()     
            do while !this.w_GSAG_MPA.Eof_Trs()
              this.w_GSAG_MPA.SetRow()     
              if this.w_GSAG_MPA.FullRow() and this.w_GSAG_MPA.w_TIPSER <> "D" and (this.w_GSAG_MPA.w_DATIPRIG $ "D-A-E" OR (this.w_GSAG_MPA.w_DATIPRI2 $ "D-A-E" and Isalt()) )
                if ! Isahe()
                  * --- Controlli ricavi
                  do case
                    case this.oParentObject.w_RESCHK<>-1 and Empty(Nvl(this.w_GSAG_MPA.w_DAVOCRIC,Space(15))) and (this.oParentObject.w_FLDANA="S" or this.oParentObject.w_FLGCOM="S")
                      ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza voce di ricavo.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                      this.oParentObject.w_RESCHK = -1
                    case this.oParentObject.w_RESCHK<>-1 and Empty(Nvl(this.w_GSAG_MPA.w_DACENRIC,Space(15))) and this.oParentObject.w_FLDANA="S"
                      ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza centro di ricavo.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                      this.oParentObject.w_RESCHK = -1
                  endcase
                  if this.oParentObject.w_RESCHK <> -1 and Not Empty(this.oParentObject.w_ATCAUDOC) and (this.w_GSAG_MPA.w_DATIPRIG $ "D-E" OR (this.w_GSAG_MPA.w_DATIPRI2 $ "D-E" and Isalt())) and this.oParentObject.w_FLGCOM ="S"
                    do case
                      case Empty(Nvl(this.w_GSAG_MPA.w_DACOMRIC,Space(15))) 
                        ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza commessa ricavi.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                        this.oParentObject.w_RESCHK = -1
                      case Empty(Nvl(this.w_GSAG_MPA.w_DAATTRIC,Space(15))) and !isAhe()
                        ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza attivit� ricavi.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                        this.oParentObject.w_RESCHK = -1
                    endcase
                  endif
                  * --- Controlli costi
                  if g_PERCCR="S" and (this.w_GSAG_MPA.w_DATIPRIG $ "A-E" OR (this.w_GSAG_MPA.w_DATIPRI2 $ "A-E" and Isalt()) ) or (this.oParentObject.w_FLANAL="S" and Nvl(this.w_GSAG_MPA.w_DACOSINT,0)>0)
                    do case
                      case this.oParentObject.w_RESCHK<>-1 and Empty(Nvl(this.w_GSAG_MPA.w_DAVOCCOS,Space(15))) AND ((this.oParentObject.w_FLANAL="S" or (this.oParentObject.w_VOCECR="C" AND (this.oParentObject.w_FLACQ="S" or this.oParentObject.w_FLACOMAQ="S")))) 
                        ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza voce di costo.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                        this.oParentObject.w_RESCHK = -1
                      case this.oParentObject.w_RESCHK<>-1 and Empty(Nvl(this.w_GSAG_MPA.w_DACENCOS,Space(15))) AND ((this.oParentObject.w_FLACQ="S" or this.oParentObject.w_FLANAL="S")) 
                        ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza centro di costo.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                        this.oParentObject.w_RESCHK = -1
                    endcase
                  endif
                  if this.oParentObject.w_RESCHK<>-1 and NOT EMPTY(this.oParentObject.w_ATCAUACQ) and (this.w_GSAG_MPA.w_DATIPRIG $ "A-E" OR (this.w_GSAG_MPA.w_DATIPRI2 $ "A-E" and Isalt())) and this.oParentObject.w_FLGCOM ="S"
                    do case
                      case Empty(Nvl(this.w_GSAG_MPA.w_DACODCOM,Space(15))) 
                        ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza commessa costi.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                        this.oParentObject.w_RESCHK = -1
                      case Empty(Nvl(this.w_GSAG_MPA.w_DAATTIVI,Space(15)))
                        ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza attivit� costi.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                        this.oParentObject.w_RESCHK = -1
                    endcase
                  endif
                else
                  if g_PERCCM="S" AND !(this.oParentObject.w_BUFLANAL<>"S" and not empty(g_CODBUN))
                    if (this.oParentObject.w_FLACQ $ "S-I" and this.w_GSAG_MPA.w_DATIPRIG $ "A-E") 
                      do case
                        case Empty(Nvl(this.w_GSAG_MPA.w_DAVOCCOS,Space(15))) 
                          ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza voce di costo.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                          this.oParentObject.w_RESCHK = -1
                        case Empty(Nvl(this.w_GSAG_MPA.w_DACENCOS,Space(15)))
                          ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza centro di costo.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                          this.oParentObject.w_RESCHK = -1
                      endcase
                    endif
                    if (this.w_GSAG_MPA.w_DATIPRIG $ "A-E" and this.oParentObject.w_FLACOMAQ<> "N") 
                      do case
                        case Empty(Nvl(this.w_GSAG_MPA.w_DACODCOM,Space(15))) 
                          ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza commessa costi.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                          this.oParentObject.w_RESCHK = -1
                        case Empty(Nvl(this.w_GSAG_MPA.w_DAATTIVI,Space(15)))
                          ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza attivit� costi.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                          this.oParentObject.w_RESCHK = -1
                      endcase
                    endif
                    if g_CACQ="S" and Not Empty(this.oParentObject.w_ATCAUACQ) and (this.w_GSAG_MPA.w_DATIPRIG $ "D-E" OR this.w_GSAG_MPA.w_DATIPRI2 $ "D-E") and this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_FLDANA $ "S-I" 
                      * --- Controllo dati analitici acquisti
                      do case
                        case Empty(Nvl(this.w_GSAG_MPA.w_DAVOCRIC,Space(15))) 
                          ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza voce di ricavo.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                          this.oParentObject.w_RESCHK = -1
                        case Empty(Nvl(this.w_GSAG_MPA.w_DACENRIC,Space(15))) 
                          ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza centro di ricavo.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                          this.oParentObject.w_RESCHK = -1
                      endcase
                    endif
                  endif
                  if Not Empty(this.oParentObject.w_ATCAUDOC) and (this.w_GSAG_MPA.w_DATIPRIG $ "D-E") and this.oParentObject.w_RESCHK <> -1 AND (((this.oParentObject.w_FLGCOM <> "N" and Isahe() and g_CACQ="S" and !(this.oParentObject.w_BUFLANAL<>"S" and not empty(g_CODBUN)))))
                    * --- Controllo dati analitici vendite
                    do case
                      case Empty(Nvl(this.w_GSAG_MPA.w_DACOMRIC,Space(15))) 
                        ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza commessa ricavi.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                        this.oParentObject.w_RESCHK = -1
                      case Empty(Nvl(this.w_GSAG_MPA.w_DAATTRIC,Space(15))) and !isAhe()
                        ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza attivit� ricavi.","!","", Alltrim(STR(this.w_GSAG_MPA.w_CPROWORD)))
                        this.oParentObject.w_RESCHK = -1
                    endcase
                  endif
                endif
              endif
              this.w_GSAG_MPA.NextRow()     
            enddo
            this.w_GSAG_MPA.RePos(.T.)     
          endif
          if this.oParentObject.w_RESCHK <> -1 and this.oParentObject.w_ATSTATUS = "P" 
            this.w_GSAG_MDD = this.oParentObject.GSAG_MDD
            this.w_GSAG_MDD.LinkPCClick(.t.)     
            * --- Nuovo Oggetto
            this.w_GSAG_MDD = this.oParentObject.GSAG_MDD
            * --- This.Cnt.bOnScreen
            if this.w_gsag_mdd.CNT.BoNsCREEN
              this.w_GSAG_MDD.ecpsave()     
            endif
            this.w_GSAG_MDD.CNT.Exec_Select("_TMP_MDD","COUNT(*) AS CONTA, t_ADSERDOC AS ADSERDOC,T_NUMDOC AS NUMDOC, t_TIPDOC AS TIPDOC","","ADSERDOC","ADSERDOC","")     
            if NVL(_TMP_MDD.CONTA,0)>1
              ah_ErrorMsg("Attenzione: il documento numero %1 con causale %2 � stato associato pi� volte","!","", ALLTRIM(STR(_TMP_MDD.NUMDOC)), ALLTRIM(_TMP_MDD.TIPDOC))
              this.oParentObject.w_RESCHK = -1
            endif
            this.w_GSAG_MDD = null
            Use in _TMP_MDD
          endif
          if this.oParentObject.w_RESCHK <> -1 and g_OFFE="S" and this.oParentObject.w_ATOPERAT=0 and !Isalt()
            this.w_CODAZI = i_CODAZI
            * --- Read from PAR_OFFE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "POTIPATT"+;
                " from "+i_cTable+" PAR_OFFE where ";
                    +"POCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                POTIPATT;
                from (i_cTable) where;
                    POCODAZI = this.w_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CAUOFFE = NVL(cp_ToDate(_read_.POTIPATT),cp_NullValue(_read_.POTIPATT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Not Empty(this.w_CAUOFFE) and this.w_CAUOFFE=this.oParentObject.w_ATCAUATT
              ah_ErrorMsg("Attenzione, attivit� associata alle offerte, inserire codice operatore nell'apposita sezione")
              this.oParentObject.w_RESCHK = -1
            endif
          endif
          if IsAhr() AND this.oParentObject.w_RESCHK <> -1 and this.oParentObject.w_ATSTATUS = "P"
            * --- Verifiche servizi a valore con prezzo zero
            this.w_MsgWarning = .F.
            this.w_GSAG_MDA = this.oParentObject.GSAG_MDA
            this.w_GSAG_MDA.Exec_Select("_TMP_MDA","*","NVL(t_DAVALRIG,0)=0 AND (t_DATIPRIG<>'N' OR t_DATIPRI2<>'N') AND t_TIPART='FO' AND (NOT EMPTY(NVL(t_DACODATT,'')) OR NOT EMPTY(NVL(t_DACODICE,'')))","","","")     
            if RECCOUNT()>0
              * --- Ci sono prestazioni a valore con prezzo zero
              * --- Dobbiamo controllare se la causale ha il flag di ricalcolo prezzi attivo e quindi leggiamo TDPRZDES
              * --- Read from TIP_DOCU
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TDPRZDES"+;
                  " from "+i_cTable+" TIP_DOCU where ";
                      +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_ATCAUDOC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TDPRZDES;
                  from (i_cTable) where;
                      TDTIPDOC = this.oParentObject.w_ATCAUDOC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_RicalcolaPrz = NVL(cp_ToDate(_read_.TDPRZDES),cp_NullValue(_read_.TDPRZDES))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_RicalcolaPrz<>"S"
                * --- La causale non ricalcola i prezzi, quindi deve segnalare che ci sono righe con prezzo a zero
                this.w_MsgWarning = .T.
              else
                * --- La causale ricalcola i prezzi, quindi deve controllare se avrebbero prezzo zero anche dopo
                 
 select _TMP_MDA 
 Go Top 
 Scan
                this.w_PrzCalcolato = GSAR_BPZ(this, t_DACODATT,this.oParentObject.w_CODCLI, "C", this.oParentObject.w_ATCODLIS, t_DAQTAMOV, this.oParentObject.w_ATCODVAL, this.oParentObject.w_ATDATINI, t_DAUNIMIS)
                if this.w_PrzCalcolato=0
                  this.w_MsgWarning = .T.
                  EXIT
                endif
                Endscan
              endif
            endif
            USE IN SELECT("_TMP_MDA")
            if this.w_MsgWarning AND !AH_YESNO( "Attenzione! Nel dettaglio prestazioni sono presenti servizi di tipo 'a valore' che avranno prezzo a zero sul documento generato! Prosegui ugualmente?" )
              this.oParentObject.w_RESCHK = -1
            endif
          endif
          if g_AGFA="S" and this.oParentObject.w_RESCHK<> -1 AND Not Empty(this.oParentObject.w_IDRICH)
            if this.oParentObject.w_RESCHK<>-1 and Not Used((this.oParentObject.w_CUR_EVE))
              if Empty(this.oParentObject.w_TIPEVE) AND this.oParentObject.w_NEWID
                this.oParentObject.w_RESCHK = -1
                ah_ErrorMsg("Attenzione, inserire tipo evento per la generazione ID richiesta")
              endif
              if this.oParentObject.w_RESCHK<>-1 
                this.oParentObject.w_CUR_EVE = SYS(2015)
                this.w_OK = .t.
                this.w_Err_Log = CREATEOBJECT("ah_ErrorLog")
                * --- Creo cursore di generazione\associazione eventi
                vq_exec("..\agen\exe\QUERY\GSFA_BIM.VQR",this,this.oParentObject.w_CUR_EVE)
                * --- Verifico se l'ID � gia presente oppure devo creare evento
                this.w_GSAG_MPA = this.oParentObject.GSAG_MPA
                this.w_GSAG_MPA.Exec_Select("_TMP_MPA","t_CPROWORD,t_PACODRIS as PACODRIS,t_PAGRURIS as PAGRURIS","t_PATIPRIS=1","t_CPROWORD","","")     
                this.w_CodRis = _TMP_MPA.PACODRIS
                this.w_CodGru = _TMP_MPA.PAGRURIS
                Use in _TMP_MPA
                * --- Read from ANEVENTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ANEVENTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2],.t.,this.ANEVENTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "EVSERIAL,EV_STATO"+;
                    " from "+i_cTable+" ANEVENTI where ";
                        +"EVFLIDPA = "+cp_ToStrODBC("S");
                        +" and EVIDRICH = "+cp_ToStrODBC(this.oParentObject.w_IDRICH);
                        +" and EV__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ATEVANNO);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    EVSERIAL,EV_STATO;
                    from (i_cTable) where;
                        EVFLIDPA = "S";
                        and EVIDRICH = this.oParentObject.w_IDRICH;
                        and EV__ANNO = this.oParentObject.w_ATEVANNO;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_EVSERIAL = NVL(cp_ToDate(_read_.EVSERIAL),cp_NullValue(_read_.EVSERIAL))
                  this.w_STATO = NVL(cp_ToDate(_read_.EV_STATO),cp_NullValue(_read_.EV_STATO))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.oParentObject.w_NOMSG = " "
                if i_ROWS=0
                  if this.oParentObject.w_RESCHK<>-1
                    * --- Devo creare l'evento inserisco dati ricavati dall'attivit�
                    this.w_CodRis = iif(Empty(this.w_CodRis),readdipend(i_CodUte, "D"),this.w_CodRis)
                    this.oParentObject.w_ATDATFIN = iif(Empty(this.oParentObject.w_ATDATFIN),this.oParentObject.w_ATDATINI + this.oParentObject.w_ATDURORE*3600 + this.oParentObject.w_ATDURMIN*60,this.oParentObject.w_ATDATFIN)
                    * --- Read from DIPENDEN
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "DPTELEF1"+;
                        " from "+i_cTable+" DIPENDEN where ";
                            +"DPCODICE = "+cp_ToStrODBC(this.w_CodRis);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        DPTELEF1;
                        from (i_cTable) where;
                            DPCODICE = this.w_CodRis;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_TELINT = NVL(cp_ToDate(_read_.DPTELEF1),cp_NullValue(_read_.DPTELEF1))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    INSERT INTO ( this.oParentObject.w_CUR_EVE ) (EV__ANNO,EVSERIAL, EVTIPEVE, EVDIREVE, EVOGGETT, EV__NOTE, EV_EMAIL, EV_EMPEC, EVDATINI, EVDATFIN,EVIDRICH,EVFLIDPA,EVSTARIC, ; 
 EV_PHONE,EVNOMINA,EVRIFPER,EVCODPRA,EVPERSON,EVGRUPPO,EVTELINT,EVCODPAR,EVNUMCEL,EVLOCALI) ; 
 VALUES (EV__ANNO,this.oParentObject.w_TIPEVE, this.oParentObject.w_TIPEVE, this.oParentObject.w_TIPDIR, this.oParentObject.w_ATOGGETT, this.oParentObject.w_ATNOTPIA, this.oParentObject.w_AT_EMAIL, this.oParentObject.w_AT_EMPEC, this.oParentObject.w_ATDATINI, this.oParentObject.w_ATDATFIN, this.oParentObject.w_IDRICH,"S", this.oParentObject.w_STARIC, ; 
 this.oParentObject.w_ATTELEFO,this.oParentObject.w_ATCODNOM,this.oParentObject.w_ATPERSON,this.oParentObject.w_ATCODPRA,this.w_CODRIS, this.w_CodGru,this.w_TELINT,IIF(this.oParentObject.w_FlCreaPers="S",this.oParentObject.w_NewCodPer,this.oParentObject.w_ATCONTAT),this.oParentObject.w_ATCELLUL,this.oParentObject.w_ATLOCALI)
                    * --- Eseguo con parametro A per non chiudere cursore eventi e avere come ritorno seriale evento generato
                    this.w_OK = GSFA_BEV(this, this.oParentObject.w_CUR_EVE , "", this.w_Err_Log, "A",.f.,.t.,.t.,.t.)
                    Select (this.oParentObject.w_CUR_EVE) 
 Go top
                    * --- Valorizzo evento padre nell'attivit�
                    this.oParentObject.w_ATSEREVE = EVSERIAL
                    this.oParentObject.w_ATEVANNO = EV__ANNO
                  endif
                else
                  * --- Aggiorno stato della richiesta
                  this.oParentObject.w_NOMSG = iif(this.w_STATO="D"," ","S")
                  if Not Empty(this.oParentObject.w_IDRICH)
                    * --- Write into ANEVENTI
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ANEVENTI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ANEVENTI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"EVSTARIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STARIC),'ANEVENTI','EVSTARIC');
                          +i_ccchkf ;
                      +" where ";
                          +"EVIDRICH = "+cp_ToStrODBC(this.oParentObject.w_IDRICH);
                             )
                    else
                      update (i_cTable) set;
                          EVSTARIC = this.oParentObject.w_STARIC;
                          &i_ccchkf. ;
                       where;
                          EVIDRICH = this.oParentObject.w_IDRICH;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                  * --- Devo solo associare evento attivit� passo solo i campi indispensabili all'associazione
                  INSERT INTO ( this.oParentObject.w_CUR_EVE ) (EVSERIAL,EV__ANNO ) VALUES (this.oParentObject.w_ATSEREVE,this.oParentObject.w_ATEVANNO)
                endif
                if this.w_Err_Log.isFullLog()
                  * --- Visualizzo eventuale log di errore
                  this.oParentObject.w_RESCHK = -1
                  ah_ErrorMsg("Attenzione, errore nella generazione evento ID richiesta")
                  this.w_Err_Log.PrintLog()     
                  this.oParentObject.w_CUR_EVE = " "
                endif
                this.w_Err_Log = .null.
              endif
            endif
          endif
          * --- Inserimento persona di nominativo
          if this.oParentObject.w_RESCHK<> -1 AND this.oParentObject.w_FlCreaPers="S" AND !EMPTY(this.oParentObject.w_ATCODNOM) AND EMPTY(this.oParentObject.w_ATCONTAT) AND !EMPTY(this.oParentObject.w_ATPERSON+this.oParentObject.w_ATTELEFO+this.oParentObject.w_ATCELLUL+this.oParentObject.w_AT_EMAIL+this.oParentObject.w_AT_EMPEC)
            if EMPTY(this.oParentObject.w_NewCodPer) 
              * --- Chiede se procedere con il calcolo in automatico del codice della persona
              this.w_OK = Ah_yesno("Attenzione: non � stato specificato un codice per la persona. Proseguire con l'inserimento in automatico?")
              if !this.w_OK
                this.w_MESS = Ah_MsgFormat("Operazione abbandonata")
                this.oParentObject.w_RESCHK = -1
              else
                this.w_MAXCODCONT = ""
                * --- Select from NOM_CONT
                i_nConn=i_TableProp[this.NOM_CONT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select MAX(NCCODCON) AS MAXCODCONT  from "+i_cTable+" NOM_CONT ";
                      +" where NCCODICE="+cp_ToStrODBC(this.oParentObject.w_ATCODNOM)+" AND NOM_CONT.NCCODCON LIKE 'AT%'";
                       ,"_Curs_NOM_CONT")
                else
                  select MAX(NCCODCON) AS MAXCODCONT from (i_cTable);
                   where NCCODICE=this.oParentObject.w_ATCODNOM AND NOM_CONT.NCCODCON LIKE "AT%";
                    into cursor _Curs_NOM_CONT
                endif
                if used('_Curs_NOM_CONT')
                  select _Curs_NOM_CONT
                  locate for 1=1
                  do while not(eof())
                  this.w_MAXCODCONT = NVL(MAXCODCONT, SPACE(5))
                  * --- Se � un nuovo contatto cerco il codice da inserire, altrimenti riuso quello che era stato specificato nel contatto di Outlook
                  this.oParentObject.w_NewCodPer = "AT"+PADL( ALLTRIM(STR(VAL(SUBSTR(this.w_MAXCODCONT,3,3))+1 )) ,3,"0")
                    select _Curs_NOM_CONT
                    continue
                  enddo
                  use
                endif
              endif
            endif
            if !EMPTY(this.oParentObject.w_NewCodPer) 
              this.w_MAXROWORD = 0
              * --- Select from NOM_CONT
              i_nConn=i_TableProp[this.NOM_CONT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX(CPROWORD) AS MAXROWORD  from "+i_cTable+" NOM_CONT ";
                    +" where NCCODICE="+cp_ToStrODBC(this.oParentObject.w_ATCODNOM)+"";
                     ,"_Curs_NOM_CONT")
              else
                select MAX(CPROWORD) AS MAXROWORD from (i_cTable);
                 where NCCODICE=this.oParentObject.w_ATCODNOM;
                  into cursor _Curs_NOM_CONT
              endif
              if used('_Curs_NOM_CONT')
                select _Curs_NOM_CONT
                locate for 1=1
                do while not(eof())
                this.w_MAXROWORD = NVL(MAXROWORD, 0)
                  select _Curs_NOM_CONT
                  continue
                enddo
                use
              endif
              this.w_MAXROWORD = this.w_MAXROWORD+10
              * --- Insert into NOM_CONT
              i_nConn=i_TableProp[this.NOM_CONT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.NOM_CONT_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"NCCODICE"+",NCCODCON"+",CPROWORD"+",NCPERSON"+",NCTELEFO"+",NCNUMCEL"+",NC_EMAIL"+",NC_EMPEC"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCODNOM),'NOM_CONT','NCCODICE');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NewCodPer),'NOM_CONT','NCCODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MAXROWORD),'NOM_CONT','CPROWORD');
                +","+cp_NullLink(cp_ToStrODBC(LEFT(this.oParentObject.w_ATPERSON,40)),'NOM_CONT','NCPERSON');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATTELEFO),'NOM_CONT','NCTELEFO');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCELLUL),'NOM_CONT','NCNUMCEL');
                +","+cp_NullLink(cp_ToStrODBC(LEFT(ALLTRIM(this.oParentObject.w_AT_EMAIL),254)),'NOM_CONT','NC_EMAIL');
                +","+cp_NullLink(cp_ToStrODBC(LEFT(ALLTRIM(this.oParentObject.w_AT_EMPEC),254)),'NOM_CONT','NC_EMPEC');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'NCCODICE',this.oParentObject.w_ATCODNOM,'NCCODCON',this.oParentObject.w_NewCodPer,'CPROWORD',this.w_MAXROWORD,'NCPERSON',LEFT(this.oParentObject.w_ATPERSON,40),'NCTELEFO',this.oParentObject.w_ATTELEFO,'NCNUMCEL',this.oParentObject.w_ATCELLUL,'NC_EMAIL',LEFT(ALLTRIM(this.oParentObject.w_AT_EMAIL),254),'NC_EMPEC',LEFT(ALLTRIM(this.oParentObject.w_AT_EMPEC),254))
                insert into (i_cTable) (NCCODICE,NCCODCON,CPROWORD,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC &i_ccchkf. );
                   values (;
                     this.oParentObject.w_ATCODNOM;
                     ,this.oParentObject.w_NewCodPer;
                     ,this.w_MAXROWORD;
                     ,LEFT(this.oParentObject.w_ATPERSON,40);
                     ,this.oParentObject.w_ATTELEFO;
                     ,this.oParentObject.w_ATCELLUL;
                     ,LEFT(ALLTRIM(this.oParentObject.w_AT_EMAIL),254);
                     ,LEFT(ALLTRIM(this.oParentObject.w_AT_EMPEC),254);
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              this.oParentObject.w_ATCONTAT = this.oParentObject.w_NewCodPer
            endif
          endif
          if this.oParentObject.w_RESCHK<>-1 AND NOT EMPTY( NVL( this.w_DATINI, CTOD( "  -  -  " ) ) ) and !Isalt()
            this.w_oERRORLOG=createobject("AH_ErrorLog")
            this.w_GSAG_MCP = this.oParentObject.GSAG_MCP
            this.w_GSAG_MCP.Exec_Select("_TMP_MCP","t_CACODIMP AS CACODIMP, t_IMDATINI AS IMDATINI, t_IMDATFIN AS IMDATFIN, t_DESCOM AS DESCOM, t_DTOBSO AS DTOBSO, t_DTOINI AS DTOINI","NOT DELETED() and Not Empty(t_CACODIMP)","","","")     
            SELECT _TMP_MCP
            GO TOP
            do while NOT EOF()
              this.w_IMDATINI = IMDATINI
              this.w_IMDATFIN = IMDATFIN
              this.w_DTOBSO = DTOBSO
              this.w_DTOINI = DTOINI
              this.w_DESCOM = DESCOM
              this.w_CACODIMP = CACODIMP
              * --- L'elemento ha una data di inzio prossima attivit�
              if NOT EMPTY( NVL( this.w_IMDATINI, CTOD( "  -  -  " ) ) )
                * --- L'impianto ha una data di inizio validit�
                if this.w_IMDATINI > this.w_DATINI
                  this.w_oERRORLOG.AddMsgLog("L'impianto %1 non pu� essere selezionato",this.w_CACODIMP)     
                  this.w_oERRORLOG.AddMsgLog("La data inizio validit� dell'impianto (%1) � successiva alla data di inizio attivit� (%2)", DTOC( this.w_IMDATINI ), DTOC( this.w_DATINI ))     
                  this.oParentObject.w_RESCHK = -1
                endif
              endif
              if NOT EMPTY( NVL( this.w_IMDATFIN, CTOD( "  -  -  " ) ) )
                * --- L'impianto ha una data di fine validit�
                if this.w_IMDATFIN <= this.w_DATFIN
                  this.w_oERRORLOG.AddMsgLog("L'impianto %1 non pu� essere selezionato",this.w_CACODIMP)     
                  this.w_oERRORLOG.AddMsgLog("La data di fine validit� dell'impianto (%1) � precedente o uguale alla data di fine attivit� (%2)", DTOC( this.w_IMDATFIN ), DTOC( this.w_DATINI ))     
                  this.oParentObject.w_RESCHK = -1
                endif
              endif
              if NOT EMPTY( NVL( this.w_DTOBSO, CTOD( "  -  -  " ) ) )
                * --- Il componente � obsoleto
                if this.w_DTOBSO<=this.w_DATFIN
                  this.w_oERRORLOG.AddMsgLog("Il componente %1 non pu� essere selezionato",ALLTRIM(this.w_DESCOM))     
                  this.w_oERRORLOG.AddMsgLog("La data di fine validit� del componente (%1) � precedente o uguale alla data di fine attivit� (%2)", DTOC( this.w_DTOBSO ), DTOC( this.w_DATINI ))     
                  this.oParentObject.w_RESCHK = -1
                endif
              endif
              if NOT EMPTY( NVL( this.w_DTOINI, CTOD( "  -  -  " ) ) )
                * --- Il componente  ha una data di inizio validit�
                if this.w_DTOINI>this.w_DATFIN
                  this.w_oERRORLOG.AddMsgLog("Il componente %1 non pu� essere selezionato",ALLTRIM(this.w_DESCOM))     
                  this.w_oERRORLOG.AddMsgLog("La data inizio validit� del componente (%1) � successiva alla data di inizio attivit� (%2)", DTOC( this.w_DTOINI ), DTOC( this.w_DATINI ))     
                  this.oParentObject.w_RESCHK = -1
                endif
              endif
              SKIP
            enddo
             
 Use in _TMP_MCP
            if this.w_oERRORLOG.IsFullLog()
              this.w_oERRORLOG.PrintLog("Impianti/Componenti",.f.,.t.,"Sono stati rilevati impianti/componenti che non possono essere selezionati.%0Si desidera stampare un resoconto?")     
              i_retcode = 'stop'
              return
            endif
          endif
          if this.oParentObject.w_RESCHK<>-1 AND NOT EMPTY(this.oParentObject.w_ATCODNOM) and !Isalt()
            this.w_GSAG_MCP = this.oParentObject.GSAG_MCP
            this.w_RIGA = this.w_GSAG_MCP.search("NOT DELETED() and Not Empty(t_CACODIMP) and ((Nvl(t_CLIIMP,' ')<>" + cp_ToStr(this.oParentObject.w_CODCLI) + " ) OR ((Nvl(t_CLIIMP,' ')=" + cp_ToStr(this.oParentObject.w_CODCLI)+" ) AND "+ cp_ToStr(NVL(this.oParentObject.w_ATCODSED,"")) +"<>Nvl(t_SEDE,' ') and Not Empty("+cp_ToStr(NVL(this.oParentObject.w_ATCODSED,""))+")))")
            if this.w_RIGA<>-1
              if !Ah_yesno("Il nominativo o la sede dell'attivit� non sono congruenti rispetto alla selezione effettuata nel dettaglio impianti.%0Procedo ugualmente?")
                this.w_MESS = Ah_MsgFormat("Operazione abbandonata")
                this.oParentObject.w_RESCHK = -1
              endif
            endif
            * --- Controllo in abbinamento elemento contratto.
            if this.oParentObject.w_RESCHK<>-1
              this.w_GSAG_MDA = this.oParentObject.GSAG_MDA
              this.w_RIGA = this.w_GSAG_MDA.search("NOT DELETED() and (Nvl(t_COCODCON,' ')<>" + cp_ToStr(this.oParentObject.w_CODCLI) + " ) AND NOT EMPTY(NVL(t_DACONTRA,'')) ")
              if this.w_RIGA<>-1
                if !Ah_yesno("Ad una o pi� righe prestazione sono stati abbinati elementi contratto relativi ad un cliente non congruente rispetto al nominativo dell'attivit�.%0Procedo ugualmente?")
                  this.w_MESS = Ah_MsgFormat("Operazione abbandonata")
                  this.oParentObject.w_RESCHK = -1
                endif
              endif
            endif
          endif
          if IsAlt() AND this.oParentObject.w_RESCHK <> -1
            * --- Controllo univocit� prestazioni per pratica
            this.w_GSAG_MDA = this.oParentObject.GSAG_MDA
            this.w_GSAG_MDA.Exec_Select("_TMP_MDA","t_cacodart As Codart, Max(t_FLUNIV) as Univoc, Max(t_NUMPRE) as Maxpre","","","t_cacodart",'Max(t_FLUNIV)="S"')     
            if RECCOUNT()>0
              * --- Ci sono prestazioni non univoche per la pratica
              this.w_CONPRA = this.oParentObject.w_ATCODPRA
              this.w_NOMCUR = "_TMP_MDA"
              this.w_CAMPO = "CODART"
              this.w_CODPRA = ""
              this.w_DESPRA = ""
              this.w_CODART = ""
              this.w_DESATT = ""
              this.w_ORDINA = ""
              this.w_CODATT = ""
              this.w_FLUNIV = ""
              this.w_NUMPRE = 0
              this.w_DATINI = CTOD("  -  -  ")
              this.w_OCCORR = 0
              this.w_OLDPRA = this.oParentObject.w_ATCODPRA
              CREATE CURSOR __tmp__ (CODPRA C(15), DESPRA C(100), CODART C(20), DESATT C(40), ORDINA C(1), CODATT C(20), NUMPRE N(4), DATINI D(8), OCCORR N(4))
              * --- Select from gsag2scu
              do vq_exec with 'gsag2scu',this,'_Curs_gsag2scu','',.f.,.t.
              if used('_Curs_gsag2scu')
                select _Curs_gsag2scu
                locate for 1=1
                do while not(eof())
                this.w_CODPRA = CODPRA
                this.w_DESPRA = DESPRA
                this.w_CODART = CODART
                this.w_DESATT = DESATT
                this.w_ORDINA = ORDINA
                this.w_CODATT = CODATT
                this.w_FLUNIV = FLUNIV
                this.w_NUMPRE = NUMPRE
                this.w_DATINI = TTOD(DATINI)
                if lower(this.oParentObject.cFunction)="load"
                  * --- Se sono in caricamento devo contare anche l'occorrenza della prestazione in quanto presente nel transitorio
                  this.w_OCCORR = OCCORR+1
                else
                  this.w_OCCORR = OCCORR
                endif
                * --- w_ORDINA='A' la riga viene dalla query (raggruppata) GSAG2SCU
                if this.w_OCCORR>this.w_NUMPRE
                  * --- Se il numero di occorrenze � maggiore del massimo previsto
                  *     inserisco la riga relativa al codice articolo
                  INSERT INTO __tmp__ (CODPRA, DESPRA,CODART,DESATT,ORDINA,CODATT,NUMPRE,DATINI,OCCORR) VALUES(this.w_CODPRA,this.w_DESPRA, this.w_CODART,this.w_DESATT,this.w_ORDINA,this.w_CODATT,this.w_NUMPRE,this.w_DATINI,this.w_OCCORR)
                endif
                  select _Curs_gsag2scu
                  continue
                enddo
                use
              endif
              if USED("__tmp__") AND RECCOUNT("__tmp__")>0
                this.w_CONFERMA = .F.
                this.w_MSG = ah_msgformat("Attenzione, esistono prestazioni non univoche. %0Si desidera procedere con il salvataggio dell'attivit�?" )
                do GSAG1SCU with this
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if NOT this.w_CONFERMA
                  this.oParentObject.w_RESCHK = -1
                endif
              endif
            endif
            USE IN SELECT("_TMP_MDA")
          endif
          if IsAlt() AND this.oParentObject.w_RESCHK <> -1 and Empty(this.oParentObject.w_AT__ENTE)
            * --- Controllo ente per il calcolo degli onorari civili o penali
            this.w_GSAG_MDA = this.oParentObject.GSAG_MDA
            this.w_GSAG_MDA.Exec_Select("_TMP_MDA","t_DACODATT","Nvl(t_ARPRESTA,' ') = 'E' or Nvl(t_ARPRESTA,' ') = 'I'","","","")     
            if Reccount("_TMP_MDA") >0 and !Ah_Yesno( "Per il calcolo degli onorari civili o penali � necessario inserire l'ente dell'attivit�. %0Prosegui ugualmente?" )
              this.oParentObject.w_RESCHK = -1
            endif
            USE IN SELECT("_TMP_MDA")
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 and this.pParam="A" and lower(this.oParentObject.cFunction)="edit" 
          this.w_GSAG_MPA.MarkPos()     
          this.w_GSAG_MDA.FirstRow()     
          do while !this.w_GSAG_MDA.Eof_trs()
            this.w_GSAG_MDA.SetRow()     
            if this.w_GSAG_MDA.RowStatus()="U" and this.w_GSAG_MDA.Fullrow()
              this.w_GSAG_MDA.Set("w_DADATMOD",i_datsys)     
              this.w_GSAG_MDA.Set("w_DACODOPE",i_codute)     
            endif
            this.w_GSAG_MDA.NextRow()     
          enddo
          this.w_GSAG_MPA.RePos(.T.)     
        endif
        if !IsAlt() AND this.oParentObject.w_RESCHK <> -1 and this.oParentObject.w_FLPDOC<>"N"
          * --- Controllo univocit� documenti
          * --- Controllo progressivo gi� assegnato
          if this.oParentObject.op_ATNUMDOC=this.oParentObject.w_ATNUMDOC AND this.oParentObject.op_ATALFDOC=this.oParentObject.w_ATALFDOC AND lower(this.oParentObject.cFunction)="load"
            * --- Alla conferma ricalcolo sempre il primo progressivo utile cos� se � gi� stato inserito da altro utente non da problemi
            i_Conn=i_TableProp[this.OFF_ATTI_IDX, 3]
            cp_AskTableProg(this.oParentObject, i_Conn, "NUATT", "i_codazi,w_ATANNDOC,w_ATALFDOC,w_ATNUMDOC")
            if this.oParentObject.op_ATNUMDOC<>this.oParentObject.w_ATNUMDOC OR this.oParentObject.op_ATALFDOC<>this.oParentObject.w_ATALFDOC
              * --- Nel caso un'altro utente avesse gi� utilizzato il progressivo proposto, mando il messaggio e 
              *     faccio in modo che non rivenga calcolato con la CP_NEXTTABLEPROG di fine batch 
              ah_ErrorMsg("Riassegnato progressivo attivit� n. %1",,"",ALLTRIM(STR(this.oParentObject.w_ATNUMDOC,15)))
              this.oParentObject.op_ATNUMDOC = this.oParentObject.w_ATNUMDOC
              this.oParentObject.w_ONUMDOC = this.oParentObject.w_ATNUMDOC
              this.oParentObject.op_ATALFDOC = this.oParentObject.w_ATALFDOC
              this.oParentObject.w_OALFDOC = this.oParentObject.w_ATALFDOC
            endif
          endif
          * --- Aggiornamento Tabella Progressivi - SOLO IN MODIFICA
          if lower(this.oParentObject.cFunction)="edit" AND (this.oParentObject.w_ATANNDOC<>this.oParentObject.w_OLDANNO OR this.oParentObject.w_ATALFDOC<>this.oParentObject.w_OALFDOC OR this.oParentObject.w_ATNUMDOC<>this.oParentObject.w_ONUMDOC)
            i_Conn=i_TableProp[this.OFF_ATTI_IDX, 3]
            cp_NextTableProg(this.oParentObject, i_Conn, "NUATT", "i_codazi,w_ATANNDOC,w_ATALFDOC,w_ATNUMDOC")
          endif
          * --- Select from CHKNUMDOC
          do vq_exec with 'CHKNUMDOC',this,'_Curs_CHKNUMDOC','',.f.,.t.
          if used('_Curs_CHKNUMDOC')
            select _Curs_CHKNUMDOC
            locate for 1=1
            do while not(eof())
            this.w_NUMDOC = _Curs_CHKNUMDOC.ATNUMDOC
            this.w_ALFDOC = _Curs_CHKNUMDOC.ATALFDOC
            this.w_ANNDOC = _Curs_CHKNUMDOC.ATANNDOC
            if this.w_NUMDOC<>0
              if !Ah_yesno("Numero/alfa attivit� gi� inserito in attivit�. Confermi ugualmente?")
                this.oParentObject.w_RESCHK = -1
              endif
            endif
              select _Curs_CHKNUMDOC
              continue
            enddo
            use
          endif
        endif
      case this.pParam="P"
        * --- GSAG_AAT - Si posiziona su ATPERSON
        if this.oParentObject.w_FlagPosiz="S"
          * --- E' stato imputato un valore nel nuovo codice persona - deve posizionarsi su ATPERSON
          this.oParentObject.w_FlagPosiz = "N"
          this.w_OBJ = this.oParentObject.Getctrl("w_ATPERSON")
          if vartype(this.w_OBJ)="O"
            this.w_OBJ.Setfocus()     
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    this.w_ResMsg=createobject("Ah_Message")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='OFFDATTI'
    this.cWorkTables[2]='PAR_AGEN'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='ATT_DCOL'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='PAR_OFFE'
    this.cWorkTables[7]='CAU_ATTI'
    this.cWorkTables[8]='ANEVENTI'
    this.cWorkTables[9]='OFF_ATTI'
    this.cWorkTables[10]='OFF_PART'
    this.cWorkTables[11]='DIPENDEN'
    this.cWorkTables[12]='NOM_CONT'
    this.cWorkTables[13]='ART_ICOL'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_OFF_ATTI')
      use in _Curs_OFF_ATTI
    endif
    if used('_Curs_ATT_DCOL')
      use in _Curs_ATT_DCOL
    endif
    if used('_Curs_GSAR_BDK')
      use in _Curs_GSAR_BDK
    endif
    if used('_Curs_NOM_CONT')
      use in _Curs_NOM_CONT
    endif
    if used('_Curs_NOM_CONT')
      use in _Curs_NOM_CONT
    endif
    if used('_Curs_gsag2scu')
      use in _Curs_gsag2scu
    endif
    if used('_Curs_CHKNUMDOC')
      use in _Curs_CHKNUMDOC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
