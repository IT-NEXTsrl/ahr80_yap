* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bd3                                                        *
*              Aggiorna impianto e componente                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-04                                                      *
* Last revis.: 2010-02-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAMETER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bd3",oParentObject,m.pPARAMETER)
return(i_retval)

define class tgsag_bd3 as StdBatch
  * --- Local variables
  pPARAMETER = space(0)
  w_CHIAVEGIAPRESENTE = .f.
  w_CPROWORD = 0
  * --- WorkFile variables
  ELE_CONT_idx=0
  IMP_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna impianto e componente
    * --- pPARMETER = "C" esegue controllo sulla nuova chiave
    * --- pPARMETER = "S" aggiorna la chiave
    do case
      case this.pPARAMETER = "C"
        * --- Controlla se la chiave che si sta cercando di creare � gi� presente
        this.w_CHIAVEGIAPRESENTE = .F.
        * --- Select from ELE_CONT
        i_nConn=i_TableProp[this.ELE_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2],.t.,this.ELE_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ELE_CONT ";
              +" where ELCONTRA = "+cp_ToStrODBC(this.oParentObject.w_ELCONTRA)+" and ELCODMOD = "+cp_ToStrODBC(this.oParentObject.w_ELCODMOD)+" and ELCODIMP = "+cp_ToStrODBC(this.oParentObject.w_CODIMP)+" and ELCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM)+"";
               ,"_Curs_ELE_CONT")
        else
          select * from (i_cTable);
           where ELCONTRA = this.oParentObject.w_ELCONTRA and ELCODMOD = this.oParentObject.w_ELCODMOD and ELCODIMP = this.oParentObject.w_CODIMP and ELCODCOM = this.oParentObject.w_CODCOM;
            into cursor _Curs_ELE_CONT
        endif
        if used('_Curs_ELE_CONT')
          select _Curs_ELE_CONT
          locate for 1=1
          do while not(eof())
          this.w_CHIAVEGIAPRESENTE = .T.
          * --- Read from IMP_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.IMP_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.IMP_DETT_idx,2],.t.,this.IMP_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWORD"+;
              " from "+i_cTable+" IMP_DETT where ";
                  +"IMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODIMP);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWORD;
              from (i_cTable) where;
                  IMCODICE = this.oParentObject.w_CODIMP;
                  and CPROWNUM = this.oParentObject.w_CODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CPROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
            select _Curs_ELE_CONT
            continue
          enddo
          use
        endif
        if this.w_CHIAVEGIAPRESENTE
          AH_ERRORMSG("Modello: %1%0Contratto: %2%0Impianto: %3%0Riga componente: %4%0Record gi� presente in archivio",48,"",this.oParentObject.w_ELCODMOD,this.oParentObject.w_ELCONTRA,this.oParentObject.w_CODIMP,NVL(this.w_CPROWORD,0))
          i_retcode = 'stop'
          i_retval = .F.
          return
        else
          i_retcode = 'stop'
          i_retval = .T.
          return
        endif
      case this.pPARAMETER = "S"
        * --- Aggiorna impianto e componente
        * --- Write into ELE_CONT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ELE_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELCODIMP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIMP),'ELE_CONT','ELCODIMP');
          +",ELCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM),'ELE_CONT','ELCODCOM');
              +i_ccchkf ;
          +" where ";
              +"ELCONTRA = "+cp_ToStrODBC(this.oParentObject.w_ELCONTRA);
              +" and ELCODMOD = "+cp_ToStrODBC(this.oParentObject.w_ELCODMOD);
              +" and ELCODIMP = "+cp_ToStrODBC(this.oParentObject.w_ELCODIMP);
              +" and ELCODCOM = "+cp_ToStrODBC(this.oParentObject.w_ELCODCOM);
                 )
        else
          update (i_cTable) set;
              ELCODIMP = this.oParentObject.w_CODIMP;
              ,ELCODCOM = this.oParentObject.w_CODCOM;
              &i_ccchkf. ;
           where;
              ELCONTRA = this.oParentObject.w_ELCONTRA;
              and ELCODMOD = this.oParentObject.w_ELCODMOD;
              and ELCODIMP = this.oParentObject.w_ELCODIMP;
              and ELCODCOM = this.oParentObject.w_ELCODCOM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAMETER)
    this.pPARAMETER=pPARAMETER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='IMP_DETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_ELE_CONT')
      use in _Curs_ELE_CONT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAMETER"
endproc
