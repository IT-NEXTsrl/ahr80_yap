* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bpg                                                        *
*              Inserimento multiplo partecipanti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-26                                                      *
* Last revis.: 2014-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bpg",oParentObject)
return(i_retval)

define class tgsag_bpg as StdBatch
  * --- Local variables
  w_Mask = .NULL.
  w_zoompart = space(1)
  w_ObjMPA = .NULL.
  w_NUMRIG = 0
  w_CODRIS = space(5)
  w_COGNOME = space(50)
  w_NOME = space(50)
  w_RIGA = 0
  w_DESCRI = space(40)
  w_TIPRIS = space(1)
  w_CODUTE = 0
  w_TIPORISORSA = space(1)
  w_GRUPPO = space(5)
  w_DATOBSO = ctod("  /  /  ")
  CONTA_PREST = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento multiplo partecipanti sull'attivit�
    this.w_Mask = this.oParentObject
    this.w_zoompart = this.w_Mask.w_zoompart.ccursor
    select (this.w_zoompart)
    COUNT FOR XCHK=1 TO this.CONTA_PREST
    if this.CONTA_PREST = 0
      ah_ErrorMsg("Non ci sono partecipanti selezionati")
      this.oParentObject.w_RESCHK = -1
      i_retcode = 'stop'
      return
    endif
    this.w_ObjMPA = iif(UPPER(this.oParentObject.oParentObject.class)="TGSAG_AAT",this.oParentObject.oParentObject.GSAG_MPA,this.oParentObject.oParentObject.GSAG_MDP)
     
 Select * from (this.w_zoompart) into cursor Temp 
 this.oParentObject.Ecpquit()
    * --- Cicla sul cursore dello zoom relativo alle prestazioni ed esamina i records selezionati
    SELECT TEMP 
 GO TOP 
 scan for XCHK=1
    this.w_TIPRIS = NVL(DPTIPRIS," ")
    this.w_CODRIS = NVL(DPCODICE,SPACE(5))
    this.w_CODUTE = NVL(DPCODUTE,0)
    this.w_TIPORISORSA = NVL(DPTIPRIS," ")
    this.w_COGNOME = NVL(DPCOGNOM,SPACE(50))
    this.w_NOME = NVL(DPNOME,SPACE(50))
    this.w_DESCRI = NVL(DPDESCRI,SPACE(40))
    this.w_GRUPPO = NVL(DPGRUPRE,SPACE(5))
    this.w_DATOBSO = NVL(DPDTOBSO,CP_CHARTODATE("  -  -  "))
    * --- Aggiunge un nuovo record nel dettaglio prestazioni
    if UPPER(this.w_OBJMPA.CLASS)="TCGSAG_MPA"
      if this.w_ObjMPA.Search("NVL(t_PACODRIS, SPACE(5))= " + cp_ToStrODBC(this.w_CODRIS) + " AND NOT DELETED()") = -1 
        this.w_ObjMPA.AddRow()     
        this.w_ObjMPA.w_PATIPRIS = this.w_TIPRIS
        this.w_ObjMPA.w_PACODRIS = this.w_CODRIS
        this.w_ObjMPA.w_CODUTE = this.w_CODUTE
        this.w_ObjMPA.w_TipoRisorsa = this.w_TipoRisorsa
        this.w_ObjMPA.w_COGNOME = this.w_COGNOME
        this.w_ObjMPA.w_NOME = this.w_NOME
        this.w_ObjMPA.w_DESCRI = this.w_DESCRI
        this.w_ObjMPA.w_DENOM = IIF(this.w_ObjMPA.w_PATIPRIS="P",alltrim(this.w_ObjMPA.w_COGNOME)+" "+alltrim(this.w_ObjMPA.w_NOME),alltrim(this.w_ObjMPA.w_DESCRI))
        this.w_ObjMPA.w_PAGRURIS = this.w_GRUPPO
        this.w_ObjMPA.w_DATOBSO = this.w_DATOBSO
        this.w_ObjMPA.SaveRow()     
        this.w_ObjMPA.Refresh()     
        this.w_ObjMPA.bUpdated = .T.
      endif
    else
      if this.w_ObjMPA.Search("NVL(t_DFCODRIS, SPACE(5))= " + cp_ToStrODBC(this.w_CODRIS) + " AND NOT DELETED()") = -1 
        this.w_ObjMPA.AddRow()     
        this.w_ObjMPA.w_DFTIPRIS = this.w_TIPRIS
        this.w_ObjMPA.w_DFCODRIS = this.w_CODRIS
        this.w_ObjMPA.w_CODUTE = this.w_CODUTE
        this.w_ObjMPA.w_TipoRisorsa = this.w_TipoRisorsa
        this.w_ObjMPA.w_COGNOME = this.w_COGNOME
        this.w_ObjMPA.w_NOME = this.w_NOME
        this.w_ObjMPA.w_DESCRI = this.w_DESCRI
        this.w_ObjMPA.w_DENOM = IIF(this.w_ObjMPA.w_DFTIPRIS="P",alltrim(this.w_ObjMPA.w_COGNOME)+" "+alltrim(this.w_ObjMPA.w_NOME),alltrim(this.w_ObjMPA.w_DESCRI))
        this.w_ObjMPA.w_DFGRURIS = this.w_GRUPPO
        this.w_ObjMPA.SaveRow()     
        this.w_ObjMPA.Refresh()     
        this.w_ObjMPA.bUpdated = .T.
      endif
    endif
    SELECT TEMP
    endscan
    * --- Chiudo il cursore
     
 Use in temp
    this.w_Mask = NULL
    this.w_ObjMPA = NULL
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
