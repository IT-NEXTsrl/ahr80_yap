* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_spr                                                        *
*              Stampa prestazioni                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-08-22                                                      *
* Last revis.: 2014-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsag_spr
if cp_TablePropScan('TMPSTPRE') > 0
   ah_errormsg("Attenzione: E' gi� aperta un'istanza di Elenco prestazioni, quella corrente verr� chiusa",48,'')
   return
endif
* --- Fine Area Manuale
return(createobject("tgsag_spr",oParentObject))

* --- Class definition
define class tgsag_spr as StdForm
  Top    = 7
  Left   = 4

  * --- Standard Properties
  Width  = 791
  Height = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-09"
  HelpContextID=141174633
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=77

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  OFFTIPAT_IDX = 0
  CPUSERS_IDX = 0
  ART_ICOL_IDX = 0
  cpusers_IDX = 0
  KEY_ARTI_IDX = 0
  OFF_NOMI_IDX = 0
  CENCOST_IDX = 0
  PAR_ALTE_IDX = 0
  PAR_AGEN_IDX = 0
  cPrg = "gsag_spr"
  cComment = "Stampa prestazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READAZI = space(5)
  o_READAZI = space(5)
  w_READ_AGEN = space(5)
  w_PAFLR_AG = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_COMODO = space(30)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_TIPPRA = space(1)
  w_FL_SOSP = space(1)
  w_InDocum = space(1)
  o_InDocum = space(1)
  w_TIPANA = space(1)
  w_STATO = space(1)
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_DATA_INI = ctot('')
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_DATA_FIN = ctot('')
  w_CODPRA = space(15)
  o_CODPRA = space(15)
  w_RIFERIMENTO = space(30)
  w_CC_CONTO = space(15)
  w_CODNOM = space(20)
  o_CODNOM = space(20)
  w_DESPRA = space(100)
  w_CODRES = space(5)
  o_CODRES = space(5)
  w_CodUte = 0
  w_CODSER = space(20)
  w_PRESERIAL = space(10)
  w_DesSer = space(40)
  o_DesSer = space(40)
  w_FiltTipRig = space(10)
  w_OnoCiv = space(1)
  w_OnoPen = space(1)
  w_OnoStrag = space(1)
  w_DirCiv = space(1)
  w_DirStrag = space(1)
  w_PreGen = space(1)
  w_PreTempo = space(1)
  w_Spese = space(1)
  w_Anticip = space(1)
  w_IMPZERO = space(1)
  w_NORESP = space(1)
  w_NOPRAT = space(1)
  w_DatiPratica = space(1)
  w_DENOM_PART = space(100)
  w_Annotazioni = space(1)
  w_SoloTot = space(1)
  o_SoloTot = space(1)
  w_CONTROP = space(1)
  w_Evasio = space(1)
  w_EscFac = space(1)
  w_FiltTipRig = space(10)
  w_FiltTipRi2 = space(10)
  w_DESUTE = space(20)
  w_DesAgg = space(0)
  w_DESNOM = space(40)
  w_OFDATDOC = ctod('  /  /  ')
  w_CODCLICONTR = space(20)
  w_DPTIPRIS = space(1)
  w_CCDESPIA = space(40)
  w_NORESP = space(1)
  w_CACODART = space(20)
  w_OB_TEST = ctod('  /  /  ')
  w_FiltSosp = space(1)
  w_TIT_PRA = space(5)
  o_TIT_PRA = space(5)
  w_RES_PRA = space(5)
  o_RES_PRA = space(5)
  w_DENOM_TIT = space(100)
  w_DENOM_RES = space(100)
  w_COGNTIT = space(40)
  w_NOMETIT = space(40)
  w_COGNRES = space(40)
  w_NOMERES = space(40)
  w_NOMPRG = space(10)
  w_PAGENPRE = space(1)
  w_DesAggPre = space(40)
  w_DelPreAtt = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsag_spr
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_sprPag1","gsag_spr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_16
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_spr
    Local nCtrl
    For nCtrl=1 to this.page1.Controls(1).ControlCount
       If UPPER(this.page1.Controls(1).Controls(nCtrl).Class)=='STDBOX'
        this.page1.Controls(1).Controls(nCtrl).Visible=IsAlt()
       EndIf
    Next
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='PRA_ENTI'
    this.cWorkTables[4]='PRA_UFFI'
    this.cWorkTables[5]='OFFTIPAT'
    this.cWorkTables[6]='CPUSERS'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='cpusers'
    this.cWorkTables[9]='KEY_ARTI'
    this.cWorkTables[10]='OFF_NOMI'
    this.cWorkTables[11]='CENCOST'
    this.cWorkTables[12]='PAR_ALTE'
    this.cWorkTables[13]='PAR_AGEN'
    return(this.OpenAllTables(13))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READAZI=space(5)
      .w_READ_AGEN=space(5)
      .w_PAFLR_AG=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_COMODO=space(30)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_TIPPRA=space(1)
      .w_FL_SOSP=space(1)
      .w_InDocum=space(1)
      .w_TIPANA=space(1)
      .w_STATO=space(1)
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_OREFIN=space(2)
      .w_DATA_INI=ctot("")
      .w_MINFIN=space(2)
      .w_DATA_FIN=ctot("")
      .w_CODPRA=space(15)
      .w_RIFERIMENTO=space(30)
      .w_CC_CONTO=space(15)
      .w_CODNOM=space(20)
      .w_DESPRA=space(100)
      .w_CODRES=space(5)
      .w_CodUte=0
      .w_CODSER=space(20)
      .w_PRESERIAL=space(10)
      .w_DesSer=space(40)
      .w_FiltTipRig=space(10)
      .w_OnoCiv=space(1)
      .w_OnoPen=space(1)
      .w_OnoStrag=space(1)
      .w_DirCiv=space(1)
      .w_DirStrag=space(1)
      .w_PreGen=space(1)
      .w_PreTempo=space(1)
      .w_Spese=space(1)
      .w_Anticip=space(1)
      .w_IMPZERO=space(1)
      .w_NORESP=space(1)
      .w_NOPRAT=space(1)
      .w_DatiPratica=space(1)
      .w_DENOM_PART=space(100)
      .w_Annotazioni=space(1)
      .w_SoloTot=space(1)
      .w_CONTROP=space(1)
      .w_Evasio=space(1)
      .w_EscFac=space(1)
      .w_FiltTipRig=space(10)
      .w_FiltTipRi2=space(10)
      .w_DESUTE=space(20)
      .w_DesAgg=space(0)
      .w_DESNOM=space(40)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_CODCLICONTR=space(20)
      .w_DPTIPRIS=space(1)
      .w_CCDESPIA=space(40)
      .w_NORESP=space(1)
      .w_CACODART=space(20)
      .w_OB_TEST=ctod("  /  /  ")
      .w_FiltSosp=space(1)
      .w_TIT_PRA=space(5)
      .w_RES_PRA=space(5)
      .w_DENOM_TIT=space(100)
      .w_DENOM_RES=space(100)
      .w_COGNTIT=space(40)
      .w_NOMETIT=space(40)
      .w_COGNRES=space(40)
      .w_NOMERES=space(40)
      .w_NOMPRG=space(10)
      .w_PAGENPRE=space(1)
      .w_DesAggPre=space(40)
      .w_DelPreAtt=space(1)
        .w_READAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READAZI))
          .link_1_1('Full')
        endif
        .w_READ_AGEN = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READ_AGEN))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(5,8,.f.)
        .w_DATFIN = i_datSys
          .DoRTCalc(10,11,.f.)
        .w_TIPPRA = 'T'
        .w_FL_SOSP = 'T'
        .w_InDocum = 'T'
        .w_TIPANA = 'C'
        .w_STATO = 'P'
        .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_CODPRA))
          .link_1_33('Full')
        endif
        .DoRTCalc(24,25,.f.)
        if not(empty(.w_CC_CONTO))
          .link_1_35('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CODNOM))
          .link_1_36('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_CODRES))
          .link_1_40('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_CodUte))
          .link_1_41('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_CODSER))
          .link_1_42('Full')
        endif
          .DoRTCalc(31,32,.f.)
        .w_FiltTipRig = ' '
        .w_OnoCiv = 'I'
        .w_OnoPen = 'E'
        .w_OnoStrag = 'T'
        .w_DirCiv = 'C'
        .w_DirStrag = 'R'
        .w_PreGen = 'G'
        .w_PreTempo = 'P'
        .w_Spese = 'S'
        .w_Anticip = 'A'
        .w_IMPZERO = 'N'
        .w_NORESP = 'N'
        .w_NOPRAT = 'N'
        .w_DatiPratica = 'S'
        .w_DENOM_PART = alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART)
        .w_Annotazioni = 'N'
        .w_SoloTot = 'N'
        .w_CONTROP = 'N'
        .w_Evasio = 'T'
        .w_EscFac = iif(!( .w_InDocum $ 'P-D-N'),'N',iif(Empty(.w_EscFac),'N',.w_EscFac))
        .w_FiltTipRig = ' '
        .w_FiltTipRi2 = ' '
      .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
          .DoRTCalc(55,57,.f.)
        .w_OFDATDOC = i_datsys
          .DoRTCalc(59,59,.f.)
        .w_DPTIPRIS = "P"
          .DoRTCalc(61,61,.f.)
        .w_NORESP = 'N'
      .oPgFrm.Page1.oPag.oObj_1_93.Calculate(IIF(IsAlt(), "Inserire il codice soggetto esterno con ruolo cliente per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto esterno selezionato)", "Inserire il codice del nominativo per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle attivit� ove � presente il nominativo selezionato)"))
      .oPgFrm.Page1.oPag.oObj_1_94.Calculate(IIF(IsAlt(), "Codice responsabile prestazione","Filtro su codice partecipante"))
        .DoRTCalc(63,63,.f.)
        if not(empty(.w_CACODART))
          .link_1_95('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
        .w_OB_TEST = i_INIDAT
        .w_FiltSosp = IIF(.w_FL_SOSP='T','',.w_FL_SOSP)
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_TIT_PRA))
          .link_1_106('Full')
        endif
        .DoRTCalc(67,67,.f.)
        if not(empty(.w_RES_PRA))
          .link_1_107('Full')
        endif
        .w_DENOM_TIT = alltrim(.w_COGNTIT)+' '+alltrim(.w_NOMETIT)
        .w_DENOM_RES = alltrim(.w_COGNRES)+' '+alltrim(.w_NOMERES)
          .DoRTCalc(70,73,.f.)
        .w_NOMPRG = iif(!isalt(),'GSAG1SPR','GSAG_SPR')
          .DoRTCalc(75,75,.f.)
        .w_DesAggPre = IIF(.w_PAFLR_AG='S',ALLTRIM(.w_DesSer),'')
        .w_DelPreAtt = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_68.enabled = this.oPgFrm.Page1.oPag.oBtn_1_68.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_69.enabled = this.oPgFrm.Page1.oPag.oBtn_1_69.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_85.enabled = this.oPgFrm.Page1.oPag.oBtn_1_85.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_118.enabled = this.oPgFrm.Page1.oPag.oBtn_1_118.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_READAZI<>.w_READAZI
            .w_READAZI = i_CODAZI
          .link_1_1('Full')
        endif
        if .o_READAZI<>.w_READAZI
            .w_READ_AGEN = i_CODAZI
          .link_1_2('Full')
        endif
        if .o_OREINI<>.w_OREINI
          .Calculate_MVFLEXPGGM()
        endif
        if .o_MININI<>.w_MININI
          .Calculate_SUGRFFLBCH()
        endif
        if .o_OREFIN<>.w_OREFIN
          .Calculate_FPJEAUJXWE()
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_KTQUMLVLEF()
        endif
        .DoRTCalc(3,16,.t.)
        if .o_OREINI<>.w_OREINI
            .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        endif
        if .o_MININI<>.w_MININI
            .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        endif
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN
            .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        endif
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN
            .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .DoRTCalc(23,46,.t.)
        if .o_CODRES<>.w_CODRES
            .w_DENOM_PART = alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART)
        endif
        .DoRTCalc(48,51,.t.)
        if .o_InDocum<>.w_InDocum
            .w_EscFac = iif(!( .w_InDocum $ 'P-D-N'),'N',iif(Empty(.w_EscFac),'N',.w_EscFac))
        endif
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        if .o_CODNOM<>.w_CODNOM
          .Calculate_PIPSHJRQYL()
        endif
        if .o_CODPRA<>.w_CODPRA
          .Calculate_JQPANHVMGV()
        endif
        if .o_SoloTot<>.w_SoloTot
          .Calculate_VVXHRZAWII()
        endif
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate(IIF(IsAlt(), "Inserire il codice soggetto esterno con ruolo cliente per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto esterno selezionato)", "Inserire il codice del nominativo per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle attivit� ove � presente il nominativo selezionato)"))
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate(IIF(IsAlt(), "Codice responsabile prestazione","Filtro su codice partecipante"))
        .DoRTCalc(53,62,.t.)
          .link_1_95('Full')
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
        .DoRTCalc(64,64,.t.)
            .w_FiltSosp = IIF(.w_FL_SOSP='T','',.w_FL_SOSP)
        .DoRTCalc(66,67,.t.)
        if .o_TIT_PRA<>.w_TIT_PRA
            .w_DENOM_TIT = alltrim(.w_COGNTIT)+' '+alltrim(.w_NOMETIT)
        endif
        if .o_RES_PRA<>.w_RES_PRA
            .w_DENOM_RES = alltrim(.w_COGNRES)+' '+alltrim(.w_NOMERES)
        endif
        .DoRTCalc(70,75,.t.)
        if .o_DesSer<>.w_DesSer
            .w_DesAggPre = IIF(.w_PAFLR_AG='S',ALLTRIM(.w_DesSer),'')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(77,77,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate(IIF(IsAlt(), "Inserire il codice soggetto esterno con ruolo cliente per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto esterno selezionato)", "Inserire il codice del nominativo per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle attivit� ove � presente il nominativo selezionato)"))
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate(IIF(IsAlt(), "Codice responsabile prestazione","Filtro su codice partecipante"))
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate()
    endwith
  return

  proc Calculate_MVFLEXPGGM()
    with this
          * --- Resetto w_oreini
          .w_OREINI = .ZeroFill(.w_OREINI)
    endwith
  endproc
  proc Calculate_SUGRFFLBCH()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_FPJEAUJXWE()
    with this
          * --- Resetto w_orefin
          .w_OREFIN = .ZeroFill(.w_OREFIN)
    endwith
  endproc
  proc Calculate_KTQUMLVLEF()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_NRTSRHMIVM()
    with this
          * --- Inizializza date
          gsag_ba3(this;
              ,'PRES';
             )
    endwith
  endproc
  proc Calculate_SXCNCBAXOP()
    with this
          * --- Inizializza ora finale
          .w_OREFIN = "23"
          .w_MINFIN = "59"
    endwith
  endproc
  proc Calculate_LYBISUKBKM()
    with this
          * --- Imposta data odierna
          .w_DATINI = i_DatSys
          .w_DATFIN = i_DatSys
          .w_OREINI = '00'
          .w_MININI = '00'
          .w_OREFIN = '23'
          .w_MINFIN = '59'
          .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '01-01-1900 00:00:00'))
          .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', '31-12-2999 23:59:59'))
    endwith
  endproc
  proc Calculate_KGFZQZXEQS()
    with this
          * --- Controllo iniziale - se alterego gsal_bck
          gsag_bsp(this;
              ,'CONTROLLO';
             )
    endwith
  endproc
  proc Calculate_PIPSHJRQYL()
    with this
          * --- Controllo nominativo=cliente
          gsag_bsp(this;
              ,'CLIENTE';
             )
    endwith
  endproc
  proc Calculate_JQPANHVMGV()
    with this
          * --- Azzera cliente
          .w_CODNOM = IIF(NOT EMPTY(.w_CODPRA) AND IsAlt(), space(20), .w_CODNOM)
          .w_DESNOM = IIF(NOT EMPTY(.w_CODPRA) AND IsAlt(), space(40), .w_DESNOM)
          .w_TIT_PRA = ''
          .link_1_106('Full')
          .w_RES_PRA = ''
          .link_1_107('Full')
    endwith
  endproc
  proc Calculate_VVXHRZAWII()
    with this
          * --- Gestione mutua esclusione check
          .w_DatiPratica = IIF(.w_SOLOTOT='S','N',.w_DatiPratica)
          .w_Annotazioni = IIF(.w_SOLOTOT='S','N',.w_Annotazioni)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDatiPratica_1_58.enabled = this.oPgFrm.Page1.oPag.oDatiPratica_1_58.mCond()
    this.oPgFrm.Page1.oPag.oAnnotazioni_1_60.enabled = this.oPgFrm.Page1.oPag.oAnnotazioni_1_60.mCond()
    this.oPgFrm.Page1.oPag.oCONTROP_1_62.enabled = this.oPgFrm.Page1.oPag.oCONTROP_1_62.mCond()
    this.oPgFrm.Page1.oPag.oEvasio_1_63.enabled_(this.oPgFrm.Page1.oPag.oEvasio_1_63.mCond())
    this.oPgFrm.Page1.oPag.oEscFac_1_64.enabled = this.oPgFrm.Page1.oPag.oEscFac_1_64.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_68.enabled = this.oPgFrm.Page1.oPag.oBtn_1_68.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_18.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_1_18.mHide()
    this.oPgFrm.Page1.oPag.oALFDOC_1_19.visible=!this.oPgFrm.Page1.oPag.oALFDOC_1_19.mHide()
    this.oPgFrm.Page1.oPag.oTIPPRA_1_20.visible=!this.oPgFrm.Page1.oPag.oTIPPRA_1_20.mHide()
    this.oPgFrm.Page1.oPag.oFL_SOSP_1_21.visible=!this.oPgFrm.Page1.oPag.oFL_SOSP_1_21.mHide()
    this.oPgFrm.Page1.oPag.oInDocum_1_22.visible=!this.oPgFrm.Page1.oPag.oInDocum_1_22.mHide()
    this.oPgFrm.Page1.oPag.oTIPANA_1_23.visible=!this.oPgFrm.Page1.oPag.oTIPANA_1_23.mHide()
    this.oPgFrm.Page1.oPag.oRIFERIMENTO_1_34.visible=!this.oPgFrm.Page1.oPag.oRIFERIMENTO_1_34.mHide()
    this.oPgFrm.Page1.oPag.oCC_CONTO_1_35.visible=!this.oPgFrm.Page1.oPag.oCC_CONTO_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCODNOM_1_36.visible=!this.oPgFrm.Page1.oPag.oCODNOM_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oPRESERIAL_1_43.visible=!this.oPgFrm.Page1.oPag.oPRESERIAL_1_43.mHide()
    this.oPgFrm.Page1.oPag.oFiltTipRig_1_45.visible=!this.oPgFrm.Page1.oPag.oFiltTipRig_1_45.mHide()
    this.oPgFrm.Page1.oPag.oOnoCiv_1_46.visible=!this.oPgFrm.Page1.oPag.oOnoCiv_1_46.mHide()
    this.oPgFrm.Page1.oPag.oOnoPen_1_47.visible=!this.oPgFrm.Page1.oPag.oOnoPen_1_47.mHide()
    this.oPgFrm.Page1.oPag.oOnoStrag_1_48.visible=!this.oPgFrm.Page1.oPag.oOnoStrag_1_48.mHide()
    this.oPgFrm.Page1.oPag.oDirCiv_1_49.visible=!this.oPgFrm.Page1.oPag.oDirCiv_1_49.mHide()
    this.oPgFrm.Page1.oPag.oDirStrag_1_50.visible=!this.oPgFrm.Page1.oPag.oDirStrag_1_50.mHide()
    this.oPgFrm.Page1.oPag.oPreGen_1_51.visible=!this.oPgFrm.Page1.oPag.oPreGen_1_51.mHide()
    this.oPgFrm.Page1.oPag.oPreTempo_1_52.visible=!this.oPgFrm.Page1.oPag.oPreTempo_1_52.mHide()
    this.oPgFrm.Page1.oPag.oSpese_1_53.visible=!this.oPgFrm.Page1.oPag.oSpese_1_53.mHide()
    this.oPgFrm.Page1.oPag.oAnticip_1_54.visible=!this.oPgFrm.Page1.oPag.oAnticip_1_54.mHide()
    this.oPgFrm.Page1.oPag.oNORESP_1_56.visible=!this.oPgFrm.Page1.oPag.oNORESP_1_56.mHide()
    this.oPgFrm.Page1.oPag.oNOPRAT_1_57.visible=!this.oPgFrm.Page1.oPag.oNOPRAT_1_57.mHide()
    this.oPgFrm.Page1.oPag.oDatiPratica_1_58.visible=!this.oPgFrm.Page1.oPag.oDatiPratica_1_58.mHide()
    this.oPgFrm.Page1.oPag.oSoloTot_1_61.visible=!this.oPgFrm.Page1.oPag.oSoloTot_1_61.mHide()
    this.oPgFrm.Page1.oPag.oCONTROP_1_62.visible=!this.oPgFrm.Page1.oPag.oCONTROP_1_62.mHide()
    this.oPgFrm.Page1.oPag.oEvasio_1_63.visible=!this.oPgFrm.Page1.oPag.oEvasio_1_63.mHide()
    this.oPgFrm.Page1.oPag.oEscFac_1_64.visible=!this.oPgFrm.Page1.oPag.oEscFac_1_64.mHide()
    this.oPgFrm.Page1.oPag.oFiltTipRig_1_65.visible=!this.oPgFrm.Page1.oPag.oFiltTipRig_1_65.mHide()
    this.oPgFrm.Page1.oPag.oFiltTipRi2_1_66.visible=!this.oPgFrm.Page1.oPag.oFiltTipRi2_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page1.oPag.oDESNOM_1_77.visible=!this.oPgFrm.Page1.oPag.oDESNOM_1_77.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_78.visible=!this.oPgFrm.Page1.oPag.oStr_1_78.mHide()
    this.oPgFrm.Page1.oPag.oCCDESPIA_1_87.visible=!this.oPgFrm.Page1.oPag.oCCDESPIA_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page1.oPag.oNORESP_1_91.visible=!this.oPgFrm.Page1.oPag.oNORESP_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_96.visible=!this.oPgFrm.Page1.oPag.oStr_1_96.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_98.visible=!this.oPgFrm.Page1.oPag.oStr_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_99.visible=!this.oPgFrm.Page1.oPag.oStr_1_99.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_104.visible=!this.oPgFrm.Page1.oPag.oStr_1_104.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_105.visible=!this.oPgFrm.Page1.oPag.oStr_1_105.mHide()
    this.oPgFrm.Page1.oPag.oTIT_PRA_1_106.visible=!this.oPgFrm.Page1.oPag.oTIT_PRA_1_106.mHide()
    this.oPgFrm.Page1.oPag.oRES_PRA_1_107.visible=!this.oPgFrm.Page1.oPag.oRES_PRA_1_107.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_108.visible=!this.oPgFrm.Page1.oPag.oStr_1_108.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_109.visible=!this.oPgFrm.Page1.oPag.oStr_1_109.mHide()
    this.oPgFrm.Page1.oPag.oDENOM_TIT_1_110.visible=!this.oPgFrm.Page1.oPag.oDENOM_TIT_1_110.mHide()
    this.oPgFrm.Page1.oPag.oDENOM_RES_1_111.visible=!this.oPgFrm.Page1.oPag.oDENOM_RES_1_111.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_117.visible=!this.oPgFrm.Page1.oPag.oStr_1_117.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_118.visible=!this.oPgFrm.Page1.oPag.oBtn_1_118.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_120.visible=!this.oPgFrm.Page1.oPag.oStr_1_120.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_123.visible=!this.oPgFrm.Page1.oPag.oStr_1_123.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_124.visible=!this.oPgFrm.Page1.oPag.oStr_1_124.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_125.visible=!this.oPgFrm.Page1.oPag.oStr_1_125.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NRTSRHMIVM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_SXCNCBAXOP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
        if lower(cEvent)==lower("ImpostaDataOdierna")
          .Calculate_LYBISUKBKM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_KGFZQZXEQS()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_93.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_94.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_97.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_spr
    * --- Al checked/unchecked della riga sullo zoom non � immediatamente visualizzato
    * --- o nascosto il bottone completa (la HideControls scatta solo al cambio
    * --- di riga dello zoom, per cui forzo la chiamata).
    If upper(cEvent)='W_AGKRA_ZOOM ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM ROW UNCHECKED' OR upper(cEvent)='W_AGKRA_ZOOM3 ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM3 ROW UNCHECKED'
        this.mHideControls()
    endif
    
    
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAGENPRE";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PAGENPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_PAGENPRE = NVL(_Link_.PAGENPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_PAGENPRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READ_AGEN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READ_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READ_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLR_AG";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READ_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READ_AGEN)
            select PACODAZI,PAFLR_AG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READ_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PAFLR_AG = NVL(_Link_.PAFLR_AG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READ_AGEN = space(5)
      endif
      this.w_PAFLR_AG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READ_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_1_33'),i_cWhere,'',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(100)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CC_CONTO
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CC_CONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CC_CONTO)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CC_CONTO))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CC_CONTO)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CC_CONTO) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCC_CONTO_1_35'),i_cWhere,'',"Centri di costo/Ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CC_CONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CC_CONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CC_CONTO)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CC_CONTO = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CC_CONTO = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CC_CONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_36'),i_cWhere,'GSAR_ANO',""+iif(isalt (), "Soggetti esterni", "Nominativi") +"",''+iif(IsAlt(),"GSAG_BSP", "GSOF3QNO")+'.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(20)
      endif
      this.w_DESNOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRES
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_DPTIPRIS;
                     ,'DPCODICE',trim(this.w_CODRES))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODRES)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODRES) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCODRES_1_40'),i_cWhere,'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPTIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODRES);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_DPTIPRIS;
                       ,'DPCODICE',this.w_CODRES)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRES = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODRES = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodUte
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodUte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CodUte);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CodUte)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CodUte) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oCodUte_1_41'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodUte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CodUte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CodUte)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodUte = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CodUte = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodUte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSER
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODSER))
          select CACODICE,CADESART,CADESSUP,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSER)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODSER) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODSER_1_42'),i_cWhere,'',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Default")+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODSER)
            select CACODICE,CADESART,CADESSUP,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSER = NVL(_Link_.CACODICE,space(20))
      this.w_comodo = NVL(_Link_.CADESART,space(30))
      this.w_DesAgg = NVL(_Link_.CADESSUP,space(0))
      this.w_CACODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODSER = space(20)
      endif
      this.w_comodo = space(30)
      this.w_DesAgg = space(0)
      this.w_CACODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODART
  func Link_1_95(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CACODART)
            select ARCODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODART = NVL(_Link_.ARCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CACODART = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIT_PRA
  func Link_1_106(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIT_PRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_DPTIPRIS;
                     ,'DPCODICE',trim(this.w_TIT_PRA))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIT_PRA)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TIT_PRA) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oTIT_PRA_1_106'),i_cWhere,'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPTIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIT_PRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_TIT_PRA);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_DPTIPRIS;
                       ,'DPCODICE',this.w_TIT_PRA)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIT_PRA = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNTIT = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMETIT = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TIT_PRA = space(5)
      endif
      this.w_COGNTIT = space(40)
      this.w_NOMETIT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIT_PRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RES_PRA
  func Link_1_107(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RES_PRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_DPTIPRIS;
                     ,'DPCODICE',trim(this.w_RES_PRA))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RES_PRA)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RES_PRA) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oRES_PRA_1_107'),i_cWhere,'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPTIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RES_PRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_RES_PRA);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_DPTIPRIS;
                       ,'DPCODICE',this.w_RES_PRA)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RES_PRA = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNRES = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMERES = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RES_PRA = space(5)
      endif
      this.w_COGNRES = space(40)
      this.w_NOMERES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RES_PRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_16.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_16.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_17.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_17.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_18.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_18.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_19.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_19.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPPRA_1_20.RadioValue()==this.w_TIPPRA)
      this.oPgFrm.Page1.oPag.oTIPPRA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFL_SOSP_1_21.RadioValue()==this.w_FL_SOSP)
      this.oPgFrm.Page1.oPag.oFL_SOSP_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oInDocum_1_22.RadioValue()==this.w_InDocum)
      this.oPgFrm.Page1.oPag.oInDocum_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPANA_1_23.RadioValue()==this.w_TIPANA)
      this.oPgFrm.Page1.oPag.oTIPANA_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_24.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRA_1_33.value==this.w_CODPRA)
      this.oPgFrm.Page1.oPag.oCODPRA_1_33.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oRIFERIMENTO_1_34.value==this.w_RIFERIMENTO)
      this.oPgFrm.Page1.oPag.oRIFERIMENTO_1_34.value=this.w_RIFERIMENTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCC_CONTO_1_35.value==this.w_CC_CONTO)
      this.oPgFrm.Page1.oPag.oCC_CONTO_1_35.value=this.w_CC_CONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_36.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_36.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRA_1_38.value==this.w_DESPRA)
      this.oPgFrm.Page1.oPag.oDESPRA_1_38.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODRES_1_40.value==this.w_CODRES)
      this.oPgFrm.Page1.oPag.oCODRES_1_40.value=this.w_CODRES
    endif
    if not(this.oPgFrm.Page1.oPag.oCodUte_1_41.value==this.w_CodUte)
      this.oPgFrm.Page1.oPag.oCodUte_1_41.value=this.w_CodUte
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSER_1_42.value==this.w_CODSER)
      this.oPgFrm.Page1.oPag.oCODSER_1_42.value=this.w_CODSER
    endif
    if not(this.oPgFrm.Page1.oPag.oPRESERIAL_1_43.value==this.w_PRESERIAL)
      this.oPgFrm.Page1.oPag.oPRESERIAL_1_43.value=this.w_PRESERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDesSer_1_44.value==this.w_DesSer)
      this.oPgFrm.Page1.oPag.oDesSer_1_44.value=this.w_DesSer
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltTipRig_1_45.RadioValue()==this.w_FiltTipRig)
      this.oPgFrm.Page1.oPag.oFiltTipRig_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOnoCiv_1_46.RadioValue()==this.w_OnoCiv)
      this.oPgFrm.Page1.oPag.oOnoCiv_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOnoPen_1_47.RadioValue()==this.w_OnoPen)
      this.oPgFrm.Page1.oPag.oOnoPen_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOnoStrag_1_48.RadioValue()==this.w_OnoStrag)
      this.oPgFrm.Page1.oPag.oOnoStrag_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDirCiv_1_49.RadioValue()==this.w_DirCiv)
      this.oPgFrm.Page1.oPag.oDirCiv_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDirStrag_1_50.RadioValue()==this.w_DirStrag)
      this.oPgFrm.Page1.oPag.oDirStrag_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPreGen_1_51.RadioValue()==this.w_PreGen)
      this.oPgFrm.Page1.oPag.oPreGen_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPreTempo_1_52.RadioValue()==this.w_PreTempo)
      this.oPgFrm.Page1.oPag.oPreTempo_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSpese_1_53.RadioValue()==this.w_Spese)
      this.oPgFrm.Page1.oPag.oSpese_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAnticip_1_54.RadioValue()==this.w_Anticip)
      this.oPgFrm.Page1.oPag.oAnticip_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPZERO_1_55.RadioValue()==this.w_IMPZERO)
      this.oPgFrm.Page1.oPag.oIMPZERO_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNORESP_1_56.RadioValue()==this.w_NORESP)
      this.oPgFrm.Page1.oPag.oNORESP_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPRAT_1_57.RadioValue()==this.w_NOPRAT)
      this.oPgFrm.Page1.oPag.oNOPRAT_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDatiPratica_1_58.RadioValue()==this.w_DatiPratica)
      this.oPgFrm.Page1.oPag.oDatiPratica_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_PART_1_59.value==this.w_DENOM_PART)
      this.oPgFrm.Page1.oPag.oDENOM_PART_1_59.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page1.oPag.oAnnotazioni_1_60.RadioValue()==this.w_Annotazioni)
      this.oPgFrm.Page1.oPag.oAnnotazioni_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSoloTot_1_61.RadioValue()==this.w_SoloTot)
      this.oPgFrm.Page1.oPag.oSoloTot_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTROP_1_62.RadioValue()==this.w_CONTROP)
      this.oPgFrm.Page1.oPag.oCONTROP_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEvasio_1_63.RadioValue()==this.w_Evasio)
      this.oPgFrm.Page1.oPag.oEvasio_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEscFac_1_64.RadioValue()==this.w_EscFac)
      this.oPgFrm.Page1.oPag.oEscFac_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltTipRig_1_65.RadioValue()==this.w_FiltTipRig)
      this.oPgFrm.Page1.oPag.oFiltTipRig_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltTipRi2_1_66.RadioValue()==this.w_FiltTipRi2)
      this.oPgFrm.Page1.oPag.oFiltTipRi2_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_70.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_70.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_77.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_77.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_87.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_87.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oNORESP_1_91.RadioValue()==this.w_NORESP)
      this.oPgFrm.Page1.oPag.oNORESP_1_91.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIT_PRA_1_106.value==this.w_TIT_PRA)
      this.oPgFrm.Page1.oPag.oTIT_PRA_1_106.value=this.w_TIT_PRA
    endif
    if not(this.oPgFrm.Page1.oPag.oRES_PRA_1_107.value==this.w_RES_PRA)
      this.oPgFrm.Page1.oPag.oRES_PRA_1_107.value=this.w_RES_PRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_TIT_1_110.value==this.w_DENOM_TIT)
      this.oPgFrm.Page1.oPag.oDENOM_TIT_1_110.value=this.w_DENOM_TIT
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_RES_1_111.value==this.w_DENOM_RES)
      this.oPgFrm.Page1.oPag.oDENOM_RES_1_111.value=this.w_DENOM_RES
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(VAL(.w_PRESERIAL)<>0 OR EMPTY(.w_PRESERIAL))  and not(.w_PAGENPRE<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRESERIAL_1_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_READAZI = this.w_READAZI
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    this.o_InDocum = this.w_InDocum
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_CODPRA = this.w_CODPRA
    this.o_CODNOM = this.w_CODNOM
    this.o_CODRES = this.w_CODRES
    this.o_DesSer = this.w_DesSer
    this.o_SoloTot = this.w_SoloTot
    this.o_TIT_PRA = this.w_TIT_PRA
    this.o_RES_PRA = this.w_RES_PRA
    return

enddefine

* --- Define pages as container
define class tgsag_sprPag1 as StdContainer
  Width  = 787
  height = 493
  stdWidth  = 787
  stdheight = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_16 as StdField with uid="LOBOVQPRWD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 171680202,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=86, Top=8

  func oDATINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_17 as StdField with uid="OWDJHUXKLB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 93233610,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=86, Top=33

  func oDATFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oNUMDOC_1_18 as StdField with uid="ALSVTWVYIC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'attivit�",;
    HelpContextID = 3210538,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=326, Top=33, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMDOC_1_18.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oALFDOC_1_19 as StdField with uid="TNHYATACTB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie dell'attivit�",;
    HelpContextID = 3241722,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=452, Top=33, InputMask=replicate('X',10)

  func oALFDOC_1_19.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc


  add object oTIPPRA_1_20 as StdCombo with uid="MQGYJCRNXQ",rtseq=12,rtrep=.f.,left=327,top=8,width=146,height=21;
    , HelpContextID = 32823498;
    , cFormVar="w_TIPPRA",RowSource=""+"Aperte,"+"Chiuse,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPPRA_1_20.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPPRA_1_20.GetRadio()
    this.Parent.oContained.w_TIPPRA = this.RadioValue()
    return .t.
  endfunc

  func oTIPPRA_1_20.SetRadio()
    this.Parent.oContained.w_TIPPRA=trim(this.Parent.oContained.w_TIPPRA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPPRA=='A',1,;
      iif(this.Parent.oContained.w_TIPPRA=='C',2,;
      iif(this.Parent.oContained.w_TIPPRA=='T',3,;
      0)))
  endfunc

  func oTIPPRA_1_20.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc


  add object oFL_SOSP_1_21 as StdCombo with uid="SOZSLOERCZ",rtseq=13,rtrep=.f.,left=327,top=33,width=146,height=21;
    , ToolTipText = "Selezione su sospensione";
    , HelpContextID = 2156202;
    , cFormVar="w_FL_SOSP",RowSource=""+"Solo pratiche sospese,"+"Escludi pratiche sospese,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFL_SOSP_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFL_SOSP_1_21.GetRadio()
    this.Parent.oContained.w_FL_SOSP = this.RadioValue()
    return .t.
  endfunc

  func oFL_SOSP_1_21.SetRadio()
    this.Parent.oContained.w_FL_SOSP=trim(this.Parent.oContained.w_FL_SOSP)
    this.value = ;
      iif(this.Parent.oContained.w_FL_SOSP=='S',1,;
      iif(this.Parent.oContained.w_FL_SOSP=='N',2,;
      iif(this.Parent.oContained.w_FL_SOSP=='T',3,;
      0)))
  endfunc

  func oFL_SOSP_1_21.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oInDocum_1_22 as StdCombo with uid="OYHKQWLGYE",rtseq=14,rtrep=.f.,left=582,top=32,width=179,height=22;
    , ToolTipText = "Stato";
    , HelpContextID = 214332538;
    , cFormVar="w_InDocum",RowSource=""+"Da proformare,"+"Da fatturare (Proformate),"+"Da fatturare (tutte),"+"Fatturate,"+"Proformate e fatturate,"+"Tutte,"+"Proformate o fatturate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oInDocum_1_22.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'F',;
    iif(this.value =5,'A',;
    iif(this.value =6,'T',;
    iif(this.value =7,'M',;
    space(1)))))))))
  endfunc
  func oInDocum_1_22.GetRadio()
    this.Parent.oContained.w_InDocum = this.RadioValue()
    return .t.
  endfunc

  func oInDocum_1_22.SetRadio()
    this.Parent.oContained.w_InDocum=trim(this.Parent.oContained.w_InDocum)
    this.value = ;
      iif(this.Parent.oContained.w_InDocum=='N',1,;
      iif(this.Parent.oContained.w_InDocum=='P',2,;
      iif(this.Parent.oContained.w_InDocum=='D',3,;
      iif(this.Parent.oContained.w_InDocum=='F',4,;
      iif(this.Parent.oContained.w_InDocum=='A',5,;
      iif(this.Parent.oContained.w_InDocum=='T',6,;
      iif(this.Parent.oContained.w_InDocum=='M',7,;
      0)))))))
  endfunc

  func oInDocum_1_22.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_STATO<>'P')
    endwith
  endfunc


  add object oTIPANA_1_23 as StdCombo with uid="OOYOOVQSKI",rtseq=15,rtrep=.f.,left=582,top=33,width=146,height=21;
    , HelpContextID = 38000842;
    , cFormVar="w_TIPANA",RowSource=""+"Costo,"+"Ricavo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPANA_1_23.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPANA_1_23.GetRadio()
    this.Parent.oContained.w_TIPANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPANA_1_23.SetRadio()
    this.Parent.oContained.w_TIPANA=trim(this.Parent.oContained.w_TIPANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPANA=='C',1,;
      iif(this.Parent.oContained.w_TIPANA=='R',2,;
      0))
  endfunc

  func oTIPANA_1_23.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oSTATO_1_24 as StdCombo with uid="FSUQLHAEMH",rtseq=16,rtrep=.f.,left=582,top=8,width=179,height=21;
    , ToolTipText = "Stato";
    , HelpContextID = 52542938;
    , cFormVar="w_STATO",RowSource=""+"Completate,"+"Da completare,"+"Da completare (solo evase),"+"Da completare (solo non evase),"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_24.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'N',;
    iif(this.value =3,'F',;
    iif(this.value =4,'S',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oSTATO_1_24.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_24.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='P',1,;
      iif(this.Parent.oContained.w_STATO=='N',2,;
      iif(this.Parent.oContained.w_STATO=='F',3,;
      iif(this.Parent.oContained.w_STATO=='S',4,;
      iif(this.Parent.oContained.w_STATO=='T',5,;
      0)))))
  endfunc

  add object oCODPRA_1_33 as StdField with uid="HTRKXPXTPK",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 32871386,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=86, Top=58, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oRIFERIMENTO_1_34 as StdField with uid="YATILJIPXZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_RIFERIMENTO", cQueryName = "RIFERIMENTO",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento della pratica attribuito dal cliente",;
    HelpContextID = 167456709,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=86, Top=83, InputMask=replicate('X',30)

  func oRIFERIMENTO_1_34.mHide()
    with this.Parent.oContained
      return (!IsAlt() )
    endwith
  endfunc

  add object oCC_CONTO_1_35 as StdField with uid="PALBMHFMEH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CC_CONTO", cQueryName = "CC_CONTO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 181342325,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=354, Top=83, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CC_CONTO"

  func oCC_CONTO_1_35.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S')
    endwith
  endfunc

  func oCC_CONTO_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oCC_CONTO_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCC_CONTO_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCC_CONTO_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Centri di costo/Ricavo",'',this.parent.oContained
  endproc

  add object oCODNOM_1_36 as StdField with uid="XRMWQEMWPW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 103257050,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=86, Top=133, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_36.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODPRA) AND ISALT())
    endwith
  endfunc

  func oCODNOM_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',""+iif(isalt (), "Soggetti esterni", "Nominativi") +"",''+iif(IsAlt(),"GSAG_BSP", "GSOF3QNO")+'.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_1_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc

  add object oDESPRA_1_38 as StdField with uid="OZIZCGPPPT",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 32812490,;
   bGlobalFont=.t.,;
    Height=21, Width=553, Left=224, Top=58, InputMask=replicate('X',100)

  add object oCODRES_1_40 as StdField with uid="OFXKGBVPDG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CODRES", cQueryName = "CODRES",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 12817370,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=86, Top=158, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_DPTIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_CODRES"

  func oCODRES_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODRES_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODRES_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_DPTIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_DPTIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCODRES_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oCodUte_1_41 as StdField with uid="QPYVSNYZHN",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CodUte", cQueryName = "CodUte",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente ultima modifica",;
    HelpContextID = 70356006,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=477, Top=158, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_CodUte"

  func oCodUte_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodUte_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodUte_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oCodUte_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oCODSER_1_42 as StdField with uid="XLNRNSVNEW",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODSER", cQueryName = "CODSER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 29529050,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=86, Top=183, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODSER"

  func oCODSER_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSER_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSER_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODSER_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Default")+'.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oPRESERIAL_1_43 as StdField with uid="DYDDJNZPAW",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PRESERIAL", cQueryName = "PRESERIAL",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo prestazione: effettua la ricerca mediante il seriale della prestazione",;
    HelpContextID = 238912759,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=319, Top=183, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  func oPRESERIAL_1_43.mHide()
    with this.Parent.oContained
      return (.w_PAGENPRE<>'S')
    endwith
  endfunc

  proc oPRESERIAL_1_43.mAfter
    with this.Parent.oContained
      .w_PRESERIAL = IIF(!empty(.w_PRESERIAL), right(repl('0', 10) + alltrim(.w_PRESERIAL), 10), space(10))
    endwith
  endproc

  func oPRESERIAL_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PRESERIAL)<>0 OR EMPTY(.w_PRESERIAL))
    endwith
    return bRes
  endfunc

  add object oDesSer_1_44 as AH_SEARCHFLD with uid="HAEQFJXMXN",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DesSer", cQueryName = "DesSer",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prestazione o parte di essa",;
    HelpContextID = 4223542,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=477, Top=183, InputMask=replicate('X',40)


  add object oFiltTipRig_1_45 as StdCombo with uid="BXKRDNCYGK",value=5,rtseq=33,rtrep=.f.,left=86,top=208,width=207,height=21;
    , ToolTipText = "Tipologia riga per la generazione del documento";
    , HelpContextID = 106001464;
    , cFormVar="w_FiltTipRig",RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. Acquisti,"+"Entrambi,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFiltTipRig_1_45.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'A',;
    iif(this.value =4,'E',;
    iif(this.value =5,' ',;
    space(10)))))))
  endfunc
  func oFiltTipRig_1_45.GetRadio()
    this.Parent.oContained.w_FiltTipRig = this.RadioValue()
    return .t.
  endfunc

  func oFiltTipRig_1_45.SetRadio()
    this.Parent.oContained.w_FiltTipRig=trim(this.Parent.oContained.w_FiltTipRig)
    this.value = ;
      iif(this.Parent.oContained.w_FiltTipRig=='N',1,;
      iif(this.Parent.oContained.w_FiltTipRig=='D',2,;
      iif(this.Parent.oContained.w_FiltTipRig=='A',3,;
      iif(this.Parent.oContained.w_FiltTipRig=='E',4,;
      iif(this.Parent.oContained.w_FiltTipRig=='',5,;
      0)))))
  endfunc

  func oFiltTipRig_1_45.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oOnoCiv_1_46 as StdCheck with uid="ELVFIVSHDY",rtseq=34,rtrep=.f.,left=86, top=245, caption="Onorario civile",;
    ToolTipText = "Se attivo, stampa gli onorari civili",;
    HelpContextID = 74464230,;
    cFormVar="w_OnoCiv", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOnoCiv_1_46.RadioValue()
    return(iif(this.value =1,'I',;
    space(1)))
  endfunc
  func oOnoCiv_1_46.GetRadio()
    this.Parent.oContained.w_OnoCiv = this.RadioValue()
    return .t.
  endfunc

  func oOnoCiv_1_46.SetRadio()
    this.Parent.oContained.w_OnoCiv=trim(this.Parent.oContained.w_OnoCiv)
    this.value = ;
      iif(this.Parent.oContained.w_OnoCiv=='I',1,;
      0)
  endfunc

  func oOnoCiv_1_46.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oOnoPen_1_47 as StdCheck with uid="FATEDFEVGH",rtseq=35,rtrep=.f.,left=320, top=245, caption="Onorario penale",;
    ToolTipText = "Se attivo, stampa gli onorari penali",;
    HelpContextID = 205339622,;
    cFormVar="w_OnoPen", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOnoPen_1_47.RadioValue()
    return(iif(this.value =1,'E',;
    space(1)))
  endfunc
  func oOnoPen_1_47.GetRadio()
    this.Parent.oContained.w_OnoPen = this.RadioValue()
    return .t.
  endfunc

  func oOnoPen_1_47.SetRadio()
    this.Parent.oContained.w_OnoPen=trim(this.Parent.oContained.w_OnoPen)
    this.value = ;
      iif(this.Parent.oContained.w_OnoPen=='E',1,;
      0)
  endfunc

  func oOnoPen_1_47.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oOnoStrag_1_48 as StdCheck with uid="JNXMKTIAUU",rtseq=36,rtrep=.f.,left=569, top=245, caption="Onorario stragiudiziale",;
    ToolTipText = "Se attivo, stampa gli onorari stragiudiziali",;
    HelpContextID = 19938381,;
    cFormVar="w_OnoStrag", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOnoStrag_1_48.RadioValue()
    return(iif(this.value =1,'T',;
    space(1)))
  endfunc
  func oOnoStrag_1_48.GetRadio()
    this.Parent.oContained.w_OnoStrag = this.RadioValue()
    return .t.
  endfunc

  func oOnoStrag_1_48.SetRadio()
    this.Parent.oContained.w_OnoStrag=trim(this.Parent.oContained.w_OnoStrag)
    this.value = ;
      iif(this.Parent.oContained.w_OnoStrag=='T',1,;
      0)
  endfunc

  func oOnoStrag_1_48.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDirCiv_1_49 as StdCheck with uid="KWCAIAQPMY",rtseq=37,rtrep=.f.,left=86, top=270, caption="Diritto civile",;
    ToolTipText = "Se attivo, stampa i diritti civili",;
    HelpContextID = 74475062,;
    cFormVar="w_DirCiv", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDirCiv_1_49.RadioValue()
    return(iif(this.value =1,'C',;
    space(1)))
  endfunc
  func oDirCiv_1_49.GetRadio()
    this.Parent.oContained.w_DirCiv = this.RadioValue()
    return .t.
  endfunc

  func oDirCiv_1_49.SetRadio()
    this.Parent.oContained.w_DirCiv=trim(this.Parent.oContained.w_DirCiv)
    this.value = ;
      iif(this.Parent.oContained.w_DirCiv=='C',1,;
      0)
  endfunc

  func oDirCiv_1_49.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDirStrag_1_50 as StdCheck with uid="YPYEWIJVVO",rtseq=38,rtrep=.f.,left=320, top=270, caption="Diritto stragiudiziale",;
    ToolTipText = "Se attivo, stampa i diritti stragiudiziali",;
    HelpContextID = 19949213,;
    cFormVar="w_DirStrag", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDirStrag_1_50.RadioValue()
    return(iif(this.value =1,'R',;
    space(1)))
  endfunc
  func oDirStrag_1_50.GetRadio()
    this.Parent.oContained.w_DirStrag = this.RadioValue()
    return .t.
  endfunc

  func oDirStrag_1_50.SetRadio()
    this.Parent.oContained.w_DirStrag=trim(this.Parent.oContained.w_DirStrag)
    this.value = ;
      iif(this.Parent.oContained.w_DirStrag=='R',1,;
      0)
  endfunc

  func oDirStrag_1_50.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oPreGen_1_51 as StdCheck with uid="TCWVESISVS",rtseq=39,rtrep=.f.,left=569, top=270, caption="Prestazione generica",;
    ToolTipText = "Se attivo, stampa le tariffe di prestazione generica",;
    HelpContextID = 204709878,;
    cFormVar="w_PreGen", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPreGen_1_51.RadioValue()
    return(iif(this.value =1,'G',;
    space(1)))
  endfunc
  func oPreGen_1_51.GetRadio()
    this.Parent.oContained.w_PreGen = this.RadioValue()
    return .t.
  endfunc

  func oPreGen_1_51.SetRadio()
    this.Parent.oContained.w_PreGen=trim(this.Parent.oContained.w_PreGen)
    this.value = ;
      iif(this.Parent.oContained.w_PreGen=='G',1,;
      0)
  endfunc

  func oPreGen_1_51.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oPreTempo_1_52 as StdCheck with uid="ZQYMXNWOEB",rtseq=40,rtrep=.f.,left=86, top=295, caption="Prestazione a tempo",;
    ToolTipText = "Se attivo, stampa le tariffe di prestazione a tempo",;
    HelpContextID = 188784741,;
    cFormVar="w_PreTempo", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPreTempo_1_52.RadioValue()
    return(iif(this.value =1,'P',;
    space(1)))
  endfunc
  func oPreTempo_1_52.GetRadio()
    this.Parent.oContained.w_PreTempo = this.RadioValue()
    return .t.
  endfunc

  func oPreTempo_1_52.SetRadio()
    this.Parent.oContained.w_PreTempo=trim(this.Parent.oContained.w_PreTempo)
    this.value = ;
      iif(this.Parent.oContained.w_PreTempo=='P',1,;
      0)
  endfunc

  func oPreTempo_1_52.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oSpese_1_53 as StdCheck with uid="GJLHVDCIUE",rtseq=41,rtrep=.f.,left=320, top=295, caption="Spesa",;
    ToolTipText = "Se attivo, stampa le spese",;
    HelpContextID = 27288026,;
    cFormVar="w_Spese", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSpese_1_53.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSpese_1_53.GetRadio()
    this.Parent.oContained.w_Spese = this.RadioValue()
    return .t.
  endfunc

  func oSpese_1_53.SetRadio()
    this.Parent.oContained.w_Spese=trim(this.Parent.oContained.w_Spese)
    this.value = ;
      iif(this.Parent.oContained.w_Spese=='S',1,;
      0)
  endfunc

  func oSpese_1_53.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oAnticip_1_54 as StdCheck with uid="IBXMGDLBBY",rtseq=42,rtrep=.f.,left=569, top=295, caption="Anticipazione",;
    ToolTipText = "Se attivo, stampa le anticipazioni",;
    HelpContextID = 121015046,;
    cFormVar="w_Anticip", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAnticip_1_54.RadioValue()
    return(iif(this.value =1,'A',;
    space(1)))
  endfunc
  func oAnticip_1_54.GetRadio()
    this.Parent.oContained.w_Anticip = this.RadioValue()
    return .t.
  endfunc

  func oAnticip_1_54.SetRadio()
    this.Parent.oContained.w_Anticip=trim(this.Parent.oContained.w_Anticip)
    this.value = ;
      iif(this.Parent.oContained.w_Anticip=='A',1,;
      0)
  endfunc

  func oAnticip_1_54.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oIMPZERO_1_55 as StdCheck with uid="ZDNIDPRXLA",rtseq=43,rtrep=.f.,left=86, top=332, caption="Solo prestazioni senza importo",;
    ToolTipText = "Se attivo, stampa solo le prestazioni senza importo",;
    HelpContextID = 29021562,;
    cFormVar="w_IMPZERO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMPZERO_1_55.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMPZERO_1_55.GetRadio()
    this.Parent.oContained.w_IMPZERO = this.RadioValue()
    return .t.
  endfunc

  func oIMPZERO_1_55.SetRadio()
    this.Parent.oContained.w_IMPZERO=trim(this.Parent.oContained.w_IMPZERO)
    this.value = ;
      iif(this.Parent.oContained.w_IMPZERO=='S',1,;
      0)
  endfunc

  add object oNORESP_1_56 as StdCheck with uid="BZDBPIHHSV",rtseq=44,rtrep=.f.,left=320, top=332, caption="Solo prestazioni senza responsabile",;
    ToolTipText = "Se attivo, stampa solo le prestazioni senza responsabile",;
    HelpContextID = 49263402,;
    cFormVar="w_NORESP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNORESP_1_56.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNORESP_1_56.GetRadio()
    this.Parent.oContained.w_NORESP = this.RadioValue()
    return .t.
  endfunc

  func oNORESP_1_56.SetRadio()
    this.Parent.oContained.w_NORESP=trim(this.Parent.oContained.w_NORESP)
    this.value = ;
      iif(this.Parent.oContained.w_NORESP=='S',1,;
      0)
  endfunc

  func oNORESP_1_56.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc

  add object oNOPRAT_1_57 as StdCheck with uid="XXBHZHTVFO",rtseq=45,rtrep=.f.,left=569, top=332, caption="Solo prestazioni senza pratica",;
    ToolTipText = "Se attivo, stampa solo le prestazioni senza pratica",;
    HelpContextID = 185130,;
    cFormVar="w_NOPRAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOPRAT_1_57.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOPRAT_1_57.GetRadio()
    this.Parent.oContained.w_NOPRAT = this.RadioValue()
    return .t.
  endfunc

  func oNOPRAT_1_57.SetRadio()
    this.Parent.oContained.w_NOPRAT=trim(this.Parent.oContained.w_NOPRAT)
    this.value = ;
      iif(this.Parent.oContained.w_NOPRAT=='S',1,;
      0)
  endfunc

  func oNOPRAT_1_57.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oDatiPratica_1_58 as StdCheck with uid="MTQOGAYNLY",rtseq=46,rtrep=.f.,left=86, top=355, caption="Dati pratica",;
    ToolTipText = "Se attivo, stampa i dati della pratica",;
    HelpContextID = 252508218,;
    cFormVar="w_DatiPratica", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDatiPratica_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDatiPratica_1_58.GetRadio()
    this.Parent.oContained.w_DatiPratica = this.RadioValue()
    return .t.
  endfunc

  func oDatiPratica_1_58.SetRadio()
    this.Parent.oContained.w_DatiPratica=trim(this.Parent.oContained.w_DatiPratica)
    this.value = ;
      iif(this.Parent.oContained.w_DatiPratica=='S',1,;
      0)
  endfunc

  func oDatiPratica_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SoloTot='N')
    endwith
   endif
  endfunc

  func oDatiPratica_1_58.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oDENOM_PART_1_59 as StdField with uid="GIFYDQCKGX",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 71672937,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=150, Top=158, InputMask=replicate('X',100)

  add object oAnnotazioni_1_60 as StdCheck with uid="BOCSCACCBJ",rtseq=48,rtrep=.f.,left=320, top=355, caption="Descrizione aggiuntiva",;
    ToolTipText = "Se attivo, stampa la descrizione aggiuntiva",;
    HelpContextID = 262983585,;
    cFormVar="w_Annotazioni", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAnnotazioni_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAnnotazioni_1_60.GetRadio()
    this.Parent.oContained.w_Annotazioni = this.RadioValue()
    return .t.
  endfunc

  func oAnnotazioni_1_60.SetRadio()
    this.Parent.oContained.w_Annotazioni=trim(this.Parent.oContained.w_Annotazioni)
    this.value = ;
      iif(this.Parent.oContained.w_Annotazioni=='S',1,;
      0)
  endfunc

  func oAnnotazioni_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SoloTot='N')
    endwith
   endif
  endfunc

  add object oSoloTot_1_61 as StdCheck with uid="QXCCWDFNKL",rtseq=49,rtrep=.f.,left=569, top=355, caption="Solo totali",;
    ToolTipText = "Se attivo, stampa solo i totalizzatori finali",;
    HelpContextID = 206310694,;
    cFormVar="w_SoloTot", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSoloTot_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSoloTot_1_61.GetRadio()
    this.Parent.oContained.w_SoloTot = this.RadioValue()
    return .t.
  endfunc

  func oSoloTot_1_61.SetRadio()
    this.Parent.oContained.w_SoloTot=trim(this.Parent.oContained.w_SoloTot)
    this.value = ;
      iif(this.Parent.oContained.w_SoloTot=='S',1,;
      0)
  endfunc

  func oSoloTot_1_61.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oCONTROP_1_62 as StdCheck with uid="SUMDWJRFWR",rtseq=50,rtrep=.f.,left=86, top=378, caption="Contropartite contabili",;
    ToolTipText = "Se attivo, stampa le contropartite contabili relative alle spese ed anticipazioni",;
    HelpContextID = 66122714,;
    cFormVar="w_CONTROP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONTROP_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCONTROP_1_62.GetRadio()
    this.Parent.oContained.w_CONTROP = this.RadioValue()
    return .t.
  endfunc

  func oCONTROP_1_62.SetRadio()
    this.Parent.oContained.w_CONTROP=trim(this.Parent.oContained.w_CONTROP)
    this.value = ;
      iif(this.Parent.oContained.w_CONTROP=='S',1,;
      0)
  endfunc

  func oCONTROP_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
   endif
  endfunc

  func oCONTROP_1_62.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oEvasio_1_63 as StdRadio with uid="IQAFOKPJWM",rtseq=51,rtrep=.f.,left=86, top=398, width=400,height=23;
    , ToolTipText = "Controllo tracciabilit� della prestazione";
    , cFormVar="w_Evasio", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oEvasio_1_63.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Da importare nei documenti"
      this.Buttons(1).HelpContextID = 228549446
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Da importare nei documenti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Inserite in un documento"
      this.Buttons(2).HelpContextID = 228549446
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Inserite in un documento","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 228549446
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Controllo tracciabilit� della prestazione")
      StdRadio::init()
    endproc

  func oEvasio_1_63.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oEvasio_1_63.GetRadio()
    this.Parent.oContained.w_Evasio = this.RadioValue()
    return .t.
  endfunc

  func oEvasio_1_63.SetRadio()
    this.Parent.oContained.w_Evasio=trim(this.Parent.oContained.w_Evasio)
    this.value = ;
      iif(this.Parent.oContained.w_Evasio=='N',1,;
      iif(this.Parent.oContained.w_Evasio=='S',2,;
      iif(this.Parent.oContained.w_Evasio=='T',3,;
      0)))
  endfunc

  func oEvasio_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STATO='P')
    endwith
   endif
  endfunc

  func oEvasio_1_63.mHide()
    with this.Parent.oContained
      return (!.w_STATO='P' OR !IsAlt())
    endwith
  endfunc

  add object oEscFac_1_64 as StdCheck with uid="QEBXDIDZIB",rtseq=52,rtrep=.f.,left=320, top=378, caption="Escl. fattura d'acconto",;
    ToolTipText = "Se attivo esclude le prestazioni gi� inserite  nelle fatture di acconto dell'elenco delle prestazioni da fatturare",;
    HelpContextID = 15892550,;
    cFormVar="w_EscFac", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEscFac_1_64.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oEscFac_1_64.GetRadio()
    this.Parent.oContained.w_EscFac = this.RadioValue()
    return .t.
  endfunc

  func oEscFac_1_64.SetRadio()
    this.Parent.oContained.w_EscFac=trim(this.Parent.oContained.w_EscFac)
    this.value = ;
      iif(this.Parent.oContained.w_EscFac=='S',1,;
      0)
  endfunc

  func oEscFac_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ( .w_InDocum $ 'P-D-N')
    endwith
   endif
  endfunc

  func oEscFac_1_64.mHide()
    with this.Parent.oContained
      return (!IsAlt() )
    endwith
  endfunc


  add object oFiltTipRig_1_65 as StdCombo with uid="SPYQCZKHCX",value=3,rtseq=53,rtrep=.f.,left=569,top=381,width=179,height=21;
    , ToolTipText = "Tipologia riga per la generazione del documento";
    , HelpContextID = 106001464;
    , cFormVar="w_FiltTipRig",RowSource=""+"Non fatturabile,"+"Fatturabile,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFiltTipRig_1_65.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,' ',;
    space(10)))))
  endfunc
  func oFiltTipRig_1_65.GetRadio()
    this.Parent.oContained.w_FiltTipRig = this.RadioValue()
    return .t.
  endfunc

  func oFiltTipRig_1_65.SetRadio()
    this.Parent.oContained.w_FiltTipRig=trim(this.Parent.oContained.w_FiltTipRig)
    this.value = ;
      iif(this.Parent.oContained.w_FiltTipRig=='N',1,;
      iif(this.Parent.oContained.w_FiltTipRig=='D',2,;
      iif(this.Parent.oContained.w_FiltTipRig=='',3,;
      0)))
  endfunc

  func oFiltTipRig_1_65.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oFiltTipRi2_1_66 as StdCombo with uid="BYBIPCOPWF",value=3,rtseq=54,rtrep=.f.,left=569,top=406,width=179,height=21;
    , ToolTipText = "Tipologia riga per la generazione della nota spese";
    , HelpContextID = 105987896;
    , cFormVar="w_FiltTipRi2",RowSource=""+"Senza nota spese,"+"Con nota spese,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFiltTipRi2_1_66.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,' ',;
    space(10)))))
  endfunc
  func oFiltTipRi2_1_66.GetRadio()
    this.Parent.oContained.w_FiltTipRi2 = this.RadioValue()
    return .t.
  endfunc

  func oFiltTipRi2_1_66.SetRadio()
    this.Parent.oContained.w_FiltTipRi2=trim(this.Parent.oContained.w_FiltTipRi2)
    this.value = ;
      iif(this.Parent.oContained.w_FiltTipRi2=='N',1,;
      iif(this.Parent.oContained.w_FiltTipRi2=='D',2,;
      iif(this.Parent.oContained.w_FiltTipRi2=='',3,;
      0)))
  endfunc

  func oFiltTipRi2_1_66.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oObj_1_67 as cp_outputCombo with uid="ZLERXIVPWT",left=245, top=433, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 36823014


  add object oBtn_1_68 as StdButton with uid="MBXAIGGBZE",left=676, top=440, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 614950;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_68.Click()
      with this.Parent.oContained
        GSAG_BSP(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_68.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_69 as StdButton with uid="ISDICPRYHO",left=728, top=440, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 133857210;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_69.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESUTE_1_70 as StdField with uid="FIHXIYIUZI",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 231714250,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=540, Top=158, InputMask=replicate('X',20)

  add object oDESNOM_1_77 as StdField with uid="QOGFAOHDZJ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 103198154,;
   bGlobalFont=.t.,;
    Height=21, Width=553, Left=224, Top=133, InputMask=replicate('X',40)

  func oDESNOM_1_77.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODPRA) AND ISALT())
    endwith
  endfunc


  add object oBtn_1_85 as StdButton with uid="JDLQWFQTWR",left=170, top=8, width=48,height=45,;
    CpPicture="bmp\date-time.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per impostare la data odierna";
    , HelpContextID = 133843738;
    , caption='\<Oggi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_85.Click()
      this.parent.oContained.NotifyEvent("ImpostaDataOdierna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCCDESPIA_1_87 as StdField with uid="KDRZJDGARK",rtseq=61,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219111527,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=489, Top=83, InputMask=replicate('X',40)

  func oCCDESPIA_1_87.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S')
    endwith
  endfunc

  add object oNORESP_1_91 as StdCheck with uid="FQGHTREUTO",rtseq=62,rtrep=.f.,left=320, top=332, caption="Solo prestazioni senza partecipante",;
    ToolTipText = "Se attivo, stampa solo le prestazioni senza partecipante",;
    HelpContextID = 49263402,;
    cFormVar="w_NORESP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNORESP_1_91.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNORESP_1_91.GetRadio()
    this.Parent.oContained.w_NORESP = this.RadioValue()
    return .t.
  endfunc

  func oNORESP_1_91.SetRadio()
    this.Parent.oContained.w_NORESP=trim(this.Parent.oContained.w_NORESP)
    this.value = ;
      iif(this.Parent.oContained.w_NORESP=='S',1,;
      0)
  endfunc

  func oNORESP_1_91.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc


  add object oObj_1_93 as cp_setobjprop with uid="VBHIYEPEDD",left=521, top=497, width=182,height=22,;
    caption='ToolTip di w_CODNOM',;
   bGlobalFont=.t.,;
    cObj="w_CODNOM",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 263704762


  add object oObj_1_94 as cp_setobjprop with uid="ABADDSFJRN",left=522, top=523, width=182,height=22,;
    caption='ToolTip di w_CODRES',;
   bGlobalFont=.t.,;
    cObj="w_CODRES",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 263726842


  add object oObj_1_97 as cp_runprogram with uid="TVWZBSDPNV",left=9, top=587, width=296,height=22,;
    caption='GSAG_BSP(POSTIN)',;
   bGlobalFont=.t.,;
    prg="GSAG_BSP('POSTIN')",;
    cEvent = "w_CODSER Changed",;
    nPag=1;
    , HelpContextID = 241795935

  add object oTIT_PRA_1_106 as StdField with uid="PBOANESIVU",rtseq=66,rtrep=.f.,;
    cFormVar = "w_TIT_PRA", cQueryName = "TIT_PRA",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice soggetto interno della pratica con ruolo di titolare per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto interno selezionato)",;
    HelpContextID = 17144010,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=86, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_DPTIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_TIT_PRA"

  func oTIT_PRA_1_106.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  func oTIT_PRA_1_106.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_106('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIT_PRA_1_106.ecpDrop(oSource)
    this.Parent.oContained.link_1_106('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIT_PRA_1_106.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_DPTIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_DPTIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oTIT_PRA_1_106'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oRES_PRA_1_107 as StdField with uid="MLEFOAIDNT",rtseq=67,rtrep=.f.,;
    cFormVar = "w_RES_PRA", cQueryName = "RES_PRA",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice soggetto interno della pratica con ruolo di responsabile per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto interno selezionato)",;
    HelpContextID = 17149162,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=477, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_DPTIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_RES_PRA"

  func oRES_PRA_1_107.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  func oRES_PRA_1_107.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_107('Part',this)
    endwith
    return bRes
  endfunc

  proc oRES_PRA_1_107.ecpDrop(oSource)
    this.Parent.oContained.link_1_107('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRES_PRA_1_107.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_DPTIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_DPTIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oRES_PRA_1_107'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oDENOM_TIT_1_110 as StdField with uid="NDNANXNDZM",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DENOM_TIT", cQueryName = "DENOM_TIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 196741055,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=151, Top=108, InputMask=replicate('X',100)

  func oDENOM_TIT_1_110.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  add object oDENOM_RES_1_111 as StdField with uid="NTERGERVTI",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DENOM_RES", cQueryName = "DENOM_RES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 196741035,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=540, Top=108, InputMask=replicate('X',100)

  func oDENOM_RES_1_111.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc


  add object oBtn_1_118 as StdButton with uid="CHMZTCBNGH",left=86, top=440, width=48,height=45,;
    CpPicture="BMP\COSTI.ICO", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa Analisi compensi e costi";
    , HelpContextID = 22971354;
    , Caption='C\<osti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_118.Click()
      do GSAG_SAC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_118.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT())
     endwith
    endif
  endfunc

  add object oStr_1_14 as StdString with uid="HVQDALZAWB",Visible=.t., Left=147, Top=433,;
    Alignment=1, Width=93, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="JQRNPBGBON",Visible=.t., Left=7, Top=8,;
    Alignment=1, Width=73, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="IZQIMNOCEG",Visible=.t., Left=7, Top=33,;
    Alignment=1, Width=73, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="IXUXLPHTKH",Visible=.t., Left=498, Top=8,;
    Alignment=1, Width=79, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="PUTEQZUBKC",Visible=.t., Left=7, Top=58,;
    Alignment=1, Width=73, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="CHDCKYFLMH",Visible=.t., Left=7, Top=158,;
    Alignment=1, Width=73, Height=18,;
    Caption="Resp.:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_71 as StdString with uid="KDNJNVZLRP",Visible=.t., Left=417, Top=158,;
    Alignment=1, Width=56, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="HEERJRJVVJ",Visible=.t., Left=7, Top=183,;
    Alignment=1, Width=73, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="YEIGMFBHWV",Visible=.t., Left=7, Top=135,;
    Alignment=1, Width=73, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_78.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODPRA) OR NOT ISALT())
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="EXFPHNQNMI",Visible=.t., Left=377, Top=183,;
    Alignment=1, Width=96, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="EBUADEBZKR",Visible=.t., Left=312, Top=83,;
    Alignment=1, Width=42, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S')
    endwith
  endfunc

  add object oStr_1_89 as StdString with uid="VGACWRSQQW",Visible=.t., Left=7, Top=58,;
    Alignment=1, Width=73, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_90 as StdString with uid="HFOIASSOUM",Visible=.t., Left=7, Top=158,;
    Alignment=1, Width=73, Height=18,;
    Caption="Part.:"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="EMOTHKNPJM",Visible=.t., Left=7, Top=133,;
    Alignment=1, Width=73, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_96 as StdString with uid="AUEJGKPLZP",Visible=.t., Left=7, Top=183,;
    Alignment=1, Width=73, Height=18,;
    Caption="Prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_96.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_98 as StdString with uid="SXGITJFDVT",Visible=.t., Left=7, Top=208,;
    Alignment=1, Width=73, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_98.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_99 as StdString with uid="RHEMLGJXYR",Visible=.t., Left=489, Top=380,;
    Alignment=1, Width=76, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_99.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oStr_1_101 as StdString with uid="KSBGPQZUEI",Visible=.t., Left=496, Top=33,;
    Alignment=1, Width=81, Height=18,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="WMBOBSIFGU",Visible=.t., Left=224, Top=33,;
    Alignment=1, Width=100, Height=18,;
    Caption="Pratiche sospese:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_104 as StdString with uid="ARJZGYKPMM",Visible=.t., Left=223, Top=8,;
    Alignment=1, Width=101, Height=18,;
    Caption="Pratiche:"  ;
  , bGlobalFont=.t.

  func oStr_1_104.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc

  add object oStr_1_105 as StdString with uid="GWEJVOQFIQ",Visible=.t., Left=474, Top=33,;
    Alignment=1, Width=103, Height=18,;
    Caption="In proforma/fattura:"  ;
  , bGlobalFont=.t.

  func oStr_1_105.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_STATO<>'P')
    endwith
  endfunc

  add object oStr_1_108 as StdString with uid="CUXHESDYFY",Visible=.t., Left=7, Top=108,;
    Alignment=1, Width=73, Height=18,;
    Caption="Titol. pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_108.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  add object oStr_1_109 as StdString with uid="FZZKNNAQGZ",Visible=.t., Left=389, Top=108,;
    Alignment=1, Width=84, Height=18,;
    Caption="Resp. pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_109.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  add object oStr_1_117 as StdString with uid="UPBDNXLWIC",Visible=.t., Left=489, Top=405,;
    Alignment=1, Width=76, Height=18,;
    Caption="Nota spese:"  ;
  , bGlobalFont=.t.

  func oStr_1_117.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oStr_1_120 as StdString with uid="BVBIGQKQSQ",Visible=.t., Left=253, Top=183,;
    Alignment=1, Width=64, Height=18,;
    Caption="ID prestaz.:"  ;
  , bGlobalFont=.t.

  func oStr_1_120.mHide()
    with this.Parent.oContained
      return (.w_PAGENPRE<>'S')
    endwith
  endfunc

  add object oStr_1_123 as StdString with uid="LIJLPIXOSP",Visible=.t., Left=284, Top=33,;
    Alignment=1, Width=40, Height=18,;
    Caption="Num.:"  ;
  , bGlobalFont=.t.

  func oStr_1_123.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_124 as StdString with uid="JMLLMGFFZD",Visible=.t., Left=443, Top=35,;
    Alignment=1, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_124.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_125 as StdString with uid="JTHCTETUHG",Visible=.t., Left=7, Top=83,;
    Alignment=1, Width=73, Height=18,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  func oStr_1_125.mHide()
    with this.Parent.oContained
      return (!IsAlt() )
    endwith
  endfunc

  add object oBox_1_73 as StdBox with uid="GUAITALLMO",left=75, top=236, width=695,height=89
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_spr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
