* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bcc                                                        *
*              Controllo completamento attivit�                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-19                                                      *
* Last revis.: 2011-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bcc",oParentObject,m.pEXEC)
return(i_retval)

define class tgsag_bcc as StdBatch
  * --- Local variables
  pEXEC = space(1)
  CAUATT = space(20)
  NUM_PRA = space(15)
  AT_SERIAL = space(15)
  ContaPrest = 0
  w_OKITER = .f.
  w_KEYATT = space(15)
  * --- WorkFile variables
  OFFDATTI_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                         Controllo su completamento attivit�
    * --- Da GSAG_KRV
    *     S Controllo su check Completa attivit� selezionata
    *     I  Verifica congruita' Iter
    if this.pEXEC="S"
      * --- Se in maschera � attivo il check Completa l'attivit� selezionata
      if this.oParentObject.w_FL_ATTCOMPL = "S"
        this.CAUATT = oParentObject.oParentObject.w_ATCAUATT
        this.NUM_PRA = oParentObject.oParentObject.COD_PRA
        this.AT_SERIAL = oParentObject.oParentObject.w_ATSERIAL
        * --- Controllo se causale attivit� � soggetta a prestazione
        if LookTab("CAUMATTI", "CAFLNSAP", "CACODICE", this.CAUATT) = "S"
          ah_ErrorMsg("Operazione non consentita: il tipo dell'attivit� non � soggetta a prestazione.")
          this.oParentObject.w_FL_ATTCOMPL = "N"
          i_retcode = 'stop'
          return
        endif
        * --- Controllo pratica
        if EMPTY(this.NUM_PRA) OR ISNULL(this.NUM_PRA)
          ah_ErrorMsg("Impossibile completare l'attivit�: pratica mancante.")
          this.oParentObject.w_FL_ATTCOMPL = "N"
          i_retcode = 'stop'
          return
        endif
        * --- Controllo presenza prestazioni e relativo responsabile
        * --- Select from OFFDATTI
        i_nConn=i_TableProp[this.OFFDATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DACODRES  from "+i_cTable+" OFFDATTI ";
              +" where DASERIAL="+cp_ToStrODBC(this.AT_SERIAL)+"";
               ,"_Curs_OFFDATTI")
        else
          select DACODRES from (i_cTable);
           where DASERIAL=this.AT_SERIAL;
            into cursor _Curs_OFFDATTI
        endif
        if used('_Curs_OFFDATTI')
          select _Curs_OFFDATTI
          locate for 1=1
          do while not(eof())
          if EMPTY(_Curs_OFFDATTI.DACODRES) OR ISNULL(_Curs_OFFDATTI.DACODRES)
            ah_ErrorMsg("Impossibile completare l'attivit�: tutte le prestazioni devono avere responsabile.")
            this.oParentObject.w_FL_ATTCOMPL = "N"
            i_retcode = 'stop'
            return
          endif
            select _Curs_OFFDATTI
            continue
          enddo
          use
        endif
        * --- Eliminazione prestazioni ad importo zero e di tipo  'A valore'
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.ContaPrest = 0
        * --- Select from OFFDATTI
        i_nConn=i_TableProp[this.OFFDATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DACODRES  from "+i_cTable+" OFFDATTI ";
              +" where DASERIAL="+cp_ToStrODBC(this.AT_SERIAL)+"";
               ,"_Curs_OFFDATTI")
        else
          select DACODRES from (i_cTable);
           where DASERIAL=this.AT_SERIAL;
            into cursor _Curs_OFFDATTI
        endif
        if used('_Curs_OFFDATTI')
          select _Curs_OFFDATTI
          locate for 1=1
          do while not(eof())
          this.ContaPrest = this.ContaPrest + 1
            select _Curs_OFFDATTI
            continue
          enddo
          use
        endif
        if this.ContaPrest = 0
          ah_ErrorMsg("Impossibile completare l'attivit�: deve essere presente almeno una prestazione.")
          this.oParentObject.w_FL_ATTCOMPL = "N"
          i_retcode = 'stop'
          return
        endif
      endif
    else
      this.w_OKITER = .t.
      * --- Select from gsag_bcc
      do vq_exec with 'gsag_bcc',this,'_Curs_gsag_bcc','',.f.,.t.
      if used('_Curs_gsag_bcc')
        select _Curs_gsag_bcc
        locate for 1=1
        do while not(eof())
        this.w_OKITER = .f.
          select _Curs_gsag_bcc
          continue
        enddo
        use
      endif
      if ! this.w_OKITER
        this.oParentObject.w_CAITER = Space(10)
         
 Ah_errormsg("Attenzione, raggruppamento con almeno un tipo attivit� di categoria udienza!",48)
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se prestazioni ad importo zero e di tipo  'A valore' imposto No documento sulla riga prestazioni
    this.w_KEYATT = this.AT_SERIAL
    * --- Write into OFFDATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DASERIAL,CPROWNUM"
      do vq_exec with '..\agen\exe\query\gsag1bel',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
              +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DATIPRIG ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG');
      +",DATIPRI2 ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2');
          +i_ccchkf;
          +" from "+i_cTable+" OFFDATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
              +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI, "+i_cQueryTable+" _t2 set ";
      +"OFFDATTI.DATIPRIG ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG');
      +",OFFDATTI.DATIPRI2 ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2');
          +Iif(Empty(i_ccchkf),"",",OFFDATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="OFFDATTI.DASERIAL = t2.DASERIAL";
              +" and "+"OFFDATTI.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI set (";
          +"DATIPRIG,";
          +"DATIPRI2";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG')+",";
          +cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
              +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI set ";
      +"DATIPRIG ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG');
      +",DATIPRI2 ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DATIPRIG ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG');
      +",DATIPRI2 ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='OFFDATTI'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='ART_ICOL'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs_gsag_bcc')
      use in _Curs_gsag_bcc
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
