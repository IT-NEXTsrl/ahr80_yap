* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bev                                                        *
*              Caricamento eventi                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-22                                                      *
* Last revis.: 2012-06-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEventsCur,pAttEventCur,pErrorLog,pCloseCursor,pAdvLog,pNoObbl,pNoUnivoc,pNotSogg
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bev",oParentObject,m.pEventsCur,m.pAttEventCur,m.pErrorLog,m.pCloseCursor,m.pAdvLog,m.pNoObbl,m.pNoUnivoc,m.pNotSogg)
return(i_retval)

define class tgsfa_bev as StdBatch
  * --- Local variables
  pEventsCur = space(15)
  pAttEventCur = space(15)
  pErrorLog = .NULL.
  pCloseCursor = space(1)
  pAdvLog = .f.
  pNoObbl = .f.
  pNoUnivoc = .f.
  pNotSogg = .f.
  w_RETVAL = .f.
  w_EV__ANNO = space(4)
  w_EVSERIAL = space(10)
  w_CURTIPEVE = space(10)
  w_TEDRIVER = space(10)
  w_TECLADOC = space(15)
  w_DE__TIPO = space(1)
  w_EVFLGCHK = space(1)
  w_SERIAL = space(10)
  w_OLDSERIAL = space(10)
  w_OLDEVSERIAL = space(10)
  w_NUMFIELD = 0
  w_COUNT = 0
  w_SOGGETTO = 0
  w_NUM_CONT = 0
  w_AGGIORNA = .f.
  w_EVTIPEVE = space(10)
  w_EV_STATO = space(1)
  w_EVDIREVE = space(1)
  w_EVOGGETT = space(100)
  w_EV__NOTE = space(0)
  w_EVNOMINA = space(15)
  w_EVRIFPER = space(254)
  w_EVDATINI = ctot("")
  w_EVDATFIN = ctot("")
  w_EVOREEFF = 0
  w_EVMINEFF = 0
  w_EV_EMAIL = space(254)
  w_EVTELINT = space(254)
  w_EV_PHONE = space(18)
  w_EVNUMCEL = space(18)
  w_EVCODPAR = space(5)
  w_EVLOCALI = space(30)
  w_EVCODPRA = space(15)
  w_EVPERSON = space(5)
  w_EVGRUPPO = space(5)
  w_UTCC = 0
  w_UTDC = ctod("  /  /  ")
  w_PERCHK = .f.
  w_EVCODPRA = space(15)
  w_CNDESCAN = space(100)
  w_CODPRA = space(15)
  w_NOTESOG = space(0)
  w_FIRST = .f.
  w_FIRSTSOG = space(0)
  w_CODICE = space(15)
  w_INSERT = .f.
  w_RET = space(254)
  w_STRFILE = space(0)
  w_FILETMP = space(254)
  w_AR_TABLE = space(15)
  w_FILENAME = space(254)
  w_NUMINSEVENT = 0
  * --- WorkFile variables
  TIPEVENT_idx=0
  DRVEVENT_idx=0
  ANEVENTI_idx=0
  PRODINDI_idx=0
  DIPENDEN_idx=0
  CAN_TIER_idx=0
  NOM_CONT_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica eventi
    *     pEventsCur: Nome cursore contenente gli eventi da inserire
    *     pAttEventCur: Nome cursore contenente gli allegati dell'evento (email)
    *     pErrorLog: Oggetto error log
    *     pCloseCursor: 'X' Entrambi (default), 'E' pEventsCur, 'A' pAttEventCur
    *     pAdvLog: Log esteso, inserisce nel log anche il numero di eventi inseriti
    *     pNoObbl: Se passato a .t. non esegue verifica campi obbligatori
    *     pNoUnivoc: Se passato a .t. non esegue le verifiche di univocit�
    *     pNotSogg: non valorizza note soggetto
    *     
    *     Ritorna:
    *     False se ci sono errori
    *     
    * --- --
    this.pCloseCursor = EVL(this.pCloseCursor, "X")
    * --- Esito import
    this.w_RETVAL = .T.
    * --- Calcolato
    *     EVSERIAL
    *     EV__ANNO
    * --- Campi cursore pEventsCur
    *     EVTIPEVE - Obbligatorio
    *     EV_STATO - 'D' (default)
    *     EVDIREVE - Obbligatorio
    *     EVNOMINA - Se vuoto ricerca il nominativo associato a telefono/email
    *     EVRIFPER  - Se vuoto ricerca il riferimento nominativo associato a telefono/email/nominativo
    *     EVOGGETT - Opzionale
    *     EV__NOTE - Opzionale
    *     EV__FAX, EVTELINT, EV_EMAIL  - Obbligatorio in base alla tipologia evento
    *     EVDATINI Obbligatorio 
    *     EVDATFIN, EVOREEFF, EVMINEFF, Calcolo solo EVDATFIN sulla base di EVOREEFF, EVMINEFF
    *     EVCODPRA - Opzionale
    *     
    *     EVSERIAL - Chiave evento, legame con pAttEventCur
    * --- Indica che per l'evento precedente � stato trovato un nominativo
    if Used(this.pEventsCur)
      this.w_SOGGETTO = 0
      this.w_AGGIORNA = .F.
      this.w_OLDSERIAL = "@@@@@@@@@@"
      this.w_CURTIPEVE = "@@@@##@@@@"
      this.w_EVFLGCHK = "N"
      SELECT(this.pEventsCur)
      this.w_NUMFIELD = AFIELDS(aEFields, this.pEventsCur)
      SELECT(this.pEventsCur)
      LOCATE FOR 1=1
      do while Not(EOF())
        this.w_PERCHK = .F.
        this.w_COUNT = 1
        L_OldErr = ON("ERROR")
        L_NewErr = .F.
        ON ERROR L_NewErr = .T.
        do while this.w_COUNT <= this.w_NUMFIELD
          * --- Aggiorno cursore eventi con i nuovi valori calcolati
          Local L_MacroVar
          if !PEMSTATUS(This, "w_"+Alltrim(aEFields[this.w_COUNT, 1]), 5)
             
 L_MacroVar = "This.AddProperty('w_"+Alltrim(aEFields[this.w_COUNT, 1])+"', NVL(&aEFields[This.w_COUNT, 1], cp_NullValue(&aEFields[This.w_COUNT, 1])))" 
 &L_MacroVar
          else
             
 L_MacroVar = "This.w_"+Alltrim(aEFields[this.w_COUNT, 1])+"=NVL(&aEFields[This.w_COUNT, 1], cp_NullValue(&aEFields[This.w_COUNT, 1]))" 
 &L_MacroVar
          endif
          this.w_COUNT = this.w_COUNT + 1
        enddo
        ON ERROR &L_OldErr
        this.w_SERIAL = NVL(EVSERIAL, "")
        this.w_EV_STATO = EVL(NVL(EV_STATO, ""), "D")
        this.w_EV__ANNO = EVL(NVL(EV__ANNO, ""), CALCESER(i_DATSYS))
        this.w_EVCODPRA = NVL(EVCODPRA, "")
        this.w_EVCODPAR = NVL(EVCODPAR, "")
        this.w_EVLOCALI = NVL(EVLOCALI, "")
        * --- Normalizzo i numeri di telefono
        this.w_EV_PHONE = ConvNum(NVL(EV_PHONE,""), "+", 1)
        this.w_EVNUMCEL = ConvNum(NVL(EVNUMCEL,""), "+", 1)
        this.w_EVTELINT = ConvNum(NVL(EVTELINT,""), "+", 1)
        * --- Controllo pratica, se nn esiste la sbianco
        if !Empty(this.w_EVCODPRA)
          this.w_CODPRA = this.w_EVCODPRA
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNDESCAN"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.w_EVCODPRA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNDESCAN;
              from (i_cTable) where;
                  CNCODCAN = this.w_EVCODPRA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CNDESCAN = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
            use
            if i_Rows=0
              this.w_EVCODPRA = ""
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Controllo campi obbligatori
        if !Empty(this.w_EVTIPEVE) And !Empty(this.w_EVDIREVE) And !Empty(this.w_EVDATINI)
          if this.w_CURTIPEVE<>this.w_EVTIPEVE
            if this.w_CURTIPEVE<>"@@@@##@@@@" And this.pAdvLog
              this.pErrorLog.AddMsgLog("Tipo evento: %1%0Numero eventi inseriti: %2", this.w_CURTIPEVE, this.w_NUMINSEVENT)     
            endif
            this.w_CURTIPEVE = this.w_EVTIPEVE
            * --- Read from TIPEVENT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIPEVENT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIPEVENT_idx,2],.t.,this.TIPEVENT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TEDRIVER,TECLADOC"+;
                " from "+i_cTable+" TIPEVENT where ";
                    +"TETIPEVE = "+cp_ToStrODBC(this.w_CURTIPEVE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TEDRIVER,TECLADOC;
                from (i_cTable) where;
                    TETIPEVE = this.w_CURTIPEVE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TEDRIVER = NVL(cp_ToDate(_read_.TEDRIVER),cp_NullValue(_read_.TEDRIVER))
              this.w_TECLADOC = NVL(cp_ToDate(_read_.TECLADOC),cp_NullValue(_read_.TECLADOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from DRVEVENT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DRVEVENT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DRVEVENT_idx,2],.t.,this.DRVEVENT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DE__TIPO"+;
                " from "+i_cTable+" DRVEVENT where ";
                    +"DEDRIVER = "+cp_ToStrODBC(this.w_TEDRIVER);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DE__TIPO;
                from (i_cTable) where;
                    DEDRIVER = this.w_TEDRIVER;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DE__TIPO = NVL(cp_ToDate(_read_.DE__TIPO),cp_NullValue(_read_.DE__TIPO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_NUMINSEVENT = 0
          endif
          if ! this.pNoObbl
            * --- Controllo campi obbligatori per tipo
            do case
              case this.w_DE__TIPO = "M"
                if Empty(this.w_EV_EMAIL)
                  this.pErrorLog.AddMsgLog("Evento: %1%0Campo obbligatorio mancante:%0Email", this.w_SERIAL)     
                  this.w_RETVAL = .F.
                  SELECT(this.pEventsCur)
                  CONTINUE
                  LOOP
                endif
                if Empty(this.w_EVPERSON)
                  this.pErrorLog.AddMsgLog("Evento: %1%0Campo obbligatorio mancante:%0Codice persona", this.w_SERIAL)     
                  this.w_RETVAL = .F.
                  SELECT(this.pEventsCur)
                  CONTINUE
                  LOOP
                endif
                if Empty(this.w_EVGRUPPO)
                  this.pErrorLog.AddMsgLog("Evento: %1%0Campo obbligatorio mancante:%0Codice gruppo", this.w_SERIAL)     
                  this.w_RETVAL = .F.
                  SELECT(this.pEventsCur)
                  CONTINUE
                  LOOP
                endif
              case this.w_DE__TIPO = "T"
                if Empty(this.w_EVTELINT)
                  this.pErrorLog.AddMsgLog("Evento: %1%0Campo obbligatorio mancante:%0Numero interno", this.w_SERIAL)     
                  this.w_RETVAL = .F.
                  SELECT(this.pEventsCur)
                  CONTINUE
                  LOOP
                else
                  * --- Determino codice persona o gruppo se vuoti
                  * --- Select from DIPENDEN
                  i_nConn=i_TableProp[this.DIPENDEN_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select DPCODICE, COUNT(*) As QUANTI  from "+i_cTable+" DIPENDEN ";
                        +" where DPTELEF1 = "+cp_ToStrODBC(this.w_EVTELINT)+" Or DPTELEF2 = "+cp_ToStrODBC(this.w_EVTELINT)+"";
                        +" group by DPCODICE";
                        +" order by DPCODICE";
                         ,"_Curs_DIPENDEN")
                  else
                    select DPCODICE, COUNT(*) As QUANTI from (i_cTable);
                     where DPTELEF1 = this.w_EVTELINT Or DPTELEF2 = this.w_EVTELINT;
                     group by DPCODICE;
                     order by DPCODICE;
                      into cursor _Curs_DIPENDEN
                  endif
                  if used('_Curs_DIPENDEN')
                    select _Curs_DIPENDEN
                    locate for 1=1
                    do while not(eof())
                    this.w_EVPERSON = EVL(this.w_EVPERSON, _Curs_DIPENDEN.DPCODICE)
                    this.w_PERCHK = _Curs_DIPENDEN.QUANTI>1
                      select _Curs_DIPENDEN
                      continue
                    enddo
                    use
                  endif
                endif
              case this.w_DE__TIPO = "F"
                * --- Nessun campo obbligatorio
              case this.w_DE__TIPO = "D"
                * --- Nessun campo obbligatorio
              case this.w_DE__TIPO = "A"
                * --- Nessun campo obbligatorio
            endcase
          endif
          this.w_EVPERSON = IIF(this.w_EVPERSON == this.w_EVGRUPPO And !Empty(this.w_EVGRUPPO), "", this.w_EVPERSON)
          * --- Ricerca soggetto
          if this.w_SERIAL <> this.w_OLDSERIAL
            this.w_NOTESOG = ""
          endif
          this.w_FIRSTSOG = ""
          this.w_FIRST = .T.
          this.w_NUM_CONT = 0
          if !this.pNotSogg
            if (Not Empty(this.w_EV_PHONE) or Not Empty(this.w_EV_EMAIL) or Not Empty(this.w_EVNOMINA))
              * --- Select from ..\AGEN\EXE\QUERY\GSFA_BEV
              do vq_exec with '..\AGEN\EXE\QUERY\GSFA_BEV',this,'_Curs__d__d__AGEN_EXE_QUERY_GSFA_BEV','',.f.,.t.
              if used('_Curs__d__d__AGEN_EXE_QUERY_GSFA_BEV')
                select _Curs__d__d__AGEN_EXE_QUERY_GSFA_BEV
                locate for 1=1
                do while not(eof())
                * --- Carico solo il primo soggetto, se ne trovo pi� di uno valorizzo w_EVFLGCHK
                this.w_NUM_CONT = this.w_NUM_CONT + 1
                if this.w_FIRST
                  this.w_EVNOMINA = IIF(Empty(this.w_EVNOMINA), NVL(CODICE, ""), this.w_EVNOMINA)
                  this.w_EVRIFPER = IIF(Empty(this.w_EVRIFPER), NVL(PERSONA,""), this.w_EVRIFPER)
                  this.w_EVCODPAR = NVL(NCCODCON, "")
                  this.w_EVLOCALI = NVL(NOLOCALI, "")
                  this.w_CODICE = iif(Empty(ALLTRIM(NVL(NODESCRI, "") )),ALLTRIM(NVL(CODICE, "") )+" ","")
                  this.w_FIRSTSOG = this.w_CODICE + ALLTRIM( NVL(NODESCRI, "") )+ IIF(EMPTY(NVL(PERSONA, " ")), "", " - " + ALLTRIM( NVL(PERSONA,"") ) )
                  if this.w_SERIAL <> this.w_OLDSERIAL
                    this.w_SOGGETTO = 1
                    this.w_AGGIORNA = .T.
                  else
                    this.w_SOGGETTO = this.w_SOGGETTO + 1
                    this.w_AGGIORNA = NVL(NOTIPNOM, "") = "C" Or !this.w_AGGIORNA
                  endif
                  this.w_FIRST = .F.
                endif
                * --- Concatener� alle note l'elenco dei soggetti papabili trovati
                this.w_NOTESOG = this.w_NOTESOG + ALLTRIM(NVL(CODICE, "") )+ " " + ALLTRIM( NVL(NODESCRI, "") )+ IIF(EMPTY(NVL(PERSONA, " ")), "", " - " + ALLTRIM( NVL(PERSONA,"") ) )+ CHR(13)+CHR(10)
                if not empty(this.w_EVNOMINA) and not empty(this.w_EVCODPAR)
                  * --- Read from NOM_CONT
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.NOM_CONT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL"+;
                      " from "+i_cTable+" NOM_CONT where ";
                          +"NCCODICE = "+cp_ToStrODBC(this.w_EVNOMINA);
                          +" and NCCODCON = "+cp_ToStrODBC(this.w_EVCODPAR);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL;
                      from (i_cTable) where;
                          NCCODICE = this.w_EVNOMINA;
                          and NCCODCON = this.w_EVCODPAR;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_EVRIFPER = NVL(cp_ToDate(_read_.NCPERSON),cp_NullValue(_read_.NCPERSON))
                    this.w_EV_PHONE = NVL(cp_ToDate(_read_.NCTELEFO),cp_NullValue(_read_.NCTELEFO))
                    this.w_EVNUMCEL = NVL(cp_ToDate(_read_.NCNUMCEL),cp_NullValue(_read_.NCNUMCEL))
                    this.w_EV_EMAIL = NVL(cp_ToDate(_read_.NC_EMAIL),cp_NullValue(_read_.NC_EMAIL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                else
                  * --- Read from OFF_NOMI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL"+;
                      " from "+i_cTable+" OFF_NOMI where ";
                          +"NOCODICE = "+cp_ToStrODBC(this.w_EVNOMINA);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL;
                      from (i_cTable) where;
                          NOCODICE = this.w_EVNOMINA;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_EVLOCALI = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
                    this.w_EV_PHONE = NVL(cp_ToDate(_read_.NOTELEFO),cp_NullValue(_read_.NOTELEFO))
                    this.w_EVNUMCEL = NVL(cp_ToDate(_read_.NONUMCEL),cp_NullValue(_read_.NONUMCEL))
                    this.w_EV_EMAIL = NVL(cp_ToDate(_read_.NO_EMAIL),cp_NullValue(_read_.NO_EMAIL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                  select _Curs__d__d__AGEN_EXE_QUERY_GSFA_BEV
                  continue
                enddo
                use
              endif
            else
              this.w_FIRSTSOG = Ah_msgformat("Sconosciuto")
              this.w_NOTESOG = this.w_NOTESOG +Ah_msgformat("Sconosciuto")+ CHR(13)+CHR(10)
            endif
          endif
          * --- Se � una telefonata e non ho oggetto valorizzato scrivo la direzione e nominativo o numero di telefono
          if this.w_DE__TIPO = "T" AND EMPTY(this.w_EVOGGETT)
            if this.w_EVDIREVE="E"
              this.w_EVOGGETT = LEFT( Ah_MsgFormat("Telefonata da %1", iif(empty(this.w_EVNOMINA), evl(this.w_EV_PHONE, ah_msgformat("sconosciuto")), this.w_FIRSTSOG)) ,100)
            else
              this.w_EVOGGETT = LEFT( Ah_MsgFormat("Telefonata a %1", iif(empty(this.w_EVNOMINA), evl(this.w_EV_PHONE, ah_msgformat("sconosciuto")), this.w_FIRSTSOG)) ,100)
            endif
          endif
          this.w_EVFLGCHK = IIF(this.w_NUM_CONT > 1, "S", "N")
          * --- Calcolo data fine se vuota
          if Empty(this.w_EVDATFIN)
            this.w_EVDATFIN = this.w_EVDATINI+3600*this.w_EVOREEFF+60*this.w_EVMINEFF
          endif
          if !this.pNotSogg
            this.w_EV__NOTE = this.w_EV__NOTE + IIF(!Empty(this.w_NOTESOG), CHR(13)+CHR(10) + Replicate("-", 100) + CHR(13)+CHR(10) + ah_msgformat("Soggetti associati:") + CHR(13)+CHR(10) + this.w_NOTESOG, "")
          endif
          if this.w_SERIAL <> this.w_OLDSERIAL
            * --- Nuovo Evento
            * --- begin transaction
            cp_BeginTrs()
            this.w_EVSERIAL = SPACE(10)
            i_Conn=i_TableProp[this.ANEVENTI_IDX, 3]
            cp_NextTableProg(this,i_nConn,"SEREVE","i_CODAZI,w_EV__ANNO,w_EVSERIAL", .T.)
            this.w_UTCC = i_CODUTE
            this.w_UTDC = SetInfoDate(g_CALUTD)
            SELECT(this.pEventsCur)
            Dimension ArrEvent(this.w_NUMFIELD, 2)
            this.w_COUNT = 1
            L_OldErr = ON("ERROR")
            L_NewErr = .F.
            ON ERROR L_NewErr = .T.
            do while this.w_COUNT <= this.w_NUMFIELD
              * --- Aggiorno cursore eventi con i nuovi valori calcolati
              L_MACRO = "REPLACE " + ALLTRIM(aEFields[this.w_COUNT, 1]) + " with this.w_"+ALLTRIM(aEFields[this.w_COUNT, 1])
              &L_MACRO
               
 ArrEvent[this.w_COUNT, 1] = aEFields[this.w_COUNT, 1] 
 ArrEvent[this.w_COUNT, 2] = &aEFields[this.w_COUNT, 1]
              this.w_COUNT = this.w_COUNT + 1
            enddo
            ON ERROR &L_OldErr
            release L_NewErr, L_MACRO
            * --- In caso di telefonata controllo se � gi� stata inserita nel db
            this.w_INSERT = .F.
            if this.w_DE__TIPO = "T" and !this.pNoUnivoc
              * --- Read from ANEVENTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ANEVENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2],.t.,this.ANEVENTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "EVSERIAL"+;
                  " from "+i_cTable+" ANEVENTI where ";
                      +"EVDIREVE = "+cp_ToStrODBC(this.w_EVDIREVE);
                      +" and EV_PHONE = "+cp_ToStrODBC(this.w_EV_PHONE);
                      +" and EVTELINT = "+cp_ToStrODBC(this.w_EVTELINT);
                      +" and EVDATINI = "+cp_ToStrODBC(this.w_EVDATINI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  EVSERIAL;
                  from (i_cTable) where;
                      EVDIREVE = this.w_EVDIREVE;
                      and EV_PHONE = this.w_EV_PHONE;
                      and EVTELINT = this.w_EVTELINT;
                      and EVDATINI = this.w_EVDATINI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                w_TMPSERIAL = NVL(cp_ToDate(_read_.EVSERIAL),cp_NullValue(_read_.EVSERIAL))
                use
                if i_Rows=0
                  this.w_INSERT = .T.
                endif
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              this.w_INSERT = .T.
            endif
            if this.w_INSERT
              * --- Inserisco l'evento in tabella
              this.w_RET = InsertTable("ANEVENTI", @ArrEvent)
            endif
            * --- Rilascio gli array
            Release ArrEvent
            if !Empty(this.w_RET)
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              this.pErrorLog.AddMsgLog("Evento: %1%0%2", this.w_SERIAL, this.w_RET)     
              this.w_RETVAL = .F.
              SELECT(this.pEventsCur)
              CONTINUE
              LOOP
            endif
            * --- Salvo vecchi seriali
            this.w_OLDSERIAL = this.w_SERIAL
            this.w_OLDEVSERIAL = this.w_EVSERIAL
            * --- Inserimento allegati
            if Used(this.pAttEventCur)
              this.w_AR_TABLE = "ANEVENTI"
              private L_SerIndex
              SELECT(this.pAttEventCur)
              LOCATE FOR EVSERIAL = this.w_SERIAL
              do while Found()
                this.w_STRFILE = ""
                this.w_FILETMP = ADDBS(TEMPADHOC())+SYS(2015)+".TXT"
                this.w_FILENAME = NVL(EV__PATH, "")
                if Cp_fileexist(Alltrim(this.w_FILENAME))
                  this.w_STRFILE = this.w_STRFILE + Alltrim(this.w_FILENAME) + CHR(13)
                endif
                if !Empty(this.w_STRFILE)
                  STRTOFILE(this.w_STRFILE, this.w_FILETMP)
                  GSUT_BSO(this,"", this.w_FILETMP)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  DELETE FILE (this.w_FILETMP)
                endif
                SELECT(this.pAttEventCur)
                CONTINUE
              enddo
            endif
            this.w_NUMINSEVENT = this.w_NUMINSEVENT + 1
          else
            * --- Evento gi� inserito
            this.w_EVFLGCHK = IIF(this.w_SOGGETTO>1 Or this.w_PERCHK, "S", this.w_EVFLGCHK)
            if this.w_AGGIORNA
              * --- Write into ANEVENTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ANEVENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ANEVENTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"EVNOMINA ="+cp_NullLink(cp_ToStrODBC(this.w_EVNOMINA),'ANEVENTI','EVNOMINA');
                +",EVRIFPER ="+cp_NullLink(cp_ToStrODBC(this.w_EVRIFPER),'ANEVENTI','EVRIFPER');
                +",EV_EMAIL ="+cp_NullLink(cp_ToStrODBC(this.w_EV_EMAIL),'ANEVENTI','EV_EMAIL');
                +",EVFLGCHK ="+cp_NullLink(cp_ToStrODBC(this.w_EVFLGCHK),'ANEVENTI','EVFLGCHK');
                +",EV__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_EV__NOTE),'ANEVENTI','EV__NOTE');
                +",EVCODPAR ="+cp_NullLink(cp_ToStrODBC(this.w_EVCODPAR),'ANEVENTI','EVCODPAR');
                +",EVNUMCEL ="+cp_NullLink(cp_ToStrODBC(this.w_EVNUMCEL),'ANEVENTI','EVNUMCEL');
                +",EVLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_EVLOCALI),'ANEVENTI','EVLOCALI');
                +",EV_PHONE ="+cp_NullLink(cp_ToStrODBC(this.w_EV_PHONE),'ANEVENTI','EV_PHONE');
                    +i_ccchkf ;
                +" where ";
                    +"EV__ANNO = "+cp_ToStrODBC(this.w_EV__ANNO);
                    +" and EVSERIAL = "+cp_ToStrODBC(this.w_OLDEVSERIAL);
                       )
              else
                update (i_cTable) set;
                    EVNOMINA = this.w_EVNOMINA;
                    ,EVRIFPER = this.w_EVRIFPER;
                    ,EV_EMAIL = this.w_EV_EMAIL;
                    ,EVFLGCHK = this.w_EVFLGCHK;
                    ,EV__NOTE = this.w_EV__NOTE;
                    ,EVCODPAR = this.w_EVCODPAR;
                    ,EVNUMCEL = this.w_EVNUMCEL;
                    ,EVLOCALI = this.w_EVLOCALI;
                    ,EV_PHONE = this.w_EV_PHONE;
                    &i_ccchkf. ;
                 where;
                    EV__ANNO = this.w_EV__ANNO;
                    and EVSERIAL = this.w_OLDEVSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          * --- commit
          cp_EndTrs(.t.)
        else
          this.pErrorLog.AddMsgLog("Evento: %1%0Campo obbligatorio mancante:%0Tipo: %2%0Direzione: %3%0Data inizio: %4", this.w_SERIAL, this.w_EVTIPEVE, this.w_EVDIREVE, TTOC(this.w_EVDATINI))     
          this.w_RETVAL = .F.
        endif
        SELECT(this.pEventsCur)
        CONTINUE
      enddo
      Release aEFields
      if this.pAdvLog
        this.pErrorLog.AddMsgLog("Tipo evento: %1%0Numero eventi inseriti: %2", this.w_EVTIPEVE, this.w_NUMINSEVENT)     
      endif
    else
      this.pErrorLog.AddMsgLog("Nessun evento da processare")     
      this.w_RETVAL = .F.
    endif
    * --- Chiusura cursori
    if this.pCloseCursor="X" Or this.pCloseCursor = "E"
      USE IN SELECT(this.pEventsCur)
    endif
    if this.pCloseCursor="X" Or this.pCloseCursor = "A"
      USE IN SELECT(this.pAttEventCur)
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  proc Init(oParentObject,pEventsCur,pAttEventCur,pErrorLog,pCloseCursor,pAdvLog,pNoObbl,pNoUnivoc,pNotSogg)
    this.pEventsCur=pEventsCur
    this.pAttEventCur=pAttEventCur
    this.pErrorLog=pErrorLog
    this.pCloseCursor=pCloseCursor
    this.pAdvLog=pAdvLog
    this.pNoObbl=pNoObbl
    this.pNoUnivoc=pNoUnivoc
    this.pNotSogg=pNotSogg
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='TIPEVENT'
    this.cWorkTables[2]='DRVEVENT'
    this.cWorkTables[3]='ANEVENTI'
    this.cWorkTables[4]='PRODINDI'
    this.cWorkTables[5]='DIPENDEN'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='NOM_CONT'
    this.cWorkTables[8]='OFF_NOMI'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_DIPENDEN')
      use in _Curs_DIPENDEN
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSFA_BEV')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSFA_BEV
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEventsCur,pAttEventCur,pErrorLog,pCloseCursor,pAdvLog,pNoObbl,pNoUnivoc,pNotSogg"
endproc
