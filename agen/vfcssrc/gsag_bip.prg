* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bip                                                        *
*              Inserimento prestazioni multiple                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-26                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bip",oParentObject)
return(i_retval)

define class tgsag_bip as StdBatch
  * --- Local variables
  w_Mask = .NULL.
  w_Gestatt = .NULL.
  w_CursorZoom = space(1)
  CONTA_PREST = 0
  w_NUMRIG = 0
  w_CODSER = space(20)
  w_CODART = space(20)
  w_UNIMIS = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_FLSERG = space(1)
  w_TIPART = space(2)
  w_DESUNI = space(35)
  w_DESPREST = space(40)
  w_DESSUP = space(0)
  w_COSORA = 0
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_DENOM_RESP = space(40)
  w_VALLIS = space(3)
  w_FLAPON = space(1)
  w_ENTE = space(10)
  w_UFFICI = space(10)
  w_FLVALO = space(1)
  w_FLVALO1 = space(1)
  w_IMPORT = 0
  w_CALDIR = space(1)
  w_COECAL = 0
  w_PARASS = 0
  w_FLAMPA = space(1)
  w_NOMEPRA = space(100)
  w_FLTEMP = space(1)
  w_DURORE = 0
  w_CADTOBSO = ctod("  /  /  ")
  w_VOCCEN = space(15)
  w_VOCCOS = space(15)
  w_CODCEN = space(15)
  w_CODCEN1 = space(15)
  w_CENCOS = space(15)
  w_FL_FRAZ = space(1)
  w_TIPO = space(1)
  w_TARTEM = space(1)
  w_TARCON = 0
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_CODNOM = space(15)
  w_SAV_PAFLDESP = space(0)
  w_CODNOM = space(15)
  w_FlCompleta = space(1)
  w_SETLINKED = .f.
  w_Mask = .NULL.
  w_Gestatt = .NULL.
  w_FLSPAN = space(1)
  w_CursorZoom = space(1)
  w_ObjMDA = .NULL.
  w_CODRESP = space(5)
  w_FLASPAN = space(1)
  w_ROPERA3 = space(1)
  w_RMOLTI3 = 0
  w_FLSPAN = space(1)
  w_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  w_TIPRIS = space(1)
  w_DCODCEN = space(15)
  w_SERPER = space(41)
  w_DESRIS = space(60)
  w_DESCOD = space(40)
  w_DESSUP = space(20)
  w_Mask = .NULL.
  w_CursorZoom = space(1)
  w_ObjMPR = .NULL.
  w_ObjKPR = .NULL.
  w_NewData = ctod("  /  /  ")
  w_ARPRESTA = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  UNIMIS_idx=0
  DIPENDEN_idx=0
  LISTINI_idx=0
  CAN_TIER_idx=0
  KEY_ARTI_idx=0
  PRA_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- INSERIMENTO MULTIPLO DELLE PRESTAZIONI 
    * --- Maschera di Scelta Multipla prestazioni
    * --- Zoom delle prestazioni
    if upper(this.oParentObject.class) = "TGSOF_BED"
      this.w_Mask = this.oParentObject.w_GSAG_KPR
      this.w_CursorZoom = this.oParentObject.w_CUR_PREST
    else
      this.w_Mask = this.oParentObject
      this.w_CursorZoom = this.w_Mask.w_AGKPM_ZOOM.ccursor
    endif
    * --- Numero riga
    * --- Costo orario responsabile prestazione
    select (this.w_CursorZoom)
    COUNT FOR XCHK=1 TO this.CONTA_PREST
    if this.CONTA_PREST = 0
      ah_ErrorMsg("Non ci sono prestazioni selezionate")
      this.oParentObject.w_RESCHK = -1
      i_retcode = 'stop'
      return
    endif
    do case
      case upper(this.oParentObject.class) = "TGSOF_BED"
        * --- Caricamento prestazioni da GSOF_BOF
        * --- Inserimento prestazioni multiple in Prestazioni rapide
        this.w_FLASPAN = "N"
        this.w_ObjKPR = this.oParentObject.w_GSAG_KPR
        this.w_ObjMPR = this.w_ObjKPR.GSAG_MPR
        this.w_CENCOS = this.w_ObjKPR.w_ATCENCOS
        this.w_CODNOM = this.w_ObjKPR.w_DACODNOM
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case upper(this.oParentObject.oParentObject.cPrg) = "GSAG_AAT"
        * --- Inserimento prestazioni multiple in Gestione Attivit�
        this.w_Gestatt = this.oParentObject.oParentObject
        this.w_CENCOS = this.w_Gestatt.w_ATCENCOS
        this.w_CODRESP = this.oParentObject.w_CODRESP
        this.w_FLASPAN = this.oParentObject.w_FLASPAN
        this.w_ObjMDA = this.oParentObject.oParentObject.GSAG_MDA
        this.w_SAV_PAFLDESP = this.oParentObject.w_PAFLDESP
         
 Select * from (this.w_CursorZoom) into cursor Temp 
 this.oParentObject.Ecpquit()
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
         
 Use in temp
      case upper(this.oParentObject.oParentObject.cPrg) = "GSAG_KPR" OR upper(this.oParentObject.oParentObject.cPrg) = "GSAG_KPP"
        * --- Inserimento prestazioni multiple in Prestazioni rapide
        this.w_FLASPAN = this.oParentObject.w_FLASPAN
        this.w_ObjKPR = this.oParentObject.oParentObject
        this.w_ObjMPR = iif(isalt(),this.oParentObject.oParentObject.GSAG_MPR,this.oParentObject.oParentObject.GSAG_MPP)
        this.w_CENCOS = this.w_ObjKPR.w_ATCENCOS
        this.w_CODNOM = this.w_ObjKPR.w_DACODNOM
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_FlCompleta = this.oParentObject.oParentObject.w_DM_FLCompleta
        if this.w_FlCompleta="S"
          this.oParentObject.oParentObject.NotifyEvent("Completa")
        endif
      case upper(this.oParentObject.oParentObject.cPrg) = "GSAG_MIT"
        this.w_ObjMPR = this.oParentObject.oParentObject
        this.w_ObjMPR.MarkPos()     
        w_WORKAREA=ALIAS()
        SELECT (this.w_CursorZoom)
        * --- Cicla sul cursore dello zoom relativo alle prestazioni e carica i record nella gestione
        scan for XCHK=1
        this.w_ObjMPR.AddRow()     
        if Isahe()
          this.w_ObjMPR.w_ITCOD_PR = CACODICE
          this.w_ObjMPR.w_DESPRE2 = CADESART
          this.w_ObjMPR.w_ECACODART = CACODART
          this.w_ObjMPR.w_ECADTOBSO = CP_TODATE(CADTOBSO)
        else
          this.w_ObjMPR.w_ITCODPRE = CACODICE
          this.w_ObjMPR.w_DESPRE = CADESART
          this.w_ObjMPR.w_ITDESPRE = CADESART
          this.w_ObjMPR.w_RCACODART = CACODART
          this.w_ObjMPR.w_RCADTOBSO = CP_TODATE(CADTOBSO)
        endif
        this.w_ObjMPR.SaveRow()     
        SELECT (this.w_CursorZoom)
        Endscan
        this.w_ObjMPR.RePos()     
        if NOT EMPTY(w_WORKAREA)
          Select (w_WORKAREA)
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento delle prestazioni multiple nelle Attivit�
    * --- Maschera di Scelta Multipla prestazioni
    * --- Zoom delle prestazioni
    * --- Dettaglio Attivit� (prestazioni)
    * --- Si posiziona sull'ultima riga
    this.w_ObjMDA.LastRow()     
    * --- Se non � verificata la condizione di riga piena
    if not(this.w_ObjMDA.FullRow() ) 
      * --- Se nel tmp vi � almeno una riga piena
      if this.w_ObjMDA.NumRow()>0
        * --- Si posiziona sulla riga precedente
        this.w_ObjMDA.PriorRow()     
        this.w_ObjMDA.SetRow()     
        this.w_NUMRIG = this.w_ObjMDA.w_CPROWORD
      else
        * --- In presenza di una sola riga
        this.w_NUMRIG = 0
      endif
    else
      * --- Se � verificata la condizione di riga piena
      this.w_ObjMDA.SetRow()     
      * --- Legge il n. di riga dell'ultima prestazione nel Dettaglio Attivit�
      this.w_NUMRIG = this.w_ObjMDA.w_CPROWORD
    endif
    SELECT TEMP
    * --- Cicla sul cursore dello zoom relativo alle prestazioni ed esamina i records selezionati
    scan for XCHK=1
    * --- Legge sul cursore dello zoom il codice della prestazione
    this.w_CODSER = CACODICE
    this.w_NUMRIG = this.w_NUMRIG+10
    * --- Aggiunge un nuovo record nel Dettaglio Attivit�
    this.w_ObjMDA.AddRow()     
    this.w_ObjMDA.w_CPROWORD = this.w_NUMRIG
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODART,CADESART,CADESSUP,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CODSER);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODART,CADESART,CADESSUP,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
        from (i_cTable) where;
            CACODICE = this.w_CODSER;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      this.w_DESPREST = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
      this.w_DESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
      this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      this.w_CADTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
      this.w_TIPO = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
      this.w_ROPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
      this.w_RMOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARTIPRIG,ARTIPRI2,ARFLSPAN"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARTIPRIG,ARTIPRI2,ARFLSPAN;
        from (i_cTable) where;
            ARCODART = this.w_CODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      this.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
      this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
      this.w_ARTIPRIG = NVL(cp_ToDate(_read_.ARTIPRIG),cp_NullValue(_read_.ARTIPRIG))
      this.w_ARTIPRI2 = NVL(cp_ToDate(_read_.ARTIPRI2),cp_NullValue(_read_.ARTIPRI2))
      this.w_FLSPAN = NVL(cp_ToDate(_read_.ARFLSPAN),cp_NullValue(_read_.ARFLSPAN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se attivo il check Usa descrizione prestazioni nei Parametri generali
    if this.w_SAV_PAFLDESP="S"
      SELECT TEMP
      this.w_DESPREST = CADESART
    endif
    * --- Read from DIPENDEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DPCODCEN"+;
        " from "+i_cTable+" DIPENDEN where ";
            +"DPCODICE = "+cp_ToStrODBC(this.w_CODRESP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DPCODCEN;
        from (i_cTable) where;
            DPCODICE = this.w_CODRESP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODCEN = NVL(cp_ToDate(_read_.DPCODCEN),cp_NullValue(_read_.DPCODCEN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_VOCCOS = SearchVoc(this,"C",this.w_CODART,this.w_Gestatt.w_ATTIPCLI,this.w_Gestatt.w_CODCLI,this.w_Gestatt.w_CAUDOC,this.w_Gestatt.w_ATCODNOM)
    this.w_VOCCEN = SearchVoc(this,"R",this.w_CODART,this.w_Gestatt.w_ATTIPCLI,this.w_Gestatt.w_CODCLI,this.w_Gestatt.w_CAUDOC,this.w_Gestatt.w_ATCODNOM)
    if Isahe()
      this.w_ObjMDA.w_DACODICE = this.w_CODSER
      this.w_ObjMDA.w_ECOD1ATT = this.w_CODART
      this.w_ObjMDA.w_ECACODART = this.w_CODART
      this.w_ObjMDA.w_ETIPSER = this.w_TIPO
      this.w_ObjMDA.w_EDESATT = this.w_DESPREST
      this.w_ObjMDA.w_EDESAGG = this.w_DESSUP
    else
      this.w_ObjMDA.w_DACODATT = this.w_CODSER
      this.w_ObjMDA.w_RCOD1ATT = this.w_CODART
      this.w_ObjMDA.w_RCACODART = this.w_CODART
      this.w_ObjMDA.w_RTIPSER = this.w_TIPO
      this.w_ObjMDA.w_RDESATT = this.w_DESPREST
      this.w_ObjMDA.w_RDESAGG = this.w_DESSUP
    endif
    this.w_ObjMDA.w_ARTIPRIG = this.w_ARTIPRIG
    this.w_ObjMDA.w_ARTIPRI2 = this.w_ARTIPRI2
    this.w_ObjMDA.w_TIPSER = this.w_TIPO
    this.w_ObjMDA.w_COD1ATT = this.w_CODART
    this.w_ObjMDA.w_RCOD1ATT = this.w_CODART
    this.w_ObjMDA.w_RCACODART = this.w_CODART
    this.w_UNIMIS = NVL(this.w_UNIMIS, SPACE(3))
    this.w_ObjMDA.w_DAUNIMIS = this.w_UNIMIS
    this.w_ObjMDA.w_ROPERA3 = this.w_ROPERA3
    this.w_ObjMDA.w_RMOLTI3 = this.w_RMOLTI3
    this.w_ObjMDA.w_FLSPAN = this.w_FLSPAN
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ;
        from (i_cTable) where;
            UMCODICE = this.w_UNIMIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESUNI = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
      this.w_FLTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
      this.w_DURORE = NVL(cp_ToDate(_read_.UMDURORE),cp_NullValue(_read_.UMDURORE))
      this.w_FL_FRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_FLTEMP = NVL(this.w_FLTEMP, SPACE(1))
    this.w_DURORE = NVL(this.w_DURORE, 0)
    this.w_ObjMDA.w_FL_FRAZ = NVL(this.w_FL_FRAZ, SPACE(1))
    this.w_ObjMDA.w_DESUNI = NVL(this.w_DESUNI, SPACE(35))
    this.w_ObjMDA.w_UNMIS1 = NVL(this.w_UNMIS1, SPACE(3))
    this.w_ObjMDA.w_UNMIS2 = NVL(this.w_UNMIS2, SPACE(3))
    this.w_ObjMDA.w_UNMIS3 = NVL(this.w_UNMIS3, SPACE(3))
    this.w_ObjMDA.w_FLSERG = NVL(this.w_FLSERG, " ")
    this.w_ObjMDA.w_DADATMOD = i_datsys
    if Isahe()
      =setvaluelinked("D", this.w_ObjMDA,"w_DACODICE", this.w_CODSER)
    else
      =setvaluelinked("D", this.w_ObjMDA,"w_DACODATT", this.w_CODSER)
    endif
    * --- Traduzione in Lingua della Descrizione Articoli 
    this.w_ObjMDA.w_DADESATT = this.w_DESPREST
    this.w_ObjMDA.w_DADESAGG = this.w_DESSUP
    this.w_ObjMDA.w_FLRESP = "N"
    this.w_ObjMDA.w_DAFLDEFF = "N"
    this.w_ObjMDA.w_DUR_ORE = this.w_DURORE
    this.w_ObjMDA.w_CHKTEMP = this.w_FLTEMP
    this.w_ObjMDA.w_FLSPAN = this.w_FLSPAN
    this.w_ObjMDA.w_DAVOCCOS = SearchVoc(this,"C",this.w_CODART,this.w_Gestatt.w_ATTIPCLI,this.w_Gestatt.w_CODCLI,this.w_Gestatt.w_CAUDOC,this.w_Gestatt.w_ATCODNOM)
    this.w_ObjMDA.w_DACENCOS = IIF(NOT EMPTY(NVL(this.w_CODCEN, SPACE(15))), NVL(this.w_CODCEN, SPACE(15)), this.w_Gestatt.w_ATCENCOS)
    this.w_ObjMDA.w_DAVOCRIC = SearchVoc(this,"R",this.w_CODART,this.w_Gestatt.w_ATTIPCLI,this.w_Gestatt.w_CODCLI,this.w_Gestatt.w_CAUDOC,this.w_Gestatt.w_ATCODNOM)
    this.w_ObjMDA.w_DACODCOM = NVL(this.w_Gestatt.w_ATCODPRA, SPACE(15))
    this.w_ObjMDA.w_DACENRIC = IIF(NOT EMPTY(NVL(this.w_CODCEN, SPACE(15))), NVL(this.w_CODCEN, SPACE(15)), this.w_Gestatt.w_ATCENRIC)
    this.w_ObjMDA.w_DACOMRIC = NVL(this.w_Gestatt.w_ATCOMRIC, SPACE(15))
    if this.w_FLTEMP="S"
      * --- La quantit� della prestazione � 1
      this.w_ObjMDA.w_DAOREEFF = INT(this.w_DURORE)
      this.w_ObjMDA.w_DAMINEFF = INT((this.w_DURORE - INT(this.w_DURORE)) * 60)
    else
      this.w_ObjMDA.w_DAOREEFF = 0
      this.w_ObjMDA.w_DAMINEFF = 0
    endif
    this.w_ObjMDA.w_DACOSINT = 0
    * --- Responsabile valorizzato in maschera
    this.w_ObjMDA.w_DACODRES = this.w_CODRESP
    * --- Lettura costo orario del responsabile della prestazione
    * --- Read from DIPENDEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DPCOSORA,DPCOGNOM,DPNOME,DPTIPRIS,DPCODCEN,DPSERPRE,DPDESCRI"+;
        " from "+i_cTable+" DIPENDEN where ";
            +"DPCODICE = "+cp_ToStrODBC(this.w_CODRESP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DPCOSORA,DPCOGNOM,DPNOME,DPTIPRIS,DPCODCEN,DPSERPRE,DPDESCRI;
        from (i_cTable) where;
            DPCODICE = this.w_CODRESP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COSORA = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
      this.w_COGNOME = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
      this.w_NOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
      this.w_TIPRIS = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
      this.w_DCODCEN = NVL(cp_ToDate(_read_.DPCODCEN),cp_NullValue(_read_.DPCODCEN))
      this.w_SERPER = NVL(cp_ToDate(_read_.DPSERPRE),cp_NullValue(_read_.DPSERPRE))
      this.w_DESRIS = NVL(cp_ToDate(_read_.DPDESCRI),cp_NullValue(_read_.DPDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ObjMDA.w_TIPRIS = NVL(this.w_TIPRIS, SPACE(1))
    this.w_COSORA = NVL(this.w_COSORA,0)
    this.w_ObjMDA.w_COST_ORA = this.w_COSORA
    this.w_ObjMDA.w_COGNOME = NVL(this.w_COGNOME, SPACE(40))
    this.w_ObjMDA.w_NOME = NVL(this.w_NOME, SPACE(40))
    this.w_ObjMDA.w_DENOM_RESP = alltrim(NVL(this.w_COGNOME, SPACE(40))) + " " + alltrim(NVL(this.w_NOME, SPACE(40)))
    this.w_ObjMDA.w_TIPART = this.w_TIPART
    this.w_ObjMDA.w_ARTIPART = this.w_TIPART
    this.w_ObjMDA.w_DACODOPE = i_codute
    this.w_ObjMDA.w_DADATMOD = i_datsys
    this.w_ObjMDA.w_CACODART = this.w_CODART
    this.w_ObjMDA.w_CADTOBSO = this.w_CADTOBSO
    this.w_ObjMDA.w_DACOSINT = (this.w_ObjMDA.w_DAOREEFF+(this.w_ObjMDA.w_DAMINEFF/60))*this.w_COSORA
    this.w_ObjMDA.w_DAQTAMOV = IIF(this.w_TIPART="DE", 0, 1)
    this.w_ObjMDA.w_DCODCEN = this.w_DCODCEN
    this.w_ObjMDA.w_SERPER = this.w_SERPER
    this.w_ObjMDA.w_DESRIS = this.w_DESRIS
    =setvaluelinked("D", this.w_ObjMDA,"w_DACODRES", this.w_CODRESP)
    this.w_ObjMDA.SaveDependsOn()     
    this.w_ObjMDA.NotifyEvent("Aggcosto")     
    this.w_ObjMDA.NotifyEvent("AggPre")     
    if NOT EMPTY(this.w_Gestatt.w_CODLIN) AND this.w_Gestatt.w_CODLIN<>g_CODLIN
      * --- Legge la Traduzione
      DECLARE ARRRIS (2) 
 a=Tradlin(this.w_ObjMDA.w_DACODATT,this.w_Gestatt.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
      if Not Empty(this.w_DESCOD)
        this.w_ObjMDA.w_DADESATT = this.w_DESCOD
        this.w_ObjMDA.w_DADESAGG = this.w_DESSUP
      endif
    endif
    if this.w_FLSPAN="S" and this.w_FLASPAN="S"
      this.w_ObjMDA.NotifyEvent("CalcPrest")     
    endif
    this.w_ObjMDA.NotifyEvent("Leggeimpcon")     
    this.w_ObjMDA.SaveRow()     
    SELECT TEMP
    endscan
    this.w_ObjMDA.Refresh()     
    this.w_ObjMDA.bUpdated = .T.
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento delle prestazioni multiple in Prestazioni rapide
    * --- Maschera di Scelta Multipla prestazioni
    * --- Zoom delle prestazioni
    if upper(this.oParentObject.class) = "TGSOF_BED"
      this.w_Mask = this.oParentObject.w_GSAG_KPR
      this.w_CursorZoom = this.oParentObject.w_CUR_PREST
    else
      this.w_Mask = this.oParentObject
      this.w_CursorZoom = this.w_Mask.w_AGKPM_ZOOM.ccursor
    endif
    * --- Prestazioni rapide (detail)
    * --- Prestazioni rapide (maschera)
    SELECT (this.w_CursorZoom)
    * --- Cicla sul cursore dello zoom relativo alle prestazioni ed esamina i records selezionati
    scan for XCHK=1
    * --- Legge sul cursore dello zoom il codice della prestazione
    this.w_CODSER = CACODICE
    this.w_NewData = this.oParentObject.w_DATAPRE
    if ISALT()
      if VARTYPE(NEWDT)="D" OR VARTYPE(NEWDT)="T"
        * --- Nello zoom � presente il campo data
        if !EMPTY(NEWDT)
          * --- La nuova data da assegnare � quella inserita nello zoom
          this.w_NewData = NEWDT
        endif
      endif
    endif
    * --- Aggiunge un nuovo record nelle Prestazioni rapide
    this.w_ObjMPR.AddRow()     
    this.w_ObjMPR.NotifyEvent("DefaultValue")     
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODART,CADESART,CADESSUP,CAUNIMIS,CADTOBSO,CA__TIPO"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CODSER);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODART,CADESART,CADESSUP,CAUNIMIS,CADTOBSO,CA__TIPO;
        from (i_cTable) where;
            CACODICE = this.w_CODSER;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      this.w_DESPREST = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
      this.w_DESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
      this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      this.w_CADTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
      this.w_TIPO = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARTIPRIG,ARTIPRI2,ARFLSPAN,ARPRESTA"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARTIPRIG,ARTIPRI2,ARFLSPAN,ARPRESTA;
        from (i_cTable) where;
            ARCODART = this.w_CODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      this.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
      this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
      this.w_ARTIPRIG = NVL(cp_ToDate(_read_.ARTIPRIG),cp_NullValue(_read_.ARTIPRIG))
      this.w_ARTIPRI2 = NVL(cp_ToDate(_read_.ARTIPRI2),cp_NullValue(_read_.ARTIPRI2))
      this.w_FLSPAN = NVL(cp_ToDate(_read_.ARFLSPAN),cp_NullValue(_read_.ARFLSPAN))
      this.w_ARPRESTA = NVL(cp_ToDate(_read_.ARPRESTA),cp_NullValue(_read_.ARPRESTA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se attivo il check Usa descrizione prestazioni nei Parametri generali
    if this.oParentObject.w_PAFLDESP="S"
      SELECT (this.w_CursorZoom)
      this.w_DESPREST = CADESART
    endif
    if NOT EMPTY(NVL(this.oParentObject.w_DESCRI,""))
      this.w_DESPREST = LEFT(this.oParentObject.w_DESCRI,40)
    endif
    if Isahe()
      this.w_ObjMPR.w_DACODICE = this.w_CODSER
      this.w_ObjMPR.w_ECOD1ATT = this.w_CODSER
      this.w_ObjMPR.w_ECACODART = this.w_CODART
      this.w_ObjMPR.w_ETIPSER = this.w_TIPO
    else
      this.w_ObjMPR.w_DACODATT = this.w_CODSER
      this.w_ObjMPR.w_RCOD1ATT = this.w_CODART
      this.w_ObjMPR.w_RCACODART = this.w_CODART
      this.w_ObjMPR.w_RTIPSER = this.w_TIPO
    endif
    this.w_ObjMPR.w_TIPSER = this.w_TIPO
    this.w_ObjMPR.w_COD1ATT = this.w_CODART
    if upper(this.oParentObject.class) = "TGSOF_BED"
      this.w_UNIMIS = NVL(ODUNIMIS, SPACE(3))
    else
      this.w_UNIMIS = NVL(this.w_UNIMIS, SPACE(3))
    endif
    this.w_ObjMPR.w_PRUNIMIS = this.w_UNIMIS
    this.w_ObjMPR.w_ARTIPRIG = this.w_ARTIPRIG
    this.w_ObjMPR.w_ARTIPRI2 = this.w_ARTIPRI2
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ;
        from (i_cTable) where;
            UMCODICE = this.w_UNIMIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESUNI = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
      this.w_FLTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
      this.w_DURORE = NVL(cp_ToDate(_read_.UMDURORE),cp_NullValue(_read_.UMDURORE))
      this.w_FL_FRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_FLTEMP = NVL(this.w_FLTEMP, SPACE(1))
    this.w_DURORE = NVL(this.w_DURORE, 0)
    this.w_ObjMPR.w_FL_FRAZ = NVL(this.w_FL_FRAZ, SPACE(1))
    this.w_ObjMPR.w_DESUNI = NVL(this.w_DESUNI, SPACE(35))
    this.w_ObjMPR.w_UNMIS1 = NVL(this.w_UNMIS1, SPACE(3))
    this.w_ObjMPR.w_UNMIS2 = NVL(this.w_UNMIS2, SPACE(3))
    this.w_ObjMPR.w_UNMIS3 = NVL(this.w_UNMIS3, SPACE(3))
    this.w_ObjMPR.w_UNIMIS = NVL(this.w_UNIMIS, SPACE(3))
    this.w_ObjMPR.w_FLSERG = NVL(this.w_FLSERG, " ")
    this.w_ObjMPR.w_PRDATMOD = i_datsys
    this.w_ObjMPR.w_PRDESPRE = this.w_DESPREST
    this.w_ObjMPR.w_PRDESAGG = this.w_DESSUP
    this.w_ObjMPR.w_DAFLDEFF = "N"
    this.w_ObjMPR.w_DUR_ORE = this.w_DURORE
    this.w_ObjMPR.w_CHKTEMP = this.w_FLTEMP
    this.w_ObjMPR.w_FLSPAN = this.w_FLSPAN
    * --- Read from DIPENDEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIPENDEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DPCOSORA,DPCODCEN"+;
        " from "+i_cTable+" DIPENDEN where ";
            +"DPCODICE = "+cp_ToStrODBC(this.w_ObjKPR.w_CODRESP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DPCOSORA,DPCODCEN;
        from (i_cTable) where;
            DPCODICE = this.w_ObjKPR.w_CODRESP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COSORA = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
      this.w_CODCEN1 = NVL(cp_ToDate(_read_.DPCODCEN),cp_NullValue(_read_.DPCODCEN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if !Isalt()
      =setvaluelinked("D", this.w_ObjMPR,"w_DACODNOM", this.w_CODNOM)
    endif
    if Isahe()
      if this.w_ObjMPR.w_FLACQ $ "S-I"
        this.w_ObjMPR.w_DACENCOS = IIF(NOT EMPTY(NVL(this.w_CODCEN1, SPACE(15))), NVL(this.w_CODCEN1, SPACE(15)), this.w_ObjKPR.w_ATCENCOS)
      endif
      if this.w_ObjMPR.w_FLDANA $ "S-I"
        this.w_ObjMPR.w_DACENRIC = IIF(NOT EMPTY(NVL(this.w_CODCEN1, SPACE(15))), NVL(this.w_CODCEN1, SPACE(15)), this.w_ObjKPR.w_ATCENRIC)
      endif
      if (this.w_ObjMPR.w_FLACQ $ "S-I" or this.w_ObjMPR.w_FLACOMAQ <>"N") 
        this.w_ObjMPR.w_DAVOCCOS = NVL(SearchVoc(this,"C",this.w_CODART,this.w_ObjMPR.w_NOTIPCLI,this.w_ObjMPR.w_NOCODCLI,this.w_ObjMPR.w_CAUDOC,this.w_ObjKPR.w_DACODNOM), SPACE(15))
      endif
      if this.w_ObjMPR.w_FLDANA $ "S-I" or this.w_ObjMPR.w_FLGCOM <>"N"
        this.w_ObjMPR.w_DAVOCRIC = NVL(SearchVoc(this,"R",this.w_CODART,this.w_ObjKPR.w_NOTIPCLI,this.w_ObjKPR.w_NOCODCLI,this.w_ObjMPR.w_CAUDOC,this.w_ObjKPR.w_DACODNOM), SPACE(15))
      endif
      if this.w_ObjMPR.w_FLACOMAQ <>"N"
        this.w_ObjMPR.w_DACODCOM = NVL(this.w_ObjKPR.w_ATCODPRA, SPACE(15))
        this.w_ObjMPR.w_PRNUMPRA = this.w_ObjMPR.w_DACODCOM
      endif
      if this.w_ObjMPR.w_FLGCOM <>"N"
        this.w_ObjMPR.w_DACOMRIC = NVL(this.w_ObjKPR.w_ATCOMRIC, SPACE(15))
      endif
    else
      this.w_ObjMPR.w_DAVOCCOS = SearchVoc(this,"C",this.w_CODART,this.w_ObjKPR.w_ATTIPCLI,this.w_ObjKPR.w_NOCODCLI,this.w_ObjKPR.w_CAUDOC,this.w_ObjKPR.w_DACODNOM)
      this.w_ObjMPR.w_DACENCOS = IIF(NOT EMPTY(NVL(this.w_CODCEN1, SPACE(15))), NVL(this.w_CODCEN1, SPACE(15)), this.w_ObjKPR.w_ATCENCOS)
      this.w_ObjMPR.w_DAVOCRIC = SearchVoc(this,"R",this.w_CODART,this.w_ObjKPR.w_ATTIPCLI,this.w_ObjKPR.w_NOCODCLI,this.w_ObjKPR.w_CAUDOC,this.w_ObjKPR.w_DACODNOM)
      this.w_ObjMPR.w_DACODCOM = NVL(this.w_ObjKPR.w_ATCODPRA, SPACE(15))
      this.w_ObjMPR.w_DACENRIC = IIF(NOT EMPTY(NVL(this.w_CODCEN1, SPACE(15))), NVL(this.w_CODCEN1, SPACE(15)),this.w_ObjKPR.w_ATCENRIC)
      this.w_ObjMPR.w_DACOMRIC = NVL(this.w_ObjKPR.w_ATCOMRIC, SPACE(15))
      this.w_ObjMPR.w_PRNUMPRA = this.oParentObject.w_NUMPRA
    endif
    if this.w_FLTEMP="S"
      * --- La quantit� della prestazione � 1
      this.w_ObjMPR.w_PROREEFF = INT(this.w_DURORE)
      this.w_ObjMPR.w_PRMINEFF = INT((this.w_DURORE - INT(this.w_DURORE)) * 60)
    else
      this.w_ObjMPR.w_PROREEFF = 0
      this.w_ObjMPR.w_PRMINEFF = 0
    endif
    this.w_ObjMPR.w_PRCOSINT = 0
    * --- Lettura costo orario del responsabile della prestazione
    this.w_COSORA = NVL(this.w_COSORA,0)
    this.w_ObjMPR.w_COST_ORA = this.w_COSORA
    this.w_ObjMPR.w_TIPART = this.w_TIPART
    this.w_ObjMPR.w_PRCODOPE = i_codute
    this.w_ObjMPR.w_PRDATMOD = i_datsys
    * --- Data prestazione
    this.w_ObjMPR.w_PR__DATA = this.w_NewData
    * --- Valuta
    this.w_ObjMPR.w_PRCODVAL = g_PERVAL
    * --- Listino
    this.w_ObjMPR.w_PRCODLIS = this.oParentObject.w_CODLISPRE
    this.w_ObjMPR.w_CCODLIS = this.oParentObject.w_CODLISPRE
    if !empty(this.oParentObject.w_CODLISPRE)
      * --- Read from LISTINI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LISTINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LSVALLIS"+;
          " from "+i_cTable+" LISTINI where ";
              +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_CODLISPRE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LSVALLIS;
          from (i_cTable) where;
              LSCODLIS = this.oParentObject.w_CODLISPRE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VALLIS = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_ObjMPR.w_VALLIS = NVL(this.w_VALLIS, SPACE(5))
    * --- Pratica
    this.w_NOMEPRA = space(100)
    this.w_FLAPON = space(1)
    this.w_ENTE = space(10)
    this.w_UFFICI = space(10)
    this.w_FLVALO = space(1)
    this.w_IMPORT = 0
    this.w_CALDIR = space(1)
    this.w_COECAL = 0
    this.w_PARASS = 0
    this.w_FLAMPA = "N"
    if !empty(this.oParentObject.w_NUMPRA)
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNDESCAN,CNFLAPON,CN__ENTE,CNUFFICI,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNFLDIND"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_NUMPRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNDESCAN,CNFLAPON,CN__ENTE,CNUFFICI,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNFLDIND;
          from (i_cTable) where;
              CNCODCAN = this.oParentObject.w_NUMPRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NOMEPRA = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
        this.w_FLAPON = NVL(cp_ToDate(_read_.CNFLAPON),cp_NullValue(_read_.CNFLAPON))
        this.w_ENTE = NVL(cp_ToDate(_read_.CN__ENTE),cp_NullValue(_read_.CN__ENTE))
        this.w_UFFICI = NVL(cp_ToDate(_read_.CNUFFICI),cp_NullValue(_read_.CNUFFICI))
        this.w_FLVALO = NVL(cp_ToDate(_read_.CNFLVALO),cp_NullValue(_read_.CNFLVALO))
        this.w_IMPORT = NVL(cp_ToDate(_read_.CNIMPORT),cp_NullValue(_read_.CNIMPORT))
        this.w_CALDIR = NVL(cp_ToDate(_read_.CNCALDIR),cp_NullValue(_read_.CNCALDIR))
        this.w_COECAL = NVL(cp_ToDate(_read_.CNCOECAL),cp_NullValue(_read_.CNCOECAL))
        this.w_PARASS = NVL(cp_ToDate(_read_.CNPARASS),cp_NullValue(_read_.CNPARASS))
        this.w_FLAMPA = NVL(cp_ToDate(_read_.CNFLAMPA),cp_NullValue(_read_.CNFLAMPA))
        this.w_TARTEM = NVL(cp_ToDate(_read_.CNTARTEM),cp_NullValue(_read_.CNTARTEM))
        this.w_TARCON = NVL(cp_ToDate(_read_.CNTARCON),cp_NullValue(_read_.CNTARCON))
        this.w_CONTRACN = NVL(cp_ToDate(_read_.CNCONTRA),cp_NullValue(_read_.CNCONTRA))
        this.w_FLVALO1 = NVL(cp_ToDate(_read_.CNFLDIND),cp_NullValue(_read_.CNFLDIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if !empty(this.w_CONTRACN)
      * --- Read from PRA_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRA_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRA_CONT_idx,2],.t.,this.PRA_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CPTIPCON,CPLISCOL,CPSTATUS"+;
          " from "+i_cTable+" PRA_CONT where ";
              +"CPCODCON = "+cp_ToStrODBC(this.w_CONTRACN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CPTIPCON,CPLISCOL,CPSTATUS;
          from (i_cTable) where;
              CPCODCON = this.w_CONTRACN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPCONCO = NVL(cp_ToDate(_read_.CPTIPCON),cp_NullValue(_read_.CPTIPCON))
        this.w_LISCOLCO = NVL(cp_ToDate(_read_.CPLISCOL),cp_NullValue(_read_.CPLISCOL))
        this.w_STATUSCO = NVL(cp_ToDate(_read_.CPSTATUS),cp_NullValue(_read_.CPSTATUS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_ObjMPR.w_NOMEPRA = NVL(this.w_NOMEPRA, SPACE(100))
    this.w_ObjMPR.w_FLAPON = NVL(this.w_FLAPON, SPACE(1))
    this.w_ObjMPR.w_ENTE = NVL(this.w_ENTE, SPACE(10))
    this.w_ObjMPR.w_UFFICI = NVL(this.w_UFFICI, SPACE(10))
    this.w_ObjMPR.w_FLVALO = NVL(this.w_FLVALO, SPACE(1))
    this.w_ObjMPR.w_FLVALO1 = NVL(this.w_FLVALO1, SPACE(1))
    this.w_ObjMPR.w_IMPORT = NVL(this.w_IMPORT, 0)
    this.w_ObjMPR.w_CALDIR = NVL(this.w_CALDIR, SPACE(1))
    this.w_ObjMPR.w_COECAL = NVL(this.w_COECAL, 0)
    this.w_ObjMPR.w_TARTEM = NVL(this.w_TARTEM, SPACE(1))
    this.w_ObjMPR.w_TARCON = NVL(this.w_TARCON, 0)
    this.w_ObjMPR.w_CONTRACN = NVL(this.w_CONTRACN, SPACE(1))
    this.w_ObjMPR.w_TIPCONCO = NVL(this.w_TIPCONCO, SPACE(1))
    this.w_ObjMPR.w_LISCOLCO = NVL(this.w_LISCOLCO, SPACE(1))
    this.w_ObjMPR.w_STATUSCO = NVL(this.w_STATUSCO, SPACE(1))
    this.w_ObjMPR.w_PARASS = NVL(this.w_PARASS, 0)
    this.w_ObjMPR.w_FLAMPA = NVL(this.w_FLAMPA, 0)
    this.w_ObjMPR.w_CACODART = this.w_CODART
    this.w_ObjMPR.w_CADTOBSO = this.w_CADTOBSO
    this.w_ObjMPR.w_PRQTAMOV = IIF(this.w_TIPART="DE", 0, 1)
    this.w_ObjMPR.o_PRUNIMIS = SPACE(3)
    this.w_ObjMPR.w_PRESTA = this.w_ARPRESTA
    this.w_ObjMPR.NotifyEvent("TarConc")     
    this.w_ObjMPR.NotifyEvent("w_PRUNIMIS Changed")     
    if upper(this.oParentObject.class) = "TGSOF_BED"
      SELECT (this.w_CursorZoom)
      this.w_ObjMPR.w_PRQTAMOV = ODQTAMOV
      this.w_ObjMPR.w_DAPREMIN = ODPREMIN
      this.w_ObjMPR.w_DAPREMAX = ODPREMAX
      this.w_ObjMPR.w_DAPREZZO = ODPREZZO
      this.w_ObjMPR.w_PRVALRIG = ODVALRIG
      this.w_ObjMPR.w_DAGAZUFF = ODGAZUFF
      this.w_ObjMPR.w_ATIPRIG = "N"
      this.w_ObjMPR.w_DATIPRIG = "N"
      this.w_ObjMPR.w_ATIPRI2 = "N"
      this.w_ObjMPR.w_DATIPRI2 = "N"
    endif
    if NOT EMPTY(this.w_ObjMPR.w_CODLIN) AND this.w_ObjMPR.w_CODLIN<>g_CODLIN
      * --- Legge la Traduzione
      DECLARE ARRRIS (2) 
 a=Tradlin(this.w_ObjMPR.w_DACODATT,this.w_ObjMPR.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
      if Not Empty(this.w_DESCOD)
        this.w_ObjMPR.w_PRDESPRE = this.w_DESCOD
        this.w_ObjMPR.w_PRDESAGG = this.w_DESSUP
      endif
    endif
    this.w_ObjMPR.SaveRow()     
    if this.w_FLSPAN="S" and this.w_FLASPAN="S"
      this.w_ObjMPR.NotifyEvent("CalcPrest")     
    endif
    SELECT (this.w_CursorZoom)
    endscan
    if this.oParentObject.w_FLDTRP="S"
      this.w_ObjMPR.NotifyEvent("Calcdatamax")     
    endif
    this.w_ObjMPR.Refresh()     
    this.w_ObjMPR.bUpdated = .T.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='DIPENDEN'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='PRA_CONT'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
