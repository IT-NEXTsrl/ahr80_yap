* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_baa                                                        *
*              Aggiornamento attivit� esistenti                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-05                                                      *
* Last revis.: 2012-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_baa",oParentObject)
return(i_retval)

define class tgsag_baa as StdBatch
  * --- Local variables
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_NUMROWAGG = 0
  w_PRINTERR = .f.
  w_bUnderTran = .f.
  * --- WorkFile variables
  OFF_PART_idx=0
  DEF_PART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_ATDATINI = DTOT(IIF(EMPTY(this.oParentObject.w_DATINI), i_IniDat, this.oParentObject.w_DATINI))
    this.w_ATDATFIN = DTOT(IIF(EMPTY(this.oParentObject.w_DATFIN), i_FinDat, this.oParentObject.w_DATFIN))
    this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    * --- Try
    local bErr_0382BA58
    bErr_0382BA58=bTrsErr
    this.Try_0382BA58()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.w_bUnderTran
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      ah_ErrorMsg("Errore durante aggiornamento codice gruppo appartenenza%0%1",48,"",MESSAGE())
    endif
    bTrsErr=bTrsErr or bErr_0382BA58
    * --- End
  endproc
  proc Try_0382BA58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_bUnderTran
      * --- begin transaction
      cp_BeginTrs()
    endif
    if this.oParentObject.w_TIPAGG$ "A-E"
      * --- Write into OFF_PART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="PASERIAL,CPROWNUM"
        do vq_exec with 'gsag_baa',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_PART_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="OFF_PART.PASERIAL = _t2.PASERIAL";
                +" and "+"OFF_PART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PAGRURIS = _t2.DPGRUPRE";
            +i_ccchkf;
            +" from "+i_cTable+" OFF_PART, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="OFF_PART.PASERIAL = _t2.PASERIAL";
                +" and "+"OFF_PART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_PART, "+i_cQueryTable+" _t2 set ";
            +"OFF_PART.PAGRURIS = _t2.DPGRUPRE";
            +Iif(Empty(i_ccchkf),"",",OFF_PART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="OFF_PART.PASERIAL = t2.PASERIAL";
                +" and "+"OFF_PART.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_PART set (";
            +"PAGRURIS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DPGRUPRE";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="OFF_PART.PASERIAL = _t2.PASERIAL";
                +" and "+"OFF_PART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_PART set ";
            +"PAGRURIS = _t2.DPGRUPRE";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".PASERIAL = "+i_cQueryTable+".PASERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PAGRURIS = (select DPGRUPRE from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.oParentObject.w_TIPAGG$ "T-E"
      * --- Write into DEF_PART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DEF_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DEF_PART_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="DFCODICE,CPROWNUM"
        do vq_exec with 'gsag3baa',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DEF_PART_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DEF_PART.DFCODICE = _t2.DFCODICE";
                +" and "+"DEF_PART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DFGRURIS = _t2.DPGRUPRE";
            +i_ccchkf;
            +" from "+i_cTable+" DEF_PART, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DEF_PART.DFCODICE = _t2.DFCODICE";
                +" and "+"DEF_PART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DEF_PART, "+i_cQueryTable+" _t2 set ";
            +"DEF_PART.DFGRURIS = _t2.DPGRUPRE";
            +Iif(Empty(i_ccchkf),"",",DEF_PART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DEF_PART.DFCODICE = t2.DFCODICE";
                +" and "+"DEF_PART.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DEF_PART set (";
            +"DFGRURIS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.DPGRUPRE";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DEF_PART.DFCODICE = _t2.DFCODICE";
                +" and "+"DEF_PART.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DEF_PART set ";
            +"DFGRURIS = _t2.DPGRUPRE";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".DFCODICE = "+i_cQueryTable+".DFCODICE";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DFGRURIS = (select DPGRUPRE from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_bUnderTran
      * --- commit
      cp_EndTrs(.t.)
    endif
    this.w_NUMROWAGG = i_Rows
    vq_exec("QUERY\GSAG1BAA.VQR",this,"__tmp__")
    if USED("__tmp__") AND RECCOUNT("__tmp__")>0
      if this.w_NUMROWAGG>0
        this.w_PRINTERR = ah_yesno("Aggiornamento codice gruppo appartenenza avvenuto correttamente.%0Esistono persone presenti in una o pi� attivit�/tipi attivit� senza gruppo predefinito.%0Procedere con la stampa?")
      else
        this.w_PRINTERR = ah_yesno("Non sono presenti attivit�/tipi attivit� da aggiornare.%0Esistono persone presenti in una o pi� attivit�/tipi attivit� senza gruppo predefinito.%0Procedere con la stampa?")
      endif
      if this.w_PRINTERR
        CP_CHPRN("QUERY\GSAG1BAA.FRX", " ", this)
      endif
    else
      if this.w_NUMROWAGG>0
        ah_ErrorMsg("Aggiornamento codice gruppo appartenenza avvenuto correttamente",64,"")
      else
        ah_ErrorMsg("Non sono presenti attivit�/tipi attivit� da aggiornare",64,"")
      endif
    endif
    USE IN SELECT("__TMP__")
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFF_PART'
    this.cWorkTables[2]='DEF_PART'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
