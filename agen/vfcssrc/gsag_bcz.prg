* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bcz                                                        *
*              Generazione nuova attivit� collegata                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-23                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bcz",oParentObject)
return(i_retval)

define class tgsag_bcz as StdBatch
  * --- Local variables
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_ATPROMEM = ctot("")
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_IMDATINI = ctod("  /  /  ")
  w_IMDATFIN = ctod("  /  /  ")
  w_NUMRIG = 0
  w_CODRIS = space(5)
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_DESCRI = space(40)
  w_TipoRisorsa = space(1)
  w_TIPOGRUP = space(1)
  w_ObjMPA = .NULL.
  w_MODAPP = space(1)
  w_OKPAR = .f.
  w_CODUTE = 0
  w_CODIMP = space(10)
  w_COIMPIM = 0
  w_COCOM = 0
  w_DESCOM = space(50)
  w_DESIMP = space(50)
  w_ObjMCP = .NULL.
  w_CLIIMP = space(15)
  w_SEDE = space(5)
  * --- WorkFile variables
  OFF_PART_idx=0
  DIPENDEN_idx=0
  COM_ATTI_idx=0
  IMP_DETT_idx=0
  IMP_MAST_idx=0
  OFF_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione nuova attivit� collegata
    if TYPE("this.oParentObject.oParentObject")="O" AND !ISNULL(this.oParentObject.oParentObject)
      * --- Viene chiusa l'Attivit� originaria
      this.oParentObject.oParentObject.ecpQuit()
    endif
    * --- Istanzio l'anagrafica
    this.w_OBJECT = GSAG_AAT()
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Vado in caricamento
    this.w_OBJECT.ECPLOAD()     
    GSUT_BVP(this,this.w_OBJECT, "w_ATCAUATT", "["+this.oParentObject.w_TIPOATTCOLL+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATCODPRA", "["+this.oParentObject.w_ATCODPRA_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATCENCOS", "["+this.oParentObject.w_ATCENCOS_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATCODNOM", "["+this.oParentObject.w_ATCODNOM_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATPERSON", "["+this.oParentObject.w_ATPERSON_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATTELEFO", "["+this.oParentObject.w_ATTELEFO_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATCELLUL", "["+this.oParentObject.w_ATCELLUL_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATLOCALI", "["+this.oParentObject.w_ATLOCALI_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_AT__ENTE", "["+this.oParentObject.w_AT__ENTE_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATUFFICI", "["+this.oParentObject.w_ATUFFICI_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATCODSED", "["+this.oParentObject.w_ATCODSED_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATCENRIC", "["+this.oParentObject.w_ATCENRIC_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATCOMRIC", "["+this.oParentObject.w_ATCOMRIC_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATATTRIC", "["+this.oParentObject.w_ATATTRIC_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATATTCOS", "["+this.oParentObject.w_ATATTCOS_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSUT_BVP(this,this.w_OBJECT, "w_ATCONTAT", "["+this.oParentObject.w_ATCONTAT_ORIG+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Dopo la valorizzazione dei seguenti campi non deve essere eseguita la mcalc()
    this.w_OBJECT.w_AT_EMAIL = this.oParentObject.w_AT_EMAIL_ORIG
    this.w_OBJECT.w_AT_EMPEC = this.oParentObject.w_AT_EMPEC_ORIG
    this.w_OBJECT.w_GIOINI = this.oParentObject.w_GIOINI
    this.w_OBJECT.w_GIOFIN = this.oParentObject.w_GIOFIN
    this.w_OBJECT.w_DATINI = this.oParentObject.w_DATINI
    this.w_OBJECT.w_ORAINI = this.oParentObject.w_ORAINI
    this.w_OBJECT.w_MININI = this.oParentObject.w_MININI
    this.w_ATDATINI = cp_CharToDatetime(DTOC(this.oParentObject.w_DATINI)+" "+this.oParentObject.w_ORAINI+":"+this.oParentObject.w_MININI+":00" ) 
    this.w_OBJECT.w_ATDATINI = this.w_ATDATINI
    this.w_OBJECT.o_ATDATINI = this.w_ATDATINI
    this.w_ATPROMEM = IIF(this.oParentObject.w_CAFLPREA="S", this.w_ATDATINI - IIF(this.oParentObject.w_CAMINPRE>0, this.oParentObject.w_CAMINPRE*60, 0), DTOT(cp_CharToDate("  -  -    ")))
    this.w_OBJECT.w_ATPROMEM = this.w_ATPROMEM
    this.w_OBJECT.w_DATAPRO = IIF(this.oParentObject.w_CAFLPREA="S", TTOD(this.w_ATPROMEM), cp_CharToDate("  -  -    "))
    this.w_OBJECT.w_ORAPRO = IIF(this.oParentObject.w_CAFLPREA="S", PADL(ALLTRIM(STR(HOUR(this.w_ATPROMEM))),2,"0"), "")
    this.w_OBJECT.w_MINPRO = IIF(this.oParentObject.w_CAFLPREA="S", PADL(ALLTRIM(STR(MINUTE(this.w_ATPROMEM))),2,"0"), "")
    this.w_OBJECT.w_GIOPRM = IIF(this.oParentObject.w_CAFLPREA="S", LEFT(g_GIORNO[DOW(this.w_ATPROMEM)],3), "")
    this.w_OBJECT.w_DATFIN = this.oParentObject.w_DATFIN
    this.w_OBJECT.w_ATANNDOC = this.oParentObject.w_ANNOPROG
    this.w_OBJECT.w_ATDATDOC = this.oParentObject.w_DATFIN
    this.w_OBJECT.w_ORAFIN = this.oParentObject.w_ORAFIN
    this.w_OBJECT.w_MINFIN = this.oParentObject.w_MINFIN
    this.w_OBJECT.w_ATDATFIN = cp_CharToDatetime(DTOC(this.oParentObject.w_DATFIN)+" "+this.oParentObject.w_ORAFIN+":"+this.oParentObject.w_MINFIN+":00" ) 
    this.w_OBJECT.w_ATDATFIN = this.w_ATDATFIN
    this.w_OBJECT.o_ATDATFIN = this.w_ATDATFIN
    this.w_OBJECT.w_ATOGGETT = this.oParentObject.w_OGGATTCOLL
    this.w_OBJECT.w_ATNOTPIA = this.oParentObject.w_ATNOTPIA_ORIG
    this.w_OBJECT.o_DATINI = this.oParentObject.w_DATINI
    this.w_OBJECT.o_ORAINI = this.oParentObject.w_ORAINI
    this.w_OBJECT.o_MININI = this.oParentObject.w_MININI
    this.w_OBJECT.o_DATFIN = this.oParentObject.w_DATFIN
    this.w_OBJECT.o_ORAFIN = this.oParentObject.w_ORAFIN
    this.w_OBJECT.o_MINFIN = this.oParentObject.w_MINFIN
    * --- Se l'attivit� originaria � collegata ad altre attivit�
    if NOT EMPTY(this.oParentObject.w_ATCAITER_ORIG)
      this.w_OBJECT.w_ATCAITER = this.oParentObject.w_ATCAITER_ORIG
    else
      this.w_OBJECT.w_ATCAITER = this.oParentObject.w_ATSERIAL_ORIG
      this.w_OBJECT.w_AGG_ATT_ORIGINE = "S"
    endif
    this.w_OBJECT.w_ATNUMGIO = INT( (cp_CharToDatetime(DTOC(this.oParentObject.w_DATINI)+" "+this.oParentObject.w_ORAINI+":"+this.oParentObject.w_MININI+":00" ) - cp_CharToDatetime(DTOC(this.oParentObject.w_DATINI_ORIG)+" "+this.oParentObject.w_ORAINI_ORIG+":"+this.oParentObject.w_MININI_ORIG+":00" )) / (3600 * 24) )
    this.w_OBJECT.w_ATRIFSER = this.oParentObject.w_ATSERIAL_ORIG
    this.w_OBJECT.w_DATINI_ORIG = this.oParentObject.w_DATINI_ORIG
    this.w_OBJECT.w_ORAINI_ORIG = this.oParentObject.w_ORAINI_ORIG
    this.w_OBJECT.w_MININI_ORIG = this.oParentObject.w_MININI_ORIG
    this.w_OBJECT.w_ATCODPRA = this.oParentObject.w_ATCODPRA_ORIG
    this.w_OBJECT.w_ATCENCOS = this.oParentObject.w_ATCENCOS_ORIG
    this.w_OBJECT.w_IDRICH = this.oParentObject.w_IDRICH_ORIG
    this.w_OBJECT.w_STARIC = this.oParentObject.w_STARIC_ORIG
    this.w_OBJECT.w_ATSEREVE = this.oParentObject.w_ATSEREVE_ORIG
    this.w_OBJECT.w_ATEVANNO = this.oParentObject.w_ATEVANNO_ORIG
    if this.oParentObject.w_CAFLPDOC="D"
      this.w_OBJECT.NOTIFYEVENT("Forzadoc")
    endif
    this.w_OBJECT.SetControlsValue()     
    this.w_OBJECT.MhideControls()     
    if NOT ISALT()
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento partecipanti attivit�
    this.w_MODAPP = this.w_OBJECT.w_MODAPP
    this.w_ObjMPA = this.w_OBJECT.GSAG_MPA
    SELECT (this.w_ObjMPA.cTrsName)
    if reccount()<>0
      * --- Azzera Righe partecipanti
      this.w_ObjMPA.MarkPos()     
      this.w_ObjMPA.FirstRow()     
      do while ! this.w_ObjMPA.Eof_Trs()
        this.w_ObjMPA.DeleteRow()     
        this.w_ObjMPA.NextRow()     
      enddo
      this.w_ObjMPA.RePos()     
    endif
    this.w_NUMRIG = 0
    this.w_OKPAR = .f.
    * --- Select from GSAG_BCZ
    do vq_exec with 'GSAG_BCZ',this,'_Curs_GSAG_BCZ','',.f.,.t.
    if used('_Curs_GSAG_BCZ')
      select _Curs_GSAG_BCZ
      locate for 1=1
      do while not(eof())
      this.w_CODRIS = NVL(_Curs_GSAG_BCZ.PACODRIS, SPACE(5))
      this.w_OKPAR = .t.
      * --- Se nel temporaneo dei Partecipanti non � gi� presente la risorsa in esame
      if this.w_ObjMPA.Search("NVL(t_PACODRIS, SPACE(5))= " + cp_ToStrODBC(this.w_CODRIS) + " AND NOT DELETED()") = -1
        * --- Nuova Riga del Temporaneo
        this.w_NUMRIG = this.w_NUMRIG+10
        this.w_ObjMPA.AddRow()     
        this.w_ObjMPA.w_CPROWORD = this.w_NUMRIG
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPCODUTE"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_CODRIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPCODUTE;
            from (i_cTable) where;
                DPCODICE = this.w_CODRIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COGNOME = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
          this.w_NOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
          this.w_DESCRI = NVL(cp_ToDate(_read_.DPDESCRI),cp_NullValue(_read_.DPDESCRI))
          this.w_TipoRisorsa = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
          w_PAGRURIS = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
          this.w_CODUTE = NVL(cp_ToDate(_read_.DPCODUTE),cp_NullValue(_read_.DPCODUTE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ObjMPA.w_PACODRIS = this.w_CODRIS
        this.w_ObjMPA.w_CODUTE = this.w_CODUTE
        this.w_ObjMPA.w_COGNOME = NVL(this.w_COGNOME, SPACE(40))
        this.w_ObjMPA.w_NOME = NVL(this.w_NOME, SPACE(40))
        this.w_ObjMPA.w_DESCRI = NVL(this.w_DESCRI, SPACE(40))
        this.w_ObjMPA.w_DENOM = IIF(_Curs_GSAG_BCZ.PATIPRIS="P",alltrim(this.w_COGNOME)+" "+alltrim(this.w_NOME),alltrim(this.w_DESCRI))
        this.w_ObjMPA.w_PATIPRIS = NVL(_Curs_GSAG_BCZ.PATIPRIS, SPACE(1))
        this.w_ObjMPA.w_PAGRURIS = NVL(_Curs_GSAG_BCZ.PAGRURIS, SPACE(5))
        this.w_ObjMPA.w_TipoRisorsa = NVL(this.w_TipoRisorsa, SPACE(1))
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPTIPRIS"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(NVL(_Curs_GSAG_BCZ.PAGRURIS, SPACE(5)));
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPTIPRIS;
            from (i_cTable) where;
                DPCODICE = NVL(_Curs_GSAG_BCZ.PAGRURIS, SPACE(5));
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPOGRUP = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ObjMPA.w_TIPOGRUP = NVL(this.w_TIPOGRUP, SPACE(1))
        this.w_ObjMPA.SaveRow()     
      endif
        select _Curs_GSAG_BCZ
        continue
      enddo
      use
    endif
    if Not this.w_OKPAR
      this.w_ObjMPA.AddRow()     
    endif
    this.w_ObjMPA.FirstRow()     
    this.w_ObjMPA.SetRow(1)     
    this.w_ObjMPA.Refresh()     
    this.w_ObjMPA.bUpdated = .T.
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento impianti Attivit�
    this.w_ObjMCP = this.w_OBJECT.GSAG_MCP
    SELECT (this.w_ObjMCP.cTrsName)
    if reccount()<>0
      * --- Azzera Righe partecipanti
      this.w_ObjMCP.MarkPos()     
      this.w_ObjMCP.FirstRow()     
      do while ! this.w_ObjMCP.Eof_Trs()
        this.w_ObjMCP.DeleteRow()     
        this.w_ObjMCP.NextRow()     
      enddo
      * --- Se elimino tutto ricreo un record vuoto e mi ci posiziono...
      this.w_ObjMCP.AddRow()     
      this.w_ObjMCP.MarkPos()     
      this.w_ObjMCP.RePos()     
    endif
    this.w_NUMRIG = 0
    * --- Select from COM_ATTI
    i_nConn=i_TableProp[this.COM_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_ATTI_idx,2],.t.,this.COM_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CACODIMP,CACODCOM  from "+i_cTable+" COM_ATTI ";
          +" where CASERIAL = "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL_ORIG)+"";
          +" order by CPROWORD";
           ,"_Curs_COM_ATTI")
    else
      select CACODIMP,CACODCOM from (i_cTable);
       where CASERIAL = this.oParentObject.w_ATSERIAL_ORIG;
       order by CPROWORD;
        into cursor _Curs_COM_ATTI
    endif
    if used('_Curs_COM_ATTI')
      select _Curs_COM_ATTI
      locate for 1=1
      do while not(eof())
      this.w_CODIMP = NVL(_Curs_COM_ATTI.CACODIMP, SPACE(10))
      this.w_COIMPIM = NVL(_Curs_COM_ATTI.CACODCOM,0)
      * --- Nuova Riga del Temporaneo
      this.w_NUMRIG = this.w_NUMRIG+10
      this.w_ObjMCP.AddRow()     
      this.w_ObjMCP.w_CPROWORD = this.w_NUMRIG
      this.w_ObjMCP.w_CACODIMP = this.w_CODIMP
      this.w_ObjMCP.w_CACODCOM = this.w_COIMPIM
      this.w_ObjMCP.o_CACODCOM = this.w_COIMPIM
      this.w_ObjMCP.o_CACODIMP = this.w_CODIMP
      this.w_COCOM = this.w_COIMPIM
      * --- Read from IMP_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.IMP_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMP_DETT_idx,2],.t.,this.IMP_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IMDESCON"+;
          " from "+i_cTable+" IMP_DETT where ";
              +"IMCODICE = "+cp_ToStrODBC(this.w_CODIMP);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_COCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IMDESCON;
          from (i_cTable) where;
              IMCODICE = this.w_CODIMP;
              and CPROWNUM = this.w_COCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESCOM = NVL(cp_ToDate(_read_.IMDESCON),cp_NullValue(_read_.IMDESCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from IMP_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.IMP_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMP_MAST_idx,2],.t.,this.IMP_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IMDESCRI,IMCODCON,IM__SEDE,IMDATINI,IMDATFIN"+;
          " from "+i_cTable+" IMP_MAST where ";
              +"IMCODICE = "+cp_ToStrODBC(this.w_CODIMP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IMDESCRI,IMCODCON,IM__SEDE,IMDATINI,IMDATFIN;
          from (i_cTable) where;
              IMCODICE = this.w_CODIMP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESIMP = NVL(cp_ToDate(_read_.IMDESCRI),cp_NullValue(_read_.IMDESCRI))
        this.w_CLIIMP = NVL(cp_ToDate(_read_.IMCODCON),cp_NullValue(_read_.IMCODCON))
        this.w_SEDE = NVL(cp_ToDate(_read_.IM__SEDE),cp_NullValue(_read_.IM__SEDE))
        this.w_IMDATINI = NVL(cp_ToDate(_read_.IMDATINI),cp_NullValue(_read_.IMDATINI))
        this.w_IMDATFIN = NVL(cp_ToDate(_read_.IMDATFIN),cp_NullValue(_read_.IMDATFIN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ObjMCP.w_COCOM = this.w_COCOM
      this.w_ObjMCP.w_DESCOM = this.w_DESCOM
      this.w_ObjMCP.w_DESIMP = this.w_DESIMP
      this.w_ObjMCP.w_CLIIMP = this.w_CLIIMP
      this.w_ObjMCP.w_SEDE = this.w_SEDE
      this.w_ObjMCP.w_IMDATINI = this.w_IMDATINI
      this.w_ObjMCP.w_IMDATFIN = this.w_IMDATFIN
      this.w_ObjMCP.SaveRow()     
        select _Curs_COM_ATTI
        continue
      enddo
      use
    endif
    if this.w_NUMRIG=0
      this.w_ObjMCP.AddRow()     
    endif
    this.w_ObjMCP.FirstRow()     
    this.w_ObjMCP.SetRow()     
    this.w_ObjMCP.Refresh()     
    this.w_ObjMCP.bUpdated = .T.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='OFF_PART'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='COM_ATTI'
    this.cWorkTables[4]='IMP_DETT'
    this.cWorkTables[5]='IMP_MAST'
    this.cWorkTables[6]='OFF_ATTI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_GSAG_BCZ')
      use in _Curs_GSAG_BCZ
    endif
    if used('_Curs_COM_ATTI')
      use in _Curs_COM_ATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
