* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_ate                                                        *
*              Tipi eventi                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-19                                                      *
* Last revis.: 2012-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_ate"))

* --- Class definition
define class tgsfa_ate as StdForm
  Top    = 4
  Left   = 10

  * --- Standard Properties
  Width  = 648
  Height = 266+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-17"
  HelpContextID=92963433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  TIPEVENT_IDX = 0
  DRVEVENT_IDX = 0
  PROMCLAS_IDX = 0
  DIPENDEN_IDX = 0
  cFile = "TIPEVENT"
  cKeySelect = "TETIPEVE"
  cKeyWhere  = "TETIPEVE=this.w_TETIPEVE"
  cKeyWhereODBC = '"TETIPEVE="+cp_ToStrODBC(this.w_TETIPEVE)';

  cKeyWhereODBCqualified = '"TIPEVENT.TETIPEVE="+cp_ToStrODBC(this.w_TETIPEVE)';

  cPrg = "gsfa_ate"
  cComment = "Tipi eventi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TETIPEVE = space(10)
  w_TEDESCRI = space(50)
  w_TEPADEVE = space(10)
  o_TEPADEVE = space(10)
  w_TEPERSON = space(5)
  o_TEPERSON = space(5)
  w_TEGRUPPO = space(5)
  w_TE__HIDE = space(1)
  w_TEDRIVER = space(10)
  o_TEDRIVER = space(10)
  w_TETIPDIR = space(1)
  w_TECLADOC = space(15)
  w_TEFLGANT = space(1)
  w_TEFILBMP = space(100)
  w_TIPPER = space(1)
  w_TIPGRU = space(1)
  w_DPDESCRI = space(60)
  w_TPDESCRI = space(50)
  w_DEDESCRI = space(50)
  w_CDDESCLA = space(50)
  w_DPNOME = space(50)
  w_DPCOGNOM = space(50)
  w_RIFTAB = space(20)
  w_DESCRIPERS = space(60)
  w_RESCHK = 0
  w_DRIVTIPO = space(1)
  w_TIPEVPAD = space(10)

  * --- Children pointers
  GSFA_MRA = .NULL.
  w_BTNQRY = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIPEVENT','gsfa_ate')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_atePag1","gsfa_ate",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tipo evento")
      .Pages(1).HelpContextID = 235710597
      .Pages(2).addobject("oPag","tgsfa_atePag2","gsfa_ate",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Raggruppamenti attivit�")
      .Pages(2).HelpContextID = 46474039
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTETIPEVE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
      DoDefault()
    proc Destroy()
      this.w_BTNQRY = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='DRVEVENT'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='DIPENDEN'
    this.cWorkTables[4]='TIPEVENT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIPEVENT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIPEVENT_IDX,3]
  return

  function CreateChildren()
    this.GSFA_MRA = CREATEOBJECT('stdDynamicChild',this,'GSFA_MRA',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSFA_MRA.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSFA_MRA)
      this.GSFA_MRA.DestroyChildrenChain()
      this.GSFA_MRA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSFA_MRA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSFA_MRA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSFA_MRA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSFA_MRA.SetKey(;
            .w_TETIPEVE,"RETIPEVE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSFA_MRA.ChangeRow(this.cRowID+'      1',1;
             ,.w_TETIPEVE,"RETIPEVE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSFA_MRA)
        i_f=.GSFA_MRA.BuildFilter()
        if !(i_f==.GSFA_MRA.cQueryFilter)
          i_fnidx=.GSFA_MRA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSFA_MRA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSFA_MRA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSFA_MRA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSFA_MRA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TETIPEVE = NVL(TETIPEVE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TIPEVENT where TETIPEVE=KeySet.TETIPEVE
    *
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIPEVENT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIPEVENT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TIPEVENT '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TETIPEVE',this.w_TETIPEVE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPPER = 'P'
        .w_TIPGRU = space(1)
        .w_DPDESCRI = space(60)
        .w_TPDESCRI = space(50)
        .w_DEDESCRI = space(50)
        .w_CDDESCLA = space(50)
        .w_DPNOME = space(50)
        .w_DPCOGNOM = space(50)
        .w_RIFTAB = space(20)
        .w_RESCHK = 0
        .w_DRIVTIPO = space(1)
        .w_TIPEVPAD = space(10)
        .w_TETIPEVE = NVL(TETIPEVE,space(10))
        .w_TEDESCRI = NVL(TEDESCRI,space(50))
        .w_TEPADEVE = NVL(TEPADEVE,space(10))
          if link_1_4_joined
            this.w_TEPADEVE = NVL(TETIPEVE104,NVL(this.w_TEPADEVE,space(10)))
            this.w_TPDESCRI = NVL(TEDESCRI104,space(50))
            this.w_TIPEVPAD = NVL(TEPADEVE104,space(10))
          else
          .link_1_4('Load')
          endif
        .w_TEPERSON = NVL(TEPERSON,space(5))
          .link_1_5('Load')
        .w_TEGRUPPO = NVL(TEGRUPPO,space(5))
          if link_1_6_joined
            this.w_TEGRUPPO = NVL(DPCODICE106,NVL(this.w_TEGRUPPO,space(5)))
            this.w_DPDESCRI = NVL(DPDESCRI106,space(60))
            this.w_TIPGRU = NVL(DPTIPRIS106,space(1))
          else
          .link_1_6('Load')
          endif
        .w_TE__HIDE = NVL(TE__HIDE,space(1))
        .w_TEDRIVER = NVL(TEDRIVER,space(10))
          if link_1_8_joined
            this.w_TEDRIVER = NVL(DEDRIVER108,NVL(this.w_TEDRIVER,space(10)))
            this.w_DEDESCRI = NVL(DEDESCRI108,space(50))
            this.w_DRIVTIPO = NVL(DE__TIPO108,space(1))
          else
          .link_1_8('Load')
          endif
        .w_TETIPDIR = NVL(TETIPDIR,space(1))
        .w_TECLADOC = NVL(TECLADOC,space(15))
          if link_1_10_joined
            this.w_TECLADOC = NVL(CDCODCLA110,NVL(this.w_TECLADOC,space(15)))
            this.w_CDDESCLA = NVL(CDDESCLA110,space(50))
            this.w_RIFTAB = NVL(CDRIFTAB110,space(20))
          else
          .link_1_10('Load')
          endif
        .w_TEFLGANT = NVL(TEFLGANT,space(1))
        .w_TEFILBMP = NVL(TEFILBMP,space(100))
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .w_DESCRIPERS = alltrim(.w_DPCOGNOM)+space(1)+alltrim(.w_DPNOME)
        cp_LoadRecExtFlds(this,'TIPEVENT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TETIPEVE = space(10)
      .w_TEDESCRI = space(50)
      .w_TEPADEVE = space(10)
      .w_TEPERSON = space(5)
      .w_TEGRUPPO = space(5)
      .w_TE__HIDE = space(1)
      .w_TEDRIVER = space(10)
      .w_TETIPDIR = space(1)
      .w_TECLADOC = space(15)
      .w_TEFLGANT = space(1)
      .w_TEFILBMP = space(100)
      .w_TIPPER = space(1)
      .w_TIPGRU = space(1)
      .w_DPDESCRI = space(60)
      .w_TPDESCRI = space(50)
      .w_DEDESCRI = space(50)
      .w_CDDESCLA = space(50)
      .w_DPNOME = space(50)
      .w_DPCOGNOM = space(50)
      .w_RIFTAB = space(20)
      .w_DESCRIPERS = space(60)
      .w_RESCHK = 0
      .w_DRIVTIPO = space(1)
      .w_TIPEVPAD = space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_TEPADEVE))
          .link_1_4('Full')
          endif
        .w_TEPERSON = IIF(EMPTY(.w_TEPADEVE),.w_TEPERSON,' ')
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_TEPERSON))
          .link_1_5('Full')
          endif
        .w_TEGRUPPO = IIF(EMPTY(.w_TEPADEVE),.w_TEGRUPPO,' ')
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_TEGRUPPO))
          .link_1_6('Full')
          endif
        .w_TE__HIDE = IIF(EMPTY(.w_TEPADEVE),'N','N')
        .w_TEDRIVER = IIF(NOT EMPTY(.w_TEPADEVE),.w_TEDRIVER,' ')
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_TEDRIVER))
          .link_1_8('Full')
          endif
        .w_TETIPDIR = 'E'
        .w_TECLADOC = IIF(EMPTY(.w_TEDRIVER),.w_TECLADOC,' ')
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_TECLADOC))
          .link_1_10('Full')
          endif
        .w_TEFLGANT = IIF(EMPTY(.w_TEDRIVER),'N','N')
          .DoRTCalc(11,11,.f.)
        .w_TIPPER = 'P'
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
          .DoRTCalc(13,20,.f.)
        .w_DESCRIPERS = alltrim(.w_DPCOGNOM)+space(1)+alltrim(.w_DPNOME)
        .w_RESCHK = 0
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIPEVENT')
    this.DoRTCalc(23,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTETIPEVE_1_1.enabled = i_bVal
      .Page1.oPag.oTEDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oTEPADEVE_1_4.enabled = i_bVal
      .Page1.oPag.oTEPERSON_1_5.enabled = i_bVal
      .Page1.oPag.oTEGRUPPO_1_6.enabled = i_bVal
      .Page1.oPag.oTE__HIDE_1_7.enabled = i_bVal
      .Page1.oPag.oTEDRIVER_1_8.enabled = i_bVal
      .Page1.oPag.oTETIPDIR_1_9.enabled = i_bVal
      .Page1.oPag.oTECLADOC_1_10.enabled = i_bVal
      .Page1.oPag.oTEFLGANT_1_11.enabled = i_bVal
      .Page1.oPag.oTEFILBMP_1_12.enabled = i_bVal
      .Page1.oPag.BTNQRY.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTETIPEVE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTETIPEVE_1_1.enabled = .t.
        .Page1.oPag.oTEDESCRI_1_3.enabled = .t.
      endif
    endwith
    this.GSFA_MRA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'TIPEVENT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSFA_MRA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TETIPEVE,"TETIPEVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TEDESCRI,"TEDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TEPADEVE,"TEPADEVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TEPERSON,"TEPERSON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TEGRUPPO,"TEGRUPPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TE__HIDE,"TE__HIDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TEDRIVER,"TEDRIVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TETIPDIR,"TETIPDIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TECLADOC,"TECLADOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TEFLGANT,"TEFLGANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TEFILBMP,"TEFILBMP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    i_lTable = "TIPEVENT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIPEVENT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TIPEVENT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIPEVENT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIPEVENT')
        i_extval=cp_InsertValODBCExtFlds(this,'TIPEVENT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TETIPEVE,TEDESCRI,TEPADEVE,TEPERSON,TEGRUPPO"+;
                  ",TE__HIDE,TEDRIVER,TETIPDIR,TECLADOC,TEFLGANT"+;
                  ",TEFILBMP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TETIPEVE)+;
                  ","+cp_ToStrODBC(this.w_TEDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_TEPADEVE)+;
                  ","+cp_ToStrODBCNull(this.w_TEPERSON)+;
                  ","+cp_ToStrODBCNull(this.w_TEGRUPPO)+;
                  ","+cp_ToStrODBC(this.w_TE__HIDE)+;
                  ","+cp_ToStrODBCNull(this.w_TEDRIVER)+;
                  ","+cp_ToStrODBC(this.w_TETIPDIR)+;
                  ","+cp_ToStrODBCNull(this.w_TECLADOC)+;
                  ","+cp_ToStrODBC(this.w_TEFLGANT)+;
                  ","+cp_ToStrODBC(this.w_TEFILBMP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIPEVENT')
        i_extval=cp_InsertValVFPExtFlds(this,'TIPEVENT')
        cp_CheckDeletedKey(i_cTable,0,'TETIPEVE',this.w_TETIPEVE)
        INSERT INTO (i_cTable);
              (TETIPEVE,TEDESCRI,TEPADEVE,TEPERSON,TEGRUPPO,TE__HIDE,TEDRIVER,TETIPDIR,TECLADOC,TEFLGANT,TEFILBMP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TETIPEVE;
                  ,this.w_TEDESCRI;
                  ,this.w_TEPADEVE;
                  ,this.w_TEPERSON;
                  ,this.w_TEGRUPPO;
                  ,this.w_TE__HIDE;
                  ,this.w_TEDRIVER;
                  ,this.w_TETIPDIR;
                  ,this.w_TECLADOC;
                  ,this.w_TEFLGANT;
                  ,this.w_TEFILBMP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TIPEVENT_IDX,i_nConn)
      *
      * update TIPEVENT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TIPEVENT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TEDESCRI="+cp_ToStrODBC(this.w_TEDESCRI)+;
             ",TEPADEVE="+cp_ToStrODBCNull(this.w_TEPADEVE)+;
             ",TEPERSON="+cp_ToStrODBCNull(this.w_TEPERSON)+;
             ",TEGRUPPO="+cp_ToStrODBCNull(this.w_TEGRUPPO)+;
             ",TE__HIDE="+cp_ToStrODBC(this.w_TE__HIDE)+;
             ",TEDRIVER="+cp_ToStrODBCNull(this.w_TEDRIVER)+;
             ",TETIPDIR="+cp_ToStrODBC(this.w_TETIPDIR)+;
             ",TECLADOC="+cp_ToStrODBCNull(this.w_TECLADOC)+;
             ",TEFLGANT="+cp_ToStrODBC(this.w_TEFLGANT)+;
             ",TEFILBMP="+cp_ToStrODBC(this.w_TEFILBMP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TIPEVENT')
        i_cWhere = cp_PKFox(i_cTable  ,'TETIPEVE',this.w_TETIPEVE  )
        UPDATE (i_cTable) SET;
              TEDESCRI=this.w_TEDESCRI;
             ,TEPADEVE=this.w_TEPADEVE;
             ,TEPERSON=this.w_TEPERSON;
             ,TEGRUPPO=this.w_TEGRUPPO;
             ,TE__HIDE=this.w_TE__HIDE;
             ,TEDRIVER=this.w_TEDRIVER;
             ,TETIPDIR=this.w_TETIPDIR;
             ,TECLADOC=this.w_TECLADOC;
             ,TEFLGANT=this.w_TEFLGANT;
             ,TEFILBMP=this.w_TEFILBMP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSFA_MRA : Saving
      this.GSFA_MRA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_TETIPEVE,"RETIPEVE";
             )
      this.GSFA_MRA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSFA_MRA : Deleting
    this.GSFA_MRA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_TETIPEVE,"RETIPEVE";
           )
    this.GSFA_MRA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TIPEVENT_IDX,i_nConn)
      *
      * delete TIPEVENT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TETIPEVE',this.w_TETIPEVE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_TEPADEVE<>.w_TEPADEVE
            .w_TEPERSON = IIF(EMPTY(.w_TEPADEVE),.w_TEPERSON,' ')
          .link_1_5('Full')
        endif
        if .o_TEPADEVE<>.w_TEPADEVE
            .w_TEGRUPPO = IIF(EMPTY(.w_TEPADEVE),.w_TEGRUPPO,' ')
          .link_1_6('Full')
        endif
        if .o_TEPADEVE<>.w_TEPADEVE
            .w_TE__HIDE = IIF(EMPTY(.w_TEPADEVE),'N','N')
        endif
        if .o_TEPADEVE<>.w_TEPADEVE
            .w_TEDRIVER = IIF(NOT EMPTY(.w_TEPADEVE),.w_TEDRIVER,' ')
          .link_1_8('Full')
        endif
        .DoRTCalc(8,8,.t.)
        if .o_TEDRIVER<>.w_TEDRIVER
            .w_TECLADOC = IIF(EMPTY(.w_TEDRIVER),.w_TECLADOC,' ')
          .link_1_10('Full')
        endif
        if .o_TEDRIVER<>.w_TEDRIVER
            .w_TEFLGANT = IIF(EMPTY(.w_TEDRIVER),'N','N')
        endif
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .DoRTCalc(11,20,.t.)
        if .o_TEPERSON<>.w_TEPERSON
            .w_DESCRIPERS = alltrim(.w_DPCOGNOM)+space(1)+alltrim(.w_DPNOME)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
    endwith
  return

  proc Calculate_WMFTZGPBJG()
    with this
          * --- Calcoli e controlli a checkform
          GSFA_BCT(this;
              ,'CHK';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTEPERSON_1_5.enabled = this.oPgFrm.Page1.oPag.oTEPERSON_1_5.mCond()
    this.oPgFrm.Page1.oPag.oTEGRUPPO_1_6.enabled = this.oPgFrm.Page1.oPag.oTEGRUPPO_1_6.mCond()
    this.oPgFrm.Page1.oPag.oTE__HIDE_1_7.enabled = this.oPgFrm.Page1.oPag.oTE__HIDE_1_7.mCond()
    this.oPgFrm.Page1.oPag.oTEDRIVER_1_8.enabled = this.oPgFrm.Page1.oPag.oTEDRIVER_1_8.mCond()
    this.oPgFrm.Page1.oPag.oTECLADOC_1_10.enabled = this.oPgFrm.Page1.oPag.oTECLADOC_1_10.mCond()
    this.oPgFrm.Page1.oPag.oTEFLGANT_1_11.enabled = this.oPgFrm.Page1.oPag.oTEFLGANT_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTETIPDIR_1_9.visible=!this.oPgFrm.Page1.oPag.oTETIPDIR_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_WMFTZGPBJG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TEPADEVE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TEPADEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ATE',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_TEPADEVE)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEPADEVE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_TEPADEVE))
          select TETIPEVE,TEDESCRI,TEPADEVE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TEPADEVE)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStrODBC(trim(this.w_TEPADEVE)+"%");

            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEPADEVE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStr(trim(this.w_TEPADEVE)+"%");

            select TETIPEVE,TEDESCRI,TEPADEVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TEPADEVE) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oTEPADEVE_1_4'),i_cWhere,'GSFA_ATE',"Tipi eventi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEPADEVE";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI,TEPADEVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TEPADEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEPADEVE";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_TEPADEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_TEPADEVE)
            select TETIPEVE,TEDESCRI,TEPADEVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TEPADEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_TPDESCRI = NVL(_Link_.TEDESCRI,space(50))
      this.w_TIPEVPAD = NVL(_Link_.TEPADEVE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_TEPADEVE = space(10)
      endif
      this.w_TPDESCRI = space(50)
      this.w_TIPEVPAD = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TEPADEVE<>.w_TETIPEVE
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo eventi non ammesso")
        endif
        this.w_TEPADEVE = space(10)
        this.w_TPDESCRI = space(50)
        this.w_TIPEVPAD = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TEPADEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPEVENT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.TETIPEVE as TETIPEVE104"+ ",link_1_4.TEDESCRI as TEDESCRI104"+ ",link_1_4.TEPADEVE as TEPADEVE104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on TIPEVENT.TEPADEVE=link_1_4.TETIPEVE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and TIPEVENT.TEPADEVE=link_1_4.TETIPEVE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TEPERSON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TEPERSON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_TEPERSON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPPER;
                     ,'DPCODICE',trim(this.w_TEPERSON))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TEPERSON)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TEPERSON) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oTEPERSON_1_5'),i_cWhere,'GSAR_BDZ',"Persone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPPER<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TEPERSON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_TEPERSON);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPPER;
                       ,'DPCODICE',this.w_TEPERSON)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TEPERSON = NVL(_Link_.DPCODICE,space(5))
      this.w_DPCOGNOM = NVL(_Link_.DPCOGNOM,space(50))
      this.w_DPNOME = NVL(_Link_.DPNOME,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_TEPERSON = space(5)
      endif
      this.w_DPCOGNOM = space(50)
      this.w_DPNOME = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TEPERSON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TEGRUPPO
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TEGRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_TEGRUPPO)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_TEGRUPPO))
          select DPCODICE,DPDESCRI,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TEGRUPPO)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_TEGRUPPO)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_TEGRUPPO)+"%");

            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TEGRUPPO) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oTEGRUPPO_1_6'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSARGADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TEGRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_TEGRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_TEGRUPPO)
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TEGRUPPO = NVL(_Link_.DPCODICE,space(5))
      this.w_DPDESCRI = NVL(_Link_.DPDESCRI,space(60))
      this.w_TIPGRU = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TEGRUPPO = space(5)
      endif
      this.w_DPDESCRI = space(60)
      this.w_TIPGRU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_TEPERSON , .w_TEGRUPPO) OR Empty(.w_TEPERSON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
        endif
        this.w_TEGRUPPO = space(5)
        this.w_DPDESCRI = space(60)
        this.w_TIPGRU = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TEGRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.DPCODICE as DPCODICE106"+ ",link_1_6.DPDESCRI as DPDESCRI106"+ ",link_1_6.DPTIPRIS as DPTIPRIS106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on TIPEVENT.TEGRUPPO=link_1_6.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and TIPEVENT.TEGRUPPO=link_1_6.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TEDRIVER
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_lTable = "DRVEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2], .t., this.DRVEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TEDRIVER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ADE',True,'DRVEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DEDRIVER like "+cp_ToStrODBC(trim(this.w_TEDRIVER)+"%");

          i_ret=cp_SQL(i_nConn,"select DEDRIVER,DEDESCRI,DE__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DEDRIVER","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DEDRIVER',trim(this.w_TEDRIVER))
          select DEDRIVER,DEDESCRI,DE__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DEDRIVER into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TEDRIVER)==trim(_Link_.DEDRIVER) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TEDRIVER) and !this.bDontReportError
            deferred_cp_zoom('DRVEVENT','*','DEDRIVER',cp_AbsName(oSource.parent,'oTEDRIVER_1_8'),i_cWhere,'GSFA_ADE',"Driver eventi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DEDESCRI,DE__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',oSource.xKey(1))
            select DEDRIVER,DEDESCRI,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TEDRIVER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DEDESCRI,DE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(this.w_TEDRIVER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',this.w_TEDRIVER)
            select DEDRIVER,DEDESCRI,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TEDRIVER = NVL(_Link_.DEDRIVER,space(10))
      this.w_DEDESCRI = NVL(_Link_.DEDESCRI,space(50))
      this.w_DRIVTIPO = NVL(_Link_.DE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TEDRIVER = space(10)
      endif
      this.w_DEDESCRI = space(50)
      this.w_DRIVTIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])+'\'+cp_ToStr(_Link_.DEDRIVER,1)
      cp_ShowWarn(i_cKey,this.DRVEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TEDRIVER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DRVEVENT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.DEDRIVER as DEDRIVER108"+ ",link_1_8.DEDESCRI as DEDESCRI108"+ ",link_1_8.DE__TIPO as DE__TIPO108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on TIPEVENT.TEDRIVER=link_1_8.DEDRIVER"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and TIPEVENT.TEDRIVER=link_1_8.DEDRIVER(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TECLADOC
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TECLADOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_TECLADOC)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_TECLADOC))
          select CDCODCLA,CDDESCLA,CDRIFTAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TECLADOC)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TECLADOC) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oTECLADOC_1_10'),i_cWhere,'GSUT_MCD',"Classi documentali",'GSFA1ATE.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDRIFTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TECLADOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_TECLADOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_TECLADOC)
            select CDCODCLA,CDDESCLA,CDRIFTAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TECLADOC = NVL(_Link_.CDCODCLA,space(15))
      this.w_CDDESCLA = NVL(_Link_.CDDESCLA,space(50))
      this.w_RIFTAB = NVL(_Link_.CDRIFTAB,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_TECLADOC = space(15)
      endif
      this.w_CDDESCLA = space(50)
      this.w_RIFTAB = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Upper(.w_RIFTAB)='ANEVENTI'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, classe documentale non riferita agli eventi ")
        endif
        this.w_TECLADOC = space(15)
        this.w_CDDESCLA = space(50)
        this.w_RIFTAB = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TECLADOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PROMCLAS_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.CDCODCLA as CDCODCLA110"+ ",link_1_10.CDDESCLA as CDDESCLA110"+ ",link_1_10.CDRIFTAB as CDRIFTAB110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on TIPEVENT.TECLADOC=link_1_10.CDCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and TIPEVENT.TECLADOC=link_1_10.CDCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTETIPEVE_1_1.value==this.w_TETIPEVE)
      this.oPgFrm.Page1.oPag.oTETIPEVE_1_1.value=this.w_TETIPEVE
    endif
    if not(this.oPgFrm.Page1.oPag.oTEDESCRI_1_3.value==this.w_TEDESCRI)
      this.oPgFrm.Page1.oPag.oTEDESCRI_1_3.value=this.w_TEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTEPADEVE_1_4.value==this.w_TEPADEVE)
      this.oPgFrm.Page1.oPag.oTEPADEVE_1_4.value=this.w_TEPADEVE
    endif
    if not(this.oPgFrm.Page1.oPag.oTEPERSON_1_5.value==this.w_TEPERSON)
      this.oPgFrm.Page1.oPag.oTEPERSON_1_5.value=this.w_TEPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oTEGRUPPO_1_6.value==this.w_TEGRUPPO)
      this.oPgFrm.Page1.oPag.oTEGRUPPO_1_6.value=this.w_TEGRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oTE__HIDE_1_7.RadioValue()==this.w_TE__HIDE)
      this.oPgFrm.Page1.oPag.oTE__HIDE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTEDRIVER_1_8.value==this.w_TEDRIVER)
      this.oPgFrm.Page1.oPag.oTEDRIVER_1_8.value=this.w_TEDRIVER
    endif
    if not(this.oPgFrm.Page1.oPag.oTETIPDIR_1_9.RadioValue()==this.w_TETIPDIR)
      this.oPgFrm.Page1.oPag.oTETIPDIR_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTECLADOC_1_10.value==this.w_TECLADOC)
      this.oPgFrm.Page1.oPag.oTECLADOC_1_10.value=this.w_TECLADOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTEFLGANT_1_11.RadioValue()==this.w_TEFLGANT)
      this.oPgFrm.Page1.oPag.oTEFLGANT_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTEFILBMP_1_12.value==this.w_TEFILBMP)
      this.oPgFrm.Page1.oPag.oTEFILBMP_1_12.value=this.w_TEFILBMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESCRI_1_15.value==this.w_DPDESCRI)
      this.oPgFrm.Page1.oPag.oDPDESCRI_1_15.value=this.w_DPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTPDESCRI_1_16.value==this.w_TPDESCRI)
      this.oPgFrm.Page1.oPag.oTPDESCRI_1_16.value=this.w_TPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDESCRI_1_17.value==this.w_DEDESCRI)
      this.oPgFrm.Page1.oPag.oDEDESCRI_1_17.value=this.w_DEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCDDESCLA_1_18.value==this.w_CDDESCLA)
      this.oPgFrm.Page1.oPag.oCDDESCLA_1_18.value=this.w_CDDESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIPERS_1_30.value==this.w_DESCRIPERS)
      this.oPgFrm.Page1.oPag.oDESCRIPERS_1_30.value=this.w_DESCRIPERS
    endif
    cp_SetControlsValueExtFlds(this,'TIPEVENT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TETIPEVE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTETIPEVE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TETIPEVE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TEPADEVE<>.w_TETIPEVE)  and not(empty(.w_TEPADEVE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTEPADEVE_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo eventi non ammesso")
          case   not(.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_TEPERSON , .w_TEGRUPPO) OR Empty(.w_TEPERSON)))  and (EMPTY(.w_TEPADEVE))  and not(empty(.w_TEGRUPPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTEGRUPPO_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
          case   (empty(.w_TEDRIVER))  and (NOT EMPTY(.w_TEPADEVE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTEDRIVER_1_8.SetFocus()
            i_bnoObbl = !empty(.w_TEDRIVER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Upper(.w_RIFTAB)='ANEVENTI')  and (NOT EMPTY(.w_TEPADEVE) AND .w_DRIVTIPO='M')  and not(empty(.w_TECLADOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTECLADOC_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, classe documentale non riferita agli eventi ")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSFA_MRA.CheckForm()
      if i_bres
        i_bres=  .GSFA_MRA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsfa_ate
      * --- Controlli Finali
      IF i_bRes
         .w_RESCHK=0
         Ah_Msg('Controlli finali...',.T.)
         .NotifyEvent('ChkFinali')
         WAIT CLEAR
         IF .w_RESCHK<>0
           i_bRes=.f.
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TEPADEVE = this.w_TEPADEVE
    this.o_TEPERSON = this.w_TEPERSON
    this.o_TEDRIVER = this.w_TEDRIVER
    * --- GSFA_MRA : Depends On
    this.GSFA_MRA.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsfa_atePag1 as StdContainer
  Width  = 646
  height = 266
  stdWidth  = 646
  stdheight = 266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTETIPEVE_1_1 as StdField with uid="WDIVNVBGFI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TETIPEVE", cQueryName = "TETIPEVE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipo evento",;
    HelpContextID = 79956091,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=15, InputMask=replicate('X',10)

  add object oTEDESCRI_1_3 as StdField with uid="JFDHDYBTCU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TEDESCRI", cQueryName = "TEDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo evento",;
    HelpContextID = 49219711,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=271, Top=15, InputMask=replicate('X',50)

  add object oTEPADEVE_1_4 as StdField with uid="BWWEENMVZH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TEPADEVE", cQueryName = "TEPADEVE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo eventi non ammesso",;
    ToolTipText = "Tipo evento padre",;
    HelpContextID = 66832507,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=40, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_TEPADEVE"

  func oTEPADEVE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTEPADEVE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTEPADEVE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oTEPADEVE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ATE',"Tipi eventi",'',this.parent.oContained
  endproc
  proc oTEPADEVE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ATE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_TEPADEVE
     i_obj.ecpSave()
  endproc

  add object oTEPERSON_1_5 as StdField with uid="NLNODBSOTR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TEPERSON", cQueryName = "TEPERSON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona",;
    HelpContextID = 48220292,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=65, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPPER", oKey_2_1="DPCODICE", oKey_2_2="this.w_TEPERSON"

  func oTEPERSON_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_TEPADEVE))
    endwith
   endif
  endfunc

  func oTEPERSON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oTEPERSON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTEPERSON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPPER)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPPER)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oTEPERSON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'',this.parent.oContained
  endproc
  proc oTEPERSON_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TIPPER
     i_obj.w_DPCODICE=this.parent.oContained.w_TEPERSON
     i_obj.ecpSave()
  endproc

  add object oTEGRUPPO_1_6 as StdField with uid="ZQYQKSLESU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TEGRUPPO", cQueryName = "TEGRUPPO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, persona non appartenente al gruppo selezionato",;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 1849477,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=90, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_TEGRUPPO"

  func oTEGRUPPO_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_TEPADEVE))
    endwith
   endif
  endfunc

  func oTEGRUPPO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oTEGRUPPO_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTEGRUPPO_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oTEGRUPPO_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSARGADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oTEGRUPPO_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_TEGRUPPO
     i_obj.ecpSave()
  endproc

  add object oTE__HIDE_1_7 as StdCheck with uid="INSAIVLGHA",rtseq=6,rtrep=.f.,left=135, top=114, caption="Nascondi",;
    ToolTipText = "Se attivo tipo evento nascosto nel cruscotto",;
    HelpContextID = 140163195,;
    cFormVar="w_TE__HIDE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTE__HIDE_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTE__HIDE_1_7.GetRadio()
    this.Parent.oContained.w_TE__HIDE = this.RadioValue()
    return .t.
  endfunc

  func oTE__HIDE_1_7.SetRadio()
    this.Parent.oContained.w_TE__HIDE=trim(this.Parent.oContained.w_TE__HIDE)
    this.value = ;
      iif(this.Parent.oContained.w_TE__HIDE=='S',1,;
      0)
  endfunc

  func oTE__HIDE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_TEPADEVE))
    endwith
   endif
  endfunc

  add object oTEDRIVER_1_8 as StdField with uid="TDBROAXOFN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TEDRIVER", cQueryName = "TEDRIVER",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice driver",;
    HelpContextID = 89917576,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=139, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="DRVEVENT", cZoomOnZoom="GSFA_ADE", oKey_1_1="DEDRIVER", oKey_1_2="this.w_TEDRIVER"

  func oTEDRIVER_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TEPADEVE))
    endwith
   endif
  endfunc

  func oTEDRIVER_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTEDRIVER_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTEDRIVER_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DRVEVENT','*','DEDRIVER',cp_AbsName(this.parent,'oTEDRIVER_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ADE',"Driver eventi",'',this.parent.oContained
  endproc
  proc oTEDRIVER_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ADE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DEDRIVER=this.parent.oContained.w_TEDRIVER
     i_obj.ecpSave()
  endproc


  add object oTETIPDIR_1_9 as StdCombo with uid="FYCWOBRSDC",rtseq=8,rtrep=.f.,left=135,top=166,width=109,height=21;
    , ToolTipText = "Direziione evento";
    , HelpContextID = 205256568;
    , cFormVar="w_TETIPDIR",RowSource=""+"Entrata,"+"Uscita", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTETIPDIR_1_9.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'U',;
    space(1))))
  endfunc
  func oTETIPDIR_1_9.GetRadio()
    this.Parent.oContained.w_TETIPDIR = this.RadioValue()
    return .t.
  endfunc

  func oTETIPDIR_1_9.SetRadio()
    this.Parent.oContained.w_TETIPDIR=trim(this.Parent.oContained.w_TETIPDIR)
    this.value = ;
      iif(this.Parent.oContained.w_TETIPDIR=='E',1,;
      iif(this.Parent.oContained.w_TETIPDIR=='U',2,;
      0))
  endfunc

  func oTETIPDIR_1_9.mHide()
    with this.Parent.oContained
      return (Empty(.w_TEDRIVER))
    endwith
  endfunc

  add object oTECLADOC_1_10 as StdField with uid="CKSMDOGIMY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_TECLADOC", cQueryName = "TECLADOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, classe documentale non riferita agli eventi ",;
    ToolTipText = "Classe documentale di riferimento",;
    HelpContextID = 47577209,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=191, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_TECLADOC"

  func oTECLADOC_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TEPADEVE) AND .w_DRIVTIPO='M')
    endwith
   endif
  endfunc

  func oTECLADOC_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oTECLADOC_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTECLADOC_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oTECLADOC_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'GSFA1ATE.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oTECLADOC_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_TECLADOC
     i_obj.ecpSave()
  endproc

  add object oTEFLGANT_1_11 as StdCheck with uid="NJCDTMDJAQ",rtseq=10,rtrep=.f.,left=135, top=215, caption="Attiva anteprima",;
    ToolTipText = "Attiva anteprima",;
    HelpContextID = 264886134,;
    cFormVar="w_TEFLGANT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTEFLGANT_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTEFLGANT_1_11.GetRadio()
    this.Parent.oContained.w_TEFLGANT = this.RadioValue()
    return .t.
  endfunc

  func oTEFLGANT_1_11.SetRadio()
    this.Parent.oContained.w_TEFLGANT=trim(this.Parent.oContained.w_TEFLGANT)
    this.value = ;
      iif(this.Parent.oContained.w_TEFLGANT=='S',1,;
      0)
  endfunc

  func oTEFLGANT_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_TEPADEVE) AND .w_DRIVTIPO='M')
    endwith
   endif
  endfunc

  add object oTEFILBMP_1_12 as StdField with uid="WTBNWVIQGH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_TEFILBMP", cQueryName = "TEFILBMP",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "File bmp da utilizzare nella treeview",;
    HelpContextID = 243062650,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=134, Top=241, InputMask=replicate('X',100)

  add object oDPDESCRI_1_15 as StdField with uid="GRDVQEAQZX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DPDESCRI", cQueryName = "DPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 49222271,;
   bGlobalFont=.t.,;
    Height=21, Width=436, Left=199, Top=90, InputMask=replicate('X',60)

  add object oTPDESCRI_1_16 as StdField with uid="PEAJTDTCVU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TPDESCRI", cQueryName = "TPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo evento padre",;
    HelpContextID = 49222527,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=271, Top=40, InputMask=replicate('X',50)

  add object oDEDESCRI_1_17 as StdField with uid="VUQSDGCHKQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DEDESCRI", cQueryName = "DEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione driver",;
    HelpContextID = 49219455,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=272, Top=139, InputMask=replicate('X',50)

  add object oCDDESCLA_1_18 as StdField with uid="MSROZBMNLQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CDDESCLA", cQueryName = "CDDESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe documentale",;
    HelpContextID = 219216281,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=272, Top=191, InputMask=replicate('X',50)


  add object BTNQRY as cp_askfile with uid="SVNANQOYOT",left=500, top=245, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_TEFILBMP",cEXT="ICO",;
    nPag=1;
    , ToolTipText = "Premere per selezionare la query";
    , HelpContextID = 92762410

  add object oDESCRIPERS_1_30 as StdField with uid="NOEMIXGPFR",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCRIPERS", cQueryName = "DESCRIPERS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 148787099,;
   bGlobalFont=.t.,;
    Height=21, Width=436, Left=199, Top=65, InputMask=replicate('X',60)

  add object oStr_1_2 as StdString with uid="RRGSMQOJMZ",Visible=.t., Left=64, Top=15,;
    Alignment=1, Width=66, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ERSUEODXFX",Visible=.t., Left=29, Top=40,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tipo evento padre:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="SCCVUVYESS",Visible=.t., Left=46, Top=90,;
    Alignment=1, Width=84, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ABEHCTUHPO",Visible=.t., Left=39, Top=65,;
    Alignment=1, Width=91, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="DWMRFWSDGQ",Visible=.t., Left=8, Top=191,;
    Alignment=1, Width=122, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="GYGDKNWOJD",Visible=.t., Left=55, Top=139,;
    Alignment=1, Width=75, Height=18,;
    Caption="Driver:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="OTINLIPVRJ",Visible=.t., Left=88, Top=241,;
    Alignment=1, Width=42, Height=18,;
    Caption="Bitmap:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="PVSXBVZWYM",Visible=.t., Left=49, Top=164,;
    Alignment=1, Width=81, Height=18,;
    Caption="Direzione:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (Empty(.w_TEDRIVER))
    endwith
  endfunc
enddefine
define class tgsfa_atePag2 as StdContainer
  Width  = 646
  height = 266
  stdWidth  = 646
  stdheight = 266
  resizeYpos=226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="SFOBMYQJLZ",left=0, top=1, width=646, height=258, bOnScreen=.t.;

enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_ate','TIPEVENT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TETIPEVE=TIPEVENT.TETIPEVE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
