* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_btr                                                        *
*              Tracciabilita di riga                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-04-05                                                      *
* Last revis.: 2013-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_btr",oParentObject)
return(i_retval)

define class tgsag_btr as StdBatch
  * --- Local variables
  w_Zoom = space(10)
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_NUMREC = 0
  w_DATREG = ctot("")
  w_TIPDOC = space(5)
  w_DESDOC = space(35)
  w_NUMDOC = 0
  w_ALFDOC = space(2)
  w_DATDOC = ctot("")
  w_ROWORD = 0
  w_CODRES = space(5)
  w_CODICE = space(20)
  w_DESART = space(40)
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_PREZZO = 0
  w_VALRIG = 0
  w_CURSERIAL = space(10)
  w_CURROWNUM = 0
  w_NUMRIF = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene lanciato dall'Attività (Evento Tracciab)
    * --- Zoom Tracciabilità di riga
    * --- w_SERTRA e w_RIGA sono le chiavi primarie della riga per la quale si desidera calcolare la tracciabilità.
    * --- w_SERIAL e w_ROWNUM sono i parametri che devono essere passati alla query.
    * --- Variabile per salvataggio posizione record corrente
    this.w_Zoom = this.oParentObject.w_ZoomTrRig
    ND = this.oParentObject.w_ZoomTrRig.cCursor
    SELECT (ND)
    GO TOP
    * --- Sbiancamento cursore dello Zoom Tracciabilità di riga
    DELETE FROM &ND
    * --- Creazione cursore DAELABOR che conterrà l'elenco delle righe per le quali deve essere calcolata la tracciabilità.
    CREATE CURSOR DAELABOR (SERIALE C (10), RIGA N (4,0))
    * --- All'inizio il cursore conterrà un'unica riga (quella selezionata nello zoom).
    INSERT INTO DAELABOR (SERIALE, RIGA) VALUES (this.oParentObject.w_SERTRA, this.oParentObject.w_RIGA)
    if RECCOUNT("DAELABOR")<>0
      SELECT DAELABOR
      GO TOP
      SCAN
      * --- Salvataggio posizione record corrente
      this.w_NUMREC = RECNO()
      * --- Parametri che devono essere passati alla query.
      this.w_SERIAL = SERIALE
      this.w_ROWNUM = RIGA
      * --- Il cursore APPO contiene il risultato della query che calcola la tracciabilità (scendendo di un solo livello) della riga selezionata.
      if !Isalt()
        vq_exec("query\GSAG_TR1",this,"APPO")
      else
        vq_exec("..\AGEN\EXE\QUERY\GSAGZTRR",this,"APPO")
      endif
      if USED("APPO")
        if RECCOUNT("APPO")<>0
          SELECT APPO
          GO TOP
          SCAN
          this.w_DATREG = MVDATREG
          this.w_TIPDOC = MVTIPDOC
          this.w_DESDOC = TDDESDOC
          this.w_NUMDOC = MVNUMDOC
          this.w_ALFDOC = MVALFDOC
          this.w_DATDOC = MVDATDOC
          this.w_ROWORD = CPROWORD
          this.w_CODRES = MVCODRES
          this.w_CODICE = MVCODICE
          this.w_DESART = MVDESART
          this.w_UNIMIS = MVUNIMIS
          this.w_QTAMOV = MVQTAMOV
          this.w_PREZZO = MVPREZZO
          this.w_VALRIG = MVVALRIG
          this.w_CURSERIAL = MVSERIAL
          this.w_CURROWNUM = CPROWNUM
          this.w_NUMRIF = MVNUMRIF
          * --- Le righe del cursore APPO vengono inserite nel cursore dello Zoom Tracciabilità di riga (ND)
          INSERT INTO (ND) ;
          (MVDATREG, MVTIPDOC, TDDESDOC, MVNUMDOC, MVALFDOC, MVDATDOC, CPROWORD, MVCODRES, MVCODICE, ;
          MVDESART, MVUNIMIS, MVQTAMOV, MVPREZZO, MVVALRIG, MVSERIAL, CPROWNUM, MVNUMRIF) VALUES ;
          (this.w_DATREG, this.w_TIPDOC, this.w_DESDOC, this.w_NUMDOC, this.w_ALFDOC, this.w_DATDOC, this.w_ROWORD, this.w_CODRES, this.w_CODICE, ;
          this.w_DESART, this.w_UNIMIS, this.w_QTAMOV, this.w_PREZZO, this.w_VALRIG, this.w_CURSERIAL, this.w_CURROWNUM, this.w_NUMRIF)
          * --- w_CURSERIAL e w_CURROWNUM vengono inserite nel cursore DAELABOR;
          *     per tale righe dovrà essere calcolata la tracciabilità.
          INSERT INTO DAELABOR (SERIALE, RIGA) VALUES (this.w_CURSERIAL, this.w_CURROWNUM)
          * --- Riposizionamento sul record corrente
          SELECT DAELABOR
          GO this.w_NUMREC
          ENDSCAN
        endif
      endif
      * --- Chiusura cursore
      if used("APPO")
        select APPO
        use
      endif
      ENDSCAN
    endif
    * --- Chiusura cursore
    if used("DAELABOR")
      select DAELABOR
      use
    endif
    SELECT ( ND)
    GO TOP
    this.w_Zoom = this.oParentObject.w_ZoomTrRig.Refresh()
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
