* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bck                                                        *
*              Controlli in prestazioni rapide                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-24                                                      *
* Last revis.: 2014-10-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bck",oParentObject,m.pParam)
return(i_retval)

define class tgsag_bck as StdBatch
  * --- Local variables
  pParam = space(1)
  w_ObjMPR = .NULL.
  w_ObjMPR = .NULL.
  w_Mask = .NULL.
  w_CursorZoomMemo = space(1)
  w_CursorZoomAtt = space(1)
  w_OBJECT = .NULL.
  w_CODRES = space(15)
  w_GRURES = space(5)
  w_MAXROW = 0
  w_SETLINKED = .f.
  w_CODLIS = space(5)
  w_OBJPRAT = .NULL.
  w_Rsp = space(5)
  w_Grp = space(5)
  w_ObjMPR = .NULL.
  w_ValoreSelez = space(1)
  w_MaxRow = 0
  w_RIFPRE = 0
  w_ROWNUM = 0
  w_RIGPRE = space(1)
  w_FLCOMP = space(1)
  w_FLCOM2 = space(1)
  w_DATMAX = ctod("  /  /  ")
  w_OGGETTO = .NULL.
  w_ZOOM = .NULL.
  w_CUR_NAME = space(15)
  * --- WorkFile variables
  RAP_PRES_idx=0
  CAN_TIER_idx=0
  DIPENDEN_idx=0
  PAR_ALTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pParam="A"
        * --- CONTROLLI IN PRESTAZIONI RAPIDE
        * --- Detail delle Prestazioni rapide
        this.w_ObjMPR = iif(Isalt(),this.oParentObject.GSAG_MPR,this.oParentObject.GSAG_MPP)
        * --- Se w_VISMES=-1 allora non deve dare il messaggio (viene valorizzata ad -1 nella routine GSAG_BPR)
        if this.w_ObjMPR.NumRow() > 20 AND this.oParentObject.w_VISMES=0
          if Ah_yesno("Esistono %1 prestazioni provvisorie. Si consiglia di renderle definitive premendo il bottone Aggiorna. Vuoi procedere con l'aggiornamento?",,alltrim(str(this.w_ObjMPR.NumRow())))
            this.oParentObject.w_RESCHK = -1
          endif
        endif
      case this.pParam="C"
        * --- CONTROLLI NELLA MASCHERA RINVIO CUMULATIVO (GSAG_KRC)
        * --- Maschera di Elenco Attivit�
        this.w_Mask = this.oParentObject.oParentObject.oParentObject.oParentObject
        * --- Zoom delle Cose da fare, Note, e Preavvisi
        this.w_CursorZoomMemo = this.w_Mask.w_AGKRA_ZOOM3.ccursor
        * --- Zoom delle Attivit�
        this.w_CursorZoomAtt = this.w_Mask.w_AGKRA_ZOOM.ccursor
        * --- Cicla sul cursore dello zoom relativo alle Cose da fare, Note, e Preavvisi ed esamina i records selezionati
        SELECT (this.w_CursorZoomMemo)
        scan for XCHK=1 
        if cp_CharToDatetime(DTOC(this.oParentObject.w_DATARINV)+" "+this.oParentObject.w_ORERINV+":"+this.oParentObject.w_MINRINV+":00") < ATDATINI
          ah_ErrorMsg("Attenzione: data rinvio non consentita.")
          this.oParentObject.w_RESCHK = -1
          exit
        endif
        endscan
        if this.oParentObject.w_RESCHK # -1
          * --- Cicla sul cursore dello zoom relativo alle ATTIVITA'  ed esamina i records selezionati
          SELECT (this.w_CursorZoomAtt)
          scan for XCHK=1 
          if cp_CharToDatetime(DTOC(this.oParentObject.w_DATARINV)+" "+this.oParentObject.w_ORERINV+":"+this.oParentObject.w_MINRINV+":00") < ATDATINI
            ah_ErrorMsg("Attenzione: data rinvio non consentita.")
            this.oParentObject.w_RESCHK = -1
            exit
          endif
          endscan
        endif
      case this.pParam="P" or this.pParam="I" or this.pParam="W"
        * --- Caricamento prestazioni da ricerca prestazioni
        if this.pParam="W"
          this.w_OBJECT = GSAG_APR()
        else
          this.w_OBJECT = iif(this.pParam="P",IIF(ISALT(),GSAG_KPR(this.oParentObject.w_PAGENPRE),GSAG_KPP()),This.oparentobject)
        endif
        * --- Controllo se ha passato il test di accesso
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        if this.pParam="I" 
          this.w_OBJECT.NotifyEvent("AggiornaXRicerca")     
        endif
        if (!EMPTY(this.oParentObject.w_CODPRA) or this.pParam $ "I-W" ) 
          * --- Anagrafica prestazioni in caricamento anche se non � stata specificata la pratica
          * --- Stiamo filtrando per pratica, passiamo il parametro al caricamento prestazioni
          if this.pParam="I"
            this.w_ObjMPR = iif(Isalt(),this.oParentObject.GSAG_MPR,this.oParentObject.GSAG_MPP)
          endif
          if this.pParam<>"W"
            if this.pParam="P"
              this.w_OBJECT.w_CodPratica = this.oParentObject.w_CODPRA
              this.w_OBJECT.w_EditCodPra = .F.
              this.w_SETLINKED = SetValueLinked("M",this.w_OBJECT,"w_CODPRATICA",this.oParentObject.w_CODPRA)
              * --- Il Check � gi� compreso nella set value linked
              if Empty(this.w_OBJECT.w_CodPratica)
                this.w_OBJECT.GSAG_MPR.bUpdated = .F.
                this.w_OBJECT.Ecpquit()     
                this.w_OBJECT = null
                i_retcode = 'stop'
                return
              endif
              * --- Forziamo anche gli o_ in modo da evitare la domanda "Salvare.."
              this.w_Rsp = ReadResp(i_codute, "C", this.oParentObject.w_CodPra)
              * --- Leggiamo il gruppo di default
              * --- Read from DIPENDEN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DIPENDEN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DPGRUPRE"+;
                  " from "+i_cTable+" DIPENDEN where ";
                      +"DPCODICE = "+cp_ToStrODBC(this.w_Rsp);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DPGRUPRE;
                  from (i_cTable) where;
                      DPCODICE = this.w_Rsp;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_Grp = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_OBJECT.o_CODRESP = this.w_Rsp
              this.w_OBJECT.w_CODRESP = this.w_Rsp
              this.w_OBJECT.o_GRUPART = this.w_Grp
              this.w_OBJECT.w_GRUPART = this.w_Grp
              this.w_SETLINKED = SetValueLinked("M",this.w_OBJECT,"w_CODRESP",this.w_Rsp)
              this.w_SETLINKED = SetValueLinked("M",this.w_OBJECT,"w_GRUPART",this.w_Grp)
              this.w_OBJECT.mcalc(.T.)     
            endif
            if this.pParam $ "P-I"
              * --- Adesso dobbiamo inserire nel transitorio il codice pratica 
              *     affinch� nella prima riga di movimentazione ci sia il codice corretto
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.w_ObjMPR.Loadrec()     
            this.w_ObjMPR.w_CodPratica = this.oParentObject.w_CODPRA
            this.w_ObjMPR.AddRow()     
            this.w_ObjMPR.w_CPROWNUM = this.w_MAXROW+1
            this.w_ObjMPR.Set("CPROWNUM" , this.w_MAXROW+1)     
            this.w_ObjMPR.i_nrownum = this.w_MAXROW+1
            if Not Empty(this.w_CODLIS)
              this.w_ObjMPR.w_PRCODLIS = this.w_CODLIS
              this.w_SETLINKED = SetValueLinked("D",this.w_ObjMPR,"w_PRCODLIS",this.w_CODLIS)
            endif
            this.w_ObjMPR.w_DACODRES = this.w_CODRES
            this.w_ObjMPR.w_PRNUMPRA = this.oParentObject.w_CODPRA
            this.w_SETLINKED = SetValueLinked("D",this.w_ObjMPR,"w_PRNUMPRA",this.oParentObject.w_CODPRA)
            this.w_ObjMPR.SaveRow()     
            this.w_ObjMPR.Refresh()     
            this.w_ObjMPR.bUpdated = .F.
          else
            this.w_OBJECT.ecpload()     
            if !EMPTY(this.oParentObject.w_CODPRA)
              this.w_OBJECT.w_PRNUMPRA = this.oParentObject.w_CODPRA
              this.w_SETLINKED = SetValueLinked("M",this.w_OBJECT,"w_PRNUMPRA",this.oParentObject.w_CODPRA)
              this.w_Rsp = ReadResp(i_codute, "C", this.oParentObject.w_CodPra)
              this.w_OBJECT.Notifyevent("LinkRes")     
            else
              this.w_OBJECT.w_EditCodPra = .T.
            endif
            if Not Empty(this.w_CODLIS)
              this.w_OBJECT.w_PRCODLIS = this.w_CODLIS
              this.w_SETLINKED = SetValueLinked("M",this.w_OBJECT,"w_PRCODLIS",this.w_CODLIS)
            endif
            this.w_OBJECT.mcalc(.T.)     
            this.w_OBJECT.CLOSE = .T.
          endif
        endif
      case this.pParam="F"
        * --- Modificato il flag di "Utilizza spunta"
        if this.oParentObject.w_SELEZ="N"
          this.w_ObjMPR = iif(Isalt(),this.oParentObject.GSAG_MPR,this.oParentObject.GSAG_MPP)
          this.w_ObjMPR.MarkPos()     
          this.w_ObjMPR.FirstRow()     
          do while !this.w_ObjMPR.Eof_Trs()
            this.w_ObjMPR.SetRow()     
            if this.w_ObjMPR.w_FLCOMP="S"
              this.w_ObjMPR.w_FLCOMP = "N"
              this.w_ObjMPR.SaveRow()     
            endif
            this.w_ObjMPR.NextRow()     
          enddo
          this.w_ObjMPR.RePos(.T.)     
        endif
      case this.pParam="S" OR this.pParam="D" OR this.pParam="B"
        * --- Seleziona o deseleziona tutto
        if this.pParam="S"
          * --- Seleziona
          this.w_ValoreSelez = "S"
        else
          * --- Deseleziona
          this.w_ValoreSelez = "N"
        endif
        this.w_ObjMPR = this.oParentObject
        this.w_ObjMPR.MarkPos()     
        this.w_ObjMPR.FirstRow()     
        * --- Se pParam='B' devo selezionare le prime N righe
        if this.pParam="B"
          this.w_MaxRow = this.oParentObject.w_DESEL_BUP
        else
          * --- Avvaloriamo w_MaxRow per completezza, ma non useremo questa variabile
          this.w_MaxRow = this.w_ObjMPR.NumRow()
        endif
        do while ( this.pParam<>"B" AND !this.w_ObjMPR.Eof_Trs() ) OR (this.pParam="B" AND this.w_ObjMPR.RowIndex()<=this.w_MaxRow)
          this.w_ObjMPR.SetRow()     
          if !this.w_ObjMPR.w_FLCOMP=this.w_ValoreSelez
            this.w_ObjMPR.w_FLCOMP = this.w_ValoreSelez
            this.w_ObjMPR.SaveRow()     
          endif
          this.w_ObjMPR.NextRow()     
        enddo
        this.w_ObjMPR.RePos(.T.)     
      case this.pParam="R"
        this.w_OBJECT = This.oparentobject.oparentobject
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ObjMPR.i_nrownum = this.w_MAXROW+1
      case this.pParam="G" OR this.pParam="H"
        * --- pParam='H' se provengo da GSAG_MDA - non devo controllare FLCOM ma DATIPRIG
        this.w_ObjMPR = this.oParentObject
        this.w_RIFPRE = this.w_ObjMPR.w_DARIFPRE
        this.w_RIGPRE = this.w_ObjMPR.w_DARIGPRE
        this.w_ROWNUM = this.w_ObjMPR.w_CPROWNUM
        if this.pParam="H"
          * --- Da GSAG_MDA
          this.w_FLCOMP = this.w_ObjMPR.w_ATIPRIG
          this.w_FLCOM2 = this.w_ObjMPR.w_ATIPRI2
        else
          this.w_FLCOMP = this.w_ObjMPR.w_FLCOMP
        endif
        if this.w_RIFPRE>0 or this.w_RIGPRE="S"
          this.w_ObjMPR.MarkPos()     
          this.w_ObjMPR.FirstRow()     
          do while !this.w_ObjMPR.Eof_Trs()
            this.w_ObjMPR.SetRow()     
            if (this.w_RIFPRE>0 AND (this.w_ObjMPR.w_CPROWNUM=this.w_RIFPRE OR this.w_ObjMPR.w_DARIFPRE=this.w_RIFPRE )) OR (this.w_RIGPRE="S" AND this.w_ObjMPR.w_DARIFPRE=this.w_ROWNUM)
              if this.pParam="H"
                * --- Da GSAG_MDA
                this.w_ObjMPR.w_DATIPRIG = this.w_FLCOMP
                this.w_ObjMPR.w_ATIPRIG = this.w_FLCOMP
                this.w_ObjMPR.w_DATIPRI2 = this.w_FLCOM2
                this.w_ObjMPR.w_ATIPRI2 = this.w_FLCOM2
              else
                this.w_ObjMPR.w_FLCOMP = this.w_FLCOMP
              endif
              this.w_ObjMPR.SaveRow()     
            endif
            this.w_ObjMPR.NextRow()     
          enddo
          this.w_ObjMPR.RePos(.T.)     
        endif
      case this.pParam="M"
        * --- pParam='M' - Determina la data pi� grande utilizzata nell'inserimento provvisorio (GSAG_MPR)
        this.w_ObjMPR = this.oParentObject
        this.w_ObjMPR.MarkPos()     
        this.w_DATMAX = this.w_ObjMPR.w_PR__DATA
        this.w_ObjMPR.Exec_Select("DATMAS", "MAX(T_PR__DATA) AS DATAMASS", "", "", "", "")     
        this.w_DATMAX = Max(NVL(DATMAS.DATAMASS,i_DATSYS),this.w_DATMAX)
        if Empty(this.w_DATMAX)
          this.w_DATMAX = i_DATSYS
        endif
        this.w_ObjMPR.RePos(.T.)     
        this.w_ObjMPR.w_DATMAX = this.w_DATMAX
        if Empty(this.w_ObjMPR.w_PR__DATA)
          this.w_ObjMPR.w_PR__DATA = IIF(this.w_ObjMPR.w_FLDTRP="S",NVL(this.w_ObjMPR.w_DATMAX,i_datsys),i_datsys)
        endif
         
 Select DATMAS 
 USE
      case this.pParam="Z"
        if Type("g_omenu.oparentobject")<>"U"
          this.w_OGGETTO = g_omenu.oparentobject
        else
          this.w_OGGETTO = this.oparentobject
          Select XCHK from (this.w_OGGETTO.w_AGZRP_ZOOM.cCursor) where XCHK=1 into cursor TMP_PRE
          if Reccount("TMP_PRE")=0
            this.w_OGGETTO.w_AGZRP_ZOOM.checkall()     
          endif
          Use in TMP_PRE
          this.w_OGGETTO.w_TOTONO = 0
          this.w_OGGETTO.w_TOTDIR = 0
          this.w_OGGETTO.w_TOTSPE = 0
          this.w_OGGETTO.w_TOTANT = 0
          this.w_OGGETTO.w_TOTACC = 0
        endif
         
 Select (this.w_OGGETTO.w_AGZRP_ZOOM.cCursor) 
 Go top
        SUM Nvl(DAVALRIG,0) TO this.w_OGGETTO.w_TOTDIR FOR XCHK=1 AND Nvl(ARPRESTA," ") $ "RC"
        SUM Nvl(DAVALRIG,0) TO this.w_OGGETTO.w_TOTONO FOR XCHK=1 AND Nvl(ARPRESTA," ") $ "IETPG"
        SUM Nvl(DAVALRIG,0) TO this.w_OGGETTO.w_TOTSPE FOR XCHK=1 AND Nvl(ARPRESTA," ") = "S"
        SUM Nvl(DAVALRIG,0) TO this.w_OGGETTO.w_TOTANT FOR XCHK=1 AND Nvl(ARPRESTA," ") = "A" AND Nvl(PRTIPCAU," ") <> "A"
        SUM Nvl(DAVALRIG,0) TO this.w_OGGETTO.w_TOTACC FOR XCHK=1 AND Nvl(PRTIPCAU," ") = "A"
        GOTO 1
        this.w_OGGETTO = .null.
      case this.pParam="L"
        this.w_ZOOM = this.oParentObject.w_AGZRP_ZOOM
        if USED(this.w_Zoom.cCursor) And RECCOUNT(this.w_Zoom.cCursor)>0
          * --- Seleziona/Deseleziona tutto
          ND = this.w_ZOOM.cCursor
          UPDATE (ND) SET XCHK=IIF(this.oParentObject.w_SELEZI="S",1,0)
          SELECT (ND)
          GOTO 1
        endif
      case this.pParam="E"
        this.w_CUR_NAME = Sys(2015)
        this.w_OGGETTO = this.oparentobject
        Select iif(TIPOPRE="Z" and Nvl(PRTIPCAU," ") <>"A" ,ATSERIAL , ATRIFDOC) as SERIALE, iif(TIPOPRE="Z" and Nvl(PRTIPCAU," ") <>"A" ,-1 , CPROWNUM) as PRROWNUM from (this.w_OGGETTO.w_AGZRP_ZOOM.cCursor) where XCHK=1 into cursor (this.w_CUR_NAME)
        gspr_bnd(this,this.w_OGGETTO.OPARENTOBJECT.w_CODPRA,0," ","S",this.w_CUR_NAME)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Si posiziona sulla prima riga utile e inizializza con il codice della pratica attiva
    this.w_GRURES = this.w_OBJECT.w_GRUPART
    this.w_CODRES = this.w_OBJECT.w_CODRESP
    * --- Select from RAP_PRES
    i_nConn=i_TableProp[this.RAP_PRES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAP_PRES_idx,2],.t.,this.RAP_PRES_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS MAXROW  from "+i_cTable+" RAP_PRES ";
          +" where DACODRES="+cp_ToStrODBC(this.w_CODRES)+" AND DAGRURES= "+cp_ToStrODBC(this.w_GRURES)+"";
           ,"_Curs_RAP_PRES")
    else
      select MAX(CPROWNUM) AS MAXROW from (i_cTable);
       where DACODRES=this.w_CODRES AND DAGRURES= this.w_GRURES;
        into cursor _Curs_RAP_PRES
    endif
    if used('_Curs_RAP_PRES')
      select _Curs_RAP_PRES
      locate for 1=1
      do while not(eof())
      this.w_MAXROW = NVL(_Curs_RAP_PRES.MAXROW,0)
      exit
        select _Curs_RAP_PRES
        continue
      enddo
      use
    endif
    this.w_ObjMPR = iif(Isalt(),this.w_OBJECT.GSAG_MPR,this.w_OBJECT.GSAG_MPP)
    * --- Anticipo assegnamento per permettere alla loadrec di eseguire filtro corretto
    this.w_ObjMPR.w_DACODRES = this.w_CODRES
    this.w_ObjMPR.w_DAGRURES = this.w_GRURES
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='RAP_PRES'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='DIPENDEN'
    this.cWorkTables[4]='PAR_ALTE'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_RAP_PRES')
      use in _Curs_RAP_PRES
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
