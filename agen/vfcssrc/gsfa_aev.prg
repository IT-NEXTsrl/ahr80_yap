* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_aev                                                        *
*              Eventi                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-19                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_aev"))

* --- Class definition
define class tgsfa_aev as StdForm
  Top    = 3
  Left   = 11

  * --- Standard Properties
  Width  = 658
  Height = 536+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-13"
  HelpContextID=76186217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=66

  * --- Constant Properties
  ANEVENTI_IDX = 0
  TIPEVENT_IDX = 0
  OFF_NOMI_IDX = 0
  CAN_TIER_IDX = 0
  DIPENDEN_IDX = 0
  DOC_MAST_IDX = 0
  TIP_DOCU_IDX = 0
  DRVEVENT_IDX = 0
  NOM_CONT_IDX = 0
  cFile = "ANEVENTI"
  cKeySelect = "EV__ANNO,EVSERIAL"
  cKeyWhere  = "EV__ANNO=this.w_EV__ANNO and EVSERIAL=this.w_EVSERIAL"
  cKeyWhereODBC = '"EV__ANNO="+cp_ToStrODBC(this.w_EV__ANNO)';
      +'+" and EVSERIAL="+cp_ToStrODBC(this.w_EVSERIAL)';

  cKeyWhereODBCqualified = '"ANEVENTI.EV__ANNO="+cp_ToStrODBC(this.w_EV__ANNO)';
      +'+" and ANEVENTI.EVSERIAL="+cp_ToStrODBC(this.w_EVSERIAL)';

  cPrg = "gsfa_aev"
  cComment = "Eventi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_EVTIPEVE = space(10)
  o_EVTIPEVE = space(10)
  w_TIPDIR = space(1)
  w_ATCAUATT = space(10)
  w_EVDIREVE = space(1)
  w_EV_STATO = space(1)
  w_EV__ANNO = space(4)
  o_EV__ANNO = space(4)
  w_EVNOMINA = space(15)
  o_EVNOMINA = space(15)
  w_EVCODPAR = space(5)
  o_EVCODPAR = space(5)
  w_EVRIFPER = space(254)
  w_EVLOCALI = space(30)
  w_EV_PHONE = space(18)
  w_EV_EMAIL = space(254)
  w_EVNUMCEL = space(20)
  w_EVTELINT = space(18)
  w_EVCODPRA = space(15)
  o_EVCODPRA = space(15)
  w_EVPERSON = space(5)
  o_EVPERSON = space(5)
  w_EVGRUPPO = space(5)
  w_EVDATINI = ctot('')
  o_EVDATINI = ctot('')
  w_IDRICH = space(15)
  w_STARIC = space(1)
  w_DRIVER = space(10)
  w_EVDATFIN = ctot('')
  w_EVOREEFF = 0
  w_EVMINEFF = 0
  w_ATEVANNO = space(4)
  w_EVIDRICH = space(15)
  o_EVIDRICH = space(15)
  w_IDRICH = space(10)
  w_EVSTARIC = space(1)
  w_EVOGGETT = space(100)
  w_EV__NOTE = space(0)
  w_EVFLGCHK = space(1)
  w_EVSERIAL = space(10)
  o_EVSERIAL = space(10)
  w_VALATT = space(20)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  w_NEWID = .F.
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_DESNOM = space(60)
  w_DESCAN = space(100)
  w_CODPRA = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_DPDESCRI = space(60)
  w_DPNOME = space(50)
  w_DPCOGNOM = space(50)
  w_TIPPER = space(1)
  w_TIPGRU = space(1)
  w_DPDESPER = space(100)
  w_DESTIP = space(50)
  w_EVFLIDPA = space(1)
  w_ATSEREVE = space(10)
  w_ATCODNOM = space(15)
  w_EVSERDOC = space(10)
  w_TIPDOC = space(5)
  w_FLVEAC = space(1)
  w_CATDOC = space(2)
  w_DE__TIPO = space(1)
  w_STARIC = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_OFCODNOM = space(15)
  w_PHONE = space(18)
  w_OFDATDOC = ctod('  /  /  ')
  w_TIPNOM = space(1)
  w_EV_EMPEC = space(254)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_EV__ANNO = this.W_EV__ANNO
  op_EVSERIAL = this.W_EVSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANEVENTI','gsfa_aev')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_aevPag1","gsfa_aev",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Evento")
      .Pages(1).HelpContextID = 36325446
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oEV__ANNO_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='TIPEVENT'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='DIPENDEN'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='TIP_DOCU'
    this.cWorkTables[7]='DRVEVENT'
    this.cWorkTables[8]='NOM_CONT'
    this.cWorkTables[9]='ANEVENTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANEVENTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANEVENTI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_EV__ANNO = NVL(EV__ANNO,space(4))
      .w_EVSERIAL = NVL(EVSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANEVENTI where EV__ANNO=KeySet.EV__ANNO
    *                            and EVSERIAL=KeySet.EVSERIAL
    *
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANEVENTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANEVENTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANEVENTI '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'EV__ANNO',this.w_EV__ANNO  ,'EVSERIAL',this.w_EVSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPDIR = space(1)
        .w_IDRICH = space(15)
        .w_STARIC = space(1)
        .w_DRIVER = space(10)
        .w_IDRICH = space(10)
        .w_VALATT = alltrim(.w_EV__ANNO)+.w_EVSERIAL
        .w_TABKEY = 'ANEVENTI'
        .w_DELIMM = .f.
        .w_NEWID = .f.
        .w_DESNOM = space(60)
        .w_DESCAN = space(100)
        .w_DPDESCRI = space(60)
        .w_DPNOME = space(50)
        .w_DPCOGNOM = space(50)
        .w_TIPPER = 'P'
        .w_TIPGRU = space(1)
        .w_DESTIP = space(50)
        .w_ATSEREVE = .w_EVSERIAL
        .w_TIPDOC = space(5)
        .w_FLVEAC = space(1)
        .w_CATDOC = space(2)
        .w_DE__TIPO = space(1)
        .w_STARIC = space(1)
        .w_DATOBSO = ctod("  /  /  ")
        .w_PHONE = space(18)
        .w_OFDATDOC = i_DatSys
        .w_TIPNOM = space(1)
        .w_EVTIPEVE = NVL(EVTIPEVE,space(10))
          if link_1_1_joined
            this.w_EVTIPEVE = NVL(TETIPEVE101,NVL(this.w_EVTIPEVE,space(10)))
            this.w_DESTIP = NVL(TEDESCRI101,space(50))
            this.w_DRIVER = NVL(TEDRIVER101,space(10))
            this.w_TIPDIR = NVL(TETIPDIR101,space(1))
          else
          .link_1_1('Load')
          endif
        .w_ATCAUATT = Gettipass(.w_EVTIPEVE)
        .w_EVDIREVE = NVL(EVDIREVE,space(1))
        .w_EV_STATO = NVL(EV_STATO,space(1))
        .w_EV__ANNO = NVL(EV__ANNO,space(4))
        .op_EV__ANNO = .w_EV__ANNO
        .w_EVNOMINA = NVL(EVNOMINA,space(15))
          if link_1_7_joined
            this.w_EVNOMINA = NVL(NOCODICE107,NVL(this.w_EVNOMINA,space(15)))
            this.w_DESNOM = NVL(NODESCRI107,space(60))
            this.w_EVLOCALI = NVL(NOLOCALI107,space(30))
            this.w_PHONE = NVL(NOTELEFO107,space(18))
            this.w_EVNUMCEL = NVL(NONUMCEL107,space(20))
            this.w_EV_EMAIL = NVL(NO_EMAIL107,space(254))
            this.w_EV_EMPEC = NVL(NO_EMPEC107,space(254))
          else
          .link_1_7('Load')
          endif
        .w_EVCODPAR = NVL(EVCODPAR,space(5))
          if link_1_8_joined
            this.w_EVCODPAR = NVL(NCCODCON108,NVL(this.w_EVCODPAR,space(5)))
            this.w_EVRIFPER = NVL(NCPERSON108,space(254))
            this.w_PHONE = NVL(NCTELEFO108,space(18))
            this.w_EVNUMCEL = NVL(NCNUMCEL108,space(20))
            this.w_EV_EMAIL = NVL(NC_EMAIL108,space(254))
            this.w_EV_EMPEC = NVL(NC_EMPEC108,space(254))
          else
          .link_1_8('Load')
          endif
        .w_EVRIFPER = NVL(EVRIFPER,space(254))
        .w_EVLOCALI = NVL(EVLOCALI,space(30))
        .w_EV_PHONE = NVL(EV_PHONE,space(18))
        .w_EV_EMAIL = NVL(EV_EMAIL,space(254))
        .w_EVNUMCEL = NVL(EVNUMCEL,space(20))
        .w_EVTELINT = NVL(EVTELINT,space(18))
        .w_EVCODPRA = NVL(EVCODPRA,space(15))
          if link_1_15_joined
            this.w_EVCODPRA = NVL(CNCODCAN115,NVL(this.w_EVCODPRA,space(15)))
            this.w_DESCAN = NVL(CNDESCAN115,space(100))
            this.w_DATOBSO = NVL(cp_ToDate(CNDTOBSO115),ctod("  /  /  "))
          else
          .link_1_15('Load')
          endif
        .w_EVPERSON = NVL(EVPERSON,space(5))
          .link_1_16('Load')
        .w_EVGRUPPO = NVL(EVGRUPPO,space(5))
          if link_1_17_joined
            this.w_EVGRUPPO = NVL(DPCODICE117,NVL(this.w_EVGRUPPO,space(5)))
            this.w_DPDESCRI = NVL(DPDESCRI117,space(60))
            this.w_TIPGRU = NVL(DPTIPRIS117,space(1))
          else
          .link_1_17('Load')
          endif
        .w_EVDATINI = NVL(EVDATINI,ctot(""))
          .link_1_21('Load')
        .w_EVDATFIN = NVL(EVDATFIN,ctot(""))
        .w_EVOREEFF = NVL(EVOREEFF,0)
        .w_EVMINEFF = NVL(EVMINEFF,0)
        .w_ATEVANNO = iif(.cfunction='Load',Space(4),.w_EV__ANNO)
        .w_EVIDRICH = NVL(EVIDRICH,space(15))
        .w_EVSTARIC = NVL(EVSTARIC,space(1))
        .w_EVOGGETT = NVL(EVOGGETT,space(100))
        .w_EV__NOTE = NVL(EV__NOTE,space(0))
        .w_EVFLGCHK = NVL(EVFLGCHK,space(1))
        .w_EVSERIAL = NVL(EVSERIAL,space(10))
        .op_EVSERIAL = .w_EVSERIAL
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CODPRA = .w_EVCODPRA
        .w_OBTEST = i_datsys
        .w_DPDESPER = ALLTRIM(Alltrim(.w_DPNOME)+'  ' + Alltrim(.w_DPCOGNOM))
        .w_EVFLIDPA = NVL(EVFLIDPA,space(1))
          .link_1_81('Load')
        .w_ATCODNOM = .w_EVNOMINA
        .w_EVSERDOC = NVL(EVSERDOC,space(10))
          .link_1_83('Load')
          .link_1_84('Load')
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .w_OFCODNOM = .w_EVNOMINA
        .w_EV_EMPEC = NVL(EV_EMPEC,space(254))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ANEVENTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_EVTIPEVE = space(10)
      .w_TIPDIR = space(1)
      .w_ATCAUATT = space(10)
      .w_EVDIREVE = space(1)
      .w_EV_STATO = space(1)
      .w_EV__ANNO = space(4)
      .w_EVNOMINA = space(15)
      .w_EVCODPAR = space(5)
      .w_EVRIFPER = space(254)
      .w_EVLOCALI = space(30)
      .w_EV_PHONE = space(18)
      .w_EV_EMAIL = space(254)
      .w_EVNUMCEL = space(20)
      .w_EVTELINT = space(18)
      .w_EVCODPRA = space(15)
      .w_EVPERSON = space(5)
      .w_EVGRUPPO = space(5)
      .w_EVDATINI = ctot("")
      .w_IDRICH = space(15)
      .w_STARIC = space(1)
      .w_DRIVER = space(10)
      .w_EVDATFIN = ctot("")
      .w_EVOREEFF = 0
      .w_EVMINEFF = 0
      .w_ATEVANNO = space(4)
      .w_EVIDRICH = space(15)
      .w_IDRICH = space(10)
      .w_EVSTARIC = space(1)
      .w_EVOGGETT = space(100)
      .w_EV__NOTE = space(0)
      .w_EVFLGCHK = space(1)
      .w_EVSERIAL = space(10)
      .w_VALATT = space(20)
      .w_TABKEY = space(8)
      .w_DELIMM = .f.
      .w_NEWID = .f.
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_DESNOM = space(60)
      .w_DESCAN = space(100)
      .w_CODPRA = space(15)
      .w_OBTEST = ctod("  /  /  ")
      .w_DPDESCRI = space(60)
      .w_DPNOME = space(50)
      .w_DPCOGNOM = space(50)
      .w_TIPPER = space(1)
      .w_TIPGRU = space(1)
      .w_DPDESPER = space(100)
      .w_DESTIP = space(50)
      .w_EVFLIDPA = space(1)
      .w_ATSEREVE = space(10)
      .w_ATCODNOM = space(15)
      .w_EVSERDOC = space(10)
      .w_TIPDOC = space(5)
      .w_FLVEAC = space(1)
      .w_CATDOC = space(2)
      .w_DE__TIPO = space(1)
      .w_STARIC = space(1)
      .w_DATOBSO = ctod("  /  /  ")
      .w_OFCODNOM = space(15)
      .w_PHONE = space(18)
      .w_OFDATDOC = ctod("  /  /  ")
      .w_TIPNOM = space(1)
      .w_EV_EMPEC = space(254)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_EVTIPEVE))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_ATCAUATT = Gettipass(.w_EVTIPEVE)
        .w_EVDIREVE = .w_TIPDIR
        .w_EV_STATO = 'D'
        .w_EV__ANNO = iif(.cfunction='Load',iif(Empty(.w_EVDATINI),Alltrim(str(YEAR(Datetime()))),Alltrim(str(YEAR(.w_EVDATINI)))),.w_EV__ANNO)
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_EVNOMINA))
          .link_1_7('Full')
          endif
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_EVCODPAR))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,10,.f.)
        .w_EV_PHONE = LEFT(.w_PHONE,18)
        .DoRTCalc(12,15,.f.)
          if not(empty(.w_EVCODPRA))
          .link_1_15('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_EVPERSON))
          .link_1_16('Full')
          endif
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_EVGRUPPO))
          .link_1_17('Full')
          endif
        .w_EVDATINI = Datetime()
        .DoRTCalc(19,21,.f.)
          if not(empty(.w_DRIVER))
          .link_1_21('Full')
          endif
        .w_EVDATFIN = Datetime()
          .DoRTCalc(23,24,.f.)
        .w_ATEVANNO = iif(.cfunction='Load',Space(4),.w_EV__ANNO)
          .DoRTCalc(26,27,.f.)
        .w_EVSTARIC = ' '
          .DoRTCalc(29,30,.f.)
        .w_EVFLGCHK = 'N'
          .DoRTCalc(32,32,.f.)
        .w_VALATT = alltrim(.w_EV__ANNO)+.w_EVSERIAL
        .w_TABKEY = 'ANEVENTI'
        .w_DELIMM = .f.
        .w_NEWID = .f.
          .DoRTCalc(37,42,.f.)
        .w_CODPRA = .w_EVCODPRA
        .w_OBTEST = i_datsys
          .DoRTCalc(45,47,.f.)
        .w_TIPPER = 'P'
          .DoRTCalc(49,49,.f.)
        .w_DPDESPER = ALLTRIM(Alltrim(.w_DPNOME)+'  ' + Alltrim(.w_DPCOGNOM))
          .DoRTCalc(51,52,.f.)
        .w_ATSEREVE = .w_EVSERIAL
        .DoRTCalc(53,53,.f.)
          if not(empty(.w_ATSEREVE))
          .link_1_81('Full')
          endif
        .w_ATCODNOM = .w_EVNOMINA
        .DoRTCalc(55,55,.f.)
          if not(empty(.w_EVSERDOC))
          .link_1_83('Full')
          endif
        .DoRTCalc(56,56,.f.)
          if not(empty(.w_TIPDOC))
          .link_1_84('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
          .DoRTCalc(57,61,.f.)
        .w_OFCODNOM = .w_EVNOMINA
          .DoRTCalc(63,63,.f.)
        .w_OFDATDOC = i_DatSys
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANEVENTI')
    this.DoRTCalc(65,66,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEREVE","i_codazi,w_EV__ANNO,w_EVSERIAL")
      .op_codazi = .w_codazi
      .op_EV__ANNO = .w_EV__ANNO
      .op_EVSERIAL = .w_EVSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oEVTIPEVE_1_1.enabled = i_bVal
      .Page1.oPag.oEVDIREVE_1_4.enabled = i_bVal
      .Page1.oPag.oEV_STATO_1_5.enabled = i_bVal
      .Page1.oPag.oEV__ANNO_1_6.enabled = i_bVal
      .Page1.oPag.oEVNOMINA_1_7.enabled = i_bVal
      .Page1.oPag.oEVCODPAR_1_8.enabled = i_bVal
      .Page1.oPag.oEVRIFPER_1_9.enabled = i_bVal
      .Page1.oPag.oEVLOCALI_1_10.enabled = i_bVal
      .Page1.oPag.oEV_PHONE_1_11.enabled = i_bVal
      .Page1.oPag.oEV_EMAIL_1_12.enabled = i_bVal
      .Page1.oPag.oEVNUMCEL_1_13.enabled = i_bVal
      .Page1.oPag.oEVTELINT_1_14.enabled = i_bVal
      .Page1.oPag.oEVCODPRA_1_15.enabled = i_bVal
      .Page1.oPag.oEVPERSON_1_16.enabled = i_bVal
      .Page1.oPag.oEVGRUPPO_1_17.enabled = i_bVal
      .Page1.oPag.oEVDATINI_1_18.enabled = i_bVal
      .Page1.oPag.oEVDATFIN_1_22.enabled = i_bVal
      .Page1.oPag.oEVOREEFF_1_23.enabled = i_bVal
      .Page1.oPag.oEVMINEFF_1_24.enabled = i_bVal
      .Page1.oPag.oEVIDRICH_1_26.enabled = i_bVal
      .Page1.oPag.oEVSTARIC_1_30.enabled = i_bVal
      .Page1.oPag.oEVOGGETT_1_31.enabled = i_bVal
      .Page1.oPag.oEV__NOTE_1_32.enabled = i_bVal
      .Page1.oPag.oEVFLGCHK_1_33.enabled = i_bVal
      .Page1.oPag.oATSEREVE_1_81.enabled = i_bVal
      .Page1.oPag.oEV_EMPEC_1_100.enabled = i_bVal
      .Page1.oPag.oBtn_1_28.enabled = i_bVal
      .Page1.oPag.oBtn_1_29.enabled = i_bVal
      .Page1.oPag.oBtn_1_58.enabled = .Page1.oPag.oBtn_1_58.mCond()
      .Page1.oPag.oBtn_1_59.enabled = .Page1.oPag.oBtn_1_59.mCond()
      .Page1.oPag.oBtn_1_60.enabled = .Page1.oPag.oBtn_1_60.mCond()
      .Page1.oPag.oBtn_1_61.enabled = .Page1.oPag.oBtn_1_61.mCond()
      .Page1.oPag.oBtn_1_62.enabled = .Page1.oPag.oBtn_1_62.mCond()
      .Page1.oPag.oBtn_1_63.enabled = .Page1.oPag.oBtn_1_63.mCond()
      .Page1.oPag.oObj_1_87.enabled = i_bVal
      .Page1.oPag.oObj_1_88.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oEV__ANNO_1_6.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oEV__ANNO_1_6.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ANEVENTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVTIPEVE,"EVTIPEVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVDIREVE,"EVDIREVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV_STATO,"EV_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV__ANNO,"EV__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVNOMINA,"EVNOMINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVCODPAR,"EVCODPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVRIFPER,"EVRIFPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVLOCALI,"EVLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV_PHONE,"EV_PHONE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV_EMAIL,"EV_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVNUMCEL,"EVNUMCEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVTELINT,"EVTELINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVCODPRA,"EVCODPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVPERSON,"EVPERSON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVGRUPPO,"EVGRUPPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVDATINI,"EVDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVDATFIN,"EVDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVOREEFF,"EVOREEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVMINEFF,"EVMINEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVIDRICH,"EVIDRICH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVSTARIC,"EVSTARIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVOGGETT,"EVOGGETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV__NOTE,"EV__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVFLGCHK,"EVFLGCHK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVSERIAL,"EVSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVFLIDPA,"EVFLIDPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVSERDOC,"EVSERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV_EMPEC,"EV_EMPEC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    i_lTable = "ANEVENTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANEVENTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSFA_SEV with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANEVENTI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEREVE","i_codazi,w_EV__ANNO,w_EVSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANEVENTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANEVENTI')
        i_extval=cp_InsertValODBCExtFlds(this,'ANEVENTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(EVTIPEVE,EVDIREVE,EV_STATO,EV__ANNO,EVNOMINA"+;
                  ",EVCODPAR,EVRIFPER,EVLOCALI,EV_PHONE,EV_EMAIL"+;
                  ",EVNUMCEL,EVTELINT,EVCODPRA,EVPERSON,EVGRUPPO"+;
                  ",EVDATINI,EVDATFIN,EVOREEFF,EVMINEFF,EVIDRICH"+;
                  ",EVSTARIC,EVOGGETT,EV__NOTE,EVFLGCHK,EVSERIAL"+;
                  ",UTCC,UTCV,UTDC,UTDV,EVFLIDPA"+;
                  ",EVSERDOC,EV_EMPEC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_EVTIPEVE)+;
                  ","+cp_ToStrODBC(this.w_EVDIREVE)+;
                  ","+cp_ToStrODBC(this.w_EV_STATO)+;
                  ","+cp_ToStrODBC(this.w_EV__ANNO)+;
                  ","+cp_ToStrODBCNull(this.w_EVNOMINA)+;
                  ","+cp_ToStrODBCNull(this.w_EVCODPAR)+;
                  ","+cp_ToStrODBC(this.w_EVRIFPER)+;
                  ","+cp_ToStrODBC(this.w_EVLOCALI)+;
                  ","+cp_ToStrODBC(this.w_EV_PHONE)+;
                  ","+cp_ToStrODBC(this.w_EV_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_EVNUMCEL)+;
                  ","+cp_ToStrODBC(this.w_EVTELINT)+;
                  ","+cp_ToStrODBCNull(this.w_EVCODPRA)+;
                  ","+cp_ToStrODBCNull(this.w_EVPERSON)+;
                  ","+cp_ToStrODBCNull(this.w_EVGRUPPO)+;
                  ","+cp_ToStrODBC(this.w_EVDATINI)+;
                  ","+cp_ToStrODBC(this.w_EVDATFIN)+;
                  ","+cp_ToStrODBC(this.w_EVOREEFF)+;
                  ","+cp_ToStrODBC(this.w_EVMINEFF)+;
                  ","+cp_ToStrODBC(this.w_EVIDRICH)+;
                  ","+cp_ToStrODBC(this.w_EVSTARIC)+;
                  ","+cp_ToStrODBC(this.w_EVOGGETT)+;
                  ","+cp_ToStrODBC(this.w_EV__NOTE)+;
                  ","+cp_ToStrODBC(this.w_EVFLGCHK)+;
                  ","+cp_ToStrODBC(this.w_EVSERIAL)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_EVFLIDPA)+;
                  ","+cp_ToStrODBCNull(this.w_EVSERDOC)+;
                  ","+cp_ToStrODBC(this.w_EV_EMPEC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANEVENTI')
        i_extval=cp_InsertValVFPExtFlds(this,'ANEVENTI')
        cp_CheckDeletedKey(i_cTable,0,'EV__ANNO',this.w_EV__ANNO,'EVSERIAL',this.w_EVSERIAL)
        INSERT INTO (i_cTable);
              (EVTIPEVE,EVDIREVE,EV_STATO,EV__ANNO,EVNOMINA,EVCODPAR,EVRIFPER,EVLOCALI,EV_PHONE,EV_EMAIL,EVNUMCEL,EVTELINT,EVCODPRA,EVPERSON,EVGRUPPO,EVDATINI,EVDATFIN,EVOREEFF,EVMINEFF,EVIDRICH,EVSTARIC,EVOGGETT,EV__NOTE,EVFLGCHK,EVSERIAL,UTCC,UTCV,UTDC,UTDV,EVFLIDPA,EVSERDOC,EV_EMPEC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_EVTIPEVE;
                  ,this.w_EVDIREVE;
                  ,this.w_EV_STATO;
                  ,this.w_EV__ANNO;
                  ,this.w_EVNOMINA;
                  ,this.w_EVCODPAR;
                  ,this.w_EVRIFPER;
                  ,this.w_EVLOCALI;
                  ,this.w_EV_PHONE;
                  ,this.w_EV_EMAIL;
                  ,this.w_EVNUMCEL;
                  ,this.w_EVTELINT;
                  ,this.w_EVCODPRA;
                  ,this.w_EVPERSON;
                  ,this.w_EVGRUPPO;
                  ,this.w_EVDATINI;
                  ,this.w_EVDATFIN;
                  ,this.w_EVOREEFF;
                  ,this.w_EVMINEFF;
                  ,this.w_EVIDRICH;
                  ,this.w_EVSTARIC;
                  ,this.w_EVOGGETT;
                  ,this.w_EV__NOTE;
                  ,this.w_EVFLGCHK;
                  ,this.w_EVSERIAL;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_EVFLIDPA;
                  ,this.w_EVSERDOC;
                  ,this.w_EV_EMPEC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANEVENTI_IDX,i_nConn)
      *
      * update ANEVENTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANEVENTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " EVTIPEVE="+cp_ToStrODBCNull(this.w_EVTIPEVE)+;
             ",EVDIREVE="+cp_ToStrODBC(this.w_EVDIREVE)+;
             ",EV_STATO="+cp_ToStrODBC(this.w_EV_STATO)+;
             ",EVNOMINA="+cp_ToStrODBCNull(this.w_EVNOMINA)+;
             ",EVCODPAR="+cp_ToStrODBCNull(this.w_EVCODPAR)+;
             ",EVRIFPER="+cp_ToStrODBC(this.w_EVRIFPER)+;
             ",EVLOCALI="+cp_ToStrODBC(this.w_EVLOCALI)+;
             ",EV_PHONE="+cp_ToStrODBC(this.w_EV_PHONE)+;
             ",EV_EMAIL="+cp_ToStrODBC(this.w_EV_EMAIL)+;
             ",EVNUMCEL="+cp_ToStrODBC(this.w_EVNUMCEL)+;
             ",EVTELINT="+cp_ToStrODBC(this.w_EVTELINT)+;
             ",EVCODPRA="+cp_ToStrODBCNull(this.w_EVCODPRA)+;
             ",EVPERSON="+cp_ToStrODBCNull(this.w_EVPERSON)+;
             ",EVGRUPPO="+cp_ToStrODBCNull(this.w_EVGRUPPO)+;
             ",EVDATINI="+cp_ToStrODBC(this.w_EVDATINI)+;
             ",EVDATFIN="+cp_ToStrODBC(this.w_EVDATFIN)+;
             ",EVOREEFF="+cp_ToStrODBC(this.w_EVOREEFF)+;
             ",EVMINEFF="+cp_ToStrODBC(this.w_EVMINEFF)+;
             ",EVIDRICH="+cp_ToStrODBC(this.w_EVIDRICH)+;
             ",EVSTARIC="+cp_ToStrODBC(this.w_EVSTARIC)+;
             ",EVOGGETT="+cp_ToStrODBC(this.w_EVOGGETT)+;
             ",EV__NOTE="+cp_ToStrODBC(this.w_EV__NOTE)+;
             ",EVFLGCHK="+cp_ToStrODBC(this.w_EVFLGCHK)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",EVFLIDPA="+cp_ToStrODBC(this.w_EVFLIDPA)+;
             ",EVSERDOC="+cp_ToStrODBCNull(this.w_EVSERDOC)+;
             ",EV_EMPEC="+cp_ToStrODBC(this.w_EV_EMPEC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANEVENTI')
        i_cWhere = cp_PKFox(i_cTable  ,'EV__ANNO',this.w_EV__ANNO  ,'EVSERIAL',this.w_EVSERIAL  )
        UPDATE (i_cTable) SET;
              EVTIPEVE=this.w_EVTIPEVE;
             ,EVDIREVE=this.w_EVDIREVE;
             ,EV_STATO=this.w_EV_STATO;
             ,EVNOMINA=this.w_EVNOMINA;
             ,EVCODPAR=this.w_EVCODPAR;
             ,EVRIFPER=this.w_EVRIFPER;
             ,EVLOCALI=this.w_EVLOCALI;
             ,EV_PHONE=this.w_EV_PHONE;
             ,EV_EMAIL=this.w_EV_EMAIL;
             ,EVNUMCEL=this.w_EVNUMCEL;
             ,EVTELINT=this.w_EVTELINT;
             ,EVCODPRA=this.w_EVCODPRA;
             ,EVPERSON=this.w_EVPERSON;
             ,EVGRUPPO=this.w_EVGRUPPO;
             ,EVDATINI=this.w_EVDATINI;
             ,EVDATFIN=this.w_EVDATFIN;
             ,EVOREEFF=this.w_EVOREEFF;
             ,EVMINEFF=this.w_EVMINEFF;
             ,EVIDRICH=this.w_EVIDRICH;
             ,EVSTARIC=this.w_EVSTARIC;
             ,EVOGGETT=this.w_EVOGGETT;
             ,EV__NOTE=this.w_EV__NOTE;
             ,EVFLGCHK=this.w_EVFLGCHK;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,EVFLIDPA=this.w_EVFLIDPA;
             ,EVSERDOC=this.w_EVSERDOC;
             ,EV_EMPEC=this.w_EV_EMPEC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsfa_aev
    This.Notifyevent('ControlliFinali')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANEVENTI_IDX,i_nConn)
      *
      * delete ANEVENTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'EV__ANNO',this.w_EV__ANNO  ,'EVSERIAL',this.w_EVSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_EVTIPEVE<>.w_EVTIPEVE
            .w_ATCAUATT = Gettipass(.w_EVTIPEVE)
        endif
        if .o_EVTIPEVE<>.w_EVTIPEVE
            .w_EVDIREVE = .w_TIPDIR
        endif
        .DoRTCalc(5,5,.t.)
        if .o_EVDATINI<>.w_EVDATINI
            .w_EV__ANNO = iif(.cfunction='Load',iif(Empty(.w_EVDATINI),Alltrim(str(YEAR(Datetime()))),Alltrim(str(YEAR(.w_EVDATINI)))),.w_EV__ANNO)
        endif
        .DoRTCalc(7,10,.t.)
        if .o_EVNOMINA<>.w_EVNOMINA.or. .o_EVCODPAR<>.w_EVCODPAR
            .w_EV_PHONE = LEFT(.w_PHONE,18)
        endif
        .DoRTCalc(12,20,.t.)
          .link_1_21('Full')
        .DoRTCalc(22,24,.t.)
        if .o_EVSERIAL<>.w_EVSERIAL.or. .o_EV__ANNO<>.w_EV__ANNO
            .w_ATEVANNO = iif(.cfunction='Load',Space(4),.w_EV__ANNO)
        endif
        .DoRTCalc(26,42,.t.)
        if .o_EVCODPRA<>.w_EVCODPRA
            .w_CODPRA = .w_EVCODPRA
        endif
            .w_OBTEST = i_datsys
        .DoRTCalc(45,49,.t.)
        if .o_EVPERSON<>.w_EVPERSON
            .w_DPDESPER = ALLTRIM(Alltrim(.w_DPNOME)+'  ' + Alltrim(.w_DPCOGNOM))
        endif
        .DoRTCalc(51,53,.t.)
        if .o_EVNOMINA<>.w_EVNOMINA
            .w_ATCODNOM = .w_EVNOMINA
        endif
          .link_1_83('Full')
          .link_1_84('Full')
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .DoRTCalc(57,61,.t.)
        if .o_EVNOMINA<>.w_EVNOMINA
            .w_OFCODNOM = .w_EVNOMINA
        endif
        if .o_EVIDRICH<>.w_EVIDRICH
          .Calculate_KOVDKNYGKF()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi .or. .op_EV__ANNO<>.w_EV__ANNO
           cp_AskTableProg(this,i_nConn,"SEREVE","i_codazi,w_EV__ANNO,w_EVSERIAL")
          .op_EVSERIAL = .w_EVSERIAL
        endif
        .op_codazi = .w_codazi
        .op_EV__ANNO = .w_EV__ANNO
      endwith
      this.DoRTCalc(63,66,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
    endwith
  return

  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_KOVDKNYGKF()
    with this
          * --- Cambio stato richiesta
          .w_EVSTARIC = IIF(EMPTY(.w_EVIDRICH),' ','A')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oEVIDRICH_1_26.enabled = this.oPgFrm.Page1.oPag.oEVIDRICH_1_26.mCond()
    this.oPgFrm.Page1.oPag.oEVSTARIC_1_30.enabled = this.oPgFrm.Page1.oPag.oEVSTARIC_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oEVIDRICH_1_26.visible=!this.oPgFrm.Page1.oPag.oEVIDRICH_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_28.visible=!this.oPgFrm.Page1.oPag.oBtn_1_28.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oEVSTARIC_1_30.visible=!this.oPgFrm.Page1.oPag.oEVSTARIC_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_58.visible=!this.oPgFrm.Page1.oPag.oBtn_1_58.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_60.visible=!this.oPgFrm.Page1.oPag.oBtn_1_60.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_62.visible=!this.oPgFrm.Page1.oPag.oBtn_1_62.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_63.visible=!this.oPgFrm.Page1.oPag.oBtn_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oATSEREVE_1_81.visible=!this.oPgFrm.Page1.oPag.oATSEREVE_1_81.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsfa_aev
    if cevent='Progre'
       cp_BeginTrs()
       This.w_EVIDRICH=Space(15)
       i_Conn=i_TableProp[this.ANEVENTI_IDX, 3]
       cp_NextTableProg(this, i_Conn, "IDRICH", "i_codazi,w_EVIDRICH")
       This.w_EVFLIDPA='S'
       This.w_EVSTARIC='A'
       cp_EndTrs(.t.)
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_87.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_88.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsfa_aev
    if cevent='w_EVIDRICH Changed' and Empty(this.w_EVIDRICH)
       This.w_EVFLIDPA=' '
    endif
    IF cEvent='Nuova'
    LOCAL cCURSORE
       IF EMPTY(this.w_EVIDRICH)
         cCURSORE=Sys(2015)
         CREATE CURSOR (cCURSORE) (EVSERIAL C(10),EV__ANNO C(4))
         APPEND BLANK
         REPLACE EVSERIAL with THIS.w_EVSERIAL
         REPLACE EV__ANNO with THIS.w_EV__ANNO
         DO gsag_bra with This,'E','A',cCURSORE
    
       ELSE
         DO gsag_bra with This,'E'
       ENDIF
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=EVTIPEVE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVTIPEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ATE',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_EVTIPEVE)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER,TETIPDIR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_EVTIPEVE))
          select TETIPEVE,TEDESCRI,TEDRIVER,TETIPDIR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVTIPEVE)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStrODBC(trim(this.w_EVTIPEVE)+"%");

            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER,TETIPDIR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStr(trim(this.w_EVTIPEVE)+"%");

            select TETIPEVE,TEDESCRI,TEDRIVER,TETIPDIR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_EVTIPEVE) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oEVTIPEVE_1_1'),i_cWhere,'GSFA_ATE',"Tipi eventi",'GSFADKTE.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER,TETIPDIR";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI,TEDRIVER,TETIPDIR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVTIPEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER,TETIPDIR";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_EVTIPEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_EVTIPEVE)
            select TETIPEVE,TEDESCRI,TEDRIVER,TETIPDIR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVTIPEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_DESTIP = NVL(_Link_.TEDESCRI,space(50))
      this.w_DRIVER = NVL(_Link_.TEDRIVER,space(10))
      this.w_TIPDIR = NVL(_Link_.TETIPDIR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_EVTIPEVE = space(10)
      endif
      this.w_DESTIP = space(50)
      this.w_DRIVER = space(10)
      this.w_TIPDIR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Not Empty(.w_DRIVER) or Empty(.w_EVTIPEVE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, tipo evento senza codice driver")
        endif
        this.w_EVTIPEVE = space(10)
        this.w_DESTIP = space(50)
        this.w_DRIVER = space(10)
        this.w_TIPDIR = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVTIPEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPEVENT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.TETIPEVE as TETIPEVE101"+ ",link_1_1.TEDESCRI as TEDESCRI101"+ ",link_1_1.TEDRIVER as TEDRIVER101"+ ",link_1_1.TETIPDIR as TETIPDIR101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on ANEVENTI.EVTIPEVE=link_1_1.TETIPEVE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and ANEVENTI.EVTIPEVE=link_1_1.TETIPEVE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EVNOMINA
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVNOMINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_EVNOMINA)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL,NO_EMPEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_EVNOMINA))
          select NOCODICE,NODESCRI,NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL,NO_EMPEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVNOMINA)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_EVNOMINA)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL,NO_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_EVNOMINA)+"%");

            select NOCODICE,NODESCRI,NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL,NO_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_EVNOMINA) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oEVNOMINA_1_7'),i_cWhere,'GSAR_ANO',"Elenco nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL,NO_EMPEC";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL,NO_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVNOMINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL,NO_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_EVNOMINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_EVNOMINA)
            select NOCODICE,NODESCRI,NOLOCALI,NOTELEFO,NONUMCEL,NO_EMAIL,NO_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVNOMINA = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(60))
      this.w_EVLOCALI = NVL(_Link_.NOLOCALI,space(30))
      this.w_PHONE = NVL(_Link_.NOTELEFO,space(18))
      this.w_EVNUMCEL = NVL(_Link_.NONUMCEL,space(20))
      this.w_EV_EMAIL = NVL(_Link_.NO_EMAIL,space(254))
      this.w_EV_EMPEC = NVL(_Link_.NO_EMPEC,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_EVNOMINA = space(15)
      endif
      this.w_DESNOM = space(60)
      this.w_EVLOCALI = space(30)
      this.w_PHONE = space(18)
      this.w_EVNUMCEL = space(20)
      this.w_EV_EMAIL = space(254)
      this.w_EV_EMPEC = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_EVNOMINA = space(15)
        this.w_DESNOM = space(60)
        this.w_EVLOCALI = space(30)
        this.w_PHONE = space(18)
        this.w_EVNUMCEL = space(20)
        this.w_EV_EMAIL = space(254)
        this.w_EV_EMPEC = space(254)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVNOMINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_NOMI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.NOCODICE as NOCODICE107"+ ",link_1_7.NODESCRI as NODESCRI107"+ ",link_1_7.NOLOCALI as NOLOCALI107"+ ",link_1_7.NOTELEFO as NOTELEFO107"+ ",link_1_7.NONUMCEL as NONUMCEL107"+ ",link_1_7.NO_EMAIL as NO_EMAIL107"+ ",link_1_7.NO_EMPEC as NO_EMPEC107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on ANEVENTI.EVNOMINA=link_1_7.NOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and ANEVENTI.EVNOMINA=link_1_7.NOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EVCODPAR
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVCODPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_EVCODPAR)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_EVNOMINA);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_EVNOMINA;
                     ,'NCCODCON',trim(this.w_EVCODPAR))
          select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVCODPAR)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EVCODPAR) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oEVCODPAR_1_8'),i_cWhere,'',"Persone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_EVNOMINA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_EVNOMINA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVCODPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_EVCODPAR);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_EVNOMINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_EVNOMINA;
                       ,'NCCODCON',this.w_EVCODPAR)
            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVCODPAR = NVL(_Link_.NCCODCON,space(5))
      this.w_EVRIFPER = NVL(_Link_.NCPERSON,space(254))
      this.w_PHONE = NVL(_Link_.NCTELEFO,space(18))
      this.w_EVNUMCEL = NVL(_Link_.NCNUMCEL,space(20))
      this.w_EV_EMAIL = NVL(_Link_.NC_EMAIL,space(254))
      this.w_EV_EMPEC = NVL(_Link_.NC_EMPEC,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_EVCODPAR = space(5)
      endif
      this.w_EVRIFPER = space(254)
      this.w_PHONE = space(18)
      this.w_EVNUMCEL = space(20)
      this.w_EV_EMAIL = space(254)
      this.w_EV_EMPEC = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVCODPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NOM_CONT_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.NCCODCON as NCCODCON108"+ ",link_1_8.NCPERSON as NCPERSON108"+ ",link_1_8.NCTELEFO as NCTELEFO108"+ ",link_1_8.NCNUMCEL as NCNUMCEL108"+ ",link_1_8.NC_EMAIL as NC_EMAIL108"+ ",link_1_8.NC_EMPEC as NC_EMPEC108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on ANEVENTI.EVCODPAR=link_1_8.NCCODCON"+" and ANEVENTI.EVNOMINA=link_1_8.NCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and ANEVENTI.EVCODPAR=link_1_8.NCCODCON(+)"'+'+" and ANEVENTI.EVNOMINA=link_1_8.NCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EVCODPRA
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_EVCODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_EVCODPRA))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVCODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_EVCODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_EVCODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_EVCODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oEVCODPRA_1_15'),i_cWhere,'',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSFAZAEV.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_EVCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_EVCODPRA)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVCODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_EVCODPRA = space(15)
      endif
      this.w_DESCAN = space(100)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_EVCODPRA = space(15)
        this.w_DESCAN = space(100)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.CNCODCAN as CNCODCAN115"+ ",link_1_15.CNDESCAN as CNDESCAN115"+ ",link_1_15.CNDTOBSO as CNDTOBSO115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on ANEVENTI.EVCODPRA=link_1_15.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and ANEVENTI.EVCODPRA=link_1_15.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EVPERSON
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVPERSON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_EVPERSON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPPER;
                     ,'DPCODICE',trim(this.w_EVPERSON))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVPERSON)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EVPERSON) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oEVPERSON_1_16'),i_cWhere,'GSAR_BDZ',"Persone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPPER<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVPERSON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_EVPERSON);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPPER;
                       ,'DPCODICE',this.w_EVPERSON)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVPERSON = NVL(_Link_.DPCODICE,space(5))
      this.w_DPCOGNOM = NVL(_Link_.DPCOGNOM,space(50))
      this.w_DPNOME = NVL(_Link_.DPNOME,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_EVPERSON = space(5)
      endif
      this.w_DPCOGNOM = space(50)
      this.w_DPNOME = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVPERSON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVGRUPPO
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVGRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_EVGRUPPO)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_EVGRUPPO))
          select DPCODICE,DPDESCRI,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVGRUPPO)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_EVGRUPPO)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_EVGRUPPO)+"%");

            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_EVGRUPPO) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oEVGRUPPO_1_17'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSARGADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVGRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_EVGRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_EVGRUPPO)
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVGRUPPO = NVL(_Link_.DPCODICE,space(5))
      this.w_DPDESCRI = NVL(_Link_.DPDESCRI,space(60))
      this.w_TIPGRU = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_EVGRUPPO = space(5)
      endif
      this.w_DPDESCRI = space(60)
      this.w_TIPGRU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_EVPERSON , .w_EVGRUPPO) OR Empty(.w_EVPERSON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
        endif
        this.w_EVGRUPPO = space(5)
        this.w_DPDESCRI = space(60)
        this.w_TIPGRU = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVGRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.DPCODICE as DPCODICE117"+ ",link_1_17.DPDESCRI as DPDESCRI117"+ ",link_1_17.DPTIPRIS as DPTIPRIS117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on ANEVENTI.EVGRUPPO=link_1_17.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and ANEVENTI.EVGRUPPO=link_1_17.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRIVER
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_lTable = "DRVEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2], .t., this.DRVEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRIVER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRIVER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(this.w_DRIVER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',this.w_DRIVER)
            select DEDRIVER,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRIVER = NVL(_Link_.DEDRIVER,space(10))
      this.w_DE__TIPO = NVL(_Link_.DE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRIVER = space(10)
      endif
      this.w_DE__TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])+'\'+cp_ToStr(_Link_.DEDRIVER,1)
      cp_ShowWarn(i_cKey,this.DRVEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRIVER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATSEREVE
  func Link_1_81(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_lTable = "ANEVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2], .t., this.ANEVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATSEREVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ANEVENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EVSERIAL like "+cp_ToStrODBC(trim(this.w_ATSEREVE)+"%");
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);

          i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVCODPRA,EVNOMINA,EVIDRICH,EVCODPAR,EVNUMCEL,EVLOCALI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EV__ANNO,EVSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EV__ANNO',this.w_ATEVANNO;
                     ,'EVSERIAL',trim(this.w_ATSEREVE))
          select EV__ANNO,EVSERIAL,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVCODPRA,EVNOMINA,EVIDRICH,EVCODPAR,EVNUMCEL,EVLOCALI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EV__ANNO,EVSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATSEREVE)==trim(_Link_.EVSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATSEREVE) and !this.bDontReportError
            deferred_cp_zoom('ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(oSource.parent,'oATSEREVE_1_81'),i_cWhere,'',"Elenco ID richieste",'GSFA_ZID.ANEVENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATEVANNO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVCODPRA,EVNOMINA,EVIDRICH,EVCODPAR,EVNUMCEL,EVLOCALI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select EV__ANNO,EVSERIAL,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVCODPRA,EVNOMINA,EVIDRICH,EVCODPAR,EVNUMCEL,EVLOCALI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVCODPRA,EVNOMINA,EVIDRICH,EVCODPAR,EVNUMCEL,EVLOCALI";
                     +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',oSource.xKey(1);
                       ,'EVSERIAL',oSource.xKey(2))
            select EV__ANNO,EVSERIAL,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVCODPRA,EVNOMINA,EVIDRICH,EVCODPAR,EVNUMCEL,EVLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATSEREVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVCODPRA,EVNOMINA,EVIDRICH,EVCODPAR,EVNUMCEL,EVLOCALI";
                   +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(this.w_ATSEREVE);
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',this.w_ATEVANNO;
                       ,'EVSERIAL',this.w_ATSEREVE)
            select EV__ANNO,EVSERIAL,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVCODPRA,EVNOMINA,EVIDRICH,EVCODPAR,EVNUMCEL,EVLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATSEREVE = NVL(_Link_.EVSERIAL,space(10))
      this.w_STARIC = NVL(_Link_.EVSTARIC,space(1))
      this.w_EV__NOTE = NVL(_Link_.EV__NOTE,space(0))
      this.w_EVOGGETT = NVL(_Link_.EVOGGETT,space(100))
      this.w_EVRIFPER = NVL(_Link_.EVRIFPER,space(254))
      this.w_PHONE = NVL(_Link_.EV_PHONE,space(18))
      this.w_EVTELINT = NVL(_Link_.EVTELINT,space(18))
      this.w_EV_EMAIL = NVL(_Link_.EV_EMAIL,space(254))
      this.w_EV_EMPEC = NVL(_Link_.EV_EMPEC,space(254))
      this.w_EVCODPRA = NVL(_Link_.EVCODPRA,space(15))
      this.w_EVNOMINA = NVL(_Link_.EVNOMINA,space(15))
      this.w_IDRICH = NVL(_Link_.EVIDRICH,space(10))
      this.w_EVCODPAR = NVL(_Link_.EVCODPAR,space(5))
      this.w_EVNUMCEL = NVL(_Link_.EVNUMCEL,space(20))
      this.w_EVLOCALI = NVL(_Link_.EVLOCALI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATSEREVE = space(10)
      endif
      this.w_STARIC = space(1)
      this.w_EV__NOTE = space(0)
      this.w_EVOGGETT = space(100)
      this.w_EVRIFPER = space(254)
      this.w_PHONE = space(18)
      this.w_EVTELINT = space(18)
      this.w_EV_EMAIL = space(254)
      this.w_EV_EMPEC = space(254)
      this.w_EVCODPRA = space(15)
      this.w_EVNOMINA = space(15)
      this.w_IDRICH = space(10)
      this.w_EVCODPAR = space(5)
      this.w_EVNUMCEL = space(20)
      this.w_EVLOCALI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])+'\'+cp_ToStr(_Link_.EV__ANNO,1)+'\'+cp_ToStr(_Link_.EVSERIAL,1)
      cp_ShowWarn(i_cKey,this.ANEVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATSEREVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVSERDOC
  func Link_1_83(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVSERDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVSERDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_EVSERDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_EVSERDOC)
            select MVSERIAL,MVTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVSERDOC = NVL(_Link_.MVSERIAL,space(10))
      this.w_TIPDOC = NVL(_Link_.MVTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EVSERDOC = space(10)
      endif
      this.w_TIPDOC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVSERDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPDOC
  func Link_1_84(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDFLVEAC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_FLVEAC = space(1)
      this.w_CATDOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oEVTIPEVE_1_1.value==this.w_EVTIPEVE)
      this.oPgFrm.Page1.oPag.oEVTIPEVE_1_1.value=this.w_EVTIPEVE
    endif
    if not(this.oPgFrm.Page1.oPag.oEVDIREVE_1_4.RadioValue()==this.w_EVDIREVE)
      this.oPgFrm.Page1.oPag.oEVDIREVE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEV_STATO_1_5.RadioValue()==this.w_EV_STATO)
      this.oPgFrm.Page1.oPag.oEV_STATO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEV__ANNO_1_6.value==this.w_EV__ANNO)
      this.oPgFrm.Page1.oPag.oEV__ANNO_1_6.value=this.w_EV__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oEVNOMINA_1_7.value==this.w_EVNOMINA)
      this.oPgFrm.Page1.oPag.oEVNOMINA_1_7.value=this.w_EVNOMINA
    endif
    if not(this.oPgFrm.Page1.oPag.oEVCODPAR_1_8.value==this.w_EVCODPAR)
      this.oPgFrm.Page1.oPag.oEVCODPAR_1_8.value=this.w_EVCODPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oEVRIFPER_1_9.value==this.w_EVRIFPER)
      this.oPgFrm.Page1.oPag.oEVRIFPER_1_9.value=this.w_EVRIFPER
    endif
    if not(this.oPgFrm.Page1.oPag.oEVLOCALI_1_10.value==this.w_EVLOCALI)
      this.oPgFrm.Page1.oPag.oEVLOCALI_1_10.value=this.w_EVLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oEV_PHONE_1_11.value==this.w_EV_PHONE)
      this.oPgFrm.Page1.oPag.oEV_PHONE_1_11.value=this.w_EV_PHONE
    endif
    if not(this.oPgFrm.Page1.oPag.oEV_EMAIL_1_12.value==this.w_EV_EMAIL)
      this.oPgFrm.Page1.oPag.oEV_EMAIL_1_12.value=this.w_EV_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oEVNUMCEL_1_13.value==this.w_EVNUMCEL)
      this.oPgFrm.Page1.oPag.oEVNUMCEL_1_13.value=this.w_EVNUMCEL
    endif
    if not(this.oPgFrm.Page1.oPag.oEVTELINT_1_14.value==this.w_EVTELINT)
      this.oPgFrm.Page1.oPag.oEVTELINT_1_14.value=this.w_EVTELINT
    endif
    if not(this.oPgFrm.Page1.oPag.oEVCODPRA_1_15.value==this.w_EVCODPRA)
      this.oPgFrm.Page1.oPag.oEVCODPRA_1_15.value=this.w_EVCODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oEVPERSON_1_16.value==this.w_EVPERSON)
      this.oPgFrm.Page1.oPag.oEVPERSON_1_16.value=this.w_EVPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oEVGRUPPO_1_17.value==this.w_EVGRUPPO)
      this.oPgFrm.Page1.oPag.oEVGRUPPO_1_17.value=this.w_EVGRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oEVDATINI_1_18.value==this.w_EVDATINI)
      this.oPgFrm.Page1.oPag.oEVDATINI_1_18.value=this.w_EVDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oEVDATFIN_1_22.value==this.w_EVDATFIN)
      this.oPgFrm.Page1.oPag.oEVDATFIN_1_22.value=this.w_EVDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oEVOREEFF_1_23.value==this.w_EVOREEFF)
      this.oPgFrm.Page1.oPag.oEVOREEFF_1_23.value=this.w_EVOREEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oEVMINEFF_1_24.value==this.w_EVMINEFF)
      this.oPgFrm.Page1.oPag.oEVMINEFF_1_24.value=this.w_EVMINEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oEVIDRICH_1_26.value==this.w_EVIDRICH)
      this.oPgFrm.Page1.oPag.oEVIDRICH_1_26.value=this.w_EVIDRICH
    endif
    if not(this.oPgFrm.Page1.oPag.oEVSTARIC_1_30.RadioValue()==this.w_EVSTARIC)
      this.oPgFrm.Page1.oPag.oEVSTARIC_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEVOGGETT_1_31.value==this.w_EVOGGETT)
      this.oPgFrm.Page1.oPag.oEVOGGETT_1_31.value=this.w_EVOGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oEV__NOTE_1_32.value==this.w_EV__NOTE)
      this.oPgFrm.Page1.oPag.oEV__NOTE_1_32.value=this.w_EV__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oEVFLGCHK_1_33.RadioValue()==this.w_EVFLGCHK)
      this.oPgFrm.Page1.oPag.oEVFLGCHK_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_54.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_54.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_55.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_55.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESCRI_1_69.value==this.w_DPDESCRI)
      this.oPgFrm.Page1.oPag.oDPDESCRI_1_69.value=this.w_DPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESPER_1_76.value==this.w_DPDESPER)
      this.oPgFrm.Page1.oPag.oDPDESPER_1_76.value=this.w_DPDESPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIP_1_77.value==this.w_DESTIP)
      this.oPgFrm.Page1.oPag.oDESTIP_1_77.value=this.w_DESTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oATSEREVE_1_81.value==this.w_ATSEREVE)
      this.oPgFrm.Page1.oPag.oATSEREVE_1_81.value=this.w_ATSEREVE
    endif
    if not(this.oPgFrm.Page1.oPag.oEV_EMPEC_1_100.value==this.w_EV_EMPEC)
      this.oPgFrm.Page1.oPag.oEV_EMPEC_1_100.value=this.w_EV_EMPEC
    endif
    cp_SetControlsValueExtFlds(this,'ANEVENTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_EVTIPEVE)) or not(Not Empty(.w_DRIVER) or Empty(.w_EVTIPEVE)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVTIPEVE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_EVTIPEVE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, tipo evento senza codice driver")
          case   (empty(.w_EV__ANNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEV__ANNO_1_6.SetFocus()
            i_bnoObbl = !empty(.w_EV__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and not(empty(.w_EVNOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVNOMINA_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(empty(.w_EVCODPRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVCODPRA_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_EVPERSON , .w_EVGRUPPO) OR Empty(.w_EVPERSON)))  and not(empty(.w_EVGRUPPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVGRUPPO_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
          case   (empty(.w_EVDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVDATINI_1_18.SetFocus()
            i_bnoObbl = !empty(.w_EVDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EVDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVDATFIN_1_22.SetFocus()
            i_bnoObbl = !empty(.w_EVDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_EVTIPEVE = this.w_EVTIPEVE
    this.o_EV__ANNO = this.w_EV__ANNO
    this.o_EVNOMINA = this.w_EVNOMINA
    this.o_EVCODPAR = this.w_EVCODPAR
    this.o_EVCODPRA = this.w_EVCODPRA
    this.o_EVPERSON = this.w_EVPERSON
    this.o_EVDATINI = this.w_EVDATINI
    this.o_EVIDRICH = this.w_EVIDRICH
    this.o_EVSERIAL = this.w_EVSERIAL
    return

  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsfa_aevPag1 as StdContainer
  Width  = 654
  height = 539
  stdWidth  = 654
  stdheight = 539
  resizeXpos=464
  resizeYpos=434
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oEVTIPEVE_1_1 as StdField with uid="JIVLZZHJZV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_EVTIPEVE", cQueryName = "EVTIPEVE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, tipo evento senza codice driver",;
    ToolTipText = "Tipo evento",;
    HelpContextID = 96737419,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=96, Top=6, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_EVTIPEVE"

  func oEVTIPEVE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVTIPEVE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVTIPEVE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oEVTIPEVE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ATE',"Tipi eventi",'GSFADKTE.TIPEVENT_VZM',this.parent.oContained
  endproc
  proc oEVTIPEVE_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ATE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_EVTIPEVE
     i_obj.ecpSave()
  endproc


  add object oEVDIREVE_1_4 as StdCombo with uid="FYCWOBRSDC",rtseq=4,rtrep=.f.,left=96,top=31,width=104,height=21;
    , ToolTipText = "Direzione evento";
    , HelpContextID = 98769035;
    , cFormVar="w_EVDIREVE",RowSource=""+"Entrata,"+"Uscita", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEVDIREVE_1_4.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'U',;
    space(1))))
  endfunc
  func oEVDIREVE_1_4.GetRadio()
    this.Parent.oContained.w_EVDIREVE = this.RadioValue()
    return .t.
  endfunc

  func oEVDIREVE_1_4.SetRadio()
    this.Parent.oContained.w_EVDIREVE=trim(this.Parent.oContained.w_EVDIREVE)
    this.value = ;
      iif(this.Parent.oContained.w_EVDIREVE=='E',1,;
      iif(this.Parent.oContained.w_EVDIREVE=='U',2,;
      0))
  endfunc


  add object oEV_STATO_1_5 as StdCombo with uid="EUROVJPAAT",rtseq=5,rtrep=.f.,left=336,top=31,width=127,height=21;
    , ToolTipText = "Stato evento";
    , HelpContextID = 34523285;
    , cFormVar="w_EV_STATO",RowSource=""+"Processato,"+"Da processare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEV_STATO_1_5.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oEV_STATO_1_5.GetRadio()
    this.Parent.oContained.w_EV_STATO = this.RadioValue()
    return .t.
  endfunc

  func oEV_STATO_1_5.SetRadio()
    this.Parent.oContained.w_EV_STATO=trim(this.Parent.oContained.w_EV_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_EV_STATO=='P',1,;
      iif(this.Parent.oContained.w_EV_STATO=='D',2,;
      0))
  endfunc

  add object oEV__ANNO_1_6 as StdField with uid="RHWEWAPOIN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_EV__ANNO", cQueryName = "EV__ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno inserimento",;
    HelpContextID = 34944875,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=502, Top=31, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  add object oEVNOMINA_1_7 as StdField with uid="UECGMQMOJG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_EVNOMINA", cQueryName = "EVNOMINA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo",;
    HelpContextID = 107366265,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=96, Top=55, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_EVNOMINA"

  func oEVNOMINA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
      if .not. empty(.w_EVCODPAR)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oEVNOMINA_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVNOMINA_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oEVNOMINA_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Elenco nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oEVNOMINA_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_EVNOMINA
     i_obj.ecpSave()
  endproc

  add object oEVCODPAR_1_8 as StdField with uid="XACTPKPIET",rtseq=8,rtrep=.f.,;
    cFormVar = "w_EVCODPAR", cQueryName = "EVCODPAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona del nominativo",;
    HelpContextID = 592024,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=96, Top=80, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCCODICE", oKey_1_2="this.w_EVNOMINA", oKey_2_1="NCCODCON", oKey_2_2="this.w_EVCODPAR"

  func oEVCODPAR_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVCODPAR_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVCODPAR_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.NOM_CONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_EVNOMINA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStr(this.Parent.oContained.w_EVNOMINA)
    endif
    do cp_zoom with 'NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(this.parent,'oEVCODPAR_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Persone",'',this.parent.oContained
  endproc

  add object oEVRIFPER_1_9 as StdField with uid="KEZMGKXFDT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_EVRIFPER", cQueryName = "EVRIFPER",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento persona",;
    HelpContextID = 2357400,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=166, Top=80, InputMask=replicate('X',254)

  add object oEVLOCALI_1_10 as StdField with uid="CSZNJTRKYU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_EVLOCALI", cQueryName = "EVLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit�",;
    HelpContextID = 252077937,;
   bGlobalFont=.t.,;
    Height=21, Width=447, Left=96, Top=105, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oEVLOCALI_1_10.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_EVLOCALI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oEV_PHONE_1_11 as StdField with uid="MXVGYFCDDA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_EV_PHONE", cQueryName = "EV_PHONE",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di telefono / FAX",;
    HelpContextID = 11810677,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=96, Top=179, InputMask=replicate('X',18)

  add object oEV_EMAIL_1_12 as StdField with uid="UZIZQAQGZX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_EV_EMAIL", cQueryName = "EV_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail",;
    HelpContextID = 242169710,;
   bGlobalFont=.t.,;
    Height=21, Width=447, Left=96, Top=130, InputMask=replicate('X',254)

  add object oEVNUMCEL_1_13 as StdField with uid="UHGDTHVCWV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_EVNUMCEL", cQueryName = "EVNUMCEL",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Numero del telefono cellulare",;
    HelpContextID = 60799122,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=369, Top=179, InputMask=replicate('X',20)

  add object oEVTELINT_1_14 as StdField with uid="RJNACRTUIU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_EVTELINT", cQueryName = "EVTELINT",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di telefono interno",;
    HelpContextID = 109045606,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=96, Top=204, InputMask=replicate('X',18)

  add object oEVCODPRA_1_15 as StdField with uid="OWQFVICYMF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_EVCODPRA", cQueryName = "EVCODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 592007,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=96, Top=229, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_EVCODPRA"

  func oEVCODPRA_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVCODPRA_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVCODPRA_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oEVCODPRA_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSFAZAEV.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oEVPERSON_1_16 as StdField with uid="NLNODBSOTR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_EVPERSON", cQueryName = "EVPERSON",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona",;
    HelpContextID = 65001620,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=96, Top=254, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPPER", oKey_2_1="DPCODICE", oKey_2_2="this.w_EVPERSON"

  func oEVPERSON_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVPERSON_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVPERSON_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPPER)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPPER)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oEVPERSON_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'',this.parent.oContained
  endproc
  proc oEVPERSON_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TIPPER
     i_obj.w_DPCODICE=this.parent.oContained.w_EVPERSON
     i_obj.ecpSave()
  endproc

  add object oEVGRUPPO_1_17 as StdField with uid="BXERVYYGHN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_EVGRUPPO", cQueryName = "EVGRUPPO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, persona non appartenente al gruppo selezionato",;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 18630805,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=96, Top=279, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_EVGRUPPO"

  func oEVGRUPPO_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVGRUPPO_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVGRUPPO_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oEVGRUPPO_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSARGADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oEVGRUPPO_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_EVGRUPPO
     i_obj.ecpSave()
  endproc

  add object oEVDATINI_1_18 as StdField with uid="AEAGFAUAUB",rtseq=18,rtrep=.f.,;
    cFormVar = "w_EVDATINI", cQueryName = "EVDATINI",;
    bObbl = .t. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 100984689,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=96, Top=304

  add object oEVDATFIN_1_22 as StdField with uid="CHZWYCUTOU",rtseq=22,rtrep=.f.,;
    cFormVar = "w_EVDATFIN", cQueryName = "EVDATFIN",;
    bObbl = .t. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 151316332,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=336, Top=304

  add object oEVOREEFF_1_23 as StdField with uid="HOPKSKORTN",rtseq=23,rtrep=.f.,;
    cFormVar = "w_EVOREEFF", cQueryName = "EVOREEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 85772428,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=579, Top=304, cSayPict='"999"', cGetPict='"999"'

  add object oEVMINEFF_1_24 as StdField with uid="REGHTMCSSO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_EVMINEFF", cQueryName = "EVMINEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 94611596,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=619, Top=304, cSayPict='"99"', cGetPict='"99"'

  add object oEVIDRICH_1_26 as StdField with uid="TJBDRPPGWK",rtseq=26,rtrep=.f.,;
    cFormVar = "w_EVIDRICH", cQueryName = "EVIDRICH",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 165570702,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=96, Top=329, InputMask=replicate('X',15)

  func oEVIDRICH_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oEVIDRICH_1_26.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  proc oEVIDRICH_1_26.mAfter
      with this.Parent.oContained
        gsag_ble(this.Parent.oContained,.w_EVIDRICH,".w_ATSEREVE",This.Parent.oContained,".w_EVNOMINA")
      endwith
  endproc


  add object oBtn_1_28 as StdButton with uid="JMPPTWBRZQ",left=216, top=329, width=20,height=22,;
    caption="?", nPag=1;
    , ToolTipText = "Premere per selezionare ID richiesta";
    , HelpContextID = 76185114;
  , bGlobalFont=.t.

    proc oBtn_1_28.Click()
      do GSFA_KID with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (empty(.w_EVIDRICH))
      endwith
    endif
  endfunc

  func oBtn_1_28.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Isalt() OR Not empty(.w_EVIDRICH))
     endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="OSSBCQXAVZ",left=238, top=329, width=20,height=22,;
    caption="+", nPag=1;
    , ToolTipText = "Nuovo ID richiesta";
    , HelpContextID = 76185434;
  , bGlobalFont=.t.

    proc oBtn_1_29.Click()
      this.parent.oContained.NotifyEvent("Progre")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (empty(.w_EVIDRICH))
      endwith
    endif
  endfunc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Isalt() or Not empty(.w_EVIDRICH))
     endwith
    endif
  endfunc


  add object oEVSTARIC_1_30 as StdCombo with uid="TRFHGFVSFK",rtseq=28,rtrep=.f.,left=336,top=330,width=127,height=21;
    , ToolTipText = "Stato della richiesta  aperto, in corso, in attesa, chiuso";
    , HelpContextID = 237041527;
    , cFormVar="w_EVSTARIC",RowSource=""+"Aperta,"+"In corso,"+"In attesa,"+"Chiusa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEVSTARIC_1_30.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'I',;
    iif(this.value =3,'S',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oEVSTARIC_1_30.GetRadio()
    this.Parent.oContained.w_EVSTARIC = this.RadioValue()
    return .t.
  endfunc

  func oEVSTARIC_1_30.SetRadio()
    this.Parent.oContained.w_EVSTARIC=trim(this.Parent.oContained.w_EVSTARIC)
    this.value = ;
      iif(this.Parent.oContained.w_EVSTARIC=='A',1,;
      iif(this.Parent.oContained.w_EVSTARIC=='I',2,;
      iif(this.Parent.oContained.w_EVSTARIC=='S',3,;
      iif(this.Parent.oContained.w_EVSTARIC=='C',4,;
      0))))
  endfunc

  func oEVSTARIC_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EVFLIDPA='S' )
    endwith
   endif
  endfunc

  func oEVSTARIC_1_30.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oEVOGGETT_1_31 as StdField with uid="JUSRBTMQDY",rtseq=29,rtrep=.f.,;
    cFormVar = "w_EVOGGETT", cQueryName = "EVOGGETT",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "oggetto evento",;
    HelpContextID = 87148698,;
   bGlobalFont=.t.,;
    Height=21, Width=554, Left=96, Top=354, InputMask=replicate('X',100)

  add object oEV__NOTE_1_32 as StdMemo with uid="GHSEQOHDIW",rtseq=30,rtrep=.f.,;
    cFormVar = "w_EV__NOTE", cQueryName = "EV__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 263899275,;
   bGlobalFont=.t.,;
    Height=135, Width=554, Left=96, Top=379

  add object oEVFLGCHK_1_33 as StdCheck with uid="ZVZVPSOMBC",rtseq=31,rtrep=.f.,left=96, top=516, caption="Soggetto da controllare",;
    ToolTipText = "Flag soggetto da controllare",;
    HelpContextID = 214550383,;
    cFormVar="w_EVFLGCHK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEVFLGCHK_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oEVFLGCHK_1_33.GetRadio()
    this.Parent.oContained.w_EVFLGCHK = this.RadioValue()
    return .t.
  endfunc

  func oEVFLGCHK_1_33.SetRadio()
    this.Parent.oContained.w_EVFLGCHK=trim(this.Parent.oContained.w_EVFLGCHK)
    this.value = ;
      iif(this.Parent.oContained.w_EVFLGCHK=='S',1,;
      0)
  endfunc

  add object oDESNOM_1_54 as StdField with uid="MKEZBKLLAH",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 230225718,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=236, Top=55, InputMask=replicate('X',60)

  add object oDESCAN_1_55 as StdField with uid="FKWZHSWJXJ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 231601974,;
   bGlobalFont=.t.,;
    Height=21, Width=413, Left=236, Top=229, InputMask=replicate('X',100)


  add object oBtn_1_58 as StdButton with uid="ANLYFEHJWV",left=548, top=5, width=48,height=45,;
    CpPicture="bmp\cattura.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 88154918;
    , tabstop=.f., UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp', caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_58.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"ANEVENTI",.w_EVSERIAL,"GSFA_AEV","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_58.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_EVSERIAL) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_58.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_59 as StdButton with uid="UVCTHODNGY",left=600, top=5, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 33389200;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"ANEVENTI",.w_EV__ANNO+.w_EVSERIAL,"GSFA_AEV","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_59.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_EVSERIAL) And .cFunction='Query')
      endwith
    endif
  endfunc


  add object oBtn_1_60 as StdButton with uid="BJRFWTRDLX",left=548, top=52, width=48,height=45,;
    CpPicture="bmp\scanner.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da periferica di acquisizione immagini";
    , HelpContextID = 130676262;
    , tabstop=.f., UID = 'SCANNER', disabledpicture = 'bmp\scannerd.bmp', caption='\<Scanner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"ANEVENTI",.w_EVSERIAL,"GSFA_AEV","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_60.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_EVSERIAL) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_60.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_61 as StdButton with uid="VMAVLCWAFG",left=600, top=52, width=48,height=45,;
    CpPicture="BMP\CARICA.bmp", caption="", nPag=1;
    , ToolTipText = "Nuova attivit�";
    , HelpContextID = 205928166;
    , tabstop=.f., Caption='\<Attivit�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_61.Click()
      this.parent.oContained.NotifyEvent("Nuova")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_61.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_EVSERIAL) AND .cFunction='Query')
      endwith
    endif
  endfunc


  add object oBtn_1_62 as StdButton with uid="CYWXQOBCPV",left=548, top=99, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Elenco attivit� associate";
    , HelpContextID = 18497094;
    , tabstop=.f., Caption='\<Att. col.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      do gsfa_kaa with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_62.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_EVSERIAL) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_62.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_EVFLIDPA <>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_63 as StdButton with uid="NCTLRMDOAF",left=600, top=99, width=48,height=45,;
    CpPicture="BMP\DOC1.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato all'evento";
    , HelpContextID = 222224074;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_63.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_EVSERDOC,.w_FLVEAC + .w_CATDOC)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_63.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_EVSERDOC) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_63.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_EVSERDOC) OR ISALT())
     endwith
    endif
  endfunc

  add object oDPDESCRI_1_69 as StdField with uid="GRDVQEAQZX",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DPDESCRI", cQueryName = "DPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 65999487,;
   bGlobalFont=.t.,;
    Height=21, Width=486, Left=163, Top=279, InputMask=replicate('X',60)

  add object oDPDESPER_1_76 as StdField with uid="PHOIZIMBYS",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DPDESPER", cQueryName = "DPDESPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 15667848,;
   bGlobalFont=.t.,;
    Height=22, Width=486, Left=163, Top=254, InputMask=replicate('X',100)

  add object oDESTIP_1_77 as StdField with uid="BJGUCKIVBH",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESTIP", cQueryName = "DESTIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 6223670,;
   bGlobalFont=.t.,;
    Height=21, Width=335, Left=208, Top=6, InputMask=replicate('X',50)

  add object oATSEREVE_1_81 as StdField with uid="CGPWQHXDGB",rtseq=53,rtrep=.f.,;
    cFormVar = "w_ATSEREVE", cQueryName = "ATSEREVE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 98567755,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=383, Top=403, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ANEVENTI", oKey_1_1="EV__ANNO", oKey_1_2="this.w_ATEVANNO", oKey_2_1="EVSERIAL", oKey_2_2="this.w_ATSEREVE"

  func oATSEREVE_1_81.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oATSEREVE_1_81.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_81('Part',this)
    endwith
    return bRes
  endfunc

  proc oATSEREVE_1_81.ecpDrop(oSource)
    this.Parent.oContained.link_1_81('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATSEREVE_1_81.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANEVENTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStrODBC(this.Parent.oContained.w_ATEVANNO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStr(this.Parent.oContained.w_ATEVANNO)
    endif
    do cp_zoom with 'ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(this.parent,'oATSEREVE_1_81'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco ID richieste",'GSFA_ZID.ANEVENTI_VZM',this.parent.oContained
  endproc


  add object oObj_1_87 as cp_runprogram with uid="HWWEHUFKJK",left=19, top=552, width=188,height=23,;
    caption='GSFA_BVS(ELIM)',;
   bGlobalFont=.t.,;
    prg="gsfa_bvs('Elim')",;
    cEvent = "Delete start",;
    nPag=1;
    , ToolTipText = "Controllo in eliminazione";
    , HelpContextID = 238469831


  add object oObj_1_88 as cp_runprogram with uid="MKHVNHAKLV",left=19, top=590, width=222,height=23,;
    caption='GSFA_BVS(AGGSTA)',;
   bGlobalFont=.t.,;
    prg="gsfa_bvs('Aggsta')",;
    cEvent = "Record Updated",;
    nPag=1;
    , ToolTipText = "Aggiorno stato eventi figli";
    , HelpContextID = 47781534

  add object oEV_EMPEC_1_100 as StdField with uid="KFLKFYTYHM",rtseq=66,rtrep=.f.,;
    cFormVar = "w_EV_EMPEC", cQueryName = "EV_EMPEC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo e-mail",;
    HelpContextID = 9488521,;
   bGlobalFont=.t.,;
    Height=21, Width=447, Left=96, Top=155, InputMask=replicate('X',254)

  add object oStr_1_34 as StdString with uid="YFHGVSAHAH",Visible=.t., Left=466, Top=31,;
    Alignment=1, Width=35, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="WQMJRTZUOS",Visible=.t., Left=28, Top=8,;
    Alignment=1, Width=66, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="PMOROHSOHQ",Visible=.t., Left=273, Top=31,;
    Alignment=1, Width=58, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="GTGEBESZPR",Visible=.t., Left=29, Top=57,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="JJITEIHJEK",Visible=.t., Left=5, Top=83,;
    Alignment=1, Width=89, Height=18,;
    Caption="Rif. persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="HVGKGVPEFR",Visible=.t., Left=9, Top=131,;
    Alignment=1, Width=85, Height=18,;
    Caption="Indirizzo e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="FSMKNOTOMB",Visible=.t., Left=9, Top=181,;
    Alignment=1, Width=85, Height=18,;
    Caption="Telefono/FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="HBRYFQGGIL",Visible=.t., Left=3, Top=307,;
    Alignment=1, Width=91, Height=18,;
    Caption="Data e ora inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="ZCXBVZUJUZ",Visible=.t., Left=241, Top=307,;
    Alignment=1, Width=90, Height=18,;
    Caption="Data e ora fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="PVSXBVZWYM",Visible=.t., Left=24, Top=32,;
    Alignment=1, Width=70, Height=18,;
    Caption="Direzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="YYMSQBIKAE",Visible=.t., Left=3, Top=357,;
    Alignment=1, Width=91, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="CDDNXNAPUW",Visible=.t., Left=53, Top=230,;
    Alignment=1, Width=41, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="LLDNDSKWUX",Visible=.t., Left=25, Top=230,;
    Alignment=1, Width=69, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="DLIFQNNQGM",Visible=.t., Left=487, Top=307,;
    Alignment=1, Width=91, Height=18,;
    Caption="Durata effettiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="EBKDKQGJLA",Visible=.t., Left=613, Top=308,;
    Alignment=0, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="FZKBMUSFFO",Visible=.t., Left=2, Top=205,;
    Alignment=1, Width=92, Height=18,;
    Caption="Telefono interno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="SCCVUVYESS",Visible=.t., Left=10, Top=281,;
    Alignment=1, Width=84, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="ABEHCTUHPO",Visible=.t., Left=3, Top=254,;
    Alignment=1, Width=91, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="CZXHGRLBZS",Visible=.t., Left=8, Top=332,;
    Alignment=1, Width=86, Height=18,;
    Caption="ID richiesta:"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="ROKAGEDAUD",Visible=.t., Left=261, Top=333,;
    Alignment=1, Width=70, Height=18,;
    Caption="Stato  rich.:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="KOYMQUGVTO",Visible=.t., Left=277, Top=181,;
    Alignment=1, Width=89, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="WBMCAXGCHJ",Visible=.t., Left=5, Top=106,;
    Alignment=1, Width=89, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="NNMUWNNGOS",Visible=.t., Left=9, Top=156,;
    Alignment=1, Width=85, Height=18,;
    Caption="Indirizzo PEC:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_aev','ANEVENTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".EV__ANNO=ANEVENTI.EV__ANNO";
  +" and "+i_cAliasName2+".EVSERIAL=ANEVENTI.EVSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
