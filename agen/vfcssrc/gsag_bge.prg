* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bge                                                        *
*              Generazione documento da attivita                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-28                                                      *
* Last revis.: 2015-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERATT,pSERDOC,pCODNOM,pPARAME,pFLRESTO,pTIPDOC,pVISPRE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bge",oParentObject,m.pSERATT,m.pSERDOC,m.pCODNOM,m.pPARAME,m.pFLRESTO,m.pTIPDOC,m.pVISPRE)
return(i_retval)

define class tgsag_bge as StdBatch
  * --- Local variables
  w_PAG5TYPE = space(1)
  pSERATT = space(20)
  pSERDOC = space(10)
  pCODNOM = space(15)
  pPARAME = 0
  pFLRESTO = .f.
  pTIPDOC = space(1)
  pVISPRE = .f.
  w_RETVAL = space(254)
  w_OK = .f.
  w_ATRIFMOV = space(10)
  w_ATDATDOC = ctod("  /  /  ")
  w_ATTCOLIS = space(5)
  w_ATTSCLIS = space(5)
  w_ATTPROLI = space(5)
  w_ATTPROSC = space(5)
  w_ATCODBUN = space(3)
  w_ATLISACQ = space(5)
  w_ATCAUDOC = space(5)
  w_ATCAUACQ = space(5)
  w_CODNOM = space(15)
  w_ISAHE = .f.
  w_ISAHR = .f.
  w_ISALT = .f.
  w_MVTINCOM = ctod("  /  /  ")
  w_MVTFICOM = ctod("  /  /  ")
  w_ATINIRIC = ctod("  /  /  ")
  w_ATFINRIC = ctod("  /  /  ")
  w_ATINICOS = ctod("  /  /  ")
  w_ATFINCOS = ctod("  /  /  ")
  w_CAFLANAL = space(1)
  w_CACAUACQ = space(5)
  w_FLGZER = space(1)
  w_GENPRE = space(1)
  w_VISPRESTAZ = space(1)
  NUM_PRE = 0
  w_TIPDOC = space(5)
  w_CAUNOT = space(5)
  w_PARAME = 0
  w_OBJDOC = .NULL.
  w_FLANAL = space(1)
  w_OBJDOCACQ = .NULL.
  w_FLDANA = space(1)
  w_VOCECR = space(1)
  w_CODCLI = space(15)
  w_ANTIPOPE = space(10)
  w_DDTIPRIF = space(2)
  w_ATCENCOS = space(15)
  w_ATATTCOS = space(15)
  w_ATCENRIC = space(15)
  w_ATATTRIC = space(15)
  w_ATCOMRIC = space(15)
  w_MVTCOMME = space(15)
  w_MVTCOATT = space(15)
  w_MVTCOCEN = space(15)
  w_bUnderTran = .f.
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_BCREADOCACQ = .f.
  w_ROWVEN = 0
  w_ROWACQ = 0
  w_BCREADOCVEN = .f.
  w_ATCODVAL = space(3)
  w_ATCODLIS = space(5)
  w_ATNOTPIA = space(0)
  w_ATCODPRA = space(15)
  w_ATSERIAL = space(20)
  w_ATRIFDOC = space(10)
  w_ATDATFIN = ctot("")
  w_ATSTATUS = space(1)
  w_ATTIPCLI = space(1)
  w_ATCODNOM = space(15)
  w_ATCAUATT = space(5)
  w_ATCODNOM = space(15)
  w_ATCODSED = space(5)
  w_CPCCCHK = space(10)
  w_MV__ANNO = 0
  w_MV__NOTE = space(0)
  w_MV_FLAGG = space(1)
  w_MV_SEGNO = space(1)
  w_MVMC_PER = space(1)
  w_MVDATOAF = ctod("  /  /  ")
  w_MVTIPDIS = space(1)
  w_MVTIPCOL = space(2)
  w_MVFLRESC = space(1)
  w_MVFLRIAP = space(1)
  w_MVFLRVCL = space(1)
  w_MVPRECON = 0
  w_MVQTANOC = 0
  w_MVQTARES = 0
  w_MVGENPOS = space(1)
  w_MVSTFILCB = space(1)
  w_MVACCOLD = 0
  w_MVACCONT = 0
  w_MVACCPRE = 0
  w_MVACCSUC = 0
  w_MVROWDDT = 0
  w_MVACIVA1 = space(5)
  w_MVACIVA2 = space(5)
  w_MVACIVA3 = space(5)
  w_MVACIVA4 = space(5)
  w_MVACIVA5 = space(5)
  w_MVACIVA6 = space(5)
  w_MVAFLOM1 = space(1)
  w_MVAFLOM2 = space(1)
  w_MVAFLOM3 = space(1)
  w_MVAFLOM4 = space(1)
  w_MVAFLOM5 = space(1)
  w_MVAFLOM6 = space(1)
  w_MVAIMPN1 = 0
  w_MVAIMPN2 = 0
  w_MVAIMPN3 = 0
  w_MVAIMPN4 = 0
  w_MVAIMPN5 = 0
  w_MVAIMPN6 = 0
  w_MVAIMPS1 = 0
  w_MVAIMPS2 = 0
  w_MVAIMPS3 = 0
  w_MVAIMPS4 = 0
  w_MVAIMPS5 = 0
  w_MVAIMPS6 = 0
  w_MVAIRPOR = space(10)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVANNDOC = space(4)
  w_MVANNPRO = space(4)
  w_MVANNRET = space(4)
  w_MVASPEST = space(30)
  w_MVCACONT = space(5)
  w_MVCAOVAL = 0
  w_MVCATCON = space(5)
  w_MVCATOPE = space(2)
  w_MVCAUCOL = space(5)
  w_MVCAUCON = space(5)
  w_MVCAUIMB = 0
  w_MVCAUMAG = space(5)
  w_MVCESSER = space(10)
  w_MVCLADOC = space(2)
  w_MVCODAG2 = space(5)
  w_MVCODAGE = space(5)
  w_MVCODART = space(20)
  w_MVCODASP = space(3)
  w_MVCODATT = space(15)
  w_MVCODBA2 = space(15)
  w_MVCODBAN = space(10)
  w_MVCODCEN = space(15)
  w_MVCODCES = space(20)
  w_MVCODCLA = space(3)
  w_MVCODCOL = space(5)
  w_MVCODCOM = space(15)
  w_MVCODCOM = space(15)
  w_MVCODCON = space(15)
  w_MVCODCOS = space(5)
  w_MVCODDES = space(5)
  w_MVCODESE = space(4)
  w_MVCODICE = space(20)
  w_MVCODIVA = space(5)
  w_MVCODIVE = space(5)
  w_MVCODLIS = space(5)
  w_MVCODLOT = space(20)
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVCODODL = space(15)
  w_MVCODORN = space(15)
  w_MVCODPAG = space(5)
  w_MVCODPOR = space(1)
  w_MVCODRES = space(5)
  w_MVCODSED = space(5)
  w_MVCODSPE = space(3)
  w_MVCODUB2 = space(20)
  w_MVCODUBI = space(20)
  w_MVCODUTE = 0
  w_MVCODVAL = space(3)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVCODVET = space(5)
  w_MVCONCON = space(1)
  w_MVCONIND = space(15)
  w_MVCONTRA = space(15)
  w_MVCONTRO = space(15)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATEST = ctod("  /  /  ")
  w_MVDATEVA = ctod("  /  /  ")
  w_MVDATGEN = ctod("  /  /  ")
  w_MVDATOAI = ctod("  /  /  ")
  w_MVDATPLA = ctod("  /  /  ")
  w_MVDATREG = ctod("  /  /  ")
  w_MVDATTRA = ctod("  /  /  ")
  w_MVDESART = space(40)
  w_MVDESDOC = space(50)
  w_MVDESSUP = space(0)
  w_MVDESSUP = space(0)
  w_MVEFFEVA = ctod("  /  /  ")
  w_MVF2CASC = space(1)
  w_MVF2IMPE = space(1)
  w_MVF2LOTT = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2RISE = space(1)
  w_MVFINCOM = ctod("  /  /  ")
  w_MVFLACCO = space(1)
  w_MVFLARIF = space(1)
  w_MVFLBLOC = space(1)
  w_MVFLCAPA = space(1)
  w_MVFLCASC = space(1)
  w_MVFLCOCO = space(1)
  w_MVFLCONT = space(1)
  w_MVFLELAN = space(1)
  w_MVFLELGM = space(1)
  w_MVFLERIF = space(1)
  w_MVFLEVAS = space(1)
  w_MVFLFOCA = space(1)
  w_MVFLFOSC = space(1)
  w_MVFLGIOM = space(1)
  w_MVFLIMPE = space(1)
  w_MVFLINTE = space(1)
  w_MVFLINTR = space(1)
  w_MVFLLOTT = space(1)
  w_MVFLNOAN = space(1)
  w_MVFLOFFE = space(1)
  w_MVFLOMAG = space(1)
  w_MVFLORCO = space(1)
  w_MVFLORDI = space(1)
  w_MVFLPROV = space(1)
  w_MVFLRAGG = space(1)
  w_MVFLRIMB = space(1)
  w_MVFLRIMB = space(1)
  w_MVFLRINC = space(1)
  w_MVFLRIPA = space(1)
  w_MVFLRISE = space(1)
  w_MVFLRTRA = space(1)
  w_MVFLRTRA = space(1)
  w_MVFLSALD = space(1)
  w_MVFLSCAF = space(1)
  w_MVFLSCOM = space(1)
  w_MVFLSEND = space(1)
  w_MVFLSFIN = space(1)
  w_MVFLTRAS = space(1)
  w_MVFLULCA = space(1)
  w_MVFLULPV = space(1)
  w_MVFLVABD = space(1)
  w_MVFLVEAC = space(1)
  w_MVGENEFF = space(1)
  w_MVGENPRO = space(1)
  w_MVIMPAC2 = 0
  w_MVIMPACC = 0
  w_MVIMPARR = 0
  w_MVIMPCAP = 0
  w_MVIMPCOM = 0
  w_MVIMPEVA = 0
  w_MVIMPFIN = 0
  w_MVIMPNAZ = 0
  w_MVIMPPRO = 0
  w_MVIMPRBA = 0
  w_MVIMPSCO = 0
  w_MVIVAARR = space(5)
  w_MVIVABOL = space(5)
  w_MVIVACAU = space(5)
  w_MVIVAIMB = space(5)
  w_MVIVAINC = space(5)
  w_MVIVATRA = space(5)
  w_MVKEYSAL = space(20)
  w_MVMAXACC = 0
  w_MVMINTRA = space(2)
  w_MVMOLSUP = 0
  w_MVMOVCOM = space(10)
  w_MVNAZPRO = space(3)
  w_MVNOMENC = space(8)
  w_MVNOTAGG = space(40)
  w_MVNUMCOL = 0
  w_MVNUMCOR = space(25)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVNUMREG = 0
  w_MVNUMRIF = 0
  w_MVORATRA = space(2)
  w_MVPAEFOR = space(3)
  w_MVPERFIN = 0
  w_MVPERPRO = 0
  w_MVPERRET = 0
  w_MVPESNET = 0
  w_MVPRD = space(2)
  w_MVPREZZO = 0
  w_MVPROCAP = 0
  w_MVPROORD = space(2)
  w_MVPRP = space(2)
  w_MVQTACOL = 0
  w_MVQTAEV1 = 0
  w_MVQTAEVA = 0
  w_MVQTAIM1 = 0
  w_MVQTAIMP = 0
  w_MVQTALOR = 0
  w_MVQTAMOV = 0
  w_MVQTAPES = 0
  w_MVQTASAL = 0
  w_MVQTAUM1 = 0
  w_MVRIFACC = space(10)
  w_MVRIFCAC = 0
  w_MVRIFCON = space(10)
  w_MVRIFDCO = space(10)
  w_MVRIFDIC = space(15)
  w_MVRIFEDI = space(40)
  w_MVRIFESC = space(10)
  w_MVRIFESP = space(10)
  w_MVRIFFAD = space(10)
  w_MVRIFKIT = 0
  w_MVRIFODL = space(10)
  w_MVRIFORD = space(10)
  w_MVRIFPIA = space(10)
  w_MVRIGMAT = 0
  w_MVRITATT = 0
  w_MVRITPRE = 0
  w_MVROWRIF = 0
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVSCONTI = 0
  w_MVSCOPAG = 0
  w_MVSERDDT = space(10)
  w_MVSEREST = space(40)
  w_MVSERIAL = space(10)
  w_MVSERRIF = space(10)
  w_MVSERWEB = space(50)
  w_MVSPEBOL = 0
  w_MVSPEIMB = 0
  w_MVSPEIMB = 0
  w_MVSPEINC = 0
  w_MVSPETRA = 0
  w_MVSPETRA = 0
  w_MVTCAMAG = space(5)
  w_MVTCOLIS = space(5)
  w_MVTCONTR = space(15)
  w_MVTFICOM = ctod("  /  /  ")
  w_MVTFRAGG = space(1)
  w_MVTINCOM = ctod("  /  /  ")
  w_MVTIPATT = space(1)
  w_MVTIPCON = space(1)
  w_MVTIPDOC = space(5)
  w_MVTIPIMB = space(1)
  w_MVTIPOPE = space(10)
  w_MVTIPORN = space(1)
  w_MVTIPPER = space(1)
  w_MVTIPPR2 = space(2)
  w_MVTIPPRO = space(2)
  w_MVTIPRIG = space(1)
  w_MVTOTENA = 0
  w_MVTOTRIT = 0
  w_MVTRAINT = 0
  w_MVUMSUPP = space(3)
  w_MVUNILOG = space(18)
  w_MVUNIMIS = space(3)
  w_MVVALMAG = 0
  w_MVVALNAZ = space(3)
  w_MVVALRIG = 0
  w_MVVALULT = 0
  w_MVVOCCEN = space(15)
  w_MVLOTMAG = space(5)
  w_MVLOTMAT = space(5)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctod("  /  /  ")
  w_UTDV = ctod("  /  /  ")
  w_MVRIFPRE = 0
  w_MVRIGPRE = space(1)
  w_MVSERIAL = space(10)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_MVTIPORN = space(1)
  w_MVCODORN = space(15)
  w_MVCAUCON = space(5)
  w_MVANNPRO = space(4)
  w_MVPRP = space(2)
  w_MVCODVAL = space(3)
  w_MVCAOVAL = 0
  w_MVTCOCEN = space(15)
  w_MVTCOMME = space(15)
  w_MVDATDIV = ctod("  /  /  ")
  w_MVCODAGE = space(5)
  w_MVCODAG2 = space(5)
  w_MVCODPAG = space(5)
  w_MVCODVET = space(5)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVBENDEP = space(1)
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVSCOPAG = 0
  w_MVSCONTI = 0
  w_MVFLFOSC = space(1)
  w_MVCODBAN = space(10)
  w_MVCODBA2 = space(15)
  w_MVFLSCOR = space(1)
  w_MVTCONTR = space(15)
  w_MVTCOLIS = space(5)
  w_MVTINCOM = ctod("  /  /  ")
  w_MVTFICOM = ctod("  /  /  ")
  w_MVDATCIV = ctod("  /  /  ")
  w_MVDATPLA = ctod("  /  /  ")
  w_MVFLRINC = space(1)
  w_MVFLRTRA = space(1)
  w_MVFLRIMB = space(1)
  w_MVACCPRE = 0
  w_MVCODPOR = space(1)
  w_MVCODSPE = space(3)
  w_MVCONCON = space(1)
  w_MVFLSCAF = space(1)
  w_MVIVABOL = space(5)
  w_MVCODDES = space(5)
  w_MVFLGDIF = space(1)
  w_MVNOTDIF = space(0)
  w_MVNOTEPN = space(0)
  w_MVPRIPIA = 0
  w_MVSPEINC = 0
  w_MVSPEIMB = 0
  w_MVSPETRA = 0
  w_MVSPEBOL = 0
  w_MVIMPARR = 0
  w_MVTCOMAG = space(5)
  w_MVTCOMAT = space(5)
  w_MVQTACOL = 0
  w_MVQTAPES = 0
  w_MVQTALOR = 0
  w_MVDINTRA = space(1)
  w_MVRIFDIC = space(10)
  w_MVCODIVE = space(5)
  w_MVIVAIMB = space(5)
  w_MVIVATRA = space(5)
  w_MVIVAINC = space(5)
  w_MVCLADOC = space(2)
  w_MVFLVEAC = space(1)
  w_MVCODESE = space(4)
  w_MVDATREG = ctod("  /  /  ")
  w_MVDATDOC = ctod("  /  /  ")
  w_MVVALNAZ = space(3)
  w_MVTIPDOC = space(5)
  w_MVFLPROV = space(1)
  w_MVTCAMAG = space(5)
  w_MVFLACCO = space(1)
  w_MVDATEST = ctod("  /  /  ")
  w_MVTDTEVA = ctod("  /  /  ")
  w_MVALFDOC = space(2)
  w_MVALFEST = space(2)
  w_MVANNDOC = space(4)
  w_MVCODUTE = 0
  w_MVFLINTE = space(1)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVNUMREG = 0
  w_MVPRD = space(2)
  w_MVANNRET = space(4)
  w_MVGENEFF = space(1)
  w_MVFLGIOM = space(1)
  w_MVGENPRO = space(1)
  w_MVFLCONT = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctod("  /  /  ")
  w_UTDV = ctod("  /  /  ")
  w_MVACCONT = 0
  w_MVASPEST = space(30)
  w_MVDATTRA = ctod("  /  /  ")
  w_MVMINTRA = space(2)
  w_MVORATRA = space(2)
  w_MVEMERIC = space(1)
  w_MVRIFGEN = space(10)
  w_MVTFRAGG = space(1)
  w_MVTCOATT = space(15)
  w_MVQTANOC = 0
  w_MVQTARES = 0
  w_MVNOCAIV = space(1)
  w_MVFLFOBO = space(1)
  w_MVTOTRIT = 0
  w_MVSEDDES = space(5)
  w_MVRIFEST = space(26)
  w_MVSERWEB = space(50)
  w_GESTGUID = space(14)
  w_MVRIFCON = space(10)
  w_MVRIFPIA = space(10)
  w_MVRIFACC = space(10)
  w_MVRIFFAD = space(10)
  w_MVPERRET = 0
  w_MVTRAINT = 0
  w_MVFLINTR = space(1)
  w_MVMAXACC = 0
  w_MVCAMINT = 0
  w_MVFLBLOC = space(1)
  w_MVIVABLO = space(1)
  w_MVSEGRET = space(1)
  w_MVRIFRIV = space(10)
  w_MVFLRESP = space(1)
  w_MVMOVCOM = space(10)
  w_MVRITPRE = 0
  w_MVTOTENA = 0
  w_MVPERFIN = 0
  w_MVIMPFIN = 0
  w_TDASPETT = space(30)
  w_CODNAZ = space(3)
  w_MVVALINT = space(3)
  w_MVTDTRCO = ctod("  /  /  ")
  w_MVESEUTE = 0
  w_MVTSCLIS = space(5)
  w_MVTPROLI = space(5)
  w_MVPROSCO = space(5)
  w_MVTPROSC = space(5)
  w_MVFLGFRZ = space(1)
  w_MVCONASS = space(1)
  w_MVTCOCO1 = space(15)
  w_MVTCOCO2 = space(15)
  w_MVTCOCO1 = space(15)
  w_MVGENPRE = space(10)
  w_MVRIFCOS = space(10)
  w_MVRIFCOP = space(10)
  w_MV__EPOS = space(1)
  w_MVORIMAG = space(5)
  w_MVSCOLIS = space(5)
  w_MVPROLIS = space(5)
  w_MVCOMMTC = space(15)
  w_MVCOATTC = space(15)
  w_MVCOBNTC = space(3)
  w_MVLISTTC = space(5)
  w_MVIMPMTC = 0
  w_MVVCADTC = space(15)
  w_MVCCADTC = space(15)
  w_MVQTALIM = 0
  w_MVOPELIM = space(1)
  w_MVROWCON = 0
  w_MVNUMATT = 0
  w_MVARTRIF = space(0)
  w_MVCODCO1 = space(15)
  w_MVCODCO2 = space(15)
  w_MVSEDOAS = space(10)
  w_MVRODOAS = 0
  w_MVSERODA = space(15)
  w_MVVALSCO = 0
  w_MVFLSMNS = space(1)
  w_MVCAPERP = 0
  w_MVIMPIND = 0
  w_MVIMPCOL = 0
  w_MVRIGOCL = 0
  w_MVSEROCL = space(15)
  w_MVCLRNUM = 0
  w_MVCLRIFE = space(10)
  w_MVDAINCO = ctod("  /  /  ")
  w_MVINICOM = ctod("  /  /  ")
  w_MVFINCOM = ctod("  /  /  ")
  w_MVCODNOM = space(15)
  w_MVCODBUD = space(10)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MVNUMRIF = 0
  w_MVTIPRIG = space(1)
  w_MVCODICE = space(41)
  w_MVCODART = space(20)
  w_MVCODVAR = space(20)
  w_MVDESART = space(40)
  w_MVDESSUP = space(0)
  w_MVUNIMIS = space(3)
  w_MVCATCON = space(5)
  w_MVCONTRO = space(15)
  w_MVCONIND = space(15)
  w_MVCAUMAG = space(5)
  w_MVCODCLA = space(3)
  w_MVCAUCOL = space(5)
  w_MVCONTRA = space(15)
  w_MVCODLIS = space(5)
  w_MVQTAMOV = 0
  w_MVQTAEVA = 0
  w_MVQTAUM1 = 0
  w_MVQTAEV1 = 0
  w_MVPREZZO = 0
  w_MVIMPEVA = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVFLOMAG = space(1)
  w_MVCODIVA = space(5)
  w_MVKEYSAL = space(40)
  w_MVFLCASC = space(1)
  w_MVFLORDI = space(1)
  w_MVFLIMPE = space(1)
  w_MVFLRISE = space(1)
  w_MVF2CASC = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2IMPE = space(1)
  w_MVF2RISE = space(1)
  w_MVPESNET = 0
  w_MVFLTRAS = space(1)
  w_MVNOMENC = space(8)
  w_MVUMSUPP = space(3)
  w_MVMOLSUP = 0
  w_MVNAZPRO = space(3)
  w_MVPROORD = space(2)
  w_MVNUMCOL = 0
  w_MVTIPCOL = space(20)
  w_MV_SEGNO = space(1)
  w_MVFINCOM = ctod("  /  /  ")
  w_MVFLRIPA = space(1)
  w_MVFLRAGG = space(1)
  w_MVQTASAL = 0
  w_MVFLEVAS = space(1)
  w_MVFLELGM = space(1)
  w_MVFLELAN = space(1)
  w_MVCODBUN = space(1)
  w_MVSERRIF = space(10)
  w_RATERIF = space(10)
  w_MVROWRIF = 0
  w_MVFLRESC = space(1)
  w_MVFLRIAP = space(1)
  w_MVPRECON = 0
  w_MVCODATT = space(15)
  w_MVFLORCO = space(1)
  w_MVFLCOCO = space(1)
  w_MVIMPCOM = 0
  w_MVDAINCO = ctod("  /  /  ")
  w_MVDAFICO = ctod("  /  /  ")
  w_MVTIPATT = space(1)
  w_MVFLRVCL = space(1)
  w_MVNOELPR = space(1)
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVVOCCEN = space(15)
  w_MVCODCEN = space(15)
  w_MVCODCOM = space(15)
  w_MVFLLOTT = space(1)
  w_MVF2LOTT = space(1)
  w_MVCODCOS = space(5)
  w_MVVALRIG = 0
  w_MVRIFKIT = 0
  w_MVFLARIF = space(1)
  w_MVFLERIF = space(1)
  w_MVLIPERP = 0
  w_MVLIMPOP = 0
  w_MVPERPRO = 0
  w_MVIMPPRO = 0
  w_MVCAPERP = 0
  w_MVCAIMPP = 0
  w_MVIMPACC = 0
  w_MVIMPSCO = 0
  w_MVVALMAG = 0
  w_MVIMPNAZ = 0
  w_MVSEROCL = space(15)
  w_MVRIGOCL = 0
  w_MVQTANOC = 0
  w_MVQTARES = 0
  w_MVVALULT = 0
  w_MVCLRIFE = space(10)
  w_MVRIFORD = space(10)
  w_MVCLRNUM = 0
  w_MVIMPCOL = 0
  w_MV_FLAGG = space(1)
  w_MVIMPIND = 0
  w_MVFLSMNS = space(1)
  w_MVNORIVA = space(1)
  w_MVLOTRIS = space(1)
  w_MVLO2RIS = space(1)
  w_MVFLESUP = space(1)
  w_MVDATRCO = ctod("  /  /  ")
  w_MVFLGDIS = space(1)
  w_MVESCRIP = space(1)
  w_DEOREEFF = 0
  w_DEMINEFF = 0
  w_DECOSINT = 0
  w_DEPREMIN = 0
  w_DEPREMAX = 0
  w_DEGAZUFF = space(6)
  w_DETIPPRA = space(10)
  w_DEMATPRA = space(10)
  w_DARIFPRE = 0
  w_DARIGPRE = space(1)
  w_DECALDIR = space(1)
  w_DEPARASS = 0
  w_DECOECAL = 0
  w_DAROWNUM = 0
  w_DACODATT = space(20)
  w_DADESATT = space(40)
  w_DADESAGG = space(0)
  w_DAQTAMOV = 0
  w_DAVALRIG = 0
  w_DAPREZZO = 0
  w_DAFLEVAS = space(1)
  w_DASERRIF = space(10)
  w_DAROWRIF = 0
  w_CHIRIG = 0
  w_TOTCALC = 0
  w_MVCODLIS = space(5)
  w_MVSCOLIS = space(5)
  w_MVPROLIS = space(5)
  w_MVPROSCO = space(5)
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_ARTIPOPE = space(10)
  w_FLGPNA = space(1)
  w_DETIPPRA = space(1)
  w_DEMATPRA = space(1)
  w_DESERIAL = space(10)
  w_DEROWNUM = 0
  w_DENUMRIF = 0
  w_SERACQ = space(10)
  w_MAXORD = 0
  w_PRCODATT = space(20)
  w_PARASS = 0
  w_PRSERAGG = space(10)
  w_PRDESPRE = space(40)
  w_PRDESAGG = space(0)
  w_PRCODOPE = 0
  w_PRDATMOD = ctod("  /  /  ")
  w_PRUNIMIS = space(3)
  w_PRQTAMOV = 0
  w_PRCENCOS = space(15)
  w_PRPREZZO = 0
  w_PRVALRIG = 0
  w_PROREEFF = 0
  w_PRMINEFF = 0
  w_PRCOSINT = 0
  w_PRCOSUNI = 0
  w_PRFLDEFF = space(1)
  w_PRPREMIN = 0
  w_PRPREMAX = 0
  w_PRGAZUFF = space(6)
  w_PRVOCCOS = space(15)
  w_PR_SEGNO = space(1)
  w_PRATTIVI = space(15)
  w_PRINICOM = ctod("  /  /  ")
  w_PRFINCOM = ctod("  /  /  ")
  w_PRQTAUM1 = 0
  w_PRTIPRIG = space(1)
  w_PRTCOINI = ctod("  /  /  ")
  w_PRTCOFIN = ctod("  /  /  ")
  w_PRCODNOM = space(15)
  w_PRTIPRI2 = space(1)
  w_PRRIFPRE = 0
  w_PRRIGPRE = space(1)
  w_UTCC = 0
  w_UTDC = ctod("  /  /  ")
  w_UTDV = ctod("  /  /  ")
  w_PRSERIAL = space(10)
  w_ROWPRE = 0
  w_OBJ = .NULL.
  w_NUMSOGG = space(15)
  * --- WorkFile variables
  DOC_MAST_idx=0
  OFF_ATTI_idx=0
  OFFDATTI_idx=0
  MOVICOST_idx=0
  TMPVEND1_idx=0
  ATT_DCOL_idx=0
  PAR_PRAT_idx=0
  CAUMATTI_idx=0
  AZIENDA_idx=0
  PAR_ALTE_idx=0
  TMP_CERT_idx=0
  RIS_ESTR_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Automatica Documentoda\Movimento di analitica Attivit�
    *     (GSAG_AAT\GSAG_BLA)
    * --- Seriale attivit�
    * --- Seriale documento gi� generato
    * --- Codice soggetto nel caso di ripartizione
    * --- Parametro di ripartizione
    * --- se true assegna resto ripartizione
    * --- Tipologia documento da creare
    *     D: documento normale
    *     R: da ripartizione
    *     N: per nota spese
    * --- Se true: visualizza seriali delle prestazioni
    this.w_ISAHE = isahe()
    this.w_ISAHR = isahr()
    this.w_ISALT = isalt()
    this.w_ATSERIAL = this.pSERATT
    if Type("pSERDOC")="C" 
      this.w_ATRIFDOC = this.pSERDOC
    endif
    * --- Read from OFF_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATATTCOS,ATATTRIC,ATCAUACQ,ATCAUATT,ATCAUDOC,ATCENCOS,ATCENRIC,ATCODBUN,ATCODLIS,ATCODNOM,ATCODPRA,ATCODSED,ATCODVAL,ATCOMRIC,ATDATDOC,ATDATFIN,ATLISACQ,ATNOTPIA,ATRIFMOV,ATSTATUS,ATTCOLIS,ATTPROLI,ATTPROSC,ATTSCLIS,ATINIRIC,ATFINRIC,ATINICOS,ATFINCOS"+;
        " from "+i_cTable+" OFF_ATTI where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATATTCOS,ATATTRIC,ATCAUACQ,ATCAUATT,ATCAUDOC,ATCENCOS,ATCENRIC,ATCODBUN,ATCODLIS,ATCODNOM,ATCODPRA,ATCODSED,ATCODVAL,ATCOMRIC,ATDATDOC,ATDATFIN,ATLISACQ,ATNOTPIA,ATRIFMOV,ATSTATUS,ATTCOLIS,ATTPROLI,ATTPROSC,ATTSCLIS,ATINIRIC,ATFINRIC,ATINICOS,ATFINCOS;
        from (i_cTable) where;
            ATSERIAL = this.w_ATSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ATATTCOS = NVL(cp_ToDate(_read_.ATATTCOS),cp_NullValue(_read_.ATATTCOS))
      this.w_ATATTRIC = NVL(cp_ToDate(_read_.ATATTRIC),cp_NullValue(_read_.ATATTRIC))
      this.w_ATCAUACQ = NVL(cp_ToDate(_read_.ATCAUACQ),cp_NullValue(_read_.ATCAUACQ))
      this.w_ATCAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
      this.w_ATCAUDOC = NVL(cp_ToDate(_read_.ATCAUDOC),cp_NullValue(_read_.ATCAUDOC))
      this.w_ATCENCOS = NVL(cp_ToDate(_read_.ATCENCOS),cp_NullValue(_read_.ATCENCOS))
      this.w_ATCENRIC = NVL(cp_ToDate(_read_.ATCENRIC),cp_NullValue(_read_.ATCENRIC))
      this.w_ATCODBUN = NVL(cp_ToDate(_read_.ATCODBUN),cp_NullValue(_read_.ATCODBUN))
      this.w_ATCODLIS = NVL(cp_ToDate(_read_.ATCODLIS),cp_NullValue(_read_.ATCODLIS))
      this.w_ATCODNOM = NVL(cp_ToDate(_read_.ATCODNOM),cp_NullValue(_read_.ATCODNOM))
      this.w_ATCODNOM = NVL(cp_ToDate(_read_.ATCODNOM),cp_NullValue(_read_.ATCODNOM))
      this.w_ATCODPRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
      this.w_ATCODSED = NVL(cp_ToDate(_read_.ATCODSED),cp_NullValue(_read_.ATCODSED))
      this.w_ATCODVAL = NVL(cp_ToDate(_read_.ATCODVAL),cp_NullValue(_read_.ATCODVAL))
      this.w_ATCOMRIC = NVL(cp_ToDate(_read_.ATCOMRIC),cp_NullValue(_read_.ATCOMRIC))
      this.w_ATDATDOC = NVL(cp_ToDate(_read_.ATDATDOC),cp_NullValue(_read_.ATDATDOC))
      this.w_ATDATFIN = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
      this.w_ATLISACQ = NVL(cp_ToDate(_read_.ATLISACQ),cp_NullValue(_read_.ATLISACQ))
      this.w_ATNOTPIA = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
      this.w_ATRIFMOV = NVL(cp_ToDate(_read_.ATRIFMOV),cp_NullValue(_read_.ATRIFMOV))
      this.w_ATSTATUS = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
      this.w_ATSTATUS = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
      this.w_ATTCOLIS = NVL(cp_ToDate(_read_.ATTCOLIS),cp_NullValue(_read_.ATTCOLIS))
      this.w_ATTPROLI = NVL(cp_ToDate(_read_.ATTPROLI),cp_NullValue(_read_.ATTPROLI))
      this.w_ATTPROSC = NVL(cp_ToDate(_read_.ATTPROSC),cp_NullValue(_read_.ATTPROSC))
      this.w_ATTSCLIS = NVL(cp_ToDate(_read_.ATTSCLIS),cp_NullValue(_read_.ATTSCLIS))
      this.w_ATINIRIC = NVL(cp_ToDate(_read_.ATINIRIC),cp_NullValue(_read_.ATINIRIC))
      this.w_ATFINRIC = NVL(cp_ToDate(_read_.ATFINRIC),cp_NullValue(_read_.ATFINRIC))
      this.w_ATINICOS = NVL(cp_ToDate(_read_.ATINICOS),cp_NullValue(_read_.ATINICOS))
      this.w_ATFINCOS = NVL(cp_ToDate(_read_.ATFINCOS),cp_NullValue(_read_.ATFINCOS))
      use
      if i_Rows=0
        this.w_RETVAL = Ah_msgformat("Attivit� inesistente impossibile generare documento collegato")
      endif
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CAFLANAL = "S"
    this.w_BCREADOCACQ = .F.
    this.w_BCREADOCVEN = .F.
    if this.w_ISALT
      * --- Read from PAR_ALTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAFLGZER,PAGENPRE,PAVISPRE"+;
          " from "+i_cTable+" PAR_ALTE where ";
              +"PACODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAFLGZER,PAGENPRE,PAVISPRE;
          from (i_cTable) where;
              PACODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLGZER = NVL(cp_ToDate(_read_.PAFLGZER),cp_NullValue(_read_.PAFLGZER))
        this.w_GENPRE = NVL(cp_ToDate(_read_.PAGENPRE),cp_NullValue(_read_.PAGENPRE))
        this.w_VISPRESTAZ = NVL(cp_ToDate(_read_.PAVISPRE),cp_NullValue(_read_.PAVISPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from PAR_PRAT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_PRAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PRAT_idx,2],.t.,this.PAR_PRAT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PACAUNOT,PACAUDOC"+;
          " from "+i_cTable+" PAR_PRAT where ";
              +"PACODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PACAUNOT,PACAUDOC;
          from (i_cTable) where;
              PACODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAUNOT = NVL(cp_ToDate(_read_.PACAUNOT),cp_NullValue(_read_.PACAUNOT))
        this.w_TIPDOC = NVL(cp_ToDate(_read_.PACAUDOC),cp_NullValue(_read_.PACAUDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.pTIPDOC $ "R-N" AND this.w_GENPRE<>"S"
      this.w_MVTIPDOC = IIF(this.pTIPDOC="R",this.w_TIPDOC,this.w_CAUNOT)
      if EMPTY(NVL(this.w_TIPDOC,"")) AND this.pTIPDOC="R"
        this.w_RETVAL = Ah_msgformat("E' necessario valorizzare il campo Tipo documento nei Parametri pratiche")
      endif
      if EMPTY(NVL(this.w_CAUNOT,"")) AND this.pTIPDOC="N"
        this.w_RETVAL = Ah_msgformat("E' necessario valorizzare il campo Tipo documento nota spese nei Parametri pratiche")
      endif
    endif
    if Type("pCODNOM")="C" and Not Empty(this.pCODNOM)
      this.w_CODNOM = this.w_ATCODNOM
      this.w_ATCODNOM = this.pCODNOM
      this.w_PRCODNOM = this.pCODNOM
      * --- Lettura causale documento per fatturazione multipla
    else
      this.w_CODNOM = Space(15)
      this.w_MVTIPCON = ""
    endif
    if Type("pPARAME")="N" 
      this.w_PARAME = this.pPARAME
    else
      this.w_PARAME = 0
    endif
    this.w_OK = .t.
    if this.w_ATSTATUS="P" and Empty(this.w_RETVAL)
      if this.w_OK
        if EMPTY(this.w_ATRIFDOC)
          this.w_MVSERIAL = space(10)
        else
          this.w_MVSERIAL = this.w_ATRIFDOC
          * --- Recupero Numero Registrazione e Numero Documento
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVNUMREG,MVNUMDOC,MVTIPDOC,MVALFDOC,MVTIPCON,MVCODCON"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVNUMREG,MVNUMDOC,MVTIPDOC,MVALFDOC,MVTIPCON,MVCODCON;
              from (i_cTable) where;
                  MVSERIAL = this.w_MVSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVNUMREG = NVL(cp_ToDate(_read_.MVNUMREG),cp_NullValue(_read_.MVNUMREG))
            this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
            this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Elimina Documento
          this.w_RETVAL = gsar_bdk(.null.,this.w_MVSERIAL,.f.,.t.)
          if Empty(this.w_RETVAL)
            * --- Cancello riga relativa nella tabella documenti associati
            * --- Delete from ATT_DCOL
            i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"ADTIPDOC <> "+cp_ToStrODBC("D");
                    +" and ADSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
                    +" and ADSERDOC = "+cp_ToStrODBC(this.w_ATRIFDOC);
                     )
            else
              delete from (i_cTable) where;
                    ADTIPDOC <> "D";
                    and ADSERIAL = this.w_ATSERIAL;
                    and ADSERDOC = this.w_ATRIFDOC;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          this.w_OK = Empty(this.w_RETVAL)
        endif
      endif
      if this.w_OK
        * --- Read from CAUMATTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAUMATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CAFLANAL"+;
            " from "+i_cTable+" CAUMATTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_ATCAUATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CAFLANAL;
            from (i_cTable) where;
                CACODICE = this.w_ATCAUATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLANAL = NVL(cp_ToDate(_read_.CAFLANAL),cp_NullValue(_read_.CAFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_GENPRE="S"
          if this.w_VISPRESTAZ = "S" AND this.pVISPRE
            * --- Genera struttura della tabella temporanea (vuota)
            * --- Create temporary table TMP_CERT
            i_nIdx=cp_AddTableDef('TMP_CERT') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('QUERY\gsag_kvv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMP_CERT_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          if this.w_bUnderTran
            * --- begin transaction
            cp_BeginTrs()
          endif
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_bUnderTran 
            if EMPTY(this.w_RETVAL)
              * --- commit
              cp_EndTrs(.t.)
            else
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
            endif
          endif
          if this.w_VISPRESTAZ = "S" AND this.pVISPRE AND this.NUM_PRE > 0 AND EMPTY(this.w_RETVAL)
            do GSAG_KVS with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if this.w_VISPRESTAZ = "S" AND this.pVISPRE
            * --- Drop temporary table TMP_CERT
            i_nIdx=cp_GetTableDefIdx('TMP_CERT')
            if i_nIdx<>0
              cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
              cp_RemoveTableDef('TMP_CERT')
            endif
          endif
        else
          * --- Scrive DOC_MAST se proviene da attivit� oppure se sta inserendo in GSAL_BIA l'attivit� OFF_ATTI
          * --- Creo oggetto per generatore documentale
          this.w_OBJDOC = CREATEOBJECT("DocumWriter",This,"DOC_MAST")
          this.w_OBJDOC.AddAdditionalProp("w_ALT_DETT",'createobject("cp_MemoryCursor",".\query\altdett.vqr",this.oParentObject)',"O","w_ALT_DETT","GSAG_BBA(This,This.w_PADRE)")     
          if !EMPTY(NVL( this.w_ATCAUATT, " "))
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "CACODICE"
            ArrWhere(1,2) = this.w_ATCAUATT
            if Empty(Nvl(this.w_MVTIPDOC," ")) 
              this.w_MVTIPDOC = IIF(NOT EMPTY(this.w_ATCAUDOC),this.w_ATCAUDOC,this.w_OBJDOC.ReadTable( "CAUMATTI" , "CACAUDOC" , @ArrWhere ))
              * --- Nel ramo else, il tipo documento � gi� valorizzato dalla lettura causale documento per fatturazione multipla
            endif
            this.w_FLANAL = this.w_OBJDOC.ReadTable( "CAUMATTI" , "CAFLANAL" , @ArrWhere )
            this.w_CACAUACQ = IIF(NOT EMPTY(this.w_ATCAUACQ),this.w_ATCAUACQ,this.w_OBJDOC.ReadTable( "CAUMATTI" , "CACAUACQ" , @ArrWhere ))
            if this.w_ISAHR
              this.w_CAFLANAL = this.w_FLANAL
            endif
            Release ArrWhere
          endif
          if this.w_CAFLANAL = "N" AND !EMPTY(this.w_CACAUACQ) AND this.w_ISAHR
            this.w_OBJDOCACQ = CREATEOBJECT("DocumWriter",This,"DOC_MAST")
            this.w_OBJDOCACQ.AddAdditionalProp("w_ALT_DETT",'createobject("cp_MemoryCursor",".\query\altdett.vqr",this.oParentObject)',"O","w_ALT_DETT","GSAG_BBA(This,This.w_PADRE)")     
          endif
          * --- assegno causale da utilizzare per documento di acquisto
          if this.w_ISAHE
            this.w_OBJDOC.cCauAcquisti = IIF(NOT EMPTY(this.w_ATCAUACQ),this.w_ATCAUACQ,this.w_CACAUACQ)
          else
            if this.w_CAFLANAL = "N" AND !EMPTY(this.w_CACAUACQ) 
              this.w_OBJDOCACQ.cCauAcquisti = IIF(NOT EMPTY(this.w_ATCAUACQ),this.w_ATCAUACQ,this.w_CACAUACQ)
            endif
          endif
          * --- Lettura da Causale Documento
          this.w_OBJDOC.cLisAcquisti = this.w_ATLISACQ
          if this.w_ISALT
            this.w_OBJDOC.bCalcSal = .f.
          endif
          if this.w_CAFLANAL = "N" AND !EMPTY(this.w_CACAUACQ) AND this.w_ISAHR
            this.w_OBJDOCACQ.cLisAcquisti = this.w_ATLISACQ
          endif
          if !EMPTY(NVL( this.w_ATCAUATT, " "))
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "TDTIPDOC"
            ArrWhere(1,2) = this.w_MVTIPDOC
            this.w_MVFLINTE = this.w_OBJDOC.ReadTable( "TIP_DOCU" , "TDFLINTE" , @ArrWhere )
            if ! this.w_ISAHE
              * --- Leggo tipo voce
              this.w_VOCECR = this.w_OBJDOC.ReadTable( "TIP_DOCU" , "TDVOCECR" , @ArrWhere )
            endif
            Release ArrWhere
          endif
          if !EMPTY(NVL( this.w_ATCODNOM, " "))
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "NOCODICE"
            ArrWhere(1,2) = this.w_ATCODNOM
            this.w_CODCLI = this.w_OBJDOC.ReadTable( "OFF_NOMI" , "NOCODCLI" , @ArrWhere )
            this.w_ATTIPCLI = this.w_OBJDOC.ReadTable( "OFF_NOMI" , "NOTIPCLI" , @ArrWhere )
            Release ArrWhere
          endif
          this.w_MVCATOPE = "OP"
          if NVL(this.w_MVFLINTE, "N")<>"N" 
            this.w_MVTIPCON = this.w_ATTIPCLI
            this.w_MVCODCON = IIF(Not Empty(this.w_MVCODCON),this.w_MVCODCON,this.w_CODCLI)
            if !EMPTY(NVL( this.w_MVCODCON, " "))
              DIMENSION ArrWhere(2,2)
              ArrWhere(1,1) = "ANTIPCON"
              ArrWhere(1,2) = this.w_MVTIPCON
              ArrWhere(2,1) = "ANCODICE"
              ArrWhere(2,2) = this.w_MVCODCON
              this.w_MVFLSCOR = this.w_OBJDOC.ReadTable( "CONTI" , "ANSCORPO" , @ArrWhere )
              this.w_ANTIPOPE = this.w_OBJDOC.ReadTable( "CONTI" , "ANTIPOPE" , @ArrWhere )
              Release ArrWhere
              * --- Sede del cliente
              if this.w_CODNOM=this.w_ATCODNOM OR EMPTY(this.w_CODNOM)
                if empty(this.w_ATCODSED)
                  * --- CERCO LA SEDE DI CONSEGNA PREDEFINITA
                  DIMENSION ArrWhere(4,2)
                  ArrWhere(1,1) = "DDTIPCON"
                  ArrWhere(1,2) = this.w_MVTIPCON
                  ArrWhere(2,1) = "DDCODICE"
                  ArrWhere(2,2) = this.w_MVCODCON
                  ArrWhere(3,1) = "DDTIPRIF"
                  ArrWhere(3,2) = "CO"
                  ArrWhere(4,1) = "DDPREDEF"
                  ArrWhere(4,2) = "S"
                  this.w_MVCODDES = this.w_OBJDOC.ReadTable( "DES_DIVE" , "DDCODDES" , @ArrWhere )
                  Release ArrWhere
                else
                  this.w_MVCODDES = this.w_ATCODSED
                endif
              endif
              if EMPTY(NVL( this.w_MVCODDES, " "))
                this.w_MVTIPOPE = SPACE(10)
              else
                DIMENSION ArrWhere(3,2)
                ArrWhere(1,1) = "DDTIPCON"
                ArrWhere(1,2) = this.w_MVTIPCON
                ArrWhere(2,1) = "DDCODICE"
                ArrWhere(2,2) = this.w_MVCODCON
                ArrWhere(3,1) = "DDCODDES"
                ArrWhere(3,2) = this.w_MVCODDES
                this.w_DDTIPRIF = this.w_OBJDOC.ReadTable( "DES_DIVE" , "DDTIPRIF" , @ArrWhere )
                this.w_MVTIPOPE = this.w_OBJDOC.ReadTable( "DES_DIVE" , "DDTIPOPE" , @ArrWhere )
                Release ArrWhere
                if this.w_DDTIPRIF<>"CO"
                  * --- Tipo sede diverso da Consegna
                  this.w_MVCODDES = SPACE(5)
                  this.w_MVTIPOPE = SPACE(10)
                endif
              endif
            endif
          else
            this.w_MVTIPCON = " "
            this.w_MVCODCON = SPACE(15)
            this.w_MVFLSCOR = " "
            this.w_ANTIPOPE = SPACE(10)
            this.w_MVCODDES = SPACE(5)
          endif
          * --- Data Registrazione
          this.w_MVDATREG = iif(not empty(this.w_ATDATDOC) and Isalt(), this.w_ATDATDOC, TTOD(this.w_ATDATFIN))
          this.w_MVDATDOC = iif(not empty(this.w_ATDATDOC), this.w_ATDATDOC, TTOD(this.w_ATDATFIN))
          if this.w_ISAHE
            this.w_MVCODBUN = this.w_ATCODBUN
            * --- Listini di Testata
            this.w_MVTCOLIS = this.w_ATTCOLIS
            this.w_MVTSCLIS = this.w_ATTSCLIS
            this.w_MVTPROLI = this.w_ATTPROLI
            this.w_MVTPROSC = this.w_ATTPROSC
            * --- Dati analitica di testata
            this.w_MVTCOCEN = this.w_ATCENRIC
            this.w_MVTCOMME = this.w_ATCOMRIC
            this.w_MVTCOATT = this.w_ATATTRIC
            this.w_MVTINCOM = this.w_ATINIRIC
            this.w_MVTFICOM = this.w_ATFINRIC
            this.w_OBJDOC.cCencosto = this.w_ATCENCOS
            this.w_OBJDOC.cComcosto = this.w_ATCODPRA
            this.w_OBJDOC.cAttcosto = this.w_ATATTCOS
            this.w_OBJDOC.dIniCom = this.w_ATINICOS
            this.w_OBJDOC.dFinCom = this.w_ATFINCOS
          else
            * --- Listino da Attivit�
            this.w_MVTCOLIS = this.w_ATCODLIS
          endif
          * --- Valuta documento da Attvivit�
          this.w_MVCODVAL = this.w_ATCODVAL
          * --- Documento Confermato
          this.w_MVFLPROV = "N"
          * --- Aggiorna progressivo Numero Documento
          this.w_MV__NOTE = this.w_ATNOTPIA
          OLD_ERR= ON("ERROR")
          ON ERROR AssVar=.T.
          this.w_OBJDOC.SetObjectValues(This)     
          if this.w_CAFLANAL = "N" AND !EMPTY(this.w_CACAUACQ) AND this.w_ISAHR
            this.w_OBJDOCACQ.SetObjectValues(This)     
            this.w_OBJDOCACQ.w_MVTIPDOC = this.w_CACAUACQ
            this.w_OBJDOCACQ.w_MVFLINTE = "N"
            this.w_OBJDOCACQ.w_MVTIPCON = " "
            this.w_OBJDOCACQ.w_MVCODCON = space(15)
            this.w_OBJDOCACQ.w_MVCODDES = space(5)
            this.w_OBJDOCACQ.w_ANTIPOPE = space(10)
            this.w_OBJDOCACQ.w_MVTCOLIS = this.w_ATLISACQ
            this.w_OBJDOCACQ.w_MVFLSCOR = " "
          endif
          ON ERROR &OLD_ERR
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          if this.w_bUnderTran
            * --- begin transaction
            cp_BeginTrs()
          endif
          if this.w_BCREADOCVEN or (this.w_BCREADOCACQ and this.w_ISAHE)
            this.w_PAG5TYPE = "V"
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if this.w_bUnderTran AND (!this.w_ISAHR OR (this.w_ISAHR AND !this.w_BCREADOCACQ ))
            if EMPTY(this.w_RETVAL)
              * --- commit
              cp_EndTrs(.t.)
            else
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
            endif
          endif
        endif
        if this.w_OK AND !this.w_ISAHE
          if this.w_FLANAL="S"
            * --- Genera movimento di analitica 
            this.w_RETVAL = gsag_bga(This,this.w_ATSERIAL,this.w_ATRIFMOV,"E")
            if EMPTY(this.w_RETVAL)
              * --- commit
              cp_EndTrs(.t.)
            else
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
            endif
          else
            if this.w_BCREADOCACQ AND this.w_CAFLANAL = "N" AND !EMPTY(this.w_CACAUACQ) AND this.w_ISAHR
              this.w_PAG5TYPE = "A"
              this.w_MVSERIAL = SPACE(10)
              this.Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_bUnderTran
                if EMPTY(this.w_RETVAL)
                  * --- commit
                  cp_EndTrs(.t.)
                else
                  * --- rollback
                  bTrsErr=.t.
                  cp_EndTrs(.t.)
                endif
              endif
            endif
          endif
        endif
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Righe di dettaglio
    this.w_CPROWNUM = 0
    if this.w_ISALT
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\AGEN\EXE\QUERY\GSAG1BGE',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    this.w_ROWVEN = 0
    this.w_ROWACQ = 0
    * --- Select from OFFDATTI
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFFDATTI ";
          +" where DASERIAL="+cp_ToStrODBC(this.pSERATT)+" and (("+cp_ToStrODBC(this.pTIPDOC)+" <> 'N' and (DATIPRIG='D' or DATIPRIG='A' or DATIPRIG='E')) or ("+cp_ToStrODBC(this.pTIPDOC)+" = 'N' and DATIPRI2='D'))";
          +" order by CPROWORD";
           ,"_Curs_OFFDATTI")
    else
      select * from (i_cTable);
       where DASERIAL=this.pSERATT and ((this.pTIPDOC <> "N" and (DATIPRIG="D" or DATIPRIG="A" or DATIPRIG="E")) or (this.pTIPDOC = "N" and DATIPRI2="D"));
       order by CPROWORD;
        into cursor _Curs_OFFDATTI
    endif
    if used('_Curs_OFFDATTI')
      select _Curs_OFFDATTI
      locate for 1=1
      do while not(eof())
      if this.pTIPDOC <> "N" and Nvl(_Curs_OFFDATTI.DATIPRIG," ")="D"
        this.w_MVSERIAL = "0000000001"
        this.w_ROWVEN = this.w_ROWVEN +10
        this.w_CPROWORD = this.w_ROWVEN
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_BCREADOCVEN = .T.
      endif
      if this.pTIPDOC = "N" and Nvl(_Curs_OFFDATTI.DATIPRI2," ")="D"
        this.w_MVSERIAL = "0000000001"
        this.w_ROWVEN = this.w_ROWVEN +10
        this.w_CPROWORD = this.w_ROWVEN
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_BCREADOCVEN = .T.
      endif
      if Nvl(_Curs_OFFDATTI.DATIPRIG," ")="E"
        this.w_MVSERIAL = "0000000001"
        this.w_ROWVEN = this.w_ROWVEN +10
        this.w_CPROWORD = this.w_ROWVEN
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_BCREADOCVEN = .T.
        if this.w_ISAHE OR (this.w_CAFLANAL <> "S" AND !EMPTY(this.w_CACAUACQ) AND this.w_ISAHR)
          this.w_MVSERIAL = "0000000002"
          this.w_ROWACQ = this.w_ROWACQ + 10
          this.w_CPROWORD = this.w_ROWACQ
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_BCREADOCACQ = .T.
        endif
      endif
      if Nvl(_Curs_OFFDATTI.DATIPRIG," ")="A"
        this.w_MVSERIAL = "0000000002"
        this.w_ROWACQ = this.w_ROWACQ + 10
        this.w_CPROWORD = this.w_ROWACQ
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_BCREADOCACQ = .T.
      endif
        select _Curs_OFFDATTI
        continue
      enddo
      use
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione Variabili
    * --- (Import) Variabili per il dettaglio del documento
    * --- Variabile per scrittura sul dettaglio attivit� della chiave di riga del documento interno
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiave di Riga
    this.w_CPROWNUM = NVL(_Curs_OFFDATTI.CPROWNUM,0)
    * --- Numero Riga
    this.w_MVNUMRIF = -20
    * --- Chiave di riga del dettaglio attivit�
    this.w_CHIRIG = NVL(_Curs_OFFDATTI.CPROWNUM,0)
    * --- Prestazione
    * --- Lettura dai codici di ricerca dell'Articolo (Prestazione) e tipologia riga
    this.w_MVCODCLA = "   "
    this.w_MVDATGEN = .NULL.
    this.w_MVEFFEVA = .NULL.
    this.w_MVDATEVA = .NULL.
    this.w_MVDATRCO = .NULL.
    * --- Descrizione
    this.w_MVDESART = NVL(_Curs_OFFDATTI.DADESATT, SPACE(40))
    * --- Unit� di Misura
    this.w_MVUNIMIS = NVL(_Curs_OFFDATTI.DAUNIMIS, SPACE(3))
    * --- Descrizione aggiuntiva
    this.w_MVDESSUP = NVL(_Curs_OFFDATTI.DADESAGG, " ")
    * --- Causale magazzino (uguale a quella di testata - da causale documento - deve essere 'NULL')
    this.w_MVCAUMAG = this.w_MVTCAMAG
    this.w_MVQTAMOV = NVL(_Curs_OFFDATTI.DAQTAMOV, 0)
    this.w_MVVALRIG = NVL(_Curs_OFFDATTI.DAVALRIG, 0)
    if this.w_PARAME>0
      this.w_MVPREZZO = (NVL(_Curs_OFFDATTI.DAPREZZO, 0)*this.w_PARAME)/100
      this.w_TOTCALC = 0
      if this.pFLRESTO
        * --- Select from GSAGRBGE
        do vq_exec with 'GSAGRBGE',this,'_Curs_GSAGRBGE','',.f.,.t.
        if used('_Curs_GSAGRBGE')
          select _Curs_GSAGRBGE
          locate for 1=1
          do while not(eof())
          this.w_TOTCALC = Nvl(_Curs_GSAGRBGE.TOTCALC,0)
          this.w_MVPREZZO = this.w_MVPREZZO + ((Nvl(_Curs_GSAGRBGE.PREZZO,0)-this.w_TOTCALC)/Nvl(_Curs_GSAGRBGE.DAQTAMOV,0))
            select _Curs_GSAGRBGE
            continue
          enddo
          use
        endif
      endif
    else
      this.w_MVPREZZO = NVL(_Curs_OFFDATTI.DAPREZZO, 0)
    endif
    if this.w_ISAHE
      this.w_MVCODICE = NVL(_Curs_OFFDATTI.DACODICE, SPACE(41))
      this.w_MVDAINCO = _Curs_OFFDATTI.DAINICOM
      this.w_MVDAFICO = _Curs_OFFDATTI.DAFINCOM
    else
      this.w_MVTIPPR2 = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
      this.w_MVCODICE = NVL(_Curs_OFFDATTI.DACODATT, SPACE(20))
      this.w_MVRIFPRE = NVL(_Curs_OFFDATTI.DARIFPRE, 0)
      this.w_MVRIGPRE = NVL(_Curs_OFFDATTI.DARIGPRE, "N")
    endif
    if Not Empty(this.w_MVCODICE)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "ARCODART"
      ArrWhere(1,2) = this.w_MVCODICE
      this.w_MVCODIVA = this.w_OBJDOC.ReadTable( "ART_ICOL" , "ARCODIVA" , @ArrWhere )
      this.w_ARTIPOPE = this.w_OBJDOC.ReadTable( "ART_ICOL" , "ARTIPOPE" , @ArrWhere )
      Release ArrWhere
    endif
    if this.w_MVSERIAL="0000000001"
      this.w_MVCODIVA = CALCODIV(this.w_MVCODICE, this.w_MVTIPCON, this.w_MVCODCON, this.w_MVTIPOPE, this.w_MVDATREG , this.w_ANTIPOPE, this.w_MVCODIVA, this.w_ARTIPOPE)
    else
      this.w_MVCODIVA = CALCODIV(this.w_MVCODICE, " ", space(15), space(10), this.w_MVDATREG , space(10), this.w_MVCODIVA, this.w_ARTIPOPE)
    endif
    this.w_MVQTAUM1 = 0
    this.w_MVVALMAG = this.w_MVVALRIG
    this.w_MVIMPNAZ = this.w_MVVALRIG
    this.w_MVFLOMAG = "X"
    * --- Dati per l'analitica
    * --- Seriale 0000000002 finto per documento di acquisto ahe / ahr
    if this.w_ISAHE
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZFLGPNA"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZFLGPNA;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLGPNA = NVL(cp_ToDate(_read_.AZFLGPNA),cp_NullValue(_read_.AZFLGPNA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_FLGPNA = " "
    endif
    if this.w_MVSERIAL="0000000002"
      this.w_MVCODCEN = NVL(_Curs_OFFDATTI.DACENCOS, SPACE(15))
      this.w_MVCODATT = NVL(_Curs_OFFDATTI.DAATTIVI, SPACE(15))
      this.w_MVVOCCEN = NVL(_Curs_OFFDATTI.DAVOCCOS, SPACE(15))
      this.w_MVCODCOM = NVL(_Curs_OFFDATTI.DACODCOM, SPACE(15))
      this.w_MVINICOM = iif(this.w_FLGPNA = "S", this.w_MVDAINCO, _Curs_OFFDATTI.DATRIINI) 
      this.w_MVFINCOM = iif(this.w_FLGPNA = "S", this.w_MVDAFICO, _Curs_OFFDATTI.DATRIFIN) 
      this.w_MVPREZZO = IIF(NVL(_Curs_OFFDATTI.DAQTAMOV, 0)>0,NVL(_Curs_OFFDATTI.DACOSINT, 0)/NVL(_Curs_OFFDATTI.DAQTAMOV, 0),0)
      this.w_MVCODLIS = this.w_ATLISACQ
      this.w_MVSCOLIS = SPACE(5)
      this.w_MVPROLIS = SPACE(5)
      this.w_MVPROSCO = SPACE(5)
      this.w_MVSCONT1 = 0
      this.w_MVSCONT2 = 0
      this.w_MVSCONT3 = 0
      this.w_MVSCONT4 = 0
    else
      if this.w_ISAHE OR (this.w_CAFLANAL = "N" AND !EMPTY(this.w_CACAUACQ) AND this.w_ISAHR)
        this.w_MVCODCEN = NVL(_Curs_OFFDATTI.DACENRIC, SPACE(15))
        this.w_MVCODATT = NVL(_Curs_OFFDATTI.DAATTRIC, SPACE(15))
        this.w_MVCODCOM = NVL(_Curs_OFFDATTI.DACOMRIC, SPACE(15))
        this.w_MVVOCCEN = NVL(_Curs_OFFDATTI.DAVOCRIC, SPACE(15))
      else
        this.w_MVCODCEN = NVL(_Curs_OFFDATTI.DACENCOS, SPACE(15))
        if this.w_VOCECR="C"
          this.w_MVVOCCEN = NVL(_Curs_OFFDATTI.DAVOCCOS, SPACE(15))
        else
          this.w_MVVOCCEN = NVL(_Curs_OFFDATTI.DAVOCRIC, SPACE(15))
        endif
        this.w_MVCODATT = NVL(_Curs_OFFDATTI.DAATTIVI, SPACE(15))
        if this.w_ISALT
          this.w_MVCODCOM = this.w_ATCODPRA
        else
          this.w_MVCODCOM = NVL(_Curs_OFFDATTI.DACODCOM, SPACE(15))
        endif
      endif
      this.w_MVINICOM = iif(this.w_FLGPNA = "S", this.w_MVDAINCO, _Curs_OFFDATTI.DATCOINI) 
      this.w_MVFINCOM = iif(this.w_FLGPNA = "S", this.w_MVDAFICO, _Curs_OFFDATTI.DATCOFIN) 
      this.w_MVCODLIS = NVL(_Curs_OFFDATTI.DACODLIS, SPACE(5))
      this.w_MVSCOLIS = NVL(_Curs_OFFDATTI.DASCOLIS, SPACE(5))
      this.w_MVPROLIS = NVL(_Curs_OFFDATTI.DAPROLIS, SPACE(5))
      this.w_MVPROSCO = NVL(_Curs_OFFDATTI.DAPROSCO, SPACE(5))
      this.w_MVSCONT1 = NVL(_Curs_OFFDATTI.DASCONT1, 0)
      this.w_MVSCONT2 = NVL(_Curs_OFFDATTI.DASCONT2, 0)
      this.w_MVSCONT3 = NVL(_Curs_OFFDATTI.DASCONT3, 0)
      this.w_MVSCONT4 = NVL(_Curs_OFFDATTI.DASCONT4, 0)
    endif
    this.w_MVTIPATT = "A"
    * --- Commessa (Pratica)
    * --- Responsabile
    this.w_MVCODRES = _Curs_OFFDATTI.DACODRES
    * --- Ulteriori campi del dettaglio esterno
    this.w_DEOREEFF = NVL(_Curs_OFFDATTI.DAOREEFF, 0)
    this.w_DEMINEFF = NVL(_Curs_OFFDATTI.DAMINEFF, 0)
    this.w_DECOSINT = NVL(_Curs_OFFDATTI.DACOSINT, 0)
    this.w_DEPREMIN = NVL(_Curs_OFFDATTI.DAPREMIN, 0)
    this.w_DEPREMAX = NVL(_Curs_OFFDATTI.DAPREMAX, 0)
    this.w_DEGAZUFF = NVL(_Curs_OFFDATTI.DAGAZUFF, SPACE(6))
    * --- Legge tipo e materia pratica
    if Not Empty(this.w_MVCODCOM)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "CNCODCAN"
      ArrWhere(1,2) = this.w_MVCODCOM
      this.w_DETIPPRA = this.w_OBJDOC.ReadTable( "CAN_TIER" , "CNTIPPRA" , @ArrWhere )
      this.w_DEMATPRA = this.w_OBJDOC.ReadTable( "CAN_TIER" , "CNMATPRA" , @ArrWhere )
      this.w_DEPARASS = this.w_OBJDOC.ReadTable( "CAN_TIER" , "CNPARASS" , @ArrWhere )
      this.w_DECALDIR = this.w_OBJDOC.ReadTable( "CAN_TIER" , "CNCALDIR" , @ArrWhere )
      this.w_DECOECAL = this.w_OBJDOC.ReadTable( "CAN_TIER" , "CNCOECAL" , @ArrWhere )
      Release ArrWhere
    endif
    if !this.w_ISAHR OR (this.w_ISAHR AND this.w_MVSERIAL="0000000001")
      this.w_OBJDOC.Adddetail(this.w_CPROWNUM,this.w_CPROWORD,this.w_MVCODICE,this.w_MVUNIMIS,this.w_MVQTAMOV,this.w_MVPREZZO,this.w_MVTIPRIG)     
      this.w_OBJDOC.w_DOC_DETT.SetObjectValues(This)     
      this.w_OBJDOC.w_DOC_DETT.SaveCurrentRecord()     
    endif
    if this.w_CAFLANAL = "N" AND !EMPTY(this.w_CACAUACQ) AND this.w_MVSERIAL="0000000002" AND this.w_ISAHR
      this.w_OBJDOCACQ.Adddetail(this.w_CPROWNUM,this.w_CPROWORD,this.w_MVCODICE,this.w_MVUNIMIS,this.w_MVQTAMOV,this.w_MVPREZZO,this.w_MVTIPRIG)     
      this.w_OBJDOCACQ.w_DOC_DETT.SetObjectValues(This)     
      this.w_OBJDOCACQ.w_DOC_DETT.SaveCurrentRecord()     
    endif
    * --- Instanzio figlio ALT_DETT
    if !this.w_ISAHR OR (this.w_ISAHR AND this.w_MVSERIAL="0000000001")
      this.w_OBJDOC.w_ALT_DETT.AppendBlank()     
    endif
    if this.w_CAFLANAL = "N" AND !EMPTY(this.w_CACAUACQ) AND this.w_MVSERIAL="0000000002" AND this.w_ISAHR
      this.w_OBJDOCACQ.w_ALT_DETT.AppendBlank()     
    endif
    this.w_DESERIAL = this.w_MVSERIAL
    this.w_DEROWNUM = this.w_CPROWNUM
    this.w_DENUMRIF = -20
    * --- Memorizza dati su oggetto dettaglio attivit�
    if !this.w_ISAHR OR (this.w_ISAHR AND this.w_MVSERIAL="0000000001")
      this.w_OBJDOC.w_ALT_DETT.SetObjectValues(This)     
      this.w_OBJDOC.w_ALT_DETT.SaveCurrentRecord()     
    endif
    if this.w_CAFLANAL = "N" AND !EMPTY(this.w_CACAUACQ) AND this.w_MVSERIAL="0000000002" AND this.w_ISAHR
      this.w_OBJDOCACQ.w_ALT_DETT.SetObjectValues(This)     
      this.w_OBJDOCACQ.w_ALT_DETT.SaveCurrentRecord()     
    endif
    * --- inserisco riga per aggiornamento riferimento di riga delle prestazioni
    if this.w_ISALT
      * --- Insert into TMPVEND1
      i_nConn=i_TableProp[this.TMPVEND1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPVEND1_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DASERIAL"+",CPROWNUM"+",DARIFRIG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.pSERATT),'TMPVEND1','DASERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CHIRIG),'TMPVEND1','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TMPVEND1','DARIFRIG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DASERIAL',this.pSERATT,'CPROWNUM',this.w_CHIRIG,'DARIFRIG',this.w_CPROWNUM)
        insert into (i_cTable) (DASERIAL,CPROWNUM,DARIFRIG &i_ccchkf. );
           values (;
             this.pSERATT;
             ,this.w_CHIRIG;
             ,this.w_CPROWNUM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_PAG5TYPE="V"
      this.w_OBJDOC.bRicPro = .t.
      this.w_RETVAL = this.w_ObjDoc.CreateDocument()
    else
      this.w_ObjDocAcq.bRicPro = .t.
      this.w_RETVAL = this.w_ObjDocAcq.CreateDocument()
    endif
    this.w_OK = Empty(this.w_RETVAL)
    if this.w_PAG5TYPE="V"
      this.w_SERACQ = this.w_OBJDOC.cSerAcquisti
      this.w_MVSERIAL = this.w_OBJDOC.w_MVSERIAL
      this.w_OBJDOC = .null.
    else
      this.w_SERACQ = this.w_OBJDOCACQ.w_MVSERIAL
      this.w_MVSERIAL = this.w_OBJDOCACQ.w_MVSERIAL
      this.w_OBJDOCACQ = .null.
    endif
    if this.w_ATSTATUS="P" and this.w_ISALT AND this.w_PAG5TYPE<>"A" and this.w_FLGZER <>"S"
      * --- Try
      local bErr_04160B08
      bErr_04160B08=bTrsErr
      this.Try_04160B08()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_RETVAL = ah_MsgFormat("Errore cancellazione righe prestazioni .%0%1", MESSAGE())
        this.w_OK = .f.
      endif
      bTrsErr=bTrsErr or bErr_04160B08
      * --- End
    endif
    if this.w_ISALT AND this.w_PAG5TYPE<>"A"
      if this.w_OK 
        * --- Try
        local bErr_0415E2E8
        bErr_0415E2E8=bTrsErr
        this.Try_0415E2E8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_RETVAL = ah_MsgFormat("Errore scrittura riferimento documento riga prestazioni .%0%1", MESSAGE())
          this.w_OK = .f.
        endif
        bTrsErr=bTrsErr or bErr_0415E2E8
        * --- End
      endif
      * --- Drop temporary table TMPVEND1
      i_nIdx=cp_GetTableDefIdx('TMPVEND1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPVEND1')
      endif
    endif
    if this.w_OK
      * --- Try
      local bErr_04166D48
      bErr_04166D48=bTrsErr
      this.Try_04166D48()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_RETVAL = ah_MsgFormat("Errore nella scrittura  riferimento movimento di analitica.%0%1", MESSAGE())
        this.w_OK = .f.
      endif
      bTrsErr=bTrsErr or bErr_04166D48
      * --- End
    endif
  endproc
  proc Try_04160B08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from OFFDATTI
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      do vq_exec with 'GSAG_BAC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0415E2E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into OFFDATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
              +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DARIFRIG = _t2.DARIFRIG";
          +i_ccchkf;
          +" from "+i_cTable+" OFFDATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
              +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI, "+i_cQueryTable+" _t2 set ";
          +"OFFDATTI.DARIFRIG = _t2.DARIFRIG";
          +Iif(Empty(i_ccchkf),"",",OFFDATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
              +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI set ";
          +"DARIFRIG = _t2.DARIFRIG";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DARIFRIG = (select DARIFRIG from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore nella scrittura riferimento'
      return
    endif
    return
  proc Try_04166D48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorno riferimento documento di vendita
    * --- Select from GSAG_BGE
    do vq_exec with 'GSAG_BGE',this,'_Curs_GSAG_BGE','',.f.,.t.
    if used('_Curs_GSAG_BGE')
      select _Curs_GSAG_BGE
      locate for 1=1
      do while not(eof())
      this.w_MAXORD = Nvl(_Curs_GSAG_BGE.MAXORD,0)
        select _Curs_GSAG_BGE
        continue
      enddo
      use
    endif
    if Not Empty(this.w_MVSERIAL) AND this.w_PAG5TYPE<>"A"
      this.w_MAXORD = this.w_MAXORD+10
      * --- Insert into ATT_DCOL
      i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATT_DCOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ADSERIAL"+",ADSERDOC"+",CPROWORD"+",ADTIPDOC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_ATSERIAL),'ATT_DCOL','ADSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'ATT_DCOL','ADSERDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAXORD),'ATT_DCOL','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC("V"),'ATT_DCOL','ADTIPDOC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ADSERIAL',this.w_ATSERIAL,'ADSERDOC',this.w_MVSERIAL,'CPROWORD',this.w_MAXORD,'ADTIPDOC',"V")
        insert into (i_cTable) (ADSERIAL,ADSERDOC,CPROWORD,ADTIPDOC &i_ccchkf. );
           values (;
             this.w_ATSERIAL;
             ,this.w_MVSERIAL;
             ,this.w_MAXORD;
             ,"V";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if Not Empty(this.w_SERACQ)
      this.w_MAXORD = this.w_MAXORD+10
      * --- Insert into ATT_DCOL
      i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATT_DCOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ADSERIAL"+",ADSERDOC"+",CPROWORD"+",ADTIPDOC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_ATSERIAL),'ATT_DCOL','ADSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SERACQ),'ATT_DCOL','ADSERDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAXORD),'ATT_DCOL','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC("A"),'ATT_DCOL','ADTIPDOC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ADSERIAL',this.w_ATSERIAL,'ADSERDOC',this.w_SERACQ,'CPROWORD',this.w_MAXORD,'ADTIPDOC',"A")
        insert into (i_cTable) (ADSERIAL,ADSERDOC,CPROWORD,ADTIPDOC &i_ccchkf. );
           values (;
             this.w_ATSERIAL;
             ,this.w_SERACQ;
             ,this.w_MAXORD;
             ,"A";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"PRE_STAZ")
    if Not Empty(this.w_ATCODPRA)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "CNCODCAN"
      ArrWhere(1,2) = this.w_ATCODPRA
      this.w_DEPARASS = this.w_OBJ.ReadTable( "CAN_TIER" , "CNPARASS" , @ArrWhere )
      Release ArrWhere
      if this.w_DEPARASS > 1
        this.w_NUMSOGG = space(15)
        * --- Legge se c'� almeno un soggetto intestatario di parcelle all'interno della pratica
        * --- Read from RIS_ESTR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SRCODSES"+;
            " from "+i_cTable+" RIS_ESTR where ";
                +"SRCODPRA = "+cp_ToStrODBC(this.w_ATCODPRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SRCODSES;
            from (i_cTable) where;
                SRCODPRA = this.w_ATCODPRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NUMSOGG = NVL(cp_ToDate(_read_.SRCODSES),cp_NullValue(_read_.SRCODSES))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Select from GSAGZBGE
    do vq_exec with 'GSAGZBGE',this,'_Curs_GSAGZBGE','',.f.,.t.
    if used('_Curs_GSAGZBGE')
      select _Curs_GSAGZBGE
      locate for 1=1
      do while not(eof())
      this.w_OBJ.Blank()     
      this.w_ROWPRE = _Curs_GSAGZBGE.CPROWNUM
      this.w_OBJ.w_PRGRURES = .Null.
      this.w_OBJ.w_PRCODESE = g_CODESE
      this.w_OBJ.w_PR__DATA = iif(not empty(this.w_ATDATDOC), this.w_ATDATDOC, TTOD(this.w_ATDATFIN))
      this.w_OBJ.w_PRNUMPRA = this.w_ATCODPRA
      this.w_OBJ.w_PRCODVAL = this.w_ATCODVAL
      this.w_OBJ.w_PRCODLIS = this.w_ATCODLIS
      this.w_OBJ.w_PRCODNOM = this.w_ATCODNOM
      this.w_OBJ.w_PRCODSED = this.w_ATCODSED
      this.w_OBJ.w_PRTIPDOC = this.w_ATCAUDOC
      this.w_OBJ.w_PRCODATT = _Curs_GSAGZBGE.DACODATT
      this.w_OBJ.w_PRCODRES = _Curs_GSAGZBGE.DACODRES
      this.w_OBJ.w_PRCOSUNI = _Curs_GSAGZBGE.DACOSUNI
      this.w_OBJ.w_PRFLDEFF = _Curs_GSAGZBGE.DAFLDEFF
      this.w_OBJ.w_PRPREMIN = _Curs_GSAGZBGE.DAPREMIN
      this.w_OBJ.w_PRPREMAX = _Curs_GSAGZBGE.DAPREMAX
      this.w_OBJ.w_PRGAZUFF = _Curs_GSAGZBGE.DAGAZUFF
      this.w_OBJ.w_PRCENCOS = _Curs_GSAGZBGE.DACENRIC
      this.w_OBJ.w_PRVOCCOS = _Curs_GSAGZBGE.DAVOCRIC
      this.w_OBJ.w_PR_SEGNO = _Curs_GSAGZBGE.DA_SEGNO
      this.w_OBJ.w_PRATTIVI = _Curs_GSAGZBGE.DAATTIVI
      this.w_OBJ.w_PRINICOM = _Curs_GSAGZBGE.DAINICOM
      this.w_OBJ.w_PRFINCOM = _Curs_GSAGZBGE.DAFINCOM
      this.w_OBJ.w_PRPREZZO = _Curs_GSAGZBGE.DAPREZZO
      this.w_OBJ.w_PRDESPRE = NVL(_Curs_GSAGZBGE.DADESATT, SPACE(40))
      this.w_OBJ.w_PRDESAGG = NVL(_Curs_GSAGZBGE.DADESAGG, " ")
      this.w_OBJ.w_PRCODOPE = i_codute
      this.w_OBJ.w_PRUNIMIS = NVL(_Curs_GSAGZBGE.DAUNIMIS, SPACE(3))
      this.w_OBJ.w_PRQTAMOV = NVL(_Curs_GSAGZBGE.DAQTAMOV, 0)
      this.w_OBJ.w_PRVALRIG = NVL(_Curs_GSAGZBGE.DAVALRIG, 0)
      this.w_OBJ.w_PROREEFF = NVL(_Curs_GSAGZBGE.DAOREEFF, 0)
      this.w_OBJ.w_PRMINEFF = NVL(_Curs_GSAGZBGE.DAMINEFF, 0)
      this.w_OBJ.w_PRCOSINT = NVL(_Curs_GSAGZBGE.DACOSINT, 0)
      this.w_OBJ.w_PRQTAUM1 = NVL(_Curs_GSAGZBGE.DAQTAMOV, 0)
      this.w_OBJ.w_PRTIPRIG = _Curs_GSAGZBGE.DATIPRIG
      this.w_OBJ.w_PRTCOINI = _Curs_GSAGZBGE.DATCOINI
      this.w_OBJ.w_PRTCOFIN = _Curs_GSAGZBGE.DATCOFIN
      this.w_OBJ.w_PRTIPRI2 = _Curs_GSAGZBGE.DATIPRI2
      this.w_OBJ.w_PRRIGPRE = _Curs_GSAGZBGE.DARIGPRE
      this.w_OBJ.w_PRRIFPRE = _Curs_GSAGZBGE.DARIFPRE
      this.w_OBJ.w_PRSERATT = this.w_ATSERIAL
      this.w_OBJ.w_PRROWATT = NVL(_Curs_GSAGZBGE.CPROWNUM, 0)
      this.w_OBJ.nIMPSPE = 0
      this.w_OBJ.nIMPANT = 0
      if Nvl(_Curs_GSAGZBGE.DARIFPRE,0)>0
        this.w_OBJ.w_PRSERAGG = this.w_PRSERAGG
      endif
      this.w_OBJ.bParass = iif(this.w_DEPARASS>1 AND NOT EMPTY(this.w_NUMSOGG),.t.,.f.)
      this.w_RETVAL = this.w_OBJ.CreatePrest()
      if Not Empty(this.w_RETVAL)
        exit
      else
        if this.w_VISPRESTAZ = "S" AND this.pVISPRE
          * --- Insert into TMP_CERT
          i_nConn=i_TableProp[this.TMP_CERT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_CERT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_CERT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PRSERIAL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_OBJ.w_PRSERIAL),'TMP_CERT','PRSERIAL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PRSERIAL',this.w_OBJ.w_PRSERIAL)
            insert into (i_cTable) (PRSERIAL &i_ccchkf. );
               values (;
                 this.w_OBJ.w_PRSERIAL;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.NUM_PRE = this.NUM_PRE + 1
        endif
      endif
      this.w_PRSERIAL = this.w_OBJ.w_PRSERIAL
      if Nvl(_Curs_GSAGZBGE.DARIGPRE," ")="S"
        this.w_PRSERAGG = this.w_PRSERIAL
      endif
        select _Curs_GSAGZBGE
        continue
      enddo
      use
    endif
    this.w_OBJ = .Null.
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pSERATT,pSERDOC,pCODNOM,pPARAME,pFLRESTO,pTIPDOC,pVISPRE)
    this.pSERATT=pSERATT
    this.pSERDOC=pSERDOC
    this.pCODNOM=pCODNOM
    this.pPARAME=pPARAME
    this.pFLRESTO=pFLRESTO
    this.pTIPDOC=pTIPDOC
    this.pVISPRE=pVISPRE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='OFF_ATTI'
    this.cWorkTables[3]='OFFDATTI'
    this.cWorkTables[4]='MOVICOST'
    this.cWorkTables[5]='*TMPVEND1'
    this.cWorkTables[6]='ATT_DCOL'
    this.cWorkTables[7]='PAR_PRAT'
    this.cWorkTables[8]='CAUMATTI'
    this.cWorkTables[9]='AZIENDA'
    this.cWorkTables[10]='PAR_ALTE'
    this.cWorkTables[11]='*TMP_CERT'
    this.cWorkTables[12]='RIS_ESTR'
    return(this.OpenAllTables(12))

  proc CloseCursors()
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs_GSAGRBGE')
      use in _Curs_GSAGRBGE
    endif
    if used('_Curs_GSAG_BGE')
      use in _Curs_GSAG_BGE
    endif
    if used('_Curs_GSAGZBGE')
      use in _Curs_GSAGZBGE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERATT,pSERDOC,pCODNOM,pPARAME,pFLRESTO,pTIPDOC,pVISPRE"
endproc
