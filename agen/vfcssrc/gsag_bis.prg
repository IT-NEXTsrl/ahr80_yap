* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bis                                                        *
*              Export\import attivit�                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-09                                                      *
* Last revis.: 2014-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bis",oParentObject,m.pEXEC)
return(i_retval)

define class tgsag_bis as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_DATSTAMP = ctot("")
  w_OBJCAL = .NULL.
  w_OBJCALEVE = .NULL.
  w_FILE = space(254)
  w_ELAB = space(254)
  w_CodPraAttivita = space(15)
  w_CodiceNominativo = space(15)
  w_CAUATT = space(20)
  w_NoteAttivita = space(0)
  w_ATSERIAL = space(10)
  w_DescrizNominativo = space(60)
  w_nomecompleto = space(200)
  w_ATNUMPRI = 0
  w_NoteAppICS = space(10)
  w_Textapp = space(254)
  w_MEMO = space(150)
  w_LIST = space(100)
  w_OBJ = .NULL.
  w_OBJCALIM = .NULL.
  w_NUMEVE = 0
  w_UID = space(150)
  w_DATFIN = ctot("")
  w_DTSTAMP = ctot("")
  w_DATINI = ctot("")
  w_SUMMARY = space(254)
  w_LOCATION = space(50)
  w_PRIORITY = 0
  w_DESCRIPTION = space(254)
  w_CATEGORIES = space(50)
  w_RESULT = space(254)
  w_OCCURNOTE = space(254)
  w_CODCAT = space(1)
  w_CODCA2 = space(1)
  w_CODCA1 = space(1)
  w_STATO = space(1)
  w_OSTATO = space(1)
  w_FLGIGN = space(1)
  w_OBJ = .NULL.
  w_OBJECT = .NULL.
  * --- WorkFile variables
  DET_EXPO_idx=0
  DET_IMPO_idx=0
  PRO_CATE_idx=0
  OFF_ATTI_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da GSAG_KIS\GSAG_KIV
    * --- I import
    *     E export
    this.w_DATSTAMP = Datetime()
     
 this.oParentObject.bupdated=.t.
    if NOT this.oParentObject.CheckForm()
      i_retcode = 'stop'
      return
    endif
    this.w_ELAB = .F.
    if this.pEXEC="E"
      this.w_FILE = IIF(Empty(Justext(this.oParentObject.w_FILEXP)),Alltrim(this.oParentObject.w_FILEXP)+iif(this.oParentObject.w_PR__TIPO="I",".ICS",".VCS"),this.oParentObject.w_FILEXP)
      * --- Se export, allora w_ELAB vale .T. solo se � stata inclusa almeno un'attivit� da esportare
      NC=(this.oParentObject.w_AGKRA_ZOOM.ccursor) 
       
 Select ATSERIAL,ATDATINI AS DTSTART,ATDATFIN AS DTEND,CODUID as CODUID,ATLOCALI AS LOCATION,ATOGGETT AS SUMMARY,ATNUMPRI AS PRIORITY,ATNOTPIA AS DESCRIPTION,; 
 this.w_datstamp as DTSTAMP, PCCATEGO as CATEGORIES FROM (NC) WHERE XCHK=1 into cursor TMP_EXP
      if Reccount("TMP_EXP")=0
        Ah_errormsg("Attenzione nessuna attivit� selezionata",48)
         
 Use in TMP_EXP
        i_retcode = 'stop'
        return
      endif
    else
      if !Cp_fileexist(this.oParentObject.w_PRPATHIM) or Empty(this.oParentObject.w_PRPATHIM)
        Ah_errormsg("Il file indicato non � valido")
        i_retcode = 'stop'
        return
      endif
      Vq_exec("Query\GSAG_KIV.vqr",this,"TMP_EXP")
      this.w_FILE = ADDBS(JUSTPATH(this.oParentObject.w_filexp)) + Sys(2015) + iif(this.oParentObject.w_PR__TIPO="I",".ICS",".VCS")
      * --- Se import, allora w_ELAB vale sempre .T.
      this.w_ELAB = .T.
    endif
    this.w_OBJCAL = Createobject("iCalCalendar")
    this.w_OBJCAL.New()     
    if Reccount("TMP_EXP")>0
      * --- **(tDtStart, tDtEnd, tDtStamp, cSummary, cDescription, cLocation, cFbType, cCategories, nPriority)
      Select TMP_EXP 
 Go top 
 Scan
      this.w_CODCAT = NVL(CATEGORIES,"")
      this.w_OBJCALEVE = Createobject("iCalEvent")
      this.w_OBJCALEVE.New(Alltrim(CODUID))     
      * --- Applico trascodifica categotrie se previsto nel profilo
      if Not Empty(this.w_CODCAT)
        if Isalt()
          * --- Read from PRO_CATE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRO_CATE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRO_CATE_idx,2],.t.,this.PRO_CATE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PCCATEGO"+;
              " from "+i_cTable+" PRO_CATE where ";
                  +"PCSERPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
                  +" and PCCODCA2 = "+cp_ToStrODBC(this.w_CODCAT);
                  +" and PCTIPTRA = "+cp_ToStrODBC("E");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PCCATEGO;
              from (i_cTable) where;
                  PCSERPRO = this.oParentObject.w_SERPRO;
                  and PCCODCA2 = this.w_CODCAT;
                  and PCTIPTRA = "E";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_Categories = NVL(cp_ToDate(_read_.PCCATEGO),cp_NullValue(_read_.PCCATEGO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from PRO_CATE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRO_CATE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRO_CATE_idx,2],.t.,this.PRO_CATE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PCCATEGO"+;
              " from "+i_cTable+" PRO_CATE where ";
                  +"PCSERPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
                  +" and PCCODCAT = "+cp_ToStrODBC(this.w_CODCAT);
                  +" and PCTIPTRA = "+cp_ToStrODBC("E");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PCCATEGO;
              from (i_cTable) where;
                  PCSERPRO = this.oParentObject.w_SERPRO;
                  and PCCODCAT = this.w_CODCAT;
                  and PCTIPTRA = "E";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_Categories = NVL(cp_ToDate(_read_.PCCATEGO),cp_NullValue(_read_.PCCATEGO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
      * --- 1 -> 1 Normale
      *     3 ->5 Urgente
      *     4 ->9 Scadenza termine
      Select TMP_EXP
      this.w_ATSERIAL = ATSERIAL
      * --- Read from OFF_ATTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCODPRA,ATCODNOM,ATCAUATT,ATNOTPIA"+;
          " from "+i_cTable+" OFF_ATTI where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCODPRA,ATCODNOM,ATCAUATT,ATNOTPIA;
          from (i_cTable) where;
              ATSERIAL = this.w_ATSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CodPraAttivita = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
        this.w_CodiceNominativo = NVL(cp_ToDate(_read_.ATCODNOM),cp_NullValue(_read_.ATCODNOM))
        this.w_CAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
        this.w_NoteAttivita = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if !EMPTY(this.w_CodiceNominativo)
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NODESCRI"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_CodiceNominativo);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NODESCRI;
            from (i_cTable) where;
                NOCODICE = this.w_CodiceNominativo;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DescrizNominativo = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_nomecompleto = alltrim(this.w_CodiceNominativo)+" "+this.w_DescrizNominativo
      endif
      Select TMP_EXP
      this.w_ATNUMPRI = icase(Nvl(PRIORITY,0)=1,1,Nvl(PRIORITY,0)=3 ,5,9)
      this.w_NoteAppICS = GSAG_BEX(this,"COMPONINOTE",ATSERIAL,this.w_CodPraAttivita,DTSTART, DTEND , SUMMARY , this.w_CAUATT,this.w_nomecompleto, this.w_NoteAttivita,"I")
      this.w_Textapp = SUBSTR(this.w_NoteAppICS,1, LEN(this.w_NoteAppICS)-2 )
      this.w_OBJCALEVE.SetInfo(DTSTART, DTEND, DTSTAMP, Alltrim(SUMMARY), Alltrim(Nvl(this.w_TEXTAPP," ")),alltrim(LOCATION), "BUSY",alltrim(this.w_CATEGORIES), this.w_ATNUMPRI)     
      this.w_OBJCAL.aVEvent.Add(this.w_OBJCALEVE, this.w_OBJCALEVE.UID)     
      this.w_MEMO = this.w_OBJCALEVE.SaveEvent()
      if this.pEXEC="E"
        * --- Try
        local bErr_00E0B090
        bErr_00E0B090=bTrsErr
        this.Try_00E0B090()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Write into DET_EXPO
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DET_EXPO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_EXPO_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DET_EXPO_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CO__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_MEMO),'DET_EXPO','CO__NOTE');
                +i_ccchkf ;
            +" where ";
                +"COSERPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
                +" and COSERATT = "+cp_ToStrODBC(ATSERIAL);
                +" and COSEREVE = "+cp_ToStrODBC(CODUID);
                   )
          else
            update (i_cTable) set;
                CO__NOTE = this.w_MEMO;
                &i_ccchkf. ;
             where;
                COSERPRO = this.oParentObject.w_SERPRO;
                and COSERATT = ATSERIAL;
                and COSEREVE = CODUID;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_00E0B090
        * --- End
      endif
       
 Endscan
      this.w_ELAB = .T.
    endif
    if this.w_ELAB
      this.w_OBJCAL.SaveIcsFile(Alltrim(this.w_FILE))     
    endif
    if this.pEXEC="I"
      this.w_OBJCALIM = Createobject("iCalCalendar")
      this.w_OBJCALIM.LoadIcsFile(Alltrim(this.oParentObject.w_PRPATHIM))     
      this.w_LIST = "DTSTART, DTEND, SUMMARY, LOCATION"
      this.w_OBJCAL.Compare(this.w_OBJCALIM, this.w_LIST, "TMP_RESU")     
      this.w_NUMEVE = this.w_OBJCALIM.AVEVENT.Count
      do while this.w_NUMEVE>0
        this.w_OBJ = this.w_OBJCALIM.AVEVENT.Item(this.w_NUMEVE)
        this.w_UID = this.w_OBJ.UID
        this.w_DATINI = this.w_OBJ.GETVALUEBYPROP("DTSTART")
        this.w_DATFIN = this.w_OBJ.GETVALUEBYPROP("DTEND")
        this.w_SUMMARY = Left(this.w_OBJ.GETVALUEBYPROP("SUMMARY", ""),254)
        this.w_LOCATION = Left(this.w_OBJ.GETVALUEBYPROP("LOCATION", ""),30)
        this.w_PRIORITY = this.w_OBJ.GETVALUEBYPROP("PRIORITY")
        this.w_RESULT = this.w_OBJ.GETVALUEBYPROP("DESCRIPTION", "")
        this.w_RESULT = STRCONV(STRCONV(this.w_RESULT,11),2)
        this.w_OCCURNOTE = OCCUR("Note:",this.w_RESULT)
        if this.w_OCCURNOTE>0
          this.w_RESULT = SUBSTR(this.w_RESULT,AT("Note:",this.w_RESULT)+5)
        endif
        this.w_DESCRIPTION = this.w_RESULT
        this.w_CATEGORIES = this.w_OBJ.GETVALUEBYPROP("CATEGORIES", "")
        this.w_DTSTAMP = this.w_OBJ.GETVALUEBYPROP("DTSTAMP")
        this.w_CODCAT = ""
        if this.w_DATINI>=this.oParentObject.w_DATA_INI
          if Not Empty(this.w_CATEGORIES)
            * --- Applico trascodifica categotrie se previsto nel profilo
            * --- Read from PRO_CATE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PRO_CATE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRO_CATE_idx,2],.t.,this.PRO_CATE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PCCODCAT,PCCODCA2"+;
                " from "+i_cTable+" PRO_CATE where ";
                    +"PCSERPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
                    +" and PCCATEGO = "+cp_ToStrODBC(this.w_CATEGORIES);
                    +" and PCTIPTRA = "+cp_ToStrODBC("I");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PCCODCAT,PCCODCA2;
                from (i_cTable) where;
                    PCSERPRO = this.oParentObject.w_SERPRO;
                    and PCCATEGO = this.w_CATEGORIES;
                    and PCTIPTRA = "I";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCA1 = NVL(cp_ToDate(_read_.PCCODCAT),cp_NullValue(_read_.PCCODCAT))
              this.w_CODCA2 = NVL(cp_ToDate(_read_.PCCODCA2),cp_NullValue(_read_.PCCODCA2))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_CODCAT = IIF(Isalt(),this.w_CODCA2,this.w_CODCA1)
          endif
          * --- Try
          local bErr_00CA3200
          bErr_00CA3200=bTrsErr
          this.Try_00CA3200()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_00CA3200
          * --- End
        endif
        this.w_NUMEVE = this.w_NUMEVE - 1
      enddo
      if this.oParentObject.w_ESCIGN="S"
        * --- Delete from DET_IMPO
        i_nConn=i_TableProp[this.DET_IMPO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".PICODPRO = "+i_cQueryTable+".PICODPRO";
                +" and "+i_cTable+".PICODUID = "+i_cQueryTable+".PICODUID";
        
          do vq_exec with 'gsag1kiv',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Compare(vCal, cListPropToCompare, cRetCursor)
       
 SELECT TMP_EXP.*, TMP_RESU.* FROM TMP_RESU ; 
 LEFT OUTER JOIN TMP_EXP ON (TMP_RESU.Veventuid=TMP_EXP.CODUID) INTO CURSOR TMP_TOT
      Select TMP_TOT 
 Go top 
 Scan for Not Empty(Veventstatus)
      this.w_DATINI = DTSTART
      this.w_DATFIN = DTEND
      this.w_SUMMARY = SUMMARY
      this.w_LOCATION = LOCATION
      this.w_PRIORITY = PRIORITY
      this.w_DESCRIPTION = DESCRIPTION
      this.w_CATEGORIES = CATEGORIES
      this.w_DTSTAMP = DTSTAMP
      this.w_UID = Veventuid
      this.w_STATO = icase(Veventstatus="A","N",Veventstatus="R","C","M")
      * --- Per mantenere stato escluse
      if this.w_STATO="C"
        * --- Try
        local bErr_00D56578
        bErr_00D56578=bTrsErr
        this.Try_00D56578()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_00D56578
        * --- End
      else
        * --- Read from DET_IMPO
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DET_IMPO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2],.t.,this.DET_IMPO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PI_STATO"+;
            " from "+i_cTable+" DET_IMPO where ";
                +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
                +" and PICODUID = "+cp_ToStrODBC(this.w_UID);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PI_STATO;
            from (i_cTable) where;
                PICODPRO = this.oParentObject.w_SERPRO;
                and PICODUID = this.w_UID;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OSTATO = NVL(cp_ToDate(_read_.PI_STATO),cp_NullValue(_read_.PI_STATO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows<>0 and this.w_OSTATO="E"
          this.w_STATO = this.w_OSTATO
        endif
        * --- Try
        local bErr_00D67D00
        bErr_00D67D00=bTrsErr
        this.Try_00D67D00()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_00D67D00
        * --- End
      endif
      Endscan
      Use in TMP_RESU 
 Use in TMP_TOT
      if !(Vartype(g_NODEL)="L" and g_NODEL)
         
 Delete File (Alltrim(this.w_FILE))
      endif
      * --- Elimino record da Ignorare
      * --- Delete from DET_IMPO
      i_nConn=i_TableProp[this.DET_IMPO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
              +" and PI_STATO = "+cp_ToStrODBC("K");
               )
      else
        delete from (i_cTable) where;
              PICODPRO = this.oParentObject.w_SERPRO;
              and PI_STATO = "K";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
     
 Use in TMP_EXP
    if this.w_ELAB
      * --- w_ELAB vale .T.
      *     - nel caso di import (sempre)
      *     - nel caso di export se � stata inclusa almeno un'attivit� da esportare
      Ah_errormsg("Elaborazione terminata","!")
      if this.pEXEC="I"
        this.w_OBJ = Gsag_kii()
        this.w_OBJECT = this.w_OBJ.getctrl("w_SERPRO")
        this.w_OBJECT.Value = this.oParentObject.w_SERPRO
        this.w_OBJECT.Valid()     
        this.w_OBJ.w_SERPRO = this.oParentObject.w_SERPRO
        this.w_OBJ.Notifyevent("w_SERPRO Changed")     
        this.w_OBJ.Refresh()     
        this.w_OBJ = null
        this.w_OBJECT = .Null.
      endif
    else
      Ah_errormsg("Per generare il file deve essere inclusa almeno un'attivit� da esportare","!")
    endif
    this.w_OBJ = .Null.
    this.w_OBJCALIM = .null.
    this.w_OBJCAL = .Null.
    this.w_OBJCALEVE = .Null.
  endproc
  proc Try_00E0B090()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DET_EXPO
    i_nConn=i_TableProp[this.DET_EXPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_EXPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_EXPO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"COSERPRO"+",COSERATT"+",COSEREVE"+",CODATESP"+",CO__NOTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERPRO),'DET_EXPO','COSERPRO');
      +","+cp_NullLink(cp_ToStrODBC(ATSERIAL),'DET_EXPO','COSERATT');
      +","+cp_NullLink(cp_ToStrODBC(CODUID),'DET_EXPO','COSEREVE');
      +","+cp_NullLink(cp_ToStrODBC(DTSTAMP),'DET_EXPO','CODATESP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MEMO),'DET_EXPO','CO__NOTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'COSERPRO',this.oParentObject.w_SERPRO,'COSERATT',ATSERIAL,'COSEREVE',CODUID,'CODATESP',DTSTAMP,'CO__NOTE',this.w_MEMO)
      insert into (i_cTable) (COSERPRO,COSERATT,COSEREVE,CODATESP,CO__NOTE &i_ccchkf. );
         values (;
           this.oParentObject.w_SERPRO;
           ,ATSERIAL;
           ,CODUID;
           ,DTSTAMP;
           ,this.w_MEMO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_00CA3200()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from DET_IMPO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2],.t.,this.DET_IMPO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PI_STATO"+;
        " from "+i_cTable+" DET_IMPO where ";
            +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and PICODUID = "+cp_ToStrODBC(this.w_UID);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PI_STATO;
        from (i_cTable) where;
            PICODPRO = this.oParentObject.w_SERPRO;
            and PICODUID = this.w_UID;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OSTATO = NVL(cp_ToDate(_read_.PI_STATO),cp_NullValue(_read_.PI_STATO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows<>0 and this.w_OSTATO="E"
      this.w_STATO = this.w_OSTATO
    else
      this.w_STATO = "K"
    endif
    * --- Delete from DET_IMPO
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and PICODUID = "+cp_ToStrODBC(this.w_UID);
             )
    else
      delete from (i_cTable) where;
            PICODPRO = this.oParentObject.w_SERPRO;
            and PICODUID = this.w_UID;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into DET_IMPO
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_IMPO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PICODPRO"+",PICODUID"+",PIDATINI"+",PIDATFIN"+",PIOGGETT"+",PILOCALI"+",PINUMPRI"+",PIDESCRI"+",PICATEGO"+",PIDATIMP"+",PI_STATO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERPRO),'DET_IMPO','PICODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UID),'DET_IMPO','PICODUID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'DET_IMPO','PIDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'DET_IMPO','PIDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SUMMARY),'DET_IMPO','PIOGGETT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCATION),'DET_IMPO','PILOCALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRIORITY),'DET_IMPO','PINUMPRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRIPTION),'DET_IMPO','PIDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCAT),'DET_IMPO','PICATEGO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DTSTAMP),'DET_IMPO','PIDATIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STATO),'DET_IMPO','PI_STATO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PICODPRO',this.oParentObject.w_SERPRO,'PICODUID',this.w_UID,'PIDATINI',this.w_DATINI,'PIDATFIN',this.w_DATFIN,'PIOGGETT',this.w_SUMMARY,'PILOCALI',this.w_LOCATION,'PINUMPRI',this.w_PRIORITY,'PIDESCRI',this.w_DESCRIPTION,'PICATEGO',this.w_CODCAT,'PIDATIMP',this.w_DTSTAMP,'PI_STATO',this.w_STATO)
      insert into (i_cTable) (PICODPRO,PICODUID,PIDATINI,PIDATFIN,PIOGGETT,PILOCALI,PINUMPRI,PIDESCRI,PICATEGO,PIDATIMP,PI_STATO &i_ccchkf. );
         values (;
           this.oParentObject.w_SERPRO;
           ,this.w_UID;
           ,this.w_DATINI;
           ,this.w_DATFIN;
           ,this.w_SUMMARY;
           ,this.w_LOCATION;
           ,this.w_PRIORITY;
           ,this.w_DESCRIPTION;
           ,this.w_CODCAT;
           ,this.w_DTSTAMP;
           ,this.w_STATO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_00D56578()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from DET_EXPO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DET_EXPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_EXPO_idx,2],.t.,this.DET_EXPO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COFLGIGN"+;
        " from "+i_cTable+" DET_EXPO where ";
            +"COSERPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and COSEREVE = "+cp_ToStrODBC(this.w_UID);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COFLGIGN;
        from (i_cTable) where;
            COSERPRO = this.oParentObject.w_SERPRO;
            and COSEREVE = this.w_UID;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLGIGN = NVL(cp_ToDate(_read_.COFLGIGN),cp_NullValue(_read_.COFLGIGN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_FLGIGN="S" AND this.oParentObject.w_ESCIGN="S"
      this.w_STATO = "K"
    endif
    * --- Delete from DET_IMPO
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and PICODUID = "+cp_ToStrODBC(this.w_UID);
             )
    else
      delete from (i_cTable) where;
            PICODPRO = this.oParentObject.w_SERPRO;
            and PICODUID = this.w_UID;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into DET_IMPO
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_IMPO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PICODPRO"+",PICODUID"+",PIDATINI"+",PIDATFIN"+",PIOGGETT"+",PILOCALI"+",PINUMPRI"+",PIDESCRI"+",PICATEGO"+",PIDATIMP"+",PI_STATO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERPRO),'DET_IMPO','PICODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UID),'DET_IMPO','PICODUID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'DET_IMPO','PIDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'DET_IMPO','PIDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SUMMARY),'DET_IMPO','PIOGGETT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCATION),'DET_IMPO','PILOCALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRIORITY),'DET_IMPO','PINUMPRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRIPTION),'DET_IMPO','PIDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCAT),'DET_IMPO','PICATEGO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DTSTAMP),'DET_IMPO','PIDATIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STATO),'DET_IMPO','PI_STATO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PICODPRO',this.oParentObject.w_SERPRO,'PICODUID',this.w_UID,'PIDATINI',this.w_DATINI,'PIDATFIN',this.w_DATFIN,'PIOGGETT',this.w_SUMMARY,'PILOCALI',this.w_LOCATION,'PINUMPRI',this.w_PRIORITY,'PIDESCRI',this.w_DESCRIPTION,'PICATEGO',this.w_CODCAT,'PIDATIMP',this.w_DTSTAMP,'PI_STATO',this.w_STATO)
      insert into (i_cTable) (PICODPRO,PICODUID,PIDATINI,PIDATFIN,PIOGGETT,PILOCALI,PINUMPRI,PIDESCRI,PICATEGO,PIDATIMP,PI_STATO &i_ccchkf. );
         values (;
           this.oParentObject.w_SERPRO;
           ,this.w_UID;
           ,this.w_DATINI;
           ,this.w_DATFIN;
           ,this.w_SUMMARY;
           ,this.w_LOCATION;
           ,this.w_PRIORITY;
           ,this.w_DESCRIPTION;
           ,this.w_CODCAT;
           ,this.w_DTSTAMP;
           ,this.w_STATO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_00D67D00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DET_IMPO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DET_IMPO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PI_STATO ="+cp_NullLink(cp_ToStrODBC(this.w_STATO),'DET_IMPO','PI_STATO');
          +i_ccchkf ;
      +" where ";
          +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
          +" and PICODUID = "+cp_ToStrODBC(this.w_UID);
             )
    else
      update (i_cTable) set;
          PI_STATO = this.w_STATO;
          &i_ccchkf. ;
       where;
          PICODPRO = this.oParentObject.w_SERPRO;
          and PICODUID = this.w_UID;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DET_EXPO'
    this.cWorkTables[2]='DET_IMPO'
    this.cWorkTables[3]='PRO_CATE'
    this.cWorkTables[4]='OFF_ATTI'
    this.cWorkTables[5]='OFF_NOMI'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
