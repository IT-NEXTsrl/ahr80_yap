* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_krp                                                        *
*              Elenco prestazioni                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-29                                                      *
* Last revis.: 2015-07-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsag_krp
if cp_TablePropScan('TMPSTPRE') > 0
   ah_errormsg("Attenzione: E' gi� aperta un'istanza di Elenco prestazioni, quella corrente verr� chiusa",48,'')
   return
endif
* --- Fine Area Manuale
return(createobject("tgsag_krp",oParentObject))

* --- Class definition
define class tgsag_krp as StdForm
  Top    = 1
  Left   = 8

  * --- Standard Properties
  Width  = 824
  Height = 582+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-21"
  HelpContextID=152426647
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=108

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  OFFTIPAT_IDX = 0
  CPUSERS_IDX = 0
  ART_ICOL_IDX = 0
  cpusers_IDX = 0
  KEY_ARTI_IDX = 0
  CAT_SOGG_IDX = 0
  OFF_NOMI_IDX = 0
  CENCOST_IDX = 0
  PAR_ALTE_IDX = 0
  PAR_AGEN_IDX = 0
  cPrg = "gsag_krp"
  cComment = "Elenco prestazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READAZI = space(5)
  o_READAZI = space(5)
  w_READ_AGEN = space(5)
  w_PAFLR_AG = space(1)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_FiltTipRig = space(10)
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_FiltTipRig = space(10)
  w_FiltTipRi2 = space(10)
  w_STATO = space(1)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_InDocum = space(1)
  o_InDocum = space(1)
  w_CODPRA = space(15)
  o_CODPRA = space(15)
  w_DATA_INI = ctot('')
  w_CODNOM = space(20)
  o_CODNOM = space(20)
  w_CODRES = space(5)
  o_CODRES = space(5)
  w_CodUte = 0
  w_CODSER = space(20)
  w_PRESERIAL = space(10)
  w_DesSer = space(40)
  o_DesSer = space(40)
  w_DATA_FIN = ctot('')
  w_ATSERIAL = space(20)
  w_DESPRA = space(100)
  w_OBTEST = ctod('  /  /  ')
  w_DENOM_PART = space(100)
  w_DESUTE = space(20)
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_COMODO = space(30)
  w_PrestRowOrd = 0
  w_DesAgg = space(0)
  w_DESNOM = space(40)
  w_OFDATDOC = ctod('  /  /  ')
  w_CODCLICONTR = space(20)
  w_ROWNUM = 0
  w_EDITCODPRA = .F.
  w_CACODART = space(20)
  w_DPTIPRIS = space(1)
  w_TIPOENTE = space(1)
  w_TipoRisorsa = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_DURTOTORE = 0
  w_DURTOTMIN = 0
  w_TOTQTAORE = 0
  w_TOTQTAMIN = 0
  w_FiltSosp = space(1)
  w_COGNTIT = space(40)
  w_NOMETIT = space(40)
  w_COGNRES = space(40)
  w_NOMERES = space(40)
  w_ATCAUATT = space(10)
  w_CODPART = space(10)
  w_ATCODPRA = space(15)
  w_ATDATINI = ctot('')
  w_CNDESCAN = space(80)
  w_DADESATT = space(40)
  w_DACODATT = space(40)
  w_NOMPRG = space(10)
  w_CONTROP = space(1)
  w_TIPOPRE = space(1)
  w_FLESPA = space(1)
  w_PAGENPRE = space(1)
  w_CODPRA = space(15)
  w_CC_CONTO = space(15)
  w_TIT_PRA = space(5)
  o_TIT_PRA = space(5)
  w_RES_PRA = space(5)
  o_RES_PRA = space(5)
  w_CODNOM = space(20)
  w_TIPPRA = space(1)
  w_TIPANA = space(1)
  w_CC_CONTO = space(15)
  w_IMPZERO = space(1)
  w_FL_SOSP = space(1)
  w_RIFERIMENTO = space(30)
  w_NORESP = space(1)
  o_NORESP = space(1)
  w_NORESP = space(1)
  w_NOPRAT = space(1)
  o_NOPRAT = space(1)
  w_OnoCiv = space(1)
  w_DirCiv = space(1)
  w_PreTempo = space(1)
  w_OnoPen = space(1)
  w_DirStrag = space(1)
  w_Spese = space(1)
  w_OnoStrag = space(1)
  w_PreGen = space(1)
  w_Anticip = space(1)
  w_EscFac = space(1)
  w_Evasio = space(1)
  w_VisTotali = space(1)
  w_DatiPratica = space(1)
  w_Annotazioni = space(1)
  w_SoloTot = space(1)
  w_DENOM_TIT = space(100)
  w_DENOM_RES = space(100)
  w_CCDESPIA = space(40)
  w_DESPRA = space(100)
  w_DESNOM = space(40)
  w_TOTSPE = 0
  w_TOTANT = 0
  w_TOTCON = 0
  w_CCDESPIA = space(40)
  w_DelPreAtt = space(1)
  w_ORDERBY = space(10)
  w_DesAggPre = space(40)
  w_TIPNOM = space(1)
  w_AGZRP_ZOOM = .NULL.
  w_STAMPA_AHR = .NULL.
  w_STAMPA_ALTE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_krp
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_krpPag1","gsag_krp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsag_krpPag2","gsag_krp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Filtri aggiuntivi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_krp
    Local nCtrl
    For nCtrl=1 to this.page2.Controls(1).ControlCount
       If UPPER(this.page2.Controls(1).Controls(nCtrl).Class)=='STDBOX'
        this.page2.Controls(1).Controls(nCtrl).Visible=IsAlt()
       EndIf
    Next
    
    if !isalt()
      local oSTAMPA_ALTE
      oSTAMPA_ALTE=this.PARENT.GetCtrl("STAMPA_ALTE")
      oSTAMPA_ALTE.Visible=.F.
      oSTAMPA_ALTE=.NULL.
    else
      local oSTAMPA_AHR
      oSTAMPA_AHR=this.PARENT.GetCtrl("STAMPA_AHR")
      oSTAMPA_AHR.Visible=.F.
      oSTAMPA_AHR=.NULL.
    endif  
    
    if isalt()
      local oAGKRP_ZOOM
      oAGKRP_ZOOM=this.Parent.GetCtrl("AGZRP_ZOOM")
      oAGKRP_ZOOM.cZoomfile="GSAGAZRP"
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGZRP_ZOOM = this.oPgFrm.Pages(1).oPag.AGZRP_ZOOM
    this.w_STAMPA_AHR = this.oPgFrm.Pages(1).oPag.STAMPA_AHR
    this.w_STAMPA_ALTE = this.oPgFrm.Pages(2).oPag.STAMPA_ALTE
    DoDefault()
    proc Destroy()
      this.w_AGZRP_ZOOM = .NULL.
      this.w_STAMPA_AHR = .NULL.
      this.w_STAMPA_ALTE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='OFFTIPAT'
    this.cWorkTables[4]='CPUSERS'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='cpusers'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='CAT_SOGG'
    this.cWorkTables[9]='OFF_NOMI'
    this.cWorkTables[10]='CENCOST'
    this.cWorkTables[11]='PAR_ALTE'
    this.cWorkTables[12]='PAR_AGEN'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READAZI=space(5)
      .w_READ_AGEN=space(5)
      .w_PAFLR_AG=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_DATFIN=ctod("  /  /  ")
      .w_FiltTipRig=space(10)
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_FiltTipRig=space(10)
      .w_FiltTipRi2=space(10)
      .w_STATO=space(1)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_InDocum=space(1)
      .w_CODPRA=space(15)
      .w_DATA_INI=ctot("")
      .w_CODNOM=space(20)
      .w_CODRES=space(5)
      .w_CodUte=0
      .w_CODSER=space(20)
      .w_PRESERIAL=space(10)
      .w_DesSer=space(40)
      .w_DATA_FIN=ctot("")
      .w_ATSERIAL=space(20)
      .w_DESPRA=space(100)
      .w_OBTEST=ctod("  /  /  ")
      .w_DENOM_PART=space(100)
      .w_DESUTE=space(20)
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_COMODO=space(30)
      .w_PrestRowOrd=0
      .w_DesAgg=space(0)
      .w_DESNOM=space(40)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_CODCLICONTR=space(20)
      .w_ROWNUM=0
      .w_EDITCODPRA=.f.
      .w_CACODART=space(20)
      .w_DPTIPRIS=space(1)
      .w_TIPOENTE=space(1)
      .w_TipoRisorsa=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DURTOTORE=0
      .w_DURTOTMIN=0
      .w_TOTQTAORE=0
      .w_TOTQTAMIN=0
      .w_FiltSosp=space(1)
      .w_COGNTIT=space(40)
      .w_NOMETIT=space(40)
      .w_COGNRES=space(40)
      .w_NOMERES=space(40)
      .w_ATCAUATT=space(10)
      .w_CODPART=space(10)
      .w_ATCODPRA=space(15)
      .w_ATDATINI=ctot("")
      .w_CNDESCAN=space(80)
      .w_DADESATT=space(40)
      .w_DACODATT=space(40)
      .w_NOMPRG=space(10)
      .w_CONTROP=space(1)
      .w_TIPOPRE=space(1)
      .w_FLESPA=space(1)
      .w_PAGENPRE=space(1)
      .w_CODPRA=space(15)
      .w_CC_CONTO=space(15)
      .w_TIT_PRA=space(5)
      .w_RES_PRA=space(5)
      .w_CODNOM=space(20)
      .w_TIPPRA=space(1)
      .w_TIPANA=space(1)
      .w_CC_CONTO=space(15)
      .w_IMPZERO=space(1)
      .w_FL_SOSP=space(1)
      .w_RIFERIMENTO=space(30)
      .w_NORESP=space(1)
      .w_NORESP=space(1)
      .w_NOPRAT=space(1)
      .w_OnoCiv=space(1)
      .w_DirCiv=space(1)
      .w_PreTempo=space(1)
      .w_OnoPen=space(1)
      .w_DirStrag=space(1)
      .w_Spese=space(1)
      .w_OnoStrag=space(1)
      .w_PreGen=space(1)
      .w_Anticip=space(1)
      .w_EscFac=space(1)
      .w_Evasio=space(1)
      .w_VisTotali=space(1)
      .w_DatiPratica=space(1)
      .w_Annotazioni=space(1)
      .w_SoloTot=space(1)
      .w_DENOM_TIT=space(100)
      .w_DENOM_RES=space(100)
      .w_CCDESPIA=space(40)
      .w_DESPRA=space(100)
      .w_DESNOM=space(40)
      .w_TOTSPE=0
      .w_TOTANT=0
      .w_TOTCON=0
      .w_CCDESPIA=space(40)
      .w_DelPreAtt=space(1)
      .w_ORDERBY=space(10)
      .w_DesAggPre=space(40)
      .w_TIPNOM=space(1)
        .w_READAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READAZI))
          .link_1_1('Full')
        endif
        .w_READ_AGEN = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READ_AGEN))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        .w_DATFIN = i_datSys
        .w_FiltTipRig = ' '
        .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        .w_FiltTipRig = ' '
        .w_FiltTipRi2 = ' '
        .w_STATO = 'P'
          .DoRTCalc(14,15,.f.)
        .w_InDocum = 'T'
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODPRA))
          .link_1_26('Full')
        endif
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODNOM))
          .link_1_28('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CODRES))
          .link_1_29('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CodUte))
          .link_1_30('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CODSER))
          .link_1_31('Full')
        endif
          .DoRTCalc(23,24,.f.)
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .w_ATSERIAL = .w_AGZRP_ZOOM.GetVar('ATSERIAL')
          .DoRTCalc(27,27,.f.)
        .w_OBTEST = i_INIDAT
        .w_DENOM_PART = alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART)
      .oPgFrm.Page1.oPag.AGZRP_ZOOM.Calculate()
          .DoRTCalc(30,33,.f.)
        .w_PrestRowOrd = .w_AGZRP_ZOOM.GetVar('CPROWORD')
          .DoRTCalc(35,36,.f.)
        .w_OFDATDOC = i_datsys
          .DoRTCalc(38,38,.f.)
        .w_ROWNUM = .w_AGZRP_ZOOM.GetVar('CPROWNUM')
        .w_EDITCODPRA = .T.
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_CACODART))
          .link_1_72('Full')
        endif
        .w_DPTIPRIS = "P"
          .DoRTCalc(43,43,.f.)
        .w_TipoRisorsa = 'P'
      .oPgFrm.Page1.oPag.oObj_1_79.Calculate(IIF(IsAlt(), "Inserire il codice soggetto esterno con ruolo cliente per cui si desidera ricercare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto esterno selezionato)", "Inserire il codice del nominativo per cui si desidera ricercare le prestazioni (la procedura ricerca le prestazioni all'interno delle attivit� ove � presente il nominativo selezionato)"))
      .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(46,49,.f.)
        .w_FiltSosp = IIF(.w_FL_SOSP='T','',.w_FL_SOSP)
      .oPgFrm.Page1.oPag.oObj_1_99.Calculate(IIF(IsAlt(), "Codice responsabile prestazione","Filtro su codice partecipante"))
      .oPgFrm.Page1.oPag.STAMPA_AHR.Calculate()
          .DoRTCalc(51,54,.f.)
        .w_ATCAUATT = .w_AGZRP_ZOOM.GetVar('ATCAUATT')
        .w_CODPART = .w_AGZRP_ZOOM.GetVar('DACODRES')
        .w_ATCODPRA = .w_AGZRP_ZOOM.GetVar('ATCODPRA')
        .w_ATDATINI = .w_AGZRP_ZOOM.GetVar('ATDATINI')
        .w_CNDESCAN = .w_AGZRP_ZOOM.GetVar('CNDESCAN')
        .w_DADESATT = .w_AGZRP_ZOOM.GetVar('DADESATT')
        .w_DACODATT = .w_AGZRP_ZOOM.GetVar('DACODATT')
        .w_NOMPRG = iif(!isalt(),'GSAG1SPR','GSAG_SPR')
        .w_CONTROP = 'N'
        .w_TIPOPRE = Nvl(.w_AGZRP_ZOOM.GetVar('TIPOPRE'),' ')
        .w_FLESPA = 'N'
        .DoRTCalc(66,67,.f.)
        if not(empty(.w_CODPRA))
          .link_2_1('Full')
        endif
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_CC_CONTO))
          .link_2_2('Full')
        endif
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_TIT_PRA))
          .link_2_3('Full')
        endif
        .DoRTCalc(70,70,.f.)
        if not(empty(.w_RES_PRA))
          .link_2_4('Full')
        endif
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_CODNOM))
          .link_2_5('Full')
        endif
        .w_TIPPRA = 'T'
        .w_TIPANA = 'C'
        .DoRTCalc(74,74,.f.)
        if not(empty(.w_CC_CONTO))
          .link_2_8('Full')
        endif
        .w_IMPZERO = 'N'
        .w_FL_SOSP = 'T'
          .DoRTCalc(77,77,.f.)
        .w_NORESP = 'N'
        .w_NORESP = 'N'
        .w_NOPRAT = 'N'
        .w_OnoCiv = 'I'
        .w_DirCiv = 'C'
        .w_PreTempo = 'P'
        .w_OnoPen = 'E'
        .w_DirStrag = 'R'
        .w_Spese = 'S'
        .w_OnoStrag = 'T'
        .w_PreGen = 'G'
        .w_Anticip = 'A'
        .w_EscFac = iif(!( .w_InDocum $ 'P-D-N'),'N',iif(Empty(.w_EscFac),'N',.w_EscFac))
        .w_Evasio = 'T'
        .w_VisTotali = 'N'
        .w_DatiPratica = 'S'
        .w_Annotazioni = 'N'
        .w_SoloTot = 'N'
      .oPgFrm.Page2.oPag.STAMPA_ALTE.Calculate()
        .w_DENOM_TIT = alltrim(.w_COGNTIT)+' '+alltrim(.w_NOMETIT)
        .w_DENOM_RES = alltrim(.w_COGNRES)+' '+alltrim(.w_NOMERES)
          .DoRTCalc(98,104,.f.)
        .w_DelPreAtt = 'S'
          .DoRTCalc(106,106,.f.)
        .w_DesAggPre = IIF(IsAlt() AND .w_PAFLR_AG='S',ALLTRIM(.w_DesSer),'')
    endwith
    this.DoRTCalc(108,108,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_73.enabled = this.oPgFrm.Page1.oPag.oBtn_1_73.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_116.enabled = this.oPgFrm.Page1.oPag.oBtn_1_116.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_33.enabled = this.oPgFrm.Page2.oPag.oBtn_2_33.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_35.enabled = this.oPgFrm.Page2.oPag.oBtn_2_35.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_48.enabled = this.oPgFrm.Page2.oPag.oBtn_2_48.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_49.enabled = this.oPgFrm.Page2.oPag.oBtn_2_49.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_READAZI<>.w_READAZI
            .w_READAZI = i_CODAZI
          .link_1_1('Full')
        endif
        if .o_READAZI<>.w_READAZI
            .w_READ_AGEN = i_CODAZI
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.t.)
        if .o_OREINI<>.w_OREINI
            .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        endif
        if .o_OREINI<>.w_OREINI
          .Calculate_MVFLEXPGGM()
        endif
        if .o_MININI<>.w_MININI
            .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        endif
        if .o_MININI<>.w_MININI
          .Calculate_SUGRFFLBCH()
        endif
        .DoRTCalc(7,8,.t.)
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN
            .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        endif
        if .o_OREFIN<>.w_OREFIN
          .Calculate_FPJEAUJXWE()
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN
            .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_KTQUMLVLEF()
        endif
        .DoRTCalc(11,17,.t.)
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .DoRTCalc(19,24,.t.)
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
            .w_ATSERIAL = .w_AGZRP_ZOOM.GetVar('ATSERIAL')
        .DoRTCalc(27,28,.t.)
        if .o_CODRES<>.w_CODRES
            .w_DENOM_PART = alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART)
        endif
        .oPgFrm.Page1.oPag.AGZRP_ZOOM.Calculate()
        .DoRTCalc(30,33,.t.)
            .w_PrestRowOrd = .w_AGZRP_ZOOM.GetVar('CPROWORD')
        if .o_CODNOM<>.w_CODNOM
          .Calculate_TFSGFYKWPK()
        endif
        if .o_CODPRA<>.w_CODPRA
          .Calculate_JQPANHVMGV()
        endif
        if .o_NORESP<>.w_NORESP
          .Calculate_JIQIQXCTID()
        endif
        if .o_NOPRAT<>.w_NOPRAT
          .Calculate_IQJNAVRXRG()
        endif
        .DoRTCalc(35,38,.t.)
            .w_ROWNUM = .w_AGZRP_ZOOM.GetVar('CPROWNUM')
        .DoRTCalc(40,40,.t.)
          .link_1_72('Full')
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate(IIF(IsAlt(), "Inserire il codice soggetto esterno con ruolo cliente per cui si desidera ricercare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto esterno selezionato)", "Inserire il codice del nominativo per cui si desidera ricercare le prestazioni (la procedura ricerca le prestazioni all'interno delle attivit� ove � presente il nominativo selezionato)"))
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .DoRTCalc(42,49,.t.)
            .w_FiltSosp = IIF(.w_FL_SOSP='T','',.w_FL_SOSP)
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate(IIF(IsAlt(), "Codice responsabile prestazione","Filtro su codice partecipante"))
        .oPgFrm.Page1.oPag.STAMPA_AHR.Calculate()
        .DoRTCalc(51,54,.t.)
            .w_ATCAUATT = .w_AGZRP_ZOOM.GetVar('ATCAUATT')
            .w_CODPART = .w_AGZRP_ZOOM.GetVar('DACODRES')
            .w_ATCODPRA = .w_AGZRP_ZOOM.GetVar('ATCODPRA')
            .w_ATDATINI = .w_AGZRP_ZOOM.GetVar('ATDATINI')
            .w_CNDESCAN = .w_AGZRP_ZOOM.GetVar('CNDESCAN')
            .w_DADESATT = .w_AGZRP_ZOOM.GetVar('DADESATT')
            .w_DACODATT = .w_AGZRP_ZOOM.GetVar('DACODATT')
        .DoRTCalc(62,63,.t.)
            .w_TIPOPRE = Nvl(.w_AGZRP_ZOOM.GetVar('TIPOPRE'),' ')
        .DoRTCalc(65,89,.t.)
        if .o_InDocum<>.w_InDocum
            .w_EscFac = iif(!( .w_InDocum $ 'P-D-N'),'N',iif(Empty(.w_EscFac),'N',.w_EscFac))
        endif
        .oPgFrm.Page2.oPag.STAMPA_ALTE.Calculate()
        .DoRTCalc(91,95,.t.)
        if .o_TIT_PRA<>.w_TIT_PRA
            .w_DENOM_TIT = alltrim(.w_COGNTIT)+' '+alltrim(.w_NOMETIT)
        endif
        if .o_RES_PRA<>.w_RES_PRA
            .w_DENOM_RES = alltrim(.w_COGNRES)+' '+alltrim(.w_NOMERES)
        endif
        .DoRTCalc(98,106,.t.)
        if .o_DesSer<>.w_DesSer
            .w_DesAggPre = IIF(IsAlt() AND .w_PAFLR_AG='S',ALLTRIM(.w_DesSer),'')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(108,108,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.AGZRP_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate(IIF(IsAlt(), "Inserire il codice soggetto esterno con ruolo cliente per cui si desidera ricercare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto esterno selezionato)", "Inserire il codice del nominativo per cui si desidera ricercare le prestazioni (la procedura ricerca le prestazioni all'interno delle attivit� ove � presente il nominativo selezionato)"))
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate(IIF(IsAlt(), "Codice responsabile prestazione","Filtro su codice partecipante"))
        .oPgFrm.Page1.oPag.STAMPA_AHR.Calculate()
        .oPgFrm.Page2.oPag.STAMPA_ALTE.Calculate()
    endwith
  return

  proc Calculate_NRTSRHMIVM()
    with this
          * --- Inizializza date
          gsag_ba3(this;
              ,'PRES';
             )
    endwith
  endproc
  proc Calculate_SXCNCBAXOP()
    with this
          * --- Inizializza ora finale
          .w_OREFIN = "23"
          .w_MINFIN = "59"
    endwith
  endproc
  proc Calculate_MVFLEXPGGM()
    with this
          * --- Resetto w_oreini
          .w_OREINI = .ZeroFill(.w_OREINI)
    endwith
  endproc
  proc Calculate_SUGRFFLBCH()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_FPJEAUJXWE()
    with this
          * --- Resetto w_orefin
          .w_OREFIN = .ZeroFill(.w_OREFIN)
    endwith
  endproc
  proc Calculate_KTQUMLVLEF()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_QZFXXCJOCZ()
    with this
          * --- Refresh zoom dalle gestioni
          gsag_bsp(this;
              ,'RICERCA';
             )
    endwith
  endproc
  proc Calculate_LYBISUKBKM()
    with this
          * --- Imposta data odierna
          .w_DATINI = i_DatSys
          .w_DATFIN = i_DatSys
          .w_OREINI = '00'
          .w_MININI = '00'
          .w_OREFIN = '23'
          .w_MINFIN = '59'
          .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '01-01-1900 00:00:00'))
          .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', '31-12-2999 23:59:59'))
    endwith
  endproc
  proc Calculate_KGFZQZXEQS()
    with this
          * --- Controllo iniziale - se alterego gsal_bck
          gsag_bsp(this;
              ,'CONTROLLO';
             )
    endwith
  endproc
  proc Calculate_TFSGFYKWPK()
    with this
          * --- Controllo nominativo=cliente
          gsag_bsp(this;
              ,'CLIENTE';
             )
    endwith
  endproc
  proc Calculate_JQPANHVMGV()
    with this
          * --- Azzera cliente
          .w_CODNOM = IIF(NOT EMPTY(.w_CODPRA) AND IsAlt(), space(20), .w_CODNOM)
          .w_DESNOM = IIF(NOT EMPTY(.w_CODPRA) AND IsAlt(), space(40), .w_DESNOM)
          .w_TIT_PRA = ''
          .link_2_3('Full')
          .w_RES_PRA = ''
          .link_2_4('Full')
    endwith
  endproc
  proc Calculate_JIQIQXCTID()
    with this
          * --- Azzera resp.
          .w_CODRES = IIF(.w_NORESP='S', space(5), .w_CODRES)
          .w_DENOM_PART = IIF(.w_NORESP='S', space(100), .w_DENOM_PART)
    endwith
  endproc
  proc Calculate_IQJNAVRXRG()
    with this
          * --- Azzera pratica
          .w_CODPRA = IIF(.w_NOPRAT='S', space(15), .w_CODPRA)
          .w_DESPRA = IIF(.w_NOPRAT='S', space(100), .w_DESPRA)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODPRA_1_26.enabled = this.oPgFrm.Page1.oPag.oCODPRA_1_26.mCond()
    this.oPgFrm.Page2.oPag.oCODPRA_2_1.enabled = this.oPgFrm.Page2.oPag.oCODPRA_2_1.mCond()
    this.oPgFrm.Page2.oPag.oEscFac_2_25.enabled = this.oPgFrm.Page2.oPag.oEscFac_2_25.mCond()
    this.oPgFrm.Page2.oPag.oEvasio_2_26.enabled_(this.oPgFrm.Page2.oPag.oEvasio_2_26.mCond())
    this.oPgFrm.Page2.oPag.oDatiPratica_2_28.enabled = this.oPgFrm.Page2.oPag.oDatiPratica_2_28.mCond()
    this.oPgFrm.Page2.oPag.oAnnotazioni_2_29.enabled = this.oPgFrm.Page2.oPag.oAnnotazioni_2_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_33.enabled = this.oPgFrm.Page2.oPag.oBtn_2_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFiltTipRig_1_13.visible=!this.oPgFrm.Page1.oPag.oFiltTipRig_1_13.mHide()
    this.oPgFrm.Page1.oPag.oFiltTipRig_1_20.visible=!this.oPgFrm.Page1.oPag.oFiltTipRig_1_20.mHide()
    this.oPgFrm.Page1.oPag.oFiltTipRi2_1_21.visible=!this.oPgFrm.Page1.oPag.oFiltTipRi2_1_21.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_23.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_1_23.mHide()
    this.oPgFrm.Page1.oPag.oALFDOC_1_24.visible=!this.oPgFrm.Page1.oPag.oALFDOC_1_24.mHide()
    this.oPgFrm.Page1.oPag.oInDocum_1_25.visible=!this.oPgFrm.Page1.oPag.oInDocum_1_25.mHide()
    this.oPgFrm.Page1.oPag.oCODPRA_1_26.visible=!this.oPgFrm.Page1.oPag.oCODPRA_1_26.mHide()
    this.oPgFrm.Page1.oPag.oCODNOM_1_28.visible=!this.oPgFrm.Page1.oPag.oCODNOM_1_28.mHide()
    this.oPgFrm.Page1.oPag.oPRESERIAL_1_32.visible=!this.oPgFrm.Page1.oPag.oPRESERIAL_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oDESPRA_1_38.visible=!this.oPgFrm.Page1.oPag.oDESPRA_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_43.visible=!this.oPgFrm.Page1.oPag.oBtn_1_43.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_44.visible=!this.oPgFrm.Page1.oPag.oBtn_1_44.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_45.visible=!this.oPgFrm.Page1.oPag.oBtn_1_45.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_46.visible=!this.oPgFrm.Page1.oPag.oBtn_1_46.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_47.visible=!this.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oDESNOM_1_61.visible=!this.oPgFrm.Page1.oPag.oDESNOM_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_78.visible=!this.oPgFrm.Page1.oPag.oStr_1_78.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oDURTOTORE_1_85.visible=!this.oPgFrm.Page1.oPag.oDURTOTORE_1_85.mHide()
    this.oPgFrm.Page1.oPag.oDURTOTMIN_1_86.visible=!this.oPgFrm.Page1.oPag.oDURTOTMIN_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.oPgFrm.Page1.oPag.oTOTQTAORE_1_89.visible=!this.oPgFrm.Page1.oPag.oTOTQTAORE_1_89.mHide()
    this.oPgFrm.Page1.oPag.oTOTQTAMIN_1_90.visible=!this.oPgFrm.Page1.oPag.oTOTQTAMIN_1_90.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_104.visible=!this.oPgFrm.Page1.oPag.oBtn_1_104.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_116.visible=!this.oPgFrm.Page1.oPag.oBtn_1_116.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_118.visible=!this.oPgFrm.Page1.oPag.oStr_1_118.mHide()
    this.oPgFrm.Page2.oPag.oCODPRA_2_1.visible=!this.oPgFrm.Page2.oPag.oCODPRA_2_1.mHide()
    this.oPgFrm.Page2.oPag.oCC_CONTO_2_2.visible=!this.oPgFrm.Page2.oPag.oCC_CONTO_2_2.mHide()
    this.oPgFrm.Page2.oPag.oTIT_PRA_2_3.visible=!this.oPgFrm.Page2.oPag.oTIT_PRA_2_3.mHide()
    this.oPgFrm.Page2.oPag.oRES_PRA_2_4.visible=!this.oPgFrm.Page2.oPag.oRES_PRA_2_4.mHide()
    this.oPgFrm.Page2.oPag.oCODNOM_2_5.visible=!this.oPgFrm.Page2.oPag.oCODNOM_2_5.mHide()
    this.oPgFrm.Page2.oPag.oTIPPRA_2_6.visible=!this.oPgFrm.Page2.oPag.oTIPPRA_2_6.mHide()
    this.oPgFrm.Page2.oPag.oTIPANA_2_7.visible=!this.oPgFrm.Page2.oPag.oTIPANA_2_7.mHide()
    this.oPgFrm.Page2.oPag.oCC_CONTO_2_8.visible=!this.oPgFrm.Page2.oPag.oCC_CONTO_2_8.mHide()
    this.oPgFrm.Page2.oPag.oFL_SOSP_2_10.visible=!this.oPgFrm.Page2.oPag.oFL_SOSP_2_10.mHide()
    this.oPgFrm.Page2.oPag.oRIFERIMENTO_2_11.visible=!this.oPgFrm.Page2.oPag.oRIFERIMENTO_2_11.mHide()
    this.oPgFrm.Page2.oPag.oNORESP_2_12.visible=!this.oPgFrm.Page2.oPag.oNORESP_2_12.mHide()
    this.oPgFrm.Page2.oPag.oNORESP_2_13.visible=!this.oPgFrm.Page2.oPag.oNORESP_2_13.mHide()
    this.oPgFrm.Page2.oPag.oNOPRAT_2_14.visible=!this.oPgFrm.Page2.oPag.oNOPRAT_2_14.mHide()
    this.oPgFrm.Page2.oPag.oOnoCiv_2_15.visible=!this.oPgFrm.Page2.oPag.oOnoCiv_2_15.mHide()
    this.oPgFrm.Page2.oPag.oDirCiv_2_16.visible=!this.oPgFrm.Page2.oPag.oDirCiv_2_16.mHide()
    this.oPgFrm.Page2.oPag.oPreTempo_2_17.visible=!this.oPgFrm.Page2.oPag.oPreTempo_2_17.mHide()
    this.oPgFrm.Page2.oPag.oOnoPen_2_18.visible=!this.oPgFrm.Page2.oPag.oOnoPen_2_18.mHide()
    this.oPgFrm.Page2.oPag.oDirStrag_2_19.visible=!this.oPgFrm.Page2.oPag.oDirStrag_2_19.mHide()
    this.oPgFrm.Page2.oPag.oSpese_2_20.visible=!this.oPgFrm.Page2.oPag.oSpese_2_20.mHide()
    this.oPgFrm.Page2.oPag.oOnoStrag_2_21.visible=!this.oPgFrm.Page2.oPag.oOnoStrag_2_21.mHide()
    this.oPgFrm.Page2.oPag.oPreGen_2_22.visible=!this.oPgFrm.Page2.oPag.oPreGen_2_22.mHide()
    this.oPgFrm.Page2.oPag.oAnticip_2_23.visible=!this.oPgFrm.Page2.oPag.oAnticip_2_23.mHide()
    this.oPgFrm.Page2.oPag.oEscFac_2_25.visible=!this.oPgFrm.Page2.oPag.oEscFac_2_25.mHide()
    this.oPgFrm.Page2.oPag.oEvasio_2_26.visible=!this.oPgFrm.Page2.oPag.oEvasio_2_26.mHide()
    this.oPgFrm.Page2.oPag.oVisTotali_2_27.visible=!this.oPgFrm.Page2.oPag.oVisTotali_2_27.mHide()
    this.oPgFrm.Page2.oPag.oDatiPratica_2_28.visible=!this.oPgFrm.Page2.oPag.oDatiPratica_2_28.mHide()
    this.oPgFrm.Page2.oPag.oAnnotazioni_2_29.visible=!this.oPgFrm.Page2.oPag.oAnnotazioni_2_29.mHide()
    this.oPgFrm.Page2.oPag.oSoloTot_2_30.visible=!this.oPgFrm.Page2.oPag.oSoloTot_2_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_31.visible=!this.oPgFrm.Page2.oPag.oStr_2_31.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_33.visible=!this.oPgFrm.Page2.oPag.oBtn_2_33.mHide()
    this.oPgFrm.Page2.oPag.oDENOM_TIT_2_34.visible=!this.oPgFrm.Page2.oPag.oDENOM_TIT_2_34.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_35.visible=!this.oPgFrm.Page2.oPag.oBtn_2_35.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_36.visible=!this.oPgFrm.Page2.oPag.oStr_2_36.mHide()
    this.oPgFrm.Page2.oPag.oDENOM_RES_2_37.visible=!this.oPgFrm.Page2.oPag.oDENOM_RES_2_37.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_38.visible=!this.oPgFrm.Page2.oPag.oStr_2_38.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_39.visible=!this.oPgFrm.Page2.oPag.oStr_2_39.mHide()
    this.oPgFrm.Page2.oPag.oCCDESPIA_2_40.visible=!this.oPgFrm.Page2.oPag.oCCDESPIA_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_41.visible=!this.oPgFrm.Page2.oPag.oStr_2_41.mHide()
    this.oPgFrm.Page2.oPag.oDESPRA_2_42.visible=!this.oPgFrm.Page2.oPag.oDESPRA_2_42.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_43.visible=!this.oPgFrm.Page2.oPag.oStr_2_43.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_44.visible=!this.oPgFrm.Page2.oPag.oStr_2_44.mHide()
    this.oPgFrm.Page2.oPag.oDESNOM_2_45.visible=!this.oPgFrm.Page2.oPag.oDESNOM_2_45.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_46.visible=!this.oPgFrm.Page2.oPag.oStr_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_120.visible=!this.oPgFrm.Page1.oPag.oStr_1_120.mHide()
    this.oPgFrm.Page1.oPag.oTOTSPE_1_121.visible=!this.oPgFrm.Page1.oPag.oTOTSPE_1_121.mHide()
    this.oPgFrm.Page1.oPag.oTOTANT_1_122.visible=!this.oPgFrm.Page1.oPag.oTOTANT_1_122.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_123.visible=!this.oPgFrm.Page1.oPag.oStr_1_123.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_124.visible=!this.oPgFrm.Page1.oPag.oStr_1_124.mHide()
    this.oPgFrm.Page1.oPag.oTOTCON_1_125.visible=!this.oPgFrm.Page1.oPag.oTOTCON_1_125.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_126.visible=!this.oPgFrm.Page1.oPag.oStr_1_126.mHide()
    this.oPgFrm.Page2.oPag.oCCDESPIA_2_50.visible=!this.oPgFrm.Page2.oPag.oCCDESPIA_2_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_130.visible=!this.oPgFrm.Page1.oPag.oStr_1_130.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_131.visible=!this.oPgFrm.Page1.oPag.oStr_1_131.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_53.visible=!this.oPgFrm.Page2.oPag.oStr_2_53.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NRTSRHMIVM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_SXCNCBAXOP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.AGZRP_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("Ricerca")
          .Calculate_QZFXXCJOCZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ImpostaDataOdierna")
          .Calculate_LYBISUKBKM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_KGFZQZXEQS()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_79.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_99.Event(cEvent)
      .oPgFrm.Page1.oPag.STAMPA_AHR.Event(cEvent)
      .oPgFrm.Page2.oPag.STAMPA_ALTE.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_krp
    * --- Al checked/unchecked della riga sullo zoom non � immediatamente visualizzato
    * --- o nascosto il bottone completa (la HideControls scatta solo al cambio
    * --- di riga dello zoom, per cui forzo la chiamata).
    If upper(cEvent)='W_AGKRA_ZOOM ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM ROW UNCHECKED' OR upper(cEvent)='W_AGKRA_ZOOM3 ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM3 ROW UNCHECKED'
        this.mHideControls()
    endif
    if cevent='Done'
        * --- Drop temporary table TMPSTPRE
        local i_nIdx,i_tabname
    
         i_nIdx=cp_GetTableDefIdx('TMPSTPRE')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPSTPRE')
        endif
    endif
    if .F.
      This.w_FLESPA='S'
      This.mHideControls()
      Select (This.w_AGZRP_ZOOM.cCursor)
      Go Top
      SUM Nvl(DAVALRIG,0) TO This.w_TOTCON  FOR  Nvl(ARPRESTA,' ') $ 'IETPGRC'
      SUM Nvl(DAVALRIG,0) TO This.w_TOTSPE  FOR  Nvl(ARPRESTA,' ') = 'S'
      SUM Nvl(DAVALRIG,0) TO This.w_TOTANT  FOR  Nvl(ARPRESTA,' ') = 'A'
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAGENPRE";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PAGENPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_PAGENPRE = NVL(_Link_.PAGENPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_PAGENPRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READ_AGEN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READ_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READ_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLR_AG";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READ_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READ_AGEN)
            select PACODAZI,PAFLR_AG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READ_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PAFLR_AG = NVL(_Link_.PAFLR_AG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READ_AGEN = space(5)
      endif
      this.w_PAFLR_AG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READ_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_bzz',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_1_26'),i_cWhere,'gsar_bzz',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(100)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_28'),i_cWhere,'GSAR_ANO',""+iif(isalt (), "Soggetti esterni", "Nominativi") +"",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(20)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRES
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_CODRES))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODRES)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODRES)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODRES) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCODRES_1_29'),i_cWhere,'GSAR_BDZ',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODRES);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CODRES)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRES = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODRES = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodUte
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodUte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CodUte);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CodUte)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CodUte) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oCodUte_1_30'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodUte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CodUte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CodUte)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodUte = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CodUte = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodUte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSER
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODSER))
          select CACODICE,CADESART,CADESSUP,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSER)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODSER) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODSER_1_31'),i_cWhere,'GSMA_ACA',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODSER)
            select CACODICE,CADESART,CADESSUP,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSER = NVL(_Link_.CACODICE,space(20))
      this.w_COMODO = NVL(_Link_.CADESART,space(30))
      this.w_DesAgg = NVL(_Link_.CADESSUP,space(0))
      this.w_CACODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODSER = space(20)
      endif
      this.w_COMODO = space(30)
      this.w_DesAgg = space(0)
      this.w_CACODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_CACODART, 'FM-FO-DE')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODSER = space(20)
        this.w_COMODO = space(30)
        this.w_DesAgg = space(0)
        this.w_CACODART = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODART
  func Link_1_72(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CACODART)
            select ARCODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODART = NVL(_Link_.ARCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CACODART = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_bzz',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_2_1'),i_cWhere,'gsar_bzz',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(100)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CC_CONTO
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CC_CONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CC_CONTO)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CC_CONTO))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CC_CONTO)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CC_CONTO) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCC_CONTO_2_2'),i_cWhere,'',"Centri di costo/Ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CC_CONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CC_CONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CC_CONTO)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CC_CONTO = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CC_CONTO = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CC_CONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIT_PRA
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIT_PRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_DPTIPRIS;
                     ,'DPCODICE',trim(this.w_TIT_PRA))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIT_PRA)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_TIT_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TIT_PRA) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oTIT_PRA_2_3'),i_cWhere,'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPTIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIT_PRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_TIT_PRA);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_DPTIPRIS;
                       ,'DPCODICE',this.w_TIT_PRA)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIT_PRA = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNTIT = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMETIT = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TIT_PRA = space(5)
      endif
      this.w_COGNTIT = space(40)
      this.w_NOMETIT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIT_PRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RES_PRA
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RES_PRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_DPTIPRIS;
                     ,'DPCODICE',trim(this.w_RES_PRA))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RES_PRA)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_RES_PRA)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DPTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RES_PRA) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oRES_PRA_2_4'),i_cWhere,'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPTIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RES_PRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_RES_PRA);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DPTIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_DPTIPRIS;
                       ,'DPCODICE',this.w_RES_PRA)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RES_PRA = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNRES = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMERES = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RES_PRA = space(5)
      endif
      this.w_COGNRES = space(40)
      this.w_NOMERES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RES_PRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_2_5'),i_cWhere,'GSAR_ANO',""+iif(isalt (), "Soggetti esterni", "Nominativi") +"",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(30))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(20)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(30)
      this.w_TIPNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(20)
        this.w_DESNOM = space(40)
        this.w_COMODO = space(30)
        this.w_TIPNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CC_CONTO
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CC_CONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CC_CONTO)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CC_CONTO))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CC_CONTO)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CC_CONTO) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCC_CONTO_2_8'),i_cWhere,'',"Centri di costo/Ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CC_CONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CC_CONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CC_CONTO)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CC_CONTO = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CC_CONTO = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CC_CONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_7.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_7.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_12.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_12.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltTipRig_1_13.RadioValue()==this.w_FiltTipRig)
      this.oPgFrm.Page1.oPag.oFiltTipRig_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltTipRig_1_20.RadioValue()==this.w_FiltTipRig)
      this.oPgFrm.Page1.oPag.oFiltTipRig_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltTipRi2_1_21.RadioValue()==this.w_FiltTipRi2)
      this.oPgFrm.Page1.oPag.oFiltTipRi2_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_22.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_23.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_23.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_24.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_24.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oInDocum_1_25.RadioValue()==this.w_InDocum)
      this.oPgFrm.Page1.oPag.oInDocum_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRA_1_26.value==this.w_CODPRA)
      this.oPgFrm.Page1.oPag.oCODPRA_1_26.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_28.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_28.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODRES_1_29.value==this.w_CODRES)
      this.oPgFrm.Page1.oPag.oCODRES_1_29.value=this.w_CODRES
    endif
    if not(this.oPgFrm.Page1.oPag.oCodUte_1_30.value==this.w_CodUte)
      this.oPgFrm.Page1.oPag.oCodUte_1_30.value=this.w_CodUte
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSER_1_31.value==this.w_CODSER)
      this.oPgFrm.Page1.oPag.oCODSER_1_31.value=this.w_CODSER
    endif
    if not(this.oPgFrm.Page1.oPag.oPRESERIAL_1_32.value==this.w_PRESERIAL)
      this.oPgFrm.Page1.oPag.oPRESERIAL_1_32.value=this.w_PRESERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDesSer_1_33.value==this.w_DesSer)
      this.oPgFrm.Page1.oPag.oDesSer_1_33.value=this.w_DesSer
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRA_1_38.value==this.w_DESPRA)
      this.oPgFrm.Page1.oPag.oDESPRA_1_38.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_PART_1_40.value==this.w_DENOM_PART)
      this.oPgFrm.Page1.oPag.oDENOM_PART_1_40.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_41.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_41.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_61.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_61.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDURTOTORE_1_85.value==this.w_DURTOTORE)
      this.oPgFrm.Page1.oPag.oDURTOTORE_1_85.value=this.w_DURTOTORE
    endif
    if not(this.oPgFrm.Page1.oPag.oDURTOTMIN_1_86.value==this.w_DURTOTMIN)
      this.oPgFrm.Page1.oPag.oDURTOTMIN_1_86.value=this.w_DURTOTMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTQTAORE_1_89.value==this.w_TOTQTAORE)
      this.oPgFrm.Page1.oPag.oTOTQTAORE_1_89.value=this.w_TOTQTAORE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTQTAMIN_1_90.value==this.w_TOTQTAMIN)
      this.oPgFrm.Page1.oPag.oTOTQTAMIN_1_90.value=this.w_TOTQTAMIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPRA_2_1.value==this.w_CODPRA)
      this.oPgFrm.Page2.oPag.oCODPRA_2_1.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oCC_CONTO_2_2.value==this.w_CC_CONTO)
      this.oPgFrm.Page2.oPag.oCC_CONTO_2_2.value=this.w_CC_CONTO
    endif
    if not(this.oPgFrm.Page2.oPag.oTIT_PRA_2_3.value==this.w_TIT_PRA)
      this.oPgFrm.Page2.oPag.oTIT_PRA_2_3.value=this.w_TIT_PRA
    endif
    if not(this.oPgFrm.Page2.oPag.oRES_PRA_2_4.value==this.w_RES_PRA)
      this.oPgFrm.Page2.oPag.oRES_PRA_2_4.value=this.w_RES_PRA
    endif
    if not(this.oPgFrm.Page2.oPag.oCODNOM_2_5.value==this.w_CODNOM)
      this.oPgFrm.Page2.oPag.oCODNOM_2_5.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPPRA_2_6.RadioValue()==this.w_TIPPRA)
      this.oPgFrm.Page2.oPag.oTIPPRA_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPANA_2_7.RadioValue()==this.w_TIPANA)
      this.oPgFrm.Page2.oPag.oTIPANA_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCC_CONTO_2_8.value==this.w_CC_CONTO)
      this.oPgFrm.Page2.oPag.oCC_CONTO_2_8.value=this.w_CC_CONTO
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPZERO_2_9.RadioValue()==this.w_IMPZERO)
      this.oPgFrm.Page2.oPag.oIMPZERO_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFL_SOSP_2_10.RadioValue()==this.w_FL_SOSP)
      this.oPgFrm.Page2.oPag.oFL_SOSP_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRIFERIMENTO_2_11.value==this.w_RIFERIMENTO)
      this.oPgFrm.Page2.oPag.oRIFERIMENTO_2_11.value=this.w_RIFERIMENTO
    endif
    if not(this.oPgFrm.Page2.oPag.oNORESP_2_12.RadioValue()==this.w_NORESP)
      this.oPgFrm.Page2.oPag.oNORESP_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNORESP_2_13.RadioValue()==this.w_NORESP)
      this.oPgFrm.Page2.oPag.oNORESP_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOPRAT_2_14.RadioValue()==this.w_NOPRAT)
      this.oPgFrm.Page2.oPag.oNOPRAT_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOnoCiv_2_15.RadioValue()==this.w_OnoCiv)
      this.oPgFrm.Page2.oPag.oOnoCiv_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDirCiv_2_16.RadioValue()==this.w_DirCiv)
      this.oPgFrm.Page2.oPag.oDirCiv_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPreTempo_2_17.RadioValue()==this.w_PreTempo)
      this.oPgFrm.Page2.oPag.oPreTempo_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOnoPen_2_18.RadioValue()==this.w_OnoPen)
      this.oPgFrm.Page2.oPag.oOnoPen_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDirStrag_2_19.RadioValue()==this.w_DirStrag)
      this.oPgFrm.Page2.oPag.oDirStrag_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSpese_2_20.RadioValue()==this.w_Spese)
      this.oPgFrm.Page2.oPag.oSpese_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOnoStrag_2_21.RadioValue()==this.w_OnoStrag)
      this.oPgFrm.Page2.oPag.oOnoStrag_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPreGen_2_22.RadioValue()==this.w_PreGen)
      this.oPgFrm.Page2.oPag.oPreGen_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAnticip_2_23.RadioValue()==this.w_Anticip)
      this.oPgFrm.Page2.oPag.oAnticip_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEscFac_2_25.RadioValue()==this.w_EscFac)
      this.oPgFrm.Page2.oPag.oEscFac_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEvasio_2_26.RadioValue()==this.w_Evasio)
      this.oPgFrm.Page2.oPag.oEvasio_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVisTotali_2_27.RadioValue()==this.w_VisTotali)
      this.oPgFrm.Page2.oPag.oVisTotali_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDatiPratica_2_28.RadioValue()==this.w_DatiPratica)
      this.oPgFrm.Page2.oPag.oDatiPratica_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAnnotazioni_2_29.RadioValue()==this.w_Annotazioni)
      this.oPgFrm.Page2.oPag.oAnnotazioni_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSoloTot_2_30.RadioValue()==this.w_SoloTot)
      this.oPgFrm.Page2.oPag.oSoloTot_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDENOM_TIT_2_34.value==this.w_DENOM_TIT)
      this.oPgFrm.Page2.oPag.oDENOM_TIT_2_34.value=this.w_DENOM_TIT
    endif
    if not(this.oPgFrm.Page2.oPag.oDENOM_RES_2_37.value==this.w_DENOM_RES)
      this.oPgFrm.Page2.oPag.oDENOM_RES_2_37.value=this.w_DENOM_RES
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESPIA_2_40.value==this.w_CCDESPIA)
      this.oPgFrm.Page2.oPag.oCCDESPIA_2_40.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRA_2_42.value==this.w_DESPRA)
      this.oPgFrm.Page2.oPag.oDESPRA_2_42.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM_2_45.value==this.w_DESNOM)
      this.oPgFrm.Page2.oPag.oDESNOM_2_45.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSPE_1_121.value==this.w_TOTSPE)
      this.oPgFrm.Page1.oPag.oTOTSPE_1_121.value=this.w_TOTSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTANT_1_122.value==this.w_TOTANT)
      this.oPgFrm.Page1.oPag.oTOTANT_1_122.value=this.w_TOTANT
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCON_1_125.value==this.w_TOTCON)
      this.oPgFrm.Page1.oPag.oTOTCON_1_125.value=this.w_TOTCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESPIA_2_50.value==this.w_CCDESPIA)
      this.oPgFrm.Page2.oPag.oCCDESPIA_2_50.value=this.w_CCDESPIA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(ChkTipArt(.w_CACODART, 'FM-FO-DE'))  and not(empty(.w_CODSER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODSER_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_PRESERIAL)<>0 OR EMPTY(.w_PRESERIAL))  and not(.w_PAGENPRE<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRESERIAL_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and not(NOT EMPTY(.w_CODPRA) OR NOT ISALT())  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODNOM_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_READAZI = this.w_READAZI
    this.o_DATINI = this.w_DATINI
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_DATFIN = this.w_DATFIN
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_InDocum = this.w_InDocum
    this.o_CODPRA = this.w_CODPRA
    this.o_CODNOM = this.w_CODNOM
    this.o_CODRES = this.w_CODRES
    this.o_DesSer = this.w_DesSer
    this.o_TIT_PRA = this.w_TIT_PRA
    this.o_RES_PRA = this.w_RES_PRA
    this.o_NORESP = this.w_NORESP
    this.o_NOPRAT = this.w_NOPRAT
    return

enddefine

* --- Define pages as container
define class tgsag_krpPag1 as StdContainer
  Width  = 822
  height = 582
  stdWidth  = 822
  stdheight = 582
  resizeXpos=643
  resizeYpos=407
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_7 as StdField with uid="VAJTUNAFFS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 146514378,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=80, Top=4

  func oDATINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_12 as StdField with uid="VRHGJGBHQK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 68067786,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=80, Top=28

  func oDATFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc


  add object oFiltTipRig_1_13 as StdCombo with uid="SPYQCZKHCX",value=3,rtseq=8,rtrep=.f.,left=324,top=4,width=117,height=21;
    , ToolTipText = "Tipologia riga per la generazione del documento";
    , HelpContextID = 137268168;
    , cFormVar="w_FiltTipRig",RowSource=""+"Non fatturabile,"+"Fatturabile,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFiltTipRig_1_13.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,' ',;
    space(10)))))
  endfunc
  func oFiltTipRig_1_13.GetRadio()
    this.Parent.oContained.w_FiltTipRig = this.RadioValue()
    return .t.
  endfunc

  func oFiltTipRig_1_13.SetRadio()
    this.Parent.oContained.w_FiltTipRig=trim(this.Parent.oContained.w_FiltTipRig)
    this.value = ;
      iif(this.Parent.oContained.w_FiltTipRig=='N',1,;
      iif(this.Parent.oContained.w_FiltTipRig=='D',2,;
      iif(this.Parent.oContained.w_FiltTipRig=='',3,;
      0)))
  endfunc

  func oFiltTipRig_1_13.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oBtn_1_19 as StdButton with uid="BQJHHOQZPU",left=164, top=5, width=48,height=45,;
    CpPicture="bmp\date-time.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per impostare la data odierna";
    , HelpContextID = 159757542;
    , caption='\<Oggi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      this.parent.oContained.NotifyEvent("ImpostaDataOdierna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oFiltTipRig_1_20 as StdCombo with uid="BXKRDNCYGK",value=5,rtseq=11,rtrep=.f.,left=324,top=4,width=117,height=21;
    , ToolTipText = "Tipologia riga per la generazione del documento";
    , HelpContextID = 137268168;
    , cFormVar="w_FiltTipRig",RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. acquisti,"+"Entrambi,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFiltTipRig_1_20.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'A',;
    iif(this.value =4,'E',;
    iif(this.value =5,' ',;
    space(10)))))))
  endfunc
  func oFiltTipRig_1_20.GetRadio()
    this.Parent.oContained.w_FiltTipRig = this.RadioValue()
    return .t.
  endfunc

  func oFiltTipRig_1_20.SetRadio()
    this.Parent.oContained.w_FiltTipRig=trim(this.Parent.oContained.w_FiltTipRig)
    this.value = ;
      iif(this.Parent.oContained.w_FiltTipRig=='N',1,;
      iif(this.Parent.oContained.w_FiltTipRig=='D',2,;
      iif(this.Parent.oContained.w_FiltTipRig=='A',3,;
      iif(this.Parent.oContained.w_FiltTipRig=='E',4,;
      iif(this.Parent.oContained.w_FiltTipRig=='',5,;
      0)))))
  endfunc

  func oFiltTipRig_1_20.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oFiltTipRi2_1_21 as StdCombo with uid="ALGPUZNRKP",value=3,rtseq=12,rtrep=.f.,left=324,top=28,width=117,height=21;
    , ToolTipText = "Tipologia riga per la generazione della nota spese";
    , HelpContextID = 137281736;
    , cFormVar="w_FiltTipRi2",RowSource=""+"Senza nota spese,"+"Con nota spese,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFiltTipRi2_1_21.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,' ',;
    space(10)))))
  endfunc
  func oFiltTipRi2_1_21.GetRadio()
    this.Parent.oContained.w_FiltTipRi2 = this.RadioValue()
    return .t.
  endfunc

  func oFiltTipRi2_1_21.SetRadio()
    this.Parent.oContained.w_FiltTipRi2=trim(this.Parent.oContained.w_FiltTipRi2)
    this.value = ;
      iif(this.Parent.oContained.w_FiltTipRi2=='N',1,;
      iif(this.Parent.oContained.w_FiltTipRi2=='D',2,;
      iif(this.Parent.oContained.w_FiltTipRi2=='',3,;
      0)))
  endfunc

  func oFiltTipRi2_1_21.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oSTATO_1_22 as StdCombo with uid="HPCXRCGRJY",rtseq=13,rtrep=.f.,left=538,top=4,width=208,height=22;
    , ToolTipText = "Check stato (� possibile ricercare le prestazioni completate e da completare; queste ultime saranno evidenziate in rosso)";
    , HelpContextID = 241058342;
    , cFormVar="w_STATO",RowSource=""+"Completate,"+"Da completare,"+"Da completare (solo evase),"+"Da completare (solo non evase),"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_22.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'N',;
    iif(this.value =3,'F',;
    iif(this.value =4,'S',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oSTATO_1_22.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_22.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='P',1,;
      iif(this.Parent.oContained.w_STATO=='N',2,;
      iif(this.Parent.oContained.w_STATO=='F',3,;
      iif(this.Parent.oContained.w_STATO=='S',4,;
      iif(this.Parent.oContained.w_STATO=='T',5,;
      0)))))
  endfunc

  add object oNUMDOC_1_23 as StdField with uid="ALSVTWVYIC",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'attivit�",;
    HelpContextID = 246480170,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=324, Top=27, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMDOC_1_23.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oALFDOC_1_24 as StdField with uid="TNHYATACTB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie dell'attivit�",;
    HelpContextID = 246511354,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=449, Top=27, InputMask=replicate('X',10)

  func oALFDOC_1_24.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc


  add object oInDocum_1_25 as StdCombo with uid="XOECEMTZKC",rtseq=16,rtrep=.f.,left=594,top=28,width=152,height=22;
    , ToolTipText = "Stato";
    , HelpContextID = 189166714;
    , cFormVar="w_InDocum",RowSource=""+"Da proformare,"+"Da fatturare (Proformate),"+"Da fatturare (tutte),"+"Fatturate,"+"Proformate e fatturate,"+"Tutte,"+"Proformate o fatturate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oInDocum_1_25.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'D',;
    iif(this.value =4,'F',;
    iif(this.value =5,'A',;
    iif(this.value =6,'T',;
    iif(this.value =7,'M',;
    space(1)))))))))
  endfunc
  func oInDocum_1_25.GetRadio()
    this.Parent.oContained.w_InDocum = this.RadioValue()
    return .t.
  endfunc

  func oInDocum_1_25.SetRadio()
    this.Parent.oContained.w_InDocum=trim(this.Parent.oContained.w_InDocum)
    this.value = ;
      iif(this.Parent.oContained.w_InDocum=='N',1,;
      iif(this.Parent.oContained.w_InDocum=='P',2,;
      iif(this.Parent.oContained.w_InDocum=='D',3,;
      iif(this.Parent.oContained.w_InDocum=='F',4,;
      iif(this.Parent.oContained.w_InDocum=='A',5,;
      iif(this.Parent.oContained.w_InDocum=='T',6,;
      iif(this.Parent.oContained.w_InDocum=='M',7,;
      0)))))))
  endfunc

  func oInDocum_1_25.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_STATO<>'P')
    endwith
  endfunc

  add object oCODPRA_1_26 as StdField with uid="LNZCMMZIKS",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 7705562,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=80, Top=52, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCODPRA)
    endwith
   endif
  endfunc

  func oCODPRA_1_26.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oCODPRA_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_bzz',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRA_1_26.mZoomOnZoom
    local i_obj
    i_obj=gsar_bzz()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRA
     i_obj.ecpSave()
  endproc

  add object oCODNOM_1_28 as StdField with uid="LDORSNJAGM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 78091226,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=80, Top=52, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_28.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODPRA) OR ISALT())
    endwith
  endfunc

  func oCODNOM_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',""+iif(isalt (), "Soggetti esterni", "Nominativi") +"",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc

  add object oCODRES_1_29 as StdField with uid="QJYWRUDDRU",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODRES", cQueryName = "CODRES",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice responsabile prestazione",;
    HelpContextID = 256087002,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=80, Top=75, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_CODRES"

  func oCODRES_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODRES_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODRES_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCODRES_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oCODRES_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TipoRisorsa
     i_obj.w_DPCODICE=this.parent.oContained.w_CODRES
     i_obj.ecpSave()
  endproc

  add object oCodUte_1_30 as StdField with uid="PIUVUYSBHD",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CodUte", cQueryName = "CodUte",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente ultima modifica",;
    HelpContextID = 172913626,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=485, Top=75, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_CodUte"

  func oCodUte_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodUte_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodUte_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oCodUte_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oCODSER_1_31 as StdField with uid="NQAZQYRMPT",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODSER", cQueryName = "CODSER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 4363226,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=80, Top=99, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODSER"

  func oCODSER_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSER_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSER_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODSER_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCODSER_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODSER
     i_obj.ecpSave()
  endproc

  add object oPRESERIAL_1_32 as StdField with uid="DYDDJNZPAW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PRESERIAL", cQueryName = "PRESERIAL",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo prestazione: effettua la ricerca mediante il seriale della prestazione",;
    HelpContextID = 264078583,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=330, Top=99, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  func oPRESERIAL_1_32.mHide()
    with this.Parent.oContained
      return (.w_PAGENPRE<>'S')
    endwith
  endfunc

  proc oPRESERIAL_1_32.mAfter
    with this.Parent.oContained
      .w_PRESERIAL = IIF(!empty(.w_PRESERIAL), right(repl('0', 10) + alltrim(.w_PRESERIAL), 10), space(10))
    endwith
  endproc

  func oPRESERIAL_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PRESERIAL)<>0 OR EMPTY(.w_PRESERIAL))
    endwith
    return bRes
  endfunc

  add object oDesSer_1_33 as AH_SEARCHFLD with uid="RAJBAVJSRR",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DesSer", cQueryName = "DesSer",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prestazione o parte di essa",;
    HelpContextID = 239046090,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=485, Top=101, InputMask=replicate('X',40)


  add object oObj_1_35 as cp_runprogram with uid="UAIOGLNCRD",left=20, top=813, width=295,height=19,;
    caption='GSAG_BRA(P, A)',;
   bGlobalFont=.t.,;
    prg="GSAG_BRA('P', w_TIPOPRE)",;
    cEvent = "w_AGZRP_ZOOM selected",;
    nPag=1;
    , HelpContextID = 243553063

  add object oDESPRA_1_38 as StdField with uid="DHZPCSQKFH",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 7646666,;
   bGlobalFont=.t.,;
    Height=21, Width=597, Left=216, Top=52, InputMask=replicate('X',100)

  func oDESPRA_1_38.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oDENOM_PART_1_40 as StdField with uid="SKXCKTCAOH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 46507113,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=146, Top=75, InputMask=replicate('X',100)

  add object oDESUTE_1_41 as StdField with uid="JXGTZBFHCK",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 206548426,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=555, Top=75, InputMask=replicate('X',20)


  add object oBtn_1_43 as StdButton with uid="ARHYBOCNYM",left=8, top=536, width=48,height=45,;
    CpPicture="bmp\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la prestazione selezionata";
    , HelpContextID = 159804678;
    , caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        GSAG_BRA(this.Parent.oContained,"P", .w_TIPOPRE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_ATSERIAL))
     endwith
    endif
  endfunc


  add object oBtn_1_44 as StdButton with uid="QJTUZLZBDL",left=58, top=536, width=48,height=45,;
    CpPicture="bmp\Modifica.BMP", caption="", nPag=1;
    , ToolTipText = "Modifica la prestazione selezionata";
    , HelpContextID = 149261607;
    , caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSAG_BRA(this.Parent.oContained,"R", .w_TIPOPRE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_44.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_44.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_ATSERIAL))
     endwith
    endif
  endfunc


  add object oBtn_1_45 as StdButton with uid="OGYQEMILIK",left=108, top=536, width=48,height=45,;
    CpPicture="bmp\Ins_rap.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inserire una nuova prestazione prima o dopo la prestazione selezionata in maschera e con la data uguale alla stessa.";
    , HelpContextID = 161645177;
    , caption='\<Inserisci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      with this.Parent.oContained
        do GSAG_KPD with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_45.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_ATSERIAL) OR !Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_46 as StdButton with uid="ZIKYOKGOJX",left=665, top=532, width=48,height=45,;
    CpPicture="bmp\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare un nuova prestazione ";
    , HelpContextID = 259833130;
    , caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      with this.Parent.oContained
        GSAG_BCK(this.Parent.oContained,"W")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_46.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_PAGENPRE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_47 as StdButton with uid="DNCLXEYAGY",left=158, top=536, width=48,height=45,;
    CpPicture="bitmap.bmp", caption="", nPag=1;
    , ToolTipText = "Legenda colori";
    , HelpContextID = 194923850;
    , caption='\<Legenda';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      do gsag_klp with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (IsAlt())
      endwith
    endif
  endfunc

  func oBtn_1_47.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt() OR EMPTY(.w_ATSERIAL))
     endwith
    endif
  endfunc


  add object oBtn_1_48 as StdButton with uid="UUNSDLSMLD",left=715, top=536, width=48,height=45,;
    CpPicture="bmp\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare una o pi� prestazioni utilizzando la maschera di inserimento provvisorio";
    , HelpContextID = 189528405;
    , caption='\<Ins. prov.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        GSAG_BCK(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_50 as StdButton with uid="TKCMXZXILU",left=765, top=536, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 159744070;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_50.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGZRP_ZOOM as cp_zoombox with uid="MPOPQLEMXY",left=5, top=120, width=817,height=380,;
    caption='AGZRP_ZOOM',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",bQueryOnDblClick=.t.,bRetriveAllRows=.f.,cTable="OFF_ATTI",cZoomFile="GSAG_ZRP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="GSAG_ZRP",bNoZoomGridShape=.f.,;
    nPag=1;
    , HelpContextID = 225318469

  add object oDESNOM_1_61 as StdField with uid="YBKTMCLSXL",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78032330,;
   bGlobalFont=.t.,;
    Height=21, Width=597, Left=216, Top=52, InputMask=replicate('X',40)

  func oDESNOM_1_61.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc


  add object oBtn_1_73 as StdButton with uid="RTTPWGKOJR",left=765, top=5, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 207522026;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_73.Click()
      with this.Parent.oContained
        gsag_bsp(this.Parent.oContained,"RICERCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_79 as cp_setobjprop with uid="VBHIYEPEDD",left=616, top=669, width=182,height=19,;
    caption='ToolTip di w_CODNOM',;
   bGlobalFont=.t.,;
    cObj="w_CODNOM",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 20435130


  add object oObj_1_81 as cp_runprogram with uid="TVWZBSDPNV",left=20, top=788, width=296,height=22,;
    caption='GSAG_BSP(POSTIN)',;
   bGlobalFont=.t.,;
    prg="GSAG_BSP('POSTIN')",;
    cEvent = "w_CODSER Changed",;
    nPag=1;
    , HelpContextID = 266961759

  add object oDURTOTORE_1_85 as StdField with uid="DABIHVXTKN",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DURTOTORE", cQueryName = "DURTOTORE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale durata effettiva in ore",;
    HelpContextID = 228632872,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=733, Top=507, cSayPict='"9999"', cGetPict='"9999"'

  func oDURTOTORE_1_85.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDURTOTMIN_1_86 as StdField with uid="HMXORHVZVX",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DURTOTMIN", cQueryName = "DURTOTMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale durata effettiva in minuti",;
    HelpContextID = 228632737,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=779, Top=507, cSayPict='"999"', cGetPict='"999" '

  func oDURTOTMIN_1_86.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oTOTQTAORE_1_89 as StdField with uid="EBCHRTMXJV",rtseq=48,rtrep=.f.,;
    cFormVar = "w_TOTQTAORE", cQueryName = "TOTQTAORE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale durata in ore",;
    HelpContextID = 5475880,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=553, Top=507, cSayPict='"9999"', cGetPict='"9999"'

  func oTOTQTAORE_1_89.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oTOTQTAMIN_1_90 as StdField with uid="WOLMPNEBCQ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_TOTQTAMIN", cQueryName = "TOTQTAMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale durata in minuti",;
    HelpContextID = 5475745,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=598, Top=507, cSayPict='"999"', cGetPict='"999"'

  func oTOTQTAMIN_1_90.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oObj_1_99 as cp_setobjprop with uid="ABADDSFJRN",left=616, top=643, width=182,height=22,;
    caption='ToolTip di w_CODRES',;
   bGlobalFont=.t.,;
    cObj="w_CODRES",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 20457210


  add object STAMPA_AHR as cp_outputCombo with uid="QREZPKJWXN",left=416, top=557, width=247,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 206446618


  add object oBtn_1_104 as StdButton with uid="ENFXDUPVYK",left=665, top=536, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 242654682;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_104.Click()
      with this.Parent.oContained
        GSAG_BSP(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_104.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc

  func oBtn_1_104.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (ISALT())
     endwith
    endif
  endfunc


  add object oBtn_1_116 as StdButton with uid="ZPXFKKEARK",left=208, top=536, width=48,height=45,;
    CpPicture="BMP\calcola.ico", caption="", nPag=1;
    , ToolTipText = "Premere per vedere un'anteprima dei calcoli della parcella";
    , HelpContextID = 59769306;
    , Caption='Calco\<la';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_116.Click()
      do GSAG1KRP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_116.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT() or .w_FLESPA='N' OR !.w_INDOCUM $ 'N-P-D' OR Empty(.w_CODPRA))
     endwith
    endif
  endfunc

  add object oTOTSPE_1_121 as StdField with uid="JMEMGHSCVX",rtseq=101,rtrep=.f.,;
    cFormVar = "w_TOTSPE", cQueryName = "TOTSPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210866890,;
   bGlobalFont=.t.,;
    Height=21, Width=102, Left=227, Top=507, cSayPict='"9999999999999.99"', cGetPict='"9999999999999.9999"'

  func oTOTSPE_1_121.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR .w_VisTotali<>'S')
    endwith
  endfunc

  add object oTOTANT_1_122 as StdField with uid="IQIVKFKJRB",rtseq=102,rtrep=.f.,;
    cFormVar = "w_TOTANT", cQueryName = "TOTANT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 230920906,;
   bGlobalFont=.t.,;
    Height=21, Width=102, Left=406, Top=507, cSayPict='"9999999999999.99"', cGetPict='"9999999999999.9999"'

  func oTOTANT_1_122.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR .w_VisTotali<>'S')
    endwith
  endfunc

  add object oTOTCON_1_125 as StdField with uid="IZAOTWGTKC",rtseq=103,rtrep=.f.,;
    cFormVar = "w_TOTCON", cQueryName = "TOTCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 61969098,;
   bGlobalFont=.t.,;
    Height=21, Width=102, Left=74, Top=507, cSayPict='"9999999999999.99"', cGetPict='"9999999999999.9999"'

  func oTOTCON_1_125.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR .w_VisTotali<>'S')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=8, Top=4,;
    Alignment=1, Width=69, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="JGUELPQNWG",Visible=.t., Left=473, Top=4,;
    Alignment=1, Width=58, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="HZALIZNENK",Visible=.t., Left=8, Top=52,;
    Alignment=1, Width=69, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="EIENBMKRMG",Visible=.t., Left=429, Top=75,;
    Alignment=1, Width=53, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="FNKXTFHGOF",Visible=.t., Left=29, Top=99,;
    Alignment=1, Width=48, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="CHDCKYFLMH",Visible=.t., Left=8, Top=75,;
    Alignment=1, Width=69, Height=18,;
    Caption="Resp.:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="RXWUWYGUBK",Visible=.t., Left=400, Top=99,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="KXVPXZIESN",Visible=.t., Left=8, Top=28,;
    Alignment=1, Width=69, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="IAJQIPOUBI",Visible=.t., Left=8, Top=75,;
    Alignment=1, Width=69, Height=18,;
    Caption="Part.:"  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="EMOTHKNPJM",Visible=.t., Left=12, Top=52,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_78.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="AUEJGKPLZP",Visible=.t., Left=9, Top=99,;
    Alignment=1, Width=68, Height=18,;
    Caption="Prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="NHQJGSHCLY",Visible=.t., Left=52, Top=502,;
    Alignment=0, Width=357, Height=17,;
    Caption="Prestazione esclusa/da escludere dalla generazione documenti"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="VUNFMVNLKY",Visible=.t., Left=8, Top=502,;
    Alignment=0, Width=44, Height=17,;
    Caption="(Blu)"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="UBRANMXJLM",Visible=.t., Left=774, Top=507,;
    Alignment=0, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="YWNVWNACCV",Visible=.t., Left=612, Top=507,;
    Alignment=1, Width=119, Height=18,;
    Caption="Durata effettiva:"  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="DNVETYMCPX",Visible=.t., Left=594, Top=507,;
    Alignment=0, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="KEFNRZPVYT",Visible=.t., Left=505, Top=507,;
    Alignment=1, Width=45, Height=18,;
    Caption="Durata:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="JYNODHTYUL",Visible=.t., Left=487, Top=29,;
    Alignment=1, Width=103, Height=18,;
    Caption="In proforma/fattura:"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_STATO<>'P')
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="YSNTXANVBM",Visible=.t., Left=8, Top=519,;
    Alignment=0, Width=44, Height=17,;
    Caption="(Rosso)"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_101 as StdString with uid="HKFTKVPMBC",Visible=.t., Left=52, Top=519,;
    Alignment=0, Width=170, Height=17,;
    Caption="Prestazione da completare"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="WUTFKAIEQF",Visible=.t., Left=325, Top=559,;
    Alignment=1, Width=90, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_118 as StdString with uid="BVBIGQKQSQ",Visible=.t., Left=246, Top=99,;
    Alignment=1, Width=82, Height=18,;
    Caption="ID prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_118.mHide()
    with this.Parent.oContained
      return (.w_PAGENPRE<>'S')
    endwith
  endfunc

  add object oStr_1_119 as StdString with uid="SXGITJFDVT",Visible=.t., Left=241, Top=4,;
    Alignment=1, Width=76, Height=17,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_120 as StdString with uid="ZYJGHCCKDR",Visible=.t., Left=248, Top=29,;
    Alignment=1, Width=69, Height=18,;
    Caption="Nota spese:"  ;
  , bGlobalFont=.t.

  func oStr_1_120.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_123 as StdString with uid="THUODGOPCA",Visible=.t., Left=180, Top=507,;
    Alignment=1, Width=44, Height=18,;
    Caption="Spese:"  ;
  , bGlobalFont=.t.

  func oStr_1_123.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR .w_VisTotali<>'S')
    endwith
  endfunc

  add object oStr_1_124 as StdString with uid="QTDMYAHVLJ",Visible=.t., Left=332, Top=507,;
    Alignment=1, Width=72, Height=18,;
    Caption="Anticipazioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_124.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR .w_VisTotali<>'S')
    endwith
  endfunc

  add object oStr_1_126 as StdString with uid="RQEFXNBSZJ",Visible=.t., Left=5, Top=507,;
    Alignment=1, Width=65, Height=18,;
    Caption="Compensi:"  ;
  , bGlobalFont=.t.

  func oStr_1_126.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR .w_VisTotali<>'S')
    endwith
  endfunc

  add object oStr_1_130 as StdString with uid="LIJLPIXOSP",Visible=.t., Left=277, Top=29,;
    Alignment=1, Width=40, Height=18,;
    Caption="Num.:"  ;
  , bGlobalFont=.t.

  func oStr_1_130.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_131 as StdString with uid="JMLLMGFFZD",Visible=.t., Left=440, Top=29,;
    Alignment=1, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_131.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc
enddefine
define class tgsag_krpPag2 as StdContainer
  Width  = 822
  height = 582
  stdWidth  = 822
  stdheight = 582
  resizeXpos=429
  resizeYpos=217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODPRA_2_1 as StdField with uid="QYSOVLITNR",rtseq=67,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 7705562,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=85, Top=28, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCODPRA)
    endwith
   endif
  endfunc

  func oCODPRA_2_1.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oCODPRA_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_bzz',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRA_2_1.mZoomOnZoom
    local i_obj
    i_obj=gsar_bzz()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRA
     i_obj.ecpSave()
  endproc

  add object oCC_CONTO_2_2 as StdField with uid="QFWFOZDSRH",rtseq=68,rtrep=.f.,;
    cFormVar = "w_CC_CONTO", cQueryName = "CC_CONTO",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo/ricavo",;
    HelpContextID = 206508149,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=85, Top=60, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CC_CONTO"

  func oCC_CONTO_2_2.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oCC_CONTO_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCC_CONTO_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCC_CONTO_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCC_CONTO_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Centri di costo/Ricavo",'',this.parent.oContained
  endproc

  add object oTIT_PRA_2_3 as StdField with uid="PBOANESIVU",rtseq=69,rtrep=.f.,;
    cFormVar = "w_TIT_PRA", cQueryName = "TIT_PRA",nZero=5,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice soggetto interno della pratica con ruolo di titolare per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto interno selezionato)",;
    HelpContextID = 260413642,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=85, Top=28, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_DPTIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_TIT_PRA"

  func oTIT_PRA_2_3.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  func oTIT_PRA_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIT_PRA_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIT_PRA_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_DPTIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_DPTIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oTIT_PRA_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oRES_PRA_2_4 as StdField with uid="MLEFOAIDNT",rtseq=70,rtrep=.f.,;
    cFormVar = "w_RES_PRA", cQueryName = "RES_PRA",nZero=5,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice soggetto interno della pratica con ruolo di responsabile per cui si desidera stampare le prestazioni (la procedura ricerca le prestazioni all'interno delle pratiche ove � presente il soggetto interno selezionato)",;
    HelpContextID = 260418794,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=464, Top=28, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_DPTIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_RES_PRA"

  func oRES_PRA_2_4.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  func oRES_PRA_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oRES_PRA_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRES_PRA_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_DPTIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_DPTIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oRES_PRA_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oCODNOM_2_5 as StdField with uid="LPGLEWCVYY",rtseq=71,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 78091226,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=85, Top=60, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_2_5.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODPRA) OR NOT ISALT())
    endwith
  endfunc

  func oCODNOM_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',""+iif(isalt (), "Soggetti esterni", "Nominativi") +"",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc


  add object oTIPPRA_2_6 as StdCombo with uid="MQGYJCRNXQ",rtseq=72,rtrep=.f.,left=85,top=92,width=146,height=22;
    , ToolTipText = "Selezione su apertura/chiusura";
    , HelpContextID = 7657674;
    , cFormVar="w_TIPPRA",RowSource=""+"Aperte,"+"Chiuse,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPPRA_2_6.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPPRA_2_6.GetRadio()
    this.Parent.oContained.w_TIPPRA = this.RadioValue()
    return .t.
  endfunc

  func oTIPPRA_2_6.SetRadio()
    this.Parent.oContained.w_TIPPRA=trim(this.Parent.oContained.w_TIPPRA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPPRA=='A',1,;
      iif(this.Parent.oContained.w_TIPPRA=='C',2,;
      iif(this.Parent.oContained.w_TIPPRA=='T',3,;
      0)))
  endfunc

  func oTIPPRA_2_6.mHide()
    with this.Parent.oContained
      return (! Isalt() or !.w_EDITCODPRA)
    endwith
  endfunc


  add object oTIPANA_2_7 as StdCombo with uid="OOYOOVQSKI",rtseq=73,rtrep=.f.,left=85,top=92,width=146,height=22;
    , HelpContextID = 12835018;
    , cFormVar="w_TIPANA",RowSource=""+"Costo,"+"Ricavo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPANA_2_7.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPANA_2_7.GetRadio()
    this.Parent.oContained.w_TIPANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPANA_2_7.SetRadio()
    this.Parent.oContained.w_TIPANA=trim(this.Parent.oContained.w_TIPANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPANA=='C',1,;
      iif(this.Parent.oContained.w_TIPANA=='R',2,;
      0))
  endfunc

  func oTIPANA_2_7.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCC_CONTO_2_8 as StdField with uid="PALBMHFMEH",rtseq=74,rtrep=.f.,;
    cFormVar = "w_CC_CONTO", cQueryName = "CC_CONTO",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo/ricavo",;
    HelpContextID = 206508149,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=85, Top=123, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CC_CONTO"

  func oCC_CONTO_2_8.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR NOT ISALT())
    endwith
  endfunc

  func oCC_CONTO_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCC_CONTO_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCC_CONTO_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCC_CONTO_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Centri di costo/Ricavo",'',this.parent.oContained
  endproc

  add object oIMPZERO_2_9 as StdCheck with uid="ZDNIDPRXLA",rtseq=75,rtrep=.f.,left=11, top=154, caption="Solo prestazioni senza importo",;
    ToolTipText = "Se attivo, riporta solo le prestazioni senza importo",;
    HelpContextID = 3855738,;
    cFormVar="w_IMPZERO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIMPZERO_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMPZERO_2_9.GetRadio()
    this.Parent.oContained.w_IMPZERO = this.RadioValue()
    return .t.
  endfunc

  func oIMPZERO_2_9.SetRadio()
    this.Parent.oContained.w_IMPZERO=trim(this.Parent.oContained.w_IMPZERO)
    this.value = ;
      iif(this.Parent.oContained.w_IMPZERO=='S',1,;
      0)
  endfunc


  add object oFL_SOSP_2_10 as StdCombo with uid="SOZSLOERCZ",rtseq=76,rtrep=.f.,left=360,top=92,width=146,height=22;
    , ToolTipText = "Selezione su sospensione";
    , HelpContextID = 245425834;
    , cFormVar="w_FL_SOSP",RowSource=""+"Solo pratiche sospese,"+"Escludi pratiche sospese,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFL_SOSP_2_10.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFL_SOSP_2_10.GetRadio()
    this.Parent.oContained.w_FL_SOSP = this.RadioValue()
    return .t.
  endfunc

  func oFL_SOSP_2_10.SetRadio()
    this.Parent.oContained.w_FL_SOSP=trim(this.Parent.oContained.w_FL_SOSP)
    this.value = ;
      iif(this.Parent.oContained.w_FL_SOSP=='S',1,;
      iif(this.Parent.oContained.w_FL_SOSP=='N',2,;
      iif(this.Parent.oContained.w_FL_SOSP=='T',3,;
      0)))
  endfunc

  func oFL_SOSP_2_10.mHide()
    with this.Parent.oContained
      return (!.w_EDITCODPRA or !IsAlt())
    endwith
  endfunc

  add object oRIFERIMENTO_2_11 as StdField with uid="YATILJIPXZ",rtseq=77,rtrep=.f.,;
    cFormVar = "w_RIFERIMENTO", cQueryName = "RIFERIMENTO",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento della pratica attribuito dal cliente",;
    HelpContextID = 142290885,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=595, Top=92, InputMask=replicate('X',30)

  func oRIFERIMENTO_2_11.mHide()
    with this.Parent.oContained
      return (!IsAlt() )
    endwith
  endfunc

  add object oNORESP_2_12 as StdCheck with uid="BZDBPIHHSV",rtseq=78,rtrep=.f.,left=11, top=173, caption="Solo prestazioni senza responsabile",;
    ToolTipText = "Se attivo, riporta solo le prestazioni (da completare) senza responsabile",;
    HelpContextID = 24097578,;
    cFormVar="w_NORESP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oNORESP_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNORESP_2_12.GetRadio()
    this.Parent.oContained.w_NORESP = this.RadioValue()
    return .t.
  endfunc

  func oNORESP_2_12.SetRadio()
    this.Parent.oContained.w_NORESP=trim(this.Parent.oContained.w_NORESP)
    this.value = ;
      iif(this.Parent.oContained.w_NORESP=='S',1,;
      0)
  endfunc

  func oNORESP_2_12.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oNORESP_2_13 as StdCheck with uid="DFIYFNVVVZ",rtseq=79,rtrep=.f.,left=11, top=173, caption="Solo prestazioni senza partecipante",;
    ToolTipText = "Se attivo, riporta solo le prestazioni (da completare) senza partecipante",;
    HelpContextID = 24097578,;
    cFormVar="w_NORESP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oNORESP_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNORESP_2_13.GetRadio()
    this.Parent.oContained.w_NORESP = this.RadioValue()
    return .t.
  endfunc

  func oNORESP_2_13.SetRadio()
    this.Parent.oContained.w_NORESP=trim(this.Parent.oContained.w_NORESP)
    this.value = ;
      iif(this.Parent.oContained.w_NORESP=='S',1,;
      0)
  endfunc

  func oNORESP_2_13.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oNOPRAT_2_14 as StdCheck with uid="XXBHZHTVFO",rtseq=80,rtrep=.f.,left=11, top=197, caption="Solo prestazioni senza pratica",;
    ToolTipText = "Se attivo, riporta solo le prestazioni senza pratica",;
    HelpContextID = 243454762,;
    cFormVar="w_NOPRAT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oNOPRAT_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOPRAT_2_14.GetRadio()
    this.Parent.oContained.w_NOPRAT = this.RadioValue()
    return .t.
  endfunc

  func oNOPRAT_2_14.SetRadio()
    this.Parent.oContained.w_NOPRAT=trim(this.Parent.oContained.w_NOPRAT)
    this.value = ;
      iif(this.Parent.oContained.w_NOPRAT=='S',1,;
      0)
  endfunc

  func oNOPRAT_2_14.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oOnoCiv_2_15 as StdCheck with uid="ELVFIVSHDY",rtseq=81,rtrep=.f.,left=241, top=154, caption="Onorario civile",;
    ToolTipText = "Se attivo, riporta gli onorari civili",;
    HelpContextID = 168805402,;
    cFormVar="w_OnoCiv", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oOnoCiv_2_15.RadioValue()
    return(iif(this.value =1,'I',;
    space(1)))
  endfunc
  func oOnoCiv_2_15.GetRadio()
    this.Parent.oContained.w_OnoCiv = this.RadioValue()
    return .t.
  endfunc

  func oOnoCiv_2_15.SetRadio()
    this.Parent.oContained.w_OnoCiv=trim(this.Parent.oContained.w_OnoCiv)
    this.value = ;
      iif(this.Parent.oContained.w_OnoCiv=='I',1,;
      0)
  endfunc

  func oOnoCiv_2_15.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDirCiv_2_16 as StdCheck with uid="KWCAIAQPMY",rtseq=82,rtrep=.f.,left=241, top=173, caption="Diritto civile",;
    ToolTipText = "Se attivo, riporta i diritti civili",;
    HelpContextID = 168794570,;
    cFormVar="w_DirCiv", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDirCiv_2_16.RadioValue()
    return(iif(this.value =1,'C',;
    space(1)))
  endfunc
  func oDirCiv_2_16.GetRadio()
    this.Parent.oContained.w_DirCiv = this.RadioValue()
    return .t.
  endfunc

  func oDirCiv_2_16.SetRadio()
    this.Parent.oContained.w_DirCiv=trim(this.Parent.oContained.w_DirCiv)
    this.value = ;
      iif(this.Parent.oContained.w_DirCiv=='C',1,;
      0)
  endfunc

  func oDirCiv_2_16.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oPreTempo_2_17 as StdCheck with uid="ZQYMXNWOEB",rtseq=83,rtrep=.f.,left=241, top=192, caption="Prestazione a tempo",;
    ToolTipText = "Se attivo, riporta le tariffe di prestazione a tempo",;
    HelpContextID = 54484891,;
    cFormVar="w_PreTempo", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPreTempo_2_17.RadioValue()
    return(iif(this.value =1,'P',;
    space(1)))
  endfunc
  func oPreTempo_2_17.GetRadio()
    this.Parent.oContained.w_PreTempo = this.RadioValue()
    return .t.
  endfunc

  func oPreTempo_2_17.SetRadio()
    this.Parent.oContained.w_PreTempo=trim(this.Parent.oContained.w_PreTempo)
    this.value = ;
      iif(this.Parent.oContained.w_PreTempo=='P',1,;
      0)
  endfunc

  func oPreTempo_2_17.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oOnoPen_2_18 as StdCheck with uid="FATEDFEVGH",rtseq=84,rtrep=.f.,left=390, top=154, caption="Onorario penale",;
    ToolTipText = "Se attivo, riporta gli onorari penali",;
    HelpContextID = 37930010,;
    cFormVar="w_OnoPen", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oOnoPen_2_18.RadioValue()
    return(iif(this.value =1,'E',;
    space(1)))
  endfunc
  func oOnoPen_2_18.GetRadio()
    this.Parent.oContained.w_OnoPen = this.RadioValue()
    return .t.
  endfunc

  func oOnoPen_2_18.SetRadio()
    this.Parent.oContained.w_OnoPen=trim(this.Parent.oContained.w_OnoPen)
    this.value = ;
      iif(this.Parent.oContained.w_OnoPen=='E',1,;
      0)
  endfunc

  func oOnoPen_2_18.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDirStrag_2_19 as StdCheck with uid="YPYEWIJVVO",rtseq=85,rtrep=.f.,left=390, top=173, caption="Diritto stragiudiziale",;
    ToolTipText = "Se attivo, riporta i diritti stragiudiziali",;
    HelpContextID = 45115037,;
    cFormVar="w_DirStrag", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDirStrag_2_19.RadioValue()
    return(iif(this.value =1,'R',;
    space(1)))
  endfunc
  func oDirStrag_2_19.GetRadio()
    this.Parent.oContained.w_DirStrag = this.RadioValue()
    return .t.
  endfunc

  func oDirStrag_2_19.SetRadio()
    this.Parent.oContained.w_DirStrag=trim(this.Parent.oContained.w_DirStrag)
    this.value = ;
      iif(this.Parent.oContained.w_DirStrag=='R',1,;
      0)
  endfunc

  func oDirStrag_2_19.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oSpese_2_20 as StdCheck with uid="GJLHVDCIUE",rtseq=86,rtrep=.f.,left=390, top=192, caption="Spesa",;
    ToolTipText = "Se attivo, riporta le spese",;
    HelpContextID = 266313254,;
    cFormVar="w_Spese", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSpese_2_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSpese_2_20.GetRadio()
    this.Parent.oContained.w_Spese = this.RadioValue()
    return .t.
  endfunc

  func oSpese_2_20.SetRadio()
    this.Parent.oContained.w_Spese=trim(this.Parent.oContained.w_Spese)
    this.value = ;
      iif(this.Parent.oContained.w_Spese=='S',1,;
      0)
  endfunc

  func oSpese_2_20.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oOnoStrag_2_21 as StdCheck with uid="JNXMKTIAUU",rtseq=87,rtrep=.f.,left=532, top=154, caption="Onorario stragiudiziale",;
    ToolTipText = "Se attivo, riporta gli onorari stragiudiziali",;
    HelpContextID = 45104205,;
    cFormVar="w_OnoStrag", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oOnoStrag_2_21.RadioValue()
    return(iif(this.value =1,'T',;
    space(1)))
  endfunc
  func oOnoStrag_2_21.GetRadio()
    this.Parent.oContained.w_OnoStrag = this.RadioValue()
    return .t.
  endfunc

  func oOnoStrag_2_21.SetRadio()
    this.Parent.oContained.w_OnoStrag=trim(this.Parent.oContained.w_OnoStrag)
    this.value = ;
      iif(this.Parent.oContained.w_OnoStrag=='T',1,;
      0)
  endfunc

  func oOnoStrag_2_21.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oPreGen_2_22 as StdCheck with uid="TCWVESISVS",rtseq=88,rtrep=.f.,left=532, top=173, caption="Prestazione generica",;
    ToolTipText = "Se attivo, riporta le tariffe di prestazione generica",;
    HelpContextID = 38559754,;
    cFormVar="w_PreGen", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPreGen_2_22.RadioValue()
    return(iif(this.value =1,'G',;
    space(1)))
  endfunc
  func oPreGen_2_22.GetRadio()
    this.Parent.oContained.w_PreGen = this.RadioValue()
    return .t.
  endfunc

  func oPreGen_2_22.SetRadio()
    this.Parent.oContained.w_PreGen=trim(this.Parent.oContained.w_PreGen)
    this.value = ;
      iif(this.Parent.oContained.w_PreGen=='G',1,;
      0)
  endfunc

  func oPreGen_2_22.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oAnticip_2_23 as StdCheck with uid="IBXMGDLBBY",rtseq=89,rtrep=.f.,left=532, top=192, caption="Anticipazione",;
    ToolTipText = "Se attivo, riporta le anticipazioni",;
    HelpContextID = 122254586,;
    cFormVar="w_Anticip", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAnticip_2_23.RadioValue()
    return(iif(this.value =1,'A',;
    space(1)))
  endfunc
  func oAnticip_2_23.GetRadio()
    this.Parent.oContained.w_Anticip = this.RadioValue()
    return .t.
  endfunc

  func oAnticip_2_23.SetRadio()
    this.Parent.oContained.w_Anticip=trim(this.Parent.oContained.w_Anticip)
    this.value = ;
      iif(this.Parent.oContained.w_Anticip=='A',1,;
      0)
  endfunc

  func oAnticip_2_23.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oEscFac_2_25 as StdCheck with uid="JVSDIXXWDU",rtseq=90,rtrep=.f.,left=681, top=154, caption="Escl. fattura d'acconto",;
    ToolTipText = "Se attivo esclude le prestazioni gi� inserite  nelle fatture di acconto dell'elenco delle prestazioni da fatturare",;
    HelpContextID = 227377082,;
    cFormVar="w_EscFac", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oEscFac_2_25.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oEscFac_2_25.GetRadio()
    this.Parent.oContained.w_EscFac = this.RadioValue()
    return .t.
  endfunc

  func oEscFac_2_25.SetRadio()
    this.Parent.oContained.w_EscFac=trim(this.Parent.oContained.w_EscFac)
    this.value = ;
      iif(this.Parent.oContained.w_EscFac=='S',1,;
      0)
  endfunc

  func oEscFac_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ( .w_InDocum $ 'P-D-N')
    endwith
   endif
  endfunc

  func oEscFac_2_25.mHide()
    with this.Parent.oContained
      return (!IsAlt() )
    endwith
  endfunc

  add object oEvasio_2_26 as StdRadio with uid="PKYSFRDKEP",rtseq=91,rtrep=.f.,left=16, top=241, width=403,height=23;
    , ToolTipText = "Controllo tracciabilit� della prestazione";
    , cFormVar="w_Evasio", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oEvasio_2_26.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Da importare nei documenti"
      this.Buttons(1).HelpContextID = 14720186
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Da importare nei documenti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Inserite in un documento"
      this.Buttons(2).HelpContextID = 14720186
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Inserite in un documento","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 14720186
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Controllo tracciabilit� della prestazione")
      StdRadio::init()
    endproc

  func oEvasio_2_26.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oEvasio_2_26.GetRadio()
    this.Parent.oContained.w_Evasio = this.RadioValue()
    return .t.
  endfunc

  func oEvasio_2_26.SetRadio()
    this.Parent.oContained.w_Evasio=trim(this.Parent.oContained.w_Evasio)
    this.value = ;
      iif(this.Parent.oContained.w_Evasio=='N',1,;
      iif(this.Parent.oContained.w_Evasio=='S',2,;
      iif(this.Parent.oContained.w_Evasio=='T',3,;
      0)))
  endfunc

  func oEvasio_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STATO='P')
    endwith
   endif
  endfunc

  func oEvasio_2_26.mHide()
    with this.Parent.oContained
      return (!.w_STATO='P' OR !IsAlt())
    endwith
  endfunc

  add object oVisTotali_2_27 as StdCheck with uid="CVTFHAFBRE",rtseq=92,rtrep=.f.,left=532, top=241, caption="Visualizza totali importi",;
    ToolTipText = "Se attivo, visualizza i totali degli importi di compensi, spese ed anticipazioni",;
    HelpContextID = 73498194,;
    cFormVar="w_VisTotali", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVisTotali_2_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVisTotali_2_27.GetRadio()
    this.Parent.oContained.w_VisTotali = this.RadioValue()
    return .t.
  endfunc

  func oVisTotali_2_27.SetRadio()
    this.Parent.oContained.w_VisTotali=trim(this.Parent.oContained.w_VisTotali)
    this.value = ;
      iif(this.Parent.oContained.w_VisTotali=='S',1,;
      0)
  endfunc

  func oVisTotali_2_27.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDatiPratica_2_28 as StdCheck with uid="YIXIXUGXSW",rtseq=93,rtrep=.f.,left=12, top=546, caption="Dati pratica",;
    ToolTipText = "Se attivo, stampa i dati della pratica",;
    HelpContextID = 9238586,;
    cFormVar="w_DatiPratica", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDatiPratica_2_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDatiPratica_2_28.GetRadio()
    this.Parent.oContained.w_DatiPratica = this.RadioValue()
    return .t.
  endfunc

  func oDatiPratica_2_28.SetRadio()
    this.Parent.oContained.w_DatiPratica=trim(this.Parent.oContained.w_DatiPratica)
    this.value = ;
      iif(this.Parent.oContained.w_DatiPratica=='S',1,;
      0)
  endfunc

  func oDatiPratica_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SoloTot='N')
    endwith
   endif
  endfunc

  func oDatiPratica_2_28.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oAnnotazioni_2_29 as StdCheck with uid="MPGQZRPFVJ",rtseq=94,rtrep=.f.,left=109, top=546, caption="Descrizione aggiuntiva",;
    ToolTipText = "Se attivo, stampa la descrizione aggiuntiva",;
    HelpContextID = 237817761,;
    cFormVar="w_Annotazioni", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAnnotazioni_2_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAnnotazioni_2_29.GetRadio()
    this.Parent.oContained.w_Annotazioni = this.RadioValue()
    return .t.
  endfunc

  func oAnnotazioni_2_29.SetRadio()
    this.Parent.oContained.w_Annotazioni=trim(this.Parent.oContained.w_Annotazioni)
    this.value = ;
      iif(this.Parent.oContained.w_Annotazioni=='S',1,;
      0)
  endfunc

  func oAnnotazioni_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SoloTot='N')
    endwith
   endif
  endfunc

  func oAnnotazioni_2_29.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oSoloTot_2_30 as StdCheck with uid="JKQYMMUCPW",rtseq=95,rtrep=.f.,left=269, top=546, caption="Solo totali",;
    ToolTipText = "Se attivo, stampa solo i totalizzatori finali",;
    HelpContextID = 231476518,;
    cFormVar="w_SoloTot", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSoloTot_2_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSoloTot_2_30.GetRadio()
    this.Parent.oContained.w_SoloTot = this.RadioValue()
    return .t.
  endfunc

  func oSoloTot_2_30.SetRadio()
    this.Parent.oContained.w_SoloTot=trim(this.Parent.oContained.w_SoloTot)
    this.value = ;
      iif(this.Parent.oContained.w_SoloTot=='S',1,;
      0)
  endfunc

  func oSoloTot_2_30.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object STAMPA_ALTE as cp_outputCombo with uid="KYMBPRIRTN",left=410, top=553, width=253,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=2;
    , HelpContextID = 206446618


  add object oBtn_2_33 as StdButton with uid="KHIRDVYTYK",left=665, top=530, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 242654682;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_33.Click()
      with this.Parent.oContained
        GSAG_BSP(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc

  func oBtn_2_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT())
     endwith
    endif
  endfunc

  add object oDENOM_TIT_2_34 as StdField with uid="NDNANXNDZM",rtseq=96,rtrep=.f.,;
    cFormVar = "w_DENOM_TIT", cQueryName = "DENOM_TIT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 221906879,;
   bGlobalFont=.t.,;
    Height=21, Width=231, Left=154, Top=28, InputMask=replicate('X',100)

  func oDENOM_TIT_2_34.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc


  add object oBtn_2_35 as StdButton with uid="CHMZTCBNGH",left=715, top=530, width=48,height=45,;
    CpPicture="BMP\COSTI.ICO", caption="", nPag=2;
    , ToolTipText = "Premere per effettuare la stampa Analisi compensi e costi";
    , HelpContextID = 266240986;
    , Caption='C\<osti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_35.Click()
      do GSAG_SAC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT())
     endwith
    endif
  endfunc

  add object oDENOM_RES_2_37 as StdField with uid="NTERGERVTI",rtseq=97,rtrep=.f.,;
    cFormVar = "w_DENOM_RES", cQueryName = "DENOM_RES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 46528597,;
   bGlobalFont=.t.,;
    Height=21, Width=231, Left=529, Top=28, InputMask=replicate('X',100)

  func oDENOM_RES_2_37.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  add object oCCDESPIA_2_40 as StdField with uid="SPFLETMJOH",rtseq=98,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 244277351,;
   bGlobalFont=.t.,;
    Height=21, Width=594, Left=224, Top=60, InputMask=replicate('X',40)

  func oCCDESPIA_2_40.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oDESPRA_2_42 as StdField with uid="KWGUQTHYGW",rtseq=99,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 7646666,;
   bGlobalFont=.t.,;
    Height=21, Width=533, Left=224, Top=28, InputMask=replicate('X',100)

  func oDESPRA_2_42.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oDESNOM_2_45 as StdField with uid="FSPJNEPZBK",rtseq=100,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78032330,;
   bGlobalFont=.t.,;
    Height=21, Width=594, Left=224, Top=60, InputMask=replicate('X',40)

  func oDESNOM_2_45.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODPRA) OR NOT ISALT())
    endwith
  endfunc


  add object oBtn_2_48 as StdButton with uid="XJHKNYRBJG",left=765, top=530, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 159744070;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_48.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_49 as StdButton with uid="JWLIEWJPTE",left=765, top=5, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Ricerca";
    , HelpContextID = 207522026;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_49.Click()
      with this.Parent.oContained
        gsag_bsp(this.Parent.oContained,"RICERCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCCDESPIA_2_50 as StdField with uid="KDRZJDGARK",rtseq=104,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 244277351,;
   bGlobalFont=.t.,;
    Height=21, Width=594, Left=224, Top=123, InputMask=replicate('X',40)

  func oCCDESPIA_2_50.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR NOT ISALT())
    endwith
  endfunc

  add object oStr_2_31 as StdString with uid="HBZOPQHXTN",Visible=.t., Left=410, Top=530,;
    Alignment=0, Width=90, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_2_31.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_2_36 as StdString with uid="CUXHESDYFY",Visible=.t., Left=-2, Top=28,;
    Alignment=1, Width=84, Height=18,;
    Caption="Titol. pratica:"  ;
  , bGlobalFont=.t.

  func oStr_2_36.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  add object oStr_2_38 as StdString with uid="FZZKNNAQGZ",Visible=.t., Left=377, Top=28,;
    Alignment=1, Width=84, Height=18,;
    Caption="Resp. pratica:"  ;
  , bGlobalFont=.t.

  func oStr_2_38.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR NOT EMPTY(.w_CODPRA))
    endwith
  endfunc

  add object oStr_2_39 as StdString with uid="KSBGPQZUEI",Visible=.t., Left=32, Top=92,;
    Alignment=1, Width=50, Height=18,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  func oStr_2_39.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_2_41 as StdString with uid="WDYYNZAUHT",Visible=.t., Left=13, Top=61,;
    Alignment=1, Width=69, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  func oStr_2_41.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_2_43 as StdString with uid="HQLATKRDLO",Visible=.t., Left=13, Top=28,;
    Alignment=1, Width=69, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_43.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_2_44 as StdString with uid="RCYDFBJYOO",Visible=.t., Left=13, Top=61,;
    Alignment=1, Width=69, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_2_44.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODPRA) OR NOT ISALT())
    endwith
  endfunc

  add object oStr_2_46 as StdString with uid="ARJZGYKPMM",Visible=.t., Left=-19, Top=92,;
    Alignment=1, Width=101, Height=18,;
    Caption="Pratiche:"  ;
  , bGlobalFont=.t.

  func oStr_2_46.mHide()
    with this.Parent.oContained
      return (! Isalt() or !.w_EDITCODPRA)
    endwith
  endfunc

  add object oStr_2_47 as StdString with uid="WMBOBSIFGU",Visible=.t., Left=253, Top=92,;
    Alignment=1, Width=100, Height=18,;
    Caption="Pratiche sospese:"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (!.w_EDITCODPRA or !IsAlt())
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="EBUADEBZKR",Visible=.t., Left=13, Top=125,;
    Alignment=1, Width=69, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S' OR !IsAlt())
    endwith
  endfunc

  add object oStr_2_53 as StdString with uid="JTHCTETUHG",Visible=.t., Left=520, Top=94,;
    Alignment=1, Width=73, Height=18,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  func oStr_2_53.mHide()
    with this.Parent.oContained
      return (!IsAlt() )
    endwith
  endfunc

  add object oBox_2_24 as StdBox with uid="GUAITALLMO",left=233, top=152, width=443,height=66
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_krp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
