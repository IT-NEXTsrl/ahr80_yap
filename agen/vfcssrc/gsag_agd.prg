* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_agd                                                        *
*              Piano di generazione documenti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-22                                                      *
* Last revis.: 2016-10-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_agd"))

* --- Class definition
define class tgsag_agd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 563
  Height = 320+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-10-14"
  HelpContextID=42608489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Constant Properties
  GES_DOCU_IDX = 0
  CONTI_IDX = 0
  CAUMATTI_IDX = 0
  TIP_DOCU_IDX = 0
  IMP_MAST_IDX = 0
  DES_DIVE_IDX = 0
  CAT_ELEM_IDX = 0
  DIPENDEN_IDX = 0
  PAR_AGEN_IDX = 0
  cFile = "GES_DOCU"
  cKeySelect = "DTSERIAL"
  cKeyWhere  = "DTSERIAL=this.w_DTSERIAL"
  cKeyWhereODBC = '"DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';

  cKeyWhereODBCqualified = '"GES_DOCU.DTSERIAL="+cp_ToStrODBC(this.w_DTSERIAL)';

  cPrg = "gsag_agd"
  cComment = "Piano di generazione documenti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DTSERIAL = space(10)
  w_READAZI = space(10)
  w_OB_TEST = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_FLAC = space(1)
  w_CODDOC = space(3)
  o_CODDOC = space(3)
  w_DTDATINI = ctod('  /  /  ')
  o_DTDATINI = ctod('  /  /  ')
  w_DTDATFIN = ctod('  /  /  ')
  w_DTFLPROV = space(1)
  w_DTDESCRI = space(60)
  w_DTCODCLI = space(15)
  w_DTCODSED = space(5)
  w_DTCODIMP = space(10)
  w_DTCAUDOC = space(5)
  w_DTTIPATT = space(20)
  w_DTGRUPAR = space(5)
  w_DTCATCON = space(5)
  w_DESELEM = space(60)
  w_DESCLI = space(60)
  w_DESIMP = space(50)
  w_NOMDES = space(50)
  w_DESDOC = space(35)
  w_DESATT = space(254)
  w_TIPCON = space(1)
  w_CODICE = space(15)
  w_SEDE = space(5)
  w_PATIPRIS = space(1)
  w_CARAGGST = space(10)
  w_DTFLRAGG = space(1)
  w_HASEVCOP = space(15)
  w_HASEVENT = .F.
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_TIPRIS = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_UTCC = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_DTSERIAL = this.W_DTSERIAL
  * --- Area Manuale = Declare Variables
  * --- gsag_agd
    proc ecpDelete()
      local bRes,nRec,bErr
      if type('this.oPgFrm.Pages(this.oPgFrm.activepage).oWarning')<>'U'
        this.oPgFrm.Pages(this.oPgFrm.ActivePage).oWarning.ecpDelete()
        this.ReleaseWarn()
        this.LoadWarn()
        return
      endif
      if !this.bSec4
        cp_errormsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
        return
      endif
      if !this.CanDelete()
        return
      endif
      IF !this.deletewarn()
        RETURN
      ENDIF
      IF this.bDeleting
        RETURN
      ENDIF
      this.bDeleting=.t.
      if this.bLoaded and this.cFunction='Query'
        * --- se il dato e' vecchio deve essere riletto per avere i timestamp aggiornati
        if this.nLoadTime=0
          this.LoadRecWarn()  && rilettura se si cancella direttamente dopo un editing senza eseguire una query
        endif
        * --- Richiesta conferma Cancellazione Record
        bRes=.t.
        if bRes and !this.CheckChildrenForDelete()
          bRes=cp_YesNo(MSG_DATA_EXIST_IN_CHILD_ENTITY_C_PROCEED_QP)
        endif
        if bRes
  				*--- Activity logger traccio la modifica di un record
  				If i_nACTIVATEPROFILER>0
  					cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UF5", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
  				Endif
  				*--- Activity logger traccio la modifica di un record
          thisform.lockscreen=.t.
          select (this.cKeySet)
          nRec=recno()
          cp_BeginTrs()
  				**** Zucchetti Aulla inizio - controllo flussi
  				This.cFlowStamp = Sys(2015)
  				This.bFlowNoRules = .F.
  				**** Zucchetti Aulla fine - controllo flussi
          this.lockscreen=.t.
       * Zucchetti aulla inizio - Gestione attributi
  	  if TYPE("this.bGestAtt")='O' And vartype(I_CURFORM.w_GESTGUID)='C'
  	  * Elimino l'attributo associato (di fatto la funzione � una DELETE (utilizziamo un comando Painter per gestire bTrsErr in automatico e la connessione...)
  	  ELIMINATT( I_CURFORM.w_GESTGUID)
  	  endif
  	 * Zucchetti aulla fine - Gestione attributi
          this.mDelete()
          bErr=bTrsErr
          this.lockscreen=.f.
          cp_EndTrs()
          IF not(bErr)
            this.NotifyEvent("Record Deleted")
          ENDIF
          this.QueryKeySet(this.cLastWhere,this.cLastOrderBy)
          if reccount()>=nRec
            goto nRec
          endif
          this.LoadRecWarn()
  				**** Zucchetti Aulla inizio - Sincronizzazione
  				*** Delete
  				If g_APPLICATION = "ad hoc ENTERPRISE" And Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_SINC)="C" And g_SINC='S'
  					Local i_cParam
  					i_cParam='DM'
  					If !Empty(i_cParam) And Type("g_SINC") = "C"
  						If g_SINC = "S"
  							l_sPrg="GSSI_BSD"
  							Do (l_sPrg) With This,i_cParam
  						Endif
  					Endif
  				Endif
  
  				**** Zucchetti Aulla fine - Sincronizzazione
          thisform.lockscreen=.f.
          this.Refresh()
        endif
      ENDIF
      this.bDeleting=.f.
    return
  
    Func ah_HasCPEvents(i_cOp)	
  	If (this.cFunction='Query' And (Upper(i_cop)='ECPDELETE'))
        if (Upper(i_cop)='ECPEDIT' And (seconds()>this.nLoadTime+60 or this.bupdated)) Or(Upper(i_cop)='ECPDELETE' And this.nLoadTime=0)
          this.LoadRecWarn()  && rilettura se sono passati piu' di 60 secondi dall' ultima lettura o il record � stato variato in Interroga
        ENDIF
  		This.w_HASEVCOP=i_cop	
  		this.NotifyEvent('HasEvent')
  		return(this.w_HASEVENT)
  	Else
  		return(.t.)	
  	endif
    EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GES_DOCU','gsag_agd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_agdPag1","gsag_agd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piano")
      .Pages(1).HelpContextID = 187017482
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAUMATTI'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='IMP_MAST'
    this.cWorkTables[5]='DES_DIVE'
    this.cWorkTables[6]='CAT_ELEM'
    this.cWorkTables[7]='DIPENDEN'
    this.cWorkTables[8]='PAR_AGEN'
    this.cWorkTables[9]='GES_DOCU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GES_DOCU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GES_DOCU_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DTSERIAL = NVL(DTSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_24_joined
    link_1_24_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GES_DOCU where DTSERIAL=KeySet.DTSERIAL
    *
    i_nConn = i_TableProp[this.GES_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GES_DOCU_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GES_DOCU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GES_DOCU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GES_DOCU '
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_24_joined=this.AddJoinedLink_1_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DTSERIAL',this.w_DTSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OB_TEST = ctod("  /  /  ")
        .w_OBTEST = ctod("  /  /  ")
        .w_FLAC = space(1)
        .w_CODDOC = space(3)
        .w_DESELEM = space(60)
        .w_DESCLI = space(60)
        .w_DESIMP = space(50)
        .w_NOMDES = space(50)
        .w_DESDOC = space(35)
        .w_DESATT = space(254)
        .w_TIPCON = 'C'
        .w_PATIPRIS = 'G'
        .w_CARAGGST = space(10)
        .w_HASEVCOP = space(15)
        .w_HASEVENT = .f.
        .w_CATDOC = space(2)
        .w_FLVEAC = space(1)
        .w_TIPRIS = space(1)
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_DTSERIAL = NVL(DTSERIAL,space(10))
        .op_DTSERIAL = .w_DTSERIAL
        .w_READAZI = I_CODAZI
          .link_1_2('Load')
        .w_DTDATINI = NVL(cp_ToDate(DTDATINI),ctod("  /  /  "))
        .w_DTDATFIN = NVL(cp_ToDate(DTDATFIN),ctod("  /  /  "))
        .w_DTFLPROV = NVL(DTFLPROV,space(1))
        .w_DTDESCRI = NVL(DTDESCRI,space(60))
        .w_DTCODCLI = NVL(DTCODCLI,space(15))
          .link_1_15('Load')
        .w_DTCODSED = NVL(DTCODSED,space(5))
          .link_1_17('Load')
        .w_DTCODIMP = NVL(DTCODIMP,space(10))
          if link_1_19_joined
            this.w_DTCODIMP = NVL(IMCODICE119,NVL(this.w_DTCODIMP,space(10)))
            this.w_DESIMP = NVL(IMDESCRI119,space(50))
          else
          .link_1_19('Load')
          endif
        .w_DTCAUDOC = NVL(DTCAUDOC,space(5))
          if link_1_21_joined
            this.w_DTCAUDOC = NVL(TDTIPDOC121,NVL(this.w_DTCAUDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC121,space(35))
            this.w_CATDOC = NVL(TDCATDOC121,space(2))
            this.w_FLVEAC = NVL(TDFLVEAC121,space(1))
          else
          .link_1_21('Load')
          endif
        .w_DTTIPATT = NVL(DTTIPATT,space(20))
          if link_1_22_joined
            this.w_DTTIPATT = NVL(CACODICE122,NVL(this.w_DTTIPATT,space(20)))
            this.w_DESATT = NVL(CADESCRI122,space(254))
          else
          .link_1_22('Load')
          endif
        .w_DTGRUPAR = NVL(DTGRUPAR,space(5))
          if link_1_24_joined
            this.w_DTGRUPAR = NVL(DPCODICE124,NVL(this.w_DTGRUPAR,space(5)))
            this.w_TIPRIS = NVL(DPTIPRIS124,space(1))
          else
          .link_1_24('Load')
          endif
        .w_DTCATCON = NVL(DTCATCON,space(5))
          if link_1_25_joined
            this.w_DTCATCON = NVL(CECODELE125,NVL(this.w_DTCATCON,space(5)))
            this.w_DESELEM = NVL(CEDESELE125,space(60))
          else
          .link_1_25('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_CODICE = .w_DTCODCLI
        .w_SEDE = .w_DTCODSED
        .w_DTFLRAGG = NVL(DTFLRAGG,space(1))
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCC = NVL(UTCC,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GES_DOCU')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DTSERIAL = space(10)
      .w_READAZI = space(10)
      .w_OB_TEST = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_FLAC = space(1)
      .w_CODDOC = space(3)
      .w_DTDATINI = ctod("  /  /  ")
      .w_DTDATFIN = ctod("  /  /  ")
      .w_DTFLPROV = space(1)
      .w_DTDESCRI = space(60)
      .w_DTCODCLI = space(15)
      .w_DTCODSED = space(5)
      .w_DTCODIMP = space(10)
      .w_DTCAUDOC = space(5)
      .w_DTTIPATT = space(20)
      .w_DTGRUPAR = space(5)
      .w_DTCATCON = space(5)
      .w_DESELEM = space(60)
      .w_DESCLI = space(60)
      .w_DESIMP = space(50)
      .w_NOMDES = space(50)
      .w_DESDOC = space(35)
      .w_DESATT = space(254)
      .w_TIPCON = space(1)
      .w_CODICE = space(15)
      .w_SEDE = space(5)
      .w_PATIPRIS = space(1)
      .w_CARAGGST = space(10)
      .w_DTFLRAGG = space(1)
      .w_HASEVCOP = space(15)
      .w_HASEVENT = .f.
      .w_CATDOC = space(2)
      .w_FLVEAC = space(1)
      .w_TIPRIS = space(1)
      .w_DTBSO_CAUMATTI = ctod("  /  /  ")
      .w_UTDC = ctot("")
      .w_UTCV = 0
      .w_UTDV = ctot("")
      .w_UTCC = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_READAZI = I_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_READAZI))
          .link_1_2('Full')
          endif
          .DoRTCalc(3,6,.f.)
        .w_DTDATINI = IIF(.w_FLAC='E', CTOD('01-01-'+ALLTRIM(STR(YEAR(I_DATSYS)))), IIF(.w_FLAC='F', CTOD('01-' +ALLTRIM(STR(MONTH(I_DATSYS)))+'-' +ALLTRIM(STR(YEAR(I_DATSYS)))),IIF(.w_FLAC='I', CTOD( DTOC(I_datsys-DOW(i_datsys,2)+1)),i_DATSYS)))
        .w_DTDATFIN = NEXTTIME(.w_CODDOC, .w_DTDATINI)
        .w_DTFLPROV = 'N'
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_DTCODCLI))
          .link_1_15('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_DTCODSED))
          .link_1_17('Full')
          endif
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_DTCODIMP))
          .link_1_19('Full')
          endif
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_DTCAUDOC))
          .link_1_21('Full')
          endif
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_DTTIPATT))
          .link_1_22('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_DTGRUPAR))
          .link_1_24('Full')
          endif
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_DTCATCON))
          .link_1_25('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
          .DoRTCalc(18,23,.f.)
        .w_TIPCON = 'C'
        .w_CODICE = .w_DTCODCLI
        .w_SEDE = .w_DTCODSED
        .w_PATIPRIS = 'G'
          .DoRTCalc(28,28,.f.)
        .w_DTFLRAGG = 'R'
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
          .DoRTCalc(30,34,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
      endif
    endwith
    cp_BlankRecExtFlds(this,'GES_DOCU')
    this.DoRTCalc(36,39,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GES_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GES_DOCU_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERDC","i_CODAZI,w_DTSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_DTSERIAL = .w_DTSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDTDATINI_1_7.enabled = i_bVal
      .Page1.oPag.oDTDATFIN_1_8.enabled = i_bVal
      .Page1.oPag.oDTFLPROV_1_9.enabled = i_bVal
      .Page1.oPag.oDTDESCRI_1_10.enabled = i_bVal
      .Page1.oPag.oDTCODCLI_1_15.enabled = i_bVal
      .Page1.oPag.oDTCODSED_1_17.enabled = i_bVal
      .Page1.oPag.oDTCODIMP_1_19.enabled = i_bVal
      .Page1.oPag.oDTCAUDOC_1_21.enabled = i_bVal
      .Page1.oPag.oDTTIPATT_1_22.enabled = i_bVal
      .Page1.oPag.oDTGRUPAR_1_24.enabled = i_bVal
      .Page1.oPag.oDTCATCON_1_25.enabled = i_bVal
      .Page1.oPag.oDTFLRAGG_1_45.enabled = i_bVal
      .Page1.oPag.oBtn_1_26.enabled = .Page1.oPag.oBtn_1_26.mCond()
      .Page1.oPag.oBtn_1_27.enabled = i_bVal
      .Page1.oPag.oBtn_1_49.enabled = .Page1.oPag.oBtn_1_49.mCond()
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oObj_1_46.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'GES_DOCU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GES_DOCU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTSERIAL,"DTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTDATINI,"DTDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTDATFIN,"DTDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTFLPROV,"DTFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTDESCRI,"DTDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTCODCLI,"DTCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTCODSED,"DTCODSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTCODIMP,"DTCODIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTCAUDOC,"DTCAUDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTTIPATT,"DTTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTGRUPAR,"DTGRUPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTCATCON,"DTCATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DTFLRAGG,"DTFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GES_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GES_DOCU_IDX,2])
    i_lTable = "GES_DOCU"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GES_DOCU_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GES_DOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GES_DOCU_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GES_DOCU_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SERDC","i_CODAZI,w_DTSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GES_DOCU
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GES_DOCU')
        i_extval=cp_InsertValODBCExtFlds(this,'GES_DOCU')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DTSERIAL,DTDATINI,DTDATFIN,DTFLPROV,DTDESCRI"+;
                  ",DTCODCLI,DTCODSED,DTCODIMP,DTCAUDOC,DTTIPATT"+;
                  ",DTGRUPAR,DTCATCON,DTFLRAGG,UTDC,UTCV"+;
                  ",UTDV,UTCC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DTSERIAL)+;
                  ","+cp_ToStrODBC(this.w_DTDATINI)+;
                  ","+cp_ToStrODBC(this.w_DTDATFIN)+;
                  ","+cp_ToStrODBC(this.w_DTFLPROV)+;
                  ","+cp_ToStrODBC(this.w_DTDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_DTCODCLI)+;
                  ","+cp_ToStrODBCNull(this.w_DTCODSED)+;
                  ","+cp_ToStrODBCNull(this.w_DTCODIMP)+;
                  ","+cp_ToStrODBCNull(this.w_DTCAUDOC)+;
                  ","+cp_ToStrODBCNull(this.w_DTTIPATT)+;
                  ","+cp_ToStrODBCNull(this.w_DTGRUPAR)+;
                  ","+cp_ToStrODBCNull(this.w_DTCATCON)+;
                  ","+cp_ToStrODBC(this.w_DTFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GES_DOCU')
        i_extval=cp_InsertValVFPExtFlds(this,'GES_DOCU')
        cp_CheckDeletedKey(i_cTable,0,'DTSERIAL',this.w_DTSERIAL)
        INSERT INTO (i_cTable);
              (DTSERIAL,DTDATINI,DTDATFIN,DTFLPROV,DTDESCRI,DTCODCLI,DTCODSED,DTCODIMP,DTCAUDOC,DTTIPATT,DTGRUPAR,DTCATCON,DTFLRAGG,UTDC,UTCV,UTDV,UTCC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DTSERIAL;
                  ,this.w_DTDATINI;
                  ,this.w_DTDATFIN;
                  ,this.w_DTFLPROV;
                  ,this.w_DTDESCRI;
                  ,this.w_DTCODCLI;
                  ,this.w_DTCODSED;
                  ,this.w_DTCODIMP;
                  ,this.w_DTCAUDOC;
                  ,this.w_DTTIPATT;
                  ,this.w_DTGRUPAR;
                  ,this.w_DTCATCON;
                  ,this.w_DTFLRAGG;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
                  ,this.w_UTCC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GES_DOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GES_DOCU_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GES_DOCU_IDX,i_nConn)
      *
      * update GES_DOCU
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GES_DOCU')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DTDATINI="+cp_ToStrODBC(this.w_DTDATINI)+;
             ",DTDATFIN="+cp_ToStrODBC(this.w_DTDATFIN)+;
             ",DTFLPROV="+cp_ToStrODBC(this.w_DTFLPROV)+;
             ",DTDESCRI="+cp_ToStrODBC(this.w_DTDESCRI)+;
             ",DTCODCLI="+cp_ToStrODBCNull(this.w_DTCODCLI)+;
             ",DTCODSED="+cp_ToStrODBCNull(this.w_DTCODSED)+;
             ",DTCODIMP="+cp_ToStrODBCNull(this.w_DTCODIMP)+;
             ",DTCAUDOC="+cp_ToStrODBCNull(this.w_DTCAUDOC)+;
             ",DTTIPATT="+cp_ToStrODBCNull(this.w_DTTIPATT)+;
             ",DTGRUPAR="+cp_ToStrODBCNull(this.w_DTGRUPAR)+;
             ",DTCATCON="+cp_ToStrODBCNull(this.w_DTCATCON)+;
             ",DTFLRAGG="+cp_ToStrODBC(this.w_DTFLRAGG)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GES_DOCU')
        i_cWhere = cp_PKFox(i_cTable  ,'DTSERIAL',this.w_DTSERIAL  )
        UPDATE (i_cTable) SET;
              DTDATINI=this.w_DTDATINI;
             ,DTDATFIN=this.w_DTDATFIN;
             ,DTFLPROV=this.w_DTFLPROV;
             ,DTDESCRI=this.w_DTDESCRI;
             ,DTCODCLI=this.w_DTCODCLI;
             ,DTCODSED=this.w_DTCODSED;
             ,DTCODIMP=this.w_DTCODIMP;
             ,DTCAUDOC=this.w_DTCAUDOC;
             ,DTTIPATT=this.w_DTTIPATT;
             ,DTGRUPAR=this.w_DTGRUPAR;
             ,DTCATCON=this.w_DTCATCON;
             ,DTFLRAGG=this.w_DTFLRAGG;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,UTCC=this.w_UTCC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GES_DOCU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GES_DOCU_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GES_DOCU_IDX,i_nConn)
      *
      * delete GES_DOCU
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DTSERIAL',this.w_DTSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GES_DOCU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GES_DOCU_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_READAZI = I_CODAZI
          .link_1_2('Full')
        .DoRTCalc(3,7,.t.)
        if .o_CODDOC<>.w_CODDOC.or. .o_DTDATINI<>.w_DTDATINI
            .w_DTDATFIN = NEXTTIME(.w_CODDOC, .w_DTDATINI)
        endif
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(9,24,.t.)
            .w_CODICE = .w_DTCODCLI
            .w_SEDE = .w_DTCODSED
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SERDC","i_CODAZI,w_DTSERIAL")
          .op_DTSERIAL = .w_DTSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(27,39,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
    endwith
  return

  proc Calculate_KVPJLUUNUL()
    with this
          * --- Cancella date
          .w_DTDATFIN = cp_chartodate('  -  -  ')
    endwith
  endproc
  proc Calculate_HGUMKCTBOL()
    with this
          * --- Forza aggiornamento
          .bupdated = .T.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDTFLPROV_1_9.enabled = this.oPgFrm.Page1.oPag.oDTFLPROV_1_9.mCond()
    this.oPgFrm.Page1.oPag.oDTFLRAGG_1_45.enabled = this.oPgFrm.Page1.oPag.oDTFLRAGG_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_DTDATINI Changed")
          .Calculate_KVPJLUUNUL()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
        if lower(cEvent)==lower("New record")
          .Calculate_HGUMKCTBOL()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PCNOFLDC,PACODDOC";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PCNOFLDC,PACODDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(10))
      this.w_FLAC = NVL(_Link_.PCNOFLDC,space(1))
      this.w_CODDOC = NVL(_Link_.PACODDOC,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(10)
      endif
      this.w_FLAC = space(1)
      this.w_CODDOC = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DTCODCLI
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DTCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DTCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_DTCODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DTCODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DTCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DTCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DTCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDTCODCLI_1_15'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DTCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DTCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_DTCODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DTCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_DTCODCLI = space(15)
      endif
      this.w_DESCLI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DTCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DTCODSED
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DTCODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_DTCODSED)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_DTCODCLI);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPCON;
                     ,'DDCODICE',this.w_DTCODCLI;
                     ,'DDCODDES',trim(this.w_DTCODSED))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DTCODSED)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DTCODSED) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oDTCODSED_1_17'),i_cWhere,'',"Sedi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);
           .or. this.w_DTCODCLI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_DTCODCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DTCODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_DTCODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_DTCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPCON;
                       ,'DDCODICE',this.w_DTCODCLI;
                       ,'DDCODDES',this.w_DTCODSED)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DTCODSED = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DTCODSED = space(5)
      endif
      this.w_NOMDES = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DTCODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DTCODIMP
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DTCODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_DTCODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_DTCODIMP))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DTCODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DTCODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oDTCODIMP_1_19'),i_cWhere,'',"Impianti",'GSAG_KTR.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DTCODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_DTCODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_DTCODIMP)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DTCODIMP = NVL(_Link_.IMCODICE,space(10))
      this.w_DESIMP = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DTCODIMP = space(10)
      endif
      this.w_DESIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DTCODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IMP_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.IMCODICE as IMCODICE119"+ ",link_1_19.IMDESCRI as IMDESCRI119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on GES_DOCU.DTCODIMP=link_1_19.IMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and GES_DOCU.DTCODIMP=link_1_19.IMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DTCAUDOC
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DTCAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_DTCAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_DTCAUDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DTCAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_DTCAUDOC)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_DTCAUDOC)+"%");

            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DTCAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oDTCAUDOC_1_21'),i_cWhere,'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DTCAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_DTCAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_DTCAUDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DTCAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DTCAUDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOC<>'OR' AND .w_FLVEAC='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti")
        endif
        this.w_DTCAUDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DTCAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.TDTIPDOC as TDTIPDOC121"+ ",link_1_21.TDDESDOC as TDDESDOC121"+ ",link_1_21.TDCATDOC as TDCATDOC121"+ ",link_1_21.TDFLVEAC as TDFLVEAC121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on GES_DOCU.DTCAUDOC=link_1_21.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and GES_DOCU.DTCAUDOC=link_1_21.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DTTIPATT
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DTTIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DTTIPATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DTTIPATT))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DTTIPATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_DTTIPATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_DTTIPATT)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DTTIPATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oDTTIPATT_1_22'),i_cWhere,'GSAG_MCA',"Tipi attivita",'GSAG1AAT.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DTTIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DTTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DTTIPATT)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DTTIPATT = NVL(_Link_.CACODICE,space(20))
      this.w_DESATT = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_DTTIPATT = space(20)
      endif
      this.w_DESATT = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DTTIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.CACODICE as CACODICE122"+ ",link_1_22.CADESCRI as CADESCRI122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on GES_DOCU.DTTIPATT=link_1_22.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and GES_DOCU.DTTIPATT=link_1_22.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DTGRUPAR
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DTGRUPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_DTGRUPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_DTGRUPAR))
          select DPCODICE,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DTGRUPAR)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DTGRUPAR) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oDTGRUPAR_1_24'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DTGRUPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_DTGRUPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_DTGRUPAR)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DTGRUPAR = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DTGRUPAR = space(5)
      endif
      this.w_TIPRIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIS='G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DTGRUPAR = space(5)
        this.w_TIPRIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DTGRUPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_24.DPCODICE as DPCODICE124"+ ",link_1_24.DPTIPRIS as DPTIPRIS124"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_24 on GES_DOCU.DTGRUPAR=link_1_24.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_24"
          i_cKey=i_cKey+'+" and GES_DOCU.DTGRUPAR=link_1_24.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DTCATCON
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ELEM_IDX,3]
    i_lTable = "CAT_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2], .t., this.CAT_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DTCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACE',True,'CAT_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODELE like "+cp_ToStrODBC(trim(this.w_DTCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODELE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODELE',trim(this.w_DTCATCON))
          select CECODELE,CEDESELE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODELE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DTCATCON)==trim(_Link_.CECODELE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESELE like "+cp_ToStrODBC(trim(this.w_DTCATCON)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESELE like "+cp_ToStr(trim(this.w_DTCATCON)+"%");

            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DTCATCON) and !this.bDontReportError
            deferred_cp_zoom('CAT_ELEM','*','CECODELE',cp_AbsName(oSource.parent,'oDTCATCON_1_25'),i_cWhere,'GSAG_ACE',"Categorie elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                     +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',oSource.xKey(1))
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DTCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(this.w_DTCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',this.w_DTCATCON)
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DTCATCON = NVL(_Link_.CECODELE,space(5))
      this.w_DESELEM = NVL(_Link_.CEDESELE,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_DTCATCON = space(5)
      endif
      this.w_DESELEM = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.CECODELE,1)
      cp_ShowWarn(i_cKey,this.CAT_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DTCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_ELEM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.CECODELE as CECODELE125"+ ",link_1_25.CEDESELE as CEDESELE125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on GES_DOCU.DTCATCON=link_1_25.CECODELE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and GES_DOCU.DTCATCON=link_1_25.CECODELE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDTDATINI_1_7.value==this.w_DTDATINI)
      this.oPgFrm.Page1.oPag.oDTDATINI_1_7.value=this.w_DTDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDTDATFIN_1_8.value==this.w_DTDATFIN)
      this.oPgFrm.Page1.oPag.oDTDATFIN_1_8.value=this.w_DTDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDTFLPROV_1_9.RadioValue()==this.w_DTFLPROV)
      this.oPgFrm.Page1.oPag.oDTFLPROV_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDTDESCRI_1_10.value==this.w_DTDESCRI)
      this.oPgFrm.Page1.oPag.oDTDESCRI_1_10.value=this.w_DTDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDTCODCLI_1_15.value==this.w_DTCODCLI)
      this.oPgFrm.Page1.oPag.oDTCODCLI_1_15.value=this.w_DTCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDTCODSED_1_17.value==this.w_DTCODSED)
      this.oPgFrm.Page1.oPag.oDTCODSED_1_17.value=this.w_DTCODSED
    endif
    if not(this.oPgFrm.Page1.oPag.oDTCODIMP_1_19.value==this.w_DTCODIMP)
      this.oPgFrm.Page1.oPag.oDTCODIMP_1_19.value=this.w_DTCODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDTCAUDOC_1_21.value==this.w_DTCAUDOC)
      this.oPgFrm.Page1.oPag.oDTCAUDOC_1_21.value=this.w_DTCAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDTTIPATT_1_22.value==this.w_DTTIPATT)
      this.oPgFrm.Page1.oPag.oDTTIPATT_1_22.value=this.w_DTTIPATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDTGRUPAR_1_24.value==this.w_DTGRUPAR)
      this.oPgFrm.Page1.oPag.oDTGRUPAR_1_24.value=this.w_DTGRUPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDTCATCON_1_25.value==this.w_DTCATCON)
      this.oPgFrm.Page1.oPag.oDTCATCON_1_25.value=this.w_DTCATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELEM_1_28.value==this.w_DESELEM)
      this.oPgFrm.Page1.oPag.oDESELEM_1_28.value=this.w_DESELEM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_29.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_29.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_30.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_30.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMDES_1_31.value==this.w_NOMDES)
      this.oPgFrm.Page1.oPag.oNOMDES_1_31.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_32.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_32.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_37.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_37.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDTFLRAGG_1_45.RadioValue()==this.w_DTFLRAGG)
      this.oPgFrm.Page1.oPag.oDTFLRAGG_1_45.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'GES_DOCU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DTDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDTDATINI_1_7.SetFocus()
            i_bnoObbl = !empty(.w_DTDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DTDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDTDATFIN_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DTDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATDOC<>'OR' AND .w_FLVEAC='V')  and not(empty(.w_DTCAUDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDTCAUDOC_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti")
          case   not(.w_TIPRIS='G')  and not(empty(.w_DTGRUPAR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDTGRUPAR_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODDOC = this.w_CODDOC
    this.o_DTDATINI = this.w_DTDATINI
    return

enddefine

* --- Define pages as container
define class tgsag_agdPag1 as StdContainer
  Width  = 559
  height = 320
  stdWidth  = 559
  stdheight = 320
  resizeXpos=301
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDTDATINI_1_7 as StdField with uid="EBMAAXOREJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DTDATINI", cQueryName = "DTDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 67407489,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=85, Top=11

  add object oDTDATFIN_1_8 as StdField with uid="HECTUJRWHP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DTDATFIN", cQueryName = "DTDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione",;
    HelpContextID = 117739132,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=202, Top=11


  add object oDTFLPROV_1_9 as StdCombo with uid="DNGQSBFHAO",rtseq=9,rtrep=.f.,left=451,top=11,width=102,height=21;
    , HelpContextID = 188313204;
    , cFormVar="w_DTFLPROV",RowSource=""+"Provvisori,"+"Confermati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDTFLPROV_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oDTFLPROV_1_9.GetRadio()
    this.Parent.oContained.w_DTFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oDTFLPROV_1_9.SetRadio()
    this.Parent.oContained.w_DTFLPROV=trim(this.Parent.oContained.w_DTFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_DTFLPROV=='S',1,;
      iif(this.Parent.oContained.w_DTFLPROV=='N',2,;
      0))
  endfunc

  func oDTFLPROV_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oDTDESCRI_1_10 as StdField with uid="MYUEGDRAGD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DTDESCRI", cQueryName = "DTDESCRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 99578239,;
   bGlobalFont=.t.,;
    Height=21, Width=467, Left=85, Top=40, InputMask=replicate('X',60)

  add object oDTCODCLI_1_15 as StdField with uid="HLDLAVVZEV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DTCODCLI", cQueryName = "DTCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente",;
    HelpContextID = 183934593,;
   bGlobalFont=.t.,;
    Height=21, Width=136, Left=85, Top=69, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DTCODCLI"

  func oDTCODCLI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
      if .not. empty(.w_DTCODSED)
        bRes2=.link_1_17('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDTCODCLI_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDTCODCLI_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDTCODCLI_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oDTCODCLI_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_DTCODCLI
     i_obj.ecpSave()
  endproc

  add object oDTCODSED_1_17 as StdField with uid="NLSLRWVAWZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DTCODSED", cQueryName = "DTCODSED",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 84500858,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=85, Top=98, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="DDCODICE", oKey_2_2="this.w_DTCODCLI", oKey_3_1="DDCODDES", oKey_3_2="this.w_DTCODSED"

  func oDTCODSED_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oDTCODSED_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDTCODSED_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_DTCODCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_DTCODCLI)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oDTCODSED_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sedi",'',this.parent.oContained
  endproc

  add object oDTCODIMP_1_19 as StdField with uid="SWZFJPISLC",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DTCODIMP", cQueryName = "DTCODIMP",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice impianto",;
    HelpContextID = 83271290,;
   bGlobalFont=.t.,;
    Height=21, Width=98, Left=85, Top=127, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", oKey_1_1="IMCODICE", oKey_1_2="this.w_DTCODIMP"

  func oDTCODIMP_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oDTCODIMP_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDTCODIMP_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oDTCODIMP_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Impianti",'GSAG_KTR.IMP_MAST_VZM',this.parent.oContained
  endproc

  add object oDTCAUDOC_1_21 as StdField with uid="SBIGHASDKL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DTCAUDOC", cQueryName = "DTCAUDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente o di tipo ordine e/o del ciclo acquisti",;
    ToolTipText = "Causale documento",;
    HelpContextID = 150249095,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=85, Top=156, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_DTCAUDOC"

  func oDTCAUDOC_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oDTCAUDOC_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDTCAUDOC_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oDTCAUDOC_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oDTCAUDOC_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_DTCAUDOC
     i_obj.ecpSave()
  endproc

  add object oDTTIPATT_1_22 as StdField with uid="DYNCJWIZZJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DTTIPATT", cQueryName = "DTTIPATT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 63205770,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=85, Top=185, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_DTTIPATT"

  func oDTTIPATT_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oDTTIPATT_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDTTIPATT_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oDTTIPATT_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'GSAG1AAT.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oDTTIPATT_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DTTIPATT
     i_obj.ecpSave()
  endproc

  add object oDTGRUPAR_1_24 as StdField with uid="BJEGDOAPKL",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DTGRUPAR", cQueryName = "DTGRUPAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo partecipanti",;
    HelpContextID = 52208008,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=85, Top=214, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_DTGRUPAR"

  func oDTGRUPAR_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oDTGRUPAR_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDTGRUPAR_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oDTGRUPAR_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oDTGRUPAR_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_DTGRUPAR
     i_obj.ecpSave()
  endproc

  add object oDTCATCON_1_25 as StdField with uid="XILKXGNJQB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DTCATCON", cQueryName = "DTCATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contratto",;
    HelpContextID = 168074876,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=85, Top=243, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_ELEM", cZoomOnZoom="GSAG_ACE", oKey_1_1="CECODELE", oKey_1_2="this.w_DTCATCON"

  func oDTCATCON_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oDTCATCON_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDTCATCON_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ELEM','*','CECODELE',cp_AbsName(this.parent,'oDTCATCON_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACE',"Categorie elementi",'',this.parent.oContained
  endproc
  proc oDTCATCON_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODELE=this.parent.oContained.w_DTCATCON
     i_obj.ecpSave()
  endproc


  add object oBtn_1_26 as StdButton with uid="KZSQCYUNAO",left=505, top=273, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 35291066;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_27 as StdButton with uid="QJZVDJKIQR",left=454, top=273, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 42579738;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESELEM_1_28 as StdField with uid="LAMEQNOKPK",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESELEM", cQueryName = "DESELEM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 142585290,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=156, Top=243, InputMask=replicate('X',60)

  add object oDESCLI_1_29 as StdField with uid="IPYBIVGFJI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 75607498,;
   bGlobalFont=.t.,;
    Height=21, Width=326, Left=227, Top=69, InputMask=replicate('X',60)

  add object oDESIMP_1_30 as StdField with uid="OWGQXUUFVS",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 43274806,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=190, Top=127, InputMask=replicate('X',50)

  add object oNOMDES_1_31 as StdField with uid="TGPRZPGZSU",rtseq=21,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 84868310,;
   bGlobalFont=.t.,;
    Height=21, Width=396, Left=157, Top=98, InputMask=replicate('X',50)

  add object oDESDOC_1_32 as StdField with uid="YTQOVOWVBP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173059530,;
   bGlobalFont=.t.,;
    Height=21, Width=396, Left=157, Top=156, InputMask=replicate('X',35)


  add object oObj_1_34 as cp_runprogram with uid="MNWXUTUMZL",left=251, top=354, width=187,height=19,;
    caption='GSAG_BDC(G)',;
   bGlobalFont=.t.,;
    prg="GSAG_BDC('G')",;
    cEvent = "Record Inserted",;
    nPag=1;
    , ToolTipText = "Lancio l'elaborazione...";
    , HelpContextID = 95689257

  add object oDESATT_1_37 as StdField with uid="TDUZEQYLCF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 117199414,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=223, Top=185, InputMask=replicate('X',254)


  add object oDTFLRAGG_1_45 as StdCombo with uid="EJVKCVFCJL",rtseq=29,rtrep=.f.,left=86,top=272,width=193,height=23;
    , HelpContextID = 65442173;
    , cFormVar="w_DTFLRAGG",RowSource=""+"Nessuna,"+"Raggruppa per prestazione,"+"Crea riferimenti contratto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDTFLRAGG_1_45.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oDTFLRAGG_1_45.GetRadio()
    this.Parent.oContained.w_DTFLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oDTFLRAGG_1_45.SetRadio()
    this.Parent.oContained.w_DTFLRAGG=trim(this.Parent.oContained.w_DTFLRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_DTFLRAGG=='N',1,;
      iif(this.Parent.oContained.w_DTFLRAGG=='S',2,;
      iif(this.Parent.oContained.w_DTFLRAGG=='R',3,;
      0)))
  endfunc

  func oDTFLRAGG_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(g_ARTDES) and .cFunction='Load')
    endwith
   endif
  endfunc


  add object oObj_1_46 as cp_runprogram with uid="YXZOEHJFAL",left=251, top=333, width=187,height=19,;
    caption='GSAG_BDC(D)',;
   bGlobalFont=.t.,;
    prg="GSAG_BDC('D')",;
    cEvent = "HasEvent",;
    nPag=1;
    , HelpContextID = 95688489


  add object oBtn_1_49 as StdButton with uid="KSUWNHATXF",left=403, top=273, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla visualizzazione dei documenti generati";
    , HelpContextID = 268246928;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      with this.Parent.oContained
        do GSAG_BZM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_49.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' and !empty(.w_DTSERIAL))
      endwith
    endif
  endfunc

  add object oStr_1_11 as StdString with uid="TVCIFMGAVJ",Visible=.t., Left=2, Top=40,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="AUBXBTGJLF",Visible=.t., Left=26, Top=11,;
    Alignment=1, Width=58, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="DGFKLDWHRT",Visible=.t., Left=180, Top=11,;
    Alignment=1, Width=21, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="JDCVRMSETW",Visible=.t., Left=18, Top=243,;
    Alignment=1, Width=66, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="ONUTPMZIOG",Visible=.t., Left=42, Top=69,;
    Alignment=1, Width=42, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="AIDRLDRRXU",Visible=.t., Left=36, Top=98,;
    Alignment=1, Width=48, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="UHBKHPYPBA",Visible=.t., Left=21, Top=127,;
    Alignment=1, Width=63, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="SJOQLOCXTS",Visible=.t., Left=17, Top=156,;
    Alignment=1, Width=67, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ZKOCZKVJXR",Visible=.t., Left=32, Top=214,;
    Alignment=1, Width=52, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="LNDNIINIWV",Visible=.t., Left=4, Top=185,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="KGOUNWWRXO",Visible=.t., Left=401, Top=11,;
    Alignment=1, Width=44, Height=18,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="KXFVGGJEDW",Visible=.t., Left=26, Top=272,;
    Alignment=1, Width=58, Height=18,;
    Caption="Opzioni:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_agd','GES_DOCU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DTSERIAL=GES_DOCU.DTSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
