var FSO = new ActiveXObject("Scripting.FileSystemObject")
var BASE_PATH
var strErrLog
var XML_FileStack
var HTM_FileList
var TOC_Tree
var CHM_Title


ReadArgs()


//********************************
function ReadArgs() {
  var objArgs = WScript.Arguments
  var NumArg = objArgs.Count()
  var XMLFile_arg = null
  var CHMFile_arg = null
  if (NumArg >= 1) {
    if (objArgs(0).toLowerCase().indexOf('.xml') > 0) {
      XMLFile_arg = objArgs(0)
    }
    if (NumArg > 1) CHMFile_arg = objArgs(1)
  }
  if (XMLFile_arg == null) {
    msgBox("Please specify an XML file.")
    WScript.Quit(1)
  } else if (!FSO.FileExists(XMLFile_arg)) {
    msgBox("File '" + XMLFile_arg + "' does not exist!")
    WScript.Quit(2)
  } else {
    InitProcess(XMLFile_arg, CHMFile_arg)
  }
}

//********************************
function InitProcess(FileToLoad, FileToCompile) {
  strErrLog = ''
  XML_FileStack = new Array()
  HTM_FileList = new Array()
  TOC_Tree = new Array()

  FileToLoad = FileToLoad.toLowerCase()
  BASE_PATH = FSO.GetParentFolderName( FileToLoad.replace(/\\/g, '/') ) + '/'

  if (FileToCompile == null) {
    FileToCompile = BASE_PATH + FSO.GetBaseName(FileToLoad) + '.chm'
  }
  var hhFilename = BASE_PATH + FSO.GetBaseName(FileToCompile)

  LoadXML(FileToLoad)

  WriteProject(hhFilename, FileToCompile)
  var keywords = WriteTOCTree(hhFilename)
  WriteIndex(keywords, hhFilename)

  if (strErrLog != '') {
    var errFile = hhFilename + '.log'
    WriteErrorLog(errFile)
    msgBox("Error found during conversion process!\n" +
           "See file '" + errFile + "' for more details.")
    WScript.Quit(3)
  } else {
    msgBox(hhFilename + ".hhp created.")
    WScript.Quit(0)
  }
}

//********************************
function LoadXML(xmlFile) {
  if (!FSO.FileExists(xmlFile)) {
    strErrLog += "File '" + xmlFile + "' not found!" + "\r\n"
  } else if (XML_FileStack.join().indexOf(xmlFile) >= 0) {
    strErrLog += "File '" + xmlFile + "' already loaded! NOT INCLUDED." + "\r\n"
  } else {
    XML_FileStack.push(xmlFile)
    var xmlSrc = InitXMLDOM()
    xmlSrc.async = false
    xmlSrc.load(xmlFile)
    if (xmlSrc.parseError.reason) {
      //msgBox(getXMLDOMErr(xmlSrc))
      strErrLog += getXMLDOMErr(xmlSrc) + "\r\n"
    } else {
      ConvertXMLtoHTML(xmlSrc)
      ReadNodes(xmlSrc.documentElement)
    }
    XML_FileStack.pop(xmlFile)
  }
}

//********************************
function ConvertXMLtoHTML(objXML) {
  var htmName = objXML.url.substr(8).replace('.xml','.htm')
  var relativeName = htmName.replace(BASE_PATH,'').toLowerCase()
  if (HTM_FileList.join().indexOf(relativeName) < 0) {
    var xslSrc = new ActiveXObject("MSXML2.FreeThreadedDOMDocument")
    xslSrc.async = false
    xslSrc.load( FSO.GetParentFolderName(htmName)+'/'+GetXSLFileNameFromPI(objXML) )
    var xslt = new ActiveXObject("MSXML2.XSLTemplate")
    xslt.stylesheet = xslSrc
    var XSLProcessor = xslt.createProcessor()
    var out = new ActiveXObject("ADODB.Stream")
    out.Open()
    out.Charset = xslSrc.selectSingleNode("/xsl:stylesheet/xsl:output/@encoding").value
    XSLProcessor.output = out
    try {
      XSLProcessor.input = objXML
      XSLProcessor.addParameter("default_extension", '.htm')
      XSLProcessor.transform()
      XSLProcessor.output.SetEOS()
      XSLProcessor.output.SaveToFile(htmName, 2)
    } catch (e) {
      strErrLog += "Error transforming xml node on " + objXML.url + ": "
      strErrLog += e.description + "\r\n"
    }
    HTM_FileList.push(relativeName)
  }
}

//********************************
function ReadNodes(objNode) {
  ProcessNode(objNode)
  for (var n = 0; n < objNode.childNodes.length; n++) {
    ReadNodes(objNode.childNodes(n))
  }
}

//********************************
function ProcessNode(objNode) {
  if (objNode.nodeType == 1) {//ELEMENT
    var XSLFileName = GetXSLFileNameFromPI(objNode.ownerDocument).replace('.xsl', '')
    switch (objNode.nodeName) {
      case 'Plan':
        var title = objNode.selectSingleNode("PlanTitle/text()")
        if (title != null) {
          title = title.nodeValue
        } else {
          title = XSLFileName
          switch (title) {
            case 'Main': title = 'Project overview'; break
            case 'TablesSummary': title = 'Tables summary'; break
            case 'EntitiesRecap': title = 'Entities summary'; break
            case 'DataDictionary': title = 'Data dictionary'; break
          }
        }
        var sort = null
        switch (XSLFileName) {
          case 'TablesSummary': case 'Tables': sort = ByName; break
          case 'Entities': sort = ByTypeAndName; break
        }
        FillTOCTree(objNode, 1, title, null, sort)
        break

      case 'Codify':
      case 'Routine':
        var level = 1
        var title = objNode.selectSingleNode("Title/text()")
        var type = objNode.selectSingleNode("Type/text()")
        if (type != null && type.nodeValue == 'Documentation') {
          level = 0
          if (title != null) CHM_Title = title.nodeValue
        }
        title = (title != null)? title.nodeValue: objNode.nodeName
        FillTOCTree(objNode, level, title)
        break

      case 'Pag':
        var title = objNode.selectSingleNode("Title")
        if (title != null) {
          var anchor = title.getAttribute("LinkID")
          title = title.selectSingleNode("text()")
          if (title != null) {
            title = title.nodeValue
          } else {
            title = (objNode.parentNode.nodeName == 'Routine')? 'Procedure ': 'Page '
            title += objNode.getAttribute("number")
          }
          FillTOCTree(objNode, 2, title, anchor)
        }
        break

      case 'Item':
        var name = objNode.selectSingleNode("Name")
        if (name != null && objNode.selectSingleNode("/Routine") == null) {
          var anchor = name.getAttribute("LinkID")
          var title = name.selectSingleNode("text()")
          if (title != null) {
            title = title.nodeValue
          } else {
            title = 'unnamed'
          }
          var descr = objNode.selectSingleNode("Comment/text()")
          if (XSLFileName == 'User' && descr != null) {
            title = descr.nodeValue
          }
          FillTOCTree(objNode, 3, title, anchor)
        }
        break

      case 'PlanItemlist':
        if (XSLFileName == 'Diagrams') {
          FillTOCTree(objNode, 2, "TOP-LEVEL", "TOP-LEVEL")
        }
        break

      case 'PlanItem':
        var title = objNode.selectSingleNode("ItemName/text()")
        var type = objNode.selectSingleNode("ItemObj/text()")
        if (title != null && type != null &&
            ((type.nodeValue == 'Group' && XSLFileName == 'Diagrams') ||
            (type.nodeValue != 'Group' && XSLFileName == 'Entities'))) {
          title = title.nodeValue
          FillTOCTree(objNode, 2, title, title, null, type.nodeValue)
        }
        break

      case 'ItemTable':
        var title = objNode.selectSingleNode("TableName/text()")
        if (title != null && XSLFileName.substr(0,6) == 'Tables') {
          title = title.nodeValue
          FillTOCTree(objNode, 2, title, 'Table_'+title)
        }
        break

      case 'File':
        var src = GetRootURL(objNode)
        src = FSO.GetParentFolderName(src) + '/' + objNode.getAttribute('src')
        LoadXML(src)
        break
    }
  }
}

//********************************
function FillTOCTree(objNode, level, title, anchor, sort, type) {
  var node = new CHM_TOCNode(level)
  node.label = title
  node.link = GetRootURL(objNode).replace(BASE_PATH, '').replace('.xml', '.htm')
  node.anchor = anchor
  node.sort = sort
  node.type = type
  AddTOCNode(TOC_Tree, node)
}

//********************************
function AddTOCNode(tree, node) {
  var last = tree.length
  if (last > 0) {
    if (node.level > tree[last-1].level) {
      AddTOCNode(tree[last-1].children, node)
    } else {
      tree.push(node)
    }
  } else {
    tree.push(node)
  }
}

//********************************
function CHM_TOCNode(lvl) {
  this.level = lvl
  this.label = ''
  this.link = ''
  this.anchor = null
  this.sort = null
  this.type = null
  this.children = new Array()

  this.quotedLabel = function() {
    return this.label.replace(/"/g, '&quot;')
  }

  this.fullLink = function() {
    if (this.anchor == null) return this.link
    else return this.link + '#' + this.anchor
  }
}

//********************************
function CHM_Keyword() {
  this.context = new Array()
  this.link = new Array()
}

//********************************
function WriteProject(hhFilename, chmFilename) {
  var f = FSO.CreateTextFile(hhFilename + '.hhp', true)
  var prjName = FSO.GetBaseName(hhFilename)
  f.WriteLine('[OPTIONS]')
  f.WriteLine('Auto Index=Yes')
  f.WriteLine('Binary Index=No')
  f.WriteLine('Compatibility=1.1 or later')  //deve essere cos� e non "1.0" altrimenti il link con altri CHM non funziona!!
  f.WriteLine('Compiled file=' + chmFilename)
  f.WriteLine('Contents file=' + prjName + '.hhc')
  f.WriteLine('Index file=' + prjName + '.hhk')
  f.WriteLine('Error log file=' + prjName + '.log')
  f.WriteLine('Default topic=' + HTM_FileList[0])
  f.WriteLine('Display compile progress=No')
  f.WriteLine('Full-text search=Yes')
  f.WriteLine('Language=0x410 Italian (Italy)')
  f.WriteLine('Title=' + (CHM_Title!=null? CHM_Title: FSO.GetBaseName(chmFilename)))
  f.WriteLine('')
  f.WriteLine('[FILES]')
  for (var n in HTM_FileList) {
    f.WriteLine(HTM_FileList[n])
  }
  f.WriteLine('')
  f.WriteLine('[ALIAS]')
  f.WriteLine('#include ' + prjName + '_alias.h')
  f.WriteLine('')
  f.WriteLine('[MAP]')
  f.WriteLine('#include ' + prjName + '.h')
  f.Close()
}

//********************************
function WriteTOCTree(hhFilename) {
  var f = FSO.CreateTextFile(hhFilename + '.hhc', true)
  var g = FSO.CreateTextFile(hhFilename + '_alias.h', true)
  var h = FSO.CreateTextFile(hhFilename + '.h', true)
  f.WriteLine('<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">')
  f.WriteLine('<HTML><HEAD>')
  f.WriteLine('<meta name="GENERATOR" content="Microsoft&reg; HTML Help Workshop 4.1">')
  f.WriteLine('</HEAD><BODY BGCOLOR="#FFFFFF">')
  f.WriteLine('<OBJECT type="text/site properties">')
  f.WriteLine('  <param name="Window Styles" value="0x800225">')
  f.WriteLine('</OBJECT>')
  var keywords = new ActiveXObject("Scripting.Dictionary")
  WriteTOCNodes(TOC_Tree, f, g, h, '', 1, keywords, '')
  f.WriteLine('</BODY></HTML>')
  f.Close()
  g.Close()
  h.Close()
  return keywords
}

//********************************
function WriteTOCNodes(nodes, f, g, h, spc, cnt, keywords, context) {
  var i, key, link, anchor
  if (nodes.length > 0) {
    f.WriteLine(spc+'<UL>')
    for (i in nodes) {
      key = nodes[i].quotedLabel()
      link = nodes[i].fullLink()
      anchor = nodes[i].anchor
      f.WriteLine(spc + '  <LI><OBJECT type="text/sitemap">')
      f.WriteLine(spc + '      <param name="Name" value="' + key + '">')
      f.WriteLine(spc + '      <param name="Local" value="' + link + '">')
      f.WriteLine(spc + '  </OBJECT></LI>')
      if (anchor != null) {
        if (parseInt(anchor) > 0) {
          g.WriteLine('IDH_' + anchor + ' = ' + link)
          h.WriteLine('#define IDH_' + anchor + ' ' + anchor)
        }
      } else {
        g.WriteLine('IDH_DUMMY' + cnt + ' = ' + link)
        h.WriteLine('#define IDH_DUMMY' + cnt + ' ' + cnt)
        ++cnt
      }
      if (!keywords.Exists(key)) {
        keywords.Add(key, new CHM_Keyword())
      }
      keywords(key).context.push(context)
      keywords(key).link.push(nodes[i].link)
      if (nodes[i].sort) nodes[i].children.sort(nodes[i].sort)
      cnt = WriteTOCNodes(nodes[i].children, f, g, h, spc+'  ', cnt, keywords, context+" / "+key)
    }
    f.WriteLine(spc+'</UL>')
  }
  return cnt
}

//********************************
function WriteIndex(keywords, hhFilename) {
  var f = FSO.CreateTextFile(hhFilename + '.hhk', true)
  f.WriteLine('<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">')
  f.WriteLine('<HTML><HEAD>')
  f.WriteLine('<meta name="GENERATOR" content="Microsoft&reg; HTML Help Workshop 4.1">')
  f.WriteLine('</HEAD><BODY BGCOLOR="#FFFFFF">')
  f.WriteLine('<UL>')

  var i, j, k
  var keys = new VBArray(keywords.Keys()).toArray()
  for (i in keys.sort(CaseInsensitive)) { //ordina alfabeticamente l'indice
    f.WriteLine('<LI><OBJECT type="text/sitemap">')
    f.WriteLine('    <param name="Name" value="' + keys[i] + '">') //Chiave
    k = keywords(keys[i])
    for (j in k.context) { //scrive i vari link alla chiave
      f.WriteLine('    <param name="Name" value="' + k.context[j].substr(3) + '">')
      f.WriteLine('    <param name="Local" value="' + k.link[j] + '">')
    }
    f.WriteLine('</OBJECT></LI>')
  }

  f.WriteLine('</UL>')
  f.WriteLine('</BODY></HTML>')
  f.Close()
}

//********************************
function WriteErrorLog(filename) {
  var f = FSO.CreateTextFile(filename, true)
  f.Write('******** ERROR LOG ********\r\n\r\n' + strErrLog)
  f.Close()
}

//********************************
function InitXMLDOM() {
  try {
    return new ActiveXObject('MSXML2.DOMDocument')
  } catch (except) {
    WScript.Quit(except.number) //XMLDOM initialization failed
  }
}

//********************************
function GetRootURL(objNode) {
  return objNode.ownerDocument.url.replace('file:///', '')
}

//********************************
function GetXSLFileNameFromPI(xmlSrc) {
  var xsl = ''
  for (var i=0; i<xmlSrc.childNodes.length; ++i) {
    if (xmlSrc.childNodes[i].nodeName == 'xml-stylesheet') {
      xsl = xmlSrc.childNodes[i].nodeValue
      xsl = xsl.substr(xsl.indexOf('href=')+6)
      xsl = xsl.substr(0, xsl.length-1)
    }
  }
  return xsl
}

//********************************
function CaseInsensitive(A, B) {
  var a = A.toLowerCase()
  var b = B.toLowerCase()
  if (a < b) return -1
  if (a > b) return +1
  return 0
}

//********************************
function ByName(A, B) {
  return CaseInsensitive(A.label, B.label)
}

//********************************
function ByTypeAndName(A, B) {
  var t = CaseInsensitive(A.type, B.type)
  if (t == 0) t = CaseInsensitive(A.label, B.label)
  return t
}

//********************************
function msgBox(text) {
  WScript.Echo(text)
}

//********************************
function getXMLDOMErr(src) {
  return src.parseError.reason + '\r\n'
       + 'File URL: ' + src.parseError.url + '\r\n'
       + 'Line No.: ' + src.parseError.line + '\r\n'
       + 'Character: ' + src.parseError.linepos + '\r\n'
       + 'File Position: ' + src.parseError.filepos + '\r\n'
       + 'Source Text: ' + src.parseError.srcText + '\r\n'
       + 'Error Code: ' + src.parseError.errorCode
}

