* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsst_mce                                                        *
*              Criteri di elaborazione                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_323]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-02                                                      *
* Last revis.: 2011-05-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsst_mce"))

* --- Class definition
define class tgsst_mce as StdTrsForm
  Top    = -4
  Left   = 15

  * --- Standard Properties
  Width  = 653
  Height = 364+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-11"
  HelpContextID=97076585
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CRIMELAB_IDX = 0
  CRIDELAB_IDX = 0
  CAU_SELE_IDX = 0
  cFile = "CRIMELAB"
  cFileDetail = "CRIDELAB"
  cKeySelect = "CECODICE"
  cKeyWhere  = "CECODICE=this.w_CECODICE"
  cKeyDetail  = "CECODICE=this.w_CECODICE"
  cKeyWhereODBC = '"CECODICE="+cp_ToStrODBC(this.w_CECODICE)';

  cKeyDetailWhereODBC = '"CECODICE="+cp_ToStrODBC(this.w_CECODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CRIDELAB.CECODICE="+cp_ToStrODBC(this.w_CECODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CRIDELAB.CPROWORD '
  cPrg = "gsst_mce"
  cComment = "Criteri di elaborazione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 14
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CECODICE = space(10)
  w_CEDESCRI = space(50)
  w_OBTEST = ctod('  /  /  ')
  w_OQRY = space(50)
  w_OREP = space(50)
  o_OREP = space(50)
  w_ODES = space(50)
  o_ODES = space(50)
  w_CPROWORD = 0
  w_CENUMCAM = 0
  o_CENUMCAM = 0
  w_CEFLGQUA = space(1)
  w_CETIPPER = space(2)
  w_REPORT = space(100)
  w_EXCEL = space(100)
  o_EXCEL = space(100)
  w_GRAFH = space(100)
  o_GRAFH = space(100)
  w_CENOMREP = space(100)
  w_CENOMXLT = space(100)
  w_CEFLGRAF = space(1)
  w_CENOMGRP = space(100)
  w_CERIGORD = 0
  w_CEDESSTA = space(50)

  * --- Children pointers
  GSST_MCS = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CRIMELAB','gsst_mce')
    stdPageFrame::Init()
    *set procedure to GSST_MCS additive
    with this
      .Pages(1).addobject("oPag","tgsst_mcePag1","gsst_mce",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Criteri di elaborazione")
      .Pages(1).HelpContextID = 9058581
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCECODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSST_MCS
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAU_SELE'
    this.cWorkTables[2]='CRIMELAB'
    this.cWorkTables[3]='CRIDELAB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CRIMELAB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CRIMELAB_IDX,3]
  return

  function CreateChildren()
    this.GSST_MCS = CREATEOBJECT('stdDynamicChild',this,'GSST_MCS',this.oPgFrm.Page1.oPag.oLinkPC_1_4)
    this.GSST_MCS.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSST_MCS)
      this.GSST_MCS.DestroyChildrenChain()
      this.GSST_MCS=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_4')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSST_MCS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSST_MCS.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSST_MCS.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSST_MCS.ChangeRow(this.cRowID+'      1',1;
             ,.w_CECODICE,"CSCODICE";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CECODICE = NVL(CECODICE,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CRIMELAB where CECODICE=KeySet.CECODICE
    *
    i_nConn = i_TableProp[this.CRIMELAB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CRIMELAB_IDX,2],this.bLoadRecFilter,this.CRIMELAB_IDX,"gsst_mce")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CRIMELAB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CRIMELAB.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CRIDELAB.","CRIMELAB.")
      i_cTable = i_cTable+' CRIMELAB '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CECODICE',this.w_CECODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_OQRY = space(50)
        .w_OREP = space(50)
        .w_ODES = space(50)
        .w_REPORT = space(100)
        .w_EXCEL = space(100)
        .w_GRAFH = space(100)
        .w_CECODICE = NVL(CECODICE,space(10))
        .w_CEDESCRI = NVL(CEDESCRI,space(50))
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .w_CENOMREP = NVL(CENOMREP,space(100))
        .w_CENOMXLT = NVL(CENOMXLT,space(100))
        .w_CEFLGRAF = NVL(CEFLGRAF,space(1))
        .w_CENOMGRP = NVL(CENOMGRP,space(100))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .w_CEDESSTA = NVL(CEDESSTA,space(50))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CRIMELAB')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CRIDELAB where CECODICE=KeySet.CECODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CRIDELAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CRIDELAB_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CRIDELAB')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CRIDELAB.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CRIDELAB"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CECODICE',this.w_CECODICE  )
        select * from (i_cTable) CRIDELAB where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CENUMCAM = NVL(CENUMCAM,0)
          .w_CEFLGQUA = NVL(CEFLGQUA,space(1))
          .w_CETIPPER = NVL(CETIPPER,space(2))
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
          .w_CERIGORD = NVL(CERIGORD,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CECODICE=space(10)
      .w_CEDESCRI=space(50)
      .w_OBTEST=ctod("  /  /  ")
      .w_OQRY=space(50)
      .w_OREP=space(50)
      .w_ODES=space(50)
      .w_CPROWORD=10
      .w_CENUMCAM=0
      .w_CEFLGQUA=space(1)
      .w_CETIPPER=space(2)
      .w_REPORT=space(100)
      .w_EXCEL=space(100)
      .w_GRAFH=space(100)
      .w_CENOMREP=space(100)
      .w_CENOMXLT=space(100)
      .w_CEFLGRAF=space(1)
      .w_CENOMGRP=space(100)
      .w_CERIGORD=0
      .w_CEDESSTA=space(50)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .DoRTCalc(4,8,.f.)
        .w_CEFLGQUA = 'N'
        .w_CETIPPER = iif(.w_CENUMCAM=13,.w_CETIPPER,'  ')
        .DoRTCalc(11,13,.f.)
        .w_CENOMREP = ALLTRIM(.w_OREP)
        .w_CENOMXLT = SYS(2014, .w_EXCEL)
        .w_CEFLGRAF = 'N'
        .w_CENOMGRP = SYS(2014, .w_GRAFH)
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
        .DoRTCalc(18,18,.f.)
        .w_CEDESSTA = alltrim(.w_ODES)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CRIMELAB')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCECODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCEDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oCENOMXLT_1_14.enabled = i_bVal
      .Page1.oPag.oCEFLGRAF_1_15.enabled = i_bVal
      .Page1.oPag.oBtn_1_12.enabled = i_bVal
      .Page1.oPag.oObj_1_6.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCECODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCECODICE_1_1.enabled = .t.
        .Page1.oPag.oCEDESCRI_1_2.enabled = .t.
      endif
    endwith
    this.GSST_MCS.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CRIMELAB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSST_MCS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CRIMELAB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CECODICE,"CECODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDESCRI,"CEDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CENOMREP,"CENOMREP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CENOMXLT,"CENOMXLT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEFLGRAF,"CEFLGRAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CENOMGRP,"CENOMGRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CEDESSTA,"CEDESSTA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CRIMELAB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CRIMELAB_IDX,2])
    i_lTable = "CRIMELAB"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CRIMELAB_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_CENUMCAM N(3);
      ,t_CEFLGQUA N(3);
      ,t_CETIPPER N(3);
      ,CPROWNUM N(10);
      ,t_CERIGORD N(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsst_mcebodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCENUMCAM_2_2.controlsource=this.cTrsName+'.t_CENUMCAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCEFLGQUA_2_3.controlsource=this.cTrsName+'.t_CEFLGQUA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCETIPPER_2_4.controlsource=this.cTrsName+'.t_CETIPPER'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(59)
    this.AddVLine(155)
    this.AddVLine(191)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CRIMELAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CRIMELAB_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CRIMELAB
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CRIMELAB')
        i_extval=cp_InsertValODBCExtFlds(this,'CRIMELAB')
        local i_cFld
        i_cFld=" "+;
                  "(CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF"+;
                  ",CENOMGRP,CEDESSTA"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CECODICE)+;
                    ","+cp_ToStrODBC(this.w_CEDESCRI)+;
                    ","+cp_ToStrODBC(this.w_CENOMREP)+;
                    ","+cp_ToStrODBC(this.w_CENOMXLT)+;
                    ","+cp_ToStrODBC(this.w_CEFLGRAF)+;
                    ","+cp_ToStrODBC(this.w_CENOMGRP)+;
                    ","+cp_ToStrODBC(this.w_CEDESSTA)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CRIMELAB')
        i_extval=cp_InsertValVFPExtFlds(this,'CRIMELAB')
        cp_CheckDeletedKey(i_cTable,0,'CECODICE',this.w_CECODICE)
        INSERT INTO (i_cTable);
              (CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF,CENOMGRP,CEDESSTA &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CECODICE;
                  ,this.w_CEDESCRI;
                  ,this.w_CENOMREP;
                  ,this.w_CENOMXLT;
                  ,this.w_CEFLGRAF;
                  ,this.w_CENOMGRP;
                  ,this.w_CEDESSTA;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CRIDELAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CRIDELAB_IDX,2])
      *
      * insert into CRIDELAB
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CECODICE,CPROWORD,CENUMCAM,CEFLGQUA,CETIPPER"+;
                  ",CERIGORD,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CECODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CENUMCAM)+","+cp_ToStrODBC(this.w_CEFLGQUA)+","+cp_ToStrODBC(this.w_CETIPPER)+;
             ","+cp_ToStrODBC(this.w_CERIGORD)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CECODICE',this.w_CECODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CECODICE,this.w_CPROWORD,this.w_CENUMCAM,this.w_CEFLGQUA,this.w_CETIPPER"+;
                ",this.w_CERIGORD,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CRIMELAB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CRIMELAB_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CRIMELAB
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CRIMELAB')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CEDESCRI="+cp_ToStrODBC(this.w_CEDESCRI)+;
             ",CENOMREP="+cp_ToStrODBC(this.w_CENOMREP)+;
             ",CENOMXLT="+cp_ToStrODBC(this.w_CENOMXLT)+;
             ",CEFLGRAF="+cp_ToStrODBC(this.w_CEFLGRAF)+;
             ",CENOMGRP="+cp_ToStrODBC(this.w_CENOMGRP)+;
             ",CEDESSTA="+cp_ToStrODBC(this.w_CEDESSTA)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CRIMELAB')
          i_cWhere = cp_PKFox(i_cTable  ,'CECODICE',this.w_CECODICE  )
          UPDATE (i_cTable) SET;
              CEDESCRI=this.w_CEDESCRI;
             ,CENOMREP=this.w_CENOMREP;
             ,CENOMXLT=this.w_CENOMXLT;
             ,CEFLGRAF=this.w_CEFLGRAF;
             ,CENOMGRP=this.w_CENOMGRP;
             ,CEDESSTA=this.w_CEDESSTA;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_CENUMCAM)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CRIDELAB_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CRIDELAB_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CRIDELAB
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CRIDELAB
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CENUMCAM="+cp_ToStrODBC(this.w_CENUMCAM)+;
                     ",CEFLGQUA="+cp_ToStrODBC(this.w_CEFLGQUA)+;
                     ",CETIPPER="+cp_ToStrODBC(this.w_CETIPPER)+;
                     ",CERIGORD="+cp_ToStrODBC(this.w_CERIGORD)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CENUMCAM=this.w_CENUMCAM;
                     ,CEFLGQUA=this.w_CEFLGQUA;
                     ,CETIPPER=this.w_CETIPPER;
                     ,CERIGORD=this.w_CERIGORD;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSST_MCS : Saving
      this.GSST_MCS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CECODICE,"CSCODICE";
             )
      this.GSST_MCS.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsst_mce
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
        * --- Riposiziona sul Primo record del Temporaneo dei Documenti
        * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        this.SaveDependsOn()
    
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSST_MCS : Deleting
    this.GSST_MCS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CECODICE,"CSCODICE";
           )
    this.GSST_MCS.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND NOT EMPTY(t_CENUMCAM)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CRIDELAB_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CRIDELAB_IDX,2])
        *
        * delete CRIDELAB
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CRIMELAB_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CRIMELAB_IDX,2])
        *
        * delete CRIMELAB
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_CENUMCAM)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsst_mce
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
        * --- Riposiziona sul Primo record del Temporaneo dei Documenti
        * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
        if not(bTrsErr)
            select (this.cTrsName)
            go top
            this.WorkFromTrs()
        endif
    endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CRIMELAB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CRIMELAB_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .DoRTCalc(1,9,.t.)
        if .o_CENUMCAM<>.w_CENUMCAM
          .w_CETIPPER = iif(.w_CENUMCAM=13,.w_CETIPPER,'  ')
        endif
        .DoRTCalc(11,13,.t.)
        if .o_OREP<>.w_OREP
          .w_CENOMREP = ALLTRIM(.w_OREP)
        endif
        if .o_EXCEL<>.w_EXCEL
          .w_CENOMXLT = SYS(2014, .w_EXCEL)
        endif
        .DoRTCalc(16,16,.t.)
        if .o_GRAFH<>.w_GRAFH
          .w_CENOMGRP = SYS(2014, .w_GRAFH)
        endif
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
        .DoRTCalc(18,18,.t.)
        if .o_ODES<>.w_ODES
          .w_CEDESSTA = alltrim(.w_ODES)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CERIGORD with this.w_CERIGORD
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCETIPPER_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCETIPPER_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCECODICE_1_1.value==this.w_CECODICE)
      this.oPgFrm.Page1.oPag.oCECODICE_1_1.value=this.w_CECODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDESCRI_1_2.value==this.w_CEDESCRI)
      this.oPgFrm.Page1.oPag.oCEDESCRI_1_2.value=this.w_CEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCENOMXLT_1_14.value==this.w_CENOMXLT)
      this.oPgFrm.Page1.oPag.oCENOMXLT_1_14.value=this.w_CENOMXLT
    endif
    if not(this.oPgFrm.Page1.oPag.oCEFLGRAF_1_15.RadioValue()==this.w_CEFLGRAF)
      this.oPgFrm.Page1.oPag.oCEFLGRAF_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDESSTA_1_20.value==this.w_CEDESSTA)
      this.oPgFrm.Page1.oPag.oCEDESSTA_1_20.value=this.w_CEDESSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCENUMCAM_2_2.RadioValue()==this.w_CENUMCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCENUMCAM_2_2.SetRadio()
      replace t_CENUMCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCENUMCAM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCEFLGQUA_2_3.RadioValue()==this.w_CEFLGQUA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCEFLGQUA_2_3.SetRadio()
      replace t_CEFLGQUA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCEFLGQUA_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCETIPPER_2_4.RadioValue()==this.w_CETIPPER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCETIPPER_2_4.SetRadio()
      replace t_CETIPPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCETIPPER_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'CRIMELAB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CECODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCECODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CECODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSST_MCS.CheckForm()
      if i_bres
        i_bres=  .GSST_MCS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_CPROWORD<>0 AND NOT EMPTY(.w_CENUMCAM)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_OREP = this.w_OREP
    this.o_ODES = this.w_ODES
    this.o_CENUMCAM = this.w_CENUMCAM
    this.o_EXCEL = this.w_EXCEL
    this.o_GRAFH = this.w_GRAFH
    * --- GSST_MCS : Depends On
    this.GSST_MCS.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND NOT EMPTY(t_CENUMCAM))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_CENUMCAM=0
      .w_CEFLGQUA=space(1)
      .w_CETIPPER=space(2)
      .w_CERIGORD=0
      .DoRTCalc(1,8,.f.)
        .w_CEFLGQUA = 'N'
        .w_CETIPPER = iif(.w_CENUMCAM=13,.w_CETIPPER,'  ')
        .oPgFrm.Page1.oPag.oObj_2_8.Calculate()
    endwith
    this.DoRTCalc(11,19,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CENUMCAM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCENUMCAM_2_2.RadioValue(.t.)
    this.w_CEFLGQUA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCEFLGQUA_2_3.RadioValue(.t.)
    this.w_CETIPPER = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCETIPPER_2_4.RadioValue(.t.)
    this.w_CERIGORD = t_CERIGORD
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CENUMCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCENUMCAM_2_2.ToRadio()
    replace t_CEFLGQUA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCEFLGQUA_2_3.ToRadio()
    replace t_CETIPPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCETIPPER_2_4.ToRadio()
    replace t_CERIGORD with this.w_CERIGORD
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsst_mcePag1 as StdContainer
  Width  = 649
  height = 364
  stdWidth  = 649
  stdheight = 364
  resizeXpos=503
  resizeYpos=205
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCECODICE_1_1 as StdField with uid="OAUCZWBIAT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CECODICE", cQueryName = "CECODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice criterio di selezione",;
    HelpContextID = 130692203,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=137, Top=12, InputMask=replicate('X',10)

  add object oCEDESCRI_1_2 as StdField with uid="FECTGQLNTF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CEDESCRI", cQueryName = "CEDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione criterio",;
    HelpContextID = 45106287,;
   bGlobalFont=.t.,;
    Height=21, Width=366, Left=235, Top=12, InputMask=replicate('X',50)


  add object oLinkPC_1_4 as stdDynamicChildContainer with uid="HYBZDKMJDL",left=287, top=71, width=359, height=159, bOnScreen=.t.;



  add object oObj_1_6 as cp_runprogram with uid="EKRMBLGCSY",left=369, top=384, width=237,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="LOOKOUT('1','GSST_MCE')",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 80921062


  add object oBtn_1_12 as StdButton with uid="WSTKXTZHJV",left=600, top=250, width=20,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 96875562;
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        LOOKOUT(this.Parent.oContained,"0","GSST_MCE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCENOMXLT_1_14 as StdField with uid="ADBHFOUBYX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CENOMXLT", cQueryName = "CENOMXLT",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 145038214,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=283, Top=294, InputMask=replicate('X',100)

  add object oCEFLGRAF_1_15 as StdCheck with uid="QWSFGNRNPI",rtseq=16,rtrep=.f.,left=283, top=325, caption="Abilita grafico nella stampa statistiche",;
    ToolTipText = "Se attivo abilita grafico nella stampa statistiche",;
    HelpContextID = 16213100,;
    cFormVar="w_CEFLGRAF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCEFLGRAF_1_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CEFLGRAF,&i_cF..t_CEFLGRAF),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCEFLGRAF_1_15.GetRadio()
    this.Parent.oContained.w_CEFLGRAF = this.RadioValue()
    return .t.
  endfunc

  func oCEFLGRAF_1_15.ToRadio()
    this.Parent.oContained.w_CEFLGRAF=trim(this.Parent.oContained.w_CEFLGRAF)
    return(;
      iif(this.Parent.oContained.w_CEFLGRAF=='S',1,;
      0))
  endfunc

  func oCEFLGRAF_1_15.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oObj_1_17 as cp_askfile with uid="KSGUVNWEHS",left=599, top=295, width=20,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_EXCEL",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il foglio Excel";
    , HelpContextID = 96875562

  add object oCEDESSTA_1_20 as StdField with uid="BYFKZSLARS",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CEDESSTA", cQueryName = "CEDESSTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 45106279,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=283, Top=249, InputMask=replicate('X',50)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=59, width=270,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Seq. ord.",Field2="CENUMCAM",Label2="Raggrup.",Field3="CEFLGQUA",Label3="Qta",Field4="CETIPPER",Label4="Periodo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219014266

  add object oStr_1_5 as StdString with uid="XFFWVHDRRA",Visible=.t., Left=6, Top=14,;
    Alignment=1, Width=128, Height=15,;
    Caption="Codice criterio:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="ZJYIKQIXII",Visible=.t., Left=290, Top=53,;
    Alignment=0, Width=297, Height=18,;
    Caption="Causali utilizzabili"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="ZLTXUENEIR",Visible=.t., Left=18, Top=41,;
    Alignment=0, Width=240, Height=18,;
    Caption="Fasi della elaborazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="KRTOTCGGWP",Visible=.t., Left=284, Top=235,;
    Alignment=0, Width=304, Height=18,;
    Caption="Stampa associata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="PPQVTGAQMI",Visible=.t., Left=285, Top=279,;
    Alignment=0, Width=304, Height=18,;
    Caption="Foglio Excel associato:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsst_mcs",lower(this.oContained.GSST_MCS.class))=0
        this.oContained.GSST_MCS.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=80,;
    width=266+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=81,width=265+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oObj_2_8 as cp_runprogram with uid="TTUVBDCWZQ",width=128,height=22,;
   left=-3, top=378,;
    caption='GSST_BCG',;
   bGlobalFont=.t.,;
    prg="GSST_BCG",;
    cEvent = "ControlliFinali",;
    nPag=2;
    , HelpContextID = 41960109

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsst_mceBodyRow as CPBodyRowCnt
  Width=256
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="UVIIBRTBLV",rtseq=7,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 251342698,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=52, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oCENUMCAM_2_2 as StdTrsCombo with uid="TTEOQKIGRT",rtrep=.t.,;
    cFormVar="w_CENUMCAM", RowSource=""+"Cliente/Fornitore,"+"Agente,"+"Zona,"+"Grup. merc.,"+"Famiglia,"+"Cat. omog.,"+"Cat. cont.,"+"Cat. comm.,"+"Marchio,"+"Articolo\serv.,"+"Magaz.,"+"Periodo,"+"Dest. div.,"+"Voce di C/R,"+"Centro di C/R,"+"Commessa,"+"Attivit�,"+"Pagam.,"+"Capo area,"+"Per conto di,"+"Vettore,"+"Provincia,"+"Mastro,"+"Unit� di mis.,"+"Forn. abit." , ;
    ToolTipText = "Campo di raggruppamento/ordinamento",;
    HelpContextID = 39904371,;
    Height=21, Width=92, Left=53, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCENUMCAM_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CENUMCAM,&i_cF..t_CENUMCAM),this.value)
    return(iif(xVal =1,2,;
    iif(xVal =2,3,;
    iif(xVal =3,4,;
    iif(xVal =4,5,;
    iif(xVal =5,6,;
    iif(xVal =6,7,;
    iif(xVal =7,8,;
    iif(xVal =8,9,;
    iif(xVal =9,10,;
    iif(xVal =10,11,;
    iif(xVal =11,12,;
    iif(xVal =12,13,;
    iif(xVal =13,14,;
    iif(xVal =14,15,;
    iif(xVal =15,16,;
    iif(xVal =16,17,;
    iif(xVal =17,18,;
    iif(xVal =18,19,;
    iif(xVal =19,20,;
    iif(xVal =20,21,;
    iif(xVal =21,22,;
    iif(xVal =22,23,;
    iif(xVal =23,24,;
    iif(xVal =24,25,;
    iif(xVal =25,26,;
    0))))))))))))))))))))))))))
  endfunc
  func oCENUMCAM_2_2.GetRadio()
    this.Parent.oContained.w_CENUMCAM = this.RadioValue()
    return .t.
  endfunc

  func oCENUMCAM_2_2.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_CENUMCAM==2,1,;
      iif(this.Parent.oContained.w_CENUMCAM==3,2,;
      iif(this.Parent.oContained.w_CENUMCAM==4,3,;
      iif(this.Parent.oContained.w_CENUMCAM==5,4,;
      iif(this.Parent.oContained.w_CENUMCAM==6,5,;
      iif(this.Parent.oContained.w_CENUMCAM==7,6,;
      iif(this.Parent.oContained.w_CENUMCAM==8,7,;
      iif(this.Parent.oContained.w_CENUMCAM==9,8,;
      iif(this.Parent.oContained.w_CENUMCAM==10,9,;
      iif(this.Parent.oContained.w_CENUMCAM==11,10,;
      iif(this.Parent.oContained.w_CENUMCAM==12,11,;
      iif(this.Parent.oContained.w_CENUMCAM==13,12,;
      iif(this.Parent.oContained.w_CENUMCAM==14,13,;
      iif(this.Parent.oContained.w_CENUMCAM==15,14,;
      iif(this.Parent.oContained.w_CENUMCAM==16,15,;
      iif(this.Parent.oContained.w_CENUMCAM==17,16,;
      iif(this.Parent.oContained.w_CENUMCAM==18,17,;
      iif(this.Parent.oContained.w_CENUMCAM==19,18,;
      iif(this.Parent.oContained.w_CENUMCAM==20,19,;
      iif(this.Parent.oContained.w_CENUMCAM==21,20,;
      iif(this.Parent.oContained.w_CENUMCAM==22,21,;
      iif(this.Parent.oContained.w_CENUMCAM==23,22,;
      iif(this.Parent.oContained.w_CENUMCAM==24,23,;
      iif(this.Parent.oContained.w_CENUMCAM==25,24,;
      iif(this.Parent.oContained.w_CENUMCAM==26,25,;
      0))))))))))))))))))))))))))
  endfunc

  func oCENUMCAM_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCEFLGQUA_2_3 as StdTrsCheck with uid="PZPHNJNCFQ",rtrep=.t.,;
    cFormVar="w_CEFLGQUA",  caption="",;
    ToolTipText = "Se attiviato associa quantit� nella stampa al relativo raggruppamento",;
    HelpContextID = 267871335,;
    Left=150, Top=0, Width=31,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.f.;
   , bGlobalFont=.t.


  func oCEFLGQUA_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CEFLGQUA,&i_cF..t_CEFLGQUA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCEFLGQUA_2_3.GetRadio()
    this.Parent.oContained.w_CEFLGQUA = this.RadioValue()
    return .t.
  endfunc

  func oCEFLGQUA_2_3.ToRadio()
    this.Parent.oContained.w_CEFLGQUA=trim(this.Parent.oContained.w_CEFLGQUA)
    return(;
      iif(this.Parent.oContained.w_CEFLGQUA=='S',1,;
      0))
  endfunc

  func oCEFLGQUA_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCETIPPER_2_4 as StdTrsCombo with uid="JJKOLUOPIM",rtrep=.t.,;
    cFormVar="w_CETIPPER", RowSource=""+"Anno,"+"Semestr.,"+"Quadrim.,"+"Trimes.,"+"Bimes.,"+"Mese,"+"Settim.,"+"Giorno" , ;
    ToolTipText = "Tipo periodo",;
    HelpContextID = 260392056,;
    Height=21, Width=66, Left=185, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCETIPPER_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CETIPPER,&i_cF..t_CETIPPER),this.value)
    return(iif(xVal =1,'AN',;
    iif(xVal =2,'SM',;
    iif(xVal =3,'QU',;
    iif(xVal =4,'TR',;
    iif(xVal =5,'BI',;
    iif(xVal =6,'ME',;
    iif(xVal =7,'SE',;
    iif(xVal =8,'GI',;
    space(2))))))))))
  endfunc
  func oCETIPPER_2_4.GetRadio()
    this.Parent.oContained.w_CETIPPER = this.RadioValue()
    return .t.
  endfunc

  func oCETIPPER_2_4.ToRadio()
    this.Parent.oContained.w_CETIPPER=trim(this.Parent.oContained.w_CETIPPER)
    return(;
      iif(this.Parent.oContained.w_CETIPPER=='AN',1,;
      iif(this.Parent.oContained.w_CETIPPER=='SM',2,;
      iif(this.Parent.oContained.w_CETIPPER=='QU',3,;
      iif(this.Parent.oContained.w_CETIPPER=='TR',4,;
      iif(this.Parent.oContained.w_CETIPPER=='BI',5,;
      iif(this.Parent.oContained.w_CETIPPER=='ME',6,;
      iif(this.Parent.oContained.w_CETIPPER=='SE',7,;
      iif(this.Parent.oContained.w_CETIPPER=='GI',8,;
      0)))))))))
  endfunc

  func oCETIPPER_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCETIPPER_2_4.mCond()
    with this.Parent.oContained
      return (.w_CENUMCAM=13)
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=13
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsst_mce','CRIMELAB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CECODICE=CRIMELAB.CECODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
