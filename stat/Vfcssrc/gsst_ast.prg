* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsst_ast                                                        *
*              Ultima generazione                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_52]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-02                                                      *
* Last revis.: 2011-12-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsst_ast"))

* --- Class definition
define class tgsst_ast as StdForm
  Top    = 43
  Left   = 103

  * --- Standard Properties
  Width  = 440
  Height = 175+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-12-20"
  HelpContextID=109659497
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  ULTIMARC_IDX = 0
  ULTIMARC_IDX = 0
  cFile = "ULTIMARC"
  cKeySelect = "UGCODULT"
  cKeyWhere  = "UGCODULT=this.w_UGCODULT"
  cKeyWhereODBC = '"UGCODULT="+cp_ToStrODBC(this.w_UGCODULT)';

  cKeyWhereODBCqualified = '"ULTIMARC.UGCODULT="+cp_ToStrODBC(this.w_UGCODULT)';

  cPrg = "gsst_ast"
  cComment = "Ultima generazione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_UGCODULT = 0
  w_UGPRIMA = ctod('  /  /  ')
  w_UGULTIMA = ctod('  /  /  ')
  w_UGDATINI = ctod('  /  /  ')
  w_UGDATFIN = ctod('  /  /  ')
  w_UGAUTNUM = 0
  w_UGFLAG = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsst_ast
  
        proc ecpLoad()
        endproc
  
        proc ecpFilter()
        endproc
  
        proc ecpNext()
        endproc
  
        proc ecpPrior()
        endproc
  
        proc ecpF6()
        endproc
  
        proc ecpZoom()
        endproc
  
        proc ecpDelete()
        endproc
  
   procedure ecpQuery()
      this.lockscreen=.t.
      * ---
      this.cFunction = "Filter"  && possono esserci degli spostamenti, evita query spurie
      *this.SetEnabled('Query')
      if !this.bFirstQuery
        this.oPgFrm.ActivePage=1
        this.oFirstControl.SetFocus()
        this.__dummy__.enabled=.f.
      endif
      * ---
      this.cFunction = "Query"
      this.SetStatus()
      this.oFirstControl.SetFocus()
      if this.bFirstQuery
  	This.BlankRec()
          this.w_UGCODULT = 1
          this.LoadRec()
      else
           This.mHideControls()
      endif
      this.lockscreen=.f.
      this.bFirstQuery=.f.
    endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ULTIMARC','gsst_ast')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsst_astPag1","gsst_ast",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Archivio")
      .Pages(1).HelpContextID = 160079499
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsst_ast
      * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ULTIMARC'
    this.cWorkTables[2]='ULTIMARC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ULTIMARC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ULTIMARC_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_UGCODULT = NVL(UGCODULT,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ULTIMARC where UGCODULT=KeySet.UGCODULT
    *
    i_nConn = i_TableProp[this.ULTIMARC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ULTIMARC_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ULTIMARC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ULTIMARC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ULTIMARC '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'UGCODULT',this.w_UGCODULT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_UGCODULT = NVL(UGCODULT,0)
          * evitabile
          *.link_1_1('Load')
        .w_UGPRIMA = NVL(cp_ToDate(UGPRIMA),ctod("  /  /  "))
        .w_UGULTIMA = NVL(cp_ToDate(UGULTIMA),ctod("  /  /  "))
        .w_UGDATINI = NVL(cp_ToDate(UGDATINI),ctod("  /  /  "))
        .w_UGDATFIN = NVL(cp_ToDate(UGDATFIN),ctod("  /  /  "))
        .w_UGAUTNUM = NVL(UGAUTNUM,0)
        .w_UGFLAG = NVL(UGFLAG,space(1))
        cp_LoadRecExtFlds(this,'ULTIMARC')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_UGCODULT = 0
      .w_UGPRIMA = ctod("  /  /  ")
      .w_UGULTIMA = ctod("  /  /  ")
      .w_UGDATINI = ctod("  /  /  ")
      .w_UGDATFIN = ctod("  /  /  ")
      .w_UGAUTNUM = 0
      .w_UGFLAG = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_UGCODULT))
          .link_1_1('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'ULTIMARC')
    this.DoRTCalc(2,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsst_ast
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oUGAUTNUM_1_6.enabled = i_bVal
      .Page1.oPag.oUGFLAG_1_13.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ULTIMARC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsst_ast
    *    this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=.f.
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ULTIMARC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UGCODULT,"UGCODULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UGPRIMA,"UGPRIMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UGULTIMA,"UGULTIMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UGDATINI,"UGDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UGDATFIN,"UGDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UGAUTNUM,"UGAUTNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UGFLAG,"UGFLAG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsst_ast
       i_cWhere = 'where UGCODULT = 1'
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ULTIMARC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ULTIMARC_IDX,2])
    i_lTable = "ULTIMARC"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ULTIMARC_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ULTIMARC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ULTIMARC_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ULTIMARC_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ULTIMARC
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ULTIMARC')
        i_extval=cp_InsertValODBCExtFlds(this,'ULTIMARC')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(UGCODULT,UGPRIMA,UGULTIMA,UGDATINI,UGDATFIN"+;
                  ",UGAUTNUM,UGFLAG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_UGCODULT)+;
                  ","+cp_ToStrODBC(this.w_UGPRIMA)+;
                  ","+cp_ToStrODBC(this.w_UGULTIMA)+;
                  ","+cp_ToStrODBC(this.w_UGDATINI)+;
                  ","+cp_ToStrODBC(this.w_UGDATFIN)+;
                  ","+cp_ToStrODBC(this.w_UGAUTNUM)+;
                  ","+cp_ToStrODBC(this.w_UGFLAG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ULTIMARC')
        i_extval=cp_InsertValVFPExtFlds(this,'ULTIMARC')
        cp_CheckDeletedKey(i_cTable,0,'UGCODULT',this.w_UGCODULT)
        INSERT INTO (i_cTable);
              (UGCODULT,UGPRIMA,UGULTIMA,UGDATINI,UGDATFIN,UGAUTNUM,UGFLAG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_UGCODULT;
                  ,this.w_UGPRIMA;
                  ,this.w_UGULTIMA;
                  ,this.w_UGDATINI;
                  ,this.w_UGDATFIN;
                  ,this.w_UGAUTNUM;
                  ,this.w_UGFLAG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ULTIMARC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ULTIMARC_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ULTIMARC_IDX,i_nConn)
      *
      * update ULTIMARC
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ULTIMARC')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " UGPRIMA="+cp_ToStrODBC(this.w_UGPRIMA)+;
             ",UGULTIMA="+cp_ToStrODBC(this.w_UGULTIMA)+;
             ",UGDATINI="+cp_ToStrODBC(this.w_UGDATINI)+;
             ",UGDATFIN="+cp_ToStrODBC(this.w_UGDATFIN)+;
             ",UGAUTNUM="+cp_ToStrODBC(this.w_UGAUTNUM)+;
             ",UGFLAG="+cp_ToStrODBC(this.w_UGFLAG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ULTIMARC')
        i_cWhere = cp_PKFox(i_cTable  ,'UGCODULT',this.w_UGCODULT  )
        UPDATE (i_cTable) SET;
              UGPRIMA=this.w_UGPRIMA;
             ,UGULTIMA=this.w_UGULTIMA;
             ,UGDATINI=this.w_UGDATINI;
             ,UGDATFIN=this.w_UGDATFIN;
             ,UGAUTNUM=this.w_UGAUTNUM;
             ,UGFLAG=this.w_UGFLAG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ULTIMARC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ULTIMARC_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ULTIMARC_IDX,i_nConn)
      *
      * delete ULTIMARC
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'UGCODULT',this.w_UGCODULT  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ULTIMARC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ULTIMARC_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UGCODULT
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ULTIMARC_IDX,3]
    i_lTable = "ULTIMARC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ULTIMARC_IDX,2], .t., this.ULTIMARC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ULTIMARC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UGCODULT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UGCODULT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UGCODULT";
                   +" from "+i_cTable+" "+i_lTable+" where UGCODULT="+cp_ToStrODBC(this.w_UGCODULT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UGCODULT',this.w_UGCODULT)
            select UGCODULT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UGCODULT = NVL(_Link_.UGCODULT,0)
    else
      if i_cCtrl<>'Load'
        this.w_UGCODULT = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ULTIMARC_IDX,2])+'\'+cp_ToStr(_Link_.UGCODULT,1)
      cp_ShowWarn(i_cKey,this.ULTIMARC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UGCODULT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oUGPRIMA_1_2.value==this.w_UGPRIMA)
      this.oPgFrm.Page1.oPag.oUGPRIMA_1_2.value=this.w_UGPRIMA
    endif
    if not(this.oPgFrm.Page1.oPag.oUGULTIMA_1_3.value==this.w_UGULTIMA)
      this.oPgFrm.Page1.oPag.oUGULTIMA_1_3.value=this.w_UGULTIMA
    endif
    if not(this.oPgFrm.Page1.oPag.oUGDATINI_1_4.value==this.w_UGDATINI)
      this.oPgFrm.Page1.oPag.oUGDATINI_1_4.value=this.w_UGDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oUGDATFIN_1_5.value==this.w_UGDATFIN)
      this.oPgFrm.Page1.oPag.oUGDATFIN_1_5.value=this.w_UGDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oUGAUTNUM_1_6.value==this.w_UGAUTNUM)
      this.oPgFrm.Page1.oPag.oUGAUTNUM_1_6.value=this.w_UGAUTNUM
    endif
    if not(this.oPgFrm.Page1.oPag.oUGFLAG_1_13.RadioValue()==this.w_UGFLAG)
      this.oPgFrm.Page1.oPag.oUGFLAG_1_13.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ULTIMARC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsst_astPag1 as StdContainer
  Width  = 436
  height = 175
  stdWidth  = 436
  stdheight = 175
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUGPRIMA_1_2 as StdField with uid="WDDJOZKSLU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_UGPRIMA", cQueryName = "UGPRIMA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del primo giorno elaborato per l'archivio statistico",;
    HelpContextID = 190711622,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=152, Top=40

  add object oUGULTIMA_1_3 as StdField with uid="UDVTTAFCTH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_UGULTIMA", cQueryName = "UGULTIMA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'ultimo giorno elaborato per l'archivio statistico",;
    HelpContextID = 133671033,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=271, Top=40

  add object oUGDATINI_1_4 as StdField with uid="VDUCZPAJBZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_UGDATINI", cQueryName = "UGDATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 134461553,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=152, Top=76

  add object oUGDATFIN_1_5 as StdField with uid="IKYELJJANM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_UGDATFIN", cQueryName = "UGDATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 184793196,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=271, Top=76

  add object oUGAUTNUM_1_6 as StdField with uid="DYSJILEWMP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_UGAUTNUM", cQueryName = "UGAUTNUM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 219158419,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=152, Top=148, cSayPict='"9,999,999,999"', cGetPict='"9,999,999,999"'

  add object oUGFLAG_1_13 as StdCheck with uid="RDLCDGMEVO",rtseq=7,rtrep=.f.,left=271, top=148, caption="Blocco generazione",;
    HelpContextID = 81225542,;
    cFormVar="w_UGFLAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUGFLAG_1_13.RadioValue()
    return(iif(this.value =1,'T',;
    'F'))
  endfunc
  func oUGFLAG_1_13.GetRadio()
    this.Parent.oContained.w_UGFLAG = this.RadioValue()
    return .t.
  endfunc

  func oUGFLAG_1_13.SetRadio()
    this.Parent.oContained.w_UGFLAG=trim(this.Parent.oContained.w_UGFLAG)
    this.value = ;
      iif(this.Parent.oContained.w_UGFLAG=='T',1,;
      0)
  endfunc

  add object oStr_1_7 as StdString with uid="TTJLYVUHPW",Visible=.t., Left=3, Top=40,;
    Alignment=1, Width=146, Height=15,;
    Caption="Archivio statistico da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="HCHFFZQSIJ",Visible=.t., Left=234, Top=40,;
    Alignment=1, Width=33, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="FXNFYDMMIZ",Visible=.t., Left=6, Top=76,;
    Alignment=1, Width=143, Height=15,;
    Caption="Ultima generazione da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="CELROHDRAJ",Visible=.t., Left=234, Top=76,;
    Alignment=1, Width=33, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="IJUVDAMPCA",Visible=.t., Left=7, Top=119,;
    Alignment=0, Width=419, Height=18,;
    Caption="Chiave archivio statistico"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="VRRHMGESXG",Visible=.t., Left=7, Top=11,;
    Alignment=0, Width=423, Height=18,;
    Caption="Periodi generati"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="EMHRLLYSQE",Visible=.t., Left=7, Top=148,;
    Alignment=1, Width=142, Height=15,;
    Caption="Ultima chiave:"  ;
  , bGlobalFont=.t.

  add object oBox_1_12 as StdBox with uid="UQXLZNFFOQ",left=7, top=136, width=424,height=0

  add object oBox_1_14 as StdBox with uid="FZFMRHEBTQ",left=7, top=28, width=424,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsst_ast','ULTIMARC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".UGCODULT=ULTIMARC.UGCODULT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
