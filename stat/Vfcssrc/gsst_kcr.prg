* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsst_kcr                                                        *
*              Stampa statistiche                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_660]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-31                                                      *
* Last revis.: 2013-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsst_kcr",oParentObject))

* --- Class definition
define class tgsst_kcr as StdForm
  Top    = 6
  Left   = 7

  * --- Standard Properties
  Width  = 610
  Height = 471+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-17"
  HelpContextID=99173737
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=138

  * --- Constant Properties
  _IDX = 0
  AGENTI_IDX = 0
  ART_ICOL_IDX = 0
  ATTIVITA_IDX = 0
  AZIENDA_IDX = 0
  CACOARTI_IDX = 0
  CAN_TIER_IDX = 0
  CATECOMM_IDX = 0
  CATEGOMO_IDX = 0
  CENCOST_IDX = 0
  CONTI_IDX = 0
  CRIDELAB_IDX = 0
  CRIMELAB_IDX = 0
  DES_DIVE_IDX = 0
  ESERCIZI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  MAGAZZIN_IDX = 0
  MARCHI_IDX = 0
  MASTRI_IDX = 0
  PAG_AMEN_IDX = 0
  UNIMIS_IDX = 0
  VALUTE_IDX = 0
  VETTORI_IDX = 0
  VOC_COST_IDX = 0
  ZONE_IDX = 0
  cPrg = "gsst_kcr"
  cComment = "Stampa statistiche"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CECODICE = space(10)
  o_CECODICE = space(10)
  w_CEPERIO2 = space(1)
  o_CEPERIO2 = space(1)
  w_CEPERIO3 = space(1)
  o_CEPERIO3 = space(1)
  w_DATINI1 = ctod('  /  /  ')
  o_DATINI1 = ctod('  /  /  ')
  w_DATFIN1 = ctod('  /  /  ')
  w_DATINI2 = ctod('  /  /  ')
  o_DATINI2 = ctod('  /  /  ')
  w_DATFIN2 = ctod('  /  /  ')
  w_DATINI3 = ctod('  /  /  ')
  o_DATINI3 = ctod('  /  /  ')
  w_DATFIN3 = ctod('  /  /  ')
  w_CATCOMINI = space(5)
  o_CATCOMINI = space(5)
  w_CATCOMFIN = space(5)
  w_DESCCOM = space(40)
  w_DESCCOM1 = space(40)
  w_DESCLI = space(40)
  w_DESFOR1 = space(40)
  w_DESFOR = space(40)
  w_DESCLI1 = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_TIPCLI = space(1)
  o_TIPCLI = space(1)
  w_TIPFOR = space(1)
  w_CCONDA = space(15)
  w_CCONA = space(15)
  w_MAGINI = space(5)
  o_MAGINI = space(5)
  w_MAGFIN = space(5)
  w_DESMAG = space(40)
  w_DESMAG1 = space(40)
  w_AGEINI = space(5)
  o_AGEINI = space(5)
  w_AGEFIN = space(5)
  w_DESAGE = space(40)
  w_DESAGE1 = space(40)
  w_ZONINI = space(3)
  o_ZONINI = space(3)
  w_ZONFIN = space(3)
  w_DESZON = space(40)
  w_DESZON1 = space(40)
  w_CATCONINI = space(5)
  o_CATCONINI = space(5)
  w_CATCONFIN = space(5)
  w_DESCCON = space(40)
  w_DESCCON1 = space(40)
  w_CLIFOR = space(1)
  o_CLIFOR = space(1)
  w_CODCLINI = space(15)
  o_CODCLINI = space(15)
  w_CODCLIFIN = space(15)
  o_CODCLIFIN = space(15)
  w_CODFORINI = space(15)
  o_CODFORINI = space(15)
  w_TIPORN = space(1)
  w_CODFORFIN = space(15)
  o_CODFORFIN = space(15)
  w_CODORNINI = space(15)
  o_CODORNINI = space(15)
  w_CODORNFIN = space(15)
  w_CEDESCRI = space(50)
  w_OK = space(1)
  w_DESCOMO = space(40)
  w_DESCOMO1 = space(40)
  w_DESCFAM = space(40)
  w_DESCFAM1 = space(40)
  w_CATOMINI = space(5)
  o_CATOMINI = space(5)
  w_CATOMFIN = space(5)
  w_FAMINI = space(5)
  o_FAMINI = space(5)
  w_FAMFIN = space(5)
  w_MARCINI = space(5)
  o_MARCINI = space(5)
  w_MARCFIN = space(5)
  w_DESGRUP = space(40)
  w_DESGRUP1 = space(40)
  w_GRUPMINI = space(5)
  o_GRUPMINI = space(5)
  w_GRUPMFIN = space(5)
  w_DESMARCA = space(40)
  w_DESMARCA1 = space(40)
  w_DESCART = space(40)
  w_DESCART1 = space(40)
  w_ARTINI = space(20)
  o_ARTINI = space(20)
  w_ARTFIN = space(20)
  w_TIPAGE = space(1)
  w_FORABINI = space(15)
  o_FORABINI = space(15)
  w_CODAGINI = space(5)
  o_CODAGINI = space(5)
  w_CODAGFIN = space(5)
  w_CODVEINI = space(15)
  o_CODVEINI = space(15)
  w_CODVEFIN = space(5)
  w_MASTRINI = space(15)
  o_MASTRINI = space(15)
  w_MASTRFIN = space(15)
  w_TIPART = space(2)
  w_READAZI = space(5)
  w_ESE = space(4)
  w_CODESINI = space(5)
  o_CODESINI = space(5)
  w_CODESFIN = space(5)
  w_PROVINI = space(2)
  o_PROVINI = space(2)
  w_PROVFIN = space(2)
  w_CODCEINI = space(15)
  o_CODCEINI = space(15)
  w_CODCEFIN = space(15)
  w_VOCCEINI = space(15)
  o_VOCCEINI = space(15)
  w_VOCCEFIN = space(15)
  w_COMMINI = space(15)
  o_COMMINI = space(15)
  w_COMMFIN = space(15)
  w_ATTIVINI = space(15)
  o_ATTIVINI = space(15)
  w_ATTIVFIN = space(15)
  w_PAGAMINI = space(5)
  o_PAGAMINI = space(5)
  w_PAGAMFIN = space(5)
  w_UNIMINI = space(3)
  o_UNIMINI = space(3)
  w_UNIMIFIN = space(3)
  w_VALAPP = space(3)
  w_monete = space(1)
  o_monete = space(1)
  w_divisa = space(3)
  o_divisa = space(3)
  w_CAOVAL = 0
  w_decimi = 0
  w_descr = space(35)
  w_REPORT = space(100)
  w_EXCEL = space(100)
  w_GRAFH = space(1)
  w_SIMVAL = space(5)
  w_CAMVAL = 0
  w_DESDEFIN = space(40)
  w_DESDEINI = space(40)
  w_DESSPIA1 = space(40)
  w_DESSPIA = space(40)
  w_VODESCR1 = space(40)
  w_VODESCR = space(40)
  w_COMMDES1 = space(40)
  w_COMMDES = space(40)
  w_ATTIDES1 = space(40)
  w_ATTIDES = space(40)
  w_PAGADES = space(30)
  w_PAGADES1 = space(40)
  w_DESFORAB = space(40)
  w_DESFORAB1 = space(40)
  w_FORABFIN = space(15)
  w_DESAG2 = space(35)
  w_DESAG21 = space(35)
  w_DESVET = space(35)
  w_DESVET1 = space(35)
  w_DESMAS = space(40)
  w_DESMAS1 = space(40)
  w_DATINIESE = ctod('  /  /  ')
  w_DATFINESE = ctod('  /  /  ')
  w_DESORN = space(40)
  w_DESORN1 = space(40)
  w_TIPAG1 = space(1)
  w_CODCONINI = space(15)
  w_CODCONFIN = space(15)
  w_TIPRIF = space(2)
  w_TIPCONTO = space(1)
  w_OBSOLETI = space(1)
  o_OBSOLETI = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsst_kcrPag1","gsst_kcr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsst_kcrPag2","gsst_kcr",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(3).addobject("oPag","tgsst_kcrPag3","gsst_kcr",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Altre selezioni")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCECODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[25]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='ATTIVITA'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='CACOARTI'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='CATECOMM'
    this.cWorkTables[8]='CATEGOMO'
    this.cWorkTables[9]='CENCOST'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='CRIDELAB'
    this.cWorkTables[12]='CRIMELAB'
    this.cWorkTables[13]='DES_DIVE'
    this.cWorkTables[14]='ESERCIZI'
    this.cWorkTables[15]='FAM_ARTI'
    this.cWorkTables[16]='GRUMERC'
    this.cWorkTables[17]='MAGAZZIN'
    this.cWorkTables[18]='MARCHI'
    this.cWorkTables[19]='MASTRI'
    this.cWorkTables[20]='PAG_AMEN'
    this.cWorkTables[21]='UNIMIS'
    this.cWorkTables[22]='VALUTE'
    this.cWorkTables[23]='VETTORI'
    this.cWorkTables[24]='VOC_COST'
    this.cWorkTables[25]='ZONE'
    return(this.OpenAllTables(25))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSST_BST(this,"REPO")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CECODICE=space(10)
      .w_CEPERIO2=space(1)
      .w_CEPERIO3=space(1)
      .w_DATINI1=ctod("  /  /  ")
      .w_DATFIN1=ctod("  /  /  ")
      .w_DATINI2=ctod("  /  /  ")
      .w_DATFIN2=ctod("  /  /  ")
      .w_DATINI3=ctod("  /  /  ")
      .w_DATFIN3=ctod("  /  /  ")
      .w_CATCOMINI=space(5)
      .w_CATCOMFIN=space(5)
      .w_DESCCOM=space(40)
      .w_DESCCOM1=space(40)
      .w_DESCLI=space(40)
      .w_DESFOR1=space(40)
      .w_DESFOR=space(40)
      .w_DESCLI1=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPCLI=space(1)
      .w_TIPFOR=space(1)
      .w_CCONDA=space(15)
      .w_CCONA=space(15)
      .w_MAGINI=space(5)
      .w_MAGFIN=space(5)
      .w_DESMAG=space(40)
      .w_DESMAG1=space(40)
      .w_AGEINI=space(5)
      .w_AGEFIN=space(5)
      .w_DESAGE=space(40)
      .w_DESAGE1=space(40)
      .w_ZONINI=space(3)
      .w_ZONFIN=space(3)
      .w_DESZON=space(40)
      .w_DESZON1=space(40)
      .w_CATCONINI=space(5)
      .w_CATCONFIN=space(5)
      .w_DESCCON=space(40)
      .w_DESCCON1=space(40)
      .w_CLIFOR=space(1)
      .w_CODCLINI=space(15)
      .w_CODCLIFIN=space(15)
      .w_CODFORINI=space(15)
      .w_TIPORN=space(1)
      .w_CODFORFIN=space(15)
      .w_CODORNINI=space(15)
      .w_CODORNFIN=space(15)
      .w_CEDESCRI=space(50)
      .w_OK=space(1)
      .w_DESCOMO=space(40)
      .w_DESCOMO1=space(40)
      .w_DESCFAM=space(40)
      .w_DESCFAM1=space(40)
      .w_CATOMINI=space(5)
      .w_CATOMFIN=space(5)
      .w_FAMINI=space(5)
      .w_FAMFIN=space(5)
      .w_MARCINI=space(5)
      .w_MARCFIN=space(5)
      .w_DESGRUP=space(40)
      .w_DESGRUP1=space(40)
      .w_GRUPMINI=space(5)
      .w_GRUPMFIN=space(5)
      .w_DESMARCA=space(40)
      .w_DESMARCA1=space(40)
      .w_DESCART=space(40)
      .w_DESCART1=space(40)
      .w_ARTINI=space(20)
      .w_ARTFIN=space(20)
      .w_TIPAGE=space(1)
      .w_FORABINI=space(15)
      .w_CODAGINI=space(5)
      .w_CODAGFIN=space(5)
      .w_CODVEINI=space(15)
      .w_CODVEFIN=space(5)
      .w_MASTRINI=space(15)
      .w_MASTRFIN=space(15)
      .w_TIPART=space(2)
      .w_READAZI=space(5)
      .w_ESE=space(4)
      .w_CODESINI=space(5)
      .w_CODESFIN=space(5)
      .w_PROVINI=space(2)
      .w_PROVFIN=space(2)
      .w_CODCEINI=space(15)
      .w_CODCEFIN=space(15)
      .w_VOCCEINI=space(15)
      .w_VOCCEFIN=space(15)
      .w_COMMINI=space(15)
      .w_COMMFIN=space(15)
      .w_ATTIVINI=space(15)
      .w_ATTIVFIN=space(15)
      .w_PAGAMINI=space(5)
      .w_PAGAMFIN=space(5)
      .w_UNIMINI=space(3)
      .w_UNIMIFIN=space(3)
      .w_VALAPP=space(3)
      .w_monete=space(1)
      .w_divisa=space(3)
      .w_CAOVAL=0
      .w_decimi=0
      .w_descr=space(35)
      .w_REPORT=space(100)
      .w_EXCEL=space(100)
      .w_GRAFH=space(1)
      .w_SIMVAL=space(5)
      .w_CAMVAL=0
      .w_DESDEFIN=space(40)
      .w_DESDEINI=space(40)
      .w_DESSPIA1=space(40)
      .w_DESSPIA=space(40)
      .w_VODESCR1=space(40)
      .w_VODESCR=space(40)
      .w_COMMDES1=space(40)
      .w_COMMDES=space(40)
      .w_ATTIDES1=space(40)
      .w_ATTIDES=space(40)
      .w_PAGADES=space(30)
      .w_PAGADES1=space(40)
      .w_DESFORAB=space(40)
      .w_DESFORAB1=space(40)
      .w_FORABFIN=space(15)
      .w_DESAG2=space(35)
      .w_DESAG21=space(35)
      .w_DESVET=space(35)
      .w_DESVET1=space(35)
      .w_DESMAS=space(40)
      .w_DESMAS1=space(40)
      .w_DATINIESE=ctod("  /  /  ")
      .w_DATFINESE=ctod("  /  /  ")
      .w_DESORN=space(40)
      .w_DESORN1=space(40)
      .w_TIPAG1=space(1)
      .w_CODCONINI=space(15)
      .w_CODCONFIN=space(15)
      .w_TIPRIF=space(2)
      .w_TIPCONTO=space(1)
      .w_OBSOLETI=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CECODICE))
          .link_1_1('Full')
        endif
        .w_CEPERIO2 = 'F'
        .w_CEPERIO3 = 'F'
        .w_DATINI1 = g_INIESE
        .w_DATFIN1 = g_FINESE
        .w_DATINI2 = cp_CharToDate('  -  -  ')
        .w_DATFIN2 = cp_CharToDate('  -  -  ')
        .w_DATINI3 = cp_CharToDate('  -  -  ')
        .w_DATFIN3 = cp_CharToDate('  -  -  ')
        .w_CATCOMINI = SPACE(5)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CATCOMINI))
          .link_1_10('Full')
        endif
        .w_CATCOMFIN = .w_CATCOMINI
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CATCOMFIN))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,18,.f.)
        .w_OBTEST = i_datsys
        .w_TIPCLI = 'C'
        .w_TIPFOR = 'F'
        .w_CCONDA = iif(.w_CLIFOR= 'C',.w_CODCLINI,.w_CODFORINI)
        .w_CCONA = iif(.w_CLIFOR= 'C',.w_CODCLIFIN,.w_CODFORFIN)
        .w_MAGINI = SPACE(5)
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_MAGINI))
          .link_1_32('Full')
        endif
        .w_MAGFIN = .w_MAGINI
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_MAGFIN))
          .link_1_33('Full')
        endif
          .DoRTCalc(26,27,.f.)
        .w_AGEINI = SPACE(5)
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_AGEINI))
          .link_1_38('Full')
        endif
        .w_AGEFIN = .w_AGEINI
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_AGEFIN))
          .link_1_39('Full')
        endif
          .DoRTCalc(30,31,.f.)
        .w_ZONINI = SPACE(3)
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_ZONINI))
          .link_1_44('Full')
        endif
        .w_ZONFIN = .w_ZONINI
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_ZONFIN))
          .link_1_45('Full')
        endif
          .DoRTCalc(34,35,.f.)
        .w_CATCONINI = SPACE(5)
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_CATCONINI))
          .link_1_50('Full')
        endif
        .w_CATCONFIN = .w_CATCONINI
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_CATCONFIN))
          .link_1_51('Full')
        endif
          .DoRTCalc(38,39,.f.)
        .w_CLIFOR = 'N'
        .w_CODCLINI = SPACE(15)
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_CODCLINI))
          .link_1_60('Full')
        endif
        .w_CODCLIFIN = IIF (EMPTY(.w_CODCLINI),.w_CODCLINI, IIF(EMPTY(.w_CODCLIFIN), .w_CODCLINI, .w_CODCLIFIN))
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_CODCLIFIN))
          .link_1_61('Full')
        endif
        .w_CODFORINI = SPACE(15)
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_CODFORINI))
          .link_1_62('Full')
        endif
        .w_TIPORN = IIF(.w_CLIFOR='C','C',IIF(.w_CLIFOR='F','F','G'))
        .w_CODFORFIN = IIF(EMPTY(.w_CODFORINI),.w_CODFORINI, IIF(EMPTY(.w_CODFORFIN), .w_CODFORINI, .w_CODFORFIN))
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_CODFORFIN))
          .link_1_64('Full')
        endif
        .w_CODORNINI = SPACE(15)
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_CODORNINI))
          .link_1_65('Full')
        endif
        .w_CODORNFIN = .w_CODORNINI
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_CODORNFIN))
          .link_1_66('Full')
        endif
          .DoRTCalc(48,48,.f.)
        .w_OK = 'T'
          .DoRTCalc(50,53,.f.)
        .w_CATOMINI = SPACE(5)
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_CATOMINI))
          .link_2_9('Full')
        endif
        .w_CATOMFIN = .w_CATOMINI
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_CATOMFIN))
          .link_2_10('Full')
        endif
        .w_FAMINI = SPACE(5)
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_FAMINI))
          .link_2_11('Full')
        endif
        .w_FAMFIN = .w_FAMINI
        .DoRTCalc(57,57,.f.)
        if not(empty(.w_FAMFIN))
          .link_2_12('Full')
        endif
        .w_MARCINI = SPACE(5)
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_MARCINI))
          .link_2_13('Full')
        endif
        .w_MARCFIN = .w_MARCINI
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_MARCFIN))
          .link_2_14('Full')
        endif
          .DoRTCalc(60,61,.f.)
        .w_GRUPMINI = SPACE(5)
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_GRUPMINI))
          .link_2_19('Full')
        endif
        .w_GRUPMFIN = .w_GRUPMINI
        .DoRTCalc(63,63,.f.)
        if not(empty(.w_GRUPMFIN))
          .link_2_20('Full')
        endif
          .DoRTCalc(64,67,.f.)
        .w_ARTINI = SPACE(20)
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_ARTINI))
          .link_2_29('Full')
        endif
        .w_ARTFIN = .w_ARTINI
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_ARTFIN))
          .link_2_30('Full')
        endif
          .DoRTCalc(70,70,.f.)
        .w_FORABINI = SPACE(15)
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_FORABINI))
          .link_2_32('Full')
        endif
        .w_CODAGINI = SPACE(5)
        .DoRTCalc(72,72,.f.)
        if not(empty(.w_CODAGINI))
          .link_2_33('Full')
        endif
        .w_CODAGFIN = .w_CODAGINI
        .DoRTCalc(73,73,.f.)
        if not(empty(.w_CODAGFIN))
          .link_2_34('Full')
        endif
        .w_CODVEINI = SPACE(5)
        .DoRTCalc(74,74,.f.)
        if not(empty(.w_CODVEINI))
          .link_2_35('Full')
        endif
        .w_CODVEFIN = .w_CODVEINI
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_CODVEFIN))
          .link_2_36('Full')
        endif
        .w_MASTRINI = SPACE(15)
        .DoRTCalc(76,76,.f.)
        if not(empty(.w_MASTRINI))
          .link_2_37('Full')
        endif
        .w_MASTRFIN = .w_MASTRINI
        .DoRTCalc(77,77,.f.)
        if not(empty(.w_MASTRFIN))
          .link_2_38('Full')
        endif
          .DoRTCalc(78,78,.f.)
        .w_READAZI = i_codazi
        .DoRTCalc(79,79,.f.)
        if not(empty(.w_READAZI))
          .link_3_1('Full')
        endif
        .w_ESE = g_codese
        .DoRTCalc(80,80,.f.)
        if not(empty(.w_ESE))
          .link_3_2('Full')
        endif
        .w_CODESINI = SPACE(5)
        .DoRTCalc(81,81,.f.)
        if not(empty(.w_CODESINI))
          .link_3_3('Full')
        endif
        .w_CODESFIN = .w_CODESINI
        .DoRTCalc(82,82,.f.)
        if not(empty(.w_CODESFIN))
          .link_3_4('Full')
        endif
        .w_PROVINI = SPACE(2)
        .w_PROVFIN = .w_PROVINI
        .w_CODCEINI = SPACE(15)
        .DoRTCalc(85,85,.f.)
        if not(empty(.w_CODCEINI))
          .link_3_7('Full')
        endif
        .w_CODCEFIN = .w_CODCEINI
        .DoRTCalc(86,86,.f.)
        if not(empty(.w_CODCEFIN))
          .link_3_8('Full')
        endif
        .w_VOCCEINI = SPACE(15)
        .DoRTCalc(87,87,.f.)
        if not(empty(.w_VOCCEINI))
          .link_3_9('Full')
        endif
        .w_VOCCEFIN = .w_VOCCEINI
        .DoRTCalc(88,88,.f.)
        if not(empty(.w_VOCCEFIN))
          .link_3_10('Full')
        endif
        .w_COMMINI = SPACE(15)
        .DoRTCalc(89,89,.f.)
        if not(empty(.w_COMMINI))
          .link_3_11('Full')
        endif
        .w_COMMFIN = .w_COMMINI
        .DoRTCalc(90,90,.f.)
        if not(empty(.w_COMMFIN))
          .link_3_12('Full')
        endif
        .w_ATTIVINI = SPACE(15)
        .DoRTCalc(91,91,.f.)
        if not(empty(.w_ATTIVINI))
          .link_3_13('Full')
        endif
        .w_ATTIVFIN = .w_ATTIVINI
        .DoRTCalc(92,92,.f.)
        if not(empty(.w_ATTIVFIN))
          .link_3_14('Full')
        endif
        .w_PAGAMINI = SPACE(15)
        .DoRTCalc(93,93,.f.)
        if not(empty(.w_PAGAMINI))
          .link_3_15('Full')
        endif
        .w_PAGAMFIN = .w_PAGAMINI
        .DoRTCalc(94,94,.f.)
        if not(empty(.w_PAGAMFIN))
          .link_3_16('Full')
        endif
        .w_UNIMINI = SPACE(3)
        .DoRTCalc(95,95,.f.)
        if not(empty(.w_UNIMINI))
          .link_3_17('Full')
        endif
        .w_UNIMIFIN = .w_UNIMINI
        .DoRTCalc(96,96,.f.)
        if not(empty(.w_UNIMIFIN))
          .link_3_18('Full')
        endif
        .w_VALAPP = g_perval
        .w_monete = 'C'
        .w_divisa = IIF(.w_monete='C',.w_valapp,.w_divisa)
        .DoRTCalc(99,99,.f.)
        if not(empty(.w_divisa))
          .link_3_21('Full')
        endif
        .w_CAOVAL = GETCAM(g_PERVAL,i_DATSYS)
          .DoRTCalc(101,105,.f.)
        .w_SIMVAL = .w_SIMVAL
        .w_CAMVAL = GETCAM(.w_DIVISA, i_DATSYS, 7)
          .DoRTCalc(108,121,.f.)
        .w_FORABFIN = .w_FORABINI
        .DoRTCalc(122,122,.f.)
        if not(empty(.w_FORABFIN))
          .link_2_42('Full')
        endif
          .DoRTCalc(123,133,.f.)
        .w_CODCONINI = IIF(.w_TIPORN='F', .w_CODFORINI, IIF(.w_TIPORN='C', .w_CODCLINI,' '))
        .w_CODCONFIN = IIF(.w_TIPORN='F', .w_CODFORFIN, IIF(.w_TIPORN='C', .w_CODCLIFIN,' '))
          .DoRTCalc(136,136,.f.)
        .w_TIPCONTO = IIF(.w_CLIFOR $ 'F-C',.w_CLIFOR,' ')
        .w_OBSOLETI = 'T'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page3.oPag.oBtn_3_30.enabled = this.oPgFrm.Page3.oPag.oBtn_3_30.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_31.enabled = this.oPgFrm.Page3.oPag.oBtn_3_31.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_32.enabled = this.oPgFrm.Page3.oPag.oBtn_3_32.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_33.enabled = this.oPgFrm.Page3.oPag.oBtn_3_33.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_CEPERIO2 = 'F'
        endif
        if .o_CEPERIO2<>.w_CEPERIO2
            .w_CEPERIO3 = 'F'
        endif
        .DoRTCalc(4,4,.t.)
        if .o_DATINI1<>.w_DATINI1.or. .o_CECODICE<>.w_CECODICE
            .w_DATFIN1 = g_FINESE
        endif
        if .o_CEPERIO2<>.w_CEPERIO2
            .w_DATINI2 = cp_CharToDate('  -  -  ')
        endif
        if .o_DATINI2<>.w_DATINI2.or. .o_CECODICE<>.w_CECODICE
            .w_DATFIN2 = cp_CharToDate('  -  -  ')
        endif
        if .o_CEPERIO3<>.w_CEPERIO3
            .w_DATINI3 = cp_CharToDate('  -  -  ')
        endif
        if .o_DATINI3<>.w_DATINI3.or. .o_CECODICE<>.w_CECODICE
            .w_DATFIN3 = cp_CharToDate('  -  -  ')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_CATCOMINI = SPACE(5)
          .link_1_10('Full')
        endif
        if .o_CATCOMINI<>.w_CATCOMINI
            .w_CATCOMFIN = .w_CATCOMINI
          .link_1_11('Full')
        endif
        .DoRTCalc(12,21,.t.)
            .w_CCONDA = iif(.w_CLIFOR= 'C',.w_CODCLINI,.w_CODFORINI)
            .w_CCONA = iif(.w_CLIFOR= 'C',.w_CODCLIFIN,.w_CODFORFIN)
        if .o_CECODICE<>.w_CECODICE
            .w_MAGINI = SPACE(5)
          .link_1_32('Full')
        endif
        if .o_MAGINI<>.w_MAGINI
            .w_MAGFIN = .w_MAGINI
          .link_1_33('Full')
        endif
        .DoRTCalc(26,27,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_AGEINI = SPACE(5)
          .link_1_38('Full')
        endif
        if .o_AGEINI<>.w_AGEINI
            .w_AGEFIN = .w_AGEINI
          .link_1_39('Full')
        endif
        .DoRTCalc(30,31,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_ZONINI = SPACE(3)
          .link_1_44('Full')
        endif
        if .o_ZONINI<>.w_ZONINI
            .w_ZONFIN = .w_ZONINI
          .link_1_45('Full')
        endif
        .DoRTCalc(34,35,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_CATCONINI = SPACE(5)
          .link_1_50('Full')
        endif
        if .o_CATCONINI<>.w_CATCONINI
            .w_CATCONFIN = .w_CATCONINI
          .link_1_51('Full')
        endif
        .DoRTCalc(38,39,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_CLIFOR = 'N'
        endif
        if .o_CECODICE<>.w_CECODICE.or. .o_CLIFOR<>.w_CLIFOR
            .w_CODCLINI = SPACE(15)
          .link_1_60('Full')
        endif
        if .o_CODCLINI<>.w_CODCLINI.or. .o_CLIFOR<>.w_CLIFOR.or. .o_CECODICE<>.w_CECODICE
            .w_CODCLIFIN = IIF (EMPTY(.w_CODCLINI),.w_CODCLINI, IIF(EMPTY(.w_CODCLIFIN), .w_CODCLINI, .w_CODCLIFIN))
          .link_1_61('Full')
        endif
        if .o_CECODICE<>.w_CECODICE.or. .o_CLIFOR<>.w_CLIFOR
            .w_CODFORINI = SPACE(15)
          .link_1_62('Full')
        endif
        if .o_CLIFOR<>.w_CLIFOR
            .w_TIPORN = IIF(.w_CLIFOR='C','C',IIF(.w_CLIFOR='F','F','G'))
        endif
        if .o_CODFORINI<>.w_CODFORINI.or. .o_CLIFOR<>.w_CLIFOR.or. .o_CECODICE<>.w_CECODICE
            .w_CODFORFIN = IIF(EMPTY(.w_CODFORINI),.w_CODFORINI, IIF(EMPTY(.w_CODFORFIN), .w_CODFORINI, .w_CODFORFIN))
          .link_1_64('Full')
        endif
        if .o_CECODICE<>.w_CECODICE.or. .o_CLIFOR<>.w_CLIFOR
            .w_CODORNINI = SPACE(15)
          .link_1_65('Full')
        endif
        if .o_CODORNINI<>.w_CODORNINI.or. .o_CLIFOR<>.w_CLIFOR.or. .o_CECODICE<>.w_CECODICE
            .w_CODORNFIN = .w_CODORNINI
          .link_1_66('Full')
        endif
        .DoRTCalc(48,53,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_CATOMINI = SPACE(5)
          .link_2_9('Full')
        endif
        if .o_CATOMINI<>.w_CATOMINI
            .w_CATOMFIN = .w_CATOMINI
          .link_2_10('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_FAMINI = SPACE(5)
          .link_2_11('Full')
        endif
        if .o_FAMINI<>.w_FAMINI
            .w_FAMFIN = .w_FAMINI
          .link_2_12('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_MARCINI = SPACE(5)
          .link_2_13('Full')
        endif
        if .o_MARCINI<>.w_MARCINI
            .w_MARCFIN = .w_MARCINI
          .link_2_14('Full')
        endif
        .DoRTCalc(60,61,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_GRUPMINI = SPACE(5)
          .link_2_19('Full')
        endif
        if .o_GRUPMINI<>.w_GRUPMINI
            .w_GRUPMFIN = .w_GRUPMINI
          .link_2_20('Full')
        endif
        .DoRTCalc(64,67,.t.)
        if .o_CECODICE<>.w_CECODICE.or. .o_OBSOLETI<>.w_OBSOLETI
            .w_ARTINI = SPACE(20)
          .link_2_29('Full')
        endif
        if .o_ARTINI<>.w_ARTINI.or. .o_OBSOLETI<>.w_OBSOLETI
            .w_ARTFIN = .w_ARTINI
          .link_2_30('Full')
        endif
        .DoRTCalc(70,70,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_FORABINI = SPACE(15)
          .link_2_32('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_CODAGINI = SPACE(5)
          .link_2_33('Full')
        endif
        if .o_CODAGINI<>.w_CODAGINI.or. .o_CECODICE<>.w_CECODICE
            .w_CODAGFIN = .w_CODAGINI
          .link_2_34('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_CODVEINI = SPACE(5)
          .link_2_35('Full')
        endif
        if .o_CODVEINI<>.w_CODVEINI.or. .o_CECODICE<>.w_CECODICE
            .w_CODVEFIN = .w_CODVEINI
          .link_2_36('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_MASTRINI = SPACE(15)
          .link_2_37('Full')
        endif
        if .o_MASTRINI<>.w_MASTRINI.or. .o_CECODICE<>.w_CECODICE
            .w_MASTRFIN = .w_MASTRINI
          .link_2_38('Full')
        endif
        .DoRTCalc(78,78,.t.)
          .link_3_1('Full')
            .w_ESE = g_codese
          .link_3_2('Full')
        if .o_CECODICE<>.w_CECODICE.or. .o_CODCLINI<>.w_CODCLINI.or. .o_CODCLIFIN<>.w_CODCLIFIN.or. .o_TIPCLI<>.w_TIPCLI.or. .o_CODFORINI<>.w_CODFORINI.or. .o_CODFORFIN<>.w_CODFORFIN
            .w_CODESINI = SPACE(5)
          .link_3_3('Full')
        endif
        if .o_CODESINI<>.w_CODESINI.or. .o_CECODICE<>.w_CECODICE.or. .o_CODCLINI<>.w_CODCLINI.or. .o_CODCLIFIN<>.w_CODCLIFIN.or. .o_CODFORINI<>.w_CODFORINI.or. .o_CODFORFIN<>.w_CODFORFIN
            .w_CODESFIN = .w_CODESINI
          .link_3_4('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_PROVINI = SPACE(2)
        endif
        if .o_PROVINI<>.w_PROVINI.or. .o_CECODICE<>.w_CECODICE
            .w_PROVFIN = .w_PROVINI
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_CODCEINI = SPACE(15)
          .link_3_7('Full')
        endif
        if .o_CODCEINI<>.w_CODCEINI.or. .o_CECODICE<>.w_CECODICE
            .w_CODCEFIN = .w_CODCEINI
          .link_3_8('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_VOCCEINI = SPACE(15)
          .link_3_9('Full')
        endif
        if .o_VOCCEINI<>.w_VOCCEINI.or. .o_CECODICE<>.w_CECODICE
            .w_VOCCEFIN = .w_VOCCEINI
          .link_3_10('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_COMMINI = SPACE(15)
          .link_3_11('Full')
        endif
        if .o_COMMINI<>.w_COMMINI.or. .o_CECODICE<>.w_CECODICE
            .w_COMMFIN = .w_COMMINI
          .link_3_12('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_ATTIVINI = SPACE(15)
          .link_3_13('Full')
        endif
        if .o_ATTIVINI<>.w_ATTIVINI.or. .o_CECODICE<>.w_CECODICE
            .w_ATTIVFIN = .w_ATTIVINI
          .link_3_14('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_PAGAMINI = SPACE(15)
          .link_3_15('Full')
        endif
        if .o_PAGAMINI<>.w_PAGAMINI.or. .o_CECODICE<>.w_CECODICE
            .w_PAGAMFIN = .w_PAGAMINI
          .link_3_16('Full')
        endif
        if .o_CECODICE<>.w_CECODICE
            .w_UNIMINI = SPACE(3)
          .link_3_17('Full')
        endif
        if .o_UNIMINI<>.w_UNIMINI.or. .o_CECODICE<>.w_CECODICE
            .w_UNIMIFIN = .w_UNIMINI
          .link_3_18('Full')
        endif
        .DoRTCalc(97,97,.t.)
        if .o_CECODICE<>.w_CECODICE
            .w_monete = 'C'
        endif
        if .o_MONETE<>.w_MONETE
            .w_divisa = IIF(.w_monete='C',.w_valapp,.w_divisa)
          .link_3_21('Full')
        endif
            .w_CAOVAL = GETCAM(g_PERVAL,i_DATSYS)
        .DoRTCalc(101,105,.t.)
            .w_SIMVAL = .w_SIMVAL
        if .o_DIVISA<>.w_DIVISA
            .w_CAMVAL = GETCAM(.w_DIVISA, i_DATSYS, 7)
        endif
        .DoRTCalc(108,121,.t.)
        if .o_FORABINI<>.w_FORABINI.or. .o_CECODICE<>.w_CECODICE
            .w_FORABFIN = .w_FORABINI
          .link_2_42('Full')
        endif
        .DoRTCalc(123,133,.t.)
        if .o_CLIFOR<>.w_CLIFOR.or. .o_CODFORINI<>.w_CODFORINI.or. .o_CODCLINI<>.w_CODCLINI.or. .o_CODFORFIN<>.w_CODFORFIN.or. .o_CODCLIFIN<>.w_CODCLIFIN
            .w_CODCONINI = IIF(.w_TIPORN='F', .w_CODFORINI, IIF(.w_TIPORN='C', .w_CODCLINI,' '))
        endif
        if .o_CLIFOR<>.w_CLIFOR.or. .o_CODFORINI<>.w_CODFORINI.or. .o_CODCLINI<>.w_CODCLINI.or. .o_CODFORFIN<>.w_CODFORFIN.or. .o_CODCLIFIN<>.w_CODCLIFIN
            .w_CODCONFIN = IIF(.w_TIPORN='F', .w_CODFORFIN, IIF(.w_TIPORN='C', .w_CODCLIFIN,' '))
        endif
        .DoRTCalc(136,136,.t.)
        if .o_CLIFOR<>.w_CLIFOR
            .w_TIPCONTO = IIF(.w_CLIFOR $ 'F-C',.w_CLIFOR,' ')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(138,138,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCEPERIO2_1_2.enabled = this.oPgFrm.Page1.oPag.oCEPERIO2_1_2.mCond()
    this.oPgFrm.Page1.oPag.oCEPERIO3_1_3.enabled = this.oPgFrm.Page1.oPag.oCEPERIO3_1_3.mCond()
    this.oPgFrm.Page3.oPag.oCODESINI_3_3.enabled = this.oPgFrm.Page3.oPag.oCODESINI_3_3.mCond()
    this.oPgFrm.Page3.oPag.oCODESFIN_3_4.enabled = this.oPgFrm.Page3.oPag.oCODESFIN_3_4.mCond()
    this.oPgFrm.Page3.oPag.oCODCEINI_3_7.enabled = this.oPgFrm.Page3.oPag.oCODCEINI_3_7.mCond()
    this.oPgFrm.Page3.oPag.oCODCEFIN_3_8.enabled = this.oPgFrm.Page3.oPag.oCODCEFIN_3_8.mCond()
    this.oPgFrm.Page3.oPag.oVOCCEINI_3_9.enabled = this.oPgFrm.Page3.oPag.oVOCCEINI_3_9.mCond()
    this.oPgFrm.Page3.oPag.oVOCCEFIN_3_10.enabled = this.oPgFrm.Page3.oPag.oVOCCEFIN_3_10.mCond()
    this.oPgFrm.Page3.oPag.oCOMMINI_3_11.enabled = this.oPgFrm.Page3.oPag.oCOMMINI_3_11.mCond()
    this.oPgFrm.Page3.oPag.oCOMMFIN_3_12.enabled = this.oPgFrm.Page3.oPag.oCOMMFIN_3_12.mCond()
    this.oPgFrm.Page3.oPag.oATTIVINI_3_13.enabled = this.oPgFrm.Page3.oPag.oATTIVINI_3_13.mCond()
    this.oPgFrm.Page3.oPag.oATTIVFIN_3_14.enabled = this.oPgFrm.Page3.oPag.oATTIVFIN_3_14.mCond()
    this.oPgFrm.Page3.oPag.odivisa_3_21.enabled = this.oPgFrm.Page3.oPag.odivisa_3_21.mCond()
    this.oPgFrm.Page3.oPag.oCAMVAL_3_29.enabled = this.oPgFrm.Page3.oPag.oCAMVAL_3_29.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_32.enabled = this.oPgFrm.Page3.oPag.oBtn_3_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATINI2_1_6.visible=!this.oPgFrm.Page1.oPag.oDATINI2_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDATFIN2_1_7.visible=!this.oPgFrm.Page1.oPag.oDATFIN2_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDATINI3_1_8.visible=!this.oPgFrm.Page1.oPag.oDATINI3_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDATFIN3_1_9.visible=!this.oPgFrm.Page1.oPag.oDATFIN3_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oDESCLI_1_18.visible=!this.oPgFrm.Page1.oPag.oDESCLI_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDESFOR1_1_19.visible=!this.oPgFrm.Page1.oPag.oDESFOR1_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oDESFOR_1_22.visible=!this.oPgFrm.Page1.oPag.oDESFOR_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDESCLI1_1_23.visible=!this.oPgFrm.Page1.oPag.oDESCLI1_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oCODCLINI_1_60.visible=!this.oPgFrm.Page1.oPag.oCODCLINI_1_60.mHide()
    this.oPgFrm.Page1.oPag.oCODCLIFIN_1_61.visible=!this.oPgFrm.Page1.oPag.oCODCLIFIN_1_61.mHide()
    this.oPgFrm.Page1.oPag.oCODFORINI_1_62.visible=!this.oPgFrm.Page1.oPag.oCODFORINI_1_62.mHide()
    this.oPgFrm.Page1.oPag.oCODFORFIN_1_64.visible=!this.oPgFrm.Page1.oPag.oCODFORFIN_1_64.mHide()
    this.oPgFrm.Page1.oPag.oCODORNINI_1_65.visible=!this.oPgFrm.Page1.oPag.oCODORNINI_1_65.mHide()
    this.oPgFrm.Page1.oPag.oCODORNFIN_1_66.visible=!this.oPgFrm.Page1.oPag.oCODORNFIN_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_30.visible=!this.oPgFrm.Page3.oPag.oBtn_3_30.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_31.visible=!this.oPgFrm.Page3.oPag.oBtn_3_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.oPgFrm.Page1.oPag.oDESORN_1_75.visible=!this.oPgFrm.Page1.oPag.oDESORN_1_75.mHide()
    this.oPgFrm.Page1.oPag.oDESORN1_1_76.visible=!this.oPgFrm.Page1.oPag.oDESORN1_1_76.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CECODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CRIMELAB_IDX,3]
    i_lTable = "CRIMELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CRIMELAB_IDX,2], .t., this.CRIMELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CRIMELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CECODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSST_MCE',True,'CRIMELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_CECODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_CECODICE))
          select CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CECODICE)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStrODBC(trim(this.w_CECODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESCRI like "+cp_ToStr(trim(this.w_CECODICE)+"%");

            select CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CECODICE) and !this.bDontReportError
            deferred_cp_zoom('CRIMELAB','*','CECODICE',cp_AbsName(oSource.parent,'oCECODICE_1_1'),i_cWhere,'GSST_MCE',"Criteri di elaborazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CECODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_CECODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_CECODICE)
            select CECODICE,CEDESCRI,CENOMREP,CENOMXLT,CEFLGRAF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CECODICE = NVL(_Link_.CECODICE,space(10))
      this.w_CEDESCRI = NVL(_Link_.CEDESCRI,space(50))
      this.w_REPORT = NVL(_Link_.CENOMREP,space(100))
      this.w_EXCEL = NVL(_Link_.CENOMXLT,space(100))
      this.w_GRAFH = NVL(_Link_.CEFLGRAF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CECODICE = space(10)
      endif
      this.w_CEDESCRI = space(50)
      this.w_REPORT = space(100)
      this.w_EXCEL = space(100)
      this.w_GRAFH = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CRIMELAB_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CRIMELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CECODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOMINI
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOMINI))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOMINI)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStrODBC(trim(this.w_CATCOMINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStr(trim(this.w_CATCOMINI)+"%");

            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATCOMINI) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOMINI_1_10'),i_cWhere,'GSAR_ACT',"Categoria commerciale",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOMINI)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOMINI = NVL(_Link_.CTCODICE,space(5))
      this.w_DESCCOM = NVL(_Link_.CTDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOMINI = space(5)
      endif
      this.w_DESCCOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOMFIN
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOMFIN))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOMFIN)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStrODBC(trim(this.w_CATCOMFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStr(trim(this.w_CATCOMFIN)+"%");

            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATCOMFIN) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOMFIN_1_11'),i_cWhere,'GSAR_ACT',"Categoria commerciale",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOMFIN)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOMFIN = NVL(_Link_.CTCODICE,space(5))
      this.w_DESCCOM1 = NVL(_Link_.CTDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOMFIN = space(5)
      endif
      this.w_DESCCOM1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATCOMINI <= .w_CATCOMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cat. commerciale inferiore alla selezione precedente")
        endif
        this.w_CATCOMFIN = space(5)
        this.w_DESCCOM1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MAGINI)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_1_32'),i_cWhere,'GSAR_AMA',"Magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAG = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MAGFIN)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_1_33'),i_cWhere,'GSAR_AMA',"Magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG1 = NVL(_Link_.MGDESMAG,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAG1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGFIN <= .w_MAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inferiore alla selezione precedente")
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAG1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGEINI
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGEINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGEINI)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGEINI))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGEINI)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGEINI)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGEINI)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGEINI) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGEINI_1_38'),i_cWhere,'GSAR_AGE',"Agenti",'Gsst_age.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGEINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGEINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGEINI)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGEINI = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_TIPAG1 = NVL(_Link_.AGTIPAGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AGEINI = space(5)
      endif
      this.w_DESAGE = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPAG1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente obsoleto")
        endif
        this.w_AGEINI = space(5)
        this.w_DESAGE = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPAG1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGEINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGEFIN
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGEFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGEFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGEFIN))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGEFIN)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGEFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGEFIN)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGEFIN) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGEFIN_1_39'),i_cWhere,'GSAR_AGE',"Agenti",'Gsst_age.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGEFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGEFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGEFIN)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGEFIN = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE1 = NVL(_Link_.AGDESAGE,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_TIPAG1 = NVL(_Link_.AGTIPAGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AGEFIN = space(5)
      endif
      this.w_DESAGE1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPAG1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_AGEINI <= .w_AGEFIN) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente inferiore alla selezione precedente")
        endif
        this.w_AGEFIN = space(5)
        this.w_DESAGE1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPAG1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGEFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZONINI
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZONINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_ZONINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_ZONINI))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZONINI)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStrODBC(trim(this.w_ZONINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStr(trim(this.w_ZONINI)+"%");

            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ZONINI) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oZONINI_1_44'),i_cWhere,'GSAR_AZO',"Zona",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZONINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_ZONINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_ZONINI)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZONINI = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ZONINI = space(3)
      endif
      this.w_DESZON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ZONINI = space(3)
        this.w_DESZON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZONINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZONFIN
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZONFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_ZONFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_ZONFIN))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZONFIN)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStrODBC(trim(this.w_ZONFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStr(trim(this.w_ZONFIN)+"%");

            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ZONFIN) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oZONFIN_1_45'),i_cWhere,'GSAR_AZO',"Zona",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZONFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_ZONFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_ZONFIN)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZONFIN = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON1 = NVL(_Link_.ZODESZON,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ZONFIN = space(3)
      endif
      this.w_DESZON1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_ZONINI <= .w_ZONFIN) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Zona inferiore alla selezione precedente")
        endif
        this.w_ZONFIN = space(3)
        this.w_DESZON1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZONFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCONINI
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCONINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_CATCONINI)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_CATCONINI))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCONINI)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" C1DESCRI like "+cp_ToStrODBC(trim(this.w_CATCONINI)+"%");

            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" C1DESCRI like "+cp_ToStr(trim(this.w_CATCONINI)+"%");

            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATCONINI) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oCATCONINI_1_50'),i_cWhere,'GSAR_AC1',"Categoria contabile",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCONINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_CATCONINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_CATCONINI)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCONINI = NVL(_Link_.C1CODICE,space(5))
      this.w_DESCCON = NVL(_Link_.C1DESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATCONINI = space(5)
      endif
      this.w_DESCCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCONINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCONFIN
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCONFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_CATCONFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_CATCONFIN))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCONFIN)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" C1DESCRI like "+cp_ToStrODBC(trim(this.w_CATCONFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" C1DESCRI like "+cp_ToStr(trim(this.w_CATCONFIN)+"%");

            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATCONFIN) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oCATCONFIN_1_51'),i_cWhere,'GSAR_AC1',"Categoria contabile",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCONFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_CATCONFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_CATCONFIN)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCONFIN = NVL(_Link_.C1CODICE,space(5))
      this.w_DESCCON1 = NVL(_Link_.C1DESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATCONFIN = space(5)
      endif
      this.w_DESCCON1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATCONINI <= .w_CATCONFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cat. contabile inferiore alla selezione precedente")
        endif
        this.w_CATCONFIN = space(5)
        this.w_DESCCON1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCONFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLINI
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLI;
                     ,'ANCODICE',trim(this.w_CODCLINI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCLINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLI);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLINI_1_60'),i_cWhere,'GSAR_BZC',"Cliente",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il cliente iniziale � maggiore di quello finale oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLI;
                       ,'ANCODICE',this.w_CODCLINI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLINI = space(15)
      endif
      this.w_DESCLI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODCLIFIN)) OR  (.w_CODCLINI <= .w_CODCLIFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il cliente iniziale � maggiore di quello finale oppure inesistente")
        endif
        this.w_CODCLINI = space(15)
        this.w_DESCLI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLIFIN
  func Link_1_61(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLIFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLIFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLI;
                     ,'ANCODICE',trim(this.w_CODCLIFIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLIFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLIFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCLIFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLI);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLIFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLIFIN_1_61'),i_cWhere,'GSAR_BZC',"Cliente",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il cliente finale � minore di quello iniziale oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLIFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLIFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLI;
                       ,'ANCODICE',this.w_CODCLIFIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLIFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLIFIN = space(15)
      endif
      this.w_DESCLI1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODCLINI <= .w_CODCLIFIN) OR(empty(.w_CODCLIFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il cliente finale � minore di quello iniziale oppure inesistente")
        endif
        this.w_CODCLIFIN = space(15)
        this.w_DESCLI1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLIFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFORINI
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFORINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFORINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPFOR;
                     ,'ANCODICE',trim(this.w_CODFORINI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFORINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFORINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFORINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPFOR);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFORINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFORINI_1_62'),i_cWhere,'GSAR_BZC',"Fornitore",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il fornitore iniziale � maggiore di quello finale oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFORINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFORINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPFOR;
                       ,'ANCODICE',this.w_CODFORINI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFORINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFORINI = space(15)
      endif
      this.w_DESFOR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODFORFIN)) OR  (.w_CODFORINI <= .w_CODFORFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il fornitore iniziale � maggiore di quello finale oppure inesistente")
        endif
        this.w_CODFORINI = space(15)
        this.w_DESFOR = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFORINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFORFIN
  func Link_1_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFORFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFORFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPFOR;
                     ,'ANCODICE',trim(this.w_CODFORFIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFORFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFORFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFORFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPFOR);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFORFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFORFIN_1_64'),i_cWhere,'GSAR_BZC',"Fornitore",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il fornitore finale � minore di quello iniziale oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFORFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFORFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPFOR;
                       ,'ANCODICE',this.w_CODFORFIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFORFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFORFIN = space(15)
      endif
      this.w_DESFOR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODFORINI <= .w_CODFORFIN) OR(empty(.w_CODFORFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il fornitore finale � minore di quello iniziale oppure inesistente")
        endif
        this.w_CODFORFIN = space(15)
        this.w_DESFOR1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFORFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODORNINI
  func Link_1_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODORNINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODORNINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPORN);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPORN;
                     ,'ANCODICE',trim(this.w_CODORNINI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODORNINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODORNINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPORN);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODORNINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPORN);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODORNINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODORNINI_1_65'),i_cWhere,'GSAR_BZC',"Per conto di",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPORN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODORNINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODORNINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPORN;
                       ,'ANCODICE',this.w_CODORNINI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODORNINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESORN = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODORNINI = space(15)
      endif
      this.w_DESORN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODORNINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODORNFIN
  func Link_1_66(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODORNFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODORNFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPORN);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPORN;
                     ,'ANCODICE',trim(this.w_CODORNFIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODORNFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODORNFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPORN);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODORNFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPORN);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODORNFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODORNFIN_1_66'),i_cWhere,'GSAR_BZC',"Per conto di",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPORN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Selezione inferiore al per conto di precedente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODORNFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODORNFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPORN;
                       ,'ANCODICE',this.w_CODORNFIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODORNFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESORN1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODORNFIN = space(15)
      endif
      this.w_DESORN1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODORNINI <= .w_CODORNFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezione inferiore al per conto di precedente")
        endif
        this.w_CODORNFIN = space(15)
        this.w_DESORN1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODORNFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOMINI
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOMINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOMINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( OMDESCRI like "+cp_ToStrODBC(trim(this.w_CATOMINI)+"%")+cp_TransWhereFldName('OMDESCRI',trim(this.w_CATOMINI))+")";

            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" OMDESCRI like "+cp_ToStr(trim(this.w_CATOMINI)+"%");

            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATOMINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOMINI_2_9'),i_cWhere,'GSAR_AOM',"Categoria omogenea",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOMINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOMINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCOMO = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATOMINI = space(5)
      endif
      this.w_DESCOMO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOMFIN
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOMFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOMFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( OMDESCRI like "+cp_ToStrODBC(trim(this.w_CATOMFIN)+"%")+cp_TransWhereFldName('OMDESCRI',trim(this.w_CATOMFIN))+")";

            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" OMDESCRI like "+cp_ToStr(trim(this.w_CATOMFIN)+"%");

            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATOMFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOMFIN_2_10'),i_cWhere,'GSAR_AOM',"Categoria omogenea",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOMFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOMFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCOMO1 = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CATOMFIN = space(5)
      endif
      this.w_DESCOMO1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATOMINI <= .w_CATOMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Cat. omogenea inferiore alla precedente")
        endif
        this.w_CATOMFIN = space(5)
        this.w_DESCOMO1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMINI
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( FADESCRI like "+cp_ToStrODBC(trim(this.w_FAMINI)+"%")+cp_TransWhereFldName('FADESCRI',trim(this.w_FAMINI))+")";

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_FAMINI)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FAMINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMINI_2_11'),i_cWhere,'GSAR_AFA',"Famiglia",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESCFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FAMINI = space(5)
      endif
      this.w_DESCFAM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMFIN
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( FADESCRI like "+cp_ToStrODBC(trim(this.w_FAMFIN)+"%")+cp_TransWhereFldName('FADESCRI',trim(this.w_FAMFIN))+")";

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_FAMFIN)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FAMFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMFIN_2_12'),i_cWhere,'GSAR_AFA',"Famiglia",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESCFAM1 = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FAMFIN = space(5)
      endif
      this.w_DESCFAM1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMINI <= .w_FAMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Famiglia inferiore alla selezione precedente")
        endif
        this.w_FAMFIN = space(5)
        this.w_DESCFAM1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARCINI
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARCINI))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARCINI)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_MARCINI)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_MARCINI)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MARCINI) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARCINI_2_13'),i_cWhere,'GSAR_AMH',"Marchio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARCINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARCINI = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARCA = NVL(_Link_.MADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MARCINI = space(5)
      endif
      this.w_DESMARCA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARCFIN
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARCFIN))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARCFIN)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_MARCFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_MARCFIN)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MARCFIN) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARCFIN_2_14'),i_cWhere,'GSAR_AMH',"Marchio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARCFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARCFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARCA1 = NVL(_Link_.MADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MARCFIN = space(5)
      endif
      this.w_DESMARCA1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MARCINI <= .w_MARCFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Marchio inferiore alla selezione precedente")
        endif
        this.w_MARCFIN = space(5)
        this.w_DESMARCA1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPMINI
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUPMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUPMINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPMINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( GMDESCRI like "+cp_ToStrODBC(trim(this.w_GRUPMINI)+"%")+cp_TransWhereFldName('GMDESCRI',trim(this.w_GRUPMINI))+")";

            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GMDESCRI like "+cp_ToStr(trim(this.w_GRUPMINI)+"%");

            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GRUPMINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUPMINI_2_19'),i_cWhere,'GSAR_AGM',"Gruppo merceologico",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUPMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUPMINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPMINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUP = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPMINI = space(5)
      endif
      this.w_DESGRUP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPMFIN
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUPMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUPMFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPMFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( GMDESCRI like "+cp_ToStrODBC(trim(this.w_GRUPMFIN)+"%")+cp_TransWhereFldName('GMDESCRI',trim(this.w_GRUPMFIN))+")";

            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GMDESCRI like "+cp_ToStr(trim(this.w_GRUPMFIN)+"%");

            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GRUPMFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUPMFIN_2_20'),i_cWhere,'GSAR_AGM',"Gruppo merceologico",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUPMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUPMFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPMFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUP1 = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPMFIN = space(5)
      endif
      this.w_DESGRUP1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUPMINI <= .w_GRUPMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo merceologico inferiore alla selezione precedente")
        endif
        this.w_GRUPMFIN = space(5)
        this.w_DESGRUP1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTINI
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTINI))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ARTINI)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTINI_2_29'),i_cWhere,'GSMA_BZA',"Articolo\servizio",'Gsst_art.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTINI)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESCART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESCART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPART = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPART$ 'PF-SE-MP-PH-MC-MA-IM-FS' OR .w_TIPART='FO' OR .w_TIPART='FM') AND iif(.w_OBSOLETI='N',.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO),IIF(.w_OBSOLETI='S',.w_DATOBSO<=.w_OBTEST AND NOT EMPTY(.w_DATOBSO),.T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo\servizio di tipo non corretto")
        endif
        this.w_ARTINI = space(20)
        this.w_DESCART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPART = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTFIN))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ARTFIN)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTFIN_2_30'),i_cWhere,'GSMA_BZA',"Articolo\servizio",'Gsst_art.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTFIN)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESCART1 = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESCART1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPART = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_ARTINI <= .w_ARTFIN) AND  (.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS' OR .w_TIPART='FO' OR .w_TIPART='FM') AND iif(.w_OBSOLETI='N',.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO),IIF(.w_OBSOLETI='S',.w_DATOBSO<=.w_OBTEST AND NOT EMPTY(.w_DATOBSO),.T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo inferiore alla selezione precedente")
        endif
        this.w_ARTFIN = space(20)
        this.w_DESCART1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPART = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORABINI
  func Link_2_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORABINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORABINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPFOR;
                     ,'ANCODICE',trim(this.w_FORABINI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORABINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORABINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORABINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPFOR);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORABINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORABINI_2_32'),i_cWhere,'GSAR_BZC',"Fornitori abituali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORABINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORABINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPFOR;
                       ,'ANCODICE',this.w_FORABINI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORABINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFORAB = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FORABINI = space(15)
      endif
      this.w_DESFORAB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORABINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGINI
  func Link_2_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGINI))
          select AGCODAGE,AGDESAGE,AGTIPAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGINI)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_CODAGINI)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_CODAGINI)+"%");

            select AGCODAGE,AGDESAGE,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODAGINI) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGINI_2_33'),i_cWhere,'GSAR_AGE',"Capo area",'GSAR1AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGINI)
            select AGCODAGE,AGDESAGE,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGINI = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAG2 = NVL(_Link_.AGDESAGE,space(35))
      this.w_TIPAGE = NVL(_Link_.AGTIPAGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGINI = space(5)
      endif
      this.w_DESAG2 = space(35)
      this.w_TIPAGE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPAGE='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente non capo area")
        endif
        this.w_CODAGINI = space(5)
        this.w_DESAG2 = space(35)
        this.w_TIPAGE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGFIN
  func Link_2_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGFIN))
          select AGCODAGE,AGDESAGE,AGTIPAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGFIN)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_CODAGFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_CODAGFIN)+"%");

            select AGCODAGE,AGDESAGE,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODAGFIN) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGFIN_2_34'),i_cWhere,'GSAR_AGE',"Capo area",'GSAR1AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGFIN)
            select AGCODAGE,AGDESAGE,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGFIN = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAG21 = NVL(_Link_.AGDESAGE,space(35))
      this.w_TIPAGE = NVL(_Link_.AGTIPAGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGFIN = space(5)
      endif
      this.w_DESAG21 = space(35)
      this.w_TIPAGE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODAGINI <= .w_CODAGFIN AND .w_TIPAGE='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezione inferiore al capo area precedente o agente non capo area")
        endif
        this.w_CODAGFIN = space(5)
        this.w_DESAG21 = space(35)
        this.w_TIPAGE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVEINI
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVEINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_CODVEINI)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_CODVEINI))
          select VTCODVET,VTDESVET;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVEINI)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStrODBC(trim(this.w_CODVEINI)+"%");

            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStr(trim(this.w_CODVEINI)+"%");

            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVEINI) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oCODVEINI_2_35'),i_cWhere,'GSAR_AVT',"Vettori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVEINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_CODVEINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_CODVEINI)
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVEINI = NVL(_Link_.VTCODVET,space(15))
      this.w_DESVET = NVL(_Link_.VTDESVET,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODVEINI = space(15)
      endif
      this.w_DESVET = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVEINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVEFIN
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVEFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_CODVEFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_CODVEFIN))
          select VTCODVET,VTDESVET;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVEFIN)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStrODBC(trim(this.w_CODVEFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStr(trim(this.w_CODVEFIN)+"%");

            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVEFIN) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oCODVEFIN_2_36'),i_cWhere,'GSAR_AVT',"Vettori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVEFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_CODVEFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_CODVEFIN)
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVEFIN = NVL(_Link_.VTCODVET,space(5))
      this.w_DESVET1 = NVL(_Link_.VTDESVET,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODVEFIN = space(5)
      endif
      this.w_DESVET1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODVEINI <= .w_CODVEFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezione inferiore al vettore precedente")
        endif
        this.w_CODVEFIN = space(5)
        this.w_DESVET1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVEFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MASTRINI
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MASTRINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_MASTRINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_MASTRINI))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MASTRINI)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_MASTRINI)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_MASTRINI)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MASTRINI) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oMASTRINI_2_37'),i_cWhere,'GSAR_AMC',"Mastri",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MASTRINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_MASTRINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_MASTRINI)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MASTRINI = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MASTRINI = space(15)
      endif
      this.w_DESMAS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MASTRINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MASTRFIN
  func Link_2_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MASTRFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_MASTRFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_MASTRFIN))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MASTRFIN)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_MASTRFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_MASTRFIN)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MASTRFIN) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oMASTRFIN_2_38'),i_cWhere,'GSAR_AMC',"Mastri",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MASTRFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_MASTRFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_MASTRFIN)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MASTRFIN = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS1 = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MASTRFIN = space(15)
      endif
      this.w_DESMAS1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MASTRINI <= .w_MASTRFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezione inferiore al fornitore precedente")
        endif
        this.w_MASTRFIN = space(15)
        this.w_DESMAS1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MASTRFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_3_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_READAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESE
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_READAZI;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
      this.w_DATINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
      this.w_DATINIESE = ctod("  /  /  ")
      this.w_DATFINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESINI
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODESINI)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCONINI);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPORN;
                     ,'DDCODICE',this.w_CODCONINI;
                     ,'DDCODDES',trim(this.w_CODESINI))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESINI)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESINI) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODESINI_3_3'),i_cWhere,'',"Destinazione",'GSVE_MDV.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPORN<>oSource.xKey(1);
           .or. this.w_CODCONINI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODCONINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODESINI);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCONINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPORN;
                       ,'DDCODICE',this.w_CODCONINI;
                       ,'DDCODDES',this.w_CODESINI)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESINI = NVL(_Link_.DDCODDES,space(5))
      this.w_DESDEINI = NVL(_Link_.DDNOMDES,space(40))
      this.w_TIPRIF = NVL(_Link_.DDTIPRIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODESINI = space(5)
      endif
      this.w_DESDEINI = space(40)
      this.w_TIPRIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODESINI) OR .w_TIPRIF='CO')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        endif
        this.w_CODESINI = space(5)
        this.w_DESDEINI = space(40)
        this.w_TIPRIF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESFIN
  func Link_3_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODESFIN)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCONINI);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPORN;
                     ,'DDCODICE',this.w_CODCONINI;
                     ,'DDCODDES',trim(this.w_CODESFIN))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESFIN)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESFIN) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODESFIN_3_4'),i_cWhere,'',"Destinazione",'GSVE_MDV.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPORN<>oSource.xKey(1);
           .or. this.w_CODCONINI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Destinazione inferiore alla selezione precedente o inesistente o di tipo diverso da consegna")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODCONINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODESFIN);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPORN);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCONINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPORN;
                       ,'DDCODICE',this.w_CODCONINI;
                       ,'DDCODDES',this.w_CODESFIN)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESFIN = NVL(_Link_.DDCODDES,space(5))
      this.w_DESDEFIN = NVL(_Link_.DDNOMDES,space(40))
      this.w_TIPRIF = NVL(_Link_.DDTIPRIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODESFIN = space(5)
      endif
      this.w_DESDEFIN = space(40)
      this.w_TIPRIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODESFIN) OR .w_TIPRIF='CO') and .w_CODESINI<=.w_CODESFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Destinazione inferiore alla selezione precedente o inesistente o di tipo diverso da consegna")
        endif
        this.w_CODESFIN = space(5)
        this.w_DESDEFIN = space(40)
        this.w_TIPRIF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCEINI
  func Link_3_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCEINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CODCEINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CODCEINI))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCEINI)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCEINI) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCODCEINI_3_7'),i_cWhere,'GSCA_ACC',"Centri di costo e ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCEINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CODCEINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CODCEINI)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCEINI = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESSPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCEINI = space(15)
      endif
      this.w_DESSPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCEINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCEFIN
  func Link_3_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCEFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CODCEFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CODCEFIN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCEFIN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCEFIN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCODCEFIN_3_8'),i_cWhere,'GSCA_ACC',"Centri di costo e ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCEFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CODCEFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CODCEFIN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCEFIN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESSPIA1 = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCEFIN = space(15)
      endif
      this.w_DESSPIA1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCEINI<=.w_CODCEFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di costo inferiore alla selezione precedente")
        endif
        this.w_CODCEFIN = space(15)
        this.w_DESSPIA1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCEFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCCEINI
  func Link_3_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCCEINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VOCCEINI)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_VOCCEINI))
          select VCCODICE,VCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCCEINI)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCCEINI) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oVOCCEINI_3_9'),i_cWhere,'GSCA_AVC',"Voce di costo e ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCCEINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VOCCEINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_VOCCEINI)
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCCEINI = NVL(_Link_.VCCODICE,space(15))
      this.w_VODESCR = NVL(_Link_.VCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_VOCCEINI = space(15)
      endif
      this.w_VODESCR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCCEINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCCEFIN
  func Link_3_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCCEFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VOCCEFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_VOCCEFIN))
          select VCCODICE,VCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCCEFIN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCCEFIN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oVOCCEFIN_3_10'),i_cWhere,'GSCA_AVC',"Voce di costo e ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCCEFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VOCCEFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_VOCCEFIN)
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCCEFIN = NVL(_Link_.VCCODICE,space(15))
      this.w_VODESCR1 = NVL(_Link_.VCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_VOCCEFIN = space(15)
      endif
      this.w_VODESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VOCCEINI<=.w_VOCCEFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Voce di costo inferiore alla selezione precedente")
        endif
        this.w_VOCCEFIN = space(15)
        this.w_VODESCR1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCCEFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMINI
  func Link_3_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMINI))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMINI)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMMINI) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMINI_3_11'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMINI)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMINI = NVL(_Link_.CNCODCAN,space(15))
      this.w_COMMDES = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COMMINI = space(15)
      endif
      this.w_COMMDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMFIN
  func Link_3_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMFIN))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMFIN)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMMFIN) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMFIN_3_12'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMFIN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMFIN = NVL(_Link_.CNCODCAN,space(15))
      this.w_COMMDES1 = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COMMFIN = space(15)
      endif
      this.w_COMMDES1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_COMMINI<=.w_COMMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Commessa inferiore alla selezione precedente")
        endif
        this.w_COMMFIN = space(15)
        this.w_COMMDES1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTIVINI
  func Link_3_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTIVINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTIVINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_ATTIVINI))
          select ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTIVINI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTIVINI) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODATT',cp_AbsName(oSource.parent,'oATTIVINI_3_13'),i_cWhere,'GSPC_BZZ',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTIVINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTIVINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_ATTIVINI)
            select ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTIVINI = NVL(_Link_.ATCODATT,space(15))
      this.w_ATTIDES = NVL(_Link_.ATDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ATTIVINI = space(15)
      endif
      this.w_ATTIDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTIVINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTIVFIN
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTIVFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTIVFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_ATTIVFIN))
          select ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTIVFIN)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTIVFIN) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODATT',cp_AbsName(oSource.parent,'oATTIVFIN_3_14'),i_cWhere,'GSPC_BZZ',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTIVFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTIVFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_ATTIVFIN)
            select ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTIVFIN = NVL(_Link_.ATCODATT,space(15))
      this.w_ATTIDES1 = NVL(_Link_.ATDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ATTIVFIN = space(15)
      endif
      this.w_ATTIDES1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ATTIVINI<=.w_ATTIVFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attivit� inferiore alla selezione precedente")
        endif
        this.w_ATTIVFIN = space(15)
        this.w_ATTIDES1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTIVFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PAGAMINI
  func Link_3_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAGAMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_PAGAMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_PAGAMINI))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAGAMINI)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PAGAMINI) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oPAGAMINI_3_15'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAGAMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_PAGAMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_PAGAMINI)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAGAMINI = NVL(_Link_.PACODICE,space(5))
      this.w_PAGADES = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PAGAMINI = space(5)
      endif
      this.w_PAGADES = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAGAMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PAGAMFIN
  func Link_3_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAGAMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_PAGAMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_PAGAMFIN))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAGAMFIN)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PAGAMFIN) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oPAGAMFIN_3_16'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAGAMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_PAGAMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_PAGAMFIN)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAGAMFIN = NVL(_Link_.PACODICE,space(5))
      this.w_PAGADES1 = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PAGAMFIN = space(5)
      endif
      this.w_PAGADES1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PAGAMINI<=.w_PAGAMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Pagamento inferiore alla selezione precedente")
        endif
        this.w_PAGAMFIN = space(5)
        this.w_PAGADES1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAGAMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNIMINI
  func Link_3_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNIMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_UNIMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_UNIMINI))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UNIMINI)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UNIMINI) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oUNIMINI_3_17'),i_cWhere,'GSAR_AUM',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNIMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNIMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNIMINI)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNIMINI = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_UNIMINI = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNIMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNIMIFIN
  func Link_3_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNIMIFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_UNIMIFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_UNIMIFIN))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UNIMIFIN)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UNIMIFIN) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oUNIMIFIN_3_18'),i_cWhere,'GSAR_AUM',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNIMIFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNIMIFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNIMIFIN)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNIMIFIN = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_UNIMIFIN = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_UNIMINI<=.w_UNIMIFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inferiore alla selezione precedente")
        endif
        this.w_UNIMIFIN = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNIMIFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=divisa
  func Link_3_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_divisa) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_divisa)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_divisa))
          select VACODVAL,VASIMVAL,VADECTOT,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_divisa)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VASIMVAL like "+cp_ToStrODBC(trim(this.w_divisa)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VASIMVAL like "+cp_ToStr(trim(this.w_divisa)+"%");

            select VACODVAL,VASIMVAL,VADECTOT,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_divisa) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'odivisa_3_21'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VADECTOT,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_divisa)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_divisa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_divisa)
            select VACODVAL,VASIMVAL,VADECTOT,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_divisa = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
      this.w_descr = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_divisa = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_decimi = 0
      this.w_descr = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_divisa Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORABFIN
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORABFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORABFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPFOR;
                     ,'ANCODICE',trim(this.w_FORABFIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORABFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORABFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORABFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPFOR);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORABFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORABFIN_2_42'),i_cWhere,'GSAR_BZC',"Fornitori abituali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Selezione inferiore al fornitore precedente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORABFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORABFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPFOR;
                       ,'ANCODICE',this.w_FORABFIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORABFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFORAB1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FORABFIN = space(15)
      endif
      this.w_DESFORAB1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FORABINI <= .w_FORABFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezione inferiore al fornitore precedente")
        endif
        this.w_FORABFIN = space(15)
        this.w_DESFORAB1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORABFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCECODICE_1_1.value==this.w_CECODICE)
      this.oPgFrm.Page1.oPag.oCECODICE_1_1.value=this.w_CECODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCEPERIO2_1_2.RadioValue()==this.w_CEPERIO2)
      this.oPgFrm.Page1.oPag.oCEPERIO2_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCEPERIO3_1_3.RadioValue()==this.w_CEPERIO3)
      this.oPgFrm.Page1.oPag.oCEPERIO3_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI1_1_4.value==this.w_DATINI1)
      this.oPgFrm.Page1.oPag.oDATINI1_1_4.value=this.w_DATINI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN1_1_5.value==this.w_DATFIN1)
      this.oPgFrm.Page1.oPag.oDATFIN1_1_5.value=this.w_DATFIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI2_1_6.value==this.w_DATINI2)
      this.oPgFrm.Page1.oPag.oDATINI2_1_6.value=this.w_DATINI2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN2_1_7.value==this.w_DATFIN2)
      this.oPgFrm.Page1.oPag.oDATFIN2_1_7.value=this.w_DATFIN2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI3_1_8.value==this.w_DATINI3)
      this.oPgFrm.Page1.oPag.oDATINI3_1_8.value=this.w_DATINI3
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN3_1_9.value==this.w_DATFIN3)
      this.oPgFrm.Page1.oPag.oDATFIN3_1_9.value=this.w_DATFIN3
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCOMINI_1_10.value==this.w_CATCOMINI)
      this.oPgFrm.Page1.oPag.oCATCOMINI_1_10.value=this.w_CATCOMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCOMFIN_1_11.value==this.w_CATCOMFIN)
      this.oPgFrm.Page1.oPag.oCATCOMFIN_1_11.value=this.w_CATCOMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCOM_1_14.value==this.w_DESCCOM)
      this.oPgFrm.Page1.oPag.oDESCCOM_1_14.value=this.w_DESCCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCOM1_1_15.value==this.w_DESCCOM1)
      this.oPgFrm.Page1.oPag.oDESCCOM1_1_15.value=this.w_DESCCOM1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_18.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_18.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR1_1_19.value==this.w_DESFOR1)
      this.oPgFrm.Page1.oPag.oDESFOR1_1_19.value=this.w_DESFOR1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_22.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_22.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI1_1_23.value==this.w_DESCLI1)
      this.oPgFrm.Page1.oPag.oDESCLI1_1_23.value=this.w_DESCLI1
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGINI_1_32.value==this.w_MAGINI)
      this.oPgFrm.Page1.oPag.oMAGINI_1_32.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGFIN_1_33.value==this.w_MAGFIN)
      this.oPgFrm.Page1.oPag.oMAGFIN_1_33.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_34.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_34.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG1_1_35.value==this.w_DESMAG1)
      this.oPgFrm.Page1.oPag.oDESMAG1_1_35.value=this.w_DESMAG1
    endif
    if not(this.oPgFrm.Page1.oPag.oAGEINI_1_38.value==this.w_AGEINI)
      this.oPgFrm.Page1.oPag.oAGEINI_1_38.value=this.w_AGEINI
    endif
    if not(this.oPgFrm.Page1.oPag.oAGEFIN_1_39.value==this.w_AGEFIN)
      this.oPgFrm.Page1.oPag.oAGEFIN_1_39.value=this.w_AGEFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_40.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_40.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE1_1_41.value==this.w_DESAGE1)
      this.oPgFrm.Page1.oPag.oDESAGE1_1_41.value=this.w_DESAGE1
    endif
    if not(this.oPgFrm.Page1.oPag.oZONINI_1_44.value==this.w_ZONINI)
      this.oPgFrm.Page1.oPag.oZONINI_1_44.value=this.w_ZONINI
    endif
    if not(this.oPgFrm.Page1.oPag.oZONFIN_1_45.value==this.w_ZONFIN)
      this.oPgFrm.Page1.oPag.oZONFIN_1_45.value=this.w_ZONFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESZON_1_46.value==this.w_DESZON)
      this.oPgFrm.Page1.oPag.oDESZON_1_46.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESZON1_1_47.value==this.w_DESZON1)
      this.oPgFrm.Page1.oPag.oDESZON1_1_47.value=this.w_DESZON1
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCONINI_1_50.value==this.w_CATCONINI)
      this.oPgFrm.Page1.oPag.oCATCONINI_1_50.value=this.w_CATCONINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCONFIN_1_51.value==this.w_CATCONFIN)
      this.oPgFrm.Page1.oPag.oCATCONFIN_1_51.value=this.w_CATCONFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCON_1_52.value==this.w_DESCCON)
      this.oPgFrm.Page1.oPag.oDESCCON_1_52.value=this.w_DESCCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCON1_1_53.value==this.w_DESCCON1)
      this.oPgFrm.Page1.oPag.oDESCCON1_1_53.value=this.w_DESCCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIFOR_1_59.RadioValue()==this.w_CLIFOR)
      this.oPgFrm.Page1.oPag.oCLIFOR_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLINI_1_60.value==this.w_CODCLINI)
      this.oPgFrm.Page1.oPag.oCODCLINI_1_60.value=this.w_CODCLINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLIFIN_1_61.value==this.w_CODCLIFIN)
      this.oPgFrm.Page1.oPag.oCODCLIFIN_1_61.value=this.w_CODCLIFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFORINI_1_62.value==this.w_CODFORINI)
      this.oPgFrm.Page1.oPag.oCODFORINI_1_62.value=this.w_CODFORINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFORFIN_1_64.value==this.w_CODFORFIN)
      this.oPgFrm.Page1.oPag.oCODFORFIN_1_64.value=this.w_CODFORFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODORNINI_1_65.value==this.w_CODORNINI)
      this.oPgFrm.Page1.oPag.oCODORNINI_1_65.value=this.w_CODORNINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODORNFIN_1_66.value==this.w_CODORNFIN)
      this.oPgFrm.Page1.oPag.oCODORNFIN_1_66.value=this.w_CODORNFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCEDESCRI_1_68.value==this.w_CEDESCRI)
      this.oPgFrm.Page1.oPag.oCEDESCRI_1_68.value=this.w_CEDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMO_2_3.value==this.w_DESCOMO)
      this.oPgFrm.Page2.oPag.oDESCOMO_2_3.value=this.w_DESCOMO
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMO1_2_4.value==this.w_DESCOMO1)
      this.oPgFrm.Page2.oPag.oDESCOMO1_2_4.value=this.w_DESCOMO1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCFAM_2_7.value==this.w_DESCFAM)
      this.oPgFrm.Page2.oPag.oDESCFAM_2_7.value=this.w_DESCFAM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCFAM1_2_8.value==this.w_DESCFAM1)
      this.oPgFrm.Page2.oPag.oDESCFAM1_2_8.value=this.w_DESCFAM1
    endif
    if not(this.oPgFrm.Page2.oPag.oCATOMINI_2_9.value==this.w_CATOMINI)
      this.oPgFrm.Page2.oPag.oCATOMINI_2_9.value=this.w_CATOMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCATOMFIN_2_10.value==this.w_CATOMFIN)
      this.oPgFrm.Page2.oPag.oCATOMFIN_2_10.value=this.w_CATOMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMINI_2_11.value==this.w_FAMINI)
      this.oPgFrm.Page2.oPag.oFAMINI_2_11.value=this.w_FAMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMFIN_2_12.value==this.w_FAMFIN)
      this.oPgFrm.Page2.oPag.oFAMFIN_2_12.value=this.w_FAMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMARCINI_2_13.value==this.w_MARCINI)
      this.oPgFrm.Page2.oPag.oMARCINI_2_13.value=this.w_MARCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oMARCFIN_2_14.value==this.w_MARCFIN)
      this.oPgFrm.Page2.oPag.oMARCFIN_2_14.value=this.w_MARCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUP_2_17.value==this.w_DESGRUP)
      this.oPgFrm.Page2.oPag.oDESGRUP_2_17.value=this.w_DESGRUP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUP1_2_18.value==this.w_DESGRUP1)
      this.oPgFrm.Page2.oPag.oDESGRUP1_2_18.value=this.w_DESGRUP1
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUPMINI_2_19.value==this.w_GRUPMINI)
      this.oPgFrm.Page2.oPag.oGRUPMINI_2_19.value=this.w_GRUPMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUPMFIN_2_20.value==this.w_GRUPMFIN)
      this.oPgFrm.Page2.oPag.oGRUPMFIN_2_20.value=this.w_GRUPMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMARCA_2_23.value==this.w_DESMARCA)
      this.oPgFrm.Page2.oPag.oDESMARCA_2_23.value=this.w_DESMARCA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMARCA1_2_24.value==this.w_DESMARCA1)
      this.oPgFrm.Page2.oPag.oDESMARCA1_2_24.value=this.w_DESMARCA1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCART_2_27.value==this.w_DESCART)
      this.oPgFrm.Page2.oPag.oDESCART_2_27.value=this.w_DESCART
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCART1_2_28.value==this.w_DESCART1)
      this.oPgFrm.Page2.oPag.oDESCART1_2_28.value=this.w_DESCART1
    endif
    if not(this.oPgFrm.Page2.oPag.oARTINI_2_29.value==this.w_ARTINI)
      this.oPgFrm.Page2.oPag.oARTINI_2_29.value=this.w_ARTINI
    endif
    if not(this.oPgFrm.Page2.oPag.oARTFIN_2_30.value==this.w_ARTFIN)
      this.oPgFrm.Page2.oPag.oARTFIN_2_30.value=this.w_ARTFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFORABINI_2_32.value==this.w_FORABINI)
      this.oPgFrm.Page2.oPag.oFORABINI_2_32.value=this.w_FORABINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGINI_2_33.value==this.w_CODAGINI)
      this.oPgFrm.Page2.oPag.oCODAGINI_2_33.value=this.w_CODAGINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGFIN_2_34.value==this.w_CODAGFIN)
      this.oPgFrm.Page2.oPag.oCODAGFIN_2_34.value=this.w_CODAGFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODVEINI_2_35.value==this.w_CODVEINI)
      this.oPgFrm.Page2.oPag.oCODVEINI_2_35.value=this.w_CODVEINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODVEFIN_2_36.value==this.w_CODVEFIN)
      this.oPgFrm.Page2.oPag.oCODVEFIN_2_36.value=this.w_CODVEFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMASTRINI_2_37.value==this.w_MASTRINI)
      this.oPgFrm.Page2.oPag.oMASTRINI_2_37.value=this.w_MASTRINI
    endif
    if not(this.oPgFrm.Page2.oPag.oMASTRFIN_2_38.value==this.w_MASTRFIN)
      this.oPgFrm.Page2.oPag.oMASTRFIN_2_38.value=this.w_MASTRFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oCODESINI_3_3.value==this.w_CODESINI)
      this.oPgFrm.Page3.oPag.oCODESINI_3_3.value=this.w_CODESINI
    endif
    if not(this.oPgFrm.Page3.oPag.oCODESFIN_3_4.value==this.w_CODESFIN)
      this.oPgFrm.Page3.oPag.oCODESFIN_3_4.value=this.w_CODESFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oPROVINI_3_5.value==this.w_PROVINI)
      this.oPgFrm.Page3.oPag.oPROVINI_3_5.value=this.w_PROVINI
    endif
    if not(this.oPgFrm.Page3.oPag.oPROVFIN_3_6.value==this.w_PROVFIN)
      this.oPgFrm.Page3.oPag.oPROVFIN_3_6.value=this.w_PROVFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oCODCEINI_3_7.value==this.w_CODCEINI)
      this.oPgFrm.Page3.oPag.oCODCEINI_3_7.value=this.w_CODCEINI
    endif
    if not(this.oPgFrm.Page3.oPag.oCODCEFIN_3_8.value==this.w_CODCEFIN)
      this.oPgFrm.Page3.oPag.oCODCEFIN_3_8.value=this.w_CODCEFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oVOCCEINI_3_9.value==this.w_VOCCEINI)
      this.oPgFrm.Page3.oPag.oVOCCEINI_3_9.value=this.w_VOCCEINI
    endif
    if not(this.oPgFrm.Page3.oPag.oVOCCEFIN_3_10.value==this.w_VOCCEFIN)
      this.oPgFrm.Page3.oPag.oVOCCEFIN_3_10.value=this.w_VOCCEFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oCOMMINI_3_11.value==this.w_COMMINI)
      this.oPgFrm.Page3.oPag.oCOMMINI_3_11.value=this.w_COMMINI
    endif
    if not(this.oPgFrm.Page3.oPag.oCOMMFIN_3_12.value==this.w_COMMFIN)
      this.oPgFrm.Page3.oPag.oCOMMFIN_3_12.value=this.w_COMMFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oATTIVINI_3_13.value==this.w_ATTIVINI)
      this.oPgFrm.Page3.oPag.oATTIVINI_3_13.value=this.w_ATTIVINI
    endif
    if not(this.oPgFrm.Page3.oPag.oATTIVFIN_3_14.value==this.w_ATTIVFIN)
      this.oPgFrm.Page3.oPag.oATTIVFIN_3_14.value=this.w_ATTIVFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oPAGAMINI_3_15.value==this.w_PAGAMINI)
      this.oPgFrm.Page3.oPag.oPAGAMINI_3_15.value=this.w_PAGAMINI
    endif
    if not(this.oPgFrm.Page3.oPag.oPAGAMFIN_3_16.value==this.w_PAGAMFIN)
      this.oPgFrm.Page3.oPag.oPAGAMFIN_3_16.value=this.w_PAGAMFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oUNIMINI_3_17.value==this.w_UNIMINI)
      this.oPgFrm.Page3.oPag.oUNIMINI_3_17.value=this.w_UNIMINI
    endif
    if not(this.oPgFrm.Page3.oPag.oUNIMIFIN_3_18.value==this.w_UNIMIFIN)
      this.oPgFrm.Page3.oPag.oUNIMIFIN_3_18.value=this.w_UNIMIFIN
    endif
    if not(this.oPgFrm.Page3.oPag.omonete_3_20.RadioValue()==this.w_monete)
      this.oPgFrm.Page3.oPag.omonete_3_20.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.odivisa_3_21.value==this.w_divisa)
      this.oPgFrm.Page3.oPag.odivisa_3_21.value=this.w_divisa
    endif
    if not(this.oPgFrm.Page3.oPag.oSIMVAL_3_28.value==this.w_SIMVAL)
      this.oPgFrm.Page3.oPag.oSIMVAL_3_28.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page3.oPag.oCAMVAL_3_29.value==this.w_CAMVAL)
      this.oPgFrm.Page3.oPag.oCAMVAL_3_29.value=this.w_CAMVAL
    endif
    if not(this.oPgFrm.Page3.oPag.oDESDEFIN_3_34.value==this.w_DESDEFIN)
      this.oPgFrm.Page3.oPag.oDESDEFIN_3_34.value=this.w_DESDEFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oDESDEINI_3_35.value==this.w_DESDEINI)
      this.oPgFrm.Page3.oPag.oDESDEINI_3_35.value=this.w_DESDEINI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSPIA1_3_40.value==this.w_DESSPIA1)
      this.oPgFrm.Page3.oPag.oDESSPIA1_3_40.value=this.w_DESSPIA1
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSPIA_3_41.value==this.w_DESSPIA)
      this.oPgFrm.Page3.oPag.oDESSPIA_3_41.value=this.w_DESSPIA
    endif
    if not(this.oPgFrm.Page3.oPag.oVODESCR1_3_44.value==this.w_VODESCR1)
      this.oPgFrm.Page3.oPag.oVODESCR1_3_44.value=this.w_VODESCR1
    endif
    if not(this.oPgFrm.Page3.oPag.oVODESCR_3_45.value==this.w_VODESCR)
      this.oPgFrm.Page3.oPag.oVODESCR_3_45.value=this.w_VODESCR
    endif
    if not(this.oPgFrm.Page3.oPag.oCOMMDES1_3_48.value==this.w_COMMDES1)
      this.oPgFrm.Page3.oPag.oCOMMDES1_3_48.value=this.w_COMMDES1
    endif
    if not(this.oPgFrm.Page3.oPag.oCOMMDES_3_49.value==this.w_COMMDES)
      this.oPgFrm.Page3.oPag.oCOMMDES_3_49.value=this.w_COMMDES
    endif
    if not(this.oPgFrm.Page3.oPag.oATTIDES1_3_52.value==this.w_ATTIDES1)
      this.oPgFrm.Page3.oPag.oATTIDES1_3_52.value=this.w_ATTIDES1
    endif
    if not(this.oPgFrm.Page3.oPag.oATTIDES_3_53.value==this.w_ATTIDES)
      this.oPgFrm.Page3.oPag.oATTIDES_3_53.value=this.w_ATTIDES
    endif
    if not(this.oPgFrm.Page3.oPag.oPAGADES_3_57.value==this.w_PAGADES)
      this.oPgFrm.Page3.oPag.oPAGADES_3_57.value=this.w_PAGADES
    endif
    if not(this.oPgFrm.Page3.oPag.oPAGADES1_3_58.value==this.w_PAGADES1)
      this.oPgFrm.Page3.oPag.oPAGADES1_3_58.value=this.w_PAGADES1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFORAB_2_40.value==this.w_DESFORAB)
      this.oPgFrm.Page2.oPag.oDESFORAB_2_40.value=this.w_DESFORAB
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFORAB1_2_41.value==this.w_DESFORAB1)
      this.oPgFrm.Page2.oPag.oDESFORAB1_2_41.value=this.w_DESFORAB1
    endif
    if not(this.oPgFrm.Page2.oPag.oFORABFIN_2_42.value==this.w_FORABFIN)
      this.oPgFrm.Page2.oPag.oFORABFIN_2_42.value=this.w_FORABFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAG2_2_45.value==this.w_DESAG2)
      this.oPgFrm.Page2.oPag.oDESAG2_2_45.value=this.w_DESAG2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAG21_2_46.value==this.w_DESAG21)
      this.oPgFrm.Page2.oPag.oDESAG21_2_46.value=this.w_DESAG21
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVET_2_49.value==this.w_DESVET)
      this.oPgFrm.Page2.oPag.oDESVET_2_49.value=this.w_DESVET
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVET1_2_50.value==this.w_DESVET1)
      this.oPgFrm.Page2.oPag.oDESVET1_2_50.value=this.w_DESVET1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAS_2_53.value==this.w_DESMAS)
      this.oPgFrm.Page2.oPag.oDESMAS_2_53.value=this.w_DESMAS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAS1_2_54.value==this.w_DESMAS1)
      this.oPgFrm.Page2.oPag.oDESMAS1_2_54.value=this.w_DESMAS1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESORN_1_75.value==this.w_DESORN)
      this.oPgFrm.Page1.oPag.oDESORN_1_75.value=this.w_DESORN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESORN1_1_76.value==this.w_DESORN1)
      this.oPgFrm.Page1.oPag.oDESORN1_1_76.value=this.w_DESORN1
    endif
    if not(this.oPgFrm.Page2.oPag.oOBSOLETI_2_57.RadioValue()==this.w_OBSOLETI)
      this.oPgFrm.Page2.oPag.oOBSOLETI_2_57.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CECODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCECODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CECODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATCOMINI <= .w_CATCOMFIN)  and not(empty(.w_CATCOMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATCOMFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cat. commerciale inferiore alla selezione precedente")
          case   not(.w_MAGFIN <= .w_MAGFIN)  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFIN_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inferiore alla selezione precedente")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_AGEINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGEINI_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente obsoleto")
          case   not((.w_AGEINI <= .w_AGEFIN) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_AGEFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGEFIN_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente inferiore alla selezione precedente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_ZONINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZONINI_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_ZONINI <= .w_ZONFIN) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ZONFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZONFIN_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Zona inferiore alla selezione precedente")
          case   not(.w_CATCONINI <= .w_CATCONFIN)  and not(empty(.w_CATCONFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATCONFIN_1_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cat. contabile inferiore alla selezione precedente")
          case   not((empty(.w_CODCLIFIN)) OR  (.w_CODCLINI <= .w_CODCLIFIN))  and not(.w_CLIFOR <> 'C')  and not(empty(.w_CODCLINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLINI_1_60.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il cliente iniziale � maggiore di quello finale oppure inesistente")
          case   not((.w_CODCLINI <= .w_CODCLIFIN) OR(empty(.w_CODCLIFIN)))  and not(.w_CLIFOR <> 'C')  and not(empty(.w_CODCLIFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLIFIN_1_61.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il cliente finale � minore di quello iniziale oppure inesistente")
          case   not((empty(.w_CODFORFIN)) OR  (.w_CODFORINI <= .w_CODFORFIN))  and not(.w_CLIFOR <> 'F')  and not(empty(.w_CODFORINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFORINI_1_62.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il fornitore iniziale � maggiore di quello finale oppure inesistente")
          case   not((.w_CODFORINI <= .w_CODFORFIN) OR(empty(.w_CODFORFIN)))  and not(.w_CLIFOR <> 'F')  and not(empty(.w_CODFORFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFORFIN_1_64.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il fornitore finale � minore di quello iniziale oppure inesistente")
          case   not(.w_CODORNINI <= .w_CODORNFIN)  and not(.w_TIPORN = 'G')  and not(empty(.w_CODORNFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODORNFIN_1_66.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione inferiore al per conto di precedente")
          case   not(.w_CATOMINI <= .w_CATOMFIN)  and not(empty(.w_CATOMFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCATOMFIN_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cat. omogenea inferiore alla precedente")
          case   not(.w_FAMINI <= .w_FAMFIN)  and not(empty(.w_FAMFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFAMFIN_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Famiglia inferiore alla selezione precedente")
          case   not(.w_MARCINI <= .w_MARCFIN)  and not(empty(.w_MARCFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMARCFIN_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Marchio inferiore alla selezione precedente")
          case   not(.w_GRUPMINI <= .w_GRUPMFIN)  and not(empty(.w_GRUPMFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUPMFIN_2_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo merceologico inferiore alla selezione precedente")
          case   not((.w_TIPART$ 'PF-SE-MP-PH-MC-MA-IM-FS' OR .w_TIPART='FO' OR .w_TIPART='FM') AND iif(.w_OBSOLETI='N',.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO),IIF(.w_OBSOLETI='S',.w_DATOBSO<=.w_OBTEST AND NOT EMPTY(.w_DATOBSO),.T.)))  and not(empty(.w_ARTINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTINI_2_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo\servizio di tipo non corretto")
          case   not((.w_ARTINI <= .w_ARTFIN) AND  (.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS' OR .w_TIPART='FO' OR .w_TIPART='FM') AND iif(.w_OBSOLETI='N',.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO),IIF(.w_OBSOLETI='S',.w_DATOBSO<=.w_OBTEST AND NOT EMPTY(.w_DATOBSO),.T.)))  and not(empty(.w_ARTFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTFIN_2_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo inferiore alla selezione precedente")
          case   not(.w_TIPAGE='C')  and not(empty(.w_CODAGINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGINI_2_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente non capo area")
          case   not(.w_CODAGINI <= .w_CODAGFIN AND .w_TIPAGE='C')  and not(empty(.w_CODAGFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODAGFIN_2_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione inferiore al capo area precedente o agente non capo area")
          case   not(.w_CODVEINI <= .w_CODVEFIN)  and not(empty(.w_CODVEFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODVEFIN_2_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione inferiore al vettore precedente")
          case   not(.w_MASTRINI <= .w_MASTRFIN)  and not(empty(.w_MASTRFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMASTRFIN_2_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione inferiore al fornitore precedente")
          case   not((EMPTY(.w_CODESINI) OR .w_TIPRIF='CO'))  and (not empty(.w_CODCONINI) AND .w_CODCONINI=.w_CODCONFIN)  and not(empty(.w_CODESINI))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODESINI_3_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
          case   not((EMPTY(.w_CODESFIN) OR .w_TIPRIF='CO') and .w_CODESINI<=.w_CODESFIN)  and (not empty(.w_CODCONINI) AND .w_CODCONINI=.w_CODCONFIN)  and not(empty(.w_CODESFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODESFIN_3_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Destinazione inferiore alla selezione precedente o inesistente o di tipo diverso da consegna")
          case   not(.w_PROVINI<=.w_PROVFIN)
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPROVFIN_3_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Provincia inferiore alla selezione precedente")
          case   not(.w_CODCEINI<=.w_CODCEFIN)  and (g_PERCCR='S')  and not(empty(.w_CODCEFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODCEFIN_3_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di costo inferiore alla selezione precedente")
          case   not(.w_VOCCEINI<=.w_VOCCEFIN)  and ((g_PERCCR='S' OR g_COMM='S'))  and not(empty(.w_VOCCEFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVOCCEFIN_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Voce di costo inferiore alla selezione precedente")
          case   not(.w_COMMINI<=.w_COMMFIN)  and ((g_PERCCR='S' OR  g_COMM='S'))  and not(empty(.w_COMMFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCOMMFIN_3_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Commessa inferiore alla selezione precedente")
          case   not(.w_ATTIVINI<=.w_ATTIVFIN)  and (g_COMM='S')  and not(empty(.w_ATTIVFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oATTIVFIN_3_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attivit� inferiore alla selezione precedente")
          case   not(.w_PAGAMINI<=.w_PAGAMFIN)  and not(empty(.w_PAGAMFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPAGAMFIN_3_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Pagamento inferiore alla selezione precedente")
          case   not(.w_UNIMINI<=.w_UNIMIFIN)  and not(empty(.w_UNIMIFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oUNIMIFIN_3_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� di misura inferiore alla selezione precedente")
          case   (empty(.w_divisa))  and (.w_monete='A')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.odivisa_3_21.SetFocus()
            i_bnoObbl = !empty(.w_divisa)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMVAL))  and (.w_MONETE='A' and .w_CAOVAL=0)
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCAMVAL_3_29.SetFocus()
            i_bnoObbl = !empty(.w_CAMVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FORABINI <= .w_FORABFIN)  and not(empty(.w_FORABFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORABFIN_2_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezione inferiore al fornitore precedente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CECODICE = this.w_CECODICE
    this.o_CEPERIO2 = this.w_CEPERIO2
    this.o_CEPERIO3 = this.w_CEPERIO3
    this.o_DATINI1 = this.w_DATINI1
    this.o_DATINI2 = this.w_DATINI2
    this.o_DATINI3 = this.w_DATINI3
    this.o_CATCOMINI = this.w_CATCOMINI
    this.o_TIPCLI = this.w_TIPCLI
    this.o_MAGINI = this.w_MAGINI
    this.o_AGEINI = this.w_AGEINI
    this.o_ZONINI = this.w_ZONINI
    this.o_CATCONINI = this.w_CATCONINI
    this.o_CLIFOR = this.w_CLIFOR
    this.o_CODCLINI = this.w_CODCLINI
    this.o_CODCLIFIN = this.w_CODCLIFIN
    this.o_CODFORINI = this.w_CODFORINI
    this.o_CODFORFIN = this.w_CODFORFIN
    this.o_CODORNINI = this.w_CODORNINI
    this.o_CATOMINI = this.w_CATOMINI
    this.o_FAMINI = this.w_FAMINI
    this.o_MARCINI = this.w_MARCINI
    this.o_GRUPMINI = this.w_GRUPMINI
    this.o_ARTINI = this.w_ARTINI
    this.o_FORABINI = this.w_FORABINI
    this.o_CODAGINI = this.w_CODAGINI
    this.o_CODVEINI = this.w_CODVEINI
    this.o_MASTRINI = this.w_MASTRINI
    this.o_CODESINI = this.w_CODESINI
    this.o_PROVINI = this.w_PROVINI
    this.o_CODCEINI = this.w_CODCEINI
    this.o_VOCCEINI = this.w_VOCCEINI
    this.o_COMMINI = this.w_COMMINI
    this.o_ATTIVINI = this.w_ATTIVINI
    this.o_PAGAMINI = this.w_PAGAMINI
    this.o_UNIMINI = this.w_UNIMINI
    this.o_monete = this.w_monete
    this.o_divisa = this.w_divisa
    this.o_OBSOLETI = this.w_OBSOLETI
    return

enddefine

* --- Define pages as container
define class tgsst_kcrPag1 as StdContainer
  Width  = 606
  height = 471
  stdWidth  = 606
  stdheight = 471
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCECODICE_1_1 as StdField with uid="XNTVZATYPS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CECODICE", cQueryName = "",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice criterio selezionato",;
    HelpContextID = 128595051,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=147, Top=11, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CRIMELAB", cZoomOnZoom="GSST_MCE", oKey_1_1="CECODICE", oKey_1_2="this.w_CECODICE"

  func oCECODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCECODICE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCECODICE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CRIMELAB','*','CECODICE',cp_AbsName(this.parent,'oCECODICE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSST_MCE',"Criteri di elaborazione",'',this.parent.oContained
  endproc
  proc oCECODICE_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSST_MCE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODICE=this.parent.oContained.w_CECODICE
     i_obj.ecpSave()
  endproc

  add object oCEPERIO2_1_2 as StdCheck with uid="PNEOJHQLEU",rtseq=2,rtrep=.f.,left=147, top=36, caption="Secondo",;
    HelpContextID = 125762472,;
    cFormVar="w_CEPERIO2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCEPERIO2_1_2.RadioValue()
    return(iif(this.value =1,'T',;
    'F'))
  endfunc
  func oCEPERIO2_1_2.GetRadio()
    this.Parent.oContained.w_CEPERIO2 = this.RadioValue()
    return .t.
  endfunc

  func oCEPERIO2_1_2.SetRadio()
    this.Parent.oContained.w_CEPERIO2=trim(this.Parent.oContained.w_CEPERIO2)
    this.value = ;
      iif(this.Parent.oContained.w_CEPERIO2=='T',1,;
      0)
  endfunc

  func oCEPERIO2_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATFIN1))
    endwith
   endif
  endfunc

  add object oCEPERIO3_1_3 as StdCheck with uid="SVFGOMBKRS",rtseq=3,rtrep=.f.,left=259, top=36, caption="Terzo",;
    HelpContextID = 125762471,;
    cFormVar="w_CEPERIO3", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCEPERIO3_1_3.RadioValue()
    return(iif(this.value =1,'T',;
    'F'))
  endfunc
  func oCEPERIO3_1_3.GetRadio()
    this.Parent.oContained.w_CEPERIO3 = this.RadioValue()
    return .t.
  endfunc

  func oCEPERIO3_1_3.SetRadio()
    this.Parent.oContained.w_CEPERIO3=trim(this.Parent.oContained.w_CEPERIO3)
    this.value = ;
      iif(this.Parent.oContained.w_CEPERIO3=='T',1,;
      0)
  endfunc

  func oCEPERIO3_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATFIN2))
    endwith
   endif
  endfunc

  add object oDATINI1_1_4 as StdField with uid="GEBVDIRZCB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATINI1", cQueryName = "DATINI1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio periodo di riferimento",;
    HelpContextID = 129679306,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=147, Top=58

  add object oDATFIN1_1_5 as StdField with uid="FDKJOULUDB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATFIN1", cQueryName = "DATFIN1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inferiore alla data precedente",;
    ToolTipText = "Data fine periodo di riferimento",;
    HelpContextID = 51232714,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=147, Top=82

  add object oDATINI2_1_6 as StdField with uid="TBTJFTXIQN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATINI2", cQueryName = "DATINI2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inferiore alla data precedente o data errata (2)",;
    ToolTipText = "Data inizio secondo periodo",;
    HelpContextID = 129679306,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=322, Top=57

  func oDATINI2_1_6.mHide()
    with this.Parent.oContained
      return ( .w_CEPERIO2='F')
    endwith
  endfunc

  add object oDATFIN2_1_7 as StdField with uid="XKRACJIEWG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATFIN2", cQueryName = "DATFIN2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inferiore alla data precedente",;
    ToolTipText = "Data fine secondo periodo",;
    HelpContextID = 51232714,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=322, Top=82

  func oDATFIN2_1_7.mHide()
    with this.Parent.oContained
      return ( .w_CEPERIO2='F')
    endwith
  endfunc

  add object oDATINI3_1_8 as StdField with uid="KCBMVUHWPS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATINI3", cQueryName = "DATINI3",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inferiore alla data precedente o data errata (3)",;
    ToolTipText = "Data inizio terzo periodo",;
    HelpContextID = 138756150,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=497, Top=58

  func oDATINI3_1_8.mHide()
    with this.Parent.oContained
      return ( .w_CEPERIO3='F')
    endwith
  endfunc

  add object oDATFIN3_1_9 as StdField with uid="BLLEPAUPIK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATFIN3", cQueryName = "DATFIN3",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data inferiore alla data precedente o data errata (3b)",;
    ToolTipText = "Data fine terzo periodo",;
    HelpContextID = 217202742,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=497, Top=83

  func oDATFIN3_1_9.mHide()
    with this.Parent.oContained
      return ( .w_CEPERIO3='F')
    endwith
  endfunc

  add object oCATCOMINI_1_10 as StdField with uid="QTFWVHEGUB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CATCOMINI", cQueryName = "CATCOMINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da categoria commerciale",;
    HelpContextID = 206521604,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=109, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOMINI"

  func oCATCOMINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOMINI_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOMINI_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOMINI_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categoria commerciale",'',this.parent.oContained
  endproc
  proc oCATCOMINI_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOMINI
     i_obj.ecpSave()
  endproc

  add object oCATCOMFIN_1_11 as StdField with uid="YWJHHYHVNH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CATCOMFIN", cQueryName = "CATCOMFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Cat. commerciale inferiore alla selezione precedente",;
    ToolTipText = "Selezione fino a categoria commerciale",;
    HelpContextID = 206521679,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=132, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOMFIN"

  func oCATCOMFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOMFIN_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOMFIN_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOMFIN_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categoria commerciale",'',this.parent.oContained
  endproc
  proc oCATCOMFIN_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOMFIN
     i_obj.ecpSave()
  endproc

  add object oDESCCOM_1_14 as StdField with uid="IOMNBGWNOZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCCOM", cQueryName = "DESCCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 40946634,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=108, InputMask=replicate('X',40)

  add object oDESCCOM1_1_15 as StdField with uid="DVVUANWBQA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCCOM1", cQueryName = "DESCCOM1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 40946585,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=132, InputMask=replicate('X',40)

  add object oDESCLI_1_18 as StdField with uid="MKQWMIOCGB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 132172746,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=275, Top=421, InputMask=replicate('X',40)

  func oDESCLI_1_18.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'C')
    endwith
  endfunc

  add object oDESFOR1_1_19 as StdField with uid="AMJWFFJKDE",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESFOR1", cQueryName = "DESFOR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 246270922,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=276, Top=445, InputMask=replicate('X',40)

  func oDESFOR1_1_19.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'F')
    endwith
  endfunc

  add object oDESFOR_1_22 as StdField with uid="UOEYOEXLOK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 246270922,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=275, Top=423, InputMask=replicate('X',40)

  func oDESFOR_1_22.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'F')
    endwith
  endfunc

  add object oDESCLI1_1_23 as StdField with uid="UNLVUPUWJX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCLI1", cQueryName = "DESCLI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 132172746,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=275, Top=447, InputMask=replicate('X',40)

  func oDESCLI1_1_23.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'C')
    endwith
  endfunc

  add object oMAGINI_1_32 as StdField with uid="ELBBSIEYCN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da magazzino",;
    HelpContextID = 129732410,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=156, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzino",'',this.parent.oContained
  endproc
  proc oMAGINI_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MAGINI
     i_obj.ecpSave()
  endproc

  add object oMAGFIN_1_33 as StdField with uid="YXHGIYTAGQ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inferiore alla selezione precedente",;
    ToolTipText = "Selezione fino a magazzino",;
    HelpContextID = 51285818,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=180, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  func oMAGFIN_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzino",'',this.parent.oContained
  endproc
  proc oMAGFIN_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MAGFIN
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_34 as StdField with uid="FIAVDVAMWR",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 176606154,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=156, InputMask=replicate('X',40)

  add object oDESMAG1_1_35 as StdField with uid="HIPFJJROAS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESMAG1", cQueryName = "DESMAG1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 176606154,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=180, InputMask=replicate('X',40)

  add object oAGEINI_1_38 as StdField with uid="QCBRHDZVRV",rtseq=28,rtrep=.f.,;
    cFormVar = "w_AGEINI", cQueryName = "AGEINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente obsoleto",;
    ToolTipText = "Selezione da agente",;
    HelpContextID = 129739258,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=204, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGEINI"

  func oAGEINI_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGEINI_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGEINI_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGEINI_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'Gsst_age.AGENTI_VZM',this.parent.oContained
  endproc
  proc oAGEINI_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGEINI
     i_obj.ecpSave()
  endproc

  add object oAGEFIN_1_39 as StdField with uid="GBXRMIGMSQ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_AGEFIN", cQueryName = "AGEFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente inferiore alla selezione precedente",;
    ToolTipText = "Selezione fino ad agente",;
    HelpContextID = 51292666,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=228, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGEFIN"

  func oAGEFIN_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGEFIN_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGEFIN_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGEFIN_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'Gsst_age.AGENTI_VZM',this.parent.oContained
  endproc
  proc oAGEFIN_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGEFIN
     i_obj.ecpSave()
  endproc

  add object oDESAGE_1_40 as StdField with uid="SOLZGVZAFM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 204655562,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=204, InputMask=replicate('X',40)

  add object oDESAGE1_1_41 as StdField with uid="FLGWEUNZEZ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESAGE1", cQueryName = "DESAGE1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 204655562,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=228, InputMask=replicate('X',40)

  add object oZONINI_1_44 as StdField with uid="SEIBHNHZZU",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ZONINI", cQueryName = "ZONINI",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da zona",;
    HelpContextID = 129699946,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=252, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_ZONINI"

  func oZONINI_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oZONINI_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZONINI_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oZONINI_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zona",'',this.parent.oContained
  endproc
  proc oZONINI_1_44.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_ZONINI
     i_obj.ecpSave()
  endproc

  add object oZONFIN_1_45 as StdField with uid="CLCVFHIFLS",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ZONFIN", cQueryName = "ZONFIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Zona inferiore alla selezione precedente",;
    ToolTipText = "Selezione fino a zona",;
    HelpContextID = 51253354,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=276, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_ZONFIN"

  func oZONFIN_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oZONFIN_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZONFIN_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oZONFIN_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zona",'',this.parent.oContained
  endproc
  proc oZONFIN_1_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_ZONFIN
     i_obj.ecpSave()
  endproc

  add object oDESZON_1_46 as StdField with uid="OIYQFQDSRF",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 43633610,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=252, InputMask=replicate('X',40)

  add object oDESZON1_1_47 as StdField with uid="MPVBYQZRAE",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESZON1", cQueryName = "DESZON1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 43633610,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=276, InputMask=replicate('X',40)

  add object oCATCONINI_1_50 as StdField with uid="XMAKEVVNHO",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CATCONINI", cQueryName = "CATCONINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "selezione da cat. contabile",;
    HelpContextID = 223298820,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=300, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_CATCONINI"

  func oCATCONINI_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCONINI_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCONINI_1_50.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oCATCONINI_1_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categoria contabile",'',this.parent.oContained
  endproc
  proc oCATCONINI_1_50.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_CATCONINI
     i_obj.ecpSave()
  endproc

  add object oCATCONFIN_1_51 as StdField with uid="VYNKTJIWOX",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CATCONFIN", cQueryName = "CATCONFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Cat. contabile inferiore alla selezione precedente",;
    ToolTipText = "Selezione fino a cat. contabile",;
    HelpContextID = 223298895,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=324, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_CATCONFIN"

  func oCATCONFIN_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCONFIN_1_51.ecpDrop(oSource)
    this.Parent.oContained.link_1_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCONFIN_1_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oCATCONFIN_1_51'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categoria contabile",'',this.parent.oContained
  endproc
  proc oCATCONFIN_1_51.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_CATCONFIN
     i_obj.ecpSave()
  endproc

  add object oDESCCON_1_52 as StdField with uid="HEOAHSPZSV",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESCCON", cQueryName = "DESCCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 40946634,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=300, InputMask=replicate('X',40)

  add object oDESCCON1_1_53 as StdField with uid="QWVNRPTHKD",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCCON1", cQueryName = "DESCCON1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 40946585,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=213, Top=324, InputMask=replicate('X',40)


  add object oCLIFOR_1_59 as StdCombo with uid="FYIUIBXSNX",rtseq=40,rtrep=.f.,left=147,top=348,width=136,height=21;
    , ToolTipText = "Cliente/Fornitore";
    , HelpContextID = 246310106;
    , cFormVar="w_CLIFOR",RowSource=""+"Cliente,"+"Fornitore,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLIFOR_1_59.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    ''))))
  endfunc
  func oCLIFOR_1_59.GetRadio()
    this.Parent.oContained.w_CLIFOR = this.RadioValue()
    return .t.
  endfunc

  func oCLIFOR_1_59.SetRadio()
    this.Parent.oContained.w_CLIFOR=trim(this.Parent.oContained.w_CLIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CLIFOR=='C',1,;
      iif(this.Parent.oContained.w_CLIFOR=='F',2,;
      iif(this.Parent.oContained.w_CLIFOR=='N',3,;
      0)))
  endfunc

  add object oCODCLINI_1_60 as StdField with uid="ZFQVSMWGQX",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CODCLINI", cQueryName = "CODCLINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il cliente iniziale � maggiore di quello finale oppure inesistente",;
    ToolTipText = "Selezione da cliente",;
    HelpContextID = 132231569,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=147, Top=423, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLINI"

  func oCODCLINI_1_60.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'C')
    endwith
  endfunc

  func oCODCLINI_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_60('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLINI_1_60.ecpDrop(oSource)
    this.Parent.oContained.link_1_60('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLINI_1_60.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLI)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLINI_1_60'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Cliente",'',this.parent.oContained
  endproc
  proc oCODCLINI_1_60.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLI
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLINI
     i_obj.ecpSave()
  endproc

  add object oCODCLIFIN_1_61 as StdField with uid="UZDNGUWLJH",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CODCLIFIN", cQueryName = "CODCLIFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il cliente finale � minore di quello iniziale oppure inesistente",;
    ToolTipText = "Selezione fino a cliente",;
    HelpContextID = 136205135,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=147, Top=446, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLIFIN"

  func oCODCLIFIN_1_61.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'C')
    endwith
  endfunc

  func oCODCLIFIN_1_61.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_61('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLIFIN_1_61.ecpDrop(oSource)
    this.Parent.oContained.link_1_61('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLIFIN_1_61.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLI)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLIFIN_1_61'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Cliente",'',this.parent.oContained
  endproc
  proc oCODCLIFIN_1_61.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLI
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLIFIN
     i_obj.ecpSave()
  endproc

  add object oCODFORINI_1_62 as StdField with uid="YZFORQGYCI",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CODFORINI", cQueryName = "CODFORINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il fornitore iniziale � maggiore di quello finale oppure inesistente",;
    ToolTipText = "Selezione da fornitore",;
    HelpContextID = 22106884,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=147, Top=422, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFORINI"

  func oCODFORINI_1_62.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'F')
    endwith
  endfunc

  func oCODFORINI_1_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFORINI_1_62.ecpDrop(oSource)
    this.Parent.oContained.link_1_62('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFORINI_1_62.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFORINI_1_62'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitore",'',this.parent.oContained
  endproc
  proc oCODFORINI_1_62.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFORINI
     i_obj.ecpSave()
  endproc

  add object oCODFORFIN_1_64 as StdField with uid="WPJZCXKSEK",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CODFORFIN", cQueryName = "CODFORFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il fornitore finale � minore di quello iniziale oppure inesistente",;
    ToolTipText = "Selezione fino a fonitore",;
    HelpContextID = 22106959,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=147, Top=447, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFORFIN"

  func oCODFORFIN_1_64.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'F')
    endwith
  endfunc

  func oCODFORFIN_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFORFIN_1_64.ecpDrop(oSource)
    this.Parent.oContained.link_1_64('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFORFIN_1_64.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFORFIN_1_64'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitore",'',this.parent.oContained
  endproc
  proc oCODFORFIN_1_64.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFORFIN
     i_obj.ecpSave()
  endproc

  add object oCODORNINI_1_65 as StdField with uid="FUVSEIYSHK",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CODORNINI", cQueryName = "CODORNINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da per conto di",;
    HelpContextID = 227169028,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=147, Top=375, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPORN", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODORNINI"

  func oCODORNINI_1_65.mHide()
    with this.Parent.oContained
      return (.w_TIPORN = 'G')
    endwith
  endfunc

  func oCODORNINI_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_65('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODORNINI_1_65.ecpDrop(oSource)
    this.Parent.oContained.link_1_65('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODORNINI_1_65.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPORN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPORN)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODORNINI_1_65'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Per conto di",'',this.parent.oContained
  endproc
  proc oCODORNINI_1_65.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPORN
     i_obj.w_ANCODICE=this.parent.oContained.w_CODORNINI
     i_obj.ecpSave()
  endproc

  add object oCODORNFIN_1_66 as StdField with uid="KYHOBMYOUT",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CODORNFIN", cQueryName = "CODORNFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Selezione inferiore al per conto di precedente",;
    ToolTipText = "Selezione fino a per conto di",;
    HelpContextID = 227169103,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=147, Top=399, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPORN", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODORNFIN"

  func oCODORNFIN_1_66.mHide()
    with this.Parent.oContained
      return (.w_TIPORN = 'G')
    endwith
  endfunc

  func oCODORNFIN_1_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_66('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODORNFIN_1_66.ecpDrop(oSource)
    this.Parent.oContained.link_1_66('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODORNFIN_1_66.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPORN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPORN)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODORNFIN_1_66'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Per conto di",'',this.parent.oContained
  endproc
  proc oCODORNFIN_1_66.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPORN
     i_obj.w_ANCODICE=this.parent.oContained.w_CODORNFIN
     i_obj.ecpSave()
  endproc

  add object oCEDESCRI_1_68 as StdField with uid="MXUWESCSEM",rtseq=48,rtrep=.f.,;
    cFormVar = "w_CEDESCRI", cQueryName = "CEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 225426321,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=373, Left=229, Top=11, InputMask=replicate('X',50)

  add object oDESORN_1_75 as StdField with uid="TGPQIAPQPE",rtseq=131,rtrep=.f.,;
    cFormVar = "w_DESORN", cQueryName = "DESORN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 41208778,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=275, Top=375, InputMask=replicate('X',40)

  func oDESORN_1_75.mHide()
    with this.Parent.oContained
      return (.w_TIPORN = 'G')
    endwith
  endfunc

  add object oDESORN1_1_76 as StdField with uid="ZPCAVTIUDW",rtseq=132,rtrep=.f.,;
    cFormVar = "w_DESORN1", cQueryName = "DESORN1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 41208778,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=275, Top=399, InputMask=replicate('X',40)

  func oDESORN1_1_76.mHide()
    with this.Parent.oContained
      return (.w_TIPORN = 'G')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="YWOTIMSYUC",Visible=.t., Left=2, Top=109,;
    Alignment=1, Width=143, Height=15,;
    Caption="Da cat. commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="DSXHWGMFWO",Visible=.t., Left=2, Top=132,;
    Alignment=1, Width=143, Height=15,;
    Caption="A cat. commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="NYKRHLJSUM",Visible=.t., Left=2, Top=425,;
    Alignment=1, Width=143, Height=15,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'C')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="ZGGVXKNBWK",Visible=.t., Left=2, Top=450,;
    Alignment=1, Width=143, Height=15,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'C')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="NVWBQYOTHW",Visible=.t., Left=2, Top=425,;
    Alignment=1, Width=143, Height=15,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'F')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="XYDSHKXDQJ",Visible=.t., Left=2, Top=450,;
    Alignment=1, Width=143, Height=15,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR <> 'F')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="UMWYRLMSNX",Visible=.t., Left=2, Top=156,;
    Alignment=1, Width=143, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HZVHQQGRSB",Visible=.t., Left=2, Top=182,;
    Alignment=1, Width=143, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="RFPTDVRJCJ",Visible=.t., Left=2, Top=204,;
    Alignment=1, Width=143, Height=15,;
    Caption="Da agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="TWRACOOXKC",Visible=.t., Left=2, Top=228,;
    Alignment=1, Width=143, Height=15,;
    Caption="Ad agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="YIVJCXWDTZ",Visible=.t., Left=2, Top=252,;
    Alignment=1, Width=143, Height=15,;
    Caption="Da zona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="YXRXQJGWAJ",Visible=.t., Left=2, Top=276,;
    Alignment=1, Width=143, Height=15,;
    Caption="A zona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="KPJUPAIHJL",Visible=.t., Left=2, Top=300,;
    Alignment=1, Width=143, Height=15,;
    Caption="Da cat. contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="DWCIVWKLLQ",Visible=.t., Left=2, Top=324,;
    Alignment=1, Width=143, Height=15,;
    Caption="A cat. contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="AHQNHFOTUB",Visible=.t., Left=2, Top=60,;
    Alignment=1, Width=143, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="EXKMMHWLDY",Visible=.t., Left=2, Top=84,;
    Alignment=1, Width=143, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="FAWFNPYLXR",Visible=.t., Left=238, Top=60,;
    Alignment=1, Width=81, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return ( .w_CEPERIO2='F')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="PUYXHMJCAT",Visible=.t., Left=238, Top=84,;
    Alignment=1, Width=81, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return ( .w_CEPERIO2='F')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="NJYNUGLHSN",Visible=.t., Left=413, Top=61,;
    Alignment=1, Width=81, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return ( .w_CEPERIO3='F')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="AYMTFRSADX",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=143, Height=15,;
    Caption="Criterio selezionato:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="HZPDRABPQF",Visible=.t., Left=2, Top=36,;
    Alignment=1, Width=143, Height=15,;
    Caption="Periodi di confronto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="ZCYCUYCUFQ",Visible=.t., Left=2, Top=349,;
    Alignment=1, Width=143, Height=15,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="LNWXRJQIYB",Visible=.t., Left=413, Top=85,;
    Alignment=1, Width=81, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return ( .w_CEPERIO3='F')
    endwith
  endfunc

  add object oStr_1_73 as StdString with uid="ODFUDHZVED",Visible=.t., Left=2, Top=376,;
    Alignment=1, Width=143, Height=18,;
    Caption="Da per conto di:"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (.w_TIPORN = 'G')
    endwith
  endfunc

  add object oStr_1_74 as StdString with uid="GKUNPYXPJY",Visible=.t., Left=2, Top=401,;
    Alignment=1, Width=143, Height=18,;
    Caption="A per conto di:"  ;
  , bGlobalFont=.t.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (.w_TIPORN = 'G')
    endwith
  endfunc
enddefine
define class tgsst_kcrPag2 as StdContainer
  Width  = 606
  height = 471
  stdWidth  = 606
  stdheight = 471
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESCOMO_2_3 as StdField with uid="EQCMMRIXSQ",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESCOMO", cQueryName = "DESCOMO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61918154,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=12, InputMask=replicate('X',40)

  add object oDESCOMO1_2_4 as StdField with uid="ESKRJKWGFY",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESCOMO1", cQueryName = "DESCOMO1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61918105,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=36, InputMask=replicate('X',40)

  add object oDESCFAM_2_7 as StdField with uid="LMRKUTFPCG",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESCFAM", cQueryName = "DESCFAM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 4246474,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=60, InputMask=replicate('X',40)

  add object oDESCFAM1_2_8 as StdField with uid="BELJDFTHUG",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESCFAM1", cQueryName = "DESCFAM1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 4246425,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=84, InputMask=replicate('X',40)

  add object oCATOMINI_2_9 as StdField with uid="ZMBNEGWELS",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CATOMINI", cQueryName = "CATOMINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da cat. omogenea",;
    HelpContextID = 130334609,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=12, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOMINI"

  func oCATOMINI_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOMINI_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOMINI_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOMINI_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categoria omogenea",'',this.parent.oContained
  endproc
  proc oCATOMINI_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOMINI
     i_obj.ecpSave()
  endproc

  add object oCATOMFIN_2_10 as StdField with uid="JBZMSALEBK",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CATOMFIN", cQueryName = "CATOMFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Cat. omogenea inferiore alla precedente",;
    ToolTipText = "Selezione fino a cat. omogenea",;
    HelpContextID = 87769204,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOMFIN"

  func oCATOMFIN_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOMFIN_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOMFIN_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOMFIN_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categoria omogenea",'',this.parent.oContained
  endproc
  proc oCATOMFIN_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOMFIN
     i_obj.ecpSave()
  endproc

  add object oFAMINI_2_11 as StdField with uid="LGYSXNLZCP",rtseq=56,rtrep=.f.,;
    cFormVar = "w_FAMINI", cQueryName = "FAMINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da famiglia",;
    HelpContextID = 129707946,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=60, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMINI"

  func oFAMINI_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMINI_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMINI_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMINI_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglia",'',this.parent.oContained
  endproc
  proc oFAMINI_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMINI
     i_obj.ecpSave()
  endproc

  add object oFAMFIN_2_12 as StdField with uid="PRLEKROEPL",rtseq=57,rtrep=.f.,;
    cFormVar = "w_FAMFIN", cQueryName = "FAMFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Famiglia inferiore alla selezione precedente",;
    ToolTipText = "Selezione fino a famiglia",;
    HelpContextID = 51261354,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMFIN"

  func oFAMFIN_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMFIN_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMFIN_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMFIN_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglia",'',this.parent.oContained
  endproc
  proc oFAMFIN_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMFIN
     i_obj.ecpSave()
  endproc

  add object oMARCINI_2_13 as StdField with uid="DHBNPLQXVS",rtseq=58,rtrep=.f.,;
    cFormVar = "w_MARCINI", cQueryName = "MARCINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da marca",;
    HelpContextID = 216998086,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARCINI"

  func oMARCINI_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARCINI_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARCINI_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARCINI_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchio",'',this.parent.oContained
  endproc
  proc oMARCINI_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARCINI
     i_obj.ecpSave()
  endproc

  add object oMARCFIN_2_14 as StdField with uid="ERNKLVQVYV",rtseq=59,rtrep=.f.,;
    cFormVar = "w_MARCFIN", cQueryName = "MARCFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Marchio inferiore alla selezione precedente",;
    ToolTipText = "Selezione fino a marca",;
    HelpContextID = 138469178,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=132, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARCFIN"

  func oMARCFIN_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARCFIN_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARCFIN_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARCFIN_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchio",'',this.parent.oContained
  endproc
  proc oMARCFIN_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARCFIN
     i_obj.ecpSave()
  endproc

  add object oDESGRUP_2_17 as StdField with uid="AFIQHCLAJC",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESGRUP", cQueryName = "DESGRUP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 192728010,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=156, InputMask=replicate('X',40)

  add object oDESGRUP1_2_18 as StdField with uid="UGJSQFASAF",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESGRUP1", cQueryName = "DESGRUP1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 192727961,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=180, InputMask=replicate('X',40)

  add object oGRUPMINI_2_19 as StdField with uid="XRLNRVIPGC",rtseq=62,rtrep=.f.,;
    cFormVar = "w_GRUPMINI", cQueryName = "GRUPMINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da gruppo merceologico",;
    HelpContextID = 130260561,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=156, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUPMINI"

  func oGRUPMINI_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPMINI_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPMINI_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUPMINI_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppo merceologico",'',this.parent.oContained
  endproc
  proc oGRUPMINI_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUPMINI
     i_obj.ecpSave()
  endproc

  add object oGRUPMFIN_2_20 as StdField with uid="TCSHLOERAN",rtseq=63,rtrep=.f.,;
    cFormVar = "w_GRUPMFIN", cQueryName = "GRUPMFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo merceologico inferiore alla selezione precedente",;
    ToolTipText = "Selezione fino a gruppo merceologico",;
    HelpContextID = 87843252,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=180, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUPMFIN"

  func oGRUPMFIN_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPMFIN_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPMFIN_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUPMFIN_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppo merceologico",'',this.parent.oContained
  endproc
  proc oGRUPMFIN_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUPMFIN
     i_obj.ecpSave()
  endproc

  add object oDESMARCA_2_23 as StdField with uid="AZDBQYOIWK",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESMARCA", cQueryName = "DESMARCA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 7943287,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=108, InputMask=replicate('X',40)

  add object oDESMARCA1_2_24 as StdField with uid="TOQYIQIYHX",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESMARCA1", cQueryName = "DESMARCA1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 7944071,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=206, Top=132, InputMask=replicate('X',40)

  add object oDESCART_2_27 as StdField with uid="SVUCNLXHDE",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESCART", cQueryName = "DESCART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 7287862,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=204, InputMask=replicate('X',40)

  add object oDESCART1_2_28 as StdField with uid="YFGQGUZEJZ",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESCART1", cQueryName = "DESCART1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 7287911,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=228, InputMask=replicate('X',40)

  add object oARTINI_2_29 as StdField with uid="ZRMFVFCJHH",rtseq=68,rtrep=.f.,;
    cFormVar = "w_ARTINI", cQueryName = "ARTINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo\servizio di tipo non corretto",;
    ToolTipText = "Selezione da articolo",;
    HelpContextID = 129675002,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=204, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTINI"

  func oARTINI_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTINI_2_29.ecpDrop(oSource)
    this.Parent.oContained.link_2_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTINI_2_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTINI_2_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articolo\servizio",'Gsst_art.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oARTINI_2_29.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTINI
     i_obj.ecpSave()
  endproc

  add object oARTFIN_2_30 as StdField with uid="PTYUHZWUNM",rtseq=69,rtrep=.f.,;
    cFormVar = "w_ARTFIN", cQueryName = "ARTFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo inferiore alla selezione precedente",;
    ToolTipText = "Selezione fino ad articolo",;
    HelpContextID = 51228410,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=228, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTFIN"

  func oARTFIN_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTFIN_2_30.ecpDrop(oSource)
    this.Parent.oContained.link_2_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTFIN_2_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTFIN_2_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articolo\servizio",'Gsst_art.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oARTFIN_2_30.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTFIN
     i_obj.ecpSave()
  endproc

  add object oFORABINI_2_32 as StdField with uid="XJQAHVJVMW",rtseq=71,rtrep=.f.,;
    cFormVar = "w_FORABINI", cQueryName = "FORABINI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da fornitore abituale",;
    HelpContextID = 142791009,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=252, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORABINI"

  func oFORABINI_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORABINI_2_32.ecpDrop(oSource)
    this.Parent.oContained.link_2_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORABINI_2_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORABINI_2_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitori abituali",'',this.parent.oContained
  endproc
  proc oFORABINI_2_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_FORABINI
     i_obj.ecpSave()
  endproc

  add object oCODAGINI_2_33 as StdField with uid="KLDQDMECPL",rtseq=72,rtrep=.f.,;
    cFormVar = "w_CODAGINI", cQueryName = "CODAGINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente non capo area",;
    ToolTipText = "Selezione da capo area",;
    HelpContextID = 137605521,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=300, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGINI"

  func oCODAGINI_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGINI_2_33.ecpDrop(oSource)
    this.Parent.oContained.link_2_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGINI_2_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGINI_2_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Capo area",'GSAR1AGE.AGENTI_VZM',this.parent.oContained
  endproc
  proc oCODAGINI_2_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODAGINI
     i_obj.ecpSave()
  endproc

  add object oCODAGFIN_2_34 as StdField with uid="GSDNTERZPB",rtseq=73,rtrep=.f.,;
    cFormVar = "w_CODAGFIN", cQueryName = "CODAGFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezione inferiore al capo area precedente o agente non capo area",;
    ToolTipText = "Selezione fino a capo area",;
    HelpContextID = 80498292,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=324, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGFIN"

  func oCODAGFIN_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGFIN_2_34.ecpDrop(oSource)
    this.Parent.oContained.link_2_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGFIN_2_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGFIN_2_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Capo area",'GSAR1AGE.AGENTI_VZM',this.parent.oContained
  endproc
  proc oCODAGFIN_2_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODAGFIN
     i_obj.ecpSave()
  endproc

  add object oCODVEINI_2_35 as StdField with uid="NAIWVLBYFP",rtseq=74,rtrep=.f.,;
    cFormVar = "w_CODVEINI", cQueryName = "CODVEINI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da vettore",;
    HelpContextID = 138326417,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=348, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_CODVEINI"

  func oCODVEINI_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVEINI_2_35.ecpDrop(oSource)
    this.Parent.oContained.link_2_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVEINI_2_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oCODVEINI_2_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"Vettori",'',this.parent.oContained
  endproc
  proc oCODVEINI_2_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_CODVEINI
     i_obj.ecpSave()
  endproc

  add object oCODVEFIN_2_36 as StdField with uid="FUSFFVHQMD",rtseq=75,rtrep=.f.,;
    cFormVar = "w_CODVEFIN", cQueryName = "CODVEFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezione inferiore al vettore precedente",;
    ToolTipText = "Selezione fino a vettore",;
    HelpContextID = 79777396,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=372, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_CODVEFIN"

  func oCODVEFIN_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVEFIN_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVEFIN_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oCODVEFIN_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"Vettori",'',this.parent.oContained
  endproc
  proc oCODVEFIN_2_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_CODVEFIN
     i_obj.ecpSave()
  endproc

  add object oMASTRINI_2_37 as StdField with uid="BPRHXFPVXO",rtseq=76,rtrep=.f.,;
    cFormVar = "w_MASTRINI", cQueryName = "MASTRINI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da mastro",;
    HelpContextID = 124767985,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=396, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_MASTRINI"

  func oMASTRINI_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oMASTRINI_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMASTRINI_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oMASTRINI_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'',this.parent.oContained
  endproc
  proc oMASTRINI_2_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_MASTRINI
     i_obj.ecpSave()
  endproc

  add object oMASTRFIN_2_38 as StdField with uid="IIXLOLUVHH",rtseq=77,rtrep=.f.,;
    cFormVar = "w_MASTRFIN", cQueryName = "MASTRFIN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Selezione inferiore al fornitore precedente",;
    ToolTipText = "Selezione fino a mastro",;
    HelpContextID = 93335828,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=420, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_MASTRFIN"

  func oMASTRFIN_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oMASTRFIN_2_38.ecpDrop(oSource)
    this.Parent.oContained.link_2_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMASTRFIN_2_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oMASTRFIN_2_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'',this.parent.oContained
  endproc
  proc oMASTRFIN_2_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_MASTRFIN
     i_obj.ecpSave()
  endproc

  add object oDESFORAB_2_40 as StdField with uid="LOXOTFCFFB",rtseq=120,rtrep=.f.,;
    cFormVar = "w_DESFORAB", cQueryName = "DESFORAB",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 246270856,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=252, InputMask=replicate('X',40)

  add object oDESFORAB1_2_41 as StdField with uid="JRFMNXBPDK",rtseq=121,rtrep=.f.,;
    cFormVar = "w_DESFORAB1", cQueryName = "DESFORAB1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 246270072,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=276, InputMask=replicate('X',40)

  add object oFORABFIN_2_42 as StdField with uid="LZTIWBPPQC",rtseq=122,rtrep=.f.,;
    cFormVar = "w_FORABFIN", cQueryName = "FORABFIN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Selezione inferiore al fornitore precedente",;
    ToolTipText = "Selezione fino a fonitore abituale",;
    HelpContextID = 75312804,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=138, Top=276, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORABFIN"

  func oFORABFIN_2_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORABFIN_2_42.ecpDrop(oSource)
    this.Parent.oContained.link_2_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORABFIN_2_42.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORABFIN_2_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitori abituali",'',this.parent.oContained
  endproc
  proc oFORABFIN_2_42.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_FORABFIN
     i_obj.ecpSave()
  endproc

  add object oDESAG2_2_45 as StdField with uid="GDSMAKRPQT",rtseq=123,rtrep=.f.,;
    cFormVar = "w_DESAG2", cQueryName = "DESAG2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 254987210,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=300, InputMask=replicate('X',35)

  add object oDESAG21_2_46 as StdField with uid="KDPPHLKWSR",rtseq=124,rtrep=.f.,;
    cFormVar = "w_DESAG21", cQueryName = "DESAG21",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 254987210,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=324, InputMask=replicate('X',35)

  add object oDESVET_2_49 as StdField with uid="LXCKKLGKIU",rtseq=125,rtrep=.f.,;
    cFormVar = "w_DESVET", cQueryName = "DESVET",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 222153674,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=348, InputMask=replicate('X',35)

  add object oDESVET1_2_50 as StdField with uid="PEXIOLZIEB",rtseq=126,rtrep=.f.,;
    cFormVar = "w_DESVET1", cQueryName = "DESVET1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 222153674,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=372, InputMask=replicate('X',35)

  add object oDESMAS_2_53 as StdField with uid="NNUHZCJZXZ",rtseq=127,rtrep=.f.,;
    cFormVar = "w_DESMAS", cQueryName = "DESMAS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243715018,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=396, InputMask=replicate('X',40)

  add object oDESMAS1_2_54 as StdField with uid="WZXRBAYJRS",rtseq=128,rtrep=.f.,;
    cFormVar = "w_DESMAS1", cQueryName = "DESMAS1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243715018,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=299, Top=420, InputMask=replicate('X',40)


  add object oOBSOLETI_2_57 as StdCombo with uid="BCIGPJPONM",rtseq=138,rtrep=.f.,left=138,top=444,width=111,height=21;
    , ToolTipText = "Stampa solo il valore degli articoli (solo obsoleti, obsoleti, tutti)";
    , HelpContextID = 69939759;
    , cFormVar="w_OBSOLETI",RowSource=""+"Solo obsoleti,"+"Non obsoleti,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOBSOLETI_2_57.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oOBSOLETI_2_57.GetRadio()
    this.Parent.oContained.w_OBSOLETI = this.RadioValue()
    return .t.
  endfunc

  func oOBSOLETI_2_57.SetRadio()
    this.Parent.oContained.w_OBSOLETI=trim(this.Parent.oContained.w_OBSOLETI)
    this.value = ;
      iif(this.Parent.oContained.w_OBSOLETI=='S',1,;
      iif(this.Parent.oContained.w_OBSOLETI=='N',2,;
      iif(this.Parent.oContained.w_OBSOLETI=='T',3,;
      0)))
  endfunc

  add object oStr_2_1 as StdString with uid="VPERBAHORM",Visible=.t., Left=2, Top=12,;
    Alignment=1, Width=133, Height=15,;
    Caption="Da cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="TSETDSNZTG",Visible=.t., Left=2, Top=36,;
    Alignment=1, Width=133, Height=15,;
    Caption="A cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="LCQKRCUCHM",Visible=.t., Left=2, Top=60,;
    Alignment=1, Width=133, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="WPJMIVSVRG",Visible=.t., Left=2, Top=84,;
    Alignment=1, Width=133, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="JSMMBGCAQQ",Visible=.t., Left=2, Top=156,;
    Alignment=1, Width=133, Height=15,;
    Caption="Da gr. merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="PVZDJMILSA",Visible=.t., Left=2, Top=180,;
    Alignment=1, Width=133, Height=15,;
    Caption="A gr. merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="GNYIOUMTLT",Visible=.t., Left=2, Top=108,;
    Alignment=1, Width=133, Height=15,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="LOVQIMYOJJ",Visible=.t., Left=2, Top=132,;
    Alignment=1, Width=133, Height=15,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="GPJDNTZWXP",Visible=.t., Left=2, Top=205,;
    Alignment=1, Width=133, Height=18,;
    Caption="Da articolo\servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="HKBPJJSZIF",Visible=.t., Left=2, Top=229,;
    Alignment=1, Width=133, Height=18,;
    Caption="Ad articolo\servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="NUYRFTFHAC",Visible=.t., Left=2, Top=253,;
    Alignment=1, Width=133, Height=18,;
    Caption="Da fornitore abituale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="TMACTUQSDA",Visible=.t., Left=2, Top=277,;
    Alignment=1, Width=133, Height=18,;
    Caption="A fornitore abituale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="SOHQOXHMVF",Visible=.t., Left=2, Top=301,;
    Alignment=1, Width=133, Height=18,;
    Caption="Da capo area:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="RWFJRBSBEJ",Visible=.t., Left=2, Top=325,;
    Alignment=1, Width=133, Height=18,;
    Caption="A capo area:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="QJBVAGYSVX",Visible=.t., Left=2, Top=349,;
    Alignment=1, Width=133, Height=18,;
    Caption="Da primo vettore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="XYOAJXBSWA",Visible=.t., Left=2, Top=373,;
    Alignment=1, Width=133, Height=18,;
    Caption="A primo vettore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="QHZDXVPYMZ",Visible=.t., Left=2, Top=397,;
    Alignment=1, Width=133, Height=18,;
    Caption="Da mastro contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="BVGLZSPSGW",Visible=.t., Left=2, Top=421,;
    Alignment=1, Width=133, Height=18,;
    Caption="A mastro contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_58 as StdString with uid="TZGFGFBJEK",Visible=.t., Left=73, Top=448,;
    Alignment=1, Width=62, Height=18,;
    Caption="Articoli:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsst_kcrPag3 as StdContainer
  Width  = 606
  height = 471
  stdWidth  = 606
  stdheight = 471
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESINI_3_3 as StdField with uid="FUGCPAKFCP",rtseq=81,rtrep=.f.,;
    cFormVar = "w_CODESINI", cQueryName = "CODESINI",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice destinazione inesistente o di tipo diverso da consegna",;
    HelpContextID = 124760465,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=118, Top=11, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPORN", oKey_2_1="DDCODICE", oKey_2_2="this.w_CODCONINI", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODESINI"

  func oCODESINI_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODCONINI) AND .w_CODCONINI=.w_CODCONFIN)
    endwith
   endif
  endfunc

  proc oCODESINI_3_3.mAfter
    with this.Parent.oContained
      .w_CODESINI=chkdesdiv(.w_CODESINI, .w_TIPORN, .w_CODCONINI)
    endwith
  endproc

  func oCODESINI_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESINI_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESINI_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPORN)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCONINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPORN)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODCONINI)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODESINI_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Destinazione",'GSVE_MDV.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oCODESFIN_3_4 as StdField with uid="KMBEERYDYQ",rtseq=82,rtrep=.f.,;
    cFormVar = "w_CODESFIN", cQueryName = "CODESFIN",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Destinazione inferiore alla selezione precedente o inesistente o di tipo diverso da consegna",;
    HelpContextID = 93343348,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=118, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPORN", oKey_2_1="DDCODICE", oKey_2_2="this.w_CODCONINI", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODESFIN"

  func oCODESFIN_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODCONINI) AND .w_CODCONINI=.w_CODCONFIN)
    endwith
   endif
  endfunc

  proc oCODESFIN_3_4.mAfter
    with this.Parent.oContained
      .w_CODESFIN=chkdesdiv(.w_CODESFIN, .w_TIPORN, .w_CODCONINI)
    endwith
  endproc

  func oCODESFIN_3_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESFIN_3_4.ecpDrop(oSource)
    this.Parent.oContained.link_3_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESFIN_3_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPORN)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCONINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPORN)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODCONINI)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODESFIN_3_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Destinazione",'GSVE_MDV.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oPROVINI_3_5 as StdField with uid="YPLARIEMXH",rtseq=83,rtrep=.f.,;
    cFormVar = "w_PROVINI", cQueryName = "PROVINI",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 218235382,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=561, Top=11, InputMask=replicate('X',2)

  add object oPROVFIN_3_6 as StdField with uid="BLRRROYWAY",rtseq=84,rtrep=.f.,;
    cFormVar = "w_PROVFIN", cQueryName = "PROVFIN",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Provincia inferiore alla selezione precedente",;
    HelpContextID = 137231882,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=561, Top=35, InputMask=replicate('X',2)

  func oPROVFIN_3_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PROVINI<=.w_PROVFIN)
    endwith
    return bRes
  endfunc

  add object oCODCEINI_3_7 as StdField with uid="BAIWAGZATA",rtseq=85,rtrep=.f.,;
    cFormVar = "w_CODCEINI", cQueryName = "CODCEINI",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da centro di costo",;
    HelpContextID = 139571601,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=61, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CODCEINI"

  func oCODCEINI_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oCODCEINI_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCEINI_3_7.ecpDrop(oSource)
    this.Parent.oContained.link_3_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCEINI_3_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCODCEINI_3_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo e ricavo",'',this.parent.oContained
  endproc
  proc oCODCEINI_3_7.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CODCEINI
     i_obj.ecpSave()
  endproc

  add object oCODCEFIN_3_8 as StdField with uid="WIMNARWGWU",rtseq=86,rtrep=.f.,;
    cFormVar = "w_CODCEFIN", cQueryName = "CODCEFIN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di costo inferiore alla selezione precedente",;
    ToolTipText = "Selezione a centro di costo",;
    HelpContextID = 78532212,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=86, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CODCEFIN"

  func oCODCEFIN_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oCODCEFIN_3_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCEFIN_3_8.ecpDrop(oSource)
    this.Parent.oContained.link_3_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCEFIN_3_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCODCEFIN_3_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo e ricavo",'',this.parent.oContained
  endproc
  proc oCODCEFIN_3_8.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CODCEFIN
     i_obj.ecpSave()
  endproc

  add object oVOCCEINI_3_9 as StdField with uid="ORWUTLYNRI",rtseq=87,rtrep=.f.,;
    cFormVar = "w_VOCCEINI", cQueryName = "VOCCEINI",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da voce di costo",;
    HelpContextID = 139575393,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=110, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_VOCCEINI"

  func oVOCCEINI_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S'  OR g_COMM='S'))
    endwith
   endif
  endfunc

  func oVOCCEINI_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCCEINI_3_9.ecpDrop(oSource)
    this.Parent.oContained.link_3_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCCEINI_3_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oVOCCEINI_3_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voce di costo e ricavo",'',this.parent.oContained
  endproc
  proc oVOCCEINI_3_9.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_VOCCEINI
     i_obj.ecpSave()
  endproc

  add object oVOCCEFIN_3_10 as StdField with uid="HZXEZXKZKH",rtseq=88,rtrep=.f.,;
    cFormVar = "w_VOCCEFIN", cQueryName = "VOCCEFIN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Voce di costo inferiore alla selezione precedente",;
    ToolTipText = "Selezione a voce di costo",;
    HelpContextID = 78528420,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=135, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_VOCCEFIN"

  func oVOCCEFIN_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S' OR g_COMM='S'))
    endwith
   endif
  endfunc

  func oVOCCEFIN_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCCEFIN_3_10.ecpDrop(oSource)
    this.Parent.oContained.link_3_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCCEFIN_3_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oVOCCEFIN_3_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voce di costo e ricavo",'',this.parent.oContained
  endproc
  proc oVOCCEFIN_3_10.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_VOCCEFIN
     i_obj.ecpSave()
  endproc

  add object oCOMMINI_3_11 as StdField with uid="MHCZTIDPJF",rtseq=89,rtrep=.f.,;
    cFormVar = "w_COMMINI", cQueryName = "COMMINI",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da commessa",;
    HelpContextID = 217636390,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=160, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMINI"

  func oCOMMINI_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S' OR g_COMM='S'))
    endwith
   endif
  endfunc

  func oCOMMINI_3_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMINI_3_11.ecpDrop(oSource)
    this.Parent.oContained.link_3_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMINI_3_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMINI_3_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCOMMINI_3_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_COMMINI
     i_obj.ecpSave()
  endproc

  add object oCOMMFIN_3_12 as StdField with uid="EDLPZMUACN",rtseq=90,rtrep=.f.,;
    cFormVar = "w_COMMFIN", cQueryName = "COMMFIN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Commessa inferiore alla selezione precedente",;
    ToolTipText = "Selezione a commessa",;
    HelpContextID = 137830874,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=185, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMFIN"

  func oCOMMFIN_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S' OR  g_COMM='S'))
    endwith
   endif
  endfunc

  func oCOMMFIN_3_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMFIN_3_12.ecpDrop(oSource)
    this.Parent.oContained.link_3_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMFIN_3_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMFIN_3_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCOMMFIN_3_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_COMMFIN
     i_obj.ecpSave()
  endproc

  add object oATTIVINI_3_13 as StdField with uid="SVCXPGOXDC",rtseq=91,rtrep=.f.,;
    cFormVar = "w_ATTIVINI", cQueryName = "ATTIVINI",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Selezione da attivit�",;
    HelpContextID = 121285809,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=209, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODATT", oKey_1_2="this.w_ATTIVINI"

  func oATTIVINI_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  func oATTIVINI_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTIVINI_3_13.ecpDrop(oSource)
    this.Parent.oContained.link_3_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTIVINI_3_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIVITA','*','ATCODATT',cp_AbsName(this.parent,'oATTIVINI_3_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'',this.parent.oContained
  endproc
  proc oATTIVINI_3_13.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATCODATT=this.parent.oContained.w_ATTIVINI
     i_obj.ecpSave()
  endproc

  add object oATTIVFIN_3_14 as StdField with uid="CWXVCUQFIT",rtseq=92,rtrep=.f.,;
    cFormVar = "w_ATTIVFIN", cQueryName = "ATTIVFIN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Attivit� inferiore alla selezione precedente",;
    ToolTipText = "Selezione a attivit�",;
    HelpContextID = 96818004,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=234, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODATT", oKey_1_2="this.w_ATTIVFIN"

  func oATTIVFIN_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  func oATTIVFIN_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTIVFIN_3_14.ecpDrop(oSource)
    this.Parent.oContained.link_3_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTIVFIN_3_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIVITA','*','ATCODATT',cp_AbsName(this.parent,'oATTIVFIN_3_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'',this.parent.oContained
  endproc
  proc oATTIVFIN_3_14.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATCODATT=this.parent.oContained.w_ATTIVFIN
     i_obj.ecpSave()
  endproc

  add object oPAGAMINI_3_15 as StdField with uid="PFODPGTHWK",rtseq=93,rtrep=.f.,;
    cFormVar = "w_PAGAMINI", cQueryName = "PAGAMINI",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Da selezione pagamento",;
    HelpContextID = 131305153,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=118, Top=259, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_PAGAMINI"

  func oPAGAMINI_3_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAGAMINI_3_15.ecpDrop(oSource)
    this.Parent.oContained.link_3_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAGAMINI_3_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oPAGAMINI_3_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oPAGAMINI_3_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_PAGAMINI
     i_obj.ecpSave()
  endproc

  add object oPAGAMFIN_3_16 as StdField with uid="TSPRLIICZS",rtseq=94,rtrep=.f.,;
    cFormVar = "w_PAGAMFIN", cQueryName = "PAGAMFIN",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Pagamento inferiore alla selezione precedente",;
    ToolTipText = "A selezione pagamento",;
    HelpContextID = 86798660,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=118, Top=283, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_PAGAMFIN"

  func oPAGAMFIN_3_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAGAMFIN_3_16.ecpDrop(oSource)
    this.Parent.oContained.link_3_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAGAMFIN_3_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oPAGAMFIN_3_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oPAGAMFIN_3_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_PAGAMFIN
     i_obj.ecpSave()
  endproc

  add object oUNIMINI_3_17 as StdField with uid="OYXEOWELRX",rtseq=95,rtrep=.f.,;
    cFormVar = "w_UNIMINI", cQueryName = "UNIMINI",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Da selezione unit� di misura",;
    HelpContextID = 217620038,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=540, Top=259, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_UNIMINI"

  func oUNIMINI_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oUNIMINI_3_17.ecpDrop(oSource)
    this.Parent.oContained.link_3_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUNIMINI_3_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oUNIMINI_3_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'',this.parent.oContained
  endproc
  proc oUNIMINI_3_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_UNIMINI
     i_obj.ecpSave()
  endproc

  add object oUNIMIFIN_3_18 as StdField with uid="YCLRRMDPDV",rtseq=96,rtrep=.f.,;
    cFormVar = "w_UNIMIFIN", cQueryName = "UNIMIFIN",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inferiore alla selezione precedente",;
    ToolTipText = "A selezione unit� di misura",;
    HelpContextID = 83402388,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=540, Top=283, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_UNIMIFIN"

  func oUNIMIFIN_3_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oUNIMIFIN_3_18.ecpDrop(oSource)
    this.Parent.oContained.link_3_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUNIMIFIN_3_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oUNIMIFIN_3_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'',this.parent.oContained
  endproc
  proc oUNIMIFIN_3_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_UNIMIFIN
     i_obj.ecpSave()
  endproc


  add object omonete_3_20 as StdCombo with uid="WVRIEOXNTF",rtseq=98,rtrep=.f.,left=145,top=372,width=149,height=21;
    , ToolTipText = "Stampa in valuta di conto o in altra valuta";
    , HelpContextID = 154988346;
    , cFormVar="w_monete",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func omonete_3_20.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func omonete_3_20.GetRadio()
    this.Parent.oContained.w_monete = this.RadioValue()
    return .t.
  endfunc

  func omonete_3_20.SetRadio()
    this.Parent.oContained.w_monete=trim(this.Parent.oContained.w_monete)
    this.value = ;
      iif(this.Parent.oContained.w_monete=='C',1,;
      iif(this.Parent.oContained.w_monete=='A',2,;
      0))
  endfunc

  add object odivisa_3_21 as StdField with uid="UFNNUYCUSJ",rtseq=99,rtrep=.f.,;
    cFormVar = "w_divisa", cQueryName = "divisa",;
    bObbl = .t. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 222852554,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=145, Top=408, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_divisa"

  func odivisa_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_monete='A')
    endwith
   endif
  endfunc

  func odivisa_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_21('Part',this)
    endwith
    return bRes
  endfunc

  proc odivisa_3_21.ecpDrop(oSource)
    this.Parent.oContained.link_3_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc odivisa_3_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'odivisa_3_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc odivisa_3_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_divisa
     i_obj.ecpSave()
  endproc

  add object oSIMVAL_3_28 as StdField with uid="LLPLHSWPKY",rtseq=106,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo della valuta",;
    HelpContextID = 92153562,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=196, Top=408, InputMask=replicate('X',5)

  add object oCAMVAL_3_29 as StdField with uid="YXSGDASPMV",rtseq=107,rtrep=.f.,;
    cFormVar = "w_CAMVAL", cQueryName = "CAMVAL",;
    bObbl = .t. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Tasso di conversione o cambio nei confronti dell'Euro",;
    HelpContextID = 92155866,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=326, Top=408, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMVAL_3_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MONETE='A' and .w_CAOVAL=0)
    endwith
   endif
  endfunc


  add object oBtn_3_30 as StdButton with uid="LMYQWELCMD",left=489, top=377, width=48,height=45,;
    CpPicture="bmp\grafico.bmp", caption="", nPag=3;
    , ToolTipText = "Premi per confermare";
    , HelpContextID = 200064666;
    , caption='\<Grafico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_30.Click()
      with this.Parent.oContained
        gsst_bST(this.Parent.oContained,"GRAF")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GRAFH<>'S')
     endwith
    endif
  endfunc


  add object oBtn_3_31 as StdButton with uid="AHTXAKJYDM",left=540, top=377, width=48,height=45,;
    CpPicture="bmp\excel.bmp", caption="", nPag=3;
    , ToolTipText = "Premi per confermare";
    , HelpContextID = 247306426;
    , caption='\<Excel';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_31.Click()
      with this.Parent.oContained
        gsst_bST(this.Parent.oContained,"EXCE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_EXCEL))
     endwith
    endif
  endfunc


  add object oBtn_3_32 as StdButton with uid="ZBDVMCZRJQ",left=489, top=424, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=3;
    , ToolTipText = "Premi per confermare";
    , HelpContextID = 99144986;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_32.Click()
      with this.Parent.oContained
        gsst_BST(this.Parent.oContained,"REPO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_CECODICE))
      endwith
    endif
  endfunc


  add object oBtn_3_33 as StdButton with uid="TPYXCZNJCW",left=540, top=424, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premi per uscire";
    , HelpContextID = 91856314;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESDEFIN_3_34 as StdField with uid="NLJVMLYULA",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESDEFIN", cQueryName = "DESDEFIN",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78656644,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=182, Top=36, InputMask=replicate('X',40)

  add object oDESDEINI_3_35 as StdField with uid="EKCMWIMKQU",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DESDEINI", cQueryName = "DESDEINI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 139447169,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=182, Top=11, InputMask=replicate('X',40)

  add object oDESSPIA1_3_40 as StdField with uid="YWRYQQZUFX",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DESSPIA1", cQueryName = "DESSPIA1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 126929817,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=242, Top=86, InputMask=replicate('X',40)

  add object oDESSPIA_3_41 as StdField with uid="RTAOKQKJCC",rtseq=111,rtrep=.f.,;
    cFormVar = "w_DESSPIA", cQueryName = "DESSPIA",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 126929866,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=242, Top=61, InputMask=replicate('X',40)

  add object oVODESCR1_3_44 as StdField with uid="HGZKDPMAEA",rtseq=112,rtrep=.f.,;
    cFormVar = "w_VODESCR1", cQueryName = "VODESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 225423481,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=242, Top=135, InputMask=replicate('X',40)

  add object oVODESCR_3_45 as StdField with uid="VNTTSLRQGW",rtseq=113,rtrep=.f.,;
    cFormVar = "w_VODESCR", cQueryName = "VODESCR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 225423530,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=242, Top=110, InputMask=replicate('X',40)

  add object oCOMMDES1_3_48 as StdField with uid="VWDLGOOKSX",rtseq=114,rtrep=.f.,;
    cFormVar = "w_COMMDES1", cQueryName = "COMMDES1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione commessa",;
    HelpContextID = 61398615,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=242, Top=185, InputMask=replicate('X',40)

  add object oCOMMDES_3_49 as StdField with uid="FQRWUIHDFG",rtseq=115,rtrep=.f.,;
    cFormVar = "w_COMMDES", cQueryName = "COMMDES",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione commessa",;
    HelpContextID = 61398566,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=242, Top=160, InputMask=replicate('X',40)

  add object oATTIDES1_3_52 as StdField with uid="NXUGQJJKCP",rtseq=116,rtrep=.f.,;
    cFormVar = "w_ATTIDES1", cQueryName = "ATTIDES1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attivit�",;
    HelpContextID = 61166391,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=242, Top=234, InputMask=replicate('X',40)

  add object oATTIDES_3_53 as StdField with uid="OOMQTSRTGX",rtseq=117,rtrep=.f.,;
    cFormVar = "w_ATTIDES", cQueryName = "ATTIDES",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attivit�",;
    HelpContextID = 61166342,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=242, Top=209, InputMask=replicate('X',40)

  add object oPAGADES_3_57 as StdField with uid="MKTQCWYWGG",rtseq=118,rtrep=.f.,;
    cFormVar = "w_PAGADES", cQueryName = "PAGADES",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 60584182,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=182, Top=259, InputMask=replicate('X',30)

  add object oPAGADES1_3_58 as StdField with uid="VFVDQHGXWK",rtseq=119,rtrep=.f.,;
    cFormVar = "w_PAGADES1", cQueryName = "PAGADES1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione pagamento",;
    HelpContextID = 60584231,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=182, Top=283, InputMask=replicate('X',40)

  add object oStr_3_36 as StdString with uid="WHUJPUEZAE",Visible=.t., Left=2, Top=14,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da destinaz.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_37 as StdString with uid="QNVMUTTXCL",Visible=.t., Left=2, Top=38,;
    Alignment=1, Width=115, Height=18,;
    Caption="A destinaz.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_38 as StdString with uid="USIEBCWIEL",Visible=.t., Left=478, Top=14,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_3_39 as StdString with uid="ZCUHVWAJIY",Visible=.t., Left=478, Top=38,;
    Alignment=1, Width=83, Height=18,;
    Caption="A provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_3_42 as StdString with uid="YTRQOOEPIM",Visible=.t., Left=2, Top=64,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da centro di C/R:"  ;
  , bGlobalFont=.t.

  add object oStr_3_43 as StdString with uid="TZPDQSVATG",Visible=.t., Left=2, Top=88,;
    Alignment=1, Width=115, Height=18,;
    Caption="A centro di C/R:"  ;
  , bGlobalFont=.t.

  add object oStr_3_46 as StdString with uid="ZZXCKGBSCJ",Visible=.t., Left=2, Top=113,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da voce di C/R:"  ;
  , bGlobalFont=.t.

  add object oStr_3_47 as StdString with uid="IJLWWRFKZP",Visible=.t., Left=2, Top=137,;
    Alignment=1, Width=115, Height=18,;
    Caption="A voce di C/R:"  ;
  , bGlobalFont=.t.

  add object oStr_3_50 as StdString with uid="THMEQKZNFD",Visible=.t., Left=2, Top=163,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_3_51 as StdString with uid="JPYZTZFIPV",Visible=.t., Left=2, Top=187,;
    Alignment=1, Width=115, Height=18,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_3_54 as StdString with uid="FNJXSMHFHS",Visible=.t., Left=2, Top=212,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_55 as StdString with uid="DRVUQCMSLI",Visible=.t., Left=2, Top=234,;
    Alignment=1, Width=115, Height=18,;
    Caption="A attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_56 as StdString with uid="OHETYMRXIC",Visible=.t., Left=2, Top=260,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_59 as StdString with uid="QEYZGBYVIW",Visible=.t., Left=2, Top=282,;
    Alignment=1, Width=115, Height=18,;
    Caption="A pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_60 as StdString with uid="TGKOULWREW",Visible=.t., Left=416, Top=260,;
    Alignment=1, Width=123, Height=18,;
    Caption="Da unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_3_61 as StdString with uid="PYXQNROSMO",Visible=.t., Left=416, Top=282,;
    Alignment=1, Width=123, Height=18,;
    Caption="A unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_3_62 as StdString with uid="IMCCVCIRSD",Visible=.t., Left=9, Top=373,;
    Alignment=1, Width=133, Height=15,;
    Caption="Valuta di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_3_63 as StdString with uid="UTVLXUSTEH",Visible=.t., Left=31, Top=408,;
    Alignment=1, Width=111, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_3_64 as StdString with uid="ZYBHKGMOLH",Visible=.t., Left=250, Top=412,;
    Alignment=1, Width=73, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_3_65 as StdString with uid="HQMWAFVCYY",Visible=.t., Left=8, Top=336,;
    Alignment=0, Width=570, Height=15,;
    Caption="Valuta di rappresentazione importi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_66 as StdBox with uid="CXFVATPOAL",left=8, top=360, width=576,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsst_kcr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
