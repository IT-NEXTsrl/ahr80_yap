* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsst_bst                                                        *
*              Elaborazione stampa statistiche                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_548]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-07                                                      *
* Last revis.: 2013-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsst_bst",oParentObject,m.pParam)
return(i_retval)

define class tgsst_bst as StdBatch
  * --- Local variables
  pParam = space(4)
  w_OBJCTRL = .NULL.
  w_GSST_KCR = .NULL.
  w_DIFFYEAR = 0
  w_ANNO = 0
  w_CAMBIO = 0
  w_CODFOR = space(15)
  w_CODAGE = space(5)
  w_CODZON = space(3)
  w_CODGRU = space(5)
  w_CODFAM = space(5)
  w_CATOMO = space(5)
  w_CATCON = space(5)
  w_CATCOM = space(5)
  w_CODMAR = space(5)
  w_CODCAU = space(5)
  w_CODDES = space(5)
  w_VOCCEN = space(15)
  w_CODCEN = space(15)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_CODPAG = space(5)
  w_CODAG2 = space(5)
  w_CODORN = space(15)
  w_CODVET = space(5)
  w_GFDESCRI = space(50)
  w_PROVIN = space(2)
  w_CONSUP = space(15)
  w_CODUNI = space(3)
  w_CODFAB = space(15)
  w_DESCRI = space(40)
  w_INDICE = 0
  w_CODART = space(20)
  w_CODMAG = space(5)
  w_UNIMIS = space(3)
  w_TOTQTA = 0
  w_TOTIMP = 0
  w_PERIOD = 0
  w_CmdSel = space(100)
  w_CmdSel1 = space(100)
  w_CmdSel2 = space(100)
  w_CmdOrd = space(100)
  w_CmdGrp = space(100)
  w_CmdGrp1 = space(100)
  w_VARAPP = space(8)
  w_CODPER = 0
  w_FILE = space(100)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_TIPPER = space(2)
  w_MESS1 = space(100)
  w_FLSEGN = space(1)
  w_DECTOT = 0
  w_CODCAU = space(5)
  w_EXMES = .f.
  w_mese = space(2)
  w_TIPCON = space(1)
  w_FLCAUS = .f.
  w_DATDOC = ctod("  /  /  ")
  w_CAOVAL = 0
  w_TOTREC = 0
  w_SEQREC = 0
  w_FIELDAXIS2 = space(100)
  w_MESS = space(100)
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  CAU_SELE_idx=0
  CRIDELAB_idx=0
  CRIMELAB_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazioni
    *     EXCE= Solo Foglio Ecel
    *     REPO= Solo stampa su report
    *     GRAF= Solo Grafico
    this.oParentObject.w_OK = "N"
    this.w_GSST_KCR = this.oParentObject
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do case
      case this.oParentObject.w_OK="B"
        * --- Messaggio  bloccante
        ah_ErrorMsg("%1",,"",this.w_MESS)
        this.w_OBJCTRL.Setfocus()     
      case this.oParentObject.w_OK="C"
        * --- Messaggio non bloccante
        if Not ah_YesNo("%1%0Continuare ugualmente?","",this.w_MESS)
          this.oParentObject.w_OK = "B"
          this.w_OBJCTRL.Setfocus()     
        endif
    endcase
    if this.oParentObject.w_OK="B" 
      * --- Se controlli bloccanti o solo controllo date mi fermo
      * --- Punto il focus sul numero documento
       
 This.oParentobject.oPgFrm.ActivePage=1
      i_retcode = 'stop'
      return
    endif
    this.w_INDICE = 0
    this.w_EXMES = .F.
    this.w_GFDESCRI = this.oParentObject.w_CEDESCRI
    * --- Creazione Cursore Causali di Selezione
    * --- Creazione Cursore
    Create Cursor TmpElab (CODFOR C(15) , CODAGE C(5) , CODZON C(3) , CODGRU C(5), ;
    CODFAM C(5) , CATOMO C(5) , CATCON C(5) , CATCOM C(5) , CODMAR C(5) , CODART C(20) , ;
    CODMAG C(5) , UNIMIS C(3) , TOTQTA N(12,3), TOTIMP N(18,4),TOTQTA2 N(12,3), TOTIMP2 N(18,4) ;
    ,TOTQTA3 N(12,3), TOTIMP3 N(18,4) ,CODPER N(14),DESCRI C(40),CODDES C(5), VOCCEN C(15);
    ,CODCEN C(15), CODCOM C(15), CODATT C(15), CODPAG C(5), CODAG2 C(5), CODORN C(15);
    ,CODVET C(5), PROVIN C(2), CONSUP C(15), CODUNI C(3), CODFAB C(15),TIPCON C(1),ANNO N(4))
    * --- Predispongo L'indice
    =wrcursor("TmpElab") 
     Index On (CODFOR+CODAGE+CODZON+CODGRU+CODFAM+CATOMO+CATCON+; 
 CATCOM+CODMAR+CODART+CODMAG+STR(CODPER)+CODDES+VOCCEN+CODCEN+; 
 CODCOM+CODATT+CODPAG+CODAG2+CODORN+CODVET+PROVIN+CONSUP+CODUNI+CODFAB) Tag idx1 collate "MACHINE"
    * --- Definizione Array Campi Raggruppamento
    Dimension Campi(26,4)
    for i=1 to 26
    campi(i,2)="N"
    endfor
    Campi(1,1) ="CODCLI"
    Campi(2,1) ="CODFOR"
    Campi(3,1) ="CODAGE"
    Campi(4,1) ="CODZON"
    Campi(5,1) ="CODGRU"
    Campi(6,1) ="CODFAM"
    Campi(7,1) ="CATOMO"
    Campi(8,1) ="CATCON"
    Campi(9,1) ="CATCOM"
    Campi(10,1) ="CODMAR"
    Campi(11,1) ="CODART"
    Campi(12,1) ="CODMAG"
    Campi(13,1) ="CODPER"
    Campi(14,1) ="CODDES"
    Campi(15,1) ="VOCCEN"
    Campi(16,1) ="CODCEN"
    Campi(17,1) ="CODCOM"
    Campi(18,1) ="CODATT"
    Campi(19,1) ="CODPAG"
    Campi(20,1) ="CODAG2"
    Campi(21,1) ="CODORN"
    Campi(22,1) ="CODVET"
    Campi(23,1) ="PROVIN"
    Campi(24,1) ="CONSUP"
    Campi(25,1) ="CODUNI"
    Campi(26,1) ="CODFAB"
    Campi(1,3)=ah_MsgFormat("Cliente:")
    Campi(2,3)=ah_MsgFormat("Cliente\fornit.:")
    Campi(3,3)=ah_MsgFormat("Agente:")
    Campi(4,3)=ah_MsgFormat("Zona:")
    Campi(5,3)=ah_MsgFormat("Gruppo merc.:")
    Campi(6,3)=ah_MsgFormat("Famiglia:")
    Campi(7,3)=ah_MsgFormat("Categoria omog.:")
    Campi(8,3)=ah_MsgFormat("Categoria cont.:")
    Campi(9,3)=ah_MsgFormat("Categoria comm.:")
    Campi(10,3)=ah_MsgFormat("Marchio:")
    Campi(11,3)=ah_MsgFormat("Articolo:")
    Campi(12,3)=ah_MsgFormat("Magazzino:")
    Campi(14,3)=ah_MsgFormat("Destinazione")
    Campi(15,3)=ah_MsgFormat("Voce di C/R")
    Campi(16,3)=ah_MsgFormat("Centro di C/R")
    Campi(17,3)=ah_MsgFormat("Commessa")
    Campi(18,3)=ah_MsgFormat("Attivit�")
    Campi(19,3)=ah_MsgFormat("Pagamento")
    Campi(20,3)=ah_MsgFormat("Capo area")
    Campi(21,3)=ah_MsgFormat("Per conto di")
    Campi(22,3)=ah_MsgFormat("Vettore")
    Campi(23,3)=ah_MsgFormat("Provincia")
    Campi(24,3)=ah_MsgFormat("Mastro cont.")
    Campi(25,3)=ah_MsgFormat("Unit� di misura")
    Campi(26,3)=ah_MsgFormat("Fornit. abituale")
    Campi(1,4) =""
    Campi(2,4) ="LOOKTAB('CONTI','ANDESCRI','ANTIPCON',TIPCON,'ANCODICE',CODFOR)"
    Campi(3,4) ="LOOKTAB('AGENTI','AGDESAGE','AGCODAGE',CODAGE)"
    Campi(4,4) ="LOOKTAB('ZONE','ZODESZON','ZOCODZON',CODZON)"
    Campi(5,4) ="LOOKTAB('GRUMERC','GMDESCRI','GMCODICE',CODGRU)"
    Campi(6,4) ="LOOKTAB('FAM_ARTI','FADESCRI','FACODICE',CODFAM)"
    Campi(7,4) ="LOOKTAB('CATEGOMO','OMDESCRI','OMCODICE',CATOMO)"
    Campi(8,4) ="LOOKTAB('CACOARTI','C1DESCRI','C1CODICE',CATCON)"
    Campi(9,4) =IIF((g_PERCCR="S" OR g_COMM="S"),"LOOKTAB('CATECOMM','CTDESCRI','CTCODICE',CATCOM)","")
    Campi(10,4) ="LOOKTAB('MARCHI','MADESCRI','MACODICE',CODMAR)"
    Campi(11,4) ="LOOKTAB('ART_ICOL','ARDESART','ARCODART',CODART)"
    Campi(12,4) ="LOOKTAB('MAGAZZIN','MGDESMAG','MGCODMAG',CODMAG)"
    Campi(14,4) ="LOOKTAB('DES_DIVE','DDNOMDES','DDTIPCON',TIPCON,'DDCODICE',CODFOR,'DDCODDES',CODDES)"
    Campi(15,4) =IIF((g_PERCCR="S" OR g_COMM="S"),"LOOKTAB('VOC_COST','VCDESCRI','VCCODICE',VOCCEN)"," ")
    Campi(16,4) =IIF(g_PERCCR="S" ,"LOOKTAB('CENCOST','CCDESPIA','CC_CONTO',CODCEN)","")
    Campi(17,4) ="LOOKTAB('CAN_TIER','CNDESCAN','CNCODCAN',CODCOM)"
    Campi(18,4) =IIF(g_COMM="S","LOOKTAB('ATTIVITA','ATDESCRI','ATCODATT',CODATT)","")
    Campi(19,4) ="LOOKTAB('PAG_AMEN','PADESCRI','PACODICE',CODPAG)"
    Campi(20,4) ="LOOKTAB('AGENTI','AGDESAGE','AGCODAGE',CODAG2)"
    Campi(21,4) ="LOOKTAB('CONTI','ANDESCRI','ANTIPCON',TIPCON,'ANCODICE',CODORN)"
    Campi(22,4) ="LOOKTAB('VETTORI','VTDESVET','VTCODVET',CODVET)"
    Campi(23,4) =""
    Campi(24,4) ="LOOKTAB('MASTRI','MCDESCRI','MCCODICE',CONSUP)"
    Campi(25,4) ="LOOKTAB('UNIMIS','UMDESCRI','UMCODICE',CODUNI)"
    Campi(26,4) ="LOOKTAB('CONTI','ANDESCRI','ANTIPCON','F','ANCODICE',CODFAB)"
    * --- Controllo se Foglio Excel Mensile
    this.w_EXMES = iif((upper(alltrim(this.oParentObject.w_EXCEL))="..\STAT\EXE\QUERY\GSST_KEX.XLT" or upper(alltrim(this.oParentObject.w_EXCEL))="..\STAT\EXE\QUERY\GSST_AGE.XLT"),.T.,.F.)
    * --- Select from CRIDELAB
    i_nConn=i_TableProp[this.CRIDELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CRIDELAB_idx,2],.t.,this.CRIDELAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CRIDELAB ";
          +" where CECODICE="+cp_ToStrODBC(this.oParentObject.w_CECODICE)+"";
          +" order by CPROWORD";
           ,"_Curs_CRIDELAB")
    else
      select * from (i_cTable);
       where CECODICE=this.oParentObject.w_CECODICE;
       order by CPROWORD;
        into cursor _Curs_CRIDELAB
    endif
    if used('_Curs_CRIDELAB')
      select _Curs_CRIDELAB
      locate for 1=1
      do while not(eof())
      * --- Definisco alcune variabili per il report
      if not empty(_Curs_CRIDELAB.CENUMCAM) and _Curs_CRIDELAB.CENUMCAM<>1
        * --- Rilevo che il raggruppamento � stato selezionato
        * --- Se indicata destinazione controllo che sia stato indicato anche il raggruppamento del Cliente\Fornitore per determinare descrizione
        if _Curs_CRIDELAB.CENUMCAM=14 AND CAMPI(2,2)<>"S"
          Campi(14,4)=""
        endif
        Campi(_Curs_CRIDELAB.CENUMCAM,2)="S"
        if (this.pParam="EXCE" AND this.w_EXMES=.T.) OR (this.pParam="GRAF")
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Calcolo Descrizione Raggruppamento periodo
        if _Curs_CRIDELAB.CENUMCAM= 13
          this.w_TIPPER = _Curs_CRIDELAB.CETIPPER
          do case
            case _Curs_CRIDELAB.CETIPPER ="AN"
              Campi(13,3)=ah_MsgFormat("Periodo: anno")
            case _Curs_CRIDELAB.CETIPPER ="SM"
              Campi(13,3)=ah_MsgFormat("Periodo: semestre")
            case _Curs_CRIDELAB.CETIPPER ="QU"
              Campi(13,3)=ah_MsgFormat("Periodo: quadrimestre")
            case _Curs_CRIDELAB.CETIPPER ="TR"
              Campi(13,3)=ah_MsgFormat("Periodo: trimestre")
            case _Curs_CRIDELAB.CETIPPER ="BI"
              Campi(13,3)=ah_MsgFormat("Periodo: bimestre")
            case _Curs_CRIDELAB.CETIPPER ="ME"
              Campi(13,3)=ah_MsgFormat("Periodo: mese")
            case _Curs_CRIDELAB.CETIPPER ="SE"
              Campi(13,3)=ah_MsgFormat("Periodo: settimana")
            case _Curs_CRIDELAB.CETIPPER ="GI"
              Campi(13,3)=ah_MsgFormat("Periodo: giorno")
          endcase
        endif
        this.w_INDICE = this.w_INDICE+1
        * --- Definisco descrizione  nel caso di ragrruppamento per periodo
        this.w_CmdGrp = alltrim(this.w_CmdGrp) + CAMPI(_Curs_CRIDELAB.CENUMCAM,1) +","
        descr= CAMPI(_Curs_CRIDELAB.CENUMCAM,3)
        if CAMPI(_Curs_CRIDELAB.CENUMCAM,1)<>"CODPER"
          if this.pParam="EXCE" and this.w_EXMES=.F.
            this.w_CmdSel = alltrim(this.w_CmdSel) + CAMPI(_Curs_CRIDELAB.CENUMCAM,1) +","
          else
            * --- Controllo che  il raggruppamento preveda la descrizione
            if EMPTY(CAMPI(_Curs_CRIDELAB.CENUMCAM,4))
              this.w_CmdSel = alltrim(this.w_CmdSel) + CAMPI(_Curs_CRIDELAB.CENUMCAM,1) + " AS grup"+alltrim(str(this.w_indice))+","
            else
              this.w_CmdSel = alltrim(this.w_CmdSel) + CAMPI(_Curs_CRIDELAB.CENUMCAM,1) + " AS grup"+alltrim(str(this.w_indice))+","+CAMPI(_Curs_CRIDELAB.CENUMCAM,4)+"as descr"+alltrim(str(this.w_indice))+","
            endif
          endif
          if _Curs_CRIDELAB.CEFLGQUA ="S"
            this.w_VARAPP = "L_"+"QTA"+alltrim(str(this.w_indice))
            QTA=this.w_VARAPP
            &QTA= descr
          endif
          this.w_CmdOrd = alltrim(this.w_CmdOrd) + CAMPI(_Curs_CRIDELAB.CENUMCAM,1) +"," 
        else
          if this.pParam="EXCE" and this.w_EXMES=.T.
            this.w_CmdSel = alltrim(this.w_CmdSel) + "DESCRI,"
            this.w_CmdOrd = alltrim(this.w_CmdOrd) + CAMPI(_Curs_CRIDELAB.CENUMCAM,1) +"," 
          else
            if (this.oParentObject.w_CEPERIO2="T" OR this.oParentObject.w_CEPERIO3="T") AND this.pParam="REPO" 
              this.w_CmdSel = alltrim(this.w_CmdSel) + "DESCRI AS grup"+alltrim(str(this.w_indice))+",ANNO,"
            else
              this.w_CmdSel = alltrim(this.w_CmdSel) + "DESCRI AS grup"+alltrim(str(this.w_indice))+","
            endif
            this.w_CmdOrd = alltrim(this.w_CmdOrd) + CAMPI(_Curs_CRIDELAB.CENUMCAM,1) +"," 
          endif
          if _Curs_CRIDELAB.CEFLGQUA ="S"
            QTA=_Curs_CRIDELAB.CEFLGQUA
            this.w_VARAPP = "L_"+"QTA"+alltrim(str(this.w_indice))
            QTA=this.w_VARAPP
            &QTA= descr
          endif
        endif
        this.w_VARAPP = "L_"+"grup"+alltrim(str(this.w_indice))
        appo=this.w_VARAPP
        &APPO= descr
        this.w_VARAPP = "L_"+CAMPI(_Curs_CRIDELAB.CENUMCAM,1)
        VARAPP = this.w_VARAPP
        &VARAPP =Ah_Msgformat("Attivato")
      endif
        select _Curs_CRIDELAB
        continue
      enddo
      use
    endif
    * --- Determino i periodi  di elaborazione
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_DIVISA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_DIVISA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAMBIO = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DATINI = this.oParentObject.w_DATINI1
    this.w_DATFIN = this.oParentObject.w_DATFIN1
    this.w_PERIOD = 1
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_CEPERIO2 ="T" 
      this.w_PERIOD = 2
      this.w_DATINI = this.oParentObject.w_DATINI2
      this.w_DATFIN = this.oParentObject.w_DATFIN2
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_CEPERIO3 ="T" 
        this.w_PERIOD = 3
        this.w_DATINI = this.oParentObject.w_DATINI3
        this.w_DATFIN = this.oParentObject.w_DATFIN3
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if empty(this.w_CmdSel)
      * --- Controlli sull'esistenza di raggruppamenti nel criterio selezionato.
      this.w_MESS1 = "Nessun raggruppamento specificato nel criterio"
      ah_ErrorMsg(this.w_MESS1,,"")
      i_retcode = 'stop'
      return
    else
      Sel = this.w_CmdSel + "MAX(UNIMIS) as UNIMIS,SUM(TOTQTA) as TOTQTA,SUM(TOTIMP) as TOTIMP"+;
      ",SUM(TOTQTA2) as TOTQTA2,SUM(TOTIMP2) as TOTIMP2 ,SUM(TOTQTA3) as TOTQTA3,SUM(TOTIMP3) as TOTIMP3"
      Ord = LEFT(this.w_CmdOrd , LEN(alltrim(this.w_CmdOrd))-1)
      Grp = LEFT(this.w_CmdGrp , LEN(alltrim(this.w_CmdGrp))-1)
    endif
    ah_Msg("Preparazione dati raggruppati per stampa...",.T.)
    Select &Sel FROM TmpElab GROUP BY &Grp ORDER BY &Ord INTO CURSOR __tmp__
    if reccount("__TMP__") =0
      * --- Controlli sull'esistenza di dati per le selezioni effettuate.
      this.w_MESS1 = "Nessun elemento riscontrato per le selezioni specificate"
      ah_ErrorMsg(this.w_MESS1,,"")
    else
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if used ("TmpElab")
      select TmpElab
      Use
    endif
    if used ("Tempor")
      select Tempor
      Use
    endif
    if used ("__TMP__")
      select __tmp__
      Use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegna variabili
    this.w_CODFOR = NVL(tempor.CODFOR,SPACE(15))
    this.w_CODAGE = NVL(tempor.CODAGE,SPACE(5))
    * --- Valorizzo la variabile globale per la selezione degli indirizzi email per l'agente
    i_AGENTCODE = this.w_CODAGE
    this.w_CODZON = NVL(tempor.CODZON,SPACE(3))
    this.w_CODGRU = NVL(tempor.CODGRU,SPACE(5))
    this.w_CODFAM = NVL(tempor.CODFAM,SPACE(5))
    this.w_CATOMO = NVL(tempor.CATOMO,SPACE(5))
    this.w_CATCON = NVL(tempor.CATCON,SPACE(5))
    this.w_CATCOM = NVL(tempor.CATCOM,SPACE(5))
    this.w_CODMAR = NVL(tempor.CODMAR,SPACE(5))
    this.w_CODART = NVL(tempor.CODART,SPACE(20))
    this.w_CODMAG = NVL(tempor.CODMAG,SPACE(5))
    this.w_UNIMIS = NVL(tempor.UNIMIS,SPACE(3))
    this.w_CODCAU = NVL(tempor.CODCAU,SPACE(5))
    this.w_CODDES = NVL(tempor.CODDES,SPACE(5))
    this.w_VOCCEN = NVL(tempor.VOCCEN,SPACE(15))
    this.w_CODCEN = NVL(tempor.CODCEN,SPACE(15))
    this.w_CODCOM = NVL(tempor.CODCOM,SPACE(15))
    this.w_CODATT = NVL(tempor.CODATT,SPACE(15))
    this.w_CODPAG = NVL(tempor.CODPAG,SPACE(5))
    this.w_CODAG2 = NVL(tempor.CODAG2,SPACE(5))
    this.w_CODORN = NVL(tempor.CODORN,SPACE(15))
    this.w_CODVET = NVL(tempor.CODVET,SPACE(5))
    this.w_PROVIN = NVL(tempor.PROVIN,SPACE(2))
    this.w_CONSUP = NVL(tempor.CONSUP,SPACE(15))
    this.w_CODUNI = NVL(tempor.CODUNI,SPACE(3))
    this.w_CODFAB = NVL(tempor.CODFAB,SPACE(15))
    this.w_TIPCON = NVL(tempor.TIPCON,SPACE(1))
    this.w_DATDOC = NVL(TEMPOR.DATDOC, CP_CHARTODATE("  -  -  "))
    this.w_CAOVAL = GETCAM(G_PERVAL, TEMPOR.DATDOC)
    this.w_TOTQTA = NVL(tempor.TOTQTA,0)
    this.w_TOTIMP = NVL(tempor.TOTIMP,0)
    * --- Applico segno impostato nelle causali del criterio utilizzato
    * --- Select from CAU_SELE
    i_nConn=i_TableProp[this.CAU_SELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_SELE_idx,2],.t.,this.CAU_SELE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAU_SELE ";
          +" where CSCODICE="+cp_ToStrODBC(this.oParentObject.w_CECODICE)+" AND CSCODCAU="+cp_ToStrODBC(this.w_CODCAU)+"";
           ,"_Curs_CAU_SELE")
    else
      select * from (i_cTable);
       where CSCODICE=this.oParentObject.w_CECODICE AND CSCODCAU=this.w_CODCAU;
        into cursor _Curs_CAU_SELE
    endif
    if used('_Curs_CAU_SELE')
      select _Curs_CAU_SELE
      locate for 1=1
      do while not(eof())
      do case
        case _Curs_CAU_SELE.CS_SEGNO="-"
          this.w_TOTIMP = -this.w_TOTIMP
        case _Curs_CAU_SELE.CS_SEGNO="="
          this.w_TOTIMP = this.w_TOTIMP*0
      endcase
      do case
        case _Curs_CAU_SELE.CSSEGQTA="-"
          this.w_TOTQTA = -this.w_TOTQTA
        case _Curs_CAU_SELE.CSSEGQTA="="
          this.w_TOTQTA = this.w_TOTQTA*0
      endcase
        select _Curs_CAU_SELE
        continue
      enddo
      use
    endif
    this.w_DIFFYEAR = (YEAR(Tempor.DATREG) -YEAR(this.w_DATINI))+1
    this.w_ANNO = YEAR(Tempor.DATREG) 
    do case
      case this.w_TIPPER ="AN"
        * --- Elaborazione per Anno
        this.w_CODPER = YEAR(Tempor.DATREG)
        this.w_DESCRI = ALLTRIM(STR(YEAR(Tempor.DATREG)))
      case this.w_TIPPER ="SM"
        * --- Elaborazione per Semestre
        if (this.oParentObject.w_CEPERIO2="T" OR this.oParentObject.w_CEPERIO3="T") AND this.pParam="REPO" 
          this.w_CODPER = VAL(ALLTRIM(STR(this.w_DIFFYEAR)) + ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+5)/6))))
          this.w_DESCRI =ah_Msgformat("%1 sem. del %2 anno", ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+5)/6))), ALLTRIM(STR(this.w_DIFFYEAR)) )
        else
          this.w_CODPER =VAL( ALLTRIM(STR(YEAR(Tempor.DATREG)))+ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+5)/6))))
          this.w_DESCRI =ah_Msgformat("%1 sem. del  %2", ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+5)/6))), ALLTRIM(STR(YEAR(Tempor.DATREG))) )
        endif
      case this.w_TIPPER ="QU"
        * --- Elaborazione per Quadrimestre
        if (this.oParentObject.w_CEPERIO2="T" OR this.oParentObject.w_CEPERIO3="T") AND this.pParam="REPO" 
          this.w_CODPER = VAL(ALLTRIM(STR(this.w_DIFFYEAR)) + ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+3)/4))))
          this.w_DESCRI = ah_Msgformat("%1 quadr. del %2 anno", ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+3)/4))) , ALLTRIM(STR(this.w_DIFFYEAR)) )
        else
          this.w_CODPER = VAL(ALLTRIM(STR(YEAR(Tempor.DATREG)))+ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+3)/4))))
          this.w_DESCRI = ah_Msgformat("%1 quadr. del %2", ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+3)/4))) , ALLTRIM(STR(YEAR(Tempor.DATREG))) )
        endif
      case this.w_TIPPER ="TR"
        * --- Elaborazione per Trimestre
        if (this.oParentObject.w_CEPERIO2="T" OR this.oParentObject.w_CEPERIO3="T") AND this.pParam="REPO" 
          this.w_CODPER = VAL(ALLTRIM(STR(this.w_DIFFYEAR)) + ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+2)/3))))
          this.w_DESCRI =ah_Msgformat("%1 trim. del %2 anno", ALLTRIM(STR(INT((MONTH(TEMPOR.DATREG)+2)/3))) , ALLTRIM(STR(this.w_DIFFYEAR)) )
        else
          this.w_CODPER = VAL(ALLTRIM(STR(YEAR(Tempor.DATREG)))+ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+2)/3))))
          this.w_DESCRI =ah_Msgformat("%1 trim. del %2", ALLTRIM(STR(INT((MONTH(TEMPOR.DATREG)+2)/3))) , ALLTRIM(STR(YEAR(Tempor.DATREG))) )
        endif
      case this.w_TIPPER ="BI"
        * --- Elaborazione per Bimestre
        if (this.oParentObject.w_CEPERIO2="T" OR this.oParentObject.w_CEPERIO3="T") AND this.pParam="REPO" 
          this.w_CODPER = VAL(ALLTRIM(STR(this.w_DIFFYEAR)) + ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+1)/2))))
          this.w_DESCRI = ah_Msgformat("%1 bim. del %2 anno", ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+1)/2))) , ALLTRIM(STR(this.w_DIFFYEAR)) )
        else
          this.w_CODPER = VAL(ALLTRIM(STR(YEAR(Tempor.DATREG)))+ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+1)/2))))
          this.w_DESCRI = ah_Msgformat("%1 bim. del  %2", ALLTRIM(STR(INT((MONTH(Tempor.DATREG)+1)/2))) , ALLTRIM(STR(YEAR(Tempor.DATREG))) )
        endif
      case this.w_TIPPER ="ME"
        * --- Elaborazione per Mese
        if (this.oParentObject.w_CEPERIO2="T" OR this.oParentObject.w_CEPERIO3="T") AND this.pParam="REPO"
          * --- Se inseriti pi� periodi di confronto inserisco periodo relativo
          this.w_CODPER = VAL(ALLTRIM(STR(this.w_DIFFYEAR)) + right(left(dtoc(Tempor.DATREG),5),2)+ALLTRIM(STR(this.w_DIFFYEAR)))
          this.w_DESCRI = ah_Msgformat("%1 del %2 anno",right(left(dtoc(Tempor.DATREG),5),2), ALLTRIM(STR(this.w_DIFFYEAR)) )
        else
          this.w_CODPER = VAL(ALLTRIM(STR(YEAR(Tempor.DATREG)))+right(left(dtoc(Tempor.DATREG),5),2))
          this.w_DESCRI = right(left(dtoc(Tempor.DATREG),5),2)+"/"+ALLTRIM(STR(YEAR(Tempor.DATREG)))
        endif
      case this.w_TIPPER ="SE"
        * --- Elaborazione per Settimana
        *     faccio VAL esterna per avere poi ordinamento corretto
        if (this.oParentObject.w_CEPERIO2="T" OR this.oParentObject.w_CEPERIO3="T") AND this.pParam="REPO" 
          * --- Se inseriti pi� periodi di confronto inserisco periodo relativo
          this.w_CODPER = VAL(ALLTRIM(STR(this.w_DIFFYEAR)) + Right("00"+ALLTRIM(STR(WEEK(Tempor.DATREG,1,2))),2))
          this.w_DESCRI = ah_Msgformat("dal %1 al %2 del %3 anno", DTOC((CP_TODATE(Tempor.datreg))-DOW((CP_TODATE(Tempor.datreg)),2)+1) , DTOC((CP_TODATE(Tempor.datreg))-DOW((CP_TODATE(Tempor.datreg)),2)+7) , ALLTRIM(STR(this.w_DIFFYEAR)) )
        else
          this.w_CODPER = VAL(ALLTRIM(STR(YEAR(CTOD( DTOC((CP_TODATE(Tempor.datreg))-DOW((CP_TODATE(Tempor.datreg)),2)+7) ))))+Right("00"+ALLTRIM(STR(WEEK(Tempor.DATREG,1,2))),2))
          this.w_DESCRI =ah_Msgformat("dal %1 al %2", DTOC((CP_TODATE(Tempor.datreg))-DOW((CP_TODATE(Tempor.datreg)),2)+1), DTOC((CP_TODATE(Tempor.datreg))-DOW((CP_TODATE(Tempor.datreg)),2)+7) ) 
        endif
      case this.w_TIPPER ="GI"
        * --- Elaborazione per Giorno
        if (this.oParentObject.w_CEPERIO2="T" OR this.oParentObject.w_CEPERIO3="T") AND this.pParam="REPO" 
          * --- Se inseriti pi� periodi di confronto inserisco periodo relativo
          this.w_CODPER = VAL(ALLTRIM(STR(this.w_DIFFYEAR)) + right(left(dtoc(TEMPOR.datreg),5),2) +left(dtoc(Tempor.datreg),2))
          this.w_DESCRI =ah_Msgformat("%1 del %2 anno", left(dtoc(Tempor.datreg),2)+"/"+right(left(dtoc(Tempor.datreg),5),2) ,ALLTRIM(STR(this.w_DIFFYEAR)) )
        else
          this.w_CODPER = VAL(ALLTRIM(STR(YEAR(Tempor.DATREG)))+right(left(dtoc(TEMPOR.datreg),5),2) +left(dtoc(Tempor.datreg),2))
          this.w_DESCRI = left(dtoc(Tempor.datreg),2)+"/"+right(left(dtoc(Tempor.datreg),5),2)+"/" + ALLTRIM(STR(YEAR(Tempor.DATREG)))
        endif
    endcase
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione Variabili Globali
    * --- Definizione Variabili Locali
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ciclo eseguito per ogni periodo
    ah_Msg("Lettura dati periodo: %1",.T.,.F.,.F., STR(this.w_PERIOD) )
    vq_exec("..\STAT\EXE\QUERY\gsst_sst.vqr",this,"tempor")
    Select tempor
    this.w_TOTREC = RECCOUNT()
    this.w_SEQREC = 0
    go top
    scan 
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_SEQREC = this.w_SEQREC + 1
    if MOD(this.w_SEQREC, 50)=0
      ah_Msg("Scansione periodo: %1- rec. %2 su %3",.T.,.F.,.F., STR(this.w_PERIOD), ALLTR(STR(this.w_SEQREC)), ALLTR(STR(this.w_TOTREC)) )
    endif
    Select TmpElab
    if Seek(this.w_CODFOR+this.w_CODAGE+this.w_CODZON+this.w_CODGRU+this.w_CODFAM+this.w_CATOMO+this.w_CATCON+; 
 this.w_CATCOM+this.w_CODMAR+this.w_CODART+this.w_CODMAG+STR(this.w_CODPER)+this.w_CODDES+this.w_VOCCEN+this.w_CODCEN+; 
 this.w_CODCOM+this.w_CODATT+this.w_CODPAG+this.w_CODAG2+this.w_CODORN+this.w_CODVET+this.w_PROVIN+this.w_CONSUP+this.w_CODUNI+this.w_CODFAB)
      do case
        case this.w_PERIOD=1
          REPLACE TmpElab.TOTQTA with TmpElab.TOTQTA +this.w_TOTQTA
          REPLACE TmpElab.TOTIMP with TmpElab.TOTIMP +VAL2VAL(this.w_TOTIMP,this.w_CAOVAL,this.w_DATDOC,i_DATSYS,this.oParentObject.w_CAMVAL,this.oParentObject.w_DECIMI)
        case this.w_PERIOD=2
          REPLACE TmpElab.TOTQTA2 with TmpElab.TOTQTA2 +this.w_TOTQTA
          REPLACE TmpElab.TOTIMP2 with TmpElab.TOTIMP2 +VAL2VAL(this.w_TOTIMP,this.w_CAOVAL,this.w_DATDOC,i_DATSYS,this.oParentObject.w_CAMVAL,this.oParentObject.w_DECIMI)
        case this.w_PERIOD=3
          REPLACE TmpElab.TOTQTA3 with TmpElab.TOTQTA3 +this.w_TOTQTA
          REPLACE TmpElab.TOTIMP3 with TmpElab.TOTIMP3 +VAL2VAL(this.w_TOTIMP,this.w_CAOVAL,this.w_DATDOC,i_DATSYS,this.oParentObject.w_CAMVAL,this.oParentObject.w_DECIMI)
      endcase
    else
      APPEND BLANK
      REPLACE CODFOR with this.w_CODFOR
      REPLACE CODZON with this.w_CODZON
      REPLACE CATOMO with this.w_CATOMO
      REPLACE CATCOM with this.w_CATCOM
      REPLACE CODAGE with this.w_CODAGE
      REPLACE CATCON with this.w_CATCON
      REPLACE CODMAR with this.w_CODMAR
      REPLACE CODMAG with this.w_CODMAG
      REPLACE CODART with this.w_CODART
      REPLACE CODFAM with this.w_CODFAM
      REPLACE CODGRU with this.w_CODGRU
      REPLACE CODPER with this.w_CODPER,DESCRI with this.w_DESCRI
      REPLACE UNIMIS with this.w_UNIMIS
      REPLACE CODDES with this.w_CODDES
      REPLACE VOCCEN with this.w_VOCCEN
      REPLACE CODCEN with this.w_CODCEN
      REPLACE CODCOM with this.w_CODCOM
      REPLACE CODATT with this.w_CODATT
      REPLACE CODPAG with this.w_CODPAG
      REPLACE CODAG2 with this.w_CODAG2
      REPLACE CODORN with this.w_CODORN
      REPLACE CODVET with this.w_CODVET
      REPLACE PROVIN with this.w_PROVIN
      REPLACE CONSUP with this.w_CONSUP
      REPLACE CODUNI with this.w_CODUNI
      REPLACE CODFAB with this.w_CODFAB
      REPLACE TIPCON with this.w_TIPCON
      do case
        case this.w_PERIOD=1
          REPLACE TOTQTA with this.w_TOTQTA
          REPLACE TOTIMP with VAL2VAL(this.w_TOTIMP,this.w_CAOVAL,this.w_DATDOC,i_DATSYS,this.oParentObject.w_CAMVAL,this.oParentObject.w_DECIMI)
        case this.w_PERIOD=2
          REPLACE TOTQTA2 with this.w_TOTQTA
          REPLACE TOTIMP2 with VAL2VAL(this.w_TOTIMP,this.w_CAOVAL,this.w_DATDOC,i_DATSYS,this.oParentObject.w_CAMVAL,this.oParentObject.w_DECIMI)
        case this.w_PERIOD=3
          REPLACE TOTQTA3 with this.w_TOTQTA
          REPLACE TOTIMP3 with VAL2VAL(this.w_TOTIMP,this.w_CAOVAL,this.w_DATDOC,i_DATSYS,this.oParentObject.w_CAMVAL,this.oParentObject.w_DECIMI)
      endcase
      REPLACE ANNO with this.w_ANNO
    endif
    endscan
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contolli sui Criteri per Excel e per i Grafici
    if empty(this.w_mess1)
      do case
        case _Curs_CRIDELAB.CENUMCAM=2 and (((this.oParentObject.w_codforini<>this.oParentObject.w_codforfin and this.pParam="EXCE") and (this.oParentObject.w_codclini<>this.oParentObject.w_codclifin and this.pParam="EXCE")) or (empty(this.oParentObject.w_codclini) and empty(this.oParentObject.w_codforini)))
          this.w_MESS1 = ah_Msgformat("Attenzione cliente\fornitore non specificato")
        case _Curs_CRIDELAB.CENUMCAM=3 and (( this.oParentObject.w_ageini<>this.oParentObject.w_agefin and this.pParam="EXCE") or empty(this.oParentObject.w_ageini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione agente non specificato")
        case _Curs_CRIDELAB.CENUMCAM=4 and ( (this.oParentObject.w_zonini<>this.oParentObject.w_zonfin and this.pParam="EXCE") or empty(this.oParentObject.w_zonini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione zona non specificata")
        case _Curs_CRIDELAB.CENUMCAM=5 and ( (this.oParentObject.w_grupmini<>this.oParentObject.w_grupmfin and this.pParam="EXCE") or empty(this.oParentObject.w_grupmini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione gruppo merceologico non specificato")
        case _Curs_CRIDELAB.CENUMCAM=6 and (( this.oParentObject.w_famini<>this.oParentObject.w_famfin and this.pParam="EXCE") or empty(this.oParentObject.w_famini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione famiglia non specificata")
        case _Curs_CRIDELAB.CENUMCAM=7 and (( this.oParentObject.w_catomini<>this.oParentObject.w_catomfin and this.pParam="EXCE") or empty(this.oParentObject.w_catomini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione categoria omogenea non specificata")
        case _Curs_CRIDELAB.CENUMCAM=8 and ( (this.oParentObject.w_catconini<>this.oParentObject.w_catconfin and this.pParam="EXCE") or empty(this.oParentObject.w_catconini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione categoria contabile non specificata")
        case _Curs_CRIDELAB.CENUMCAM=9 and ( (this.oParentObject.w_catcomini<>this.oParentObject.w_catcomfin and this.pParam="EXCE") or empty(this.oParentObject.w_catcomini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione categoria commerciale non specificata")
        case _Curs_CRIDELAB.CENUMCAM=10 and ( (this.oParentObject.w_marcini<>this.oParentObject.w_marcfin and this.pParam="EXCE")or empty(this.oParentObject.w_marcini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione marca non specificata")
        case _Curs_CRIDELAB.CENUMCAM=11 and (( this.oParentObject.w_artini<>this.oParentObject.w_artfin and this.pParam="EXCE") or empty(this.oParentObject.w_artini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione articolo non specificato")
        case _Curs_CRIDELAB.CENUMCAM=12 and ( (this.oParentObject.w_magini<>this.oParentObject.w_magfin and this.pParam="EXCE") or empty(this.oParentObject.w_magini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione magazzino non specificato")
      endcase
      do case
        case _Curs_CRIDELAB.CENUMCAM=14 and (( w_coddesini<>w_coddesfin and this.pParam="EXCE") or empty(w_coddesini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione destinazione non specificata")
        case _Curs_CRIDELAB.CENUMCAM=15 and ( (this.oParentObject.w_vocceini<>this.oParentObject.w_voccefin and this.pParam="EXCE") or empty(this.oParentObject.w_vocceini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione voce di C\R non specificata")
        case _Curs_CRIDELAB.CENUMCAM=16 and (( this.oParentObject.w_codceini<>this.oParentObject.w_codcefin and this.pParam="EXCE") or empty(this.oParentObject.w_codceini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione centro di C\R non specificato")
        case _Curs_CRIDELAB.CENUMCAM=17 and ( (this.oParentObject.w_commini<>this.oParentObject.w_commfin and this.pParam="EXCE") or empty(this.oParentObject.w_commini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione commessa non specificata")
        case _Curs_CRIDELAB.CENUMCAM=18 and ( (this.oParentObject.w_attivini<>this.oParentObject.w_attivfin and this.pParam="EXCE") or empty(this.oParentObject.w_attivini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione attivit� non specificata")
        case _Curs_CRIDELAB.CENUMCAM=19 and (( this.oParentObject.w_pagamini<>this.oParentObject.w_pagamfin and this.pParam="EXCE") or empty(this.oParentObject.w_pagamini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione pagamento non specificato")
        case _Curs_CRIDELAB.CENUMCAM=20 and (( this.oParentObject.w_codagini<>this.oParentObject.w_codagfin and this.pParam="EXCE") or empty(this.oParentObject.w_codagini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione capo area non specificato")
        case _Curs_CRIDELAB.CENUMCAM=21 and ( (this.oParentObject.w_codornini<>this.oParentObject.w_codornfin and this.pParam="EXCE") or empty(this.oParentObject.w_codornini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione per conto di non specificato")
        case _Curs_CRIDELAB.CENUMCAM=22 and ( (this.oParentObject.w_codveini<>this.oParentObject.w_codvefin and this.pParam="EXCE") or empty(this.oParentObject.w_codveini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione vettore non specificato")
        case _Curs_CRIDELAB.CENUMCAM=23 and ( (this.oParentObject.w_provini<>this.oParentObject.w_provfin and this.pParam="EXCE")or empty(this.oParentObject.w_provini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione provincia non specificata")
        case _Curs_CRIDELAB.CENUMCAM=24 and (( this.oParentObject.w_mastrini<>this.oParentObject.w_mastrfin and this.pParam="EXCE") or empty(this.oParentObject.w_mastrini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione mastro non specificato")
        case _Curs_CRIDELAB.CENUMCAM=25 and ( (this.oParentObject.w_unimini<>this.oParentObject.w_unimifin and this.pParam="EXCE") or empty(this.oParentObject.w_unimini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione unit� di misura non specificato")
        case _Curs_CRIDELAB.CENUMCAM=26 and ( (this.oParentObject.w_forabini<>this.oParentObject.w_forabfin and this.pParam="EXCE") or empty(this.oParentObject.w_forabini)) 
          this.w_MESS1 = ah_Msgformat("Attenzione fornitore abituale non specificato")
      endcase
    endif
    if empty(this.w_mess1) 
      this.w_CmdSel1 = alltrim(this.w_CmdSel1) + Campi(_Curs_CRIDELAB.CENUMCAM,1)+","
      this.w_CmdGrp1 = alltrim(this.w_CmdGrp1) +Campi(_Curs_CRIDELAB.CENUMCAM,1) +","
      this.w_CmdSel2 = Campi(_Curs_CRIDELAB.CENUMCAM,1) +","
      this.w_FIELDAXIS2 = alltrim(Campi(_Curs_CRIDELAB.CENUMCAM,1))
    endif
    if not empty(this.w_mess1)
      if not ah_YesNo("%1%0Vuoi andare avanti ugualmente?","", alltrim(this.w_MESS1) )
        i_retcode = 'stop'
        return
      else
        this.w_MESS1 = space(100)
      endif
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili da inviare al report
    L_CECODICE=this.oParentObject.w_CECODICE
    L_CEDESCRI=this.oParentObject.w_CEDESCRI
    L_DIVISA=this.oParentObject.w_DIVISA
    L_CAMVAL=this.oParentObject.w_CAMVAL
    L_CATCOMINI=this.oParentObject.w_CATCOMINI
    L_CATCOMFIN=this.oParentObject.w_CATCOMFIN
    L_OBSOLETI=this.oParentObject.w_OBSOLETI
    L_DESCCOM=this.oParentObject.w_DESCCOM
    L_DESCCOM1=this.oParentObject.w_DESCCOM1
    L_DESMAG1=this.oParentObject.w_DESMAG1
    L_DESMAG=this.oParentObject.w_DESMAG
    L_MAGINI=this.oParentObject.w_MAGINI
    L_MAGFIN=this.oParentObject.w_MAGFIN
    L_DESAGE1=this.oParentObject.w_DESAGE1
    L_DESAGE=this.oParentObject.w_DESAGE
    L_AGEINI=this.oParentObject.w_AGEINI
    L_AGEFIN=this.oParentObject.w_AGEFIN
    L_DESZON1=this.oParentObject.w_DESZON1
    L_DESZON=this.oParentObject.w_DESZON
    L_ZONINI=this.oParentObject.w_ZONINI
    L_ZONFIN=this.oParentObject.w_ZONFIN
    L_CATCONINI=this.oParentObject.w_CATCONINI
    L_CATCONFIN=this.oParentObject.w_CATCONFIN
    L_DESCCON=this.oParentObject.w_DESCCON
    L_DESCCON1=this.oParentObject.w_DESCCON1
    L_CODFORINI=this.oParentObject.w_CODFORINI
    L_CODFORFIN=this.oParentObject.w_CODFORFIN
    L_DESCLI=this.oParentObject.w_DESCLI
    L_DESCLI1=this.oParentObject.w_DESCLI1
    L_DESFOR=this.oParentObject.w_DESFOR
    L_DESFOR1=this.oParentObject.w_DESFOR1
    L_CODCLINI=this.oParentObject.w_CODCLINI
    L_CODCLIFIN=this.oParentObject.w_CODCLIFIN
    L_CATOMINI=this.oParentObject.w_CATOMINI
    L_CATOMFIN=this.oParentObject.w_CATOMFIN
    L_DESCOMO=this.oParentObject.w_DESCOMO
    L_DESCOMO1=this.oParentObject.w_DESCOMO1
    L_FAMINI=this.oParentObject.w_FAMINI
    L_FAMFIN=this.oParentObject.w_FAMFIN
    L_DESCFAM= this.oParentObject.w_DESCFAM
    L_DESCFAM1= this.oParentObject.w_DESCFAM1
    L_MARCINI=this.oParentObject.w_MARCINI
    L_MARCFIN=this.oParentObject.w_MARCFIN
    L_DESMARCA=this.oParentObject.w_DESMARCA
    L_DESMARCA1=this.oParentObject.w_DESMARCA1
    L_GRUPMINI=this.oParentObject.w_GRUPMINI
    L_GRUPMFIN=this.oParentObject.w_GRUPMFIN
    L_FORABINI=this.oParentObject.w_FORABINI
    L_FORABFIN=this.oParentObject.w_FORABFIN
    L_DESFORAB=this.oParentObject.w_DESFORAB
    L_DESFORAB1=this.oParentObject.w_DESFORAB1
    L_CODAGINI=this.oParentObject.w_CODAGINI
    L_CODAGFIN=this.oParentObject.w_CODAGFIN
    L_DESAG2=this.oParentObject.w_DESAG2
    L_DESAG21=this.oParentObject.w_DESAG21
    L_CODVEINI=this.oParentObject.w_CODVEINI
    L_CODVEFIN=this.oParentObject.w_CODVEFIN
    L_DESVET=this.oParentObject.w_DESVET
    L_DESVET1=this.oParentObject.w_DESVET1
    L_MASTRINI=this.oParentObject.w_MASTRINI
    L_MASTRFIN=this.oParentObject.w_MASTRFIN
    L_DESMAS=this.oParentObject.w_DESMAS
    L_DESMAS1=this.oParentObject.w_DESMAS1
    L_CODESINI=this.oParentObject.w_CODESINI
    L_CODESFIN=this.oParentObject.w_CODESFIN
    L_DESDEINI=this.oParentObject.w_DESDEINI
    L_DESDEFIN=this.oParentObject.w_DESDEFIN
    L_CODCEINI=this.oParentObject.w_CODCEINI
    L_CODCEFIN=this.oParentObject.w_CODCEFIN
    L_DESSPIA=this.oParentObject.w_DESSPIA
    L_DESSPIA1=this.oParentObject.w_DESSPIA1
    L_VOCCEINI=this.oParentObject.w_VOCCEINI
    L_VOCCEFIN=this.oParentObject.w_VOCCEFIN
    L_VODESCR=this.oParentObject.w_VODESCR
    L_VODESCR1=this.oParentObject.w_VODESCR1
    L_COMMINI=this.oParentObject.w_COMMINI
    L_COMMFIN=this.oParentObject.w_COMMFIN
    L_COMMDES=this.oParentObject.w_COMMDES
    L_COMMDES1=this.oParentObject.w_COMMDES1
    L_CODORNINI=this.oParentObject.w_CODORNINI
    L_CODORNFIN=this.oParentObject.w_CODORNFIN
    L_DESORN=this.oParentObject.w_DESORN
    L_DESORN1=this.oParentObject.w_DESORN1
    L_ATTIVINI=this.oParentObject.w_ATTIVINI
    L_ATTIVFIN=this.oParentObject.w_ATTIVFIN
    L_ATTIDES=this.oParentObject.w_ATTIDES
    L_ATTIDES1=this.oParentObject.w_ATTIDES1
    L_PROVINI=this.oParentObject.w_PROVINI
    L_PROVFIN=this.oParentObject.w_PROVFIN
    L_DESGRUP=this.oParentObject.w_DESGRUP
    L_DESGRUP1=this.oParentObject.w_DESGRUP1
    L_PAGAMINI=this.oParentObject.w_PAGAMINI
    L_PAGAMFIN=this.oParentObject.w_PAGAMFIN
    L_PAGADES=this.oParentObject.w_PAGADES
    L_PAGADES1=this.oParentObject.w_PAGADES1
    L_UNIMINI=this.oParentObject.w_UNIMINI
    L_UNIMIFIN=this.oParentObject.w_UNIMIFIN
    L_ARTINI=this.oParentObject.w_ARTINI
    L_ARTFIN=this.oParentObject.w_ARTFIN
    L_DESCART=this.oParentObject.w_DESCART
    L_DESCART1=this.oParentObject.w_DESCART1
    L_DECIMI=this.oParentObject.w_DECIMI
    L_DIVISA=this.oParentObject.w_DIVISA
    L_CAMVAL=this.oParentObject.w_CAMVAL
    L_SIMVAL=this.oParentObject.w_SIMVAL
    L_DESCR=this.oParentObject.w_DESCR
    L_FORMAT = v_PV[20*(this.oParentObject.w_decimi+2)]
    do case
      case this.oParentObject.w_CEPERIO2="F"
        L_DATFIN=this.oParentObject.w_DATFIN1
        L_DATINI=this.oParentObject.w_DATINI1
      case this.oParentObject.w_CEPERIO2="T" AND this.oParentObject.w_CEPERIO3="F"
        L_DATFIN=this.oParentObject.w_DATFIN1
        L_DATINI=this.oParentObject.w_DATINI1
        L_CEPERIO2=this.oParentObject.w_CEPERIO2
        L_DATFIN2=this.oParentObject.w_DATFIN2
        L_DATINI2=this.oParentObject.w_DATINI2
      case this.oParentObject.w_CEPERIO3="T"
        L_DATFIN=this.oParentObject.w_DATFIN1
        L_DATINI=this.oParentObject.w_DATINI1
        L_CEPERIO2=this.oParentObject.w_CEPERIO2
        L_DATFIN2=this.oParentObject.w_DATFIN2
        L_DATINI2=this.oParentObject.w_DATINI2
        L_DATFIN3=this.oParentObject.w_DATFIN3
        L_DATINI3=this.oParentObject.w_DATINI3
        L_CEPERIO3=this.oParentObject.w_CEPERIO3
    endcase
    do case
      case this.pParam="REPO"
        this.w_FILE = this.oParentObject.w_REPORT
      case this.pParam="EXCE"
        this.w_FILE = this.oParentObject.w_EXCEL
        if this.w_EXMES=.T. AND type("L_codper")<>"U" and not empty(this.w_cmdsel1) and this.w_tipper="ME"
          * --- Se non � specificato il raggruppamento per periodo il cursore non viene rielaborato 
          if type("L_codper")<>"U" and not empty(this.w_cmdsel1) and this.w_tipper="ME"
            Sel1 = this.w_CmdSel1 + " UNIMIS,sum(TOTQTA) as TOTQTA,sum(TOTIMP) as TOTIMP"+;
            ",sum(TOTQTA2 )as TOTQTA2,sum(TOTIMP2) as TOTIMP2 ,sum( TOTQTA3) "+ ;
            +"as TOTQTA3,sum(TOTIMP3) as TOTIMP3"
            grp1= LEFT(this.w_CmdGrp1 , LEN(alltrim(this.w_CmdGrp1))-1)
            Select &Sel1 FROM tmpelab group by &grp1 INTO CURSOR tempor
            * --- rielaboro cursore per foglio excel mensile
            if (year(this.oParentObject.w_datini1)+year(this.oParentObject.w_datini2)+year(this.oParentObject.w_datini3))=(year(this.oParentObject.w_datfin1)+year(this.oParentObject.w_datfin2)+year(this.oParentObject.w_datfin3))
              select tempor
              b = WRCURSOR("tempor")
              GO TOP
              scan
              select tempor
              this.w_mese = right(alltrim(str(tempor.CODPER)),2)
              replace tempor.CODPER with val(this.w_mese)
              endscan
              for i=1 to 12
              GO TOP
              locate for int(val(right(alltrim(str(tempor.CODPER)),2))) =i
              if found()
                replace tempor.CODPER with i
                replace tempor.TOTIMP with cp_ROUND(TEMPOR.TOTIMP,this.w_DECTOT)
                replace tempor.TOTIMP2 with cp_ROUND(TEMPOR.TOTIMP2,this.w_DECTOT)
                replace tempor.TOTIMP3 with cp_ROUND(TEMPOR.TOTIMP3,this.w_DECTOT)
              else
                * --- inserisco record del mese se assente
                append blank
                replace tempor.CODPER with i
              endif
              endfor
              select &sel1 from tempor group by codper order by tempor.CODPER into cursor __tmp__
            else
              ah_ErrorMsg("Attenzione date non all'interno dello stesso anno",,"")
              i_retcode = 'stop'
              return
            endif
          endif
        else
          * --- Arrotondo Importi per Foglio Excel Senza rielaborazione Mensile
          b = WRCURSOR("__tmp__")
          select __TMP__
          go top
          scan
          replace TOTIMP with cp_ROUND(TOTIMP,this.w_DECTOT)
          replace TOTIMP2 with cp_ROUND(TOTIMP2,this.w_DECTOT)
          replace TOTIMP3 with cp_ROUND(TOTIMP3,this.w_DECTOT)
          endscan
        endif
      case this.pParam="GRAF"
        this.w_FILE = this.oParentObject.w_GRAFH
        Sel1 = this.w_CmdSel2 + "UNIMIS,sum(TOTQTA) as Quantit�1,sum(TOTIMP) as Periodo1"+;
        ",sum(TOTQTA2 )as Quantit�2,sum(TOTIMP2) as Periodo2 ,sum( TOTQTA3) "+ ;
        +"as Quantit�3,sum(TOTIMP3) as Periodo3"
        grp1= LEFT(this.w_Cmdsel2 , LEN(alltrim(this.w_Cmdsel2))-1)
        if empty(nvl(this.w_cmdsel2," "))
          ah_ErrorMsg("Attenzione nessun intervallo di valori, per i raggruppamenti del criterio, � stato specificato",,"")
          i_retcode = 'stop'
          return
        else
          USE IN SELECT("fieldinfo")
          CREATE CURSOR FIELDINFO ( srcfldname C(20) , Legend C(20) ) 
 
 Insert Into FIELDINFO Values ("Quantit�1" , "Quantit�1") 
 Insert Into FIELDINFO Values ("Periodo1" , "Periodo1") 
 Insert Into FIELDINFO Values ("Quantit�2" , "Quantit�2") 
 Insert Into FIELDINFO Values ("Periodo2" , "Periodo2") 
 Insert Into FIELDINFO Values ("Quantit�3" , "Quantit�3") 
 Insert Into FIELDINFO Values ("Periodo3" , "Periodo3")
          Select &Sel1 FROM tmpelab group by &grp1 ORDER BY &grp1 INTO CURSOR __tmp__ readwrite
          do gsst_kgs with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
    if this.pParam<>"GRAF"
      CP_CHPRN(Alltrim(this.w_FILE), " ", this)
    endif
    this.oParentObject.w_MONETE = "C"
    this.oParentObject.w_DIVISA = g_PERVAL
    this.oParentObject.w_CAMVAL = GETCAM(this.oParentObject.w_DIVISA, i_DATSYS, 7)
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali Sulle Date
    if (EMPTY(this.oParentObject.w_DATINI1) OR EMPTY(this.oParentObject.w_DATFIN1)) and empty(this.w_MESS)
      this.w_MESS = ah_MsgFormat("Primo periodo mancante o incompleto")
      this.w_OBJCTRL = this.w_GSST_KCR.GetCtrl( "w_DATINI1" )
      this.oParentObject.w_OK = "B"
    endif
    if this.oParentObject.w_DATINI1> this.oParentObject.w_DATFIN1 and empty(this.w_MESS)
      this.w_MESS = ah_MsgFormat("Data finale inferiore data iniziale nel primo periodo")
      this.w_OBJCTRL = this.w_GSST_KCR.GetCtrl( "w_DATFIN1" )
      this.oParentObject.w_OK = "B"
    endif
    if this.oParentObject.w_CEPERIO2 = "T" AND (this.oParentObject.w_DATINI2 < this.oParentObject.w_DATFIN1 OR this.oParentObject.w_DATFIN2 < this.oParentObject.w_DATINI2 OR (EMPTY(this.oParentObject.w_DATINI2) OR EMPTY(this.oParentObject.w_DATFIN2)) )
      if (EMPTY(this.oParentObject.w_DATINI2) OR EMPTY(this.oParentObject.w_DATFIN2)) and empty(this.w_MESS)
        this.w_MESS = ah_MsgFormat("Secondo periodo mancante o incompleto")
        this.w_OBJCTRL = this.w_GSST_KCR.GetCtrl( "w_DATINI2" )
        this.oParentObject.w_OK = "B"
      endif
      if this.oParentObject.w_DATINI2 > this.oParentObject.w_DATFIN2 and empty(this.w_MESS)
        this.w_MESS = ah_MsgFormat("Data finale inferiore data iniziale nel secondo periodo")
        this.w_OBJCTRL = this.w_GSST_KCR.GetCtrl( "w_DATFIN2" )
        this.oParentObject.w_OK = "B"
      endif
      if this.oParentObject.w_DATINI2 < this.oParentObject.w_DATFIN1 and empty(this.w_MESS) 
        this.w_MESS = ah_MsgFormat("Data iniziale secondo periodo inferiore data finale primo periodo")
        this.w_OBJCTRL = this.w_GSST_KCR.GetCtrl( "w_DATINI2" )
        this.oParentObject.w_OK = "C"
      endif
    endif
    if this.oParentObject.w_CEPERIO3 = "T" and empty(this.w_MESS) and (this.oParentObject.w_DATINI3 < this.oParentObject.w_DATFIN2 OR this.oParentObject.w_DATFIN3 < this.oParentObject.w_DATINI3 OR (EMPTY(this.oParentObject.w_DATINI3) OR EMPTY(this.oParentObject.w_DATFIN3)) )
      if (EMPTY(this.oParentObject.w_DATINI3) OR EMPTY(this.oParentObject.w_DATFIN3))and empty(this.w_MESS)
        this.w_MESS = ah_Msgformat("Terzo periodo mancante o incompleto")
        this.w_OBJCTRL = this.w_GSST_KCR.GetCtrl( "w_DATINI3" )
        this.oParentObject.w_OK = "B"
      endif
      if this.oParentObject.w_DATINI3 > this.oParentObject.w_DATFIN3 and empty(this.w_MESS)
        this.w_MESS = ah_MsgFormat("Data finale inferiore data iniziale nel terzo periodo")
        this.w_OBJCTRL = this.w_GSST_KCR.GetCtrl( "w_DATFIN3" )
        this.oParentObject.w_OK = "B"
      endif
      if this.oParentObject.w_DATINI3 < this.oParentObject.w_DATFIN2 and empty(this.w_MESS) 
        this.w_MESS = ah_MsgFormat("Data iniziale terzo periodo inferiore data finale secondo periodo")
        this.w_OBJCTRL = this.w_GSST_KCR.GetCtrl( "w_DATINI3" )
        this.oParentObject.w_OK = "C"
      endif
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='CAU_SELE'
    this.cWorkTables[3]='CRIDELAB'
    this.cWorkTables[4]='CRIMELAB'
    this.cWorkTables[5]='VALUTE'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_CRIDELAB')
      use in _Curs_CRIDELAB
    endif
    if used('_Curs_CAU_SELE')
      use in _Curs_CAU_SELE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
