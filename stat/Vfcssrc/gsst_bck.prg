* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsst_bck                                                        *
*              Controlli data generazione                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_24]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-31                                                      *
* Last revis.: 2000-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsst_bck",oParentObject)
return(i_retval)

define class tgsst_bck as StdBatch
  * --- Local variables
  w_MESS = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if not empty(this.oParentObject.w_DATINI) AND NOT EMPTY(this.oParentObject.w_DATULT) AND this.oParentObject.w_DATINI < this.oParentObject.w_DATULT
      this.w_MESS = "La data inserita � inferiore alla data di ultima generazione%0verranno rigenerati i movimenti relativi ai giorni precedenti al %1"
      ah_ErrorMsg(this.w_MESS,,"",DTOC(this.oParentObject.w_DATULT))
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
