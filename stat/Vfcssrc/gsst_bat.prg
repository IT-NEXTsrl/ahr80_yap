* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsst_bat                                                        *
*              Ultima creazione statistica                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_228]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-31                                                      *
* Last revis.: 2000-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsst_bat",oParentObject)
return(i_retval)

define class tgsst_bat as StdBatch
  * --- Local variables
  w_TROV = .f.
  * --- WorkFile variables
  ULTIMARC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruzione Archivio Statistico (da GSST_KEL)
    this.w_TROV = .F.
    * --- Select from ULTIMARC
    i_nConn=i_TableProp[this.ULTIMARC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ULTIMARC_idx,2],.t.,this.ULTIMARC_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ULTIMARC ";
           ,"_Curs_ULTIMARC")
    else
      select * from (i_cTable);
        into cursor _Curs_ULTIMARC
    endif
    if used('_Curs_ULTIMARC')
      select _Curs_ULTIMARC
      locate for 1=1
      do while not(eof())
      this.w_TROV = .T.
        select _Curs_ULTIMARC
        continue
      enddo
      use
    endif
    if this.w_TROV=.T.
      * --- Se esiste il record legge parametri
      * --- Try
      local bErr_03582700
      bErr_03582700=bTrsErr
      this.Try_03582700()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile lettura archivio ultima generazione",,"")
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_03582700
      * --- End
    else
      * --- Prima creazione del record
      this.oParentObject.w_DATULT = i_DATSYS
      * --- Insert into ULTIMARC
      i_nConn=i_TableProp[this.ULTIMARC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ULTIMARC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ULTIMARC_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"UGCODULT"+",UGAUTNUM"+",UGFLAG"+",UGPRIMA"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(1),'ULTIMARC','UGCODULT');
        +","+cp_NullLink(cp_ToStrODBC(1),'ULTIMARC','UGAUTNUM');
        +","+cp_NullLink(cp_ToStrODBC("F"),'ULTIMARC','UGFLAG');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATULT),'ULTIMARC','UGPRIMA');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'UGCODULT',1,'UGAUTNUM',1,'UGFLAG',"F",'UGPRIMA',this.oParentObject.w_DATULT)
        insert into (i_cTable) (UGCODULT,UGAUTNUM,UGFLAG,UGPRIMA &i_ccchkf. );
           values (;
             1;
             ,1;
             ,"F";
             ,this.oParentObject.w_DATULT;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Insert record ultima creazione fallita'
        return
      endif
      this.oParentObject.w_DATULT = cp_CharToDate("  -  -  ")
      this.oParentObject.w_DATINI = i_DATSYS
      this.oParentObject.w_DATFIN = this.oParentObject.w_DATINI + 30
    endif
  endproc
  proc Try_03582700()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from ULTIMARC
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ULTIMARC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ULTIMARC_idx,2],.t.,this.ULTIMARC_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UGULTIMA"+;
        " from "+i_cTable+" ULTIMARC where ";
            +"UGCODULT = "+cp_ToStrODBC(1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UGULTIMA;
        from (i_cTable) where;
            UGCODULT = 1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DATULT = NVL(cp_ToDate(_read_.UGULTIMA),cp_NullValue(_read_.UGULTIMA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.oParentObject.w_DATINI = this.oParentObject.w_DATULT 
    this.oParentObject.w_DATFIN = this.oParentObject.w_DATINI + 30
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ULTIMARC'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ULTIMARC')
      use in _Curs_ULTIMARC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
