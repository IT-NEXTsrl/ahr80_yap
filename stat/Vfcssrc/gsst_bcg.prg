* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsst_bcg                                                        *
*              Controlli sui tipi di raggruppamento                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_61]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-07                                                      *
* Last revis.: 2000-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsst_bcg",oParentObject)
return(i_retval)

define class tgsst_bcg as StdBatch
  * --- Local variables
  w_NUMCAM = 0
  w_NOMCAM = space(10)
  w_COUNT = 0
  w_COUNT1 = 0
  w_MESS = space(100)
  w_TESTPER = .f.
  * --- WorkFile variables
  CRIDELAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli sui tipi di raggruppamenti
    this.w_TESTPER = .F.
    this.w_NUMCAM = 0
    do while this.w_COUNT<=1 AND this.w_NUMCAM<13 and (not this.w_TESTPER)
      this.w_COUNT1 = 0
      this.w_COUNT = 0
      this.w_NUMCAM = this.w_NUMCAM +1
      SELECT (this.oParentObject.cTrsName)
      go top
      scan for t_CPROWORD<>0 AND t_CENUMCAM<>0
      this.w_COUNT1 = this.w_COUNT1 +1
      if t_CENUMCAM=12 AND EMPTY(NVL(t_CETIPPER,0))
        this.w_TESTPER = .T.
      endif
      if t_CENUMCAM=this.w_NUMCAM 
        this.w_COUNT = this.w_COUNT+1
      endif
      endscan
    enddo
    if this.w_COUNT1>13 or this.w_COUNT>1 or this.w_TESTPER
      do case
        case this.w_COUNT>1
          this.w_MESS = ah_MsgFormat("Esistono raggruppamenti inseriti pi� volte")
        case this.w_COUNT1>13 
          this.w_MESS = ah_MsgFormat("Superato numero massimo di raggruppamenti inseribili")
        case  this.w_TESTPER
          this.w_MESS = ah_MsgFormat("Specificare tipo periodo nel relativo raggruppamento")
      endcase
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CRIDELAB'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
