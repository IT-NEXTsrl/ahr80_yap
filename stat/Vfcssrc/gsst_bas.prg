* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsst_bas                                                        *
*              Creazione archivio statistico                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_544]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-31                                                      *
* Last revis.: 2007-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsst_bas",oParentObject)
return(i_retval)

define class tgsst_bas as StdBatch
  * --- Local variables
  w_PRIMA = ctod("  /  /  ")
  w_ULTIMA = ctod("  /  /  ")
  w_NUMERO = 0
  w_FLAG = space(1)
  w_MSG = space(100)
  w_MESS = space(100)
  w_NREC = 0
  w_EREC = 0
  w_INIMOV = ctod("  /  /  ")
  w_FINMOV = ctod("  /  /  ")
  w_STSERIAL = 0
  w_STSERIA1 = 0
  w_STSERIA2 = 0
  w_STVALNAZ = space(5)
  * --- WorkFile variables
  ART_ICOL_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  STATISTI_idx=0
  ULTIMARC_idx=0
  STATELAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruzione Archivio Statistico
    * --- Legge permesso di scrittura chiave archivio Statistico
    this.w_FLAG = " "
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_025D1388
    bErr_025D1388=bTrsErr
    this.Try_025D1388()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_FLAG = " "
      ah_ErrorMsg("Impossibile leggere archivio ultima generazione",,"")
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_025D1388
    * --- End
    * --- Se posso scrivere la tabella in maniera esclusiva...
    do case
      case this.w_FLAG = "F"
        * --- Try
        local bErr_025D18C8
        bErr_025D18C8=bTrsErr
        this.Try_025D18C8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Impossibile leggere archivio ultima generazione",,"")
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_025D18C8
        * --- End
      case this.w_FLAG = "T"
        this.w_MSG = "Archivio ultima generazione in uso da altro processo%0impossibile continuare"
        ah_ErrorMsg(this.w_MSG,,"")
        * --- accept error
        bTrsErr=.f.
    endcase
    * --- commit
    cp_EndTrs(.t.)
    * --- Se NON HA DIRITTO di generazione allora finisce
    if this.w_FLAG <> "F"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Esco dal Batch
      i_retcode = 'stop'
      return
    endif
    * --- Inserimento Archivio Statistico
    ah_Msg("Generazione archivio statistico dal %1 al %2",.T.,.F.,.F.,dtoc(this.oParentObject.w_DATINI),dtoc(this.oParentObject.w_DATFIN))
    this.w_NREC = 0
    this.w_EREC = 0
    this.w_INIMOV = this.oParentObject.w_DATINI
    this.w_FINMOV = this.oParentObject.w_DATINI
    this.w_STVALNAZ = g_PERVAL
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Messaggio di chiusura
    if this.w_NREC<>0
      this.w_MSG = "Elaborazione ultimata%0sono stati estratti %1 movimenti su %2"
      ah_ErrorMsg(this.w_MSG,,"",alltrim(str(this.w_NREC)),alltrim(str(this.w_EREC)))
      * --- Rilascio FLAG per SCRITTURA, Aggiorna Date di Elaborazione
      this.w_PRIMA = IIF(this.oParentObject.w_DATINI<=this.w_PRIMA, this.w_INIMOV, this.w_PRIMA)
      this.w_ULTIMA = IIF(this.oParentObject.w_DATFIN>=this.w_ULTIMA, this.w_FINMOV, this.w_ULTIMA)
      this.w_PRIMA = IIF(this.w_PRIMA>this.w_ULTIMA, this.w_ULTIMA, this.w_PRIMA)
      * --- Try
      local bErr_025D1AA8
      bErr_025D1AA8=bTrsErr
      this.Try_025D1AA8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile scrivere archivio ultima generazione; intervenire manualmente",,"")
      endif
      bTrsErr=bTrsErr or bErr_025D1AA8
      * --- End
    else
      this.w_MSG = "Per le selezioni impostate; non esistono movimenti da elaborare"
      ah_ErrorMsg(this.w_MSG,,"")
      * --- Rilascio FLAG per SCRITTURA
      * --- Try
      local bErr_025D16E8
      bErr_025D16E8=bTrsErr
      this.Try_025D16E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile scrivere archivio ultima generazione; intervenire manualmente",,"")
      endif
      bTrsErr=bTrsErr or bErr_025D16E8
      * --- End
    endif
  endproc
  proc Try_025D1388()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from ULTIMARC
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ULTIMARC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ULTIMARC_idx,2],.t.,this.ULTIMARC_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UGPRIMA,UGULTIMA,UGAUTNUM,UGFLAG"+;
        " from "+i_cTable+" ULTIMARC where ";
            +"UGCODULT = "+cp_ToStrODBC(1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UGPRIMA,UGULTIMA,UGAUTNUM,UGFLAG;
        from (i_cTable) where;
            UGCODULT = 1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PRIMA = NVL(cp_ToDate(_read_.UGPRIMA),cp_NullValue(_read_.UGPRIMA))
      this.w_ULTIMA = NVL(cp_ToDate(_read_.UGULTIMA),cp_NullValue(_read_.UGULTIMA))
      this.w_NUMERO = NVL(cp_ToDate(_read_.UGAUTNUM),cp_NullValue(_read_.UGAUTNUM))
      this.w_FLAG = NVL(cp_ToDate(_read_.UGFLAG),cp_NullValue(_read_.UGFLAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return
  proc Try_025D18C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ULTIMARC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ULTIMARC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ULTIMARC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ULTIMARC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"UGFLAG ="+cp_NullLink(cp_ToStrODBC("T"),'ULTIMARC','UGFLAG');
          +i_ccchkf ;
      +" where ";
          +"UGCODULT = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          UGFLAG = "T";
          &i_ccchkf. ;
       where;
          UGCODULT = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_025D1AA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ULTIMARC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ULTIMARC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ULTIMARC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ULTIMARC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"UGFLAG ="+cp_NullLink(cp_ToStrODBC("F"),'ULTIMARC','UGFLAG');
      +",UGAUTNUM ="+cp_NullLink(cp_ToStrODBC(this.w_STSERIAL),'ULTIMARC','UGAUTNUM');
      +",UGPRIMA ="+cp_NullLink(cp_ToStrODBC(this.w_PRIMA),'ULTIMARC','UGPRIMA');
      +",UGULTIMA ="+cp_NullLink(cp_ToStrODBC(this.w_ULTIMA),'ULTIMARC','UGULTIMA');
      +",UGDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_INIMOV),'ULTIMARC','UGDATINI');
      +",UGDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_FINMOV),'ULTIMARC','UGDATFIN');
          +i_ccchkf ;
      +" where ";
          +"UGCODULT = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          UGFLAG = "F";
          ,UGAUTNUM = this.w_STSERIAL;
          ,UGPRIMA = this.w_PRIMA;
          ,UGULTIMA = this.w_ULTIMA;
          ,UGDATINI = this.w_INIMOV;
          ,UGDATFIN = this.w_FINMOV;
          &i_ccchkf. ;
       where;
          UGCODULT = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Write record ultima creazione fallita'
      return
    endif
    return
  proc Try_025D16E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ULTIMARC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ULTIMARC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ULTIMARC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ULTIMARC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"UGFLAG ="+cp_NullLink(cp_ToStrODBC("F"),'ULTIMARC','UGFLAG');
          +i_ccchkf ;
      +" where ";
          +"UGCODULT = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          UGFLAG = "F";
          &i_ccchkf. ;
       where;
          UGCODULT = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Write record ultima creazione fallita'
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Archivio Statistico
    * --- Cancello i record da ricalcolare:  data iniziale < ultima data finale
    ah_Msg("Eliminazione statistiche del periodo...",.T.)
    * --- Delete from STATISTI
    i_nConn=i_TableProp[this.STATISTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STATISTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.STATISTI_idx,2])
      i_cTempTable=cp_GetTempTableName(i_nConn)
      i_aIndex[1]='STSERIAL,STSERRIF,STROWNUM,STNUMRIF'
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cQueryTable+" where (STDATREG >= "+cp_ToStrODBC(this.oParentObject.w_DATINI)+") AND (STDATREG <= "+cp_ToStrODBC(this.oParentObject.w_DATFIN)+")",.f.,@i_aIndex)
      i_cQueryTable=i_cTempTable
      i_cWhere=i_cTable+".STSERIAL = "+i_cQueryTable+".STSERIAL";
            +" and "+i_cTable+".STSERRIF = "+i_cQueryTable+".STSERRIF";
            +" and "+i_cTable+".STROWNUM = "+i_cQueryTable+".STROWNUM";
            +" and "+i_cTable+".STNUMRIF = "+i_cQueryTable+".STNUMRIF";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".STSERIAL = "+i_cQueryTable+".STSERIAL";
            +" and "+i_cTable+".STSERRIF = "+i_cQueryTable+".STSERRIF";
            +" and "+i_cTable+".STROWNUM = "+i_cQueryTable+".STROWNUM";
            +" and "+i_cTable+".STNUMRIF = "+i_cQueryTable+".STNUMRIF";
            +")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Cancellazione archivi statistici fallita'
      return
    endif
    ah_Msg("Lettura ultimo movimento elaborato...",.T.)
    this.w_STSERIAL = this.w_NUMERO
    VQ_EXEC("..\STAT\EXE\QUERY\ULTSTA.VQR",this,"ELABSTAT")
    if USED("ELABSTAT")
      SELECT ELABSTAT
      GO TOP
      SCAN 
      this.w_STSERIAL = MAX(NVL(ULTREC, 0), this.w_NUMERO)
      ENDSCAN 
      USE
    endif
    ah_Msg("Lettura movimenti da elaborare...",.T.)
    this.w_STSERIA1 = this.w_STSERIAL + 1
    this.w_STSERIA2 = this.w_STSERIAL + 2
    * --- Create temporary table STATELAB
    i_nIdx=cp_AddTableDef('STATELAB') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\STAT\EXE\QUERY\GSST_DOC.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.STATELAB_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    ah_Msg("Scrittura archivio statistiche...",.T.)
    * --- Try
    local bErr_03854B50
    bErr_03854B50=bTrsErr
    this.Try_03854B50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Inserimento record in archivio statistico fallito%0%1",,"",Message())
    endif
    bTrsErr=bTrsErr or bErr_03854B50
    * --- End
    this.w_NREC = i_Rows
    this.w_STSERIAL = this.w_STSERIA2
    VQ_EXEC("..\STAT\EXE\QUERY\ULTTMP.VQR",this,"ELABSTAT")
    if USED("ELABSTAT")
      SELECT ELABSTAT
      GO TOP
      SCAN 
      this.w_EREC = MAX(NVL(TOTREC, 0), this.w_EREC)
      * --- Setta Date limite di Elaborazione
      this.w_PRIMA = MIN(this.w_PRIMA, CP_TODATE(DATMIN))
      this.w_ULTIMA = MAX(this.w_ULTIMA, CP_TODATE(DATMAX))
      this.w_INIMOV = MIN(this.w_INIMOV, CP_TODATE(DATMIN))
      this.w_FINMOV = MAX(this.w_FINMOV, CP_TODATE(DATMAX))
      ENDSCAN 
      USE
    endif
    * --- Drop temporary table STATELAB
    i_nIdx=cp_GetTableDefIdx('STATELAB')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('STATELAB')
    endif
  endproc
  proc Try_03854B50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STATISTI
    i_nConn=i_TableProp[this.STATISTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STATISTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.STATELAB_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.STATISTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali
    * --- Se HA DIRITTO di generazione allora lo RILASCIA
    if this.w_FLAG = "F"
      * --- Try
      local bErr_025D2858
      bErr_025D2858=bTrsErr
      this.Try_025D2858()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile scrivere archivio ultima generazione",,"")
        ah_ErrorMsg("Sbloccare flag scrittura prima della prossima generazione",,"")
      endif
      bTrsErr=bTrsErr or bErr_025D2858
      * --- End
    endif
  endproc
  proc Try_025D2858()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ULTIMARC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ULTIMARC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ULTIMARC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ULTIMARC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"UGFLAG ="+cp_NullLink(cp_ToStrODBC("F"),'ULTIMARC','UGFLAG');
          +i_ccchkf ;
      +" where ";
          +"UGCODULT = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          UGFLAG = "F";
          &i_ccchkf. ;
       where;
          UGCODULT = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Write record ultima creazione fallita'
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='MVM_DETT'
    this.cWorkTables[5]='MVM_MAST'
    this.cWorkTables[6]='STATISTI'
    this.cWorkTables[7]='ULTIMARC'
    this.cWorkTables[8]='*STATELAB'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
