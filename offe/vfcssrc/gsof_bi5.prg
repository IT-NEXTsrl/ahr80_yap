* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bi5                                                        *
*              Eventi da import dettalio offerte                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_123]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-24                                                      *
* Last revis.: 2003-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bi5",oParentObject,m.pOPER)
return(i_retval)

define class tgsof_bi5 as StdBatch
  * --- Local variables
  pOPER = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue eventi da Selezione/Spostamento Riga Master (da GSOF_ERTE)
    ND = this.oParentObject.w_ZoomDett.cCursor
    * --- Evento w_SELEZI Changed
    * --- Seleziona/Deselezione la Righe Dettaglio 
    if USED(this.oParentObject.w_ZoomDett.cCursor)
      do case
        case this.pOPER="SEL"
          * --- Selezionato il Documento Master
          if this.oParentObject.w_SELEZI = "S"
            * --- Seleziona Tutto
            UPDATE (ND) SET XCHK=1
          else
            * --- deseleziona Tutto
            UPDATE (ND) SET XCHK=0
          endif
        case this.pOPER="BQD"
          * --- Seleziona Tutto
          UPDATE (ND) SET XCHK=1
          this.oParentObject.w_SELEZI = "S"
      endcase
    endif
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
