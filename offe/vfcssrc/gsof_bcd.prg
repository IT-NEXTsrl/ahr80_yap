* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bcd                                                        *
*              Calcola prezzo da offerta                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_557]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-21                                                      *
* Last revis.: 2012-10-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bcd",oParentObject,m.pExec)
return(i_retval)

define class tgsof_bcd as StdBatch
  * --- Local variables
  pExec = space(1)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_PADRE = .NULL.
  w_GEST = .NULL.
  w_LENSCF = 0
  w_MESS = space(1)
  w_ARTCLI = space(1)
  w_cKey = space(20)
  w_DESCOD = space(40)
  w_DESSUP = space(20)
  w_PREZUM = space(1)
  w_UMLIPREZZO = space(3)
  w_OBJCTRL = .NULL.
  w_CHECK = .f.
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_QTALIS = 0
  w_CODART = space(20)
  w_DATREG = ctod("  /  /  ")
  w_CATCLI = space(5)
  w_CATART = space(5)
  w_CODLIS = space(5)
  w_CODVAL = space(3)
  w_CODCON = space(15)
  w_CODGRU = space(5)
  w_SCOCON = .f.
  w_OLIPREZ = 0
  w_OCONTRA = space(15)
  w_OSCONT1 = 0
  w_OSCONT2 = 0
  w_OSCONT3 = 0
  w_OSCONT4 = 0
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_OPREZZO = 0
  w_OK = .f.
  w_OK0 = .f.
  w_OK1 = .f.
  w_OK2 = .f.
  w_DT0 = ctod("  /  /  ")
  w_DT1 = ctod("  /  /  ")
  w_APPO = space(12)
  w_QUAN = 0
  w_QTAUM1 = 0
  w_PZ0 = 0
  w_PZ1 = 0
  w_S10 = 0
  w_S20 = 0
  w_S30 = 0
  w_S40 = 0
  w_S11 = 0
  w_S21 = 0
  w_S31 = 0
  w_S41 = 0
  w_ROWNUM = 0
  w_OK3 = .f.
  w_NC0 = space(15)
  w_NC2 = space(15)
  w_CV0 = space(3)
  w_CV2 = space(3)
  w_IC0 = space(1)
  w_IC2 = space(1)
  w_FC0 = space(1)
  w_FC2 = space(1)
  w_NQ0 = space(1)
  w_NQ2 = space(1)
  w_RN0 = 0
  w_RN1 = 0
  w_RN2 = 0
  w_PZ2 = 0
  w_S12 = 0
  w_S22 = 0
  w_S32 = 0
  w_S42 = 0
  w_PP0 = 0
  w_PP1 = 0
  w_PP2 = 0
  w_NC3 = space(15)
  w_NQ3 = space(1)
  w_IC3 = space(1)
  w_FC3 = space(1)
  w_PZ3 = 0
  w_S13 = 0
  w_S23 = 0
  w_S33 = 0
  w_S43 = 0
  w_PP3 = 0
  w_CV3 = space(3)
  w_RN3 = 0
  w_FLARTI = space(1)
  w_QUACON = space(1)
  w_IVACON = space(1)
  w_FLUCOA = space(1)
  w_QTAMOV = 0
  w_PREZZO = 0
  w_QTA1 = 0
  w_APPO1 = 0
  w_CODICE = space(5)
  w_VALUCA = 0
  w_TIPCON = space(1)
  w_CODCLI = space(15)
  w_CODLIN = space(3)
  w_NETTO = 0
  w_CALPRZ = 0
  w_CAOVAL = 0
  w_CODVAA = space(3)
  w_RICSCO = .f.
  w_RICPRE = .f.
  w_RICTOT = .f.
  w_FLFRAZ = space(1)
  w_QTATEST = 0
  w_DTOBS1 = ctod("  /  /  ")
  w_MESS = space(1)
  w_TESTPRZ = 0
  w_ORIGINE = space(10)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CON_TRAM_idx=0
  CON_COSC_idx=0
  CLA_RICA_idx=0
  SALDIART_idx=0
  TRADARTI_idx=0
  KEY_ARTI_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Prezzo da Listino/Contratto o esegue lo Scorporo (da GSOF_MDO)
    * --- Lanciato da:
    * --- M = Cambio Articolo; A = Cambio Quantita'; R = Automatismi (ev. Ricalcola); S = Zoom su Prezzo (Scorporo); U = Unita' di Misura; P = Qta in 1^ U.M.
    * --- Attiva il Post-In relativo al Codice Articolo associato al Codice di Ricerca
    * --- Se Codice Descrittivo non puo' essere da riconfermare
    * --- Modifica della quantit� con Qt�<>0 e non movimentata la prima U.M. non per Servizi a valore
    * --- Oggetto per messaggi incrementali
    this.w_PADRE = this.oParentObject
    if this.pExec="S" AND IsAlt()
      * --- Notifico che la riga � variata
      if this.oParentObject.w_PARAME="S"
        this.oParentObject.w_ODPREZZO = gsag_bmp(.Null.,"V",this.oParentObject.w_ODPREZZO,this.oParentObject.w_ODPREMAX,this.oParentObject.w_ODPREMIN,this.oParentObject.w_ODCODART)
        this.w_PADRE.SetupdateRow()     
      endif
      i_retcode = 'stop'
      return
    endif
    this.w_GEST = this.oParentObject.oParentObject
    this.w_oMess=createobject("Ah_Message")
    if Not(this.pExec="P")
      * --- Se non modifico a mano la Qt� nella Prima U.M.
      *     La ricalcolo poich� non � ancora stata calcolata dall'mCalc
      this.oParentObject.w_ODQTAUM1 = IIF(this.oParentObject.w_ODTIPRIG="F", 1, CALQTA(this.oParentObject.w_ODQTAMOV, this.oParentObject.w_ODUNIMIS, this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_FLFRAZ1, this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
    endif
    if this.pExec="U" and empty(this.oParentObject.w_ODUNIMIS)
      * --- Cambio unit� di misura da cp_zoom, il campo � ancora vuoto e bisogna ancora selezionare l'unit�.
      Ah_ErrorMsg("Unit� di misura non selezionata!",48,"")
      i_retcode = 'stop'
      return
    endif
    if this.pExec$"AU" And this.oParentObject.w_ODQTAMOV<>0 And this.oParentObject.w_ODUNIMIS<>this.oParentObject.w_UNMIS1 And this.oParentObject.w_ODTIPRIG<>"F"
      * --- Lettura dei flag U.M. frazionabile e Forza 2^ U.M.
      * --- Ricalcolo della Qt� nella Prima U.M. poich� al lancio del batch non � ancora stata calcolata
      this.oParentObject.w_ODQTAUM1 = CALQTAADV(this.oParentObject.w_ODQTAMOV, this.oParentObject.w_ODUNIMIS, this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_FLFRAZ1, this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3, , ,This.oparentobject, "ODQTAMOV", .T.)
      this.w_QTAUM1 = this.oParentObject.w_ODQTAUM1
    endif
    this.w_CODCLI = this.w_PADRE.w_CODCLI
    if this.pExec="M" AND NOT EMPTY(this.oParentObject.w_ODCODART)
      if upper(g_APPLICATION) = "ADHOC REVOLUTION"
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CADTOBSO,CALENSCF,CATIPCON"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_ODCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CADTOBSO,CALENSCF,CATIPCON;
            from (i_cTable) where;
                CACODICE = this.oParentObject.w_ODCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DTOBS1 = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
          this.w_LENSCF = NVL(cp_ToDate(_read_.CALENSCF),cp_NullValue(_read_.CALENSCF))
          this.w_ARTCLI = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CADTOBSO"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_ODCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CADTOBSO;
            from (i_cTable) where;
                CACODICE = this.oParentObject.w_ODCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.W_DTOBS1 = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Se � stata modificata la data di obsolescenza di un articolo in un ordine allora non deve essere visualizzato
      if NOT EMPTY(this.W_DTOBS1) AND this.W_DTOBS1<=this.oParentObject.W_OBTEST AND NOT EMPTY(this.oParentObject.w_ODCODICE)
        this.oParentObject.w_ODCODICE = Space(20)
        this.oParentObject.w_ODDESART = Space(40)
        ah_ErrorMsg("Codice articolo obsoleto dal %1","!","", dtoc(this.w_dtobs1) )
        i_retcode = 'stop'
        return
      endif
      if NOT EMPTY(this.oParentObject.w_ODARNOCO) AND this.oParentObject.w_ODTIPRIG="D" AND NOT EMPTY(this.oParentObject.o_ODCODICE)
        ah_ErrorMsg("Impossibile selezionare un codice descrittivo da confermare","stop","")
        this.oParentObject.w_ODCODICE = this.oParentObject.o_ODCODICE
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CADESART,CACODART,CA__TIPO,CADESSUP,CAUNIMIS,CAMOLTIP,CAOPERAT"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_ODCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CADESART,CACODART,CA__TIPO,CADESSUP,CAUNIMIS,CAMOLTIP,CAOPERAT;
            from (i_cTable) where;
                CACODICE = this.oParentObject.w_ODCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_ODDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          this.oParentObject.w_ODCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          this.oParentObject.w_ODTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
          this.oParentObject.w_ODNOTAGG = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
          this.oParentObject.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
          this.oParentObject.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
          this.oParentObject.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        i_retcode = 'stop'
        return
      endif
      if upper(g_APPLICATION) = "ADHOC REVOLUTION"
        if g_FLCESC = "S" And ((this.w_LENSCF<>0 And Right(Alltrim(this.oParentObject.w_ODCODICE), this.w_LENSCF) <> IIF(Empty(Alltrim(this.oParentObject.w_CODESC)),"######",Alltrim(this.oParentObject.w_CODESC))) Or (this.w_LENSCF=0 And Not Empty(this.oParentObject.w_CODESC) And this.w_ARTCLI$"C-F"))
          this.w_oMess.AddMsgPartNL("Codice articolo incongruente con intestatario")     
          if Empty(this.oParentObject.w_CODESC)
            this.w_oPart = this.w_oMess.AddMsgPartNL("Nessun suffisso codice di ricerca legato a %1")
            this.w_oPart.AddParam(Alltrim(this.w_CODCLI))     
          else
            this.w_oPart = this.w_oMess.AddMsgPartNL("Suffisso codice di ricerca legato a %1 = %2")
            this.w_oPart.AddParam(Alltrim(this.w_CODCLI))     
            this.w_oPart.AddParam(this.oParentObject.w_CODESC)     
          endif
          if this.w_LENSCF = 0
            this.w_oMess.AddMsgPartNL("Nessun suffisso assegnato al codice di ricerca di tipo cliente")     
          else
            this.w_oPart = this.w_oMess.AddMsgPartNL("Suffisso assegnato al codice di ricerca = %1")
            this.w_oPart.AddParam(Right(Alltrim(this.oParentObject.w_ODCODICE),this.w_LENSCF))     
          endif
          this.w_oMess.Ah_ErrorMsg()     
          this.oParentObject.w_ODCODICE = Space(20)
          this.oParentObject.w_ODDESART = Space(40)
          i_retcode = 'stop'
          return
        endif
      endif
      this.w_cKey = cp_setAzi(i_TableProp[this.w_PADRE.ART_ICOL_IDX, 2]) + "\" + cp_ToStr(this.oParentObject.w_ODCODART, 1)
      cp_ShowWarn(this.w_cKey, this.w_PADRE.ART_ICOL_IDX)
      if upper(g_APPLICATION) = "ADHOC REVOLUTION"
        if this.oParentObject.w_ODTIPRIG="D"
          * --- Nel caso di modifica codice articolo e inserita riga descrittiva, azzero il prezzo
          *     che altrimenti rimarrebbe quello dell'articolo.
          *     Questo errore si presenta solo se il prezzo precedente era stato scritto a mano e non calcolato 
          *     da Contratto/listino o altro.
          this.oParentObject.w_ODPREZZO = 0
        endif
      endif
      if NOT EMPTY(this.oParentObject.w_ODCODART) AND this.oParentObject.w_ODQTAMOV<>0
        this.w_APPO = SPACE(5)
        this.w_APPO1 = SPACE(5)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARGRUMER,ARCATSCM,ARFLUSEP,ARFLSERG,ARTIPSER,ARCODIVA"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ODCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARGRUMER,ARCATSCM,ARFLUSEP,ARFLSERG,ARTIPSER,ARCODIVA;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_ODCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.oParentObject.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.oParentObject.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.oParentObject.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.oParentObject.w_GRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
          this.oParentObject.w_CATSCA = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
          this.oParentObject.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
          this.oParentObject.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
          this.oParentObject.w_FLSERA = NVL(cp_ToDate(_read_.ARTIPSER),cp_NullValue(_read_.ARTIPSER))
          this.oParentObject.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_ODUNIMIS = IIF(NOT EMPTY(this.oParentObject.w_UNMIS3) AND this.oParentObject.w_MOLTI3<>0, this.oParentObject.w_UNMIS3, this.oParentObject.w_UNMIS1)
      endif
    endif
    if this.pExec= "P" And this.oParentObject.w_FLUSEP = "Q"
      * --- Cambio della quantit� della prima unit� di misura con U.M. Separate su Per Quantit�
      *     Non devo effettuare nessun ricalcolo.
      Ah_ErrorMsg("Ricalcolo non necessario!",48,"")
      i_retcode = 'stop'
      return
    endif
    this.oParentObject.w_ODARNOCO = IIF(this.oParentObject.w_ODTIPRIG="D", " ", this.oParentObject.w_ODARNOCO)
    this.w_TIPCON = "C"
    this.w_CODART = this.oParentObject.w_ODCODART
    this.w_DATREG = this.oParentObject.w_DATDOC
    this.oParentObject.w_OFLSCO = IIF(this.pExec="M", " ", this.oParentObject.w_OFLSCO)
    this.w_CATCLI = this.oParentObject.w_CATSCC
    this.w_CATART = this.oParentObject.w_CATSCA
    this.w_CODGRU = this.oParentObject.w_GRUMER
    this.w_CODLIS = this.w_PADRE.w_CODLIS
    this.w_CODVAL = this.w_PADRE.w_CODVAL
    this.w_CODLIN = this.w_GEST.w_NCODLIN
    this.w_SCOCON = .F.
    this.w_OLIPREZ = this.oParentObject.w_LIPREZZO
    this.w_CODCON = this.oParentObject.w_ODCONTRA
    this.w_OCONTRA = this.oParentObject.w_ODCONTRA
    this.w_OSCONT1 = this.oParentObject.w_ODSCONT1
    this.w_OSCONT2 = this.oParentObject.w_ODSCONT2
    this.w_OSCONT3 = this.oParentObject.w_ODSCONT3
    this.w_OSCONT4 = this.oParentObject.w_ODSCONT4
    this.w_OPREZZO = -9876
    * --- Calcolo il cambio della valuta utilizzata
    this.w_CAOVAL = GETCAM(this.w_CODVAL, this.w_DATREG, 7)
    if (this.pExec="A" OR this.pExec="P" OR this.pExec="U" OR this.pExec="M") AND this.oParentObject.w_ODPREZZO<>0 AND (NOT EMPTY(this.w_CODLIS) OR NOT EMPTY(this.w_CODCON))
      * --- Segna il Prezzo di origine per eventuale non Conferma finale
      this.w_OPREZZO = this.oParentObject.w_ODPREZZO
    endif
    * --- Traduzione in Lingua della Descrizione Articoli 
    if this.pExec="M" AND NOT EMPTY(this.w_CODLIN) AND this.w_CODLIN<>g_CODLIN
      * --- Legge la Traduzione
      DECLARE ARRRIS (2) 
 a=Tradlin(this.oParentObject.w_ODCODICE,this.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
      if Not Empty(this.w_DESCOD)
        this.oParentObject.w_ODDESART = this.w_DESCOD
        this.oParentObject.w_ODNOTAGG = this.w_DESSUP
      endif
    endif
    if this.pExec="S" AND this.oParentObject.w_FLSCOR<>"S" AND this.oParentObject.w_ACTSCOR=.F. And this.oParentObject.w_ODPREZZO<>0
      if ah_YesNo("Vuoi scorporare il prezzo di riga?")
        * --- Scorpora
        this.oParentObject.w_ACTSCOR = .T.
        * --- Eventuale Iva Scorporata
        this.w_NETTO = cp_ROUND(this.oParentObject.w_ODPREZZO / (1 + (this.oParentObject.w_PERIVA / 100)), this.oParentObject.w_DECUNI)
        this.oParentObject.w_IVASCOR = this.oParentObject.w_ODPREZZO - this.w_NETTO
        this.oParentObject.w_ODPREZZO = this.w_NETTO
        * --- Notifica che la Riga e' Stata Variata
        SELECT (this.w_PADRE.cTrsName)
        if EMPTY(i_SRV) AND NOT DELETED()
          REPLACE i_SRV WITH "U"
        endif
      endif
      i_retcode = 'stop'
      return
    endif
    if this.pExec="S" AND this.oParentObject.w_FLSCOR<>"S" AND this.oParentObject.w_ACTSCOR=.T.And Not(ah_YesNo("Vuoi ricalcolare il prezzo di riga?"))
      * --- F9 sul prezzo per la seconda volta (ACTSCOR=.T.) riporto il prezzo nella situazione originaria (lo ricalcolo)
      i_retcode = 'stop'
      return
    endif
    if (this.oParentObject.w_ODQTAMOV=0 AND this.oParentObject.w_ODTIPRIG $ "RMA") OR EMPTY(this.w_CODART)
      * --- Se non Specifico la Quantita' o l'Articolo esce
      i_retcode = 'stop'
      return
    endif
    * --- Le lettura precedente su ART_ICOL nel caso di unit� di misura vuota esegue L'NVL con ' ' come secondo parametro
    *     UNMISx deve essere obbligatoriamente di 3 caratteri par far si che CALMMLIS funzioni correttamente
    this.w_APPO = (IIF(Empty(this.oParentObject.w_UNMIS1),Space(3),this.oParentObject.w_UNMIS1))+(IIF(Empty(this.oParentObject.w_UNMIS2),Space(3),this.oParentObject.w_UNMIS2))+(IIF(Empty(this.oParentObject.w_UNMIS3),Space(3),this.oParentObject.w_UNMIS3))
    * --- Ricalcolo Quantit� nella Prima unit� di misura
    if this.w_QTAUM1=0
      if this.pExec $ "PRS" AND this.oParentObject.w_ODQTAUM1<>0
        * --- Modificata 1^UM
        this.w_QTAUM1 = this.oParentObject.w_ODQTAUM1
      else
        this.w_QTAUM1 = IIF(this.oParentObject.w_ODTIPRIG="F", 1, CALQTA(this.oParentObject.w_ODQTAMOV, this.oParentObject.w_ODUNIMIS, this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_FLFRAZ1, this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
      endif
    endif
    this.w_VALUCA = 0
    * --- Legge U.C.A.
    * --- Read from SALDIART
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SLVALUCA,SLCODVAV"+;
        " from "+i_cTable+" SALDIART where ";
            +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_ODCODART);
            +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SLVALUCA,SLCODVAV;
        from (i_cTable) where;
            SLCODICE = this.oParentObject.w_ODCODART;
            and SLCODMAG = this.oParentObject.w_CODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VALUCA = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
      this.w_CODVAA = NVL(cp_ToDate(_read_.SLCODVAV),cp_NullValue(_read_.SLCODVAV))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if upper(g_APPLICATION) <> "ADHOC REVOLUTION" 
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CAPREZUM"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_ODCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CAPREZUM;
          from (i_cTable) where;
              CACODICE = this.oParentObject.w_ODCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_PREZUM = NVL(cp_ToDate(_read_.CAPREZUM),cp_NullValue(_read_.CAPREZUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      DECLARE ARRCALC (9,1)
      * --- Azzero l'Array che verr� riempito dalla Funzione
      ARRCALC(1)=0
      this.w_CALPRZ = CalPrzli( this.oParentObject.w_ODCONTRA , "C" , this.w_CODLIS , this.w_CODART , SPACE(20) ,this.w_CODGRU , this.w_QTAUM1 , this.oParentObject.w_CODMAG , this.w_CODVAL , this.w_CAOVAL , this.w_DatReg , this.w_CATCLI , this.w_CATART,@ARRCALC, this.oParentObject.w_ODUNIMIS, this.oParentObject.w_PREZUM, this.oParentObject.w_PRZVAC,"","")
      this.w_UMLIPREZZO = ARRCALC(9)
      this.oParentObject.w_LIPREZZO = ARRCALC(5)
      this.w_OK = ARRCALC(7)=2 OR ARRCALC(8)=1
      this.oParentObject.w_LISCON = ARRCALC(7)
      this.oParentObject.w_CLUNIMIS = ""
    else
      if this.oParentObject.w_ODTIPRIG="F"
        this.w_OBJCTRL = this.w_PADRE.GetCtrl( "w_ODCODART" )
        L_Method_Name="this.w_PADRE.Link"+Right( this.w_OBJCTRL.Name , LEN( this.w_OBJCTRL.Name ) - rat("_" , this.w_OBJCTRL.Name , 2)+1)+"('Full',this)"
        this.w_CHECK = &L_Method_Name
        this.w_QTAUM1 = this.oParentObject.w_ODQTAUM1
        this.oParentObject.w_ODQTAMOV = IIF(this.oParentObject.w_ODQTAMOV=0, 1, this.oParentObject.w_ODQTAMOV)
      endif
      DECLARE ARRCALC (16,1)
      * --- Azzero l'Array che verr� riempito dalla Funzione
      ARRCALC(1)=0
      this.w_QTAUM3 = CALQTA(this.oParentObject.w_ODQTAUM1,this.oParentObject.w_UNMIS3, Space(3),IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
      this.w_QTAUM2 = CALQTA(this.oParentObject.w_ODQTAUM1,this.oParentObject.w_UNMIS2, this.oParentObject.w_UNMIS2,IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
      DIMENSION pArrUm[9]
      pArrUm [1] = this.oParentObject.w_PREZUM 
 pArrUm [2] = this.oParentObject.w_ODUNIMIS 
 pArrUm [3] = this.oParentObject.w_ODQTAMOV 
 pArrUm [4] = this.oParentObject.w_UNMIS1 
 pArrUm [5] = this.oParentObject.w_ODQTAUM1 
 pArrUm [6] = this.oParentObject.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.oParentObject.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
      this.w_CALPRZ = CalPrzli( this.oParentObject.w_ODCONTRA , "C" , this.w_CODLIS , this.w_CODART , this.w_CODGRU , this.w_QTAUM1 , this.w_CODVAL , this.w_CAOVAL , this.w_DatReg , this.w_CATCLI , this.w_CATART, this.w_CODVAA, this.w_CODCLI, this.oParentObject.w_CATCOM, this.oParentObject.w_FLSCOR, this.oParentObject.w_SCOLIS, this.w_VALUCA,"O", @ARRCALC, this.oParentObject.w_PRZVAC, "V","N", @pArrUm)
      this.oParentObject.w_CLUNIMIS = ARRCALC(16)
      this.oParentObject.w_LIPREZZO = ARRCALC(5)
      this.w_OK = ARRCALC(7)=2 OR ARRCALC(8)=1
      this.oParentObject.w_LISCON = ARRCALC(7)
      if this.oParentObject.w_ODUNIMIS<>this.oParentObject.w_CLUNIMIS AND NOT EMPTY(this.oParentObject.w_CLUNIMIS)
        this.w_QTALIS = IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS1,this.oParentObject.w_ODQTAUM1, IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
        this.oParentObject.w_LIPREZZO = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, this.oParentObject.w_ODQTAMOV, this.w_QTALIS, this.oParentObject.w_IVALIS,this.oParentObject.w_PERIVA, this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
      endif
    endif
    * --- Aggiorno gli sconti solo se arrivano da Listino/Contratto/Tabella ScontiMagg.
    this.oParentObject.w_ODSCONT1 = ARRCALC(1)
    this.oParentObject.w_ODSCONT2 = ARRCALC(2)
    this.oParentObject.w_ODSCONT3 = ARRCALC(3)
    this.oParentObject.w_ODSCONT4 = ARRCALC(4)
    * --- Impedisco il ricalcolo degli sconti
    this.oParentObject.o_ODSCONT1 = this.oParentObject.w_ODSCONT1
    this.oParentObject.o_ODSCONT2 = this.oParentObject.w_ODSCONT2
    this.oParentObject.o_ODSCONT3 = this.oParentObject.w_ODSCONT3
    if upper(g_APPLICATION) = "ADHOC REVOLUTION" 
      this.oParentObject.w_ODCONTRA = ARRCALC(9)
      this.oParentObject.w_ACTSCOR = ARRCALC(10)
      this.w_IVACON = ARRCALC(12)
    endif
    if this.oParentObject.w_FLSCOR<>"S" AND this.w_IVACON="L" AND this.oParentObject.w_LIPREZZO<>0 AND this.oParentObject.w_PERIVA<>0
      * --- Cli/For senza Scorporo e Contratto al Lordo: Scorpora l'IVA
      this.oParentObject.w_LIPREZZO = cp_ROUND(this.oParentObject.w_LIPREZZO / (1 + (this.oParentObject.w_PERIVA / 100)), this.oParentObject.w_DECUNI)
    endif
    if this.oParentObject.w_ODTIPRIG $ "RFMA" AND g_PERAGE="S" AND this.oParentObject.w_ODTIPPRO$"DC-CC"
      if ARRCALC(6)<>0
        this.oParentObject.w_ODPERPRO = ARRCALC(6)
        this.oParentObject.w_ODTIPPRO = "CC"
        this.w_PADRE.Set("w_ODTIPPRO" , this.oParentObject.w_ODTIPPRO)     
      endif
    endif
    if this.oParentObject.w_ODTIPRIG $ "RFMA" AND g_PERAGE="S" AND this.oParentObject.w_ODTIPPR2$"DC-CC"
      if ARRCALC(15)<>0
        this.oParentObject.w_ODPROCAP = ARRCALC(15)
        this.oParentObject.w_ODTIPPR2 = "CC"
        this.w_PADRE.Set("w_ODTIPPR2" , this.oParentObject.w_ODTIPPR2)     
      endif
    endif
    * --- Controllo se sono stati modificati prezzo o sconti
    this.w_RICSCO = IIF(this.oParentObject.w_NUMSCO>0, this.oParentObject.w_ODSCONT1, 0)<>this.w_OSCONT1 And this.w_OSCONT1<>0
    this.w_RICSCO = this.w_RICSCO OR (IIF(this.oParentObject.w_NUMSCO>1, this.oParentObject.w_ODSCONT2, 0)<>this.w_OSCONT2 And this.w_OSCONT2<>0)
    this.w_RICSCO = this.w_RICSCO OR (IIF(this.oParentObject.w_NUMSCO>2, this.oParentObject.w_ODSCONT3, 0)<>this.w_OSCONT3 And this.w_OSCONT3<>0)
    this.w_RICSCO = this.w_RICSCO OR (IIF(this.oParentObject.w_NUMSCO>3, this.oParentObject.w_ODSCONT4, 0)<>this.w_OSCONT4 And this.w_OSCONT4<>0)
    if upper(g_APPLICATION) <> "ADHOC REVOLUTION" 
      * --- Enterprise
      this.w_TESTPRZ = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_ODUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, "N")+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, 0),this.oParentObject.w_DECUNI)
    else
      if this.pExec="P"
        * --- Ricalcolo prezzo se cambio quantit� nella prima U.M con CALMMPZ
        *     poich� ricalcola in base qlle quantit� effettivamente digitate (per U.M separate)
        this.w_TESTPRZ = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, this.oParentObject.w_ODQTAMOV, this.oParentObject.w_ODQTAUM1, IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, "N"), IIF(this.oParentObject.w_FLSCOR="S", 0, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI), this.oParentObject.w_DECUNI)
      else
        if EMPTY(this.oParentObject.w_CLUNIMIS)
          this.w_TESTPRZ = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_ODUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, "N")+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, 0),this.oParentObject.w_DECUNI)
        else
          if this.oParentObject.w_ODUNIMIS=this.oParentObject.w_CLUNIMIS
            * --- mi serve solo calcolare l'iva pertanto inganno il ricalcolo delle quantit� mettendo i moltiplicatori a 1
            this.w_TESTPRZ = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_ODUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, "N")+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), 1, 1, 0),this.oParentObject.w_DECUNI)
          else
            this.w_TESTPRZ = this.oParentObject.w_LIPREZZO
          endif
        endif
      endif
    endif
    this.w_RICPRE = this.w_TESTPRZ <>this.oParentObject.w_ODPREZZO 
    * --- Al cambio della quantit� se modificato il prezzo e il Listino/Contratto sono gestiti a sconti 
    *     oppure gli sonti derivano dalla tabella Sconti Maggiorazioni chiedo se si vogliono modificare
    if upper(g_APPLICATION) = "ADHOC REVOLUTION" 
      if (this.pExec="A" OR this.pExec="P" OR this.pExec="U" Or this.pExec="M") AND ((this.w_OPREZZO<>-9876 AND this.w_RICPRE) Or this.w_RICSCO)
        * --- Nella domanda di ricalcolo prezzi evidenzio anche il nuovo prezzo e sconti e l'origine di questi
        this.w_oMESS.AddMsgPartNL("Ricalcolo il prezzo e gli sconti in base al listino/contratto/tabella sconti maggiorazioni?")     
        do case
          case Arrcalc(7)=1
            this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 calcolato da contratto")
            this.w_oPART.addParam(Alltrim(Str(this.w_TESTPRZ,18,5)))     
          case Arrcalc(7)=2
            this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 calcolato da listino")
            this.w_oPART.addParam(Alltrim(Str(this.w_TESTPRZ,18,5)))     
          case Arrcalc(7)=3
            this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 calcolato da U.C.A., U.P.V")
            this.w_oPART.addParam(Alltrim(Str(this.w_TESTPRZ,18,5)))     
          otherwise
            this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 nessuna origine")
            this.w_oPART.addParam(Alltrim(Str(this.w_TESTPRZ,18,5)))     
        endcase
        if this.oParentObject.w_NUMSCO>0
          do case
            case Arrcalc(8)=1
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da contratto")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_ODSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT4,6,2)),""))     
            case Arrcalc(8)=3
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da listino")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_ODSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT4,6,2)),""))     
            case Arrcalc(8)=4
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da U.C.A., U.P.V")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_ODSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT4,6,2)),""))     
            case Arrcalc(8)=2
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da tab. sconti/magg.")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_ODSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT4,6,2)),""))     
            otherwise
              this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 nessuna origine")
              this.w_oPART.addParam(Alltrim(Str(this.oParentObject.w_ODSCONT1,6,2))+IIF(this.oParentObject.w_NUMSCO>1,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT2,6,2)),"") + IIF(this.oParentObject.w_NUMSCO>2,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT3,6,2)),"")+IIF(this.oParentObject.w_NUMSCO>3,"   "+Alltrim(Str(this.oParentObject.w_ODSCONT4,6,2)),""))     
          endcase
        endif
        if NOT this.w_oMESS.ah_YesNo()
          * --- Ripristina
          this.oParentObject.w_LIPREZZO = this.w_OLIPREZ
          this.oParentObject.w_ODCONTRA = this.w_OCONTRA
          this.oParentObject.w_ODSCONT1 = this.w_OSCONT1
          this.oParentObject.w_ODSCONT2 = this.w_OSCONT2
          this.oParentObject.w_ODSCONT3 = this.w_OSCONT3
          this.oParentObject.w_ODSCONT4 = this.w_OSCONT4
          * --- Impedisco il ricalcolo degli sconti
          this.oParentObject.o_ODSCONT1 = this.oParentObject.w_ODSCONT1
          this.oParentObject.o_ODSCONT2 = this.oParentObject.w_ODSCONT2
          this.oParentObject.o_ODSCONT3 = this.oParentObject.w_ODSCONT3
          i_retcode = 'stop'
          return
        else
          if (this.pExec="A" OR this.pExec="P" OR this.pExec="U" Or this.pExec="M") AND this.oParentObject.o_LIPREZZO=this.oParentObject.w_LIPREZZO
            * --- Forza il Ricalcolo se variate QTA
            this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO + 1
          endif
        endif
      endif
      * --- Ricerca Sconti (se non trovati nei Contratti)
      if this.w_SCOCON=.F. AND this.oParentObject.w_SCOLIS<>"S"
        if this.w_GEST.cFunction="Edit"
          * --- Se sono in variazione ricalcola solo se nuova riga mai Modificata
          SELECT (this.w_PADRE.cTrsName)
          this.oParentObject.w_OFLSCO = IIF(i_SRV="A" OR ARRCALC(11)="C", ARRCALC(11), "S")
        endif
      endif
    endif
    * --- w_OFLSCO alla fine deve valere solo ' ' o 'S'
    this.oParentObject.w_OFLSCO = IIF(this.oParentObject.w_OFLSCO="C", " ", this.oParentObject.w_OFLSCO)
    this.w_RICTOT = .F.
    if this.pExec="S" AND this.w_OK=.F. AND this.oParentObject.w_FLSCOR<>"S" AND this.oParentObject.w_ACTSCOR=.F. And this.oParentObject.w_ODPREZZO<>0
      this.w_RICTOT = .T.
      * --- Se non presente Listino Ripristina situazione pre Scorporo
      this.oParentObject.w_ODPREZZO = this.oParentObject.w_ODPREZZO+this.oParentObject.w_IVASCOR
      this.oParentObject.w_IVASCOR = 0
      * --- Notifica che la Riga e' Stata Variata
      SELECT (this.w_PADRE.cTrsName)
      if EMPTY(i_SRV) AND NOT DELETED()
        REPLACE i_SRV WITH "U"
      endif
    endif
    if this.pExec $ "APRSUM" AND (this.w_OK Or this.w_RICTOT)
      if Not (this.w_RICTOT)
        * --- Ricalcolo il prezzo solo se non deriva da Reincorporo dell'Iva 
        if this.pExec="P"
          * --- Modificata 1^UM
          *     Ricalcolo il prezzo con CALMMPZ per U.M. separate
          *     e imposto o_LIPREZZO = w_LIPREZZO per evitare il ricalcolo effettuato sui documenti
          this.oParentObject.w_ODPREZZO = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, this.oParentObject.w_ODQTAMOV, this.oParentObject.w_ODQTAUM1, IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, "N"), IIF(this.oParentObject.w_FLSCOR="S", 0, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI), this.oParentObject.w_DECUNI)
          this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
        else
          if EMPTY(this.oParentObject.w_CLUNIMIS)
            this.oParentObject.w_ODPREZZO = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_ODUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, "N")+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, IIF(this.oParentObject.w_FLSCOR="S",0,this.oParentObject.w_PERIVA)),this.oParentObject.w_DECUNI)
          else
            if this.oParentObject.w_ODUNIMIS=this.oParentObject.w_CLUNIMIS
              * --- mi serve solo calcolare l'iva pertanto inganno il ricalcolo delle quantit� mettendo i moltiplicatori a 1
              this.oParentObject.w_ODPREZZO = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO+this.oParentObject.w_ODUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+IIF(this.oParentObject.w_LISCON=2, this.oParentObject.w_IVALIS, "N")+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), 1,1, IIF(this.oParentObject.w_FLSCOR="S",0,this.oParentObject.w_PERIVA)),this.oParentObject.w_DECUNI)
            else
              this.oParentObject.w_ODPREZZO = this.oParentObject.w_LIPREZZO
            endif
          endif
        endif
      endif
      this.oParentObject.w_TOTRIG = this.oParentObject.w_TOTRIG - this.oParentObject.w_ODVALRIG
      this.oParentObject.w_TOTMERCE = this.oParentObject.w_TOTMERCE - this.oParentObject.w_RIGNET
      this.oParentObject.w_TOTRIPD = this.oParentObject.w_TOTRIPD - this.oParentObject.w_RIGNET1
      if upper(g_APPLICATION) <> "ADHOC REVOLUTION"
        this.oParentObject.w_VALUNI = cp_ROUND(this.oParentObject.w_ODPREZZO * (1+this.oParentObject.w_ODSCONT1/100)*(1+this.oParentObject.w_ODSCONT2/100)*(1+this.oParentObject.w_ODSCONT3/100)*(1+this.oParentObject.w_ODSCONT4/100), 5)
      endif
      this.oParentObject.w_ODVALRIG = CAVALRIG(this.oParentObject.w_ODPREZZO,this.oParentObject.w_ODQTAMOV, this.oParentObject.w_ODSCONT1,this.oParentObject.w_ODSCONT2,this.oParentObject.w_ODSCONT3,this.oParentObject.w_ODSCONT4,this.oParentObject.w_DECTOT)
      this.oParentObject.w_RIGNET = IIF(this.oParentObject.w_ODFLOMAG="X" AND this.oParentObject.w_FLSERA<>"S", this.oParentObject.w_ODVALRIG, 0)
      this.oParentObject.w_RIGNET1 = IIF(this.oParentObject.w_ODFLOMAG="X", this.oParentObject.w_ODVALRIG, 0)
      this.oParentObject.w_TOTRIG = this.oParentObject.w_TOTRIG + this.oParentObject.w_ODVALRIG
      this.oParentObject.w_TOTMERCE = this.oParentObject.w_TOTMERCE + this.oParentObject.w_RIGNET
      this.oParentObject.w_TOTRIPD = this.oParentObject.w_TOTRIPD + this.oParentObject.w_RIGNET1
      * --- Notifica che la Riga e' Stata Variata
      SELECT (this.w_PADRE.cTrsName)
      if EMPTY(i_SRV) AND NOT DELETED()
        REPLACE i_SRV WITH "U"
      endif
    endif
    * --- Num. Max. Sconti Consentiti
    this.oParentObject.w_ODSCONT1 = IIF(this.oParentObject.w_NUMSCO>0, this.oParentObject.w_ODSCONT1, 0)
    this.oParentObject.w_ODSCONT2 = IIF(this.oParentObject.w_NUMSCO>1, this.oParentObject.w_ODSCONT2, 0)
    this.oParentObject.w_ODSCONT3 = IIF(this.oParentObject.w_NUMSCO>2, this.oParentObject.w_ODSCONT3, 0)
    this.oParentObject.w_ODSCONT4 = IIF(this.oParentObject.w_NUMSCO>3, this.oParentObject.w_ODSCONT4, 0)
    * --- Impedisco il ricalcolo degli sconti
    this.oParentObject.o_ODSCONT1 = this.oParentObject.w_ODSCONT1
    this.oParentObject.o_ODSCONT2 = this.oParentObject.w_ODSCONT2
    this.oParentObject.o_ODSCONT3 = this.oParentObject.w_ODSCONT3
    if upper(g_APPLICATION) = "ADHOC REVOLUTION"
      if this.pExec="S"
        * --- Valorizzo o_LIPREZZO con w_LIPREZZO per evitare il ricalcolo
        *     del prezzo nel caso in cui l'utente modifica il prezzo a mano
        *     a seguito di un F9 sul prezzo.
        this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
      endif
    else
      if ARRCALC(7)>0 
        * --- Se attivo flag ARPREZUM oppure l'Unit� di Misura del Listino/Contratto 
        *     coincide con Unit� di Misura movimentata non effettuo conversioni
        if this.oParentObject.w_PREZUM="S" or this.w_UMLIPREZZO=this.oParentObject.w_ODUNIMIS
          this.oParentObject.w_ODPREZZO = IIF(this.oParentObject.w_IVALIS="L",CALNET( this.oParentObject.w_LIPREZZO , IIF(this.oParentObject.w_FLSCOR="S", 0, this.oParentObject.w_PERIVA) , this.oParentObject.w_DECUNI , "" , IIF(this.oParentObject.w_FLSCOR="S", 0, this.oParentObject.w_PERIVA)), this.oParentObject.w_LIPREZZO)
        else
          do case
            case this.w_UMLIPREZZO=this.oParentObject.w_UNMIS1 and (this.oParentObject.w_ODUNIMIS=this.oParentObject.w_UNMIS2 or this.oParentObject.w_ODUNIMIS=this.oParentObject.w_UNMIS3)
              * --- U.M. Listini/Contratti = UM1 e U.M. movimentata = UM2 or UM3
              *     (Converto in UM2)
              this.w_APPO = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_ODUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+this.oParentObject.w_IVALIS+"P"
              this.oParentObject.w_ODPREZZO = CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO, this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, IIF(this.oParentObject.w_FLSCOR="S", 0, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI)
            case this.w_UMLIPREZZO=this.oParentObject.w_UNMIS2 and this.oParentObject.w_ODUNIMIS=this.oParentObject.w_UNMIS1
              * --- U.M. Listini/Contratti = UM2 e U.M. movimentata = UM1
              *     (Converto in UM1)
              this.w_APPO = this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS3+this.oParentObject.w_ODUNIMIS+IIF(this.oParentObject.w_OPERAT="/","*","/")+this.oParentObject.w_OPERA3+this.oParentObject.w_IVALIS+"P"
              this.oParentObject.w_ODPREZZO = CALMMLIS(this.oParentObject.w_LIPREZZO, this.w_APPO, this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, IIF(this.oParentObject.w_FLSCOR="S", 0, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI)
            case this.w_UMLIPREZZO=this.oParentObject.w_UNMIS2 and this.oParentObject.w_ODUNIMIS=this.oParentObject.w_UNMIS3
              * --- U.M. Listini/Contratti = UM2 e U.M. movimentata = UM3
              *     Listino/ Contratto  Non valido
              this.oParentObject.w_ODPREZZO = 0
              this.oParentObject.w_LIPREZZO = 0
          endcase
        endif
        * --- Non deve rieseguire calcolo ODPREZZO mcalc
        this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
        if this.oParentObject.o_ODUNIMIS <> this.oParentObject.w_ODUNIMIS
          this.oParentObject.o_ODUNIMIS = this.oParentObject.w_ODUNIMIS
          * --- Ricalcolo ODQTAUM1
          this.oParentObject.w_ODQTAUM1 = this.w_QTAUM1
        endif
        this.oParentObject.w_VALUNI = cp_Round ( this.oParentObject.w_ODPREZZO * (1+this.oParentObject.w_ODSCONT1/100)*(1+this.oParentObject.w_ODSCONT2/100)*(1+this.oParentObject.w_ODSCONT3/100)*(1+this.oParentObject.w_ODSCONT4/100) , 5)
        this.oParentObject.w_TOTRIG = this.oParentObject.w_TOTRIG - this.oParentObject.w_ODVALRIG
        this.oParentObject.w_TOTMERCE = this.oParentObject.w_TOTMERCE - this.oParentObject.w_RIGNET
        this.oParentObject.w_TOTRIPD = this.oParentObject.w_TOTRIPD - this.oParentObject.w_RIGNET1
        this.oParentObject.w_ODVALRIG = cp_ROUND(this.oParentObject.w_ODQTAMOV * this.oParentObject.w_VALUNI, this.oParentObject.w_DECTOT)
        this.oParentObject.w_RIGNET = IIF(this.oParentObject.w_ODFLOMAG="X" AND this.oParentObject.w_FLSERA<>"S", this.oParentObject.w_ODVALRIG, 0)
        this.oParentObject.w_RIGNET1 = IIF(this.oParentObject.w_ODFLOMAG="X", this.oParentObject.w_ODVALRIG, 0)
        this.oParentObject.w_TOTRIG = this.oParentObject.w_TOTRIG + this.oParentObject.w_ODVALRIG
        this.oParentObject.w_TOTMERCE = this.oParentObject.w_TOTMERCE + this.oParentObject.w_RIGNET
        this.oParentObject.w_TOTRIPD = this.oParentObject.w_TOTRIPD + this.oParentObject.w_RIGNET1
        this.w_PADRE.TrsFromWork()
        * --- Notifica che la Riga e' Stata Variata
        SELECT (this.w_PADRE.cTrsName)
        if EMPTY(i_SRV) AND NOT DELETED()
          REPLACE i_SRV WITH "U"
        endif
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazione Variabili Locali e Globali
    if upper(g_APPLICATION) <> "ADHOC REVOLUTION" 
    endif
  endproc


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CON_TRAM'
    this.cWorkTables[3]='CON_COSC'
    this.cWorkTables[4]='CLA_RICA'
    this.cWorkTables[5]='SALDIART'
    this.cWorkTables[6]='TRADARTI'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='UNIMIS'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
