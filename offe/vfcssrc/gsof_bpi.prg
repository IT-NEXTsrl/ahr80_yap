* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bpi                                                        *
*              Caricamento righe preventivo da iter                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-04-18                                                      *
* Last revis.: 2012-05-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bpi",oParentObject)
return(i_retval)

define class tgsof_bpi as StdBatch
  * --- Local variables
  w_READAZI = space(5)
  w_PREACC = space(20)
  w_OBJ = .NULL.
  * --- WorkFile variables
  PAR_ALTE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione righe preventivo da Iter da GSPR_AOF
    this.w_READAZI = i_CODAZI
    * --- Read from PAR_ALTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAPREFOR"+;
        " from "+i_cTable+" PAR_ALTE where ";
            +"PACODAZI = "+cp_ToStrODBC(this.w_READAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAPREFOR;
        from (i_cTable) where;
            PACODAZI = this.w_READAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PREACC = NVL(cp_ToDate(_read_.PAPREFOR),cp_NullValue(_read_.PAPREFOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if (this.oParentObject.w_TIPRAG="A" and Not Empty(this.oParentObject.w_CODITER)) or (this.oParentObject.w_TIPRAG="P" and Not Empty(this.oParentObject.w_CODPRE)) OR (this.oParentObject.w_TIPRAG="F" and this.oParentObject.w_IMPFOR<>0)
      this.w_OBJ = This.oparentobject.GSPR_MDO
      this.w_OBJ.Markpos()     
      if this.oParentObject.w_TIPRAG<>"F"
        * --- Select from GSOF_BPI
        do vq_exec with 'GSOF_BPI',this,'_Curs_GSOF_BPI','',.f.,.t.
        if used('_Curs_GSOF_BPI')
          select _Curs_GSOF_BPI
          locate for 1=1
          do while not(eof())
          this.w_OBJ.AddRow()     
          this.w_OBJ.w_ODCODICE = _Curs_GSOF_BPI.ODCODICE
          this.w_OBJ.Notifyevent("Linkpre")     
          this.w_OBJ.SetControlsValue()     
          this.w_OBJ.w_ODDESART = Nvl(_Curs_GSOF_BPI.ODDESART," ")
          this.w_OBJ.w_ODNOTAGG = Nvl(_Curs_GSOF_BPI.ODNOTAGG," ")
          this.w_OBJ.Notifyevent("w_ODQTAMOV Changed")     
          this.w_OBJ.SaveRow()     
            select _Curs_GSOF_BPI
            continue
          enddo
          use
        endif
      else
        this.w_OBJ.AddRow()     
        this.w_OBJ.w_ODCODICE = this.w_PREACC
        this.w_OBJ.Notifyevent("Linkpre")     
        this.w_OBJ.SetControlsValue()     
        this.w_OBJ.w_ODPREZZO = this.oParentObject.w_IMPFOR
        this.w_OBJ.mcalc(.t.)     
        this.w_OBJ.SaveRow()     
      endif
      this.w_OBJ.w_TOTRIPD = this.w_OBJ.w_TOTRIG
      this.w_OBJ.Repos()     
      this.oParentObject.w_TOTRIP = this.w_OBJ.w_TOTRIG
      this.oParentObject.w_OFSPETRA = this.w_OBJ.w_TOTANT
      this.oParentObject.w_OFSPEIMB = this.w_OBJ.w_TOTSPE
      this.oParentObject.w_OFIMPOFF = this.w_OBJ.w_TOTRIG
      this.w_OBJ = Null
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_ALTE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSOF_BPI')
      use in _Curs_GSOF_BPI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
