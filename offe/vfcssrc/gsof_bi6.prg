* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bi6                                                        *
*              Importa offerta selezionata                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-24                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bi6",oParentObject,m.pParam)
return(i_retval)

define class tgsof_bi6 as StdBatch
  * --- Local variables
  pParam = space(1)
  w_CNT = 0
  w_APPO = space(10)
  w_RICALP = .f.
  w_CODLIN = space(3)
  w_PADRE = .NULL.
  w_RICTOT = .f.
  * --- WorkFile variables
  TRADARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa I'Offerta o il Kit Selezionato (da GSOF_MDO)
    *     pParam : O -> Importa Offerte 
    *                      K -> Importa Kit
    this.w_PADRE = this.oParentObject
    this.w_CODLIN = this.w_PADRE.oParentObject.w_NCODLIN
    if USED("impOff")
      * --- Se importo Offerte chiedo se voglio importare i prezzi
      if Isalt()
        this.w_RICALP = this.pParam="O" And ah_YesNo("Importo anche i prezzi presenti sul preventivo?")
      else
        this.w_RICALP = this.pParam="O" And ah_YesNo("Importo anche i prezzi presenti sull'offerta?")
      endif
      this.w_PADRE.MarkPos()     
      SELECT ImpOff
      GO TOP
      this.w_CNT = 0
      SCAN FOR CPROWNUM<>0 AND NOT EMPTY(NVL(ODCODICE, ""))
      SELECT (this.w_PADRE.cTrsName)
      this.w_PADRE.AddRow()     
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      SELECT ImpOff
      this.oParentObject.w_ODCODICE = ODCODICE
      this.oParentObject.w_ODDESART = NVL(ODDESART, SPACE(5))
      this.oParentObject.w_ODNOTAGG = NVL(ODNOTAGG, "")
      * --- Traduzione in Lingua della Descrizione Articoli 
      if NOT EMPTY(this.w_CODLIN) AND this.w_CODLIN<>g_CODLIN
        * --- Legge la Traduzione
        * --- Select from TRADARTI
        i_nConn=i_TableProp[this.TRADARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2],.t.,this.TRADARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TRADARTI ";
              +" where LGCODICE="+cp_ToStrODBC(this.oParentObject.w_ODCODICE)+" AND LGCODLIN="+cp_ToStrODBC(this.w_CODLIN)+"";
               ,"_Curs_TRADARTI")
        else
          select * from (i_cTable);
           where LGCODICE=this.oParentObject.w_ODCODICE AND LGCODLIN=this.w_CODLIN;
            into cursor _Curs_TRADARTI
        endif
        if used('_Curs_TRADARTI')
          select _Curs_TRADARTI
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_ODDESART = NVL(_Curs_TRADARTI.LGDESCOD, SPACE(5))
          this.oParentObject.w_ODNOTAGG = NVL(_Curs_TRADARTI.LGDESSUP, "")
            select _Curs_TRADARTI
            continue
          enddo
          use
        endif
        SELECT ImpOff
      endif
      this.oParentObject.w_ODCODGRU = NVL(ODCODGRU, SPACE(5))
      this.oParentObject.w_ODCODSOT = NVL(ODCODSOT, SPACE(5))
      this.oParentObject.w_ODUNIMIS = NVL(ODUNIMIS, SPACE(3))
      this.oParentObject.w_ODQTAMOV = NVL(ODQTAMOV, 0)
      this.oParentObject.w_ODPREZZO = NVL(ODPREZZO, 0)
      this.oParentObject.w_ODFLOMAG = NVL(ODFLOMAG, " ")
      this.oParentObject.w_ODSCONT1 = NVL(ODSCONT1, 0)
      this.oParentObject.w_ODSCONT2 = NVL(ODSCONT2, 0)
      this.oParentObject.w_ODSCONT3 = NVL(ODSCONT3, 0)
      this.oParentObject.w_ODSCONT4 = NVL(ODSCONT4, 0)
      this.oParentObject.w_ODTIPRIG = NVL(ODTIPRIG, " ")
      this.oParentObject.w_ODCODART = NVL(ODCODART, SPACE(20))
      this.oParentObject.w_CODIVA = NVL(ARCODIVA, SPACE(5))
      this.oParentObject.w_GRUMER = NVL(ARGRUMER, SPACE(5))
      this.oParentObject.w_CATSCA = NVL(ARCATSCM, SPACE(5))
      this.oParentObject.w_PERIVA = NVL(IVPERIVA, 0)
      this.oParentObject.w_LIPREZZO = 0
      this.oParentObject.w_ODARNOCO = IIF(this.oParentObject.w_ODTIPRIG="D", " ", NVL(ODARNOCO, " "))
      this.oParentObject.w_ARTNOC = this.oParentObject.w_ODARNOCO
      this.oParentObject.w_OFLSCO = " "
      this.oParentObject.w_UNMIS1 = NVL(UNMIS1, SPACE(3))
      this.oParentObject.w_UNMIS2 = NVL(UNMIS2, SPACE(3))
      this.oParentObject.w_UNMIS3 = NVL(UNMIS3, SPACE(3))
      this.oParentObject.w_MOLTIP = NVL(MOLTIP, 0)
      this.oParentObject.w_MOLTI3 = NVL(MOLTI3, 0)
      this.oParentObject.w_OPERAT = NVL(OPERAT, " ")
      this.oParentObject.w_OPERA3 = NVL(OPERA3, " ")
      this.oParentObject.w_FLUSEP = NVL(FLUSEP, " ")
      this.oParentObject.w_FLSERG = NVL(FLSERG, " ")
      this.oParentObject.w_FLSERA = NVL(FLSERA, " ")
      this.oParentObject.w_FLFRAZ = NVL(FLFRAZ, " ")
      this.oParentObject.w_ODCONTRA = SPACE(15)
      * --- Unita' di Misura
      this.oParentObject.w_UNMIS1 = LEFT(this.oParentObject.w_UNMIS1+SPACE(3), 3)
      this.oParentObject.w_UNMIS2 = LEFT(this.oParentObject.w_UNMIS2+SPACE(3), 3)
      this.oParentObject.w_UNMIS3 = LEFT(this.oParentObject.w_UNMIS3+SPACE(3), 3)
      this.oParentObject.w_UMCAL = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+LEFT(this.oParentObject.w_ODUNIMIS+"   ", 3)+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3
      this.oParentObject.w_ODQTAUM1 = IIF(this.oParentObject.w_ODTIPRIG="F", 1, CALQTA(this.oParentObject.w_ODQTAMOV, LEFT(this.oParentObject.w_ODUNIMIS+"   ", 3),this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, "", "", "",,this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
      * --- Ricalcola Il Prezzo se :
      *     Importo un'offerta e rispondo di No alla domanda se voglio importare i prezzi 
      *     Importo un Kit e il prezzo importato � 0
      this.w_RICTOT = .T.
      if (this.w_RICALP=.F. And this.pParam="O") Or (this.oParentObject.w_ODPREZZO=0 And this.pParam="K")
        this.w_RICTOT = .F.
        * --- Per non eseguire i calcoli nel Batch GSOF_BCD
        this.oParentObject.w_ODPREZZO = 0
        this.w_APPO = this.oParentObject.o_ODCODICE
        this.oParentObject.o_ODCODICE = this.oParentObject.w_ODCODICE
        this.w_PADRE.NotifyEvent("Ricalcola")     
        this.oParentObject.o_ODCODICE = this.w_APPO
      endif
      if UPPER(g_APPLICATION)<>"ADHOC REVOLUTION"
        this.oParentObject.w_VALUNI = cp_ROUND(this.oParentObject.w_ODPREZZO * (1+this.oParentObject.w_ODSCONT1/100)*(1+this.oParentObject.w_ODSCONT2/100)*(1+this.oParentObject.w_ODSCONT3/100)*(1+this.oParentObject.w_ODSCONT4/100),5)
        this.oParentObject.w_ODVALRIG = cp_ROUND(this.oParentObject.w_ODQTAMOV * this.oParentObject.w_VALUNI, this.oParentObject.w_DECTOT)
      else
        this.oParentObject.w_ODVALRIG = CAVALRIG(this.oParentObject.w_ODPREZZO,this.oParentObject.w_ODQTAMOV, this.oParentObject.w_ODSCONT1,this.oParentObject.w_ODSCONT2,this.oParentObject.w_ODSCONT3,this.oParentObject.w_ODSCONT4,this.oParentObject.w_DECTOT)
      endif
      this.oParentObject.w_RIGNET = IIF(this.oParentObject.w_ODFLOMAG="X" AND this.oParentObject.w_FLSERA<>"S", this.oParentObject.w_ODVALRIG, 0)
      this.oParentObject.w_RIGNET1 = IIF(this.oParentObject.w_ODFLOMAG="X", this.oParentObject.w_ODVALRIG, 0)
      if this.w_RICTOT
        * --- Se ho eseguito ricalcola non devo ricalcolare i totali perche sono gi� calcolati
        *     per esecuzione mCalc
        this.oParentObject.w_TOTRIG = this.oParentObject.w_TOTRIG + this.oParentObject.w_ODVALRIG
        this.oParentObject.w_TOTMERCE = this.oParentObject.w_TOTMERCE + this.oParentObject.w_RIGNET
        this.oParentObject.w_TOTRIPD = this.oParentObject.w_TOTRIPD + this.oParentObject.w_RIGNET1
      endif
      * --- Ricalcola Il Prezzo
      * --- Carica il Temporaneo
      this.w_CNT = this.w_CNT + 1
      ah_Msg("Inserimento riga: %1",.T.,.F.,.F., ALLTRIM(STR(this.w_CNT)) )
      * --- Carica il Temporaneo dei Dati
      SELECT ( this.w_PADRE.cTrsName )
      this.w_PADRE.TrsFromWork()     
      SELECT ImpOff
      ENDSCAN
      * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
      this.w_PADRE.RePos(.F.)     
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TRADARTI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_TRADARTI')
      use in _Curs_TRADARTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
