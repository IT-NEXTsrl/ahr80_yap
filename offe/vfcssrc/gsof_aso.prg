* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_aso                                                        *
*              Sezioni offerta                                                 *
*                                                                              *
*      Author: Maurizio Rossetti                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_aso"))

* --- Class definition
define class tgsof_aso as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 598
  Height = 347+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-18"
  HelpContextID=158717591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  SEZ_OFFE_IDX = 0
  TIT_SEZI_IDX = 0
  SOT_SEZI_IDX = 0
  cFile = "SEZ_OFFE"
  cKeySelect = "SOCODICE"
  cKeyWhere  = "SOCODICE=this.w_SOCODICE"
  cKeyWhereODBC = '"SOCODICE="+cp_ToStrODBC(this.w_SOCODICE)';

  cKeyWhereODBCqualified = '"SEZ_OFFE.SOCODICE="+cp_ToStrODBC(this.w_SOCODICE)';

  cPrg = "gsof_aso"
  cComment = "Sezioni offerta"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SOCODICE = space(10)
  w_SODESCRI = space(35)
  w_SOTIPSEZ = space(1)
  w_SOFILGRU = space(5)
  w_SOFILSOT = space(5)
  w_SOSEGNWP = space(254)
  w_SOPRESEG = space(30)
  w_SOMODEWP = space(254)
  w_SO__NOTE = space(0)
  w_SOSTRSEZ = space(3)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SEZ_OFFE','gsof_aso')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_asoPag1","gsof_aso",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Sezione offerta")
      .Pages(1).HelpContextID = 94196170
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSOCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsof_aso
    * --- Imposta il Titolo della Finestra
    WITH THIS.PARENT
       If Isalt()
       .cComment = CP_TRANSLATE('Sezioni preventivo')
       Endif
    ENDWIT
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIT_SEZI'
    this.cWorkTables[2]='SOT_SEZI'
    this.cWorkTables[3]='SEZ_OFFE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SEZ_OFFE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SEZ_OFFE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SOCODICE = NVL(SOCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SEZ_OFFE where SOCODICE=KeySet.SOCODICE
    *
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SEZ_OFFE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SEZ_OFFE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SEZ_OFFE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SOCODICE',this.w_SOCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SOCODICE = NVL(SOCODICE,space(10))
        .w_SODESCRI = NVL(SODESCRI,space(35))
        .w_SOTIPSEZ = NVL(SOTIPSEZ,space(1))
        .w_SOFILGRU = NVL(SOFILGRU,space(5))
          * evitabile
          *.link_1_6('Load')
        .w_SOFILSOT = NVL(SOFILSOT,space(5))
          * evitabile
          *.link_1_8('Load')
        .w_SOSEGNWP = NVL(SOSEGNWP,space(254))
        .w_SOPRESEG = NVL(SOPRESEG,space(30))
        .w_SOMODEWP = NVL(SOMODEWP,space(254))
        .w_SO__NOTE = NVL(SO__NOTE,space(0))
        .w_SOSTRSEZ = NVL(SOSTRSEZ,space(3))
        cp_LoadRecExtFlds(this,'SEZ_OFFE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SOCODICE = space(10)
      .w_SODESCRI = space(35)
      .w_SOTIPSEZ = space(1)
      .w_SOFILGRU = space(5)
      .w_SOFILSOT = space(5)
      .w_SOSEGNWP = space(254)
      .w_SOPRESEG = space(30)
      .w_SOMODEWP = space(254)
      .w_SO__NOTE = space(0)
      .w_SOSTRSEZ = space(3)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_SOTIPSEZ = 'D'
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_SOFILGRU))
          .link_1_6('Full')
          endif
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_SOFILSOT))
          .link_1_8('Full')
          endif
          .DoRTCalc(6,9,.f.)
        .w_SOSTRSEZ = 'TAB'
      endif
    endwith
    cp_BlankRecExtFlds(this,'SEZ_OFFE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSOCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oSODESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oSOTIPSEZ_1_4.enabled = i_bVal
      .Page1.oPag.oSOFILGRU_1_6.enabled = i_bVal
      .Page1.oPag.oSOFILSOT_1_8.enabled = i_bVal
      .Page1.oPag.oSOSEGNWP_1_10.enabled = i_bVal
      .Page1.oPag.oSOPRESEG_1_12.enabled = i_bVal
      .Page1.oPag.oSOMODEWP_1_14.enabled = i_bVal
      .Page1.oPag.oSO__NOTE_1_16.enabled = i_bVal
      .Page1.oPag.oSOSTRSEZ_1_24.enabled = i_bVal
      .Page1.oPag.oBtn_1_22.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSOCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSOCODICE_1_1.enabled = .t.
        .Page1.oPag.oSODESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SEZ_OFFE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOCODICE,"SOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SODESCRI,"SODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOTIPSEZ,"SOTIPSEZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOFILGRU,"SOFILGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOFILSOT,"SOFILSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOSEGNWP,"SOSEGNWP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOPRESEG,"SOPRESEG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOMODEWP,"SOMODEWP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SO__NOTE,"SO__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOSTRSEZ,"SOSTRSEZ",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
    i_lTable = "SEZ_OFFE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SEZ_OFFE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsof_sso with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SEZ_OFFE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SEZ_OFFE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SEZ_OFFE')
        i_extval=cp_InsertValODBCExtFlds(this,'SEZ_OFFE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT"+;
                  ",SOSEGNWP,SOPRESEG,SOMODEWP,SO__NOTE,SOSTRSEZ "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SOCODICE)+;
                  ","+cp_ToStrODBC(this.w_SODESCRI)+;
                  ","+cp_ToStrODBC(this.w_SOTIPSEZ)+;
                  ","+cp_ToStrODBCNull(this.w_SOFILGRU)+;
                  ","+cp_ToStrODBCNull(this.w_SOFILSOT)+;
                  ","+cp_ToStrODBC(this.w_SOSEGNWP)+;
                  ","+cp_ToStrODBC(this.w_SOPRESEG)+;
                  ","+cp_ToStrODBC(this.w_SOMODEWP)+;
                  ","+cp_ToStrODBC(this.w_SO__NOTE)+;
                  ","+cp_ToStrODBC(this.w_SOSTRSEZ)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SEZ_OFFE')
        i_extval=cp_InsertValVFPExtFlds(this,'SEZ_OFFE')
        cp_CheckDeletedKey(i_cTable,0,'SOCODICE',this.w_SOCODICE)
        INSERT INTO (i_cTable);
              (SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT,SOSEGNWP,SOPRESEG,SOMODEWP,SO__NOTE,SOSTRSEZ  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SOCODICE;
                  ,this.w_SODESCRI;
                  ,this.w_SOTIPSEZ;
                  ,this.w_SOFILGRU;
                  ,this.w_SOFILSOT;
                  ,this.w_SOSEGNWP;
                  ,this.w_SOPRESEG;
                  ,this.w_SOMODEWP;
                  ,this.w_SO__NOTE;
                  ,this.w_SOSTRSEZ;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SEZ_OFFE_IDX,i_nConn)
      *
      * update SEZ_OFFE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SEZ_OFFE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SODESCRI="+cp_ToStrODBC(this.w_SODESCRI)+;
             ",SOTIPSEZ="+cp_ToStrODBC(this.w_SOTIPSEZ)+;
             ",SOFILGRU="+cp_ToStrODBCNull(this.w_SOFILGRU)+;
             ",SOFILSOT="+cp_ToStrODBCNull(this.w_SOFILSOT)+;
             ",SOSEGNWP="+cp_ToStrODBC(this.w_SOSEGNWP)+;
             ",SOPRESEG="+cp_ToStrODBC(this.w_SOPRESEG)+;
             ",SOMODEWP="+cp_ToStrODBC(this.w_SOMODEWP)+;
             ",SO__NOTE="+cp_ToStrODBC(this.w_SO__NOTE)+;
             ",SOSTRSEZ="+cp_ToStrODBC(this.w_SOSTRSEZ)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SEZ_OFFE')
        i_cWhere = cp_PKFox(i_cTable  ,'SOCODICE',this.w_SOCODICE  )
        UPDATE (i_cTable) SET;
              SODESCRI=this.w_SODESCRI;
             ,SOTIPSEZ=this.w_SOTIPSEZ;
             ,SOFILGRU=this.w_SOFILGRU;
             ,SOFILSOT=this.w_SOFILSOT;
             ,SOSEGNWP=this.w_SOSEGNWP;
             ,SOPRESEG=this.w_SOPRESEG;
             ,SOMODEWP=this.w_SOMODEWP;
             ,SO__NOTE=this.w_SO__NOTE;
             ,SOSTRSEZ=this.w_SOSTRSEZ;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SEZ_OFFE_IDX,i_nConn)
      *
      * delete SEZ_OFFE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SOCODICE',this.w_SOCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSOFILGRU_1_6.enabled = this.oPgFrm.Page1.oPag.oSOFILGRU_1_6.mCond()
    this.oPgFrm.Page1.oPag.oSOFILSOT_1_8.enabled = this.oPgFrm.Page1.oPag.oSOFILSOT_1_8.mCond()
    this.oPgFrm.Page1.oPag.oSOSEGNWP_1_10.enabled = this.oPgFrm.Page1.oPag.oSOSEGNWP_1_10.mCond()
    this.oPgFrm.Page1.oPag.oSOPRESEG_1_12.enabled = this.oPgFrm.Page1.oPag.oSOPRESEG_1_12.mCond()
    this.oPgFrm.Page1.oPag.oSOMODEWP_1_14.enabled = this.oPgFrm.Page1.oPag.oSOMODEWP_1_14.mCond()
    this.oPgFrm.Page1.oPag.oSO__NOTE_1_16.enabled = this.oPgFrm.Page1.oPag.oSO__NOTE_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oSOFILSOT_1_8.visible=!this.oPgFrm.Page1.oPag.oSOFILSOT_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oSOSEGNWP_1_10.visible=!this.oPgFrm.Page1.oPag.oSOSEGNWP_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oSOPRESEG_1_12.visible=!this.oPgFrm.Page1.oPag.oSOPRESEG_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oSOMODEWP_1_14.visible=!this.oPgFrm.Page1.oPag.oSOMODEWP_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oSO__NOTE_1_16.visible=!this.oPgFrm.Page1.oPag.oSO__NOTE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oSOSTRSEZ_1_24.visible=!this.oPgFrm.Page1.oPag.oSOSTRSEZ_1_24.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SOFILGRU
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIT_SEZI_IDX,3]
    i_lTable = "TIT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2], .t., this.TIT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOFILGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATS',True,'TIT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TSCODICE like "+cp_ToStrODBC(trim(this.w_SOFILGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select TSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TSCODICE',trim(this.w_SOFILGRU))
          select TSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOFILGRU)==trim(_Link_.TSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOFILGRU) and !this.bDontReportError
            deferred_cp_zoom('TIT_SEZI','*','TSCODICE',cp_AbsName(oSource.parent,'oSOFILGRU_1_6'),i_cWhere,'GSOF_ATS',"Gruppi articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',oSource.xKey(1))
            select TSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOFILGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(this.w_SOFILGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',this.w_SOFILGRU)
            select TSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOFILGRU = NVL(_Link_.TSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SOFILGRU = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.TSCODICE,1)
      cp_ShowWarn(i_cKey,this.TIT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOFILGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOFILSOT
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_lTable = "SOT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2], .t., this.SOT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOFILSOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ASF',True,'SOT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SGCODSOT like "+cp_ToStrODBC(trim(this.w_SOFILSOT)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_SOFILGRU);

          i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SGCODGRU,SGCODSOT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SGCODGRU',this.w_SOFILGRU;
                     ,'SGCODSOT',trim(this.w_SOFILSOT))
          select SGCODGRU,SGCODSOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SGCODGRU,SGCODSOT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOFILSOT)==trim(_Link_.SGCODSOT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOFILSOT) and !this.bDontReportError
            deferred_cp_zoom('SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(oSource.parent,'oSOFILSOT_1_8'),i_cWhere,'GSOF_ASF',"Sottogruppi articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SOFILGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SGCODGRU,SGCODSOT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                     +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SGCODGRU="+cp_ToStrODBC(this.w_SOFILGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',oSource.xKey(1);
                       ,'SGCODSOT',oSource.xKey(2))
            select SGCODGRU,SGCODSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOFILSOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                   +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(this.w_SOFILSOT);
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_SOFILGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',this.w_SOFILGRU;
                       ,'SGCODSOT',this.w_SOFILSOT)
            select SGCODGRU,SGCODSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOFILSOT = NVL(_Link_.SGCODSOT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SOFILSOT = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.SGCODGRU,1)+'\'+cp_ToStr(_Link_.SGCODSOT,1)
      cp_ShowWarn(i_cKey,this.SOT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOFILSOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSOCODICE_1_1.value==this.w_SOCODICE)
      this.oPgFrm.Page1.oPag.oSOCODICE_1_1.value=this.w_SOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSODESCRI_1_3.value==this.w_SODESCRI)
      this.oPgFrm.Page1.oPag.oSODESCRI_1_3.value=this.w_SODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSOTIPSEZ_1_4.RadioValue()==this.w_SOTIPSEZ)
      this.oPgFrm.Page1.oPag.oSOTIPSEZ_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSOFILGRU_1_6.value==this.w_SOFILGRU)
      this.oPgFrm.Page1.oPag.oSOFILGRU_1_6.value=this.w_SOFILGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oSOFILSOT_1_8.value==this.w_SOFILSOT)
      this.oPgFrm.Page1.oPag.oSOFILSOT_1_8.value=this.w_SOFILSOT
    endif
    if not(this.oPgFrm.Page1.oPag.oSOSEGNWP_1_10.value==this.w_SOSEGNWP)
      this.oPgFrm.Page1.oPag.oSOSEGNWP_1_10.value=this.w_SOSEGNWP
    endif
    if not(this.oPgFrm.Page1.oPag.oSOPRESEG_1_12.value==this.w_SOPRESEG)
      this.oPgFrm.Page1.oPag.oSOPRESEG_1_12.value=this.w_SOPRESEG
    endif
    if not(this.oPgFrm.Page1.oPag.oSOMODEWP_1_14.value==this.w_SOMODEWP)
      this.oPgFrm.Page1.oPag.oSOMODEWP_1_14.value=this.w_SOMODEWP
    endif
    if not(this.oPgFrm.Page1.oPag.oSO__NOTE_1_16.value==this.w_SO__NOTE)
      this.oPgFrm.Page1.oPag.oSO__NOTE_1_16.value=this.w_SO__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oSOSTRSEZ_1_24.RadioValue()==this.w_SOSTRSEZ)
      this.oPgFrm.Page1.oPag.oSOSTRSEZ_1_24.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'SEZ_OFFE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SOCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSOCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_SOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_SOMODEWP) OR FILE(FULLPATH(ALLTRIM(.w_SOMODEWP))))  and not(!(.w_SOTIPSEZ='W' OR .w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S'))  and (.w_SOTIPSEZ='W' OR .w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSOMODEWP_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il percorso per il modello word processor non � valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_aso
      * --- Controllo campi obbligatori
      if i_bRes=.T.
         do case
         case .w_SOTIPSEZ = 'D'
            if EMPTY(.w_SOSEGNWP)
             i_bRes=.F.
             i_bnoChk=.F.
             i_cErrorMsg=Ah_MsgFormat("Campo segnalibro WP obbligatorio")
            endif
         case .w_SOTIPSEZ = 'W' or .w_SOSTRSEZ = 'MOD'
            if EMPTY(.w_SOMODEWP)
             i_bRes=.F.
             i_bnoChk=.F.
             i_cErrorMsg=Ah_MsgFormat("Campo modello WP obbligatorio")
            endif
          endcase
      endif
      if i_bRes
        i_bRes =CHKMEMO(.w_SO__NOTE)
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsof_asoPag1 as StdContainer
  Width  = 594
  height = 347
  stdWidth  = 594
  stdheight = 347
  resizeXpos=372
  resizeYpos=283
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSOCODICE_1_1 as StdField with uid="OKACXMPSIV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SOCODICE", cQueryName = "SOCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice sezione",;
    HelpContextID = 150381717,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=84, Left=89, Top=16, InputMask=replicate('X',10)

  add object oSODESCRI_1_3 as StdField with uid="EFALNKQVCD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SODESCRI", cQueryName = "SODESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 235967633,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=178, Top=16, InputMask=replicate('X',35)


  add object oSOTIPSEZ_1_4 as StdCombo with uid="NGTUSFJDRV",rtseq=3,rtrep=.f.,left=496,top=16,width=92,height=21;
    , ToolTipText = "Tipo";
    , HelpContextID = 29649792;
    , cFormVar="w_SOTIPSEZ",RowSource=""+"Descrittiva,"+"File WP,"+"Articoli,"+"Gruppi,"+"Sottogruppi,"+"Dati fissi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSOTIPSEZ_1_4.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'W',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'S',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oSOTIPSEZ_1_4.GetRadio()
    this.Parent.oContained.w_SOTIPSEZ = this.RadioValue()
    return .t.
  endfunc

  func oSOTIPSEZ_1_4.SetRadio()
    this.Parent.oContained.w_SOTIPSEZ=trim(this.Parent.oContained.w_SOTIPSEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SOTIPSEZ=='D',1,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='W',2,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='A',3,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='G',4,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='S',5,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='T',6,;
      0))))))
  endfunc

  add object oSOFILGRU_1_6 as StdField with uid="JKEDTKFVUJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SOFILGRU", cQueryName = "SOFILGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Filtro gruppo",;
    HelpContextID = 175928453,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=89, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIT_SEZI", cZoomOnZoom="GSOF_ATS", oKey_1_1="TSCODICE", oKey_1_2="this.w_SOFILGRU"

  func oSOFILGRU_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S')
    endwith
   endif
  endfunc

  func oSOFILGRU_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
      if .not. empty(.w_SOFILSOT)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSOFILGRU_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOFILGRU_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIT_SEZI','*','TSCODICE',cp_AbsName(this.parent,'oSOFILGRU_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATS',"Gruppi articoli/servizi",'',this.parent.oContained
  endproc
  proc oSOFILGRU_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TSCODICE=this.parent.oContained.w_SOFILGRU
     i_obj.ecpSave()
  endproc

  add object oSOFILSOT_1_8 as StdField with uid="EDCIDKEPWY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SOFILSOT", cQueryName = "SOFILSOT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Filtro sottogruppo",;
    HelpContextID = 243037318,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=308, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SOT_SEZI", cZoomOnZoom="GSOF_ASF", oKey_1_1="SGCODGRU", oKey_1_2="this.w_SOFILGRU", oKey_2_1="SGCODSOT", oKey_2_2="this.w_SOFILSOT"

  func oSOFILSOT_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SOTIPSEZ='A' OR .w_SOTIPSEZ='S')
    endwith
   endif
  endfunc

  func oSOFILSOT_1_8.mHide()
    with this.Parent.oContained
      return (!(.w_SOTIPSEZ='A' OR .w_SOTIPSEZ='S'))
    endwith
  endfunc

  func oSOFILSOT_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOFILSOT_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOFILSOT_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SOT_SEZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStrODBC(this.Parent.oContained.w_SOFILGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStr(this.Parent.oContained.w_SOFILGRU)
    endif
    do cp_zoom with 'SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(this.parent,'oSOFILSOT_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ASF',"Sottogruppi articoli/servizi",'',this.parent.oContained
  endproc
  proc oSOFILSOT_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ASF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SGCODGRU=w_SOFILGRU
     i_obj.w_SGCODSOT=this.parent.oContained.w_SOFILSOT
     i_obj.ecpSave()
  endproc

  add object oSOSEGNWP_1_10 as StdField with uid="WSROFGTIDU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SOSEGNWP", cQueryName = "SOSEGNWP",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Segnalibro Word Processor",;
    HelpContextID = 204495734,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=178, Top=128, InputMask=replicate('X',254)

  func oSOSEGNWP_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SOTIPSEZ='D' OR .w_SOTIPSEZ='W' OR .w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S')
    endwith
   endif
  endfunc

  func oSOSEGNWP_1_10.mHide()
    with this.Parent.oContained
      return (.w_SOTIPSEZ='T')
    endwith
  endfunc

  add object oSOPRESEG_1_12 as StdField with uid="XCYHUYVEXT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SOPRESEG", cQueryName = "SOPRESEG",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso segnalibro Word Processor",;
    HelpContextID = 18688877,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=178, Top=156, InputMask=replicate('X',30)

  func oSOPRESEG_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S' OR .w_SOTIPSEZ='T')
    endwith
   endif
  endfunc

  func oSOPRESEG_1_12.mHide()
    with this.Parent.oContained
      return (!(.w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S' OR .w_SOTIPSEZ='T'))
    endwith
  endfunc

  add object oSOMODEWP_1_14 as StdField with uid="KTFKZXAHUR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SOMODEWP", cQueryName = "SOMODEWP",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il percorso per il modello word processor non � valido",;
    ToolTipText = "Modello Word Processor",;
    HelpContextID = 50985846,;
   bGlobalFont=.t.,;
    Height=21, Width=336, Left=178, Top=220, InputMask=replicate('X',254)

  func oSOMODEWP_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SOTIPSEZ='W' OR .w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S')
    endwith
   endif
  endfunc

  func oSOMODEWP_1_14.mHide()
    with this.Parent.oContained
      return (!(.w_SOTIPSEZ='W' OR .w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S'))
    endwith
  endfunc

  func oSOMODEWP_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_SOMODEWP) OR FILE(FULLPATH(ALLTRIM(.w_SOMODEWP))))
    endwith
    return bRes
  endfunc

  add object oSO__NOTE_1_16 as StdMemo with uid="GFVYQUDZIC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SO__NOTE", cQueryName = "SO__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 38069397,;
   bGlobalFont=.t.,;
    Height=86, Width=484, Left=104, Top=252

  func oSO__NOTE_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SOTIPSEZ='D')
    endwith
   endif
  endfunc

  func oSO__NOTE_1_16.mHide()
    with this.Parent.oContained
      return (.w_SOTIPSEZ<>'D')
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="GWKJEGKECT",left=518, top=220, width=24,height=22,;
    caption="...", nPag=1;
    , HelpContextID = 158918614;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .w_SOMODEWP=left(getfile('Doc,Dot,Sxw,Stw','File modello ')+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_SOTIPSEZ='W' OR .w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S')
      endwith
    endif
  endfunc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!(.w_SOTIPSEZ='W' OR .w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S'))
     endwith
    endif
  endfunc


  add object oSOSTRSEZ_1_24 as StdCombo with uid="SQLRNKKBGW",rtseq=10,rtrep=.f.,left=459,top=51,width=128,height=21;
    , ToolTipText = "Tipo documento associato";
    , HelpContextID = 32463744;
    , cFormVar="w_SOSTRSEZ",RowSource=""+"Tabella,"+"Modello", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSOSTRSEZ_1_24.RadioValue()
    return(iif(this.value =1,'TAB',;
    iif(this.value =2,'MOD',;
    space(3))))
  endfunc
  func oSOSTRSEZ_1_24.GetRadio()
    this.Parent.oContained.w_SOSTRSEZ = this.RadioValue()
    return .t.
  endfunc

  func oSOSTRSEZ_1_24.SetRadio()
    this.Parent.oContained.w_SOSTRSEZ=trim(this.Parent.oContained.w_SOSTRSEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SOSTRSEZ=='TAB',1,;
      iif(this.Parent.oContained.w_SOSTRSEZ=='MOD',2,;
      0))
  endfunc

  func oSOSTRSEZ_1_24.mHide()
    with this.Parent.oContained
      return (.w_SOTIPSEZ='D' or .w_SOTIPSEZ='W' or .w_SOTIPSEZ='T')
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="ERJKRYLXWT",Visible=.t., Left=27, Top=16,;
    Alignment=1, Width=60, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="UOJQQGSMPD",Visible=.t., Left=455, Top=16,;
    Alignment=1, Width=38, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="PARTMTIGMN",Visible=.t., Left=3, Top=51,;
    Alignment=1, Width=84, Height=18,;
    Caption="Filtro gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (!(.w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S'))
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="HHUYRPVHQH",Visible=.t., Left=158, Top=51,;
    Alignment=1, Width=148, Height=18,;
    Caption="Filtro sottogruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (!(.w_SOTIPSEZ='A' OR .w_SOTIPSEZ='S'))
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="PWNGQLQGQE",Visible=.t., Left=20, Top=132,;
    Alignment=1, Width=156, Height=18,;
    Caption="Segnalibro WP:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_SOTIPSEZ='T')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="YFHOBPOKST",Visible=.t., Left=20, Top=156,;
    Alignment=1, Width=156, Height=18,;
    Caption="Prefisso segnalibro:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (!(.w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S' OR .w_SOTIPSEZ='T'))
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="EGYXOJWHNR",Visible=.t., Left=37, Top=220,;
    Alignment=1, Width=139, Height=18,;
    Caption="Modello WP:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (!(.w_SOTIPSEZ='W' OR .w_SOTIPSEZ='A' OR .w_SOTIPSEZ='G' OR .w_SOTIPSEZ='S'))
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="BUFEAISCTA",Visible=.t., Left=26, Top=252,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_SOTIPSEZ<>'D')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="ZBCZETAOVP",Visible=.t., Left=10, Top=100,;
    Alignment=0, Width=330, Height=18,;
    Caption="Riferimenti documento W.P. di destinazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="SGLGVBQTZB",Visible=.t., Left=10, Top=193,;
    Alignment=0, Width=181, Height=18,;
    Caption="Elementi di origine"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="EFKTICOKYF",Visible=.t., Left=376, Top=51,;
    Alignment=1, Width=80, Height=18,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_SOTIPSEZ='D' or .w_SOTIPSEZ='W' or .w_SOTIPSEZ='T')
    endwith
  endfunc

  add object oBox_1_19 as StdBox with uid="KQCVEYSBZV",left=8, top=118, width=576,height=2

  add object oBox_1_21 as StdBox with uid="CYJHZDGVYW",left=8, top=211, width=576,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_aso','SEZ_OFFE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SOCODICE=SEZ_OFFE.SOCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
