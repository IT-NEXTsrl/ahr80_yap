* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bvp                                                        *
*              Apre offerta versione precedente                                *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-10                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bvp",oParentObject)
return(i_retval)

define class tgsof_bvp as StdBatch
  * --- Local variables
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apre l'eventuale versione precedente di offerta (da GSOF_AOF)
    if EMPTY(this.oparentobject.w_OFSERPRE)
      ah_errormsg("Non esistono versioni precedenti")
      i_retcode = 'stop'
      return
    else
      this.w_PROG = IIF(Isalt(),GSPR_AOF(),GSOF_AOF())
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_PROG.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_PROG.ecpFilter()     
      this.w_PROG.w_OFSERIAL = this.oparentobject.w_OFSERPRE
      this.w_PROG.ecpSave()     
      this.w_PROG.ecpQuery()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
