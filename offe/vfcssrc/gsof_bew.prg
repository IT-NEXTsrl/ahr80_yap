* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bew                                                        *
*              Esportazione documenti su Word                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_1317]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2017-05-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bew",oParentObject)
return(i_retval)

define class tgsof_bew as StdBatch
  * --- Local variables
  w_ArcNom = space(1)
  w_RagSoc = space(40)
  w_PROGRESS = 0
  w_PATHALL = space(254)
  w_messerr = .f.
  w_OFSERIAL = space(10)
  oWord = .NULL.
  oShape = .NULL.
  w_CONTINUA = .f.
  w_DOCOPEN = .f.
  w_EXISTFOLD = .f.
  w_PATHMODELLO = space(254)
  w_RISCRIVI = .f.
  w_NUMCOLON = 0
  w_oPosizione = .NULL.
  w_CURSORE = space(10)
  w_COLONNA = 0
  w_SEGNDOC = space(254)
  w_DATO = space(254)
  w_SEGNALIBRO = space(254)
  w_PRIMO = .f.
  w_NUMRIG = 0
  w_NUMPROG = 0
  w_NUMCAMPI = 0
  w_DATA = space(10)
  w_NOMEDOC = space(254)
  w_NOMEPDF = space(254)
  w_TIPOSEZ = space(1)
  w_SEGNA = space(254)
  w_ENDPOS = 0
  w_GRUPPO = space(5)
  w_SOTTOG = space(5)
  w_SEZIONE = space(10)
  w_MODELLO = space(254)
  w_PREFISSO = space(30)
  w_MESS = space(100)
  w_PRINTERDEFA = space(254)
  w_ARTLETTO = .f.
  w_GRUPLETTO = .f.
  w_SGRUPLETTO = .f.
  w_FORMULA = space(254)
  w_PATHMOD = space(254)
  w_PATHARC = space(254)
  w_TIPODOC = space(1)
  w_APPOVAR = space(40)
  w_VALVAR = space(40)
  w_APPOFOR = space(254)
  w_FINDPOS = 0
  w_NOTROVA = .f.
  w_SEGNALIBRO_EXISTS = .f.
  w_SUBSTRING = space(1)
  w_ASCITEST = 0
  w_TABLENAME = space(20)
  w_CELLNAME = space(5)
  w_LENGTH = 0
  w_STRING_N = space(10)
  w_STRING_I = 0
  oManager = .NULL.
  oDesktop = .NULL.
  oWriter = .NULL.
  oText = .NULL.
  oCurs = .NULL.
  oBookmark = .NULL.
  oAnchor = .NULL.
  oTable = .NULL.
  w_Immagine = .f.
  w_Immagine1 = .f.
  w_CODCLI = space(15)
  w_STRUTSEZ = space(3)
  oGraphic = .NULL.
  w_ESTENSIONE = space(3)
  w_OOTOTCOL = 0
  w_OOTOTROW = 0
  w_OOATTCOL = 0
  w_OOATTROW = 0
  w_OOTOTCOL = 0
  w_OOTOTROW = 0
  w_OOATTCOL = 0
  w_OOATTROW = 0
  oGraphic = .NULL.
  * --- WorkFile variables
  MOD_OFFE_idx=0
  OFF_ERTE_idx=0
  PAR_OFFE_idx=0
  OFF_NOMI_idx=0
  NOM_CONT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Programma lanciato alla conferma delle offerte (GSOF_AOF)
    * --- Controllo se sto salvando una opportunit�
    if !Isalt()
      if this.oParentObject.GSOF_MDO.NumRow()<1
        * --- Se non ho righe di dettaglio non lancio la stampa e la generazione
        i_retcode = 'stop'
        return
      endif
    else
      if this.oParentObject.GSPR_MDO.NumRow()<1
        * --- Se non ho righe di dettaglio non lancio la stampa e la generazione
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Variabili per cartella nominativo
    * --- Variabile per definire la sezione da escludere
    * --- Percorso completo documento
    * --- Controllo se errore Word
    DIMENSION w_CAMPI(100,3), Args(2), noArgs(1), w_CELLNAMES(100)
    * --- Indica la struttura della sezione per le sezioni di tipo Articolo, gruppi, sottogruppi.
    *     Introduce la possibilit� di inserire i campi di dettaglio in forma tabellare (TAB)
    *     o di inserire un intero documento e quindi un modello, per ogni riga (MOD).
    * --- Oggetto grafico utilizzato per inserire una immagine in OpenOffice
    * --- Codice cliente azienda: utilizzata in GSOF_BOF
    this.w_CODCLI = this.oParentObject.w_CODCLI
    this.w_OFSERIAL = this.oParentObject.w_OFSERIAL
    this.w_PROGRESS = -1
    * --- *** COSTANTI WORD PROCESSOR ***
    * --- MS Word: Segnalibro che identifica l'intero documento
    this.w_SEGNDOC = "\EndOfDoc"
    * --- MS Word: wdEndOfRangeColumnNumber = 17, numero colonna della tabella
    * --- Pari a -1 se siamo fuori di una tabella
    this.w_NUMCOLON = 17
    * --- Per determinare la colonna in una texttable di Ooo
    this.w_ASCITEST = 64
    this.w_DOCOPEN = .F.
    if this.oParentObject.w_OFSTATUS $ "SRV"
      * --- Se l'Offerta e' Sospesa o Rifiutata non deve generare niente
      this.bUpdateParentObject=.f.
      i_retcode = 'stop'
      return
    endif
    this.w_FORMULA = " "
    this.w_PATHMODELLO = " "
    this.w_PATHMOD = " "
    this.w_PATHARC = " "
    this.w_TIPODOC = "N"
    this.w_CURSORE = " "
    NUMERR=0
    if NOT EMPTY(this.oParentObject.w_OFCODMOD)
      * --- Read from MOD_OFFE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MOPATMOD,MOPATARC,MONOMDOC,MOTIPFOR"+;
          " from "+i_cTable+" MOD_OFFE where ";
              +"MOCODICE = "+cp_ToStrODBC(this.oParentObject.w_OFCODMOD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MOPATMOD,MOPATARC,MONOMDOC,MOTIPFOR;
          from (i_cTable) where;
              MOCODICE = this.oParentObject.w_OFCODMOD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PATHMOD = NVL(cp_ToDate(_read_.MOPATMOD),cp_NullValue(_read_.MOPATMOD))
        this.w_PATHARC = NVL(cp_ToDate(_read_.MOPATARC),cp_NullValue(_read_.MOPATARC))
        this.w_FORMULA = NVL(cp_ToDate(_read_.MONOMDOC),cp_NullValue(_read_.MONOMDOC))
        this.w_TIPODOC = NVL(cp_ToDate(_read_.MOTIPFOR),cp_NullValue(_read_.MOTIPFOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Mette path assoluto
      this.w_PATHMOD = IIF(NOT EMPTY(this.w_PATHMOD),FULLPATH(this.w_PATHMOD),this.w_PATHMOD)
      this.w_PATHARC = AddBs(IIF(NOT EMPTY(this.w_PATHARC),FULLPATH(this.w_PATHARC),this.w_PATHARC))
    endif
    if not empty(this.w_PATHMOD)
      this.w_ESTENSIONE = JUSTEXT(this.w_PATHMOD)
      do case
        case this.oParentObject.w_TIPOWP="W"
          if Empty(this.w_ESTENSIONE)
            if FILE(substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4)+".dot")
              this.w_PATHMOD = substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4)+".dot"
            else
              if FILE(substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4)+".doc")
                this.w_PATHMOD = substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4)+".doc"
              else
                ah_ErrorMsg("Il file %1 non esiste",,"", substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4) )
                this.oParentObject.w_TIPOWP = "N"
              endif
            endif
          else
            if !lower(this.w_ESTENSIONE) $ "dot-doc-docx-dotx"
              * --- Tipologia documento non gestita
              ah_ErrorMsg("Tipo documento non gestito!%0Impossibile proseguire.",16,"")
              this.oParentObject.w_TIPOWP = "N"
            endif
          endif
        case this.oParentObject.w_TIPOWP="O"
          if Empty(this.w_ESTENSIONE)
            if FILE(substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4)+".stw")
              this.w_PATHMOD = substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4)+".stw"
            else
              if FILE(substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4)+".sxw")
                this.w_PATHMOD = substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4)+".sxw"
              else
                if FILE(alltrim(this.w_PATHMOD))
                  if !ah_YesNo("Il file %1 non esiste%0Si vuole procedere con l'apertura dello stesso documento in versione word?","", substr(this.w_PATHMOD,1,len(alltrim(this.w_PATHMOD))-4))
                    i_retcode = 'stop'
                    return
                  endif
                endif
              endif
            endif
          else
            if !lower(this.w_ESTENSIONE) $ "stw-sxw-odt-ott"
              * --- Tipologia documento non gestita
              ah_ErrorMsg("Tipo documento non gestito!%0Impossibile proseguire.",16,"")
              this.oParentObject.w_TIPOWP = "N"
            endif
          endif
      endcase
    endif
    * --- Disabilita il Decoartor altrimento word genera errori quando l utente muove il mouse sul form
    DecoratorState ("D")
    * --- Se non � stato definito nessun Processor e il percorso del modello non � valido
    *     non genera documento WP o PDF
    if this.oParentObject.w_TIPOWP<>"N" AND DIRECTORY(this.w_PATHARC)
      * --- Se non esiste il modello o non esiste il percorso su cui salvare il documento non esegue nulla
      this.w_DATA = " "
      this.w_ARTLETTO = .F.
      this.w_GRUPLETTO = .F.
      this.w_SGRUPLETTO = .F.
      this.w_RISCRIVI = .t.
      * --- Creazione tabella SEZIONI
      vq_exec("..\OFFE\EXE\QUERY\GSOFSWOR.VQR",this,"SEZIONI")
      if reccount("SEZIONI")=0 
        AH_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare")
        DecoratorState ("A")
        i_retcode = 'stop'
        return
      endif
      * --- Definizione della data nella forma aaaa/mm/gg
      this.w_DATA = ALLTRIM(STR(YEAR(this.oParentObject.w_OFDATDOC)))+"-"+ALLTRIM(STR(MONTH(this.oParentObject.w_OFDATDOC)))+"-"+ALLTRIM(STR(DAY(this.oParentObject.w_OFDATDOC)))
      * --- Viene istanziato l'oggetto Word e aperto il documento di default
      * --- Gestioni errori Word
      OLDERR = ON("ERROR")
      ON ERROR NUMERR=ERROR()
      if this.oParentObject.w_TIPOWP="W"
        this.oWord = CREATEOBJECT("Word.Application")
      else
        * --- Inizializzo il ServiceManager, il controller desktop e l'array di PropertyValues che utilizzo nell'apertura del Documento...
        this.oManager = createObject("com.sun.star.ServiceManager")
        this.oDesktop = this.oManager.createInstance("com.sun.star.frame.Desktop")
        comarray(this.oDesktop,10)
        && Creo la Struttura di PropertyValues e setto la propriet� per il metodo LoadComponentFromURL 
 Args[1] = this.oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue") 
 Args[2] = this.oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue") 
 Args[1].name="Hidden" 
 Args[1].Value=.T. 
 
 && Preparo la struttura Array di PropertyValues per il metodo InsertDocumentFromURL... 
 noArgs[1] = this.oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
      endif
      * --- Gestione errore
      if NUMERR<>0
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.oParentObject.w_TIPOWP="W"
        AH_Msg("Generazione documento Word/PDF")
      else
        AH_Msg("Generazione documento OpenOffice Writer/PDF")
      endif
      * --- Se il modello WP esiste prende quello definito nel modello offerta
      *     altrimenti lo cerca nella prima sezione WP con percorso valido del modello.
      if NOT EMPTY(this.w_PATHMOD)
        if this.oParentObject.w_TIPOWP="W"
          this.oWord.Documents.Add(FULLPATH(ALLTRIM(this.w_PATHMOD)))     
        else
          * --- Apro il documento
          this.oWriter = this.oDesktop.LoadComponentFromUrl( "file:///"+StrTran(ALLTRIM(FULLPATH(this.w_PATHMOD)),CHR(92),CHR(47)),"_blank", 0, @Args )
          this.oText = this.oWriter.getText()
          this.oCurs = this.oText.createTextCursorByRange(this.oText.getEnd())
        endif
        if NUMERR<>0
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Gestione errore
        this.w_PATHMODELLO = ALLTRIM(this.w_PATHMOD)
      else
        SELECT SEZIONI
        GO TOP
        SCAN FOR NOT EMPTY(NVL(MODELLO," ")) AND TIPOSEZIONE="W"
        if FILE(FULLPATH(ALLTRIM(MODELLO)))
          if this.oParentObject.w_TIPOWP="W"
            if FILE(substr(MODELLO,1,len(alltrim(MODELLO))-4)+".dot")
              this.w_MODELLO = substr(MODELLO,1,len(alltrim(MODELLO))-4)+".dot"
            else
              if FILE(substr(MODELLO,1,len(alltrim(MODELLO))-4)+".doc")
                this.w_MODELLO = substr(MODELLO,1,len(alltrim(MODELLO))-4)+".doc"
              else
                AH_ErrorMsg("Il file %1 non esiste","","",substr(MODELLO,1,len(alltrim(MODELLO))-4))
              endif
            endif
            this.oWord.Documents.Add(FULLPATH(ALLTRIM(this.w_MODELLO)))     
          else
            if FILE(substr(MODELLO,1,len(alltrim(MODELLO))-4)+".stw")
              this.w_MODELLO = substr(MODELLO,1,len(alltrim(MODELLO))-4)+".stw"
            else
              if FILE(substr(MODELLO,1,len(alltrim(MODELLO))-4)+".sxw")
                this.w_MODELLO = substr(MODELLO,1,len(alltrim(MODELLO))-4)+".sxw"
              else
                if !ah_YesNo("Il file %1 non esiste%0Si vuole procedere con l'apertura dello stesso documento in versione word?", "", substr(MODELLO,1,len(alltrim(MODELLO))-4) )
                  i_retcode = 'stop'
                  return
                endif
              endif
            endif
            this.oWriter = this.oDesktop.LoadComponentFromUrl( "file:///"+StrTran(ALLTRIM(FULLPATH(this.w_MODELLO)),CHR(92),CHR(47)),"_blank", 0, @Args )
            this.oText = this.oWriter.getText()
          endif
          * --- Gestione errore
          if NUMERR<>0
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_PROGRESS = PROGRESS
          this.w_PATHMODELLO = IIF(NOT EMPTY(this.w_MODELLO),FULLPATH(ALLTRIM(this.w_MODELLO)),this.w_MODELLO)
          EXIT
        else
          this.w_SEZIONE = ALLTRIM(CODICE)
          this.w_NOTROVA = .T.
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        ENDSCAN
      endif
      * --- Se non viene definita una formula per il nome documento viene utilizzata quella standard
      * --- Formula inserita manualmente : per le stringhe si usano i doppi apici (") e per separare le variabili o stringhe la virgola (,)
      if NOT EMPTY(this.w_FORMULA)
        this.w_NOMEDOC = " "
        this.w_APPOFOR = ALLTRIM(this.w_FORMULA)
        do while NOT EMPTY(this.w_APPOFOR)
          this.w_FINDPOS = AT(",",this.w_APPOFOR,1)
          * --- Controllo se esiste il carattere ',' all'interno della formula (va messo anche in fondo)
          this.w_CONTINUA = .T.
          if this.w_FINDPOS<>0
            this.w_APPOVAR = SUBSTR(this.w_APPOFOR,1,AT(",",this.w_APPOFOR,1)-1)
            if SUBSTR(this.w_APPOVAR,1,1)='"' AND SUBSTR(this.w_APPOVAR,len(this.w_APPOVAR),1)='"'
              VALORE= SUBSTR(this.w_APPOVAR,2,len(this.w_APPOVAR)-2)
            else
              variabile="this.oParentObject.w_"+ALLTRIM(this.w_APPOVAR)
              * --- Valore della variabile estrapolata dalla formula
              if TYPE(variabile)="U"
                this.w_CONTINUA = .F.
              else
                VALORE=&variabile
              endif
            endif
            this.w_APPOFOR = ALLTRIM(SUBSTR(this.w_APPOFOR,AT(",",this.w_APPOFOR,1)+1,100))
          else
            this.w_APPOVAR = ALLTRIM(this.w_APPOFOR)
            if SUBSTR(this.w_APPOVAR,1,1)='"' AND SUBSTR(this.w_APPOVAR,len(this.w_APPOVAR),1)='"'
              VALORE= SUBSTR(this.w_APPOVAR,2,len(this.w_APPOVAR)-2)
            else
              variabile="this.oParentObject.w_"+ALLTRIM(this.w_APPOVAR)
              * --- Valore della variabile estrapolata dalla formula
              if TYPE(variabile)="U"
                this.w_CONTINUA = .F.
              else
                VALORE=&variabile
              endif
            endif
            this.w_APPOFOR = " "
          endif
          * --- Controllo del tipo del valore 
          if this.w_CONTINUA
            if EMPTY(this.w_NOMEDOC)
              this.w_NOMEDOC = IIF(TYPE("VALORE")="C",ALLTRIM(VALORE),IIF(TYPE("VALORE")="N",ALLTRIM(STR(VALORE)),IIF(TYPE("VALORE")="D",ALLTRIM(DTOS(CP_TODATE(VALORE))),IIF(TYPE("VALORE")="T",ALLTRIM(DTOS(CP_TODATE(VALORE)))," "))))
            else
              this.w_NOMEDOC = this.w_NOMEDOC+IIF(TYPE("VALORE")="C",ALLTRIM(VALORE),IIF(TYPE("VALORE")="N",ALLTRIM(STR(VALORE)),IIF(TYPE("VALORE")="D",ALLTRIM(DTOS(CP_TODATE(VALORE))),IIF(TYPE("VALORE")="T",ALLTRIM(DTOS(CP_TODATE(VALORE)))," "))))
            endif
          endif
        enddo
        this.w_NOMEDOC = ALLTRIM(this.w_NOMEDOC)+"_"+ALLTRIM(STR(this.oParentObject.w_OFNUMVER))
        if EMPTY(this.w_NOMEDOC)
          this.w_NOMEDOC = ALLTRIM(this.w_DATA) +ALLTRIM(this.oParentObject.w_OFCODNOM) + ALLTRIM(this.oParentObject.w_OFCODMOD) + ALLTRIM(STR(this.oParentObject.w_OFNUMDOC,15)) + ALLTRIM(this.oParentObject.w_OFSERDOC) + ALLTRIM(STR(this.oParentObject.w_OFNUMVER))+ ".DOC"
        else
          this.w_NOMEDOC = this.w_NOMEDOC+".DOC"
        endif
      else
        * --- Formula standard
        this.w_NOMEDOC = ALLTRIM(this.w_DATA) +IIF(EMPTY(this.w_DATA),"","_")+ALLTRIM(this.oParentObject.w_OFCODNOM) + IIF(EMPTY(this.oParentObject.w_OFCODNOM),"","_") + ALLTRIM(this.oParentObject.w_OFCODMOD) +IIF(EMPTY(this.oParentObject.w_OFCODMOD),"","_")+ ALLTRIM(STR(this.oParentObject.w_OFNUMDOC,15)) +IIF(EMPTY(this.oParentObject.w_OFNUMDOC),"","_")+ ALLTRIM(this.oParentObject.w_OFSERDOC)+IIF(EMPTY(this.oParentObject.w_OFSERDOC),"","_")+ ALLTRIM(STR(this.oParentObject.w_OFNUMVER))+ ".DOC"
      endif
      this.w_NOMEPDF = WEVERTXT(SUBSTR(this.w_NOMEDOC,1,LEN(this.w_NOMEDOC)-4),.F.,"A")
      do case
        case this.w_TIPODOC="W"
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if File(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC))
            if this.oParentObject.w_TIPOWP="W"
              this.w_MESS = "Il documento Word Processor esiste gi�%0Si desidera rigenerarlo?"
            else
              this.w_MESS = "Il documento SWriter di OpenOffice esiste gi�%0Si desidera rigenerarlo?"
            endif
            if !ah_YesNo(this.w_MESS)
              this.w_RISCRIVI = .f.
            endif
          endif
        case this.w_TIPODOC="P"
          * --- File PDF
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Calcolo del periodo
          if File(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)+".PDF")
            * --- Se esiste il file
            if !AH_YesNo("Il documento PDF esiste gi�%0Confermi ugualmente?")
              this.w_RISCRIVI = .f.
            endif
          endif
        case this.w_TIPODOC="E"
          * --- Documento Word/PDF
          * --- Calcolo del periodo
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if File(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC)) OR File(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)+".PDF")
            * --- Se esiste il file
            if this.oParentObject.w_TIPOWP="W"
              this.w_MESS = "Il documento Word Processor/PDF esiste gi�%0Confermi ugualmente?"
            else
              this.w_MESS = "Il documento SWriter/PDF esiste gi�%0Confermi ugualmente?"
            endif
            if !ah_YesNo(this.w_MESS)
              this.w_RISCRIVI = .f.
            endif
          endif
      endcase
      PATH=this.w_PATHALL
      * --- Se si decide di non rigenerarlo si ferma
      if !this.w_RISCRIVI
        if this.oParentObject.w_TIPOWP="W"
          this.oWord.Quit()     
        else
          this.oWriter.close(.T.)     
        endif
        * --- Gestione errore
        if NUMERR<>0
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        i_retcode = 'stop'
        return
      endif
      * --- Controlla esistenza del folder
      this.w_EXISTFOLD = DIRECTORY(ALLTRIM(this.w_PATHARC))
      if this.w_EXISTFOLD AND NOT EMPTY(this.w_PATHARC) AND NOT EMPTY(this.w_PATHMODELLO) AND FILE(ALLTRIM(this.w_PATHMODELLO))
        if this.oParentObject.w_TIPOWP="W"
          this.oWord.visible = .F.
        endif
        if NUMERR<>0
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Scansione Sezioni
        SELECT SEZIONI
        GO TOP
        SCAN 
        this.w_GRUPPO = NVL(ALLTRIM(SEZIONI.FILTROGRUPPO)," ")
        this.w_SOTTOG = NVL(ALLTRIM(SEZIONI.FILTROSUB)," ")
        this.w_TIPOSEZ = ALLTRIM(SEZIONI.TIPOSEZIONE)
        this.w_SEZIONE = ALLTRIM(SEZIONI.CODICE)
        this.w_PREFISSO = ALLTRIM(SEZIONI.PREFISSO)
        this.w_MODELLO = IIF(EMPTY(ALLTRIM(SEZIONI.MODELLO)),ALLTRIM(SEZIONI.MODELLO),FULLPATH(ALLTRIM(SEZIONI.MODELLO)))
        this.w_STRUTSEZ = NVL(ALLTRIM(SEZIONI.STRUTTURA)," ")
        if NOT FILE(FULLPATH(this.w_MODELLO)) AND NOT EMPTY(MODELLO)
          this.w_NOTROVA = .T.
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_SEGNA = ALLTRIM(SEZIONI.SEGNALIBRO)
        if this.w_TIPOSEZ$"W-A-G-S" AND NOT EMPTY(this.w_MODELLO)
          this.w_MODELLO = FULLPATH(this.w_MODELLO)
          if this.oParentObject.w_TIPOWP="W"
            if FILE(substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4)+".dot")
              this.w_MODELLO = substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4)+".dot"
            else
              if FILE(substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4)+".doc")
                this.w_MODELLO = substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4)+".doc"
              else
                ah_ErrorMsg("Il file %1 non esiste",,"", substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4) )
              endif
            endif
          else
            if FILE(substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4)+".stw")
              this.w_MODELLO = substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4)+".stw"
            else
              if FILE(substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4)+".sxw")
                this.w_MODELLO = substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4)+".sxw"
              else
                if !ah_YesNo( "Il file %1 non esiste%0Si vuole procedere con l'apertura dello stesso documento in versione word?","", substr(this.w_MODELLO,1,len(alltrim(this.w_MODELLO))-4) )
                  DecoratorState ("A")
                  i_retcode = 'stop'
                  return
                endif
              endif
            endif
          endif
        endif
        do case
          case this.w_TIPOSEZ="D"
            * --- Dati descrittivi
            if this.oParentObject.w_TIPOWP="W"
              if !this.oWord.ActiveDocument.Bookmarks.Exists(this.w_SEGNA)
                * --- Se non esiste lo inserisce in fondo al documento
                this.oWord.ActiveDocument.Bookmarks(this.w_SEGNDOC).Range.InsertAfter(ALLTRIM(NVL(SEZIONI.NOTE,"")))     
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              else
                * --- Inserisce il campo note dopo la posizione del segnalibro
                this.oWord.ActiveDocument.Bookmarks(this.w_SEGNA).Range.InsertAfter(ALLTRIM(NVL(SEZIONI.NOTE,"")))     
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
              * --- Gestione errore
              if NUMERR<>0
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              if TYPE("this.oWriter.getBookmarks().getByName(this.w_SEGNA)")<>"O"
                this.oText = this.oWriter.getText()
                this.oCurs = this.oText.createTextCursorByRange(this.oText.getEnd())
                comarray(this.oCurs,10)
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              else
                this.oBookmark = this.oWriter.Bookmarks.getByName(this.w_SEGNA)
                this.oCurs = this.oBookmark.Anchor.Text.createTextCursorByRange(this.oBookmark.Anchor)
                comarray(this.oCurs,10)
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
              this.oCurs.setString(STRTRAN(ALLTRIM(NVL(SEZIONI.NOTE,"")),CHR(13),""))     
              this.oCurs.collapseToEnd()     
              * --- Gestione errore
              if NUMERR<>0
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          case this.w_TIPOSEZ="G"
            if NOT(this.w_GRUPLETTO)
              * --- Creazione tabella GRUPPI
              vq_exec("..\OFFE\EXE\QUERY\GSOF_GRUPPI.VQR",this,"GRUPPI")
              this.w_GRUPLETTO = .T.
            endif
            * --- Sezione Gruppo
            if reccount("GRUPPI")<>0 
              * --- Definizione del cursore Gruppi in base alla sezione e al filtro su gruppo
              if NOT EMPTY(this.w_GRUPPO)
                SELECT * FROM GRUPPI WHERE GRUPPO_G=this.w_GRUPPO INTO CURSOR GRUP2
              else
                SELECT * FROM GRUPPI INTO CURSOR GRUP2
              endif
              if reccount("GRUP2")<>0
                SELECT GRUP2
                this.w_CURSORE = "GRUP2"
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
          case this.w_TIPOSEZ="S"
            if NOT(this.w_SGRUPLETTO)
              * --- Creazione tabella SOTTOGRUPPI
              vq_exec("..\OFFE\EXE\QUERY\GSOF_SOTTOGRUPPI.VQR",this,"SOTTOG")
              this.w_SGRUPLETTO = .T.
            endif
            * --- Sezione SottoGruppo
            if reccount("SOTTOG")<>0 
              * --- Definizione del cursore SottoGruppi in base della sezione e del filtro su gruppo e sottogruppo
              if EMPTY(this.w_GRUPPO)
                if EMPTY(this.w_SOTTOG)
                  * --- Se entrambi vuoti i filtri prende tutto
                  SELECT * FROM SOTTOG INTO CURSOR SOTTOG2
                else
                  * --- Se il sottogruppo non � vuoto filtra su quello
                  SELECT * FROM SOTTOG WHERE SOTTOGRUPPO_SG=this.w_SOTTOG INTO CURSOR SOTTOG2
                endif
              else
                if EMPTY(this.w_SOTTOG)
                  * --- Se il gruppo non � vuoto filtra su quello
                  SELECT * FROM SOTTOG WHERE GRUPPO_SOTTOGR=this.w_GRUPPO INTO CURSOR SOTTOG2
                else
                  * --- Se gruppo e sottogruppo non sono vuoti fa filtro
                  SELECT * FROM SOTTOG WHERE GRUPPO_SOTTOGR=this.w_GRUPPO AND SOTTOGRUPPO_SG=this.w_SOTTOG INTO CURSOR SOTTOG2
                endif
              endif
              if reccount("SOTTOG2")<>0
                SELECT SOTTOG2
                this.w_CURSORE = "SOTTOG2"
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
          case this.w_TIPOSEZ="W"
            * --- File Word Processor
            if PROGRESS<>this.w_PROGRESS
              if NOT EMPTY(this.w_MODELLO)
                if this.oParentObject.w_TIPOWP="W"
                  if !this.oWord.ActiveDocument.Bookmarks.Exists(this.w_SEGNA)
                    * --- Inserisce il file modello alla fine del documento attivo
                    this.oWord.ActiveDocument.Bookmarks(this.w_SEGNDOC).Range.InsertFile(ALLTRIM(NVL(this.w_MODELLO,"")))     
                    * --- Gestione errore
                    if NUMERR<>0
                      this.Page_4()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                  else
                    * --- Inserisce il file modello nel documento attivo
                    this.oWord.ActiveDocument.Bookmarks(this.w_SEGNA).Range.InsertFile(ALLTRIM(NVL(this.w_MODELLO,"")))     
                    * --- Gestione errore
                    if NUMERR<>0
                      this.Page_4()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                  endif
                  * --- Gestione errore
                  if NUMERR<>0
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                else
                  if TYPE("this.oWriter.getBookmarks().getByName(this.w_SEGNA)")<>"O"
                    * --- Se non esiste il segnalibro posiziono il cursore alla fine del documento
                    this.oText = this.oWriter.getText()
                    this.oCurs = this.oText.createTextCursorByRange(this.oText.getEnd())
                    comarray(this.oCurs,10)
                    * --- Gestione errore
                    if NUMERR<>0
                      this.Page_4()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                  else
                    * --- Altrimenti posiziono il cursore sul segnalibro
                    this.oBookmark = this.oWriter.getBookmarks().getByName(this.w_SEGNA)
                    this.oCurs = this.oBookmark.Anchor.Text.createTextCursorByRange(this.oBookmark.Anchor)
                    comarray(this.oCurs,10)
                    * --- Gestione errore
                    if NUMERR<>0
                      this.Page_4()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                  endif
                  * --- Inserisco il documento....
                  this.oCurs.InsertDocumentFromURL("file:///"+StrTran(ALLTRIM(FULLPATH(this.w_MODELLO)),CHR(92),CHR(47)), @noArgs)     
                  this.oCurs.collapseToEnd()     
                  * --- Gestione errore
                  if NUMERR<>0
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                endif
              endif
            endif
          case this.w_TIPOSEZ="A"
            if NOT(this.w_ARTLETTO)
              * --- Creazione tabella ARTICOLI
              vq_exec("..\OFFE\EXE\QUERY\GSOF_ARTICOLI.VQR",this,"ARTICO")
              this.w_ARTLETTO = .T.
            endif
            * --- Sezione Articoli
            if reccount("ARTICO")<>0 
              * --- Definizione del cursore Articoli in base della sezione e dei filtri 
              *     su gruppo e sottogruppo
              if EMPTY(this.w_GRUPPO)
                if EMPTY(this.w_SOTTOG)
                  * --- Se entrambi vuoti i filtri prende tutto
                  SELECT * FROM ARTICO INTO CURSOR ARTI2
                else
                  * --- Se il sottogruppo non � vuoto filtra su quello
                  SELECT * FROM ARTICO WHERE SOTTOGRUPPO=this.w_SOTTOG INTO CURSOR ARTI2
                endif
              else
                if EMPTY(this.w_SOTTOG)
                  * --- Se il gruppo non � vuoto filtra su quello
                  SELECT * FROM ARTICO WHERE GRUPPO=this.w_GRUPPO INTO CURSOR ARTI2
                else
                  * --- Se gruppo e sottogruppo non sono vuoti fa filtra 
                  SELECT * FROM ARTICO WHERE GRUPPO=this.w_GRUPPO AND SOTTOGRUPPO=this.w_SOTTOG INTO CURSOR ARTI2
                endif
              endif
              if reccount("ARTI2")<>0
                SELECT ARTI2
                this.w_CURSORE = "ARTI2"
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
          case this.w_TIPOSEZ="T"
            * --- Dati fissi
            vq_exec("..\OFFE\EXE\QUERY\GSOF_DATIFISSI.VQR",this,"FISSI")
            if reccount("FISSI")<>0
              * --- Inserisce i campi della tabella attiva nell'array w_CAMPI 
              *     e ne restituisce il numero in w_NUMCAMPI
              this.w_NUMCAMPI = AFIELDS(w_CAMPI)
              SELECT FISSI
              GO TOP
              this.w_NUMPROG = 1
              do while this.w_NUMPROG <= this.w_NUMCAMPI
                this.w_Immagine1 = .F.
                this.w_SEGNALIBRO = this.w_PREFISSO+w_CAMPI(this.w_NUMPROG,1)
                * --- Inserisce il campo fisso nel segnalibro se esiste
                this.w_SEGNALIBRO_EXISTS = .F.
                if this.oParentObject.w_TIPOWP="W"
                  if this.oWord.ActiveDocument.Bookmarks.Exists(this.w_SEGNALIBRO)
                    this.w_SEGNALIBRO_EXISTS = .T.
                  endif
                  * --- Gestione errore
                  if NUMERR<>0
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                else
                  if TYPE("this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO)")="O"
                    this.w_SEGNALIBRO_EXISTS = .T.
                  endif
                  * --- Gestione errore
                  if NUMERR<>0
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                endif
                if this.w_SEGNALIBRO_EXISTS
                  * --- Inserisce i campi
                  do case
                    case INLIST(w_CAMPI(this.w_NUMPROG,2),"D","T")
                      this.w_DATO = DTOC(cp_todate(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1))))
                    case INLIST(w_CAMPI(this.w_NUMPROG,2),"C","M")
                      if "_P" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
                        this.w_Immagine1 = .T.
                      endif
                      if Not this.w_Immagine1 And this.oParentObject.w_TIPOWP="O"
                        * --- Se non � un'immagine e utilizzo Open Office devo eliminare tutti i CHR(13) dalla stringa
                        *     Se � un campo memo infatti visto che sotto windows l'invio inserisce CHR(10)+CHR(13)
                        *     in open office da 2 invii
                        this.w_DATO = STRTRAN(ALLTRIM(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)),"")),CHR(13),"")
                      else
                        this.w_DATO = ALLTRIM(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)),""))
                      endif
                    case INLIST(w_CAMPI(this.w_NUMPROG,2),"N","F", "I", "B", "Y")
                      do case
                        case "_Q" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
                          * --- Quantit�
                          this.w_DATO = ALLTRIM(TRAN(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)), 0), v_PQ[15]))
                        case "_I" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
                          * --- Importi
                          this.w_DATO = ALLTRIM(TRAN(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)), 0), v_PV[20]))
                        case "_0" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
                          * --- Nessun decimale
                          this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)), 0) ,0), v_PV[40]))
                        case "_1" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
                          * --- 1 decimale
                          this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)), 0) ,1), v_PV[60]))
                        case "_2" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
                          * --- 2 decimali
                          this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)), 0) ,2 ), v_PV[80]))
                        case "_3" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
                          * --- 3 decimali
                          this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)), 0) ,3 ), v_PV[100]))
                        case "_4" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
                          * --- 4 decimali
                          this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)), 0) , 4 ), v_PV[120]))
                        case "_5" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
                          * --- 5 decimali
                          this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)), 0) , 5 ), v_PV[140]))
                        otherwise
                          * --- Nessun decimale se non specificato nulla
                          this.w_DATO = ALLTRIM(TRAN(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)), 0), v_PV[40]))
                      endcase
                    otherwise
                      this.w_DATO = ALLTRIM(STR(NVL(EVALUATE("FISSI." + w_CAMPI(this.w_NUMPROG,1)),"")))
                  endcase
                  if this.oParentObject.w_TIPOWP="W"
                    if this.w_Immagine1
                      if file(this.w_DATO)
                        this.oShape = this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.InlineShapes
                        this.oShape.AddPicture(this.w_DATO)     
                      endif
                    else
                      this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.InsertAfter(this.w_DATO)     
                    endif
                    if NUMERR<>0
                      this.Page_4()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                    this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Delete()     
                    * --- Gestione errore
                    if NUMERR<>0
                      this.Page_4()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                  else
                    this.oBookmark = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO)
                    this.oCurs = this.oBookmark.Anchor.Text.createTextCursorByRange(this.oBookmark.Anchor)
                    this.oAnchor = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO).getAnchor()
                    if this.w_Immagine1
                      * --- Devo inserire un'immagine
                      if file(this.w_DATO)
                        this.oGraphic = this.oWriter.createInstance("com.sun.star.text.GraphicObject")
                        * --- E' necessario assegnare la struttura ad una variabile array per evitare errori di Office
                         
 Dimension argTemp(1) 
 argTemp[1]=this.oGraphic.Bridge_getStruct("com.sun.star.beans.PropertyValue")
                        this.oGraphic.setPropertyValue("GraphicURL","file:///"+strtran(alltrim(this.w_DATO),chr(92),chr(47)))     
                        if isNull(this.oAnchor.TextTable)
                          * --- Il bookmark � fuori da una tabella: inserisco immagine
                          this.oWriter.text.insertTextContent(this.oCurs, this.oGraphic, .F.)     
                        else
                          * --- Il bookmark � dentro una tabella: inserisco immagine
                          this.oBookmark.Anchor.text.insertTextContent(this.oBookmark.Anchor,this.oGraphic,.F.)     
                        endif
                      endif
                    else
                      this.oCurs.setString(this.w_DATO)     
                      this.oCurs.collapseToEnd()     
                    endif
                    this.oWriter.Text.RemoveTextContent(this.oBookmark)     
                    * --- Gestione errore
                    if NUMERR<>0
                      this.Page_4()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                  endif
                endif
                this.w_NUMPROG = this.w_NUMPROG + 1
              enddo
            endif
        endcase
        ENDSCAN
        * --- Salvataggio documento Word/PDF
        do case
          case this.w_TIPODOC="W"
            * --- Se esiste il file
            if File(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC))
              if this.w_RISCRIVI
                * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                if this.oParentObject.w_TIPOWP="W"
                  this.oWord.ActiveDocument.SaveAs(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC))     
                else
                   
 Args[1].name="Overwrite" 
 Args[1].value=.T. 
 Args[2].name="FilterName" 
 Args[2].value="MS Word 97" 
 this.oWriter.storeAsURL("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC)),CHR(92),CHR(47)), @Args)
                  * --- Chiudo il documento in modalit� Hidden 
                  *     e lo riapro per renderlo visibile
                  this.oWriter.close(.T.)     
                   
 Args[1].name="AsTemplate" 
 Args[1].value=.F. 
 this.oWriter=this.oDesktop.loadComponentFromURL("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC)),CHR(92),CHR(47)), "_blank",0, @Args)
                endif
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                * --- Write into OFF_ERTE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OFPATFWP ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEDOC),'OFF_ERTE','OFPATFWP');
                      +i_ccchkf ;
                  +" where ";
                      +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                      +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                         )
                else
                  update (i_cTable) set;
                      OFPATFWP = this.w_NOMEDOC;
                      &i_ccchkf. ;
                   where;
                      OFSERIAL = this.oParentObject.w_OFSERIAL;
                      and OFCODNOM = this.oParentObject.w_OFCODNOM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            else
              * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
              if NOT DIRECTORY(this.w_PATHALL)
                MD (PATH)
              endif
              if this.oParentObject.w_TIPOWP="W"
                this.oWord.ActiveDocument.SaveAs(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC))     
              else
                 
 Args[1].name="Overwrite" 
 Args[1].value=.F. 
 Args[2].name="FilterName" 
 Args[2].value="MS Word 97" 
 this.oWriter.StoreAsUrl("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC)),CHR(92),CHR(47)), @Args)
                * --- Chiudo il documento in modalit� Hidden 
                *     e lo riapro per renderlo visibile
                this.oWriter.close(.T.)     
                 
 Args[1].name="AsTemplate" 
 Args[1].value=.F. 
 this.oWriter=this.oDesktop.loadComponentFromURL("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC)),CHR(92),CHR(47)), "_blank",0, @Args)
              endif
              * --- Gestione errore
              if NUMERR<>0
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              * --- Write into OFF_ERTE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OFPATFWP ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEDOC),'OFF_ERTE','OFPATFWP');
                    +i_ccchkf ;
                +" where ";
                    +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                    +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                       )
              else
                update (i_cTable) set;
                    OFPATFWP = this.w_NOMEDOC;
                    &i_ccchkf. ;
                 where;
                    OFSERIAL = this.oParentObject.w_OFSERIAL;
                    and OFCODNOM = this.oParentObject.w_OFCODNOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            if this.oParentObject.w_TIPOWP="W"
              this.oWord.visible = .t.
            else
            endif
            * --- Gestione errore
            if NUMERR<>0
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if this.oParentObject.w_TIPOWP="W"
              this.oWord = NULL
            else
              this.oWriter = NULL
            endif
            * --- Gestione errore
            if NUMERR<>0
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          case this.w_TIPODOC="P"
            if this.oParentObject.w_TIPOWP="W"
              * --- File PDF
              this.w_PRINTERDEFA = this.oWord.ActivePrinter
              if File(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)+".PDF")
                if this.w_RISCRIVI
                  * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                  AH_Msg("Stampa su PDF...")
                  * --- Stampa utilizzando driver PDF
                  =PDF_Format(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF),"DocumentoWord"," ",this.oWord)
                  if FILE(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF))
                    file_no_ext=this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)
                    file_with_ext=file_no_ext+".PDF"
                    COPY FILE (file_no_ext) TO (file_with_ext)
                  endif
                  AH_Msg("Fine stampa su PDF...")
                  * --- Gestione errore
                  if NUMERR<>0
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  * --- Write into OFF_ERTE
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"OFPATPDF ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEPDF+".PDF"),'OFF_ERTE','OFPATPDF');
                        +i_ccchkf ;
                    +" where ";
                        +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                        +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                           )
                  else
                    update (i_cTable) set;
                        OFPATPDF = this.w_NOMEPDF+".PDF";
                        &i_ccchkf. ;
                     where;
                        OFSERIAL = this.oParentObject.w_OFSERIAL;
                        and OFCODNOM = this.oParentObject.w_OFCODNOM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              else
                * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                AH_Msg("Stampa su PDF...")
                * --- Stampa utilizzando driver PDF
                if NOT DIRECTORY(this.w_PATHALL)
                  MD (PATH)
                endif
                =PDF_Format(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF),"DocumentoWord"," ",this.oWord)
                if FILE(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF))
                  file_no_ext=this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)
                  file_with_ext=file_no_ext+".PDF"
                  COPY FILE (file_no_ext) TO (file_with_ext)
                endif
                AH_Msg("Fine stampa su PDF...")
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                * --- Write into OFF_ERTE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OFPATPDF ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEPDF+".PDF"),'OFF_ERTE','OFPATPDF');
                      +i_ccchkf ;
                  +" where ";
                      +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                      +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                         )
                else
                  update (i_cTable) set;
                      OFPATPDF = this.w_NOMEPDF+".PDF";
                      &i_ccchkf. ;
                   where;
                      OFSERIAL = this.oParentObject.w_OFSERIAL;
                      and OFCODNOM = this.oParentObject.w_OFCODNOM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              this.oWord.ActivePrinter = this.w_PRINTERDEFA
              this.oWord.ActiveDocument.Close(0,,)     
              * --- Gestione errore
              if NUMERR<>0
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              DELETE FILE this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)
              this.oWord.Quit()     
              * --- Gestione errore
              if NUMERR<>0
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              * --- Gestione PDF con OpenOffice
              if File(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)+".PDF")
                if this.w_RISCRIVI
                  * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                  AH_Msg("Stampa su PDF...")
                  * --- Stampa utilizzando l'export di OpenOffice
                   
 Args[1].name="FilterName" 
 Args[1].value="writer_pdf_Export" 
 this.oWriter.storeToURL("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)),CHR(92),CHR(47))+".PDF", @Args)
                  * --- Gestione errore
                  if NUMERR<>0
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  AH_Msg("Fine stampa su PDF...")
                  this.oWriter.close(.T.)     
                  * --- Write into OFF_ERTE
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"OFPATPDF ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEPDF+".PDF"),'OFF_ERTE','OFPATPDF');
                        +i_ccchkf ;
                    +" where ";
                        +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                        +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                           )
                  else
                    update (i_cTable) set;
                        OFPATPDF = this.w_NOMEPDF+".PDF";
                        &i_ccchkf. ;
                     where;
                        OFSERIAL = this.oParentObject.w_OFSERIAL;
                        and OFCODNOM = this.oParentObject.w_OFCODNOM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  * --- Vedere come aprire il file pdf........
                  *     
                endif
              else
                * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                AH_Msg("Stampa su PDF...")
                * --- Stampa utilizzando l'export di OpenOffice
                 
 Args[1].name="FilterName" 
 Args[1].value="writer_pdf_Export" 
 this.oWriter.storeToURL("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)),CHR(92),CHR(47))+".PDF", @Args)
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                AH_Msg("Fine stampa su PDF...")
                this.oWriter.close(.T.)     
                * --- Write into OFF_ERTE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OFPATPDF ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEPDF+".PDF"),'OFF_ERTE','OFPATPDF');
                      +i_ccchkf ;
                  +" where ";
                      +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                      +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                         )
                else
                  update (i_cTable) set;
                      OFPATPDF = this.w_NOMEPDF+".PDF";
                      &i_ccchkf. ;
                   where;
                      OFSERIAL = this.oParentObject.w_OFSERIAL;
                      and OFCODNOM = this.oParentObject.w_OFCODNOM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
            result2=STAPDF(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)+".PDF", "OPEN", " ")
          case this.w_TIPODOC="E"
            * --- File di Word / OpenOffice
            if File(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC))
              if this.w_RISCRIVI
                * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                if this.oParentObject.w_TIPOWP="W"
                  this.oWord.ActiveDocument.SaveAs(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC))     
                else
                   
 Args[1].name="Overwrite" 
 Args[1].value=.T. 
 Args[2].name="FilterName" 
 Args[2].value="MS Word 97" 
 this.oWriter.StoreAsUrl("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC)),CHR(92),CHR(47)), @Args)
                  * --- Chiudo il documento in modalit� Hidden 
                  *     e lo riapro per renderlo visibile
                  this.oWriter.close(.T.)     
                   
 Args[1].name="AsTemplate" 
 Args[1].value=.F. 
 this.oWriter=this.oDesktop.loadComponentFromURL("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC)),CHR(92),CHR(47)), "_blank",0, @Args)
                endif
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                ON ERROR this.w_DOCOPEN=.T.
                if this.w_DOCOPEN
                  AH_ErrorMsg("Il documento � attualmente aperto")
                  i_retcode = 'stop'
                  return
                else
                  * --- Write into OFF_ERTE
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"OFPATFWP ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEDOC),'OFF_ERTE','OFPATFWP');
                        +i_ccchkf ;
                    +" where ";
                        +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                        +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                           )
                  else
                    update (i_cTable) set;
                        OFPATFWP = this.w_NOMEDOC;
                        &i_ccchkf. ;
                     where;
                        OFSERIAL = this.oParentObject.w_OFSERIAL;
                        and OFCODNOM = this.oParentObject.w_OFCODNOM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
                ON ERROR NUMERR=ERROR()
              endif
            else
              if NOT DIRECTORY(this.w_PATHALL)
                MD (PATH)
              endif
              * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
              if this.oParentObject.w_TIPOWP="W"
                this.oWord.ActiveDocument.SaveAs(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC))     
              else
                 
 Args[1].name="Overwrite" 
 Args[1].value=.T. 
 Args[2].name="FilterName" 
 Args[2].value="MS Word 97" 
 this.oWriter.StoreAsUrl("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC)),CHR(92),CHR(47)), @Args)
                * --- Chiudo il documento in modalit� Hidden
                *     e lo riapro per renderlo visibile.
                this.oWriter.close(.T.)     
                 
 Args[1].name="AsTemplate" 
 Args[1].value=.F. 
 this.oWriter=this.oDesktop.loadComponentFromURL("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEDOC)),CHR(92),CHR(47)), "_blank",0, @Args)
              endif
              * --- Gestione errore
              if NUMERR<>0
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              * --- Write into OFF_ERTE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OFPATFWP ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEDOC),'OFF_ERTE','OFPATFWP');
                    +i_ccchkf ;
                +" where ";
                    +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                    +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                       )
              else
                update (i_cTable) set;
                    OFPATFWP = this.w_NOMEDOC;
                    &i_ccchkf. ;
                 where;
                    OFSERIAL = this.oParentObject.w_OFSERIAL;
                    and OFCODNOM = this.oParentObject.w_OFCODNOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- File PDF
            if this.oParentObject.w_TIPOWP="W"
              this.w_PRINTERDEFA = this.oWord.ActivePrinter
              if File(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)+".PDF")
                if this.w_RISCRIVI
                  * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                  AH_Msg("Stampa su PDF...")
                  * --- Stampa utilizzando driver PDF
                  =PDF_Format(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF),"DocumentoWord"," ",this.oWord)
                  if FILE(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF))
                    file_no_ext=this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)
                    file_with_ext=file_no_ext+".PDF"
                    COPY FILE (file_no_ext) TO (file_with_ext)
                  endif
                  AH_Msg("Fine stampa su PDF...")
                  * --- Gestione errore
                  if NUMERR<>0
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  * --- Write into OFF_ERTE
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"OFPATPDF ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEPDF+".PDF"),'OFF_ERTE','OFPATPDF');
                        +i_ccchkf ;
                    +" where ";
                        +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                        +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                           )
                  else
                    update (i_cTable) set;
                        OFPATPDF = this.w_NOMEPDF+".PDF";
                        &i_ccchkf. ;
                     where;
                        OFSERIAL = this.oParentObject.w_OFSERIAL;
                        and OFCODNOM = this.oParentObject.w_OFCODNOM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              else
                if NOT DIRECTORY(this.w_PATHALL)
                  MD (PATH)
                endif
                * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                AH_Msg("Stampa su PDF...")
                * --- Stampa utilizzando driver PDF
                =PDF_Format(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF),"DocumentoWord"," ",this.oWord)
                if FILE(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF))
                  file_no_ext=this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)
                  file_with_ext=file_no_ext+".PDF"
                  COPY FILE (file_no_ext) TO (file_with_ext)
                endif
                AH_Msg("Fine stampa su PDF...")
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                * --- Write into OFF_ERTE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OFPATPDF ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEPDF+".PDF"),'OFF_ERTE','OFPATPDF');
                      +i_ccchkf ;
                  +" where ";
                      +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                      +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                         )
                else
                  update (i_cTable) set;
                      OFPATPDF = this.w_NOMEPDF+".PDF";
                      &i_ccchkf. ;
                   where;
                      OFSERIAL = this.oParentObject.w_OFSERIAL;
                      and OFCODNOM = this.oParentObject.w_OFCODNOM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              this.oWord.ActivePrinter = this.w_PRINTERDEFA
              DELETE FILE this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)
              this.oWord.visible = .t.
              * --- Gestione errore
              if NUMERR<>0
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              * --- Gestione PDF con OpenOffice
              if File(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)+".PDF")
                if this.w_RISCRIVI
                  * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                  AH_Msg("Stampa su PDF...")
                  * --- Stampa utilizzando l'export di OpenOffice
                   
 Args[1].name="FilterName" 
 Args[1].value="writer_pdf_Export" 
 this.oWriter.storeToURL("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)),CHR(92),CHR(47))+".PDF", @Args)
                  AH_Msg("Fine stampa su PDF...")
                  * --- Gestione errore
                  if NUMERR<>0
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  * --- Write into OFF_ERTE
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"OFPATPDF ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEPDF+".PDF"),'OFF_ERTE','OFPATPDF');
                        +i_ccchkf ;
                    +" where ";
                        +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                        +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                           )
                  else
                    update (i_cTable) set;
                        OFPATPDF = this.w_NOMEPDF+".PDF";
                        &i_ccchkf. ;
                     where;
                        OFSERIAL = this.oParentObject.w_OFSERIAL;
                        and OFCODNOM = this.oParentObject.w_OFCODNOM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              else
                if NOT DIRECTORY(this.w_PATHALL)
                  MD (PATH)
                endif
                * --- Salva il documento nella directory di lavoro definita nel modello e con il nome standard o definito
                AH_Msg("Stampa su PDF...")
                * --- Stampa utilizzando l'export di OpenOffice
                 
 Args[1].name="FilterName" 
 Args[1].value="writer_pdf_Export" 
 this.oWriter.storeToURL("file:///"+StrTran(ALLTRIM(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)),CHR(92),CHR(47))+".PDF", @Args)
                AH_Msg("Fine stampa su PDF...")
                * --- Gestione errore
                if NUMERR<>0
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                * --- Write into OFF_ERTE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OFPATPDF ="+cp_NullLink(cp_ToStrODBC(this.w_NOMEPDF+".PDF"),'OFF_ERTE','OFPATPDF');
                      +i_ccchkf ;
                  +" where ";
                      +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                      +" and OFCODNOM = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                         )
                else
                  update (i_cTable) set;
                      OFPATPDF = this.w_NOMEPDF+".PDF";
                      &i_ccchkf. ;
                   where;
                      OFSERIAL = this.oParentObject.w_OFSERIAL;
                      and OFCODNOM = this.oParentObject.w_OFCODNOM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- VedereCome aprire il file pdf
              endif
            endif
            result2=STAPDF(this.w_PATHALL+ALLTRIM(this.w_NOMEPDF)+".PDF", "OPEN", " ")
        endcase
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if EMPTY(this.w_PATHARC)
          AH_ErrorMsg("Non � stato selezionato un percorso per la memorizzazione")
        else
          if NOT(this.w_EXISTFOLD)
            AH_ErrorMsg("Il percorso per la memorizzazione non � corretto")
          endif
        endif
        if EMPTY(this.w_PATHMODELLO)
          if this.oParentObject.w_TIPOWP="W"
            ah_ErrorMsg("Non � stato selezionato un modello per il documento Word",,"")
          else
            ah_ErrorMsg("Non � stato selezionato un modello per il documento SWriter di OpenOffice",,"")
          endif
        else
          if NOT FILE(ALLTRIM(this.w_PATHMODELLO)) 
            if this.oParentObject.w_TIPOWP="W"
              AH_ErrorMsg("Il percorso selezionato per il modello Word non � corretto")
            else
              AH_ErrorMsg("Il percorso selezionato per il modello SWriter di OpenOffice non � corretto")
            endif
          endif
        endif
        if this.oParentObject.w_TIPOWP="W"
          this.oWord.ActiveDocument.Close(0,,)     
          this.oWord.Quit()     
        else
          this.oWriter.close(.T.)     
        endif
        * --- Gestione errore
        if NUMERR<>0
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    else
      if this.oParentObject.w_TIPOWP<>"N"
        if NOT DIRECTORY(this.w_PATHARC)
          AH_ErrorMsg("Il percorso per la memorizzazione del documento non � valido")
        endif
        if FILE(this.w_PATHMODELLO)
          if this.oParentObject.w_TIPOWP="W"
            ah_ErrorMsg("Il percorso per il modello Word Processor non � valido",,"")
          else
            ah_ErrorMsg("Il percorso per il modello SWriter di OpenOffice non � valido",,"")
          endif
        endif
      endif
    endif
    DecoratorState ("A")
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CANCELLAZIONI CURSORI
    if used("SEZIONI")
      select SEZIONI
      use
    endif
    if used("GRUP2")
      select GRUP2
      use
    endif
    if used("SOTTOG2")
      select SOTTOG2
      use
    endif
    if used("ARTI2")
      select ARTI2
      use
    endif
    if used("FISSI")
      select FISSI
      use
    endif
    if used("GRUPPI")
      select GRUPPI
      use
    endif
    if used("SOTTOG")
      select SOTTOG
      use
    endif
    if used("ARTICO")
      select ARTICO
      use
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura documento Word
    if this.w_STRUTSEZ="MOD"
       
 Select (this.w_CURSORE) 
 GO TOP
      * --- Gestione Modelli in forma non Tabellare.
      *     Per ogni riga di dettaglio, quindi per ogni record del cursore dati, inserisco 
      *     un nuovo documento (modello) inserendo di conseguenza i dati nei segnalibri.
      *     Tutto ci� viene fatto in pagina 7
      scan
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Dato che itero sul cursore ed inserisco un nuovo modello sempre nel
      *     segnalibro w_SEGNA, devo cancellare proprio questo segnalibro in quanto sar� presente 
      *     all'interno del modello di dettaglio.
      if this.oParentObject.w_TIPOWP="W"
        if not empty(this.w_SEGNA) and this.oWord.ActiveDocument.Bookmarks.Exists(this.w_SEGNA)
          * --- Cancello il Segnalibro
          this.oWord.ActiveDocument.Bookmarks(this.w_SEGNA).Delete()     
        endif
        * --- if w_strutsez='MTB'
        *     Vado alla fine del documento e cancello una riga
        *     owor.activedocument.bookmarks(this.w_segndoc).range.delete(1,1)
        *     endif
      else
        if not empty(this.w_SEGNA) and TYPE("this.oWriter.getBookmarks().getByName(this.w_SEGNA)" ) = "O"
          * --- Cancella segnalibro
          this.oWriter.Text.removeTextContent(this.oWriter.getBookmarks().getByName(this.w_SEGNA))     
        endif
        * --- if w_STRUTSEZ='MTB'
        *       Non � ancora stato verificato il comando per OpenOffice per la cancellazione 
        *       dell'ultima riga del documento.
        *     endif
      endif
      * --- Dato che si tratta di inserire sempre lo stesso modello alla fine del documento,
      *     sbianchiamo eventualmente la variabile w_segna che contiene il segnalibro 
      *     di aggancio
      this.w_SEGNA = ""
      endscan
    else
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_NUMRIG = 2
    this.w_PRIMO = .T.
     
 Select (this.w_CURSORE) 
 GO TOP
     SCAN
    this.w_NUMPROG = 1
    if !this.w_PRIMO AND (this.w_NUMPROG <= this.w_NUMCAMPI)
      if this.oParentObject.w_TIPOWP="W"
        * --- Aggiunge un a riga alla 1� tabella recuperata nel testo---
        *     --- NON CREA UNA TABELLA VUOTA! ----
        this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.Tables(1).Rows.Add()     
      else
        * --- ...Il bookmark � in una tabella...
        this.oBookmark = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO)
        this.oAnchor = this.oBookmark.getAnchor()
        this.w_TABLENAME = this.oAnchor.TextTable.getName()
        this.oTable = this.oWriter.getTextTables().getByName(this.w_TABLENAME)
        * --- Aggiunge una riga alla tabella w_TABLENAME
        local L_OldErrCellTables, L_ErrCellTabes
        L_OldErrCellTables = ON("ERROR")
        ON ERROR L_ErrCellTabes = .T.
        * --- Recupero il numero di colonne e righe
        this.w_OOTOTCOL = this.oTable.getColumns().getCount()
        this.w_OOTOTROW = this.oTable.getRows().getCount()
        DIMENSION oArrayCelle(this.w_OOTOTCOL, this.w_OOTOTROW)
        this.w_OOATTROW = 0
        do while this.w_OOATTROW <= this.w_OOTOTROW
          this.w_OOATTCOL = 0
          do while this.w_OOATTCOL <= this.w_OOTOTCOL
            oArrayCelle(this.w_OOATTCOL + 1, this.w_OOATTROW + 1) = this.oTable.getCellByPosition(this.w_OOATTCOL,this.w_OOATTROW).getStart().Cell.CellName
            this.w_OOATTCOL = this.w_OOATTCOL + 1
          enddo
          this.w_OOATTROW = this.w_OOATTROW + 1
        enddo
        ON ERROR &L_OldErrCellTables
        release L_OldErrCellTables, L_ErrCellTabes
        this.w_LENGTH = ALEN(oArrayCelle)
        this.w_STRING_N = oArrayCelle[this.w_LENGTH]
        this.w_STRING_N = SUBSTR(this.w_STRING_N,2)
        this.w_STRING_I = INT(VAL(this.w_STRING_N))
        this.oTable.getRows().insertByIndex(this.w_STRING_I,1)     
      endif
    endif
    do while this.w_NUMPROG <= this.w_NUMCAMPI
      this.w_Immagine = .F.
      this.w_SEGNALIBRO = this.w_PREFISSO+w_CAMPI(this.w_NUMPROG,1)
      * --- Determinazione del range del segnalibro
      if this.oParentObject.w_TIPOWP="W"
        this.w_oPosizione = this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range
      else
        * --- ...Il bookmark � dentro una cella....
        this.oAnchor = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO).getAnchor()
        this.w_TABLENAME = this.oAnchor.TextTable.getName()
      endif
      if this.w_PRIMO
        * --- Posizione del segnalibro nella tabella
        if this.oParentObject.w_TIPOWP="W"
          this.w_COLONNA = this.w_oPosizione.Information(this.w_NUMCOLON)
        else
          * --- ...Il bookmark � dentro una cella....
          this.w_SUBSTRING = substr(this.oAnchor.Cell.CellName,1,1)
          this.w_COLONNA = ASC(UPPER(this.w_SUBSTRING)) - this.w_ASCITEST
        endif
        w_CAMPI(this.w_NUMPROG,3) = this.w_COLONNA
      else
        this.w_COLONNA = w_CAMPI(this.w_NUMPROG,3)
      endif
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_TIPOWP="W"
        * --- Inserisce il dato nella cella
        if this.w_Immagine
          if Not Empty(this.w_DATO) and file(this.w_DATO)
            this.oShape = this.w_oPosizione.Tables(1).Cell(this.w_NUMRIG,this.w_COLONNA).Range.InlineShapes
            this.oShape.AddPicture(this.w_DATO)     
          endif
        else
          this.w_oPosizione.Tables(1).Cell(this.w_NUMRIG,this.w_COLONNA).Range.InsertAfter(this.w_DATO)     
        endif
      else
        local L_OldErrCellTables, L_ErrCellTabes
        L_OldErrCellTables = ON("ERROR")
        ON ERROR L_ErrCellTabes = .T.
        * --- Recupero il numero di colonne e righe
        this.w_OOTOTCOL = this.oWriter.getTextTables().getByName(this.w_TABLENAME).getColumns().getCount()
        this.w_OOTOTROW = this.oWriter.getTextTables().getByName(this.w_TABLENAME).getRows().getCount()
        DIMENSION w_CELLNAMES(this.w_OOTOTCOL, this.w_OOTOTROW)
        this.w_OOATTROW = 0
        do while this.w_OOATTROW <= this.w_OOTOTROW
          this.w_OOATTCOL = 0
          do while this.w_OOATTCOL <= this.w_OOTOTCOL
            w_CELLNAMES(this.w_OOATTCOL + 1, this.w_OOATTROW + 1) = this.oWriter.getTextTables().getByName(this.w_TABLENAME).getCellByPosition(this.w_OOATTCOL, this.w_OOATTROW).getStart().Cell.CellName
            this.w_OOATTCOL = this.w_OOATTCOL + 1
          enddo
          this.w_OOATTROW = this.w_OOATTROW + 1
        enddo
        ON ERROR &L_OldErrCellTables
        release L_OldErrCellTables, L_ErrCellTabes
        this.w_CELLNAME = UPPER(CHR(this.w_COLONNA+this.w_ASCITEST))+ALLTRIM(STR(this.w_NUMRIG))
        if ascan(w_CELLNAMES,this.w_CELLNAME)>0
          this.oCurs = this.oWriter.getTextTables().getByName(this.w_TABLENAME).getCellByName(this.w_CELLNAME)
          comarray(this.oCurs,10)
          if this.w_Immagine
            if not Empty (this.w_DATO) and file(this.w_DATO)
              this.oGraphic = this.oWriter.createInstance("com.sun.star.text.GraphicObject")
              * --- E' necessario assegnare la struttura ad una variabile array per evitare di Office
               
 Dimension argTemp(1) 
 argTemp[1]=this.oGraphic.Bridge_getStruct("com.sun.star.beans.PropertyValue")
              this.oGraphic.setPropertyValue("GraphicURL","file:///"+strtran(alltrim(this.w_DATO),chr(92),chr(47)))     
              * --- Il bookmark � dentro una tabella: inserisco immagine
              this.oCurs.insertTextContent(this.oCurs.createTextCursor(),this.oGraphic,.F.)     
            endif
          else
            this.oCurs.createTextCursor().setString(this.w_DATO)     
          endif
        endif
      endif
      this.w_NUMPROG = this.w_NUMPROG + 1
    enddo
    this.w_PRIMO = .F.
    this.w_NUMRIG = this.w_NUMRIG + 1
    ENDSCAN
    if this.w_STRUTSEZ<>"MOD"
      this.w_NUMPROG = 1
      do while this.w_NUMPROG <= this.w_NUMCAMPI
        this.w_SEGNALIBRO = this.w_PREFISSO+w_CAMPI(this.w_NUMPROG,1)
        if this.oParentObject.w_TIPOWP="W"
          if this.oWord.ActiveDocument.Bookmarks.Exists(this.w_SEGNALIBRO)
            * --- Cancello il Segnalibro
            this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Delete()     
          endif
        else
          if TYPE("this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO)" ) = "O"
            * --- Cancella segnalibro
            this.oWriter.Text.removeTextContent(this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO))     
          endif
        endif
        this.w_NUMPROG = this.w_NUMPROG + 1
      enddo
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione messaggi di errore
    if this.w_NOTROVA=.T.
      if this.oParentObject.w_TIPOWP="W"
        this.w_MESS = "Il percorso per il modello Word Processor della sezione %1 non � valido"
      else
        this.w_MESS = "Il percorso per il modello Writer della sezione %1 non � valido"
      endif
      ah_ErrorMsg(this.w_MESS,,"", this.w_SEZIONE)
    else
      if this.oParentObject.w_TIPOWP="W"
        this.w_MESS = "Errore nella generazione Word Processor:%0%1%0Verificare struttura modello in sezioni"
      else
        this.w_MESS = "Errore nella generazione SWriter di OpenOffice:%0%1%0Verificare struttura modello in sezioni"
      endif
      ah_ErrorMsg(this.w_MESS,,"", MESSAGE())
    endif
    if this.oParentObject.w_TIPOWP="W"
      this.oWord.ActiveDocument.Close(0,,)     
      this.oWord.Quit()     
    else
      this.oWriter.close(.T.)     
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se esiste il file calcolo nominativo
    * --- Read from PAR_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "POFLCAAR"+;
        " from "+i_cTable+" PAR_OFFE where ";
            +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        POFLCAAR;
        from (i_cTable) where;
            POCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ArcNom = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ArcNom="S"
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NODESCRI"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NODESCRI;
          from (i_cTable) where;
              NOCODICE = this.oParentObject.w_OFCODNOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RagSoc = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PATHALL = ALLTRIM(this.w_PATHARC)+alltrim(this.w_RagSoc)+"\" 
    else
      this.w_PATHALL = ALLTRIM(this.w_PATHARC)
    endif
    * --- Se la rag.sociale termina con ' . ' lo elimino in quanto provoca errore nella ricerca della cartella creata sempre senza 
    if this.oParentObject.w_TIPOWP<>"W" and right(ALLTRIM(this.w_PATHALL),2)= "." 
      this.w_PATHALL = left(this.w_PATHALL, LEN(this.w_PATHALL)-2) +"\"
    endif
    * --- Calcolo del periodo
    do case
      case this.oParentObject.w_PERIODO="M"
        this.w_PATHALL = this.w_PATHALL+ALLTRIM(CMONTH(this.oParentObject.w_OFDATDOC))+"\"
      case this.oParentObject.w_PERIODO="T"
        do case
          case MONTH(this.oParentObject.w_OFDATDOC)<=3
            this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Trimestre1\")
          case MONTH(this.oParentObject.w_OFDATDOC)>3 AND MONTH(this.oParentObject.w_OFDATDOC)<=6
            this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Trimestre2\")
          case MONTH(this.oParentObject.w_OFDATDOC)>6 AND MONTH(this.oParentObject.w_OFDATDOC)<=9
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Trimestre3\")
          case MONTH(this.oParentObject.w_OFDATDOC)>9 AND MONTH(this.oParentObject.w_OFDATDOC)<=12
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Trimestre4\")
        endcase
      case this.oParentObject.w_PERIODO="Q"
        do case
          case MONTH(this.oParentObject.w_OFDATDOC)<=4
            this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Quadrimestre1\")
          case MONTH(this.oParentObject.w_OFDATDOC)>4 AND MONTH(this.oParentObject.w_OFDATDOC)<=8
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Quadrimestre2\")
          case MONTH(this.oParentObject.w_OFDATDOC)>8 AND MONTH(this.oParentObject.w_OFDATDOC)<=12
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Quadrimestre3\")
        endcase
      case this.oParentObject.w_PERIODO="S"
        do case
          case MONTH(this.oParentObject.w_OFDATDOC)<=6
            this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Semestre1\")
          case MONTH(this.oParentObject.w_OFDATDOC)>6 AND MONTH(this.oParentObject.w_OFDATDOC)<=12
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Semestre2\")
        endcase
      case this.oParentObject.w_PERIODO="A"
        this.w_PATHALL = this.w_PATHALL+ALLTRIM(STR(YEAR(this.oParentObject.w_OFDATDOC)))+"\"
    endcase
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Valori del Cursore
    do case
      case INLIST(w_CAMPI(this.w_NUMPROG,2),"D","T")
        this.w_DATO = DTOC(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)),""))
      case INLIST(w_CAMPI(this.w_NUMPROG,2),"C","M")
        if "_P" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
          this.w_Immagine = .T.
        endif
        if Not this.w_Immagine And this.oParentObject.w_TIPOWP="O"
          * --- Se non � un'immagine e utilizzo Open Office devo eliminare tutti i CHR(13) dalla stringa
          *     Se � un campo memo infatti visto che sotto windows l'invio inserisce CHR(10)+CHR(13)
          *     in open office da 2 invii
          this.w_DATO = STRTRAN(ALLTRIM(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)),"")),CHR(13),"")
        else
          this.w_DATO = ALLTRIM(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)),""))
        endif
      case INLIST(w_CAMPI(this.w_NUMPROG,2),"N","F", "I", "B", "Y")
        * --- Nel caso il contenuto del campo sia Nullo lo sostituisco con ''
        *     Per esempio le Righe descrittive non devono avere Quantit� 0 ma ''
        if Isnull( EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)) )
          this.w_DATO = ""
        else
          do case
            case "_Q" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
              * --- Quantit�
              this.w_DATO = ALLTRIM(TRAN(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)), 0), v_PQ[15]))
            case "_I" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
              * --- Importi
              this.w_DATO = ALLTRIM(TRAN(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)), 0), v_PV[20]))
            case "_0" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
              * --- Nessun decimale
              this.w_DATO = ALLTRIM(TRAN( cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)), 0) , 0), v_PV[40]))
            case "_1" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
              * --- 1 decimale
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)), 0) ,1), v_PV[60]))
            case "_2" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
              * --- 2 decimali
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL( EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1) ), 0) ,2 ), v_PV[80]))
            case "_3" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
              * --- 3 decimali
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1) ), 0) , 3), v_PV[100]))
            case "_4" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
              * --- 4 decimali
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)), 0) ,4), v_PV[120]))
            case "_5" = RIGHT(w_CAMPI(this.w_NUMPROG,1) ,2)
              * --- 5 decimali
              this.w_DATO = ALLTRIM(TRAN(cp_Round(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)), 0) , 5), v_PV[140]))
            otherwise
              * --- Nessun decimale se non specificato nulla
              this.w_DATO = ALLTRIM(TRAN(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)), 0), v_PV[40]))
          endcase
        endif
      otherwise
        this.w_DATO = ALLTRIM(STR(NVL(EVALUATE(this.w_CURSORE + "." + w_CAMPI(this.w_NUMPROG,1)),"")))
    endcase
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiunge un file modello se presente, ricerca 
    *     i segnalibri nel documento e se presenti scrive
    *     i dati all'interno.
    *     ! CHIUDERE I BLOCCHI PER VISUALIZZARE I VAR ICOMMENTI !
    if not empty(this.w_MODELLO)
      if this.oParentObject.w_TIPOWP="W"
        if !this.oWord.ActiveDocument.Bookmarks.Exists(this.w_SEGNA)
          * --- Inserisce il file modello alla fine del documento attivo
          this.oWord.ActiveDocument.Bookmarks(this.w_SEGNDOC).Range.InsertFile(ALLTRIM(NVL(this.w_MODELLO,"")))     
        else
          * --- Inserisce il file modello nel documento attivo
          this.oWord.ActiveDocument.Bookmarks(this.w_SEGNA).Range.InsertFile(ALLTRIM(NVL(this.w_MODELLO,"")))     
        endif
      else
        if TYPE("this.oWriter.getBookmarks().getByName(this.w_SEGNA)" )<> "O"
          * --- Se non esiste il segnalibro posiziono il cursore alla fine del documento
          this.oCurs = this.oText.createTextCursorByRange(this.oText.getEnd())
        else
          * --- Altrimenti posiziono il cursore sul segnalibro
          this.oBookmark = this.oWriter.Bookmarks.getByName(this.w_SEGNA)
          this.oCurs = this.oBookmark.Anchor.Text.createTextCursorByRange(this.oBookmark.Anchor)
        endif
        * --- Inserisco il documento....
        this.oCurs.InsertDocumentFromURL("file:///"+StrTran(ALLTRIM(this.w_MODELLO),CHR(92),CHR(47)), @noArgs)     
      endif
    endif
    * --- Inserisce i campi della tabella attiva nell'array w_CAMPI 
    *     e ne restituisce il numero in w_NUMCAMPI
    this.w_NUMCAMPI = AFIELDS(w_CAMPI)
    * --- Scansione Campi Cursore: cancellazione per far rimanere solo quelli Esistenti 
    this.w_NUMPROG = 1
    do while this.w_NUMPROG <= this.w_NUMCAMPI
      this.w_SEGNALIBRO = this.w_PREFISSO+w_CAMPI(this.w_NUMPROG,1)
      this.w_SEGNALIBRO_EXISTS = .F.
      * --- Cerco nel testo i segnalibri
      if this.oParentObject.w_TIPOWP="W"
        if !this.oWord.ActiveDocument.Bookmarks.Exists(this.w_SEGNALIBRO)
          this.w_SEGNALIBRO_EXISTS = .T.
        endif
      else
        if TYPE("this.oWriter.getBookmarks().getByname(this.w_SEGNALIBRO)")<>"O"
          this.w_SEGNALIBRO_EXISTS = .T.
        endif
      endif
      * --- Se i segnalibri NON sono presenti nel testo li ELIMINO dall'array
      if this.w_SEGNALIBRO_EXISTS
        ADEL(w_CAMPI,this.w_NUMPROG)
        this.w_NUMCAMPI = this.w_NUMCAMPI - 1
      else
        if this.w_STRUTSEZ="MOD"
          * --- Se siamo in condizione di avere un modello per ogni riga di dettaglio, 
          *     non controllo la posizione del segnalibro: ho gi� verificato prima che esiste!
          this.w_SEGNALIBRO_EXISTS = .T.
        else
          * --- Il segnalibro � presente nel modello
          this.w_SEGNALIBRO_EXISTS = .F.
          if this.oParentObject.w_TIPOWP="W"
            * --- oWord.ActiveDocument.Bookmarks(Segnalibro).Range.Information(17) = -1 : Fuori tabella
            if this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.Information(this.w_NUMCOLON)=-1
              this.w_SEGNALIBRO_EXISTS = .T.
            endif
          else
            this.oAnchor = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO).getAnchor()
            if isNull(this.oAnchor.TextTable)
              * --- Il bookmarks � fuori dalla tabella
              this.w_SEGNALIBRO_EXISTS = .T.
            endif
          endif
        endif
        * --- Se il segnalibro esiste ed � fuori da una tabella, inserisco subito il dato al suo interno
        if this.w_SEGNALIBRO_EXISTS
          this.w_Immagine = .F.
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_TIPOWP="W"
            if this.w_Immagine
              * --- Gestione inserimento immagine
              if Not Empty(this.w_DATO) and file(this.w_DATO)
                this.oShape = this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.InlineShapes
                this.oShape.AddPicture(this.w_DATO)     
              endif
            else
              * --- Inserimento Dato standard
              this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Range.InsertAfter(this.w_DATO)     
            endif
            * --- Eliminazione segnalibro appena valorizzato
            this.oWord.ActiveDocument.Bookmarks(this.w_SEGNALIBRO).Delete()     
          else
            this.oBookmark = this.oWriter.Bookmarks.getByName(this.w_SEGNALIBRO)
            this.oCurs = this.oBookmark.Anchor.Text.createTextCursorByRange(this.oBookmark.Anchor)
            comarray(this.oCurs,10)
            if this.w_Immagine
              * --- Gestione inserimento immagine
              if Not Empty(this.w_DATO) and file(this.w_DATO)
                this.oAnchor = this.oWriter.getBookmarks().getByName(this.w_SEGNALIBRO).getAnchor()
                this.oGraphic = this.oWriter.createInstance("com.sun.star.text.GraphicObject")
                * --- E' necessario assegnare la struttura ad una variabile array per evitare errori di Office
                 
 Dimension argTemp(1) 
 argTemp[1]=this.oGraphic.Bridge_getStruct("com.sun.star.beans.PropertyValue")
                this.oGraphic.setPropertyValue("GraphicURL","file:///"+strtran(alltrim(this.w_DATO),chr(92),chr(47)))     
                if isNull(this.oAnchor.TextTable)
                  * --- Il bookmark � fuori da una tabella: inserisco immagine
                  this.oWriter.text.insertTextContent(this.oCurs, this.oGraphic, .F.)     
                else
                  * --- Il bookmark � dentro una tabella: inserisco immagine
                  this.oBookmark.Anchor.text.insertTextContent(this.oBookmark.Anchor,this.oGraphic,.F.)     
                endif
              endif
            else
              * --- Inserimento Dato standard
              this.oCurs.setString(this.w_DATO)     
            endif
            * --- Eliminazione segnalibro appena valorizzato
            this.oCurs.collapseToEnd()     
            this.oWriter.Text.removeTextContent(this.oBookmark)     
          endif
          ADEL(w_CAMPI,this.w_NUMPROG)
          this.w_NUMCAMPI = this.w_NUMCAMPI - 1
        else
          this.w_NUMPROG = this.w_NUMPROG + 1
        endif
      endif
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='MOD_OFFE'
    this.cWorkTables[2]='OFF_ERTE'
    this.cWorkTables[3]='PAR_OFFE'
    this.cWorkTables[4]='OFF_NOMI'
    this.cWorkTables[5]='NOM_CONT'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
