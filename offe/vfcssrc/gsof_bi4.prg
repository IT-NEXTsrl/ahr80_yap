* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bi4                                                        *
*              Import kit selezionato                                          *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_230]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-24                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bi4",oParentObject)
return(i_retval)

define class tgsof_bi4 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Evento Importa Offerte selezionato (da GSOF_KIO)
    ND = this.oParentObject.w_ZoomDett.cCursor
    if USED(this.oParentObject.w_ZoomDett.cCursor)
      * --- Selezionato il Documento Master
      SELECT * FROM (ND) WHERE XCHK=1 INTO CURSOR ImpOff ORDER BY CPROWORD
      if USED("impOff")
        SELECT ImpOff
        if RECCOUNT()>0
          if Isalt()
            this.oParentObject.oParentObject.GSPR_MDO.NotifyEvent("ImportaOfferte")
          else
            this.oParentObject.oParentObject.GSOF_MDO.NotifyEvent("ImportaOfferte")
          endif
        endif
      endif
    endif
    if USED("impOff")
      SELECT ImpOff
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
