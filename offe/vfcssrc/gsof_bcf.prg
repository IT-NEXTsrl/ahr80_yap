* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bcf                                                        *
*              Copia allegati offerte                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-21                                                      *
* Last revis.: 2010-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bcf",oParentObject,m.pTIPOPE)
return(i_retval)

define class tgsof_bcf as StdBatch
  * --- Local variables
  pTIPOPE = space(1)
  w_Periodo = ctod("  /  /  ")
  w_Pat = space(254)
  w_numfile = 0
  w_CritPeri = space(1)
  w_Ok = .f.
  w_esiste = 0
  w_EsistDir = .f.
  w_ArcNom = space(1)
  w_RagSoc = space(40)
  * --- WorkFile variables
  PAR_OFFE_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia / Elimina Allegati (da GSOF_AAL)
    this.w_Periodo = this.oParentObject.w_ALDATREG
    do case
      case this.pTIPOPE="S"
        this.oParentObject.w_NOMFIL = left(getfile("*",ah_Msgformat("File allegato"))+space(254),254)
        this.oParentObject.w_NFILONLY = JUSTFNAME(this.oParentObject.w_NOMFIL)
        this.oParentObject.w_ALPATFIL = iif(this.oParentObject.w_ALTIPALL="L",alltrim(this.oParentObject.w_NOMFIL),alltrim(this.oParentObject.w_NFILONLY))
      case this.pTIPOPE="I"
        if this.oParentObject.w_ALTIPALL="F" 
          this.w_Pat = ""
          this.w_numfile = 0
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_EsistDir=directory(this.w_Pat)
          if this.w_EsistDir=.F.
            md (this.w_Pat)
          endif
          this.w_numfile = ADIR(w_ARRAY,this.w_Pat + "*" )
          this.w_Pat = this.w_Pat + this.oParentObject.w_ALPATFIL
          this.w_esiste = 0
          if this.w_numfile <> 0
            this.w_esiste = ASCAN(w_ARRAY,Upper(this.oParentObject.w_ALPATFIL))
          endif
          if NOT EMPTY(this.oParentObject.w_NOMFIL)
            if this.w_esiste = 0 
              * --- Copia direttamente
              copy file alltrim(this.oParentObject.w_NOMFIL) to alltrim(this.w_Pat)
            else
              this.w_Ok = AH_YesNo("Nella cartella %1 � gi� presente un file con lo stesso nome%0Si desidera sovrascriverlo?","",alltrim(this.w_Pat))
              if this.w_Ok = .T.
                copy file alltrim(this.oParentObject.w_NOMFIL) to alltrim(this.w_Pat)
              endif
            endif
          else
            AH_ErrorMsg("Nome file non definito")
          endif
        endif
      case this.pTIPOPE="D" AND this.oParentObject.w_ALTIPALL="F" AND NOT EMPTY(this.oParentObject.w_ALPATFIL)
        this.w_Pat = ""
        this.w_numfile = 0
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_PAT = ALLTRIM(this.w_PAT)
        if NOT EMPTY(this.w_PAT)
          this.w_PAT = ADDBS(this.w_PAT) + ALLTRIM(this.oParentObject.w_ALPATFIL)
          DELFIL = ALLTRIM(this.w_PAT)
          if FILE(DELFIL)
            DELETE FILE (DELFIL)
          endif
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Periodo
    * --- Calcolo nome directory memorizzazione allegati
    do case
      case this.oParentObject.w_ALTIPORI="O"
        * --- Read from PAR_OFFE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "POPATOFF,POPEROFF,POFLCAAR"+;
            " from "+i_cTable+" PAR_OFFE where ";
                +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            POPATOFF,POPEROFF,POFLCAAR;
            from (i_cTable) where;
                POCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_Pat = NVL(cp_ToDate(_read_.POPATOFF),cp_NullValue(_read_.POPATOFF))
          this.w_CritPeri = NVL(cp_ToDate(_read_.POPEROFF),cp_NullValue(_read_.POPEROFF))
          this.w_ArcNom = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if right(alltrim(this.w_Pat),1)="\"
          this.w_Pat = LEFT(ALLTRIM(this.w_Pat),LEN(ALLTRIM(this.w_Pat))-1)+"\" 
        else
          this.w_Pat = ALLTRIM(this.w_Pat)+"\" 
        endif
      case this.oParentObject.w_ALTIPORI="N"
        * --- Read from PAR_OFFE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "POPATALN,POPERALN,POFLCAAR"+;
            " from "+i_cTable+" PAR_OFFE where ";
                +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            POPATALN,POPERALN,POFLCAAR;
            from (i_cTable) where;
                POCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_Pat = NVL(cp_ToDate(_read_.POPATALN),cp_NullValue(_read_.POPATALN))
          this.w_CritPeri = NVL(cp_ToDate(_read_.POPERALN),cp_NullValue(_read_.POPERALN))
          this.w_ArcNom = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if right(alltrim(this.w_Pat),1)="\"
          this.w_Pat = LEFT(ALLTRIM(this.w_Pat),LEN(ALLTRIM(this.w_Pat))-1)+"\" 
        else
          this.w_Pat = ALLTRIM(this.w_Pat)+"\" 
        endif
    endcase
    if this.w_ArcNom="S"
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NODESCRI"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_ALRIFNOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NODESCRI;
          from (i_cTable) where;
              NOCODICE = this.oParentObject.w_ALRIFNOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RagSoc = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_Pat = alltrim(this.w_Pat)+alltrim(this.w_RagSoc)+"\" 
    else
      this.w_Pat = alltrim(this.w_Pat)
    endif
    do case
      case this.w_CritPeri = "M"
        * --- Raggruppamento per mesi
        this.w_Pat = this.w_Pat + right("00"+alltrim(str(month(this.w_Periodo),2,0)), 2) + str(year(this.w_Periodo),4,0)
      case this.w_CritPeri = "T"
        * --- Raggruppamento per trimestri
        do case
          case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 4
            this.w_Pat = this.w_Pat + "T1" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 3 .and. month(this.w_Periodo) < 7
            this.w_Pat = this.w_Pat + "T2" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 6 .and. month(this.w_Periodo) < 10
            this.w_Pat = this.w_Pat + "T3" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 9
            this.w_Pat = this.w_Pat + "T4" + str(year(this.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "Q"
        * --- Raggruppamento per quadrimestri
        do case
          case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 5
            this.w_Pat = this.w_Pat + "Q1" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 4 .and. month(this.w_Periodo) < 9
            this.w_Pat = this.w_Pat + "Q2" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 8
            this.w_Pat = this.w_Pat + "Q3" + str(year(this.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "S"
        * --- Raggruppamento per semestri
        this.w_Pat = this.w_Pat + iif( month(this.w_Periodo) >6 , "S2", "S1") + str(year(this.w_Periodo),4,0)
      case this.w_CritPeri = "A"
        * --- Raggruppamento per anni
        this.w_Pat = this.w_Pat + str(year(this.w_Periodo),4,0)
    endcase
    this.w_Pat = this.w_Pat+"\" 
  endproc


  proc Init(oParentObject,pTIPOPE)
    this.pTIPOPE=pTIPOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_OFFE'
    this.cWorkTables[2]='OFF_NOMI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE"
endproc
