* --- Container for functions
* --- START OFCHKCLI
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ofchkcli                                                        *
*              Controllo univocit� cliente                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-23                                                      *
* Last revis.: 2003-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func ofchkcli
param pCODICE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODCLI
  m.w_CODCLI=space(15)
  private w_APPO
  m.w_APPO=.f.
* --- WorkFile variables
  private OFF_NOMI_idx
  OFF_NOMI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "ofchkcli"
if vartype(__ofchkcli_hook__)='O'
  __ofchkcli_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'ofchkcli('+Transform(pCODICE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ofchkcli')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if ofchkcli_OpenTables()
  ofchkcli_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'ofchkcli('+Transform(pCODICE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ofchkcli')
Endif
*--- Activity log
if vartype(__ofchkcli_hook__)='O'
  __ofchkcli_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure ofchkcli_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Funzione per il controllo di univocit� del codice cliente
  m.w_APPO = .F.
  m.w_CODCLI = ALLTRIM(m.pCODICE)
  * --- Read from OFF_NOMI
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[OFF_NOMI_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[OFF_NOMI_idx,2],.t.,OFF_NOMI_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "NOCODCLI"+;
      " from "+i_cTable+" OFF_NOMI where ";
          +"NOCODCLI = "+cp_ToStrODBC(m.w_CODCLI);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      NOCODCLI;
      from (i_cTable) where;
          NOCODCLI = m.w_CODCLI;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_CODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if i_Rows=0
    m.w_APPO = .T.
  endif
  i_retcode = 'stop'
  i_retval = m.w_APPO
  return
endproc


  function ofchkcli_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='OFF_NOMI'
    return(cp_OpenFuncTables(1))
* --- END OFCHKCLI
* --- START OFCHKMOD
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ofchkmod                                                        *
*              Verifica congruit� modello                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-29                                                      *
* Last revis.: 2008-03-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func ofchkmod
param pCodMod,pDatDoc,pCodUte,pCodPri,pCodGrn,pCodOri,pCodZon,pCodLin

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_OK
  m.w_OK=.f.
  private w_DATVAL
  m.w_DATVAL=ctod("  /  /  ")
  private w_DATOBS
  m.w_DATOBS=ctod("  /  /  ")
  private w_CODUTE
  m.w_CODUTE=0
  private w_GRPUTE
  m.w_GRPUTE=0
  private w_CODGRN
  m.w_CODGRN=space(5)
  private w_CODORI
  m.w_CODORI=space(5)
  private w_CODZON
  m.w_CODZON=space(3)
  private w_CODLIN
  m.w_CODLIN=space(3)
  private w_CODPRI
  m.w_CODPRI=space(5)
  private w_NUMINI
  m.w_NUMINI=0
  private w_NUMFIN
  m.w_NUMFIN=0
* --- WorkFile variables
  private MOD_OFFE_idx
  MOD_OFFE_idx=0
  private GRU_PRIO_idx
  GRU_PRIO_idx=0
  private CPUSRGRP_idx
  CPUSRGRP_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "ofchkmod"
if vartype(__ofchkmod_hook__)='O'
  __ofchkmod_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'ofchkmod('+Transform(pCodMod)+','+Transform(pDatDoc)+','+Transform(pCodUte)+','+Transform(pCodPri)+','+Transform(pCodGrn)+','+Transform(pCodOri)+','+Transform(pCodZon)+','+Transform(pCodLin)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ofchkmod')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if ofchkmod_OpenTables()
  ofchkmod_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_CPUSRGRP')
  use in _Curs_CPUSRGRP
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'ofchkmod('+Transform(pCodMod)+','+Transform(pDatDoc)+','+Transform(pCodUte)+','+Transform(pCodPri)+','+Transform(pCodGrn)+','+Transform(pCodOri)+','+Transform(pCodZon)+','+Transform(pCodLin)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ofchkmod')
Endif
*--- Activity log
if vartype(__ofchkmod_hook__)='O'
  __ofchkmod_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure ofchkmod_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Check Verifica congruita' Modello Offerta (da  GSOF_AOF)
  m.w_OK = .T.
  if NOT EMPTY(m.pCodMod)
    m.w_DATVAL = cp_CharToDate("  -  -  ")
    m.w_DATOBS = cp_CharToDate("  -  -  ")
    m.w_CODUTE = 0
    m.w_CODGRN = SPACE(5)
    m.w_CODORI = SPACE(5)
    m.w_CODZON = SPACE(3)
    m.w_CODLIN = SPACE(3)
    m.w_CODPRI = SPACE(5)
    m.w_NUMINI = 0
    m.w_NUMFIN = 0
    m.w_GRPUTE = 0
    * --- Read from MOD_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[MOD_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[MOD_OFFE_idx,2],.t.,MOD_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MODATVAL,MODATOBS,MOCODUTE,MOCODGRN,MOCODORI,MOCODZON,MOCODLIN,MOCODPRI,MOGRPUTE"+;
        " from "+i_cTable+" MOD_OFFE where ";
            +"MOCODICE = "+cp_ToStrODBC(m.pCodMod);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MODATVAL,MODATOBS,MOCODUTE,MOCODGRN,MOCODORI,MOCODZON,MOCODLIN,MOCODPRI,MOGRPUTE;
        from (i_cTable) where;
            MOCODICE = m.pCodMod;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_DATVAL = NVL(cp_ToDate(_read_.MODATVAL),cp_NullValue(_read_.MODATVAL))
      m.w_DATOBS = NVL(cp_ToDate(_read_.MODATOBS),cp_NullValue(_read_.MODATOBS))
      m.w_CODUTE = NVL(cp_ToDate(_read_.MOCODUTE),cp_NullValue(_read_.MOCODUTE))
      m.w_CODGRN = NVL(cp_ToDate(_read_.MOCODGRN),cp_NullValue(_read_.MOCODGRN))
      m.w_CODORI = NVL(cp_ToDate(_read_.MOCODORI),cp_NullValue(_read_.MOCODORI))
      m.w_CODZON = NVL(cp_ToDate(_read_.MOCODZON),cp_NullValue(_read_.MOCODZON))
      m.w_CODLIN = NVL(cp_ToDate(_read_.MOCODLIN),cp_NullValue(_read_.MOCODLIN))
      m.w_CODPRI = NVL(cp_ToDate(_read_.MOCODPRI),cp_NullValue(_read_.MOCODPRI))
      m.w_GRPUTE = NVL(cp_ToDate(_read_.MOGRPUTE),cp_NullValue(_read_.MOGRPUTE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case NOT (EMPTY(m.w_DATVAL) OR m.w_DATVAL<=m.pDatDoc)
        ah_errormsg("Data inizio validit� modello offerta incongruente")
        m.w_OK = .F.
      case NOT (EMPTY(m.w_DATOBS) OR m.w_DATOBS>m.pDatDoc)
        ah_errormsg("Data fine validit� modello offerta incongruente")
        m.w_OK = .F.
      case NOT (EMPTY(m.pCodUte) OR EMPTY(m.w_CODUTE) OR m.pCodUte=m.w_CODUTE)
        ah_errormsg("Codice utente modello offerta incongruente")
        m.w_OK = .F.
      case NOT (EMPTY(m.pCodGrn) OR EMPTY(m.w_CODGRN) OR m.pCodGrn=m.w_CODGRN)
        ah_errormsg("Codice gruppo nominativi modello offerta incongruente")
        m.w_OK = .F.
      case NOT (EMPTY(m.pCodOri) OR EMPTY(m.w_CODORI) OR m.pCodOri=m.w_CODORI)
        ah_errormsg("Codice origine modello offerta incongruente")
        m.w_OK = .F.
      case NOT (EMPTY(m.pCodZon) OR EMPTY(m.w_CODZON) OR m.pCodZon=m.w_CODZON)
        ah_errormsg("Codice zona modello offerta incongruente")
        m.w_OK = .F.
      case NOT (EMPTY(m.pCodLin) OR EMPTY(m.w_CODLIN) OR m.pCodLin=m.w_CODLIN)
        ah_errormsg("Codice lingua modello offerta incongruente")
        m.w_OK = .F.
    endcase
    if m.w_OK=.T. AND NOT EMPTY(m.w_CODPRI)
      * --- Read from GRU_PRIO
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[GRU_PRIO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[GRU_PRIO_idx,2],.t.,GRU_PRIO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "GPNUMINI,GPNUMFIN"+;
          " from "+i_cTable+" GRU_PRIO where ";
              +"GPCODICE = "+cp_ToStrODBC(m.w_CODPRI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          GPNUMINI,GPNUMFIN;
          from (i_cTable) where;
              GPCODICE = m.w_CODPRI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_NUMINI = NVL(cp_ToDate(_read_.GPNUMINI),cp_NullValue(_read_.GPNUMINI))
        m.w_NUMFIN = NVL(cp_ToDate(_read_.GPNUMFIN),cp_NullValue(_read_.GPNUMFIN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do case
        case NOT (EMPTY(m.w_NUMINI) OR m.w_NUMINI<=m.pCodPri)
          ah_errormsg("Livello iniziale priorit� modello offerta incongruente")
          m.w_OK = .F.
        case NOT (EMPTY(m.w_NUMFIN) OR m.w_NUMFIN>=m.pCodPri)
          ah_errormsg("Livello finale priorit� modello offerta incongruente")
          m.w_OK = .F.
      endcase
    endif
    if m.w_OK=.T. AND NOT EMPTY(m.pCodUte) AND NOT EMPTY(m.w_GRPUTE)
      m.w_OK = .F.
      * --- Select from CPUSRGRP
      i_nConn=i_TableProp[CPUSRGRP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[CPUSRGRP_idx,2],.t.,CPUSRGRP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" CPUSRGRP ";
            +" where USERCODE="+cp_ToStrODBC(m.pCodUte)+"";
             ,"_Curs_CPUSRGRP")
      else
        select * from (i_cTable);
         where USERCODE=m.pCodUte;
          into cursor _Curs_CPUSRGRP
      endif
      if used('_Curs_CPUSRGRP')
        select _Curs_CPUSRGRP
        locate for 1=1
        do while not(eof())
        if m.w_GRPUTE = NVL(_Curs_CPUSRGRP.GROUPCODE, 0)
          m.w_OK = .T.
        endif
          select _Curs_CPUSRGRP
          continue
        enddo
        use
      endif
      if m.w_OK=.F. 
        ah_errormsg("Gruppo utente priorit� modello offerta incongruente")
        m.w_OK = .F.
      endif
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_OK
  return
endproc


  function ofchkmod_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='MOD_OFFE'
    i_cWorkTables[2]='GRU_PRIO'
    i_cWorkTables[3]='CPUSRGRP'
    return(cp_OpenFuncTables(3))
* --- END OFCHKMOD
* --- START OFOPENFILE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ofopenfile                                                      *
*              Open file (doc, PDF, xls, etc.)                                 *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-20                                                      *
* Last revis.: 2010-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func ofopenfile
param cLink,cSerial,cTipori,cRifNom

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
* --- WorkFile variables
  private ALL_EGAT_idx
  ALL_EGAT_idx=0
  private PAR_OFFE_idx
  PAR_OFFE_idx=0
  private OFF_NOMI_idx
  OFF_NOMI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "ofopenfile"
if vartype(__ofopenfile_hook__)='O'
  __ofopenfile_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'ofopenfile('+Transform(cLink)+','+Transform(cSerial)+','+Transform(cTipori)+','+Transform(cRifNom)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ofopenfile')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if ofopenfile_OpenTables()
  ofopenfile_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'ofopenfile('+Transform(cLink)+','+Transform(cSerial)+','+Transform(cTipori)+','+Transform(cRifNom)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ofopenfile')
Endif
*--- Activity log
if vartype(__ofopenfile_hook__)='O'
  __ofopenfile_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure ofopenfile_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  stapdf( ofsrcfile( m.cLink, m.cSerial, m.cTipori, m.cRifNom), "Open", "", .T.)
endproc


  function ofopenfile_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='ALL_EGAT'
    i_cWorkTables[2]='PAR_OFFE'
    i_cWorkTables[3]='OFF_NOMI'
    return(cp_OpenFuncTables(3))
* --- END OFOPENFILE
* --- START OFSRCFILE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: ofsrcfile                                                       *
*              Interrogazione nome file completo di path allegato              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-30                                                      *
* Last revis.: 2010-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func ofsrcfile
param cLink,cSerial,cTipori,cRifNom

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_PERIODO
  m.w_PERIODO=ctod("  /  /  ")
  private w_CRITPERI
  m.w_CRITPERI=space(1)
  private w_ARCHNOMI
  m.w_ARCHNOMI=space(1)
  private w_PATHFILE
  m.w_PATHFILE=space(254)
  private w_NODESCRI
  m.w_NODESCRI=space(40)
  private w_ALTIPALL
  m.w_ALTIPALL=space(1)
  private w_RETVAL
  m.w_RETVAL=space(254)
* --- WorkFile variables
  private ALL_EGAT_idx
  ALL_EGAT_idx=0
  private PAR_OFFE_idx
  PAR_OFFE_idx=0
  private OFF_NOMI_idx
  OFF_NOMI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "ofsrcfile"
if vartype(__ofsrcfile_hook__)='O'
  __ofsrcfile_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'ofsrcfile('+Transform(cLink)+','+Transform(cSerial)+','+Transform(cTipori)+','+Transform(cRifNom)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ofsrcfile')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if ofsrcfile_OpenTables()
  ofsrcfile_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'ofsrcfile('+Transform(cLink)+','+Transform(cSerial)+','+Transform(cTipori)+','+Transform(cRifNom)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'ofsrcfile')
Endif
*--- Activity log
if vartype(__ofsrcfile_hook__)='O'
  __ofsrcfile_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure ofsrcfile_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato il seriale dell'allegato restituisce il nome del file comprensivo del percorso assoluto
  m.cTipori = IIF(VARTYPE(m.cTipori)="C", m.cTipori, "O")
  m.cRifNom = IIF(VARTYPE(m.cRifNom)="C", m.cRifNom, "")
  * --- Read from ALL_EGAT
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[ALL_EGAT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ALL_EGAT_idx,2],.t.,ALL_EGAT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "ALTIPALL,ALDATREG,ALRIFNOM"+;
      " from "+i_cTable+" ALL_EGAT where ";
          +"ALSERIAL = "+cp_ToStrODBC(m.cSerial);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      ALTIPALL,ALDATREG,ALRIFNOM;
      from (i_cTable) where;
          ALSERIAL = m.cSerial;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_ALTIPALL = NVL(cp_ToDate(_read_.ALTIPALL),cp_NullValue(_read_.ALTIPALL))
    m.w_PERIODO = NVL(cp_ToDate(_read_.ALDATREG),cp_NullValue(_read_.ALDATREG))
    w_ALRIFNOM = NVL(cp_ToDate(_read_.ALRIFNOM),cp_NullValue(_read_.ALRIFNOM))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if m.w_ALTIPALL="F"
    do case
      case m.cTipori="O"
        * --- Read from PAR_OFFE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[PAR_OFFE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[PAR_OFFE_idx,2],.t.,PAR_OFFE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "POPATOFF,POPEROFF,POFLCAAR"+;
            " from "+i_cTable+" PAR_OFFE where ";
                +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            POPATOFF,POPEROFF,POFLCAAR;
            from (i_cTable) where;
                POCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          m.w_PATHFILE = NVL(cp_ToDate(_read_.POPATOFF),cp_NullValue(_read_.POPATOFF))
          m.w_CRITPERI = NVL(cp_ToDate(_read_.POPEROFF),cp_NullValue(_read_.POPEROFF))
          m.w_ARCHNOMI = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        m.w_PATHFILE = ADDBS(ALLTRIM(m.w_PATHFILE))
      case m.cTipori="N"
        * --- Read from PAR_OFFE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[PAR_OFFE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[PAR_OFFE_idx,2],.t.,PAR_OFFE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "POPATALN,POPERALN,POFLCAAR"+;
            " from "+i_cTable+" PAR_OFFE where ";
                +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            POPATALN,POPERALN,POFLCAAR;
            from (i_cTable) where;
                POCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          m.w_PATHFILE = NVL(cp_ToDate(_read_.POPATALN),cp_NullValue(_read_.POPATALN))
          m.w_CRITPERI = NVL(cp_ToDate(_read_.POPERALN),cp_NullValue(_read_.POPERALN))
          m.w_ARCHNOMI = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        m.w_PATHFILE = ADDBS(ALLTRIM(m.w_PATHFILE))
    endcase
    * --- Controllo se selezionato Archiviazione per Nominativo
    if m.w_ARCHNOMI="S"
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[OFF_NOMI_idx,2],.t.,OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NODESCRI"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOCODICE = "+cp_ToStrODBC(w_ALRIFNOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NODESCRI;
          from (i_cTable) where;
              NOCODICE = w_ALRIFNOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_NODESCRI = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      m.w_PATHFILE = ADDBS(ALLTRIM(m.w_PATHFILE)+ALLTRIM(m.w_NODESCRI))
    else
      m.w_PATHFILE = ADDBS(alltrim(m.w_PATHFILE))
    endif
    do case
      case m.w_CRITPERI = "M"
        * --- Raggruppamento per mesi
        m.w_PATHFILE = m.w_PATHFILE + right("00"+alltrim(str(month(m.w_PERIODO),2,0)), 2) + str(year(m.w_PERIODO),4,0)
      case m.w_CRITPERI = "T"
        * --- Raggruppamento per trimestri
        do case
          case month(m.w_PERIODO) > 0 AND month(m.w_PERIODO) < 4
            m.w_PATHFILE = m.w_PATHFILE + "T1" + str(year(m.w_PERIODO),4,0)
          case month(m.w_PERIODO) > 3 AND month(m.w_PERIODO) < 7
            m.w_PATHFILE = m.w_PATHFILE + "T2" + str(year(m.w_PERIODO),4,0)
          case month(m.w_PERIODO) > 6 AND month(m.w_PERIODO) < 10
            m.w_PATHFILE = m.w_PATHFILE + "T3" + str(year(m.w_PERIODO),4,0)
          case month(m.w_PERIODO) > 9
            m.w_PATHFILE = m.w_PATHFILE + "T4" + str(year(m.w_PERIODO),4,0)
        endcase
      case m.w_CRITPERI = "Q"
        * --- Raggruppamento per quadrimestri
        do case
          case month(m.w_PERIODO) > 0 AND month(m.w_PERIODO) < 5
            m.w_PATHFILE = m.w_PATHFILE + "Q1" + str(year(m.w_PERIODO),4,0)
          case month(m.w_PERIODO) > 4 AND month(m.w_PERIODO) < 9
            m.w_PATHFILE = m.w_PATHFILE + "Q2" + str(year(m.w_PERIODO),4,0)
          case month(m.w_PERIODO) > 8
            m.w_PATHFILE = m.w_PATHFILE + "Q3" + str(year(m.w_PERIODO),4,0)
        endcase
      case m.w_CRITPERI = "S"
        * --- Raggruppamento per semestri
        m.w_PATHFILE = m.w_PATHFILE + iif( month(m.w_PERIODO) >6 , "S2", "S1") + str(year(m.w_PERIODO),4,0)
      case m.w_CRITPERI = "A"
        * --- Raggruppamento per anni
        m.w_PATHFILE = m.w_PATHFILE + str(year(m.w_PERIODO),4,0)
    endcase
    m.w_RETVAL = ADDBS(ALLTRIM(m.w_PATHFILE)) + m.cLink
  else
    m.w_RETVAL = ALLTRIM(m.cLink)
  endif
  i_retcode = 'stop'
  i_retval = m.w_RETVAL
  return
endproc


  function ofsrcfile_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='ALL_EGAT'
    i_cWorkTables[2]='PAR_OFFE'
    i_cWorkTables[3]='OFF_NOMI'
    return(cp_OpenFuncTables(3))
* --- END OFSRCFILE
