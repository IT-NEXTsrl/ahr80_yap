* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_kim                                                        *
*              Importa kit promozionali                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2008-06-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_kim",oParentObject))

* --- Class definition
define class tgsof_kim as StdForm
  Top    = 0
  Left   = 16

  * --- Standard Properties
  Width  = 791
  Height = 497
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-06-27"
  HelpContextID=1431191
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  TIT_SEZI_IDX = 0
  SOT_SEZI_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsof_kim"
  cComment = "Importa kit promozionali"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_OFDATDOC = ctod('  /  /  ')
  w_OFCODVAL = space(3)
  w_OFCODUTE = 0
  w_NCODGRN = space(5)
  w_NCODORI = space(5)
  w_NCODPRF = space(5)
  w_NCODZON = space(3)
  w_OFCODAGE = space(5)
  w_OFNUMPRI = 0
  w_NCODPRI = 0
  w_OBTEST = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_CODUTE = 0
  w_CODGRN = space(5)
  w_CODORI = space(5)
  w_CODPRF = space(5)
  w_CODZON = space(3)
  w_CODAGE = space(5)
  w_NUMPRI = 0
  w_NOMPRI = 0
  w_FILART = space(20)
  o_FILART = space(20)
  w_DESFIL = space(40)
  w_CODART = space(20)
  w_FILGRU = space(5)
  o_FILGRU = space(5)
  w_DESGRU = space(35)
  w_FILSOT = space(5)
  w_DESSOT = space(35)
  w_FILDES = space(35)
  w_CODKIT = space(15)
  w_SERIAL = space(10)
  w_SELEZI = space(1)
  w_UMCAL = space(14)
  w_ZoomMast = .NULL.
  w_ZoomDett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_kimPag1","gsof_kim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Documenti")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILART_1_21
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMast = this.oPgFrm.Pages(1).oPag.ZoomMast
    this.w_ZoomDett = this.oPgFrm.Pages(1).oPag.ZoomDett
    DoDefault()
    proc Destroy()
      this.w_ZoomMast = .NULL.
      this.w_ZoomDett = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='TIT_SEZI'
    this.cWorkTables[3]='SOT_SEZI'
    this.cWorkTables[4]='ART_ICOL'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OFDATDOC=ctod("  /  /  ")
      .w_OFCODVAL=space(3)
      .w_OFCODUTE=0
      .w_NCODGRN=space(5)
      .w_NCODORI=space(5)
      .w_NCODPRF=space(5)
      .w_NCODZON=space(3)
      .w_OFCODAGE=space(5)
      .w_OFNUMPRI=0
      .w_NCODPRI=0
      .w_OBTEST=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_CODUTE=0
      .w_CODGRN=space(5)
      .w_CODORI=space(5)
      .w_CODPRF=space(5)
      .w_CODZON=space(3)
      .w_CODAGE=space(5)
      .w_NUMPRI=0
      .w_NOMPRI=0
      .w_FILART=space(20)
      .w_DESFIL=space(40)
      .w_CODART=space(20)
      .w_FILGRU=space(5)
      .w_DESGRU=space(35)
      .w_FILSOT=space(5)
      .w_DESSOT=space(35)
      .w_FILDES=space(35)
      .w_CODKIT=space(15)
      .w_SERIAL=space(10)
      .w_SELEZI=space(1)
      .w_UMCAL=space(14)
      .w_OFDATDOC=oParentObject.w_OFDATDOC
      .w_OFCODVAL=oParentObject.w_OFCODVAL
      .w_OFCODUTE=oParentObject.w_OFCODUTE
      .w_NCODGRN=oParentObject.w_NCODGRN
      .w_NCODORI=oParentObject.w_NCODORI
      .w_NCODPRF=oParentObject.w_NCODPRF
      .w_NCODZON=oParentObject.w_NCODZON
      .w_OFCODAGE=oParentObject.w_OFCODAGE
      .w_OFNUMPRI=oParentObject.w_OFNUMPRI
      .w_NCODPRI=oParentObject.w_NCODPRI
          .DoRTCalc(1,10,.f.)
        .w_OBTEST = .w_OFDATDOC
        .w_CODVAL = .w_OFCODVAL
        .w_CODUTE = .w_OFCODUTE
        .w_CODGRN = .w_NCODGRN
        .w_CODORI = .w_NCODORI
        .w_CODPRF = .w_NCODPRF
        .w_CODZON = .w_NCODZON
        .w_CODAGE = .w_OFCODAGE
        .w_NUMPRI = .w_OFNUMPRI
        .w_NOMPRI = .w_NCODPRI
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_FILART))
          .link_1_21('Full')
        endif
        .DoRTCalc(22,23,.f.)
        if not(empty(.w_CODART))
          .link_1_24('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_FILGRU))
          .link_1_25('Full')
        endif
        .DoRTCalc(25,26,.f.)
        if not(empty(.w_FILSOT))
          .link_1_27('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomMast.Calculate()
          .DoRTCalc(27,28,.f.)
        .w_CODKIT = NVL(.w_ZoomMast.getVar('KPCODKIT'), SPACE(15))
        .w_SERIAL = IIF(EMPTY(NVL(.w_CODKIT,' ')),'zzzyyyzzkk', .w_CODKIT)
      .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .w_SELEZI = 'S'
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
    endwith
    this.DoRTCalc(32,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_OFDATDOC=.w_OFDATDOC
      .oParentObject.w_OFCODVAL=.w_OFCODVAL
      .oParentObject.w_OFCODUTE=.w_OFCODUTE
      .oParentObject.w_NCODGRN=.w_NCODGRN
      .oParentObject.w_NCODORI=.w_NCODORI
      .oParentObject.w_NCODPRF=.w_NCODPRF
      .oParentObject.w_NCODZON=.w_NCODZON
      .oParentObject.w_OFCODAGE=.w_OFCODAGE
      .oParentObject.w_OFNUMPRI=.w_OFNUMPRI
      .oParentObject.w_NCODPRI=.w_NCODPRI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
            .w_OBTEST = .w_OFDATDOC
            .w_CODVAL = .w_OFCODVAL
            .w_CODUTE = .w_OFCODUTE
            .w_CODGRN = .w_NCODGRN
            .w_CODORI = .w_NCODORI
            .w_CODPRF = .w_NCODPRF
            .w_CODZON = .w_NCODZON
            .w_CODAGE = .w_OFCODAGE
            .w_NUMPRI = .w_OFNUMPRI
            .w_NOMPRI = .w_NCODPRI
        .DoRTCalc(21,22,.t.)
        if .o_FILART<>.w_FILART
          .link_1_24('Full')
        endif
        if .o_FILART<>.w_FILART
          .link_1_25('Full')
        endif
        .DoRTCalc(25,25,.t.)
        if .o_FILGRU<>.w_FILGRU.or. .o_FILART<>.w_FILART
          .link_1_27('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .DoRTCalc(27,28,.t.)
            .w_CODKIT = NVL(.w_ZoomMast.getVar('KPCODKIT'), SPACE(15))
            .w_SERIAL = IIF(EMPTY(NVL(.w_CODKIT,' ')),'zzzyyyzzkk', .w_CODKIT)
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(31,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFILSOT_1_27.enabled = this.oPgFrm.Page1.oPag.oFILSOT_1_27.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomMast.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDett.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FILART
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_FILART)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_FILART))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILART)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILART) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oFILART_1_21'),i_cWhere,'GSMA_BZA',"Elenco articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_FILART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_FILART)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILART = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIL = NVL(_Link_.CADESART,space(40))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_FILART = space(20)
      endif
      this.w_DESFIL = space(40)
      this.w_CODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARCODGRU,ARCODSOT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARCODGRU,ARCODSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_FILGRU = NVL(_Link_.ARCODGRU,space(5))
      this.w_FILSOT = NVL(_Link_.ARCODSOT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_FILGRU = space(5)
      this.w_FILSOT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILGRU
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIT_SEZI_IDX,3]
    i_lTable = "TIT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2], .t., this.TIT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATS',True,'TIT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TSCODICE like "+cp_ToStrODBC(trim(this.w_FILGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TSCODICE',trim(this.w_FILGRU))
          select TSCODICE,TSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILGRU)==trim(_Link_.TSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILGRU) and !this.bDontReportError
            deferred_cp_zoom('TIT_SEZI','*','TSCODICE',cp_AbsName(oSource.parent,'oFILGRU_1_25'),i_cWhere,'GSOF_ATS',"Codici gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',oSource.xKey(1))
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(this.w_FILGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',this.w_FILGRU)
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILGRU = NVL(_Link_.TSCODICE,space(5))
      this.w_DESGRU = NVL(_Link_.TSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FILGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.TSCODICE,1)
      cp_ShowWarn(i_cKey,this.TIT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILSOT
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_lTable = "SOT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2], .t., this.SOT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILSOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ASF',True,'SOT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SGCODSOT like "+cp_ToStrODBC(trim(this.w_FILSOT)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_FILGRU);

          i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SGCODGRU,SGCODSOT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SGCODGRU',this.w_FILGRU;
                     ,'SGCODSOT',trim(this.w_FILSOT))
          select SGCODGRU,SGCODSOT,SGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SGCODGRU,SGCODSOT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILSOT)==trim(_Link_.SGCODSOT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILSOT) and !this.bDontReportError
            deferred_cp_zoom('SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(oSource.parent,'oFILSOT_1_27'),i_cWhere,'GSOF_ASF',"Codici sottogruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FILGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SGCODGRU="+cp_ToStrODBC(this.w_FILGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',oSource.xKey(1);
                       ,'SGCODSOT',oSource.xKey(2))
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILSOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(this.w_FILSOT);
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_FILGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',this.w_FILGRU;
                       ,'SGCODSOT',this.w_FILSOT)
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILSOT = NVL(_Link_.SGCODSOT,space(5))
      this.w_DESSOT = NVL(_Link_.SGDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FILSOT = space(5)
      endif
      this.w_DESSOT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.SGCODGRU,1)+'\'+cp_ToStr(_Link_.SGCODSOT,1)
      cp_ShowWarn(i_cKey,this.SOT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILSOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFILART_1_21.value==this.w_FILART)
      this.oPgFrm.Page1.oPag.oFILART_1_21.value=this.w_FILART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIL_1_22.value==this.w_DESFIL)
      this.oPgFrm.Page1.oPag.oDESFIL_1_22.value=this.w_DESFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oFILGRU_1_25.value==this.w_FILGRU)
      this.oPgFrm.Page1.oPag.oFILGRU_1_25.value=this.w_FILGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_26.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_26.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oFILSOT_1_27.value==this.w_FILSOT)
      this.oPgFrm.Page1.oPag.oFILSOT_1_27.value=this.w_FILSOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSOT_1_28.value==this.w_DESSOT)
      this.oPgFrm.Page1.oPag.oDESSOT_1_28.value=this.w_DESSOT
    endif
    if not(this.oPgFrm.Page1.oPag.oFILDES_1_29.value==this.w_FILDES)
      this.oPgFrm.Page1.oPag.oFILDES_1_29.value=this.w_FILDES
    endif
    if not(this.oPgFrm.Page1.oPag.oCODKIT_1_31.value==this.w_CODKIT)
      this.oPgFrm.Page1.oPag.oCODKIT_1_31.value=this.w_CODKIT
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_34.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_34.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsof_kim
      * --- Controlli Finali
      this.NotifyEvent('ControlliFinali')
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FILART = this.w_FILART
    this.o_FILGRU = this.w_FILGRU
    return

enddefine

* --- Define pages as container
define class tgsof_kimPag1 as StdContainer
  Width  = 787
  height = 497
  stdWidth  = 787
  stdheight = 497
  resizeXpos=577
  resizeYpos=287
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFILART_1_21 as StdField with uid="MOXWBWUSAC",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FILART", cQueryName = "FILART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Filtra i kit in cui l'articolo selezionato � presente",;
    HelpContextID = 109321130,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=95, Top=22, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_FILART"

  func oFILART_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILART_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILART_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oFILART_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'',this.parent.oContained
  endproc
  proc oFILART_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_FILART
     i_obj.ecpSave()
  endproc

  add object oDESFIL_1_22 as StdField with uid="GSHQEZYMST",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESFIL", cQueryName = "DESFIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 252620746,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=264, Top=22, InputMask=replicate('X',40)


  add object oBtn_1_23 as StdButton with uid="NUPAKQWDAI",left=726, top=6, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 90082026;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        do GSOF_BIM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFILGRU_1_25 as StdField with uid="IMYLTTXKDW",rtseq=24,rtrep=.f.,;
    cFormVar = "w_FILGRU", cQueryName = "FILGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Filtra i kit in cui il gruppo selezionato � presente (spazio=tutti)",;
    HelpContextID = 92150698,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=95, Top=54, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIT_SEZI", cZoomOnZoom="GSOF_ATS", oKey_1_1="TSCODICE", oKey_1_2="this.w_FILGRU"

  func oFILGRU_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
      if .not. empty(.w_FILSOT)
        bRes2=.link_1_27('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oFILGRU_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILGRU_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIT_SEZI','*','TSCODICE',cp_AbsName(this.parent,'oFILGRU_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATS',"Codici gruppi",'',this.parent.oContained
  endproc
  proc oFILGRU_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TSCODICE=this.parent.oContained.w_FILGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_26 as StdField with uid="HCSJHXSEIE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 92123082,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=162, Top=54, InputMask=replicate('X',35)

  add object oFILSOT_1_27 as StdField with uid="RYBREGPWOS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_FILSOT", cQueryName = "FILSOT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Filtra i kit in cui il sottogruppo � presente (spazio=tutti)",;
    HelpContextID = 111287210,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=471, Top=54, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SOT_SEZI", cZoomOnZoom="GSOF_ASF", oKey_1_1="SGCODGRU", oKey_1_2="this.w_FILGRU", oKey_2_1="SGCODSOT", oKey_2_2="this.w_FILSOT"

  func oFILSOT_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_FILGRU))
    endwith
   endif
  endfunc

  func oFILSOT_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILSOT_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILSOT_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SOT_SEZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStrODBC(this.Parent.oContained.w_FILGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStr(this.Parent.oContained.w_FILGRU)
    endif
    do cp_zoom with 'SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(this.parent,'oFILSOT_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ASF',"Codici sottogruppi",'',this.parent.oContained
  endproc
  proc oFILSOT_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ASF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SGCODGRU=w_FILGRU
     i_obj.w_SGCODSOT=this.parent.oContained.w_FILSOT
     i_obj.ecpSave()
  endproc

  add object oDESSOT_1_28 as StdField with uid="FCHWRPBFGI",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESSOT", cQueryName = "DESSOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 111259594,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=538, Top=54, InputMask=replicate('X',35)

  add object oFILDES_1_29 as StdField with uid="IZZIUZBDLM",rtseq=28,rtrep=.f.,;
    cFormVar = "w_FILDES", cQueryName = "FILDES",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale filtro su descrizione kit",;
    HelpContextID = 139533226,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=95, Top=81, InputMask=replicate('X',35)


  add object ZoomMast as cp_zoombox with uid="VJRPDOWVIW",left=-3, top=124, width=269,height=318,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="KITMPROM",cZoomFile="GSOFMKIT",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 89006618

  add object oCODKIT_1_31 as StdField with uid="GWTJAQXOOC",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODKIT", cQueryName = "CODKIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del kit selezionato",;
    HelpContextID = 118134234,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=116, Top=448, InputMask=replicate('X',15)


  add object ZoomDett as cp_szoombox with uid="NEMWPYAYDC",left=257, top=124, width=528,height=318,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ART_ICOL",cZoomFile="GSOFDKIT",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.f.,cMenuFile="",cZoomOnZoom="GSMA_BZA",bAdvOptions=.t.,;
    cEvent = "CalcRig",;
    nPag=1;
    , HelpContextID = 89006618

  add object oSELEZI_1_34 as StdRadio with uid="VNICEXLLTR",rtseq=31,rtrep=.f.,left=261, top=448, width=157,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_34.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 16785114
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 16785114
      this.Buttons(2).Top=15
      this.SetAll("Width",155)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_34.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_34.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_37 as StdButton with uid="DYTBZCRAGX",left=678, top=446, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 1451750;
    , tabStop=.f.,Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="GPQALFSGAA",left=730, top=446, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 212719354;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_39 as cp_runprogram with uid="FHFZMDWAGY",left=0, top=502, width=240,height=19,;
    caption='GSOF_BI1(SEL)',;
   bGlobalFont=.t.,;
    prg="GSOF_BI1('SEL')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 187810839


  add object oObj_1_40 as cp_runprogram with uid="PWUVEHENLH",left=0, top=545, width=240,height=19,;
    caption='GSOF_BIM',;
   bGlobalFont=.t.,;
    prg="GSOF_BIM",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 139534003


  add object oObj_1_42 as cp_runprogram with uid="YHDJKTNFOV",left=0, top=566, width=240,height=19,;
    caption='GSOF_BI2',;
   bGlobalFont=.t.,;
    prg="GSOF_BI2",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 139533976


  add object oObj_1_43 as cp_runprogram with uid="JDFLXSDHRU",left=0, top=524, width=253,height=19,;
    caption='GSOF_BI1(BQD)',;
   bGlobalFont=.t.,;
    prg="GSOF_BI1('BQD')",;
    cEvent = "ZOOMDETT after query",;
    nPag=1;
    , HelpContextID = 187331351

  add object oStr_1_35 as StdString with uid="BNCNHVKGYN",Visible=.t., Left=4, Top=106,;
    Alignment=0, Width=196, Height=15,;
    Caption="Elenco kit promozionali"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="DCXZNKWDRF",Visible=.t., Left=266, Top=106,;
    Alignment=0, Width=109, Height=15,;
    Caption="Dettaglio righe"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="UPFFWQYPNA",Visible=.t., Left=6, Top=448,;
    Alignment=1, Width=108, Height=18,;
    Caption="Kit selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="JLPTGYJBLO",Visible=.t., Left=587, Top=105,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Giallo)"    , ForeColor=RGB(255,255,0), BackColor=RGB(255,255,0), BackStyle=1, BorderStyle=1, BorderCOlor=RGB(0,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="KPEGKTKNKF",Visible=.t., Left=650, Top=106,;
    Alignment=0, Width=126, Height=17,;
    Caption="Articolo da definire"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="QLLAIDYZBL",Visible=.t., Left=23, Top=22,;
    Alignment=1, Width=69, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="UKTGPBHJBZ",Visible=.t., Left=31, Top=54,;
    Alignment=1, Width=61, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="RNOOVLGJGH",Visible=.t., Left=382, Top=54,;
    Alignment=1, Width=86, Height=18,;
    Caption="Sottogruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="UIQHMTNPVD",Visible=.t., Left=5, Top=81,;
    Alignment=1, Width=87, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="QRIBEUFOJG",Visible=.t., Left=9, Top=2,;
    Alignment=0, Width=99, Height=18,;
    Caption="Filtri selezioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_kim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
