* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_aat                                                        *
*              Attivit� preventive e consuntive                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_54]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-26                                                      *
* Last revis.: 2011-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_aat"))

* --- Class definition
define class tgsof_aat as StdForm
  Top    = 39
  Left   = 53

  * --- Standard Properties
  Width  = 639
  Height = 415+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-16"
  HelpContextID=143272297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=47

  * --- Constant Properties
  OFF_ATTI_IDX = 0
  OFF_NOMI_IDX = 0
  OFFTIPAT_IDX = 0
  OFF_ESIA_IDX = 0
  NOM_CONT_IDX = 0
  CPUSERS_IDX = 0
  OFF_ERTE_IDX = 0
  CAUMATTI_IDX = 0
  PAR_OFFE_IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  cFile = "OFF_ATTI"
  cKeySelect = "ATSERIAL"
  cKeyWhere  = "ATSERIAL=this.w_ATSERIAL"
  cKeyWhereODBC = '"ATSERIAL="+cp_ToStrODBC(this.w_ATSERIAL)';

  cKeyWhereODBCqualified = '"OFF_ATTI.ATSERIAL="+cp_ToStrODBC(this.w_ATSERIAL)';

  cPrg = "gsof_aat"
  cComment = "Attivit� preventive e consuntive"
  icon = "anag.ico"
  cAutoZoom = 'GSOF_AAT'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DATAINI = ctod('  /  /  ')
  o_DATAINI = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATAFIN = ctod('  /  /  ')
  o_DATAFIN = ctod('  /  /  ')
  w_ORAFIN = space(2)
  o_ORAFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_ATSTATUS = space(1)
  w_ATCODATT = space(5)
  o_ATCODATT = space(5)
  w_ATNUMPRI = 0
  w_ATNUMPRI = 0
  w_TADESCRI = space(50)
  w_ATOGGETT = space(254)
  w_ATCODNOM = space(20)
  w_ATCONTAT = space(5)
  w_ATOPERAT = 0
  w_ATSERIAL = space(20)
  o_ATSERIAL = space(20)
  w_READAZI = space(5)
  w_CAUATT = space(20)
  w_ATDATINI = ctot('')
  w_ATDATFIN = ctot('')
  w_NCPERSON = space(40)
  w_NAME = space(20)
  w_OFCODNOM = space(20)
  w_ATOPPOFF = space(10)
  w_OFSTATUS = space(1)
  w_OFNUMDOC = 0
  w_OFDATDOC = ctod('  /  /  ')
  w_OFSERDOC = space(10)
  w_ATCAUATT = space(20)
  o_ATCAUATT = space(20)
  w_OLDOPE = 0
  w_ATPERSON = space(60)
  w_ATPERSON = space(60)
  w_ATTELEFO = space(18)
  w_ATCELLUL = space(18)
  w_AT_EMAIL = space(0)
  w_ATLOCALI = space(30)
  w_ATTIPCLI = space(1)
  w_AT___FAX = space(18)
  w_ATCODBUN = space(3)
  w_FLNSAP = space(1)
  w_ATDATDOC = ctod('  /  /  ')
  w_ATCODVAL = space(3)
  w_ATSTATUS = space(1)
  w_ATNOTPIA = space(0)
  w_ATCODESI = space(5)
  w_AT_ESITO = space(0)
  w_ATFLATRI = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_ATSERIAL = this.W_ATSERIAL
  * --- Area Manuale = Declare Variables
  * --- gsof_aat
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'OFF_ATTI','gsof_aat')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_aatPag1","gsof_aat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Attivit�")
      .Pages(1).HelpContextID = 138842086
      .Pages(2).addobject("oPag","tgsof_aatPag2","gsof_aat",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Note")
      .Pages(2).HelpContextID = 136148266
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='OFFTIPAT'
    this.cWorkTables[3]='OFF_ESIA'
    this.cWorkTables[4]='NOM_CONT'
    this.cWorkTables[5]='CPUSERS'
    this.cWorkTables[6]='OFF_ERTE'
    this.cWorkTables[7]='CAUMATTI'
    this.cWorkTables[8]='PAR_OFFE'
    this.cWorkTables[9]='PRA_ENTI'
    this.cWorkTables[10]='PRA_UFFI'
    this.cWorkTables[11]='OFF_ATTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OFF_ATTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OFF_ATTI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ATSERIAL = NVL(ATSERIAL,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from OFF_ATTI where ATSERIAL=KeySet.ATSERIAL
    *
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OFF_ATTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OFF_ATTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OFF_ATTI '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DATAINI = i_DATSYS
        .w_ORAINI = '00'
        .w_MININI = '00'
        .w_DATAFIN = ctod("  /  /  ")
        .w_ORAFIN = '00'
        .w_MINFIN = '00'
        .w_TADESCRI = space(50)
        .w_READAZI = i_CODAZI
        .w_CAUATT = space(20)
        .w_NCPERSON = space(40)
        .w_NAME = space(20)
        .w_OFSTATUS = space(1)
        .w_OFNUMDOC = 0
        .w_OFDATDOC = ctod("  /  /  ")
        .w_OFSERDOC = space(10)
        .w_ATTIPCLI = space(1)
        .w_FLNSAP = space(1)
        .w_ATSTATUS = NVL(ATSTATUS,space(1))
        .w_ATCODATT = NVL(ATCODATT,space(5))
          if link_1_8_joined
            this.w_ATCODATT = NVL(TACODTIP108,NVL(this.w_ATCODATT,space(5)))
            this.w_TADESCRI = NVL(TADESCRI108,space(50))
          else
          .link_1_8('Load')
          endif
        .w_ATNUMPRI = NVL(ATNUMPRI,0)
        .w_ATNUMPRI = NVL(ATNUMPRI,0)
        .w_ATOGGETT = NVL(ATOGGETT,space(254))
        .w_ATCODNOM = NVL(ATCODNOM,space(20))
          if link_1_13_joined
            this.w_ATCODNOM = NVL(NOCODICE113,NVL(this.w_ATCODNOM,space(20)))
            this.w_ATPERSON = NVL(NODESCRI113,space(60))
            this.w_ATTELEFO = NVL(NOTELEFO113,space(18))
            this.w_AT___FAX = NVL(NOTELFAX113,space(18))
            this.w_AT_EMAIL = NVL(NO_EMAIL113,space(0))
            this.w_ATCELLUL = NVL(NONUMCEL113,space(18))
            this.w_ATTIPCLI = NVL(NOTIPCLI113,space(1))
          else
          .link_1_13('Load')
          endif
        .w_ATCONTAT = NVL(ATCONTAT,space(5))
          if link_1_14_joined
            this.w_ATCONTAT = NVL(NCCODCON114,NVL(this.w_ATCONTAT,space(5)))
            this.w_NCPERSON = NVL(NCPERSON114,space(40))
          else
          .link_1_14('Load')
          endif
        .w_ATOPERAT = NVL(ATOPERAT,0)
          if link_1_15_joined
            this.w_ATOPERAT = NVL(CODE115,NVL(this.w_ATOPERAT,0))
            this.w_NAME = NVL(NAME115,space(20))
          else
          .link_1_15('Load')
          endif
        .w_ATSERIAL = NVL(ATSERIAL,space(20))
        .op_ATSERIAL = .w_ATSERIAL
          .link_1_17('Load')
        .w_ATDATINI = NVL(ATDATINI,ctot(""))
        .w_ATDATFIN = NVL(ATDATFIN,ctot(""))
        .w_OFCODNOM = .w_ATCODNOM
        .w_ATOPPOFF = NVL(ATOPPOFF,space(10))
          .link_1_44('Load')
        .w_ATCAUATT = NVL(ATCAUATT,space(20))
          .link_1_58('Load')
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .w_OLDOPE = .w_ATOPERAT
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .w_ATPERSON = NVL(ATPERSON,space(60))
        .w_ATPERSON = NVL(ATPERSON,space(60))
        .w_ATTELEFO = NVL(ATTELEFO,space(18))
        .w_ATCELLUL = NVL(ATCELLUL,space(18))
        .w_AT_EMAIL = NVL(AT_EMAIL,space(0))
        .w_ATLOCALI = NVL(ATLOCALI,space(30))
        .w_AT___FAX = NVL(AT___FAX,space(18))
        .w_ATCODBUN = NVL(ATCODBUN,space(3))
        .w_ATDATDOC = NVL(cp_ToDate(ATDATDOC),ctod("  /  /  "))
        .w_ATCODVAL = NVL(ATCODVAL,space(3))
        .w_ATSTATUS = NVL(ATSTATUS,space(1))
        .w_ATNOTPIA = NVL(ATNOTPIA,space(0))
        .w_ATCODESI = NVL(ATCODESI,space(5))
          * evitabile
          *.link_2_2('Load')
        .w_AT_ESITO = NVL(AT_ESITO,space(0))
        .w_ATFLATRI = NVL(ATFLATRI,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'OFF_ATTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATAINI = ctod("  /  /  ")
      .w_ORAINI = space(2)
      .w_MININI = space(2)
      .w_DATAFIN = ctod("  /  /  ")
      .w_ORAFIN = space(2)
      .w_MINFIN = space(2)
      .w_ATSTATUS = space(1)
      .w_ATCODATT = space(5)
      .w_ATNUMPRI = 0
      .w_ATNUMPRI = 0
      .w_TADESCRI = space(50)
      .w_ATOGGETT = space(254)
      .w_ATCODNOM = space(20)
      .w_ATCONTAT = space(5)
      .w_ATOPERAT = 0
      .w_ATSERIAL = space(20)
      .w_READAZI = space(5)
      .w_CAUATT = space(20)
      .w_ATDATINI = ctot("")
      .w_ATDATFIN = ctot("")
      .w_NCPERSON = space(40)
      .w_NAME = space(20)
      .w_OFCODNOM = space(20)
      .w_ATOPPOFF = space(10)
      .w_OFSTATUS = space(1)
      .w_OFNUMDOC = 0
      .w_OFDATDOC = ctod("  /  /  ")
      .w_OFSERDOC = space(10)
      .w_ATCAUATT = space(20)
      .w_OLDOPE = 0
      .w_ATPERSON = space(60)
      .w_ATPERSON = space(60)
      .w_ATTELEFO = space(18)
      .w_ATCELLUL = space(18)
      .w_AT_EMAIL = space(0)
      .w_ATLOCALI = space(30)
      .w_ATTIPCLI = space(1)
      .w_AT___FAX = space(18)
      .w_ATCODBUN = space(3)
      .w_FLNSAP = space(1)
      .w_ATDATDOC = ctod("  /  /  ")
      .w_ATCODVAL = space(3)
      .w_ATSTATUS = space(1)
      .w_ATNOTPIA = space(0)
      .w_ATCODESI = space(5)
      .w_AT_ESITO = space(0)
      .w_ATFLATRI = space(1)
      if .cFunction<>"Filter"
        .w_DATAINI = i_DATSYS
        .w_ORAINI = '00'
        .w_MININI = '00'
          .DoRTCalc(4,4,.f.)
        .w_ORAFIN = '00'
        .w_MINFIN = '00'
        .w_ATSTATUS = iif(g_AGEN='S','D','A')
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_ATCODATT))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,9,.f.)
        .w_ATNUMPRI = IIF(EMPTY(.w_ATNUMPRI),1,.w_ATNUMPRI)
          .DoRTCalc(11,11,.f.)
        .w_ATOGGETT = IIF(EMPTY(.w_ATOGGETT), .w_TADESCRI, .w_ATOGGETT)
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_ATCODNOM))
          .link_1_13('Full')
          endif
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_ATCONTAT))
          .link_1_14('Full')
          endif
        .w_ATOPERAT = i_CODUTE
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_ATOPERAT))
          .link_1_15('Full')
          endif
          .DoRTCalc(16,16,.f.)
        .w_READAZI = i_CODAZI
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_READAZI))
          .link_1_17('Full')
          endif
          .DoRTCalc(18,22,.f.)
        .w_OFCODNOM = .w_ATCODNOM
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_ATOPPOFF))
          .link_1_44('Full')
          endif
          .DoRTCalc(25,28,.f.)
        .w_ATCAUATT = .w_CAUATT
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_ATCAUATT))
          .link_1_58('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .w_OLDOPE = .w_ATOPERAT
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
          .DoRTCalc(31,38,.f.)
        .w_ATCODBUN = g_CODBUN
          .DoRTCalc(40,40,.f.)
        .w_ATDATDOC = IIF(.w_FLNSAP<>'S',.w_DATAFIN,cp_Chartodate('  -  -    '))
        .w_ATCODVAL = g_PERVAL
        .w_ATSTATUS = iif(g_AGEN='S','D','A')
        .DoRTCalc(44,45,.f.)
          if not(empty(.w_ATCODESI))
          .link_2_2('Full')
          endif
          .DoRTCalc(46,46,.f.)
        .w_ATFLATRI = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'OFF_ATTI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEATT","i_codazi,w_ATSERIAL")
      .op_codazi = .w_codazi
      .op_ATSERIAL = .w_ATSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDATAINI_1_1.enabled = i_bVal
      .Page1.oPag.oORAINI_1_2.enabled = i_bVal
      .Page1.oPag.oMININI_1_3.enabled = i_bVal
      .Page1.oPag.oDATAFIN_1_4.enabled = i_bVal
      .Page1.oPag.oORAFIN_1_5.enabled = i_bVal
      .Page1.oPag.oMINFIN_1_6.enabled = i_bVal
      .Page1.oPag.oATSTATUS_1_7.enabled = i_bVal
      .Page1.oPag.oATCODATT_1_8.enabled = i_bVal
      .Page1.oPag.oATNUMPRI_1_9.enabled = i_bVal
      .Page1.oPag.oATNUMPRI_1_10.enabled = i_bVal
      .Page1.oPag.oATOGGETT_1_12.enabled = i_bVal
      .Page1.oPag.oATCODNOM_1_13.enabled = i_bVal
      .Page1.oPag.oATCONTAT_1_14.enabled = i_bVal
      .Page1.oPag.oATOPERAT_1_15.enabled = i_bVal
      .Page1.oPag.oATOPPOFF_1_44.enabled = i_bVal
      .Page1.oPag.oATSTATUS_1_76.enabled = i_bVal
      .Page2.oPag.oATNOTPIA_2_1.enabled = i_bVal
      .Page2.oPag.oATCODESI_2_2.enabled = i_bVal
      .Page2.oPag.oAT_ESITO_2_3.enabled = i_bVal
      .Page1.oPag.oBtn_1_45.enabled = i_bVal
      .Page1.oPag.oBtn_1_56.enabled = i_bVal
      .Page1.oPag.oBtn_1_57.enabled = .Page1.oPag.oBtn_1_57.mCond()
      .Page1.oPag.oObj_1_60.enabled = i_bVal
      .Page1.oPag.oObj_1_62.enabled = i_bVal
      .Page1.oPag.oObj_1_63.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'OFF_ATTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSTATUS,"ATSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODATT,"ATCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNUMPRI,"ATNUMPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNUMPRI,"ATNUMPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATOGGETT,"ATOGGETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODNOM,"ATCODNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCONTAT,"ATCONTAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATOPERAT,"ATOPERAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSERIAL,"ATSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATINI,"ATDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATFIN,"ATDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATOPPOFF,"ATOPPOFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCAUATT,"ATCAUATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPERSON,"ATPERSON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPERSON,"ATPERSON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTELEFO,"ATTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCELLUL,"ATCELLUL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT_EMAIL,"AT_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATLOCALI,"ATLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT___FAX,"AT___FAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODBUN,"ATCODBUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATDOC,"ATDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODVAL,"ATCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSTATUS,"ATSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNOTPIA,"ATNOTPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODESI,"ATCODESI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT_ESITO,"AT_ESITO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFLATRI,"ATFLATRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
    i_lTable = "OFF_ATTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.OFF_ATTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.OFF_ATTI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEATT","i_codazi,w_ATSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into OFF_ATTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OFF_ATTI')
        i_extval=cp_InsertValODBCExtFlds(this,'OFF_ATTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ATSTATUS,ATCODATT,ATNUMPRI,ATOGGETT,ATCODNOM"+;
                  ",ATCONTAT,ATOPERAT,ATSERIAL,ATDATINI,ATDATFIN"+;
                  ",ATOPPOFF,ATCAUATT,ATPERSON,ATTELEFO,ATCELLUL"+;
                  ",AT_EMAIL,ATLOCALI,AT___FAX,ATCODBUN,ATDATDOC"+;
                  ",ATCODVAL,ATNOTPIA,ATCODESI,AT_ESITO,ATFLATRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ATSTATUS)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODATT)+;
                  ","+cp_ToStrODBC(this.w_ATNUMPRI)+;
                  ","+cp_ToStrODBC(this.w_ATOGGETT)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODNOM)+;
                  ","+cp_ToStrODBCNull(this.w_ATCONTAT)+;
                  ","+cp_ToStrODBCNull(this.w_ATOPERAT)+;
                  ","+cp_ToStrODBC(this.w_ATSERIAL)+;
                  ","+cp_ToStrODBC(this.w_ATDATINI)+;
                  ","+cp_ToStrODBC(this.w_ATDATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_ATOPPOFF)+;
                  ","+cp_ToStrODBCNull(this.w_ATCAUATT)+;
                  ","+cp_ToStrODBC(this.w_ATPERSON)+;
                  ","+cp_ToStrODBC(this.w_ATTELEFO)+;
                  ","+cp_ToStrODBC(this.w_ATCELLUL)+;
                  ","+cp_ToStrODBC(this.w_AT_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_ATLOCALI)+;
                  ","+cp_ToStrODBC(this.w_AT___FAX)+;
                  ","+cp_ToStrODBC(this.w_ATCODBUN)+;
                  ","+cp_ToStrODBC(this.w_ATDATDOC)+;
                  ","+cp_ToStrODBC(this.w_ATCODVAL)+;
                  ","+cp_ToStrODBC(this.w_ATNOTPIA)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODESI)+;
                  ","+cp_ToStrODBC(this.w_AT_ESITO)+;
                  ","+cp_ToStrODBC(this.w_ATFLATRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OFF_ATTI')
        i_extval=cp_InsertValVFPExtFlds(this,'OFF_ATTI')
        cp_CheckDeletedKey(i_cTable,0,'ATSERIAL',this.w_ATSERIAL)
        INSERT INTO (i_cTable);
              (ATSTATUS,ATCODATT,ATNUMPRI,ATOGGETT,ATCODNOM,ATCONTAT,ATOPERAT,ATSERIAL,ATDATINI,ATDATFIN,ATOPPOFF,ATCAUATT,ATPERSON,ATTELEFO,ATCELLUL,AT_EMAIL,ATLOCALI,AT___FAX,ATCODBUN,ATDATDOC,ATCODVAL,ATNOTPIA,ATCODESI,AT_ESITO,ATFLATRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ATSTATUS;
                  ,this.w_ATCODATT;
                  ,this.w_ATNUMPRI;
                  ,this.w_ATOGGETT;
                  ,this.w_ATCODNOM;
                  ,this.w_ATCONTAT;
                  ,this.w_ATOPERAT;
                  ,this.w_ATSERIAL;
                  ,this.w_ATDATINI;
                  ,this.w_ATDATFIN;
                  ,this.w_ATOPPOFF;
                  ,this.w_ATCAUATT;
                  ,this.w_ATPERSON;
                  ,this.w_ATTELEFO;
                  ,this.w_ATCELLUL;
                  ,this.w_AT_EMAIL;
                  ,this.w_ATLOCALI;
                  ,this.w_AT___FAX;
                  ,this.w_ATCODBUN;
                  ,this.w_ATDATDOC;
                  ,this.w_ATCODVAL;
                  ,this.w_ATNOTPIA;
                  ,this.w_ATCODESI;
                  ,this.w_AT_ESITO;
                  ,this.w_ATFLATRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.OFF_ATTI_IDX,i_nConn)
      *
      * update OFF_ATTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'OFF_ATTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ATSTATUS="+cp_ToStrODBC(this.w_ATSTATUS)+;
             ",ATCODATT="+cp_ToStrODBCNull(this.w_ATCODATT)+;
             ",ATNUMPRI="+cp_ToStrODBC(this.w_ATNUMPRI)+;
             ",ATOGGETT="+cp_ToStrODBC(this.w_ATOGGETT)+;
             ",ATCODNOM="+cp_ToStrODBCNull(this.w_ATCODNOM)+;
             ",ATCONTAT="+cp_ToStrODBCNull(this.w_ATCONTAT)+;
             ",ATOPERAT="+cp_ToStrODBCNull(this.w_ATOPERAT)+;
             ",ATDATINI="+cp_ToStrODBC(this.w_ATDATINI)+;
             ",ATDATFIN="+cp_ToStrODBC(this.w_ATDATFIN)+;
             ",ATOPPOFF="+cp_ToStrODBCNull(this.w_ATOPPOFF)+;
             ",ATCAUATT="+cp_ToStrODBCNull(this.w_ATCAUATT)+;
             ",ATPERSON="+cp_ToStrODBC(this.w_ATPERSON)+;
             ",ATTELEFO="+cp_ToStrODBC(this.w_ATTELEFO)+;
             ",ATCELLUL="+cp_ToStrODBC(this.w_ATCELLUL)+;
             ",AT_EMAIL="+cp_ToStrODBC(this.w_AT_EMAIL)+;
             ",ATLOCALI="+cp_ToStrODBC(this.w_ATLOCALI)+;
             ",AT___FAX="+cp_ToStrODBC(this.w_AT___FAX)+;
             ",ATCODBUN="+cp_ToStrODBC(this.w_ATCODBUN)+;
             ",ATDATDOC="+cp_ToStrODBC(this.w_ATDATDOC)+;
             ",ATCODVAL="+cp_ToStrODBC(this.w_ATCODVAL)+;
             ",ATNOTPIA="+cp_ToStrODBC(this.w_ATNOTPIA)+;
             ",ATCODESI="+cp_ToStrODBCNull(this.w_ATCODESI)+;
             ",AT_ESITO="+cp_ToStrODBC(this.w_AT_ESITO)+;
             ",ATFLATRI="+cp_ToStrODBC(this.w_ATFLATRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'OFF_ATTI')
        i_cWhere = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
        UPDATE (i_cTable) SET;
              ATSTATUS=this.w_ATSTATUS;
             ,ATCODATT=this.w_ATCODATT;
             ,ATNUMPRI=this.w_ATNUMPRI;
             ,ATOGGETT=this.w_ATOGGETT;
             ,ATCODNOM=this.w_ATCODNOM;
             ,ATCONTAT=this.w_ATCONTAT;
             ,ATOPERAT=this.w_ATOPERAT;
             ,ATDATINI=this.w_ATDATINI;
             ,ATDATFIN=this.w_ATDATFIN;
             ,ATOPPOFF=this.w_ATOPPOFF;
             ,ATCAUATT=this.w_ATCAUATT;
             ,ATPERSON=this.w_ATPERSON;
             ,ATTELEFO=this.w_ATTELEFO;
             ,ATCELLUL=this.w_ATCELLUL;
             ,AT_EMAIL=this.w_AT_EMAIL;
             ,ATLOCALI=this.w_ATLOCALI;
             ,AT___FAX=this.w_AT___FAX;
             ,ATCODBUN=this.w_ATCODBUN;
             ,ATDATDOC=this.w_ATDATDOC;
             ,ATCODVAL=this.w_ATCODVAL;
             ,ATNOTPIA=this.w_ATNOTPIA;
             ,ATCODESI=this.w_ATCODESI;
             ,AT_ESITO=this.w_AT_ESITO;
             ,ATFLATRI=this.w_ATFLATRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsof_aat
    This.Notifyevent('ControlliFinali')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.OFF_ATTI_IDX,i_nConn)
      *
      * delete OFF_ATTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_ATNUMPRI = IIF(EMPTY(.w_ATNUMPRI),1,.w_ATNUMPRI)
        endif
        .DoRTCalc(11,11,.t.)
        if .o_ATCODATT<>.w_ATCODATT
            .w_ATOGGETT = IIF(EMPTY(.w_ATOGGETT), .w_TADESCRI, .w_ATOGGETT)
        endif
        .DoRTCalc(13,16,.t.)
          .link_1_17('Full')
        if .o_ORAINI<>.w_ORAINI
          .Calculate_SLCYSAMDOK()
        endif
        if .o_MININI<>.w_MININI
          .Calculate_XLTWHZKGZW()
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_TTQFMXGTWH()
        endif
        if .o_ORAFIN<>.w_ORAFIN
          .Calculate_EMHXGAZDWG()
        endif
        .DoRTCalc(18,22,.t.)
            .w_OFCODNOM = .w_ATCODNOM
        if .o_DATAINI<>.w_DATAINI.or. .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI
          .Calculate_UGUEDMJCNH()
        endif
        if .o_DATAFIN<>.w_DATAFIN.or. .o_ORAFIN<>.w_ORAFIN.or. .o_MINFIN<>.w_MINFIN
          .Calculate_HODRFYTEIS()
        endif
        .DoRTCalc(24,28,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_ATCAUATT = .w_CAUATT
          .link_1_58('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        if .o_ATSERIAL<>.w_ATSERIAL
            .w_OLDOPE = .w_ATOPERAT
        endif
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .DoRTCalc(31,38,.t.)
            .w_ATCODBUN = g_CODBUN
        .DoRTCalc(40,40,.t.)
            .w_ATDATDOC = IIF(.w_FLNSAP<>'S',.w_DATAFIN,cp_Chartodate('  -  -    '))
            .w_ATCODVAL = g_PERVAL
        .DoRTCalc(43,46,.t.)
            .w_ATFLATRI = 'N'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEATT","i_codazi,w_ATSERIAL")
          .op_ATSERIAL = .w_ATSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
    endwith
  return

  proc Calculate_SLCYSAMDOK()
    with this
          * --- Resetto w_oraini
          .w_ORAINI = .ZeroFill(.w_ORAINI)
    endwith
  endproc
  proc Calculate_XLTWHZKGZW()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_TTQFMXGTWH()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_EMHXGAZDWG()
    with this
          * --- Resetto w_orafin
          .w_ORAFIN = .ZeroFill(.w_ORAFIN)
    endwith
  endproc
  proc Calculate_MKXFVJDJML()
    with this
          * --- Scomponi data/ora inizio
          .w_DATAINI = TTOD(.w_ATDATINI)
          .w_ORAINI = SUBSTR(TTOC(.w_ATDATINI), 12, 2)
          .w_MININI = SUBSTR(TTOC(.w_ATDATINI), 15, 2)
    endwith
  endproc
  proc Calculate_UGUEDMJCNH()
    with this
          * --- Setta w_atdatini
          .w_ATDATINI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAINI),  DTOC(.w_DATAINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_HODRFYTEIS()
    with this
          * --- Setta w_atdatfin
          .w_ATDATFIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAFIN),  DTOC(.w_DATAFIN)+' '+.w_ORAFIN+':'+.w_MINFIN+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_CEUVBMSQYV()
    with this
          * --- Abilita bottone
          AbilitaBottone(this;
             )
    endwith
  endproc
  proc Calculate_OOZAFXLYXQ()
    with this
          * --- Scomponi data/ora fine
          .w_DATAFIN = TTOD(.w_ATDATFIN)
          .w_ORAFIN = IIF(EMPTY(.w_ATDATFIN), '00', SUBSTR(TTOC(.w_ATDATFIN), 12, 2))
          .w_MINFIN = IIF(EMPTY(.w_ATDATFIN), '00', SUBSTR(TTOC(.w_ATDATFIN), 15, 2))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATOPERAT_1_15.enabled = this.oPgFrm.Page1.oPag.oATOPERAT_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oATSTATUS_1_7.visible=!this.oPgFrm.Page1.oPag.oATSTATUS_1_7.mHide()
    this.oPgFrm.Page1.oPag.oATNUMPRI_1_9.visible=!this.oPgFrm.Page1.oPag.oATNUMPRI_1_9.mHide()
    this.oPgFrm.Page1.oPag.oATNUMPRI_1_10.visible=!this.oPgFrm.Page1.oPag.oATNUMPRI_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oATOPPOFF_1_44.visible=!this.oPgFrm.Page1.oPag.oATOPPOFF_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oATSTATUS_1_76.visible=!this.oPgFrm.Page1.oPag.oATSTATUS_1_76.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Load")
          .Calculate_MKXFVJDJML()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_UGUEDMJCNH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_CEUVBMSQYV()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_OOZAFXLYXQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ATCODATT
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATA',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_ATCODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_ATCODATT))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODATT)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODATT) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oATCODATT_1_8'),i_cWhere,'GSOF_ATA',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_ATCODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_ATCODATT)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODATT = NVL(_Link_.TACODTIP,space(5))
      this.w_TADESCRI = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODATT = space(5)
      endif
      this.w_TADESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFFTIPAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.TACODTIP as TACODTIP108"+ ",link_1_8.TADESCRI as TADESCRI108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on OFF_ATTI.ATCODATT=link_1_8.TACODTIP"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCODATT=link_1_8.TACODTIP(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCODNOM
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_ATCODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTELEFO,NOTELFAX,NO_EMAIL,NONUMCEL,NOTIPCLI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_ATCODNOM))
          select NOCODICE,NODESCRI,NOTELEFO,NOTELFAX,NO_EMAIL,NONUMCEL,NOTIPCLI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oATCODNOM_1_13'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTELEFO,NOTELFAX,NO_EMAIL,NONUMCEL,NOTIPCLI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NOTELEFO,NOTELFAX,NO_EMAIL,NONUMCEL,NOTIPCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTELEFO,NOTELFAX,NO_EMAIL,NONUMCEL,NOTIPCLI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_ATCODNOM)
            select NOCODICE,NODESCRI,NOTELEFO,NOTELFAX,NO_EMAIL,NONUMCEL,NOTIPCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_ATPERSON = NVL(_Link_.NODESCRI,space(60))
      this.w_ATTELEFO = NVL(_Link_.NOTELEFO,space(18))
      this.w_AT___FAX = NVL(_Link_.NOTELFAX,space(18))
      this.w_AT_EMAIL = NVL(_Link_.NO_EMAIL,space(0))
      this.w_ATCELLUL = NVL(_Link_.NONUMCEL,space(18))
      this.w_ATTIPCLI = NVL(_Link_.NOTIPCLI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODNOM = space(20)
      endif
      this.w_ATPERSON = space(60)
      this.w_ATTELEFO = space(18)
      this.w_AT___FAX = space(18)
      this.w_AT_EMAIL = space(0)
      this.w_ATCELLUL = space(18)
      this.w_ATTIPCLI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_NOMI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.NOCODICE as NOCODICE113"+ ",link_1_13.NODESCRI as NODESCRI113"+ ",link_1_13.NOTELEFO as NOTELEFO113"+ ",link_1_13.NOTELFAX as NOTELFAX113"+ ",link_1_13.NO_EMAIL as NO_EMAIL113"+ ",link_1_13.NONUMCEL as NONUMCEL113"+ ",link_1_13.NOTIPCLI as NOTIPCLI113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on OFF_ATTI.ATCODNOM=link_1_13.NOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCODNOM=link_1_13.NOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCONTAT
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCONTAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_ATCODNOM;
                     ,'NCCODCON',trim(this.w_ATCONTAT))
          select NCCODICE,NCCODCON,NCPERSON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCONTAT)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCONTAT) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oATCONTAT_1_14'),i_cWhere,'',"Contatti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODNOM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCONTAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_ATCONTAT);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_ATCODNOM;
                       ,'NCCODCON',this.w_ATCONTAT)
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCONTAT = NVL(_Link_.NCCODCON,space(5))
      this.w_NCPERSON = NVL(_Link_.NCPERSON,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ATCONTAT = space(5)
      endif
      this.w_NCPERSON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCONTAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NOM_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.NCCODCON as NCCODCON114"+ ",link_1_14.NCPERSON as NCPERSON114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on OFF_ATTI.ATCONTAT=link_1_14.NCCODCON"+" and OFF_ATTI.ATCODNOM=link_1_14.NCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCONTAT=link_1_14.NCCODCON(+)"'+'+" and OFF_ATTI.ATCODNOM=link_1_14.NCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATOPERAT
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATOPERAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_ATOPERAT);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_ATOPERAT)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_ATOPERAT) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oATOPERAT_1_15'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATOPERAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_ATOPERAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_ATOPERAT)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATOPERAT = NVL(_Link_.CODE,0)
      this.w_NAME = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_ATOPERAT = 0
      endif
      this.w_NAME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATOPERAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPUSERS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.CODE as CODE115"+ ",link_1_15.NAME as NAME115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on OFF_ATTI.ATOPERAT=link_1_15.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATOPERAT=link_1_15.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_lTable = "PAR_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2], .t., this.PAR_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODAZI,POTIPATT";
                   +" from "+i_cTable+" "+i_lTable+" where POCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODAZI',this.w_READAZI)
            select POCODAZI,POTIPATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.POCODAZI,space(5))
      this.w_CAUATT = NVL(_Link_.POTIPATT,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_CAUATT = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.POCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATOPPOFF
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    i_lTable = "OFF_ERTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2], .t., this.OFF_ERTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATOPPOFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AOF',True,'OFF_ERTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OFSERIAL like "+cp_ToStrODBC(trim(this.w_ATOPPOFF)+"%");
                   +" and OFCODNOM="+cp_ToStrODBC(this.w_ATCODNOM);

          i_ret=cp_SQL(i_nConn,"select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OFCODNOM,OFSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OFCODNOM',this.w_ATCODNOM;
                     ,'OFSERIAL',trim(this.w_ATOPPOFF))
          select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OFCODNOM,OFSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATOPPOFF)==trim(_Link_.OFSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATOPPOFF) and !this.bDontReportError
            deferred_cp_zoom('OFF_ERTE','*','OFCODNOM,OFSERIAL',cp_AbsName(oSource.parent,'oATOPPOFF_1_44'),i_cWhere,'GSOF_AOF',"Opportunita/offerte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODNOM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                     +" from "+i_cTable+" "+i_lTable+" where OFSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and OFCODNOM="+cp_ToStrODBC(this.w_ATCODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OFCODNOM',oSource.xKey(1);
                       ,'OFSERIAL',oSource.xKey(2))
            select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATOPPOFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                   +" from "+i_cTable+" "+i_lTable+" where OFSERIAL="+cp_ToStrODBC(this.w_ATOPPOFF);
                   +" and OFCODNOM="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OFCODNOM',this.w_ATCODNOM;
                       ,'OFSERIAL',this.w_ATOPPOFF)
            select OFCODNOM,OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATOPPOFF = NVL(_Link_.OFSERIAL,space(10))
      this.w_OFNUMDOC = NVL(_Link_.OFNUMDOC,0)
      this.w_OFDATDOC = NVL(cp_ToDate(_Link_.OFDATDOC),ctod("  /  /  "))
      this.w_OFSERDOC = NVL(_Link_.OFSERDOC,space(10))
      this.w_OFSTATUS = NVL(_Link_.OFSTATUS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATOPPOFF = space(10)
      endif
      this.w_OFNUMDOC = 0
      this.w_OFDATDOC = ctod("  /  /  ")
      this.w_OFSERDOC = space(10)
      this.w_OFSTATUS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])+'\'+cp_ToStr(_Link_.OFCODNOM,1)+'\'+cp_ToStr(_Link_.OFSERIAL,1)
      cp_ShowWarn(i_cKey,this.OFF_ERTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATOPPOFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCAUATT
  func Link_1_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CAFLNSAP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ATCAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ATCAUATT)
            select CACODICE,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUATT = NVL(_Link_.CACODICE,space(20))
      this.w_FLNSAP = NVL(_Link_.CAFLNSAP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUATT = space(20)
      endif
      this.w_FLNSAP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODESI
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_ESIA_IDX,3]
    i_lTable = "OFF_ESIA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ESIA_IDX,2], .t., this.OFF_ESIA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ESIA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODESI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AES',True,'OFF_ESIA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESI like "+cp_ToStrODBC(trim(this.w_ATCODESI)+"%");

          i_ret=cp_SQL(i_nConn,"select ESCODESI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODESI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODESI',trim(this.w_ATCODESI))
          select ESCODESI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODESI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODESI)==trim(_Link_.ESCODESI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODESI) and !this.bDontReportError
            deferred_cp_zoom('OFF_ESIA','*','ESCODESI',cp_AbsName(oSource.parent,'oATCODESI_2_2'),i_cWhere,'GSOF_AES',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODESI";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODESI',oSource.xKey(1))
            select ESCODESI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODESI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODESI";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESI="+cp_ToStrODBC(this.w_ATCODESI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODESI',this.w_ATCODESI)
            select ESCODESI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODESI = NVL(_Link_.ESCODESI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODESI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_ESIA_IDX,2])+'\'+cp_ToStr(_Link_.ESCODESI,1)
      cp_ShowWarn(i_cKey,this.OFF_ESIA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODESI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_1.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_1.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_2.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_2.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_3.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_3.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_4.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_4.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_5.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_5.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_6.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_6.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oATSTATUS_1_7.RadioValue()==this.w_ATSTATUS)
      this.oPgFrm.Page1.oPag.oATSTATUS_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODATT_1_8.value==this.w_ATCODATT)
      this.oPgFrm.Page1.oPag.oATCODATT_1_8.value=this.w_ATCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATNUMPRI_1_9.value==this.w_ATNUMPRI)
      this.oPgFrm.Page1.oPag.oATNUMPRI_1_9.value=this.w_ATNUMPRI
    endif
    if not(this.oPgFrm.Page1.oPag.oATNUMPRI_1_10.RadioValue()==this.w_ATNUMPRI)
      this.oPgFrm.Page1.oPag.oATNUMPRI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETT_1_12.value==this.w_ATOGGETT)
      this.oPgFrm.Page1.oPag.oATOGGETT_1_12.value=this.w_ATOGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODNOM_1_13.value==this.w_ATCODNOM)
      this.oPgFrm.Page1.oPag.oATCODNOM_1_13.value=this.w_ATCODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oATCONTAT_1_14.value==this.w_ATCONTAT)
      this.oPgFrm.Page1.oPag.oATCONTAT_1_14.value=this.w_ATCONTAT
    endif
    if not(this.oPgFrm.Page1.oPag.oATOPERAT_1_15.value==this.w_ATOPERAT)
      this.oPgFrm.Page1.oPag.oATOPERAT_1_15.value=this.w_ATOPERAT
    endif
    if not(this.oPgFrm.Page1.oPag.oNCPERSON_1_29.value==this.w_NCPERSON)
      this.oPgFrm.Page1.oPag.oNCPERSON_1_29.value=this.w_NCPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oNAME_1_30.value==this.w_NAME)
      this.oPgFrm.Page1.oPag.oNAME_1_30.value=this.w_NAME
    endif
    if not(this.oPgFrm.Page1.oPag.oATOPPOFF_1_44.value==this.w_ATOPPOFF)
      this.oPgFrm.Page1.oPag.oATOPPOFF_1_44.value=this.w_ATOPPOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oOFSTATUS_1_46.RadioValue()==this.w_OFSTATUS)
      this.oPgFrm.Page1.oPag.oOFSTATUS_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOFNUMDOC_1_47.value==this.w_OFNUMDOC)
      this.oPgFrm.Page1.oPag.oOFNUMDOC_1_47.value=this.w_OFNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDATDOC_1_48.value==this.w_OFDATDOC)
      this.oPgFrm.Page1.oPag.oOFDATDOC_1_48.value=this.w_OFDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFSERDOC_1_49.value==this.w_OFSERDOC)
      this.oPgFrm.Page1.oPag.oOFSERDOC_1_49.value=this.w_OFSERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oATPERSON_1_64.value==this.w_ATPERSON)
      this.oPgFrm.Page1.oPag.oATPERSON_1_64.value=this.w_ATPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oATSTATUS_1_76.RadioValue()==this.w_ATSTATUS)
      this.oPgFrm.Page1.oPag.oATSTATUS_1_76.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATNOTPIA_2_1.value==this.w_ATNOTPIA)
      this.oPgFrm.Page2.oPag.oATNOTPIA_2_1.value=this.w_ATNOTPIA
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODESI_2_2.RadioValue()==this.w_ATCODESI)
      this.oPgFrm.Page2.oPag.oATCODESI_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAT_ESITO_2_3.value==this.w_AT_ESITO)
      this.oPgFrm.Page2.oPag.oAT_ESITO_2_3.value=this.w_AT_ESITO
    endif
    cp_SetControlsValueExtFlds(this,'OFF_ATTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_ATDATINI<=.w_ATDATFIN Or EMPTY(.w_ATDATFIN))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data e ora di inizio attivit� devono essere minori di data e ora di fine attivit�")
          case   (empty(.w_DATAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINI_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DATAINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ORAINI)) or not(VAL(.w_ORAINI) < 24))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_2.SetFocus()
            i_bnoObbl = !empty(.w_ORAINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MININI)) or not(VAL(.w_MININI) < 60))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_MININI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_ORAFIN) < 24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_ATOGGETT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATOGGETT_1_12.SetFocus()
            i_bnoObbl = !empty(.w_ATOGGETT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATOPERAT))  and (CP_ISADMINISTRATOR())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATOPERAT_1_15.SetFocus()
            i_bnoObbl = !empty(.w_ATOPERAT)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_aat
       if i_bRes
         if Empty(.w_ATDATFIN) and g_AGEN='S'
            i_bRes=.F.
            i_bnoChk=.F.
            i_cErrorMsg=Ah_MsgFormat("Data di fine attivit� obbligatoria")
         endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATAINI = this.w_DATAINI
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    this.o_DATAFIN = this.w_DATAFIN
    this.o_ORAFIN = this.w_ORAFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_ATCODATT = this.w_ATCODATT
    this.o_ATSERIAL = this.w_ATSERIAL
    this.o_ATCAUATT = this.w_ATCAUATT
    return

  func CanAdd()
    local i_res
    i_res=(Not Empty(this.w_CAUATT) and  g_AGEN='S' ) or  g_AGEN<>'S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, tipo attivit� non presente nei parametri offerte, impossibile caricare  "))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsof_aatPag1 as StdContainer
  Width  = 772
  height = 415
  stdWidth  = 772
  stdheight = 415
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATAINI_1_1 as StdField with uid="ZTJTUAFVRT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio attivit�",;
    HelpContextID = 95658954,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=143, Top=15

  add object oORAINI_1_2 as StdField with uid="IFZETBJSDN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio attivit�",;
    HelpContextID = 173851162,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=370, Top=15, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_3 as StdField with uid="BMAZHAYBOV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio attivit�",;
    HelpContextID = 173800250,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=409, Top=15, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATAFIN_1_4 as StdField with uid="FTOHZHZPSH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine attivit�",;
    HelpContextID = 182690762,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=143, Top=41

  add object oORAFIN_1_5 as StdField with uid="TWVCQEVEXZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine attivit�",;
    HelpContextID = 95404570,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=370, Top=41, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_6 as StdField with uid="VEYPJJGXRE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine attivit�",;
    HelpContextID = 95353658,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=409, Top=41, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc


  add object oATSTATUS_1_7 as StdCombo with uid="GUFZGBKGXO",rtseq=7,rtrep=.f.,left=507,top=13,width=99,height=21;
    , ToolTipText = "Stato attivit�";
    , HelpContextID = 266297177;
    , cFormVar="w_ATSTATUS",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATSTATUS_1_7.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oATSTATUS_1_7.GetRadio()
    this.Parent.oContained.w_ATSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oATSTATUS_1_7.SetRadio()
    this.Parent.oContained.w_ATSTATUS=trim(this.Parent.oContained.w_ATSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_ATSTATUS=='T',1,;
      iif(this.Parent.oContained.w_ATSTATUS=='D',2,;
      iif(this.Parent.oContained.w_ATSTATUS=='I',3,;
      iif(this.Parent.oContained.w_ATSTATUS=='F',4,;
      0))))
  endfunc

  func oATSTATUS_1_7.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  add object oATCODATT_1_8 as StdField with uid="SCBDNDOAUO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ATCODATT", cQueryName = "ATCODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia attivit�",;
    HelpContextID = 218718042,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=143, Top=102, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", cZoomOnZoom="GSOF_ATA", oKey_1_1="TACODTIP", oKey_1_2="this.w_ATCODATT"

  func oATCODATT_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODATT_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODATT_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oATCODATT_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATA',"Tipologie attivit�",'',this.parent.oContained
  endproc
  proc oATCODATT_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODTIP=this.parent.oContained.w_ATCODATT
     i_obj.ecpSave()
  endproc

  add object oATNUMPRI_1_9 as StdField with uid="RPJRKGFEPK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ATNUMPRI", cQueryName = "ATNUMPRI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit� attivit�",;
    HelpContextID = 211816271,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=478, Top=102, cSayPict='"999999"', cGetPict='"999999"'

  func oATNUMPRI_1_9.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc


  add object oATNUMPRI_1_10 as StdCombo with uid="OLRHIUHGZO",rtseq=10,rtrep=.f.,left=479,top=104,width=116,height=21;
    , ToolTipText = "Priorit� da assegnare all'attivit� (normale, urgente, scadenza termine=da fare in un determinato giorno ad un'ora prestabilita)";
    , HelpContextID = 211816271;
    , cFormVar="w_ATNUMPRI",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATNUMPRI_1_10.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,3,;
    iif(this.value =3,4,;
    0))))
  endfunc
  func oATNUMPRI_1_10.GetRadio()
    this.Parent.oContained.w_ATNUMPRI = this.RadioValue()
    return .t.
  endfunc

  func oATNUMPRI_1_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ATNUMPRI==1,1,;
      iif(this.Parent.oContained.w_ATNUMPRI==3,2,;
      iif(this.Parent.oContained.w_ATNUMPRI==4,3,;
      0)))
  endfunc

  func oATNUMPRI_1_10.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  add object oATOGGETT_1_12 as StdField with uid="MRLXYSREPN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ATOGGETT", cQueryName = "ATOGGETT",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto libero",;
    HelpContextID = 20062042,;
   bGlobalFont=.t.,;
    Height=21, Width=453, Left=143, Top=128, InputMask=replicate('X',254)

  add object oATCODNOM_1_13 as StdField with uid="UTACBYLWZX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ATCODNOM", cQueryName = "ATCODNOM",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 100049069,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=143, Top=155, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_ATCODNOM"

  func oATCODNOM_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
      if .not. empty(.w_ATCONTAT)
        bRes2=.link_1_14('Full')
      endif
      if .not. empty(.w_ATOPPOFF)
        bRes2=.link_1_44('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCODNOM_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODNOM_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oATCODNOM_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oATCODNOM_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_ATCODNOM
     i_obj.ecpSave()
  endproc

  add object oATCONTAT_1_14 as StdField with uid="MZPCVJWOSS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ATCONTAT", cQueryName = "ATCONTAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice contatto",;
    HelpContextID = 11099994,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=143, Top=182, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCCODICE", oKey_1_2="this.w_ATCODNOM", oKey_2_1="NCCODCON", oKey_2_2="this.w_ATCONTAT"

  func oATCONTAT_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCONTAT_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCONTAT_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.NOM_CONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_ATCODNOM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStr(this.Parent.oContained.w_ATCODNOM)
    endif
    do cp_zoom with 'NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(this.parent,'oATCONTAT_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Contatti",'',this.parent.oContained
  endproc

  add object oATOPERAT_1_15 as StdField with uid="JTUYGRLMDX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ATOPERAT", cQueryName = "ATOPERAT",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente/operatore",;
    HelpContextID = 236658522,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=143, Top=209, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_ATOPERAT"

  func oATOPERAT_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (CP_ISADMINISTRATOR())
    endwith
   endif
  endfunc

  func oATOPERAT_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oATOPERAT_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATOPERAT_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oATOPERAT_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oNCPERSON_1_29 as StdField with uid="ZADRIKKLNS",rtseq=21,rtrep=.f.,;
    cFormVar = "w_NCPERSON", cQueryName = "NCPERSON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contatto",;
    HelpContextID = 2089180,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=210, Top=182, InputMask=replicate('X',40)

  add object oNAME_1_30 as StdField with uid="QNPDZXTFIM",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NAME", cQueryName = "NAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente/operatore",;
    HelpContextID = 138416938,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=210, Top=209, InputMask=replicate('X',20)

  add object oATOPPOFF_1_44 as StdField with uid="NVUJCUJCPZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ATOPPOFF", cQueryName = "ATOPPOFF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Deve essere edit per effettuare i link in area manuale function and proc..",;
    HelpContextID = 197861196,;
   bGlobalFont=.t.,;
    Height=21, Width=81, Left=691, Top=84, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="OFF_ERTE", cZoomOnZoom="GSOF_AOF", oKey_1_1="OFCODNOM", oKey_1_2="this.w_ATCODNOM", oKey_2_1="OFSERIAL", oKey_2_2="this.w_ATOPPOFF"

  func oATOPPOFF_1_44.mHide()
    with this.Parent.oContained
      return (1 = 1)
    endwith
  endfunc

  func oATOPPOFF_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oATOPPOFF_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATOPPOFF_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.OFF_ERTE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OFCODNOM="+cp_ToStrODBC(this.Parent.oContained.w_ATCODNOM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OFCODNOM="+cp_ToStr(this.Parent.oContained.w_ATCODNOM)
    endif
    do cp_zoom with 'OFF_ERTE','*','OFCODNOM,OFSERIAL',cp_AbsName(this.parent,'oATOPPOFF_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AOF',"Opportunita/offerte",'',this.parent.oContained
  endproc
  proc oATOPPOFF_1_44.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AOF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.OFCODNOM=w_ATCODNOM
     i_obj.w_OFSERIAL=this.parent.oContained.w_ATOPPOFF
     i_obj.ecpSave()
  endproc


  add object oBtn_1_45 as StdButton with uid="CXRFQHUCZY",left=514, top=238, width=20,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare una opportunit�/offerta";
    , HelpContextID = 143071274;
  , bGlobalFont=.t.

    proc oBtn_1_45.Click()
      with this.Parent.oContained
        LinkOfferte(This.Parent.oContained)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oOFSTATUS_1_46 as StdCombo with uid="WJAVJWDSCL",rtseq=25,rtrep=.f.,left=143,top=270,width=115,height=21, enabled=.f.;
    , ToolTipText = "Stato opportunit�/offerta";
    , HelpContextID = 266293817;
    , cFormVar="w_OFSTATUS",RowSource=""+"In corso,"+"Inviata,"+"Confermata,"+"Versione chiusa,"+"Rifiutata,"+"Sospesa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFSTATUS_1_46.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'V',;
    iif(this.value =5,'R',;
    iif(this.value =6,'S',;
    space(1))))))))
  endfunc
  func oOFSTATUS_1_46.GetRadio()
    this.Parent.oContained.w_OFSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oOFSTATUS_1_46.SetRadio()
    this.Parent.oContained.w_OFSTATUS=trim(this.Parent.oContained.w_OFSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_OFSTATUS=='I',1,;
      iif(this.Parent.oContained.w_OFSTATUS=='A',2,;
      iif(this.Parent.oContained.w_OFSTATUS=='C',3,;
      iif(this.Parent.oContained.w_OFSTATUS=='V',4,;
      iif(this.Parent.oContained.w_OFSTATUS=='R',5,;
      iif(this.Parent.oContained.w_OFSTATUS=='S',6,;
      0))))))
  endfunc

  add object oOFNUMDOC_1_47 as StdField with uid="LQFYCLYDMU",rtseq=26,rtrep=.f.,;
    cFormVar = "w_OFNUMDOC", cQueryName = "OFNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 257949143,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=143, Top=238

  add object oOFDATDOC_1_48 as StdField with uid="SCFATDDIMX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_OFDATDOC", cQueryName = "OFDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 251960791,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=433, Top=238

  add object oOFSERDOC_1_49 as StdField with uid="LRTHJETKKI",rtseq=28,rtrep=.f.,;
    cFormVar = "w_OFSERDOC", cQueryName = "OFSERDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 253734359,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=275, Top=238, InputMask=replicate('X',10)


  add object oBtn_1_56 as StdButton with uid="PVRNPREXQE",left=506, top=43, width=48,height=45,;
    CpPicture="bmp\AltreOff.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire l'offerta collegata";
    , HelpContextID = 50438630;
    , caption='\<Offerte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSOF_AOF', 'OFSERIAL', .w_ATOPPOFF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_56.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_ATOPPOFF))
      endwith
    endif
  endfunc


  add object oBtn_1_57 as StdButton with uid="AXAXNIQDQZ",left=558, top=43, width=48,height=45,;
    CpPicture="bmp\ctegorie.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il nominativo collegato";
    , HelpContextID = 264670111;
    , caption='\<Nominat.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSAR_ANO', 'NOCODICE', .w_ATCODNOM)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_57.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_ATCODNOM))
      endwith
    endif
  endfunc


  add object oObj_1_60 as cp_runprogram with uid="IEFCTGMQXB",left=164, top=496, width=336,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSOF_BAI('A')",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , ToolTipText = "Aggiorna\inserisce intestatario";
    , HelpContextID = 34725350


  add object oObj_1_62 as cp_runprogram with uid="WGOTSDYZIK",left=164, top=530, width=155,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSOF_BAI('C')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , ToolTipText = "Aggiorna\inserisce intestatario";
    , HelpContextID = 34725350


  add object oObj_1_63 as cp_runprogram with uid="CBBBMJHJMD",left=353, top=533, width=155,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSOF_BAI('D')",;
    cEvent = "Delete start",;
    nPag=1;
    , ToolTipText = "Elimina partecipante";
    , HelpContextID = 34725350

  add object oATPERSON_1_64 as StdField with uid="FVATKYOVNM",rtseq=31,rtrep=.f.,;
    cFormVar = "w_ATPERSON", cQueryName = "ATPERSON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 2085036,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=303, Top=155, InputMask=replicate('X',60)


  add object oATSTATUS_1_76 as StdCombo with uid="ZNZROMJJUQ",rtseq=43,rtrep=.f.,left=507,top=13,width=99,height=21;
    , ToolTipText = "Stato attivit�";
    , HelpContextID = 266297177;
    , cFormVar="w_ATSTATUS",RowSource=""+"Aperta,"+"Chiusa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATSTATUS_1_76.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oATSTATUS_1_76.GetRadio()
    this.Parent.oContained.w_ATSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oATSTATUS_1_76.SetRadio()
    this.Parent.oContained.w_ATSTATUS=trim(this.Parent.oContained.w_ATSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_ATSTATUS=='A',1,;
      iif(this.Parent.oContained.w_ATSTATUS=='C',2,;
      0))
  endfunc

  func oATSTATUS_1_76.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="XCNTBWTQWS",Visible=.t., Left=33, Top=155,;
    Alignment=1, Width=109, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="EGPNDYECDA",Visible=.t., Left=33, Top=182,;
    Alignment=1, Width=109, Height=18,;
    Caption="Contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="EHJVTZDRGG",Visible=.t., Left=33, Top=209,;
    Alignment=1, Width=109, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="QDGUMTSQPE",Visible=.t., Left=33, Top=128,;
    Alignment=1, Width=109, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="PWAUQUYBQY",Visible=.t., Left=33, Top=103,;
    Alignment=1, Width=109, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="SHXAEVCVKS",Visible=.t., Left=322, Top=104,;
    Alignment=1, Width=153, Height=18,;
    Caption="Priorit� attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="QLWZWVVLGK",Visible=.t., Left=6, Top=15,;
    Alignment=1, Width=136, Height=18,;
    Caption="Data inizio attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="DWHQDTLVLL",Visible=.t., Left=442, Top=15,;
    Alignment=1, Width=64, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="MRHLWBSMWN",Visible=.t., Left=4, Top=41,;
    Alignment=1, Width=138, Height=18,;
    Caption="Data fine attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="NORSJWWDPX",Visible=.t., Left=398, Top=15,;
    Alignment=1, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="CXKRSVQCMU",Visible=.t., Left=225, Top=15,;
    Alignment=1, Width=141, Height=18,;
    Caption="Ora inizio attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="APZVWNKQUA",Visible=.t., Left=398, Top=41,;
    Alignment=1, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="YYAKHNGDYF",Visible=.t., Left=239, Top=41,;
    Alignment=1, Width=127, Height=18,;
    Caption="Ora fine attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="PDAGFAJEHK",Visible=.t., Left=264, Top=238,;
    Alignment=2, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="MSWKBXMGNU",Visible=.t., Left=33, Top=270,;
    Alignment=1, Width=109, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="RHWKDBVNLE",Visible=.t., Left=372, Top=238,;
    Alignment=1, Width=58, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="RJNIIFDFWJ",Visible=.t., Left=33, Top=238,;
    Alignment=1, Width=109, Height=18,;
    Caption="Opportunit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_OFSTATUS<>'I')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="VINNSDQUWW",Visible=.t., Left=33, Top=238,;
    Alignment=1, Width=109, Height=18,;
    Caption="Offerta:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_OFSTATUS='I')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="VCJREPVBEK",Visible=.t., Left=33, Top=103,;
    Alignment=1, Width=109, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (g_AGEN='N')
    endwith
  endfunc
enddefine
define class tgsof_aatPag2 as StdContainer
  Width  = 772
  height = 415
  stdWidth  = 772
  stdheight = 415
  resizeXpos=245
  resizeYpos=332
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATNOTPIA_2_1 as StdMemo with uid="FJYKPZSDQC",rtseq=44,rtrep=.f.,;
    cFormVar = "w_ATNOTPIA", cQueryName = "ATNOTPIA",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note pianificazione",;
    HelpContextID = 49672377,;
   bGlobalFont=.t.,;
    Height=160, Width=597, Left=8, Top=34


  add object oATCODESI_2_2 as StdTableCombo with uid="TVGKQXYXJN",rtseq=45,rtrep=.f.,left=400,top=208,width=205,height=21;
    , ToolTipText = "Esito attivit�";
    , HelpContextID = 17391439;
    , cFormVar="w_ATCODESI",tablefilter="", bObbl = .f. , nPag = 2;
    , cLinkFile="OFF_ESIA";
    , cTable='OFF_ESIA',cKey='ESCODESI',cValue='ESDESCRI',cOrderBy='ESCODESI',xDefault=space(5);
  , bGlobalFont=.t.


  func oATCODESI_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODESI_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oAT_ESITO_2_3 as StdMemo with uid="NVPASHRWOT",rtseq=46,rtrep=.f.,;
    cFormVar = "w_AT_ESITO", cQueryName = "AT_ESITO",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note esito della attivit�",;
    HelpContextID = 99688277,;
   bGlobalFont=.t.,;
    Height=160, Width=597, Left=9, Top=251

  add object oStr_2_4 as StdString with uid="IAPOBNRKVE",Visible=.t., Left=9, Top=230,;
    Alignment=0, Width=184, Height=18,;
    Caption="Note esito"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="LCLBOVZIPX",Visible=.t., Left=205, Top=211,;
    Alignment=1, Width=190, Height=18,;
    Caption="Esito attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="IODNKONVYP",Visible=.t., Left=8, Top=13,;
    Alignment=0, Width=307, Height=18,;
    Caption="Note pianificazione"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_aat','OFF_ATTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ATSERIAL=OFF_ATTI.ATSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsof_aat
*--- Effettua il link sulle offerte, lanciato dal bottone ...
Proc LinkOfferte(oParent)
    l_obj = oParent.GetCtrl('w_ATOPPOFF')
    oParent.bDontReportError=.t.
    public i_lastindirectaction
    i_lastindirectaction='ecpZoom'	
    l_obj.setFocus()
	  l_obj.mzoom()
EndFunc
Proc AbilitaBottone(oParent)
    obj = oParent.GetCtrl(Cp_Translate('\<Offerte'))
    obj.enabled=not empty(oParent.w_ATOPPOFF)
    obj = oParent.GetCtrl(Cp_Translate('\<Nominat.'))
    obj.enabled=not empty(oParent.w_ATCODNOM)
endproc
* --- Fine Area Manuale
