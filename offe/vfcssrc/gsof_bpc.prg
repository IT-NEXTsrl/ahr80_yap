* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bpc                                                        *
*              Aggiorna le picture                                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_302]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-09                                                      *
* Last revis.: 2005-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bpc",oParentObject)
return(i_retval)

define class tgsof_bpc as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli alla Variazione Valuta Modelli Offerte (da GSOF_AMO)
    if this.oParentObject.w_MOCODVAL<>this.oParentObject.o_MOCODVAL
      if NOT EMPTY(this.oParentObject.w_MOCODVAL) AND NOT EMPTY(this.oParentObject.o_MOCODVAL) AND this.oParentObject.w_MOCODVAL<>this.oParentObject.o_MOCODVAL
        if upper(g_Application)="ADHOC REVOLUTION"
          ah_ErrorMsg("Codice valuta modificato; riverificare gli importi delle spese",,"")
        else
          ah_ErrorMsg("Codice valuta modificato; riverificare gli importi delle spese","!","")
        endif
      endif
      this.oParentObject.w_MOCODLIS = SPACE(5)
      this.oParentObject.w_MOSPEINC = 0
      this.oParentObject.w_MOSPEIMB = 0
      this.oParentObject.w_MOSPETRA = 0
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
