* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_ba2                                                        *
*              Aggiorna righe dettaglio offerta                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_50]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-22                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_ba2",oParentObject,m.pTIPOPE)
return(i_retval)

define class tgsof_ba2 as StdBatch
  * --- Local variables
  pTIPOPE = space(1)
  w_RECO = 0
  w_AGGRIG = .f.
  w_OK = .f.
  w_CATSCC = space(5)
  w_MESS = space(10)
  w_GSOF_MDO = .NULL.
  w_CODLIS = space(5)
  o_CODLIS = space(5)
  * --- WorkFile variables
  TAB_SCON_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento righe Dettaglio al Cambio Nominativo, Valuta, Listino (da GSOF_MDO)
    * --- Valore su Cambio: 
    *     V=Valuta
    *     L=Listino Valorizzato  
    *     S=Listino non Valorizzato (Calcola solo gli Sconti)
    *     D=Data Offerta
    this.w_GSOF_MDO = this.oParentObject
    this.w_CATSCC = this.w_GSOF_MDO.oParentObject.w_CATSCC
    this.w_CODLIS = this.w_GSOF_MDO.oParentObject.w_OFCODLIS
    this.o_CODLIS = this.w_GSOF_MDO.oParentObject.o_OFCODLIS
    * --- Memorizzo la posizione sul cursore
    this.w_GSOF_MDO.MarkPos()     
    SELECT (this.w_GSOF_MDO.cTrsName)
    COUNT FOR NOT DELETED() AND NOT EMPTY(t_ODCODICE) TO this.w_RECO
    if g_FLCESC = "S" And this.w_RECO<>0
      * --- Codifica Cliente/Fornitore Esclusiva attiva in Azienda
      ah_ErrorMsg("Attiva gestione codici clienti/fornitori esclusivi%0Sulle righe potrebbero essere presenti codici incongruenti con questo intestatario","i","")
    endif
    if this.pTIPOPE="L" And this.w_CODLIS = this.o_CODLIS
      * --- Se riselezionato lo stesso codice listino da Zoom mi fermo
      *     (in questo caso verrebbe comunque notificato w_OFCODLIS Changed)
      * --- Riposiziono il temporaneo
      this.w_GSOF_MDO.RePos()     
      i_retcode = 'stop'
      return
    endif
    * --- Riaggiorna i Prezzi e il Listino sulle Righe
    this.w_OK = .T.
    if this.w_RECO<>0
      if this.pTIPOPE="S"
        this.w_AGGRIG = .F.
        if this.w_GSOF_MDO.oParentObject.w_OLDSCO="S"
          this.w_OK = ah_YesNo("Aggiorno gli sconti secondo la tabella sconti/maggiorazioni?%0Se le nuove impostazioni non trovano sconti validi,%0verranno mantenuti quelli precedentemente calcolati")
        endif
      else
        if this.pTIPOPE="D"
          this.w_MESS = "Aggiorno anche i prezzi sulle righe?"
        else
          do case
            case Isahe()
              this.w_MESS = "Aggiorno i prezzi sulle righe in base al listino impostato?%0Eventuali sconti / maggiorazioni verranno%0reperiti dalla relativa tabella"
            case Isahr()
              this.w_MESS = "Aggiorno i prezzi sulle righe in base al listino impostato?%0Nel caso il listino non sia gestito a sconti,%0questi verranno ricalcolati da tabella sconti/maggiorazioni"
            otherwise
              this.w_MESS = "Aggiorno le tariffe sulle righe ?"
          endcase
        endif
        this.w_AGGRIG = ah_YesNo(this.w_MESS)
      endif
      if this.w_OK
        SELECT (this.w_GSOF_MDO.cTrsName)
        GO TOP
        SCAN FOR t_CPROWORD<>0 AND t_ODTIPRIG<>" " AND NOT EMPTY(t_ODCODICE) AND NOT DELETED()
        * --- Legge i Dati del Temporaneo
        this.w_GSOF_MDO.WorkFromTrs()     
        this.w_GSOF_MDO.SaveDependsOn()     
        if this.w_AGGRIG OR this.pTIPOPE="V"
          * --- Se riaggiorno i Prezzi o Cambiata la Valuta Ricalcola il Listino/Contratto
          this.oParentObject.w_ODCONTRA = SPACE(15)
        endif
        SELECT (this.w_GSOF_MDO.cTrsName)
        if this.w_AGGRIG
          * --- Forzo il ricalcolo se nel GSVE_BCD viene trovato un prezzo
          this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO+0.0000001
          * --- Ricalcola anche gli Sconti
          this.oParentObject.w_OFLSCO = "C"
          this.w_GSOF_MDO.NotifyEvent("Ricalcola")     
          SELECT (this.w_GSOF_MDO.cTrsName)
        endif
        * --- Ricalcolo da Tabella solo se non ho un contratto sulla riga (LISCON=1)
        if this.pTIPOPE $ "SD" And this.oParentObject.w_LISCON<>1
          * --- Select from TAB_SCON
          i_nConn=i_TableProp[this.TAB_SCON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TAB_SCON_idx,2],.t.,this.TAB_SCON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" TAB_SCON ";
                +" where TSCATCLI="+cp_ToStrODBC(this.w_CATSCC)+" AND TSCATARR="+cp_ToStrODBC(this.oParentObject.w_CATSCA)+" AND TSDATINI<="+cp_ToStrODBC(this.oParentObject.w_DATDOC)+"";
                +" order by TSDATINI";
                 ,"_Curs_TAB_SCON")
          else
            select * from (i_cTable);
             where TSCATCLI=this.w_CATSCC AND TSCATARR=this.oParentObject.w_CATSCA AND TSDATINI<=this.oParentObject.w_DATDOC;
             order by TSDATINI;
              into cursor _Curs_TAB_SCON
          endif
          if used('_Curs_TAB_SCON')
            select _Curs_TAB_SCON
            locate for 1=1
            do while not(eof())
            if CP_TODATE(_Curs_TAB_SCON.TSDATFIN)>=this.oParentObject.w_DATDOC OR EMPTY(CP_TODATE(_Curs_TAB_SCON.TSDATFIN))
              this.oParentObject.w_ODSCONT1 = _Curs_TAB_SCON.TSSCONT1
              this.oParentObject.w_ODSCONT2 = _Curs_TAB_SCON.TSSCONT2
              this.oParentObject.w_ODSCONT3 = _Curs_TAB_SCON.TSSCONT3
              this.oParentObject.w_ODSCONT4 = _Curs_TAB_SCON.TSSCONT4
            endif
              select _Curs_TAB_SCON
              continue
            enddo
            use
          endif
        endif
        * --- Carica il Temporaneo dei Dati
        this.w_GSOF_MDO.TrsFromWork()     
        * --- Flag Notifica Riga Variata
        if i_SRV<>"A"
          replace i_SRV with "U"
        endif
        ENDSCAN
        * --- Riposiziono il temporaneo
        this.w_GSOF_MDO.RePos()     
      endif
    endif
  endproc


  proc Init(oParentObject,pTIPOPE)
    this.pTIPOPE=pTIPOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TAB_SCON'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_TAB_SCON')
      use in _Curs_TAB_SCON
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE"
endproc
