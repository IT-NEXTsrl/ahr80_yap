* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_ba1                                                        *
*              Gestione eventi da offerte                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_57]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-03                                                      *
* Last revis.: 2012-06-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_ba1",oParentObject,m.pTIPOPE)
return(i_retval)

define class tgsof_ba1 as StdBatch
  * --- Local variables
  pTIPOPE = space(1)
  w_OLDLIS = space(5)
  w_OBJMDO = .NULL.
  w_OBJMSO = .NULL.
  w_PADRE = .NULL.
  w_OLDVALUE = space(1)
  w_CTRL = .NULL.
  * --- WorkFile variables
  AGENTI_idx=0
  LISTINI_idx=0
  PAG_AMEN_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione >Eventi al Cambio: A=Agente ; P=Pagamento V=Valuta ; L=Listino ; X=Edit Started, Load (da GSOF_AOF)
    this.w_PADRE = This.oParentObject
    if Isalt()
      this.w_OBJMSO = this.w_PADRE.GSPR_MSO
      this.w_OBJMDO = this.w_PADRE.GSPR_MDO
    else
      this.w_OBJMDO = this.w_PADRE.GSOF_MDO
      this.w_OBJMSO = this.w_PADRE.GSOF_MSO
    endif
    this.w_OLDLIS = this.oParentObject.o_OFCODLIS
    do case
      case this.pTIPOPE="A"
        if NOT EMPTY(this.oParentObject.w_CODCLI)
          * --- Leggo eventuale agente legato al cliente...
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODAG1"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCLI);
                  +" and ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCLI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODAG1;
              from (i_cTable) where;
                  ANCODICE = this.oParentObject.w_CODCLI;
                  and ANTIPCON = this.oParentObject.w_TIPCLI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_OFCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Aggiorno i dati legati al cliente associato al nominativo..
          this.w_PADRE.mCalc(.T.)     
          * --- Se Nominativo anche cliente imposta Listino, Sconti Globali e Pagamento
          this.oParentObject.w_OFCODLIS = IIF(NOT EMPTY(this.oParentObject.w_LISCLI), this.oParentObject.w_LISCLI, IIF(EMPTY(this.oParentObject.w_LISMOD), this.oParentObject.w_OFCODLIS, this.oParentObject.w_LISMOD))
          this.oParentObject.w_OFCODPAG = IIF(NOT EMPTY(this.oParentObject.w_PAGCLI), this.oParentObject.w_PAGCLI, IIF(EMPTY(this.oParentObject.w_PAGMOD), this.oParentObject.w_OFCODPAG, this.oParentObject.w_PAGMOD))
          this.oParentObject.w_OFSCOCL1 = this.oParentObject.w_SC1CLI
          this.oParentObject.w_OFSCOCL2 = this.oParentObject.w_SC2CLI
        else
          * --- Se nominativo non ha cliente associato leggo il pagamento dall
          *     modello offerte..
          *     e il codice agente legato al nominativo
          this.oParentObject.w_OFCODLIS = IIF(NOT EMPTY(this.oParentObject.w_LISNOM), this.oParentObject.w_LISNOM, IIF(EMPTY(this.oParentObject.w_LISMOD), this.oParentObject.w_OFCODLIS, this.oParentObject.w_LISMOD))
          this.oParentObject.w_OFCODPAG = IIF(NOT EMPTY(this.oParentObject.w_NOCODPAG), this.oParentObject.w_NOCODPAG, IIF(EMPTY(this.oParentObject.w_PAGMOD), this.oParentObject.w_OFCODPAG, this.oParentObject.w_PAGMOD))
          this.oParentObject.w_OFCODAGE = this.oParentObject.w_CODAGE
        endif
        if !Isalt()
          * --- Valorizzo listino..
          setvaluelinked ( "M" , this.w_PADRE , "w_OFCODLIS" , this.oParentObject.w_OFCODLIS)
          * --- Valorizzo l'agente..
          this.w_CTRL = this.w_PADRE.GetCtrl("w_OFCODAGE")
          this.w_CTRL.bUpd = .T.
          this.w_CTRL.Value = this.oParentObject.w_OFCODAGE
          this.w_CTRL.Valid()     
        endif
        this.w_PADRE.mCalc(.T.)     
        if Not Empty( this.oParentObject.w_OFCODPAG )
          * --- Valorizzo pagamento..
          this.w_CTRL = this.w_PADRE.GetCtrl("w_OFCODPAG")
          this.w_CTRL.Value = this.oParentObject.w_OFCODPAG
          this.w_CTRL.bUpd = .T.
          * --- Forzo il valore della globale relativa alla configurazione interfaccia
          *     per non far scattare lo zoom dei pagamenti dopo aver inserito
          *     il nominativo. La valid faceva scattare il controllo e partiva lo zoom.
          *     Viene salvato il vecchio valore, forzato a 'N' e quindi rimesso come era.
          this.w_OLDVALUE = g_RICPERCONT
          g_RICPERCONT="N"
          this.w_CTRL.Valid()     
          g_RICPERCONT=this.w_OLDVALUE
        endif
        * --- Cambio Nominativo ; Refresh sui dati Dettaglio Offerta
        this.w_OBJMDO.mCalc(.T.)     
      case this.pTIPOPE $ "LV"
        * --- Cambio Valuta o Listino ; Refresh sui dati Dettaglio Offerta
        this.w_PADRE.mCalc(.T.)     
        this.w_OBJMDO.mCalc(.T.)     
      case this.pTIPOPE="P"
        * --- Cambio Codice Pagamento
        this.oParentObject.w_OFSCOPAG = this.oParentObject.w_SCOPAG
        this.oParentObject.w_OFSPEINC = IIF(this.oParentObject.w_OFCODVAL=this.oParentObject.w_VALINC, this.oParentObject.w_SPEINC, IIF(this.oParentObject.w_OFCODVAL=this.oParentObject.w_VALIN2, this.oParentObject.w_SPEIN2, 0))
      case this.pTIPOPE="X"
        * --- Init
        this.w_PADRE.mCalc(.T.)     
        this.w_OBJMDO.mCalc(.T.)     
        if ! Isalt()
          this.w_OBJMSO.mCalc(.T.)     
        endif
    endcase
    * --- Aggiorna Righe Dettaglio 
    do case
      case this.pTIPOPE="V" AND NOT EMPTY(this.oParentObject.w_OFCODVAL)
        this.w_OBJMDO.NotifyEvent("AggiornaRighe2")     
      case this.pTIPOPE="A" AND NOT EMPTY(this.oParentObject.w_CODCLI)
        this.w_OBJMDO.NotifyEvent("AggiornaRighe1")     
      case this.pTIPOPE="L" 
        if EMPTY(this.oParentObject.w_OFCODLIS)
          if NOT EMPTY(this.w_OLDLIS) AND NOT EMPTY(this.oParentObject.w_CATSCC)
            * --- Read from LISTINI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LISTINI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LSFLSCON"+;
                " from "+i_cTable+" LISTINI where ";
                    +"LSCODLIS = "+cp_ToStrODBC(this.w_OLDLIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LSFLSCON;
                from (i_cTable) where;
                    LSCODLIS = this.w_OLDLIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_OLDSCO = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_OBJMDO.NotifyEvent("AggiornaRighe3")     
          endif
        else
          this.w_OBJMDO.NotifyEvent("AggiornaRighe1")     
        endif
    endcase
  endproc


  proc Init(oParentObject,pTIPOPE)
    this.pTIPOPE=pTIPOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='LISTINI'
    this.cWorkTables[3]='PAG_AMEN'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE"
endproc
