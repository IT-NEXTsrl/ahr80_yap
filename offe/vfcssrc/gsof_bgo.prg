* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bgo                                                        *
*              Eventi da gestione offerte                                      *
*                                                                              *
*      Author: Iorio Nunzio                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_29]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-05                                                      *
* Last revis.: 2010-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bgo",oParentObject,m.Azione)
return(i_retval)

define class tgsof_bgo as StdBatch
  * --- Local variables
  Azione = space(10)
  GestALL = .NULL.
  TMPc = space(10)
  pop = space(100)
  w_TIPALL = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Gestione Offerte (da GSOF_AOF)
    if Empty(this.oParentObject.w_SERALL) And this.Azione<>"CARICA_ALLEGATO"
      ah_ErrorMsg("Selezionare un allegato",,"")
      i_retcode = 'stop'
      return
    endif
    * --- CARICA_ALLEGATO = Inserisce un Nuovo Allegato
    do case
      case this.Azione="CARICA_ALLEGATO"
        * --- Nuovo Allegato
        this.GestALL = GSOF_AAL(this.oParentObject.w_OFSERIAL, "O"," ",this.oParentObject.w_OFNUMDOC,this.oParentObject.w_OFSERDOC,this.oParentObject.w_OFDATDOC,this.oParentObject.w_OFNUMVER)
        * --- Controllo se ha passato il test di accesso 
        if !(this.GestALL.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Lancia Caricamento ...
        this.GestALL.ECPLoad()     
        this.GestALL.w_ALTIPORI = "O"
        this.GestALL.w_ALRIFOFF = this.oParentObject.w_OFSERIAL
        this.GestALL.mCalc(.T.)     
      case this.Azione $"VARIA_ALLEGATO|ELIMINA_ALLEGATO"
        this.GestALL = GSOF_AAL(this.oParentObject.w_OFSERIAL, "O"," ",this.oParentObject.w_OFNUMDOC,this.oParentObject.w_OFSERDOC,this.oParentObject.w_OFDATDOC,this.oParentObject.w_OFNUMVER)
        * --- Controllo se ha passato il test di accesso 
        if !(this.GestALL.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carica record e lascia in interrograzione ...
        this.GestALL.w_ALSERIAL = this.oParentObject.w_SERALL
        this.TMPc = "ALSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERALL)
        this.GestALL.QueryKeySet(this.TmpC,"")     
        this.GestALL.LoadRecWarn()     
        do case
          case this.Azione = "VARIA_ALLEGATO"
            * --- Modifica record
            this.GestALL.ECPEdit()     
          case this.Azione = "ELIMINA_ALLEGATO"
            * --- Cancella record ...
            this.GestALL.ECPDelete()     
        endcase
      case this.Azione = "APRI_ALLEGATO"
        * --- Apri file ...
        this.pop = ofopenfile(ALLTRIM(this.oParentObject.w_PATALL), ALLTRIM(this.oParentObject.w_SERALL),"O",ALLTRIM(this.oParentObject.w_OFCODNOM))
    endcase
  endproc


  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
