* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bcs                                                        *
*              Calcola scadenze                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134][VRS_28]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-28                                                      *
* Last revis.: 2002-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bcs",oParentObject)
return(i_retval)

define class tgsof_bcs as StdBatch
  * --- Local variables
  w_MESISCA = 0
  w_GIOR = 0
  w_MESE = 0
  w_ANNO = 0
  w_DATRIF = ctod("  /  /  ")
  w_OK = .f.
  w_CGIOR = space(2)
  w_CMESE = space(2)
  w_CANNO = space(4)
  w_APPO = 0
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola data Scadenza (da GSOF_AOF)
    this.w_PADRE = this.oParentObject
    this.w_DATRIF = this.oParentObject.w_OFDATDOC
    this.w_MESISCA = this.oParentObject.w_OFGIOSCA / 30
    if this.w_MESISCA = INT(this.w_MESISCA)
      * --- Calcola data Scadenza a 30 giorni o multipli (Cambia Giorno se Tipo Pag.="DD")
      this.w_GIOR = DAY(this.w_DATRIF)
      this.w_MESE = MONTH(this.w_DATRIF) + this.w_MESISCA
      this.w_ANNO = YEAR(this.w_DATRIF)
    else
      * --- Calcola data Scadenza x rate diverse da 30 giorni o multipli
      this.w_GIOR = DAY(this.w_DATRIF + this.oParentObject.w_OFGIOSCA)
      this.w_MESE = MONTH(this.w_DATRIF + this.oParentObject.w_OFGIOSCA)
      this.w_ANNO = YEAR(this.w_DATRIF + this.oParentObject.w_OFGIOSCA)
    endif
    * --- Se > 12 scatta all'Anno successivo 
    do while this.w_MESE>12
      this.w_MESE = this.w_MESE - 12
      this.w_ANNO = this.w_ANNO + 1
    enddo
    * --- Calcola giorno Pagamento Fine Mese
    if this.oParentObject.w_OFINISCA = "FM" OR this.oParentObject.w_OFINISCA = "FF" 
      * --- Sposta tutto all primo giorno del mese successivo
      this.w_MESE = this.w_MESE + 1
      * --- Se > 12 scatta all'anno successivo 
      do while this.w_MESE>12
        this.w_MESE = this.w_MESE - 12
        this.w_ANNO = this.w_ANNO + 1
      enddo
      this.w_CMESE = RIGHT("00"+ALLTRIM(STR(this.w_MESE)),2)
      this.w_CANNO = RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)),4)
      * --- Quindi toglie un giorno per tornare alla Fine del mese calcolato
      this.w_APPO = cp_CharToDate("01-" + this.w_CMESE + "-" + this.w_CANNO) - 1
      this.w_GIOR = DAY(this.w_APPO)
      this.w_MESE = MONTH(this.w_APPO)
      this.w_ANNO = YEAR(this.w_APPO)
    endif
    * --- Se Giorno Fisso cambia giorno (ev.te sposta avanti di un mese)
    if this.oParentObject.w_OFINISCA="GF" OR (this.oParentObject.w_OFINISCA="FM" AND this.oParentObject.w_OFGIOFIS<>0)
      this.w_MESE = iif(this.oParentObject.w_OFGIOFIS<this.w_GIOR, this.w_MESE+1, this.w_MESE)
      do while this.w_MESE>12
        this.w_MESE = this.w_MESE - 12
        this.w_ANNO = this.w_ANNO + 1
      enddo
      this.w_GIOR = this.oParentObject.w_OFGIOFIS
    endif
    * --- Verifica se il giorno e' congruente
    this.w_CMESE = RIGHT("00" + ALLTRIM(STR(this.w_MESE)), 2)
    this.w_CANNO = RIGHT("0000" + ALLTRIM(STR(this.w_ANNO)), 4)
    if this.w_CMESE $ "02-04-06-09-11" .and. this.w_GIOR > 28
      * --- Trova l'Ultimo giorno del mese
      this.w_CGIOR = DAY(cp_CharToDate("01-" + RIGHT("00"+ALLTRIM(STR(this.w_MESE+1)),2) + "-" + this.w_CANNO) - 1)
      * --- Se giorno > fine mese calcola a fine mese
      this.w_GIOR = IIF(this.w_GIOR > this.w_CGIOR, this.w_CGIOR, this.w_GIOR)
    endif
    this.w_CGIOR = RIGHT("00" + ALLTRIM(STR(this.w_GIOR)), 2)
    * --- Calcola data Scadenza
    this.oParentObject.w_OFDATSCA = cp_CharToDate(this.w_CGIOR + "-" + this.w_CMESE + "-" + this.w_CANNO)
    this.oParentObject.w_OFDATRIC = this.oParentObject.w_OFDATSCA
    * --- Se cambio data Refresh sui dati Dettaglio Offerta
    this.w_PADRE.mCalc(.T.)     
    this.w_PADRE.GSOF_MDO.mCalc(.T.)     
    this.w_PADRE.GSOF_MDO.NotifyEvent("AggiornaRighe4")     
    this.oParentObject.o_OFCODVAL = this.oParentObject.w_OFCODVAL
    this.oParentObject.o_OFDATDOC = this.oParentObject.w_OFDATDOC
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
