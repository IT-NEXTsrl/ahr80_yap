* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_mdo                                                        *
*              Dettaglio offerte                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_296]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2014-07-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsof_mdo")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsof_mdo")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsof_mdo")
  return

* --- Class definition
define class tgsof_mdo as StdPCForm
  Width  = 809
  Height = 378
  Top    = 7
  Left   = 3
  cComment = "Dettaglio offerte"
  cPrg = "gsof_mdo"
  HelpContextID=188077719
  add object cnt as tcgsof_mdo
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsof_mdo as PCContext
  w_ODSERIAL = space(10)
  w_ODCODICE = space(20)
  w_CPROWORD = 0
  w_NUMSCO = 0
  w_DECTOT = 0
  w_IVALIS = space(1)
  w_DECUNI = 0
  w_CODCLI = space(15)
  w_CATCOM = space(5)
  w_FLSCOR = space(1)
  w_CATSCC = space(5)
  w_DATDOC = space(8)
  w_CODLIS = space(5)
  w_SCOLIS = space(1)
  w_CODVAL = space(5)
  w_CODMAG = space(5)
  w_ODTIPRIG = space(1)
  w_ODCODART = space(20)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_OPERA3 = space(1)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_ODNOTAGG = space(10)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_FLUSEP = space(1)
  w_FLSERG = space(1)
  w_FLSERA = space(1)
  w_GRUMER = space(5)
  w_CATSCA = space(5)
  w_ODCODGRU = space(5)
  w_ODCODSOT = space(5)
  w_ODDESART = space(40)
  w_ODUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_ODQTAMOV = 0
  w_ODFLOMAG = space(1)
  w_ODARNOCO = space(1)
  w_ODQTAUM1 = 0
  w_LIPREZZO = 0
  w_ODPREZZO = 0
  w_ODSCONT1 = 0
  w_ODSCONT2 = 0
  w_ODSCONT3 = 0
  w_ODSCONT4 = 0
  w_ODDATPCO = space(8)
  w_ODVALRIG = 0
  w_RIGNET = 0
  w_RIGNET1 = 0
  w_TOTRIG = 0
  w_TOTMERCE = 0
  w_TOTRIPD = 0
  w_ODCONTRA = space(15)
  w_OFLSCO = space(1)
  w_ARTNOC = space(1)
  w_OGG = space(10)
  w_CALCPICT = 0
  w_CALCPICU = 0
  w_LISCON = 0
  w_PRZVAC = space(1)
  w_IVASCOR = 0
  w_ACTSCOR = space(1)
  w_OBTEST = space(8)
  w_CODESC = space(5)
  w_SIMVAL = space(5)
  w_GIACON = space(1)
  w_ODTIPPRO = space(2)
  w_ODTIPPR2 = space(2)
  w_ODPERPRO = 0
  w_ODPROCAP = 0
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(10)
  w_CLUNIMIS = space(3)
  w_PREZUM = space(1)
  w_UMCAL = space(14)
  proc Save(i_oFrom)
    this.w_ODSERIAL = i_oFrom.w_ODSERIAL
    this.w_ODCODICE = i_oFrom.w_ODCODICE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_NUMSCO = i_oFrom.w_NUMSCO
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_IVALIS = i_oFrom.w_IVALIS
    this.w_DECUNI = i_oFrom.w_DECUNI
    this.w_CODCLI = i_oFrom.w_CODCLI
    this.w_CATCOM = i_oFrom.w_CATCOM
    this.w_FLSCOR = i_oFrom.w_FLSCOR
    this.w_CATSCC = i_oFrom.w_CATSCC
    this.w_DATDOC = i_oFrom.w_DATDOC
    this.w_CODLIS = i_oFrom.w_CODLIS
    this.w_SCOLIS = i_oFrom.w_SCOLIS
    this.w_CODVAL = i_oFrom.w_CODVAL
    this.w_CODMAG = i_oFrom.w_CODMAG
    this.w_ODTIPRIG = i_oFrom.w_ODTIPRIG
    this.w_ODCODART = i_oFrom.w_ODCODART
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_CODIVA = i_oFrom.w_CODIVA
    this.w_PERIVA = i_oFrom.w_PERIVA
    this.w_ODNOTAGG = i_oFrom.w_ODNOTAGG
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_FLUSEP = i_oFrom.w_FLUSEP
    this.w_FLSERG = i_oFrom.w_FLSERG
    this.w_FLSERA = i_oFrom.w_FLSERA
    this.w_GRUMER = i_oFrom.w_GRUMER
    this.w_CATSCA = i_oFrom.w_CATSCA
    this.w_ODCODGRU = i_oFrom.w_ODCODGRU
    this.w_ODCODSOT = i_oFrom.w_ODCODSOT
    this.w_ODDESART = i_oFrom.w_ODDESART
    this.w_ODUNIMIS = i_oFrom.w_ODUNIMIS
    this.w_FLFRAZ = i_oFrom.w_FLFRAZ
    this.w_ODQTAMOV = i_oFrom.w_ODQTAMOV
    this.w_ODFLOMAG = i_oFrom.w_ODFLOMAG
    this.w_ODARNOCO = i_oFrom.w_ODARNOCO
    this.w_ODQTAUM1 = i_oFrom.w_ODQTAUM1
    this.w_LIPREZZO = i_oFrom.w_LIPREZZO
    this.w_ODPREZZO = i_oFrom.w_ODPREZZO
    this.w_ODSCONT1 = i_oFrom.w_ODSCONT1
    this.w_ODSCONT2 = i_oFrom.w_ODSCONT2
    this.w_ODSCONT3 = i_oFrom.w_ODSCONT3
    this.w_ODSCONT4 = i_oFrom.w_ODSCONT4
    this.w_ODDATPCO = i_oFrom.w_ODDATPCO
    this.w_ODVALRIG = i_oFrom.w_ODVALRIG
    this.w_RIGNET = i_oFrom.w_RIGNET
    this.w_RIGNET1 = i_oFrom.w_RIGNET1
    this.w_TOTRIG = i_oFrom.w_TOTRIG
    this.w_TOTMERCE = i_oFrom.w_TOTMERCE
    this.w_TOTRIPD = i_oFrom.w_TOTRIPD
    this.w_ODCONTRA = i_oFrom.w_ODCONTRA
    this.w_OFLSCO = i_oFrom.w_OFLSCO
    this.w_ARTNOC = i_oFrom.w_ARTNOC
    this.w_OGG = i_oFrom.w_OGG
    this.w_CALCPICT = i_oFrom.w_CALCPICT
    this.w_CALCPICU = i_oFrom.w_CALCPICU
    this.w_LISCON = i_oFrom.w_LISCON
    this.w_PRZVAC = i_oFrom.w_PRZVAC
    this.w_IVASCOR = i_oFrom.w_IVASCOR
    this.w_ACTSCOR = i_oFrom.w_ACTSCOR
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CODESC = i_oFrom.w_CODESC
    this.w_SIMVAL = i_oFrom.w_SIMVAL
    this.w_GIACON = i_oFrom.w_GIACON
    this.w_ODTIPPRO = i_oFrom.w_ODTIPPRO
    this.w_ODTIPPR2 = i_oFrom.w_ODTIPPR2
    this.w_ODPERPRO = i_oFrom.w_ODPERPRO
    this.w_ODPROCAP = i_oFrom.w_ODPROCAP
    this.w_FLFRAZ1 = i_oFrom.w_FLFRAZ1
    this.w_MODUM2 = i_oFrom.w_MODUM2
    this.w_CLUNIMIS = i_oFrom.w_CLUNIMIS
    this.w_PREZUM = i_oFrom.w_PREZUM
    this.w_UMCAL = i_oFrom.w_UMCAL
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ODSERIAL = this.w_ODSERIAL
    i_oTo.w_ODCODICE = this.w_ODCODICE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_NUMSCO = this.w_NUMSCO
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_IVALIS = this.w_IVALIS
    i_oTo.w_DECUNI = this.w_DECUNI
    i_oTo.w_CODCLI = this.w_CODCLI
    i_oTo.w_CATCOM = this.w_CATCOM
    i_oTo.w_FLSCOR = this.w_FLSCOR
    i_oTo.w_CATSCC = this.w_CATSCC
    i_oTo.w_DATDOC = this.w_DATDOC
    i_oTo.w_CODLIS = this.w_CODLIS
    i_oTo.w_SCOLIS = this.w_SCOLIS
    i_oTo.w_CODVAL = this.w_CODVAL
    i_oTo.w_CODMAG = this.w_CODMAG
    i_oTo.w_ODTIPRIG = this.w_ODTIPRIG
    i_oTo.w_ODCODART = this.w_ODCODART
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_CODIVA = this.w_CODIVA
    i_oTo.w_PERIVA = this.w_PERIVA
    i_oTo.w_ODNOTAGG = this.w_ODNOTAGG
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_FLUSEP = this.w_FLUSEP
    i_oTo.w_FLSERG = this.w_FLSERG
    i_oTo.w_FLSERA = this.w_FLSERA
    i_oTo.w_GRUMER = this.w_GRUMER
    i_oTo.w_CATSCA = this.w_CATSCA
    i_oTo.w_ODCODGRU = this.w_ODCODGRU
    i_oTo.w_ODCODSOT = this.w_ODCODSOT
    i_oTo.w_ODDESART = this.w_ODDESART
    i_oTo.w_ODUNIMIS = this.w_ODUNIMIS
    i_oTo.w_FLFRAZ = this.w_FLFRAZ
    i_oTo.w_ODQTAMOV = this.w_ODQTAMOV
    i_oTo.w_ODFLOMAG = this.w_ODFLOMAG
    i_oTo.w_ODARNOCO = this.w_ODARNOCO
    i_oTo.w_ODQTAUM1 = this.w_ODQTAUM1
    i_oTo.w_LIPREZZO = this.w_LIPREZZO
    i_oTo.w_ODPREZZO = this.w_ODPREZZO
    i_oTo.w_ODSCONT1 = this.w_ODSCONT1
    i_oTo.w_ODSCONT2 = this.w_ODSCONT2
    i_oTo.w_ODSCONT3 = this.w_ODSCONT3
    i_oTo.w_ODSCONT4 = this.w_ODSCONT4
    i_oTo.w_ODDATPCO = this.w_ODDATPCO
    i_oTo.w_ODVALRIG = this.w_ODVALRIG
    i_oTo.w_RIGNET = this.w_RIGNET
    i_oTo.w_RIGNET1 = this.w_RIGNET1
    i_oTo.w_TOTRIG = this.w_TOTRIG
    i_oTo.w_TOTMERCE = this.w_TOTMERCE
    i_oTo.w_TOTRIPD = this.w_TOTRIPD
    i_oTo.w_ODCONTRA = this.w_ODCONTRA
    i_oTo.w_OFLSCO = this.w_OFLSCO
    i_oTo.w_ARTNOC = this.w_ARTNOC
    i_oTo.w_OGG = this.w_OGG
    i_oTo.w_CALCPICT = this.w_CALCPICT
    i_oTo.w_CALCPICU = this.w_CALCPICU
    i_oTo.w_LISCON = this.w_LISCON
    i_oTo.w_PRZVAC = this.w_PRZVAC
    i_oTo.w_IVASCOR = this.w_IVASCOR
    i_oTo.w_ACTSCOR = this.w_ACTSCOR
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CODESC = this.w_CODESC
    i_oTo.w_SIMVAL = this.w_SIMVAL
    i_oTo.w_GIACON = this.w_GIACON
    i_oTo.w_ODTIPPRO = this.w_ODTIPPRO
    i_oTo.w_ODTIPPR2 = this.w_ODTIPPR2
    i_oTo.w_ODPERPRO = this.w_ODPERPRO
    i_oTo.w_ODPROCAP = this.w_ODPROCAP
    i_oTo.w_FLFRAZ1 = this.w_FLFRAZ1
    i_oTo.w_MODUM2 = this.w_MODUM2
    i_oTo.w_CLUNIMIS = this.w_CLUNIMIS
    i_oTo.w_PREZUM = this.w_PREZUM
    i_oTo.w_UMCAL = this.w_UMCAL
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsof_mdo as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 809
  Height = 378
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-29"
  HelpContextID=188077719
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=78

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  OFF_DETT_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  TIT_SEZI_IDX = 0
  SOT_SEZI_IDX = 0
  VOCIIVA_IDX = 0
  CONTI_IDX = 0
  cFile = "OFF_DETT"
  cKeySelect = "ODSERIAL"
  cKeyWhere  = "ODSERIAL=this.w_ODSERIAL"
  cKeyDetail  = "ODSERIAL=this.w_ODSERIAL"
  cKeyWhereODBC = '"ODSERIAL="+cp_ToStrODBC(this.w_ODSERIAL)';

  cKeyDetailWhereODBC = '"ODSERIAL="+cp_ToStrODBC(this.w_ODSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"OFF_DETT.ODSERIAL="+cp_ToStrODBC(this.w_ODSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'OFF_DETT.CPROWORD '
  cPrg = "gsof_mdo"
  cComment = "Dettaglio offerte"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 14
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ODSERIAL = space(10)
  w_ODCODICE = space(20)
  o_ODCODICE = space(20)
  w_CPROWORD = 0
  w_NUMSCO = 0
  w_DECTOT = 0
  o_DECTOT = 0
  w_IVALIS = space(1)
  w_DECUNI = 0
  o_DECUNI = 0
  w_CODCLI = space(15)
  w_CATCOM = space(5)
  w_FLSCOR = space(1)
  w_CATSCC = space(5)
  w_DATDOC = ctod('  /  /  ')
  w_CODLIS = space(5)
  w_SCOLIS = space(1)
  w_CODVAL = space(5)
  w_CODMAG = space(5)
  w_ODTIPRIG = space(1)
  w_ODCODART = space(20)
  o_ODCODART = space(20)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_OPERA3 = space(1)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_ODNOTAGG = space(0)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_FLUSEP = space(1)
  w_FLSERG = space(1)
  w_FLSERA = space(1)
  w_GRUMER = space(5)
  w_CATSCA = space(5)
  w_ODCODGRU = space(5)
  o_ODCODGRU = space(5)
  w_ODCODSOT = space(5)
  w_ODDESART = space(40)
  w_ODUNIMIS = space(3)
  o_ODUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_ODQTAMOV = 0
  o_ODQTAMOV = 0
  w_ODFLOMAG = space(1)
  o_ODFLOMAG = space(1)
  w_ODARNOCO = space(1)
  w_ODQTAUM1 = 0
  w_LIPREZZO = 0
  o_LIPREZZO = 0
  w_ODPREZZO = 0
  o_ODPREZZO = 0
  w_ODSCONT1 = 0
  o_ODSCONT1 = 0
  w_ODSCONT2 = 0
  o_ODSCONT2 = 0
  w_ODSCONT3 = 0
  o_ODSCONT3 = 0
  w_ODSCONT4 = 0
  o_ODSCONT4 = 0
  w_ODDATPCO = ctod('  /  /  ')
  w_ODVALRIG = 0
  w_RIGNET = 0
  w_RIGNET1 = 0
  w_TOTRIG = 0
  w_TOTMERCE = 0
  w_TOTRIPD = 0
  w_ODCONTRA = space(15)
  w_OFLSCO = space(1)
  w_ARTNOC = space(1)
  w_OGG = space(10)
  w_CALCPICT = 0
  w_CALCPICU = 0
  w_LISCON = 0
  w_PRZVAC = space(1)
  w_IVASCOR = 0
  w_ACTSCOR = .F.
  w_OBTEST = ctod('  /  /  ')
  w_CODESC = space(5)
  w_SIMVAL = space(5)
  w_GIACON = space(1)
  w_ODTIPPRO = space(2)
  w_ODTIPPR2 = space(2)
  w_ODPERPRO = 0
  w_ODPROCAP = 0
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(10)
  w_CLUNIMIS = space(3)
  w_PREZUM = space(1)
  w_UMCAL = space(14)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_mdoPag1","gsof_mdo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='TIT_SEZI'
    this.cWorkTables[5]='SOT_SEZI'
    this.cWorkTables[6]='VOCIIVA'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='OFF_DETT'
    * --- Area Manuale = Open Work Table
    * --- gsof_mdo
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.DynamicBackColor= ;
         "IIF(t_ODARNOCO=1,RGB(255,255,0), RGB(255,255,255))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OFF_DETT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OFF_DETT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsof_mdo'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_23_joined
    link_2_23_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from OFF_DETT where ODSERIAL=KeySet.ODSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2],this.bLoadRecFilter,this.OFF_DETT_IDX,"gsof_mdo")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OFF_DETT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OFF_DETT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OFF_DETT '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_23_joined=this.AddJoinedLink_2_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ODSERIAL',this.w_ODSERIAL  )
      select * from (i_cTable) OFF_DETT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTRIG = 0
        .w_TOTMERCE = 0
        .w_TOTRIPD = 0
        .w_OGG = SPACE(10)
        .w_CODESC = space(5)
        .w_ODSERIAL = NVL(ODSERIAL,space(10))
        .w_NUMSCO = this.oParentObject .w_NUMSCO
        .w_DECTOT = this.oParentObject .w_DECTOT
        .w_IVALIS = this.oParentObject .w_IVALIS
        .w_DECUNI = this.oParentObject .w_DECUNI
        .w_CODCLI = this.oParentObject .w_CODCLI
          .link_1_6('Load')
        .w_CATCOM = this.oParentObject .w_CATCOM
        .w_FLSCOR = this.oParentObject .w_FLSCOR
        .w_CATSCC = this.oParentObject .w_CATSCC
        .w_DATDOC = this.oParentObject .w_OFDATDOC
        .w_CODLIS = this.oParentObject .w_OFCODLIS
        .w_SCOLIS = this.oParentObject .w_SCOLIS
        .w_CODVAL = this.oParentObject .w_OFCODVAL
        .w_CODMAG = this.oParentObject .w_CODMAG
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_PRZVAC = this.oParentObject .w_PRZVAC
        .w_SIMVAL = this.oParentObject .w_SIMVAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'OFF_DETT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTRIG = 0
      this.w_TOTMERCE = 0
      this.w_TOTRIPD = 0
      scan
        with this
          .w_UNMIS3 = space(3)
          .w_MOLTI3 = 0
          .w_OPERA3 = space(1)
          .w_CODIVA = space(5)
          .w_PERIVA = 0
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_MOLTIP = 0
          .w_OPERAT = space(1)
          .w_FLUSEP = space(1)
          .w_FLSERG = space(1)
          .w_FLSERA = space(1)
          .w_GRUMER = space(5)
          .w_CATSCA = space(5)
          .w_FLFRAZ = space(1)
          .w_LIPREZZO = 0
          .w_LISCON = 0
          .w_IVASCOR = 0
        .w_ACTSCOR = .F.
        .w_GIACON = IIF(.cFunction='Load', ' ', 'X')
          .w_FLFRAZ1 = space(1)
          .w_MODUM2 = space(10)
          .w_CLUNIMIS = space(3)
          .w_PREZUM = space(1)
          .w_UMCAL = space(14)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_ODCODICE = NVL(ODCODICE,space(20))
          if link_2_1_joined
            this.w_ODCODICE = NVL(CACODICE201,NVL(this.w_ODCODICE,space(20)))
            this.w_ODCODART = NVL(CACODART201,space(20))
            this.w_ODDESART = NVL(CADESART201,space(40))
            this.w_UNMIS3 = NVL(CAUNIMIS201,space(3))
            this.w_MOLTI3 = NVL(CAMOLTIP201,0)
            this.w_OPERA3 = NVL(CAOPERAT201,space(1))
            this.w_ODTIPRIG = NVL(CA__TIPO201,space(1))
            this.w_ODNOTAGG = NVL(CADESSUP201,space(0))
          else
          .link_2_1('Load')
          endif
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_ODTIPRIG = NVL(ODTIPRIG,space(1))
          .w_ODCODART = NVL(ODCODART,space(20))
          if link_2_4_joined
            this.w_ODCODART = NVL(ARCODART204,NVL(this.w_ODCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1204,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2204,space(3))
            this.w_FLSERG = NVL(ARFLSERG204,space(1))
            this.w_FLSERA = NVL(ARTIPSER204,space(1))
            this.w_FLUSEP = NVL(ARFLUSEP204,space(1))
            this.w_OPERAT = NVL(AROPERAT204,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP204,0)
            this.w_ODCODGRU = NVL(ARCODGRU204,space(5))
            this.w_ODCODSOT = NVL(ARCODSOT204,space(5))
            this.w_CODIVA = NVL(ARCODIVA204,space(5))
            this.w_GRUMER = NVL(ARGRUMER204,space(5))
            this.w_CATSCA = NVL(ARCATSCM204,space(5))
            this.w_PREZUM = NVL(ARPREZUM204,space(1))
          else
          .link_2_4('Load')
          endif
          .link_2_8('Load')
          .w_ODNOTAGG = NVL(ODNOTAGG,space(0))
          .link_2_11('Load')
          .w_ODCODGRU = NVL(ODCODGRU,space(5))
          * evitabile
          *.link_2_20('Load')
          .w_ODCODSOT = NVL(ODCODSOT,space(5))
          * evitabile
          *.link_2_21('Load')
          .w_ODDESART = NVL(ODDESART,space(40))
          .w_ODUNIMIS = NVL(ODUNIMIS,space(3))
          if link_2_23_joined
            this.w_ODUNIMIS = NVL(UMCODICE223,NVL(this.w_ODUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ223,space(1))
          else
          .link_2_23('Load')
          endif
          .w_ODQTAMOV = NVL(ODQTAMOV,0)
          .w_ODFLOMAG = NVL(ODFLOMAG,space(1))
          .w_ODARNOCO = NVL(ODARNOCO,space(1))
          .w_ODQTAUM1 = NVL(ODQTAUM1,0)
          .w_ODPREZZO = NVL(ODPREZZO,0)
          .w_ODSCONT1 = NVL(ODSCONT1,0)
          .w_ODSCONT2 = NVL(ODSCONT2,0)
          .w_ODSCONT3 = NVL(ODSCONT3,0)
          .w_ODSCONT4 = NVL(ODSCONT4,0)
          .w_ODDATPCO = NVL(cp_ToDate(ODDATPCO),ctod("  /  /  "))
          .w_ODVALRIG = NVL(ODVALRIG,0)
        .w_RIGNET = IIF(.w_ODFLOMAG='X' AND .w_FLSERA<>'S', .w_ODVALRIG, 0)
        .w_RIGNET1 = IIF(.w_ODFLOMAG='X', .w_ODVALRIG, 0)
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_ODUNIMIS=.w_UNMIS1 OR EMPTY(.w_ODCODICE), '',ah_msgformat( 'Qta nella 1^UM:')))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_45.Calculate()
          .w_ODCONTRA = NVL(ODCONTRA,space(15))
        .w_OFLSCO = ' '
        .w_ARTNOC = .w_ODARNOCO
        .w_OBTEST = .w_DATDOC
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_56.Calculate()
          .w_ODTIPPRO = NVL(ODTIPPRO,space(2))
          .w_ODTIPPR2 = NVL(ODTIPPR2,space(2))
          .w_ODPERPRO = NVL(ODPERPRO,0)
          .w_ODPROCAP = NVL(ODPROCAP,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTRIG = .w_TOTRIG+.w_ODVALRIG
          .w_TOTMERCE = .w_TOTMERCE+.w_RIGNET
          .w_TOTRIPD = .w_TOTRIPD+.w_RIGNET1
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_NUMSCO = this.oParentObject .w_NUMSCO
        .w_DECTOT = this.oParentObject .w_DECTOT
        .w_IVALIS = this.oParentObject .w_IVALIS
        .w_DECUNI = this.oParentObject .w_DECUNI
        .w_CODCLI = this.oParentObject .w_CODCLI
        .w_CATCOM = this.oParentObject .w_CATCOM
        .w_FLSCOR = this.oParentObject .w_FLSCOR
        .w_CATSCC = this.oParentObject .w_CATSCC
        .w_DATDOC = this.oParentObject .w_OFDATDOC
        .w_CODLIS = this.oParentObject .w_OFCODLIS
        .w_SCOLIS = this.oParentObject .w_SCOLIS
        .w_CODVAL = this.oParentObject .w_OFCODVAL
        .w_CODMAG = this.oParentObject .w_CODMAG
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_PRZVAC = this.oParentObject .w_PRZVAC
        .w_OBTEST = .w_DATDOC
        .w_SIMVAL = this.oParentObject .w_SIMVAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_57.enabled = .oPgFrm.Page1.oPag.oBtn_2_57.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_58.enabled = .oPgFrm.Page1.oPag.oBtn_2_58.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_ODSERIAL=space(10)
      .w_ODCODICE=space(20)
      .w_CPROWORD=10
      .w_NUMSCO=0
      .w_DECTOT=0
      .w_IVALIS=space(1)
      .w_DECUNI=0
      .w_CODCLI=space(15)
      .w_CATCOM=space(5)
      .w_FLSCOR=space(1)
      .w_CATSCC=space(5)
      .w_DATDOC=ctod("  /  /  ")
      .w_CODLIS=space(5)
      .w_SCOLIS=space(1)
      .w_CODVAL=space(5)
      .w_CODMAG=space(5)
      .w_ODTIPRIG=space(1)
      .w_ODCODART=space(20)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_OPERA3=space(1)
      .w_CODIVA=space(5)
      .w_PERIVA=0
      .w_ODNOTAGG=space(0)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_FLUSEP=space(1)
      .w_FLSERG=space(1)
      .w_FLSERA=space(1)
      .w_GRUMER=space(5)
      .w_CATSCA=space(5)
      .w_ODCODGRU=space(5)
      .w_ODCODSOT=space(5)
      .w_ODDESART=space(40)
      .w_ODUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_ODQTAMOV=0
      .w_ODFLOMAG=space(1)
      .w_ODARNOCO=space(1)
      .w_ODQTAUM1=0
      .w_LIPREZZO=0
      .w_ODPREZZO=0
      .w_ODSCONT1=0
      .w_ODSCONT2=0
      .w_ODSCONT3=0
      .w_ODSCONT4=0
      .w_ODDATPCO=ctod("  /  /  ")
      .w_ODVALRIG=0
      .w_RIGNET=0
      .w_RIGNET1=0
      .w_TOTRIG=0
      .w_TOTMERCE=0
      .w_TOTRIPD=0
      .w_ODCONTRA=space(15)
      .w_OFLSCO=space(1)
      .w_ARTNOC=space(1)
      .w_OGG=space(10)
      .w_CALCPICT=0
      .w_CALCPICU=0
      .w_LISCON=0
      .w_PRZVAC=space(1)
      .w_IVASCOR=0
      .w_ACTSCOR=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_CODESC=space(5)
      .w_SIMVAL=space(5)
      .w_GIACON=space(1)
      .w_ODTIPPRO=space(2)
      .w_ODTIPPR2=space(2)
      .w_ODPERPRO=0
      .w_ODPROCAP=0
      .w_FLFRAZ1=space(1)
      .w_MODUM2=space(10)
      .w_CLUNIMIS=space(3)
      .w_PREZUM=space(1)
      .w_UMCAL=space(14)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_ODCODICE))
         .link_2_1('Full')
        endif
        .DoRTCalc(3,3,.f.)
        .w_NUMSCO = this.oParentObject .w_NUMSCO
        .w_DECTOT = this.oParentObject .w_DECTOT
        .w_IVALIS = this.oParentObject .w_IVALIS
        .w_DECUNI = this.oParentObject .w_DECUNI
        .w_CODCLI = this.oParentObject .w_CODCLI
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODCLI))
         .link_1_6('Full')
        endif
        .w_CATCOM = this.oParentObject .w_CATCOM
        .w_FLSCOR = this.oParentObject .w_FLSCOR
        .w_CATSCC = this.oParentObject .w_CATSCC
        .w_DATDOC = this.oParentObject .w_OFDATDOC
        .w_CODLIS = this.oParentObject .w_OFCODLIS
        .w_SCOLIS = this.oParentObject .w_SCOLIS
        .w_CODVAL = this.oParentObject .w_OFCODVAL
        .w_CODMAG = this.oParentObject .w_CODMAG
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_ODCODART))
         .link_2_4('Full')
        endif
        .DoRTCalc(19,22,.f.)
        if not(empty(.w_CODIVA))
         .link_2_8('Full')
        endif
        .DoRTCalc(23,25,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_11('Full')
        endif
        .DoRTCalc(26,34,.f.)
        if not(empty(.w_ODCODGRU))
         .link_2_20('Full')
        endif
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_ODCODSOT))
         .link_2_21('Full')
        endif
        .DoRTCalc(36,36,.f.)
        .w_ODUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_ODUNIMIS))
         .link_2_23('Full')
        endif
        .DoRTCalc(38,38,.f.)
        .w_ODQTAMOV = IIF(.w_ODTIPRIG='F', 1, IIF(.w_ODTIPRIG='D', 0, .w_ODQTAMOV))
        .w_ODFLOMAG = 'X'
        .w_ODARNOCO = ' '
        .w_ODQTAUM1 = IIF(.w_ODTIPRIG='F', 1, .w_ODQTAUM1)
        .DoRTCalc(43,43,.f.)
        .w_ODPREZZO = IIF((.w_ODTIPRIG $ 'RFMA'), cp_Round(CALMMLIS(.w_LIPREZZO, .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_ODUNIMIS+.w_OPERAT+.w_OPERA3+IIF(.w_LISCON=2, .w_IVALIS, 'N')+'P'+ALLTRIM(STR(.w_DECUNI)), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTIP,1), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTI3,1), IIF(.w_FLSCOR='S',0,.w_PERIVA)), .w_DECUNI), 0)
        .DoRTCalc(45,49,.f.)
        .w_ODVALRIG = CAVALRIG(.w_ODPREZZO,.w_ODQTAMOV, .w_ODSCONT1,.w_ODSCONT2,.w_ODSCONT3,.w_ODSCONT4,.w_DECTOT)
        .w_RIGNET = IIF(.w_ODFLOMAG='X' AND .w_FLSERA<>'S', .w_ODVALRIG, 0)
        .w_RIGNET1 = IIF(.w_ODFLOMAG='X', .w_ODVALRIG, 0)
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_ODUNIMIS=.w_UNMIS1 OR EMPTY(.w_ODCODICE), '',ah_msgformat( 'Qta nella 1^UM:')))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_45.Calculate()
        .DoRTCalc(53,56,.f.)
        .w_OFLSCO = ' '
        .w_ARTNOC = .w_ODARNOCO
        .w_OGG = SPACE(10)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(62,62,.f.)
        .w_PRZVAC = this.oParentObject .w_PRZVAC
        .DoRTCalc(64,64,.f.)
        .w_ACTSCOR = .F.
        .w_OBTEST = .w_DATDOC
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_56.Calculate()
        .DoRTCalc(67,67,.f.)
        .w_SIMVAL = this.oParentObject .w_SIMVAL
        .w_GIACON = IIF(.cFunction='Load', ' ', 'X')
        .w_ODTIPPRO = IIF(.w_ODFLOMAG='X','DC','ES')
        .w_ODTIPPR2 = IIF(.w_ODFLOMAG='X','DC','ES')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'OFF_DETT')
    this.DoRTCalc(72,78,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_57.enabled = this.oPgFrm.Page1.oPag.oBtn_2_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_58.enabled = this.oPgFrm.Page1.oPag.oBtn_2_58.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oODFLOMAG_2_26.enabled = i_bVal
      .Page1.oPag.oODARNOCO_2_27.enabled = i_bVal
      .Page1.oPag.oODQTAUM1_2_28.enabled = i_bVal
      .Page1.oPag.oODSCONT1_2_31.enabled = i_bVal
      .Page1.oPag.oODSCONT2_2_32.enabled = i_bVal
      .Page1.oPag.oODSCONT3_2_33.enabled = i_bVal
      .Page1.oPag.oODSCONT4_2_34.enabled = i_bVal
      .Page1.oPag.oODDATPCO_2_35.enabled = i_bVal
      .Page1.oPag.oBtn_2_57.enabled = .Page1.oPag.oBtn_2_57.mCond()
      .Page1.oPag.oBtn_2_58.enabled = .Page1.oPag.oBtn_2_58.mCond()
      .Page1.oPag.oBtn_2_59.enabled = i_bVal
      .Page1.oPag.oObj_2_40.enabled = i_bVal
      .Page1.oPag.oObj_2_41.enabled = i_bVal
      .Page1.oPag.oObj_2_42.enabled = i_bVal
      .Page1.oPag.oObj_2_43.enabled = i_bVal
      .Page1.oPag.oObj_2_44.enabled = i_bVal
      .Page1.oPag.oObj_2_45.enabled = i_bVal
      .Page1.oPag.oObj_1_21.enabled = i_bVal
      .Page1.oPag.oObj_1_22.enabled = i_bVal
      .Page1.oPag.oObj_2_52.enabled = i_bVal
      .Page1.oPag.oObj_2_53.enabled = i_bVal
      .Page1.oPag.oObj_2_54.enabled = i_bVal
      .Page1.oPag.oObj_2_55.enabled = i_bVal
      .Page1.oPag.oObj_2_56.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'OFF_DETT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ODSERIAL,"ODSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ODCODICE C(20);
      ,t_CPROWORD N(5);
      ,t_ODCODART C(20);
      ,t_UNMIS1 C(3);
      ,t_ODCODGRU C(5);
      ,t_ODCODSOT C(5);
      ,t_ODDESART C(40);
      ,t_ODUNIMIS C(3);
      ,t_ODQTAMOV N(12,3);
      ,t_ODFLOMAG N(3);
      ,t_ODARNOCO N(3);
      ,t_ODQTAUM1 N(12,3);
      ,t_ODPREZZO N(18,5);
      ,t_ODSCONT1 N(6,2);
      ,t_ODSCONT2 N(6,2);
      ,t_ODSCONT3 N(6,2);
      ,t_ODSCONT4 N(6,2);
      ,t_ODDATPCO D(8);
      ,t_ODVALRIG N(18,4);
      ,CPROWNUM N(10);
      ,t_ODTIPRIG C(1);
      ,t_UNMIS3 C(3);
      ,t_MOLTI3 N(10,4);
      ,t_OPERA3 C(1);
      ,t_CODIVA C(5);
      ,t_PERIVA N(5,2);
      ,t_ODNOTAGG M(10);
      ,t_UNMIS2 C(3);
      ,t_MOLTIP N(10,5);
      ,t_OPERAT C(1);
      ,t_FLUSEP C(1);
      ,t_FLSERG C(1);
      ,t_FLSERA C(1);
      ,t_GRUMER C(5);
      ,t_CATSCA C(5);
      ,t_FLFRAZ C(1);
      ,t_LIPREZZO N(18,4);
      ,t_RIGNET N(18,4);
      ,t_RIGNET1 N(18,4);
      ,t_ODCONTRA C(15);
      ,t_OFLSCO C(1);
      ,t_ARTNOC C(1);
      ,t_LISCON N(1);
      ,t_IVASCOR N(18,5);
      ,t_ACTSCOR L(1);
      ,t_OBTEST D(8);
      ,t_GIACON C(1);
      ,t_ODTIPPRO C(2);
      ,t_ODTIPPR2 C(2);
      ,t_ODPERPRO N(5,2);
      ,t_ODPROCAP N(5,2);
      ,t_FLFRAZ1 C(1);
      ,t_MODUM2 C(10);
      ,t_CLUNIMIS C(3);
      ,t_PREZUM C(1);
      ,t_UMCAL C(14);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsof_mdobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_1.controlsource=this.cTrsName+'.t_ODCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oODCODART_2_4.controlsource=this.cTrsName+'.t_ODCODART'
    this.oPgFRm.Page1.oPag.oUNMIS1_2_11.controlsource=this.cTrsName+'.t_UNMIS1'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODCODGRU_2_20.controlsource=this.cTrsName+'.t_ODCODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODCODSOT_2_21.controlsource=this.cTrsName+'.t_ODCODSOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODDESART_2_22.controlsource=this.cTrsName+'.t_ODDESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODUNIMIS_2_23.controlsource=this.cTrsName+'.t_ODUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODQTAMOV_2_25.controlsource=this.cTrsName+'.t_ODQTAMOV'
    this.oPgFRm.Page1.oPag.oODFLOMAG_2_26.controlsource=this.cTrsName+'.t_ODFLOMAG'
    this.oPgFRm.Page1.oPag.oODARNOCO_2_27.controlsource=this.cTrsName+'.t_ODARNOCO'
    this.oPgFRm.Page1.oPag.oODQTAUM1_2_28.controlsource=this.cTrsName+'.t_ODQTAUM1'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODPREZZO_2_30.controlsource=this.cTrsName+'.t_ODPREZZO'
    this.oPgFRm.Page1.oPag.oODSCONT1_2_31.controlsource=this.cTrsName+'.t_ODSCONT1'
    this.oPgFRm.Page1.oPag.oODSCONT2_2_32.controlsource=this.cTrsName+'.t_ODSCONT2'
    this.oPgFRm.Page1.oPag.oODSCONT3_2_33.controlsource=this.cTrsName+'.t_ODSCONT3'
    this.oPgFRm.Page1.oPag.oODSCONT4_2_34.controlsource=this.cTrsName+'.t_ODSCONT4'
    this.oPgFRm.Page1.oPag.oODDATPCO_2_35.controlsource=this.cTrsName+'.t_ODDATPCO'
    this.oPgFRm.Page1.oPag.oODVALRIG_2_36.controlsource=this.cTrsName+'.t_ODVALRIG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(49)
    this.AddVLine(194)
    this.AddVLine(264)
    this.AddVLine(340)
    this.AddVLine(546)
    this.AddVLine(580)
    this.AddVLine(659)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
      *
      * insert into OFF_DETT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OFF_DETT')
        i_extval=cp_InsertValODBCExtFlds(this,'OFF_DETT')
        i_cFldBody=" "+;
                  "(ODSERIAL,ODCODICE,CPROWORD,ODTIPRIG,ODCODART"+;
                  ",ODNOTAGG,ODCODGRU,ODCODSOT,ODDESART,ODUNIMIS"+;
                  ",ODQTAMOV,ODFLOMAG,ODARNOCO,ODQTAUM1,ODPREZZO"+;
                  ",ODSCONT1,ODSCONT2,ODSCONT3,ODSCONT4,ODDATPCO"+;
                  ",ODVALRIG,ODCONTRA,ODTIPPRO,ODTIPPR2,ODPERPRO"+;
                  ",ODPROCAP,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ODSERIAL)+","+cp_ToStrODBCNull(this.w_ODCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_ODTIPRIG)+","+cp_ToStrODBCNull(this.w_ODCODART)+;
             ","+cp_ToStrODBC(this.w_ODNOTAGG)+","+cp_ToStrODBCNull(this.w_ODCODGRU)+","+cp_ToStrODBCNull(this.w_ODCODSOT)+","+cp_ToStrODBC(this.w_ODDESART)+","+cp_ToStrODBCNull(this.w_ODUNIMIS)+;
             ","+cp_ToStrODBC(this.w_ODQTAMOV)+","+cp_ToStrODBC(this.w_ODFLOMAG)+","+cp_ToStrODBC(this.w_ODARNOCO)+","+cp_ToStrODBC(this.w_ODQTAUM1)+","+cp_ToStrODBC(this.w_ODPREZZO)+;
             ","+cp_ToStrODBC(this.w_ODSCONT1)+","+cp_ToStrODBC(this.w_ODSCONT2)+","+cp_ToStrODBC(this.w_ODSCONT3)+","+cp_ToStrODBC(this.w_ODSCONT4)+","+cp_ToStrODBC(this.w_ODDATPCO)+;
             ","+cp_ToStrODBC(this.w_ODVALRIG)+","+cp_ToStrODBC(this.w_ODCONTRA)+","+cp_ToStrODBC(this.w_ODTIPPRO)+","+cp_ToStrODBC(this.w_ODTIPPR2)+","+cp_ToStrODBC(this.w_ODPERPRO)+;
             ","+cp_ToStrODBC(this.w_ODPROCAP)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OFF_DETT')
        i_extval=cp_InsertValVFPExtFlds(this,'OFF_DETT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'ODSERIAL',this.w_ODSERIAL)
        INSERT INTO (i_cTable) (;
                   ODSERIAL;
                  ,ODCODICE;
                  ,CPROWORD;
                  ,ODTIPRIG;
                  ,ODCODART;
                  ,ODNOTAGG;
                  ,ODCODGRU;
                  ,ODCODSOT;
                  ,ODDESART;
                  ,ODUNIMIS;
                  ,ODQTAMOV;
                  ,ODFLOMAG;
                  ,ODARNOCO;
                  ,ODQTAUM1;
                  ,ODPREZZO;
                  ,ODSCONT1;
                  ,ODSCONT2;
                  ,ODSCONT3;
                  ,ODSCONT4;
                  ,ODDATPCO;
                  ,ODVALRIG;
                  ,ODCONTRA;
                  ,ODTIPPRO;
                  ,ODTIPPR2;
                  ,ODPERPRO;
                  ,ODPROCAP;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ODSERIAL;
                  ,this.w_ODCODICE;
                  ,this.w_CPROWORD;
                  ,this.w_ODTIPRIG;
                  ,this.w_ODCODART;
                  ,this.w_ODNOTAGG;
                  ,this.w_ODCODGRU;
                  ,this.w_ODCODSOT;
                  ,this.w_ODDESART;
                  ,this.w_ODUNIMIS;
                  ,this.w_ODQTAMOV;
                  ,this.w_ODFLOMAG;
                  ,this.w_ODARNOCO;
                  ,this.w_ODQTAUM1;
                  ,this.w_ODPREZZO;
                  ,this.w_ODSCONT1;
                  ,this.w_ODSCONT2;
                  ,this.w_ODSCONT3;
                  ,this.w_ODSCONT4;
                  ,this.w_ODDATPCO;
                  ,this.w_ODVALRIG;
                  ,this.w_ODCONTRA;
                  ,this.w_ODTIPPRO;
                  ,this.w_ODTIPPR2;
                  ,this.w_ODPERPRO;
                  ,this.w_ODPROCAP;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODCODICE)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'OFF_DETT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'OFF_DETT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODCODICE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update OFF_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'OFF_DETT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ODCODICE="+cp_ToStrODBCNull(this.w_ODCODICE)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",ODTIPRIG="+cp_ToStrODBC(this.w_ODTIPRIG)+;
                     ",ODCODART="+cp_ToStrODBCNull(this.w_ODCODART)+;
                     ",ODNOTAGG="+cp_ToStrODBC(this.w_ODNOTAGG)+;
                     ",ODCODGRU="+cp_ToStrODBCNull(this.w_ODCODGRU)+;
                     ",ODCODSOT="+cp_ToStrODBCNull(this.w_ODCODSOT)+;
                     ",ODDESART="+cp_ToStrODBC(this.w_ODDESART)+;
                     ",ODUNIMIS="+cp_ToStrODBCNull(this.w_ODUNIMIS)+;
                     ",ODQTAMOV="+cp_ToStrODBC(this.w_ODQTAMOV)+;
                     ",ODFLOMAG="+cp_ToStrODBC(this.w_ODFLOMAG)+;
                     ",ODARNOCO="+cp_ToStrODBC(this.w_ODARNOCO)+;
                     ",ODQTAUM1="+cp_ToStrODBC(this.w_ODQTAUM1)+;
                     ",ODPREZZO="+cp_ToStrODBC(this.w_ODPREZZO)+;
                     ",ODSCONT1="+cp_ToStrODBC(this.w_ODSCONT1)+;
                     ",ODSCONT2="+cp_ToStrODBC(this.w_ODSCONT2)+;
                     ",ODSCONT3="+cp_ToStrODBC(this.w_ODSCONT3)+;
                     ",ODSCONT4="+cp_ToStrODBC(this.w_ODSCONT4)+;
                     ",ODDATPCO="+cp_ToStrODBC(this.w_ODDATPCO)+;
                     ",ODVALRIG="+cp_ToStrODBC(this.w_ODVALRIG)+;
                     ",ODCONTRA="+cp_ToStrODBC(this.w_ODCONTRA)+;
                     ",ODTIPPRO="+cp_ToStrODBC(this.w_ODTIPPRO)+;
                     ",ODTIPPR2="+cp_ToStrODBC(this.w_ODTIPPR2)+;
                     ",ODPERPRO="+cp_ToStrODBC(this.w_ODPERPRO)+;
                     ",ODPROCAP="+cp_ToStrODBC(this.w_ODPROCAP)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'OFF_DETT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ODCODICE=this.w_ODCODICE;
                     ,CPROWORD=this.w_CPROWORD;
                     ,ODTIPRIG=this.w_ODTIPRIG;
                     ,ODCODART=this.w_ODCODART;
                     ,ODNOTAGG=this.w_ODNOTAGG;
                     ,ODCODGRU=this.w_ODCODGRU;
                     ,ODCODSOT=this.w_ODCODSOT;
                     ,ODDESART=this.w_ODDESART;
                     ,ODUNIMIS=this.w_ODUNIMIS;
                     ,ODQTAMOV=this.w_ODQTAMOV;
                     ,ODFLOMAG=this.w_ODFLOMAG;
                     ,ODARNOCO=this.w_ODARNOCO;
                     ,ODQTAUM1=this.w_ODQTAUM1;
                     ,ODPREZZO=this.w_ODPREZZO;
                     ,ODSCONT1=this.w_ODSCONT1;
                     ,ODSCONT2=this.w_ODSCONT2;
                     ,ODSCONT3=this.w_ODSCONT3;
                     ,ODSCONT4=this.w_ODSCONT4;
                     ,ODDATPCO=this.w_ODDATPCO;
                     ,ODVALRIG=this.w_ODVALRIG;
                     ,ODCONTRA=this.w_ODCONTRA;
                     ,ODTIPPRO=this.w_ODTIPPRO;
                     ,ODTIPPR2=this.w_ODTIPPR2;
                     ,ODPERPRO=this.w_ODPERPRO;
                     ,ODPROCAP=this.w_ODPROCAP;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODCODICE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete OFF_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODCODICE)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .w_NUMSCO = this.oParentObject .w_NUMSCO
          .w_DECTOT = this.oParentObject .w_DECTOT
          .w_IVALIS = this.oParentObject .w_IVALIS
          .w_DECUNI = this.oParentObject .w_DECUNI
          .w_CODCLI = this.oParentObject .w_CODCLI
          .link_1_6('Full')
          .w_CATCOM = this.oParentObject .w_CATCOM
          .w_FLSCOR = this.oParentObject .w_FLSCOR
          .w_CATSCC = this.oParentObject .w_CATSCC
          .w_DATDOC = this.oParentObject .w_OFDATDOC
          .w_CODLIS = this.oParentObject .w_OFCODLIS
          .w_SCOLIS = this.oParentObject .w_SCOLIS
          .w_CODVAL = this.oParentObject .w_OFCODVAL
          .w_CODMAG = this.oParentObject .w_CODMAG
        .DoRTCalc(17,17,.t.)
        if .o_ODCODICE<>.w_ODCODICE
          .link_2_4('Full')
        endif
        .DoRTCalc(19,21,.t.)
        if .o_ODCODICE<>.w_ODCODICE
          .link_2_8('Full')
        endif
        .DoRTCalc(23,24,.t.)
          .link_2_11('Full')
        .DoRTCalc(26,33,.t.)
        if .o_ODCODICE<>.w_ODCODICE
          .link_2_20('Full')
        endif
        if .o_ODCODICE<>.w_ODCODICE.or. .o_ODCODGRU<>.w_ODCODGRU
          .link_2_21('Full')
        endif
        .DoRTCalc(36,36,.t.)
        if .o_ODCODICE<>.w_ODCODICE
          .w_ODUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_2_23('Full')
        endif
        .DoRTCalc(38,38,.t.)
        if .o_ODCODICE<>.w_ODCODICE
          .w_ODQTAMOV = IIF(.w_ODTIPRIG='F', 1, IIF(.w_ODTIPRIG='D', 0, .w_ODQTAMOV))
        endif
        .DoRTCalc(40,40,.t.)
        if .o_ODCODICE<>.w_ODCODICE
          .w_ODARNOCO = ' '
        endif
        if .o_ODQTAMOV<>.w_ODQTAMOV.or. .o_ODUNIMIS<>.w_ODUNIMIS
          .w_ODQTAUM1 = IIF(.w_ODTIPRIG='F', 1, .w_ODQTAUM1)
        endif
        .DoRTCalc(43,43,.t.)
        if .o_LIPREZZO<>.w_LIPREZZO
          .w_ODPREZZO = IIF((.w_ODTIPRIG $ 'RFMA'), cp_Round(CALMMLIS(.w_LIPREZZO, .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_ODUNIMIS+.w_OPERAT+.w_OPERA3+IIF(.w_LISCON=2, .w_IVALIS, 'N')+'P'+ALLTRIM(STR(.w_DECUNI)), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTIP,1), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTI3,1), IIF(.w_FLSCOR='S',0,.w_PERIVA)), .w_DECUNI), 0)
        endif
        .DoRTCalc(45,49,.t.)
        if .o_ODQTAMOV<>.w_ODQTAMOV.or. .o_ODPREZZO<>.w_ODPREZZO.or. .o_ODSCONT1<>.w_ODSCONT1.or. .o_ODSCONT2<>.w_ODSCONT2.or. .o_ODSCONT3<>.w_ODSCONT3.or. .o_ODSCONT4<>.w_ODSCONT4
          .w_TOTRIG = .w_TOTRIG-.w_odvalrig
          .w_ODVALRIG = CAVALRIG(.w_ODPREZZO,.w_ODQTAMOV, .w_ODSCONT1,.w_ODSCONT2,.w_ODSCONT3,.w_ODSCONT4,.w_DECTOT)
          .w_TOTRIG = .w_TOTRIG+.w_odvalrig
        endif
          .w_TOTMERCE = .w_TOTMERCE-.w_rignet
          .w_RIGNET = IIF(.w_ODFLOMAG='X' AND .w_FLSERA<>'S', .w_ODVALRIG, 0)
          .w_TOTMERCE = .w_TOTMERCE+.w_rignet
          .w_TOTRIPD = .w_TOTRIPD-.w_rignet1
          .w_RIGNET1 = IIF(.w_ODFLOMAG='X', .w_ODVALRIG, 0)
          .w_TOTRIPD = .w_TOTRIPD+.w_rignet1
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_ODUNIMIS=.w_UNMIS1 OR EMPTY(.w_ODCODICE), '',ah_msgformat( 'Qta nella 1^UM:')))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_45.Calculate()
        .DoRTCalc(53,56,.t.)
        if .o_ODCODART<>.w_ODCODART
          .w_OFLSCO = ' '
        endif
          .w_ARTNOC = .w_ODARNOCO
        .DoRTCalc(59,59,.t.)
        if .o_DECTOT<>.w_DECTOT
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_DECUNI<>.w_DECUNI
          .w_CALCPICU = DEFPIU(.w_DECUNI)
        endif
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(62,62,.t.)
          .w_PRZVAC = this.oParentObject .w_PRZVAC
        .DoRTCalc(64,65,.t.)
          .w_OBTEST = .w_DATDOC
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_56.Calculate()
        .DoRTCalc(67,67,.t.)
          .w_SIMVAL = this.oParentObject .w_SIMVAL
        .DoRTCalc(69,69,.t.)
        if .o_ODFLOMAG<>.w_ODFLOMAG
          .w_ODTIPPRO = IIF(.w_ODFLOMAG='X','DC','ES')
        endif
        if .o_ODFLOMAG<>.w_ODFLOMAG
          .w_ODTIPPR2 = IIF(.w_ODFLOMAG='X','DC','ES')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- gsof_mdo
        this.oParentObject.mcalc(.t.)
        
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(72,78,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_ODTIPRIG with this.w_ODTIPRIG
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_OPERA3 with this.w_OPERA3
      replace t_CODIVA with this.w_CODIVA
      replace t_PERIVA with this.w_PERIVA
      replace t_ODNOTAGG with this.w_ODNOTAGG
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_MOLTIP with this.w_MOLTIP
      replace t_OPERAT with this.w_OPERAT
      replace t_FLUSEP with this.w_FLUSEP
      replace t_FLSERG with this.w_FLSERG
      replace t_FLSERA with this.w_FLSERA
      replace t_GRUMER with this.w_GRUMER
      replace t_CATSCA with this.w_CATSCA
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_LIPREZZO with this.w_LIPREZZO
      replace t_RIGNET with this.w_RIGNET
      replace t_RIGNET1 with this.w_RIGNET1
      replace t_ODCONTRA with this.w_ODCONTRA
      replace t_OFLSCO with this.w_OFLSCO
      replace t_ARTNOC with this.w_ARTNOC
      replace t_LISCON with this.w_LISCON
      replace t_IVASCOR with this.w_IVASCOR
      replace t_ACTSCOR with this.w_ACTSCOR
      replace t_OBTEST with this.w_OBTEST
      replace t_GIACON with this.w_GIACON
      replace t_ODTIPPRO with this.w_ODTIPPRO
      replace t_ODTIPPR2 with this.w_ODTIPPR2
      replace t_ODPERPRO with this.w_ODPERPRO
      replace t_ODPROCAP with this.w_ODPROCAP
      replace t_FLFRAZ1 with this.w_FLFRAZ1
      replace t_MODUM2 with this.w_MODUM2
      replace t_CLUNIMIS with this.w_CLUNIMIS
      replace t_PREZUM with this.w_PREZUM
      replace t_UMCAL with this.w_UMCAL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_ODUNIMIS=.w_UNMIS1 OR EMPTY(.w_ODCODICE), '',ah_msgformat( 'Qta nella 1^UM:')))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_56.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_ODUNIMIS=.w_UNMIS1 OR EMPTY(.w_ODCODICE), '',ah_msgformat( 'Qta nella 1^UM:')))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_56.Calculate()
    endwith
  return
  proc Calculate_SKICZIXJXT()
    with this
          * --- Ricalcolo II sconto
          .w_ODSCONT2 = 0
          .w_ODSCONT3 = 0
          .w_ODSCONT4 = 0
    endwith
  endproc
  proc Calculate_ULAREFMTKG()
    with this
          * --- Ricalcolo III sconto
          .w_ODSCONT3 = 0
          .w_ODSCONT4 = 0
    endwith
  endproc
  proc Calculate_ZRHIZLLPMJ()
    with this
          * --- Ricalcolo iv sconto
          .w_ODSCONT4 = 0
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODCODSOT_2_21.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODCODSOT_2_21.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODDESART_2_22.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODDESART_2_22.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODUNIMIS_2_23.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODUNIMIS_2_23.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODQTAMOV_2_25.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODQTAMOV_2_25.mCond()
    this.oPgFrm.Page1.oPag.oODARNOCO_2_27.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oODARNOCO_2_27.mCond()
    this.oPgFrm.Page1.oPag.oODQTAUM1_2_28.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oODQTAUM1_2_28.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODPREZZO_2_30.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODPREZZO_2_30.mCond()
    this.oPgFrm.Page1.oPag.oODSCONT1_2_31.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oODSCONT1_2_31.mCond()
    this.oPgFrm.Page1.oPag.oODSCONT2_2_32.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oODSCONT2_2_32.mCond()
    this.oPgFrm.Page1.oPag.oODSCONT3_2_33.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oODSCONT3_2_33.mCond()
    this.oPgFrm.Page1.oPag.oODSCONT4_2_34.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oODSCONT4_2_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_57.enabled =this.oPgFrm.Page1.oPag.oBtn_2_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_58.enabled =this.oPgFrm.Page1.oPag.oBtn_2_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_59.enabled =this.oPgFrm.Page1.oPag.oBtn_2_59.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oODCODART_2_4.visible=!this.oPgFrm.Page1.oPag.oODCODART_2_4.mHide()
    this.oPgFrm.Page1.oPag.oUNMIS1_2_11.visible=!this.oPgFrm.Page1.oPag.oUNMIS1_2_11.mHide()
    this.oPgFrm.Page1.oPag.oODQTAUM1_2_28.visible=!this.oPgFrm.Page1.oPag.oODQTAUM1_2_28.mHide()
    this.oPgFrm.Page1.oPag.oODSCONT2_2_32.visible=!this.oPgFrm.Page1.oPag.oODSCONT2_2_32.mHide()
    this.oPgFrm.Page1.oPag.oODSCONT3_2_33.visible=!this.oPgFrm.Page1.oPag.oODSCONT3_2_33.mHide()
    this.oPgFrm.Page1.oPag.oODSCONT4_2_34.visible=!this.oPgFrm.Page1.oPag.oODSCONT4_2_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_58.visible=!this.oPgFrm.Page1.oPag.oBtn_2_58.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_2_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("w_ODSCONT1 Changed")
          .Calculate_SKICZIXJXT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ODSCONT2 Changed")
          .Calculate_ULAREFMTKG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ODSCONT3 Changed")
          .Calculate_ZRHIZLLPMJ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ODCODICE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ODCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ODCODICE))
          select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStrODBC(trim(this.w_ODCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStr(trim(this.w_ODCODICE)+"%");

            select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_ODCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_ODCODICE)+"%");

            select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ODCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oODCODICE_2_1'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSVE_MDV.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ODCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ODCODICE)
            select CACODICE,CACODART,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CA__TIPO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_ODCODART = NVL(_Link_.CACODART,space(20))
      this.w_ODDESART = NVL(_Link_.CADESART,space(40))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_ODTIPRIG = NVL(_Link_.CA__TIPO,space(1))
      this.w_ODNOTAGG = NVL(_Link_.CADESSUP,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_ODCODICE = space(20)
      endif
      this.w_ODCODART = space(20)
      this.w_ODDESART = space(40)
      this.w_UNMIS3 = space(3)
      this.w_MOLTI3 = 0
      this.w_OPERA3 = space(1)
      this.w_ODTIPRIG = space(1)
      this.w_ODNOTAGG = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CACODICE as CACODICE201"+ ",link_2_1.CACODART as CACODART201"+ ",link_2_1.CADESART as CADESART201"+ ",link_2_1.CAUNIMIS as CAUNIMIS201"+ ",link_2_1.CAMOLTIP as CAMOLTIP201"+ ",link_2_1.CAOPERAT as CAOPERAT201"+ ",link_2_1.CA__TIPO as CA__TIPO201"+ ",link_2_1.CADESSUP as CADESSUP201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on OFF_DETT.ODCODICE=link_2_1.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and OFF_DETT.ODCODICE=link_2_1.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCODESC";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC('C');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON','C';
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_CODESC = NVL(_Link_.ANCODESC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_CODESC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODCODART
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPSER,ARFLUSEP,AROPERAT,ARMOLTIP,ARCODGRU,ARCODSOT,ARCODIVA,ARGRUMER,ARCATSCM,ARPREZUM";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ODCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ODCODART)
            select ARCODART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPSER,ARFLUSEP,AROPERAT,ARMOLTIP,ARCODGRU,ARCODSOT,ARCODIVA,ARGRUMER,ARCATSCM,ARPREZUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_FLSERA = NVL(_Link_.ARTIPSER,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_ODCODGRU = NVL(_Link_.ARCODGRU,space(5))
      this.w_ODCODSOT = NVL(_Link_.ARCODSOT,space(5))
      this.w_CODIVA = NVL(_Link_.ARCODIVA,space(5))
      this.w_GRUMER = NVL(_Link_.ARGRUMER,space(5))
      this.w_CATSCA = NVL(_Link_.ARCATSCM,space(5))
      this.w_PREZUM = NVL(_Link_.ARPREZUM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ODCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_FLSERA = space(1)
      this.w_FLUSEP = space(1)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_ODCODGRU = space(5)
      this.w_ODCODSOT = space(5)
      this.w_CODIVA = space(5)
      this.w_GRUMER = space(5)
      this.w_CATSCA = space(5)
      this.w_PREZUM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.ARCODART as ARCODART204"+ ",link_2_4.ARUNMIS1 as ARUNMIS1204"+ ",link_2_4.ARUNMIS2 as ARUNMIS2204"+ ",link_2_4.ARFLSERG as ARFLSERG204"+ ",link_2_4.ARTIPSER as ARTIPSER204"+ ",link_2_4.ARFLUSEP as ARFLUSEP204"+ ",link_2_4.AROPERAT as AROPERAT204"+ ",link_2_4.ARMOLTIP as ARMOLTIP204"+ ",link_2_4.ARCODGRU as ARCODGRU204"+ ",link_2_4.ARCODSOT as ARCODSOT204"+ ",link_2_4.ARCODIVA as ARCODIVA204"+ ",link_2_4.ARGRUMER as ARGRUMER204"+ ",link_2_4.ARCATSCM as ARCATSCM204"+ ",link_2_4.ARPREZUM as ARPREZUM204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on OFF_DETT.ODCODART=link_2_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and OFF_DETT.ODCODART=link_2_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_PERIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_FLFRAZ1 = space(1)
      this.w_MODUM2 = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODCODGRU
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIT_SEZI_IDX,3]
    i_lTable = "TIT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2], .t., this.TIT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATS',True,'TIT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TSCODICE like "+cp_ToStrODBC(trim(this.w_ODCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select TSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TSCODICE',trim(this.w_ODCODGRU))
          select TSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODCODGRU)==trim(_Link_.TSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODCODGRU) and !this.bDontReportError
            deferred_cp_zoom('TIT_SEZI','*','TSCODICE',cp_AbsName(oSource.parent,'oODCODGRU_2_20'),i_cWhere,'GSOF_ATS',"Codice gruppo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',oSource.xKey(1))
            select TSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(this.w_ODCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',this.w_ODCODGRU)
            select TSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODCODGRU = NVL(_Link_.TSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ODCODGRU = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.TSCODICE,1)
      cp_ShowWarn(i_cKey,this.TIT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODCODSOT
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_lTable = "SOT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2], .t., this.SOT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODCODSOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ASF',True,'SOT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SGCODSOT like "+cp_ToStrODBC(trim(this.w_ODCODSOT)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ODCODGRU);

          i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SGCODGRU,SGCODSOT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SGCODGRU',this.w_ODCODGRU;
                     ,'SGCODSOT',trim(this.w_ODCODSOT))
          select SGCODGRU,SGCODSOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SGCODGRU,SGCODSOT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODCODSOT)==trim(_Link_.SGCODSOT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODCODSOT) and !this.bDontReportError
            deferred_cp_zoom('SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(oSource.parent,'oODCODSOT_2_21'),i_cWhere,'GSOF_ASF',"Codici sottogruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ODCODGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SGCODGRU,SGCODSOT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                     +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SGCODGRU="+cp_ToStrODBC(this.w_ODCODGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',oSource.xKey(1);
                       ,'SGCODSOT',oSource.xKey(2))
            select SGCODGRU,SGCODSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODCODSOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                   +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(this.w_ODCODSOT);
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ODCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',this.w_ODCODGRU;
                       ,'SGCODSOT',this.w_ODCODSOT)
            select SGCODGRU,SGCODSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODCODSOT = NVL(_Link_.SGCODSOT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ODCODSOT = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.SGCODGRU,1)+'\'+cp_ToStr(_Link_.SGCODSOT,1)
      cp_ShowWarn(i_cKey,this.SOT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODCODSOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODUNIMIS
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ODUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ODUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oODUNIMIS_2_23'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ODUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ODUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ODUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_ODUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_ODUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_ODUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_23.UMCODICE as UMCODICE223"+ ",link_2_23.UMFLFRAZ as UMFLFRAZ223"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_23 on OFF_DETT.ODUNIMIS=link_2_23.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_23"
          i_cKey=i_cKey+'+" and OFF_DETT.ODUNIMIS=link_2_23.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oODCODART_2_4.value==this.w_ODCODART)
      this.oPgFrm.Page1.oPag.oODCODART_2_4.value=this.w_ODCODART
      replace t_ODCODART with this.oPgFrm.Page1.oPag.oODCODART_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oUNMIS1_2_11.value==this.w_UNMIS1)
      this.oPgFrm.Page1.oPag.oUNMIS1_2_11.value=this.w_UNMIS1
      replace t_UNMIS1 with this.oPgFrm.Page1.oPag.oUNMIS1_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oODFLOMAG_2_26.RadioValue()==this.w_ODFLOMAG)
      this.oPgFrm.Page1.oPag.oODFLOMAG_2_26.SetRadio()
      replace t_ODFLOMAG with this.oPgFrm.Page1.oPag.oODFLOMAG_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oODARNOCO_2_27.RadioValue()==this.w_ODARNOCO)
      this.oPgFrm.Page1.oPag.oODARNOCO_2_27.SetRadio()
      replace t_ODARNOCO with this.oPgFrm.Page1.oPag.oODARNOCO_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oODQTAUM1_2_28.value==this.w_ODQTAUM1)
      this.oPgFrm.Page1.oPag.oODQTAUM1_2_28.value=this.w_ODQTAUM1
      replace t_ODQTAUM1 with this.oPgFrm.Page1.oPag.oODQTAUM1_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oODSCONT1_2_31.value==this.w_ODSCONT1)
      this.oPgFrm.Page1.oPag.oODSCONT1_2_31.value=this.w_ODSCONT1
      replace t_ODSCONT1 with this.oPgFrm.Page1.oPag.oODSCONT1_2_31.value
    endif
    if not(this.oPgFrm.Page1.oPag.oODSCONT2_2_32.value==this.w_ODSCONT2)
      this.oPgFrm.Page1.oPag.oODSCONT2_2_32.value=this.w_ODSCONT2
      replace t_ODSCONT2 with this.oPgFrm.Page1.oPag.oODSCONT2_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oODSCONT3_2_33.value==this.w_ODSCONT3)
      this.oPgFrm.Page1.oPag.oODSCONT3_2_33.value=this.w_ODSCONT3
      replace t_ODSCONT3 with this.oPgFrm.Page1.oPag.oODSCONT3_2_33.value
    endif
    if not(this.oPgFrm.Page1.oPag.oODSCONT4_2_34.value==this.w_ODSCONT4)
      this.oPgFrm.Page1.oPag.oODSCONT4_2_34.value=this.w_ODSCONT4
      replace t_ODSCONT4 with this.oPgFrm.Page1.oPag.oODSCONT4_2_34.value
    endif
    if not(this.oPgFrm.Page1.oPag.oODDATPCO_2_35.value==this.w_ODDATPCO)
      this.oPgFrm.Page1.oPag.oODDATPCO_2_35.value=this.w_ODDATPCO
      replace t_ODDATPCO with this.oPgFrm.Page1.oPag.oODDATPCO_2_35.value
    endif
    if not(this.oPgFrm.Page1.oPag.oODVALRIG_2_36.value==this.w_ODVALRIG)
      this.oPgFrm.Page1.oPag.oODVALRIG_2_36.value=this.w_ODVALRIG
      replace t_ODVALRIG with this.oPgFrm.Page1.oPag.oODVALRIG_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRIG_3_4.value==this.w_TOTRIG)
      this.oPgFrm.Page1.oPag.oTOTRIG_3_4.value=this.w_TOTRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_1.value==this.w_ODCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_1.value=this.w_ODCODICE
      replace t_ODCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODGRU_2_20.value==this.w_ODCODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODGRU_2_20.value=this.w_ODCODGRU
      replace t_ODCODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODGRU_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODSOT_2_21.value==this.w_ODCODSOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODSOT_2_21.value=this.w_ODCODSOT
      replace t_ODCODSOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODSOT_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODDESART_2_22.value==this.w_ODDESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODDESART_2_22.value=this.w_ODDESART
      replace t_ODDESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODDESART_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODUNIMIS_2_23.value==this.w_ODUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODUNIMIS_2_23.value=this.w_ODUNIMIS
      replace t_ODUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODUNIMIS_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODQTAMOV_2_25.value==this.w_ODQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODQTAMOV_2_25.value=this.w_ODQTAMOV
      replace t_ODQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODQTAMOV_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODPREZZO_2_30.value==this.w_ODPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODPREZZO_2_30.value=this.w_ODPREZZO
      replace t_ODPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODPREZZO_2_30.value
    endif
    cp_SetControlsValueExtFlds(this,'OFF_DETT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_mdo
      *Controllo il numero di righe del dettaglio dell'offerta
      if i_bRes
         if this.oParentObject.w_OFSTATUS<>'I' and this.NumRow()<1
           Ah_ErrorMsg('Inserire almeno una riga di dettaglio',"!")
           i_bRes=.f.
         endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_ODUNIMIS) or not(CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_ODUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_ODUNIMIS))) and (.w_ODTIPRIG $ 'RM' AND NOT EMPTY(.w_ODCODICE) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S')) and (NOT EMPTY(.w_CPROWORD) AND NOT EMPTY(.w_ODCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODUNIMIS_2_23
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   not((.w_ODQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_ODQTAMOV=INT(.w_ODQTAMOV))) OR NOT .w_ODTIPRIG $ 'RM') and (.w_ODTIPRIG $ 'RMA' AND NOT EMPTY(.w_ODCODICE)) and (NOT EMPTY(.w_CPROWORD) AND NOT EMPTY(.w_ODCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODQTAMOV_2_25
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
      endcase
      if NOT EMPTY(.w_CPROWORD) AND NOT EMPTY(.w_ODCODICE)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ODCODICE = this.w_ODCODICE
    this.o_DECTOT = this.w_DECTOT
    this.o_DECUNI = this.w_DECUNI
    this.o_ODCODART = this.w_ODCODART
    this.o_ODCODGRU = this.w_ODCODGRU
    this.o_ODUNIMIS = this.w_ODUNIMIS
    this.o_ODQTAMOV = this.w_ODQTAMOV
    this.o_ODFLOMAG = this.w_ODFLOMAG
    this.o_LIPREZZO = this.w_LIPREZZO
    this.o_ODPREZZO = this.w_ODPREZZO
    this.o_ODSCONT1 = this.w_ODSCONT1
    this.o_ODSCONT2 = this.w_ODSCONT2
    this.o_ODSCONT3 = this.w_ODSCONT3
    this.o_ODSCONT4 = this.w_ODSCONT4
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODCODICE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ODCODICE=space(20)
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_ODTIPRIG=space(1)
      .w_ODCODART=space(20)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_OPERA3=space(1)
      .w_CODIVA=space(5)
      .w_PERIVA=0
      .w_ODNOTAGG=space(0)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_FLUSEP=space(1)
      .w_FLSERG=space(1)
      .w_FLSERA=space(1)
      .w_GRUMER=space(5)
      .w_CATSCA=space(5)
      .w_ODCODGRU=space(5)
      .w_ODCODSOT=space(5)
      .w_ODDESART=space(40)
      .w_ODUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_ODQTAMOV=0
      .w_ODFLOMAG=space(1)
      .w_ODARNOCO=space(1)
      .w_ODQTAUM1=0
      .w_LIPREZZO=0
      .w_ODPREZZO=0
      .w_ODSCONT1=0
      .w_ODSCONT2=0
      .w_ODSCONT3=0
      .w_ODSCONT4=0
      .w_ODDATPCO=ctod("  /  /  ")
      .w_ODVALRIG=0
      .w_RIGNET=0
      .w_RIGNET1=0
      .w_ODCONTRA=space(15)
      .w_OFLSCO=space(1)
      .w_ARTNOC=space(1)
      .w_LISCON=0
      .w_IVASCOR=0
      .w_ACTSCOR=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_GIACON=space(1)
      .w_ODTIPPRO=space(2)
      .w_ODTIPPR2=space(2)
      .w_ODPERPRO=0
      .w_ODPROCAP=0
      .w_FLFRAZ1=space(1)
      .w_MODUM2=space(10)
      .w_CLUNIMIS=space(3)
      .w_PREZUM=space(1)
      .w_UMCAL=space(14)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_ODCODICE))
        .link_2_1('Full')
      endif
      .DoRTCalc(3,18,.f.)
      if not(empty(.w_ODCODART))
        .link_2_4('Full')
      endif
      .DoRTCalc(19,22,.f.)
      if not(empty(.w_CODIVA))
        .link_2_8('Full')
      endif
      .DoRTCalc(23,25,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_11('Full')
      endif
      .DoRTCalc(26,34,.f.)
      if not(empty(.w_ODCODGRU))
        .link_2_20('Full')
      endif
      .DoRTCalc(35,35,.f.)
      if not(empty(.w_ODCODSOT))
        .link_2_21('Full')
      endif
      .DoRTCalc(36,36,.f.)
        .w_ODUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
      .DoRTCalc(37,37,.f.)
      if not(empty(.w_ODUNIMIS))
        .link_2_23('Full')
      endif
      .DoRTCalc(38,38,.f.)
        .w_ODQTAMOV = IIF(.w_ODTIPRIG='F', 1, IIF(.w_ODTIPRIG='D', 0, .w_ODQTAMOV))
        .w_ODFLOMAG = 'X'
        .w_ODARNOCO = ' '
        .w_ODQTAUM1 = IIF(.w_ODTIPRIG='F', 1, .w_ODQTAUM1)
      .DoRTCalc(43,43,.f.)
        .w_ODPREZZO = IIF((.w_ODTIPRIG $ 'RFMA'), cp_Round(CALMMLIS(.w_LIPREZZO, .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_ODUNIMIS+.w_OPERAT+.w_OPERA3+IIF(.w_LISCON=2, .w_IVALIS, 'N')+'P'+ALLTRIM(STR(.w_DECUNI)), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTIP,1), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTI3,1), IIF(.w_FLSCOR='S',0,.w_PERIVA)), .w_DECUNI), 0)
      .DoRTCalc(45,49,.f.)
        .w_ODVALRIG = CAVALRIG(.w_ODPREZZO,.w_ODQTAMOV, .w_ODSCONT1,.w_ODSCONT2,.w_ODSCONT3,.w_ODSCONT4,.w_DECTOT)
        .w_RIGNET = IIF(.w_ODFLOMAG='X' AND .w_FLSERA<>'S', .w_ODVALRIG, 0)
        .w_RIGNET1 = IIF(.w_ODFLOMAG='X', .w_ODVALRIG, 0)
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_ODUNIMIS=.w_UNMIS1 OR EMPTY(.w_ODCODICE), '',ah_msgformat( 'Qta nella 1^UM:')))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_45.Calculate()
      .DoRTCalc(53,56,.f.)
        .w_OFLSCO = ' '
        .w_ARTNOC = .w_ODARNOCO
      .DoRTCalc(59,64,.f.)
        .w_ACTSCOR = .F.
        .w_OBTEST = .w_DATDOC
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_56.Calculate()
      .DoRTCalc(67,68,.f.)
        .w_GIACON = IIF(.cFunction='Load', ' ', 'X')
        .w_ODTIPPRO = IIF(.w_ODFLOMAG='X','DC','ES')
        .w_ODTIPPR2 = IIF(.w_ODFLOMAG='X','DC','ES')
    endwith
    this.DoRTCalc(72,78,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ODCODICE = t_ODCODICE
    this.w_CPROWORD = t_CPROWORD
    this.w_ODTIPRIG = t_ODTIPRIG
    this.w_ODCODART = t_ODCODART
    this.w_UNMIS3 = t_UNMIS3
    this.w_MOLTI3 = t_MOLTI3
    this.w_OPERA3 = t_OPERA3
    this.w_CODIVA = t_CODIVA
    this.w_PERIVA = t_PERIVA
    this.w_ODNOTAGG = t_ODNOTAGG
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_MOLTIP = t_MOLTIP
    this.w_OPERAT = t_OPERAT
    this.w_FLUSEP = t_FLUSEP
    this.w_FLSERG = t_FLSERG
    this.w_FLSERA = t_FLSERA
    this.w_GRUMER = t_GRUMER
    this.w_CATSCA = t_CATSCA
    this.w_ODCODGRU = t_ODCODGRU
    this.w_ODCODSOT = t_ODCODSOT
    this.w_ODDESART = t_ODDESART
    this.w_ODUNIMIS = t_ODUNIMIS
    this.w_FLFRAZ = t_FLFRAZ
    this.w_ODQTAMOV = t_ODQTAMOV
    this.w_ODFLOMAG = this.oPgFrm.Page1.oPag.oODFLOMAG_2_26.RadioValue(.t.)
    this.w_ODARNOCO = this.oPgFrm.Page1.oPag.oODARNOCO_2_27.RadioValue(.t.)
    this.w_ODQTAUM1 = t_ODQTAUM1
    this.w_LIPREZZO = t_LIPREZZO
    this.w_ODPREZZO = t_ODPREZZO
    this.w_ODSCONT1 = t_ODSCONT1
    this.w_ODSCONT2 = t_ODSCONT2
    this.w_ODSCONT3 = t_ODSCONT3
    this.w_ODSCONT4 = t_ODSCONT4
    this.w_ODDATPCO = t_ODDATPCO
    this.w_ODVALRIG = t_ODVALRIG
    this.w_RIGNET = t_RIGNET
    this.w_RIGNET1 = t_RIGNET1
    this.w_ODCONTRA = t_ODCONTRA
    this.w_OFLSCO = t_OFLSCO
    this.w_ARTNOC = t_ARTNOC
    this.w_LISCON = t_LISCON
    this.w_IVASCOR = t_IVASCOR
    this.w_ACTSCOR = t_ACTSCOR
    this.w_OBTEST = t_OBTEST
    this.w_GIACON = t_GIACON
    this.w_ODTIPPRO = t_ODTIPPRO
    this.w_ODTIPPR2 = t_ODTIPPR2
    this.w_ODPERPRO = t_ODPERPRO
    this.w_ODPROCAP = t_ODPROCAP
    this.w_FLFRAZ1 = t_FLFRAZ1
    this.w_MODUM2 = t_MODUM2
    this.w_CLUNIMIS = t_CLUNIMIS
    this.w_PREZUM = t_PREZUM
    this.w_UMCAL = t_UMCAL
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ODCODICE with this.w_ODCODICE
    replace t_CPROWORD with this.w_CPROWORD
    replace t_ODTIPRIG with this.w_ODTIPRIG
    replace t_ODCODART with this.w_ODCODART
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_OPERA3 with this.w_OPERA3
    replace t_CODIVA with this.w_CODIVA
    replace t_PERIVA with this.w_PERIVA
    replace t_ODNOTAGG with this.w_ODNOTAGG
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_MOLTIP with this.w_MOLTIP
    replace t_OPERAT with this.w_OPERAT
    replace t_FLUSEP with this.w_FLUSEP
    replace t_FLSERG with this.w_FLSERG
    replace t_FLSERA with this.w_FLSERA
    replace t_GRUMER with this.w_GRUMER
    replace t_CATSCA with this.w_CATSCA
    replace t_ODCODGRU with this.w_ODCODGRU
    replace t_ODCODSOT with this.w_ODCODSOT
    replace t_ODDESART with this.w_ODDESART
    replace t_ODUNIMIS with this.w_ODUNIMIS
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_ODQTAMOV with this.w_ODQTAMOV
    replace t_ODFLOMAG with this.oPgFrm.Page1.oPag.oODFLOMAG_2_26.ToRadio()
    replace t_ODARNOCO with this.oPgFrm.Page1.oPag.oODARNOCO_2_27.ToRadio()
    replace t_ODQTAUM1 with this.w_ODQTAUM1
    replace t_LIPREZZO with this.w_LIPREZZO
    replace t_ODPREZZO with this.w_ODPREZZO
    replace t_ODSCONT1 with this.w_ODSCONT1
    replace t_ODSCONT2 with this.w_ODSCONT2
    replace t_ODSCONT3 with this.w_ODSCONT3
    replace t_ODSCONT4 with this.w_ODSCONT4
    replace t_ODDATPCO with this.w_ODDATPCO
    replace t_ODVALRIG with this.w_ODVALRIG
    replace t_RIGNET with this.w_RIGNET
    replace t_RIGNET1 with this.w_RIGNET1
    replace t_ODCONTRA with this.w_ODCONTRA
    replace t_OFLSCO with this.w_OFLSCO
    replace t_ARTNOC with this.w_ARTNOC
    replace t_LISCON with this.w_LISCON
    replace t_IVASCOR with this.w_IVASCOR
    replace t_ACTSCOR with this.w_ACTSCOR
    replace t_OBTEST with this.w_OBTEST
    replace t_GIACON with this.w_GIACON
    replace t_ODTIPPRO with this.w_ODTIPPRO
    replace t_ODTIPPR2 with this.w_ODTIPPR2
    replace t_ODPERPRO with this.w_ODPERPRO
    replace t_ODPROCAP with this.w_ODPROCAP
    replace t_FLFRAZ1 with this.w_FLFRAZ1
    replace t_MODUM2 with this.w_MODUM2
    replace t_CLUNIMIS with this.w_CLUNIMIS
    replace t_PREZUM with this.w_PREZUM
    replace t_UMCAL with this.w_UMCAL
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTRIG = .w_TOTRIG-.w_odvalrig
        .w_TOTMERCE = .w_TOTMERCE-.w_rignet
        .w_TOTRIPD = .w_TOTRIPD-.w_rignet1
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsof_mdoPag1 as StdContainer
  Width  = 805
  height = 378
  stdWidth  = 805
  stdheight = 378
  resizeXpos=400
  resizeYpos=233
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_21 as cp_runprogram with uid="UJBKJJAGKH",left=467, top=392, width=183,height=19,;
    caption='GSOF_BK0',;
   bGlobalFont=.t.,;
    prg="GSOF_BK0",;
    cEvent = "ConfermaArticoli",;
    nPag=1;
    , HelpContextID = 57745046


  add object oObj_1_22 as cp_runprogram with uid="EBCNHWYSGV",left=466, top=410, width=183,height=19,;
    caption='GSOF_BK2',;
   bGlobalFont=.t.,;
    prg="GSOF_BK2",;
    cEvent = "ConfermaRighe",;
    nPag=1;
    , HelpContextID = 57745048


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=4, width=792,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="ODCODICE",Label2="Articolo",Field3="ODCODGRU",Label3="Gruppo",Field4="ODCODSOT",Label4="Sottogr.",Field5="ODDESART",Label5="Descrizione",Field6="ODUNIMIS",Label6="U.M.",Field7="ODQTAMOV",Label7="Quantit�",Field8="ODPREZZO",Label8="Prezzo unitario",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32702342

  add object oStr_1_15 as StdString with uid="JICAJYOVUU",Visible=.t., Left=350, Top=329,;
    Alignment=2, Width=13, Height=13,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<2)
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="YWAHJHMMRB",Visible=.t., Left=417, Top=329,;
    Alignment=2, Width=13, Height=13,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<3)
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="FVBLITXHIH",Visible=.t., Left=483, Top=329,;
    Alignment=2, Width=13, Height=13,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<4)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=23,;
    width=788+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=24,width=787+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|TIT_SEZI|SOT_SEZI|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oODCODART_2_4.Refresh()
      this.Parent.oUNMIS1_2_11.Refresh()
      this.Parent.oODFLOMAG_2_26.Refresh()
      this.Parent.oODARNOCO_2_27.Refresh()
      this.Parent.oODQTAUM1_2_28.Refresh()
      this.Parent.oODSCONT1_2_31.Refresh()
      this.Parent.oODSCONT2_2_32.Refresh()
      this.Parent.oODSCONT3_2_33.Refresh()
      this.Parent.oODSCONT4_2_34.Refresh()
      this.Parent.oODDATPCO_2_35.Refresh()
      this.Parent.oODVALRIG_2_36.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oODCODICE_2_1
      case cFile='TIT_SEZI'
        oDropInto=this.oBodyCol.oRow.oODCODGRU_2_20
      case cFile='SOT_SEZI'
        oDropInto=this.oBodyCol.oRow.oODCODSOT_2_21
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oODUNIMIS_2_23
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oODCODART_2_4 as StdTrsField with uid="ILSFMVFBNM",rtseq=18,rtrep=.t.,;
    cFormVar="w_ODCODART",value=space(20),enabled=.f.,;
    HelpContextID = 255242182,;
    cTotal="", bFixedPos=.t., cQueryName = "ODCODART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=22, Width=92, Left=856, Top=198, InputMask=replicate('X',20), cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_ODCODART"

  func oODCODART_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.T.)
    endwith
    endif
  endfunc

  func oODCODART_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oUNMIS1_2_11 as StdTrsField with uid="TGZAXWRTJP",rtseq=25,rtrep=.t.,;
    cFormVar="w_UNMIS1",value=space(3),enabled=.f.,;
    HelpContextID = 239863226,;
    cTotal="", bFixedPos=.t., cQueryName = "UNMIS1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=29, Left=441, Top=298, InputMask=replicate('X',3), cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_UNMIS1"

  func oUNMIS1_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ODUNIMIS=.w_UNMIS1 OR EMPTY(.w_ODCODICE))
    endwith
    endif
  endfunc

  func oUNMIS1_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oODFLOMAG_2_26 as StdTrsCombo with uid="KNGAJRPZOO",rtrep=.t.,;
    cFormVar="w_ODFLOMAG", RowSource=""+"Normale,"+"Sconto merce,"+"Omaggio imponibile,"+"Omaggio imp. + IVA" , ;
    ToolTipText = "Test riga omaggio/sconto merce o normale",;
    HelpContextID = 42565587,;
    Height=25, Width=132, Left=6, Top=298,;
    cTotal="", cQueryName = "ODFLOMAG",;
    bObbl = .f. , nPag=2  , tabstop=.f.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oODFLOMAG_2_26.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ODFLOMAG,&i_cF..t_ODFLOMAG),this.value)
    return(iif(xVal =1,'X',;
    iif(xVal =2,'S',;
    iif(xVal =3,'I',;
    iif(xVal =4,'E',;
    space(1))))))
  endfunc
  func oODFLOMAG_2_26.GetRadio()
    this.Parent.oContained.w_ODFLOMAG = this.RadioValue()
    return .t.
  endfunc

  func oODFLOMAG_2_26.ToRadio()
    this.Parent.oContained.w_ODFLOMAG=trim(this.Parent.oContained.w_ODFLOMAG)
    return(;
      iif(this.Parent.oContained.w_ODFLOMAG=='X',1,;
      iif(this.Parent.oContained.w_ODFLOMAG=='S',2,;
      iif(this.Parent.oContained.w_ODFLOMAG=='I',3,;
      iif(this.Parent.oContained.w_ODFLOMAG=='E',4,;
      0)))))
  endfunc

  func oODFLOMAG_2_26.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oODARNOCO_2_27 as StdTrsCheck with uid="OHKZSHNOMX",rtrep=.t.,;
    cFormVar="w_ODARNOCO",  caption="Articolo da ridefinire",;
    ToolTipText = "Se attivo: identifica una codifica generica a fronte di un articolo/servizio da ridefinire prima della conferma",;
    HelpContextID = 9686987,;
    Left=155, Top=298,;
    cTotal="", cQueryName = "ODARNOCO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oODARNOCO_2_27.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ODARNOCO,&i_cF..t_ODARNOCO),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oODARNOCO_2_27.GetRadio()
    this.Parent.oContained.w_ODARNOCO = this.RadioValue()
    return .t.
  endfunc

  func oODARNOCO_2_27.ToRadio()
    this.Parent.oContained.w_ODARNOCO=trim(this.Parent.oContained.w_ODARNOCO)
    return(;
      iif(this.Parent.oContained.w_ODARNOCO=='S',1,;
      0))
  endfunc

  func oODARNOCO_2_27.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oODARNOCO_2_27.mCond()
    with this.Parent.oContained
      return (.w_ODTIPRIG<>'D')
    endwith
  endfunc

  add object oODQTAUM1_2_28 as StdTrsField with uid="TAOYKHCBPX",rtseq=42,rtrep=.t.,;
    cFormVar="w_ODQTAUM1",value=0,;
    ToolTipText = "Quantit� movimentata riferita alla 1^UM",;
    HelpContextID = 190894057,;
    cTotal="", bFixedPos=.t., cQueryName = "ODQTAUM1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=473, Top=298, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)]

  func oODQTAUM1_2_28.mCond()
    with this.Parent.oContained
      return (.w_FLUSEP$'SQ' AND .w_ODUNIMIS<>.w_UNMIS1 AND .w_ODTIPRIG $ 'RMA' AND NOT EMPTY(.w_ODCODICE))
    endwith
  endfunc

  func oODQTAUM1_2_28.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ODUNIMIS=.w_UNMIS1 OR EMPTY(.w_ODCODICE))
    endwith
    endif
  endfunc

  add object oODSCONT1_2_31 as StdTrsField with uid="MJOIFTABZS",rtseq=45,rtrep=.t.,;
    cFormVar="w_ODSCONT1",value=0,;
    ToolTipText = "1^% Maggiorazione (se positiva) o sconto (se negativa) di riga",;
    HelpContextID = 26324969,;
    cTotal="", bFixedPos=.t., cQueryName = "ODSCONT1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=294, Top=326, cSayPict=["999.99"], cGetPict=["999.99"]

  func oODSCONT1_2_31.mCond()
    with this.Parent.oContained
      return (.w_NUMSCO>0 AND .w_ODTIPRIG $ 'RFMA')
    endwith
  endfunc

  add object oODSCONT2_2_32 as StdTrsField with uid="ZSXHFAMHZR",rtseq=46,rtrep=.t.,;
    cFormVar="w_ODSCONT2",value=0,;
    ToolTipText = "2^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 26324968,;
    cTotal="", bFixedPos=.t., cQueryName = "ODSCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=361, Top=326, cSayPict=["999.99"], cGetPict=["999.99"]

  func oODSCONT2_2_32.mCond()
    with this.Parent.oContained
      return (.w_ODSCONT1<>0 AND .w_ODTIPRIG $ 'RFMA')
    endwith
  endfunc

  func oODSCONT2_2_32.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO<2)
    endwith
    endif
  endfunc

  add object oODSCONT3_2_33 as StdTrsField with uid="NERXQIVVWS",rtseq=47,rtrep=.t.,;
    cFormVar="w_ODSCONT3",value=0,;
    ToolTipText = "3^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 26324967,;
    cTotal="", bFixedPos=.t., cQueryName = "ODSCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=428, Top=326, cSayPict=["999.99"], cGetPict=["999.99"]

  func oODSCONT3_2_33.mCond()
    with this.Parent.oContained
      return (.w_ODSCONT2<>0 AND .w_ODTIPRIG $ 'RFMA')
    endwith
  endfunc

  func oODSCONT3_2_33.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO<3)
    endwith
    endif
  endfunc

  add object oODSCONT4_2_34 as StdTrsField with uid="MROBVAWCSJ",rtseq=48,rtrep=.t.,;
    cFormVar="w_ODSCONT4",value=0,;
    ToolTipText = "4^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 26324966,;
    cTotal="", bFixedPos=.t., cQueryName = "ODSCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=495, Top=326, cSayPict=["999.99"], cGetPict=["999.99"]

  func oODSCONT4_2_34.mCond()
    with this.Parent.oContained
      return (.w_ODSCONT3<>0 AND .w_ODTIPRIG $ 'RFMA')
    endwith
  endfunc

  func oODSCONT4_2_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO<4)
    endwith
    endif
  endfunc

  add object oODDATPCO_2_35 as StdTrsField with uid="GJPWHSWACV",rtseq=49,rtrep=.t.,;
    cFormVar="w_ODDATPCO",value=ctod("  /  /  "),;
    ToolTipText = "Data prevista consegna",;
    HelpContextID = 256155595,;
    cTotal="", bFixedPos=.t., cQueryName = "ODDATPCO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=294, Top=350

  add object oODVALRIG_2_36 as StdTrsField with uid="XLOXGNSPPM",rtseq=50,rtrep=.t.,;
    cFormVar="w_ODVALRIG",value=0,enabled=.f.,;
    ToolTipText = "Importo totale di riga",;
    HelpContextID = 37519405,;
    cTotal="this.Parent.oContained.w_totrig", bFixedPos=.t., cQueryName = "ODVALRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=632, Top=298, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oObj_2_39 as cp_calclbl with uid="CQSIWRBIHF",width=104,height=18,;
   left=333, top=298,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 170795546

  add object oObj_2_40 as cp_runprogram with uid="FVXTCQLNMH",width=203,height=19,;
   left=12, top=391,;
    caption='GSOF_BI6(K)',;
   bGlobalFont=.t.,;
    prg="GSOF_BI6('K')",;
    cEvent = "ImportaKit",;
    nPag=2;
    , HelpContextID = 57932828

  add object oObj_2_41 as cp_runprogram with uid="VAPWMINFAO",width=240,height=19,;
   left=219, top=391,;
    caption='GSOF_BCD',;
   bGlobalFont=.t.,;
    prg="GSOF_BCD('A')",;
    cEvent = "w_ODQTAMOV Changed",;
    nPag=2;
    , HelpContextID = 210690390

  add object oObj_2_42 as cp_runprogram with uid="VPMDXEIVYV",width=240,height=19,;
   left=219, top=409,;
    caption='GSOF_BCD',;
   bGlobalFont=.t.,;
    prg="GSOF_BCD('M')",;
    cEvent = "w_ODCODICE Changed",;
    nPag=2;
    , HelpContextID = 210690390

  add object oObj_2_43 as cp_runprogram with uid="QBZEXATMZL",width=240,height=19,;
   left=219, top=427,;
    caption='GSOF_BCD',;
   bGlobalFont=.t.,;
    prg="GSOF_BCD('R')",;
    cEvent = "Ricalcola",;
    nPag=2;
    , HelpContextID = 210690390

  add object oObj_2_44 as cp_runprogram with uid="CEPPYQHJJO",width=240,height=19,;
   left=219, top=445,;
    caption='GSOF_BCD',;
   bGlobalFont=.t.,;
    prg="GSOF_BCD('U')",;
    cEvent = "w_ODUNIMIS Changed",;
    nPag=2;
    , HelpContextID = 210690390

  add object oObj_2_45 as cp_runprogram with uid="LFBTJMHTQP",width=240,height=19,;
   left=219, top=463,;
    caption='GSOF_BCD',;
   bGlobalFont=.t.,;
    prg="GSOF_BCD('P')",;
    cEvent = "w_ODQTAUM1 Changed",;
    nPag=2;
    , HelpContextID = 210690390

  add object oObj_2_52 as cp_runprogram with uid="MGHHHBORCZ",width=203,height=19,;
   left=12, top=409,;
    caption='GSOF_BA2(L)',;
   bGlobalFont=.t.,;
    prg="GSOF_BA2('L')",;
    cEvent = "AggiornaRighe1",;
    nPag=2;
    , HelpContextID = 210502376

  add object oObj_2_53 as cp_runprogram with uid="KAHKYNSWXH",width=203,height=19,;
   left=12, top=427,;
    caption='GSOF_BA2(V)',;
   bGlobalFont=.t.,;
    prg="GSOF_BA2('V')",;
    cEvent = "AggiornaRighe2",;
    nPag=2;
    , HelpContextID = 210499816

  add object oObj_2_54 as cp_runprogram with uid="TVCGJGUCLX",width=203,height=19,;
   left=12, top=481,;
    caption='GSOF_BI6(O)',;
   bGlobalFont=.t.,;
    prg="GSOF_BI6('O')",;
    cEvent = "ImportaOfferte",;
    nPag=2;
    , HelpContextID = 57933852

  add object oObj_2_55 as cp_runprogram with uid="IYZUKAAQUQ",width=203,height=19,;
   left=12, top=445,;
    caption='GSOF_BA2(S)',;
   bGlobalFont=.t.,;
    prg="GSOF_BA2('S')",;
    cEvent = "AggiornaRighe3",;
    nPag=2;
    , HelpContextID = 210500584

  add object oObj_2_56 as cp_runprogram with uid="WMJJIEVSZC",width=203,height=19,;
   left=12, top=463,;
    caption='GSOF_BA2(D)',;
   bGlobalFont=.t.,;
    prg="GSOF_BA2('D')",;
    cEvent = "AggiornaRighe4",;
    nPag=2;
    , HelpContextID = 210504424

  add object oBtn_2_57 as StdButton with uid="BGSAEWIFZZ",width=48,height=45,;
   left=7, top=322,;
    CpPicture="BMP\Costi.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere agli ultimi prezzi al cliente";
    , HelpContextID = 188079174;
    , tabstop=.f., Caption='\<Ult.prezzi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_57.Click()
      with this.Parent.oContained
        GSAR_BLU(this.Parent.oContained,"OU")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_57.mCond()
    with this.Parent.oContained
      return (.w_ODTIPRIG='R' AND NOT EMPTY(.w_ODCODART))
    endwith
  endfunc

  add object oBtn_2_58 as StdButton with uid="YOECWZONEB",width=48,height=45,;
   left=57, top=322,;
    CpPicture="BMP\ULTACQ.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere agli ultimi costi da tutti i fornitori";
    , HelpContextID = 188078886;
    , tabStop=.f.,Caption='\<Costi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_58.Click()
      with this.Parent.oContained
        GSAR_BLU(this.Parent.oContained,"OC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_58.mCond()
    with this.Parent.oContained
      return (.w_ODTIPRIG='R' AND NOT EMPTY(.w_ODCODART) AND g_ACQU='S')
    endwith
  endfunc

  func oBtn_2_58.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ACQU<>'S')
    endwith
   endif
  endfunc

  add object oBtn_2_59 as StdButton with uid="NNVHEFBBUC",width=48,height=45,;
   left=107, top=322,;
    CpPicture="BMP\ULTVEN.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere agli articoli/prezzi alternativi";
    , HelpContextID = 188079094;
    , tabstop=.f., Caption='\<Art. alt.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_59.Click()
      do GSAR_KCA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_59.mCond()
    with this.Parent.oContained
      return (.w_ODTIPRIG='R' AND NOT EMPTY(.w_ODCODART))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTRIG_3_4 as StdField with uid="NIXHLOQONO",rtseq=53,rtrep=.f.,;
    cFormVar="w_TOTRIG",value=0,enabled=.f.,;
    ToolTipText = "Totale importi di riga (gi� scontati)",;
    HelpContextID = 149066954,;
    cTotal="", bFixedPos=.t., cQueryName = "TOTRIG",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=632, Top=326, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oStr_3_1 as StdString with uid="HHNFUZIVIH",Visible=.t., Left=556, Top=298,;
    Alignment=1, Width=73, Height=18,;
    Caption="Netto riga:"  ;
  , bGlobalFont=.t.

  add object oStr_3_2 as StdString with uid="QYHQQKFACL",Visible=.t., Left=556, Top=326,;
    Alignment=1, Width=73, Height=18,;
    Caption="Totale righe:"  ;
  , bGlobalFont=.t.

  add object oStr_3_3 as StdString with uid="AMTVZDMFVX",Visible=.t., Left=161, Top=326,;
    Alignment=1, Width=128, Height=18,;
    Caption="Sconti/magg.ni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_7 as StdString with uid="EFVTJFBYRI",Visible=.t., Left=161, Top=351,;
    Alignment=1, Width=128, Height=18,;
    Caption="Data consegna:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsof_mdoBodyRow as CPBodyRowCnt
  Width=778
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oODCODICE_2_1 as StdTrsField with uid="WITWPKDCXL",rtseq=2,rtrep=.t.,;
    cFormVar="w_ODCODICE",value=space(20),;
    ToolTipText = "Codice di ricerca articolo",;
    HelpContextID = 121024469,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente o inesistente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=141, Left=42, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , BackStyle=0, cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_ODCODICE"

  func oODCODICE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oODCODICE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oODCODICE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oODCODICE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSVE_MDV.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oODCODICE_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ODCODICE
    i_obj.ecpSave()
  endproc

  add object oCPROWORD_2_2 as StdTrsField with uid="AVIXBOFWAM",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 373910,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], tabstop=.f.,BackStyle=0

  add object oODCODGRU_2_20 as StdTrsField with uid="TBAIYOVTPH",rtseq=34,rtrep=.t.,;
    cFormVar="w_ODCODGRU",value=space(5),;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 154578885,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=64, Left=188, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , BackStyle=0, cLinkFile="TIT_SEZI", cZoomOnZoom="GSOF_ATS", oKey_1_1="TSCODICE", oKey_1_2="this.w_ODCODGRU"

  func oODCODGRU_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
      if .not. empty(.w_ODCODSOT)
        bRes2=.link_2_21('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oODCODGRU_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oODCODGRU_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIT_SEZI','*','TSCODICE',cp_AbsName(this.parent,'oODCODGRU_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATS',"Codice gruppo",'',this.parent.oContained
  endproc
  proc oODCODGRU_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TSCODICE=this.parent.oContained.w_ODCODGRU
    i_obj.ecpSave()
  endproc

  add object oODCODSOT_2_21 as StdTrsField with uid="JSPHEKCKPA",rtseq=35,rtrep=.t.,;
    cFormVar="w_ODCODSOT",value=space(5),;
    HelpContextID = 221687750,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=72, Left=258, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , BackStyle=0, cLinkFile="SOT_SEZI", cZoomOnZoom="GSOF_ASF", oKey_1_1="SGCODGRU", oKey_1_2="this.w_ODCODGRU", oKey_2_1="SGCODSOT", oKey_2_2="this.w_ODCODSOT"

  func oODCODSOT_2_21.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ODCODGRU))
    endwith
  endfunc

  func oODCODSOT_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oODCODSOT_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oODCODSOT_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SOT_SEZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStrODBC(this.Parent.oContained.w_ODCODGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStr(this.Parent.oContained.w_ODCODGRU)
    endif
    do cp_zoom with 'SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(this.parent,'oODCODSOT_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ASF',"Codici sottogruppi",'',this.parent.oContained
  endproc
  proc oODCODSOT_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ASF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SGCODGRU=w_ODCODGRU
     i_obj.w_SGCODSOT=this.parent.oContained.w_ODCODSOT
    i_obj.ecpSave()
  endproc

  add object oODDESART_2_22 as StdTrsField with uid="NAMECXAXGG",rtseq=36,rtrep=.t.,;
    cFormVar="w_ODDESART",value=space(40),;
    ToolTipText = "Descrizione articolo (F9 = accede alle descrizioni aggiuntive)",;
    HelpContextID = 240164806,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=205, Left=332, Top=0, InputMask=replicate('X',40), bHasZoom = .t. , BackStyle=0

  func oODDESART_2_22.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ODCODICE))
    endwith
  endfunc

  proc oODDESART_2_22.mZoom
    do GSOF_KD2 with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oODUNIMIS_2_23 as StdTrsField with uid="XRVQJJTZCR",rtseq=37,rtrep=.t.,;
    cFormVar="w_ODUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 219770937,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=32, Left=538, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , BackStyle=0, cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ODUNIMIS"

  func oODUNIMIS_2_23.mCond()
    with this.Parent.oContained
      return (.w_ODTIPRIG $ 'RM' AND NOT EMPTY(.w_ODCODICE) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))
    endwith
  endfunc

  func oODUNIMIS_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oODUNIMIS_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oODUNIMIS_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oODUNIMIS_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oODUNIMIS_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ODUNIMIS
    i_obj.ecpSave()
  endproc

  add object oODQTAMOV_2_25 as StdTrsField with uid="KDYMPBNDOS",rtseq=39,rtrep=.t.,;
    cFormVar="w_ODQTAMOV",value=0,;
    HelpContextID = 56676292,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=77, Left=573, Top=0, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)], BackStyle=0

  func oODQTAMOV_2_25.mCond()
    with this.Parent.oContained
      return (.w_ODTIPRIG $ 'RMA' AND NOT EMPTY(.w_ODCODICE))
    endwith
  endfunc

  func oODQTAMOV_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ODQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_ODQTAMOV=INT(.w_ODQTAMOV))) OR NOT .w_ODTIPRIG $ 'RM')
    endwith
    return bRes
  endfunc

  add object oODPREZZO_2_30 as StdTrsField with uid="NTNGHEHTZO",rtseq=44,rtrep=.t.,;
    cFormVar="w_ODPREZZO",value=0,;
    ToolTipText = "Prezzo (F9) = esegue lo scorporo",;
    HelpContextID = 165486645,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=122, Left=651, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)], bHasZoom = .t. , BackStyle=0

  func oODPREZZO_2_30.mCond()
    with this.Parent.oContained
      return (.w_ODTIPRIG $ 'RFMA' AND NOT EMPTY(.w_ODCODICE))
    endwith
  endfunc

  proc oODPREZZO_2_30.mZoom
      with this.Parent.oContained
        GSOF_BCD(this.Parent.oContained,"S")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oODCODICE_2_1.When()
    return(.t.)
  proc oODCODICE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oODCODICE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=13
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_mdo','OFF_DETT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ODSERIAL=OFF_DETT.ODSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
