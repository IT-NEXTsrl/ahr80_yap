* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bi2                                                        *
*              Import kit selezionato                                          *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_227]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bi2",oParentObject)
return(i_retval)

define class tgsof_bi2 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Evento Importa kit selezionato (da GSOF_KIM)
    ND = this.oParentObject.w_ZoomDett.cCursor
    if USED(this.oParentObject.w_ZoomDett.cCursor)
      * --- Selezionato il Documento Master
      SELECT * FROM (ND) WHERE XCHK=1 INTO CURSOR ImpOff ORDER BY CPROWORD
      if Isalt()
        this.oParentObject.oParentObject.GSPR_MDO.NotifyEvent("ImportaKit")
      else
        this.oParentObject.oParentObject.GSOF_MDO.NotifyEvent("ImportaKit")
      endif
    endif
    if USED("impOff")
      SELECT ImpOff
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
