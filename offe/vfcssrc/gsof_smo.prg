* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_smo                                                        *
*              Stampa modelli offerte                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-15                                                      *
* Last revis.: 2007-07-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_smo",oParentObject))

* --- Class definition
define class tgsof_smo as StdForm
  Top    = 67
  Left   = 110

  * --- Standard Properties
  Width  = 550
  Height = 351
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-26"
  HelpContextID=76928663
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  MOD_OFFE_IDX = 0
  GRU_PRIO_IDX = 0
  GRU_NOMI_IDX = 0
  ORI_NOMI_IDX = 0
  UTE_AZI_IDX = 0
  ZONE_IDX = 0
  LINGUE_IDX = 0
  cpusers_IDX = 0
  TIP_DOCU_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsof_smo"
  cComment = "Stampa modelli offerte"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PDATINIZ = ctod('  /  /  ')
  o_PDATINIZ = ctod('  /  /  ')
  w_SMOCOMO1 = space(5)
  w_DESMO1 = space(35)
  w_SMOCOMO2 = space(5)
  w_DESMO2 = space(35)
  w_SMOCODPRI = space(5)
  w_DESPRIO = space(35)
  w_SMOCODGRU = space(5)
  w_DESCGN = space(35)
  w_SMOCODORI = space(5)
  w_DESORI = space(35)
  w_SMOCODOP = 0
  w_DESOPE = space(35)
  w_SMOCODZON = space(3)
  w_DESZONE = space(35)
  w_SMOCODLI = space(3)
  w_DESLING = space(30)
  w_obsodat1 = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_smoPag1","gsof_smo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPDATINIZ_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='MOD_OFFE'
    this.cWorkTables[2]='GRU_PRIO'
    this.cWorkTables[3]='GRU_NOMI'
    this.cWorkTables[4]='ORI_NOMI'
    this.cWorkTables[5]='UTE_AZI'
    this.cWorkTables[6]='ZONE'
    this.cWorkTables[7]='LINGUE'
    this.cWorkTables[8]='cpusers'
    this.cWorkTables[9]='TIP_DOCU'
    this.cWorkTables[10]='VALUTE'
    return(this.OpenAllTables(10))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsof_smo
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PDATINIZ=ctod("  /  /  ")
      .w_SMOCOMO1=space(5)
      .w_DESMO1=space(35)
      .w_SMOCOMO2=space(5)
      .w_DESMO2=space(35)
      .w_SMOCODPRI=space(5)
      .w_DESPRIO=space(35)
      .w_SMOCODGRU=space(5)
      .w_DESCGN=space(35)
      .w_SMOCODORI=space(5)
      .w_DESORI=space(35)
      .w_SMOCODOP=0
      .w_DESOPE=space(35)
      .w_SMOCODZON=space(3)
      .w_DESZONE=space(35)
      .w_SMOCODLI=space(3)
      .w_DESLING=space(30)
      .w_obsodat1=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
        .w_PDATINIZ = i_datsys
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SMOCOMO1))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_SMOCOMO2))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_SMOCODPRI))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_SMOCODGRU))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_SMOCODORI))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_SMOCODOP = IIF(upper(g_APPLICATION) <> "ADHOC REVOLUTION",0,i_CODUTE)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_SMOCODOP))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_SMOCODZON))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_SMOCODLI))
          .link_1_16('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_obsodat1 = 'N'
        .w_OBTEST = .w_PDATINIZ
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
    this.DoRTCalc(20,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,18,.t.)
        if .o_PDATINIZ<>.w_PDATINIZ
            .w_OBTEST = .w_PDATINIZ
        endif
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SMOCOMO1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_lTable = "MOD_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2], .t., this.MOD_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMOCOMO1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AMO',True,'MOD_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_SMOCOMO1)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_SMOCOMO1))
          select MOCODICE,MODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMOCOMO1)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMOCOMO1) and !this.bDontReportError
            deferred_cp_zoom('MOD_OFFE','*','MOCODICE',cp_AbsName(oSource.parent,'oSMOCOMO1_1_2'),i_cWhere,'GSOF_AMO',"Modelli offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMOCOMO1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_SMOCOMO1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_SMOCOMO1)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMOCOMO1 = NVL(_Link_.MOCODICE,space(5))
      this.w_DESMO1 = NVL(_Link_.MODESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SMOCOMO1 = space(5)
      endif
      this.w_DESMO1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_SMOCOMO2) OR  (UPPER(.w_SMOCOMO1))<= UPPER(.w_SMOCOMO2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice modello iniziale � maggiore di quello finale")
        endif
        this.w_SMOCOMO1 = space(5)
        this.w_DESMO1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMOCOMO1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SMOCOMO2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_lTable = "MOD_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2], .t., this.MOD_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMOCOMO2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AMO',True,'MOD_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_SMOCOMO2)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_SMOCOMO2))
          select MOCODICE,MODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMOCOMO2)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMOCOMO2) and !this.bDontReportError
            deferred_cp_zoom('MOD_OFFE','*','MOCODICE',cp_AbsName(oSource.parent,'oSMOCOMO2_1_4'),i_cWhere,'GSOF_AMO',"Modelli offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMOCOMO2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_SMOCOMO2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_SMOCOMO2)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMOCOMO2 = NVL(_Link_.MOCODICE,space(5))
      this.w_DESMO2 = NVL(_Link_.MODESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SMOCOMO2 = space(5)
      endif
      this.w_DESMO2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(UPPER(.w_SMOCOMO1) <= UPPER(.w_SMOCOMO2)) or empty(.w_SMOCOMO2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice modello iniziale � maggiore di quello finale")
        endif
        this.w_SMOCOMO2 = space(5)
        this.w_DESMO2 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMOCOMO2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SMOCODPRI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMOCODPRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsof_agp',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_SMOCODPRI)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_SMOCODPRI))
          select GPCODICE,GPDESCRI,GPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMOCODPRI)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMOCODPRI) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oSMOCODPRI_1_6'),i_cWhere,'gsof_agp',"Gruppi priorit�",'GSOF_ZGP.GRU_PRIO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMOCODPRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_SMOCODPRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_SMOCODPRI)
            select GPCODICE,GPDESCRI,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMOCODPRI = NVL(_Link_.GPCODICE,space(5))
      this.w_DESPRIO = NVL(_Link_.GPDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.GPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SMOCODPRI = space(5)
      endif
      this.w_DESPRIO = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_SMOCODPRI = space(5)
        this.w_DESPRIO = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMOCODPRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SMOCODGRU
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMOCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGN',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_SMOCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_SMOCODGRU))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMOCODGRU)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMOCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oSMOCODGRU_1_8'),i_cWhere,'GSAR_AGN',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMOCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_SMOCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_SMOCODGRU)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMOCODGRU = NVL(_Link_.GNCODICE,space(5))
      this.w_DESCGN = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SMOCODGRU = space(5)
      endif
      this.w_DESCGN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMOCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SMOCODORI
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMOCODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AON',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_SMOCODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_SMOCODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMOCODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMOCODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oSMOCODORI_1_10'),i_cWhere,'GSAR_AON',"Codici origine nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMOCODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_SMOCODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_SMOCODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMOCODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_DESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SMOCODORI = space(5)
      endif
      this.w_DESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMOCODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SMOCODOP
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMOCODOP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_SMOCODOP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_SMOCODOP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SMOCODOP) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oSMOCODOP_1_12'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMOCODOP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_SMOCODOP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_SMOCODOP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMOCODOP = NVL(_Link_.code,0)
      this.w_DESOPE = NVL(_Link_.name,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SMOCODOP = 0
      endif
      this.w_DESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMOCODOP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SMOCODZON
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMOCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_SMOCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_SMOCODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMOCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMOCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oSMOCODZON_1_14'),i_cWhere,'',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMOCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_SMOCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_SMOCODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMOCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZONE = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SMOCODZON = space(3)
      endif
      this.w_DESZONE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMOCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SMOCODLI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMOCODLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_SMOCODLI)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_SMOCODLI))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMOCODLI)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMOCODLI) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oSMOCODLI_1_16'),i_cWhere,'',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMOCODLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_SMOCODLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_SMOCODLI)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMOCODLI = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLING = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_SMOCODLI = space(3)
      endif
      this.w_DESLING = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMOCODLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPDATINIZ_1_1.value==this.w_PDATINIZ)
      this.oPgFrm.Page1.oPag.oPDATINIZ_1_1.value=this.w_PDATINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oSMOCOMO1_1_2.value==this.w_SMOCOMO1)
      this.oPgFrm.Page1.oPag.oSMOCOMO1_1_2.value=this.w_SMOCOMO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMO1_1_3.value==this.w_DESMO1)
      this.oPgFrm.Page1.oPag.oDESMO1_1_3.value=this.w_DESMO1
    endif
    if not(this.oPgFrm.Page1.oPag.oSMOCOMO2_1_4.value==this.w_SMOCOMO2)
      this.oPgFrm.Page1.oPag.oSMOCOMO2_1_4.value=this.w_SMOCOMO2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMO2_1_5.value==this.w_DESMO2)
      this.oPgFrm.Page1.oPag.oDESMO2_1_5.value=this.w_DESMO2
    endif
    if not(this.oPgFrm.Page1.oPag.oSMOCODPRI_1_6.value==this.w_SMOCODPRI)
      this.oPgFrm.Page1.oPag.oSMOCODPRI_1_6.value=this.w_SMOCODPRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRIO_1_7.value==this.w_DESPRIO)
      this.oPgFrm.Page1.oPag.oDESPRIO_1_7.value=this.w_DESPRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oSMOCODGRU_1_8.value==this.w_SMOCODGRU)
      this.oPgFrm.Page1.oPag.oSMOCODGRU_1_8.value=this.w_SMOCODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGN_1_9.value==this.w_DESCGN)
      this.oPgFrm.Page1.oPag.oDESCGN_1_9.value=this.w_DESCGN
    endif
    if not(this.oPgFrm.Page1.oPag.oSMOCODORI_1_10.value==this.w_SMOCODORI)
      this.oPgFrm.Page1.oPag.oSMOCODORI_1_10.value=this.w_SMOCODORI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESORI_1_11.value==this.w_DESORI)
      this.oPgFrm.Page1.oPag.oDESORI_1_11.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page1.oPag.oSMOCODOP_1_12.value==this.w_SMOCODOP)
      this.oPgFrm.Page1.oPag.oSMOCODOP_1_12.value=this.w_SMOCODOP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOPE_1_13.value==this.w_DESOPE)
      this.oPgFrm.Page1.oPag.oDESOPE_1_13.value=this.w_DESOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oSMOCODZON_1_14.value==this.w_SMOCODZON)
      this.oPgFrm.Page1.oPag.oSMOCODZON_1_14.value=this.w_SMOCODZON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESZONE_1_15.value==this.w_DESZONE)
      this.oPgFrm.Page1.oPag.oDESZONE_1_15.value=this.w_DESZONE
    endif
    if not(this.oPgFrm.Page1.oPag.oSMOCODLI_1_16.value==this.w_SMOCODLI)
      this.oPgFrm.Page1.oPag.oSMOCODLI_1_16.value=this.w_SMOCODLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLING_1_17.value==this.w_DESLING)
      this.oPgFrm.Page1.oPag.oDESLING_1_17.value=this.w_DESLING
    endif
    if not(this.oPgFrm.Page1.oPag.oobsodat1_1_18.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page1.oPag.oobsodat1_1_18.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PDATINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDATINIZ_1_1.SetFocus()
            i_bnoObbl = !empty(.w_PDATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_SMOCOMO2) OR  (UPPER(.w_SMOCOMO1))<= UPPER(.w_SMOCOMO2))  and not(empty(.w_SMOCOMO1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSMOCOMO1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice modello iniziale � maggiore di quello finale")
          case   not((UPPER(.w_SMOCOMO1) <= UPPER(.w_SMOCOMO2)) or empty(.w_SMOCOMO2))  and not(empty(.w_SMOCOMO2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSMOCOMO2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice modello iniziale � maggiore di quello finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_SMOCODPRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSMOCODPRI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PDATINIZ = this.w_PDATINIZ
    return

enddefine

* --- Define pages as container
define class tgsof_smoPag1 as StdContainer
  Width  = 546
  height = 351
  stdWidth  = 546
  stdheight = 351
  resizeXpos=332
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPDATINIZ_1_1 as StdField with uid="SATTXTLERE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PDATINIZ", cQueryName = "PDATINIZ",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza e di inizio validit�",;
    HelpContextID = 125710416,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=164, Top=2

  add object oSMOCOMO1_1_2 as StdField with uid="FLQRTXUTOU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SMOCOMO1", cQueryName = "SMOCOMO1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice modello iniziale � maggiore di quello finale",;
    ToolTipText = "Dal codice modello offerta",;
    HelpContextID = 154265257,;
   bGlobalFont=.t.,;
    Height=21, Width=82, Left=164, Top=27, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MOD_OFFE", cZoomOnZoom="GSOF_AMO", oKey_1_1="MOCODICE", oKey_1_2="this.w_SMOCOMO1"

  func oSMOCOMO1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMOCOMO1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMOCOMO1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_OFFE','*','MOCODICE',cp_AbsName(this.parent,'oSMOCOMO1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AMO',"Modelli offerta",'',this.parent.oContained
  endproc
  proc oSMOCOMO1_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AMO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_SMOCOMO1
     i_obj.ecpSave()
  endproc

  add object oDESMO1_1_3 as StdField with uid="HATUFIKTQL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESMO1", cQueryName = "DESMO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 86486986,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=252, Top=27, InputMask=replicate('X',35)

  add object oSMOCOMO2_1_4 as StdField with uid="CXAQMLPLPX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SMOCOMO2", cQueryName = "SMOCOMO2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice modello iniziale � maggiore di quello finale",;
    ToolTipText = "Al codice modello offerta",;
    HelpContextID = 154265256,;
   bGlobalFont=.t.,;
    Height=21, Width=82, Left=164, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MOD_OFFE", cZoomOnZoom="GSOF_AMO", oKey_1_1="MOCODICE", oKey_1_2="this.w_SMOCOMO2"

  func oSMOCOMO2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMOCOMO2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMOCOMO2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_OFFE','*','MOCODICE',cp_AbsName(this.parent,'oSMOCOMO2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AMO',"Modelli offerta",'',this.parent.oContained
  endproc
  proc oSMOCOMO2_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AMO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_SMOCOMO2
     i_obj.ecpSave()
  endproc

  add object oDESMO2_1_5 as StdField with uid="ZVRJEJJYIK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESMO2", cQueryName = "DESMO2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 69709770,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=252, Top=51, InputMask=replicate('X',35)

  add object oSMOCODPRI_1_6 as StdField with uid="KUEIEJAHIM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SMOCODPRI", cQueryName = "SMOCODPRI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Priorit� da attribuire al nominativo",;
    HelpContextID = 36823544,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=164, Top=86, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", cZoomOnZoom="gsof_agp", oKey_1_1="GPCODICE", oKey_1_2="this.w_SMOCODPRI"

  func oSMOCODPRI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMOCODPRI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMOCODPRI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oSMOCODPRI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'gsof_agp',"Gruppi priorit�",'GSOF_ZGP.GRU_PRIO_VZM',this.parent.oContained
  endproc
  proc oSMOCODPRI_1_6.mZoomOnZoom
    local i_obj
    i_obj=gsof_agp()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_SMOCODPRI
     i_obj.ecpSave()
  endproc

  add object oDESPRIO_1_7 as StdField with uid="XTKEVJMOAS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESPRIO", cQueryName = "DESPRIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione priorit�",;
    HelpContextID = 217362378,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=242, Top=86, InputMask=replicate('X',35)

  add object oSMOCODGRU_1_8 as StdField with uid="UNPJLMPEEC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SMOCODGRU", cQueryName = "SMOCODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo di appartenenza",;
    HelpContextID = 231612104,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=164, Top=110, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", cZoomOnZoom="GSAR_AGN", oKey_1_1="GNCODICE", oKey_1_2="this.w_SMOCODGRU"

  func oSMOCODGRU_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMOCODGRU_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMOCODGRU_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oSMOCODGRU_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGN',"Gruppi nominativi",'',this.parent.oContained
  endproc
  proc oSMOCODGRU_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GNCODICE=this.parent.oContained.w_SMOCODGRU
     i_obj.ecpSave()
  endproc

  add object oDESCGN_1_9 as StdField with uid="OXGVGSHZVB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCGN", cQueryName = "DESCGN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo nominativo",;
    HelpContextID = 145862602,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=242, Top=110, InputMask=replicate('X',35)

  add object oSMOCODORI_1_10 as StdField with uid="PNBSTVXJHA",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SMOCODORI", cQueryName = "SMOCODORI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine reperimento nominativo",;
    HelpContextID = 36823544,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=164, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", cZoomOnZoom="GSAR_AON", oKey_1_1="ONCODICE", oKey_1_2="this.w_SMOCODORI"

  func oSMOCODORI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMOCODORI_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMOCODORI_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oSMOCODORI_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AON',"Codici origine nominativi",'',this.parent.oContained
  endproc
  proc oSMOCODORI_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AON()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ONCODICE=this.parent.oContained.w_SMOCODORI
     i_obj.ecpSave()
  endproc

  add object oDESORI_1_11 as StdField with uid="FBXJICCBRH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione origine",;
    HelpContextID = 217427914,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=242, Top=134, InputMask=replicate('X',35)

  add object oSMOCODOP_1_12 as StdField with uid="WUQFLGQQWQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SMOCODOP", cQueryName = "SMOCODOP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 36824714,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=164, Top=158, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_SMOCODOP"

  func oSMOCODOP_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMOCODOP_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMOCODOP_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oSMOCODOP_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESOPE_1_13 as StdField with uid="UODXIIMSHN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESOPE", cQueryName = "DESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice operatore",;
    HelpContextID = 18198474,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=242, Top=158, InputMask=replicate('X',35)

  add object oSMOCODZON_1_14 as StdField with uid="JEIWNKIDFS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SMOCODZON", cQueryName = "SMOCODZON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice zona",;
    HelpContextID = 231611989,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=164, Top=182, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_SMOCODZON"

  func oSMOCODZON_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMOCODZON_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMOCODZON_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oSMOCODZON_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Zone",'',this.parent.oContained
  endproc

  add object oDESZONE_1_15 as StdField with uid="PJLDMIGCKS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESZONE", cQueryName = "DESZONE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione zone",;
    HelpContextID = 132468790,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=242, Top=182, InputMask=replicate('X',35)

  add object oSMOCODLI_1_16 as StdField with uid="LNZXWBFTUB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SMOCODLI", cQueryName = "SMOCODLI",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice lingua",;
    HelpContextID = 231610735,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=164, Top=206, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", oKey_1_1="LUCODICE", oKey_1_2="this.w_SMOCODLI"

  func oSMOCODLI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMOCODLI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMOCODLI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oSMOCODLI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lingue",'',this.parent.oContained
  endproc

  add object oDESLING_1_17 as StdField with uid="KTLQZGYNXB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESLING", cQueryName = "DESLING",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 125259830,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=242, Top=206, InputMask=replicate('X',30)

  add object oobsodat1_1_18 as StdCheck with uid="NUZMWBAOBT",rtseq=18,rtrep=.f.,left=164, top=232, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo obsoleti",;
    HelpContextID = 206336023,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oobsodat1_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_1_18.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_1_18.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc


  add object oObj_1_31 as cp_outputCombo with uid="ZLERXIVPWT",left=164, top=258, width=371,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 13509146


  add object oBtn_1_32 as StdButton with uid="WIEIPKIZCN",left=436, top=299, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 76928758;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_33 as StdButton with uid="GBTFLBEXBQ",left=487, top=299, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 76928758;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_21 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=12, Top=29,;
    Alignment=1, Width=150, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="BRSOYYEROT",Visible=.t., Left=12, Top=53,;
    Alignment=1, Width=150, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="GYZPKGJJXH",Visible=.t., Left=12, Top=112,;
    Alignment=1, Width=150, Height=15,;
    Caption="Gruppo nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="EIVPTJURQF",Visible=.t., Left=12, Top=88,;
    Alignment=1, Width=150, Height=15,;
    Caption="Gruppo priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="JINCGGAKVH",Visible=.t., Left=12, Top=135,;
    Alignment=1, Width=150, Height=15,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="GGMQEJEICX",Visible=.t., Left=12, Top=183,;
    Alignment=1, Width=150, Height=15,;
    Caption="Codice zona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="PXOJZPVSOB",Visible=.t., Left=12, Top=206,;
    Alignment=1, Width=150, Height=15,;
    Caption="Codice lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="FSDBXSLPSK",Visible=.t., Left=12, Top=160,;
    Alignment=1, Width=150, Height=15,;
    Caption="Codice operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="KHWVLZCVUG",Visible=.t., Left=12, Top=260,;
    Alignment=1, Width=150, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="GOVMDWKHTK",Visible=.t., Left=12, Top=4,;
    Alignment=1, Width=150, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_28 as StdBox with uid="XTOSGASICN",left=11, top=79, width=525,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_smo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
