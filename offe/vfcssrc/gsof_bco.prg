* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bco                                                        *
*              Check offerte                                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bco",oParentObject)
return(i_retval)

define class tgsof_bco as StdBatch
  * --- Local variables
  w_CODRUO = space(5)
  w_FLINOL = space(1)
  w_CFUNC = space(10)
  w_MESS = space(10)
  w_RIGA = space(10)
  w_TRIG = 0
  w_TMP_CHK = space(10)
  w_ODCODART = space(20)
  w_ARDTOBSO = ctod("  /  /  ")
  w_OFDATDOC = ctod("  /  /  ")
  * --- WorkFile variables
  NOM_CONT_idx=0
  RUO_CONT_idx=0
  OFF_NOMI_idx=0
  OFF_ERTE_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Check alla Conferma Offerta (da GSOF_AOF)
    * --- Controlli del Form
    this.w_CFUNC = this.oParentObject.cFunction
    this.oParentObject.w_BRES = .T.
    if this.oParentObject.w_CHECKDETAIL
      this.w_TRIG = 0
      SELECT (this.oParentObject.GSOF_MDO.cTrsName)
      Count for Not Empty( t_ODCODICE ) And Not Deleted() TO this.w_TRIG
      if this.w_TRIG=0
        ah_errormsg("Impossibile confermare, registrazione senza righe di dettaglio")
        this.oParentObject.w_BRES = .F.
        i_retcode = 'stop'
        return
      endif
    endif
    if this.w_CFUNC="Edit"
      if this.oParentObject.w_OLDSTAT = "V"
        ah_errormsg("Offerta in stato versione chiusa non modificabile")
        this.oParentObject.w_BRES = .F.
        i_retcode = 'stop'
        return
      endif
      if this.oParentObject.w_OLDSTAT = "A" and this.oParentObject.w_MODOFF = "S"
        ah_errormsg( "Offerta in stato inviata bloccata non modificabile")
        this.oParentObject.w_BRES = .F.
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_BRES AND this.oParentObject.w_TIPNOM = "G"
      ah_errormsg("Nominativo di tipo congelato")
      this.oParentObject.w_BRES = .F.
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_BRES AND this.w_CFUNC="Load" 
      * --- Select from OFF_ERTE
      i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select OFSERIAL  from "+i_cTable+" OFF_ERTE ";
            +" where OFCODESE="+cp_ToStrODBC(this.oParentObject.w_OFCODESE)+" AND OFNUMDOC="+cp_ToStrODBC(this.oParentObject.w_OFNUMDOC)+" AND OFSERDOC="+cp_ToStrODBC(this.oParentObject.w_OFSERDOC)+" AND OFSTATUS<>'V' AND OFSERIAL<>"+cp_ToStrODBC(this.oParentObject.w_OFSERIAL)+"";
             ,"_Curs_OFF_ERTE")
      else
        select OFSERIAL from (i_cTable);
         where OFCODESE=this.oParentObject.w_OFCODESE AND OFNUMDOC=this.oParentObject.w_OFNUMDOC AND OFSERDOC=this.oParentObject.w_OFSERDOC AND OFSTATUS<>"V" AND OFSERIAL<>this.oParentObject.w_OFSERIAL;
          into cursor _Curs_OFF_ERTE
      endif
      if used('_Curs_OFF_ERTE')
        select _Curs_OFF_ERTE
        locate for 1=1
        do while not(eof())
        this.oParentObject.w_BRES = .F.
          select _Curs_OFF_ERTE
          continue
        enddo
        use
      endif
      if this.oParentObject.w_BRES=.F. 
        ah_errormsg("Per l'esercizio in corso, � presente un'altra offerta con lo stesso numero e serie documento")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_BRES
      this.w_FLINOL = "S"
      * --- Select from NOM_CONT
      i_nConn=i_TableProp[this.NOM_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" NOM_CONT ";
            +" where NCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM)+" and NCCODCON = "+cp_ToStrODBC(this.oParentObject.w_OFCODCON)+"";
             ,"_Curs_NOM_CONT")
      else
        select * from (i_cTable);
         where NCCODICE = this.oParentObject.w_OFCODNOM and NCCODCON = this.oParentObject.w_OFCODCON;
          into cursor _Curs_NOM_CONT
      endif
      if used('_Curs_NOM_CONT')
        select _Curs_NOM_CONT
        locate for 1=1
        do while not(eof())
        this.w_CODRUO = NVL(_Curs_NOM_CONT.nccodruo, SPACE(5))
        if NOT EMPTY(this.w_CODRUO)
          * --- Read from RUO_CONT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.RUO_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RUO_CONT_idx,2],.t.,this.RUO_CONT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "RCFLINOL"+;
              " from "+i_cTable+" RUO_CONT where ";
                  +"RCCODICE = "+cp_ToStrODBC(this.w_CODRUO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              RCFLINOL;
              from (i_cTable) where;
                  RCCODICE = this.w_CODRUO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLINOL = NVL(cp_ToDate(_read_.RCFLINOL),cp_NullValue(_read_.RCFLINOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
          select _Curs_NOM_CONT
          continue
        enddo
        use
      endif
      if this.w_FLINOL <> "S"
        ah_errormsg("Contatto non abilitato alla ricezione dell'offerta")
        this.oParentObject.w_BRES = .F.
        this.oParentObject.w_OFCODCON = space (5)
        this.oParentObject.w_PERSON = space (35)
      endif
    endif
    if this.oParentObject.w_BRES AND this.oParentObject.w_OFSTATUS="C"
      SELECT (this.oParentObject.GSOF_MDO.cTrsName)
      GO TOP
      LOCATE FOR NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODCODICE) AND NOT DELETED() AND t_ARTNOC="S"
      if FOUND()
        this.w_RIGA = ALLTRIM(STR(t_CPROWORD))
        ah_errormsg("Impossibile confermare l'offerta%0Sono presenti righe di dettaglio con articoli da definire%0Procedere all'imputazione di un articolo/servizio (n.riga %1)",48,"",this.w_RIGA)
        this.oParentObject.w_BRES = .F.
      endif
    endif
    if this.oParentObject.w_BRES
      * --- Controlla se ci sono articoli obsoleti
      this.w_OFDATDOC = THIS.OPARENTOBJECT.w_OFDATDOC
      this.w_TMP_CHK = SYS( 2015 )
      this.oParentObject.GSOF_MDO.Exec_Select( this.w_TMP_CHK, "t_CPROWORD, t_ODCODART","NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODCODART) AND NOT DELETED()" )
      SELECT ( this.w_TMP_CHK )
      GO TOP
      do while NOT EOF()
        * --- Scorre le righe cercando un articolo obsoleto
        this.w_ODCODART = t_ODCODART
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARDTOBSO"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ODCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARDTOBSO;
            from (i_cTable) where;
                ARCODART = this.w_ODCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ARDTOBSO = NVL( this.w_ARDTOBSO , CTOD( "  -  -  " ) )
        if NOT EMPTY( this.w_ARDTOBSO ) AND this.w_ARDTOBSO <= this.w_OFDATDOC
          this.w_RIGA = ALLTRIM(STR(t_CPROWORD))
          ah_ErrorMsg("Impossibile confermare l'offerta:%0Sono presenti righe di dettaglio con articoli obsoleti%0Controllare l'articolo %1 alla riga %2",,"", ALLTRIM( this.w_ODCODART) , this.w_RIGA)
          this.oParentObject.w_BRES = .F.
          exit
        endif
        SKIP
      enddo
      if USED( this.w_TMP_CHK )
        SELECT ( this.w_TMP_CHK )
        USE
      endif
    endif
    if this.oParentObject.w_BRES AND this.oParentObject.w_OFSTATUS $ "IA" AND NOT EMPTY(this.oParentObject.w_OFCODNOM) AND this.oParentObject.w_TIPNOM<>"C" and !Isalt()
      * --- Se Nominativo e' da Valutare setta Prospect
      * --- Write into OFF_NOMI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC("P"),'OFF_NOMI','NOTIPNOM');
            +i_ccchkf ;
        +" where ";
            +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
               )
      else
        update (i_cTable) set;
            NOTIPNOM = "P";
            &i_ccchkf. ;
         where;
            NOCODICE = this.oParentObject.w_OFCODNOM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='NOM_CONT'
    this.cWorkTables[2]='RUO_CONT'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='OFF_ERTE'
    this.cWorkTables[5]='ART_ICOL'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_OFF_ERTE')
      use in _Curs_OFF_ERTE
    endif
    if used('_Curs_NOM_CONT')
      use in _Curs_NOM_CONT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
