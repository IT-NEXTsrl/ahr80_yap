* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bie                                                        *
*              Carica/salva modelli offerte                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-18                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PATH,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bie",oParentObject,m.w_PATH,m.pTipo)
return(i_retval)

define class tgsof_bie as StdBatch
  * --- Local variables
  w_PATH = space(254)
  pTipo = space(20)
  w_StrLetta = space(0)
  w_SezAtt = space(20)
  w_CAUDOC = space(5)
  w_CODLIS = space(5)
  w_CODVAL = space(3)
  * --- WorkFile variables
  TIT_SEZI_idx=0
  SOT_SEZI_idx=0
  SEZ_OFFE_idx=0
  MOD_OFFE_idx=0
  MOD_SEZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.pTipo="CARICA"
      * --- Variabili utilizzate in fase di import delle offerte.
      *     w_StrLetta Serve per immagazzinare la linea letta dal file di testo
      *                          � di tipo memo perch� la linea � pi� lunga di 254 caratteri
      *     w_SezAtt    Serve per memorizare in quale sezione ci si trova del file
      * --- Queste variabii vengono utilizzate come caller dalla maschera 
      *     Servono a valorizzare dati ne modello offerta che non possono
      *     essere esportati/importati in quanto potrebbero non essere presenti
      *     sul database di destinazione
      do GSOF_KIE with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if EMPTY(this.w_CAUDOC) OR EMPTY(this.w_CODVAL)
        AH_ErrorMsg("Dati non impostati: impossibile proseguire",16)
        i_retcode = 'stop'
        return
      else
        this.w_SezAtt = "NESSUNA"
        L_File=FOPEN(this.w_PATH,0)
        do while NOT FEOF(L_File)
          * --- Leggo la linea del file (il secondo parametro � impostato al massimo
          *     ritornabile dalla funzione FGETS altrimenti ritorna comunque 254 caratteri
          *     generando errori di import. In caso la linea sia pi� corta viene troncata
          *     quando viene riscontrata la presenta del CarriageReturn
          this.w_StrLetta = FGETS(L_File,8192)
          do case
            case this.w_StrLetta=="[GRUPPI]"
              this.w_SezAtt = LEFT(ALLTRIM(this.w_StrLetta),20)
            case this.w_StrLetta=="[SOTTOGRUPPI]"
              this.w_SezAtt = LEFT(ALLTRIM(this.w_StrLetta),20)
            case this.w_StrLetta=="[SEZIONI]"
              this.w_SezAtt = LEFT(ALLTRIM(this.w_StrLetta),20)
            case this.w_StrLetta=="[MODELLI]"
              this.w_SezAtt = LEFT(ALLTRIM(this.w_StrLetta),20)
            case this.w_StrLetta=="[SEZIONIMODELLI]"
              this.w_SezAtt = LEFT(ALLTRIM(this.w_StrLetta),20)
            otherwise
              do case
                case this.w_SezAtt=="[GRUPPI]"
                  * --- Try
                  local bErr_03379440
                  bErr_03379440=bTrsErr
                  this.Try_03379440()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                    * --- Se gi� presente accetto l'errore e provo a fare la Write
                    * --- Write into TIT_SEZI
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.TIT_SEZI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TIT_SEZI_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIT_SEZI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"TSDESCRI ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,7,35)),'TIT_SEZI','TSDESCRI');
                          +i_ccchkf ;
                      +" where ";
                          +"TSCODICE = "+cp_ToStrODBC(LEFT(this.w_StrLetta,5));
                             )
                    else
                      update (i_cTable) set;
                          TSDESCRI = SUBSTR(this.w_StrLetta,7,35);
                          &i_ccchkf. ;
                       where;
                          TSCODICE = LEFT(this.w_StrLetta,5);

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                  bTrsErr=bTrsErr or bErr_03379440
                  * --- End
                case this.w_SezAtt=="[SOTTOGRUPPI]"
                  * --- Try
                  local bErr_0329D2C8
                  bErr_0329D2C8=bTrsErr
                  this.Try_0329D2C8()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                    * --- Se gi� presente accetto l'errore e provo a fare la Write
                    * --- Write into SOT_SEZI
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SOT_SEZI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SOT_SEZI_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SOT_SEZI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SGDESCRI ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,13,35)),'SOT_SEZI','SGDESCRI');
                          +i_ccchkf ;
                      +" where ";
                          +"SGCODGRU = "+cp_ToStrODBC(LEFT(this.w_StrLetta,5));
                          +" and SGCODSOT = "+cp_ToStrODBC(SUBSTR(this.w_StrLetta,7,5));
                             )
                    else
                      update (i_cTable) set;
                          SGDESCRI = SUBSTR(this.w_StrLetta,13,35);
                          &i_ccchkf. ;
                       where;
                          SGCODGRU = LEFT(this.w_StrLetta,5);
                          and SGCODSOT = SUBSTR(this.w_StrLetta,7,5);

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                  bTrsErr=bTrsErr or bErr_0329D2C8
                  * --- End
                case this.w_SezAtt=="[SEZIONI]"
                  * --- Try
                  local bErr_02438BA0
                  bErr_02438BA0=bTrsErr
                  this.Try_02438BA0()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                    * --- Se gi� presente accetto l'errore e provo a fare la Write
                    * --- Write into SEZ_OFFE
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SEZ_OFFE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SEZ_OFFE_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SEZ_OFFE_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SODESCRI ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,12,35)),'SEZ_OFFE','SODESCRI');
                      +",SOTIPSEZ ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,48,1)),'SEZ_OFFE','SOTIPSEZ');
                      +",SOFILGRU ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,50,5)),'SEZ_OFFE','SOFILGRU');
                      +",SOFILSOT ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,56,5)),'SEZ_OFFE','SOFILSOT');
                      +",SOSEGNWP ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,62,254)),'SEZ_OFFE','SOSEGNWP');
                      +",SOPRESEG ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,317,30)),'SEZ_OFFE','SOPRESEG');
                      +",SOMODEWP ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,348,254)),'SEZ_OFFE','SOMODEWP');
                          +i_ccchkf ;
                      +" where ";
                          +"SOCODICE = "+cp_ToStrODBC(LEFT(this.w_StrLetta,10));
                             )
                    else
                      update (i_cTable) set;
                          SODESCRI = SUBSTR(this.w_StrLetta,12,35);
                          ,SOTIPSEZ = SUBSTR(this.w_StrLetta,48,1);
                          ,SOFILGRU = SUBSTR(this.w_StrLetta,50,5);
                          ,SOFILSOT = SUBSTR(this.w_StrLetta,56,5);
                          ,SOSEGNWP = SUBSTR(this.w_StrLetta,62,254);
                          ,SOPRESEG = SUBSTR(this.w_StrLetta,317,30);
                          ,SOMODEWP = SUBSTR(this.w_StrLetta,348,254);
                          &i_ccchkf. ;
                       where;
                          SOCODICE = LEFT(this.w_StrLetta,10);

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                  bTrsErr=bTrsErr or bErr_02438BA0
                  * --- End
                case this.w_SezAtt=="[MODELLI]"
                  * --- Try
                  local bErr_032F3080
                  bErr_032F3080=bTrsErr
                  this.Try_032F3080()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                    * --- Se gi� presente accetto l'errore e provo a fare la Write
                    * --- Write into MOD_OFFE
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOD_OFFE_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"MODESCRI ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,7,35)),'MOD_OFFE','MODESCRI');
                      +",MOSERDOC ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,43,2)),'MOD_OFFE','MOSERDOC');
                      +",MONUMSCO ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,46,1))),'MOD_OFFE','MONUMSCO');
                      +",MOINISCA ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,48,2)),'MOD_OFFE','MOINISCA');
                      +",MOGIOSCA ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,51,3))),'MOD_OFFE','MOGIOSCA');
                      +",MOGIOFIS ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,55,2))),'MOD_OFFE','MOGIOFIS');
                      +",MOCODCAU ="+cp_NullLink(cp_ToStrODBC(this.w_CAUDOC),'MOD_OFFE','MOCODCAU');
                      +",MOFLPROV ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,58,1)),'MOD_OFFE','MOFLPROV');
                      +",MOCODLIS ="+cp_NullLink(cp_ToStrODBC(this.w_CODLIS),'MOD_OFFE','MOCODLIS');
                      +",MODATVAL ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate(SUBSTR(this.w_StrLetta,60,8))),'MOD_OFFE','MODATVAL');
                      +",MODATOBS ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate(SUBSTR(this.w_StrLetta,69,8))),'MOD_OFFE','MODATOBS');
                      +",MOSCOCL1 ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,78,8))),'MOD_OFFE','MOSCOCL1');
                      +",MOSCOCL2 ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,87,8))),'MOD_OFFE','MOSCOCL2');
                      +",MOSPEINC ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,96,22))),'MOD_OFFE','MOSPEINC');
                      +",MOSPEIMB ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,119,22))),'MOD_OFFE','MOSPEIMB');
                      +",MOSPETRA ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,142,22))),'MOD_OFFE','MOSPETRA');
                      +",MOTIPOWP ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,165,1)),'MOD_OFFE','MOTIPOWP');
                      +",MOPATMOD ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,167,254)),'MOD_OFFE','MOPATMOD');
                      +",MONUMREP ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,422,3))),'MOD_OFFE','MONUMREP');
                      +",MONUMPER ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,426,1)),'MOD_OFFE','MONUMPER');
                      +",MONOMDOC ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,428,254)),'MOD_OFFE','MONOMDOC');
                      +",MOTIPFOR ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,683,1)),'MOD_OFFE','MOTIPFOR');
                      +",MOSEZVAR ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,685,1)),'MOD_OFFE','MOSEZVAR');
                      +",MOCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'MOD_OFFE','MOCODVAL');
                      +",MOPRZVAC ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,687,1)),'MOD_OFFE','MOPRZVAC');
                      +",MOPATARC ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,688,254)),'MOD_OFFE','MOPATARC');
                          +i_ccchkf ;
                      +" where ";
                          +"MOCODICE = "+cp_ToStrODBC(LEFT(this.w_StrLetta,5));
                             )
                    else
                      update (i_cTable) set;
                          MODESCRI = SUBSTR(this.w_StrLetta,7,35);
                          ,MOSERDOC = SUBSTR(this.w_StrLetta,43,2);
                          ,MONUMSCO = VAL(SUBSTR(this.w_StrLetta,46,1));
                          ,MOINISCA = SUBSTR(this.w_StrLetta,48,2);
                          ,MOGIOSCA = VAL(SUBSTR(this.w_StrLetta,51,3));
                          ,MOGIOFIS = VAL(SUBSTR(this.w_StrLetta,55,2));
                          ,MOCODCAU = this.w_CAUDOC;
                          ,MOFLPROV = SUBSTR(this.w_StrLetta,58,1);
                          ,MOCODLIS = this.w_CODLIS;
                          ,MODATVAL = cp_CharToDate(SUBSTR(this.w_StrLetta,60,8));
                          ,MODATOBS = cp_CharToDate(SUBSTR(this.w_StrLetta,69,8));
                          ,MOSCOCL1 = VAL(SUBSTR(this.w_StrLetta,78,8));
                          ,MOSCOCL2 = VAL(SUBSTR(this.w_StrLetta,87,8));
                          ,MOSPEINC = VAL(SUBSTR(this.w_StrLetta,96,22));
                          ,MOSPEIMB = VAL(SUBSTR(this.w_StrLetta,119,22));
                          ,MOSPETRA = VAL(SUBSTR(this.w_StrLetta,142,22));
                          ,MOTIPOWP = SUBSTR(this.w_StrLetta,165,1);
                          ,MOPATMOD = SUBSTR(this.w_StrLetta,167,254);
                          ,MONUMREP = VAL(SUBSTR(this.w_StrLetta,422,3));
                          ,MONUMPER = SUBSTR(this.w_StrLetta,426,1);
                          ,MONOMDOC = SUBSTR(this.w_StrLetta,428,254);
                          ,MOTIPFOR = SUBSTR(this.w_StrLetta,683,1);
                          ,MOSEZVAR = SUBSTR(this.w_StrLetta,685,1);
                          ,MOCODVAL = this.w_CODVAL;
                          ,MOPRZVAC = SUBSTR(this.w_StrLetta,687,1);
                          ,MOPATARC = SUBSTR(this.w_StrLetta,688,254);
                          &i_ccchkf. ;
                       where;
                          MOCODICE = LEFT(this.w_StrLetta,5);

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                  bTrsErr=bTrsErr or bErr_032F3080
                  * --- End
                case this.w_SezAtt=="[SEZIONIMODELLI]"
                  * --- Try
                  local bErr_033BB7F8
                  bErr_033BB7F8=bTrsErr
                  this.Try_033BB7F8()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                    * --- Se gi� presente accetto l'errore e provo a fare la Write
                    * --- Write into MOD_SEZI
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.MOD_SEZI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MOD_SEZI_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOD_SEZI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,11,4))),'MOD_SEZI','CPROWORD');
                      +",MSCODSEZ ="+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,16,26)),'MOD_SEZI','MSCODSEZ');
                          +i_ccchkf ;
                      +" where ";
                          +"MSCODICE = "+cp_ToStrODBC(LEFT(this.w_StrLetta,5));
                          +" and CPROWNUM = "+cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,7,3)));
                             )
                    else
                      update (i_cTable) set;
                          CPROWORD = VAL(SUBSTR(this.w_StrLetta,11,4));
                          ,MSCODSEZ = SUBSTR(this.w_StrLetta,16,26);
                          &i_ccchkf. ;
                       where;
                          MSCODICE = LEFT(this.w_StrLetta,5);
                          and CPROWNUM = VAL(SUBSTR(this.w_StrLetta,7,3));

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                  bTrsErr=bTrsErr or bErr_033BB7F8
                  * --- End
              endcase
          endcase
        enddo
        FCLOSE(L_File)
      endif
    else
      L_File=FCREATE(this.w_PATH)
      if L_File<0
        AH_ErrorMsg("Impossibile aprire file (%1) in scrittura",16,"",this.w_PATH)
      else
        L_ErrScri=FPUTS(L_File,"[GRUPPI]")
        if L_ErrScri<=0
          AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
          FCLOSE(L_File)
          i_retcode = 'stop'
          return
        else
          * --- Select from TIT_SEZI
          i_nConn=i_TableProp[this.TIT_SEZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIT_SEZI_idx,2],.t.,this.TIT_SEZI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select TSCODICE,TSDESCRI  from "+i_cTable+" TIT_SEZI ";
                 ,"_Curs_TIT_SEZI")
          else
            select TSCODICE,TSDESCRI from (i_cTable);
              into cursor _Curs_TIT_SEZI
          endif
          if used('_Curs_TIT_SEZI')
            select _Curs_TIT_SEZI
            locate for 1=1
            do while not(eof())
            L_ErrScri=FPUTS(L_File,LEFT(ALLTRIM(NVL(_Curs_TIT_SEZI.TSCODICE," "))+SPACE(5),5)+" "+LEFT(ALLTRIM(NVL(_Curs_TIT_SEZI.TSDESCRI," "))+SPACE(35),35))
            if L_ErrScri<=0
              AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
              FCLOSE(L_File)
              i_retcode = 'stop'
              return
            endif
              select _Curs_TIT_SEZI
              continue
            enddo
            use
          endif
          * --- In assenza di errori continuo e scrivo informazioni sottogruppi
          L_ErrScri=FPUTS(L_File,"[SOTTOGRUPPI]")
          if L_ErrScri<=0
            AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
            FCLOSE(L_File)
            i_retcode = 'stop'
            return
          else
            * --- Select from SOT_SEZI
            i_nConn=i_TableProp[this.SOT_SEZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SOT_SEZI_idx,2],.t.,this.SOT_SEZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI  from "+i_cTable+" SOT_SEZI ";
                   ,"_Curs_SOT_SEZI")
            else
              select SGCODGRU,SGCODSOT,SGDESCRI from (i_cTable);
                into cursor _Curs_SOT_SEZI
            endif
            if used('_Curs_SOT_SEZI')
              select _Curs_SOT_SEZI
              locate for 1=1
              do while not(eof())
              L_ErrScri=FPUTS(L_File,LEFT(ALLTRIM(NVL(_Curs_SOT_SEZI.SGCODGRU," "))+SPACE(5),5)+" "+LEFT(ALLTRIM(NVL(_Curs_SOT_SEZI.SGCODSOT," "))+SPACE(5),5)+" "+LEFT(ALLTRIM(NVL(_Curs_SOT_SEZI.SGDESCRI," "))+SPACE(35),35))
              if L_ErrScri<=0
                AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                FCLOSE(L_File)
                i_retcode = 'stop'
                return
              endif
                select _Curs_SOT_SEZI
                continue
              enddo
              use
            endif
            * --- In assenza di errori continuo e scrivo informazioni delle sezioni
            L_ErrScri=FPUTS(L_File,"[SEZIONI]")
            if L_ErrScri<=0
              AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
              FCLOSE(L_File)
              i_retcode = 'stop'
              return
            else
              * --- Select from SEZ_OFFE
              i_nConn=i_TableProp[this.SEZ_OFFE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SEZ_OFFE_idx,2],.t.,this.SEZ_OFFE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT,SOSEGNWP,SOPRESEG,SOMODEWP  from "+i_cTable+" SEZ_OFFE ";
                     ,"_Curs_SEZ_OFFE")
              else
                select SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT,SOSEGNWP,SOPRESEG,SOMODEWP from (i_cTable);
                  into cursor _Curs_SEZ_OFFE
              endif
              if used('_Curs_SEZ_OFFE')
                select _Curs_SEZ_OFFE
                locate for 1=1
                do while not(eof())
                L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_SEZ_OFFE.SOCODICE," "))+SPACE(10),10)+" ")
                if L_ErrScri<=0
                  AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                  FCLOSE(L_File)
                  i_retcode = 'stop'
                  return
                endif
                L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_SEZ_OFFE.SODESCRI," "))+SPACE(35),35)+" ")
                if L_ErrScri<=0
                  AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                  FCLOSE(L_File)
                  i_retcode = 'stop'
                  return
                endif
                L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_SEZ_OFFE.SOTIPSEZ," "))+SPACE(1),1)+" ")
                if L_ErrScri<=0
                  AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                  FCLOSE(L_File)
                  i_retcode = 'stop'
                  return
                endif
                L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_SEZ_OFFE.SOFILGRU," "))+SPACE(5),5)+" ")
                if L_ErrScri<=0
                  AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                  FCLOSE(L_File)
                  i_retcode = 'stop'
                  return
                endif
                L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_SEZ_OFFE.SOFILSOT," "))+SPACE(5),5)+" ")
                if L_ErrScri<=0
                  AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                  FCLOSE(L_File)
                  i_retcode = 'stop'
                  return
                endif
                L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_SEZ_OFFE.SOSEGNWP," "))+SPACE(254),254)+" ")
                if L_ErrScri<=0
                  AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                  FCLOSE(L_File)
                  i_retcode = 'stop'
                  return
                endif
                L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_SEZ_OFFE.SOPRESEG," "))+SPACE(30),30)+" ")
                if L_ErrScri<=0
                  AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                  FCLOSE(L_File)
                  i_retcode = 'stop'
                  return
                endif
                L_ErrScri=FPUTS(L_File,LEFT(ALLTRIM(NVL(_Curs_SEZ_OFFE.SOMODEWP," "))+SPACE(254),254))
                if L_ErrScri<=0
                  AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                  FCLOSE(L_File)
                  i_retcode = 'stop'
                  return
                endif
                  select _Curs_SEZ_OFFE
                  continue
                enddo
                use
              endif
              * --- In assenza di errori continuo e scrivo informazioni modelli
              L_ErrScri=FPUTS(L_File,"[MODELLI]")
              if L_ErrScri<=0
                AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                FCLOSE(L_File)
                i_retcode = 'stop'
                return
              else
                * --- Select from MOD_OFFE
                i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOD_OFFE ";
                       ,"_Curs_MOD_OFFE")
                else
                  select * from (i_cTable);
                    into cursor _Curs_MOD_OFFE
                endif
                if used('_Curs_MOD_OFFE')
                  select _Curs_MOD_OFFE
                  locate for 1=1
                  do while not(eof())
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOCODICE," "))+SPACE(5),5)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MODESCRI," "))+SPACE(35),35)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOSERDOC," "))+SPACE(2),2)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_OFFE.MONUMSCO))+SPACE(1),1)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOINISCA," "))+SPACE(2),2)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_OFFE.MOGIOSCA))+SPACE(3),3)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_OFFE.MOGIOFIS))+SPACE(2),2)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOFLPROV," "))+SPACE(1),1)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(DTOC(_Curs_MOD_OFFE.MODATVAL)," "))+SPACE(8),8)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(DTOC(_Curs_MOD_OFFE.MODATOBS)," "))+SPACE(8),8)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_OFFE.MOSCOCL1,6,2))+SPACE(8),8)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_OFFE.MOSCOCL2,6,2))+SPACE(8),8)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_OFFE.MOSPEINC,18,4))+SPACE(22),22)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_OFFE.MOSPEIMB,18,4))+SPACE(22),22)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_OFFE.MOSPETRA,18,4))+SPACE(22),22)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOTIPOWP," "))+SPACE(1),1)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOPATMOD," "))+SPACE(254),254)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(IIF(NVL(_Curs_MOD_OFFE.MONUMREP,0)<>0,STR(NVL(_Curs_MOD_OFFE.MONUMREP,0),3,0)," "))+SPACE(3),3)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MONUMPER," "))+SPACE(1),1)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MONOMDOC," "))+SPACE(254),254)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOTIPFOR," "))+SPACE(1),1)+ " ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOSEZVAR," "))+SPACE(1),1)+" ")
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOPRZVAC," "))+SPACE(1),1))
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                  L_ErrScri=FPUTS(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_OFFE.MOPATARC," "))+SPACE(254),254))
                  if L_ErrScri<=0
                    AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                    FCLOSE(L_File)
                    i_retcode = 'stop'
                    return
                  endif
                    select _Curs_MOD_OFFE
                    continue
                  enddo
                  use
                endif
                * --- In assenza di errori continuo e scrivo informazioni delle sezioni dei modelli
                L_ErrScri=FPUTS(L_File,"[SEZIONIMODELLI]")
                if L_ErrScri<=0
                  AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                  FCLOSE(L_File)
                  i_retcode = 'stop'
                  return
                else
                  * --- Select from MOD_SEZI
                  i_nConn=i_TableProp[this.MOD_SEZI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.MOD_SEZI_idx,2],.t.,this.MOD_SEZI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select MSCODICE,CPROWNUM,CPROWORD,MSCODSEZ  from "+i_cTable+" MOD_SEZI ";
                         ,"_Curs_MOD_SEZI")
                  else
                    select MSCODICE,CPROWNUM,CPROWORD,MSCODSEZ from (i_cTable);
                      into cursor _Curs_MOD_SEZI
                  endif
                  if used('_Curs_MOD_SEZI')
                    select _Curs_MOD_SEZI
                    locate for 1=1
                    do while not(eof())
                    L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_SEZI.MSCODICE," "))+SPACE(5),5)+" ")
                    if L_ErrScri<=0
                      AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                      FCLOSE(L_File)
                      i_retcode = 'stop'
                      return
                    endif
                    L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_SEZI.CPROWNUM,3,0))+SPACE(3),3)+" ")
                    if L_ErrScri<=0
                      AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                      FCLOSE(L_File)
                      i_retcode = 'stop'
                      return
                    endif
                    L_ErrScri=FWRITE(L_File,LEFT(ALLTRIM(STR(_Curs_MOD_SEZI.CPROWORD,4,0))+SPACE(4),4)+" ")
                    if L_ErrScri<=0
                      AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                      FCLOSE(L_File)
                      i_retcode = 'stop'
                      return
                    endif
                    L_ErrScri=FPUTS(L_File,LEFT(ALLTRIM(NVL(_Curs_MOD_SEZI.MSCODSEZ," "))+SPACE(10),10))
                    if L_ErrScri<=0
                      AH_ErrorMsg("Impossibile scrivere sul file (%1)",16,"",this.w_PATH)
                      FCLOSE(L_File)
                      i_retcode = 'stop'
                      return
                    endif
                      select _Curs_MOD_SEZI
                      continue
                    enddo
                    use
                  endif
                  FCLOSE(L_File)
                endif
              endif
            endif
          endif
        endif
      endif
    endif
  endproc
  proc Try_03379440()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TIT_SEZI
    i_nConn=i_TableProp[this.TIT_SEZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIT_SEZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIT_SEZI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TSCODICE"+",TSDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(LEFT(this.w_StrLetta,5)),'TIT_SEZI','TSCODICE');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,7,35)),'TIT_SEZI','TSDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TSCODICE',LEFT(this.w_StrLetta,5),'TSDESCRI',SUBSTR(this.w_StrLetta,7,35))
      insert into (i_cTable) (TSCODICE,TSDESCRI &i_ccchkf. );
         values (;
           LEFT(this.w_StrLetta,5);
           ,SUBSTR(this.w_StrLetta,7,35);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0329D2C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SOT_SEZI
    i_nConn=i_TableProp[this.SOT_SEZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SOT_SEZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SOT_SEZI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SGCODGRU"+",SGCODSOT"+",SGDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(LEFT(this.w_StrLetta,5)),'SOT_SEZI','SGCODGRU');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,7,5)),'SOT_SEZI','SGCODSOT');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,13,35)),'SOT_SEZI','SGDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SGCODGRU',LEFT(this.w_StrLetta,5),'SGCODSOT',SUBSTR(this.w_StrLetta,7,5),'SGDESCRI',SUBSTR(this.w_StrLetta,13,35))
      insert into (i_cTable) (SGCODGRU,SGCODSOT,SGDESCRI &i_ccchkf. );
         values (;
           LEFT(this.w_StrLetta,5);
           ,SUBSTR(this.w_StrLetta,7,5);
           ,SUBSTR(this.w_StrLetta,13,35);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02438BA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SEZ_OFFE
    i_nConn=i_TableProp[this.SEZ_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SEZ_OFFE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SEZ_OFFE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SOCODICE"+",SODESCRI"+",SOTIPSEZ"+",SOFILGRU"+",SOFILSOT"+",SOSEGNWP"+",SOPRESEG"+",SOMODEWP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(LEFT(this.w_StrLetta,10)),'SEZ_OFFE','SOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,12,35)),'SEZ_OFFE','SODESCRI');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,48,1)),'SEZ_OFFE','SOTIPSEZ');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,50,5)),'SEZ_OFFE','SOFILGRU');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,56,5)),'SEZ_OFFE','SOFILSOT');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,62,254)),'SEZ_OFFE','SOSEGNWP');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,317,30)),'SEZ_OFFE','SOPRESEG');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,348,254)),'SEZ_OFFE','SOMODEWP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SOCODICE',LEFT(this.w_StrLetta,10),'SODESCRI',SUBSTR(this.w_StrLetta,12,35),'SOTIPSEZ',SUBSTR(this.w_StrLetta,48,1),'SOFILGRU',SUBSTR(this.w_StrLetta,50,5),'SOFILSOT',SUBSTR(this.w_StrLetta,56,5),'SOSEGNWP',SUBSTR(this.w_StrLetta,62,254),'SOPRESEG',SUBSTR(this.w_StrLetta,317,30),'SOMODEWP',SUBSTR(this.w_StrLetta,348,254))
      insert into (i_cTable) (SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT,SOSEGNWP,SOPRESEG,SOMODEWP &i_ccchkf. );
         values (;
           LEFT(this.w_StrLetta,10);
           ,SUBSTR(this.w_StrLetta,12,35);
           ,SUBSTR(this.w_StrLetta,48,1);
           ,SUBSTR(this.w_StrLetta,50,5);
           ,SUBSTR(this.w_StrLetta,56,5);
           ,SUBSTR(this.w_StrLetta,62,254);
           ,SUBSTR(this.w_StrLetta,317,30);
           ,SUBSTR(this.w_StrLetta,348,254);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_032F3080()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOD_OFFE
    i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOD_OFFE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MOCODICE"+",MODESCRI"+",MOSERDOC"+",MONUMSCO"+",MOINISCA"+",MOGIOSCA"+",MOGIOFIS"+",MOCODCAU"+",MOFLPROV"+",MOCODLIS"+",MODATVAL"+",MODATOBS"+",MOSCOCL1"+",MOSCOCL2"+",MOSPEINC"+",MOSPEIMB"+",MOSPETRA"+",MOTIPOWP"+",MOPATMOD"+",MONUMREP"+",MONUMPER"+",MONOMDOC"+",MOTIPFOR"+",MOSEZVAR"+",MOCODVAL"+",MOPRZVAC"+",MOPATARC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(LEFT(this.w_StrLetta,5)),'MOD_OFFE','MOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,7,35)),'MOD_OFFE','MODESCRI');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,43,2)),'MOD_OFFE','MOSERDOC');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,46,1))),'MOD_OFFE','MONUMSCO');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,48,2)),'MOD_OFFE','MOINISCA');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,51,3))),'MOD_OFFE','MOGIOSCA');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,55,2))),'MOD_OFFE','MOGIOFIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAUDOC),'MOD_OFFE','MOCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,58,1)),'MOD_OFFE','MOFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODLIS),'MOD_OFFE','MOCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate(SUBSTR(this.w_StrLetta,60,8))),'MOD_OFFE','MODATVAL');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate(SUBSTR(this.w_StrLetta,69,8))),'MOD_OFFE','MODATOBS');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,78,8))),'MOD_OFFE','MOSCOCL1');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,87,8))),'MOD_OFFE','MOSCOCL2');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,96,22))),'MOD_OFFE','MOSPEINC');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,119,22))),'MOD_OFFE','MOSPEIMB');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,142,22))),'MOD_OFFE','MOSPETRA');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,165,1)),'MOD_OFFE','MOTIPOWP');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,167,254)),'MOD_OFFE','MOPATMOD');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,422,3))),'MOD_OFFE','MONUMREP');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,426,1)),'MOD_OFFE','MONUMPER');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,428,254)),'MOD_OFFE','MONOMDOC');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,683,1)),'MOD_OFFE','MOTIPFOR');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,685,1)),'MOD_OFFE','MOSEZVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'MOD_OFFE','MOCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,687,1)),'MOD_OFFE','MOPRZVAC');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,688,254)),'MOD_OFFE','MOPATARC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MOCODICE',LEFT(this.w_StrLetta,5),'MODESCRI',SUBSTR(this.w_StrLetta,7,35),'MOSERDOC',SUBSTR(this.w_StrLetta,43,2),'MONUMSCO',VAL(SUBSTR(this.w_StrLetta,46,1)),'MOINISCA',SUBSTR(this.w_StrLetta,48,2),'MOGIOSCA',VAL(SUBSTR(this.w_StrLetta,51,3)),'MOGIOFIS',VAL(SUBSTR(this.w_StrLetta,55,2)),'MOCODCAU',this.w_CAUDOC,'MOFLPROV',SUBSTR(this.w_StrLetta,58,1),'MOCODLIS',this.w_CODLIS,'MODATVAL',cp_CharToDate(SUBSTR(this.w_StrLetta,60,8)),'MODATOBS',cp_CharToDate(SUBSTR(this.w_StrLetta,69,8)))
      insert into (i_cTable) (MOCODICE,MODESCRI,MOSERDOC,MONUMSCO,MOINISCA,MOGIOSCA,MOGIOFIS,MOCODCAU,MOFLPROV,MOCODLIS,MODATVAL,MODATOBS,MOSCOCL1,MOSCOCL2,MOSPEINC,MOSPEIMB,MOSPETRA,MOTIPOWP,MOPATMOD,MONUMREP,MONUMPER,MONOMDOC,MOTIPFOR,MOSEZVAR,MOCODVAL,MOPRZVAC,MOPATARC &i_ccchkf. );
         values (;
           LEFT(this.w_StrLetta,5);
           ,SUBSTR(this.w_StrLetta,7,35);
           ,SUBSTR(this.w_StrLetta,43,2);
           ,VAL(SUBSTR(this.w_StrLetta,46,1));
           ,SUBSTR(this.w_StrLetta,48,2);
           ,VAL(SUBSTR(this.w_StrLetta,51,3));
           ,VAL(SUBSTR(this.w_StrLetta,55,2));
           ,this.w_CAUDOC;
           ,SUBSTR(this.w_StrLetta,58,1);
           ,this.w_CODLIS;
           ,cp_CharToDate(SUBSTR(this.w_StrLetta,60,8));
           ,cp_CharToDate(SUBSTR(this.w_StrLetta,69,8));
           ,VAL(SUBSTR(this.w_StrLetta,78,8));
           ,VAL(SUBSTR(this.w_StrLetta,87,8));
           ,VAL(SUBSTR(this.w_StrLetta,96,22));
           ,VAL(SUBSTR(this.w_StrLetta,119,22));
           ,VAL(SUBSTR(this.w_StrLetta,142,22));
           ,SUBSTR(this.w_StrLetta,165,1);
           ,SUBSTR(this.w_StrLetta,167,254);
           ,VAL(SUBSTR(this.w_StrLetta,422,3));
           ,SUBSTR(this.w_StrLetta,426,1);
           ,SUBSTR(this.w_StrLetta,428,254);
           ,SUBSTR(this.w_StrLetta,683,1);
           ,SUBSTR(this.w_StrLetta,685,1);
           ,this.w_CODVAL;
           ,SUBSTR(this.w_StrLetta,687,1);
           ,SUBSTR(this.w_StrLetta,688,254);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_033BB7F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOD_SEZI
    i_nConn=i_TableProp[this.MOD_SEZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_SEZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOD_SEZI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MSCODICE"+",CPROWNUM"+",CPROWORD"+",MSCODSEZ"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(LEFT(this.w_StrLetta,5)),'MOD_SEZI','MSCODICE');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,7,3))),'MOD_SEZI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(VAL(SUBSTR(this.w_StrLetta,11,4))),'MOD_SEZI','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(SUBSTR(this.w_StrLetta,16,26)),'MOD_SEZI','MSCODSEZ');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MSCODICE',LEFT(this.w_StrLetta,5),'CPROWNUM',VAL(SUBSTR(this.w_StrLetta,7,3)),'CPROWORD',VAL(SUBSTR(this.w_StrLetta,11,4)),'MSCODSEZ',SUBSTR(this.w_StrLetta,16,26))
      insert into (i_cTable) (MSCODICE,CPROWNUM,CPROWORD,MSCODSEZ &i_ccchkf. );
         values (;
           LEFT(this.w_StrLetta,5);
           ,VAL(SUBSTR(this.w_StrLetta,7,3));
           ,VAL(SUBSTR(this.w_StrLetta,11,4));
           ,SUBSTR(this.w_StrLetta,16,26);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_PATH,pTipo)
    this.w_PATH=w_PATH
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='TIT_SEZI'
    this.cWorkTables[2]='SOT_SEZI'
    this.cWorkTables[3]='SEZ_OFFE'
    this.cWorkTables[4]='MOD_OFFE'
    this.cWorkTables[5]='MOD_SEZI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_TIT_SEZI')
      use in _Curs_TIT_SEZI
    endif
    if used('_Curs_SOT_SEZI')
      use in _Curs_SOT_SEZI
    endif
    if used('_Curs_SEZ_OFFE')
      use in _Curs_SEZ_OFFE
    endif
    if used('_Curs_MOD_OFFE')
      use in _Curs_MOD_OFFE
    endif
    if used('_Curs_MOD_SEZI')
      use in _Curs_MOD_SEZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PATH,pTipo"
endproc
