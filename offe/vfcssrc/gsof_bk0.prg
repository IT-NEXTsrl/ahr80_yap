* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bk0                                                        *
*              Conferma articoli                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-14                                                      *
* Last revis.: 2003-02-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bk0",oParentObject)
return(i_retval)

define class tgsof_bk0 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conferma Articoli da Confermare (da GSOF_MDO)
    * --- Definisce l'Oggetto  (Deriv. MOVIMENTAZIONI)
    if USED("APPART")
      SELECT APPART
      USE
    endif
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    SELECT * FROM (this.oParentObject.cTrsName) ;
    WHERE NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODCODICE) AND t_ODARNOCO=1 ;
    INTO CURSOR APPART
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    if USED("APPART")
      SELECT APPART
      if RECCOUNT()>0
        * --- Puntatore all Oggetto Chiamante
        PRIVATE p_PUNTAT
        p_PUNTAT = this.oParentObject
        this.oParentObject.w_OGG = GSOF_MDC()
        * --- Controllo se ha passato il test di accesso 
        if !(this.oParentObject.w_OGG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Lancia la Conferma Articoli (in Caricamento)
        this.oParentObject.w_OGG.ecpLoad()     
      else
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
