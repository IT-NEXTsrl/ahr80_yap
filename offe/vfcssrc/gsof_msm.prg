* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_msm                                                        *
*              Sezioni modelli                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2011-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsof_msm")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsof_msm")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsof_msm")
  return

* --- Class definition
define class tgsof_msm as StdPCForm
  Width  = 672
  Height = 237
  Top    = 17
  Left   = 23
  cComment = "Sezioni modelli"
  cPrg = "gsof_msm"
  HelpContextID=171300503
  add object cnt as tcgsof_msm
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsof_msm as PCContext
  w_MSCODICE = space(5)
  w_CPROWORD = 0
  w_MSCODSEZ = space(10)
  w_MSDESCRI = space(35)
  w_MSTIPSEZ = space(1)
  w_MSGRUP = space(5)
  w_MSSOTG = space(5)
  w_DESTIPO = space(12)
  proc Save(i_oFrom)
    this.w_MSCODICE = i_oFrom.w_MSCODICE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MSCODSEZ = i_oFrom.w_MSCODSEZ
    this.w_MSDESCRI = i_oFrom.w_MSDESCRI
    this.w_MSTIPSEZ = i_oFrom.w_MSTIPSEZ
    this.w_MSGRUP = i_oFrom.w_MSGRUP
    this.w_MSSOTG = i_oFrom.w_MSSOTG
    this.w_DESTIPO = i_oFrom.w_DESTIPO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MSCODICE = this.w_MSCODICE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MSCODSEZ = this.w_MSCODSEZ
    i_oTo.w_MSDESCRI = this.w_MSDESCRI
    i_oTo.w_MSTIPSEZ = this.w_MSTIPSEZ
    i_oTo.w_MSGRUP = this.w_MSGRUP
    i_oTo.w_MSSOTG = this.w_MSSOTG
    i_oTo.w_DESTIPO = this.w_DESTIPO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsof_msm as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 672
  Height = 237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-28"
  HelpContextID=171300503
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MOD_SEZI_IDX = 0
  SEZ_OFFE_IDX = 0
  cFile = "MOD_SEZI"
  cKeySelect = "MSCODICE"
  cKeyWhere  = "MSCODICE=this.w_MSCODICE"
  cKeyDetail  = "MSCODICE=this.w_MSCODICE"
  cKeyWhereODBC = '"MSCODICE="+cp_ToStrODBC(this.w_MSCODICE)';

  cKeyDetailWhereODBC = '"MSCODICE="+cp_ToStrODBC(this.w_MSCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"MOD_SEZI.MSCODICE="+cp_ToStrODBC(this.w_MSCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MOD_SEZI.CPROWORD '
  cPrg = "gsof_msm"
  cComment = "Sezioni modelli"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MSCODICE = space(5)
  w_CPROWORD = 0
  w_MSCODSEZ = space(10)
  w_MSDESCRI = space(35)
  w_MSTIPSEZ = space(1)
  w_MSGRUP = space(5)
  w_MSSOTG = space(5)
  w_DESTIPO = space(12)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_msmPag1","gsof_msm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='SEZ_OFFE'
    this.cWorkTables[2]='MOD_SEZI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_SEZI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_SEZI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsof_msm'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MOD_SEZI where MSCODICE=KeySet.MSCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MOD_SEZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_SEZI_IDX,2],this.bLoadRecFilter,this.MOD_SEZI_IDX,"gsof_msm")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_SEZI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_SEZI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_SEZI '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MSCODICE',this.w_MSCODICE  )
      select * from (i_cTable) MOD_SEZI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MSCODICE = NVL(MSCODICE,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MOD_SEZI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_MSDESCRI = space(35)
          .w_MSTIPSEZ = space(1)
          .w_MSGRUP = space(5)
          .w_MSSOTG = space(5)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MSCODSEZ = NVL(MSCODSEZ,space(10))
          if link_2_2_joined
            this.w_MSCODSEZ = NVL(SOCODICE202,NVL(this.w_MSCODSEZ,space(10)))
            this.w_MSDESCRI = NVL(SODESCRI202,space(35))
            this.w_MSTIPSEZ = NVL(SOTIPSEZ202,space(1))
            this.w_MSGRUP = NVL(SOFILGRU202,space(5))
            this.w_MSSOTG = NVL(SOFILSOT202,space(5))
          else
          .link_2_2('Load')
          endif
        .w_DESTIPO = ah_msgformat(iif(.w_MSTIPSEZ='D','Descrittiva',iif(.w_MSTIPSEZ='W','File W.P.',iif(.w_MSTIPSEZ='A','Articoli',iif(.w_MSTIPSEZ='G','Gruppi',iif(.w_MSTIPSEZ='S','Sottogruppi',iif(.w_MSTIPSEZ='T','Dati Fissi','')))))))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MSCODICE=space(5)
      .w_CPROWORD=10
      .w_MSCODSEZ=space(10)
      .w_MSDESCRI=space(35)
      .w_MSTIPSEZ=space(1)
      .w_MSGRUP=space(5)
      .w_MSSOTG=space(5)
      .w_DESTIPO=space(12)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_MSCODSEZ))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,7,.f.)
        .w_DESTIPO = ah_msgformat(iif(.w_MSTIPSEZ='D','Descrittiva',iif(.w_MSTIPSEZ='W','File W.P.',iif(.w_MSTIPSEZ='A','Articoli',iif(.w_MSTIPSEZ='G','Gruppi',iif(.w_MSTIPSEZ='S','Sottogruppi',iif(.w_MSTIPSEZ='T','Dati Fissi','')))))))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_SEZI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MOD_SEZI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_SEZI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSCODICE,"MSCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_MSCODSEZ C(10);
      ,t_MSDESCRI C(35);
      ,t_MSGRUP C(5);
      ,t_MSSOTG C(5);
      ,t_DESTIPO C(12);
      ,CPROWNUM N(10);
      ,t_MSTIPSEZ C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsof_msmbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMSCODSEZ_2_2.controlsource=this.cTrsName+'.t_MSCODSEZ'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMSDESCRI_2_3.controlsource=this.cTrsName+'.t_MSDESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMSGRUP_2_5.controlsource=this.cTrsName+'.t_MSGRUP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMSSOTG_2_6.controlsource=this.cTrsName+'.t_MSSOTG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESTIPO_2_7.controlsource=this.cTrsName+'.t_DESTIPO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(61)
    this.AddVLine(160)
    this.AddVLine(425)
    this.AddVLine(523)
    this.AddVLine(582)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_SEZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_SEZI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_SEZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_SEZI_IDX,2])
      *
      * insert into MOD_SEZI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_SEZI')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_SEZI')
        i_cFldBody=" "+;
                  "(MSCODICE,CPROWORD,MSCODSEZ,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MSCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_MSCODSEZ)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_SEZI')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_SEZI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MSCODICE',this.w_MSCODICE)
        INSERT INTO (i_cTable) (;
                   MSCODICE;
                  ,CPROWORD;
                  ,MSCODSEZ;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MSCODICE;
                  ,this.w_CPROWORD;
                  ,this.w_MSCODSEZ;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.MOD_SEZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_SEZI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_MSCODSEZ)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_SEZI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_SEZI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_MSCODSEZ)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MOD_SEZI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_SEZI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MSCODSEZ="+cp_ToStrODBCNull(this.w_MSCODSEZ)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_SEZI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MSCODSEZ=this.w_MSCODSEZ;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_SEZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_SEZI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_MSCODSEZ)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MOD_SEZI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_MSCODSEZ)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_SEZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_SEZI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
          .w_DESTIPO = ah_msgformat(iif(.w_MSTIPSEZ='D','Descrittiva',iif(.w_MSTIPSEZ='W','File W.P.',iif(.w_MSTIPSEZ='A','Articoli',iif(.w_MSTIPSEZ='G','Gruppi',iif(.w_MSTIPSEZ='S','Sottogruppi',iif(.w_MSTIPSEZ='T','Dati Fissi','')))))))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MSTIPSEZ with this.w_MSTIPSEZ
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MSCODSEZ
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_lTable = "SEZ_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2], .t., this.SEZ_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MSCODSEZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ASO',True,'SEZ_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SOCODICE like "+cp_ToStrODBC(trim(this.w_MSCODSEZ)+"%");

          i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SOCODICE',trim(this.w_MSCODSEZ))
          select SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MSCODSEZ)==trim(_Link_.SOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MSCODSEZ) and !this.bDontReportError
            deferred_cp_zoom('SEZ_OFFE','*','SOCODICE',cp_AbsName(oSource.parent,'oMSCODSEZ_2_2'),i_cWhere,'GSOF_ASO',"Sezioni offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT";
                     +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',oSource.xKey(1))
            select SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MSCODSEZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT";
                   +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(this.w_MSCODSEZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',this.w_MSCODSEZ)
            select SOCODICE,SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MSCODSEZ = NVL(_Link_.SOCODICE,space(10))
      this.w_MSDESCRI = NVL(_Link_.SODESCRI,space(35))
      this.w_MSTIPSEZ = NVL(_Link_.SOTIPSEZ,space(1))
      this.w_MSGRUP = NVL(_Link_.SOFILGRU,space(5))
      this.w_MSSOTG = NVL(_Link_.SOFILSOT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MSCODSEZ = space(10)
      endif
      this.w_MSDESCRI = space(35)
      this.w_MSTIPSEZ = space(1)
      this.w_MSGRUP = space(5)
      this.w_MSSOTG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.SOCODICE,1)
      cp_ShowWarn(i_cKey,this.SEZ_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MSCODSEZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.SEZ_OFFE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.SOCODICE as SOCODICE202"+ ",link_2_2.SODESCRI as SODESCRI202"+ ",link_2_2.SOTIPSEZ as SOTIPSEZ202"+ ",link_2_2.SOFILGRU as SOFILGRU202"+ ",link_2_2.SOFILSOT as SOFILSOT202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on MOD_SEZI.MSCODSEZ=link_2_2.SOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and MOD_SEZI.MSCODSEZ=link_2_2.SOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSCODSEZ_2_2.value==this.w_MSCODSEZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSCODSEZ_2_2.value=this.w_MSCODSEZ
      replace t_MSCODSEZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSCODSEZ_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSDESCRI_2_3.value==this.w_MSDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSDESCRI_2_3.value=this.w_MSDESCRI
      replace t_MSDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSDESCRI_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSGRUP_2_5.value==this.w_MSGRUP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSGRUP_2_5.value=this.w_MSGRUP
      replace t_MSGRUP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSGRUP_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSSOTG_2_6.value==this.w_MSSOTG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSSOTG_2_6.value=this.w_MSSOTG
      replace t_MSSOTG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMSSOTG_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESTIPO_2_7.value==this.w_DESTIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESTIPO_2_7.value=this.w_DESTIPO
      replace t_DESTIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESTIPO_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'MOD_SEZI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_CPROWORD) AND NOT EMPTY(.w_MSCODSEZ)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_MSCODSEZ))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_MSCODSEZ=space(10)
      .w_MSDESCRI=space(35)
      .w_MSTIPSEZ=space(1)
      .w_MSGRUP=space(5)
      .w_MSSOTG=space(5)
      .w_DESTIPO=space(12)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_MSCODSEZ))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,7,.f.)
        .w_DESTIPO = ah_msgformat(iif(.w_MSTIPSEZ='D','Descrittiva',iif(.w_MSTIPSEZ='W','File W.P.',iif(.w_MSTIPSEZ='A','Articoli',iif(.w_MSTIPSEZ='G','Gruppi',iif(.w_MSTIPSEZ='S','Sottogruppi',iif(.w_MSTIPSEZ='T','Dati Fissi','')))))))
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MSCODSEZ = t_MSCODSEZ
    this.w_MSDESCRI = t_MSDESCRI
    this.w_MSTIPSEZ = t_MSTIPSEZ
    this.w_MSGRUP = t_MSGRUP
    this.w_MSSOTG = t_MSSOTG
    this.w_DESTIPO = t_DESTIPO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MSCODSEZ with this.w_MSCODSEZ
    replace t_MSDESCRI with this.w_MSDESCRI
    replace t_MSTIPSEZ with this.w_MSTIPSEZ
    replace t_MSGRUP with this.w_MSGRUP
    replace t_MSSOTG with this.w_MSSOTG
    replace t_DESTIPO with this.w_DESTIPO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsof_msmPag1 as StdContainer
  Width  = 668
  height = 237
  stdWidth  = 668
  stdheight = 237
  resizeXpos=348
  resizeYpos=127
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=19, top=12, width=643,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="MSCODSEZ",Label2="Sezione",Field3="MSDESCRI",Label3="Descrizione",Field4="DESTIPO",Label4="Tipo",Field5="MSGRUP",Label5="Gruppo",Field6="MSSOTG",Label6="Sottogruppo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49479558

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=9,top=31,;
    width=639+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=10,top=32,width=638+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='SEZ_OFFE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='SEZ_OFFE'
        oDropInto=this.oBodyCol.oRow.oMSCODSEZ_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsof_msmBodyRow as CPBodyRowCnt
  Width=629
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="QEBAZAWLDO",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "N.riga",;
    HelpContextID = 17151126,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=42, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oMSCODSEZ_2_2 as StdTrsField with uid="ZPNOUFIXVJ",rtseq=3,rtrep=.t.,;
    cFormVar="w_MSCODSEZ",value=space(10),;
    ToolTipText = "Codice sezione",;
    HelpContextID = 238461152,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=41, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SEZ_OFFE", cZoomOnZoom="GSOF_ASO", oKey_1_1="SOCODICE", oKey_1_2="this.w_MSCODSEZ"

  func oMSCODSEZ_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMSCODSEZ_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMSCODSEZ_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SEZ_OFFE','*','SOCODICE',cp_AbsName(this.parent,'oMSCODSEZ_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ASO',"Sezioni offerta",'',this.parent.oContained
  endproc
  proc oMSCODSEZ_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ASO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SOCODICE=this.parent.oContained.w_MSCODSEZ
    i_obj.ecpSave()
  endproc

  add object oMSDESCRI_2_3 as StdTrsField with uid="EQFCAEWPGM",rtseq=4,rtrep=.t.,;
    cFormVar="w_MSDESCRI",value=space(35),enabled=.f.,;
    HelpContextID = 223383793,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=264, Left=140, Top=0, InputMask=replicate('X',35)

  add object oMSGRUP_2_5 as StdTrsField with uid="VQLVJMRNND",rtseq=6,rtrep=.t.,;
    cFormVar="w_MSGRUP",value=space(5),enabled=.f.,;
    HelpContextID = 2318650,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=504, Top=0, InputMask=replicate('X',5)

  add object oMSSOTG_2_6 as StdTrsField with uid="GEHVBASAEA",rtseq=7,rtrep=.t.,;
    cFormVar="w_MSSOTG",value=space(5),enabled=.f.,;
    HelpContextID = 154509626,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=61, Left=563, Top=0, InputMask=replicate('X',5)

  add object oDESTIPO_2_7 as StdTrsField with uid="EITQWWGJHE",rtseq=8,rtrep=.t.,;
    cFormVar="w_DESTIPO",value=space(12),enabled=.f.,;
    HelpContextID = 14725066,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=404, Top=0, InputMask=replicate('X',12)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_msm','MOD_SEZI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MSCODICE=MOD_SEZI.MSCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
