* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bd1                                                        *
*              Ritorna progressivo ordine da PDA                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-22                                                      *
* Last revis.: 2003-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bd1",oParentObject)
return(i_retval)

define class tgsof_bd1 as StdBatch
  * --- Local variables
  w_MVANNDOC = space(4)
  w_MVPRD = space(2)
  w_MVALFDOC = space(10)
  w_MVNUMDOC = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Torna Progressivo Documento  (da GSOF_KDO)
    this.w_MVANNDOC = this.oParentObject.w_MVANNDOC
    this.w_MVPRD = this.oParentObject.w_MVPRD
    this.w_MVALFDOC = this.oParentObject.w_MVALFDOC
    this.w_MVNUMDOC = 0
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_AskTableProg(this, i_Conn, "PRDOC", "i_CODAZI,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    this.oParentObject.w_MVNUMDOC = this.w_MVNUMDOC
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
