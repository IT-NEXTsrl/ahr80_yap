* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bof                                                        *
*              Stampa del report offerta                                       *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_322]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2016-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bof",oParentObject)
return(i_retval)

define class tgsof_bof as StdBatch
  * --- Local variables
  w_CODCLI = space(15)
  a = 0
  b = 0
  w_OK = .f.
  w_PRGSTA = space(30)
  w_CODMOD = space(5)
  w_REP = 0
  w_OREP = space(50)
  w_OQRY = space(50)
  w_OFSERIA1 = space(10)
  w_ORDINE = space(1)
  w_OFDATDOC = ctod("  /  /  ")
  w_OFNUMDOC = 0
  w_OFSERDOC = space(10)
  w_OFNUMVER = 0
  w_OFSCOCL1 = 0
  w_OFSCOCL2 = 0
  w_VASIMVAL = space(5)
  w_VADECTOT = 0
  w_VACODVAL = space(3)
  w_VACAOVAL = 0
  w_VADECUNI = 0
  w_ANCODICE = space(20)
  w_ANDESCRI = space(40)
  w_ANDESCR2 = space(40)
  w_ANINDIRI = space(35)
  w_ANINDIR2 = space(35)
  w_AN__CAP = space(8)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANNAZION = space(3)
  w_ANPARIVA = space(12)
  w_ANCODFIS = space(16)
  w_OFCODPAG = space(5)
  w_OFSCOPAG = 0
  w_OFSCONTI = 0
  w_OFSPEINC = 0
  w_OFSPEIMB = 0
  w_OFSPETRA = 0
  w_OFCODUTE = 0
  w_OFCODCON = space(5)
  w_OFCODAGE = space(5)
  w_OFCODAG2 = space(5)
  w_OFCODPOR = space(1)
  w_OFCODSPE = space(3)
  w_ODCODGRU = space(5)
  w_ODCODSOT = space(5)
  w_ODCODICE = space(20)
  w_ODCODART = space(20)
  w_ODUNIMIS = space(3)
  w_ODQTAMOV = 0
  w_ODFLOMAG = space(1)
  w_ODSCONT1 = 0
  w_ODSCONT2 = 0
  w_ODSCONT3 = 0
  w_ODSCONT4 = 0
  w_ODTIPRIG = space(1)
  w_ODVALRIG = 0
  w_ODPREZZO = 0
  w_ODQTAUM1 = 0
  w_ODNOTAGG = space(0)
  w_ODDESART = space(40)
  w_NCPERSON = space(40)
  w_OFDATSCA = ctod("  /  /  ")
  w_OFRIFDES = space(45)
  w_TSDESCRI = space(35)
  w_ARSTASUP = space(1)
  w_ARSTACOD = space(1)
  RECTRS = 0
  w_MODESCRI = space(35)
  w_OLDSUBJECT = space(254)
  w_CONTRIF = space(5)
  w_LUCODISO = space(3)
  w_MOALMAIL = space(1)
  w_PATHARC = space(254)
  w_PATHALL = space(254)
  w_RagSoc = space(60)
  w_CODNOM = space(20)
  w_EMAIL = space(254)
  w_EMPEC = space(254)
  w_TELFAX = space(18)
  w_RIFPERS = space(40)
  w_KEYOBS = ctod("  /  /  ")
  w_CODICE1 = space(20)
  w_LENSCF = 0
  w_LINGUA = space(3)
  w_DESART1 = space(40)
  w_DESSUP1 = space(0)
  w_ArcNom = space(1)
  * --- WorkFile variables
  OFF_ERTE_idx=0
  MOD_OFFE_idx=0
  OUT_PUTS_idx=0
  OFF_SEZI_idx=0
  OFF_NOMI_idx=0
  KEY_ARTI_idx=0
  TRADARTI_idx=0
  NOM_CONT_idx=0
  ALL_EGAT_idx=0
  PAR_OFFE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue la Stampa del Report Offerta  (da GSOF_AOF) o da GSOF_BEW
    this.w_PRGSTA = LEFT("GSOF_AOF"+SPACE(30),30)
    * --- Read from OFF_ERTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OFCODMOD,OFCODNOM,OFCODCON"+;
        " from "+i_cTable+" OFF_ERTE where ";
            +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OFCODMOD,OFCODNOM,OFCODCON;
        from (i_cTable) where;
            OFSERIAL = this.oParentObject.w_OFSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODMOD = NVL(cp_ToDate(_read_.OFCODMOD),cp_NullValue(_read_.OFCODMOD))
      this.w_CODNOM = NVL(cp_ToDate(_read_.OFCODNOM),cp_NullValue(_read_.OFCODNOM))
      this.w_CONTRIF = NVL(cp_ToDate(_read_.OFCODCON),cp_NullValue(_read_.OFCODCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from OFF_NOMI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "NOCODCLI,NODESCRI"+;
        " from "+i_cTable+" OFF_NOMI where ";
            +"NOCODICE = "+cp_ToStrODBC(this.w_CODNOM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        NOCODCLI,NODESCRI;
        from (i_cTable) where;
            NOCODICE = this.w_CODNOM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
      this.w_RagSoc = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from MOD_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MONUMREP,MODESCRI,MOALMAIL,MOPATARC"+;
        " from "+i_cTable+" MOD_OFFE where ";
            +"MOCODICE = "+cp_ToStrODBC(this.w_CODMOD);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MONUMREP,MODESCRI,MOALMAIL,MOPATARC;
        from (i_cTable) where;
            MOCODICE = this.w_CODMOD;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_REP = NVL(cp_ToDate(_read_.MONUMREP),cp_NullValue(_read_.MONUMREP))
      this.w_MODESCRI = NVL(cp_ToDate(_read_.MODESCRI),cp_NullValue(_read_.MODESCRI))
      this.w_MOALMAIL = NVL(cp_ToDate(_read_.MOALMAIL),cp_NullValue(_read_.MOALMAIL))
      this.w_PATHARC = NVL(cp_ToDate(_read_.MOPATARC),cp_NullValue(_read_.MOPATARC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from OUT_PUTS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2],.t.,this.OUT_PUTS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OUNOMQUE,OUNOMREP"+;
        " from "+i_cTable+" OUT_PUTS where ";
            +"OUNOMPRG = "+cp_ToStrODBC(this.w_PRGSTA);
            +" and OUROWNUM = "+cp_ToStrODBC(this.w_REP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OUNOMQUE,OUNOMREP;
        from (i_cTable) where;
            OUNOMPRG = this.w_PRGSTA;
            and OUROWNUM = this.w_REP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OQRY = NVL(cp_ToDate(_read_.OUNOMQUE),cp_NullValue(_read_.OUNOMQUE))
      this.w_OREP = NVL(cp_ToDate(_read_.OUNOMREP),cp_NullValue(_read_.OUNOMREP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_OK = .T.
    do case
      case EMPTY(this.w_REP)
        this.w_OK = .F.
      case EMPTY(this.w_OREP) AND NOT EMPTY(this.w_REP)
        AH_ErrorMsg("File .Frx non definito nel programma di stampa")
        this.w_OK = .F.
      case EMPTY(this.w_OQRY) AND NOT EMPTY(this.w_REP)
        AH_ErrorMsg("File .Vqr non definito nel programma di stampa")
        this.w_OK = .F.
    endcase
    * --- Cambio della Valuta in LIRE utilizzato in talune stampe
    l_cambio = GETCAM(g_PERVAL, I_DATSYS)
    if this.w_OK
      AH_Msg( "Stampa in corso..." )
      * --- Creo il Temporaneo 
      vq_exec(alltrim(this.w_OQRY),this,"__TMP__")
      * --- Creo il Temporaneo contenente le sole sezioni descrittive dell'offerta
      vq_exec("..\offe\exe\query\Gsof1sof.vqr",this,"CURS")
      this.RECTRS = reccount("CURS")
      * --- Creo il Temporaneo 
      *     nota descrittiva di intestazione  
      *     ordine = A
      *     il dettaglio articoli
      *     ordine = B
      *     note descrittive di piede
      *     ordine = C
      if this.RECTRS >0 AND !Isalt()
        this.a = 0
        this.b = 0
        Select * from __TMP__ into cursor TEMP
        SELECT __TMP__
        ZAP
        SELECT CURS
        GO TOP
        SCAN 
        if this.a = 0
          SELECT TEMP
          GO TOP
          SCAN 
          if this.b = 0
            this.w_ORDINE = "A"
            this.w_ODVALRIG = 0
            this.w_ODNOTAGG = NVL(ALLTRIM(CURS.OS__NOTE)," ")
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.b = this.b + 1
          endif
          this.w_ORDINE = "B"
          this.w_ODVALRIG = TEMP.ODVALRIG
          this.w_ODNOTAGG = NVL(ALLTRIM(TEMP.ODNOTAGG)," ")
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_ODNOTAGG = ""
          ENDSCAN
        else
          SELECT TEMP
          GO TOP
          SCAN RECORD 1
          this.w_ORDINE = "C"
          this.w_ODVALRIG = 0
          this.w_ODNOTAGG = CURS.OS__NOTE
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ENDSCAN
        endif
        this.a = this.a + 1
        ENDSCAN
      else
        * --- Valorizza solo i campi che servono per l'invio e-mail
        Select * from __TMP__ into cursor TEMP
        this.w_OFDATDOC = TEMP.OFDATDOC
        this.w_OFNUMDOC = TEMP.OFNUMDOC
        this.w_OFSERDOC = TEMP.OFSERDOC
      endif
      * --- Lancio la Stampa
      * --- Cerca il bmp del logo con il nome del codice dell'azienda se non lo trova mette quello di default
      L_LOGO=GETBMP(I_CODAZI)
      * --- Verifico se � stato selezionato un contatto di riferimento, in tale caso prendo come indirizzo email e
      *     numero di Fax quelli ad esso associati, altrimenti prendo quelli del nominativo
      i_CLIFORDES="C"
      if NOT EMPTY(this.w_CODCLI)
        i_CODDES=this.w_CODCLI
      else
        * --- altrimenti leggo il codice nominativo
        i_CODDES=this.w_CODNOM
      endif
      i_ORIGINE="O"
      * --- Ricerca nelle sedi di consegna il numero di Fax
      i_TIPDES="CO"
      if not empty(this.w_CONTRIF)
        * --- Read from NOM_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NOM_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NC_EMAIL,NC_EMPEC,NCTELFAX,NCPERSON"+;
            " from "+i_cTable+" NOM_CONT where ";
                +"NCCODICE = "+cp_ToStrODBC(this.w_CODNOM);
                +" and NCCODCON = "+cp_ToStrODBC(this.w_CONTRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NC_EMAIL,NC_EMPEC,NCTELFAX,NCPERSON;
            from (i_cTable) where;
                NCCODICE = this.w_CODNOM;
                and NCCODCON = this.w_CONTRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_EMAIL = NVL(cp_ToDate(_read_.NC_EMAIL),cp_NullValue(_read_.NC_EMAIL))
          this.w_EMPEC = NVL(cp_ToDate(_read_.NC_EMPEC),cp_NullValue(_read_.NC_EMPEC))
          this.w_TELFAX = NVL(cp_ToDate(_read_.NCTELFAX),cp_NullValue(_read_.NCTELFAX))
          this.w_RIFPERS = NVL(cp_ToDate(_read_.NCPERSON),cp_NullValue(_read_.NCPERSON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Verifico se � valorizzato l'indirizzo mail
        if empty(this.w_EMAIL) 
          * --- Read from OFF_NOMI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NO_EMAIL"+;
              " from "+i_cTable+" OFF_NOMI where ";
                  +"NOCODICE = "+cp_ToStrODBC(this.w_CODNOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NO_EMAIL;
              from (i_cTable) where;
                  NOCODICE = this.w_CODNOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_EMAIL = NVL(cp_ToDate(_read_.NO_EMAIL),cp_NullValue(_read_.NO_EMAIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Verifico se � valorizzato l'indirizzo mail PEC
        if empty(this.w_EMPEC) 
          * --- Read from OFF_NOMI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NO_EMPEC"+;
              " from "+i_cTable+" OFF_NOMI where ";
                  +"NOCODICE = "+cp_ToStrODBC(this.w_CODNOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NO_EMPEC;
              from (i_cTable) where;
                  NOCODICE = this.w_CODNOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_EMPEC = NVL(cp_ToDate(_read_.NO_EMPEC),cp_NullValue(_read_.NO_EMPEC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Verifico se � valorizzato il numero di fax
        if empty(this.w_TELFAX)
          * --- Read from OFF_NOMI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NOTELFAX"+;
              " from "+i_cTable+" OFF_NOMI where ";
                  +"NOCODICE = "+cp_ToStrODBC(this.w_CODNOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NOTELFAX;
              from (i_cTable) where;
                  NOCODICE = this.w_CODNOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TELFAX = NVL(cp_ToDate(_read_.NOTELFAX),cp_NullValue(_read_.NOTELFAX))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      else
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NO_EMAIL,NO_EMPEC,NOTELFAX,NODESCRI"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_CODNOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NO_EMAIL,NO_EMPEC,NOTELFAX,NODESCRI;
            from (i_cTable) where;
                NOCODICE = this.w_CODNOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_EMAIL = NVL(cp_ToDate(_read_.NO_EMAIL),cp_NullValue(_read_.NO_EMAIL))
          this.w_EMPEC = NVL(cp_ToDate(_read_.NO_EMPEC),cp_NullValue(_read_.NO_EMPEC))
          this.w_TELFAX = NVL(cp_ToDate(_read_.NOTELFAX),cp_NullValue(_read_.NOTELFAX))
          this.w_RIFPERS = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      i_DEST= this.w_RIFPERS
      i_FAXNO = this.w_TELFAX
      * --- Salvo il vecchio oggetto (definito nel cnf)
      this.w_OLDSUBJECT = i_EMAILSUBJECT
      i_EMAIL = this.w_EMAIL
      i_EMAIL_PEC = this.w_EMPEC
      if EMPTY(this.w_MODESCRI)
         i_EMAILSUBJECT =ah_Msgformat("Documento di offerta (%1) n. %2 del %3",this.w_CODMOD, alltrim(STR(this.w_OFNUMDOC,15,0))+IIF(EMPTY(this.w_OFSERDOC),"","/")+alltrim(this.w_OFSERDOC), dtoc(this.w_OFDATDOC) )
      else
         i_EMAILSUBJECT =ah_Msgformat("%1 n. %2 del %3", ALLTRIM(this.w_MODESCRI), alltrim(STR(this.w_OFNUMDOC,15,0))+IIF(EMPTY(this.w_OFSERDOC),"","/")+alltrim(this.w_OFSERDOC), dtoc(this.w_OFDATDOC) )
      endif
      i_WEDEST = "C" + this.w_CODNOM
      if VARTYPE(i_OFALLENAME)<>"C"
        public i_OFALLENAME
      endif
      i_OFALLENAME = ""
      if this.w_MOALMAIL="M" OR this.w_MOALMAIL="E"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_OFALLENAME = this.w_PATHALL+ALLTRIM(this.oParentObject.w_OFPATPDF)+";"
      endif
      * --- Select from gsof_bia
      do vq_exec with 'gsof_bia',this,'_Curs_gsof_bia','',.f.,.t.
      if used('_Curs_gsof_bia')
        select _Curs_gsof_bia
        locate for 1=1
        do while not(eof())
        i_OFALLENAME = i_OFALLENAME + ofsrcfile( ALLTRIM(NVL(_Curs_gsof_bia.ALPATFIL, " ")), ALLTRIM(NVL(_Curs_gsof_bia.ALSERIAL, " "))) +"; "
          select _Curs_gsof_bia
          continue
        enddo
        use
      endif
      i_OFALLENAME = IIF(LEN(i_OFALLENAME)>2, LEFT(i_OFALLENAME, LEN(i_OFALLENAME)-1), "")
      i_WEALLETITLE = ""
      CP_CHPRN( ALLTRIM(this.w_OREP),"",this.oParentObject )
      WAIT CLEAR
      * --- Resetta Fax/Email
      i_FAXNO = ""
      i_EMAIL = ""
      i_EMAIL_PEC = ""
      i_DEST = " "
      i_WEDEST = " "
      i_OFALLENAME = " "
      i_WEALLENAME = " "
      i_WEALLETITLE = " "
      i_EMAILSUBJECT=this.w_OLDSUBJECT
      * --- Resetto le variabili per la selezione del Fax
      i_CLIFORDES=" "
      i_CODDES=" "
      i_TIPDES=" "
      i_ORIGINE=" "
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
    if used("TEMP")
      select TEMP
      use
    endif
    if used("CURS")
      select CURS
      use
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive il cursore di stampa
    INSERT INTO __TMP__ ;
    (OFSERIAL,ORDINE,OFDATDOC,OFNUMDOC,OFSERDOC,OFNUMVER,OFSCOCL1,OFSCOCL2,VASIMVAL,VADECTOT, ;
    VACODVAL,VACAOVAL,VADECUNI,ANCODICE,ANDESCRI,ANDESCR2,ANINDIRI,ANINDIR2,AN__CAP,ANLOCALI,;
    ANPROVIN,ANNAZION,ANPARIVA,ANCODFIS,OFCODPAG,OFSCOPAG,OFSCONTI,OFSPEINC,OFSPEIMB,OFSPETRA,OFCODUTE,OFCODCON, ;
    OFCODAGE,OFCODAG2,OFCODPOR,OFCODSPE,ODCODGRU,ODCODSOT,ODCODICE,ODCODART,ODUNIMIS,ODQTAMOV,;
    ODFLOMAG,ODSCONT1,ODSCONT2,ODSCONT3,ODSCONT4,ODTIPRIG,ODVALRIG,ODPREZZO,ODQTAUM1,ODNOTAGG,;
    ODDESART,NCPERSON,OFDATSCA,OFRIFDES,TSDESCRI,ARSTASUP,ARSTACOD,OFCODNOM,LUCODISO);
    VALUES (this.w_OFSERIA1,this.w_ORDINE,this.w_OFDATDOC,this.w_OFNUMDOC,this.w_OFSERDOC,this.w_OFNUMVER,this.w_OFSCOCL1,this.w_OFSCOCL2,this.w_VASIMVAL,this.w_VADECTOT, ;
    this.w_VACODVAL,this.w_VACAOVAL,this.w_VADECUNI,this.w_ANCODICE,this.w_ANDESCRI,this.w_ANDESCR2,this.w_ANINDIRI,this.w_ANINDIR2,this.w_AN__CAP,this.w_ANLOCALI,;
    this.w_ANPROVIN,this.w_ANNAZION,this.w_ANPARIVA,this.w_ANCODFIS,this.w_OFCODPAG,this.w_OFSCOPAG,this.w_OFSCONTI,this.w_OFSPEINC,this.w_OFSPEIMB,this.w_OFSPETRA,this.w_OFCODUTE,this.w_OFCODCON, ;
    this.w_OFCODAGE,this.w_OFCODAG2,this.w_OFCODPOR,this.w_OFCODSPE,this.w_ODCODGRU,this.w_ODCODSOT,this.w_ODCODICE,this.w_ODCODART,this.w_ODUNIMIS,this.w_ODQTAMOV,;
    this.w_ODFLOMAG,this.w_ODSCONT1,this.w_ODSCONT2,this.w_ODSCONT3,this.w_ODSCONT4,this.w_ODTIPRIG,this.w_ODVALRIG,this.w_ODPREZZO,this.w_ODQTAUM1,this.w_ODNOTAGG,;
    this.w_ODDESART,this.w_NCPERSON,this.w_OFDATSCA,this.w_OFRIFDES,this.w_TSDESCRI,this.w_ARSTASUP,this.w_ARSTACOD,this.w_CODNOM,this.w_LUCODISO)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Muovo tutti i campi del cursore origine 
    this.w_OFSERIA1 = this.oParentObject.w_OFSERIAL
    this.w_OFDATDOC = TEMP.OFDATDOC
    this.w_OFNUMDOC = TEMP.OFNUMDOC
    this.w_OFSERDOC = TEMP.OFSERDOC
    this.w_OFNUMVER = TEMP.OFNUMVER
    this.w_OFSCOCL1 = TEMP.OFSCOCL1
    this.w_OFSCOCL2 = TEMP.OFSCOCL2
    this.w_VASIMVAL = TEMP.VASIMVAL
    this.w_VADECTOT = TEMP.VADECTOT
    this.w_VACODVAL = TEMP.VACODVAL
    this.w_VACAOVAL = TEMP.VACAOVAL
    this.w_VADECUNI = TEMP.VADECUNI
    this.w_ANCODICE = TEMP.ANCODICE
    this.w_ANDESCRI = TEMP.ANDESCRI
    this.w_ANDESCR2 = TEMP.ANDESCR2
    this.w_ANINDIRI = TEMP.ANINDIRI
    this.w_ANINDIR2 = TEMP.ANINDIR2
    this.w_AN__CAP = TEMP.AN__CAP
    this.w_ANLOCALI = TEMP.ANLOCALI
    this.w_ANPROVIN = TEMP.ANPROVIN
    this.w_ANNAZION = TEMP.ANNAZION
    this.w_ANPARIVA = TEMP.ANPARIVA
    this.w_ANCODFIS = TEMP.ANCODFIS
    this.w_OFCODPAG = TEMP.OFCODPAG
    this.w_OFSCOPAG = TEMP.OFSCOPAG
    this.w_OFSCONTI = TEMP.OFSCONTI
    this.w_OFSPEINC = TEMP.OFSPEINC
    this.w_OFSPEIMB = TEMP.OFSPEIMB
    this.w_OFSPETRA = TEMP.OFSPETRA
    this.w_OFCODUTE = TEMP.OFCODUTE
    this.w_OFCODCON = TEMP.OFCODCON
    this.w_OFCODAGE = TEMP.OFCODAGE
    this.w_OFCODAG2 = TEMP.OFCODAG2
    this.w_OFCODPOR = TEMP.OFCODPOR
    this.w_OFCODSPE = TEMP.OFCODSPE
    this.w_ODCODGRU = TEMP.ODCODGRU
    this.w_ODCODSOT = TEMP.ODCODSOT
    this.w_ODCODICE = TEMP.ODCODICE
    this.w_ODCODART = TEMP.ODCODART
    this.w_ODUNIMIS = TEMP.ODUNIMIS
    this.w_ODQTAMOV = TEMP.ODQTAMOV
    this.w_ODFLOMAG = TEMP.ODFLOMAG
    this.w_ODSCONT1 = TEMP.ODSCONT1
    this.w_ODSCONT2 = TEMP.ODSCONT2
    this.w_ODSCONT3 = TEMP.ODSCONT3
    this.w_ODSCONT4 = TEMP.ODSCONT4
    this.w_ODTIPRIG = TEMP.ODTIPRIG
    this.w_ODPREZZO = TEMP.ODPREZZO
    this.w_ODQTAUM1 = TEMP.ODQTAUM1
    this.w_ODDESART = TEMP.ODDESART
    this.w_NCPERSON = TEMP.NCPERSON
    this.w_OFDATSCA = TEMP.OFDATSCA
    this.w_OFRIFDES = TEMP.OFRIFDES
    this.w_TSDESCRI = TEMP.TSDESCRI
    this.w_ARSTASUP = TEMP.ARSTASUP
    this.w_ARSTACOD = TEMP.ARSTACOD
    this.w_LUCODISO = TEMP.LUCODISO
    this.w_LINGUA = NVL(TEMP.NOCODLIN,"")
    this.w_KEYOBS = this.w_OFDATDOC
    this.w_CODICE1 = SPACE(20)
    this.w_LENSCF = 0
    * --- Select from KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CACODICE,CADTOBSO,CALENSCF  from "+i_cTable+" KEY_ARTI ";
          +" where CACODART= "+cp_ToStrODBC(this.w_ODCODART)+" AND CACODCON= "+cp_ToStrODBC(this.w_CODCLI)+" AND CATIPCON= 'C' AND CACODICE<> "+cp_ToStrODBC(this.w_ODCODART)+" ";
          +" order by CATIPBAR DESC,CALENSCF, CACODICE";
           ,"_Curs_KEY_ARTI")
    else
      select CACODICE,CADTOBSO,CALENSCF from (i_cTable);
       where CACODART= this.w_ODCODART AND CACODCON= this.w_CODCLI AND CATIPCON= "C" AND CACODICE<> this.w_ODCODART ;
       order by CATIPBAR DESC,CALENSCF, CACODICE;
        into cursor _Curs_KEY_ARTI
    endif
    if used('_Curs_KEY_ARTI')
      select _Curs_KEY_ARTI
      locate for 1=1
      do while not(eof())
      if (CP_TODATE(_Curs_KEY_ARTI.CADTOBSO) > this.w_KEYOBS) OR EMPTY(NVL(CP_TODATE(_Curs_KEY_ARTI.CADTOBSO)," "))
        this.w_CODICE1 = _Curs_KEY_ARTI.CACODICE 
        this.w_LENSCF = Nvl(_Curs_KEY_ARTI.CALENSCF,0)
        if this.w_CODICE1= this.w_ODCODICE
          * --- Nel caso sia stato trovato come codice di ricerca valido lo stesso usato sul documento
          *     esce dal ciclo e mantiene questo.
          EXIT
        endif
      endif
        select _Curs_KEY_ARTI
        continue
      enddo
      use
    endif
    if NOT EMPTY(NVL(this.w_CODICE1,""))
      if i_Rows<>0
        if NOT EMPTY(NVL(this.w_DESART1," "))
          this.w_ODDESART = this.w_DESART1
        endif
        if NOT EMPTY(NVL(this.w_DESSUP1," "))
          this.w_ODNOTAGG = this.w_DESSUP1
        endif
      endif
      * --- Se Attivo Flag Codifica Cliente/Fornitore Esclusiva in Azienda
      *     Prendo solo la prima parte del codice di ricerca (elimino il suffisso da destra)
      if g_FLCESC = "S" And this.w_LENSCF <> 0
        this.w_CODICE1 = Left(Alltrim(this.w_CODICE1),Len(Alltrim(this.w_CODICE1))-this.w_LENSCF)
      endif
      this.w_ODCODICE = this.w_CODICE1
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se esiste il file calcolo nominativo
    * --- Read from PAR_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "POFLCAAR"+;
        " from "+i_cTable+" PAR_OFFE where ";
            +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        POFLCAAR;
        from (i_cTable) where;
            POCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ArcNom = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ArcNom="S"
      this.w_PATHALL = ALLTRIM(this.w_PATHARC)+alltrim(this.w_RagSoc)+"\" 
    else
      this.w_PATHALL = ALLTRIM(this.w_PATHARC)
    endif
    * --- Calcolo del periodo
    do case
      case this.oParentObject.w_PERIODO="M"
        this.w_PATHALL = this.w_PATHALL+ALLTRIM(CMONTH(this.w_OFDATDOC))+"\"
      case this.oParentObject.w_PERIODO="T"
        do case
          case MONTH(this.w_OFDATDOC)<=3
            this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Trimestre1\")
          case MONTH(this.w_OFDATDOC)>3 AND MONTH(this.w_OFDATDOC)<=6
            this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Trimestre2\")
          case MONTH(this.w_OFDATDOC)>6 AND MONTH(this.w_OFDATDOC)<=9
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Trimestre3\")
          case MONTH(this.w_OFDATDOC)>9 AND MONTH(this.w_OFDATDOC)<=12
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Trimestre4\")
        endcase
      case this.oParentObject.w_PERIODO="Q"
        do case
          case MONTH(this.w_OFDATDOC)<=4
            this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Quadrimestre1\")
          case MONTH(this.w_OFDATDOC)>4 AND MONTH(this.w_OFDATDOC)<=8
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Quadrimestre2\")
          case MONTH(this.w_OFDATDOC)>8 AND MONTH(this.w_OFDATDOC)<=12
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Quadrimestre3\")
        endcase
      case this.oParentObject.w_PERIODO="S"
        do case
          case MONTH(this.w_OFDATDOC)<=6
            this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Semestre1\")
          case MONTH(this.w_OFDATDOC)>6 AND MONTH(this.w_OFDATDOC)<=12
            this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Semestre2\")
        endcase
      case this.oParentObject.w_PERIODO="A"
        this.w_PATHALL = this.w_PATHALL+ALLTRIM(STR(YEAR(this.w_OFDATDOC)))+"\"
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='OFF_ERTE'
    this.cWorkTables[2]='MOD_OFFE'
    this.cWorkTables[3]='OUT_PUTS'
    this.cWorkTables[4]='OFF_SEZI'
    this.cWorkTables[5]='OFF_NOMI'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='TRADARTI'
    this.cWorkTables[8]='NOM_CONT'
    this.cWorkTables[9]='ALL_EGAT'
    this.cWorkTables[10]='PAR_OFFE'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_gsof_bia')
      use in _Curs_gsof_bia
    endif
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
