* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bg2                                                        *
*              Legge dati per mappe (per GSUT_BGM)                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-11-03                                                      *
* Last revis.: 2010-11-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODCON
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bg2",oParentObject,m.pCODCON)
return(i_retval)

define class tgsof_bg2 as StdBatch
  * --- Local variables
  pCODCON = space(15)
  * --- WorkFile variables
  OFF_ERTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSUT_BGM
    *     Legge dati per mappe
    * --- Read from OFF_ERTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OFCODNOM"+;
        " from "+i_cTable+" OFF_ERTE where ";
            +"OFSERIAL = "+cp_ToStrODBC(this.pCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OFCODNOM;
        from (i_cTable) where;
            OFSERIAL = this.pCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_CODCON = NVL(cp_ToDate(_read_.OFCODNOM),cp_NullValue(_read_.OFCODNOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
  endproc


  proc Init(oParentObject,pCODCON)
    this.pCODCON=pCODCON
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_ERTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODCON"
endproc
