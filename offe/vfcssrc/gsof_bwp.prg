* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bwp                                                        *
*              Visualizza documento Word/PDF                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_34]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-20                                                      *
* Last revis.: 2013-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bwp",oParentObject,m.pParam)
return(i_retval)

define class tgsof_bwp as StdBatch
  * --- Local variables
  pParam = space(1)
  w_PATHALL = space(254)
  w_ArcNom = space(1)
  w_RagSoc = space(40)
  w_NOMFILE = space(50)
  oManager = .NULL.
  oDesktop = .NULL.
  oWriter = .NULL.
  * --- WorkFile variables
  OFF_NOMI_idx=0
  PAR_OFFE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura file Word/PDF
    * --- Lanciato da GSOF_AOF (Offerte) o dalle ricerche e da nominativi GSAR_ANO
    * --- Parametro per distinguere documento Word (W) da PDF (P)
    * --- Read from PAR_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "POFLCAAR"+;
        " from "+i_cTable+" PAR_OFFE where ";
            +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        POFLCAAR;
        from (i_cTable) where;
            POCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ArcNom = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ArcNom="S"
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NODESCRI"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NODESCRI;
          from (i_cTable) where;
              NOCODICE = this.oParentObject.w_OFCODNOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RagSoc = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PATHALL = AddBs(FULLPATH(ALLTRIM(this.oParentObject.w_PATHARC))+alltrim(this.w_RagSoc))
    else
      this.w_PATHALL = AddBs(FULLPATH(ALLTRIM(this.oParentObject.w_PATHARC)))
    endif
    if NOT EMPTY(this.oParentObject.w_PATHARC)
      * --- Calcolo del periodo
      do case
        case this.oParentObject.w_PERIODO="M"
          this.w_PATHALL = FULLPATH(ALLTRIM(this.w_PATHALL))+ALLTRIM(CMONTH(this.oParentObject.w_OFDATDOC))+"\"
        case this.oParentObject.w_PERIODO="T"
          do case
            case MONTH(this.oParentObject.w_OFDATDOC)<=3
              this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Trimestre1\")
            case MONTH(this.oParentObject.w_OFDATDOC)>3 AND MONTH(this.oParentObject.w_OFDATDOC)<=6
              this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Trimestre2\")
            case MONTH(this.oParentObject.w_OFDATDOC)>6 AND MONTH(this.oParentObject.w_OFDATDOC)<=9
              this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Trimestre3\")
            case MONTH(this.oParentObject.w_OFDATDOC)>9 AND MONTH(this.oParentObject.w_OFDATDOC)<=12
              this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Trimestre4\")
          endcase
        case this.oParentObject.w_PERIODO="Q"
          do case
            case MONTH(this.oParentObject.w_OFDATDOC)<=4
              this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Quadrimestre1\")
            case MONTH(this.oParentObject.w_OFDATDOC)>4 AND MONTH(this.oParentObject.w_OFDATDOC)<=8
              this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Quadrimestre2\")
            case MONTH(this.oParentObject.w_OFDATDOC)>8 AND MONTH(this.oParentObject.w_OFDATDOC)<=12
              this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Quadrimestre3\")
          endcase
        case this.oParentObject.w_PERIODO="S"
          do case
            case MONTH(this.oParentObject.w_OFDATDOC)<=6
              this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Semestre1\")
            case MONTH(this.oParentObject.w_OFDATDOC)>6 AND MONTH(this.oParentObject.w_OFDATDOC)<=12
              this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Semestre2\")
          endcase
        case this.oParentObject.w_PERIODO="A"
          this.w_PATHALL = this.w_PATHALL+ALLTRIM(STR(YEAR(this.oParentObject.w_OFDATDOC)))+"\"
      endcase
      if this.pParam="P"
        if NOT EMPTY(this.oParentObject.w_OFPATPDF)
          if FILE(FULLPATH(ALLTRIM(this.w_PATHALL))+ALLTRIM(this.oParentObject.w_OFPATPDF))=.F.
            AH_ErrorMsg("Il percorso per il modello PDF non � valido",48)
            i_retcode = 'stop'
            return
          endif
          * --- Se la maschera delle offerte � in edit allora viene visualizzata una copia dell offerta
          if UPPER (this.oparentobject.cfunction)="QUERY" or UPPER (this.oparentobject.class)="TGSOF_KRO"
            DELETE FILE SYS(2023)+"\Copia di " + alltrim (this.oParentObject.w_OFPATPDF) 
 COPY FILE FULLPATH(ALLTRIM(this.w_PATHALL))+ALLTRIM(this.oParentObject.w_OFPATPDF) to SYS(2023)+"\Copia di " + alltrim (this.oParentObject.w_OFPATPDF)
            result2=STAPDF( SYS(2023)+"\Copia di " + alltrim(this.oParentObject.w_OFPATPDF), "OPEN", " ")
          else
            result2=STAPDF( FULLPATH(ALLTRIM(this.w_PATHALL))+ALLTRIM(this.oParentObject.w_OFPATPDF), "OPEN", " ")
          endif
        else
          AH_ErrorMsg("Il file PDF � vuoto",48)
        endif
      endif
      if this.pParam="W"
        if NOT EMPTY(this.oParentObject.w_OFPATFWP)
          if FILE(FULLPATH(ALLTRIM(this.w_PATHALL))+ALLTRIM(this.oParentObject.w_OFPATFWP))=.F.
            AH_ErrorMsg("Il percorso per il modello Word Processor non � valido",16)
            i_retcode = 'stop'
            return
          endif
          * --- Se la maschera delle offerte � in edit allora viene visualizzata una copia dell offerta
          if (UPPER (this.oparentobject.cfunction)="QUERY"or UPPER (this.oparentobject.class)="TGSOF_KRO") AND !Isalt()
            DELETE FILE SYS(2023)+"\Copia di " + alltrim (this.oParentObject.w_OFPATFWP) 
 COPY FILE FULLPATH(ALLTRIM(this.w_PATHALL))+ALLTRIM(this.oParentObject.w_OFPATFWP) to SYS(2023)+"\Copia di " + alltrim (this.oParentObject.w_OFPATFWP)
            this.w_PATHALL = SYS(2023)
            this.w_NOMFILE = "\Copia di " + alltrim (this.oParentObject.w_OFPATFWP)
          else
            this.w_NOMFILE = alltrim(this.oParentObject.w_OFPATFWP)
          endif
          if g_OFFICE="M"
            result2=STAPDF( FULLPATH(ALLTRIM(this.w_PATHALL))+ALLTRIM(this.w_NOMFILE), "OPEN", " ")
          else
            DIMENSION Args(1)
            this.oManager = createObject("com.sun.star.ServiceManager")
            this.oDesktop = this.oManager.createInstance("com.sun.star.frame.Desktop")
            comarray(this.oDesktop,10)
            && Creo la Struttura di PropertyValues e setto la propriet� per il metodo LoadComponentFromURL 
 Args[1] = this.oManager.Bridge_GetStruct("com.sun.star.beans.PropertyValue") 
 Args[1].name="ReadOnly" 
 Args[1].Value=.F. 
 this.oWriter = this.oDesktop.LoadComponentFromUrl("file:///"+StrTran(ALLTRIM(FULLPATH(this.w_PATHALL)+; 
 ALLTRIM(this.w_NOMFILE)),CHR(92),CHR(47)),"_blank", 0, @Args )
          endif
        else
          AH_ErrorMsg("Il file W.P. � vuoto",48)
        endif
      endif
    else
      AH_ErrorMsg("Il percorso di memorizzazione del modello � vuoto",48)
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='PAR_OFFE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
