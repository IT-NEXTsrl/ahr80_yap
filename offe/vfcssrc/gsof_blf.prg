* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_blf                                                        *
*              Decodifica formula nome doc                                     *
*                                                                              *
*      Author: Iorio Nunzio                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_66]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-18                                                      *
* Last revis.: 2002-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_blf",oParentObject)
return(i_retval)

define class tgsof_blf as StdBatch
  * --- Local variables
  POSIZ = 0
  NAMFIELD = space(15)
  * --- WorkFile variables
  MOD_SEZI_idx=0
  SEZ_OFFE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Decodifica la formula nome documento
    this.POSIZ = rat("+",this.oParentObject.w_FORMULA)
    this.NAMFIELD = substr(this.oParentObject.w_FORMULA,w_POSIZ+1,15)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOD_SEZI'
    this.cWorkTables[2]='SEZ_OFFE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
