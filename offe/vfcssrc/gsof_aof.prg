* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_aof                                                        *
*              Offerte                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_672]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2018-02-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsof_aof
* --- Inizializza Picture
VVL=20*g_PERPVL
VVU=20*g_PERPUL

* --- Fine Area Manuale
return(createobject("tgsof_aof"))

* --- Class definition
define class tgsof_aof as StdForm
  Top    = 3
  Left   = 9

  * --- Standard Properties
  Width  = 807
  Height = 471+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-02-26"
  HelpContextID=176826729
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=220

  * --- Constant Properties
  OFF_ERTE_IDX = 0
  OFF_NOMI_IDX = 0
  MOD_OFFE_IDX = 0
  VALUTE_IDX = 0
  LISTINI_IDX = 0
  PAG_AMEN_IDX = 0
  NOM_CONT_IDX = 0
  AGENTI_IDX = 0
  MODASPED_IDX = 0
  PORTI_IDX = 0
  AZIENDA_IDX = 0
  PAR_OFFE_IDX = 0
  CPUSERS_IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  cFile = "OFF_ERTE"
  cKeySelect = "OFSERIAL"
  cKeyWhere  = "OFSERIAL=this.w_OFSERIAL"
  cKeyWhereODBC = '"OFSERIAL="+cp_ToStrODBC(this.w_OFSERIAL)';

  cKeyWhereODBCqualified = '"OFF_ERTE.OFSERIAL="+cp_ToStrODBC(this.w_OFSERIAL)';

  cPrg = "gsof_aof"
  cComment = "Offerte"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_OFSERIAL = space(10)
  o_OFSERIAL = space(10)
  w_OFCODESE = space(4)
  w_CFUNC = space(10)
  w_OFCODNOM = space(15)
  o_OFCODNOM = space(15)
  w_PAGNOM = space(5)
  w_LISNOM = space(5)
  w_OFCODMOD = space(5)
  o_OFCODMOD = space(5)
  w_TIPCLI = space(15)
  w_CODCLI = space(15)
  w_CATCOM = space(3)
  w_FLSCOR = space(1)
  w_CATSCC = space(5)
  w_OFNUMDOC = 0
  w_OFSERDOC = space(10)
  w_OFDATDOC = ctod('  /  /  ')
  o_OFDATDOC = ctod('  /  /  ')
  w_OFRIFDES = space(45)
  w_OFNUMVER = 0
  w_OFSTATUS = space(1)
  o_OFSTATUS = space(1)
  w_DESMOD = space(35)
  w_NOMDES = space(40)
  w_NOMIND = space(35)
  w_NOMCAP = space(8)
  w_NOMLOC = space(30)
  w_NOMPRO = space(2)
  w_SEZVAR = space(1)
  w_OFDATSCA = ctod('  /  /  ')
  w_OFDATINV = ctod('  /  /  ')
  w_OFDATCHI = ctod('  /  /  ')
  w_OFRIFDOC = space(10)
  w_OFSERPRE = space(10)
  w_VALCLF = space(3)
  w_FORMULA = space(254)
  w_OFTIPFOR = space(1)
  w_OFINISCA = space(2)
  w_OFGIOSCA = 0
  w_OFGIOFIS = 0
  w_PROTECT = space(1)
  w_CODAZI = space(5)
  w_RCODAZI = space(5)
  w_MODOFF = space(1)
  w_PATHMOD = space(254)
  w_PATHARC = space(254)
  w_TIPODOC = space(1)
  w_TIPONOM = space(1)
  w_NCODZON = space(3)
  w_NCODPRF = space(5)
  w_NCODORI = space(5)
  w_NCODGRN = space(5)
  w_NCODPRI = 0
  w_OLDSTAT = space(1)
  w_TIPNOM = space(1)
  w_DTOBS1 = ctod('  /  /  ')
  w_PAGCLI = space(5)
  w_SC1CLI = 0
  w_SC2CLI = 0
  w_LISCLI = space(5)
  w_SC1MOD = 0
  w_SC2MOD = 0
  w_SCPMOD = 0
  w_LISMOD = space(5)
  w_PAGMOD = space(5)
  w_NCODLIN = space(3)
  w_PRIDEF = 0
  w_OLDSCO = space(1)
  w_TIPOWP = space(1)
  w_OFDATAPE = ctod('  /  /  ')
  w_CAUDOC = space(5)
  o_CAUDOC = space(5)
  w_MAGDOC = space(5)
  w_CODMAG = space(5)
  w_PERIODO = space(1)
  w_PRZVAC = space(1)
  w_CODGRN = space(5)
  w_CODAGE = space(5)
  w_NOCODPAG = space(5)
  w_NOTELEFO = space(18)
  w_NONUMCEL = space(18)
  w_OFCODVAL = space(3)
  o_OFCODVAL = space(3)
  w_DECTOT = 0
  w_DECUNI = 0
  w_CALCPICT = 0
  w_CALCPICU = 0
  w_DESVAL = space(35)
  w_OFCODLIS = space(5)
  o_OFCODLIS = space(5)
  w_SCOLIS = space(1)
  w_VALLIS = space(3)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_NUMSCO = 0
  w_IVALIS = space(1)
  w_DESLIS = space(40)
  w_SIMVAL = space(5)
  w_OFCODPAG = space(5)
  o_OFCODPAG = space(5)
  w_DESPAG = space(30)
  w_OFSCOCL1 = 0
  o_OFSCOCL1 = 0
  w_OFSCOCL2 = 0
  o_OFSCOCL2 = 0
  w_OFSCOPAG = 0
  o_OFSCOPAG = 0
  w_TOTRIP = 0
  w_TOTMERCE = 0
  o_TOTMERCE = 0
  w_SCOPAG = 0
  w_VALINC = space(3)
  w_VALIN2 = space(3)
  w_SPEINC = 0
  w_SPEIN2 = 0
  w_DTOBSO = ctod('  /  /  ')
  w_OFSCONTI = 0
  w_MOSPEINC = 0
  o_MOSPEINC = 0
  w_OFFLFOSC = space(1)
  w_OFSPEINC = 0
  w_OFSPEIMB = 0
  w_OFSPETRA = 0
  w_OFIMPOFF = 0
  w_OF__NOTE = space(0)
  w_codval = space(3)
  w_NO__NOTE = space(0)
  w_NULLA = space(1)
  w_OFPATFWP = space(254)
  w_OFPATPDF = space(254)
  w_OFCODCON = space(5)
  o_OFCODCON = space(5)
  w_OFDATRIC = ctod('  /  /  ')
  w_OFCODUTE = 0
  w_OFNUMPRI = 0
  w_OFCODAGE = space(5)
  w_OFFLFOAG = space(1)
  w_CODAG2 = space(5)
  w_OFCODAG2 = space(5)
  w_OFFLFOA2 = space(1)
  w_OFCODPOR = space(1)
  w_OFFLFOPO = space(1)
  w_OFCODSPE = space(3)
  o_OFCODSPE = space(3)
  w_OFFLFOSP = space(1)
  w_DESOPE = space(35)
  w_DESAGE = space(35)
  w_DTOBAGE = ctod('  /  /  ')
  w_PERSON = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DESCAPO = space(35)
  w_DESPORT = space(35)
  w_DESSPE = space(35)
  w_DATOBSO = ctod('  /  /  ')
  w_BRES = .F.
  w_TIPAG2 = space(1)
  w_CODRUO = space(5)
  w_NCTELEFO = space(18)
  w_NCNUMCEL = space(18)
  w_NCTELEF2 = space(18)
  w_NOMFIL = space(30)
  w_OGGETT = space(30)
  w_NOTE = space(30)
  w_SERALL = space(10)
  w_PATALL = space(254)
  w_NAME = space(20)
  w_OPERAT = 0
  w_STAT1 = space(1)
  o_STAT1 = space(1)
  w_STAT2 = space(1)
  o_STAT2 = space(1)
  w_STATO = space(1)
  w_DATAINI = ctod('  /  /  ')
  o_DATAINI = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATAFIN = ctod('  /  /  ')
  o_DATAFIN = ctod('  /  /  ')
  w_ORAFIN = space(2)
  o_ORAFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_SUBJECT = space(254)
  o_SUBJECT = space(254)
  w_CONTINI = space(5)
  w_PRIOINI = 0
  o_PRIOINI = 0
  w_TIPOATINI = space(5)
  w_TIPOATFIN = space(5)
  w_ATSERIAL = space(20)
  w_OGGETTO = space(254)
  w_ATDATINI = ctot('')
  w_ATDATFIN = ctot('')
  w_ATDATINI1 = ctot('')
  w_ATDATFIN1 = ctot('')
  w_NOMINI = space(15)
  o_NOMINI = space(15)
  w_NOMFIN = space(15)
  o_NOMFIN = space(15)
  w_ESITOATI = space(5)
  w_ESITOATF = space(5)
  w_PRIOFIN = 0
  w_DESCPER = space(40)
  w_OFF_SER = space(10)
  w_PRIOINI = 0
  w_FLSPIN = space(1)
  w_CHKSPEIMB = .F.
  w_CHKSPETRA = .F.
  w_MCALST1 = space(5)
  o_MCALST1 = space(5)
  w_MCALSI4 = space(5)
  o_MCALSI4 = space(5)
  w_MCALST4 = space(5)
  o_MCALST4 = space(5)
  w_MCALSI3 = space(5)
  o_MCALSI3 = space(5)
  w_MCALST3 = space(5)
  o_MCALST3 = space(5)
  w_MCALSI2 = space(5)
  o_MCALSI2 = space(5)
  w_MCALSI = space(5)
  w_MCALST2 = space(5)
  o_MCALST2 = space(5)
  w_MCALST = space(5)
  w_MCSIVT1 = space(5)
  w_MCSTVT1 = space(5)
  w_MCSIVT2 = space(5)
  w_MCSTVT2 = space(5)
  w_MCSIVT3 = space(5)
  w_MCSTVT3 = space(5)
  w_CALSPEINC = 0
  w_MCALSI1 = space(5)
  o_MCALSI1 = space(5)
  w_FLSPIM = space(1)
  w_FLSPTR = space(1)
  w_MOSPEIMB = 0
  w_MOSPETRA = 0
  w_OMCALSI = space(5)
  w_OMCALST = space(5)
  w_OMCSIVT1 = space(5)
  w_OMCSTVT1 = space(5)
  w_OMCSIVT2 = space(5)
  w_OMCSTVT2 = space(5)
  w_OMCSIVT3 = space(5)
  w_OMCSTVT3 = space(5)
  w_CODMOD = space(10)
  w_MOALMAIL = space(1)
  w_MOCODVAL = space(3)
  w_EMAIL = space(254)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_OFSERIAL = this.W_OFSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_OFCODESE = this.W_OFCODESE
  op_OFSERDOC = this.W_OFSERDOC
  op_OFNUMDOC = this.W_OFNUMDOC

  * --- Children pointers
  GSOF_MSO = .NULL.
  GSOF_MDO = .NULL.
  GSOF_MAS = .NULL.
  w_ZoomAll = .NULL.
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsof_aof
  *--- Necessaria per anfibizzazione
  w_SBLOCCO = .T.
  w_CHECKDETAIL = .T.
  
  o_OFSTATUS = space (1)
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=7, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'OFF_ERTE','gsof_aof')
    stdPageFrame::Init()
    *set procedure to GSOF_MSO additive
    with this
      .Pages(1).addobject("oPag","tgsof_aofPag1","gsof_aof",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Testata")
      .Pages(1).HelpContextID = 94518
      .Pages(2).addobject("oPag","tgsof_aofPag2","gsof_aof",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio")
      .Pages(2).HelpContextID = 50431887
      .Pages(3).addobject("oPag","tgsof_aofPag3","gsof_aof",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Piede")
      .Pages(3).HelpContextID = 63925002
      .Pages(4).addobject("oPag","tgsof_aofPag4","gsof_aof",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Dati generali")
      .Pages(4).HelpContextID = 169278220
      .Pages(5).addobject("oPag","tgsof_aofPag5","gsof_aof",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Allegati")
      .Pages(5).HelpContextID = 223479663
      .Pages(6).addobject("oPag","tgsof_aofPag6","gsof_aof",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Agenda attivit�")
      .Pages(6).HelpContextID = 130572889
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSOF_MSO
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZoomAll = this.oPgFrm.Pages(5).oPag.ZoomAll
      this.w_ZOOM = this.oPgFrm.Pages(6).oPag.ZOOM
      DoDefault()
    proc Destroy()
      this.w_ZoomAll = .NULL.
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[15]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='MOD_OFFE'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='PAG_AMEN'
    this.cWorkTables[6]='NOM_CONT'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='MODASPED'
    this.cWorkTables[9]='PORTI'
    this.cWorkTables[10]='AZIENDA'
    this.cWorkTables[11]='PAR_OFFE'
    this.cWorkTables[12]='CPUSERS'
    this.cWorkTables[13]='CONTI'
    this.cWorkTables[14]='TIP_DOCU'
    this.cWorkTables[15]='OFF_ERTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(15))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OFF_ERTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OFF_ERTE_IDX,3]
  return

  function CreateChildren()
    this.GSOF_MSO = CREATEOBJECT('stdDynamicChild',this,'GSOF_MSO',this.oPgFrm.Page1.oPag.oLinkPC_1_42)
    this.GSOF_MSO.createrealchild()
    this.GSOF_MDO = CREATEOBJECT('stdDynamicChild',this,'GSOF_MDO',this.oPgFrm.Page2.oPag.oLinkPC_2_16)
    this.GSOF_MDO.createrealchild()
    this.GSOF_MAS = CREATEOBJECT('stdDynamicChild',this,'GSOF_MAS',this.oPgFrm.Page4.oPag.oLinkPC_4_24)
    this.GSOF_MAS.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSOF_MSO)
      this.GSOF_MSO.DestroyChildrenChain()
      this.GSOF_MSO=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_42')
    if !ISNULL(this.GSOF_MDO)
      this.GSOF_MDO.DestroyChildrenChain()
      this.GSOF_MDO=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_16')
    if !ISNULL(this.GSOF_MAS)
      this.GSOF_MAS.DestroyChildrenChain()
      this.GSOF_MAS=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_24')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSOF_MSO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSOF_MDO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSOF_MAS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSOF_MSO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSOF_MDO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSOF_MAS.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSOF_MSO.NewDocument()
    this.GSOF_MDO.NewDocument()
    this.GSOF_MAS.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSOF_MSO.SetKey(;
            .w_OFSERIAL,"OSCODICE";
            )
      this.GSOF_MDO.SetKey(;
            .w_OFSERIAL,"ODSERIAL";
            )
      this.GSOF_MAS.SetKey(;
            .w_OFSERIAL,"OACODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSOF_MSO.ChangeRow(this.cRowID+'      1',1;
             ,.w_OFSERIAL,"OSCODICE";
             )
      .GSOF_MDO.ChangeRow(this.cRowID+'      1',1;
             ,.w_OFSERIAL,"ODSERIAL";
             )
      .GSOF_MAS.ChangeRow(this.cRowID+'      1',1;
             ,.w_OFSERIAL,"OACODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSOF_MSO)
        i_f=.GSOF_MSO.BuildFilter()
        if !(i_f==.GSOF_MSO.cQueryFilter)
          i_fnidx=.GSOF_MSO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSOF_MSO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSOF_MSO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSOF_MSO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSOF_MSO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSOF_MDO)
        i_f=.GSOF_MDO.BuildFilter()
        if !(i_f==.GSOF_MDO.cQueryFilter)
          i_fnidx=.GSOF_MDO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSOF_MDO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSOF_MDO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSOF_MDO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSOF_MDO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSOF_MAS)
        i_f=.GSOF_MAS.BuildFilter()
        if !(i_f==.GSOF_MAS.cQueryFilter)
          i_fnidx=.GSOF_MAS.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSOF_MAS.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSOF_MAS.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSOF_MAS.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSOF_MAS.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_OFSERIAL = NVL(OFSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_3_1_joined
    link_3_1_joined=.f.
    local link_4_6_joined
    link_4_6_joined=.f.
    local link_4_8_joined
    link_4_8_joined=.f.
    local link_4_11_joined
    link_4_11_joined=.f.
    local link_4_15_joined
    link_4_15_joined=.f.
    local link_4_18_joined
    link_4_18_joined=.f.
    local link_4_21_joined
    link_4_21_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from OFF_ERTE where OFSERIAL=KeySet.OFSERIAL
    *
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OFF_ERTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OFF_ERTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OFF_ERTE '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_1_joined=this.AddJoinedLink_3_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_6_joined=this.AddJoinedLink_4_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_8_joined=this.AddJoinedLink_4_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_11_joined=this.AddJoinedLink_4_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_15_joined=this.AddJoinedLink_4_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_18_joined=this.AddJoinedLink_4_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_21_joined=this.AddJoinedLink_4_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'OFSERIAL',this.w_OFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PAGNOM = space(5)
        .w_LISNOM = space(5)
        .w_TIPCLI = space(15)
        .w_CODCLI = space(15)
        .w_CATCOM = space(3)
        .w_FLSCOR = space(1)
        .w_CATSCC = space(5)
        .w_DESMOD = space(35)
        .w_NOMDES = space(40)
        .w_NOMIND = space(35)
        .w_NOMCAP = space(8)
        .w_NOMLOC = space(30)
        .w_NOMPRO = space(2)
        .w_SEZVAR = space(1)
        .w_VALCLF = space(3)
        .w_FORMULA = space(254)
        .w_OFTIPFOR = space(1)
        .w_OFINISCA = space(2)
        .w_OFGIOSCA = 0
        .w_OFGIOFIS = 0
        .w_CODAZI = i_CODAZI
        .w_RCODAZI = i_CODAZI
        .w_MODOFF = space(1)
        .w_PATHMOD = space(254)
        .w_PATHARC = space(254)
        .w_TIPODOC = space(1)
        .w_TIPONOM = space(1)
        .w_NCODZON = space(3)
        .w_NCODPRF = space(5)
        .w_NCODORI = space(5)
        .w_NCODGRN = space(5)
        .w_NCODPRI = 0
        .w_TIPNOM = space(1)
        .w_DTOBS1 = ctod("  /  /  ")
        .w_PAGCLI = space(5)
        .w_SC1CLI = 0
        .w_SC2CLI = 0
        .w_LISCLI = space(5)
        .w_SC1MOD = 0
        .w_SC2MOD = 0
        .w_SCPMOD = 0
        .w_LISMOD = space(5)
        .w_PAGMOD = space(5)
        .w_NCODLIN = space(3)
        .w_PRIDEF = 0
        .w_OLDSCO = space(1)
        .w_TIPOWP = space(1)
        .w_CAUDOC = space(5)
        .w_MAGDOC = space(5)
        .w_PERIODO = space(1)
        .w_PRZVAC = space(1)
        .w_CODGRN = space(5)
        .w_CODAGE = space(5)
        .w_NOCODPAG = space(5)
        .w_NOTELEFO = space(18)
        .w_NONUMCEL = space(18)
        .w_DECTOT = 0
        .w_DECUNI = 0
        .w_DESVAL = space(35)
        .w_SCOLIS = space(1)
        .w_VALLIS = space(3)
        .w_INILIS = ctod("  /  /  ")
        .w_FINLIS = ctod("  /  /  ")
        .w_NUMSCO = 0
        .w_IVALIS = space(1)
        .w_DESLIS = space(40)
        .w_SIMVAL = space(5)
        .w_DESPAG = space(30)
        .w_SCOPAG = 0
        .w_VALINC = space(3)
        .w_VALIN2 = space(3)
        .w_SPEINC = 0
        .w_SPEIN2 = 0
        .w_DTOBSO = ctod("  /  /  ")
        .w_MOSPEINC = 0
        .w_NO__NOTE = space(0)
        .w_CODAG2 = space(5)
        .w_DESOPE = space(35)
        .w_DESAGE = space(35)
        .w_DTOBAGE = ctod("  /  /  ")
        .w_PERSON = space(35)
        .w_DESCAPO = space(35)
        .w_DESPORT = space(35)
        .w_DESSPE = space(35)
        .w_DATOBSO = ctod("  /  /  ")
        .w_BRES = .T.
        .w_TIPAG2 = space(1)
        .w_CODRUO = space(5)
        .w_NCTELEFO = space(18)
        .w_NCNUMCEL = space(18)
        .w_NCTELEF2 = space(18)
        .w_NOMFIL = space(30)
        .w_OGGETT = space(30)
        .w_NOTE = space(30)
        .w_NAME = space(20)
        .w_OPERAT = 0
        .w_STAT1 = 'A'
        .w_STAT2 = 'N'
        .w_DATAINI = ctod("  /  /  ")
        .w_ORAINI = '00'
        .w_MININI = '00'
        .w_DATAFIN = ctod("  /  /  ")
        .w_ORAFIN = '23'
        .w_MINFIN = '59'
        .w_SUBJECT = space(254)
        .w_CONTINI = space(5)
        .w_PRIOINI = 0
        .w_TIPOATINI = space(5)
        .w_TIPOATFIN = space(5)
        .w_ATDATINI = ctot("")
        .w_ATDATFIN = ctot("")
        .w_ATDATINI1 = ctot("")
        .w_ATDATFIN1 = ctot("")
        .w_ESITOATI = space(5)
        .w_ESITOATF = space(5)
        .w_DESCPER = space(40)
        .w_PRIOINI = 0
        .w_FLSPIN = space(1)
        .w_CHKSPEIMB = .T.
        .w_CHKSPETRA = .T.
        .w_MCALST1 = space(5)
        .w_MCALSI4 = space(5)
        .w_MCALST4 = space(5)
        .w_MCALSI3 = space(5)
        .w_MCALST3 = space(5)
        .w_MCALSI2 = space(5)
        .w_MCALST2 = space(5)
        .w_MCSIVT1 = space(5)
        .w_MCSTVT1 = space(5)
        .w_MCSIVT2 = space(5)
        .w_MCSTVT2 = space(5)
        .w_MCSIVT3 = space(5)
        .w_MCSTVT3 = space(5)
        .w_CALSPEINC = 0
        .w_MCALSI1 = space(5)
        .w_FLSPIM = space(1)
        .w_FLSPTR = space(1)
        .w_MOSPEIMB = 0
        .w_MOSPETRA = 0
        .w_OMCALSI = space(5)
        .w_OMCALST = space(5)
        .w_OMCSIVT1 = space(5)
        .w_OMCSTVT1 = space(5)
        .w_OMCSIVT2 = space(5)
        .w_OMCSTVT2 = space(5)
        .w_OMCSIVT3 = space(5)
        .w_OMCSTVT3 = space(5)
        .w_CODMOD = space(10)
        .w_MOALMAIL = space(1)
        .w_MOCODVAL = space(3)
        .w_EMAIL = space(254)
        .w_OFSERIAL = NVL(OFSERIAL,space(10))
        .op_OFSERIAL = .w_OFSERIAL
        .w_OFCODESE = NVL(OFCODESE,space(4))
        .op_OFCODESE = .w_OFCODESE
        .w_CFUNC = UPPER(this.cFunction)
        .w_OFCODNOM = NVL(OFCODNOM,space(15))
          if link_1_4_joined
            this.w_OFCODNOM = NVL(NOCODICE104,NVL(this.w_OFCODNOM,space(15)))
            this.w_NOMDES = NVL(NODESCRI104,space(40))
            this.w_NOMIND = NVL(NOINDIRI104,space(35))
            this.w_NOMCAP = NVL(NO___CAP104,space(8))
            this.w_NOMLOC = NVL(NOLOCALI104,space(30))
            this.w_NOMPRO = NVL(NOPROVIN104,space(2))
            this.w_VALCLF = NVL(NOCODVAL104,space(3))
            this.w_NCODZON = NVL(NOCODZON104,space(3))
            this.w_NCODORI = NVL(NOCODORI104,space(5))
            this.w_NCODGRN = NVL(NOCODGRU104,space(5))
            this.w_NCODPRI = NVL(NOCODPRI104,0)
            this.w_CODCLI = NVL(NOCODCLI104,space(15))
            this.w_TIPNOM = NVL(NOTIPNOM104,space(1))
            this.w_DTOBS1 = NVL(cp_ToDate(NODTOBSO104),ctod("  /  /  "))
            this.w_OFCODUTE = NVL(NOCODOPE104,0)
            this.w_NCODLIN = NVL(NOCODLIN104,space(3))
            this.w_CODAGE = NVL(NOCODAGE104,space(5))
            this.w_TIPCLI = NVL(NOTIPCLI104,space(15))
            this.w_NOCODPAG = NVL(NOCODPAG104,space(5))
            this.w_LISNOM = NVL(NONUMLIS104,space(5))
            this.w_NOTELEFO = NVL(NOTELEFO104,space(18))
            this.w_NONUMCEL = NVL(NONUMCEL104,space(18))
            this.w_EMAIL = NVL(NO_EMAIL104,space(254))
          else
          .link_1_4('Load')
          endif
        .w_OFCODMOD = NVL(OFCODMOD,space(5))
          if link_1_7_joined
            this.w_OFCODMOD = NVL(MOCODICE107,NVL(this.w_OFCODMOD,space(5)))
            this.w_DESMOD = NVL(MODESCRI107,space(35))
            this.w_OFSERDOC = NVL(MOSERDOC107,space(10))
            this.w_SEZVAR = NVL(MOSEZVAR107,space(1))
            this.w_MOSPEINC = NVL(MOSPEINC107,0)
            this.w_MOSPEIMB = NVL(MOSPEIMB107,0)
            this.w_MOSPETRA = NVL(MOSPETRA107,0)
            this.w_FORMULA = NVL(MONOMDOC107,space(254))
            this.w_SC1MOD = NVL(MOSCOCL1107,0)
            this.w_SC2MOD = NVL(MOSCOCL2107,0)
            this.w_SCPMOD = NVL(MOSCOPAG107,0)
            this.w_LISMOD = NVL(MOCODLIS107,space(5))
            this.w_PAGMOD = NVL(MOCODPAG107,space(5))
            this.w_OFCODSPE = NVL(MOCODSPE107,space(3))
            this.w_OFCODPOR = NVL(MOCODPOR107,space(1))
            this.w_OFTIPFOR = NVL(MOTIPFOR107,space(1))
            this.w_OFINISCA = NVL(MOINISCA107,space(2))
            this.w_OFGIOSCA = NVL(MOGIOSCA107,0)
            this.w_OFGIOFIS = NVL(MOGIOFIS107,0)
            this.w_NUMSCO = NVL(MONUMSCO107,0)
            this.w_PATHMOD = NVL(MOPATMOD107,space(254))
            this.w_PATHARC = NVL(MOPATARC107,space(254))
            this.w_TIPODOC = NVL(MOTIPFOR107,space(1))
            this.w_NCODPRF = NVL(MOCODPRI107,space(5))
            this.w_MOCODVAL = NVL(MOCODVAL107,space(3))
            this.w_TIPOWP = NVL(MOTIPOWP107,space(1))
            this.w_CAUDOC = NVL(MOCODCAU107,space(5))
            this.w_PERIODO = NVL(MONUMPER107,space(1))
            this.w_PRZVAC = NVL(MOPRZVAC107,space(1))
            this.w_CODGRN = NVL(MOCODGRN107,space(5))
          else
          .link_1_7('Load')
          endif
          .link_1_9('Load')
        .w_OFNUMDOC = NVL(OFNUMDOC,0)
        .op_OFNUMDOC = .w_OFNUMDOC
        .w_OFSERDOC = NVL(OFSERDOC,space(10))
        .op_OFSERDOC = .w_OFSERDOC
        .w_OFDATDOC = NVL(cp_ToDate(OFDATDOC),ctod("  /  /  "))
        .w_OFRIFDES = NVL(OFRIFDES,space(45))
        .w_OFNUMVER = NVL(OFNUMVER,0)
        .w_OFSTATUS = NVL(OFSTATUS,space(1))
        .w_OFDATSCA = NVL(cp_ToDate(OFDATSCA),ctod("  /  /  "))
        .w_OFDATINV = NVL(cp_ToDate(OFDATINV),ctod("  /  /  "))
        .w_OFDATCHI = NVL(cp_ToDate(OFDATCHI),ctod("  /  /  "))
        .w_OFRIFDOC = NVL(OFRIFDOC,space(10))
        .w_OFSERPRE = NVL(OFSERPRE,space(10))
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .w_PROTECT = IIF (.w_OFSTATUS = 'V','S','N')
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
          .link_1_60('Load')
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .w_OLDSTAT = .w_OFSTATUS
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .w_OFDATAPE = NVL(cp_ToDate(OFDATAPE),ctod("  /  /  "))
          .link_1_93('Load')
        .w_CODMAG = IIF(EMPTY(.w_MAGDOC), g_MAGAZI, .w_MAGDOC)
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .w_OFCODVAL = NVL(OFCODVAL,space(3))
          if link_2_1_joined
            this.w_OFCODVAL = NVL(VACODVAL201,NVL(this.w_OFCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL201,space(35))
            this.w_DECTOT = NVL(VADECTOT201,0)
            this.w_DATOBSO = NVL(cp_ToDate(VADTOBSO201),ctod("  /  /  "))
            this.w_DECUNI = NVL(VADECUNI201,0)
            this.w_SIMVAL = NVL(VASIMVAL201,space(5))
          else
          .link_2_1('Load')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_OFCODLIS = NVL(OFCODLIS,space(5))
          if link_2_8_joined
            this.w_OFCODLIS = NVL(LSCODLIS208,NVL(this.w_OFCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS208,space(40))
            this.w_IVALIS = NVL(LSIVALIS208,space(1))
            this.w_SCOLIS = NVL(LSFLSCON208,space(1))
            this.w_VALLIS = NVL(LSVALLIS208,space(3))
            this.w_INILIS = NVL(cp_ToDate(LSDTINVA208),ctod("  /  /  "))
            this.w_FINLIS = NVL(cp_ToDate(LSDTOBSO208),ctod("  /  /  "))
          else
          .link_2_8('Load')
          endif
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate()
        .w_OFCODPAG = NVL(OFCODPAG,space(5))
          if link_3_1_joined
            this.w_OFCODPAG = NVL(PACODICE301,NVL(this.w_OFCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI301,space(30))
            this.w_DTOBSO = NVL(cp_ToDate(PADTOBSO301),ctod("  /  /  "))
            this.w_SCOPAG = NVL(PASCONTO301,0)
            this.w_VALINC = NVL(PAVALINC301,space(3))
            this.w_SPEINC = NVL(PASPEINC301,0)
            this.w_VALIN2 = NVL(PAVALIN2301,space(3))
            this.w_SPEIN2 = NVL(PASPEIN2301,0)
          else
          .link_3_1('Load')
          endif
        .w_OFSCOCL1 = NVL(OFSCOCL1,0)
        .w_OFSCOCL2 = NVL(OFSCOCL2,0)
        .w_OFSCOPAG = NVL(OFSCOPAG,0)
        .w_TOTRIP = IIF( Upper(this.GSOF_MDO.class)='STDDYNAMICCHILD' , 0 , this.gsof_mdo .w_TOTRIPD )
        .w_TOTMERCE = IIF( Upper(this.GSOF_MDO.class)='STDDYNAMICCHILD' , 0 , this.gsof_mdo .w_TOTMERCE )
        .w_OFSCONTI = NVL(OFSCONTI,0)
        .w_OFFLFOSC = NVL(OFFLFOSC,space(1))
        .w_OFSPEINC = NVL(OFSPEINC,0)
        .w_OFSPEIMB = NVL(OFSPEIMB,0)
        .w_OFSPETRA = NVL(OFSPETRA,0)
        .w_OFIMPOFF = NVL(OFIMPOFF,0)
        .w_OF__NOTE = NVL(OF__NOTE,space(0))
        .w_codval = .w_OFCODVAL
        .oPgFrm.Page3.oPag.oObj_3_36.Calculate()
        .w_NULLA = ' '
        .w_OFPATFWP = NVL(OFPATFWP,space(254))
        .w_OFPATPDF = NVL(OFPATPDF,space(254))
        .w_OFCODCON = NVL(OFCODCON,space(5))
          if link_4_6_joined
            this.w_OFCODCON = NVL(NCCODCON406,NVL(this.w_OFCODCON,space(5)))
            this.w_PERSON = NVL(NCPERSON406,space(35))
            this.w_CODRUO = NVL(NCCODRUO406,space(5))
            this.w_NCTELEFO = NVL(NCTELEFO406,space(18))
            this.w_NCNUMCEL = NVL(NCNUMCEL406,space(18))
            this.w_NCTELEF2 = NVL(NCTELEF2406,space(18))
          else
          .link_4_6('Load')
          endif
        .w_OFDATRIC = NVL(cp_ToDate(OFDATRIC),ctod("  /  /  "))
        .w_OFCODUTE = NVL(OFCODUTE,0)
          if link_4_8_joined
            this.w_OFCODUTE = NVL(CODE408,NVL(this.w_OFCODUTE,0))
            this.w_DESOPE = NVL(NAME408,space(35))
          else
          .link_4_8('Load')
          endif
        .w_OFNUMPRI = NVL(OFNUMPRI,0)
        .w_OFCODAGE = NVL(OFCODAGE,space(5))
          if link_4_11_joined
            this.w_OFCODAGE = NVL(AGCODAGE411,NVL(this.w_OFCODAGE,space(5)))
            this.w_DESAGE = NVL(AGDESAGE411,space(35))
            this.w_DTOBAGE = NVL(cp_ToDate(AGDTOBSO411),ctod("  /  /  "))
            this.w_CODAG2 = NVL(AGCZOAGE411,space(5))
          else
          .link_4_11('Load')
          endif
        .w_OFFLFOAG = NVL(OFFLFOAG,space(1))
        .w_OFCODAG2 = NVL(OFCODAG2,space(5))
          if link_4_15_joined
            this.w_OFCODAG2 = NVL(AGCODAGE415,NVL(this.w_OFCODAG2,space(5)))
            this.w_DESCAPO = NVL(AGDESAGE415,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(AGDTOBSO415),ctod("  /  /  "))
            this.w_TIPAG2 = NVL(AGTIPAGE415,space(1))
          else
          .link_4_15('Load')
          endif
        .w_OFFLFOA2 = NVL(OFFLFOA2,space(1))
        .w_OFCODPOR = NVL(OFCODPOR,space(1))
          if link_4_18_joined
            this.w_OFCODPOR = NVL(POCODPOR418,NVL(this.w_OFCODPOR,space(1)))
            this.w_DESPORT = NVL(PODESPOR418,space(35))
          else
          .link_4_18('Load')
          endif
        .w_OFFLFOPO = NVL(OFFLFOPO,space(1))
        .w_OFCODSPE = NVL(OFCODSPE,space(3))
          if link_4_21_joined
            this.w_OFCODSPE = NVL(SPCODSPE421,NVL(this.w_OFCODSPE,space(3)))
            this.w_DESSPE = NVL(SPDESSPE421,space(35))
            this.w_MCALSI3 = NVL(SPMCCODI421,space(5))
            this.w_MCALST3 = NVL(SPMCCODT421,space(5))
          else
          .link_4_21('Load')
          endif
        .w_OFFLFOSP = NVL(OFFLFOSP,space(1))
        .w_OBTEST = i_datsys
        .oPgFrm.Page4.oPag.oObj_4_39.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_41.Calculate()
        .oPgFrm.Page5.oPag.ZoomAll.Calculate()
        .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
        .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
        .oPgFrm.Page6.oPag.ZOOM.Calculate()
          .link_6_6('Load')
        .w_STATO = IIF(.cFunction<>'Load',IIF(g_AGEN<>'S', .w_STAT1,.w_STAT2),'X')
          .link_6_19('Load')
        .w_ATSERIAL = .w_ZOOM.GetVar('ATSERIAL')
        .w_OGGETTO = '%'+ALLTRIM(.w_SUBJECT)
        .w_NOMINI = .w_OFCODNOM
        .w_NOMFIN = .w_OFCODNOM
        .w_PRIOFIN = .w_PRIOINI
        .w_OFF_SER = .w_OFSERIAL
        .w_MCALSI = IIF(.w_MOSPEIMB<>0, '', ICASE( NOT EMPTY( nvl(.w_MCALSI3, space(5)) ), .w_MCALSI3, NOT EMPTY(nvl( .w_MCALSI2 , space(5))), .w_MCALSI2, .w_MCALSI1) )
        .w_MCALST = IIF(.w_MOSPETRA<>0, '', ICASE( NOT EMPTY( nvl(.w_MCALST3, space(5)) ), .w_MCALST3, NOT EMPTY( nvl(.w_MCALST2, space(5)) ), .w_MCALST2, .w_MCALST1) )
        .oPgFrm.Page2.oPag.oObj_2_22.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'OFF_ERTE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_5.enabled = this.oPgFrm.Page5.oPag.oBtn_5_5.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_7.enabled = this.oPgFrm.Page5.oPag.oBtn_5_7.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_8.enabled = this.oPgFrm.Page5.oPag.oBtn_5_8.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_9.enabled = this.oPgFrm.Page5.oPag.oBtn_5_9.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_10.enabled = this.oPgFrm.Page5.oPag.oBtn_5_10.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_2.enabled = this.oPgFrm.Page6.oPag.oBtn_6_2.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_3.enabled = this.oPgFrm.Page6.oPag.oBtn_6_3.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_4.enabled = this.oPgFrm.Page6.oPag.oBtn_6_4.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_22.enabled = this.oPgFrm.Page6.oPag.oBtn_6_22.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_48.enabled = this.oPgFrm.Page3.oPag.oBtn_3_48.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsof_aof
    this.o_OFSTATUS = this.w_OFSTATUS
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OFSERIAL = space(10)
      .w_OFCODESE = space(4)
      .w_CFUNC = space(10)
      .w_OFCODNOM = space(15)
      .w_PAGNOM = space(5)
      .w_LISNOM = space(5)
      .w_OFCODMOD = space(5)
      .w_TIPCLI = space(15)
      .w_CODCLI = space(15)
      .w_CATCOM = space(3)
      .w_FLSCOR = space(1)
      .w_CATSCC = space(5)
      .w_OFNUMDOC = 0
      .w_OFSERDOC = space(10)
      .w_OFDATDOC = ctod("  /  /  ")
      .w_OFRIFDES = space(45)
      .w_OFNUMVER = 0
      .w_OFSTATUS = space(1)
      .w_DESMOD = space(35)
      .w_NOMDES = space(40)
      .w_NOMIND = space(35)
      .w_NOMCAP = space(8)
      .w_NOMLOC = space(30)
      .w_NOMPRO = space(2)
      .w_SEZVAR = space(1)
      .w_OFDATSCA = ctod("  /  /  ")
      .w_OFDATINV = ctod("  /  /  ")
      .w_OFDATCHI = ctod("  /  /  ")
      .w_OFRIFDOC = space(10)
      .w_OFSERPRE = space(10)
      .w_VALCLF = space(3)
      .w_FORMULA = space(254)
      .w_OFTIPFOR = space(1)
      .w_OFINISCA = space(2)
      .w_OFGIOSCA = 0
      .w_OFGIOFIS = 0
      .w_PROTECT = space(1)
      .w_CODAZI = space(5)
      .w_RCODAZI = space(5)
      .w_MODOFF = space(1)
      .w_PATHMOD = space(254)
      .w_PATHARC = space(254)
      .w_TIPODOC = space(1)
      .w_TIPONOM = space(1)
      .w_NCODZON = space(3)
      .w_NCODPRF = space(5)
      .w_NCODORI = space(5)
      .w_NCODGRN = space(5)
      .w_NCODPRI = 0
      .w_OLDSTAT = space(1)
      .w_TIPNOM = space(1)
      .w_DTOBS1 = ctod("  /  /  ")
      .w_PAGCLI = space(5)
      .w_SC1CLI = 0
      .w_SC2CLI = 0
      .w_LISCLI = space(5)
      .w_SC1MOD = 0
      .w_SC2MOD = 0
      .w_SCPMOD = 0
      .w_LISMOD = space(5)
      .w_PAGMOD = space(5)
      .w_NCODLIN = space(3)
      .w_PRIDEF = 0
      .w_OLDSCO = space(1)
      .w_TIPOWP = space(1)
      .w_OFDATAPE = ctod("  /  /  ")
      .w_CAUDOC = space(5)
      .w_MAGDOC = space(5)
      .w_CODMAG = space(5)
      .w_PERIODO = space(1)
      .w_PRZVAC = space(1)
      .w_CODGRN = space(5)
      .w_CODAGE = space(5)
      .w_NOCODPAG = space(5)
      .w_NOTELEFO = space(18)
      .w_NONUMCEL = space(18)
      .w_OFCODVAL = space(3)
      .w_DECTOT = 0
      .w_DECUNI = 0
      .w_CALCPICT = 0
      .w_CALCPICU = 0
      .w_DESVAL = space(35)
      .w_OFCODLIS = space(5)
      .w_SCOLIS = space(1)
      .w_VALLIS = space(3)
      .w_INILIS = ctod("  /  /  ")
      .w_FINLIS = ctod("  /  /  ")
      .w_NUMSCO = 0
      .w_IVALIS = space(1)
      .w_DESLIS = space(40)
      .w_SIMVAL = space(5)
      .w_OFCODPAG = space(5)
      .w_DESPAG = space(30)
      .w_OFSCOCL1 = 0
      .w_OFSCOCL2 = 0
      .w_OFSCOPAG = 0
      .w_TOTRIP = 0
      .w_TOTMERCE = 0
      .w_SCOPAG = 0
      .w_VALINC = space(3)
      .w_VALIN2 = space(3)
      .w_SPEINC = 0
      .w_SPEIN2 = 0
      .w_DTOBSO = ctod("  /  /  ")
      .w_OFSCONTI = 0
      .w_MOSPEINC = 0
      .w_OFFLFOSC = space(1)
      .w_OFSPEINC = 0
      .w_OFSPEIMB = 0
      .w_OFSPETRA = 0
      .w_OFIMPOFF = 0
      .w_OF__NOTE = space(0)
      .w_codval = space(3)
      .w_NO__NOTE = space(0)
      .w_NULLA = space(1)
      .w_OFPATFWP = space(254)
      .w_OFPATPDF = space(254)
      .w_OFCODCON = space(5)
      .w_OFDATRIC = ctod("  /  /  ")
      .w_OFCODUTE = 0
      .w_OFNUMPRI = 0
      .w_OFCODAGE = space(5)
      .w_OFFLFOAG = space(1)
      .w_CODAG2 = space(5)
      .w_OFCODAG2 = space(5)
      .w_OFFLFOA2 = space(1)
      .w_OFCODPOR = space(1)
      .w_OFFLFOPO = space(1)
      .w_OFCODSPE = space(3)
      .w_OFFLFOSP = space(1)
      .w_DESOPE = space(35)
      .w_DESAGE = space(35)
      .w_DTOBAGE = ctod("  /  /  ")
      .w_PERSON = space(35)
      .w_OBTEST = ctod("  /  /  ")
      .w_DESCAPO = space(35)
      .w_DESPORT = space(35)
      .w_DESSPE = space(35)
      .w_DATOBSO = ctod("  /  /  ")
      .w_BRES = .f.
      .w_TIPAG2 = space(1)
      .w_CODRUO = space(5)
      .w_NCTELEFO = space(18)
      .w_NCNUMCEL = space(18)
      .w_NCTELEF2 = space(18)
      .w_NOMFIL = space(30)
      .w_OGGETT = space(30)
      .w_NOTE = space(30)
      .w_SERALL = space(10)
      .w_PATALL = space(254)
      .w_NAME = space(20)
      .w_OPERAT = 0
      .w_STAT1 = space(1)
      .w_STAT2 = space(1)
      .w_STATO = space(1)
      .w_DATAINI = ctod("  /  /  ")
      .w_ORAINI = space(2)
      .w_MININI = space(2)
      .w_DATAFIN = ctod("  /  /  ")
      .w_ORAFIN = space(2)
      .w_MINFIN = space(2)
      .w_SUBJECT = space(254)
      .w_CONTINI = space(5)
      .w_PRIOINI = 0
      .w_TIPOATINI = space(5)
      .w_TIPOATFIN = space(5)
      .w_ATSERIAL = space(20)
      .w_OGGETTO = space(254)
      .w_ATDATINI = ctot("")
      .w_ATDATFIN = ctot("")
      .w_ATDATINI1 = ctot("")
      .w_ATDATFIN1 = ctot("")
      .w_NOMINI = space(15)
      .w_NOMFIN = space(15)
      .w_ESITOATI = space(5)
      .w_ESITOATF = space(5)
      .w_PRIOFIN = 0
      .w_DESCPER = space(40)
      .w_OFF_SER = space(10)
      .w_PRIOINI = 0
      .w_FLSPIN = space(1)
      .w_CHKSPEIMB = .f.
      .w_CHKSPETRA = .f.
      .w_MCALST1 = space(5)
      .w_MCALSI4 = space(5)
      .w_MCALST4 = space(5)
      .w_MCALSI3 = space(5)
      .w_MCALST3 = space(5)
      .w_MCALSI2 = space(5)
      .w_MCALSI = space(5)
      .w_MCALST2 = space(5)
      .w_MCALST = space(5)
      .w_MCSIVT1 = space(5)
      .w_MCSTVT1 = space(5)
      .w_MCSIVT2 = space(5)
      .w_MCSTVT2 = space(5)
      .w_MCSIVT3 = space(5)
      .w_MCSTVT3 = space(5)
      .w_CALSPEINC = 0
      .w_MCALSI1 = space(5)
      .w_FLSPIM = space(1)
      .w_FLSPTR = space(1)
      .w_MOSPEIMB = 0
      .w_MOSPETRA = 0
      .w_OMCALSI = space(5)
      .w_OMCALST = space(5)
      .w_OMCSIVT1 = space(5)
      .w_OMCSTVT1 = space(5)
      .w_OMCSIVT2 = space(5)
      .w_OMCSTVT2 = space(5)
      .w_OMCSIVT3 = space(5)
      .w_OMCSTVT3 = space(5)
      .w_CODMOD = space(10)
      .w_MOALMAIL = space(1)
      .w_MOCODVAL = space(3)
      .w_EMAIL = space(254)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_OFCODESE = g_CODESE
        .w_CFUNC = UPPER(this.cFunction)
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_OFCODNOM))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,7,.f.)
          if not(empty(.w_OFCODMOD))
          .link_1_7('Full')
          endif
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_CODCLI))
          .link_1_9('Full')
          endif
          .DoRTCalc(10,14,.f.)
        .w_OFDATDOC = i_datsys
          .DoRTCalc(16,16,.f.)
        .w_OFNUMVER = iif (.w_OFNUMVER = 0,1,.w_OFNUMVER)
        .w_OFSTATUS = 'I'
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
          .DoRTCalc(19,36,.f.)
        .w_PROTECT = IIF (.w_OFSTATUS = 'V','S','N')
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .w_CODAZI = i_CODAZI
        .w_RCODAZI = i_CODAZI
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_RCODAZI))
          .link_1_60('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
          .DoRTCalc(40,49,.f.)
        .w_OLDSTAT = .w_OFSTATUS
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
          .DoRTCalc(51,65,.f.)
        .w_OFDATAPE = IIF(.w_OFNUMVER=1, .w_OFDATDOC, .w_OFDATAPE)
        .DoRTCalc(67,67,.f.)
          if not(empty(.w_CAUDOC))
          .link_1_93('Full')
          endif
          .DoRTCalc(68,68,.f.)
        .w_CODMAG = IIF(EMPTY(.w_MAGDOC), g_MAGAZI, .w_MAGDOC)
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
          .DoRTCalc(70,76,.f.)
        .w_OFCODVAL = iif(empty(.w_OFCODMOD),g_PERVAL,.w_MOCODVAL)
        .DoRTCalc(77,77,.f.)
          if not(empty(.w_OFCODVAL))
          .link_2_1('Full')
          endif
          .DoRTCalc(78,79,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
          .DoRTCalc(82,82,.f.)
        .w_OFCODLIS = icase( EMPTY(.w_LISNOM) AND NOT EMPTY(.w_LISMOD),.w_LISMOD,.w_OFCODLIS)
        .DoRTCalc(83,83,.f.)
          if not(empty(.w_OFCODLIS))
          .link_2_8('Full')
          endif
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate()
          .DoRTCalc(84,91,.f.)
        .w_OFCODPAG = IIF(EMPTY(.w_NOCODPAG) AND NOT EMPTY(.w_PAGMOD), .w_PAGMOD, .w_OFCODPAG)
        .DoRTCalc(92,92,.f.)
          if not(empty(.w_OFCODPAG))
          .link_3_1('Full')
          endif
          .DoRTCalc(93,94,.f.)
        .w_OFSCOCL2 = IIF(.w_OFSCOCL1=0, 0, .w_OFSCOCL2)
          .DoRTCalc(96,96,.f.)
        .w_TOTRIP = IIF( Upper(this.GSOF_MDO.class)='STDDYNAMICCHILD' , 0 , this.gsof_mdo .w_TOTRIPD )
        .w_TOTMERCE = IIF( Upper(this.GSOF_MDO.class)='STDDYNAMICCHILD' , 0 , this.gsof_mdo .w_TOTMERCE )
          .DoRTCalc(99,104,.f.)
        .w_OFSCONTI = IIF(.w_OFFLFOSC='S',.w_OFSCONTI,Calsco(.w_TOTMERCE, .w_OFSCOCL1, .w_OFSCOCL2, .w_OFSCOPAG, .w_DECTOT))
          .DoRTCalc(106,107,.f.)
        .w_OFSPEINC = IIF(EMPTY(.w_CALSPEINC), .w_MOSPEINC, .w_CALSPEINC)
        .w_OFSPEIMB = .w_MOSPEIMB
        .w_OFSPETRA = .w_MOSPETRA
        .w_OFIMPOFF = .w_TOTRIP+.w_OFSCONTI+.w_OFSPEINC+.w_OFSPEIMB+.w_OFSPETRA
          .DoRTCalc(112,112,.f.)
        .w_codval = .w_OFCODVAL
        .oPgFrm.Page3.oPag.oObj_3_36.Calculate()
          .DoRTCalc(114,114,.f.)
        .w_NULLA = ' '
          .DoRTCalc(116,117,.f.)
        .w_OFCODCON = Space(5)
        .DoRTCalc(118,118,.f.)
          if not(empty(.w_OFCODCON))
          .link_4_6('Full')
          endif
          .DoRTCalc(119,119,.f.)
        .w_OFCODUTE = IIF(EMPTY(.w_OFCODUTE), i_Codute, .w_OFCODUTE)
        .DoRTCalc(120,120,.f.)
          if not(empty(.w_OFCODUTE))
          .link_4_8('Full')
          endif
        .w_OFNUMPRI = .w_PRIDEF
        .DoRTCalc(122,122,.f.)
          if not(empty(.w_OFCODAGE))
          .link_4_11('Full')
          endif
        .w_OFFLFOAG = 'N'
        .DoRTCalc(124,125,.f.)
          if not(empty(.w_OFCODAG2))
          .link_4_15('Full')
          endif
        .w_OFFLFOA2 = 'N'
        .DoRTCalc(127,127,.f.)
          if not(empty(.w_OFCODPOR))
          .link_4_18('Full')
          endif
        .w_OFFLFOPO = 'N'
        .DoRTCalc(129,129,.f.)
          if not(empty(.w_OFCODSPE))
          .link_4_21('Full')
          endif
        .w_OFFLFOSP = 'N'
          .DoRTCalc(131,134,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page4.oPag.oObj_4_39.Calculate()
          .DoRTCalc(136,139,.f.)
        .w_BRES = .T.
        .oPgFrm.Page4.oPag.oObj_4_41.Calculate()
        .oPgFrm.Page5.oPag.ZoomAll.Calculate()
          .DoRTCalc(141,148,.f.)
        .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
        .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
        .oPgFrm.Page6.oPag.ZOOM.Calculate()
          .DoRTCalc(151,151,.f.)
        .w_OPERAT = 0
        .DoRTCalc(152,152,.f.)
          if not(empty(.w_OPERAT))
          .link_6_6('Full')
          endif
        .w_STAT1 = 'A'
        .w_STAT2 = 'N'
        .w_STATO = IIF(.cFunction<>'Load',IIF(g_AGEN<>'S', .w_STAT1,.w_STAT2),'X')
          .DoRTCalc(156,156,.f.)
        .w_ORAINI = '00'
        .w_MININI = '00'
          .DoRTCalc(159,159,.f.)
        .w_ORAFIN = '23'
        .w_MINFIN = '59'
        .DoRTCalc(162,163,.f.)
          if not(empty(.w_CONTINI))
          .link_6_19('Full')
          endif
          .DoRTCalc(164,166,.f.)
        .w_ATSERIAL = .w_ZOOM.GetVar('ATSERIAL')
        .w_OGGETTO = '%'+ALLTRIM(.w_SUBJECT)
          .DoRTCalc(169,172,.f.)
        .w_NOMINI = .w_OFCODNOM
        .w_NOMFIN = .w_OFCODNOM
          .DoRTCalc(175,176,.f.)
        .w_PRIOFIN = .w_PRIOINI
          .DoRTCalc(178,178,.f.)
        .w_OFF_SER = .w_OFSERIAL
        .w_PRIOINI = 0
          .DoRTCalc(181,181,.f.)
        .w_CHKSPEIMB = .T.
        .w_CHKSPETRA = .T.
          .DoRTCalc(184,189,.f.)
        .w_MCALSI = IIF(.w_MOSPEIMB<>0, '', ICASE( NOT EMPTY( nvl(.w_MCALSI3, space(5)) ), .w_MCALSI3, NOT EMPTY(nvl( .w_MCALSI2 , space(5))), .w_MCALSI2, .w_MCALSI1) )
          .DoRTCalc(191,191,.f.)
        .w_MCALST = IIF(.w_MOSPETRA<>0, '', ICASE( NOT EMPTY( nvl(.w_MCALST3, space(5)) ), .w_MCALST3, NOT EMPTY( nvl(.w_MCALST2, space(5)) ), .w_MCALST2, .w_MCALST1) )
        .oPgFrm.Page2.oPag.oObj_2_22.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'OFF_ERTE')
    this.DoRTCalc(193,220,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_5.enabled = this.oPgFrm.Page5.oPag.oBtn_5_5.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_7.enabled = this.oPgFrm.Page5.oPag.oBtn_5_7.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_8.enabled = this.oPgFrm.Page5.oPag.oBtn_5_8.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_9.enabled = this.oPgFrm.Page5.oPag.oBtn_5_9.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_10.enabled = this.oPgFrm.Page5.oPag.oBtn_5_10.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_2.enabled = this.oPgFrm.Page6.oPag.oBtn_6_2.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_3.enabled = this.oPgFrm.Page6.oPag.oBtn_6_3.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_4.enabled = this.oPgFrm.Page6.oPag.oBtn_6_4.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_22.enabled = this.oPgFrm.Page6.oPag.oBtn_6_22.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_48.enabled = this.oPgFrm.Page3.oPag.oBtn_3_48.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsof_aof
    * --- Refresh dello Zoom Allegati
    if this.cFunction='Load'
       this.NotifyEvent('Calcola')
    endif
    
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEOFFERT","i_codazi,w_OFSERIAL")
      cp_AskTableProg(this,i_nConn,"PROFFERT","i_codazi,w_OFCODESE,w_OFSERDOC,w_OFNUMDOC")
      .op_codazi = .w_codazi
      .op_OFSERIAL = .w_OFSERIAL
      .op_codazi = .w_codazi
      .op_OFCODESE = .w_OFCODESE
      .op_OFSERDOC = .w_OFSERDOC
      .op_OFNUMDOC = .w_OFNUMDOC
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oOFCODNOM_1_4.enabled = i_bVal
      .Page1.oPag.oOFCODMOD_1_7.enabled = i_bVal
      .Page1.oPag.oOFNUMDOC_1_14.enabled = i_bVal
      .Page1.oPag.oOFSERDOC_1_16.enabled = i_bVal
      .Page1.oPag.oOFDATDOC_1_18.enabled = i_bVal
      .Page1.oPag.oOFRIFDES_1_20.enabled = i_bVal
      .Page1.oPag.oOFSTATUS_1_23.enabled = i_bVal
      .Page1.oPag.oOFDATSCA_1_39.enabled = i_bVal
      .Page1.oPag.oOFDATINV_1_40.enabled = i_bVal
      .Page2.oPag.oOFCODVAL_2_1.enabled = i_bVal
      .Page2.oPag.oOFCODLIS_2_8.enabled = i_bVal
      .Page3.oPag.oOFCODPAG_3_1.enabled = i_bVal
      .Page3.oPag.oOFSCOCL1_3_3.enabled = i_bVal
      .Page3.oPag.oOFSCOCL2_3_4.enabled = i_bVal
      .Page3.oPag.oOFSCOPAG_3_5.enabled = i_bVal
      .Page3.oPag.oOFSCONTI_3_20.enabled = i_bVal
      .Page3.oPag.oOFFLFOSC_3_22.enabled = i_bVal
      .Page3.oPag.oOFSPEIMB_3_24.enabled = i_bVal
      .Page3.oPag.oOFSPETRA_3_25.enabled = i_bVal
      .Page3.oPag.oOF__NOTE_3_28.enabled = i_bVal
      .Page4.oPag.oOFCODCON_4_6.enabled = i_bVal
      .Page4.oPag.oOFDATRIC_4_7.enabled = i_bVal
      .Page4.oPag.oOFCODUTE_4_8.enabled = i_bVal
      .Page4.oPag.oOFNUMPRI_4_10.enabled = i_bVal
      .Page4.oPag.oOFCODAGE_4_11.enabled = i_bVal
      .Page4.oPag.oOFFLFOAG_4_12.enabled = i_bVal
      .Page4.oPag.oOFCODAG2_4_15.enabled = i_bVal
      .Page4.oPag.oOFFLFOA2_4_16.enabled = i_bVal
      .Page4.oPag.oOFCODPOR_4_18.enabled = i_bVal
      .Page4.oPag.oOFFLFOPO_4_19.enabled = i_bVal
      .Page4.oPag.oOFCODSPE_4_21.enabled = i_bVal
      .Page4.oPag.oOFFLFOSP_4_22.enabled = i_bVal
      .Page5.oPag.oNOMFIL_5_1.enabled = i_bVal
      .Page5.oPag.oOGGETT_5_2.enabled = i_bVal
      .Page5.oPag.oNOTE_5_3.enabled = i_bVal
      .Page6.oPag.oOPERAT_6_6.enabled = i_bVal
      .Page6.oPag.oSTAT1_6_7.enabled = i_bVal
      .Page6.oPag.oSTAT2_6_8.enabled = i_bVal
      .Page6.oPag.oDATAINI_6_10.enabled = i_bVal
      .Page6.oPag.oORAINI_6_11.enabled = i_bVal
      .Page6.oPag.oMININI_6_12.enabled = i_bVal
      .Page6.oPag.oDATAFIN_6_14.enabled = i_bVal
      .Page6.oPag.oORAFIN_6_15.enabled = i_bVal
      .Page6.oPag.oMINFIN_6_16.enabled = i_bVal
      .Page6.oPag.oSUBJECT_6_18.enabled = i_bVal
      .Page6.oPag.oCONTINI_6_19.enabled = i_bVal
      .Page6.oPag.oPRIOINI_6_20.enabled = i_bVal
      .Page6.oPag.oPRIOINI_6_52.enabled = i_bVal
      .Page1.oPag.oBtn_1_24.enabled = .Page1.oPag.oBtn_1_24.mCond()
      .Page1.oPag.oBtn_1_25.enabled = .Page1.oPag.oBtn_1_25.mCond()
      .Page1.oPag.oBtn_1_26.enabled = .Page1.oPag.oBtn_1_26.mCond()
      .Page1.oPag.oBtn_1_46.enabled = .Page1.oPag.oBtn_1_46.mCond()
      .Page1.oPag.oBtn_1_47.enabled = .Page1.oPag.oBtn_1_47.mCond()
      .Page1.oPag.oBtn_1_48.enabled = .Page1.oPag.oBtn_1_48.mCond()
      .Page2.oPag.oBtn_2_18.enabled = i_bVal
      .Page2.oPag.oBtn_2_19.enabled = i_bVal
      .Page3.oPag.oBtn_3_33.enabled = i_bVal
      .Page5.oPag.oBtn_5_5.enabled = .Page5.oPag.oBtn_5_5.mCond()
      .Page5.oPag.oBtn_5_7.enabled = .Page5.oPag.oBtn_5_7.mCond()
      .Page5.oPag.oBtn_5_8.enabled = .Page5.oPag.oBtn_5_8.mCond()
      .Page5.oPag.oBtn_5_9.enabled = .Page5.oPag.oBtn_5_9.mCond()
      .Page5.oPag.oBtn_5_10.enabled = .Page5.oPag.oBtn_5_10.mCond()
      .Page6.oPag.oBtn_6_2.enabled = .Page6.oPag.oBtn_6_2.mCond()
      .Page6.oPag.oBtn_6_3.enabled = .Page6.oPag.oBtn_6_3.mCond()
      .Page6.oPag.oBtn_6_4.enabled = .Page6.oPag.oBtn_6_4.mCond()
      .Page6.oPag.oBtn_6_22.enabled = .Page6.oPag.oBtn_6_22.mCond()
      .Page3.oPag.oBtn_3_47.enabled = i_bVal
      .Page3.oPag.oBtn_3_48.enabled = .Page3.oPag.oBtn_3_48.mCond()
      .Page1.oPag.oObj_1_50.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.oObj_1_58.enabled = i_bVal
      .Page1.oPag.oObj_1_66.enabled = i_bVal
      .Page1.oPag.oObj_1_73.enabled = i_bVal
      .Page1.oPag.oObj_1_74.enabled = i_bVal
      .Page1.oPag.oObj_1_75.enabled = i_bVal
      .Page1.oPag.oObj_1_76.enabled = i_bVal
      .Page1.oPag.oObj_1_96.enabled = i_bVal
      .Page2.oPag.oObj_2_20.enabled = i_bVal
      .Page3.oPag.oObj_3_36.enabled = i_bVal
      .Page4.oPag.oObj_4_39.enabled = i_bVal
      .Page4.oPag.oObj_4_41.enabled = i_bVal
      .Page2.oPag.oObj_2_22.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oOFCODNOM_1_4.enabled = .t.
        .Page1.oPag.oOFNUMDOC_1_14.enabled = .t.
        .Page1.oPag.oOFDATDOC_1_18.enabled = .t.
        .Page5.oPag.oNOMFIL_5_1.enabled = .t.
        .Page5.oPag.oOGGETT_5_2.enabled = .t.
        .Page5.oPag.oNOTE_5_3.enabled = .t.
        .Page6.oPag.oOPERAT_6_6.enabled = .t.
        .Page6.oPag.oSTAT1_6_7.enabled = .t.
        .Page6.oPag.oSTAT2_6_8.enabled = .t.
        .Page6.oPag.oDATAINI_6_10.enabled = .t.
        .Page6.oPag.oORAINI_6_11.enabled = .t.
        .Page6.oPag.oMININI_6_12.enabled = .t.
        .Page6.oPag.oDATAFIN_6_14.enabled = .t.
        .Page6.oPag.oORAFIN_6_15.enabled = .t.
        .Page6.oPag.oMINFIN_6_16.enabled = .t.
        .Page6.oPag.oSUBJECT_6_18.enabled = .t.
        .Page6.oPag.oCONTINI_6_19.enabled = .t.
        .Page6.oPag.oPRIOINI_6_20.enabled = .t.
        .Page6.oPag.oPRIOINI_6_52.enabled = .t.
      endif
    endwith
    this.GSOF_MSO.SetStatus(i_cOp)
    this.GSOF_MDO.SetStatus(i_cOp)
    this.GSOF_MAS.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'OFF_ERTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSOF_MSO.SetChildrenStatus(i_cOp)
  *  this.GSOF_MDO.SetChildrenStatus(i_cOp)
  *  this.GSOF_MAS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSERIAL,"OFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODESE,"OFCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODNOM,"OFCODNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODMOD,"OFCODMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFNUMDOC,"OFNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSERDOC,"OFSERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFDATDOC,"OFDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFRIFDES,"OFRIFDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFNUMVER,"OFNUMVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSTATUS,"OFSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFDATSCA,"OFDATSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFDATINV,"OFDATINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFDATCHI,"OFDATCHI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFRIFDOC,"OFRIFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSERPRE,"OFSERPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFDATAPE,"OFDATAPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODVAL,"OFCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODLIS,"OFCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODPAG,"OFCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSCOCL1,"OFSCOCL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSCOCL2,"OFSCOCL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSCOPAG,"OFSCOPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSCONTI,"OFSCONTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFFLFOSC,"OFFLFOSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSPEINC,"OFSPEINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSPEIMB,"OFSPEIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFSPETRA,"OFSPETRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFIMPOFF,"OFIMPOFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OF__NOTE,"OF__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFPATFWP,"OFPATFWP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFPATPDF,"OFPATPDF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODCON,"OFCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFDATRIC,"OFDATRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODUTE,"OFCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFNUMPRI,"OFNUMPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODAGE,"OFCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFFLFOAG,"OFFLFOAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODAG2,"OFCODAG2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFFLFOA2,"OFFLFOA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODPOR,"OFCODPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFFLFOPO,"OFFLFOPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFCODSPE,"OFCODSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OFFLFOSP,"OFFLFOSP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsof_aof
    *-- in caso di cancellazione di una nuova versione, apro l'offerta di versione precedente
    IF thisform.w_OFNUMVER > 1 and not(i_cWhere==" where OFSERIAL="+"'"+thisform.w_OFSERIAL+"'") &&evito creazione nuova offerta
        i_cWhere=" where OFSERIAL like "+"'"+thisform.w_OFSERPRE+"%'"
    ENDIF
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
    i_lTable = "OFF_ERTE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.OFF_ERTE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do GSOF_BOF with this
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.OFF_ERTE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEOFFERT","i_codazi,w_OFSERIAL")
          cp_NextTableProg(this,i_nConn,"PROFFERT","i_codazi,w_OFCODESE,w_OFSERDOC,w_OFNUMDOC")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into OFF_ERTE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OFF_ERTE')
        i_extval=cp_InsertValODBCExtFlds(this,'OFF_ERTE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(OFSERIAL,OFCODESE,OFCODNOM,OFCODMOD,OFNUMDOC"+;
                  ",OFSERDOC,OFDATDOC,OFRIFDES,OFNUMVER,OFSTATUS"+;
                  ",OFDATSCA,OFDATINV,OFDATCHI,OFRIFDOC,OFSERPRE"+;
                  ",OFDATAPE,OFCODVAL,OFCODLIS,OFCODPAG,OFSCOCL1"+;
                  ",OFSCOCL2,OFSCOPAG,OFSCONTI,OFFLFOSC,OFSPEINC"+;
                  ",OFSPEIMB,OFSPETRA,OFIMPOFF,OF__NOTE,OFPATFWP"+;
                  ",OFPATPDF,OFCODCON,OFDATRIC,OFCODUTE,OFNUMPRI"+;
                  ",OFCODAGE,OFFLFOAG,OFCODAG2,OFFLFOA2,OFCODPOR"+;
                  ",OFFLFOPO,OFCODSPE,OFFLFOSP,UTCC,UTCV"+;
                  ",UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_OFSERIAL)+;
                  ","+cp_ToStrODBC(this.w_OFCODESE)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODNOM)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODMOD)+;
                  ","+cp_ToStrODBC(this.w_OFNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_OFSERDOC)+;
                  ","+cp_ToStrODBC(this.w_OFDATDOC)+;
                  ","+cp_ToStrODBC(this.w_OFRIFDES)+;
                  ","+cp_ToStrODBC(this.w_OFNUMVER)+;
                  ","+cp_ToStrODBC(this.w_OFSTATUS)+;
                  ","+cp_ToStrODBC(this.w_OFDATSCA)+;
                  ","+cp_ToStrODBC(this.w_OFDATINV)+;
                  ","+cp_ToStrODBC(this.w_OFDATCHI)+;
                  ","+cp_ToStrODBC(this.w_OFRIFDOC)+;
                  ","+cp_ToStrODBC(this.w_OFSERPRE)+;
                  ","+cp_ToStrODBC(this.w_OFDATAPE)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODVAL)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODLIS)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODPAG)+;
                  ","+cp_ToStrODBC(this.w_OFSCOCL1)+;
                  ","+cp_ToStrODBC(this.w_OFSCOCL2)+;
                  ","+cp_ToStrODBC(this.w_OFSCOPAG)+;
                  ","+cp_ToStrODBC(this.w_OFSCONTI)+;
                  ","+cp_ToStrODBC(this.w_OFFLFOSC)+;
                  ","+cp_ToStrODBC(this.w_OFSPEINC)+;
                  ","+cp_ToStrODBC(this.w_OFSPEIMB)+;
                  ","+cp_ToStrODBC(this.w_OFSPETRA)+;
                  ","+cp_ToStrODBC(this.w_OFIMPOFF)+;
                  ","+cp_ToStrODBC(this.w_OF__NOTE)+;
                  ","+cp_ToStrODBC(this.w_OFPATFWP)+;
                  ","+cp_ToStrODBC(this.w_OFPATPDF)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODCON)+;
                  ","+cp_ToStrODBC(this.w_OFDATRIC)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODUTE)+;
                  ","+cp_ToStrODBC(this.w_OFNUMPRI)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODAGE)+;
                  ","+cp_ToStrODBC(this.w_OFFLFOAG)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODAG2)+;
                  ","+cp_ToStrODBC(this.w_OFFLFOA2)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODPOR)+;
                  ","+cp_ToStrODBC(this.w_OFFLFOPO)+;
                  ","+cp_ToStrODBCNull(this.w_OFCODSPE)+;
                  ","+cp_ToStrODBC(this.w_OFFLFOSP)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OFF_ERTE')
        i_extval=cp_InsertValVFPExtFlds(this,'OFF_ERTE')
        cp_CheckDeletedKey(i_cTable,0,'OFSERIAL',this.w_OFSERIAL)
        INSERT INTO (i_cTable);
              (OFSERIAL,OFCODESE,OFCODNOM,OFCODMOD,OFNUMDOC,OFSERDOC,OFDATDOC,OFRIFDES,OFNUMVER,OFSTATUS,OFDATSCA,OFDATINV,OFDATCHI,OFRIFDOC,OFSERPRE,OFDATAPE,OFCODVAL,OFCODLIS,OFCODPAG,OFSCOCL1,OFSCOCL2,OFSCOPAG,OFSCONTI,OFFLFOSC,OFSPEINC,OFSPEIMB,OFSPETRA,OFIMPOFF,OF__NOTE,OFPATFWP,OFPATPDF,OFCODCON,OFDATRIC,OFCODUTE,OFNUMPRI,OFCODAGE,OFFLFOAG,OFCODAG2,OFFLFOA2,OFCODPOR,OFFLFOPO,OFCODSPE,OFFLFOSP,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_OFSERIAL;
                  ,this.w_OFCODESE;
                  ,this.w_OFCODNOM;
                  ,this.w_OFCODMOD;
                  ,this.w_OFNUMDOC;
                  ,this.w_OFSERDOC;
                  ,this.w_OFDATDOC;
                  ,this.w_OFRIFDES;
                  ,this.w_OFNUMVER;
                  ,this.w_OFSTATUS;
                  ,this.w_OFDATSCA;
                  ,this.w_OFDATINV;
                  ,this.w_OFDATCHI;
                  ,this.w_OFRIFDOC;
                  ,this.w_OFSERPRE;
                  ,this.w_OFDATAPE;
                  ,this.w_OFCODVAL;
                  ,this.w_OFCODLIS;
                  ,this.w_OFCODPAG;
                  ,this.w_OFSCOCL1;
                  ,this.w_OFSCOCL2;
                  ,this.w_OFSCOPAG;
                  ,this.w_OFSCONTI;
                  ,this.w_OFFLFOSC;
                  ,this.w_OFSPEINC;
                  ,this.w_OFSPEIMB;
                  ,this.w_OFSPETRA;
                  ,this.w_OFIMPOFF;
                  ,this.w_OF__NOTE;
                  ,this.w_OFPATFWP;
                  ,this.w_OFPATPDF;
                  ,this.w_OFCODCON;
                  ,this.w_OFDATRIC;
                  ,this.w_OFCODUTE;
                  ,this.w_OFNUMPRI;
                  ,this.w_OFCODAGE;
                  ,this.w_OFFLFOAG;
                  ,this.w_OFCODAG2;
                  ,this.w_OFFLFOA2;
                  ,this.w_OFCODPOR;
                  ,this.w_OFFLFOPO;
                  ,this.w_OFCODSPE;
                  ,this.w_OFFLFOSP;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.OFF_ERTE_IDX,i_nConn)
      *
      * update OFF_ERTE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'OFF_ERTE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " OFCODESE="+cp_ToStrODBC(this.w_OFCODESE)+;
             ",OFCODNOM="+cp_ToStrODBCNull(this.w_OFCODNOM)+;
             ",OFCODMOD="+cp_ToStrODBCNull(this.w_OFCODMOD)+;
             ",OFNUMDOC="+cp_ToStrODBC(this.w_OFNUMDOC)+;
             ",OFSERDOC="+cp_ToStrODBC(this.w_OFSERDOC)+;
             ",OFDATDOC="+cp_ToStrODBC(this.w_OFDATDOC)+;
             ",OFRIFDES="+cp_ToStrODBC(this.w_OFRIFDES)+;
             ",OFNUMVER="+cp_ToStrODBC(this.w_OFNUMVER)+;
             ",OFSTATUS="+cp_ToStrODBC(this.w_OFSTATUS)+;
             ",OFDATSCA="+cp_ToStrODBC(this.w_OFDATSCA)+;
             ",OFDATINV="+cp_ToStrODBC(this.w_OFDATINV)+;
             ",OFDATCHI="+cp_ToStrODBC(this.w_OFDATCHI)+;
             ",OFRIFDOC="+cp_ToStrODBC(this.w_OFRIFDOC)+;
             ",OFSERPRE="+cp_ToStrODBC(this.w_OFSERPRE)+;
             ",OFDATAPE="+cp_ToStrODBC(this.w_OFDATAPE)+;
             ",OFCODVAL="+cp_ToStrODBCNull(this.w_OFCODVAL)+;
             ",OFCODLIS="+cp_ToStrODBCNull(this.w_OFCODLIS)+;
             ",OFCODPAG="+cp_ToStrODBCNull(this.w_OFCODPAG)+;
             ",OFSCOCL1="+cp_ToStrODBC(this.w_OFSCOCL1)+;
             ",OFSCOCL2="+cp_ToStrODBC(this.w_OFSCOCL2)+;
             ",OFSCOPAG="+cp_ToStrODBC(this.w_OFSCOPAG)+;
             ",OFSCONTI="+cp_ToStrODBC(this.w_OFSCONTI)+;
             ",OFFLFOSC="+cp_ToStrODBC(this.w_OFFLFOSC)+;
             ",OFSPEINC="+cp_ToStrODBC(this.w_OFSPEINC)+;
             ",OFSPEIMB="+cp_ToStrODBC(this.w_OFSPEIMB)+;
             ",OFSPETRA="+cp_ToStrODBC(this.w_OFSPETRA)+;
             ",OFIMPOFF="+cp_ToStrODBC(this.w_OFIMPOFF)+;
             ",OF__NOTE="+cp_ToStrODBC(this.w_OF__NOTE)+;
             ",OFPATFWP="+cp_ToStrODBC(this.w_OFPATFWP)+;
             ",OFPATPDF="+cp_ToStrODBC(this.w_OFPATPDF)+;
             ",OFCODCON="+cp_ToStrODBCNull(this.w_OFCODCON)+;
             ",OFDATRIC="+cp_ToStrODBC(this.w_OFDATRIC)+;
             ",OFCODUTE="+cp_ToStrODBCNull(this.w_OFCODUTE)+;
             ",OFNUMPRI="+cp_ToStrODBC(this.w_OFNUMPRI)+;
             ",OFCODAGE="+cp_ToStrODBCNull(this.w_OFCODAGE)+;
             ",OFFLFOAG="+cp_ToStrODBC(this.w_OFFLFOAG)+;
             ",OFCODAG2="+cp_ToStrODBCNull(this.w_OFCODAG2)+;
             ",OFFLFOA2="+cp_ToStrODBC(this.w_OFFLFOA2)+;
             ",OFCODPOR="+cp_ToStrODBCNull(this.w_OFCODPOR)+;
             ",OFFLFOPO="+cp_ToStrODBC(this.w_OFFLFOPO)+;
             ",OFCODSPE="+cp_ToStrODBCNull(this.w_OFCODSPE)+;
             ",OFFLFOSP="+cp_ToStrODBC(this.w_OFFLFOSP)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'OFF_ERTE')
        i_cWhere = cp_PKFox(i_cTable  ,'OFSERIAL',this.w_OFSERIAL  )
        UPDATE (i_cTable) SET;
              OFCODESE=this.w_OFCODESE;
             ,OFCODNOM=this.w_OFCODNOM;
             ,OFCODMOD=this.w_OFCODMOD;
             ,OFNUMDOC=this.w_OFNUMDOC;
             ,OFSERDOC=this.w_OFSERDOC;
             ,OFDATDOC=this.w_OFDATDOC;
             ,OFRIFDES=this.w_OFRIFDES;
             ,OFNUMVER=this.w_OFNUMVER;
             ,OFSTATUS=this.w_OFSTATUS;
             ,OFDATSCA=this.w_OFDATSCA;
             ,OFDATINV=this.w_OFDATINV;
             ,OFDATCHI=this.w_OFDATCHI;
             ,OFRIFDOC=this.w_OFRIFDOC;
             ,OFSERPRE=this.w_OFSERPRE;
             ,OFDATAPE=this.w_OFDATAPE;
             ,OFCODVAL=this.w_OFCODVAL;
             ,OFCODLIS=this.w_OFCODLIS;
             ,OFCODPAG=this.w_OFCODPAG;
             ,OFSCOCL1=this.w_OFSCOCL1;
             ,OFSCOCL2=this.w_OFSCOCL2;
             ,OFSCOPAG=this.w_OFSCOPAG;
             ,OFSCONTI=this.w_OFSCONTI;
             ,OFFLFOSC=this.w_OFFLFOSC;
             ,OFSPEINC=this.w_OFSPEINC;
             ,OFSPEIMB=this.w_OFSPEIMB;
             ,OFSPETRA=this.w_OFSPETRA;
             ,OFIMPOFF=this.w_OFIMPOFF;
             ,OF__NOTE=this.w_OF__NOTE;
             ,OFPATFWP=this.w_OFPATFWP;
             ,OFPATPDF=this.w_OFPATPDF;
             ,OFCODCON=this.w_OFCODCON;
             ,OFDATRIC=this.w_OFDATRIC;
             ,OFCODUTE=this.w_OFCODUTE;
             ,OFNUMPRI=this.w_OFNUMPRI;
             ,OFCODAGE=this.w_OFCODAGE;
             ,OFFLFOAG=this.w_OFFLFOAG;
             ,OFCODAG2=this.w_OFCODAG2;
             ,OFFLFOA2=this.w_OFFLFOA2;
             ,OFCODPOR=this.w_OFCODPOR;
             ,OFFLFOPO=this.w_OFFLFOPO;
             ,OFCODSPE=this.w_OFCODSPE;
             ,OFFLFOSP=this.w_OFFLFOSP;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSOF_MSO : Saving
      this.GSOF_MSO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_OFSERIAL,"OSCODICE";
             )
      this.GSOF_MSO.mReplace()
      * --- GSOF_MDO : Saving
      this.GSOF_MDO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_OFSERIAL,"ODSERIAL";
             )
      this.GSOF_MDO.mReplace()
      * --- GSOF_MAS : Saving
      this.GSOF_MAS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_OFSERIAL,"OACODICE";
             )
      this.GSOF_MAS.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSOF_MSO : Deleting
    this.GSOF_MSO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_OFSERIAL,"OSCODICE";
           )
    this.GSOF_MSO.mDelete()
    * --- GSOF_MDO : Deleting
    this.GSOF_MDO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_OFSERIAL,"ODSERIAL";
           )
    this.GSOF_MDO.mDelete()
    * --- GSOF_MAS : Deleting
    this.GSOF_MAS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_OFSERIAL,"OACODICE";
           )
    this.GSOF_MAS.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.OFF_ERTE_IDX,i_nConn)
      *
      * delete OFF_ERTE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'OFSERIAL',this.w_OFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_CFUNC = UPPER(this.cFunction)
        .DoRTCalc(4,8,.t.)
        if .o_OFCODNOM<>.w_OFCODNOM
          .link_1_9('Full')
        endif
        .DoRTCalc(10,16,.t.)
            .w_OFNUMVER = iif (.w_OFNUMVER = 0,1,.w_OFNUMVER)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .DoRTCalc(18,36,.t.)
            .w_PROTECT = IIF (.w_OFSTATUS = 'V','S','N')
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .DoRTCalc(38,38,.t.)
          .link_1_60('Full')
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .DoRTCalc(40,49,.t.)
        if .o_OFSERIAL<>.w_OFSERIAL
            .w_OLDSTAT = .w_OFSTATUS
        endif
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .DoRTCalc(51,65,.t.)
            .w_OFDATAPE = IIF(.w_OFNUMVER=1, .w_OFDATDOC, .w_OFDATAPE)
        if .o_OFCODMOD<>.w_OFCODMOD
          .link_1_93('Full')
        endif
        .DoRTCalc(68,68,.t.)
            .w_CODMAG = IIF(EMPTY(.w_MAGDOC), g_MAGAZI, .w_MAGDOC)
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        if .o_OFSTATUS<>.w_OFSTATUS
          .Calculate_TNMMBPWIVH()
        endif
        .DoRTCalc(70,76,.t.)
        if .o_OFCODMOD<>.w_OFCODMOD
            .w_OFCODVAL = iif(empty(.w_OFCODMOD),g_PERVAL,.w_MOCODVAL)
          .link_2_1('Full')
        endif
        .DoRTCalc(78,79,.t.)
        if .o_OFCODVAL<>.w_OFCODVAL.or. .o_OFCODPAG<>.w_OFCODPAG.or. .o_OFCODLIS<>.w_OFCODLIS
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_OFCODVAL<>.w_OFCODVAL.or. .o_OFCODPAG<>.w_OFCODPAG
            .w_CALCPICU = DEFPIU(.w_DECUNI)
        endif
        .DoRTCalc(82,82,.t.)
        if .o_OFCODMOD<>.w_OFCODMOD.or. .o_OFCODVAL<>.w_OFCODVAL
            .w_OFCODLIS = icase( EMPTY(.w_LISNOM) AND NOT EMPTY(.w_LISMOD),.w_LISMOD,.w_OFCODLIS)
          .link_2_8('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate()
        .DoRTCalc(84,91,.t.)
        if .o_OFCODMOD<>.w_OFCODMOD
            .w_OFCODPAG = IIF(EMPTY(.w_NOCODPAG) AND NOT EMPTY(.w_PAGMOD), .w_PAGMOD, .w_OFCODPAG)
          .link_3_1('Full')
        endif
        .DoRTCalc(93,94,.t.)
        if .o_OFSCOCL1<>.w_OFSCOCL1
            .w_OFSCOCL2 = IIF(.w_OFSCOCL1=0, 0, .w_OFSCOCL2)
        endif
        .DoRTCalc(96,96,.t.)
            .w_TOTRIP = IIF( Upper(this.GSOF_MDO.class)='STDDYNAMICCHILD' , 0 , this.gsof_mdo .w_TOTRIPD )
            .w_TOTMERCE = IIF( Upper(this.GSOF_MDO.class)='STDDYNAMICCHILD' , 0 , this.gsof_mdo .w_TOTMERCE )
        .DoRTCalc(99,104,.t.)
        if .o_OFSCOCL1<>.w_OFSCOCL1.or. .o_OFSCOCL2<>.w_OFSCOCL2.or. .o_OFCODPAG<>.w_OFCODPAG.or. .o_OFSCOPAG<>.w_OFSCOPAG
            .w_OFSCONTI = IIF(.w_OFFLFOSC='S',.w_OFSCONTI,Calsco(.w_TOTMERCE, .w_OFSCOCL1, .w_OFSCOCL2, .w_OFSCOPAG, .w_DECTOT))
        endif
        .DoRTCalc(106,107,.t.)
        if .o_OFCODPAG<>.w_OFCODPAG.or. .o_OFCODNOM<>.w_OFCODNOM.or. .o_OFCODVAL<>.w_OFCODVAL.or. .o_MOSPEINC<>.w_MOSPEINC
            .w_OFSPEINC = IIF(EMPTY(.w_CALSPEINC), .w_MOSPEINC, .w_CALSPEINC)
        endif
        if .o_OFCODMOD<>.w_OFCODMOD
            .w_OFSPEIMB = .w_MOSPEIMB
        endif
        if .o_OFCODMOD<>.w_OFCODMOD
            .w_OFSPETRA = .w_MOSPETRA
        endif
            .w_OFIMPOFF = .w_TOTRIP+.w_OFSCONTI+.w_OFSPEINC+.w_OFSPEIMB+.w_OFSPETRA
        .DoRTCalc(112,112,.t.)
            .w_codval = .w_OFCODVAL
        .oPgFrm.Page3.oPag.oObj_3_36.Calculate()
        .DoRTCalc(114,114,.t.)
        if .o_TOTMERCE<>.w_TOTMERCE.or. .o_OFDATDOC<>.w_OFDATDOC
            .w_NULLA = ' '
        endif
        .DoRTCalc(116,117,.t.)
        if .o_OFCODNOM<>.w_OFCODNOM
            .w_OFCODCON = Space(5)
          .link_4_6('Full')
        endif
        .DoRTCalc(119,119,.t.)
        if .o_OFCODNOM<>.w_OFCODNOM
            .w_OFCODUTE = IIF(EMPTY(.w_OFCODUTE), i_Codute, .w_OFCODUTE)
          .link_4_8('Full')
        endif
        if .o_OFCODNOM<>.w_OFCODNOM
            .w_OFNUMPRI = .w_PRIDEF
        endif
        if .o_OFCODNOM<>.w_OFCODNOM
          .link_4_11('Full')
        endif
        .DoRTCalc(123,126,.t.)
        if .o_OFCODMOD<>.w_OFCODMOD
          .link_4_18('Full')
        endif
        .DoRTCalc(128,128,.t.)
        if .o_OFCODMOD<>.w_OFCODMOD
          .link_4_21('Full')
        endif
        .DoRTCalc(130,134,.t.)
            .w_OBTEST = i_datsys
        .oPgFrm.Page4.oPag.oObj_4_39.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_41.Calculate()
        .oPgFrm.Page5.oPag.ZoomAll.Calculate()
        .DoRTCalc(136,148,.t.)
            .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
            .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
        .oPgFrm.Page6.oPag.ZOOM.Calculate()
        .DoRTCalc(151,154,.t.)
        if .o_STAT1<>.w_STAT1.or. .o_STAT2<>.w_STAT2
            .w_STATO = IIF(.cFunction<>'Load',IIF(g_AGEN<>'S', .w_STAT1,.w_STAT2),'X')
        endif
        if .o_DATAINI<>.w_DATAINI.or. .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI
          .Calculate_HEZCDUNPFW()
        endif
        if .o_DATAFIN<>.w_DATAFIN.or. .o_ORAFIN<>.w_ORAFIN.or. .o_MINFIN<>.w_MINFIN
          .Calculate_ZQLIBXAFGH()
        endif
        .DoRTCalc(156,162,.t.)
        if .o_NOMINI<>.w_NOMINI.or. .o_NOMFIN<>.w_NOMFIN
          .link_6_19('Full')
        endif
        .DoRTCalc(164,166,.t.)
            .w_ATSERIAL = .w_ZOOM.GetVar('ATSERIAL')
        if .o_SUBJECT<>.w_SUBJECT
            .w_OGGETTO = '%'+ALLTRIM(.w_SUBJECT)
        endif
        if .o_ORAINI<>.w_ORAINI
          .Calculate_SLCYSAMDOK()
        endif
        if .o_MININI<>.w_MININI
          .Calculate_XLTWHZKGZW()
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_TTQFMXGTWH()
        endif
        if .o_ORAFIN<>.w_ORAFIN
          .Calculate_EMHXGAZDWG()
        endif
        .DoRTCalc(169,172,.t.)
        if .o_OFCODNOM<>.w_OFCODNOM
            .w_NOMINI = .w_OFCODNOM
        endif
        if .o_OFCODNOM<>.w_OFCODNOM
            .w_NOMFIN = .w_OFCODNOM
        endif
        .DoRTCalc(175,176,.t.)
        if .o_PRIOINI<>.w_PRIOINI
            .w_PRIOFIN = .w_PRIOINI
        endif
        .DoRTCalc(178,178,.t.)
        if .o_OFSERIAL<>.w_OFSERIAL
            .w_OFF_SER = .w_OFSERIAL
        endif
        if .o_OFCODCON<>.w_OFCODCON.or. .o_OFCODVAL<>.w_OFCODVAL.or. .o_OFCODPAG<>.w_OFCODPAG.or. .o_OFCODMOD<>.w_OFCODMOD
          .Calculate_VSEQLRXTWX()
        endif
        .DoRTCalc(180,189,.t.)
        if .o_MCALSI1<>.w_MCALSI1.or. .o_MCALSI2<>.w_MCALSI2.or. .o_MCALSI3<>.w_MCALSI3.or. .o_OFCODNOM<>.w_OFCODNOM.or. .o_CAUDOC<>.w_CAUDOC.or. .o_OFCODSPE<>.w_OFCODSPE
            .w_MCALSI = IIF(.w_MOSPEIMB<>0, '', ICASE( NOT EMPTY( nvl(.w_MCALSI3, space(5)) ), .w_MCALSI3, NOT EMPTY(nvl( .w_MCALSI2 , space(5))), .w_MCALSI2, .w_MCALSI1) )
        endif
        .DoRTCalc(191,191,.t.)
        if .o_MCALST1<>.w_MCALST1.or. .o_MCALST2<>.w_MCALST2.or. .o_MCALST3<>.w_MCALST3.or. .o_OFCODSPE<>.w_OFCODSPE.or. .o_OFCODNOM<>.w_OFCODNOM.or. .o_CAUDOC<>.w_CAUDOC
            .w_MCALST = IIF(.w_MOSPETRA<>0, '', ICASE( NOT EMPTY( nvl(.w_MCALST3, space(5)) ), .w_MCALST3, NOT EMPTY( nvl(.w_MCALST2, space(5)) ), .w_MCALST2, .w_MCALST1) )
        endif
        .oPgFrm.Page2.oPag.oObj_2_22.Calculate()
        * --- Area Manuale = Calculate
        * --- gsof_aof
        if  .o_TOTMERCE<>.w_TOTMERCE AND .w_OFFLFOSC<>'S'
          .w_OFSCONTI = cp_ROUND(.w_TOTMERCE * (1+.w_OFSCOCL1/100)*(1+.w_OFSCOCL2/100)*(1+.w_OFSCOPAG/100), .w_DECTOT)-.w_TOTMERCE
        endif
        
        
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEOFFERT","i_codazi,w_OFSERIAL")
          .op_OFSERIAL = .w_OFSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_OFCODESE<>.w_OFCODESE .or. .op_OFSERDOC<>.w_OFSERDOC
           cp_AskTableProg(this,i_nConn,"PROFFERT","i_codazi,w_OFCODESE,w_OFSERDOC,w_OFNUMDOC")
          .op_OFNUMDOC = .w_OFNUMDOC
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_OFCODESE = .w_OFCODESE
        .op_OFSERDOC = .w_OFSERDOC
      endwith
      this.DoRTCalc(193,220,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_36.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_39.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_41.Calculate()
        .oPgFrm.Page5.oPag.ZoomAll.Calculate()
        .oPgFrm.Page6.oPag.ZOOM.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_22.Calculate()
    endwith
  return

  proc Calculate_TNMMBPWIVH()
    with this
          * --- Cambio caption (opportunita/offerte)
          .cComment = IIF(.w_OFSTATUS='I', STRTRAN(.cComment, AH_MSGFORMAT('Offerte'),AH_MSGFORMAT('Opportunit�')), STRTRAN(.cComment, AH_MSGFORMAT('Opportunit�'), AH_MSGFORMAT('Offerte')))
          .Caption = .cComment
    endwith
  endproc
  proc Calculate_QHBHIDPQEQ()
    with this
          * --- Word + stampa
          GSOF_BEW(this;
             )
          GSOF_BOF(this;
             )
    endwith
  endproc
  proc Calculate_HEZCDUNPFW()
    with this
          * --- Setta w_atdatini
          .w_ATDATINI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAINI),  DTOC(.w_DATAINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_ZQLIBXAFGH()
    with this
          * --- Setta w_atdatfin1
          .w_ATDATFIN1 = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAFIN),  DTOC(.w_DATAFIN)+' '+.w_ORAFIN+':'+.w_MINFIN+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_SLCYSAMDOK()
    with this
          * --- Resetto w_oraini
          .w_ORAINI = .ZeroFill(.w_ORAINI)
    endwith
  endproc
  proc Calculate_XLTWHZKGZW()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_TTQFMXGTWH()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_EMHXGAZDWG()
    with this
          * --- Resetto w_orafin
          .w_ORAFIN = .ZeroFill(.w_ORAFIN)
    endwith
  endproc
  proc Calculate_VSEQLRXTWX()
    with this
          * --- Valorizza spese incasso
          .w_CALSPEINC = CALSPEINC( 'V', .w_OFCODPAG, .w_OFCODVAL, 'C', .w_OFCODCON, .w_FLSPIN )
    endwith
  endproc
  proc Calculate_DETWXWFZDZ()
    with this
          * --- Check modifica ofspeimb
          .w_CHKSPEIMB =  .F.
    endwith
  endproc
  proc Calculate_XZSSGVIHAT()
    with this
          * --- Check modifica ofspetra
          .w_CHKSPETRA = .F.
    endwith
  endproc
  proc Calculate_TSLSBPBJGU()
    with this
          * --- Valorizza spese IMBALLO E TRASPORTO
          GSAR_BST(this;
             )
    endwith
  endproc
  proc Calculate_EEJLTGVTHY()
    with this
          * --- Modello di default
          .w_OFCODMOD = .w_CODMOD
          .link_1_7('Full')
          gsof_blm(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOFCODNOM_1_4.enabled = this.oPgFrm.Page1.oPag.oOFCODNOM_1_4.mCond()
    this.oPgFrm.Page1.oPag.oOFCODMOD_1_7.enabled = this.oPgFrm.Page1.oPag.oOFCODMOD_1_7.mCond()
    this.oPgFrm.Page1.oPag.oOFNUMDOC_1_14.enabled = this.oPgFrm.Page1.oPag.oOFNUMDOC_1_14.mCond()
    this.oPgFrm.Page1.oPag.oOFSERDOC_1_16.enabled = this.oPgFrm.Page1.oPag.oOFSERDOC_1_16.mCond()
    this.oPgFrm.Page1.oPag.oOFDATINV_1_40.enabled = this.oPgFrm.Page1.oPag.oOFDATINV_1_40.mCond()
    this.oPgFrm.Page2.oPag.oOFCODVAL_2_1.enabled = this.oPgFrm.Page2.oPag.oOFCODVAL_2_1.mCond()
    this.oPgFrm.Page3.oPag.oOFSCOCL2_3_4.enabled = this.oPgFrm.Page3.oPag.oOFSCOCL2_3_4.mCond()
    this.oPgFrm.Page3.oPag.oOFSCONTI_3_20.enabled = this.oPgFrm.Page3.oPag.oOFSCONTI_3_20.mCond()
    this.oPgFrm.Page4.oPag.oOFCODAGE_4_11.enabled = this.oPgFrm.Page4.oPag.oOFCODAGE_4_11.mCond()
    this.oPgFrm.Page4.oPag.oOFCODAG2_4_15.enabled = this.oPgFrm.Page4.oPag.oOFCODAG2_4_15.mCond()
    this.oPgFrm.Page5.oPag.oNOMFIL_5_1.enabled = this.oPgFrm.Page5.oPag.oNOMFIL_5_1.mCond()
    this.oPgFrm.Page5.oPag.oOGGETT_5_2.enabled = this.oPgFrm.Page5.oPag.oOGGETT_5_2.mCond()
    this.oPgFrm.Page5.oPag.oNOTE_5_3.enabled = this.oPgFrm.Page5.oPag.oNOTE_5_3.mCond()
    this.oPgFrm.Page6.oPag.oOPERAT_6_6.enabled = this.oPgFrm.Page6.oPag.oOPERAT_6_6.mCond()
    this.oPgFrm.Page6.oPag.oSTAT1_6_7.enabled = this.oPgFrm.Page6.oPag.oSTAT1_6_7.mCond()
    this.oPgFrm.Page6.oPag.oSTAT2_6_8.enabled = this.oPgFrm.Page6.oPag.oSTAT2_6_8.mCond()
    this.oPgFrm.Page6.oPag.oDATAINI_6_10.enabled = this.oPgFrm.Page6.oPag.oDATAINI_6_10.mCond()
    this.oPgFrm.Page6.oPag.oORAINI_6_11.enabled = this.oPgFrm.Page6.oPag.oORAINI_6_11.mCond()
    this.oPgFrm.Page6.oPag.oMININI_6_12.enabled = this.oPgFrm.Page6.oPag.oMININI_6_12.mCond()
    this.oPgFrm.Page6.oPag.oDATAFIN_6_14.enabled = this.oPgFrm.Page6.oPag.oDATAFIN_6_14.mCond()
    this.oPgFrm.Page6.oPag.oORAFIN_6_15.enabled = this.oPgFrm.Page6.oPag.oORAFIN_6_15.mCond()
    this.oPgFrm.Page6.oPag.oMINFIN_6_16.enabled = this.oPgFrm.Page6.oPag.oMINFIN_6_16.mCond()
    this.oPgFrm.Page6.oPag.oSUBJECT_6_18.enabled = this.oPgFrm.Page6.oPag.oSUBJECT_6_18.mCond()
    this.oPgFrm.Page6.oPag.oCONTINI_6_19.enabled = this.oPgFrm.Page6.oPag.oCONTINI_6_19.mCond()
    this.oPgFrm.Page6.oPag.oPRIOINI_6_20.enabled = this.oPgFrm.Page6.oPag.oPRIOINI_6_20.mCond()
    this.oPgFrm.Page6.oPag.oPRIOINI_6_52.enabled = this.oPgFrm.Page6.oPag.oPRIOINI_6_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_5.enabled = this.oPgFrm.Page5.oPag.oBtn_5_5.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_7.enabled = this.oPgFrm.Page5.oPag.oBtn_5_7.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_8.enabled = this.oPgFrm.Page5.oPag.oBtn_5_8.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_9.enabled = this.oPgFrm.Page5.oPag.oBtn_5_9.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_10.enabled = this.oPgFrm.Page5.oPag.oBtn_5_10.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_2.enabled = this.oPgFrm.Page6.oPag.oBtn_6_2.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_3.enabled = this.oPgFrm.Page6.oPag.oBtn_6_3.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_4.enabled = this.oPgFrm.Page6.oPag.oBtn_6_4.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_22.enabled = this.oPgFrm.Page6.oPag.oBtn_6_22.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_47.enabled = this.oPgFrm.Page3.oPag.oBtn_3_47.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_48.enabled = this.oPgFrm.Page3.oPag.oBtn_3_48.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_46.visible=!this.oPgFrm.Page1.oPag.oBtn_1_46.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_47.visible=!this.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_48.visible=!this.oPgFrm.Page1.oPag.oBtn_1_48.mHide()
    this.oPgFrm.Page6.oPag.oNAME_6_5.visible=!this.oPgFrm.Page6.oPag.oNAME_6_5.mHide()
    this.oPgFrm.Page6.oPag.oSTAT1_6_7.visible=!this.oPgFrm.Page6.oPag.oSTAT1_6_7.mHide()
    this.oPgFrm.Page6.oPag.oSTAT2_6_8.visible=!this.oPgFrm.Page6.oPag.oSTAT2_6_8.mHide()
    this.oPgFrm.Page6.oPag.oPRIOINI_6_20.visible=!this.oPgFrm.Page6.oPag.oPRIOINI_6_20.mHide()
    this.oPgFrm.Page6.oPag.oDESCPER_6_49.visible=!this.oPgFrm.Page6.oPag.oDESCPER_6_49.mHide()
    this.oPgFrm.Page6.oPag.oPRIOINI_6_52.visible=!this.oPgFrm.Page6.oPag.oPRIOINI_6_52.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_73.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_75.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_96.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Load")
          .Calculate_TNMMBPWIVH()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_20.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_36.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_39.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_41.Event(cEvent)
      .oPgFrm.Page5.oPag.ZoomAll.Event(cEvent)
        if lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_QHBHIDPQEQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page6.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_OFSPEIMB Changed")
          .Calculate_DETWXWFZDZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_OFSPETRA Changed")
          .Calculate_XZSSGVIHAT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Calspeacc")
          .Calculate_TSLSBPBJGU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record")
          .Calculate_EEJLTGVTHY()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsof_aof
    if cevent='New record' and Not Empty(this.w_CODMOD)
       This.gsof_mso.Notifyevent('AggiornaSezioni')
    endif
    if cevent='w_OFCODMOD Changed'
       This.o_OFCODLIS=' '
       this.GSOF_MDO.NotifyEvent("AggiornaRighe1")  
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=OFCODNOM
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_OFCODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_OFCODNOM))
          select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_OFCODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_OFCODNOM)+"%");

            select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NOINDIRI like "+cp_ToStrODBC(trim(this.w_OFCODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NOINDIRI like "+cp_ToStr(trim(this.w_OFCODNOM)+"%");

            select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFCODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oOFCODNOM_1_4'),i_cWhere,'GSAR_ANO',"Elenco nominativi",'GSOF_AOF.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_OFCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_OFCODNOM)
            select NOCODICE,NODESCRI,NOINDIRI,NO___CAP,NOLOCALI,NOPROVIN,NOCODVAL,NOCODZON,NOCODORI,NOCODGRU,NOCODPRI,NOCODCLI,NOTIPNOM,NODTOBSO,NOCODOPE,NOCODLIN,NOCODAGE,NOTIPCLI,NOCODPAG,NONUMLIS,NOTELEFO,NONUMCEL,NO_EMAIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_NOMDES = NVL(_Link_.NODESCRI,space(40))
      this.w_NOMIND = NVL(_Link_.NOINDIRI,space(35))
      this.w_NOMCAP = NVL(_Link_.NO___CAP,space(8))
      this.w_NOMLOC = NVL(_Link_.NOLOCALI,space(30))
      this.w_NOMPRO = NVL(_Link_.NOPROVIN,space(2))
      this.w_VALCLF = NVL(_Link_.NOCODVAL,space(3))
      this.w_NCODZON = NVL(_Link_.NOCODZON,space(3))
      this.w_NCODORI = NVL(_Link_.NOCODORI,space(5))
      this.w_NCODGRN = NVL(_Link_.NOCODGRU,space(5))
      this.w_NCODPRI = NVL(_Link_.NOCODPRI,0)
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_OFCODUTE = NVL(_Link_.NOCODOPE,0)
      this.w_NCODLIN = NVL(_Link_.NOCODLIN,space(3))
      this.w_CODAGE = NVL(_Link_.NOCODAGE,space(5))
      this.w_TIPCLI = NVL(_Link_.NOTIPCLI,space(15))
      this.w_NOCODPAG = NVL(_Link_.NOCODPAG,space(5))
      this.w_LISNOM = NVL(_Link_.NONUMLIS,space(5))
      this.w_NOTELEFO = NVL(_Link_.NOTELEFO,space(18))
      this.w_NONUMCEL = NVL(_Link_.NONUMCEL,space(18))
      this.w_EMAIL = NVL(_Link_.NO_EMAIL,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODNOM = space(15)
      endif
      this.w_NOMDES = space(40)
      this.w_NOMIND = space(35)
      this.w_NOMCAP = space(8)
      this.w_NOMLOC = space(30)
      this.w_NOMPRO = space(2)
      this.w_VALCLF = space(3)
      this.w_NCODZON = space(3)
      this.w_NCODORI = space(5)
      this.w_NCODGRN = space(5)
      this.w_NCODPRI = 0
      this.w_CODCLI = space(15)
      this.w_TIPNOM = space(1)
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_OFCODUTE = 0
      this.w_NCODLIN = space(3)
      this.w_CODAGE = space(5)
      this.w_TIPCLI = space(15)
      this.w_NOCODPAG = space(5)
      this.w_LISNOM = space(5)
      this.w_NOTELEFO = space(18)
      this.w_NONUMCEL = space(18)
      this.w_EMAIL = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DTOBS1) OR .w_DTOBS1>.w_OFDATDOC) AND .w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Nominativo inesistente o obsoleto oppure di tipo congelato")
        endif
        this.w_OFCODNOM = space(15)
        this.w_NOMDES = space(40)
        this.w_NOMIND = space(35)
        this.w_NOMCAP = space(8)
        this.w_NOMLOC = space(30)
        this.w_NOMPRO = space(2)
        this.w_VALCLF = space(3)
        this.w_NCODZON = space(3)
        this.w_NCODORI = space(5)
        this.w_NCODGRN = space(5)
        this.w_NCODPRI = 0
        this.w_CODCLI = space(15)
        this.w_TIPNOM = space(1)
        this.w_DTOBS1 = ctod("  /  /  ")
        this.w_OFCODUTE = 0
        this.w_NCODLIN = space(3)
        this.w_CODAGE = space(5)
        this.w_TIPCLI = space(15)
        this.w_NOCODPAG = space(5)
        this.w_LISNOM = space(5)
        this.w_NOTELEFO = space(18)
        this.w_NONUMCEL = space(18)
        this.w_EMAIL = space(254)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 23 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_NOMI_IDX,3] and i_nFlds+23<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.NOCODICE as NOCODICE104"+ ",link_1_4.NODESCRI as NODESCRI104"+ ",link_1_4.NOINDIRI as NOINDIRI104"+ ",link_1_4.NO___CAP as NO___CAP104"+ ",link_1_4.NOLOCALI as NOLOCALI104"+ ",link_1_4.NOPROVIN as NOPROVIN104"+ ",link_1_4.NOCODVAL as NOCODVAL104"+ ",link_1_4.NOCODZON as NOCODZON104"+ ",link_1_4.NOCODORI as NOCODORI104"+ ",link_1_4.NOCODGRU as NOCODGRU104"+ ",link_1_4.NOCODPRI as NOCODPRI104"+ ",link_1_4.NOCODCLI as NOCODCLI104"+ ",link_1_4.NOTIPNOM as NOTIPNOM104"+ ",link_1_4.NODTOBSO as NODTOBSO104"+ ",link_1_4.NOCODOPE as NOCODOPE104"+ ",link_1_4.NOCODLIN as NOCODLIN104"+ ",link_1_4.NOCODAGE as NOCODAGE104"+ ",link_1_4.NOTIPCLI as NOTIPCLI104"+ ",link_1_4.NOCODPAG as NOCODPAG104"+ ",link_1_4.NONUMLIS as NONUMLIS104"+ ",link_1_4.NOTELEFO as NOTELEFO104"+ ",link_1_4.NONUMCEL as NONUMCEL104"+ ",link_1_4.NO_EMAIL as NO_EMAIL104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on OFF_ERTE.OFCODNOM=link_1_4.NOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+23
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODNOM=link_1_4.NOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+23
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OFCODMOD
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_lTable = "MOD_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2], .t., this.MOD_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AMO',True,'MOD_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_OFCODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_OFCODMOD))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFCODMOD) and !this.bDontReportError
            deferred_cp_zoom('MOD_OFFE','*','MOCODICE',cp_AbsName(oSource.parent,'oOFCODMOD_1_7'),i_cWhere,'GSOF_AMO',"Modelli offerte",'GSOF_AOF.MOD_OFFE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_OFCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_OFCODMOD)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODMOD = NVL(_Link_.MOCODICE,space(5))
      this.w_DESMOD = NVL(_Link_.MODESCRI,space(35))
      this.w_OFSERDOC = NVL(_Link_.MOSERDOC,space(10))
      this.w_SEZVAR = NVL(_Link_.MOSEZVAR,space(1))
      this.w_MOSPEINC = NVL(_Link_.MOSPEINC,0)
      this.w_MOSPEIMB = NVL(_Link_.MOSPEIMB,0)
      this.w_MOSPETRA = NVL(_Link_.MOSPETRA,0)
      this.w_FORMULA = NVL(_Link_.MONOMDOC,space(254))
      this.w_SC1MOD = NVL(_Link_.MOSCOCL1,0)
      this.w_SC2MOD = NVL(_Link_.MOSCOCL2,0)
      this.w_SCPMOD = NVL(_Link_.MOSCOPAG,0)
      this.w_LISMOD = NVL(_Link_.MOCODLIS,space(5))
      this.w_PAGMOD = NVL(_Link_.MOCODPAG,space(5))
      this.w_OFCODSPE = NVL(_Link_.MOCODSPE,space(3))
      this.w_OFCODPOR = NVL(_Link_.MOCODPOR,space(1))
      this.w_OFTIPFOR = NVL(_Link_.MOTIPFOR,space(1))
      this.w_OFINISCA = NVL(_Link_.MOINISCA,space(2))
      this.w_OFGIOSCA = NVL(_Link_.MOGIOSCA,0)
      this.w_OFGIOFIS = NVL(_Link_.MOGIOFIS,0)
      this.w_NUMSCO = NVL(_Link_.MONUMSCO,0)
      this.w_PATHMOD = NVL(_Link_.MOPATMOD,space(254))
      this.w_PATHARC = NVL(_Link_.MOPATARC,space(254))
      this.w_TIPODOC = NVL(_Link_.MOTIPFOR,space(1))
      this.w_NCODPRF = NVL(_Link_.MOCODPRI,space(5))
      this.w_MOCODVAL = NVL(_Link_.MOCODVAL,space(3))
      this.w_TIPOWP = NVL(_Link_.MOTIPOWP,space(1))
      this.w_CAUDOC = NVL(_Link_.MOCODCAU,space(5))
      this.w_PERIODO = NVL(_Link_.MONUMPER,space(1))
      this.w_PRZVAC = NVL(_Link_.MOPRZVAC,space(1))
      this.w_CODGRN = NVL(_Link_.MOCODGRN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODMOD = space(5)
      endif
      this.w_DESMOD = space(35)
      this.w_OFSERDOC = space(10)
      this.w_SEZVAR = space(1)
      this.w_MOSPEINC = 0
      this.w_MOSPEIMB = 0
      this.w_MOSPETRA = 0
      this.w_FORMULA = space(254)
      this.w_SC1MOD = 0
      this.w_SC2MOD = 0
      this.w_SCPMOD = 0
      this.w_LISMOD = space(5)
      this.w_PAGMOD = space(5)
      this.w_OFCODSPE = space(3)
      this.w_OFCODPOR = space(1)
      this.w_OFTIPFOR = space(1)
      this.w_OFINISCA = space(2)
      this.w_OFGIOSCA = 0
      this.w_OFGIOFIS = 0
      this.w_NUMSCO = 0
      this.w_PATHMOD = space(254)
      this.w_PATHARC = space(254)
      this.w_TIPODOC = space(1)
      this.w_NCODPRF = space(5)
      this.w_MOCODVAL = space(3)
      this.w_TIPOWP = space(1)
      this.w_CAUDOC = space(5)
      this.w_PERIODO = space(1)
      this.w_PRZVAC = space(1)
      this.w_CODGRN = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CFUNC<>'LOAD' OR OFCHKMOD(.w_OFCODMOD, .w_OFDATDOC, .w_OFCODUTE, .w_NCODPRI, .w_NCODGRN, .w_NCODORI, .w_NCODZON, .w_NCODLIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OFCODMOD = space(5)
        this.w_DESMOD = space(35)
        this.w_OFSERDOC = space(10)
        this.w_SEZVAR = space(1)
        this.w_MOSPEINC = 0
        this.w_MOSPEIMB = 0
        this.w_MOSPETRA = 0
        this.w_FORMULA = space(254)
        this.w_SC1MOD = 0
        this.w_SC2MOD = 0
        this.w_SCPMOD = 0
        this.w_LISMOD = space(5)
        this.w_PAGMOD = space(5)
        this.w_OFCODSPE = space(3)
        this.w_OFCODPOR = space(1)
        this.w_OFTIPFOR = space(1)
        this.w_OFINISCA = space(2)
        this.w_OFGIOSCA = 0
        this.w_OFGIOFIS = 0
        this.w_NUMSCO = 0
        this.w_PATHMOD = space(254)
        this.w_PATHARC = space(254)
        this.w_TIPODOC = space(1)
        this.w_NCODPRF = space(5)
        this.w_MOCODVAL = space(3)
        this.w_TIPOWP = space(1)
        this.w_CAUDOC = space(5)
        this.w_PERIODO = space(1)
        this.w_PRZVAC = space(1)
        this.w_CODGRN = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 30 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MOD_OFFE_IDX,3] and i_nFlds+30<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.MOCODICE as MOCODICE107"+ ",link_1_7.MODESCRI as MODESCRI107"+ ",link_1_7.MOSERDOC as MOSERDOC107"+ ",link_1_7.MOSEZVAR as MOSEZVAR107"+ ",link_1_7.MOSPEINC as MOSPEINC107"+ ",link_1_7.MOSPEIMB as MOSPEIMB107"+ ",link_1_7.MOSPETRA as MOSPETRA107"+ ",link_1_7.MONOMDOC as MONOMDOC107"+ ",link_1_7.MOSCOCL1 as MOSCOCL1107"+ ",link_1_7.MOSCOCL2 as MOSCOCL2107"+ ",link_1_7.MOSCOPAG as MOSCOPAG107"+ ",link_1_7.MOCODLIS as MOCODLIS107"+ ",link_1_7.MOCODPAG as MOCODPAG107"+ ",link_1_7.MOCODSPE as MOCODSPE107"+ ",link_1_7.MOCODPOR as MOCODPOR107"+ ",link_1_7.MOTIPFOR as MOTIPFOR107"+ ",link_1_7.MOINISCA as MOINISCA107"+ ",link_1_7.MOGIOSCA as MOGIOSCA107"+ ",link_1_7.MOGIOFIS as MOGIOFIS107"+ ",link_1_7.MONUMSCO as MONUMSCO107"+ ",link_1_7.MOPATMOD as MOPATMOD107"+ ",link_1_7.MOPATARC as MOPATARC107"+ ",link_1_7.MOTIPFOR as MOTIPFOR107"+ ",link_1_7.MOCODPRI as MOCODPRI107"+ ",link_1_7.MOCODVAL as MOCODVAL107"+ ",link_1_7.MOTIPOWP as MOTIPOWP107"+ ",link_1_7.MOCODCAU as MOCODCAU107"+ ",link_1_7.MONUMPER as MONUMPER107"+ ",link_1_7.MOPRZVAC as MOPRZVAC107"+ ",link_1_7.MOCODGRN as MOCODGRN107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on OFF_ERTE.OFCODMOD=link_1_7.MOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+30
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODMOD=link_1_7.MOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+30
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANSCORPO,ANCATCOM,ANCATSCM,ANCODPAG,AN1SCONT,AN2SCONT,ANNUMLIS,ANMCALST,ANMCALSI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLI;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANSCORPO,ANCATCOM,ANCATSCM,ANCODPAG,AN1SCONT,AN2SCONT,ANNUMLIS,ANMCALST,ANMCALSI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_CATCOM = NVL(_Link_.ANCATCOM,space(3))
      this.w_CATSCC = NVL(_Link_.ANCATSCM,space(5))
      this.w_PAGCLI = NVL(_Link_.ANCODPAG,space(5))
      this.w_SC1CLI = NVL(_Link_.AN1SCONT,0)
      this.w_SC2CLI = NVL(_Link_.AN2SCONT,0)
      this.w_LISCLI = NVL(_Link_.ANNUMLIS,space(5))
      this.w_MCALST2 = NVL(_Link_.ANMCALST,space(5))
      this.w_MCALSI2 = NVL(_Link_.ANMCALSI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_FLSCOR = space(1)
      this.w_CATCOM = space(3)
      this.w_CATSCC = space(5)
      this.w_PAGCLI = space(5)
      this.w_SC1CLI = 0
      this.w_SC2CLI = 0
      this.w_LISCLI = space(5)
      this.w_MCALST2 = space(5)
      this.w_MCALSI2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RCODAZI
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_lTable = "PAR_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2], .t., this.PAR_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODAZI,POPRIDEF,POMODOFF,POCODMOD";
                   +" from "+i_cTable+" "+i_lTable+" where POCODAZI="+cp_ToStrODBC(this.w_RCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODAZI',this.w_RCODAZI)
            select POCODAZI,POPRIDEF,POMODOFF,POCODMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RCODAZI = NVL(_Link_.POCODAZI,space(5))
      this.w_PRIDEF = NVL(_Link_.POPRIDEF,0)
      this.w_MODOFF = NVL(_Link_.POMODOFF,space(1))
      this.w_CODMOD = NVL(_Link_.POCODMOD,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_RCODAZI = space(5)
      endif
      this.w_PRIDEF = 0
      this.w_MODOFF = space(1)
      this.w_CODMOD = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.POCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUDOC
  func Link_1_93(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDCODMAG,TDFLSPIN,TDFLSPIM,TDFLSPTR,TDMCALST,TDMCALSI";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDCODMAG,TDFLSPIN,TDFLSPIM,TDFLSPTR,TDMCALST,TDMCALSI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_MAGDOC = NVL(_Link_.TDCODMAG,space(5))
      this.w_FLSPIN = NVL(_Link_.TDFLSPIN,space(1))
      this.w_FLSPIM = NVL(_Link_.TDFLSPIM,space(1))
      this.w_FLSPTR = NVL(_Link_.TDFLSPTR,space(1))
      this.w_MCALST1 = NVL(_Link_.TDMCALST,space(5))
      this.w_MCALSI1 = NVL(_Link_.TDMCALSI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_MAGDOC = space(5)
      this.w_FLSPIN = space(1)
      this.w_FLSPIM = space(1)
      this.w_FLSPTR = space(1)
      this.w_MCALST1 = space(5)
      this.w_MCALSI1 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFCODVAL
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_OFCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VADECUNI,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_OFCODVAL))
          select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VADECUNI,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_OFCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VADECUNI,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_OFCODVAL)+"%");

            select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VADECUNI,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oOFCODVAL_2_1'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VADECUNI,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VADECUNI,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VADECUNI,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_OFCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_OFCODVAL)
            select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VADECUNI,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DECTOT = 0
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DECUNI = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE",CHKDTOBS(this, .w_DATOBSO, .w_OBTEST, "Valuta obsoleta alla data Attuale!"),CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
        endif
        this.w_OFCODVAL = space(3)
        this.w_DESVAL = space(35)
        this.w_DECTOT = 0
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DECUNI = 0
        this.w_SIMVAL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.VACODVAL as VACODVAL201"+ ",link_2_1.VADESVAL as VADESVAL201"+ ",link_2_1.VADECTOT as VADECTOT201"+ ",link_2_1.VADTOBSO as VADTOBSO201"+ ",link_2_1.VADECUNI as VADECUNI201"+ ",link_2_1.VASIMVAL as VASIMVAL201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on OFF_ERTE.OFCODVAL=link_2_1.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODVAL=link_2_1.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OFCODLIS
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_OFCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS,LSDTINVA,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_OFCODLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS,LSDTINVA,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_OFCODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_OFCODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oOFCODLIS_2_8'),i_cWhere,'GSAR_ALI',"Listini",'GSOF_AOF.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS,LSDTINVA,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_OFCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_OFCODLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_SCOLIS = NVL(_Link_.LSFLSCON,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
      this.w_SCOLIS = space(1)
      this.w_VALLIS = space(3)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_OFCODLIS) OR CHKLISD(.w_OFCODLIS,.w_IVALIS,.w_VALLIS, .w_INILIS,.w_FINLIS, .w_FLSCOR, .w_OFCODVAL, .w_OFDATDOC)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OFCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_IVALIS = space(1)
        this.w_SCOLIS = space(1)
        this.w_VALLIS = space(3)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.LSCODLIS as LSCODLIS208"+ ",link_2_8.LSDESLIS as LSDESLIS208"+ ",link_2_8.LSIVALIS as LSIVALIS208"+ ",link_2_8.LSFLSCON as LSFLSCON208"+ ",link_2_8.LSVALLIS as LSVALLIS208"+ ",link_2_8.LSDTINVA as LSDTINVA208"+ ",link_2_8.LSDTOBSO as LSDTOBSO208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on OFF_ERTE.OFCODLIS=link_2_8.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODLIS=link_2_8.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OFCODPAG
  func Link_3_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_OFCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_OFCODPAG))
          select PACODICE,PADESCRI,PADTOBSO,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_OFCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_OFCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_OFCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oOFCODPAG_3_1'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_OFCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_OFCODPAG)
            select PACODICE,PADESCRI,PADTOBSO,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
      this.w_SCOPAG = NVL(_Link_.PASCONTO,0)
      this.w_VALINC = NVL(_Link_.PAVALINC,space(3))
      this.w_SPEINC = NVL(_Link_.PASPEINC,0)
      this.w_VALIN2 = NVL(_Link_.PAVALIN2,space(3))
      this.w_SPEIN2 = NVL(_Link_.PASPEIN2,0)
    else
      if i_cCtrl<>'Load'
        this.w_OFCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_SCOPAG = 0
      this.w_VALINC = space(3)
      this.w_SPEINC = 0
      this.w_VALIN2 = space(3)
      this.w_SPEIN2 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento non definito oppure obsoleto")
        endif
        this.w_OFCODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_SCOPAG = 0
        this.w_VALINC = space(3)
        this.w_SPEINC = 0
        this.w_VALIN2 = space(3)
        this.w_SPEIN2 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_1.PACODICE as PACODICE301"+ ","+cp_TransLinkFldName('link_3_1.PADESCRI')+" as PADESCRI301"+ ",link_3_1.PADTOBSO as PADTOBSO301"+ ",link_3_1.PASCONTO as PASCONTO301"+ ",link_3_1.PAVALINC as PAVALINC301"+ ",link_3_1.PASPEINC as PASPEINC301"+ ",link_3_1.PAVALIN2 as PAVALIN2301"+ ",link_3_1.PASPEIN2 as PASPEIN2301"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_1 on OFF_ERTE.OFCODPAG=link_3_1.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_1"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODPAG=link_3_1.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OFCODCON
  func Link_4_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_BAN',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_OFCODCON)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_OFCODNOM);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCCODRUO,NCTELEFO,NCNUMCEL,NCTELEF2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_OFCODNOM;
                     ,'NCCODCON',trim(this.w_OFCODCON))
          select NCCODICE,NCCODCON,NCPERSON,NCCODRUO,NCTELEFO,NCNUMCEL,NCTELEF2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODCON)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFCODCON) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oOFCODCON_4_6'),i_cWhere,'GSOF_BAN',"Contatti",'GSOF1CON.NOM_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_OFCODNOM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCCODRUO,NCTELEFO,NCNUMCEL,NCTELEF2";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON,NCPERSON,NCCODRUO,NCTELEFO,NCNUMCEL,NCTELEF2;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCCODRUO,NCTELEFO,NCNUMCEL,NCTELEF2";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_OFCODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON,NCPERSON,NCCODRUO,NCTELEFO,NCNUMCEL,NCTELEF2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCCODRUO,NCTELEFO,NCNUMCEL,NCTELEF2";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_OFCODCON);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_OFCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_OFCODNOM;
                       ,'NCCODCON',this.w_OFCODCON)
            select NCCODICE,NCCODCON,NCPERSON,NCCODRUO,NCTELEFO,NCNUMCEL,NCTELEF2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODCON = NVL(_Link_.NCCODCON,space(5))
      this.w_PERSON = NVL(_Link_.NCPERSON,space(35))
      this.w_CODRUO = NVL(_Link_.NCCODRUO,space(5))
      this.w_NCTELEFO = NVL(_Link_.NCTELEFO,space(18))
      this.w_NCNUMCEL = NVL(_Link_.NCNUMCEL,space(18))
      this.w_NCTELEF2 = NVL(_Link_.NCTELEF2,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODCON = space(5)
      endif
      this.w_PERSON = space(35)
      this.w_CODRUO = space(5)
      this.w_NCTELEFO = space(18)
      this.w_NCNUMCEL = space(18)
      this.w_NCTELEF2 = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_CODRUO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OFCODCON = space(5)
        this.w_PERSON = space(35)
        this.w_CODRUO = space(5)
        this.w_NCTELEFO = space(18)
        this.w_NCNUMCEL = space(18)
        this.w_NCTELEF2 = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NOM_CONT_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_6.NCCODCON as NCCODCON406"+ ",link_4_6.NCPERSON as NCPERSON406"+ ",link_4_6.NCCODRUO as NCCODRUO406"+ ",link_4_6.NCTELEFO as NCTELEFO406"+ ",link_4_6.NCNUMCEL as NCNUMCEL406"+ ",link_4_6.NCTELEF2 as NCTELEF2406"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_6 on OFF_ERTE.OFCODCON=link_4_6.NCCODCON"+" and OFF_ERTE.OFCODNOM=link_4_6.NCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_6"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODCON=link_4_6.NCCODCON(+)"'+'+" and OFF_ERTE.OFCODNOM=link_4_6.NCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OFCODUTE
  func Link_4_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_OFCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_OFCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_OFCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oOFCODUTE_4_8'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_OFCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_OFCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODUTE = NVL(_Link_.CODE,0)
      this.w_DESOPE = NVL(_Link_.NAME,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODUTE = 0
      endif
      this.w_DESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPUSERS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_8.CODE as CODE408"+ ",link_4_8.NAME as NAME408"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_8 on OFF_ERTE.OFCODUTE=link_4_8.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_8"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODUTE=link_4_8.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OFCODAGE
  func Link_4_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_OFCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_OFCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_OFCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_OFCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oOFCODAGE_4_11'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_OFCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_OFCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGCZOAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DTOBAGE = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_CODAG2 = NVL(_Link_.AGCZOAGE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DTOBAGE = ctod("  /  /  ")
      this.w_CODAG2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_11.AGCODAGE as AGCODAGE411"+ ",link_4_11.AGDESAGE as AGDESAGE411"+ ",link_4_11.AGDTOBSO as AGDTOBSO411"+ ",link_4_11.AGCZOAGE as AGCZOAGE411"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_11 on OFF_ERTE.OFCODAGE=link_4_11.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_11"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODAGE=link_4_11.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OFCODAG2
  func Link_4_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODAG2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_OFCODAG2)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_OFCODAG2))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODAG2)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFCODAG2) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oOFCODAG2_4_15'),i_cWhere,'GSAR_AGE',"Capoarea",'GSVE1KFT.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODAG2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_OFCODAG2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_OFCODAG2)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODAG2 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESCAPO = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_TIPAG2 = NVL(_Link_.AGTIPAGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODAG2 = space(5)
      endif
      this.w_DESCAPO = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPAG2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPAG2 $ ' C' AND EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto oppure non capoarea")
        endif
        this.w_OFCODAG2 = space(5)
        this.w_DESCAPO = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPAG2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODAG2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_15.AGCODAGE as AGCODAGE415"+ ",link_4_15.AGDESAGE as AGDESAGE415"+ ",link_4_15.AGDTOBSO as AGDTOBSO415"+ ",link_4_15.AGTIPAGE as AGTIPAGE415"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_15 on OFF_ERTE.OFCODAG2=link_4_15.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_15"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODAG2=link_4_15.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OFCODPOR
  func Link_4_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_lTable = "PORTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2], .t., this.PORTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APO',True,'PORTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" POCODPOR like "+cp_ToStrODBC(trim(this.w_OFCODPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by POCODPOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'POCODPOR',trim(this.w_OFCODPOR))
          select POCODPOR,PODESPOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by POCODPOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODPOR)==trim(_Link_.POCODPOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFCODPOR) and !this.bDontReportError
            deferred_cp_zoom('PORTI','*','POCODPOR',cp_AbsName(oSource.parent,'oOFCODPOR_4_18'),i_cWhere,'GSAR_APO',"Porti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                     +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',oSource.xKey(1))
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                   +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(this.w_OFCODPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',this.w_OFCODPOR)
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODPOR = NVL(_Link_.POCODPOR,space(1))
      this.w_DESPORT = NVL(cp_TransLoadField('_Link_.PODESPOR'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODPOR = space(1)
      endif
      this.w_DESPORT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])+'\'+cp_ToStr(_Link_.POCODPOR,1)
      cp_ShowWarn(i_cKey,this.PORTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PORTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_18.POCODPOR as POCODPOR418"+ ","+cp_TransLinkFldName('link_4_18.PODESPOR')+" as PODESPOR418"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_18 on OFF_ERTE.OFCODPOR=link_4_18.POCODPOR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_18"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODPOR=link_4_18.POCODPOR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OFCODSPE
  func Link_4_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_lTable = "MODASPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2], .t., this.MODASPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASP',True,'MODASPED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SPCODSPE like "+cp_ToStrODBC(trim(this.w_OFCODSPE)+"%");

          i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+",SPMCCODI,SPMCCODT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SPCODSPE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SPCODSPE',trim(this.w_OFCODSPE))
          select SPCODSPE,SPDESSPE,SPMCCODI,SPMCCODT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SPCODSPE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODSPE)==trim(_Link_.SPCODSPE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFCODSPE) and !this.bDontReportError
            deferred_cp_zoom('MODASPED','*','SPCODSPE',cp_AbsName(oSource.parent,'oOFCODSPE_4_21'),i_cWhere,'GSAR_ASP',"Spedizioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+",SPMCCODI,SPMCCODT";
                     +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',oSource.xKey(1))
            select SPCODSPE,SPDESSPE,SPMCCODI,SPMCCODT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+",SPMCCODI,SPMCCODT";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(this.w_OFCODSPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',this.w_OFCODSPE)
            select SPCODSPE,SPDESSPE,SPMCCODI,SPMCCODT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODSPE = NVL(_Link_.SPCODSPE,space(3))
      this.w_DESSPE = NVL(cp_TransLoadField('_Link_.SPDESSPE'),space(35))
      this.w_MCALSI3 = NVL(_Link_.SPMCCODI,space(5))
      this.w_MCALST3 = NVL(_Link_.SPMCCODT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODSPE = space(3)
      endif
      this.w_DESSPE = space(35)
      this.w_MCALSI3 = space(5)
      this.w_MCALST3 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])+'\'+cp_ToStr(_Link_.SPCODSPE,1)
      cp_ShowWarn(i_cKey,this.MODASPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODASPED_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_21.SPCODSPE as SPCODSPE421"+ ","+cp_TransLinkFldName('link_4_21.SPDESSPE')+" as SPDESSPE421"+ ",link_4_21.SPMCCODI as SPMCCODI421"+ ",link_4_21.SPMCCODT as SPMCCODT421"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_21 on OFF_ERTE.OFCODSPE=link_4_21.SPCODSPE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_21"
          i_cKey=i_cKey+'+" and OFF_ERTE.OFCODSPE=link_4_21.SPCODSPE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OPERAT
  func Link_6_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OPERAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_OPERAT);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_OPERAT)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_OPERAT) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oOPERAT_6_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OPERAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_OPERAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_OPERAT)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OPERAT = NVL(_Link_.CODE,0)
      this.w_NAME = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OPERAT = 0
      endif
      this.w_NAME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OPERAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTINI
  func Link_6_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_CONTINI)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_OFCODNOM);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_OFCODNOM;
                     ,'NCCODCON',trim(this.w_CONTINI))
          select NCCODICE,NCCODCON,NCPERSON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTINI)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONTINI) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oCONTINI_6_19'),i_cWhere,'',"Contatti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_OFCODNOM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_OFCODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_CONTINI);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_OFCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_OFCODNOM;
                       ,'NCCODCON',this.w_CONTINI)
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTINI = NVL(_Link_.NCCODCON,space(5))
      this.w_DESCPER = NVL(_Link_.NCPERSON,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CONTINI = space(5)
      endif
      this.w_DESCPER = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOFCODNOM_1_4.value==this.w_OFCODNOM)
      this.oPgFrm.Page1.oPag.oOFCODNOM_1_4.value=this.w_OFCODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oOFCODMOD_1_7.value==this.w_OFCODMOD)
      this.oPgFrm.Page1.oPag.oOFCODMOD_1_7.value=this.w_OFCODMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oOFNUMDOC_1_14.value==this.w_OFNUMDOC)
      this.oPgFrm.Page1.oPag.oOFNUMDOC_1_14.value=this.w_OFNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFSERDOC_1_16.value==this.w_OFSERDOC)
      this.oPgFrm.Page1.oPag.oOFSERDOC_1_16.value=this.w_OFSERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDATDOC_1_18.value==this.w_OFDATDOC)
      this.oPgFrm.Page1.oPag.oOFDATDOC_1_18.value=this.w_OFDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFRIFDES_1_20.value==this.w_OFRIFDES)
      this.oPgFrm.Page1.oPag.oOFRIFDES_1_20.value=this.w_OFRIFDES
    endif
    if not(this.oPgFrm.Page1.oPag.oOFNUMVER_1_21.value==this.w_OFNUMVER)
      this.oPgFrm.Page1.oPag.oOFNUMVER_1_21.value=this.w_OFNUMVER
    endif
    if not(this.oPgFrm.Page1.oPag.oOFSTATUS_1_23.RadioValue()==this.w_OFSTATUS)
      this.oPgFrm.Page1.oPag.oOFSTATUS_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOD_1_27.value==this.w_DESMOD)
      this.oPgFrm.Page1.oPag.oDESMOD_1_27.value=this.w_DESMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMDES_1_33.value==this.w_NOMDES)
      this.oPgFrm.Page1.oPag.oNOMDES_1_33.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMIND_1_34.value==this.w_NOMIND)
      this.oPgFrm.Page1.oPag.oNOMIND_1_34.value=this.w_NOMIND
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMCAP_1_35.value==this.w_NOMCAP)
      this.oPgFrm.Page1.oPag.oNOMCAP_1_35.value=this.w_NOMCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMLOC_1_36.value==this.w_NOMLOC)
      this.oPgFrm.Page1.oPag.oNOMLOC_1_36.value=this.w_NOMLOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMPRO_1_37.value==this.w_NOMPRO)
      this.oPgFrm.Page1.oPag.oNOMPRO_1_37.value=this.w_NOMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDATSCA_1_39.value==this.w_OFDATSCA)
      this.oPgFrm.Page1.oPag.oOFDATSCA_1_39.value=this.w_OFDATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDATINV_1_40.value==this.w_OFDATINV)
      this.oPgFrm.Page1.oPag.oOFDATINV_1_40.value=this.w_OFDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDATCHI_1_41.value==this.w_OFDATCHI)
      this.oPgFrm.Page1.oPag.oOFDATCHI_1_41.value=this.w_OFDATCHI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTELEFO_1_103.value==this.w_NOTELEFO)
      this.oPgFrm.Page1.oPag.oNOTELEFO_1_103.value=this.w_NOTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oNONUMCEL_1_104.value==this.w_NONUMCEL)
      this.oPgFrm.Page1.oPag.oNONUMCEL_1_104.value=this.w_NONUMCEL
    endif
    if not(this.oPgFrm.Page2.oPag.oOFCODVAL_2_1.value==this.w_OFCODVAL)
      this.oPgFrm.Page2.oPag.oOFCODVAL_2_1.value=this.w_OFCODVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVAL_2_6.value==this.w_DESVAL)
      this.oPgFrm.Page2.oPag.oDESVAL_2_6.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oOFCODLIS_2_8.value==this.w_OFCODLIS)
      this.oPgFrm.Page2.oPag.oOFCODLIS_2_8.value=this.w_OFCODLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLIS_2_17.value==this.w_DESLIS)
      this.oPgFrm.Page2.oPag.oDESLIS_2_17.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page3.oPag.oOFCODPAG_3_1.value==this.w_OFCODPAG)
      this.oPgFrm.Page3.oPag.oOFCODPAG_3_1.value=this.w_OFCODPAG
    endif
    if not(this.oPgFrm.Page3.oPag.oDESPAG_3_2.value==this.w_DESPAG)
      this.oPgFrm.Page3.oPag.oDESPAG_3_2.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page3.oPag.oOFSCOCL1_3_3.value==this.w_OFSCOCL1)
      this.oPgFrm.Page3.oPag.oOFSCOCL1_3_3.value=this.w_OFSCOCL1
    endif
    if not(this.oPgFrm.Page3.oPag.oOFSCOCL2_3_4.value==this.w_OFSCOCL2)
      this.oPgFrm.Page3.oPag.oOFSCOCL2_3_4.value=this.w_OFSCOCL2
    endif
    if not(this.oPgFrm.Page3.oPag.oOFSCOPAG_3_5.value==this.w_OFSCOPAG)
      this.oPgFrm.Page3.oPag.oOFSCOPAG_3_5.value=this.w_OFSCOPAG
    endif
    if not(this.oPgFrm.Page3.oPag.oTOTRIP_3_6.value==this.w_TOTRIP)
      this.oPgFrm.Page3.oPag.oTOTRIP_3_6.value=this.w_TOTRIP
    endif
    if not(this.oPgFrm.Page3.oPag.oOFSCONTI_3_20.value==this.w_OFSCONTI)
      this.oPgFrm.Page3.oPag.oOFSCONTI_3_20.value=this.w_OFSCONTI
    endif
    if not(this.oPgFrm.Page3.oPag.oOFFLFOSC_3_22.RadioValue()==this.w_OFFLFOSC)
      this.oPgFrm.Page3.oPag.oOFFLFOSC_3_22.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oOFSPEINC_3_23.value==this.w_OFSPEINC)
      this.oPgFrm.Page3.oPag.oOFSPEINC_3_23.value=this.w_OFSPEINC
    endif
    if not(this.oPgFrm.Page3.oPag.oOFSPEIMB_3_24.value==this.w_OFSPEIMB)
      this.oPgFrm.Page3.oPag.oOFSPEIMB_3_24.value=this.w_OFSPEIMB
    endif
    if not(this.oPgFrm.Page3.oPag.oOFSPETRA_3_25.value==this.w_OFSPETRA)
      this.oPgFrm.Page3.oPag.oOFSPETRA_3_25.value=this.w_OFSPETRA
    endif
    if not(this.oPgFrm.Page3.oPag.oOFIMPOFF_3_26.value==this.w_OFIMPOFF)
      this.oPgFrm.Page3.oPag.oOFIMPOFF_3_26.value=this.w_OFIMPOFF
    endif
    if not(this.oPgFrm.Page3.oPag.oOF__NOTE_3_28.value==this.w_OF__NOTE)
      this.oPgFrm.Page3.oPag.oOF__NOTE_3_28.value=this.w_OF__NOTE
    endif
    if not(this.oPgFrm.Page3.oPag.ocodval_3_32.value==this.w_codval)
      this.oPgFrm.Page3.oPag.ocodval_3_32.value=this.w_codval
    endif
    if not(this.oPgFrm.Page4.oPag.oOFPATFWP_4_1.value==this.w_OFPATFWP)
      this.oPgFrm.Page4.oPag.oOFPATFWP_4_1.value=this.w_OFPATFWP
    endif
    if not(this.oPgFrm.Page4.oPag.oOFPATPDF_4_3.value==this.w_OFPATPDF)
      this.oPgFrm.Page4.oPag.oOFPATPDF_4_3.value=this.w_OFPATPDF
    endif
    if not(this.oPgFrm.Page4.oPag.oOFCODCON_4_6.value==this.w_OFCODCON)
      this.oPgFrm.Page4.oPag.oOFCODCON_4_6.value=this.w_OFCODCON
    endif
    if not(this.oPgFrm.Page4.oPag.oOFDATRIC_4_7.value==this.w_OFDATRIC)
      this.oPgFrm.Page4.oPag.oOFDATRIC_4_7.value=this.w_OFDATRIC
    endif
    if not(this.oPgFrm.Page4.oPag.oOFCODUTE_4_8.value==this.w_OFCODUTE)
      this.oPgFrm.Page4.oPag.oOFCODUTE_4_8.value=this.w_OFCODUTE
    endif
    if not(this.oPgFrm.Page4.oPag.oOFNUMPRI_4_10.value==this.w_OFNUMPRI)
      this.oPgFrm.Page4.oPag.oOFNUMPRI_4_10.value=this.w_OFNUMPRI
    endif
    if not(this.oPgFrm.Page4.oPag.oOFCODAGE_4_11.value==this.w_OFCODAGE)
      this.oPgFrm.Page4.oPag.oOFCODAGE_4_11.value=this.w_OFCODAGE
    endif
    if not(this.oPgFrm.Page4.oPag.oOFFLFOAG_4_12.RadioValue()==this.w_OFFLFOAG)
      this.oPgFrm.Page4.oPag.oOFFLFOAG_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oOFCODAG2_4_15.value==this.w_OFCODAG2)
      this.oPgFrm.Page4.oPag.oOFCODAG2_4_15.value=this.w_OFCODAG2
    endif
    if not(this.oPgFrm.Page4.oPag.oOFFLFOA2_4_16.RadioValue()==this.w_OFFLFOA2)
      this.oPgFrm.Page4.oPag.oOFFLFOA2_4_16.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oOFCODPOR_4_18.value==this.w_OFCODPOR)
      this.oPgFrm.Page4.oPag.oOFCODPOR_4_18.value=this.w_OFCODPOR
    endif
    if not(this.oPgFrm.Page4.oPag.oOFFLFOPO_4_19.RadioValue()==this.w_OFFLFOPO)
      this.oPgFrm.Page4.oPag.oOFFLFOPO_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oOFCODSPE_4_21.value==this.w_OFCODSPE)
      this.oPgFrm.Page4.oPag.oOFCODSPE_4_21.value=this.w_OFCODSPE
    endif
    if not(this.oPgFrm.Page4.oPag.oOFFLFOSP_4_22.RadioValue()==this.w_OFFLFOSP)
      this.oPgFrm.Page4.oPag.oOFFLFOSP_4_22.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDESOPE_4_27.value==this.w_DESOPE)
      this.oPgFrm.Page4.oPag.oDESOPE_4_27.value=this.w_DESOPE
    endif
    if not(this.oPgFrm.Page4.oPag.oDESAGE_4_28.value==this.w_DESAGE)
      this.oPgFrm.Page4.oPag.oDESAGE_4_28.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page4.oPag.oPERSON_4_30.value==this.w_PERSON)
      this.oPgFrm.Page4.oPag.oPERSON_4_30.value=this.w_PERSON
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCAPO_4_35.value==this.w_DESCAPO)
      this.oPgFrm.Page4.oPag.oDESCAPO_4_35.value=this.w_DESCAPO
    endif
    if not(this.oPgFrm.Page4.oPag.oDESPORT_4_36.value==this.w_DESPORT)
      this.oPgFrm.Page4.oPag.oDESPORT_4_36.value=this.w_DESPORT
    endif
    if not(this.oPgFrm.Page4.oPag.oDESSPE_4_37.value==this.w_DESSPE)
      this.oPgFrm.Page4.oPag.oDESSPE_4_37.value=this.w_DESSPE
    endif
    if not(this.oPgFrm.Page4.oPag.oNCTELEFO_4_44.value==this.w_NCTELEFO)
      this.oPgFrm.Page4.oPag.oNCTELEFO_4_44.value=this.w_NCTELEFO
    endif
    if not(this.oPgFrm.Page4.oPag.oNCNUMCEL_4_45.value==this.w_NCNUMCEL)
      this.oPgFrm.Page4.oPag.oNCNUMCEL_4_45.value=this.w_NCNUMCEL
    endif
    if not(this.oPgFrm.Page4.oPag.oNCTELEF2_4_48.value==this.w_NCTELEF2)
      this.oPgFrm.Page4.oPag.oNCTELEF2_4_48.value=this.w_NCTELEF2
    endif
    if not(this.oPgFrm.Page5.oPag.oNOMFIL_5_1.value==this.w_NOMFIL)
      this.oPgFrm.Page5.oPag.oNOMFIL_5_1.value=this.w_NOMFIL
    endif
    if not(this.oPgFrm.Page5.oPag.oOGGETT_5_2.value==this.w_OGGETT)
      this.oPgFrm.Page5.oPag.oOGGETT_5_2.value=this.w_OGGETT
    endif
    if not(this.oPgFrm.Page5.oPag.oNOTE_5_3.value==this.w_NOTE)
      this.oPgFrm.Page5.oPag.oNOTE_5_3.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page6.oPag.oNAME_6_5.value==this.w_NAME)
      this.oPgFrm.Page6.oPag.oNAME_6_5.value=this.w_NAME
    endif
    if not(this.oPgFrm.Page6.oPag.oOPERAT_6_6.value==this.w_OPERAT)
      this.oPgFrm.Page6.oPag.oOPERAT_6_6.value=this.w_OPERAT
    endif
    if not(this.oPgFrm.Page6.oPag.oSTAT1_6_7.RadioValue()==this.w_STAT1)
      this.oPgFrm.Page6.oPag.oSTAT1_6_7.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oSTAT2_6_8.RadioValue()==this.w_STAT2)
      this.oPgFrm.Page6.oPag.oSTAT2_6_8.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oDATAINI_6_10.value==this.w_DATAINI)
      this.oPgFrm.Page6.oPag.oDATAINI_6_10.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page6.oPag.oORAINI_6_11.value==this.w_ORAINI)
      this.oPgFrm.Page6.oPag.oORAINI_6_11.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page6.oPag.oMININI_6_12.value==this.w_MININI)
      this.oPgFrm.Page6.oPag.oMININI_6_12.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page6.oPag.oDATAFIN_6_14.value==this.w_DATAFIN)
      this.oPgFrm.Page6.oPag.oDATAFIN_6_14.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page6.oPag.oORAFIN_6_15.value==this.w_ORAFIN)
      this.oPgFrm.Page6.oPag.oORAFIN_6_15.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page6.oPag.oMINFIN_6_16.value==this.w_MINFIN)
      this.oPgFrm.Page6.oPag.oMINFIN_6_16.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page6.oPag.oSUBJECT_6_18.value==this.w_SUBJECT)
      this.oPgFrm.Page6.oPag.oSUBJECT_6_18.value=this.w_SUBJECT
    endif
    if not(this.oPgFrm.Page6.oPag.oCONTINI_6_19.value==this.w_CONTINI)
      this.oPgFrm.Page6.oPag.oCONTINI_6_19.value=this.w_CONTINI
    endif
    if not(this.oPgFrm.Page6.oPag.oPRIOINI_6_20.value==this.w_PRIOINI)
      this.oPgFrm.Page6.oPag.oPRIOINI_6_20.value=this.w_PRIOINI
    endif
    if not(this.oPgFrm.Page6.oPag.oDESCPER_6_49.value==this.w_DESCPER)
      this.oPgFrm.Page6.oPag.oDESCPER_6_49.value=this.w_DESCPER
    endif
    if not(this.oPgFrm.Page6.oPag.oPRIOINI_6_52.RadioValue()==this.w_PRIOINI)
      this.oPgFrm.Page6.oPag.oPRIOINI_6_52.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'OFF_ERTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_OFCODNOM)) or not((EMPTY(.w_DTOBS1) OR .w_DTOBS1>.w_OFDATDOC) AND .w_TIPNOM<>'G' AND .w_TIPNOM<>'F'))  and (.w_OFNUMVER<2 OR .cFunction = 'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODNOM_1_4.SetFocus()
            i_bnoObbl = !empty(.w_OFCODNOM)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Nominativo inesistente o obsoleto oppure di tipo congelato")
          case   ((empty(.w_OFCODMOD)) or not(.w_CFUNC<>'LOAD' OR OFCHKMOD(.w_OFCODMOD, .w_OFDATDOC, .w_OFCODUTE, .w_NCODPRI, .w_NCODGRN, .w_NCODORI, .w_NCODZON, .w_NCODLIN)))  and (.w_OFNUMVER<2 AND .cFunction = 'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODMOD_1_7.SetFocus()
            i_bnoObbl = !empty(.w_OFCODMOD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_OFNUMDOC))  and (.w_OFNUMVER<2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFNUMDOC_1_14.SetFocus()
            i_bnoObbl = !empty(.w_OFNUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_OFDATDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFDATDOC_1_18.SetFocus()
            i_bnoObbl = !empty(.w_OFDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_OFCODVAL)) or not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE",CHKDTOBS(this, .w_DATOBSO, .w_OBTEST, "Valuta obsoleta alla data Attuale!"),CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.))))  and (EMPTY(.w_OFCODVAL) OR .cFunction<>'Edit')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOFCODVAL_2_1.SetFocus()
            i_bnoObbl = !empty(.w_OFCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
          case   not(EMPTY(.w_OFCODLIS) OR CHKLISD(.w_OFCODLIS,.w_IVALIS,.w_VALLIS, .w_INILIS,.w_FINLIS, .w_FLSCOR, .w_OFCODVAL, .w_OFDATDOC))  and not(empty(.w_OFCODLIS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOFCODLIS_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(empty(.w_OFCODPAG))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oOFCODPAG_3_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento non definito oppure obsoleto")
          case   not(NOT EMPTY(.w_CODRUO))  and not(empty(.w_OFCODCON))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oOFCODCON_4_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPAG2 $ ' C' AND EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (g_PERAGE='S' AND NOT EMPTY(.w_OFCODAGE))  and not(empty(.w_OFCODAG2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oOFCODAG2_4_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto oppure non capoarea")
          case   not(VAL(.w_ORAINI) < 24)  and (.cFunction<>'Load')
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oORAINI_6_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (.cFunction<>'Load')
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMININI_6_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_ORAFIN) < 24)  and (.cFunction<>'Load')
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oORAFIN_6_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and (.cFunction<>'Load')
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMINFIN_6_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSOF_MSO.CheckForm()
      if i_bres
        i_bres=  .GSOF_MSO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSOF_MDO.CheckForm()
      if i_bres
        i_bres=  .GSOF_MDO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSOF_MAS.CheckForm()
      if i_bres
        i_bres=  .GSOF_MAS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsof_aof
      * --- Controlli ----
      if i_bRes=.t. and .cFunction = 'Edit'
         if not OFCHKMOD(.w_OFCODMOD, .w_OFDATDOC, .w_OFCODUTE, .w_NCODPRI, .w_NCODGRN, .w_NCODORI, .w_NCODZON, .w_NCODLIN)
           i_bnoChk = .t.
           i_bRes = .f.
         endif
      endif
      if i_bRes=.t.
         .w_BRES=.t.
          * ---- Ricalcola Sempre le spese accessorie
          .notifyevent('Calspeacc')
         if .w_OFFLFOSC<>'S'
           * ---- Ricalcola Sempre lo Sconto Globale
           .w_OFSCONTI = Calsco(.w_TOTMERCE, .w_OFSCOCL1, .w_OFSCOCL2, .w_OFSCOPAG, .w_DECTOT)
         endif
         AH_mSG('Controlli finali',.T.)
           .NotifyEvent('ControlliFinali')
           WAIT CLEAR
           if .w_BRES=.f.
              i_bRes=.f.
           endif
      endif
      * --- Controllo campi obbligatori
      if i_bRes=.T.
         do case
         case .w_OFSTATUS = 'A'
            if EMPTY(.w_OFDATINV)
             i_bRes=.F.
             i_bnoChk=.F.
             i_cErrorMsg=Ah_MsgFormat("Data di invio obbligatoria")
            endif
         endcase
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_OFSERIAL = this.w_OFSERIAL
    this.o_OFCODNOM = this.w_OFCODNOM
    this.o_OFCODMOD = this.w_OFCODMOD
    this.o_OFDATDOC = this.w_OFDATDOC
    this.o_OFSTATUS = this.w_OFSTATUS
    this.o_CAUDOC = this.w_CAUDOC
    this.o_OFCODVAL = this.w_OFCODVAL
    this.o_OFCODLIS = this.w_OFCODLIS
    this.o_OFCODPAG = this.w_OFCODPAG
    this.o_OFSCOCL1 = this.w_OFSCOCL1
    this.o_OFSCOCL2 = this.w_OFSCOCL2
    this.o_OFSCOPAG = this.w_OFSCOPAG
    this.o_TOTMERCE = this.w_TOTMERCE
    this.o_MOSPEINC = this.w_MOSPEINC
    this.o_OFCODCON = this.w_OFCODCON
    this.o_OFCODSPE = this.w_OFCODSPE
    this.o_STAT1 = this.w_STAT1
    this.o_STAT2 = this.w_STAT2
    this.o_DATAINI = this.w_DATAINI
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    this.o_DATAFIN = this.w_DATAFIN
    this.o_ORAFIN = this.w_ORAFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_SUBJECT = this.w_SUBJECT
    this.o_PRIOINI = this.w_PRIOINI
    this.o_NOMINI = this.w_NOMINI
    this.o_NOMFIN = this.w_NOMFIN
    this.o_MCALST1 = this.w_MCALST1
    this.o_MCALSI4 = this.w_MCALSI4
    this.o_MCALST4 = this.w_MCALST4
    this.o_MCALSI3 = this.w_MCALSI3
    this.o_MCALST3 = this.w_MCALST3
    this.o_MCALSI2 = this.w_MCALSI2
    this.o_MCALST2 = this.w_MCALST2
    this.o_MCALSI1 = this.w_MCALSI1
    * --- GSOF_MSO : Depends On
    this.GSOF_MSO.SaveDependsOn()
    * --- GSOF_MDO : Depends On
    this.GSOF_MDO.SaveDependsOn()
    * --- GSOF_MAS : Depends On
    this.GSOF_MAS.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=NOT (this.w_OFSTATUS $ 'CV' OR (this.w_OFSTATUS='A' AND this.w_MODOFF='S'))
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Offerta in stato "+ iif(this.w_ofstatus='V', "versione chiusa", iif(this.w_ofstatus='C', "confermata", "inviata")) + "; non modificabile"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsof_aofPag1 as StdContainer
  Width  = 803
  height = 471
  stdWidth  = 803
  stdheight = 471
  resizeXpos=353
  resizeYpos=359
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOFCODNOM_1_4 as StdField with uid="NZTOIINCXN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_OFCODNOM", cQueryName = "OFCODNOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Nominativo inesistente o obsoleto oppure di tipo congelato",;
    ToolTipText = "Codice del nominativo",;
    HelpContextID = 134828595,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=86, Top=11, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_OFCODNOM"

  func oOFCODNOM_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFNUMVER<2 OR .cFunction = 'Load')
    endwith
   endif
  endfunc

  func oOFCODNOM_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_OFCODCON)
        bRes2=.link_4_6('Full')
      endif
      if .not. empty(.w_CONTINI)
        bRes2=.link_6_19('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oOFCODNOM_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODNOM_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oOFCODNOM_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Elenco nominativi",'GSOF_AOF.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oOFCODNOM_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_OFCODNOM
     i_obj.ecpSave()
  endproc

  add object oOFCODMOD_1_7 as StdField with uid="UOKZWHISGP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_OFCODMOD", cQueryName = "OFCODMOD",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Modello di riferimento",;
    HelpContextID = 118051370,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=86, Top=136, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MOD_OFFE", cZoomOnZoom="GSOF_AMO", oKey_1_1="MOCODICE", oKey_1_2="this.w_OFCODMOD"

  func oOFCODMOD_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFNUMVER<2 AND .cFunction = 'Load')
    endwith
   endif
  endfunc

  func oOFCODMOD_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODMOD_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODMOD_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_OFFE','*','MOCODICE',cp_AbsName(this.parent,'oOFCODMOD_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AMO',"Modelli offerte",'GSOF_AOF.MOD_OFFE_VZM',this.parent.oContained
  endproc
  proc oOFCODMOD_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AMO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_OFCODMOD
     i_obj.ecpSave()
  endproc

  add object oOFNUMDOC_1_14 as StdField with uid="ZPGHQKEKMD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_OFNUMDOC", cQueryName = "OFNUMDOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento di offerta",;
    HelpContextID = 245367337,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=86, Top=161, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oOFNUMDOC_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFNUMVER<2)
    endwith
   endif
  endfunc

  proc oOFNUMDOC_1_14.mBefore
    with this.Parent.oContained
      this.cQueryName = "OFCODESE,OFSERDOC,OFNUMDOC"
    endwith
  endproc

  add object oOFSERDOC_1_16 as StdField with uid="QEWXBEZQWM",rtseq=14,rtrep=.f.,;
    cFormVar = "w_OFSERDOC", cQueryName = "OFSERDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento di offerta",;
    HelpContextID = 249582121,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=213, Top=161, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oOFSERDOC_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFNUMVER<2)
    endwith
   endif
  endfunc

  add object oOFDATDOC_1_18 as StdField with uid="MWIBDWCYBP",rtseq=15,rtrep=.f.,;
    cFormVar = "w_OFDATDOC", cQueryName = "OFDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento di offerta",;
    HelpContextID = 251355689,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=389, Top=161

  add object oOFRIFDES_1_20 as StdField with uid="UXJDODCEKI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_OFRIFDES", cQueryName = "OFRIFDES",;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale riferimento descrittivo",;
    HelpContextID = 237257273,;
   bGlobalFont=.t.,;
    Height=21, Width=380, Left=86, Top=186, InputMask=replicate('X',45)

  add object oOFNUMVER_1_21 as StdField with uid="RGDHYLRFQY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_OFNUMVER", cQueryName = "OFNUMVER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo versione",;
    HelpContextID = 257949128,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=562, Top=36, cSayPict='"9999"', cGetPict='"9999"'


  add object oOFSTATUS_1_23 as StdCombo with uid="WPQCMLNQPE",rtseq=18,rtrep=.f.,left=562,top=11,width=116,height=21;
    , ToolTipText = "Stato: in corso; inviata; confermata; versione chiusa; sospesa; rifiutata";
    , HelpContextID = 232739385;
    , cFormVar="w_OFSTATUS",RowSource=""+"In corso,"+"Inviata,"+"Confermata,"+"Versione chiusa,"+"Rifiutata,"+"Sospesa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFSTATUS_1_23.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'V',;
    iif(this.value =5,'R',;
    iif(this.value =6,'S',;
    space(1))))))))
  endfunc
  func oOFSTATUS_1_23.GetRadio()
    this.Parent.oContained.w_OFSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oOFSTATUS_1_23.SetRadio()
    this.Parent.oContained.w_OFSTATUS=trim(this.Parent.oContained.w_OFSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_OFSTATUS=='I',1,;
      iif(this.Parent.oContained.w_OFSTATUS=='A',2,;
      iif(this.Parent.oContained.w_OFSTATUS=='C',3,;
      iif(this.Parent.oContained.w_OFSTATUS=='V',4,;
      iif(this.Parent.oContained.w_OFSTATUS=='R',5,;
      iif(this.Parent.oContained.w_OFSTATUS=='S',6,;
      0))))))
  endfunc


  add object oBtn_1_24 as StdButton with uid="QULVRYBYAS",left=702, top=11, width=48,height=45,;
    CpPicture="bmp\Cerifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare l'offerta";
    , HelpContextID = 238234247;
    , Caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSOF_BED(this.Parent.oContained,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_OFSTATUS $ 'IA' AND .cFunction = 'Query' AND NOT EMPTY(.w_OFCODNOM))
      endwith
    endif
  endfunc

  func oBtn_1_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((Not ( .w_OFSTATUS $ 'IA' AND .cFunction = 'Query' AND NOT EMPTY(.w_OFCODNOM) )))
     endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="ADLKSOBJIT",left=702, top=57, width=48,height=45,;
    CpPicture="bmp\NewOffe.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per generare una nuova versione di offerta e chiudere quella in corso";
    , HelpContextID = 128365733;
    , Caption='\<Nuova Ver';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        do GSOF_BCV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_OFSTATUS $ 'IA' AND .cFunction <> 'Load' AND NOT EMPTY(.w_OFCODNOM))
      endwith
    endif
  endfunc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_OFSTATUS $ 'CVRS')
     endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="LZVVZJATNU",left=702, top=103, width=48,height=45,;
    CpPicture="bmp\OffPrec.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla precedente versione dell'offerta";
    , HelpContextID = 1897403;
    , Caption='\<Ver. prec.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        do GSOF_BVP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OFSERPRE))
      endwith
    endif
  endfunc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_OFSERPRE))
     endwith
    endif
  endfunc

  add object oDESMOD_1_27 as StdField with uid="SGCGWPCXUC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESMOD", cQueryName = "DESMOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione modello offerta",;
    HelpContextID = 246960182,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=159, Top=136, InputMask=replicate('X',35)

  add object oNOMDES_1_33 as StdField with uid="ELFBEHCBNZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del nominativo",;
    HelpContextID = 219085526,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=86, Top=36, InputMask=replicate('X',40)

  add object oNOMIND_1_34 as StdField with uid="NUSLGCMCZX",rtseq=21,rtrep=.f.,;
    cFormVar = "w_NOMIND", cQueryName = "NOMIND",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo",;
    HelpContextID = 245627606,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=86, Top=61, InputMask=replicate('X',35)

  add object oNOMCAP_1_35 as StdField with uid="DQHURHCKRX",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NOMCAP", cQueryName = "NOMCAP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "CAP",;
    HelpContextID = 164494038,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=86, Top=86, InputMask=replicate('X',8)

  add object oNOMLOC_1_36 as StdField with uid="PMZHYPZKOB",rtseq=23,rtrep=.f.,;
    cFormVar = "w_NOMLOC", cQueryName = "NOMLOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit�",;
    HelpContextID = 230095574,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=159, Top=86, InputMask=replicate('X',30)

  add object oNOMPRO_1_37 as StdField with uid="VUIHGANBQA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_NOMPRO", cQueryName = "NOMPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 166394582,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=388, Top=86, InputMask=replicate('X',2)

  add object oOFDATSCA_1_39 as StdField with uid="BHMZLHPYOI",rtseq=26,rtrep=.f.,;
    cFormVar = "w_OFDATSCA", cQueryName = "OFDATSCA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza offerta",;
    HelpContextID = 234578471,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=602, Top=136

  add object oOFDATINV_1_40 as StdField with uid="PNABZQCFMD",rtseq=27,rtrep=.f.,;
    cFormVar = "w_OFDATINV", cQueryName = "OFDATINV",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di invio offerta",;
    HelpContextID = 66806332,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=602, Top=161

  func oOFDATINV_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFSTATUS = 'A')
    endwith
   endif
  endfunc

  add object oOFDATCHI_1_41 as StdField with uid="EUNJLEHBMR",rtseq=28,rtrep=.f.,;
    cFormVar = "w_OFDATCHI", cQueryName = "OFDATCHI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data chiusura offerta",;
    HelpContextID = 33856977,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=602, Top=186


  add object oLinkPC_1_42 as stdDynamicChildContainer with uid="DLMMJMPJRO",left=17, top=209, width=765, height=262, bOnScreen=.t.;



  add object oBtn_1_46 as StdButton with uid="AITASDRNUF",left=702, top=211, width=48,height=45,;
    CpPicture="bmp\CONTRATT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per interrogare il documento di conferma associato all'offerta";
    , HelpContextID = 19858948;
    , Caption='\<Doc. Conf.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      with this.Parent.oContained
        do GSOF_BZM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_46.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OFRIFDOC) AND .cFunction = 'Query')
      endwith
    endif
  endfunc

  func oBtn_1_46.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_OFRIFDOC))
     endwith
    endif
  endfunc


  add object oBtn_1_47 as StdButton with uid="YCMBZTYYLM",left=702, top=257, width=48,height=45,;
    CpPicture="bmp\word.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento Word di offerta generato.";
    , HelpContextID = 249694086;
    , Caption='\<File W.P.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      with this.Parent.oContained
        GSOF_BWP(this.Parent.oContained,"W")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OFPATFWP) AND NOT .cFunction $ 'Load')
      endwith
    endif
  endfunc

  func oBtn_1_47.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_OFPATFWP))
     endwith
    endif
  endfunc


  add object oBtn_1_48 as StdButton with uid="QQJJWFVXMW",left=702, top=303, width=48,height=45,;
    CpPicture="bmp\pdf.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento PDF di offerta generato.";
    , HelpContextID = 132252828;
    , Caption='File \<PDF';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        GSOF_BWP(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_48.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OFPATPDF) AND NOT .cFunction $ 'Load')
      endwith
    endif
  endfunc

  func oBtn_1_48.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_OFPATPDF))
     endwith
    endif
  endfunc


  add object oObj_1_50 as cp_runprogram with uid="XQSIDUPHLB",left=247, top=569, width=279,height=19,;
    caption='GSOF_BLM',;
   bGlobalFont=.t.,;
    prg="GSOF_BLM",;
    cEvent = "w_OFCODMOD Changed",;
    nPag=1;
    , HelpContextID = 38723917


  add object oObj_1_57 as cp_runprogram with uid="BYUPEFHEDG",left=247, top=587, width=279,height=19,;
    caption='GSOF_BVN',;
   bGlobalFont=.t.,;
    prg="GSOF_BVN",;
    cEvent = "w_OFSTATUS Changed",;
    nPag=1;
    , HelpContextID = 229711540


  add object oObj_1_58 as cp_runprogram with uid="URZGTTUHPN",left=-1, top=569, width=239,height=19,;
    caption='GSOF_BA1',;
   bGlobalFont=.t.,;
    prg="GSOF_BA1('A')",;
    cEvent = "w_OFCODNOM Changed",;
    nPag=1;
    , HelpContextID = 229711511


  add object oObj_1_66 as cp_runprogram with uid="OZCPSQOTQX",left=-1, top=587, width=239,height=19,;
    caption='GSOF_BZD',;
   bGlobalFont=.t.,;
    prg="GSOF_BZD",;
    cEvent = "Delete end",;
    nPag=1;
    , HelpContextID = 38723926


  add object oObj_1_73 as cp_runprogram with uid="VDMYQTKWJG",left=247, top=551, width=279,height=19,;
    caption='GSOF_BCS',;
   bGlobalFont=.t.,;
    prg="GSOF_BCS",;
    cEvent = "w_OFDATDOC Changed",;
    nPag=1;
    , HelpContextID = 229711545


  add object oObj_1_74 as cp_runprogram with uid="SEPPMADJDX",left=-1, top=551, width=239,height=19,;
    caption='GSOF_BA1',;
   bGlobalFont=.t.,;
    prg="GSOF_BA1('X')",;
    cEvent = "Edit Started,Load",;
    nPag=1;
    , HelpContextID = 229711511


  add object oObj_1_75 as cp_runprogram with uid="NUTTVLBYHM",left=247, top=623, width=279,height=19,;
    caption='GSOF_BED',;
   bGlobalFont=.t.,;
    prg="GSOF_BED('A')",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 229711530


  add object oObj_1_76 as cp_runprogram with uid="CNZUFDUFPH",left=-1, top=623, width=239,height=19,;
    caption='GSOF_BDO',;
   bGlobalFont=.t.,;
    prg="GSOF_BDO",;
    cEvent = "GeneraDocumento",;
    nPag=1;
    , HelpContextID = 229711541


  add object oObj_1_96 as cp_runprogram with uid="DJPBCAJWWP",left=-1, top=605, width=239,height=19,;
    caption='GSOF_BED',;
   bGlobalFont=.t.,;
    prg="GSOF_BED('C')",;
    cEvent = "Aggiorna2",;
    nPag=1;
    , HelpContextID = 229711530

  add object oNOTELEFO_1_103 as StdField with uid="ZJXIQFUEHK",rtseq=75,rtrep=.f.,;
    cFormVar = "w_NOTELEFO", cQueryName = "NOTELEFO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 260074277,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=86, Top=111, InputMask=replicate('X',18)

  add object oNONUMCEL_1_104 as StdField with uid="LQZZXNYAGV",rtseq=76,rtrep=.f.,;
    cFormVar = "w_NONUMCEL", cQueryName = "NONUMCEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 228592418,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=388, Top=111, InputMask=replicate('X',18)

  add object oStr_1_13 as StdString with uid="KKFZSFWCRU",Visible=.t., Left=4, Top=11,;
    Alignment=1, Width=80, Height=18,;
    Caption="Nominativo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="KJIJHORJDB",Visible=.t., Left=9, Top=161,;
    Alignment=1, Width=76, Height=18,;
    Caption="Doc.N.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="ENLGJDBELN",Visible=.t., Left=205, Top=163,;
    Alignment=2, Width=6, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="UZMFQDFRHO",Visible=.t., Left=345, Top=161,;
    Alignment=1, Width=42, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="KREGIBBBXH",Visible=.t., Left=465, Top=36,;
    Alignment=1, Width=94, Height=18,;
    Caption="Versione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="TGAUIJNXUC",Visible=.t., Left=465, Top=11,;
    Alignment=1, Width=94, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="QNLKVRQQOT",Visible=.t., Left=493, Top=136,;
    Alignment=1, Width=106, Height=18,;
    Caption="Data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="TCOYAGSROM",Visible=.t., Left=493, Top=186,;
    Alignment=1, Width=106, Height=18,;
    Caption="Data chiusura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HHOQGKDUCD",Visible=.t., Left=4, Top=136,;
    Alignment=1, Width=80, Height=18,;
    Caption="Modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="JRNZTOOUDZ",Visible=.t., Left=4, Top=186,;
    Alignment=1, Width=80, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="RTOXJATBVM",Visible=.t., Left=493, Top=161,;
    Alignment=1, Width=106, Height=18,;
    Caption="Data di invio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_105 as StdString with uid="QUAPWKLMTA",Visible=.t., Left=4, Top=113,;
    Alignment=1, Width=80, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_106 as StdString with uid="KJTCIULWCJ",Visible=.t., Left=283, Top=113,;
    Alignment=1, Width=102, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_aofPag2 as StdContainer
  Width  = 803
  height = 471
  stdWidth  = 803
  stdheight = 471
  resizeXpos=397
  resizeYpos=295
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOFCODVAL_2_1 as StdField with uid="UHCIMXFXUW",rtseq=77,rtrep=.f.,;
    cFormVar = "w_OFCODVAL", cQueryName = "OFCODVAL",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o obsoleta o valuta esercizio non definita",;
    ToolTipText = "Codice valuta dell'offerta",;
    HelpContextID = 610866,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=65, Top=10, InputMask=replicate('X',3), bHasZoom = .t. , tabstop=.f., cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_OFCODVAL"

  func oOFCODVAL_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_OFCODVAL) OR .cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oOFCODVAL_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODVAL_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODVAL_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oOFCODVAL_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oOFCODVAL_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_OFCODVAL
     i_obj.ecpSave()
  endproc

  add object oDESVAL_2_6 as StdField with uid="JVDEUHNYEW",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione valuta",;
    HelpContextID = 98652214,;
   bGlobalFont=.t.,;
    Height=21, Width=189, Left=114, Top=10, InputMask=replicate('X',35)

  add object oOFCODLIS_2_8 as StdField with uid="BWALYSOXDX",rtseq=83,rtrep=.f.,;
    cFormVar = "w_OFCODLIS", cQueryName = "OFCODLIS",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino",;
    HelpContextID = 167161287,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=65, Top=34, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_OFCODLIS"

  func oOFCODLIS_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODLIS_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODLIS_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oOFCODLIS_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSOF_AOF.LISTINI_VZM',this.parent.oContained
  endproc
  proc oOFCODLIS_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_OFCODLIS
     i_obj.ecpSave()
  endproc


  add object oLinkPC_2_16 as stdDynamicChildContainer with uid="RHHUPIMIEY",left=-2, top=64, width=803, height=378, bOnScreen=.t.;


  add object oDESLIS_2_17 as StdField with uid="IWRXSEETMM",rtseq=90,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione listino",;
    HelpContextID = 223825974,;
   bGlobalFont=.t.,;
    Height=21, Width=206, Left=130, Top=34, InputMask=replicate('X',40)


  add object oBtn_2_18 as StdButton with uid="QFZPOLRVRW",left=683, top=7, width=48,height=45,;
    CpPicture="bmp\INS_RAP.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere all'importazione dei kit promozionali";
    , HelpContextID = 245206019;
    , Caption='\<Kit Promo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_18.Click()
      do GSOF_KIM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_19 as StdButton with uid="UPUQEIVTXM",left=734, top=7, width=48,height=45,;
    CpPicture="bmp\AltreOff.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere all'importazione del dettaglio di altre offerte.";
    , HelpContextID = 176826634;
    , Caption='\<Altre Off.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_19.Click()
      do GSOF_KIO with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_20 as cp_runprogram with uid="CRAUSCTZTZ",left=273, top=494, width=237,height=19,;
    caption='GSOF_BA1(V)',;
   bGlobalFont=.t.,;
    prg="GSOF_BA1('V')",;
    cEvent = "w_OFCODVAL Changed",;
    nPag=2;
    , HelpContextID = 229902103


  add object oObj_2_22 as cp_runprogram with uid="OJCOLYBCLP",left=31, top=494, width=237,height=19,;
    caption='GSOF_BA1(L)',;
   bGlobalFont=.t.,;
    prg="GSOF_BA1('L')",;
    cEvent = "w_OFCODLIS Changed",;
    nPag=2;
    , HelpContextID = 229899543

  add object oStr_2_7 as StdString with uid="QJNACXPCZP",Visible=.t., Left=6, Top=10,;
    Alignment=1, Width=57, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="DRCSBLYEGE",Visible=.t., Left=6, Top=34,;
    Alignment=1, Width=57, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_aofPag3 as StdContainer
  Width  = 803
  height = 471
  stdWidth  = 803
  stdheight = 471
  resizeXpos=376
  resizeYpos=278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOFCODPAG_3_1 as StdField with uid="JAWIFYFVMN",rtseq=92,rtrep=.f.,;
    cFormVar = "w_OFCODPAG", cQueryName = "OFCODPAG",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento non definito oppure obsoleto",;
    ToolTipText = "Codice pagamento associato al documento",;
    HelpContextID = 168383021,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=105, Top=15, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_OFCODPAG"

  func oOFCODPAG_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODPAG_3_1.ecpDrop(oSource)
    this.Parent.oContained.link_3_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODPAG_3_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oOFCODPAG_3_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oOFCODPAG_3_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_OFCODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_3_2 as StdField with uid="OHUYBRXZDG",rtseq=93,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione pagamento",;
    HelpContextID = 14372918,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=171, Top=15, InputMask=replicate('X',30)

  add object oOFSCOCL1_3_3 as StdField with uid="OMNCFDBJHL",rtseq=94,rtrep=.f.,;
    cFormVar = "w_OFSCOCL1", cQueryName = "OFSCOCL1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^Maggiorazione (se positiva) o sconto (se negativa) applicata al modello",;
    HelpContextID = 38907369,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=43, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oOFSCOCL2_3_4 as StdField with uid="GEUYSNCBQN",rtseq=95,rtrep=.f.,;
    cFormVar = "w_OFSCOCL2", cQueryName = "OFSCOCL2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^ Maggiorazione (se positiva) o sconto (se negativa) applicata al modello",;
    HelpContextID = 38907368,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=43, cSayPict='"999.99"', cGetPict='"999.99"'

  func oOFSCOCL2_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFSCOCL1<>0)
    endwith
   endif
  endfunc

  add object oOFSCOPAG_3_5 as StdField with uid="CIFOZEJWPY",rtseq=96,rtrep=.f.,;
    cFormVar = "w_OFSCOPAG", cQueryName = "OFSCOPAG",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% Maggiorazione (se positiva) o sconto (se negativa) applicata al pagamento",;
    HelpContextID = 179196461,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=329, Top=43, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oTOTRIP_3_6 as StdField with uid="WBDSUHMUYE",rtseq=97,rtrep=.f.,;
    cFormVar = "w_TOTRIP", cQueryName = "TOTRIP",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale righe documento (escluse righe omaggio / sconto merce)",;
    HelpContextID = 173894454,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=530, Top=15, cSayPict="v_PV(38+VVL)", cGetPict="v_PV(38+VVL)"

  add object oOFSCONTI_3_20 as StdField with uid="LKXNGFRBHM",rtseq=105,rtrep=.f.,;
    cFormVar = "w_OFSCONTI", cQueryName = "OFSCONTI",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Sconti/maggiorazioni globali",;
    HelpContextID = 145642031,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=530, Top=43, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oOFSCONTI_3_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFFLFOSC='S')
    endwith
   endif
  endfunc

  add object oOFFLFOSC_3_22 as StdCheck with uid="WASRJVMNTQ",rtseq=107,rtrep=.f.,left=674, top=43, caption="Forza sconto",;
    ToolTipText = "Se attivo: consente di forzare lo sconto/maggiorazione",;
    HelpContextID = 153518633,;
    cFormVar="w_OFFLFOSC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oOFFLFOSC_3_22.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oOFFLFOSC_3_22.GetRadio()
    this.Parent.oContained.w_OFFLFOSC = this.RadioValue()
    return .t.
  endfunc

  func oOFFLFOSC_3_22.SetRadio()
    this.Parent.oContained.w_OFFLFOSC=trim(this.Parent.oContained.w_OFFLFOSC)
    this.value = ;
      iif(this.Parent.oContained.w_OFFLFOSC=='S',1,;
      0)
  endfunc

  add object oOFSPEINC_3_23 as StdField with uid="XDFPDSTBKR",rtseq=108,rtrep=.f.,;
    cFormVar = "w_OFSPEINC", cQueryName = "OFSPEINC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese incasso lette dal codice pagamento",;
    HelpContextID = 52122153,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=530, Top=71, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oOFSPEIMB_3_24 as StdField with uid="RILFFDCIDX",rtseq=109,rtrep=.f.,;
    cFormVar = "w_OFSPEIMB", cQueryName = "OFSPEIMB",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese imballo",;
    HelpContextID = 216313304,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=530, Top=99, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oOFSPETRA_3_25 as StdField with uid="NAEYRSFJIK",rtseq=110,rtrep=.f.,;
    cFormVar = "w_OFSPETRA", cQueryName = "OFSPETRA",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese trasporto",;
    HelpContextID = 236671527,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=530, Top=127, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oOFIMPOFF_3_26 as StdField with uid="FSSKKZVLBG",rtseq=111,rtrep=.f.,;
    cFormVar = "w_OFIMPOFF", cQueryName = "OFIMPOFF",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo offerta",;
    HelpContextID = 104353236,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=530, Top=155, cSayPict="v_PV(38+VVL)", cGetPict="v_PV(38+VVL)"

  add object oOF__NOTE_3_28 as StdMemo with uid="TCOTDGOKNS",rtseq=112,rtrep=.f.,;
    cFormVar = "w_OF__NOTE", cQueryName = "OF__NOTE",;
    bObbl = .f. , nPag = 3, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 163254827,;
   bGlobalFont=.t.,;
    Height=240, Width=779, Left=14, Top=180

  add object ocodval_3_32 as StdField with uid="PHKCKFAFKR",rtseq=113,rtrep=.f.,;
    cFormVar = "w_codval", cQueryName = "codval",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta",;
    HelpContextID = 134384678,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=486, Top=155, InputMask=replicate('X',3)


  add object oBtn_3_33 as StdButton with uid="QGLKPDYHVZ",left=743, top=425, width=48,height=45,;
    CpPicture="bmp\note.bmp", caption="", nPag=3;
    , ToolTipText = "Nuova nota";
    , HelpContextID = 66868859;
    , Caption='\<Nota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_33.Click()
      with this.Parent.oContained
        gsAR_bno (this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_3_36 as cp_runprogram with uid="VBNFBJFHMO",left=-4, top=489, width=232,height=20,;
    caption='GSOF_BA1',;
   bGlobalFont=.t.,;
    prg="GSOF_BA1('P')",;
    cEvent = "w_OFCODPAG Changed",;
    nPag=3;
    , HelpContextID = 229711511


  add object oBtn_3_47 as StdButton with uid="DMZJVXFITG",left=688, top=131, width=48,height=45,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per calcolare subito le spese accessorie";
    , HelpContextID = 169870298;
    , Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_47.Click()
      with this.Parent.oContained
        do GSAR_BST with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Query' AND NOT EMPTY(.w_OFCODMOD))
      endwith
    endif
  endfunc


  add object oBtn_3_48 as StdButton with uid="BJXOHDVDWF",left=743, top=131, width=48,height=45,;
    CpPicture="BMP\compone.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per visualizzare i metodi di calcolo delle spese imballo e trasporto";
    , HelpContextID = 93920569;
    , TabStop=.f.,Caption='\<Met.spese';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_48.Click()
      do GSAR_KMS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_48.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLSPIM='S' OR .w_FLSPTR='S')
      endwith
    endif
  endfunc

  add object oStr_3_13 as StdString with uid="LHAJPCIQMG",Visible=.t., Left=5, Top=15,;
    Alignment=1, Width=97, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_14 as StdString with uid="KFJJAGOJOR",Visible=.t., Left=398, Top=43,;
    Alignment=1, Width=129, Height=18,;
    Caption="Sconti/maggiorazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="MNPJNQPJAN",Visible=.t., Left=18, Top=161,;
    Alignment=0, Width=75, Height=18,;
    Caption="Note"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_16 as StdString with uid="KVBCXOODAH",Visible=.t., Left=423, Top=155,;
    Alignment=1, Width=61, Height=18,;
    Caption="Totale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_18 as StdString with uid="UTHNTZQURO",Visible=.t., Left=5, Top=43,;
    Alignment=1, Width=97, Height=18,;
    Caption="Magg./sconti:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="NSNREYYTZB",Visible=.t., Left=248, Top=43,;
    Alignment=1, Width=78, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_27 as StdString with uid="MLMKFTYPHA",Visible=.t., Left=168, Top=43,;
    Alignment=2, Width=8, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_3_29 as StdString with uid="QOSYEODVCY",Visible=.t., Left=398, Top=127,;
    Alignment=1, Width=129, Height=18,;
    Caption="Spese trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_30 as StdString with uid="ELDYNVDBRF",Visible=.t., Left=398, Top=71,;
    Alignment=1, Width=129, Height=18,;
    Caption="Spese incasso:"  ;
  , bGlobalFont=.t.

  add object oStr_3_31 as StdString with uid="LMOXBMKJVK",Visible=.t., Left=398, Top=99,;
    Alignment=1, Width=129, Height=18,;
    Caption="Spese imballo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_34 as StdString with uid="TDJVNNLNLA",Visible=.t., Left=471, Top=15,;
    Alignment=1, Width=56, Height=18,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_aofPag4 as StdContainer
  Width  = 803
  height = 471
  stdWidth  = 803
  stdheight = 471
  resizeXpos=422
  resizeYpos=309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOFPATFWP_4_1 as StdField with uid="TJUAJVTYWL",rtseq=116,rtrep=.f.,;
    cFormVar = "w_OFPATFWP", cQueryName = "OFPATFWP",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file WP di output",;
    HelpContextID = 251911626,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=146, Top=419, InputMask=replicate('X',254)

  add object oOFPATPDF_4_3 as StdField with uid="PGYESYCIHI",rtseq=117,rtrep=.f.,;
    cFormVar = "w_OFPATPDF", cQueryName = "OFPATPDF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file PDF di output",;
    HelpContextID = 184295980,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=146, Top=446, InputMask=replicate('X',254)

  add object oOFCODCON_4_6 as StdField with uid="FDCCRXBJSM",rtseq=118,rtrep=.f.,;
    cFormVar = "w_OFCODCON", cQueryName = "OFCODCON",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Contatto di riferimento",;
    HelpContextID = 218714676,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=146, Top=17, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NOM_CONT", cZoomOnZoom="GSOF_BAN", oKey_1_1="NCCODICE", oKey_1_2="this.w_OFCODNOM", oKey_2_1="NCCODCON", oKey_2_2="this.w_OFCODCON"

  func oOFCODCON_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODCON_4_6.ecpDrop(oSource)
    this.Parent.oContained.link_4_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODCON_4_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.NOM_CONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_OFCODNOM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStr(this.Parent.oContained.w_OFCODNOM)
    endif
    do cp_zoom with 'NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(this.parent,'oOFCODCON_4_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_BAN',"Contatti",'GSOF1CON.NOM_CONT_VZM',this.parent.oContained
  endproc
  proc oOFCODCON_4_6.mZoomOnZoom
    local i_obj
    i_obj=GSOF_BAN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.NCCODICE=w_OFCODNOM
     i_obj.w_NCCODCON=this.parent.oContained.w_OFCODCON
     i_obj.ecpSave()
  endproc

  add object oOFDATRIC_4_7 as StdField with uid="QMLVKOXTWE",rtseq=119,rtrep=.f.,;
    cFormVar = "w_OFDATRIC", cQueryName = "OFDATRIC",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ricontattare il",;
    HelpContextID = 50634199,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=648, Top=17

  add object oOFCODUTE_4_8 as StdField with uid="TEMYSDFCLO",rtseq=120,rtrep=.f.,;
    cFormVar = "w_OFCODUTE", cQueryName = "OFCODUTE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore associato all'offerta",;
    HelpContextID = 252269099,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=146, Top=68, bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_OFCODUTE"

  func oOFCODUTE_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODUTE_4_8.ecpDrop(oSource)
    this.Parent.oContained.link_4_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODUTE_4_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oOFCODUTE_4_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
  endproc

  add object oOFNUMPRI_4_10 as StdField with uid="PWDNWROVVA",rtseq=121,rtrep=.f.,;
    cFormVar = "w_OFNUMPRI", cQueryName = "OFNUMPRI",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit� offerta",;
    HelpContextID = 178258479,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=146, Top=93, cSayPict='"999999"', cGetPict='"999999"'

  add object oOFCODAGE_4_11 as StdField with uid="VFVHOJTMPS",rtseq=122,rtrep=.f.,;
    cFormVar = "w_OFCODAGE", cQueryName = "OFCODAGE",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente che cura i rapporti",;
    HelpContextID = 83275221,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=146, Top=118, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_OFCODAGE"

  func oOFCODAGE_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERAGE='S')
    endwith
   endif
  endfunc

  func oOFCODAGE_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODAGE_4_11.ecpDrop(oSource)
    this.Parent.oContained.link_4_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODAGE_4_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oOFCODAGE_4_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oOFCODAGE_4_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_OFCODAGE
     i_obj.ecpSave()
  endproc

  add object oOFFLFOAG_4_12 as StdCheck with uid="PYFFIMQFZY",rtseq=123,rtrep=.f.,left=477, top=120, caption="Forza",;
    ToolTipText = "Se attivo il codice agente impostato verr� utilizzato per la creazione del documento",;
    HelpContextID = 153518637,;
    cFormVar="w_OFFLFOAG", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oOFFLFOAG_4_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oOFFLFOAG_4_12.GetRadio()
    this.Parent.oContained.w_OFFLFOAG = this.RadioValue()
    return .t.
  endfunc

  func oOFFLFOAG_4_12.SetRadio()
    this.Parent.oContained.w_OFFLFOAG=trim(this.Parent.oContained.w_OFFLFOAG)
    this.value = ;
      iif(this.Parent.oContained.w_OFFLFOAG=='S',1,;
      0)
  endfunc

  add object oOFCODAG2_4_15 as StdField with uid="PFHPSBRRCE",rtseq=125,rtrep=.f.,;
    cFormVar = "w_OFCODAG2", cQueryName = "OFCODAG2",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto oppure non capoarea",;
    ToolTipText = "Capo area",;
    HelpContextID = 83275240,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=146, Top=143, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_OFCODAG2"

  func oOFCODAG2_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERAGE='S' AND NOT EMPTY(.w_OFCODAGE))
    endwith
   endif
  endfunc

  func oOFCODAG2_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODAG2_4_15.ecpDrop(oSource)
    this.Parent.oContained.link_4_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODAG2_4_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oOFCODAG2_4_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Capoarea",'GSVE1KFT.AGENTI_VZM',this.parent.oContained
  endproc
  proc oOFCODAG2_4_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_OFCODAG2
     i_obj.ecpSave()
  endproc

  add object oOFFLFOA2_4_16 as StdCheck with uid="TEQVJWFOJK",rtseq=126,rtrep=.f.,left=477, top=146, caption="Forza",;
    ToolTipText = "Se attivo il codice capo area impostato verr� utilizzato per la creazione del documento",;
    HelpContextID = 153518616,;
    cFormVar="w_OFFLFOA2", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oOFFLFOA2_4_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oOFFLFOA2_4_16.GetRadio()
    this.Parent.oContained.w_OFFLFOA2 = this.RadioValue()
    return .t.
  endfunc

  func oOFFLFOA2_4_16.SetRadio()
    this.Parent.oContained.w_OFFLFOA2=trim(this.Parent.oContained.w_OFFLFOA2)
    this.value = ;
      iif(this.Parent.oContained.w_OFFLFOA2=='S',1,;
      0)
  endfunc

  add object oOFCODPOR_4_18 as StdField with uid="DODOCUBOTS",rtseq=127,rtrep=.f.,;
    cFormVar = "w_OFCODPOR", cQueryName = "OFCODPOR",;
    bObbl = .f. , nPag = 4, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Porto",;
    HelpContextID = 168383032,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=146, Top=168, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="PORTI", cZoomOnZoom="GSAR_APO", oKey_1_1="POCODPOR", oKey_1_2="this.w_OFCODPOR"

  func oOFCODPOR_4_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODPOR_4_18.ecpDrop(oSource)
    this.Parent.oContained.link_4_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODPOR_4_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PORTI','*','POCODPOR',cp_AbsName(this.parent,'oOFCODPOR_4_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APO',"Porti",'',this.parent.oContained
  endproc
  proc oOFCODPOR_4_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_POCODPOR=this.parent.oContained.w_OFCODPOR
     i_obj.ecpSave()
  endproc

  add object oOFFLFOPO_4_19 as StdCheck with uid="ZBMGILWFQZ",rtseq=128,rtrep=.f.,left=477, top=171, caption="Forza",;
    ToolTipText = "Se attivo il codice porto impostato verr� utilizzato per la creazione del documento",;
    HelpContextID = 153518645,;
    cFormVar="w_OFFLFOPO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oOFFLFOPO_4_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oOFFLFOPO_4_19.GetRadio()
    this.Parent.oContained.w_OFFLFOPO = this.RadioValue()
    return .t.
  endfunc

  func oOFFLFOPO_4_19.SetRadio()
    this.Parent.oContained.w_OFFLFOPO=trim(this.Parent.oContained.w_OFFLFOPO)
    this.value = ;
      iif(this.Parent.oContained.w_OFFLFOPO=='S',1,;
      0)
  endfunc

  add object oOFCODSPE_4_21 as StdField with uid="RWIBSGMUDB",rtseq=129,rtrep=.f.,;
    cFormVar = "w_OFCODSPE", cQueryName = "OFCODSPE",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Spedizione",;
    HelpContextID = 218714667,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=146, Top=193, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODASPED", cZoomOnZoom="GSAR_ASP", oKey_1_1="SPCODSPE", oKey_1_2="this.w_OFCODSPE"

  func oOFCODSPE_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODSPE_4_21.ecpDrop(oSource)
    this.Parent.oContained.link_4_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODSPE_4_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODASPED','*','SPCODSPE',cp_AbsName(this.parent,'oOFCODSPE_4_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASP',"Spedizioni",'',this.parent.oContained
  endproc
  proc oOFCODSPE_4_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SPCODSPE=this.parent.oContained.w_OFCODSPE
     i_obj.ecpSave()
  endproc

  add object oOFFLFOSP_4_22 as StdCheck with uid="LHRLNTQLEK",rtseq=130,rtrep=.f.,left=477, top=196, caption="Forza",;
    ToolTipText = "Se attivo il codice spedizione impostato verr� utilizzato per la creazione del documento",;
    HelpContextID = 153518646,;
    cFormVar="w_OFFLFOSP", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oOFFLFOSP_4_22.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oOFFLFOSP_4_22.GetRadio()
    this.Parent.oContained.w_OFFLFOSP = this.RadioValue()
    return .t.
  endfunc

  func oOFFLFOSP_4_22.SetRadio()
    this.Parent.oContained.w_OFFLFOSP=trim(this.Parent.oContained.w_OFFLFOSP)
    this.value = ;
      iif(this.Parent.oContained.w_OFFLFOSP=='S',1,;
      0)
  endfunc


  add object oLinkPC_4_24 as stdDynamicChildContainer with uid="GGYGNMRNIJ",left=14, top=235, width=628, height=156, bOnScreen=.t.;


  add object oDESOPE_4_27 as StdField with uid="VFRULGHWXN",rtseq=131,rtrep=.f.,;
    cFormVar = "w_DESOPE", cQueryName = "DESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione operatore",;
    HelpContextID = 264917046,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=68, InputMask=replicate('X',35)

  add object oDESAGE_4_28 as StdField with uid="QHWWKCHFKZ",rtseq=132,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 254562358,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=118, InputMask=replicate('X',35)

  add object oPERSON_4_30 as StdField with uid="BSNNJPTQEM",rtseq=134,rtrep=.f.,;
    cFormVar = "w_PERSON", cQueryName = "PERSON",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contatto",;
    HelpContextID = 146686198,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=17, InputMask=replicate('X',35)

  add object oDESCAPO_4_35 as StdField with uid="OGTKGLMZOS",rtseq=136,rtrep=.f.,;
    cFormVar = "w_DESCAPO", cQueryName = "DESCAPO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione capo area",;
    HelpContextID = 164515894,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=143, InputMask=replicate('X',35)

  add object oDESPORT_4_36 as StdField with uid="KRXYHUEJKR",rtseq=137,rtrep=.f.,;
    cFormVar = "w_DESPORT", cQueryName = "DESPORT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione porto",;
    HelpContextID = 213602358,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=168, InputMask=replicate('X',35)

  add object oDESSPE_4_37 as StdField with uid="GWODSSNBBI",rtseq=138,rtrep=.f.,;
    cFormVar = "w_DESSPE", cQueryName = "DESSPE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione spedizione",;
    HelpContextID = 265179190,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=193, InputMask=replicate('X',35)


  add object oObj_4_39 as cp_runprogram with uid="YUVAXTNYPJ",left=11, top=496, width=172,height=19,;
    caption='GSOF_BCO',;
   bGlobalFont=.t.,;
    prg="GSOF_BCO",;
    cEvent = "ControlliFinali",;
    nPag=4;
    , HelpContextID = 229711541


  add object oObj_4_41 as cp_runprogram with uid="QEPAQWFGDY",left=195, top=496, width=239,height=19,;
    caption='GSOF_BAG',;
   bGlobalFont=.t.,;
    prg="GSOF_BAG",;
    cEvent = "w_OFCODAGE Changed",;
    nPag=4;
    , HelpContextID = 229711533

  add object oNCTELEFO_4_44 as StdField with uid="BUXCQAAEPN",rtseq=143,rtrep=.f.,;
    cFormVar = "w_NCTELEFO", cQueryName = "NCTELEFO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 260071205,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=146, Top=43, InputMask=replicate('X',18)

  add object oNCNUMCEL_4_45 as StdField with uid="DPLNENGJKT",rtseq=144,rtrep=.f.,;
    cFormVar = "w_NCNUMCEL", cQueryName = "NCNUMCEL",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 228589346,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=648, Top=43, InputMask=replicate('X',18)

  add object oNCTELEF2_4_48 as StdField with uid="UOURFNIDGD",rtseq=145,rtrep=.f.,;
    cFormVar = "w_NCTELEF2", cQueryName = "NCTELEF2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 260071176,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=395, Top=43, InputMask=replicate('X',18)

  add object oStr_4_2 as StdString with uid="NABUBCMRHA",Visible=.t., Left=7, Top=423,;
    Alignment=1, Width=137, Height=18,;
    Caption="Nome file W.P:"  ;
  , bGlobalFont=.t.

  add object oStr_4_4 as StdString with uid="VLFKDDXCGV",Visible=.t., Left=7, Top=450,;
    Alignment=1, Width=137, Height=18,;
    Caption="Nome file PDF:"  ;
  , bGlobalFont=.t.

  add object oStr_4_5 as StdString with uid="PVJIGOPVEQ",Visible=.t., Left=7, Top=93,;
    Alignment=1, Width=137, Height=18,;
    Caption="Priorit� offerta:"  ;
  , bGlobalFont=.t.

  add object oStr_4_9 as StdString with uid="BBXPZICDYY",Visible=.t., Left=1, Top=17,;
    Alignment=1, Width=143, Height=18,;
    Caption="Contatto di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="DURFIDQOTD",Visible=.t., Left=7, Top=118,;
    Alignment=1, Width=137, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_4_17 as StdString with uid="EOTWWKFUPM",Visible=.t., Left=7, Top=143,;
    Alignment=1, Width=137, Height=18,;
    Caption="Capo area:"  ;
  , bGlobalFont=.t.

  add object oStr_4_20 as StdString with uid="ICGLFHDCBT",Visible=.t., Left=7, Top=168,;
    Alignment=1, Width=137, Height=18,;
    Caption="Porto:"  ;
  , bGlobalFont=.t.

  add object oStr_4_23 as StdString with uid="WKCAALFTAH",Visible=.t., Left=7, Top=193,;
    Alignment=1, Width=137, Height=18,;
    Caption="Spedizione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_25 as StdString with uid="TQRPXCHSYJ",Visible=.t., Left=513, Top=17,;
    Alignment=1, Width=133, Height=19,;
    Caption="Ricontattare il:"  ;
  , bGlobalFont=.t.

  add object oStr_4_26 as StdString with uid="WTNVOFUPVJ",Visible=.t., Left=7, Top=68,;
    Alignment=1, Width=137, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_33 as StdString with uid="LCAYAHJFZO",Visible=.t., Left=16, Top=393,;
    Alignment=0, Width=190, Height=18,;
    Caption="Documenti di output"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_34 as StdString with uid="QNAIDWKUUG",Visible=.t., Left=16, Top=215,;
    Alignment=0, Width=101, Height=18,;
    Caption="Attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_46 as StdString with uid="JFSAJUCXWP",Visible=.t., Left=1, Top=44,;
    Alignment=1, Width=143, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_4_47 as StdString with uid="VCTHVGHZUE",Visible=.t., Left=540, Top=44,;
    Alignment=1, Width=102, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_4_49 as StdString with uid="NCERXDJBEE",Visible=.t., Left=290, Top=44,;
    Alignment=1, Width=102, Height=18,;
    Caption="2� Telefono:"  ;
  , bGlobalFont=.t.

  add object oBox_4_31 as StdBox with uid="QJKIMKTQTV",left=16, top=411, width=626,height=2
enddefine
define class tgsof_aofPag5 as StdContainer
  Width  = 803
  height = 471
  stdWidth  = 803
  stdheight = 471
  resizeXpos=484
  resizeYpos=273
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNOMFIL_5_1 as StdField with uid="NJMFEJYBNS",rtseq=146,rtrep=.f.,;
    cFormVar = "w_NOMFIL", cQueryName = "",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca nome o link del file",;
    HelpContextID = 105970390,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=120, Top=14, InputMask=replicate('X',30)

  func oNOMFIL_5_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oOGGETT_5_2 as StdField with uid="ZULEMOOYTM",rtseq=147,rtrep=.f.,;
    cFormVar = "w_OGGETT", cQueryName = "",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca oggetto allegato",;
    HelpContextID = 251630310,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=120, Top=38, InputMask=replicate('X',30)

  func oOGGETT_5_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oNOTE_5_3 as StdField with uid="PWLNYTXMPQ",rtseq=148,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca note allegato",;
    HelpContextID = 171939114,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=444, Top=38, InputMask=replicate('X',30)

  func oNOTE_5_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc


  add object ZoomAll as cp_zoombox with uid="EKACRPXFEN",left=17, top=64, width=735,height=354,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSOF_AOF",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="ALL_EGAT",;
    cEvent = "Calcola,Load",;
    nPag=5;
    , HelpContextID = 1170918


  add object oBtn_5_5 as StdButton with uid="EJSAYZGKEG",left=700, top=16, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per ricercare i documenti allegati";
    , HelpContextID = 95510;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_5.Click()
      with this.Parent.oContained
        .Notifyevent('Calcola')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' and NOT EMPTY(.w_OFCODNOM))
      endwith
    endif
  endfunc


  add object oBtn_5_7 as StdButton with uid="GMVMXTKTJO",left=97, top=422, width=48,height=45,;
    CpPicture="bmp\VISUALI.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per aprire il documento allegato";
    , HelpContextID = 169448698;
    , Caption='\<Apri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_7.Click()
      with this.Parent.oContained
        GSOF_BGO(this.Parent.oContained,"APRI_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc


  add object oBtn_5_8 as StdButton with uid="RXUIEFGJTJ",left=149, top=422, width=48,height=45,;
    CpPicture="bmp\Modifica.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per modificare i riferimenti del documento allegato";
    , HelpContextID = 88443687;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_8.Click()
      with this.Parent.oContained
        GSOF_BGO(this.Parent.oContained,"VARIA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc


  add object oBtn_5_9 as StdButton with uid="VKYMQOFCIQ",left=201, top=422, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per cancellare i riferimenti del documento allegato";
    , HelpContextID = 175757126;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_9.Click()
      with this.Parent.oContained
        GSOF_BGO(this.Parent.oContained,"ELIMINA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc


  add object oBtn_5_10 as StdButton with uid="IVGLYAPRFS",left=699, top=422, width=48,height=45,;
    CpPicture="bmp\CARICA.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per associare un nuovo allegato";
    , HelpContextID = 52215594;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_10.Click()
      with this.Parent.oContained
        GSOF_BGO(this.Parent.oContained,"CARICA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' and NOT EMPTY(.w_OFCODNOM))
      endwith
    endif
  endfunc

  add object oStr_5_11 as StdString with uid="FXVGAXWYFQ",Visible=.t., Left=29, Top=14,;
    Alignment=1, Width=89, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.

  add object oStr_5_12 as StdString with uid="XRKKMGDKHL",Visible=.t., Left=29, Top=38,;
    Alignment=1, Width=89, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_5_13 as StdString with uid="HNPBTANUPR",Visible=.t., Left=368, Top=38,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_aofPag6 as StdContainer
  Width  = 803
  height = 471
  stdWidth  = 803
  stdheight = 471
  resizeXpos=304
  resizeYpos=304
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOM as cp_zoombox with uid="XBTEELQVTF",left=8, top=144, width=653,height=322,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",bQueryOnLoad=.f.,bReadOnly=.t.,cZoomFile="GSOF_KAT",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca,Load,New record",;
    nPag=6;
    , HelpContextID = 1170918


  add object oBtn_6_2 as StdButton with uid="PKVNCOXTJT",left=675, top=144, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=6;
    , ToolTipText = "Visualizza attivit�";
    , HelpContextID = 134029712;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_2.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_6_3 as StdButton with uid="NBCVTUMNVJ",left=675, top=191, width=48,height=45,;
    CpPicture="BMP\MODIFICA.BMP", caption="", nPag=6;
    , ToolTipText = "Modifica attivit�";
    , HelpContextID = 88443687;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_3.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_6_4 as StdButton with uid="UUNSDLSMLD",left=675, top=238, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=6;
    , ToolTipText = "Nuova attivit�";
    , HelpContextID = 52215594;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_4.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load')
      endwith
    endif
  endfunc

  add object oNAME_6_5 as StdField with uid="WASGPNEQLK",rtseq=151,rtrep=.f.,;
    cFormVar = "w_NAME", cQueryName = "NAME",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione operatore",;
    HelpContextID = 171971370,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=133, Top=15, InputMask=replicate('X',20)

  func oNAME_6_5.mHide()
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
  endfunc

  add object oOPERAT_6_6 as StdField with uid="TVADFWZWKK",rtseq=152,rtrep=.f.,;
    cFormVar = "w_OPERAT", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 232553446,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=82, Top=15, cSayPict='"999999"', cGetPict='"999999"', cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_OPERAT"

  func oOPERAT_6_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (CP_ISADMINISTRATOR() and .cFunction<>'Load')
    endwith
   endif
  endfunc

  func oOPERAT_6_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oOPERAT_6_6.ecpDrop(oSource)
    this.Parent.oContained.link_6_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oSTAT1_6_7 as StdCombo with uid="YCYLXOXQTX",rtseq=153,rtrep=.f.,left=574,top=15,width=84,height=21;
    , ToolTipText = "Stato attivit�";
    , HelpContextID = 119652314;
    , cFormVar="w_STAT1",RowSource=""+"Aperta,"+"Chiusa", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oSTAT1_6_7.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oSTAT1_6_7.GetRadio()
    this.Parent.oContained.w_STAT1 = this.RadioValue()
    return .t.
  endfunc

  func oSTAT1_6_7.SetRadio()
    this.Parent.oContained.w_STAT1=trim(this.Parent.oContained.w_STAT1)
    this.value = ;
      iif(this.Parent.oContained.w_STAT1=='A',1,;
      iif(this.Parent.oContained.w_STAT1=='C',2,;
      0))
  endfunc

  func oSTAT1_6_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oSTAT1_6_7.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc


  add object oSTAT2_6_8 as StdCombo with uid="GYEEEOZQNA",rtseq=154,rtrep=.f.,left=575,top=15,width=84,height=21;
    , ToolTipText = "Stato attivit�";
    , HelpContextID = 118603738;
    , cFormVar="w_STAT2",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Tutti", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oSTAT2_6_8.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'K',;
    space(1))))))))
  endfunc
  func oSTAT2_6_8.GetRadio()
    this.Parent.oContained.w_STAT2 = this.RadioValue()
    return .t.
  endfunc

  func oSTAT2_6_8.SetRadio()
    this.Parent.oContained.w_STAT2=trim(this.Parent.oContained.w_STAT2)
    this.value = ;
      iif(this.Parent.oContained.w_STAT2=='N',1,;
      iif(this.Parent.oContained.w_STAT2=='D',2,;
      iif(this.Parent.oContained.w_STAT2=='I',3,;
      iif(this.Parent.oContained.w_STAT2=='T',4,;
      iif(this.Parent.oContained.w_STAT2=='F',5,;
      iif(this.Parent.oContained.w_STAT2=='K',6,;
      0))))))
  endfunc

  func oSTAT2_6_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oSTAT2_6_8.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  add object oDATAINI_6_10 as StdField with uid="QXSPXYXHLS",rtseq=156,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit�",;
    HelpContextID = 129213386,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=82, Top=43

  func oDATAINI_6_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oORAINI_6_11 as StdField with uid="IFZETBJSDN",rtseq=157,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 61029862,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=257, Top=43, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_6_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oORAINI_6_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_6_12 as StdField with uid="BMAZHAYBOV",rtseq=158,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 61080774,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=295, Top=43, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_6_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oMININI_6_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATAFIN_6_14 as StdField with uid="IMMOZELIYO",rtseq=159,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit�",;
    HelpContextID = 52190262,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=398, Top=43

  func oDATAFIN_6_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oORAFIN_6_15 as StdField with uid="TWVCQEVEXZ",rtseq=160,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora fine attivit�",;
    HelpContextID = 139476454,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=575, Top=43, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN_6_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oORAFIN_6_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_6_16 as StdField with uid="VEYPJJGXRE",rtseq=161,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti fine attivit�",;
    HelpContextID = 139527366,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=613, Top=43, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_6_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oMINFIN_6_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oSUBJECT_6_18 as StdField with uid="MWYPXPNUGK",rtseq=162,rtrep=.f.,;
    cFormVar = "w_SUBJECT", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto attivit�",;
    HelpContextID = 219435302,;
   bGlobalFont=.t.,;
    Height=21, Width=354, Left=82, Top=71, InputMask=replicate('X',254)

  func oSUBJECT_6_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oCONTINI_6_19 as StdField with uid="TPGDOPMLNT",rtseq=163,rtrep=.f.,;
    cFormVar = "w_CONTINI", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Contatto",;
    HelpContextID = 127989210,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=82, Top=99, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCCODICE", oKey_1_2="this.w_OFCODNOM", oKey_2_1="NCCODCON", oKey_2_2="this.w_CONTINI"

  func oCONTINI_6_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oCONTINI_6_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTINI_6_19.ecpDrop(oSource)
    this.Parent.oContained.link_6_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTINI_6_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.NOM_CONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_OFCODNOM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStr(this.Parent.oContained.w_OFCODNOM)
    endif
    do cp_zoom with 'NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(this.parent,'oCONTINI_6_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Contatti",'',this.parent.oContained
  endproc

  add object oPRIOINI_6_20 as StdField with uid="JXKZIUYBKI",rtseq=164,rtrep=.f.,;
    cFormVar = "w_PRIOINI", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit� attivit�",;
    HelpContextID = 128336394,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=575, Top=99, cSayPict='"999999"', cGetPict='"999999"'

  func oPRIOINI_6_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oPRIOINI_6_20.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc


  add object oBtn_6_22 as StdButton with uid="PHEWMSSLEL",left=675, top=15, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=6;
    , ToolTipText = "Ricerca attivit�";
    , HelpContextID = 95510;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_22.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OFSERIAL) and .cFunction<>'Load')
      endwith
    endif
  endfunc

  add object oDESCPER_6_49 as StdField with uid="WTNPLHLSCK",rtseq=178,rtrep=.f.,;
    cFormVar = "w_DESCPER", cQueryName = "DESCPER",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contatto",;
    HelpContextID = 264130614,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=160, Top=99, InputMask=replicate('X',40)

  func oDESCPER_6_49.mHide()
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
  endfunc


  add object oPRIOINI_6_52 as StdCombo with uid="TGDRVMLJAL",value=1,rtseq=180,rtrep=.f.,left=575,top=99,width=130,height=21;
    , ToolTipText = "Priorit� attivit�";
    , HelpContextID = 128336394;
    , cFormVar="w_PRIOINI",RowSource=""+"Tutti,"+"Normale,"+"Urgente,"+"Scadenza termine", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oPRIOINI_6_52.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    0)))))
  endfunc
  func oPRIOINI_6_52.GetRadio()
    this.Parent.oContained.w_PRIOINI = this.RadioValue()
    return .t.
  endfunc

  func oPRIOINI_6_52.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PRIOINI==0,1,;
      iif(this.Parent.oContained.w_PRIOINI==1,2,;
      iif(this.Parent.oContained.w_PRIOINI==3,3,;
      iif(this.Parent.oContained.w_PRIOINI==4,4,;
      0))))
  endfunc

  func oPRIOINI_6_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oPRIOINI_6_52.mHide()
    with this.Parent.oContained
      return ( g_AGEN<>'S')
    endwith
  endfunc

  add object oStr_6_21 as StdString with uid="TDPGPHZSYZ",Visible=.t., Left=2, Top=15,;
    Alignment=1, Width=79, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_6_23 as StdString with uid="VQVHVEIIGN",Visible=.t., Left=505, Top=15,;
    Alignment=1, Width=67, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_6_24 as StdString with uid="NPICABCYRW",Visible=.t., Left=2, Top=75,;
    Alignment=1, Width=79, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_6_29 as StdString with uid="NORSJWWDPX",Visible=.t., Left=287, Top=43,;
    Alignment=1, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_6_30 as StdString with uid="CXKRSVQCMU",Visible=.t., Left=168, Top=43,;
    Alignment=1, Width=86, Height=18,;
    Caption="Ora inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_6_31 as StdString with uid="APZVWNKQUA",Visible=.t., Left=604, Top=43,;
    Alignment=1, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_6_32 as StdString with uid="YYAKHNGDYF",Visible=.t., Left=505, Top=43,;
    Alignment=1, Width=67, Height=18,;
    Caption="Ora fine:"  ;
  , bGlobalFont=.t.

  add object oStr_6_33 as StdString with uid="PGFKKSQZAP",Visible=.t., Left=2, Top=43,;
    Alignment=1, Width=79, Height=18,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_6_34 as StdString with uid="UKKSUZUVPA",Visible=.t., Left=326, Top=43,;
    Alignment=1, Width=71, Height=18,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_6_48 as StdString with uid="AFHPWIJAZL",Visible=.t., Left=505, Top=99,;
    Alignment=1, Width=67, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_6_50 as StdString with uid="EBFZHXXMTL",Visible=.t., Left=2, Top=99,;
    Alignment=1, Width=79, Height=18,;
    Caption="Contatto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_aof','OFF_ERTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".OFSERIAL=OFF_ERTE.OFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
