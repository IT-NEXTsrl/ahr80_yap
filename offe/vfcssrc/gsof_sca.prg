* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_sca                                                        *
*              Stampa categorie attributi                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-26                                                      *
* Last revis.: 2007-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_sca",oParentObject))

* --- Class definition
define class tgsof_sca as StdForm
  Top    = 13
  Left   = 4

  * --- Standard Properties
  Width  = 576
  Height = 228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-31"
  HelpContextID=177591959
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  OFF_NOMI_IDX = 0
  SEZ_OFFE_IDX = 0
  CAT_ATTR_IDX = 0
  TIP_CATT_IDX = 0
  cPrg = "gsof_sca"
  cComment = "Stampa categorie attributi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CACODIC1 = space(10)
  w_CACODIC2 = space(10)
  w_CACODTA1 = space(5)
  w_CACODTA2 = space(5)
  w_CADESNO1 = space(40)
  w_CADESNO2 = space(40)
  w_CADESTA1 = space(40)
  w_CADESTA2 = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_scaPag1","gsof_sca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODIC1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='SEZ_OFFE'
    this.cWorkTables[3]='CAT_ATTR'
    this.cWorkTables[4]='TIP_CATT'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CACODIC1=space(10)
      .w_CACODIC2=space(10)
      .w_CACODTA1=space(5)
      .w_CACODTA2=space(5)
      .w_CADESNO1=space(40)
      .w_CADESNO2=space(40)
      .w_CADESTA1=space(40)
      .w_CADESTA2=space(40)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CACODIC1))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CACODIC2))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CACODTA1))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CACODTA2))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
    this.DoRTCalc(5,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODIC1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODIC1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CACODIC1)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CACODIC1))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODIC1)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODIC1) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oCACODIC1_1_1'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODIC1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CACODIC1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CACODIC1)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODIC1 = NVL(_Link_.CTCODICE,space(10))
      this.w_CADESNO1 = NVL(_Link_.CTDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CACODIC1 = space(10)
      endif
      this.w_CADESNO1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_CACODIC2) OR  (UPPER(.w_CACODIC1)<= UPPER(.w_CACODIC2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice categoria iniziale � maggiore di quello finale")
        endif
        this.w_CACODIC1 = space(10)
        this.w_CADESNO1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODIC1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODIC2
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODIC2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CACODIC2)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CACODIC2))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODIC2)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODIC2) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oCACODIC2_1_2'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODIC2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CACODIC2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CACODIC2)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODIC2 = NVL(_Link_.CTCODICE,space(10))
      this.w_CADESNO2 = NVL(_Link_.CTDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CACODIC2 = space(10)
      endif
      this.w_CADESNO2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(UPPER(.w_CACODIC1) <= UPPER(.w_CACODIC2)) or empty(.w_CACODIC2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice categoria iniziale � maggiore di quello finale")
        endif
        this.w_CACODIC2 = space(10)
        this.w_CADESNO2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODIC2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODTA1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODTA1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOC',True,'TIP_CATT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CACODTA1)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CACODTA1))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODTA1)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODTA1) and !this.bDontReportError
            deferred_cp_zoom('TIP_CATT','*','TCCODICE',cp_AbsName(oSource.parent,'oCACODTA1_1_3'),i_cWhere,'GSAR_AOC',"Tipo categorie attributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODTA1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CACODTA1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CACODTA1)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODTA1 = NVL(_Link_.TCCODICE,space(5))
      this.w_CADESTA1 = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CACODTA1 = space(5)
      endif
      this.w_CADESTA1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_CACODTA2) OR  (UPPER(.w_CACODTA1)<= UPPER(.w_CACODTA2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice tipo categorie attributo iniziale � maggiore di quello finale")
        endif
        this.w_CACODTA1 = space(5)
        this.w_CADESTA1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODTA1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODTA2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODTA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOC',True,'TIP_CATT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CACODTA2)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CACODTA2))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODTA2)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODTA2) and !this.bDontReportError
            deferred_cp_zoom('TIP_CATT','*','TCCODICE',cp_AbsName(oSource.parent,'oCACODTA2_1_4'),i_cWhere,'GSAR_AOC',"Tipo categorie attributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODTA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CACODTA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CACODTA2)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODTA2 = NVL(_Link_.TCCODICE,space(5))
      this.w_CADESTA2 = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CACODTA2 = space(5)
      endif
      this.w_CADESTA2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(UPPER(.w_CACODTA1) <= UPPER(.w_CACODTA2)) or empty(.w_CACODTA2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice categoria attributo iniziale � maggiore di quello finale")
        endif
        this.w_CACODTA2 = space(5)
        this.w_CADESTA2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODTA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCACODIC1_1_1.value==this.w_CACODIC1)
      this.oPgFrm.Page1.oPag.oCACODIC1_1_1.value=this.w_CACODIC1
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODIC2_1_2.value==this.w_CACODIC2)
      this.oPgFrm.Page1.oPag.oCACODIC2_1_2.value=this.w_CACODIC2
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODTA1_1_3.value==this.w_CACODTA1)
      this.oPgFrm.Page1.oPag.oCACODTA1_1_3.value=this.w_CACODTA1
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODTA2_1_4.value==this.w_CACODTA2)
      this.oPgFrm.Page1.oPag.oCACODTA2_1_4.value=this.w_CACODTA2
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESNO1_1_10.value==this.w_CADESNO1)
      this.oPgFrm.Page1.oPag.oCADESNO1_1_10.value=this.w_CADESNO1
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESNO2_1_11.value==this.w_CADESNO2)
      this.oPgFrm.Page1.oPag.oCADESNO2_1_11.value=this.w_CADESNO2
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESTA1_1_15.value==this.w_CADESTA1)
      this.oPgFrm.Page1.oPag.oCADESTA1_1_15.value=this.w_CADESTA1
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESTA2_1_16.value==this.w_CADESTA2)
      this.oPgFrm.Page1.oPag.oCADESTA2_1_16.value=this.w_CADESTA2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_CACODIC2) OR  (UPPER(.w_CACODIC1)<= UPPER(.w_CACODIC2)))  and not(empty(.w_CACODIC1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODIC1_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice categoria iniziale � maggiore di quello finale")
          case   not((UPPER(.w_CACODIC1) <= UPPER(.w_CACODIC2)) or empty(.w_CACODIC2))  and not(empty(.w_CACODIC2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODIC2_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice categoria iniziale � maggiore di quello finale")
          case   not(empty(.w_CACODTA2) OR  (UPPER(.w_CACODTA1)<= UPPER(.w_CACODTA2)))  and not(empty(.w_CACODTA1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODTA1_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice tipo categorie attributo iniziale � maggiore di quello finale")
          case   not((UPPER(.w_CACODTA1) <= UPPER(.w_CACODTA2)) or empty(.w_CACODTA2))  and not(empty(.w_CACODTA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODTA2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice categoria attributo iniziale � maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsof_scaPag1 as StdContainer
  Width  = 572
  height = 228
  stdWidth  = 572
  stdheight = 228
  resizeXpos=315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODIC1_1_1 as StdField with uid="FLQRTXUTOU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CACODIC1", cQueryName = "CACODIC1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice categoria iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione dal codice categoria",;
    HelpContextID = 131511209,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=116, Top=7, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_CACODIC1"

  func oCACODIC1_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODIC1_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODIC1_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oCACODIC1_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oCACODIC1_1_1.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CACODIC1
     i_obj.ecpSave()
  endproc

  add object oCACODIC2_1_2 as StdField with uid="VIKAEKGHKM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CACODIC2", cQueryName = "CACODIC2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice categoria iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione al codice categoria",;
    HelpContextID = 131511208,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=116, Top=30, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_CACODIC2"

  func oCACODIC2_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODIC2_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODIC2_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oCACODIC2_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oCACODIC2_1_2.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CACODIC2
     i_obj.ecpSave()
  endproc

  add object oCACODTA1_1_3 as StdField with uid="TCWRRAKJBJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CACODTA1", cQueryName = "CACODTA1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice tipo categorie attributo iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione dal codice tipo categorie attributo",;
    HelpContextID = 215397289,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=116, Top=61, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_CATT", cZoomOnZoom="GSAR_AOC", oKey_1_1="TCCODICE", oKey_1_2="this.w_CACODTA1"

  func oCACODTA1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODTA1_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODTA1_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_CATT','*','TCCODICE',cp_AbsName(this.parent,'oCACODTA1_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOC',"Tipo categorie attributo",'',this.parent.oContained
  endproc
  proc oCACODTA1_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CACODTA1
     i_obj.ecpSave()
  endproc

  add object oCACODTA2_1_4 as StdField with uid="WAYVPYJULU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CACODTA2", cQueryName = "CACODTA2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice categoria attributo iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione al codice tipo categorie attributo",;
    HelpContextID = 215397288,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=116, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_CATT", cZoomOnZoom="GSAR_AOC", oKey_1_1="TCCODICE", oKey_1_2="this.w_CACODTA2"

  func oCACODTA2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODTA2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODTA2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_CATT','*','TCCODICE',cp_AbsName(this.parent,'oCACODTA2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOC',"Tipo categorie attributo",'',this.parent.oContained
  endproc
  proc oCACODTA2_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CACODTA2
     i_obj.ecpSave()
  endproc


  add object oObj_1_5 as cp_outputCombo with uid="ZLERXIVPWT",left=116, top=152, width=446,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181281306


  add object oBtn_1_6 as StdButton with uid="NKRIGPTXTA",left=462, top=178, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 177612518;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="PSEBADBYTD",left=514, top=178, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 36558586;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCADESNO1_1_10 as StdField with uid="RCFFKFXLNJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CADESNO1", cQueryName = "CADESNO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32547753,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=269, Top=7, InputMask=replicate('X',40)

  add object oCADESNO2_1_11 as StdField with uid="TKYSFKDMDA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CADESNO2", cQueryName = "CADESNO2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32547752,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=269, Top=30, InputMask=replicate('X',40)

  add object oCADESTA1_1_15 as StdField with uid="LLKWFBMWIC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CADESTA1", cQueryName = "CADESTA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200319913,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=269, Top=61, InputMask=replicate('X',40)

  add object oCADESTA2_1_16 as StdField with uid="XCHTPCGHKU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CADESTA2", cQueryName = "CADESTA2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200319912,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=269, Top=84, InputMask=replicate('X',40)

  add object oStr_1_8 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=9, Top=7,;
    Alignment=1, Width=103, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="BRSOYYEROT",Visible=.t., Left=9, Top=30,;
    Alignment=1, Width=103, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="PXGRYNJQTA",Visible=.t., Left=9, Top=152,;
    Alignment=1, Width=103, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="UXSAGSXEYV",Visible=.t., Left=9, Top=61,;
    Alignment=1, Width=103, Height=18,;
    Caption="Da codice tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RWVKJZVNSM",Visible=.t., Left=9, Top=84,;
    Alignment=1, Width=103, Height=18,;
    Caption="A codice tipo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_sca','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
