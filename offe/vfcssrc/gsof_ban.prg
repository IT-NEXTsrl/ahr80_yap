* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_ban                                                        *
*              Zoom contatti nominativi                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-30                                                      *
* Last revis.: 2010-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_ban",oParentObject)
return(i_retval)

define class tgsof_ban as StdBatch
  * --- Local variables
  w_NOCODICE = space(15)
  w_NCCODCON = space(15)
  w_PROG = .NULL.
  w_DXBTN = .f.
  w_GSAR_MCN = .NULL.
  w_NUMREC = 0
  w_LOADREQU = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LOADREQU = .F.
    this.w_DXBTN = Type("g_oMenu.oKey")<>"U"
    if this.w_DXBTN
      * --- Lanciato tramite tasto destro 
      this.w_NOCODICE = Nvl(g_oMenu.oKey(1,3),"")
      this.w_NCCODCON = Nvl(g_oMenu.oKey(2,3),"")
    else
      if isnull(i_curform)
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      this.w_NOCODICE = &cCurs..NCCODICE
      this.w_NCCODCON = &cCurs..NCCODCON
    endif
    this.w_PROG = GSAR_ANO()
    if this.w_DXBTN
      * --- Nel caso ho premuto tasto destro sul controllo apro la anagrafica sul codice 
      *     presente nella variabile
      if g_oMenu.cBatchType$"AML"
        * --- Apri, modifica o carica
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_NOCODICE = this.w_NOCODICE
        this.w_PROG.ecpSave()     
        if g_oMenu.cBatchType$"ML"
          * --- Modifica
          this.w_PROG.ecpEdit()     
        endif
        this.w_LOADREQU = g_oMenu.cBatchType="L"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      if this.w_PROG.bSEC1
        this.w_PROG.w_NOCODICE = this.w_NOCODICE
        this.w_PROG.QueryKeySet("NOCODICE='"+this.w_NOCODICE+"'")     
        this.w_PROG.LoadRecWarn()     
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        L_OLDERR=ON("ERROR")
        ON ERROR L_FErr=.T.
        L_T=1/"A"
        ON ERROR &L_OLDERR
      endif
    endif
    this.w_PROG = .NULL.
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PROG.oPgFrm.ActivePage = 5
    if !EMPTY(NVL(this.w_NCCODCON, " ")) OR this.w_LOADREQU
      this.w_GSAR_MCN = this.w_PROG.GSAR_MCN
      if this.w_LOADREQU
        this.w_GSAR_MCN.AddRow()     
      else
        this.w_GSAR_MCN.FirstRow()     
        this.w_NUMREC = this.w_GSAR_MCN.Search("t_NCCODCON='"+ALLTRIM(this.w_NCCODCON)+"'")
        if this.w_NUMREC<>-1
          this.w_GSAR_MCN.SetRow(this.w_NUMREC)     
        endif
      endif
      this.w_GSAR_MCN.oPgfrm.Page1.oPag.oBody.SetFocus()     
      this.w_GSAR_MCN = .NULL.
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
