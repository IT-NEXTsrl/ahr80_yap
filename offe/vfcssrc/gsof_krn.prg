* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_krn                                                        *
*              Ricerca nominativi                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2009-06-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_krn",oParentObject))

* --- Class definition
define class tgsof_krn as StdForm
  Top    = 0
  Left   = 3

  * --- Standard Properties
  Width  = 744
  Height = 453+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-06-30"
  HelpContextID=152426135
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=74

  * --- Constant Properties
  _IDX = 0
  ZONE_IDX = 0
  TIP_CATT_IDX = 0
  RUO_CONT_IDX = 0
  ORI_NOMI_IDX = 0
  OFF_NOMI_IDX = 0
  GRU_PRIO_IDX = 0
  GRU_NOMI_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  CAT_ATTR_IDX = 0
  AGENTI_IDX = 0
  cPrg = "gsof_krn"
  cComment = "Ricerca nominativi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_RACODATT = space(10)
  w_DESCAT = space(35)
  w_DESATT = space(35)
  w_SNTIPNOM = space(1)
  w_TIPOPE = space(1)
  o_TIPOPE = space(1)
  w_CODOPE = 0
  w_DESOPE = space(35)
  w_CODORI = space(5)
  w_DESORI = space(35)
  w_SNCODGRU = space(5)
  w_DESCGN = space(35)
  w_CONTATTO = space(35)
  w_ORDNOM = space(1)
  w_SERCOD = space(20)
  w_TIPCAT1 = space(5)
  w_TIPCAT2 = space(5)
  w_TIPCAT3 = space(5)
  w_TIPCAT4 = space(5)
  w_TIPCAT5 = space(5)
  w_TIPCAT6 = space(5)
  w_TIPCAT7 = space(5)
  w_TIPCAT8 = space(5)
  w_TIPCAT9 = space(5)
  w_COD = space(20)
  w_DESCAT1 = space(35)
  w_DESATT1 = space(35)
  w_DESCAT2 = space(35)
  w_DESATT2 = space(35)
  w_DESCAT3 = space(35)
  w_DESATT3 = space(35)
  w_DESCAT4 = space(35)
  w_DESATT4 = space(35)
  w_DESCAT5 = space(35)
  w_DESATT5 = space(35)
  w_DESCAT6 = space(35)
  w_DESATT6 = space(35)
  w_DESCAT7 = space(35)
  w_DESATT7 = space(35)
  w_DESCAT8 = space(35)
  w_DESATT8 = space(35)
  w_DESCAT9 = space(35)
  w_DESATT9 = space(35)
  w_SNCODPRI = space(5)
  w_DESPRIO = space(35)
  w_SNNUMINI = 0
  w_SNNUMFIN = 0
  w_SNCODIC1 = space(20)
  w_SNCODIC2 = space(20)
  w_STDESNO1 = space(40)
  w_OBSOLETI = space(1)
  w_STDESNO2 = space(40)
  w_LOCALITA = space(30)
  w_LOCALITA = space(30)
  w_SNPROVIN = space(2)
  w_SNPROVIN = space(2)
  w_DESZONE = space(35)
  w_DESAGE = space(35)
  w_SNCODZON = space(3)
  w_SNCODAGE = space(5)
  w_SNCDRUO = space(10)
  w_DESRUO = space(35)
  w_TIPCAT = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_DTOBAGE = ctod('  /  /  ')
  w_RACODAT1 = space(10)
  w_RACODAT2 = space(10)
  w_RACODAT3 = space(10)
  w_RACODAT4 = space(10)
  w_RACODAT5 = space(10)
  w_RACODAT6 = space(10)
  w_RACODAT7 = space(10)
  w_RACODAT8 = space(10)
  w_RACODAT9 = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_ZoomAll = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_krnPag1","gsof_krn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsof_krnPag2","gsof_krn",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRACODATT_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomAll = this.oPgFrm.Pages(1).oPag.ZoomAll
    DoDefault()
    proc Destroy()
      this.w_ZoomAll = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='ZONE'
    this.cWorkTables[2]='TIP_CATT'
    this.cWorkTables[3]='RUO_CONT'
    this.cWorkTables[4]='ORI_NOMI'
    this.cWorkTables[5]='OFF_NOMI'
    this.cWorkTables[6]='GRU_PRIO'
    this.cWorkTables[7]='GRU_NOMI'
    this.cWorkTables[8]='CPUSERS'
    this.cWorkTables[9]='CPGROUPS'
    this.cWorkTables[10]='CAT_ATTR'
    this.cWorkTables[11]='AGENTI'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RACODATT=space(10)
      .w_DESCAT=space(35)
      .w_DESATT=space(35)
      .w_SNTIPNOM=space(1)
      .w_TIPOPE=space(1)
      .w_CODOPE=0
      .w_DESOPE=space(35)
      .w_CODORI=space(5)
      .w_DESORI=space(35)
      .w_SNCODGRU=space(5)
      .w_DESCGN=space(35)
      .w_CONTATTO=space(35)
      .w_ORDNOM=space(1)
      .w_SERCOD=space(20)
      .w_TIPCAT1=space(5)
      .w_TIPCAT2=space(5)
      .w_TIPCAT3=space(5)
      .w_TIPCAT4=space(5)
      .w_TIPCAT5=space(5)
      .w_TIPCAT6=space(5)
      .w_TIPCAT7=space(5)
      .w_TIPCAT8=space(5)
      .w_TIPCAT9=space(5)
      .w_COD=space(20)
      .w_DESCAT1=space(35)
      .w_DESATT1=space(35)
      .w_DESCAT2=space(35)
      .w_DESATT2=space(35)
      .w_DESCAT3=space(35)
      .w_DESATT3=space(35)
      .w_DESCAT4=space(35)
      .w_DESATT4=space(35)
      .w_DESCAT5=space(35)
      .w_DESATT5=space(35)
      .w_DESCAT6=space(35)
      .w_DESATT6=space(35)
      .w_DESCAT7=space(35)
      .w_DESATT7=space(35)
      .w_DESCAT8=space(35)
      .w_DESATT8=space(35)
      .w_DESCAT9=space(35)
      .w_DESATT9=space(35)
      .w_SNCODPRI=space(5)
      .w_DESPRIO=space(35)
      .w_SNNUMINI=0
      .w_SNNUMFIN=0
      .w_SNCODIC1=space(20)
      .w_SNCODIC2=space(20)
      .w_STDESNO1=space(40)
      .w_OBSOLETI=space(1)
      .w_STDESNO2=space(40)
      .w_LOCALITA=space(30)
      .w_LOCALITA=space(30)
      .w_SNPROVIN=space(2)
      .w_SNPROVIN=space(2)
      .w_DESZONE=space(35)
      .w_DESAGE=space(35)
      .w_SNCODZON=space(3)
      .w_SNCODAGE=space(5)
      .w_SNCDRUO=space(10)
      .w_DESRUO=space(35)
      .w_TIPCAT=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DTOBAGE=ctod("  /  /  ")
      .w_RACODAT1=space(10)
      .w_RACODAT2=space(10)
      .w_RACODAT3=space(10)
      .w_RACODAT4=space(10)
      .w_RACODAT5=space(10)
      .w_RACODAT6=space(10)
      .w_RACODAT7=space(10)
      .w_RACODAT8=space(10)
      .w_RACODAT9=space(10)
      .w_OBTEST=ctod("  /  /  ")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_RACODATT))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,3,.f.)
        .w_SNTIPNOM = 'X'
        .w_TIPOPE = 'A'
        .w_CODOPE = IIF(.w_TIPOPE<>'A',0,.w_CODOPE)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODOPE))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_CODORI))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_SNCODGRU))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,12,.f.)
        .w_ORDNOM = 'D'
        .w_SERCOD = NVL(.w_ZoomAll.getVar('NOCODICE'), SPACE(20))
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_TIPCAT1))
          .link_2_1('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_TIPCAT2))
          .link_2_2('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_TIPCAT3))
          .link_2_3('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_TIPCAT4))
          .link_2_4('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_TIPCAT5))
          .link_2_5('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_TIPCAT6))
          .link_2_6('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_TIPCAT7))
          .link_2_7('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_TIPCAT8))
          .link_2_8('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_TIPCAT9))
          .link_2_9('Full')
        endif
        .w_COD = ' '
        .DoRTCalc(25,43,.f.)
        if not(empty(.w_SNCODPRI))
          .link_2_30('Full')
        endif
          .DoRTCalc(44,44,.f.)
        .w_SNNUMINI = 0
        .w_SNNUMFIN = 999999
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_SNCODIC1))
          .link_2_36('Full')
        endif
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_SNCODIC2))
          .link_2_37('Full')
        endif
          .DoRTCalc(49,49,.f.)
        .w_OBSOLETI = 'N'
        .DoRTCalc(51,58,.f.)
        if not(empty(.w_SNCODZON))
          .link_2_52('Full')
        endif
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_SNCODAGE))
          .link_2_53('Full')
        endif
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_SNCDRUO))
          .link_2_55('Full')
        endif
        .DoRTCalc(61,62,.f.)
        if not(empty(.w_TIPCAT))
          .link_1_25('Full')
        endif
        .DoRTCalc(63,65,.f.)
        if not(empty(.w_RACODAT1))
          .link_2_60('Full')
        endif
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_RACODAT2))
          .link_2_61('Full')
        endif
        .DoRTCalc(67,67,.f.)
        if not(empty(.w_RACODAT3))
          .link_2_62('Full')
        endif
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_RACODAT4))
          .link_2_63('Full')
        endif
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_RACODAT5))
          .link_2_64('Full')
        endif
        .DoRTCalc(70,70,.f.)
        if not(empty(.w_RACODAT6))
          .link_2_65('Full')
        endif
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_RACODAT7))
          .link_2_66('Full')
        endif
        .DoRTCalc(72,72,.f.)
        if not(empty(.w_RACODAT8))
          .link_2_67('Full')
        endif
        .DoRTCalc(73,73,.f.)
        if not(empty(.w_RACODAT9))
          .link_2_68('Full')
        endif
        .w_OBTEST = i_datsys
      .oPgFrm.Page1.oPag.ZoomAll.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_69.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_70.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_71.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_72.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_73.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_74.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_75.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_76.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_78.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_TIPOPE<>.w_TIPOPE
            .w_CODOPE = IIF(.w_TIPOPE<>'A',0,.w_CODOPE)
          .link_1_6('Full')
        endif
        .DoRTCalc(7,13,.t.)
            .w_SERCOD = NVL(.w_ZoomAll.getVar('NOCODICE'), SPACE(20))
          .link_2_1('Full')
          .link_2_2('Full')
          .link_2_3('Full')
          .link_2_4('Full')
          .link_2_5('Full')
          .link_2_6('Full')
          .link_2_7('Full')
          .link_2_8('Full')
          .link_2_9('Full')
        .DoRTCalc(24,61,.t.)
          .link_1_25('Full')
        .DoRTCalc(63,73,.t.)
            .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.ZoomAll.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_69.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_70.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_71.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_72.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_73.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_74.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_75.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_76.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_78.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomAll.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_69.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_70.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_71.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_72.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_73.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_74.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_75.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_76.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_78.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODOPE_1_6.enabled = this.oPgFrm.Page1.oPag.oCODOPE_1_6.mCond()
    this.oPgFrm.Page2.oPag.oSNNUMINI_2_32.enabled = this.oPgFrm.Page2.oPag.oSNNUMINI_2_32.mCond()
    this.oPgFrm.Page2.oPag.oSNNUMFIN_2_33.enabled = this.oPgFrm.Page2.oPag.oSNNUMFIN_2_33.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT1_2_60.enabled = this.oPgFrm.Page2.oPag.oRACODAT1_2_60.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT2_2_61.enabled = this.oPgFrm.Page2.oPag.oRACODAT2_2_61.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT3_2_62.enabled = this.oPgFrm.Page2.oPag.oRACODAT3_2_62.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT4_2_63.enabled = this.oPgFrm.Page2.oPag.oRACODAT4_2_63.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT5_2_64.enabled = this.oPgFrm.Page2.oPag.oRACODAT5_2_64.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT6_2_65.enabled = this.oPgFrm.Page2.oPag.oRACODAT6_2_65.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT7_2_66.enabled = this.oPgFrm.Page2.oPag.oRACODAT7_2_66.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT8_2_67.enabled = this.oPgFrm.Page2.oPag.oRACODAT8_2_67.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT9_2_68.enabled = this.oPgFrm.Page2.oPag.oRACODAT9_2_68.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oLOCALITA_2_41.visible=!this.oPgFrm.Page2.oPag.oLOCALITA_2_41.mHide()
    this.oPgFrm.Page2.oPag.oLOCALITA_2_42.visible=!this.oPgFrm.Page2.oPag.oLOCALITA_2_42.mHide()
    this.oPgFrm.Page2.oPag.oSNPROVIN_2_46.visible=!this.oPgFrm.Page2.oPag.oSNPROVIN_2_46.mHide()
    this.oPgFrm.Page2.oPag.oSNPROVIN_2_47.visible=!this.oPgFrm.Page2.oPag.oSNPROVIN_2_47.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomAll.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_69.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_70.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_71.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_72.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_73.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_74.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_75.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_76.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_78.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RACODATT
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODATT))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODATT)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODATT) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODATT_1_1'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODATT)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODATT = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODATT = space(10)
      endif
      this.w_DESATT = space(35)
      this.w_TIPCAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODOPE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODOPE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODOPE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODOPE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCODOPE_1_6'),i_cWhere,'',"Elenco operatori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODOPE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODOPE = NVL(_Link_.CODE,0)
      this.w_DESOPE = NVL(_Link_.NAME,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODOPE = 0
      endif
      this.w_DESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODORI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AON',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_CODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_CODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oCODORI_1_8'),i_cWhere,'GSAR_AON',"Origine",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_CODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_CODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_DESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODORI = space(5)
      endif
      this.w_DESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODGRU
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGN',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_SNCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_SNCODGRU))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODGRU)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oSNCODGRU_1_10'),i_cWhere,'GSAR_AGN',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_SNCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_SNCODGRU)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODGRU = NVL(_Link_.GNCODICE,space(5))
      this.w_DESCGN = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODGRU = space(5)
      endif
      this.w_DESCGN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT1
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT1)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT1 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT1 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT1 = space(5)
      endif
      this.w_DESCAT1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT2
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT2)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT2 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT2 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT2 = space(5)
      endif
      this.w_DESCAT2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT3
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT3)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT3 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT3 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT3 = space(5)
      endif
      this.w_DESCAT3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT4
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT4)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT4 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT4 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT4 = space(5)
      endif
      this.w_DESCAT4 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT5
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT5)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT5 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT5 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT5 = space(5)
      endif
      this.w_DESCAT5 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT6
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT6)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT6 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT6 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT6 = space(5)
      endif
      this.w_DESCAT6 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT7
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT7)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT7 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT7 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT7 = space(5)
      endif
      this.w_DESCAT7 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT8
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT8)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT8 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT8 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT8 = space(5)
      endif
      this.w_DESCAT8 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT9
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT9)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT9 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT9 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT9 = space(5)
      endif
      this.w_DESCAT9 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODPRI
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODPRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AGP',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_SNCODPRI)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_SNCODPRI))
          select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODPRI)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODPRI) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oSNCODPRI_2_30'),i_cWhere,'GSOF_AGP',"Gruppi priorit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODPRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_SNCODPRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_SNCODPRI)
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODPRI = NVL(_Link_.GPCODICE,space(5))
      this.w_DESPRIO = NVL(_Link_.GPDESCRI,space(35))
      this.w_SNNUMINI = NVL(_Link_.GPNUMINI,0)
      this.w_SNNUMFIN = NVL(_Link_.GPNUMFIN,0)
    else
      if i_cCtrl<>'Load'
        this.w_SNCODPRI = space(5)
      endif
      this.w_DESPRIO = space(35)
      this.w_SNNUMINI = 0
      this.w_SNNUMFIN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODPRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODIC1
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODIC1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_SNCODIC1)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_SNCODIC1))
          select NOCODICE,NODESCRI,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODIC1)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODIC1) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oSNCODIC1_2_36'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODIC1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_SNCODIC1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_SNCODIC1)
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODIC1 = NVL(_Link_.NOCODICE,space(20))
      this.w_STDESNO1 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODIC1 = space(20)
      endif
      this.w_STDESNO1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_SNCODIC2)) OR  (UPPER(.w_SNCODIC1)<= UPPER(.w_SNCODIC2))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_SNCODIC1 = space(20)
        this.w_STDESNO1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODIC1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODIC2
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODIC2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_SNCODIC2)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_SNCODIC2))
          select NOCODICE,NODESCRI,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODIC2)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODIC2) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oSNCODIC2_2_37'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODIC2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_SNCODIC2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_SNCODIC2)
            select NOCODICE,NODESCRI,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODIC2 = NVL(_Link_.NOCODICE,space(20))
      this.w_STDESNO2 = NVL(_Link_.NODESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODIC2 = space(20)
      endif
      this.w_STDESNO2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((UPPER(.w_SNCODIC1) <= UPPER(.w_SNCODIC2)) or (empty(.w_SNCODIC2))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_SNCODIC2 = space(20)
        this.w_STDESNO2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODIC2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODZON
  func Link_2_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_SNCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_SNCODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oSNCODZON_2_52'),i_cWhere,'',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_SNCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_SNCODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZONE = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODZON = space(3)
      endif
      this.w_DESZONE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCODAGE
  func Link_2_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_SNCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_SNCODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_SNCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_SNCODAGE)+"%");

            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SNCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oSNCODAGE_2_53'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_SNCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_SNCODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SNCDRUO
  func Link_2_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RUO_CONT_IDX,3]
    i_lTable = "RUO_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2], .t., this.RUO_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SNCDRUO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ARC',True,'RUO_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RCCODICE like "+cp_ToStrODBC(trim(this.w_SNCDRUO)+"%");

          i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RCCODICE',trim(this.w_SNCDRUO))
          select RCCODICE,RCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SNCDRUO)==trim(_Link_.RCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SNCDRUO) and !this.bDontReportError
            deferred_cp_zoom('RUO_CONT','*','RCCODICE',cp_AbsName(oSource.parent,'oSNCDRUO_2_55'),i_cWhere,'GSAR_ARC',"Ruolo contatti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',oSource.xKey(1))
            select RCCODICE,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SNCDRUO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(this.w_SNCDRUO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',this.w_SNCDRUO)
            select RCCODICE,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SNCDRUO = NVL(_Link_.RCCODICE,space(10))
      this.w_DESRUO = NVL(_Link_.RCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SNCDRUO = space(10)
      endif
      this.w_DESRUO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2])+'\'+cp_ToStr(_Link_.RCCODICE,1)
      cp_ShowWarn(i_cKey,this.RUO_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SNCDRUO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT1
  func Link_2_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT1))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT1)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT1) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT1_2_60'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT1)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT1 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT1 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT1 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT1 = space(10)
      endif
      this.w_DESATT1 = space(35)
      this.w_TIPCAT1 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT2
  func Link_2_61(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT2))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT2)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT2) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT2_2_61'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT2)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT2 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT2 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT2 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT2 = space(10)
      endif
      this.w_DESATT2 = space(35)
      this.w_TIPCAT2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT3
  func Link_2_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT3))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT3)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT3) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT3_2_62'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT3)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT3 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT3 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT3 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT3 = space(10)
      endif
      this.w_DESATT3 = space(35)
      this.w_TIPCAT3 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT4
  func Link_2_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT4)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT4))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT4)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT4) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT4_2_63'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT4)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT4 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT4 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT4 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT4 = space(10)
      endif
      this.w_DESATT4 = space(35)
      this.w_TIPCAT4 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT5
  func Link_2_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT5)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT5))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT5)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT5) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT5_2_64'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT5)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT5 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT5 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT5 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT5 = space(10)
      endif
      this.w_DESATT5 = space(35)
      this.w_TIPCAT5 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT6
  func Link_2_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT6)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT6))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT6)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT6) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT6_2_65'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT6)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT6 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT6 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT6 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT6 = space(10)
      endif
      this.w_DESATT6 = space(35)
      this.w_TIPCAT6 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT7
  func Link_2_66(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT7)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT7))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT7)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT7) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT7_2_66'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT7)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT7 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT7 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT7 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT7 = space(10)
      endif
      this.w_DESATT7 = space(35)
      this.w_TIPCAT7 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT8
  func Link_2_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT8)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT8))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT8)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT8) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT8_2_67'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT8)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT8 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT8 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT8 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT8 = space(10)
      endif
      this.w_DESATT8 = space(35)
      this.w_TIPCAT8 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT9
  func Link_2_68(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT9)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT9))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT9)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT9) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT9_2_68'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT9)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT9 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT9 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT9 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT9 = space(10)
      endif
      this.w_DESATT9 = space(35)
      this.w_TIPCAT9 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRACODATT_1_1.value==this.w_RACODATT)
      this.oPgFrm.Page1.oPag.oRACODATT_1_1.value=this.w_RACODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_2.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_2.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_3.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_3.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oSNTIPNOM_1_4.RadioValue()==this.w_SNTIPNOM)
      this.oPgFrm.Page1.oPag.oSNTIPNOM_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPE_1_5.RadioValue()==this.w_TIPOPE)
      this.oPgFrm.Page1.oPag.oTIPOPE_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODOPE_1_6.value==this.w_CODOPE)
      this.oPgFrm.Page1.oPag.oCODOPE_1_6.value=this.w_CODOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOPE_1_7.value==this.w_DESOPE)
      this.oPgFrm.Page1.oPag.oDESOPE_1_7.value=this.w_DESOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODORI_1_8.value==this.w_CODORI)
      this.oPgFrm.Page1.oPag.oCODORI_1_8.value=this.w_CODORI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESORI_1_9.value==this.w_DESORI)
      this.oPgFrm.Page1.oPag.oDESORI_1_9.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page1.oPag.oSNCODGRU_1_10.value==this.w_SNCODGRU)
      this.oPgFrm.Page1.oPag.oSNCODGRU_1_10.value=this.w_SNCODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCGN_1_11.value==this.w_DESCGN)
      this.oPgFrm.Page1.oPag.oDESCGN_1_11.value=this.w_DESCGN
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTATTO_1_12.value==this.w_CONTATTO)
      this.oPgFrm.Page1.oPag.oCONTATTO_1_12.value=this.w_CONTATTO
    endif
    if not(this.oPgFrm.Page1.oPag.oORDNOM_1_14.RadioValue()==this.w_ORDNOM)
      this.oPgFrm.Page1.oPag.oORDNOM_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT1_2_11.value==this.w_DESCAT1)
      this.oPgFrm.Page2.oPag.oDESCAT1_2_11.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT1_2_12.value==this.w_DESATT1)
      this.oPgFrm.Page2.oPag.oDESATT1_2_12.value=this.w_DESATT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT2_2_13.value==this.w_DESCAT2)
      this.oPgFrm.Page2.oPag.oDESCAT2_2_13.value=this.w_DESCAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT2_2_14.value==this.w_DESATT2)
      this.oPgFrm.Page2.oPag.oDESATT2_2_14.value=this.w_DESATT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT3_2_15.value==this.w_DESCAT3)
      this.oPgFrm.Page2.oPag.oDESCAT3_2_15.value=this.w_DESCAT3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT3_2_16.value==this.w_DESATT3)
      this.oPgFrm.Page2.oPag.oDESATT3_2_16.value=this.w_DESATT3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT4_2_17.value==this.w_DESCAT4)
      this.oPgFrm.Page2.oPag.oDESCAT4_2_17.value=this.w_DESCAT4
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT4_2_18.value==this.w_DESATT4)
      this.oPgFrm.Page2.oPag.oDESATT4_2_18.value=this.w_DESATT4
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT5_2_19.value==this.w_DESCAT5)
      this.oPgFrm.Page2.oPag.oDESCAT5_2_19.value=this.w_DESCAT5
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT5_2_20.value==this.w_DESATT5)
      this.oPgFrm.Page2.oPag.oDESATT5_2_20.value=this.w_DESATT5
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT6_2_21.value==this.w_DESCAT6)
      this.oPgFrm.Page2.oPag.oDESCAT6_2_21.value=this.w_DESCAT6
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT6_2_22.value==this.w_DESATT6)
      this.oPgFrm.Page2.oPag.oDESATT6_2_22.value=this.w_DESATT6
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT7_2_23.value==this.w_DESCAT7)
      this.oPgFrm.Page2.oPag.oDESCAT7_2_23.value=this.w_DESCAT7
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT7_2_24.value==this.w_DESATT7)
      this.oPgFrm.Page2.oPag.oDESATT7_2_24.value=this.w_DESATT7
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT8_2_25.value==this.w_DESCAT8)
      this.oPgFrm.Page2.oPag.oDESCAT8_2_25.value=this.w_DESCAT8
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT8_2_26.value==this.w_DESATT8)
      this.oPgFrm.Page2.oPag.oDESATT8_2_26.value=this.w_DESATT8
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT9_2_27.value==this.w_DESCAT9)
      this.oPgFrm.Page2.oPag.oDESCAT9_2_27.value=this.w_DESCAT9
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT9_2_28.value==this.w_DESATT9)
      this.oPgFrm.Page2.oPag.oDESATT9_2_28.value=this.w_DESATT9
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODPRI_2_30.value==this.w_SNCODPRI)
      this.oPgFrm.Page2.oPag.oSNCODPRI_2_30.value=this.w_SNCODPRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRIO_2_31.value==this.w_DESPRIO)
      this.oPgFrm.Page2.oPag.oDESPRIO_2_31.value=this.w_DESPRIO
    endif
    if not(this.oPgFrm.Page2.oPag.oSNNUMINI_2_32.value==this.w_SNNUMINI)
      this.oPgFrm.Page2.oPag.oSNNUMINI_2_32.value=this.w_SNNUMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oSNNUMFIN_2_33.value==this.w_SNNUMFIN)
      this.oPgFrm.Page2.oPag.oSNNUMFIN_2_33.value=this.w_SNNUMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODIC1_2_36.value==this.w_SNCODIC1)
      this.oPgFrm.Page2.oPag.oSNCODIC1_2_36.value=this.w_SNCODIC1
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODIC2_2_37.value==this.w_SNCODIC2)
      this.oPgFrm.Page2.oPag.oSNCODIC2_2_37.value=this.w_SNCODIC2
    endif
    if not(this.oPgFrm.Page2.oPag.oSTDESNO1_2_38.value==this.w_STDESNO1)
      this.oPgFrm.Page2.oPag.oSTDESNO1_2_38.value=this.w_STDESNO1
    endif
    if not(this.oPgFrm.Page2.oPag.oOBSOLETI_2_39.RadioValue()==this.w_OBSOLETI)
      this.oPgFrm.Page2.oPag.oOBSOLETI_2_39.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSTDESNO2_2_40.value==this.w_STDESNO2)
      this.oPgFrm.Page2.oPag.oSTDESNO2_2_40.value=this.w_STDESNO2
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCALITA_2_41.value==this.w_LOCALITA)
      this.oPgFrm.Page2.oPag.oLOCALITA_2_41.value=this.w_LOCALITA
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCALITA_2_42.value==this.w_LOCALITA)
      this.oPgFrm.Page2.oPag.oLOCALITA_2_42.value=this.w_LOCALITA
    endif
    if not(this.oPgFrm.Page2.oPag.oSNPROVIN_2_46.value==this.w_SNPROVIN)
      this.oPgFrm.Page2.oPag.oSNPROVIN_2_46.value=this.w_SNPROVIN
    endif
    if not(this.oPgFrm.Page2.oPag.oSNPROVIN_2_47.value==this.w_SNPROVIN)
      this.oPgFrm.Page2.oPag.oSNPROVIN_2_47.value=this.w_SNPROVIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZONE_2_50.value==this.w_DESZONE)
      this.oPgFrm.Page2.oPag.oDESZONE_2_50.value=this.w_DESZONE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_51.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_51.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODZON_2_52.value==this.w_SNCODZON)
      this.oPgFrm.Page2.oPag.oSNCODZON_2_52.value=this.w_SNCODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCODAGE_2_53.value==this.w_SNCODAGE)
      this.oPgFrm.Page2.oPag.oSNCODAGE_2_53.value=this.w_SNCODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oSNCDRUO_2_55.value==this.w_SNCDRUO)
      this.oPgFrm.Page2.oPag.oSNCDRUO_2_55.value=this.w_SNCDRUO
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRUO_2_56.value==this.w_DESRUO)
      this.oPgFrm.Page2.oPag.oDESRUO_2_56.value=this.w_DESRUO
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT1_2_60.value==this.w_RACODAT1)
      this.oPgFrm.Page2.oPag.oRACODAT1_2_60.value=this.w_RACODAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT2_2_61.value==this.w_RACODAT2)
      this.oPgFrm.Page2.oPag.oRACODAT2_2_61.value=this.w_RACODAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT3_2_62.value==this.w_RACODAT3)
      this.oPgFrm.Page2.oPag.oRACODAT3_2_62.value=this.w_RACODAT3
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT4_2_63.value==this.w_RACODAT4)
      this.oPgFrm.Page2.oPag.oRACODAT4_2_63.value=this.w_RACODAT4
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT5_2_64.value==this.w_RACODAT5)
      this.oPgFrm.Page2.oPag.oRACODAT5_2_64.value=this.w_RACODAT5
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT6_2_65.value==this.w_RACODAT6)
      this.oPgFrm.Page2.oPag.oRACODAT6_2_65.value=this.w_RACODAT6
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT7_2_66.value==this.w_RACODAT7)
      this.oPgFrm.Page2.oPag.oRACODAT7_2_66.value=this.w_RACODAT7
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT8_2_67.value==this.w_RACODAT8)
      this.oPgFrm.Page2.oPag.oRACODAT8_2_67.value=this.w_RACODAT8
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT9_2_68.value==this.w_RACODAT9)
      this.oPgFrm.Page2.oPag.oRACODAT9_2_68.value=this.w_RACODAT9
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_SNNUMINI<= .w_SNNUMFIN)  and (EMPTY(.w_SNCODPRI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSNNUMINI_2_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice priorit� nominativo iniziale � maggiore di quello finale.")
          case   not(.w_SNNUMFIN>= .w_SNNUMINI)  and (EMPTY(.w_SNCODPRI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSNNUMFIN_2_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice priorit� nominativo iniziale � maggiore di quello finale.")
          case   not(((empty(.w_SNCODIC2)) OR  (UPPER(.w_SNCODIC1)<= UPPER(.w_SNCODIC2))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_SNCODIC1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSNCODIC1_2_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto")
          case   not(((UPPER(.w_SNCODIC1) <= UPPER(.w_SNCODIC2)) or (empty(.w_SNCODIC2))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_SNCODIC2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSNCODIC2_2_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_krn
      * --- Disabilita tasto F10
         i_bRes=.f.
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOPE = this.w_TIPOPE
    return

enddefine

* --- Define pages as container
define class tgsof_krnPag1 as StdContainer
  Width  = 740
  height = 454
  stdWidth  = 740
  stdheight = 454
  resizeXpos=379
  resizeYpos=265
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRACODATT_1_1 as StdField with uid="FXJGYGRWAP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RACODATT", cQueryName = "RACODATT",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459030,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=107, Top=13, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODATT"

  func oRACODATT_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODATT_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODATT_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODATT_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODATT_1_1.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODATT
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_2 as StdField with uid="WUEOCBDKUE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 244428746,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=210, Top=13, InputMask=replicate('X',35)

  add object oDESATT_1_3 as StdField with uid="HMQDUTQTVT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=359, Top=13, InputMask=replicate('X',35)


  add object oSNTIPNOM_1_4 as StdCombo with uid="GNJTDAXDDK",rtseq=4,rtrep=.f.,left=607,top=13,width=128,height=21;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 60528013;
    , cFormVar="w_SNTIPNOM",RowSource=""+"Tutti,"+"Da valutare,"+"Potenziale,"+"Lead,"+"Prospect,"+"Cliente,"+"Congelato,"+"Non interessante", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSNTIPNOM_1_4.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'L',;
    iif(this.value =5,'P',;
    iif(this.value =6,'C',;
    iif(this.value =7,'G',;
    iif(this.value =8,'N',;
    space(1))))))))))
  endfunc
  func oSNTIPNOM_1_4.GetRadio()
    this.Parent.oContained.w_SNTIPNOM = this.RadioValue()
    return .t.
  endfunc

  func oSNTIPNOM_1_4.SetRadio()
    this.Parent.oContained.w_SNTIPNOM=trim(this.Parent.oContained.w_SNTIPNOM)
    this.value = ;
      iif(this.Parent.oContained.w_SNTIPNOM=='X',1,;
      iif(this.Parent.oContained.w_SNTIPNOM=='T',2,;
      iif(this.Parent.oContained.w_SNTIPNOM=='Z',3,;
      iif(this.Parent.oContained.w_SNTIPNOM=='L',4,;
      iif(this.Parent.oContained.w_SNTIPNOM=='P',5,;
      iif(this.Parent.oContained.w_SNTIPNOM=='C',6,;
      iif(this.Parent.oContained.w_SNTIPNOM=='G',7,;
      iif(this.Parent.oContained.w_SNTIPNOM=='N',8,;
      0))))))))
  endfunc


  add object oTIPOPE_1_5 as StdCombo with uid="KZBPIWKVJJ",rtseq=5,rtrep=.f.,left=107,top=41,width=101,height=21;
    , ToolTipText = "Tipo operatore: assegnato, non assegnato, tutti";
    , HelpContextID = 211147466;
    , cFormVar="w_TIPOPE",RowSource=""+"Assegnato,"+"Non assegnato,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPE_1_5.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPOPE_1_5.GetRadio()
    this.Parent.oContained.w_TIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPE_1_5.SetRadio()
    this.Parent.oContained.w_TIPOPE=trim(this.Parent.oContained.w_TIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPE=='A',1,;
      iif(this.Parent.oContained.w_TIPOPE=='N',2,;
      iif(this.Parent.oContained.w_TIPOPE=='T',3,;
      0)))
  endfunc

  add object oCODOPE_1_6 as StdField with uid="AFUOFUUABX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODOPE", cQueryName = "CODOPE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 211195354,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=210, Top=41, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CODOPE"

  func oCODOPE_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOPE='A')
    endwith
   endif
  endfunc

  func oCODOPE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODOPE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODOPE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCODOPE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco operatori",'',this.parent.oContained
  endproc

  add object oDESOPE_1_7 as StdField with uid="OXGMWWENRK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESOPE", cQueryName = "DESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione operatore",;
    HelpContextID = 211136458,;
   bGlobalFont=.t.,;
    Height=21, Width=155, Left=268, Top=41, InputMask=replicate('X',35)

  add object oCODORI_1_8 as StdField with uid="YMSBZEWCYG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODORI", cQueryName = "CODORI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine reperimento nominitavo",;
    HelpContextID = 141989338,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=501, Top=41, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", cZoomOnZoom="GSAR_AON", oKey_1_1="ONCODICE", oKey_1_2="this.w_CODORI"

  func oCODORI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODORI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODORI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oCODORI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AON',"Origine",'',this.parent.oContained
  endproc
  proc oCODORI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AON()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ONCODICE=this.parent.oContained.w_CODORI
     i_obj.ecpSave()
  endproc

  add object oDESORI_1_9 as StdField with uid="NKVOEYSFDR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione origine",;
    HelpContextID = 141930442,;
   bGlobalFont=.t.,;
    Height=21, Width=169, Left=568, Top=41, InputMask=replicate('X',35)

  add object oSNCODGRU_1_10 as StdField with uid="UNPJLMPEEC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SNCODGRU", cQueryName = "SNCODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo di appartenenza",;
    HelpContextID = 190227845,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=68, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", cZoomOnZoom="GSAR_AGN", oKey_1_1="GNCODICE", oKey_1_2="this.w_SNCODGRU"

  func oSNCODGRU_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODGRU_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODGRU_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oSNCODGRU_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGN',"Gruppi nominativi",'',this.parent.oContained
  endproc
  proc oSNCODGRU_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GNCODICE=this.parent.oContained.w_SNCODGRU
     i_obj.ecpSave()
  endproc

  add object oDESCGN_1_11 as StdField with uid="OXGVGSHZVB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCGN", cQueryName = "DESCGN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo nominativo",;
    HelpContextID = 70365130,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=173, Top=68, InputMask=replicate('X',35)

  add object oCONTATTO_1_12 as StdField with uid="SQNMVIDPCM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CONTATTO", cQueryName = "CONTATTO",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Contatto",;
    HelpContextID = 243332491,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=501, Top=68, InputMask=replicate('X',35)


  add object oBtn_1_13 as StdButton with uid="VDUJCYNPWD",left=33, top=409, width=48,height=45,;
    CpPicture="bmp\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la scheda nominativo";
    , HelpContextID = 157186161;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSOF_BRN(this.Parent.oContained,"APRI_NOMINATIVO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERCOD))
      endwith
    endif
  endfunc


  add object oORDNOM_1_14 as StdCombo with uid="ABZPFJBGDG",rtseq=13,rtrep=.f.,left=498,top=422,width=151,height=21;
    , ToolTipText = "Ordinamento";
    , HelpContextID = 78090778;
    , cFormVar="w_ORDNOM",RowSource=""+"Descrizione,"+"Priorit�,"+"Tipo,"+"Assegnato il,"+"Operatore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORDNOM_1_14.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'P',;
    iif(this.value =3,'T',;
    iif(this.value =4,'A',;
    iif(this.value =5,'O',;
    space(1)))))))
  endfunc
  func oORDNOM_1_14.GetRadio()
    this.Parent.oContained.w_ORDNOM = this.RadioValue()
    return .t.
  endfunc

  func oORDNOM_1_14.SetRadio()
    this.Parent.oContained.w_ORDNOM=trim(this.Parent.oContained.w_ORDNOM)
    this.value = ;
      iif(this.Parent.oContained.w_ORDNOM=='D',1,;
      iif(this.Parent.oContained.w_ORDNOM=='P',2,;
      iif(this.Parent.oContained.w_ORDNOM=='T',3,;
      iif(this.Parent.oContained.w_ORDNOM=='A',4,;
      iif(this.Parent.oContained.w_ORDNOM=='O',5,;
      0)))))
  endfunc


  add object oBtn_1_15 as StdButton with uid="FOXXTEANXJ",left=684, top=409, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 207522538;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSOF_BRN(this.Parent.oContained,"RICERCA_NOMINATIVO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomAll as cp_zoombox with uid="TIHUYNVHMX",left=7, top=100, width=727,height=305,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_NOMI",cZoomFile="GSOF_KRN",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 206447130

  add object oStr_1_16 as StdString with uid="APHYIAFQKO",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=100, Height=18,;
    Caption="Attributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="VTGVPSLHTP",Visible=.t., Left=397, Top=424,;
    Alignment=1, Width=94, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="VRMHPTRCMR",Visible=.t., Left=3, Top=41,;
    Alignment=1, Width=100, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="EGOZMSROEY",Visible=.t., Left=560, Top=13,;
    Alignment=1, Width=44, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="IBSCBYSJER",Visible=.t., Left=429, Top=41,;
    Alignment=1, Width=70, Height=18,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="GYZPKGJJXH",Visible=.t., Left=3, Top=68,;
    Alignment=1, Width=100, Height=18,;
    Caption="Gr. nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="EHKOJGSWYD",Visible=.t., Left=430, Top=68,;
    Alignment=1, Width=69, Height=18,;
    Caption="Contatto:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_krnPag2 as StdContainer
  Width  = 740
  height = 454
  stdWidth  = 740
  stdheight = 454
  resizeXpos=572
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESCAT1_2_11 as StdField with uid="RQHPFELTYB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 244428746,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=228, Top=214, InputMask=replicate('X',35)

  add object oDESATT1_2_12 as StdField with uid="BNZQCLINZM",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESATT1", cQueryName = "DESATT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=411, Top=214, InputMask=replicate('X',35)

  add object oDESCAT2_2_13 as StdField with uid="WJCRSTIJKM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCAT2", cQueryName = "DESCAT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 244428746,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=228, Top=239, InputMask=replicate('X',35)

  add object oDESATT2_2_14 as StdField with uid="ZBFJSTINNA",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESATT2", cQueryName = "DESATT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=411, Top=239, InputMask=replicate('X',35)

  add object oDESCAT3_2_15 as StdField with uid="NKTPRZLJCA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCAT3", cQueryName = "DESCAT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 244428746,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=228, Top=264, InputMask=replicate('X',35)

  add object oDESATT3_2_16 as StdField with uid="TBHEYGIQXL",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESATT3", cQueryName = "DESATT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=411, Top=264, InputMask=replicate('X',35)

  add object oDESCAT4_2_17 as StdField with uid="KQQPWWSABB",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCAT4", cQueryName = "DESCAT4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 244428746,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=228, Top=289, InputMask=replicate('X',35)

  add object oDESATT4_2_18 as StdField with uid="KWTSJDMBDL",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESATT4", cQueryName = "DESATT4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=411, Top=289, InputMask=replicate('X',35)

  add object oDESCAT5_2_19 as StdField with uid="LDOMVUPTHB",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCAT5", cQueryName = "DESCAT5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=228, Top=314, InputMask=replicate('X',35)

  add object oDESATT5_2_20 as StdField with uid="ELRCCPHXUB",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESATT5", cQueryName = "DESATT5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=411, Top=314, InputMask=replicate('X',35)

  add object oDESCAT6_2_21 as StdField with uid="IBXPDXZSZR",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESCAT6", cQueryName = "DESCAT6",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=228, Top=339, InputMask=replicate('X',35)

  add object oDESATT6_2_22 as StdField with uid="IIBGTGBLKA",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESATT6", cQueryName = "DESATT6",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=411, Top=339, InputMask=replicate('X',35)

  add object oDESCAT7_2_23 as StdField with uid="OKAELXSYCJ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESCAT7", cQueryName = "DESCAT7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=228, Top=364, InputMask=replicate('X',35)

  add object oDESATT7_2_24 as StdField with uid="PQZJXXURBV",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESATT7", cQueryName = "DESATT7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=411, Top=364, InputMask=replicate('X',35)

  add object oDESCAT8_2_25 as StdField with uid="VPSVPPKLZC",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCAT8", cQueryName = "DESCAT8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=228, Top=389, InputMask=replicate('X',35)

  add object oDESATT8_2_26 as StdField with uid="GFJMETBLWA",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESATT8", cQueryName = "DESATT8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=411, Top=389, InputMask=replicate('X',35)

  add object oDESCAT9_2_27 as StdField with uid="IBDWPEMNIP",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCAT9", cQueryName = "DESCAT9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=228, Top=414, InputMask=replicate('X',35)

  add object oDESATT9_2_28 as StdField with uid="ZPDGAGPBDR",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESATT9", cQueryName = "DESATT9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=411, Top=414, InputMask=replicate('X',35)

  add object oSNCODPRI_2_30 as StdField with uid="KUEIEJAHIM",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SNCODPRI", cQueryName = "SNCODPRI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Priorit� attribuita al nominativo",;
    HelpContextID = 39232913,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=9, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", cZoomOnZoom="GSOF_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_SNCODPRI"

  func oSNCODPRI_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODPRI_2_30.ecpDrop(oSource)
    this.Parent.oContained.link_2_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODPRI_2_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oSNCODPRI_2_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AGP',"Gruppi priorit�",'',this.parent.oContained
  endproc
  proc oSNCODPRI_2_30.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_SNCODPRI
     i_obj.ecpSave()
  endproc

  add object oDESPRIO_2_31 as StdField with uid="XTKEVJMOAS",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESPRIO", cQueryName = "DESPRIO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione priorit�",;
    HelpContextID = 141864906,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=189, Top=9, InputMask=replicate('X',35)

  add object oSNNUMINI_2_32 as StdField with uid="CAPUQROJMA",rtseq=45,rtrep=.f.,;
    cFormVar = "w_SNNUMINI", cQueryName = "SNNUMINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice priorit� nominativo iniziale � maggiore di quello finale.",;
    ToolTipText = "Valore priorit� iniziale",;
    HelpContextID = 146797969,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=506, Top=9, cSayPict='"999999"', cGetPict='"999999"'

  func oSNNUMINI_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_SNCODPRI))
    endwith
   endif
  endfunc

  func oSNNUMINI_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SNNUMINI<= .w_SNNUMFIN)
    endwith
    return bRes
  endfunc

  add object oSNNUMFIN_2_33 as StdField with uid="ZSOPDFPLJQ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_SNNUMFIN", cQueryName = "SNNUMFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il codice priorit� nominativo iniziale � maggiore di quello finale.",;
    ToolTipText = "Valore priorit� finale",;
    HelpContextID = 71305844,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=654, Top=9, cSayPict='"999999"', cGetPict='"999999"'

  func oSNNUMFIN_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_SNCODPRI))
    endwith
   endif
  endfunc

  func oSNNUMFIN_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SNNUMFIN>= .w_SNNUMINI)
    endwith
    return bRes
  endfunc

  add object oSNCODIC1_2_36 as StdField with uid="FLQRTXUTOU",rtseq=47,rtrep=.f.,;
    cFormVar = "w_SNCODIC1", cQueryName = "SNCODIC1",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Selezione dal codice nominativo",;
    HelpContextID = 156673449,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=124, Top=34, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_SNCODIC1"

  func oSNCODIC1_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODIC1_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODIC1_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oSNCODIC1_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oSNCODIC1_2_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_SNCODIC1
     i_obj.ecpSave()
  endproc

  add object oSNCODIC2_2_37 as StdField with uid="VIKAEKGHKM",rtseq=48,rtrep=.f.,;
    cFormVar = "w_SNCODIC2", cQueryName = "SNCODIC2",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice nominativo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Selezione al codice nominativo",;
    HelpContextID = 156673448,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=124, Top=59, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_SNCODIC2"

  func oSNCODIC2_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODIC2_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODIC2_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oSNCODIC2_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oSNCODIC2_2_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_SNCODIC2
     i_obj.ecpSave()
  endproc

  add object oSTDESNO1_2_38 as StdField with uid="RCFFKFXLNJ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_STDESNO1", cQueryName = "STDESNO1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 57708457,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=294, Top=34, InputMask=replicate('X',40)

  add object oOBSOLETI_2_39 as StdCheck with uid="LLGAKQJVHH",rtseq=50,rtrep=.f.,left=604, top=37, caption="Solo obsoleti",;
    ToolTipText = "Visualizza solo nominativi obsoleti",;
    HelpContextID = 215331281,;
    cFormVar="w_OBSOLETI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oOBSOLETI_2_39.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOBSOLETI_2_39.GetRadio()
    this.Parent.oContained.w_OBSOLETI = this.RadioValue()
    return .t.
  endfunc

  func oOBSOLETI_2_39.SetRadio()
    this.Parent.oContained.w_OBSOLETI=trim(this.Parent.oContained.w_OBSOLETI)
    this.value = ;
      iif(this.Parent.oContained.w_OBSOLETI=='S',1,;
      0)
  endfunc

  add object oSTDESNO2_2_40 as StdField with uid="TKYSFKDMDA",rtseq=51,rtrep=.f.,;
    cFormVar = "w_STDESNO2", cQueryName = "STDESNO2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 57708456,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=294, Top=59, InputMask=replicate('X',40)

  add object oLOCALITA_2_41 as StdField with uid="PPKIBKNUJR",rtseq=52,rtrep=.f.,;
    cFormVar = "w_LOCALITA", cQueryName = "LOCALITA",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 149202185,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=124, Top=84, InputMask=replicate('X',30)

  func oLOCALITA_2_41.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oLOCALITA_2_42 as StdField with uid="OTMSAIAULF",rtseq=53,rtrep=.f.,;
    cFormVar = "w_LOCALITA", cQueryName = "LOCALITA",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 149202185,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=124, Top=84, InputMask=replicate('X',30), bHasZoom = .t. 

  func oLOCALITA_2_42.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oLOCALITA_2_42.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_LOCALITA",".w_SNPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSNPROVIN_2_46 as StdField with uid="UCFVECZVAP",rtseq=54,rtrep=.f.,;
    cFormVar = "w_SNPROVIN", cQueryName = "SNPROVIN",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 73214580,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=453, Top=84, InputMask=replicate('X',2)

  func oSNPROVIN_2_46.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oSNPROVIN_2_47 as StdField with uid="LFUIBJWDEQ",rtseq=55,rtrep=.f.,;
    cFormVar = "w_SNPROVIN", cQueryName = "SNPROVIN",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 73214580,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=446, Top=84, InputMask=replicate('X',2), bHasZoom = .t. 

  func oSNPROVIN_2_47.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oSNPROVIN_2_47.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_SNPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDESZONE_2_50 as StdField with uid="PJLDMIGCKS",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESZONE", cQueryName = "DESZONE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione zone",;
    HelpContextID = 60469194,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=196, Top=109, InputMask=replicate('X',35)

  add object oDESAGE_2_51 as StdField with uid="NZBPQJVISD",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 221491146,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=196, Top=134, InputMask=replicate('X',35)

  add object oSNCODZON_2_52 as StdField with uid="JEIWNKIDFS",rtseq=58,rtrep=.f.,;
    cFormVar = "w_SNCODZON", cQueryName = "SNCODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice zona di appartenenza",;
    HelpContextID = 139896204,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=109, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_SNCODZON"

  func oSNCODZON_2_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODZON_2_52.ecpDrop(oSource)
    this.Parent.oContained.link_2_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODZON_2_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oSNCODZON_2_52'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Zone",'',this.parent.oContained
  endproc

  add object oSNCODAGE_2_53 as StdField with uid="UDANMVNSCD",rtseq=59,rtrep=.f.,;
    cFormVar = "w_SNCODAGE", cQueryName = "SNCODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente",;
    HelpContextID = 245979755,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_SNCODAGE"

  func oSNCODAGE_2_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_53('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCODAGE_2_53.ecpDrop(oSource)
    this.Parent.oContained.link_2_53('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCODAGE_2_53.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oSNCODAGE_2_53'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oSNCODAGE_2_53.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_SNCODAGE
     i_obj.ecpSave()
  endproc

  add object oSNCDRUO_2_55 as StdField with uid="BTWAVAOLMZ",rtseq=60,rtrep=.f.,;
    cFormVar = "w_SNCDRUO", cQueryName = "SNCDRUO",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Ruolo della persona contattata",;
    HelpContextID = 209823194,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=160, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="RUO_CONT", cZoomOnZoom="GSAR_ARC", oKey_1_1="RCCODICE", oKey_1_2="this.w_SNCDRUO"

  func oSNCDRUO_2_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_55('Part',this)
    endwith
    return bRes
  endfunc

  proc oSNCDRUO_2_55.ecpDrop(oSource)
    this.Parent.oContained.link_2_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSNCDRUO_2_55.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RUO_CONT','*','RCCODICE',cp_AbsName(this.parent,'oSNCDRUO_2_55'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ARC',"Ruolo contatti",'',this.parent.oContained
  endproc
  proc oSNCDRUO_2_55.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ARC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RCCODICE=this.parent.oContained.w_SNCDRUO
     i_obj.ecpSave()
  endproc

  add object oDESRUO_2_56 as StdField with uid="JBUFKLOFBA",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESRUO", cQueryName = "DESRUO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 37924810,;
   bGlobalFont=.t.,;
    Height=21, Width=263, Left=224, Top=160, InputMask=replicate('X',35)

  add object oRACODAT1_2_60 as StdField with uid="QBQJLRJLTX",rtseq=65,rtrep=.f.,;
    cFormVar = "w_RACODAT1", cQueryName = "RACODAT1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459065,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=214, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT1"

  func oRACODAT1_2_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODATT)))
    endwith
   endif
  endfunc

  func oRACODAT1_2_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_60('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT1_2_60.ecpDrop(oSource)
    this.Parent.oContained.link_2_60('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT1_2_60.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT1_2_60'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT1_2_60.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT1
     i_obj.ecpSave()
  endproc

  add object oRACODAT2_2_61 as StdField with uid="ZGTNSJFFHG",rtseq=66,rtrep=.f.,;
    cFormVar = "w_RACODAT2", cQueryName = "RACODAT2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459064,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=239, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT2"

  func oRACODAT2_2_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT1)))
    endwith
   endif
  endfunc

  func oRACODAT2_2_61.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_61('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT2_2_61.ecpDrop(oSource)
    this.Parent.oContained.link_2_61('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT2_2_61.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT2_2_61'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT2_2_61.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT2
     i_obj.ecpSave()
  endproc

  add object oRACODAT3_2_62 as StdField with uid="YAADXVOHNX",rtseq=67,rtrep=.f.,;
    cFormVar = "w_RACODAT3", cQueryName = "RACODAT3",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459063,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=264, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT3"

  func oRACODAT3_2_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT2)))
    endwith
   endif
  endfunc

  func oRACODAT3_2_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT3_2_62.ecpDrop(oSource)
    this.Parent.oContained.link_2_62('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT3_2_62.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT3_2_62'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT3_2_62.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT3
     i_obj.ecpSave()
  endproc

  add object oRACODAT4_2_63 as StdField with uid="UKTASPMEPX",rtseq=68,rtrep=.f.,;
    cFormVar = "w_RACODAT4", cQueryName = "RACODAT4",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459062,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=289, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT4"

  func oRACODAT4_2_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT3)))
    endwith
   endif
  endfunc

  func oRACODAT4_2_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_63('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT4_2_63.ecpDrop(oSource)
    this.Parent.oContained.link_2_63('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT4_2_63.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT4_2_63'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT4_2_63.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT4
     i_obj.ecpSave()
  endproc

  add object oRACODAT5_2_64 as StdField with uid="BUPMCOLWZP",rtseq=69,rtrep=.f.,;
    cFormVar = "w_RACODAT5", cQueryName = "RACODAT5",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459061,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=314, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT5"

  func oRACODAT5_2_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT4)))
    endwith
   endif
  endfunc

  func oRACODAT5_2_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT5_2_64.ecpDrop(oSource)
    this.Parent.oContained.link_2_64('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT5_2_64.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT5_2_64'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT5_2_64.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT5
     i_obj.ecpSave()
  endproc

  add object oRACODAT6_2_65 as StdField with uid="UGZCRFAVOZ",rtseq=70,rtrep=.f.,;
    cFormVar = "w_RACODAT6", cQueryName = "RACODAT6",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459060,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=339, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT6"

  func oRACODAT6_2_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT5)))
    endwith
   endif
  endfunc

  func oRACODAT6_2_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_65('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT6_2_65.ecpDrop(oSource)
    this.Parent.oContained.link_2_65('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT6_2_65.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT6_2_65'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT6_2_65.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT6
     i_obj.ecpSave()
  endproc

  add object oRACODAT7_2_66 as StdField with uid="NBCPGPORSO",rtseq=71,rtrep=.f.,;
    cFormVar = "w_RACODAT7", cQueryName = "RACODAT7",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459059,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=364, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT7"

  func oRACODAT7_2_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT6)))
    endwith
   endif
  endfunc

  func oRACODAT7_2_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_66('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT7_2_66.ecpDrop(oSource)
    this.Parent.oContained.link_2_66('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT7_2_66.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT7_2_66'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT7_2_66.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT7
     i_obj.ecpSave()
  endproc

  add object oRACODAT8_2_67 as StdField with uid="KTMOQZCVRK",rtseq=72,rtrep=.f.,;
    cFormVar = "w_RACODAT8", cQueryName = "RACODAT8",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459058,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=389, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT8"

  func oRACODAT8_2_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT7)))
    endwith
   endif
  endfunc

  func oRACODAT8_2_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT8_2_67.ecpDrop(oSource)
    this.Parent.oContained.link_2_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT8_2_67.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT8_2_67'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT8_2_67.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT8
     i_obj.ecpSave()
  endproc

  add object oRACODAT9_2_68 as StdField with uid="RRLXYHGTCR",rtseq=73,rtrep=.f.,;
    cFormVar = "w_RACODAT9", cQueryName = "RACODAT9",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 22459057,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=124, Top=414, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT9"

  func oRACODAT9_2_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT8)))
    endwith
   endif
  endfunc

  func oRACODAT9_2_68.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_68('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT9_2_68.ecpDrop(oSource)
    this.Parent.oContained.link_2_68('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT9_2_68.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT9_2_68'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT9_2_68.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT9
     i_obj.ecpSave()
  endproc


  add object oObj_2_69 as cp_runprogram with uid="QZVAIZWTMN",left=2, top=468, width=370,height=19,;
    caption='GSOF_BR2(1)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('1')",;
    cEvent = "w_RACODAT1 Changed,w_RACODATT Changed",;
    nPag=2;
    , HelpContextID = 246160872


  add object oObj_2_70 as cp_runprogram with uid="KJXNRPYDTF",left=2, top=514, width=251,height=19,;
    caption='GSOF_BR2(2)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('2')",;
    cEvent = "w_RACODAT2 Changed",;
    nPag=2;
    , HelpContextID = 246160616


  add object oObj_2_71 as cp_runprogram with uid="SZAKSNTISM",left=2, top=491, width=251,height=19,;
    caption='GSOF_BR2(3)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('3')",;
    cEvent = "w_RACODAT3 Changed",;
    nPag=2;
    , HelpContextID = 246160360


  add object oObj_2_72 as cp_runprogram with uid="TFGWNFTNEG",left=2, top=537, width=251,height=19,;
    caption='GSOF_BR2(4)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('4')",;
    cEvent = "w_RACODAT4 Changed",;
    nPag=2;
    , HelpContextID = 246160104


  add object oObj_2_73 as cp_runprogram with uid="QUUSTHOGIA",left=259, top=491, width=251,height=19,;
    caption='GSOF_BR2(5)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('5')",;
    cEvent = "w_RACODAT5 Changed",;
    nPag=2;
    , HelpContextID = 246159848


  add object oObj_2_74 as cp_runprogram with uid="UTLRHCTLQO",left=259, top=514, width=251,height=19,;
    caption='GSOF_BR2(6)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('6')",;
    cEvent = "w_RACODAT6 Changed",;
    nPag=2;
    , HelpContextID = 246159592


  add object oObj_2_75 as cp_runprogram with uid="BWSHPVKQRX",left=259, top=537, width=251,height=19,;
    caption='GSOF_BR2(7)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('7')",;
    cEvent = "w_RACODAT7 Changed",;
    nPag=2;
    , HelpContextID = 246159336


  add object oObj_2_76 as cp_runprogram with uid="LRLLYTYNTI",left=2, top=560, width=251,height=19,;
    caption='GSOF_BR2(8)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('8')",;
    cEvent = "w_RACODAT8 Changed",;
    nPag=2;
    , HelpContextID = 246159080


  add object oObj_2_78 as cp_runprogram with uid="UDJJOCDODD",left=375, top=468, width=370,height=19,;
    caption='GSOF_BR3(N) (Priorit� default)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR3('N')",;
    cEvent = "w_SNCODPRI Changed",;
    nPag=2;
    , HelpContextID = 215826377

  add object oStr_2_10 as StdString with uid="XQIYOIBOJG",Visible=.t., Left=119, Top=187,;
    Alignment=0, Width=166, Height=18,;
    Caption="Altri attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_29 as StdString with uid="NJZGBOAVCW",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=116, Height=18,;
    Caption="Priorit� nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=5, Top=34,;
    Alignment=1, Width=116, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="BRSOYYEROT",Visible=.t., Left=5, Top=59,;
    Alignment=1, Width=116, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="DIDSDLFDYF",Visible=.t., Left=411, Top=9,;
    Alignment=1, Width=92, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="OSCFIYTRUO",Visible=.t., Left=576, Top=9,;
    Alignment=1, Width=74, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="SPZPYYFLGV",Visible=.t., Left=366, Top=84,;
    Alignment=1, Width=77, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="GGMQEJEICX",Visible=.t., Left=5, Top=109,;
    Alignment=1, Width=116, Height=18,;
    Caption="Codice zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="EDIGVBZFUW",Visible=.t., Left=5, Top=134,;
    Alignment=1, Width=116, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="VXDDRJCQVP",Visible=.t., Left=5, Top=159,;
    Alignment=1, Width=116, Height=18,;
    Caption="Ruolo contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="BREXXSYABX",Visible=.t., Left=5, Top=84,;
    Alignment=1, Width=116, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oBox_2_77 as StdBox with uid="UFZFHCAVGL",left=117, top=206, width=484,height=239
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_krn','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
