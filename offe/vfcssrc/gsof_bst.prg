* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bst                                                        *
*              Elaborazione stampa statistiche                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_774]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-31                                                      *
* Last revis.: 2010-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bst",oParentObject,m.pParam)
return(i_retval)

define class tgsof_bst as StdBatch
  * --- Local variables
  pParam = space(4)
  w_CmdSel1 = space(100)
  w_CmdSel2 = space(100)
  w_CmdGrp1 = space(100)
  w_INDICE = 0
  w_CmdOrd = space(100)
  w_CmdGrp = space(100)
  w_CmdSel = space(100)
  w_VARAPP = space(8)
  w_TIPPER = space(2)
  w_CLAIMA = 0
  w_CLAIMB = 0
  w_CLAIMC = 0
  w_CLAIMD = 0
  w_CLAIME = 0
  w_CLAPRA = 0
  w_CLAPRB = 0
  w_CLAPRC = 0
  w_CLAPRD = 0
  w_CLAPRE = 0
  w_TOTREC = 0
  w_SEQREC = 0
  w_TIPATT = space(5)
  w_CODATT = space(10)
  w_DATREG = ctod("  /  /  ")
  w_NCLAIMP = 0
  w_NCLAPRI = 0
  w_CLAIMP = space(18)
  w_CLAPRI = space(6)
  w_STATUS = space(1)
  w_CODOPE = space(4)
  w_CODORI = space(5)
  w_GRUNOM = space(5)
  w_CODZON = space(3)
  w_CODAGE = space(5)
  w_CODAG2 = space(5)
  w_CODNOM = space(20)
  w_DATAPE = ctod("  /  /  ")
  w_DATCHI = ctod("  /  /  ")
  w_CODPER = space(14)
  w_DESPER = space(40)
  w_TOTOFF = 0
  w_TOTIMP = 0
  w_TOTOFFC = 0
  w_TOTIMPC = 0
  w_DURTRA = 0
  w_NUMREV = 0
  w_FLCLIM = .f.
  w_FLCLPR = .f.
  w_VALOFF = space(3)
  w_CAOOFF = 0
  w_SEROFF = space(10)
  w_MESS = space(100)
  w_FILE = space(100)
  w_CODAT1N = space(10)
  w_CODAT2N = space(10)
  w_CODAT3N = space(10)
  w_CODAT4N = space(10)
  w_CODAT5N = space(10)
  w_CODAT6N = space(10)
  w_CODAT7N = space(10)
  w_CODAT8N = space(10)
  w_CODAT1O = space(10)
  w_CODAT2O = space(10)
  w_CODAT3O = space(10)
  w_CODAT4O = space(10)
  w_CODAT5O = space(10)
  w_CODAT6O = space(10)
  w_CODAT7O = space(10)
  w_CODAT8O = space(10)
  w_TIPAT1N = space(5)
  w_TIPAT2N = space(5)
  w_TIPAT3N = space(5)
  w_TIPAT4N = space(5)
  w_TIPAT5N = space(5)
  w_TIPAT6N = space(5)
  w_TIPAT7N = space(5)
  w_TIPAT8N = space(5)
  w_TIPAT1O = space(5)
  w_TIPAT2O = space(5)
  w_TIPAT3O = space(5)
  w_TIPAT4O = space(5)
  w_TIPAT5O = space(5)
  w_TIPAT6O = space(5)
  w_TIPAT7O = space(5)
  w_TIPAT8O = space(5)
  w_TIPRAG = 0
  w_TROV = .f.
  * --- WorkFile variables
  CRIDOFFE_idx=0
  TMPATTNO_idx=0
  TMPATTOF_idx=0
  TMPSEZOF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Statistiche Offerte (da GSSO_SST)
    * --- REPO= Stampa su Report ; EXCE=Genera Foglio Excel
    this.oParentObject.w_FILNOM = IIF(this.oParentObject.w_TIPNOM="X", " ", this.oParentObject.w_TIPNOM)
    this.oParentObject.w_FILSTI = IIF(this.oParentObject.w_STAOFF $ "XIE", "I", "X")
    this.oParentObject.w_FILSTA = IIF(this.oParentObject.w_STAOFF $ "XAE", "A", "X")
    this.oParentObject.w_FILSTC = IIF(this.oParentObject.w_STAOFF $ "XC", "C", "X")
    this.oParentObject.w_FILSTR = IIF(this.oParentObject.w_STAOFF $ "XR", "R", "X")
    this.oParentObject.w_FILSTS = IIF(this.oParentObject.w_STAOFF $ "XS", "S", "X")
    this.w_CODAT1N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_CODAT1)
    this.w_CODAT2N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_CODAT2)
    this.w_CODAT3N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_CODAT3)
    this.w_CODAT4N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_CODAT4)
    this.w_CODAT5N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_CODAT5)
    this.w_CODAT6N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_CODAT6)
    this.w_CODAT7N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_CODAT7)
    this.w_CODAT8N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_CODAT8)
    this.w_CODAT1O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_CODAT1)
    this.w_CODAT2O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_CODAT2)
    this.w_CODAT3O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_CODAT3)
    this.w_CODAT4O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_CODAT4)
    this.w_CODAT5O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_CODAT5)
    this.w_CODAT6O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_CODAT6)
    this.w_CODAT7O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_CODAT7)
    this.w_CODAT8O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_CODAT8)
    this.w_TIPAT1N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_TIPAT1)
    this.w_TIPAT2N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_TIPAT2)
    this.w_TIPAT3N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_TIPAT3)
    this.w_TIPAT4N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_TIPAT4)
    this.w_TIPAT5N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_TIPAT5)
    this.w_TIPAT6N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_TIPAT6)
    this.w_TIPAT7N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_TIPAT7)
    this.w_TIPAT8N = IIF(this.oParentObject.w_TIPFIL="O", " ", this.oParentObject.w_TIPAT8)
    this.w_TIPAT1O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_TIPAT1)
    this.w_TIPAT2O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_TIPAT2)
    this.w_TIPAT3O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_TIPAT3)
    this.w_TIPAT4O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_TIPAT4)
    this.w_TIPAT5O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_TIPAT5)
    this.w_TIPAT6O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_TIPAT6)
    this.w_TIPAT7O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_TIPAT7)
    this.w_TIPAT8O = IIF(this.oParentObject.w_TIPFIL="N", " ", this.oParentObject.w_TIPAT8)
    * --- Utilizzate dalla Query Principale (GSOF_SST)
    * --- Create temporary table TMPSEZOF
    i_nIdx=cp_AddTableDef('TMPSEZOF') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\OFFE\EXE\QUERY\GSOF1SST.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPSEZOF_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPATTNO
    i_nIdx=cp_AddTableDef('TMPATTNO') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\OFFE\EXE\QUERY\GSOF2SST.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPATTNO_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPATTOF
    i_nIdx=cp_AddTableDef('TMPATTOF') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\OFFE\EXE\QUERY\GSOF3SST.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPATTOF_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_INDICE = 0
    this.w_TIPRAG = 0
    do case
      case this.pParam="REPO" AND EMPTY(this.oParentObject.w_REPORT)
        this.w_MESS = "Nessun file di stampa associato al criterio"
        ah_ErrorMsg(this.w_MESS,,"")
        i_retcode = 'stop'
        return
      case this.pParam="EXCE" AND EMPTY(this.oParentObject.w_EXCEL)
        this.w_MESS = "Nessun foglio Excel associato al criterio"
        ah_ErrorMsg(this.w_MESS,,"")
        i_retcode = 'stop'
        return
    endcase
    Dimension Campi(13,4)
    for i=1 to 13
    campi(i,2)="N"
    endfor
    * --- Nomi Campi
    Campi(1,1) ="TIPATT"
    Campi(2,1) ="CODATT"
    Campi(3,1) ="CODPER"
    Campi(4,1) ="CLAIMP"
    Campi(5,1) ="CLAPRI"
    Campi(6,1) ="STATUS"
    Campi(7,1) ="CODOPE"
    Campi(8,1) ="CODORI"
    Campi(9,1) ="GRUNOM"
    Campi(10,1) ="CODZON"
    Campi(11,1) ="CODAGE"
    Campi(12,1) ="CODAG2"
    Campi(13,1) ="CODNOM"
    * --- Descrizioni 
    Campi(1,3)=ah_MsgFormat("Tipologia:")
    Campi(2,3)=ah_MsgFormat("Attributo:")
    Campi(3,3)=ah_MsgFormat("Periodo:")
    Campi(4,3)=ah_MsgFormat("Classe importo fino a:")
    Campi(5,3)=ah_MsgFormat("Classe priorit� fino a:")
    Campi(6,3)=ah_MsgFormat("Status:")
    Campi(7,3)=ah_MsgFormat("Operatore:")
    Campi(8,3)=ah_MsgFormat("Origine:")
    Campi(9,3)=ah_MsgFormat("Gruppo nominativo:")
    Campi(10,3)=ah_MsgFormat("Zona:")
    Campi(11,3)=ah_MsgFormat("Agente:")
    Campi(12,3)=ah_MsgFormat("Capoarea:")
    Campi(13,3)=ah_MsgFormat("Nominativo:")
    Campi(1,4) ="LOOKTAB('TIP_CATT', 'TCDESCRI', 'TCCODICE', TIPATT)"
    Campi(2,4) ="LOOKTAB('CAT_ATTR', 'CTDESCRI', 'CTCODICE', CODATT)"
    Campi(3,4) ="DESPER"
    Campi(4,4) =""
    Campi(5,4) =""
     
 msg1=ah_MsgFormat("'In corso%1'",space(2)) 
 msg2=ah_MsgFormat("'Inviata%1'",space(3)) 
 msg3=ah_MsgFormat("'Confermata'") 
 msg4=ah_MsgFormat("'Chiusa%1'",space(4)) 
 msg5=ah_MsgFormat("'Rifiutata%1'",space(1)) 
 msg6=ah_MsgFormat("'Sospesa%1'",space(3))
    Campi(6,4) ="IIF(STATUS='I', " +msg1+", IIF(STATUS='A', "+ msg2+", IIF(STATUS='C', " + msg3 + ",IIF(STATUS='V', "+ msg4 + ",IIF(STATUS='R', " + msg5+", " + msg6 +")))))"
    Campi(7,4)="IIF(EMPTY(CODOPE), "+ah_MsgFormat("'Nessun operatore%1'",SPACE(4))+", LOOKTAB('CPUSERS', 'NAME', 'CODE', VAL(CODOPE)))"
    Campi(8,4) ="LOOKTAB('ORI_NOMI', 'ONDESCRI', 'ONCODICE', CODORI)"
    Campi(9,4) ="LOOKTAB('GRU_NOMI', 'GNDESCRI', 'GNCODICE', GRUNOM)"
    Campi(10,4) ="LOOKTAB('ZONE', 'ZODESZON', 'ZOCODZON', CODZON)"
    Campi(11,4) ="LOOKTAB('AGENTI', 'AGDESAGE', 'AGCODAGE', CODAGE)"
    Campi(12,4) ="LOOKTAB('AGENTI', 'AGDESAGE', 'AGCODAGE', CODAG2)"
    Campi(13,4) ="LOOKTAB('OFF_NOMI', 'NODESCRI', 'NOCODICE', CODNOM)"
    * --- Controllo se Foglio Excel Mensile
    this.w_CmdSel = ""
    this.w_CmdOrd = ""
    this.w_CmdGrp = ""
    this.w_CmdSel1 = ""
    this.w_CmdSel2 = ""
    this.w_CmdGrp1 = ""
    this.w_TIPPER = "  "
    this.w_FLCLIM = .F.
    this.w_FLCLPR = .F.
    * --- Select from CRIDOFFE
    i_nConn=i_TableProp[this.CRIDOFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CRIDOFFE_idx,2],.t.,this.CRIDOFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CRIDOFFE ";
          +" where CECODICE="+cp_ToStrODBC(this.oParentObject.w_CECODICE)+"";
          +" order by CPROWORD";
           ,"_Curs_CRIDOFFE")
    else
      select * from (i_cTable);
       where CECODICE=this.oParentObject.w_CECODICE;
       order by CPROWORD;
        into cursor _Curs_CRIDOFFE
    endif
    if used('_Curs_CRIDOFFE')
      select _Curs_CRIDOFFE
      locate for 1=1
      do while not(eof())
      * --- Definisco alcune variabili per il report
      if NVL(_Curs_CRIDOFFE.CENUMCAM, 0)<>0 
        * --- Tipo Raggruppamento: 1 = Tipo Attributo ; 2 = Codice Attributo ; 3 = Entrambi
        this.w_TIPRAG = this.w_TIPRAG + IIF(_Curs_CRIDOFFE.CENUMCAM=1, 1, IIF(_Curs_CRIDOFFE.CENUMCAM=2, 2, 0))
        * --- Rilevo che il raggruppamento � stato selezionato
        Campi(_Curs_CRIDOFFE.CENUMCAM, 2)="S"
        if this.pParam="EXCE"
          this.w_CmdSel1 = alltrim(this.w_CmdSel1) + Campi(_Curs_CRIDOFFE.CENUMCAM,1)+","
          this.w_CmdGrp1 = alltrim(this.w_CmdGrp1) +Campi(_Curs_CRIDOFFE.CENUMCAM,1) +","
          this.w_CmdSel2 = Campi(_Curs_CRIDOFFE.CENUMCAM,1) +","
        endif
        * --- Calcolo Descrizione Raggruppamento periodo
        if _Curs_CRIDOFFE.CENUMCAM=3 AND NOT EMPTY(NVL(_Curs_CRIDOFFE.CETIPPER, ""))
          this.w_TIPPER = _Curs_CRIDOFFE.CETIPPER
          do case
            case _Curs_CRIDOFFE.CETIPPER ="AN"
              Campi(3,3)=ah_MsgFormat("Periodo: anno")
            case _Curs_CRIDOFFE.CETIPPER ="SM"
              Campi(3,3)=ah_MsgFormat("Periodo: semestre")
            case _Curs_CRIDOFFE.CETIPPER ="QU"
              Campi(3,3)=ah_MsgFormat("Periodo: quadrimestre")
            case _Curs_CRIDOFFE.CETIPPER ="TR"
              Campi(3,3)=ah_MsgFormat("Periodo: trimestre")
            case _Curs_CRIDOFFE.CETIPPER ="BI"
              Campi(3,3)=ah_MsgFormat("Periodo: bimestre")
            case _Curs_CRIDOFFE.CETIPPER ="ME"
              Campi(3,3)=ah_MsgFormat("Periodo: mese")
            case _Curs_CRIDOFFE.CETIPPER ="SE"
              Campi(3,3)=ah_MsgFormat("Periodo: settimana")
            otherwise
              Campi(3,3)=ah_MsgFormat("Periodo: giorno")
          endcase
        endif
        if _Curs_CRIDOFFE.CENUMCAM=4
          this.w_FLCLIM = .T.
          this.w_CLAIMA = NVL(_Curs_CRIDOFFE.CECLAIMP, 0)
          this.w_CLAIMB = NVL(_Curs_CRIDOFFE.CECLBIMP, 0)
          this.w_CLAIMC = NVL(_Curs_CRIDOFFE.CECLCIMP, 0)
          this.w_CLAIMD = NVL(_Curs_CRIDOFFE.CECLDIMP, 0)
          * --- Converte le Classi Importo da EURO a valuta di Stampa
          if this.oParentObject.w_CODVAL<>g_PERVAL AND this.oParentObject.w_CAMVAL<>0
            this.w_CLAIMA = cp_ROUND(this.w_CLAIMA * this.oParentObject.w_CAMVAL, 2)
            this.w_CLAIMB = cp_ROUND(this.w_CLAIMB * this.oParentObject.w_CAMVAL, 2)
            this.w_CLAIMC = cp_ROUND(this.w_CLAIMC * this.oParentObject.w_CAMVAL, 2)
            this.w_CLAIMD = cp_ROUND(this.w_CLAIMD * this.oParentObject.w_CAMVAL, 2)
          endif
          this.w_CLAIME = IIF(this.w_CLAIMD=0, 0, 999999999999999)
          this.w_CLAIMD = IIF(this.w_CLAIMC=0, 0, IIF(this.w_CLAIMD=0, 999999999999999, this.w_CLAIMD))
          this.w_CLAIMC = IIF(this.w_CLAIMB=0, 0, IIF(this.w_CLAIMC=0, 999999999999999, this.w_CLAIMC))
          this.w_CLAIMB = IIF(this.w_CLAIMA=0, 0, IIF(this.w_CLAIMB=0, 999999999999999, this.w_CLAIMB))
          this.w_CLAIMA = IIF(this.w_CLAIMA=0, 999999999999999, this.w_CLAIMA)
        endif
        if _Curs_CRIDOFFE.CENUMCAM=5
          this.w_FLCLPR = .T.
          this.w_CLAPRA = NVL(_Curs_CRIDOFFE.CECLAPRI, 0)
          this.w_CLAPRB = NVL(_Curs_CRIDOFFE.CECLBPRI, 0)
          this.w_CLAPRC = NVL(_Curs_CRIDOFFE.CECLCPRI, 0)
          this.w_CLAPRD = NVL(_Curs_CRIDOFFE.CECLDPRI, 0)
          this.w_CLAPRE = IIF(this.w_CLAPRD=0, 0, 999999)
          this.w_CLAPRD = IIF(this.w_CLAPRC=0, 0, IIF(this.w_CLAPRD=0, 999999, this.w_CLAPRD))
          this.w_CLAPRC = IIF(this.w_CLAPRB=0, 0, IIF(this.w_CLAPRC=0, 999999, this.w_CLAPRC))
          this.w_CLAPRB = IIF(this.w_CLAPRA=0, 0, IIF(this.w_CLAPRB=0, 999999, this.w_CLAPRB))
          this.w_CLAPRA = IIF(this.w_CLAPRA=0, 999999, this.w_CLAPRA)
        endif
        this.w_INDICE = this.w_INDICE+1
        * --- Costruisce la Select ...
        this.w_CmdOrd = ALLTRIM(this.w_CmdOrd) + CAMPI(_Curs_CRIDOFFE.CENUMCAM,1) +"," 
        this.w_CmdGrp = ALLTRIM(this.w_CmdGrp) + CAMPI(_Curs_CRIDOFFE.CENUMCAM,1) +","
        descr= CAMPI(_Curs_CRIDOFFE.CENUMCAM,3)
        if this.pParam="EXCE"
          this.w_CmdSel = ALLTRIM(this.w_CmdSel) + CAMPI(_Curs_CRIDOFFE.CENUMCAM,1) +","
          * --- Controllo che  il raggruppamento preveda la descrizione
          if NOT EMPTY(CAMPI(_Curs_CRIDOFFE.CENUMCAM,4))
            this.w_CmdSel = ALLTRIM(this.w_CmdSel) + CAMPI(_Curs_CRIDOFFE.CENUMCAM,4) + " AS DES" + CAMPI(_Curs_CRIDOFFE.CENUMCAM,1) +","
          else
            this.w_CmdSel = ALLTRIM(this.w_CmdSel) + " SPACE(1) AS DES" + CAMPI(_Curs_CRIDOFFE.CENUMCAM,1) +","
          endif
        else
          do case
            case Campi(_Curs_CRIDOFFE.CENUMCAM,1)="STATUS"
              * --- Solo Descrizione
              this.w_CmdSel = ALLTRIM(this.w_CmdSel) + CAMPI(_Curs_CRIDOFFE.CENUMCAM,4) + " AS GRUP" + ALLTRIM(STR(this.w_INDICE))+","
            case Campi(_Curs_CRIDOFFE.CENUMCAM,1)="CODPER"
              * --- Solo Descrizione
              this.w_CmdSel = ALLTRIM(this.w_CmdSel) + CAMPI(_Curs_CRIDOFFE.CENUMCAM,4) + " AS GRUP" + ALLTRIM(STR(this.w_INDICE))+","
            otherwise
              this.w_CmdSel = ALLTRIM(this.w_CmdSel) + CAMPI(_Curs_CRIDOFFE.CENUMCAM,1) + " AS GRUP" + ALLTRIM(STR(this.w_INDICE))+","
              * --- Controllo che  il raggruppamento preveda la descrizione
              if NOT EMPTY(CAMPI(_Curs_CRIDOFFE.CENUMCAM,4))
                this.w_CmdSel = ALLTRIM(this.w_CmdSel) + CAMPI(_Curs_CRIDOFFE.CENUMCAM,4) + " AS DESCR" + ALLTRIM(STR(this.w_INDICE))+","
              endif
          endcase
        endif
        this.w_VARAPP = "L_"+"grup"+alltrim(str(this.w_indice))
        appo=this.w_VARAPP
        &APPO= descr
        this.w_VARAPP = "L_"+CAMPI(_Curs_CRIDOFFE.CENUMCAM,1)
        VARAPP = this.w_VARAPP
        &VARAPP=ah_MsgFormat("Attivato")
      endif
        select _Curs_CRIDOFFE
        continue
      enddo
      use
    endif
    * --- Determino i periodi  di elaborazione
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if empty(this.w_CmdSel)
      * --- Controlli sull'esistenza di raggruppamenti nel criterio selezionato.
      this.w_MESS = "Nessun raggruppamento specificato nel criterio"
      ah_ErrorMsg(this.w_MESS,,"")
      i_retcode = 'stop'
      return
    else
      if this.pParam="EXCE"
        for i=1 to 13
        if campi(i,2)="N"
          this.w_CmdSel = ALLTRIM(this.w_CmdSel) + "SPACE(1) AS " + CAMPI(i,1) +","
          * --- Controllo che  il raggruppamento preveda la descrizione
          if NOT EMPTY(CAMPI(i,4))
            this.w_CmdSel = ALLTRIM(this.w_CmdSel) + " SPACE(1) AS DES" + CAMPI(i,1) +","
          endif
        endif
        endfor
      endif
      Sel = this.w_CmdSel + "SUM(TOTIMP) as TOTIMP, SUM(TOTOFF) AS TOTOFF, " 
      Sel = Sel + "SUM(TOTIMPC) AS TOTIMPC, SUM(TOTOFFC) AS TOTOFFC, " 
      Sel = Sel + "SUM(DURTRA) AS DURTRA, SUM(NUMREV) AS NUMREV"
      Ord = LEFT(this.w_CmdOrd , LEN(alltrim(this.w_CmdOrd))-1)
      Grp = LEFT(this.w_CmdGrp , LEN(alltrim(this.w_CmdGrp))-1)
    endif
    ah_Msg("Preparazione dati raggruppati per stampa...",.T.)
    Select &Sel FROM TmpElab GROUP BY &Grp ORDER BY &Ord INTO CURSOR __tmp__
    if reccount("__TMP__") =0
      * --- Controlli sull'esistenza di dati per le selezioni effettuate.
      this.w_MESS = "Nessun elemento riscontrato per le selezioni specificate"
      if upper(g_Application)="ADHOC REVOLUTION"
        ah_ErrorMsg(this.w_MESS,,"")
      else
        ah_ErrorMsg(this.w_MESS,"!","")
      endif
    else
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Chiusura Cursori
    * --- Drop temporary table TMPATTNO
    i_nIdx=cp_GetTableDefIdx('TMPATTNO')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPATTNO')
    endif
    * --- Drop temporary table TMPATTOF
    i_nIdx=cp_GetTableDefIdx('TMPATTOF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPATTOF')
    endif
    * --- Drop temporary table TMPSEZOF
    i_nIdx=cp_GetTableDefIdx('TMPSEZOF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSEZOF')
    endif
    if used ("TmpElab")
      select TmpElab
      Use
    endif
    if USED("TEMPOR")
      SELECT TEMPOR
      USE
    endif
    if used ("__TMP__")
      select __tmp__
      Use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione Variabili
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura Dati Offerte
    * --- Creazione Cursore
    Create Cursor TmpElab (TIPATT C(5), CODATT C(10), CODPER C(14), DESPER C(40), CLAIMP C(20), CLAPRI C(6), ;
    STATUS C(1), CODOPE C(4), CODORI C(5), GRUNOM C(5), CODZON C(3), CODAGE C(5), CODAG2 C(5), CODNOM C(20), SEROFF C(10), ;
    TOTIMP N(18,4), TOTOFF N(5,0), TOTIMPC N(18,4), TOTOFFC N(5,0), DURTRA N(5,0), NUMREV N(4,0))
    * --- Predispongo L'indice
    =wrcursor("TmpElab") 
    do case
      case this.w_TIPRAG=0
        Index On (SEROFF) Tag idx1
      case this.w_TIPRAG=1
        Index On (TIPATT+SEROFF) Tag idx1
      case this.w_TIPRAG=2
        Index On (CODATT+SEROFF) Tag idx1
      otherwise
        * --- Indice su Tipo+Codice+Seriale
        Index On (TIPATT+CODATT+SEROFF) Tag idx1
    endcase
    ah_Msg("Lettura dati offerte",.T.)
    VQ_EXEC("..\OFFE\EXE\QUERY\GSOF_SST.VQR", this, "TEMPOR")
    SELECT TEMPOR
    this.w_TOTREC = RECCOUNT()
    this.w_SEQREC = 0
    GO TOP
    SCAN FOR NOT EMPTY(NVL(SEROFF,""))
    this.w_TIPATT = NVL(TIPATT, SPACE(5))
    this.w_CODATT = NVL(CODATT, SPACE(10))
    this.w_DATREG = CP_TODATE(DATREG)
    this.w_NCLAIMP = NVL(CLAIMP, 0)
    this.w_NCLAPRI = NVL(CLAPRI, 0)
    this.w_STATUS = NVL(STATUS, "I")
    this.w_CODOPE = IIF(NVL(CODOPE,0)=0, SPACE(4), STR(NVL(CODOPE, 0), 4, 0))
    this.w_CODORI = NVL(CODORI, SPACE(5))
    this.w_GRUNOM = NVL(GRUNOM, SPACE(5))
    this.w_CODZON = NVL(CODZON, SPACE(3))
    this.w_CODAGE = NVL(CODAGE, SPACE(5))
    * --- Valorizzo la variabile globale per la selezione degli indirizzi email per l'agente
    i_AGENTCODE = this.w_CODAGE
    this.w_CODAG2 = NVL(CODAG2, SPACE(5))
    this.w_CODNOM = NVL(CODNOM, SPACE(20))
    this.w_VALOFF = NVL(CODVAL, g_PERVAL)
    this.w_CAOOFF = NVL(CAOVAL, 0)
    this.w_SEROFF = NVL(SEROFF, SPACE(10))
    this.w_TOTIMP = NVL(TOTIMP, 0)
    this.w_TOTOFF = 1
    this.w_DATAPE = CP_TODATE(DATAPE)
    if EMPTY(this.w_DATAPE)
      this.w_DATAPE = CP_TODATE(DATREG)
    endif
    this.w_DATCHI = CP_TODATE(DATCHI)
    if EMPTY(this.w_DATCHI)
      this.w_DATCHI = CP_TODATE(DATREG)
    endif
    this.w_TOTIMPC = 0
    this.w_TOTOFFC = 0
    this.w_DURTRA = 0
    this.w_NUMREV = 0
    if this.w_STATUS="C"
      this.w_TOTIMPC = NVL(TOTIMP, 0)
      this.w_TOTOFFC = 1
      this.w_DURTRA = (this.w_DATCHI + 1) - this.w_DATAPE
      this.w_NUMREV = NVL(NUMVER, 1)
    endif
    * --- Converte le Offerte da Valuta Offerta a EURO
    if this.w_VALOFF<>g_PERVAL AND NOT EMPTY(this.w_VALOFF)
      if this.w_CAOOFF=0
        this.w_CAOOFF = GETCAM(this.w_VALOFF, i_DATSYS, 0)
      endif
      if this.w_CAOOFF<>0
        this.w_TOTIMP = cp_ROUND(this.w_TOTIMP / this.w_CAOOFF, 2)
        this.w_TOTIMPC = cp_ROUND(this.w_TOTIMPC / this.w_CAOOFF, 2)
        this.w_NCLAIMP = cp_ROUND(this.w_NCLAIMP / this.w_CAOOFF, 2)
      endif
    endif
    * --- Converte le Offerte da EURO a valuta di Stampa
    if this.oParentObject.w_CODVAL<>g_PERVAL AND this.oParentObject.w_CAMVAL<>0
      this.w_TOTIMP = cp_ROUND(this.w_TOTIMP * this.oParentObject.w_CAMVAL, 2)
      this.w_TOTIMPC = cp_ROUND(this.w_TOTIMPC * this.oParentObject.w_CAMVAL, 2)
      this.w_NCLAIMP = cp_ROUND(this.w_NCLAIMP * this.oParentObject.w_CAMVAL, 2)
    endif
    this.w_CODPER = SPACE(14)
    this.w_DESPER = SPACE(40)
    this.w_CLAIMP = SPACE(18)
    this.w_CLAPRI = SPACE(6)
    if NOT EMPTY(this.w_TIPPER)
      do case
        case this.w_TIPPER ="AN"
          this.w_CODPER = ALLTRIM(STR(YEAR(this.w_DATREG)))
          this.w_DESPER = ""
        case this.w_TIPPER ="SM"
          this.w_CODPER = ALLTRIM(STR(YEAR(this.w_DATREG)))+ALLTRIM(STR(INT((MONTH(this.w_DATREG)+5)/6)))
          this.w_DESPER = ah_MsgFormat("%1 sem. del %2",ALLTRIM(STR(INT((MONTH(this.w_DATREG)+5)/6))),ALLTRIM(STR(YEAR(this.w_DATREG))))
        case this.w_TIPPER ="QU"
          this.w_CODPER = ALLTRIM(STR(YEAR(this.w_DATREG)))+ALLTRIM(STR(INT((MONTH(this.w_DATREG)+3)/4)))
          this.w_DESPER = ah_MsgFormat("%1 quadr. del %2",ALLTRIM(STR(INT((MONTH(this.w_DATREG)+3)/4))),ALLTRIM(STR(YEAR(this.w_DATREG))))
        case this.w_TIPPER ="TR"
          this.w_CODPER = ALLTRIM(STR(YEAR(this.w_DATREG)))+ALLTRIM(STR(INT((MONTH(this.w_DATREG)+2)/3)))
          this.w_DESPER = ah_MsgFormat("%1 trim. del %2",ALLTRIM(STR(INT((MONTH(this.w_DATREG)+2)/3))),ALLTRIM(STR(YEAR(this.w_DATREG))))
        case this.w_TIPPER ="BI"
          this.w_CODPER = ALLTRIM(STR(YEAR(this.w_DATREG)))+ALLTRIM(STR(INT((MONTH(this.w_DATREG)+1)/2)))
          this.w_DESPER = ah_MsgFormat("%1 bim. del %2",ALLTRIM(STR(INT((MONTH(this.w_DATREG)+1)/2))),ALLTRIM(STR(YEAR(this.w_DATREG))))
        case this.w_TIPPER ="ME"
          this.w_CODPER = ALLTRIM(STR(YEAR(this.w_DATREG)))+right(left(dtoc(this.w_DATREG),5),2)
          this.w_DESPER = right(left(dtoc(this.w_DATREG),5),2)+"/"+ALLTRIM(STR(YEAR(this.w_DATREG)))
        case this.w_TIPPER ="SE"
          this.w_CODPER = ALLTRIM(STR(YEAR(Tempor.DATREG)))+ALLTRIM(STR(WEEK(Tempor.DATREG,1,2)))
          this.w_DESPER = ah_MsgFormat("dal %1 al %2",DTOC((CP_TODATE(this.w_DATREG))-DOW((CP_TODATE(this.w_DATREG)),2)+1),DTOC((CP_TODATE(this.w_DATREG))-DOW((CP_TODATE(this.w_DATREG)),2)+7))
        otherwise
          this.w_CODPER = ALLTRIM(STR(YEAR(this.w_DATREG)))+right(left(dtoc(this.w_DATREG),5),2) +left(dtoc(this.w_DATREG),2)
          this.w_DESPER = left(dtoc(this.w_DATREG),2)+"/"+right(left(dtoc(this.w_DATREG),5),2)+"/" + ALLTRIM(STR(YEAR(this.w_DATREG)))
      endcase
    endif
    if this.w_FLCLIM=.T.
      do case
        case this.w_NCLAIMP<=this.w_CLAIMA
          this.w_CLAIMP = IIF(this.w_CLAIMA=999999999999999,ah_MsgFormat("Oltre"),TRAN(this.w_CLAIMA,v_PV[20]))
        case this.w_NCLAIMP<=this.w_CLAIMB
          this.w_CLAIMP = IIF(this.w_CLAIMB=999999999999999,ah_MsgFormat("Oltre"),TRAN(this.w_CLAIMB,v_PV[20]))
        case this.w_NCLAIMP<=this.w_CLAIMC
          this.w_CLAIMP = IIF(this.w_CLAIMC=999999999999999,ah_MsgFormat("Oltre"),TRAN(this.w_CLAIMC,v_PV[20]))
        case this.w_NCLAIMP<=this.w_CLAIMD
          this.w_CLAIMP = IIF(this.w_CLAIMD=999999999999999,ah_MsgFormat("Oltre"),TRAN(this.w_CLAIMD,v_PV[20]))
        case this.w_NCLAIMP<=this.w_CLAIME
          this.w_CLAIMP = IIF(this.w_CLAIME=999999999999999,ah_MsgFormat("Oltre"),TRAN(this.w_CLAIME,v_PV[20]))
      endcase
    endif
    if this.w_FLCLPR=.T.
      do case
        case this.w_NCLAPRI<=this.w_CLAPRA
          this.w_CLAPRI = IIF(this.w_CLAPRA=999999,ah_MsgFormat("Oltre"),STR(this.w_CLAPRA,6,0))
        case this.w_NCLAPRI<=this.w_CLAPRB
          this.w_CLAPRI = IIF(this.w_CLAPRB=999999,ah_MsgFormat("Oltre"),STR(this.w_CLAPRB,6,0))
        case this.w_NCLAPRI<=this.w_CLAPRC
          this.w_CLAPRI = IIF(this.w_CLAPRC=999999,ah_MsgFormat("Oltre"),STR(this.w_CLAPRC,6,0))
        case this.w_NCLAPRI<=this.w_CLAPRD
          this.w_CLAPRI = IIF(this.w_CLAPRD=999999,ah_MsgFormat("Oltre"),STR(this.w_CLAPRD,6,0))
        case this.w_NCLAPRI<=this.w_CLAPRE
          this.w_CLAPRI = IIF(this.w_CLAPRE=999999,ah_MsgFormat("Oltre"),STR(this.w_CLAPRE,6,0))
      endcase
    endif
    this.w_SEQREC = this.w_SEQREC + 1
    if MOD(this.w_SEQREC, 50)=0
      ah_Msg("1^ scansione record %1 su %2",.T.,.F.,.F.,ALLTR(STR(this.w_SEQREC)),ALLTR(STR(this.w_TOTREC)))
    endif
    this.w_TROV = .F.
    * --- Seleziona i record in base ai Raggruppamenti, per evitare record ripetuti
    SELECT TmpElab
    GO TOP
    do case
      case this.w_TIPRAG=0
        this.w_TROV = SEEK(this.w_SEROFF)
      case this.w_TIPRAG=1
        this.w_TROV = SEEK(this.w_TIPATT+this.w_SEROFF)
      case this.w_TIPRAG=2
        this.w_TROV = SEEK(this.w_CODATT+this.w_SEROFF)
      otherwise
        this.w_TROV = SEEK(this.w_TIPATT+this.w_CODATT+this.w_SEROFF)
    endcase
    if this.w_TROV=.F.
      INSERT INTO TmpElab ;
      (TIPATT, CODATT, CODPER, DESPER, CLAIMP, CLAPRI, ;
      STATUS, CODOPE, CODORI, GRUNOM, CODZON, CODAGE, CODAG2, CODNOM, SEROFF, ;
      TOTIMP, TOTOFF, TOTIMPC, TOTOFFC, DURTRA, NUMREV) ;
      VALUES ;
      (this.w_TIPATT, this.w_CODATT, this.w_CODPER, this.w_DESPER, this.w_CLAIMP, this.w_CLAPRI, ;
      this.w_STATUS, this.w_CODOPE, this.w_CODORI, this.w_GRUNOM, this.w_CODZON, this.w_CODAGE, this.w_CODAG2, this.w_CODNOM, this.w_SEROFF, ;
      this.w_TOTIMP, this.w_TOTOFF, this.w_TOTIMPC, this.w_TOTOFFC, this.w_DURTRA, this.w_NUMREV)
    endif
    SELECT TEMPOR
    ENDSCAN
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Preparazione dati per il report
    L_CECODICE=this.oParentObject.w_CECODICE
    L_CEDESCRI=this.oParentObject.w_CEDESCRI
    L_DTOINI=DTOC(this.oParentObject.w_DTOINI)
    L_DTOFIN=DTOC(this.oParentObject.w_DTOFIN)
    L_DTIINI=DTOC(this.oParentObject.w_DTIINI)
    L_DTIFIN=DTOC(this.oParentObject.w_DTIFIN)
    L_TIPNOM=this.oParentObject.w_TIPNOM
    L_STAOFF=this.oParentObject.w_STAOFF
    L_OPEFIL=ALLTR(STR(this.oParentObject.w_OPEFIL))
    L_DESOPE=this.oParentObject.w_DESOPE
    L_ORINOM=this.oParentObject.w_ORINOM
    L_ORIDES=this.oParentObject.w_ORIDES
    L_GRUFIL=this.oParentObject.w_GRUFIL
    L_GRUDES=this.oParentObject.w_GRUDES
    L_CODPRN=this.oParentObject.w_CODPRN
    L_DESPRN=this.oParentObject.w_DESPRN
    L_NOMFIL=this.oParentObject.w_NOMFIL
    L_NU1PRN=ALLTR(STR(this.oParentObject.w_NU1PRN))
    L_NU2PRN=ALLTR(STR(this.oParentObject.w_NU2PRN))
    L_DTAINI=DTOC(this.oParentObject.w_DTAINI)
    L_DTAFIN=DTOC(this.oParentObject.w_DTAFIN)
    L_AGEFIL=this.oParentObject.w_AGEFIL
    L_DESAGE=this.oParentObject.w_DESAGE
    L_ZONFIL=this.oParentObject.w_ZONFIL
    L_DESZON=this.oParentObject.w_DESZON
    L_TIPVAL=this.oParentObject.w_TIPVAL
    L_CODVAL=this.oParentObject.w_CODVAL
    L_SIMVAL=this.oParentObject.w_SIMVAL
    L_CAMVAL=this.oParentObject.w_CAMVAL
    L_DECIMI=this.oParentObject.w_DECTOT
    L_FORMAT=v_PV[20]
    L_CODPRO=this.oParentObject.w_CODPRO
    L_DESPRO=this.oParentObject.w_DESPRO
    L_NU1PRO=this.oParentObject.w_NU1PRO
    L_NU2PRO=this.oParentObject.w_NU2PRO
    L_IMPINI=ALLTRIM(TRAN(this.oParentObject.w_IMPINI, v_PV[20]))
    L_IMPFIN=ALLTRIM(TRAN(this.oParentObject.w_IMPFIN, v_PV[20]))
    L_CODART=this.oParentObject.w_CODART
    L_DESART=this.oParentObject.w_DESART
    L_CODGRU=this.oParentObject.w_CODGRU
    L_DESGRU=this.oParentObject.w_DESGRU
    L_CODSOT=this.oParentObject.w_CODSOT
    L_DESSOT=this.oParentObject.w_DESSOT
    L_CODMOD=this.oParentObject.w_CODMOD
    L_DESMOD=this.oParentObject.w_DESMOD
    L_DTSINI=DTOC(this.oParentObject.w_DTSINI)
    L_DTSFIN=DTOC(this.oParentObject.w_DTSFIN)
    L_DTCINI=DTOC(this.oParentObject.w_DTCINI)
    L_DTCFIN=DTOC(this.oParentObject.w_DTCFIN)
    L_NUMINI=ALLTR(STR(this.oParentObject.w_NUMINI))
    L_NUMFIN=ALLTR(STR(this.oParentObject.w_NUMFIN))
    L_ALFINI=this.oParentObject.w_ALFINI
    L_ALFFIN=this.oParentObject.w_ALFFIN
    L_TIPFIL=this.oParentObject.w_TIPFIL
    L_CODAT1=this.oParentObject.w_CODAT1
    L_CODAT2=this.oParentObject.w_CODAT2
    L_CODAT3=this.oParentObject.w_CODAT3
    L_CODAT4=this.oParentObject.w_CODAT4
    L_CODAT5=this.oParentObject.w_CODAT5
    L_CODAT6=this.oParentObject.w_CODAT6
    L_CODAT7=this.oParentObject.w_CODAT7
    L_CODAT8=this.oParentObject.w_CODAT8
    L_TIPAT1=this.oParentObject.w_TIPAT1
    L_TIPAT2=this.oParentObject.w_TIPAT2
    L_TIPAT3=this.oParentObject.w_TIPAT3
    L_TIPAT4=this.oParentObject.w_TIPAT4
    L_TIPAT5=this.oParentObject.w_TIPAT5
    L_TIPAT6=this.oParentObject.w_TIPAT6
    L_TIPAT7=this.oParentObject.w_TIPAT7
    L_TIPAT8=this.oParentObject.w_TIPAT8
    L_DES1=this.oParentObject.w_DES1
    L_DES2=this.oParentObject.w_DES2
    L_DES3=this.oParentObject.w_DES3
    L_DES4=this.oParentObject.w_DES4
    L_DES5=this.oParentObject.w_DES5
    L_DES6=this.oParentObject.w_DES6
    L_DES7=this.oParentObject.w_DES7
    L_DES8=this.oParentObject.w_DES8
    L_CODSE1=this.oParentObject.w_CODSE1
    L_CODSE2=this.oParentObject.w_CODSE2
    L_CODSE3=this.oParentObject.w_CODSE3
    L_DESSE1=this.oParentObject.w_DESSE1
    L_DESSE2=this.oParentObject.w_DESSE2
    L_DESSE3=this.oParentObject.w_DESSE3
    do case
      case this.pParam="REPO"
        this.w_FILE = this.oParentObject.w_REPORT
      case this.pParam="EXCE"
        this.w_FILE = this.oParentObject.w_EXCEL
    endcase
    if NOT EMPTY(this.w_FILE)
      CP_CHPRN(ALLTRIM(this.w_FILE), " ", this)
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CRIDOFFE'
    this.cWorkTables[2]='*TMPATTNO'
    this.cWorkTables[3]='*TMPATTOF'
    this.cWorkTables[4]='*TMPSEZOF'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CRIDOFFE')
      use in _Curs_CRIDOFFE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
