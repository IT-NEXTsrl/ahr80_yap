* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bgd                                                        *
*              Generazione documento da offerta                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_239]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-22                                                      *
* Last revis.: 2016-06-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bgd",oParentObject)
return(i_retval)

define class tgsof_bgd as StdBatch
  * --- Local variables
  w_LCODMAG = space(5)
  w_FLFOAG = space(1)
  w_FLFOA2 = space(1)
  w_FLFOPO = space(1)
  w_FLFOSP = space(1)
  w_ANFLINCA = space(1)
  w_DATOAI = ctod("  /  /  ")
  w_oMess = .NULL.
  w_oPart = .NULL.
  DR = space(10)
  w_LOOP = 0
  w_COPERTRA = 0
  w_COPERIMB = 0
  w_COPERINC = 0
  w_OLDMFLORDI = space(1)
  w_OLDMFLIMPE = space(1)
  w_OLDMFLCASC = space(1)
  w_OLDMFLRISE = space(1)
  w_MODRIF = space(5)
  w_CODLIN = space(3)
  w_DESSUP = space(0)
  w_VALINT = space(3)
  w_CAMINT = 0
  w_TIPOPER = space(1)
  w_IMPDIC = 0
  w_IMPUTI = 0
  w_TmpC = space(10)
  w_TmpN = 0
  w_DIDATINI = ctod("  /  /  ")
  w_TmpD = ctod("  /  /  ")
  w_CODCONDIC = space(15)
  w_DTOBSO = ctod("  /  /  ")
  w_MVFLORCO = space(1)
  w_OFSERIAL = space(10)
  w_CPROWORD = 0
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVCLADOC = space(2)
  w_MVCONCON = space(1)
  w_MVCONTRA = space(15)
  w_MVSERRIF = space(10)
  w_MV__NOTE = space(10)
  w_MVIVAINC = space(5)
  w_MVSPEINC = 0
  w_MVNUMRIF = 0
  w_MVCONIND = space(15)
  w_MVROWRIF = 0
  w_MVFLRINC = space(1)
  w_MVFLSALD = space(1)
  w_CPROWNUM = 0
  w_MVTIPRIG = space(1)
  w_MVCODLIS = space(5)
  w_MVSPEIMB = 0
  w_MVFLORDI = space(1)
  w_MVCODICE = space(20)
  w_MVQTAMOV = 0
  w_MVTINCOM = ctod("  /  /  ")
  w_MVIVAIMB = space(5)
  w_MVFLIMPE = space(1)
  w_MVCODART = space(20)
  w_MVQTAUM1 = 0
  w_MVACCONT = 0
  w_MVFLRIMB = space(1)
  w_MVPREZZO = 0
  w_MVSCOCL1 = 0
  w_MVSPETRA = 0
  w_MVACCSUC = 0
  w_MVVOCCEN = space(15)
  w_MVDESART = space(40)
  w_MVSCONT1 = 0
  w_MVSCOCL2 = 0
  w_MVIVATRA = space(5)
  w_MVCODCEN = space(15)
  w_MVDESSUP = space(10)
  w_MVSCONT2 = 0
  w_MVSCOPAG = 0
  w_MVFLRTRA = space(1)
  w_MVCODCOM = space(15)
  w_MVUNIMIS = space(3)
  w_MVSCONT3 = 0
  w_MVCAUMAG = space(5)
  w_MVSPEBOL = 0
  w_MVCATCON = space(5)
  w_MVSCONT4 = 0
  w_MVMOLSUP = 0
  w_MVIVABOL = space(5)
  w_MVFLSCOR = space(1)
  w_MVCODCLA = space(3)
  w_MVFLOMAG = space(1)
  w_MVSCONTI = 0
  w_MVCODCON = space(15)
  w_MVCODIVA = space(5)
  w_VALUNI = 0
  w_MVCODIVE = space(5)
  w_MVCODPAG = space(5)
  w_MVKEYSAL = space(20)
  w_MVVALNAZ = space(3)
  w_MFLAVAL = space(1)
  w_MVCODAGE = space(5)
  w_MVCODAG2 = space(5)
  w_MVCODBAN = space(10)
  w_MVNUMCOR = space(25)
  w_MVCODBA2 = space(10)
  w_MVCODPOR = space(1)
  w_MVCAOVAL = 0
  w_MVTCONTR = space(15)
  w_MVCODVAL = space(3)
  w_MVCODVET = space(5)
  w_MVVALRIG = 0
  w_MVFLELGM = space(1)
  w_MVPESNET = 0
  w_MVCODSPE = space(3)
  w_MVIMPSCO = 0
  w_MVRIFDIC = space(10)
  w_MVFLTRAS = space(1)
  w_MVTIPATT = space(1)
  w_MVVALMAG = 0
  w_MVPESNET = 0
  w_MVTCOLIS = space(5)
  w_MVNOMENC = space(8)
  w_MVCODATT = space(15)
  w_MVIMPNAZ = 0
  w_MVACIVA1 = space(5)
  w_MVAIMPN1 = 0
  w_MVAFLOM1 = space(1)
  w_MVAIMPS1 = 0
  w_MVUMSUPP = space(3)
  w_MVACIVA2 = space(5)
  w_MVAIMPN2 = 0
  w_MVAFLOM2 = space(1)
  w_MVAIMPS2 = 0
  w_MVFLVEAC = space(1)
  w_MVACIVA3 = space(5)
  w_MVAIMPN3 = 0
  w_MVAFLOM3 = space(1)
  w_MVAIMPS3 = 0
  w_MVCODDES = space(5)
  w_MVACIVA4 = space(5)
  w_MVAIMPN4 = 0
  w_CAOCON = 0
  w_VALCON = space(3)
  w_MVAFLOM4 = space(1)
  w_MVAIMPS4 = 0
  w_MVIMPARR = 0
  w_MVACIVA5 = space(5)
  w_MVAIMPN5 = 0
  w_MVAFLOM5 = space(1)
  w_MVAIMPS5 = 0
  w_MVACIVA6 = space(5)
  w_MVAIMPN6 = 0
  w_MVAFLOM6 = space(1)
  w_MVAIMPS6 = 0
  w_MVACCPRE = 0
  w_MVTOTRIT = 0
  w_MVTOTENA = 0
  w_MVIMPACC = 0
  w_MVVOCCOS = space(15)
  w_MVRITPRE = 0
  w_MVFLFOSC = space(1)
  w_CAOVAL = 0
  w_BOLARR = 0
  w_PERIVA = 0
  w_PEIINC = 0
  w_MFLCASC = space(1)
  w_IMPARR = 0
  w_BOLMIN = 0
  w_BOLIVA = space(1)
  w_BOLINC = space(1)
  w_MFLORDI = space(1)
  w_RSIMPRAT = 0
  w_TOTMERCE = 0
  w_MESE1 = 0
  w_PEIIMB = 0
  w_MFLIMPE = space(1)
  w_DECTOT = 0
  w_TOTALE = 0
  w_MESE2 = 0
  w_BOLIMB = space(1)
  w_MFLRISE = space(1)
  w_BOLESE = 0
  w_TOTIMPON = 0
  w_GIORN1 = 0
  w_FLINCA = space(1)
  w_PEITRA = 0
  w_MFLELGM = space(1)
  w_BOLSUP = 0
  w_TOTIMPOS = 0
  w_GIORN2 = 0
  w_BOLTRA = space(1)
  w_MFLCOMM = space(1)
  w_BOLCAM = 0
  w_TOTFATTU = 0
  w_GIOFIS = 0
  w_BOLBOL = space(1)
  w_PERIVE = 0
  w_CAONAZ = 0
  w_CLBOLFAT = space(1)
  w_BOLIVE = space(1)
  w_RSMODPAG = space(10)
  w_RSNUMRAT = 0
  w_RSDATRAT = ctod("  /  /  ")
  w_CODESE = space(4)
  w_ACCPRE = 0
  w_ORDNUM = 0
  w_CODNAZ = space(3)
  w_DETNUM = 0
  w_FLFOBO = space(1)
  w_UNMIS1 = space(3)
  w_VALUNI = 0
  w_IVALIS = space(1)
  w_UNMIS2 = space(3)
  w_QUALIS = space(1)
  w_UNMIS3 = space(3)
  w_DECUNI = 0
  w_MOLTIP = 0
  w_DATREG = ctod("  /  /  ")
  w_GRUMER = space(5)
  w_QUAN = 0
  w_MOLTI3 = 0
  w_CODVAL = space(3)
  w_QTAUM1 = 0
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_ARRSUP = 0
  w_ACQINT = space(1)
  w_OK_ANALI = .f.
  w_OK_COMM = .f.
  w_SCOLIS = space(1)
  w_FLSERA = space(1)
  w_INDIVE = 0
  w_INDIVA = 0
  w_APPART = space(10)
  w_OFNUMDOC = 0
  w_OFSERDOC = space(10)
  w_OFDATDOC = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_MVFLVABD = space(1)
  w_OK_LET = .f.
  w_MVTIPOPE = space(10)
  w_MVFLFOBO = space(1)
  w_MVTDTEVA = ctod("  /  /  ")
  w_MVBENDEP = space(1)
  w_MFLELAN = space(1)
  w_MSEGNO = space(1)
  w_MVTCOMAG = space(5)
  w_CAUNULLA = space(1)
  w_MVIMPCOM = 0
  w_MVTIPPRO = space(2)
  w_MVPERPRO = 0
  w_MVTIPPR2 = space(2)
  w_MVPROCAP = 0
  w_CONCOR = space(25)
  w_ARDTOBSO = ctod("  /  /  ")
  w_ESISTEARTICOLOOBSOLETO = .f.
  w_ESISTEARTICOLONONOBSOLETO = .f.
  w_oERRORLOG = .NULL.
  w_MVFLCASC = space(1)
  w_MVFLRISE = space(1)
  w_MV_FLAGG = space(1)
  w_ARUTISER = space(1)
  w_ARDATINT = space(1)
  w_ODDATPCO = ctod("  /  /  ")
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  w_MVFLSCOM = space(1)
  w_SPEACC = 0
  w_SPEACC_IVA = 0
  w_DATCOM = ctod("  /  /  ")
  w_ANFLAPCA = space(1)
  w_TDFLAPCA = space(1)
  w_ARFLAPCA = space(1)
  w_FLLOTT = space(1)
  w_MVRIFCAC = 0
  w_MVCACONT = space(5)
  w_SEARCHROW = 0
  w_ACTPOS = 0
  w_MVFLLOTT = space(1)
  w_FLUBIC = space(1)
  w_PPCALPRO = space(2)
  w_OLDCALPRO = space(2)
  w_PPCALSCO = space(1)
  w_PPLISRIF = space(5)
  w_MASSGEN = space(1)
  w_SERPRO = space(10)
  w_TOTDOC = 0
  w_MINRATA = 0
  w_MSGRET = space(254)
  w_TIMPSCO = 0
  w_PRES = 0
  w_SPEBOL = 0
  w_TOTDOC = 0
  w_OK = .f.
  w_FIDRES = 0
  w_FID1 = 0
  w_FID2 = 0
  w_FID3 = 0
  w_FID4 = 0
  w_FID5 = 0
  w_FID6 = 0
  w_FIDD = ctod("  /  /  ")
  w_ANFIDO = .f.
  w_MESS = space(10)
  w_COFRAZ = space(1)
  w_UNIMIS = space(3)
  w_KCODICE = space(20)
  w_KUNIMIS = space(3)
  w_KQTAMOV = 0
  w_KPREZZO = 0
  w_KCODIVA = space(3)
  w_CONTRIB = space(5)
  w_RIFCACESP = 0
  w_CATDOC = space(2)
  w_POSAPP = 0
  * --- WorkFile variables
  AGENTI_idx=0
  ART_ICOL_idx=0
  AZIENDA_idx=0
  BAN_CONTI_idx=0
  CAM_AGAZ_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  KEY_ARTI_idx=0
  LISTINI_idx=0
  MAGAZZIN_idx=0
  OFF_ERTE_idx=0
  PAR_PROV_idx=0
  SALDIART_idx=0
  SIT_FIDI_idx=0
  TIP_DOCU_idx=0
  UNIMIS_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Documento da Offerta (da GSOF_BDO)
    * --- La lettura dei campi relativi alla causale documento, viene fatta nella maschera GSOF_KDO 
    *     lanciata dal batch GSOF_BDO (Padre)
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.w_OFSERIAL = this.oParentObject.w_OFSERIAL
    this.w_OFNUMDOC = this.oParentObject.w_OFNUMDOC
    this.w_OFSERDOC = this.oParentObject.w_OFSERDOC
    this.w_OFDATDOC = this.oParentObject.w_OFDATDOC
    * --- Attenzione: queste w_MV... vanno lasciate Locali (lette in GSAR_BFA)
    this.w_MVDATDOC = this.oParentObject.w_MVDATDOC
    this.w_MVCLADOC = this.oParentObject.w_MVCLADOC
    this.w_MVDATDIV = this.oParentObject.w_MVDATDIV
    this.w_MVVALNAZ = g_PERVAL
    this.w_MVFLVEAC = "V"
    this.w_CODAZI = i_CODAZI
    this.w_MVFLVABD = " "
    this.w_MVBENDEP = " "
    * --- Lettura Falg Bene Deperibile
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZFLVEBD"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZFLVEBD;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVFLVABD = NVL(cp_ToDate(_read_.AZFLVEBD),cp_NullValue(_read_.AZFLVEBD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CAONAZ = GETCAM(this.w_MVVALNAZ, this.w_MVDATDOC, 0)
    this.w_CODESE = this.oParentObject.w_MVCODESE
    this.w_FLFOBO = " "
    this.w_FLINCA = " "
    this.w_ACQINT = "N"
    * --- Rate Scadenze
    DIMENSION DR[1000, 9]
    this.w_LOOP = 1
    do while this.w_LOOP <= 1000
      DR[ this.w_LOOP , 1] = cp_CharToDate("  -  -  ")
      DR[ this.w_LOOP , 2] = 0
      DR[ this.w_LOOP , 3] = "  "
      DR[ this.w_LOOP , 4] = "  "
      DR[ this.w_LOOP , 5] = "  "
      DR[ this.w_LOOP , 6] = "  "
      DR[ this.w_LOOP , 7] = " "
      DR[ this.w_LOOP , 8] = " "
      DR[ this.w_LOOP , 9] = " "
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Legge Informazioni di Riga di Default
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_MVCAUMAG = this.oParentObject.w_MVTCAMAG
    this.w_MFLCASC = " "
    this.w_MFLORDI = " "
    this.w_MFLIMPE = " "
    this.w_MFLRISE = " "
    this.w_MFLELGM = " "
    this.w_MFLAVAL = " "
    this.w_MFLELAN = " "
    this.w_MSEGNO = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVTCAMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM;
        from (i_cTable) where;
            CMCODICE = this.oParentObject.w_MVTCAMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      this.w_MFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
      this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
      this.w_MFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Codici IVA di testata: Trasporto, Imballo, Incasso e Bolli
    if NOT EMPTY(g_COITRA)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(g_COITRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = g_COITRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COPERTRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_COPERTRA = 0
    endif
    if NOT EMPTY(g_COIIMB)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(g_COIIMB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = g_COIIMB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COPERIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_COPERIMB = 0
    endif
    if NOT EMPTY(g_COIINC)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(g_COIINC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = g_COIINC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COPERINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_COPERINC = 0
    endif
    this.w_OLDMFLORDI = this.w_MFLORDI
    this.w_OLDMFLIMPE = this.w_MFLIMPE
    * --- Valorizzo il flag dell'impegnato in base allo stato del documento da generare
    if this.oParentObject.w_MVFLPROV = "S"
      this.w_MFLIMPE = " "
    endif
    this.w_OLDMFLCASC = this.w_MFLCASC
    this.w_OLDMFLRISE = this.w_MFLRISE
    this.w_MFLCASC = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLCASC)
    this.w_MFLORDI = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLORDI)
    this.w_MFLRISE = IIF(this.oParentObject.w_MVFLPROV="S"," ",this.w_MFLRISE)
    this.w_OK = .T.
    * --- Carica i Documenti da Generare
    * --- Prepara il Temporaneo di Elaborazione
    VQ_EXEC("..\OFFE\EXE\QUERY\GSOF_QGD.VQR",this,"GENEORDI")
    if USED("GENEORDI")
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_OK
        if USED("GeneApp")
          * --- Inizio Aggiornamento
          if RECCOUNT("GeneApp") > 0
            * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
            this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
            * --- Inizio Aggiornamento
            ah_Msg("Inizio fase di generazione...",.T.)
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_OK
              ah_ErrorMsg("Operazione completata",,"")
              this.oParentObject.oParentObject.mHideControls()
            else
              if ! EMPTY( this.w_MESS )
                ah_ErrorMsg("%1",,"", this.w_MESS)
              endif
            endif
          else
            ah_ErrorMsg("Non esistono dati da generare",,"")
          endif
        endif
      endif
    endif
    * --- Chiude i Cursori
    USE IN SELECT("GENEORDI")
    USE IN SELECT("GeneApp")
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza il Temporaneo di Appoggio
    CREATE CURSOR GeneApp ;
    (t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ;
    t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ;
    t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), ;
    t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), ;
    t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), ;
    t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), ;
    t_MVPESNET N(9,3), t_MVNOMENC C(8), t_MVUMSUPP C(3), t_MVMOLSUP N(8,3), ;
    t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ;
    t_MVCODMAG C(5), t_MVCODCEN C(15), t_MVVOCCOS C(15), ;
    t_MVCODCOM C(15), t_MVTIPATT C(1), t_MVCODATT C(15), t_MVDATEVA D(8), t_INDIVA N(3,0), ;
    t_FLSERA C(1), t_MVIMPCOM N(18,4), t_MVIMPAC2 N(18,4), t_CPROWORD N(5,0) ,;
    t_MVTIPPRO C(2), t_MVPERPRO N(5,2), t_MVTIPPR2 C(2), t_MVPROCAP N(5,2), t_ARFLAPCA C(1) , t_CPROWNUM N(4) , t_MVCACONT C(5), t_MVFLORCO C(1) )
    if NOT EMPTY(g_ARTDES)
      this.w_MVCODART = SPACE(20)
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(g_ARTDES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART;
          from (i_cTable) where;
              CACODICE = g_ARTDES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows>0
        * --- Potrebbe essere presente l'articolo per riferimenti anche se non esiste nell'anagrafica articoli.
        *     Pu� essere causato da una creazione azienda
        *     In questo caso non devo inserire le righe descrittive poich� darebbe errore di inserzione
        SELECT GENEORDI
        GO TOP
        this.w_MVCODCON = ALLTRIM(NVL(ANCODICE,SPACE(15)))
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODLIN,ANFLAPCA"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC("C");
                +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODLIN,ANFLAPCA;
            from (i_cTable) where;
                ANTIPCON = "C";
                and ANCODICE = this.w_MVCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
          this.w_ANFLAPCA = NVL(cp_ToDate(_read_.ANFLAPCA),cp_NullValue(_read_.ANFLAPCA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDMODRIF,TDFLAPCA,TDCATDOC"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDMODRIF,TDFLAPCA,TDCATDOC;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MODRIF = NVL(cp_ToDate(_read_.TDMODRIF),cp_NullValue(_read_.TDMODRIF))
          this.w_TDFLAPCA = NVL(cp_ToDate(_read_.TDFLAPCA),cp_NullValue(_read_.TDFLAPCA))
          this.w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Scrive nuova Riga Descrittiva sul Temporaneo di Appoggio
         
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_OFNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=Alltrim(this.w_OFSERDOC) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(CP_TODATE(this.w_OFDATDOC)) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATPRO" 
 ARPARAM[5,2]="" 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=this.w_MVCODCON 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=NVL(OFCODAGE, SPACE(5)) 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= "" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=""
         
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]=ALLTRIM(STR(this.w_OFNUMDOC,15))+IIF(EMPTY(this.w_OFSERDOC),"", "/"+Alltrim(this.w_OFSERDOC)) 
 ARPARAM2[2]= DTOC(CP_TODATE(this.w_OFDATDOC))
        this.w_APPART = CALRIFDES("dso",this.w_CODLIN, this.w_MODRIF, @ARPARAM, "", @ARPARAM2) 
        this.w_MVTIPRIG = "D"
        this.w_MVFLOMAG = "X"
        this.w_MVCODICE = g_ARTDES
        this.w_DESSUP = ""
        if len(this.w_APPART) >40
          * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
          *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
          *     dello spazio nei primi 30 caratteri.
          *     Il resto viene inserito nella descrizione supplementare.
          this.w_DESSUP = SUBSTR(this.w_APPART,(RATC(" ",LEFT(this.w_APPART,30))+1))
          this.w_APPART = LEFT(this.w_APPART, RATC(" ", LEFT(this.w_APPART,30)))
        endif
        this.w_MVDESART = this.w_APPART
        this.w_MVDESSUP = this.w_DESSUP
        * --- Inserimento della riga descrittiva del riferimento all'offerta
        INSERT INTO GeneApp (t_MVTIPRIG, t_MVCODICE, t_MVCODART, t_MVDESART, t_MVDESSUP, t_MVDATEVA, t_MVFLOMAG) ;
        VALUES (this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, this.w_MVDESART, this.w_MVDESSUP, this.oParentObject.w_MVDATEVA, this.w_MVFLOMAG)
      endif
    endif
    * --- Genera i Documenti di Ordine 
    SELECT GENEORDI
    GO TOP
    * --- Inizializza i dati di Testata del Nuovo Documento
    this.w_MVCODCON = ANCODICE
    this.w_MVCODIVE = NVL(CODIVE, SPACE(5))
    this.w_MVCODPAG = NVL(OFCODPAG, SPACE(5))
    this.w_MVCODBAN = NVL(CODBAN, SPACE(10))
    this.w_CONCOR = NVL(CODCONCO,SPACE(25))
    * --- Ricerca del conto corrente dell'intestatario documento
    * --- Verifico se � presente un conto corrente di 'default' per il cliente e la banca specificiati
    * --- Read from BAN_CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCCONCOR"+;
        " from "+i_cTable+" BAN_CONTI where ";
            +"CCTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
            +" and CCCODCON = "+cp_ToStrODBC(this.w_MVCODCON);
            +" and CCCODBAN = "+cp_ToStrODBC(this.w_MVCODBAN);
            +" and CCCONCOR = "+cp_ToStrODBC(this.w_CONCOR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCCONCOR;
        from (i_cTable) where;
            CCTIPCON = this.oParentObject.w_MVTIPCON;
            and CCCODCON = this.w_MVCODCON;
            and CCCODBAN = this.w_MVCODBAN;
            and CCCONCOR = this.w_CONCOR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVNUMCOR = NVL(cp_ToDate(_read_.CCCONCOR),cp_NullValue(_read_.CCCONCOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_MVNUMCOR)
      * --- Non esiste un conto corrente di default, quindi prendo il primo associato al cliente ed alla banca specificati
      * --- Read from BAN_CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCCONCOR"+;
          " from "+i_cTable+" BAN_CONTI where ";
              +"CCTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
              +" and CCCODCON = "+cp_ToStrODBC(this.w_MVCODCON);
              +" and CCCODBAN = "+cp_ToStrODBC(this.w_MVCODBAN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCCONCOR;
          from (i_cTable) where;
              CCTIPCON = this.oParentObject.w_MVTIPCON;
              and CCCODCON = this.w_MVCODCON;
              and CCCODBAN = this.w_MVCODBAN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVNUMCOR = NVL(cp_ToDate(_read_.CCCONCOR),cp_NullValue(_read_.CCCONCOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    SELECT GENEORDI
    this.w_MVCODBA2 = ANCODBA2
    this.w_MVCONCON = NVL(ANCONCON, " ")
    this.w_MVCODVAL = NVL(OFCODVAL, SPACE(5))
    this.w_MVSCOCL1 = NVL(OFSCOCL1,0)
    this.w_MVSCOCL2 = NVL(OFSCOCL2,0)
    this.w_MVSCOPAG = NVL(OFSCOPAG,0)
    this.w_MVCODAGE = NVL(OFCODAGE, SPACE(5))
    this.w_MVCODAG2 = NVL(OFCODAG2, SPACE(5))
    this.w_MVCODPOR = NVL(OFCODPOR, " ")
    this.w_MVCODSPE = NVL(OFCODSPE, SPACE(3))
    this.w_FLFOAG = NVL(OFFLFOAG, "N")
    this.w_FLFOA2 = NVL(OFFLFOA2, "N")
    this.w_FLFOPO = NVL(OFFLFOPO, "N")
    this.w_FLFOSP = NVL(OFFLFOSP, "N")
    this.w_MVSPEINC = NVL(OFSPEINC, 0)
    this.w_ANFLINCA = NVL(ANFLINCA," ")
    this.w_MVSPEIMB = NVL(OFSPEIMB, 0)
    this.w_MVSPETRA = NVL(OFSPETRA, 0)
    this.w_MVSPEBOL = 0
    this.w_MVIMPARR = 0
    this.w_MVACCPRE = 0
    this.w_MVACCSUC = 0
    this.w_MVFLSCOR = NVL(FLSCOR, "N")
    this.w_MVFLFOSC = NVL(OFFLFOSC, " ")
    this.w_MVSCONTI = NVL(OFSCONTI, 0)
    this.w_GIORN1 = NVL(ANGIOSC1, 0)
    this.w_GIORN2 = NVL(ANGIOSC2, 0)
    this.w_MESE1 = NVL(AN1MESCL, 0)
    this.w_MESE2 = NVL(AN2MESCL, 0)
    this.w_GIOFIS = NVL(ANGIOFIS, 0)
    this.w_CLBOLFAT = NVL(ANBOLFAT, " ")
    this.w_CODNAZ = NVL(ANNAZION, g_CODNAZ)
    this.w_MVTCOLIS = NVL(OFCODLIS, SPACE(5))
    this.w_MVTCONTR = SPACE(15)
    this.w_MV__NOTE = NVL(OFRIFDES, " ")
    this.w_MVCODVAL = EVL(this.w_MVCODVAL, this.w_MVVALNAZ )
    this.w_MVCAOVAL = this.w_CAONAZ
    this.oParentObject.w_MVDATCIV = this.w_MVDATDOC
    this.oParentObject.w_MVDATREG = this.w_MVDATDOC
    if this.w_MVCODVAL<>this.w_MVVALNAZ
      this.w_MVCAOVAL = GETCAM(this.w_MVCODVAL, this.w_MVDATDOC, 0)
    endif
    this.w_MVCAOVAL = IIF(this.w_MVCAOVAL=0, GETCAM(G_PERVAL, I_DATSYS), this.w_MVCAOVAL)
    this.w_CAOCON = this.w_MVCAOVAL
    this.w_VALCON = this.w_MVCODVAL
    * --- Legge Dati Associati alla Valuta
    this.w_DECTOT = 0
    this.w_DECUNI = 0
    this.w_BOLESE = 0
    this.w_BOLSUP = 0
    this.w_BOLCAM = 0
    this.w_BOLARR = 0
    this.w_BOLMIN = 0
    this.w_MVRIFDIC = Space(10)
    * --- Calcolo la lettera di intento solo se non ho un codice Iva esenzione particolare sul Cliente
    *     In questo caso mi comporto come nel caricamento manuale Ordini dove il codice Iva sul Cliente
    *     vince sulla lettera di intento.
    if Empty(this.w_MVCODIVE)
      * --- Lettura lettera di intento valida
      DECLARE ARRDIC (14,1)
      * --- Azzero l'Array che verr� riempito dalla Funzione
      ARRDIC(1)=0
      this.w_OK_LET = CAL_LETT(this.w_MVDATDOC,this.oParentObject.w_MVTIPCON,this.w_MVCODCON, @ArrDic, SPACE(10), this.w_MVCODDES)
      if this.w_OK_LET
        * --- Parametri
        *     pDatRif : Data di Riferimento per filtro su Lettere di intento
        *     pTipCon : Tipo Conto : 'C' Clienti, 'F' : Fornitori
        *     pCodCon : Codice Cliente/Fornitore
        *     pArrDic : Array passato per riferimento: conterr� anche i dati letti dalla lettera di intento
        *     
        *     pArrDic[ 1 ]   = Numero Dichiarazione
        *     pArrDic[ 2 ]   = Tipo Operazione
        *     pArrDic[ 3 ]   = Anno Dichiarazione
        *     pArrDic[ 4 ]   = Importo Dichiarazione
        *     pArrDic[ 5 ]   = Data Dichiarazione
        *     pArrDic[ 6 ]   = Importo Utilizzato
        *     pArrDic[ 7 ]   = Tipo conto: Cliente/Fornitore
        *     pArrDic[ 8 ]   = Codice Iva Agevolata
        *     pArrDic[ 9 ]   = Tipo Iva (Agevolata o senza)
        *     pArrDic[ 10 ] = Data Inizio Validit�
        *     pArrDic[ 11 ] = Codice Dichiarazione (Se a clienti = codice Cliente, Se Fornitori= codice progressivo)
        *     pArrDic[ 12 ] = Data Obsolescenza
        this.w_MVCODIVE = ArrDic(8)
        this.w_MVRIFDIC = ArrDic(11)
      endif
    endif
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI;
        from (i_cTable) where;
            VACODVAL = this.w_MVCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
      this.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
      this.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
      this.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
      this.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
      this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PERIVE = 0
    this.w_INDIVE = 0
    this.w_BOLIVE = " "
    if NOT EMPTY(this.w_MVCODIVE)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVBOLIVA,IVPERIVA,IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVBOLIVA,IVPERIVA,IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_MVCODIVE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- calcola le Spese di Incasso
    this.w_MVIVAINC = IIF(NOT EMPTY(this.w_MVCODIVE), IIF(this.w_PERIVE<this.w_COPERINC,this.w_MVCODIVE,g_COIINC), g_COIINC)
    this.w_MVIVAIMB = IIF(NOT EMPTY(this.w_MVCODIVE), IIF(this.w_PERIVE<this.w_COPERIMB,this.w_MVCODIVE,g_COIIMB), g_COIIMB)
    this.w_MVIVATRA = IIF(NOT EMPTY(this.w_MVCODIVE),IIF(this.w_PERIVE<this.w_COPERTRA,this.w_MVCODIVE,g_COITRA), g_COITRA)
    this.w_MVIVABOL = g_COIBOL
    this.w_MVCODVET = SPACE(5)
    this.w_MVTIPOPE = SPACE(10)
    * --- Propone dati Accompagnatori (Riferimento: 'CO' = Consegna) Predefinita
    * --- Select from DES_DIVE
    i_nConn=i_TableProp[this.DES_DIVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
          +" where DDTIPCON="+cp_ToStrODBC(this.oParentObject.w_MVTIPCON)+" AND DDCODICE="+cp_ToStrODBC(this.w_MVCODCON)+" AND DDTIPRIF='CO' AND DDPREDEF='S'";
           ,"_Curs_DES_DIVE")
    else
      select * from (i_cTable);
       where DDTIPCON=this.oParentObject.w_MVTIPCON AND DDCODICE=this.w_MVCODCON AND DDTIPRIF="CO" AND DDPREDEF="S";
        into cursor _Curs_DES_DIVE
    endif
    if used('_Curs_DES_DIVE')
      select _Curs_DES_DIVE
      locate for 1=1
      do while not(eof())
      this.w_DTOBSO = Cp_ToDate(_Curs_DES_DIVE.DDDTOBSO)
      if this.w_DTOBSO>this.w_MVDATDOC Or Empty(this.w_DTOBSO)
        this.w_MVCODDES = _Curs_DES_DIVE.DDCODDES
        this.w_MVCODVET = Nvl(_Curs_DES_DIVE.DDCODVET,Space(5))
        this.w_MVCODPOR = IIF( this.w_FLFOPO="S", this.w_MVCODPOR , Nvl(_Curs_DES_DIVE.DDCODPOR," ") )
        this.w_MVCODSPE = IIF( this.w_FLFOSP="S", this.w_MVCODSPE, Nvl(_Curs_DES_DIVE.DDCODSPE,Space(3)) )
        this.w_MVTIPOPE = Nvl(_Curs_DES_DIVE.DDTIPOPE,Space(10))
        * --- Se l'agente nella sede predefinita � vuoto, prende comunque quello della anagrafica cliente
        this.w_MVCODAGE = IIF( Empty( Nvl( _Curs_DES_DIVE.DDCODAGE, Space(5) ) ) OR this.w_FLFOAG="S", this.w_MVCODAGE, Nvl(_Curs_DES_DIVE.DDCODAGE,Space(5)) )
        if Not Empty(this.w_MVCODAGE) AND this.w_FLFOA2<>"S"
          * --- Lettura Capoarea
          * --- Read from AGENTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AGCZOAGE"+;
              " from "+i_cTable+" AGENTI where ";
                  +"AGCODAGE = "+cp_ToStrODBC(this.w_MVCODAGE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AGCZOAGE;
              from (i_cTable) where;
                  AGCODAGE = this.w_MVCODAGE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVCODAG2 = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        EXIT
      endif
        select _Curs_DES_DIVE
        continue
      enddo
      use
    endif
    this.w_BOLBOL = " "
    this.w_PEIINC = 0
    this.w_BOLINC = " "
    if NOT EMPTY(this.w_MVIVAINC)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAINC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAINC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEIINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLINC = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_PEIIMB = 0
    this.w_BOLIMB = " "
    if NOT EMPTY(this.w_MVIVAIMB)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAIMB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAIMB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEIIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLIMB = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_PEITRA = 0
    this.w_BOLTRA = " "
    if NOT EMPTY(this.w_MVIVATRA)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVATRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVATRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PEITRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_BOLTRA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if NOT EMPTY(this.w_MVIVABOL)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVBOLIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVABOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVBOLIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVABOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_BOLBOL = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_IVALIS = " "
    this.w_QUALIS = " "
    if NOT EMPTY(this.w_MVTCOLIS)
      * --- Read from LISTINI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LISTINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LSIVALIS,LSQUANTI"+;
          " from "+i_cTable+" LISTINI where ";
              +"LSCODLIS = "+cp_ToStrODBC(this.w_MVTCOLIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LSIVALIS,LSQUANTI;
          from (i_cTable) where;
              LSCODLIS = this.w_MVTCOLIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IVALIS = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
        this.w_QUALIS = NVL(cp_ToDate(_read_.LSQUANTI),cp_NullValue(_read_.LSQUANTI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    SELECT GENEORDI
    GO TOP
    SCAN FOR NOT EMPTY(NVL(OFSERIAL," "))
    * --- Scrive nuova Riga sul Temporaneo di Appoggio
    this.w_CPROWORD = NVL( CPROWORD , 0 )
    this.w_MVTIPRIG = NVL(ODTIPRIG, "R")
    this.w_MVCODICE = NVL(ODCODICE, SPACE(20))
    this.w_MVCODART = NVL(ODCODART, SPACE(20))
    this.w_MVDESART = NVL(CADESART, SPACE(40))
    this.w_MVDESSUP = NVL(ODNOTAGG, " ")
    this.w_MVUNIMIS = NVL(ODUNIMIS, SPACE(3))
    this.w_MVCATCON = NVL(ARCATCON, SPACE(5))
    this.w_MVCODCLA = NVL(ARCODCLA, SPACE(3))
    this.w_MVCONTRA = NVL(ODCONTRA, SPACE(15))
    this.w_GRUMER = NVL(ARGRUMER, SPACE(5))
    this.w_MVCODLIS = NVL(this.w_MVTCOLIS, SPACE(5))
    this.w_MVQTAMOV = NVL(ODQTAMOV, 0)
    this.w_MVQTAUM1 = NVL(ODQTAUM1, 0)
    this.w_MVPREZZO = NVL(ODPREZZO, 0)
    this.w_MVSCONT1 = NVL(ODSCONT1, 0)
    this.w_MVSCONT2 = NVL(ODSCONT2, 0)
    this.w_MVSCONT3 = NVL(ODSCONT3, 0)
    this.w_MVSCONT4 = NVL(ODSCONT4, 0)
    this.w_MVFLOMAG = NVL(ODFLOMAG, "X")
    * --- Ricalcolo Codice IVA sulla base della Tripla calcolata
    if this.w_MVTIPRIG <>"D"
      this.w_MVCODIVA = CALCODIV(this.w_MVCODART, this.oParentObject.w_MVTIPCON, this.w_MVCODCON, this.w_MVTIPOPE, this.oParentObject.w_MVDATREG)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVBOLIVA,IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVBOLIVA,IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_BOLIVA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_MVCODIVA = SPACE(5)
      this.w_BOLIVA = SPACE(1)
      this.w_PERIVA = 0
    endif
    SELECT GENEORDI
    this.w_UNMIS1 = NVL(ARUNMIS1, SPACE(3))
    this.w_UNMIS2 = NVL(ARUNMIS2, SPACE(3))
    this.w_UNMIS3 = NVL(CAUNIMIS, SPACE(3))
    this.w_MOLTIP = NVL(ARMOLTIP, 0)
    this.w_MOLTI3 = NVL(CAMOLTIP, 0)
    this.w_OPERAT = NVL(AROPERAT, " ")
    this.w_OPERA3 = NVL(CAOPERAT, " ")
    this.w_FLSERA = NVL(ARTIPSER,SPACE(1))
    this.w_MVIMPACC = 0
    this.w_MVIMPSCO = 0
    this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
    this.w_MVVALMAG = CAVALMAG(this.w_MVFLSCOR, this.w_MVVALRIG, this.w_MVIMPSCO, this.w_MVIMPACC, this.w_PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE )
    this.w_INDIVA = IIF(this.w_PERIVA<>0 AND this.w_PERIVE=0, IVPERIND, 0)
    this.w_MVPESNET = NVL(ARPESNET, 0)
    this.w_MVNOMENC = NVL(ARNOMENC, SPACE(8))
    this.w_MVUMSUPP = NVL(ARUMSUPP, SPACE(3))
    this.w_MVMOLSUP = NVL(ARMOLSUP, 0)
    * --- Valorizzazione Centro di Costo
    this.w_MVCODCEN = NVL(ARCODCEN, SPACE(15))
    this.w_MVVOCCOS = NVL(MVVOCCOS, SPACE(15))
    this.w_MVCODCOM = SPACE(15)
    this.w_MVFLORCO = " "
    this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_MVCODIVE, this.w_PERIVE, this.w_INDIVE, this.w_PERIVA, this.w_INDIVA )
    this.w_MVIMPCOM = 0
    this.w_MVTIPATT = "A"
    this.w_MVCODATT = SPACE(15)
    * --- Provvigioni
    * --- Se attivo 'Provvigioni in Generazione Docuemnti' su paramentri provvigioni
    *     devo impostare il tipo provvigione come se caricassi a mano un documento
    *     Verr� poi calcolato da GSVE_BPP
    this.w_MVTIPPRO = IIF(this.w_MVFLOMAG<>"X","ES",IIF(g_PROGEN = "S", IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")) ,NVL(ODTIPPRO,"DC")))
    this.w_MVPERPRO = NVL(ODPERPRO,0)
    this.w_MVTIPPR2 = IIF(this.w_MVFLOMAG<>"X","ES",IIF(g_PROGEN = "S", IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")) ,NVL(ODTIPPR2,"DC")))
    this.w_MVPROCAP = NVL(ODPROCAP,0)
    this.w_ARFLAPCA = Nvl( ARFLAPCA, Space(1))
    this.w_ODDATPCO = IIF(EMPTY(NVL( ODDATPCO, " " )), this.oParentObject.w_MVDATEVA, NVL( ODDATPCO, this.oParentObject.w_MVDATEVA ) )
    * --- Totalizzatori
    this.w_TOTALE = this.w_TOTALE + this.w_MVVALRIG
    this.w_TOTMERCE = this.w_TOTMERCE + IIF(this.w_MVFLOMAG="X" AND this.w_FLSERA<>"S", this.w_MVVALRIG, 0)
    INSERT INTO GeneApp ;
    (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ;
    t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ;
    t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ;
    t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ;
    t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ, ;
    t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ;
    t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, ;
    t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ;
    t_MVCODMAG, t_MVCODCEN, t_MVVOCCOS, ;
    t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA, t_INDIVA, t_FLSERA, t_MVIMPCOM, t_MVIMPAC2, t_CPROWORD, ;
    t_MVTIPPRO, t_MVPERPRO, t_MVTIPPR2, t_MVPROCAP, t_ARFLAPCA, t_MVFLORCO ) ;
    VALUES (this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ;
    this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ;
    this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ;
    this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ;
    this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, ;
    this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ;
    this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ;
    this.w_MVPESNET, this.w_MVNOMENC, this.w_MVUMSUPP, this.w_MVMOLSUP, ;
    this.oParentObject.w_MVCODMAG, this.w_MVCODCEN, this.w_MVVOCCOS, ;
    this.w_MVCODCOM, this.w_MVTIPATT, this.w_MVCODATT, this.w_ODDATPCO, this.w_INDIVA, this.w_FLSERA, this.w_MVIMPCOM, 0, this.w_CPROWORD, ;
    this.w_MVTIPPRO, this.w_MVPERPRO, this.w_MVTIPPR2, this.w_MVPROCAP, this.w_ARFLAPCA, this.w_MVFLORCO )
    SELECT GENEORDI
    ENDSCAN
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizioni Variabili Locali
    * --- Inizializza le Variabili di Lavoro
    this.w_TOTMERCE = 0
    this.w_MVIVAIMB = SPACE(5)
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVIVAINC = SPACE(5)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVSPEINC = 0
    this.w_MVIVATRA = SPACE(5)
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVSPEIMB = 0
    this.w_MVIVABOL = SPACE(5)
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVSPETRA = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVSPEBOL = 0
    this.w_MVFLRIMB = " "
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVACCONT = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVIMPARR = 0
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVACCPRE = 0
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVTOTRIT = 0
    this.w_MVTOTENA = 0
    this.w_MVTOTRIT = 0
    * --- Se attivo 'Provvigioni in Generazione Docuemnti' su paramentri provvigioni
    *     devo impostare il tipo provvigione come se caricassi a mano un documento
    *     Verr� poi calcolato da GSVE_BPP
    this.w_MVTIPPRO = IIF(g_PROGEN = "S", IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")) ,"DC")
    this.w_MVPERPRO = 0
    this.w_MVTIPPR2 = IIF(g_PROGEN = "S", IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT")) ,"DC")
    this.w_MVPROCAP = 0
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna le Spese Accessorie e gli Sconti Finali
    if this.w_MVFLFOSC<>"S"
      this.w_MVSCONTI = Calsco(this.w_TOTMERCE, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
    endif
    this.w_MVFLSALD = " "
    * --- Lancia Batch per ripartizione spese accessorie
    *     Il 5� e l'ultimo parametro sono le spese accessorie che dal POS non esistono
    * --- Ripartisce Spese Accessorie e Sconti
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDRIPINC,TDRIPIMB,TDRIPTRA"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDRIPINC,TDRIPIMB,TDRIPTRA;
        from (i_cTable) where;
            TDTIPDOC = this.oParentObject.w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVFLRINC = NVL(cp_ToDate(_read_.TDRIPINC),cp_NullValue(_read_.TDRIPINC))
      this.w_MVFLRIMB = NVL(cp_ToDate(_read_.TDRIPIMB),cp_NullValue(_read_.TDRIPIMB))
      this.w_MVFLRTRA = NVL(cp_ToDate(_read_.TDRIPTRA),cp_NullValue(_read_.TDRIPTRA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Totale Spese da Ripartire
    *     Nel caso di Scorporo piede fattura e codice Iva spese presente, calcolo in w_SPEACC_IVA 
    *     la somma delle spese scorporate della loro Iva.
    this.w_SPEACC_IVA = 0
    this.w_SPEACC = 0
    if this.w_ANFLINCA="S"
      this.w_MVSPEINC = 0
    else
      if this.w_MVFLRINC="S"
        if Not Empty(this.w_MVIVAINC) 
          if this.w_MVFLSCOR="S"
            this.w_SPEACC_IVA = Calnet( this.w_MVSPEINC, this.w_PEIINC, this.w_DECTOT, Space(5), 0 )
          else
            this.w_SPEACC_IVA = this.w_MVSPEINC
          endif
        else
          this.w_SPEACC = this.w_MVSPEINC
        endif
      endif
    endif
    if Not Empty(this.w_MVIVAIMB) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRIMB="S", Calnet( this.w_MVSPEIMB, this.w_PEIIMB, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRIMB="S", this.w_MVSPEIMB, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.w_MVFLRIMB="S", this.w_MVSPEIMB, 0)
    endif
    if Not Empty(this.w_MVIVATRA) 
      if this.w_MVFLSCOR="S"
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRTRA="S", Calnet( this.w_MVSPETRA, this.w_PEITRA, this.w_DECTOT, Space(5), 0 ) , 0)
      else
        this.w_SPEACC_IVA = this.w_SPEACC_IVA + IIF(this.w_MVFLRTRA="S", this.w_MVSPETRA, 0)
      endif
    else
      this.w_SPEACC = this.w_SPEACC + IIF(this.w_MVFLRTRA="S", this.w_MVSPETRA, 0)
    endif
    * --- La ripartizione delle spese viene fatta solo se non � attivo
    *     il flag 'Ripartisce contributi accessori' della causale documento.
    *     Se il flag � attivo la ripartizione sar� chiamata da GSAR_BRD nel caso sia stata attivata
    *     la variabile w_RICTOT
    this.w_DATCOM = EVL(this.w_MVDATDOC, this.oParentObject.w_MVDATREG )
    GSAR_BRS(this,"B", "GeneApp", this.w_MVFLVEAC, this.w_MVFLSCOR, this.w_SPEACC, this.w_MVSCONTI, this.w_TOTMERCE, this.w_MVCODIVE, this.w_MVCAOVAL, this.w_DATCOM, this.w_CAONAZ, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_SPEACC_IVA)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Calcola Sconti su Omaggio
    this.w_MVFLSCOM = g_FLSCOM
    if g_COAC="S" And this.w_ANFLAPCA="S" And this.w_TDFLAPCA="S" 
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Calcoli Finali
    GSAR_BFA(this,"D")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Controllo articoli obsoleti
    this.w_ESISTEARTICOLOOBSOLETO = .F.
    this.w_ESISTEARTICOLONONOBSOLETO = .F.
    this.w_oERRORLOG=createobject("AH_ERRORLOG")
    SELECT GENEAPP
    GO TOP
    SCAN FOR T_MVTIPRIG = "R"
    this.w_MVCODART = NVL( t_MVCODART , "" )
    this.w_CPROWORD = NVL( t_CPROWORD , 0 )
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARDTOBSO"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARDTOBSO;
        from (i_cTable) where;
            ARCODART = this.w_MVCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ARDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if !EMPTY( NVL( this.w_ARDTOBSO , CTOD( "  -  -  " ) ) ) AND CP_TODATE( this.w_ARDTOBSO ) <= CP_TODATE( this.oParentObject.w_MVDATREG )
      this.w_oERRORLOG.AddMsgLog("Riga %1 - Articolo %2", ALLTRIM(STR(this.w_CPROWORD)) , this.w_MVCODART)     
      this.w_ESISTEARTICOLOOBSOLETO = .T.
    else
      this.w_ESISTEARTICOLONONOBSOLETO = .T.
    endif
    ENDSCAN
    if this.w_ESISTEARTICOLOOBSOLETO
      this.w_oERRORLOG.PrintLog(this, "Elenco articoli obsoleti" , .t. , "Sono stati rilevati articoli obsoleti nell'offerta.%0Si desidera stampare l'elenco?")     
      if this.w_ESISTEARTICOLONONOBSOLETO
        if !AH_YESNO( "Si desidera proseguire utilizzando soltanto gli articoli non obsoleti?" )
          this.w_OK = .F.
          this.w_MESS = AH_MSGFORMAT( "Operazione abbandonata" )
        else
          * --- Elimina gli articoli obsoleti
          SELECT GENEAPP
          GO TOP
          SCAN FOR T_MVTIPRIG = "R"
          this.w_MVCODART = NVL( t_MVCODART , "" )
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDTOBSO"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDTOBSO;
              from (i_cTable) where;
                  ARCODART = this.w_MVCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ARDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !EMPTY( NVL( this.w_ARDTOBSO , CTOD( "  -  -  " ) ) ) AND CP_TODATE( this.w_ARDTOBSO ) <= CP_TODATE( this.oParentObject.w_MVDATREG )
            replace t_MVTIPRIG with " "
            replace t_MVCODART with SPACE( 20 )
          endif
          ENDSCAN
        endif
      else
        this.w_OK = .F.
        this.w_MESS = AH_MSGFORMAT( "Operazione abbandonata" )
      endif
    endif
    if this.w_OK AND this.oParentObject.w_MVFLPROV<>"S"
      * --- Controllo disponibilit�
      Select Min(t_CPROWORD) As Riga ,Sum(t_MVQTAUM1*IIF( this.w_MFLCASC="-",-1, IIF( this.w_MFLCASC="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MFLRISE="-",-1, IIF( this.w_MFLRISE="+",1, 0))) As Disp ,; 
 Sum(t_MVQTAUM1*IIF( this.w_MFLCASC="-",-1, IIF( this.w_MFLCASC="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MFLRISE="-",-1, IIF( this.w_MFLRISE="+",1, 0)) + ; 
 t_MVQTAUM1*IIF( this.w_MFLORDI="-",-1, IIF( this.w_MFLORDI="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MFLIMPE="-",-1, IIF( this.w_MFLIMPE="+",1, 0))) As DispC ,; 
 Sum(t_MVQTAUM1*IIF( this.w_MFLRISE="-",-1, IIF( this.w_MFLRISE="+",1, 0))) As DispR , ; 
 t_MVCODMAG As CodMag, IIF(t_MVTIPRIG="R" And Not Empty(Alltrim(this.w_MFLORDI+this.w_MFLIMPE+this.w_MFLCASC+this.w_MFLRISE)) , t_MVCODART, SPACE(20)) As KEYSAL,Max(Left(Alltrim(this.w_MFLCASC+this.w_MFLRISE),1)) As Mov_Disp, Max(Left(Alltrim(this.w_MFLORDI+this.w_MFLIMPE),1)) As Mov_Cont,Max(this.w_MFLRISE) As Mov_Rise ; 
 from GeneApp group by KEYSAL, CODMAG into cursor TRS_ADD
      * --- Costruisci il cursore TRS_ADD utilizzato da GSAR_BDA
      this.w_OK = GSAR_BDA( This , "S", This )
    endif
    * --- Calcola MVSERIAL, MVNUMREG, MVNUMDOC
    if this.w_OK
      this.oParentObject.w_MVSERIAL = SPACE(10)
      this.oParentObject.w_MVNUMEST = 0
      this.oParentObject.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_MVNUMRIF = -20
      * --- Try
      local bErr_046E2BE0
      bErr_046E2BE0=bTrsErr
      this.Try_046E2BE0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_046E2BE0
      * --- End
    endif
    * --- Indico a GSOF_BDO se il documento � stato generato
    if Type("this.oParentObject.w_OKGEN")="L"
      this.oParentObject.w_OKGEN=this.w_OK
    endif
    USE IN SELECT("TRS_ADD")
  endproc
  proc Try_046E2BE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this.oParentObject, i_Conn, "SEDOC", "i_codazi,w_MVSERIAL")
    this.oParentObject.w_MVNUMDOC = 0
    cp_NextTableProg(this.oParentObject, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    * --- Valorizzo Numero Registrazione
    this.oParentObject.w_MVNUMREG = this.oParentObject.w_MVNUMDOC
    * --- Scrive la Testata
    * --- Try
    local bErr_046F9CA8
    bErr_046F9CA8=bTrsErr
    this.Try_046F9CA8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_errormsg("Errore in inserimento DOC_MAST%0%1",,"",MESSAGE () )
    endif
    bTrsErr=bTrsErr or bErr_046F9CA8
    * --- End
    * --- Try
    local bErr_046EA5B8
    bErr_046EA5B8=bTrsErr
    this.Try_046EA5B8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_errormsg("Errore in scrittura DOC_MAST%0%1",,"",MESSAGE () )
    endif
    bTrsErr=bTrsErr or bErr_046EA5B8
    * --- End
    * --- Cicla sulle Righe Documento
    SELECT GeneApp
    GO TOP
    SCAN FOR t_MVTIPRIG<>" " AND NOT EMPTY(t_MVCODICE)
    * --- Scrive il Dettaglio
    this.w_MVTIPRIG = t_MVTIPRIG
    this.w_MVCODICE = t_MVCODICE
    this.w_MVCODART = t_MVCODART
    this.w_MVDESART = t_MVDESART
    this.w_MVDESSUP = t_MVDESSUP
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_MVCATCON = t_MVCATCON
    this.w_MVCODCLA = t_MVCODCLA
    this.w_MVCONTRA = t_MVCONTRA
    this.w_MVCODLIS = t_MVCODLIS
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVPREZZO = t_MVPREZZO
    this.w_MVIMPACC = t_MVIMPACC
    this.w_MVIMPSCO = t_MVIMPSCO
    this.w_MVVALMAG = t_MVVALMAG
    this.w_MVIMPNAZ = t_MVIMPNAZ
    this.w_MVSCONT1 = t_MVSCONT1
    this.w_MVSCONT2 = t_MVSCONT2
    this.w_MVSCONT3 = t_MVSCONT3
    this.w_MVSCONT4 = t_MVSCONT4
    this.w_MVFLOMAG = t_MVFLOMAG
    this.w_MVCODIVA = t_MVCODIVA
    this.w_MVVALRIG = t_MVVALRIG
    this.w_MVPESNET = t_MVPESNET
    this.w_MVNOMENC = t_MVNOMENC
    this.w_MVUMSUPP = t_MVUMSUPP
    this.w_MVMOLSUP = t_MVMOLSUP
    this.w_LCODMAG = t_MVCODMAG
    this.w_MVCODCEN = t_MVCODCEN
    this.w_MVVOCCOS = t_MVVOCCOS
    this.w_MVCODCOM = t_MVCODCOM
    this.w_MVIMPCOM = t_MVIMPCOM
    this.w_MVTIPATT = t_MVTIPATT
    this.w_MVCODATT = t_MVCODATT
    this.oParentObject.w_MVDATEVA = t_MVDATEVA
    this.w_MVCAUMAG = this.oParentObject.w_MVTCAMAG
    this.w_MVFLORDI = this.w_OLDMFLORDI
    this.w_MVFLIMPE = this.w_OLDMFLIMPE
    this.w_MVFLCASC = this.w_OLDMFLCASC
    this.w_MVFLRISE = this.w_OLDMFLRISE
    this.w_MVFLELGM = this.w_MFLELGM
    this.w_MVTIPPRO = t_MVTIPPRO
    this.w_MVPERPRO = t_MVPERPRO
    this.w_MVTIPPR2 = t_MVTIPPR2
    this.w_MVPROCAP = t_MVPROCAP
    this.w_MVKEYSAL = IIF(this.w_MVTIPRIG="R" AND this.w_MFLAVAL<>"N" And Not Empty(Alltrim(this.w_MVFLORDI+this.w_MVFLIMPE+this.w_MVFLCASC+this.w_MVFLRISE)), this.w_MVCODART, SPACE(20))
    * --- Solo per AHR
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUTISER,ARDATINT,ARFLLOTT"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUTISER,ARDATINT,ARFLLOTT;
        from (i_cTable) where;
            ARCODART = this.w_MVCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ARUTISER = NVL(cp_ToDate(_read_.ARUTISER),cp_NullValue(_read_.ARUTISER))
      this.w_ARDATINT = NVL(cp_ToDate(_read_.ARDATINT),cp_NullValue(_read_.ARDATINT))
      this.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_ARUTISER = "S"
      this.w_MVFLTRAS = "S"
    else
      do case
        case this.w_ARDATINT = "N"
          this.w_MVFLTRAS = "S"
        case this.w_ARDATINT = "S"
          this.w_MVFLTRAS = "Z"
        case this.w_ARDATINT = "F"
          this.w_MVFLTRAS = " "
        case this.w_ARDATINT = "I"
          this.w_MVFLTRAS = "I"
      endcase
    endif
    this.w_LCODMAG = IIF(EMPTY(this.w_MVKEYSAL), SPACE(5), this.w_LCODMAG)
    * --- Gestione Analitica
    this.w_MVVOCCEN = SPACE(15)
    this.w_OK_ANALI = .T.
    this.w_OK_COMM = .T.
    if g_PERCCR="S" 
      * --- Voce di Costo 
      this.w_MVVOCCEN = this.w_MVVOCCOS
      if NOT EMPTY(this.w_MVVOCCEN) OR this.w_MVTIPRIG $ "MF"
        this.w_MVCODCEN = t_MVCODCEN
      endif
      this.w_MVCODCOM = IIF(g_COMM="S", t_MVCODCOM, SPACE(15))
      this.w_MVCODATT = IIF(g_COMM="S", t_MVCODATT, SPACE(15))
      this.w_OK_ANALI = NOT EMPTY(this.w_MVCODCEN) AND NOT EMPTY(this.w_MVVOCCEN)
      this.w_OK_COMM = g_COMM<>"S" or this.w_MFLCOMM<>"S" or not empty(this.w_MVCODCOM) and not empty(this.w_MVCODATT)
    endif
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    if g_COAC="S"
      * --- Marco sul trransitorio il reale CPROWNUM della riga se non riferimento accessorio..
      if Empty( Nvl(t_MVCACONT, Space(5)) )
         
 Select "GeneApp" 
 Replace t_CPROWNUM With this.w_CPROWNUM
        this.w_MVRIFCAC = 0
      else
        * --- Se t_CPROWNUM valorizzato mi sposto alla riga e leggo il CPROWNUM
        *     effetivamente salvato sul documento..
        this.w_SEARCHROW = Nvl( geneApp.t_CPROWNUM, 0)
        if this.w_SEARCHROW>0
          if not Eof( "GeneApp" )
            this.w_ACTPOS = Recno("GeneApp")
          else
            this.w_ACTPOS = Recno("GeneApp") -1
          endif
           
 Select GeneApp 
 Go ( this.w_SEARCHROW )
          this.w_MVRIFCAC = Nvl(GeneApp.t_CPROWNUM,0)
           
 Select GeneApp 
 Go ( this.w_ACTPOS )
          * --- Rileggo dal documento il numero di riga dell'articolo al quale il contributo � legato..
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWORD"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVRIFCAC);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWORD;
              from (i_cTable) where;
                  MVSERIAL = this.oParentObject.w_MVSERIAL;
                  and CPROWNUM = this.w_MVRIFCAC;
                  and MVNUMRIF = this.w_MVNUMRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CPROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Al CPROWORD del padre aggiungo 1 pi� il progressivo di ogni contributo
          *     (il primo 0) calcolata nella pagina di esplosione).
          this.w_CPROWORD = this.w_CPROWORD + 1 + Nvl( GeneApp.t_CPROWORD , 0 )
        else
          this.w_MVRIFCAC = 0
        endif
      endif
      this.w_MVCACONT = Nvl(t_MVCACONT, Space(5)) 
    else
      this.w_MVRIFCAC = 0
      this.w_MVCACONT = Space(5)
    endif
    * --- Try
    local bErr_0472B0E8
    bErr_0472B0E8=bTrsErr
    this.Try_0472B0E8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_errormsg("Errore in inserimento DOC_DETT%0%1",,"",MESSAGE () )
    endif
    bTrsErr=bTrsErr or bErr_0472B0E8
    * --- End
    * --- Try
    local bErr_0472BCB8
    bErr_0472BCB8=bTrsErr
    this.Try_0472BCB8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_errormsg("Errore in scrittura DOC_DETT%0%1",,"",MESSAGE () )
    endif
    bTrsErr=bTrsErr or bErr_0472BCB8
    * --- End
    * --- Aggiorna Saldi
    if this.w_MVQTAUM1<>0 AND NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_LCODMAG)
      if NOT EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
        * --- Try
        local bErr_0473E8E0
        bErr_0473E8E0=bTrsErr
        this.Try_0473E8E0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0473E8E0
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_LCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MVKEYSAL;
              and SLCODMAG = this.w_LCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore in scrittura SALDIART'
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.w_MVCODCOM,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.w_MVCODCOM
          endif
          * --- Try
          local bErr_04740C20
          bErr_04740C20=bTrsErr
          this.Try_04740C20()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_LCODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTAPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  ,SCQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_MVKEYSAL;
                  and SCCODMAG = this.w_LCODMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa'
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04740C20
          * --- End
        endif
      endif
    endif
    SELECT GeneApp
    ENDSCAN
    if g_PROGEN= "S"
      this.w_SERPRO = this.oParentObject.w_MVSERIAL
      this.w_MASSGEN = "S"
      * --- Considero sempre come se precedentemente avesi avuto disattivo
      *     In questo modo mi aggiorna il documento generato nel modo corretto
      this.w_OLDCALPRO = "DI"
      * --- Read from PAR_PROV
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_PROV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PPCALPRO,PPCALSCO,PPLISRIF"+;
          " from "+i_cTable+" PAR_PROV where ";
              +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PPCALPRO,PPCALSCO,PPLISRIF;
          from (i_cTable) where;
              PPCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PPCALPRO = NVL(cp_ToDate(_read_.PPCALPRO),cp_NullValue(_read_.PPCALPRO))
        this.w_PPCALSCO = NVL(cp_ToDate(_read_.PPCALSCO),cp_NullValue(_read_.PPCALSCO))
        this.w_PPLISRIF = NVL(cp_ToDate(_read_.PPLISRIF),cp_NullValue(_read_.PPLISRIF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do GSVE_BPP with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_CPROWNUM=0
      * --- NESSUNA RIGA NEL DETTAGLIO
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    else
      this.w_TOTDOC = 0
      this.w_MINRATA = 0
      * --- Cicla Sul Vettore delle Rate (Calcolato in GSAR_BFA)
      FOR l_i=1 TO 999
      if NOT EMPTY(DR[l_i, 1]) AND NOT EMPTY(DR[l_i, 2])
        this.w_RSNUMRAT = l_i
        this.w_RSDATRAT = DR[l_i, 1]
        this.w_RSIMPRAT = DR[l_i, 2]
        this.w_RSMODPAG = DR[l_i, 3]
        this.w_TOTDOC = this.w_TOTDOC + this.w_RSIMPRAT
        this.w_MINRATA = MIN(this.w_MINRATA, this.w_RSIMPRAT)
        * --- Scrive Doc_Rate
        * --- Try
        local bErr_04746DA0
        bErr_04746DA0=bTrsErr
        this.Try_04746DA0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_errormsg("Errore in inserimento DOC_RATE%0%1",,"",MESSAGE () )
          this.w_OK = .F.
        endif
        bTrsErr=bTrsErr or bErr_04746DA0
        * --- End
      endif
      NEXT
      if this.w_TOTDOC<0 && oppure verifica per singola rata: this.w_MINRATA<0
        * --- Documento non Accettato
        if this.w_TOTDOC<0
          this.w_MSGRET = "importo totale negativo"
        else
          this.w_MSGRET = "rate con importo negativo"
        endif
        ah_errormsg("Errore inserimento rate: %1",,"", this.w_MSGRET)
        this.w_OK = .F.
      endif
      if this.w_OK
        * --- Aggiorna Riferimento sul Documento di Offerta
        * --- Try
        local bErr_04724608
        bErr_04724608=bTrsErr
        this.Try_04724608()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_errormsg("Errore in scrittura OFF_ERTE%0%1",,"",MESSAGE () )
        endif
        bTrsErr=bTrsErr or bErr_04724608
        * --- End
        this.oParentObject.oParentObject.w_OFRIFDOC = this.oParentObject.w_MVSERIAL
        this.oParentObject.oParentObject.w_OFSTATUS = "C"
        this.oParentObject.oParentObject.w_OFDATCHI = i_DATSYS
        Select Min(t_CPROWORD) As Riga ,Sum(t_MVQTAUM1*IIF( this.w_MFLCASC="-",-1, IIF( this.w_MFLCASC="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MFLRISE="-",-1, IIF( this.w_MFLRISE="+",1, 0))) As Disp ,; 
 Sum(t_MVQTAUM1*IIF( this.w_MFLCASC="-",-1, IIF( this.w_MFLCASC="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MFLRISE="-",-1, IIF( this.w_MFLRISE="+",1, 0)) + ; 
 t_MVQTAUM1*IIF( this.w_MFLORDI="-",-1, IIF( this.w_MFLORDI="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MFLIMPE="-",-1, IIF( this.w_MFLIMPE="+",1, 0))) As DispC ,; 
 Sum(t_MVQTAUM1*IIF( this.w_MFLRISE="-",-1, IIF( this.w_MFLRISE="+",1, 0))) As DispR , ; 
 t_MVCODMAG As CodMag, IIF(t_MVTIPRIG="R" And Not Empty(Alltrim(this.w_MFLORDI+this.w_MFLIMPE+this.w_MFLCASC+this.w_MFLRISE)) , t_MVCODART, SPACE(20)) As KEYSAL,; 
 Max(Left(Alltrim(this.w_MFLCASC+this.w_MFLRISE),1)) As Mov_Disp, Max(Left(Alltrim(this.w_MFLORDI+this.w_MFLIMPE),1)) As Mov_Cont,Max(this.w_MFLRISE) As Mov_Rise ; 
 from GeneApp group by KEYSAL, CODMAG into cursor TRS_ADD
        this.w_OK = GSAR_BDA( This , " ", This )
      endif
      if ! this.w_OK
        * --- Raise
        i_Error="DISPFAIL"
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
    endif
    return
  proc Try_046F9CA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVALFDOC"+",MVANNDOC"+",MVCAUCON"+",MVCLADOC"+",MVCODAGE"+",MVCODCON"+",MVCODDES"+",MVCODESE"+",MVCODPOR"+",MVCODSPE"+",MVCODUTE"+",MVCODVET"+",MVCONCON"+",MVDATCIV"+",MVDATDOC"+",MVDATREG"+",MVFLACCO"+",MVFLCONT"+",MVFLGIOM"+",MVFLINTE"+",MVFLPROV"+",MVFLSALD"+",MVFLSCOR"+",MVFLVEAC"+",MVEMERIC"+",MVGENEFF"+",MVGENPRO"+",MVNUMDOC"+",MVNUMREG"+",MVPRD"+",MVRIFDIC"+",MVSERIAL"+",MVTCAMAG"+",MVTCOLIS"+",MVTCONTR"+",MVTFRAGG"+",MVTIPCON"+",MVTIPDOC"+",MVTIPORN"+",MVTIPOPE"+",MVCATOPE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVALFDOC),'DOC_MAST','MVALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVANNDOC),'DOC_MAST','MVANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCAUCON),'DOC_MAST','MVCAUCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCLADOC),'DOC_MAST','MVCLADOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODAGE),'DOC_MAST','MVCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'DOC_MAST','MVCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODDES),'DOC_MAST','MVCODDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODESE),'DOC_MAST','MVCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODPOR),'DOC_MAST','MVCODPOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODSPE),'DOC_MAST','MVCODSPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODUTE),'DOC_MAST','MVCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODVET),'DOC_MAST','MVCODVET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONCON),'DOC_MAST','MVCONCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATCIV),'DOC_MAST','MVDATCIV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATREG),'DOC_MAST','MVDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLACCO),'DOC_MAST','MVFLACCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLINTE),'DOC_MAST','MVFLINTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVFLPROV),'DOC_MAST','MVFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSALD),'DOC_MAST','MVFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVEMERIC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENEFF');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMREG),'DOC_MAST','MVNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVPRD),'DOC_MAST','MVPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVRIFDIC),'DOC_MAST','MVRIFDIC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DOC_MAST','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCONTR),'DOC_MAST','MVTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DOC_MAST','MVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DOC_MAST','MVTIPORN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPOPE),'DOC_MAST','MVTIPOPE');
      +","+cp_NullLink(cp_ToStrODBC("OP"),'DOC_MAST','MVCATOPE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVALFDOC',this.oParentObject.w_MVALFDOC,'MVANNDOC',this.oParentObject.w_MVANNDOC,'MVCAUCON',this.oParentObject.w_MVCAUCON,'MVCLADOC',this.w_MVCLADOC,'MVCODAGE',this.w_MVCODAGE,'MVCODCON',this.w_MVCODCON,'MVCODDES',this.w_MVCODDES,'MVCODESE',this.oParentObject.w_MVCODESE,'MVCODPOR',this.w_MVCODPOR,'MVCODSPE',this.w_MVCODSPE,'MVCODUTE',this.oParentObject.w_MVCODUTE,'MVCODVET',this.w_MVCODVET)
      insert into (i_cTable) (MVALFDOC,MVANNDOC,MVCAUCON,MVCLADOC,MVCODAGE,MVCODCON,MVCODDES,MVCODESE,MVCODPOR,MVCODSPE,MVCODUTE,MVCODVET,MVCONCON,MVDATCIV,MVDATDOC,MVDATREG,MVFLACCO,MVFLCONT,MVFLGIOM,MVFLINTE,MVFLPROV,MVFLSALD,MVFLSCOR,MVFLVEAC,MVEMERIC,MVGENEFF,MVGENPRO,MVNUMDOC,MVNUMREG,MVPRD,MVRIFDIC,MVSERIAL,MVTCAMAG,MVTCOLIS,MVTCONTR,MVTFRAGG,MVTIPCON,MVTIPDOC,MVTIPORN,MVTIPOPE,MVCATOPE &i_ccchkf. );
         values (;
           this.oParentObject.w_MVALFDOC;
           ,this.oParentObject.w_MVANNDOC;
           ,this.oParentObject.w_MVCAUCON;
           ,this.w_MVCLADOC;
           ,this.w_MVCODAGE;
           ,this.w_MVCODCON;
           ,this.w_MVCODDES;
           ,this.oParentObject.w_MVCODESE;
           ,this.w_MVCODPOR;
           ,this.w_MVCODSPE;
           ,this.oParentObject.w_MVCODUTE;
           ,this.w_MVCODVET;
           ,this.w_MVCONCON;
           ,this.oParentObject.w_MVDATCIV;
           ,this.w_MVDATDOC;
           ,this.oParentObject.w_MVDATREG;
           ,this.oParentObject.w_MVFLACCO;
           ," ";
           ," ";
           ,this.oParentObject.w_MVFLINTE;
           ,this.oParentObject.w_MVFLPROV;
           ,this.w_MVFLSALD;
           ,this.w_MVFLSCOR;
           ,this.w_MVFLVEAC;
           ,this.w_MVFLVEAC;
           ," ";
           ," ";
           ,this.oParentObject.w_MVNUMDOC;
           ,this.oParentObject.w_MVNUMREG;
           ,this.oParentObject.w_MVPRD;
           ,this.w_MVRIFDIC;
           ,this.oParentObject.w_MVSERIAL;
           ,this.oParentObject.w_MVTCAMAG;
           ,this.w_MVTCOLIS;
           ,this.w_MVTCONTR;
           ,this.oParentObject.w_MVTFRAGG;
           ,this.oParentObject.w_MVTIPCON;
           ,this.oParentObject.w_MVTIPDOC;
           ,this.oParentObject.w_MVTIPCON;
           ,this.w_MVTIPOPE;
           ,"OP";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_046EA5B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVNOTAGG ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVNOTAGG');
      +",MVFLFOSC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLFOSC),'DOC_MAST','MVFLFOSC');
      +",MVALFEST ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVALFEST),'DOC_MAST','MVALFEST');
      +",MVANNPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVANNPRO),'DOC_MAST','MVANNPRO');
      +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
      +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBA2),'DOC_MAST','MVCODBA2');
      +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBAN),'DOC_MAST','MVCODBAN');
      +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVE),'DOC_MAST','MVCODIVE');
      +",MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
      +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
      +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
      +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRINC),'DOC_MAST','MVFLRINC');
      +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
      +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
      +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
      +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
      +",MVNUMEST ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMEST),'DOC_MAST','MVNUMEST');
      +",MVPRP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVPRP),'DOC_MAST','MVPRP');
      +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
      +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
      +",MVSCONTI ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONTI),'DOC_MAST','MVSCONTI');
      +",MVSCOPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOPAG),'DOC_MAST','MVSCOPAG');
      +",MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEBOL),'DOC_MAST','MVSPEBOL');
      +",MVSPEIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEIMB),'DOC_MAST','MVSPEIMB');
      +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEINC),'DOC_MAST','MVSPEINC');
      +",MVSPETRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPETRA),'DOC_MAST','MVSPETRA');
      +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
      +",MVIVACAU ="+cp_NullLink(cp_ToStrODBC(g_COICAU),'DOC_MAST','MVIVACAU');
      +",MVACCONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCONT),'DOC_MAST','MVACCONT');
      +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCPRE),'DOC_MAST','MVACCPRE');
      +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA1),'DOC_MAST','MVACIVA1');
      +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA2),'DOC_MAST','MVACIVA2');
      +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA3),'DOC_MAST','MVACIVA3');
      +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA4),'DOC_MAST','MVACIVA4');
      +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA5),'DOC_MAST','MVACIVA5');
      +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA6),'DOC_MAST','MVACIVA6');
      +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM1),'DOC_MAST','MVAFLOM1');
      +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM2),'DOC_MAST','MVAFLOM2');
      +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM3),'DOC_MAST','MVAFLOM3');
      +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM4),'DOC_MAST','MVAFLOM4');
      +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM5),'DOC_MAST','MVAFLOM5');
      +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM6),'DOC_MAST','MVAFLOM6');
      +",MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN1),'DOC_MAST','MVAIMPN1');
      +",MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN2),'DOC_MAST','MVAIMPN2');
      +",MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN3),'DOC_MAST','MVAIMPN3');
      +",MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN4),'DOC_MAST','MVAIMPN4');
      +",MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN5),'DOC_MAST','MVAIMPN5');
      +",MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN6),'DOC_MAST','MVAIMPN6');
      +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS1),'DOC_MAST','MVAIMPS1');
      +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS2),'DOC_MAST','MVAIMPS2');
      +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS3),'DOC_MAST','MVAIMPS3');
      +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS4),'DOC_MAST','MVAIMPS4');
      +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS5),'DOC_MAST','MVAIMPS5');
      +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS6),'DOC_MAST','MVAIMPS6');
      +",MVASPEST ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVASPEST),'DOC_MAST','MVASPEST');
      +",MVCODAG2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODAG2),'DOC_MAST','MVCODAG2');
      +",MVDATDIV ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDIV),'DOC_MAST','MVDATDIV');
      +",MVDATTRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATTRA),'DOC_MAST','MVDATTRA');
      +",MVDESDOC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVDESDOC');
      +",MVFLOFFE ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLOFFE');
      +",MVIMPARR ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPARR),'DOC_MAST','MVIMPARR');
      +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
      +",MVMINTRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVMINTRA),'DOC_MAST','MVMINTRA');
      +",MVORATRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVORATRA),'DOC_MAST','MVORATRA');
      +",MV__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_MV__NOTE),'DOC_MAST','MV__NOTE');
      +",MVFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLVABD),'DOC_MAST','MVFLVABD');
      +",MVFLSCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
      +",MVNUMCOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMCOR),'DOC_MAST','MVNUMCOR');
      +",UTCC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCC');
      +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DOC_MAST','UTDC');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVNOTAGG = " ";
          ,MVFLFOSC = this.w_MVFLFOSC;
          ,MVALFEST = this.oParentObject.w_MVALFEST;
          ,MVANNPRO = this.oParentObject.w_MVANNPRO;
          ,MVCAOVAL = this.w_MVCAOVAL;
          ,MVCODBA2 = this.w_MVCODBA2;
          ,MVCODBAN = this.w_MVCODBAN;
          ,MVCODIVE = this.w_MVCODIVE;
          ,MVCODPAG = this.w_MVCODPAG;
          ,MVCODVAL = this.w_MVCODVAL;
          ,MVFLRIMB = this.w_MVFLRIMB;
          ,MVFLRINC = this.w_MVFLRINC;
          ,MVFLRTRA = this.w_MVFLRTRA;
          ,MVIVAIMB = this.w_MVIVAIMB;
          ,MVIVAINC = this.w_MVIVAINC;
          ,MVIVATRA = this.w_MVIVATRA;
          ,MVNUMEST = this.oParentObject.w_MVNUMEST;
          ,MVPRP = this.oParentObject.w_MVPRP;
          ,MVSCOCL1 = this.w_MVSCOCL1;
          ,MVSCOCL2 = this.w_MVSCOCL2;
          ,MVSCONTI = this.w_MVSCONTI;
          ,MVSCOPAG = this.w_MVSCOPAG;
          ,MVSPEBOL = this.w_MVSPEBOL;
          ,MVSPEIMB = this.w_MVSPEIMB;
          ,MVSPEINC = this.w_MVSPEINC;
          ,MVSPETRA = this.w_MVSPETRA;
          ,MVVALNAZ = this.w_MVVALNAZ;
          ,MVIVACAU = g_COICAU;
          ,MVACCONT = this.w_MVACCONT;
          ,MVACCPRE = this.w_MVACCPRE;
          ,MVACIVA1 = this.w_MVACIVA1;
          ,MVACIVA2 = this.w_MVACIVA2;
          ,MVACIVA3 = this.w_MVACIVA3;
          ,MVACIVA4 = this.w_MVACIVA4;
          ,MVACIVA5 = this.w_MVACIVA5;
          ,MVACIVA6 = this.w_MVACIVA6;
          ,MVAFLOM1 = this.w_MVAFLOM1;
          ,MVAFLOM2 = this.w_MVAFLOM2;
          ,MVAFLOM3 = this.w_MVAFLOM3;
          ,MVAFLOM4 = this.w_MVAFLOM4;
          ,MVAFLOM5 = this.w_MVAFLOM5;
          ,MVAFLOM6 = this.w_MVAFLOM6;
          ,MVAIMPN1 = this.w_MVAIMPN1;
          ,MVAIMPN2 = this.w_MVAIMPN2;
          ,MVAIMPN3 = this.w_MVAIMPN3;
          ,MVAIMPN4 = this.w_MVAIMPN4;
          ,MVAIMPN5 = this.w_MVAIMPN5;
          ,MVAIMPN6 = this.w_MVAIMPN6;
          ,MVAIMPS1 = this.w_MVAIMPS1;
          ,MVAIMPS2 = this.w_MVAIMPS2;
          ,MVAIMPS3 = this.w_MVAIMPS3;
          ,MVAIMPS4 = this.w_MVAIMPS4;
          ,MVAIMPS5 = this.w_MVAIMPS5;
          ,MVAIMPS6 = this.w_MVAIMPS6;
          ,MVASPEST = this.oParentObject.w_MVASPEST;
          ,MVCODAG2 = this.w_MVCODAG2;
          ,MVDATDIV = this.w_MVDATDIV;
          ,MVDATTRA = this.oParentObject.w_MVDATTRA;
          ,MVDESDOC = " ";
          ,MVFLOFFE = "S";
          ,MVIMPARR = this.w_MVIMPARR;
          ,MVIVABOL = this.w_MVIVABOL;
          ,MVMINTRA = this.oParentObject.w_MVMINTRA;
          ,MVORATRA = this.oParentObject.w_MVORATRA;
          ,MV__NOTE = this.w_MV__NOTE;
          ,MVFLVABD = this.w_MVFLVABD;
          ,MVFLSCOM = this.w_MVFLSCOM;
          ,MVNUMCOR = this.w_MVNUMCOR;
          ,UTCC = i_CODUTE;
          ,UTDC = SetInfoDate( g_CALUTD );
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0472B0E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVDESART"+",MVDESSUP"+",MVUNIMIS"+",MVCATCON"+",MVCONTRO"+",MVCAUMAG"+",MVCODCLA"+",MVCONTRA"+",MVCODLIS"+",MVQTAMOV"+",MVQTAUM1"+",MVPREZZO"+",MVSCONT1"+",MVSCONT2"+",MVSCONT3"+",MVSCONT4"+",MVFLOMAG"+",MVCODIVA"+",MVCONIND"+",MVVALRIG"+",MVIMPACC"+",MVIMPSCO"+",MVVALMAG"+",MVIMPNAZ"+",MVPERPRO"+",MVIMPPRO"+",MVPESNET"+",MVNOMENC"+",MVMOLSUP"+",MVNUMCOL"+",MVFLTRAS"+",MVFLRAGG"+",MVSERRIF"+",MVROWRIF"+",MVFLARIF"+",MVQTASAL"+",MVFLLOTT"+",MVF2LOTT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DOC_DETT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DOC_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCONTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPERPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVNUMCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2LOTT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.oParentObject.w_MVSERIAL,'CPROWNUM',this.w_CPROWNUM,'MVNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_CPROWORD,'MVTIPRIG',this.w_MVTIPRIG,'MVCODICE',this.w_MVCODICE,'MVCODART',this.w_MVCODART,'MVDESART',this.w_MVDESART,'MVDESSUP',this.w_MVDESSUP,'MVUNIMIS',this.w_MVUNIMIS,'MVCATCON',this.w_MVCATCON,'MVCONTRO',SPACE(15))
      insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVDESART,MVDESSUP,MVUNIMIS,MVCATCON,MVCONTRO,MVCAUMAG,MVCODCLA,MVCONTRA,MVCODLIS,MVQTAMOV,MVQTAUM1,MVPREZZO,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVFLOMAG,MVCODIVA,MVCONIND,MVVALRIG,MVIMPACC,MVIMPSCO,MVVALMAG,MVIMPNAZ,MVPERPRO,MVIMPPRO,MVPESNET,MVNOMENC,MVMOLSUP,MVNUMCOL,MVFLTRAS,MVFLRAGG,MVSERRIF,MVROWRIF,MVFLARIF,MVQTASAL,MVFLLOTT,MVF2LOTT &i_ccchkf. );
         values (;
           this.oParentObject.w_MVSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_MVNUMRIF;
           ,this.w_CPROWORD;
           ,this.w_MVTIPRIG;
           ,this.w_MVCODICE;
           ,this.w_MVCODART;
           ,this.w_MVDESART;
           ,this.w_MVDESSUP;
           ,this.w_MVUNIMIS;
           ,this.w_MVCATCON;
           ,SPACE(15);
           ,this.w_MVCAUMAG;
           ,this.w_MVCODCLA;
           ,this.w_MVCONTRA;
           ,this.w_MVCODLIS;
           ,this.w_MVQTAMOV;
           ,this.w_MVQTAUM1;
           ,this.w_MVPREZZO;
           ,this.w_MVSCONT1;
           ,this.w_MVSCONT2;
           ,this.w_MVSCONT3;
           ,this.w_MVSCONT4;
           ,this.w_MVFLOMAG;
           ,this.w_MVCODIVA;
           ,this.w_MVCONIND;
           ,this.w_MVVALRIG;
           ,this.w_MVIMPACC;
           ,this.w_MVIMPSCO;
           ,this.w_MVVALMAG;
           ,this.w_MVIMPNAZ;
           ,0;
           ,0;
           ,this.w_MVPESNET;
           ,this.w_MVNOMENC;
           ,this.w_MVMOLSUP;
           ,0;
           ,this.w_MVFLTRAS;
           ,this.oParentObject.w_MVTFRAGG;
           ,SPACE(10);
           ,0;
           ," ";
           ,this.w_MVQTAUM1;
           ," ";
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0472BCB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Valorizzo il flag 'Flag riga riaddebita spese', in base alla tipologia del documento da creare. Se ordine � sempre vuoto, altrimenti assegno il valore di default definito in analisi
    this.w_MV_FLAGG = IIF(this.w_MVCLADOC="OR"," ","N")
    this.w_DATOAI = IIF(this.w_MVCLADOC="DI",this.oParentObject.w_MVDATREG,i_DATSYS)
    if g_PERUBI="S"
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGFLUBIC"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_LCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGFLUBIC;
          from (i_cTable) where;
              MGCODMAG = this.w_LCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_MVFLLOTT = IIF((g_PERLOT="S" AND this.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.w_FLUBIC="S"), LEFT(ALLTRIM(this.w_MVFLCASC)+IIF(this.w_MVFLRISE="+", "-", IIF(this.w_MVFLRISE="-", "+", " ")), 1), " ")
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCAUCOL');
      +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
      +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
      +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
      +",MVCODLOT ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVCODLOT');
      +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_LCODMAG),'DOC_DETT','MVCODMAG');
      +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
      +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVCODUB2');
      +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVCODUBI');
      +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVDATEVA),'DOC_DETT','MVDATEVA');
      +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2CASC');
      +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2IMPE');
      +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2ORDI');
      +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVF2RISE');
      +",MVFLARIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
      +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MFLCASC),'DOC_DETT','MVFLCASC');
      +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MFLELGM),'DOC_DETT','MVFLELGM');
      +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLIMPE),'DOC_DETT','MVFLIMPE');
      +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MFLORDI),'DOC_DETT','MVFLORDI');
      +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
      +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLRISE),'DOC_DETT','MVFLRISE');
      +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
      +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
      +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
      +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
      +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
      +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
      +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
      +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
      +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
      +",MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
      +",MVPERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
      +",MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
      +",MVPROCAP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPROCAP),'DOC_DETT','MVPROCAP');
      +",MVCACONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCACONT),'DOC_DETT','MVCACONT');
      +",MVRIFCAC ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIFCAC),'DOC_DETT','MVRIFCAC');
      +",MV_FLAGG ="+cp_NullLink(cp_ToStrODBC(this.w_MV_FLAGG),'DOC_DETT','MV_FLAGG');
      +",MVDATOAI ="+cp_NullLink(cp_ToStrODBC(this.w_DATOAI),'DOC_DETT','MVDATOAI');
      +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
             )
    else
      update (i_cTable) set;
          MVCAUCOL = SPACE(5);
          ,MVCAUMAG = this.w_MVCAUMAG;
          ,MVCODATT = this.w_MVCODATT;
          ,MVCODCEN = this.w_MVCODCEN;
          ,MVCODCOM = this.w_MVCODCOM;
          ,MVCODLOT = SPACE(20);
          ,MVCODMAG = this.w_LCODMAG;
          ,MVCODMAT = SPACE(5);
          ,MVCODUB2 = SPACE(20);
          ,MVCODUBI = SPACE(20);
          ,MVDATEVA = this.oParentObject.w_MVDATEVA;
          ,MVF2CASC = " ";
          ,MVF2IMPE = " ";
          ,MVF2ORDI = " ";
          ,MVF2RISE = " ";
          ,MVFLARIF = " ";
          ,MVFLCASC = this.w_MFLCASC;
          ,MVFLELGM = this.w_MFLELGM;
          ,MVFLERIF = " ";
          ,MVFLEVAS = " ";
          ,MVFLIMPE = this.w_MFLIMPE;
          ,MVFLORDI = this.w_MFLORDI;
          ,MVFLRIPA = " ";
          ,MVFLRISE = this.w_MFLRISE;
          ,MVIMPEVA = 0;
          ,MVKEYSAL = this.w_MVKEYSAL;
          ,MVQTAEV1 = 0;
          ,MVQTAEVA = 0;
          ,MVTIPATT = this.w_MVTIPATT;
          ,MVVOCCEN = this.w_MVVOCCEN;
          ,MVQTAIMP = 0;
          ,MVQTAIM1 = 0;
          ,MV_SEGNO = this.oParentObject.w_MV_SEGNO;
          ,MVIMPCOM = this.w_MVIMPCOM;
          ,MVTIPPRO = this.w_MVTIPPRO;
          ,MVPERPRO = this.w_MVPERPRO;
          ,MVTIPPR2 = this.w_MVTIPPR2;
          ,MVPROCAP = this.w_MVPROCAP;
          ,MVCACONT = this.w_MVCACONT;
          ,MVRIFCAC = this.w_MVRIFCAC;
          ,MV_FLAGG = this.w_MV_FLAGG;
          ,MVDATOAI = this.w_DATOAI;
          ,MVFLLOTT = this.w_MVFLLOTT;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_MVSERIAL;
          and CPROWNUM = this.w_CPROWNUM;
          and MVNUMRIF = this.w_MVNUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_MVKEYSAL = IIF(this.oParentObject.w_MVFLPROV="N" AND this.w_MVTIPRIG="R" AND this.w_MFLAVAL<>"N" And Not Empty(Alltrim(this.w_MVFLORDI+this.w_MVFLIMPE+this.w_MVFLCASC+this.w_MVFLRISE)), this.w_MVCODART, SPACE(20))
    this.w_LCODMAG = IIF(this.oParentObject.w_MVFLPROV="N" AND EMPTY(this.w_MVKEYSAL), SPACE(5), this.w_LCODMAG)
    return
  proc Try_0473E8E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MVKEYSAL,'SLCODMAG',this.w_LCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_LCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SALDIART'
      return
    endif
    return
  proc Try_04740C20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MVKEYSAL,'SCCODMAG',this.w_LCODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_LCODMAG;
           ,this.w_COMMAPPO;
           ,this.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04746DA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_RATE
    i_nConn=i_TableProp[this.DOC_RATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DOC_RATE','RSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSNUMRAT),'DOC_RATE','RSNUMRAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSDATRAT),'DOC_RATE','RSDATRAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSIMPRAT),'DOC_RATE','RSIMPRAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RSMODPAG),'DOC_RATE','RSMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_RATE','RSFLSOSP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.oParentObject.w_MVSERIAL,'RSNUMRAT',this.w_RSNUMRAT,'RSDATRAT',this.w_RSDATRAT,'RSIMPRAT',this.w_RSIMPRAT,'RSMODPAG',this.w_RSMODPAG,'RSFLSOSP'," ")
      insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP &i_ccchkf. );
         values (;
           this.oParentObject.w_MVSERIAL;
           ,this.w_RSNUMRAT;
           ,this.w_RSDATRAT;
           ,this.w_RSIMPRAT;
           ,this.w_RSMODPAG;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04724608()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into OFF_ERTE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OFRIFDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'OFF_ERTE','OFRIFDOC');
      +",OFSTATUS ="+cp_NullLink(cp_ToStrODBC("C"),'OFF_ERTE','OFSTATUS');
      +",OFDATCHI ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'OFF_ERTE','OFDATCHI');
          +i_ccchkf ;
      +" where ";
          +"OFSERIAL = "+cp_ToStrODBC(this.w_OFSERIAL);
             )
    else
      update (i_cTable) set;
          OFRIFDOC = this.oParentObject.w_MVSERIAL;
          ,OFSTATUS = "C";
          ,OFDATCHI = i_DATSYS;
          &i_ccchkf. ;
       where;
          OFSERIAL = this.w_OFSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripartisce Sconti
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se Documento non Provvisorio controllo Fido
    if this.oParentObject.w_MVFLPROV="N" And g_PERFID="S" AND this.oParentObject.w_FLFIDO="S" AND this.oParentObject.w_FLRISC$"SD" AND this.oParentObject.w_MVTIPCON="C" AND NOT EMPTY(this.w_MVCODCON) AND this.w_OK
      * --- Controllo del Rischio
      * --- Storno l'acconto se contabilizzato..
      *     (per ora questo accade sempre visto che questo codice scatta solo alla
      *     conferma del caricamento)
      this.w_TOTDOC = this.w_TOTFATTU 
      * --- w_SPEBOL sono presenti solo in fattura negli altri casi sono zero
      this.w_SPEBOL = this.w_MVSPEBOL
      if this.w_MVCODVAL<>this.w_MVVALNAZ
        * --- Se il Documento e' in Valuta Converte in Moneta di Conto
        this.w_TOTDOC = VAL2MON(this.w_TOTDOC, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ,this.w_DECTOT)
        this.w_SPEBOL = VAL2MON(this.w_SPEBOL, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_DECTOT)
      endif
      * --- Toglie Totale Documento
      this.w_FIDRES = this.oParentObject.w_MAXFID - this.w_TOTDOC
      this.w_FID1 = 0
      this.w_FID2 = 0
      this.w_FID3 = 0
      this.w_FID4 = 0
      this.w_FID5 = 0
      this.w_FID6 = 0
      this.w_FIDD = CTOD("  -  -  ")
      * --- Read from SIT_FIDI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2],.t.,this.SIT_FIDI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT,FIDATELA"+;
          " from "+i_cTable+" SIT_FIDI where ";
              +"FICODCLI = "+cp_ToStrODBC(this.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT,FIDATELA;
          from (i_cTable) where;
              FICODCLI = this.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FID1 = NVL(cp_ToDate(_read_.FIIMPPAP),cp_NullValue(_read_.FIIMPPAP))
        this.w_FID2 = NVL(cp_ToDate(_read_.FIIMPESC),cp_NullValue(_read_.FIIMPESC))
        this.w_FID3 = NVL(cp_ToDate(_read_.FIIMPESO),cp_NullValue(_read_.FIIMPESO))
        this.w_FID4 = NVL(cp_ToDate(_read_.FIIMPORD),cp_NullValue(_read_.FIIMPORD))
        this.w_FID5 = NVL(cp_ToDate(_read_.FIIMPDDT),cp_NullValue(_read_.FIIMPDDT))
        this.w_FID6 = NVL(cp_ToDate(_read_.FIIMPFAT),cp_NullValue(_read_.FIIMPFAT))
        this.w_FIDD = NVL(cp_ToDate(_read_.FIDATELA),cp_NullValue(_read_.FIDATELA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Toglie Totale Altri Documenti
      * --- Anagrafica Fido gia presente ?
      this.w_ANFIDO = i_Rows<>0
      if this.w_ANFIDO
        this.w_FIDRES = this.w_FIDRES - (this.w_FID1+this.w_FID2+this.w_FID3+this.w_FID4+this.w_FID5+this.w_FID6)
      endif
      if this.w_FIDRES<0 AND this.oParentObject.w_FLCRIS="S"
        this.w_oPart = this.w_oMess.AddMsgPartNL("Superato massimo importo fido cliente%0Residuo: %1 alla data elaborazione %2")
        this.w_oPart.AddParam(ALLTRIM(STR(this.w_FIDRES,18+this.w_DECTOT,this.w_DECTOT)))     
        this.w_oPart.AddParam(DTOC(this.w_FIDD))     
        if this.oParentObject.w_FLBLVE="S"
          this.w_OK = .F.
          this.w_MESS = this.w_oMess.ComposeMessage()
        else
          this.w_oMess.AddMsgPartNL("Confermi ugualmente?")     
          this.w_OK = this.w_oMess.ah_YesNo()
          this.w_MESS = Ah_MsgFormat("Transazione abbandonata")
        endif
      endif
      if this.w_OK AND this.w_MVFLVEAC="V" AND this.w_MVCLADOC="OR" AND this.oParentObject.w_MAXORD<>0 AND this.w_TOTDOC>this.oParentObject.w_MAXORD AND this.oParentObject.w_FLCRIS="S"
        * --- Massimo Ordinabile
        this.w_oPart = this.w_oMess.AddMsgPartNL("Superato massimo ordinabile cliente%0Max. ordinabile: %1")
        this.w_oPart.AddParam(ALLTRIM(STR(this.oParentObject.w_MAXORD,18+this.w_DECTOT,this.w_DECTOT)))     
        if this.oParentObject.w_FLBLVE="S"
          this.w_OK = .F.
          this.w_MESS = this.w_oMess.ComposeMessage()
        else
          this.w_oMess.AddMsgPartNL("Confermi ugualmente?")     
          this.w_OK = this.w_oMess.ah_YesNo()
          this.w_MESS = Ah_MsgFormat("Transazione abbandonata")
        endif
      endif
    endif
    * --- Aggiorna Rischio relativo al Documento
    if this.oParentObject.w_FLRISC$"SD" AND this.w_OK
      this.w_FID4 = cp_ROUND(this.w_TOTDOC-this.w_SPEBOL ,this.w_DECTOT)
      this.w_FID4 = IIF(this.oParentObject.w_FLRISC="S",1,IIF(this.oParentObject.w_FLRISC="D",-1,0)) * this.w_FID4
      * --- Aggiorno l'anagrafica Fido
      * --- Try
      local bErr_047F2F50
      bErr_047F2F50=bTrsErr
      this.Try_047F2F50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_047F2F50
      * --- End
    endif
  endproc
  proc Try_047F2F50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Not this.w_ANFIDO
      * --- Insert into SIT_FIDI
      i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SIT_FIDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"FICODCLI"+",FIIMPORD"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'SIT_FIDI','FICODCLI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FID4),'SIT_FIDI','FIIMPORD');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'FICODCLI',this.w_MVCODCON,'FIIMPORD',this.w_FID4)
        insert into (i_cTable) (FICODCLI,FIIMPORD &i_ccchkf. );
           values (;
             this.w_MVCODCON;
             ,this.w_FID4;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Write into SIT_FIDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SIT_FIDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FIIMPORD =FIIMPORD+ "+cp_ToStrODBC(this.w_FID4);
            +i_ccchkf ;
        +" where ";
            +"FICODCLI = "+cp_ToStrODBC(this.w_MVCODCON);
               )
      else
        update (i_cTable) set;
            FIIMPORD = FIIMPORD + this.w_FID4;
            &i_ccchkf. ;
         where;
            FICODCLI = this.w_MVCODCON;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato il cursore del dettaglio esplode riga per riga i contributi di riga e
    *     sul cursore determina il dettaglio per l'esplosione a peso
    Select GeneApp 
 Go Top
    do while Not Eof( "GeneApp" )
      * --- L'ultimo carattere contiene il check contributi accessori dell'articolo
      if GeneApp.t_ARFLAPCA="S"
        this.w_RIFCACESP = Recno( "GeneApp" )
        * --- Come riferimento passo il numero di riga dell'articolo che provoca
        *     l'esplosione
        this.w_UNIMIS = GeneApp.t_MVUNIMIS
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMFLFRAZ"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMFLFRAZ;
            from (i_cTable) where;
                UMCODICE = this.w_UNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        GSAR_BEG(this,GeneApp.t_MVCODART, this.oParentObject.w_MVTIPCON, this.w_MVCODCON , this.w_MVDATDOC, GeneApp.t_MVCODIVA,GeneApp.t_MVUNIMIS ,GeneApp.t_MVQTAMOV, GeneApp.t_MVPREZZO, this.w_MVCODVAL, IIF(this.w_MVFLSCOR="S" ,GeneApp.t_PERIVA ,0), this.w_COFRAZ, "ACC", "CURESPL")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Used( "CURESPL")
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      Select GeneApp 
 Skip
    enddo
    if this.w_CATDOC="FA" Or this.w_CATDOC="NC"
      * --- Esplosione a peso, solo per fatture / note di credito
      *     (da offerte ad oggi non possibile...)
      this.w_RIFCACESP = -1
      * --- Alla routine devo passare un cursore che contiene il dettaglio
      *     del documento appena caricato
      GSAR_BGA(this,"GEN", This , 0 , 0 , 10 , "I")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Used("CurEspl")
      Select CurEspl 
 Go top
      this.w_POSAPP = RecNo("GeneApp")
      this.w_CPROWORD = 0
      do while (.not. eof("CurEspl"))
        * --- RIcostruisco tutti i dati di riga da quanto restituito dal cursore di esplosione..
        this.w_KCODICE = CURESPL.CODICE
        this.w_KUNIMIS = CURESPL.UNIMIS
        this.w_KQTAMOV = CURESPL.QTAMOV
        this.w_KPREZZO = CURESPL.PREZZO
        this.w_KCODIVA = CURESPL.CODIVA
        this.w_CONTRIB = CURESPL.CATCON
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CA__TIPO,CACODART,CADESSUP,CADESART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_KCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CA__TIPO,CACODART,CADESSUP,CADESART;
            from (i_cTable) where;
                CACODICE = this.w_KCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
          this.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          this.w_MVDESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
          this.w_MVDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARNOMENC,ARUMSUPP,ARMOLSUP,ARPESNET,ARCATCON,ARCODCLA,ARVOCRIC"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARNOMENC,ARUMSUPP,ARMOLSUP,ARPESNET,ARCATCON,ARCODCLA,ARVOCRIC;
            from (i_cTable) where;
                ARCODART = this.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVNOMENC = NVL(cp_ToDate(_read_.ARNOMENC),cp_NullValue(_read_.ARNOMENC))
          this.w_MVUMSUPP = NVL(cp_ToDate(_read_.ARUMSUPP),cp_NullValue(_read_.ARUMSUPP))
          this.w_MVMOLSUP = NVL(cp_ToDate(_read_.ARMOLSUP),cp_NullValue(_read_.ARMOLSUP))
          this.w_MVPESNET = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
          this.w_MVCATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
          this.w_MVCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
          this.w_MVVOCCOS = NVL(cp_ToDate(_read_.ARVOCRIC),cp_NullValue(_read_.ARVOCRIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIND,IVPERIVA,IVBOLIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_KCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIND,IVPERIVA,IVBOLIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_KCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_BOLIVA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVVALRIG = CAVALRIG(this.w_KPREZZO,this.w_KQTAMOV, 0,0,0,0,this.w_DECTOT)
        this.w_MVVALMAG = CAVALMAG(this.w_MVFLSCOR, this.w_MVVALRIG, 0, 0, this.w_PERIVA, this.w_DECTOT, this.w_MVCODIVE, this.w_PERIVE )
        this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_MVCODIVE, this.w_PERIVE, this.w_INDIVE, this.w_PERIVA, this.w_INDIVA )
        this.w_MVFLTRAS = " "
        this.w_MVCODCEN = Space(15)
        this.w_MVVOCCEN = Space(15)
        this.w_MVCODCOM = Space(15)
        this.w_MVFLORCO = " "
        this.w_MVTIPPRO = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
        this.w_MVIMPCOM = 0
        this.w_MVTIPPR2 = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
         
 INSERT INTO GeneApp ; 
 (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ; 
 t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ; 
 t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ; 
 t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ; 
 t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ; 
 t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG,; 
 t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ; 
 t_MVIMPACC, t_MVIMPSCO , t_MVVALMAG , t_MVIMPNAZ, ; 
 t_MVCODMAG, t_MVCODCEN, t_MVVOCCOS, ; 
 t_MVCODCOM, t_MVTIPATT, t_MVCODATT, t_MVDATEVA, t_INDIVA,; 
 t_FLSERA,t_MVIMPCOM, t_MVIMPAC2, t_CPROWORD, ; 
 t_MVTIPPRO, t_MVPERPRO, t_MVTIPPR2, t_MVPROCAP, t_ARFLAPCA, t_CPROWNUM , t_MVCACONT, t_MVFLORCO ) ; 
 VALUES ; 
 ( this.w_MVTIPRIG, this.w_KCODICE, this.w_MVCODART, ; 
 this.w_MVDESART, this.w_MVDESSUP, this.w_KUNIMIS, this.w_MVCATCON, ; 
 this.w_MVCODCLA, Space(15), Space(5), ; 
 this.w_KQTAMOV, this.w_KQTAMOV , this.w_KPREZZO, ; 
 0, 0, 0, 0, ; 
 "X", this.w_KCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ; 
 0, Space(8) , Space(3), 0, ; 
 0,0, this.w_MVVALMAG, this.w_MVIMPNAZ,; 
 this.oParentObject.w_MVCODMAG , this.w_MVCODCEN, this.w_MVVOCCOS, ; 
 this.w_MVCODCOM, "A" , Space(15), this.oParentObject.w_MVDATEVA, this.w_INDIVA,; 
 this.w_FLSERA, this.w_MVIMPCOM, 0 , this.w_CPROWORD , ; 
 this.w_MVTIPPRO,0 ,this.w_MVTIPPR2,0, this.w_ARFLAPCA, this.w_RIFCACESP , this.w_CONTRIB, this.w_MVFLORCO )
        this.w_CPROWORD = this.w_CPROWORD + 1
        Select CurEspl
        if Not Eof("CurEspl")
          Skip
        endif
      enddo
      USE IN SELECT("CurEspl")
      Select "GeneApp" 
 Go ( this.w_POSAPP )
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,22)]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='BAN_CONTI'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='DES_DIVE'
    this.cWorkTables[8]='DOC_DETT'
    this.cWorkTables[9]='DOC_MAST'
    this.cWorkTables[10]='DOC_RATE'
    this.cWorkTables[11]='KEY_ARTI'
    this.cWorkTables[12]='LISTINI'
    this.cWorkTables[13]='MAGAZZIN'
    this.cWorkTables[14]='OFF_ERTE'
    this.cWorkTables[15]='PAR_PROV'
    this.cWorkTables[16]='SALDIART'
    this.cWorkTables[17]='SIT_FIDI'
    this.cWorkTables[18]='TIP_DOCU'
    this.cWorkTables[19]='UNIMIS'
    this.cWorkTables[20]='VALUTE'
    this.cWorkTables[21]='VOCIIVA'
    this.cWorkTables[22]='SALDICOM'
    return(this.OpenAllTables(22))

  proc CloseCursors()
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
