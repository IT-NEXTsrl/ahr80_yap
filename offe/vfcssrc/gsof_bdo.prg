* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bdo                                                        *
*              Prepara documento di offerta                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_61]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-22                                                      *
* Last revis.: 2014-03-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bdo",oParentObject)
return(i_retval)

define class tgsof_bdo as StdBatch
  * --- Local variables
  w_OFSERIAL = space(10)
  w_OFNUMDOC = 0
  w_OFSERDOC = space(10)
  w_OFCODVAL = space(3)
  w_OFDATDOC = ctod("  /  /  ")
  w_MVSERIAL = space(10)
  w_MVTIPDOC = space(5)
  w_MVCLADOC = space(2)
  w_MVANNPRO = space(4)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVPRP = space(2)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVPRD = space(2)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVTCAMAG = space(5)
  w_MVTIPCON = space(1)
  w_MVANNDOC = space(4)
  w_MVTFRAGG = space(1)
  w_MVCODESE = space(4)
  w_MVFLPROV = space(1)
  w_MVCODUTE = 0
  w_MVCAUCON = space(5)
  w_MVFLACCO = space(1)
  w_MVNUMREG = 0
  w_NUMSCO = 0
  w_MVFLINTE = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVDATEVA = ctod("  /  /  ")
  w_MVCODMAG = space(5)
  w_MVASPEST = space(20)
  w_MVDATTRA = ctod("  /  /  ")
  w_MVORATRA = space(2)
  w_MVMINTRA = space(2)
  w_MV_SEGNO = space(1)
  w_FLDTRC = space(1)
  w_MVDATRCO = ctod("  /  /  ")
  w_OK = .f.
  w_OKGEN = .f.
  w_OFCODNOM = space(15)
  w_OFCODMOD = space(5)
  w_CCCODICE = space(15)
  w_CCDESCRI = space(40)
  w_CCDESCR2 = space(40)
  w_CCINDIRI = space(35)
  w_CCINDIR2 = space(35)
  w_CC___CAP = space(8)
  w_CCLOCALI = space(30)
  w_CCPROVIN = space(2)
  w_CCNAZION = space(3)
  w_CCCODFIS = space(16)
  w_CCPARIVA = space(12)
  w_CCTELEFO = space(18)
  w_CCTELFAX = space(18)
  w_CCINDWEB = space(50)
  w_CC_EMAIL = space(50)
  w_CCDTINVA = ctod("  /  /  ")
  w_CCDTOBSO = ctod("  /  /  ")
  w_CCCODZON = space(3)
  w_CCCODLIN = space(3)
  w_CCCODVAL = space(3)
  w_CCCODAGE = space(5)
  w_ANCODICE = space(15)
  w_ANTIPCON = space(1)
  w_CODICE = space(15)
  w_NOTIFICA = space(1)
  w_AUTN = 0
  w_CODN = space(15)
  w_LENC = 0
  w_OLDSTA = space(1)
  w_OLDSTAT = space(1)
  w_CODPAG = space(5)
  w_OKDATDIV = .f.
  w_OFCODLIS = space(5)
  w_OFSCOLIS = space(5)
  w_OFPROLIS = space(5)
  w_OFPROSCO = space(5)
  w_CCMASCON = space(15)
  w_CCCATCON = space(5)
  w_CCCODSAL = space(5)
  w_CCFLRITE = space(1)
  w_CCCODCAT = space(4)
  w_CCCODPAG = space(5)
  w_NONUMLIS = space(5)
  w_NOCATCOM = space(3)
  w_NOCODBAN = space(10)
  w_NOCODBA2 = space(15)
  w_FLRISC = space(1)
  w_FLCRIS = space(1)
  w_FLMGPR = space(1)
  w_MAGDOC = space(5)
  w_OMAG = space(5)
  w_NOTIPNOM = space(1)
  w_NOTIPCLI = space(1)
  w_MSGERR = space(50)
  w_FLFIDO = space(1)
  w_FLBLVE = space(1)
  w_MAXFID = 0
  w_MAXORD = 0
  w_ANFLGCON = space(1)
  * --- WorkFile variables
  OFF_ERTE_idx=0
  MOD_OFFE_idx=0
  OFF_NOMI_idx=0
  CONTI_idx=0
  PAG_2AME_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Preparazione del Documento di Offerta da generare (da GSOF_AOF)
    this.w_LENC = MIN(LEN(ALLTRIM(p_CLI)), 15)
    this.w_OK = .F.
    this.w_MVSERIAL = SPACE(10)
    this.w_OFCODNOM = SPACE(15)
    this.w_OFCODMOD = SPACE(5)
    this.w_ANTIPCON = "C"
    this.w_OFSERIAL = this.oParentObject.w_OFSERIAL
    this.w_OFNUMDOC = this.oParentObject.w_OFNUMDOC
    this.w_OFSERDOC = this.oParentObject.w_OFSERDOC
    this.w_OFDATDOC = this.oParentObject.w_OFDATDOC
    this.w_OLDSTAT = this.oParentObject.w_OLDSTAT
    this.w_CODPAG = this.oParentObject.w_OFCODPAG
    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
      this.w_OFCODLIS = this.oParentObject.w_OFCODLIS
      this.w_OFSCOLIS = this.oParentObject.w_OFSCOLIS
      this.w_OFPROLIS = this.oParentObject.w_OFPROLIS
      this.w_OFPROSCO = this.oParentObject.w_OFPROSCO
      this.w_OFCODVAL = this.oParentObject.w_OFCODVAL
    endif
    this.w_OLDSTA = " "
    this.w_OKGEN = .F.
    this.w_FLDTRC = "N"
    if NOT EMPTY(this.w_OFSERIAL)
      * --- Read from OFF_ERTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OFCODNOM,OFCODMOD,OFSTATUS"+;
          " from "+i_cTable+" OFF_ERTE where ";
              +"OFSERIAL = "+cp_ToStrODBC(this.w_OFSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OFCODNOM,OFCODMOD,OFSTATUS;
          from (i_cTable) where;
              OFSERIAL = this.w_OFSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OFCODNOM = NVL(cp_ToDate(_read_.OFCODNOM),cp_NullValue(_read_.OFCODNOM))
        this.w_OFCODMOD = NVL(cp_ToDate(_read_.OFCODMOD),cp_NullValue(_read_.OFCODMOD))
        this.w_OLDSTA = NVL(cp_ToDate(_read_.OFSTATUS),cp_NullValue(_read_.OFSTATUS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from MOD_OFFE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MOCODCAU,MOFLPROV"+;
          " from "+i_cTable+" MOD_OFFE where ";
              +"MOCODICE = "+cp_ToStrODBC(this.w_OFCODMOD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MOCODCAU,MOFLPROV;
          from (i_cTable) where;
              MOCODICE = this.w_OFCODMOD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MOCODCAU),cp_NullValue(_read_.MOCODCAU))
        this.w_MVFLPROV = NVL(cp_ToDate(_read_.MOFLPROV),cp_NullValue(_read_.MOFLPROV))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if UPPER(g_APPLICATION)="AD HOC ENTERPRISE" 
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLRISC,TDFLCRIS,TDCODMAG"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLRISC,TDFLCRIS,TDCODMAG;
            from (i_cTable) where;
                TDTIPDOC = this.w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLRISC = NVL(cp_ToDate(_read_.TDFLRISC),cp_NullValue(_read_.TDFLRISC))
          this.w_FLCRIS = NVL(cp_ToDate(_read_.TDFLCRIS),cp_NullValue(_read_.TDFLCRIS))
          this.w_MAGDOC = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLRISC,TDFLCRIS,TDFLMGPR,TDCODMAG"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLRISC,TDFLCRIS,TDFLMGPR,TDCODMAG;
            from (i_cTable) where;
                TDTIPDOC = this.w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLRISC = NVL(cp_ToDate(_read_.TDFLRISC),cp_NullValue(_read_.TDFLRISC))
          this.w_FLCRIS = NVL(cp_ToDate(_read_.TDFLCRIS),cp_NullValue(_read_.TDFLCRIS))
          this.w_FLMGPR = NVL(cp_ToDate(_read_.TDFLMGPR),cp_NullValue(_read_.TDFLMGPR))
          this.w_MAGDOC = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_MVFLPROV = IIF(this.w_MVFLPROV $ "SN", this.w_MVFLPROV, "N")
      if NOT EMPTY(this.w_OFCODMOD)
        if NOT EMPTY(this.w_OFCODNOM)
          this.w_CCCODICE = SPACE(15)
          * --- Read from OFF_NOMI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" OFF_NOMI where ";
                  +"NOCODICE = "+cp_ToStrODBC(this.w_OFCODNOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  NOCODICE = this.w_OFCODNOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CCDESCRI = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
            this.w_CCDESCR2 = NVL(cp_ToDate(_read_.NODESCR2),cp_NullValue(_read_.NODESCR2))
            this.w_CCINDIRI = NVL(cp_ToDate(_read_.NOINDIRI),cp_NullValue(_read_.NOINDIRI))
            this.w_CCINDIR2 = NVL(cp_ToDate(_read_.NOINDIR2),cp_NullValue(_read_.NOINDIR2))
            this.w_CC___CAP = NVL(cp_ToDate(_read_.NO___CAP),cp_NullValue(_read_.NO___CAP))
            this.w_CCLOCALI = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
            this.w_CCPROVIN = NVL(cp_ToDate(_read_.NOPROVIN),cp_NullValue(_read_.NOPROVIN))
            this.w_CCNAZION = NVL(cp_ToDate(_read_.NONAZION),cp_NullValue(_read_.NONAZION))
            this.w_CCCODFIS = NVL(cp_ToDate(_read_.NOCODFIS),cp_NullValue(_read_.NOCODFIS))
            this.w_CCPARIVA = NVL(cp_ToDate(_read_.NOPARIVA),cp_NullValue(_read_.NOPARIVA))
            this.w_CCTELEFO = NVL(cp_ToDate(_read_.NOTELEFO),cp_NullValue(_read_.NOTELEFO))
            this.w_CCTELFAX = NVL(cp_ToDate(_read_.NOTELFAX),cp_NullValue(_read_.NOTELFAX))
            this.w_CC_EMAIL = NVL(cp_ToDate(_read_.NO_EMAIL),cp_NullValue(_read_.NO_EMAIL))
            this.w_CCINDWEB = NVL(cp_ToDate(_read_.NOINDWEB),cp_NullValue(_read_.NOINDWEB))
            this.w_CCDTINVA = NVL(cp_ToDate(_read_.NODTINVA),cp_NullValue(_read_.NODTINVA))
            this.w_CCDTOBSO = NVL(cp_ToDate(_read_.NODTOBSO),cp_NullValue(_read_.NODTOBSO))
            this.w_CCCODZON = NVL(cp_ToDate(_read_.NOCODZON),cp_NullValue(_read_.NOCODZON))
            this.w_CCCODLIN = NVL(cp_ToDate(_read_.NOCODLIN),cp_NullValue(_read_.NOCODLIN))
            this.w_CCCODVAL = NVL(cp_ToDate(_read_.NOCODVAL),cp_NullValue(_read_.NOCODVAL))
            this.w_CCCODAGE = NVL(cp_ToDate(_read_.NOCODAGE),cp_NullValue(_read_.NOCODAGE))
            this.w_ANCODICE = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
            this.w_CCCODICE = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
            this.w_CCCODPAG = NVL(cp_ToDate(_read_.NOCODPAG),cp_NullValue(_read_.NOCODPAG))
            this.w_NONUMLIS = NVL(cp_ToDate(_read_.NONUMLIS),cp_NullValue(_read_.NONUMLIS))
            this.w_NOCATCOM = NVL(cp_ToDate(_read_.NOCATCOM),cp_NullValue(_read_.NOCATCOM))
            this.w_NOCODBAN = NVL(cp_ToDate(_read_.NOCODBAN),cp_NullValue(_read_.NOCODBAN))
            this.w_NOCODBA2 = NVL(cp_ToDate(_read_.NOCODBA2),cp_NullValue(_read_.NOCODBA2))
            this.w_NOTIPNOM = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
            this.w_NOTIPCLI = NVL(cp_ToDate(_read_.NOTIPCLI),cp_NullValue(_read_.NOTIPCLI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CCCODVAL = LEFT(this.w_CCCODVAL, 3)
          this.w_MSGERR = ""
          this.w_NOTIFICA = "S"
          if this.w_NOTIPCLI="F" AND ISALT()
            ah_errormsg("Nominativo di tipo fornitore: non ammesso")
          else
            if NOT EMPTY(this.w_CCCODICE)
              if NOT EMPTY(this.w_ANCODICE)
                this.w_CODICE = this.w_ANCODICE
                this.w_ANCODICE = SPACE(15)
                * --- Verifica Effettiva esistenza del Cliente
                * --- Read from CONTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ANCODICE,ANFLFIDO,ANMAXORD,ANVALFID,ANFLBLVE"+;
                    " from "+i_cTable+" CONTI where ";
                        +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                        +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ANCODICE,ANFLFIDO,ANMAXORD,ANVALFID,ANFLBLVE;
                    from (i_cTable) where;
                        ANTIPCON = this.w_ANTIPCON;
                        and ANCODICE = this.w_CODICE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_ANCODICE = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                  this.w_FLFIDO = NVL(cp_ToDate(_read_.ANFLFIDO),cp_NullValue(_read_.ANFLFIDO))
                  this.w_MAXORD = NVL(cp_ToDate(_read_.ANMAXORD),cp_NullValue(_read_.ANMAXORD))
                  this.w_MAXFID = NVL(cp_ToDate(_read_.ANVALFID),cp_NullValue(_read_.ANVALFID))
                  this.w_FLBLVE = NVL(cp_ToDate(_read_.ANFLBLVE),cp_NullValue(_read_.ANFLBLVE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if UPPER(g_APPLICATION)="AD HOC ENTERPRISE" AND NVL(this.w_FLBLVE," ")="S" AND g_PERFID="S"
                  this.w_MVFLPROV = "S"
                endif
                if UPPER(g_APPLICATION)="ADHOC REVOLUTION"
                  * --- Read from CONTI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.CONTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ANFLGCON"+;
                      " from "+i_cTable+" CONTI where ";
                          +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                          +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ANFLGCON;
                      from (i_cTable) where;
                          ANTIPCON = this.w_ANTIPCON;
                          and ANCODICE = this.w_CODICE;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_ANFLGCON = NVL(cp_ToDate(_read_.ANFLGCON),cp_NullValue(_read_.ANFLGCON))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                else
                  this.w_ANFLGCON = "N"
                endif
                if NVL(this.w_ANFLGCON," ")="S"
                  this.w_NOTIFICA = "N"
                  this.w_MSGERR = ah_msgFormat("%0Cliente con flag bloccato per contenzioso attivo")
                endif
              else
                * --- Generazione Nuovo Cliente
                * --- Se la generazione fallisce il campo notipnom viene ripristinato con il valore di w_NOTIPNOM
                * --- Write into OFF_NOMI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC("C"),'OFF_NOMI','NOTIPNOM');
                      +i_ccchkf ;
                  +" where ";
                      +"NOCODICE = "+cp_ToStrODBC(this.w_OFCODNOM);
                         )
                else
                  update (i_cTable) set;
                      NOTIPNOM = "C";
                      &i_ccchkf. ;
                   where;
                      NOCODICE = this.w_OFCODNOM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                this.w_ANCODICE = GSAR_BO1( THIS , this.w_OFCODNOM , this.w_NOTIPNOM , " " )
                * --- Notifica Se Aggiornamento Cliente OK
                if EMPTY( this.w_ANCODICE )
                  this.w_NOTIFICA = "N"
                endif
              endif
              if this.w_NOTIFICA="S"
                this.w_OKDATDIV = .F.
                * --- Select from PAG_2AME
                i_nConn=i_TableProp[this.PAG_2AME_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2],.t.,this.PAG_2AME_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select P2TIPSCA  from "+i_cTable+" PAG_2AME ";
                      +" where P2CODICE="+cp_ToStrODBC(this.w_CODPAG)+"";
                       ,"_Curs_PAG_2AME")
                else
                  select P2TIPSCA from (i_cTable);
                   where P2CODICE=this.w_CODPAG;
                    into cursor _Curs_PAG_2AME
                endif
                if used('_Curs_PAG_2AME')
                  select _Curs_PAG_2AME
                  locate for 1=1
                  do while not(eof())
                  * --- Controllo se nel Dettaglio del pagamento esiste almeno un record 
                  *     con Inizio Scadenza impostato a 'Data Diversa'
                  this.w_OKDATDIV = this.w_OKDATDIV Or _Curs_PAG_2AME.P2TIPSCA="DD"
                    select _Curs_PAG_2AME
                    continue
                  enddo
                  use
                endif
                * --- Apre Maschera di CONFERMA DOCUMENTO
                *     All'interno della maschera vengono letti anche i valori relativi alla causale documento
                this.w_OK = .F.
                * --- Ricalcolo le caller
                this.w_MVDATDOC = i_DATSYS
                this.w_MVDATEVA = this.w_MVDATDOC
                if !Isalt()
                  this.w_OMAG = IIF(EMPTY(this.w_MAGDOC), g_MAGAZI, this.w_MAGDOC)
                  if UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE"
                    this.w_MVCODMAG = CALCMAG(1, this.w_FLMGPR, "     ", this.w_MAGDOC, this.w_OMAG, "", "")
                  endif
                  do GSOF_KDO with this
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if this.w_OK=.T.
                    do GSOF_BGD with this
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
                      * --- Per Revolution OKGEN viene aggiornato in GSOF_BGD
                      this.w_OKGEN = .T.
                    endif
                  else
                    ah_errormsg("Generazione documento abbandonata")
                  endif
                else
                  if Not Empty(this.w_MVTIPDOC)
                    this.w_OKGEN = Empty(GSPR_BGD(This,this.w_OFSERIAL))
                  else
                    ah_errormsg("Generazione documento abbandonata")
                  endif
                endif
              else
                ah_errormsg("Generazione documento abbandonata%1", ,,this.w_MSGERR)
              endif
              if this.w_OKGEN=.F. AND this.w_OLDSTA="C"
                * --- Riporta allo Stato in Corso
                this.w_OLDSTA = IIF(EMPTY(this.w_OLDSTAT) OR this.w_OLDSTAT="C", "I", this.w_OLDSTAT)
                this.oParentObject.w_OFDATCHI = IIF( this.w_OLDSTA $ "VCSR",this.oParentObject.w_OFDATCHI,cp_CharToDate("  -  -  ") )
                * --- Write into OFF_ERTE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OFSTATUS ="+cp_NullLink(cp_ToStrODBC(this.w_OLDSTA),'OFF_ERTE','OFSTATUS');
                  +",OFDATCHI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFDATCHI),'OFF_ERTE','OFDATCHI');
                      +i_ccchkf ;
                  +" where ";
                      +"OFSERIAL = "+cp_ToStrODBC(this.w_OFSERIAL);
                         )
                else
                  update (i_cTable) set;
                      OFSTATUS = this.w_OLDSTA;
                      ,OFDATCHI = this.oParentObject.w_OFDATCHI;
                      &i_ccchkf. ;
                   where;
                      OFSERIAL = this.w_OFSERIAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                this.oParentObject.w_OFSTATUS = this.w_OLDSTA
              endif
            else
              ah_errormsg("Codice nominativo non definito")
            endif
          endif
        else
          ah_errormsg("Codice nominativo offerta non definito")
        endif
      else
        ah_errormsg("Modello di offerta non definito")
      endif
    else
      ah_errormsg("Offerta non definita")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='OFF_ERTE'
    this.cWorkTables[2]='MOD_OFFE'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='PAG_2AME'
    this.cWorkTables[6]='TIP_DOCU'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_PAG_2AME')
      use in _Curs_PAG_2AME
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
