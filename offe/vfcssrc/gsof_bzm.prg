* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bzm                                                        *
*              Interroga documento generato da offerta                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_32]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-22                                                      *
* Last revis.: 2005-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bzm",oParentObject)
return(i_retval)

define class tgsof_bzm as StdBatch
  * --- Local variables
  w_CLADOC = space(2)
  w_FLVEAC = space(1)
  w_PROG = .NULL.
  w_CPHK = space(10)
  w_ARCHIVIO = space(50)
  w_FRASE = space(900)
  iRows = 0
  w_NCONN = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  OFF_ERTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizza l'eventuale documento generato da una Offerta Confermata (da GSOF_AOF)
    this.w_CLADOC = "  "
    this.w_FLVEAC = " "
    if this.oParentObject.cFunction<>"Query"
      AH_ErrorMsg("Accesso consentito solo in interrogazione",48)
      i_retcode = 'stop'
      return
    endif
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVCLADOC,MVFLVEAC"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFRIFDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVCLADOC,MVFLVEAC;
        from (i_cTable) where;
            MVSERIAL = this.oParentObject.w_OFRIFDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
      this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows=0 OR EMPTY(this.w_CLADOC) OR EMPTY(this.w_FLVEAC)
      if ah_YesNo("Documento inesistente%0Elimino il riferimento?")
        this.oParentObject.w_OFSTATUS = IIF(this.oParentObject.w_OFSTATUS="C", "I", this.oParentObject.w_OFSTATUS)
        this.oParentObject.w_OFRIFDOC = SPACE(10)
        this.oParentObject.w_OFDATCHI = cp_CharToDate("  -  -    ")
        * --- Lettura cpccchk
        * --- Read from OFF_ERTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPCCCHK"+;
            " from "+i_cTable+" OFF_ERTE where ";
                +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPCCCHK;
            from (i_cTable) where;
                OFSERIAL = this.oParentObject.w_OFSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CPHK = NVL(cp_ToDate(_read_.CPCCCHK),cp_NullValue(_read_.CPCCCHK))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Write into OFF_ERTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OFRIFDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFRIFDOC),'OFF_ERTE','OFRIFDOC');
          +",OFSTATUS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSTATUS),'OFF_ERTE','OFSTATUS');
          +",OFDATCHI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFDATCHI),'OFF_ERTE','OFDATCHI');
              +i_ccchkf ;
          +" where ";
              +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                 )
        else
          update (i_cTable) set;
              OFRIFDOC = this.oParentObject.w_OFRIFDOC;
              ,OFSTATUS = this.oParentObject.w_OFSTATUS;
              ,OFDATCHI = this.oParentObject.w_OFDATCHI;
              &i_ccchkf. ;
           where;
              OFSERIAL = this.oParentObject.w_OFSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Ripristino cpccchk
        this.w_NCONN = i_TableProp[this.OFF_ERTE_idx,3]
        this.w_ARCHIVIO = cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
        this.w_FRASE = "UPDATE "+this.w_ARCHIVIO+" SET CPCCCHK =  '" + this.w_CPHK + "' where OFSERIAL = '" + this.oParentObject.w_OFSERIAL +"'"
        this.iRows = (cp_TrsSQL(this.w_NCONN,rtrim(this.w_FRASE)))
        this.oParentObject.mHideControls()
      endif
    else
      if upper(g_APPLICATION) <> "ADHOC REVOLUTION" 
        this.w_PROG = GSVE_MDV(this.w_FLVEAC+this.w_CLADOC)
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_MVSERIAL = this.oParentObject.w_OFRIFDOC
        this.w_PROG.QueryKeySet("MVSERIAL='" + this.oParentObject.w_OFRIFDOC + "'","")     
        * --- mi metto in interrogazione
        this.w_PROG.LoadRecWarn()     
      else
        gsar_bzm(this,this.oParentObject.w_OFRIFDOC,-20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='OFF_ERTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
