* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bs1                                                        *
*              Aggiornamento sezioni offerta                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-22                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bs1",oParentObject)
return(i_retval)

define class tgsof_bs1 as StdBatch
  * --- Local variables
  w_OFSERIAL = space(10)
  w_OFCODMOD = space(5)
  w_OK = .f.
  w_RECO = 0
  w_RIGHE = 0
  w_CODSEZ = space(10)
  * --- WorkFile variables
  MOD_SEZI_idx=0
  SEZ_OFFE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica le sezioni Offerta (da GSOF_MSO)
    this.w_OFSERIAL = this.oParentObject.oParentObject.w_OFSERIAL
    this.w_OFCODMOD = this.oParentObject.oParentObject.w_OFCODMOD
    this.w_OK = .T.
    this.w_RECO = RECCOUNT(this.oParentObject.cTrsName)
    if this.oParentObject.w_OSEZVAR<>"S" AND (this.w_RECO>1 OR (this.w_RECO=1 AND NOT EMPTY(this.oParentObject.w_OSCODSEZ)))
      this.w_OK = Isalt() or AH_YesNo("Acquisizione sezioni da modello selezionato: sovrascrivo sezioni gi� esistenti?")
    endif
    * --- Legge le sezioni del Modello Offerte 
    if this.w_OK=.T.
      this.w_RIGHE = 0
      SELECT (this.oParentObject.cTrsName)
      ZAP
      GO TOP
      * --- Select from MOD_SEZI
      i_nConn=i_TableProp[this.MOD_SEZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_SEZI_idx,2],.t.,this.MOD_SEZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOD_SEZI ";
            +" where MSCODICE = "+cp_ToStrODBC(this.w_OFCODMOD)+"";
            +" order by CPROWORD";
             ,"_Curs_MOD_SEZI")
      else
        select * from (i_cTable);
         where MSCODICE = this.w_OFCODMOD;
         order by CPROWORD;
          into cursor _Curs_MOD_SEZI
      endif
      if used('_Curs_MOD_SEZI')
        select _Curs_MOD_SEZI
        locate for 1=1
        do while not(eof())
        * --- Carica il Temporaneo dei Dati
        this.w_CODSEZ = NVL(_Curs_MOD_SEZI.MSCODSEZ,"")
        if NOT EMPTY(this.w_CODSEZ)
          SELECT (this.oParentObject.cTrsName)
          this.oParentObject.InitRow()
          this.oParentObject.w_OSCODSEZ = this.w_CODSEZ
          * --- Read from SEZ_OFFE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.SEZ_OFFE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SEZ_OFFE_idx,2],.t.,this.SEZ_OFFE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT,SO__NOTE"+;
              " from "+i_cTable+" SEZ_OFFE where ";
                  +"SOCODICE = "+cp_ToStrODBC(this.oParentObject.w_OSCODSEZ);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SODESCRI,SOTIPSEZ,SOFILGRU,SOFILSOT,SO__NOTE;
              from (i_cTable) where;
                  SOCODICE = this.oParentObject.w_OSCODSEZ;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_OSDESCRI = NVL(cp_ToDate(_read_.SODESCRI),cp_NullValue(_read_.SODESCRI))
            this.oParentObject.w_OSTIPSEZ = NVL(cp_ToDate(_read_.SOTIPSEZ),cp_NullValue(_read_.SOTIPSEZ))
            this.oParentObject.w_OSGRUP = NVL(cp_ToDate(_read_.SOFILGRU),cp_NullValue(_read_.SOFILGRU))
            this.oParentObject.w_OSSOTG = NVL(cp_ToDate(_read_.SOFILSOT),cp_NullValue(_read_.SOFILSOT))
            this.oParentObject.w_OS__NOTE = NVL(cp_ToDate(_read_.SO__NOTE),cp_NullValue(_read_.SO__NOTE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_DESTIPO = iif(this.oParentObject.w_ostipsez="D",ah_Msgformat("Descrittiva"),iif(this.oParentObject.w_ostipsez="W",ah_Msgformat("File W.P."),iif(this.oParentObject.w_ostipsez="A",ah_Msgformat("Articoli"),iif(this.oParentObject.w_ostipsez="G",ah_Msgformat("Gruppi"),iif(this.oParentObject.w_ostipsez="S",ah_Msgformat("Sottogruppi"),iif(this.oParentObject.w_ostipsez="T",ah_Msgformat("Dati fissi"),""))))))
          this.w_RIGHE = this.w_RIGHE + 1
          SELECT (this.oParentObject.cTrsName)
          this.oParentObject.TrsFromWork
        endif
          select _Curs_MOD_SEZI
          continue
        enddo
        use
      endif
      * --- Questa Parte derivata dal Metodo LoadRec
      SELECT (this.oParentObject.cTrsName)
      if this.w_RIGHE=0
        * --- Inserisce almeno una riga
        this.oParentObject.InitRow()
      endif
      GO TOP
      With this.oParentObject
      .WorkFromTrs()
      .SaveDependsOn()
      .SetControlsValue()
      .ChildrenChangeRow()
      .oPgFrm.Page1.oPag.oBody.Refresh()
      .oPgFrm.Page1.oPag.oBody.nAbsRow=1
      .oPgFrm.Page1.oPag.oBody.nRelRow=1
      EndWith
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOD_SEZI'
    this.cWorkTables[2]='SEZ_OFFE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_MOD_SEZI')
      use in _Curs_MOD_SEZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
