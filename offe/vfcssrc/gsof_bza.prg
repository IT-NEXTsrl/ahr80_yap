* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bza                                                        *
*              Lancia causali clienti                                          *
*                                                                              *
*      Author: Nunzio Iorio                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-28                                                      *
* Last revis.: 2002-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bza",oParentObject)
return(i_retval)

define class tgsof_bza as StdBatch
  * --- Local variables
  w_TDTIPDOC = space(5)
  w_TDCATDOC = space(2)
  w_PROG = .NULL.
  w_DXBTN = .f.
  * --- WorkFile variables
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia le causali vendite o causali ordini  (da GSOF_AMO ,zoom on zoom)
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_TDTIPDOC = Nvl(g_oMenu.oKey(1,3),"")
    else
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Causale
      this.w_TDTIPDOC = &cCurs..TDTIPDOC
    endif
    this.w_TDCATDOC = "XX"
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDCATDOC"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_TDTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDCATDOC;
        from (i_cTable) where;
            TDTIPDOC = this.w_TDTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TDCATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if upper(g_APPLICATION) = "ADHOC REVOLUTION" 
      * --- Sono su AHR 
      do case
        case this.w_TDCATDOC $ "OR"
          * --- Lancia Causali Ordini
          this.w_PROG = GSOR_ATD()
          this.w_PROG.Init()     
        otherwise
          * --- Lancia Causali Vendite
          this.w_PROG = GSVE_ATD()
          this.w_PROG.Init()     
      endcase
    else
      * --- Sono su AHE, dove esiste solo un unica gestione Causali Vendite
      * --- Lancia Causali Vendite
      this.w_PROG = GSVE_ATD()
      this.w_PROG.Init()     
    endif
    if this.w_DXBTN
      * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
      *     presente nella variabile
      if g_oMenu.cBatchType$"AM"
        * --- Apri o Modifica
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_TDTIPDOC = this.w_TDTIPDOC
        this.w_PROG.ecpSave()     
        if g_oMenu.cBatchType="M"
          * --- Modifica
          this.w_PROG.ecpEdit()     
        endif
      endif
      if g_oMenu.cBatchType="L"
        * --- Carica
        this.w_PROG.ecpLoad()     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIP_DOCU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
