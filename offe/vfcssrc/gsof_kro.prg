* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_kro                                                        *
*              Ricerca offerte                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2014-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_kro",oParentObject))

* --- Class definition
define class tgsof_kro as StdForm
  Top    = 0
  Left   = 3

  * --- Standard Properties
  Width  = 794
  Height = 423+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-18"
  HelpContextID=152426135
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=129

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  TIP_DOCU_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  AGENTI_IDX = 0
  OFF_NOMI_IDX = 0
  ORI_NOMI_IDX = 0
  CPUSERS_IDX = 0
  GRU_PRIO_IDX = 0
  KEY_ARTI_IDX = 0
  TIT_SEZI_IDX = 0
  SOT_SEZI_IDX = 0
  MOD_OFFE_IDX = 0
  GRU_NOMI_IDX = 0
  ZONE_IDX = 0
  SEZ_OFFE_IDX = 0
  CAT_ATTR_IDX = 0
  OFF_ERTE_IDX = 0
  cPrg = "gsof_kro"
  cComment = "Ricerca offerte"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_RONOMINA = space(1)
  o_RONOMINA = space(1)
  w_TIPNOM = space(1)
  w_SOTIPNOM1 = space(1)
  w_ROCODNOM = space(20)
  w_DESNOM = space(40)
  w_RODADATA = ctod('  /  /  ')
  w_ROADATA = ctod('  /  /  ')
  w_ROSTATO = space(2)
  w_STATO = space(1)
  w_RORICDAL = ctod('  /  /  ')
  w_RORICAL = ctod('  /  /  ')
  w_ROORINOM = space(5)
  w_RORIFDES = space(45)
  w_ROCODUTE = 0
  w_OrderBy = space(7)
  w_ROPRIOFF = space(5)
  w_DESPRI = space(35)
  w_DESORI = space(35)
  w_DESOPE = space(35)
  w_RODANUME = 0
  w_ROANUMER = 0
  w_RODAIMPO = 0
  w_ROAIMPOR = 0
  w_ROARTSER = space(20)
  w_DESARSE = space(40)
  w_ROGRUART = space(5)
  w_DESGRU = space(35)
  w_ROSGRART = space(5)
  w_DESSGR = space(35)
  w_ROCODMOD = space(5)
  w_DESMOD = space(35)
  w_RODADTSC = ctod('  /  /  ')
  w_ROADATSC = ctod('  /  /  ')
  w_RODADTIV = ctod('  /  /  ')
  w_ROADATIV = ctod('  /  /  ')
  w_RODADTCH = ctod('  /  /  ')
  w_ROADATCH = ctod('  /  /  ')
  w_RODANUMD = 0
  w_RODAALFN = space(2)
  w_ROANUMED = 0
  w_ROAALFNU = space(2)
  w_ROPRINOM = space(5)
  o_ROPRINOM = space(5)
  w_DESPRINOM = space(35)
  w_RODANUPR = 0
  w_ROANUMPR = 0
  w_CONTATTO = space(40)
  w_ROCODAGE = space(5)
  w_DESAGE = space(35)
  w_ROCODGRN = space(5)
  w_DESGRN = space(35)
  w_ROCODZON = space(5)
  w_DESZON = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_ROASSATT = space(1)
  w_TIPCAT0 = space(5)
  w_DESATT0 = space(35)
  w_ROCODAT1 = space(10)
  w_TIPCAT1 = space(5)
  w_DESATT1 = space(35)
  w_ROCODAT2 = space(10)
  w_TIPCAT2 = space(5)
  w_DESATT2 = space(35)
  w_ROCODAT3 = space(10)
  w_TIPCAT3 = space(5)
  w_DESATT3 = space(35)
  w_ROCODAT4 = space(10)
  w_TIPCAT4 = space(5)
  w_DESATT4 = space(35)
  w_ROCODAT5 = space(10)
  w_TIPCAT5 = space(5)
  w_DESATT5 = space(35)
  w_ROCODAT6 = space(10)
  w_TIPCAT6 = space(5)
  w_DESATT6 = space(35)
  w_ROCODAT7 = space(10)
  w_TIPCAT7 = space(5)
  w_DESATT7 = space(35)
  w_ROCODAT8 = space(10)
  w_TIPCAT8 = space(5)
  w_DESATT8 = space(35)
  w_ROCODAT9 = space(10)
  w_TIPCAT9 = space(5)
  w_DESATT9 = space(35)
  w_ROCODSO0 = space(10)
  w_DESSEZ0 = space(35)
  w_DESSEZ1 = space(35)
  w_DESSEZ2 = space(35)
  w_ROCODSO1 = space(10)
  w_ROCODSO2 = space(10)
  w_SEROFF = space(10)
  w_CHECK = space(10)
  w_PATHARC = space(254)
  w_OFPATPDF = space(254)
  w_OFPATFWP = space(254)
  w_ROCODAT0 = space(10)
  w_CODATNO3 = space(10)
  w_CODATNO4 = space(10)
  w_CODATNO8 = space(10)
  w_CODATNO6 = space(10)
  w_CODATNO7 = space(10)
  w_CODATNO9 = space(10)
  w_CODATNO5 = space(10)
  w_CODATNO2 = space(10)
  w_CODATNO1 = space(10)
  w_CODATNO0 = space(10)
  w_CODATOF3 = space(10)
  w_CODATOF4 = space(10)
  w_CODATOF8 = space(10)
  w_CODATOF6 = space(10)
  w_CODATOF7 = space(10)
  w_CODATOF9 = space(10)
  w_CODATOF5 = space(10)
  w_CODATOF2 = space(10)
  w_CODATOF1 = space(10)
  w_CODATOF0 = space(10)
  w_OFSERIAL = space(10)
  w_OFDATDOC = ctod('  /  /  ')
  w_PERIODO = space(1)
  w_OFCODNOM = space(20)
  w_CODESC = space(5)
  w_RODANUMD = 0
  w_RODAALFN = space(10)
  w_ROANUMED = 0
  w_ROAALFNU = space(10)
  w_ROARTSER = space(20)
  w_OFCODMOD = space(5)
  w_TIPOMAIL = space(1)
  w_STATO2 = space(1)
  w_ROCONTAT = space(42)
  w_ZoomOff = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_kroPag1","gsof_kro",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsof_kroPag2","gsof_kro",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(3).addobject("oPag","tgsof_kroPag3","gsof_kro",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Attributi/sezioni")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRONOMINA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomOff = this.oPgFrm.Pages(1).oPag.ZoomOff
    DoDefault()
    proc Destroy()
      this.w_ZoomOff = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[19]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='AGENTI'
    this.cWorkTables[7]='OFF_NOMI'
    this.cWorkTables[8]='ORI_NOMI'
    this.cWorkTables[9]='CPUSERS'
    this.cWorkTables[10]='GRU_PRIO'
    this.cWorkTables[11]='KEY_ARTI'
    this.cWorkTables[12]='TIT_SEZI'
    this.cWorkTables[13]='SOT_SEZI'
    this.cWorkTables[14]='MOD_OFFE'
    this.cWorkTables[15]='GRU_NOMI'
    this.cWorkTables[16]='ZONE'
    this.cWorkTables[17]='SEZ_OFFE'
    this.cWorkTables[18]='CAT_ATTR'
    this.cWorkTables[19]='OFF_ERTE'
    return(this.OpenAllTables(19))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RONOMINA=space(1)
      .w_TIPNOM=space(1)
      .w_SOTIPNOM1=space(1)
      .w_ROCODNOM=space(20)
      .w_DESNOM=space(40)
      .w_RODADATA=ctod("  /  /  ")
      .w_ROADATA=ctod("  /  /  ")
      .w_ROSTATO=space(2)
      .w_STATO=space(1)
      .w_RORICDAL=ctod("  /  /  ")
      .w_RORICAL=ctod("  /  /  ")
      .w_ROORINOM=space(5)
      .w_RORIFDES=space(45)
      .w_ROCODUTE=0
      .w_OrderBy=space(7)
      .w_ROPRIOFF=space(5)
      .w_DESPRI=space(35)
      .w_DESORI=space(35)
      .w_DESOPE=space(35)
      .w_RODANUME=0
      .w_ROANUMER=0
      .w_RODAIMPO=0
      .w_ROAIMPOR=0
      .w_ROARTSER=space(20)
      .w_DESARSE=space(40)
      .w_ROGRUART=space(5)
      .w_DESGRU=space(35)
      .w_ROSGRART=space(5)
      .w_DESSGR=space(35)
      .w_ROCODMOD=space(5)
      .w_DESMOD=space(35)
      .w_RODADTSC=ctod("  /  /  ")
      .w_ROADATSC=ctod("  /  /  ")
      .w_RODADTIV=ctod("  /  /  ")
      .w_ROADATIV=ctod("  /  /  ")
      .w_RODADTCH=ctod("  /  /  ")
      .w_ROADATCH=ctod("  /  /  ")
      .w_RODANUMD=0
      .w_RODAALFN=space(2)
      .w_ROANUMED=0
      .w_ROAALFNU=space(2)
      .w_ROPRINOM=space(5)
      .w_DESPRINOM=space(35)
      .w_RODANUPR=0
      .w_ROANUMPR=0
      .w_CONTATTO=space(40)
      .w_ROCODAGE=space(5)
      .w_DESAGE=space(35)
      .w_ROCODGRN=space(5)
      .w_DESGRN=space(35)
      .w_ROCODZON=space(5)
      .w_DESZON=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_ROASSATT=space(1)
      .w_TIPCAT0=space(5)
      .w_DESATT0=space(35)
      .w_ROCODAT1=space(10)
      .w_TIPCAT1=space(5)
      .w_DESATT1=space(35)
      .w_ROCODAT2=space(10)
      .w_TIPCAT2=space(5)
      .w_DESATT2=space(35)
      .w_ROCODAT3=space(10)
      .w_TIPCAT3=space(5)
      .w_DESATT3=space(35)
      .w_ROCODAT4=space(10)
      .w_TIPCAT4=space(5)
      .w_DESATT4=space(35)
      .w_ROCODAT5=space(10)
      .w_TIPCAT5=space(5)
      .w_DESATT5=space(35)
      .w_ROCODAT6=space(10)
      .w_TIPCAT6=space(5)
      .w_DESATT6=space(35)
      .w_ROCODAT7=space(10)
      .w_TIPCAT7=space(5)
      .w_DESATT7=space(35)
      .w_ROCODAT8=space(10)
      .w_TIPCAT8=space(5)
      .w_DESATT8=space(35)
      .w_ROCODAT9=space(10)
      .w_TIPCAT9=space(5)
      .w_DESATT9=space(35)
      .w_ROCODSO0=space(10)
      .w_DESSEZ0=space(35)
      .w_DESSEZ1=space(35)
      .w_DESSEZ2=space(35)
      .w_ROCODSO1=space(10)
      .w_ROCODSO2=space(10)
      .w_SEROFF=space(10)
      .w_CHECK=space(10)
      .w_PATHARC=space(254)
      .w_OFPATPDF=space(254)
      .w_OFPATFWP=space(254)
      .w_ROCODAT0=space(10)
      .w_CODATNO3=space(10)
      .w_CODATNO4=space(10)
      .w_CODATNO8=space(10)
      .w_CODATNO6=space(10)
      .w_CODATNO7=space(10)
      .w_CODATNO9=space(10)
      .w_CODATNO5=space(10)
      .w_CODATNO2=space(10)
      .w_CODATNO1=space(10)
      .w_CODATNO0=space(10)
      .w_CODATOF3=space(10)
      .w_CODATOF4=space(10)
      .w_CODATOF8=space(10)
      .w_CODATOF6=space(10)
      .w_CODATOF7=space(10)
      .w_CODATOF9=space(10)
      .w_CODATOF5=space(10)
      .w_CODATOF2=space(10)
      .w_CODATOF1=space(10)
      .w_CODATOF0=space(10)
      .w_OFSERIAL=space(10)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_PERIODO=space(1)
      .w_OFCODNOM=space(20)
      .w_CODESC=space(5)
      .w_RODANUMD=0
      .w_RODAALFN=space(10)
      .w_ROANUMED=0
      .w_ROAALFNU=space(10)
      .w_ROARTSER=space(20)
      .w_OFCODMOD=space(5)
      .w_TIPOMAIL=space(1)
      .w_STATO2=space(1)
      .w_ROCONTAT=space(42)
        .w_RONOMINA = 'A'
        .w_TIPNOM = .w_RONOMINA
        .w_SOTIPNOM1 = IIF(.w_RONOMINA='A', '', .w_RONOMINA)
        .w_ROCODNOM = SPACE(20)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ROCODNOM))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,7,.f.)
        .w_ROSTATO = 'I '
        .w_STATO = LEFT(.w_ROSTATO,1)
        .DoRTCalc(10,12,.f.)
        if not(empty(.w_ROORINOM))
          .link_1_12('Full')
        endif
          .DoRTCalc(13,13,.f.)
        .w_ROCODUTE = IIF(upper(g_APPLICATION) <> "ADHOC REVOLUTION" ,0, i_CODUTE)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_ROCODUTE))
          .link_1_14('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomOff.Calculate()
        .w_OrderBy = "4,11"
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_ROPRIOFF))
          .link_2_1('Full')
        endif
          .DoRTCalc(17,19,.f.)
        .w_RODANUME = 0
        .w_ROANUMER = 999999
        .DoRTCalc(22,24,.f.)
        if not(empty(.w_ROARTSER))
          .link_2_13('Full')
        endif
        .DoRTCalc(25,26,.f.)
        if not(empty(.w_ROGRUART))
          .link_2_15('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_ROSGRART))
          .link_2_18('Full')
        endif
        .DoRTCalc(29,30,.f.)
        if not(empty(.w_ROCODMOD))
          .link_2_21('Full')
        endif
          .DoRTCalc(31,39,.f.)
        .w_ROANUMED = IIF(upper(g_APPLICATION) = "AD HOC ENTERPRISE", 999999 , 999999999999999)
        .DoRTCalc(41,42,.f.)
        if not(empty(.w_ROPRINOM))
          .link_2_46('Full')
        endif
          .DoRTCalc(43,43,.f.)
        .w_RODANUPR = 0
        .w_ROANUMPR = IIF(Empty(.w_ROPRINOM),999999,.w_ROANUMPR)
        .DoRTCalc(46,47,.f.)
        if not(empty(.w_ROCODAGE))
          .link_2_55('Full')
        endif
        .DoRTCalc(48,49,.f.)
        if not(empty(.w_ROCODGRN))
          .link_2_58('Full')
        endif
        .DoRTCalc(50,51,.f.)
        if not(empty(.w_ROCODZON))
          .link_2_61('Full')
        endif
          .DoRTCalc(52,52,.f.)
        .w_OBTEST = i_datsys
        .w_ROASSATT = 'O'
        .DoRTCalc(55,57,.f.)
        if not(empty(.w_ROCODAT1))
          .link_3_7('Full')
        endif
        .DoRTCalc(58,60,.f.)
        if not(empty(.w_ROCODAT2))
          .link_3_10('Full')
        endif
        .DoRTCalc(61,63,.f.)
        if not(empty(.w_ROCODAT3))
          .link_3_13('Full')
        endif
        .DoRTCalc(64,66,.f.)
        if not(empty(.w_ROCODAT4))
          .link_3_16('Full')
        endif
        .DoRTCalc(67,69,.f.)
        if not(empty(.w_ROCODAT5))
          .link_3_19('Full')
        endif
        .DoRTCalc(70,72,.f.)
        if not(empty(.w_ROCODAT6))
          .link_3_22('Full')
        endif
        .DoRTCalc(73,75,.f.)
        if not(empty(.w_ROCODAT7))
          .link_3_25('Full')
        endif
        .DoRTCalc(76,78,.f.)
        if not(empty(.w_ROCODAT8))
          .link_3_28('Full')
        endif
        .DoRTCalc(79,81,.f.)
        if not(empty(.w_ROCODAT9))
          .link_3_31('Full')
        endif
        .DoRTCalc(82,84,.f.)
        if not(empty(.w_ROCODSO0))
          .link_3_36('Full')
        endif
        .DoRTCalc(85,88,.f.)
        if not(empty(.w_ROCODSO1))
          .link_3_40('Full')
        endif
        .DoRTCalc(89,89,.f.)
        if not(empty(.w_ROCODSO2))
          .link_3_41('Full')
        endif
        .w_SEROFF = NVL(.w_ZoomOff.getVar('OFSERIAL'), SPACE(10))
          .DoRTCalc(91,91,.f.)
        .w_PATHARC = NVL(.w_ZoomOff.getVar('MOPATARC'), SPACE(254))
        .w_OFPATPDF = NVL(.w_ZoomOff.getVar('OFPATPDF'), SPACE(254))
        .w_OFPATFWP = NVL(.w_ZoomOff.getVar('OFPATFWP'), SPACE(254))
      .oPgFrm.Page3.oPag.oObj_3_42.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_43.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_44.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_45.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_46.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_47.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_48.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_49.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_50.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_51.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_52.Calculate()
        .DoRTCalc(95,95,.f.)
        if not(empty(.w_ROCODAT0))
          .link_3_53('Full')
        endif
        .w_CODATNO3 = IIF(.w_ROASSATT='N',.w_ROCODAT3,' ')
        .DoRTCalc(96,96,.f.)
        if not(empty(.w_CODATNO3))
          .link_3_54('Full')
        endif
        .w_CODATNO4 = IIF(.w_ROASSATT='N',.w_ROCODAT4,' ')
        .DoRTCalc(97,97,.f.)
        if not(empty(.w_CODATNO4))
          .link_3_55('Full')
        endif
        .w_CODATNO8 = IIF(.w_ROASSATT='N',.w_ROCODAT8,' ')
        .DoRTCalc(98,98,.f.)
        if not(empty(.w_CODATNO8))
          .link_3_56('Full')
        endif
        .w_CODATNO6 = IIF(.w_ROASSATT='N',.w_ROCODAT6,' ')
        .DoRTCalc(99,99,.f.)
        if not(empty(.w_CODATNO6))
          .link_3_57('Full')
        endif
        .w_CODATNO7 = IIF(.w_ROASSATT='N',.w_ROCODAT7,' ')
        .DoRTCalc(100,100,.f.)
        if not(empty(.w_CODATNO7))
          .link_3_58('Full')
        endif
        .w_CODATNO9 = IIF(.w_ROASSATT='N',.w_ROCODAT9,' ')
        .DoRTCalc(101,101,.f.)
        if not(empty(.w_CODATNO9))
          .link_3_59('Full')
        endif
        .w_CODATNO5 = IIF(.w_ROASSATT='N',.w_ROCODAT5,' ')
        .DoRTCalc(102,102,.f.)
        if not(empty(.w_CODATNO5))
          .link_3_60('Full')
        endif
        .w_CODATNO2 = IIF(.w_ROASSATT='N',.w_ROCODAT2,' ')
        .DoRTCalc(103,103,.f.)
        if not(empty(.w_CODATNO2))
          .link_3_61('Full')
        endif
        .w_CODATNO1 = IIF(.w_ROASSATT='N',.w_ROCODAT1,' ')
        .DoRTCalc(104,104,.f.)
        if not(empty(.w_CODATNO1))
          .link_3_62('Full')
        endif
        .w_CODATNO0 = IIF(.w_ROASSATT='N',.w_ROCODAT0,' ')
        .DoRTCalc(105,105,.f.)
        if not(empty(.w_CODATNO0))
          .link_3_63('Full')
        endif
        .w_CODATOF3 = IIF(.w_ROASSATT='O',.w_ROCODAT3,' ')
        .DoRTCalc(106,106,.f.)
        if not(empty(.w_CODATOF3))
          .link_3_64('Full')
        endif
        .w_CODATOF4 = IIF(.w_ROASSATT='O',.w_ROCODAT4,' ')
        .DoRTCalc(107,107,.f.)
        if not(empty(.w_CODATOF4))
          .link_3_65('Full')
        endif
        .w_CODATOF8 = IIF(.w_ROASSATT='O',.w_ROCODAT8,' ')
        .DoRTCalc(108,108,.f.)
        if not(empty(.w_CODATOF8))
          .link_3_66('Full')
        endif
        .w_CODATOF6 = IIF(.w_ROASSATT='O',.w_ROCODAT6,' ')
        .DoRTCalc(109,109,.f.)
        if not(empty(.w_CODATOF6))
          .link_3_67('Full')
        endif
        .w_CODATOF7 = IIF(.w_ROASSATT='O',.w_ROCODAT7,' ')
        .DoRTCalc(110,110,.f.)
        if not(empty(.w_CODATOF7))
          .link_3_68('Full')
        endif
        .w_CODATOF9 = IIF(.w_ROASSATT='O',.w_ROCODAT9,' ')
        .DoRTCalc(111,111,.f.)
        if not(empty(.w_CODATOF9))
          .link_3_69('Full')
        endif
        .w_CODATOF5 = IIF(.w_ROASSATT='O',.w_ROCODAT5,' ')
        .DoRTCalc(112,112,.f.)
        if not(empty(.w_CODATOF5))
          .link_3_70('Full')
        endif
        .w_CODATOF2 = IIF(.w_ROASSATT='O',.w_ROCODAT2,' ')
        .DoRTCalc(113,113,.f.)
        if not(empty(.w_CODATOF2))
          .link_3_71('Full')
        endif
        .w_CODATOF1 = IIF(.w_ROASSATT='O',.w_ROCODAT1,' ')
        .DoRTCalc(114,114,.f.)
        if not(empty(.w_CODATOF1))
          .link_3_72('Full')
        endif
        .w_CODATOF0 = IIF(.w_ROASSATT='O',.w_ROCODAT0,' ')
        .DoRTCalc(115,115,.f.)
        if not(empty(.w_CODATOF0))
          .link_3_73('Full')
        endif
        .w_OFSERIAL = NVL(.w_ZoomOff.getVar('OFSERIAL'), SPACE(10))
      .oPgFrm.Page2.oPag.oObj_2_67.Calculate()
        .w_OFDATDOC = NVL(.w_ZoomOff.getVar('OFDATDOC'), cp_CharToDate('  /  /  '))
        .w_PERIODO = NVL(.w_ZoomOff.getVar('MONUMPER'), SPACE(1))
        .w_OFCODNOM = NVL(.w_ZoomOff.getVar('OFCODNOM'), SPACE(20))
          .DoRTCalc(120,122,.f.)
        .w_ROANUMED = IIF(upper(g_APPLICATION) = "AD HOC ENTERPRISE", 999999 , 999999999999999)
        .DoRTCalc(124,125,.f.)
        if not(empty(.w_ROARTSER))
          .link_2_75('Full')
        endif
        .w_OFCODMOD = NVL(.w_ZoomOff.getVar('OFCODMOD'), SPACE(5))
        .DoRTCalc(126,126,.f.)
        if not(empty(.w_OFCODMOD))
          .link_1_44('Full')
        endif
          .DoRTCalc(127,127,.f.)
        .w_STATO2 = RIGHT(.w_ROSTATO,1)
        .w_ROCONTAT = IIF(EMPTY(.w_CONTATTO),' ','%'+ALLTRIM(.w_CONTATTO)+'%')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_RONOMINA<>.w_RONOMINA
            .w_TIPNOM = .w_RONOMINA
        endif
        if .o_RONOMINA<>.w_RONOMINA
            .w_SOTIPNOM1 = IIF(.w_RONOMINA='A', '', .w_RONOMINA)
        endif
        if .o_RONOMINA<>.w_RONOMINA
            .w_ROCODNOM = SPACE(20)
          .link_1_4('Full')
        endif
        .DoRTCalc(5,8,.t.)
            .w_STATO = LEFT(.w_ROSTATO,1)
        .oPgFrm.Page1.oPag.ZoomOff.Calculate()
        .DoRTCalc(10,44,.t.)
        if .o_ROPRINOM<>.w_ROPRINOM
            .w_ROANUMPR = IIF(Empty(.w_ROPRINOM),999999,.w_ROANUMPR)
        endif
        .DoRTCalc(46,52,.t.)
            .w_OBTEST = i_datsys
        .DoRTCalc(54,89,.t.)
            .w_SEROFF = NVL(.w_ZoomOff.getVar('OFSERIAL'), SPACE(10))
        .DoRTCalc(91,91,.t.)
            .w_PATHARC = NVL(.w_ZoomOff.getVar('MOPATARC'), SPACE(254))
            .w_OFPATPDF = NVL(.w_ZoomOff.getVar('OFPATPDF'), SPACE(254))
            .w_OFPATFWP = NVL(.w_ZoomOff.getVar('OFPATFWP'), SPACE(254))
        .oPgFrm.Page3.oPag.oObj_3_42.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_43.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_44.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_45.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_46.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_47.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_48.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_49.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_50.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_51.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_52.Calculate()
        .DoRTCalc(95,95,.t.)
            .w_CODATNO3 = IIF(.w_ROASSATT='N',.w_ROCODAT3,' ')
          .link_3_54('Full')
            .w_CODATNO4 = IIF(.w_ROASSATT='N',.w_ROCODAT4,' ')
          .link_3_55('Full')
            .w_CODATNO8 = IIF(.w_ROASSATT='N',.w_ROCODAT8,' ')
          .link_3_56('Full')
            .w_CODATNO6 = IIF(.w_ROASSATT='N',.w_ROCODAT6,' ')
          .link_3_57('Full')
            .w_CODATNO7 = IIF(.w_ROASSATT='N',.w_ROCODAT7,' ')
          .link_3_58('Full')
            .w_CODATNO9 = IIF(.w_ROASSATT='N',.w_ROCODAT9,' ')
          .link_3_59('Full')
            .w_CODATNO5 = IIF(.w_ROASSATT='N',.w_ROCODAT5,' ')
          .link_3_60('Full')
            .w_CODATNO2 = IIF(.w_ROASSATT='N',.w_ROCODAT2,' ')
          .link_3_61('Full')
            .w_CODATNO1 = IIF(.w_ROASSATT='N',.w_ROCODAT1,' ')
          .link_3_62('Full')
            .w_CODATNO0 = IIF(.w_ROASSATT='N',.w_ROCODAT0,' ')
          .link_3_63('Full')
            .w_CODATOF3 = IIF(.w_ROASSATT='O',.w_ROCODAT3,' ')
          .link_3_64('Full')
            .w_CODATOF4 = IIF(.w_ROASSATT='O',.w_ROCODAT4,' ')
          .link_3_65('Full')
            .w_CODATOF8 = IIF(.w_ROASSATT='O',.w_ROCODAT8,' ')
          .link_3_66('Full')
            .w_CODATOF6 = IIF(.w_ROASSATT='O',.w_ROCODAT6,' ')
          .link_3_67('Full')
            .w_CODATOF7 = IIF(.w_ROASSATT='O',.w_ROCODAT7,' ')
          .link_3_68('Full')
            .w_CODATOF9 = IIF(.w_ROASSATT='O',.w_ROCODAT9,' ')
          .link_3_69('Full')
            .w_CODATOF5 = IIF(.w_ROASSATT='O',.w_ROCODAT5,' ')
          .link_3_70('Full')
            .w_CODATOF2 = IIF(.w_ROASSATT='O',.w_ROCODAT2,' ')
          .link_3_71('Full')
            .w_CODATOF1 = IIF(.w_ROASSATT='O',.w_ROCODAT1,' ')
          .link_3_72('Full')
            .w_CODATOF0 = IIF(.w_ROASSATT='O',.w_ROCODAT0,' ')
          .link_3_73('Full')
            .w_OFSERIAL = NVL(.w_ZoomOff.getVar('OFSERIAL'), SPACE(10))
        .oPgFrm.Page2.oPag.oObj_2_67.Calculate()
            .w_OFDATDOC = NVL(.w_ZoomOff.getVar('OFDATDOC'), cp_CharToDate('  /  /  '))
            .w_PERIODO = NVL(.w_ZoomOff.getVar('MONUMPER'), SPACE(1))
            .w_OFCODNOM = NVL(.w_ZoomOff.getVar('OFCODNOM'), SPACE(20))
        .DoRTCalc(120,125,.t.)
            .w_OFCODMOD = NVL(.w_ZoomOff.getVar('OFCODMOD'), SPACE(5))
          .link_1_44('Full')
        .DoRTCalc(127,127,.t.)
            .w_STATO2 = RIGHT(.w_ROSTATO,1)
            .w_ROCONTAT = IIF(EMPTY(.w_CONTATTO),' ','%'+ALLTRIM(.w_CONTATTO)+'%')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomOff.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_42.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_43.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_44.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_45.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_46.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_47.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_48.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_49.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_50.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_51.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_52.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_67.Calculate()
    endwith
  return

  proc Calculate_SATNIHJYAT()
    with this
          * --- Modifico query zoom
          .w_ZoomOff.ccpqueryname = "..\OFFE\EXE\QUERY\GSOF1KRO"
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oRODANUME_2_5.enabled = this.oPgFrm.Page2.oPag.oRODANUME_2_5.mCond()
    this.oPgFrm.Page2.oPag.oROANUMER_2_7.enabled = this.oPgFrm.Page2.oPag.oROANUMER_2_7.mCond()
    this.oPgFrm.Page2.oPag.oROARTSER_2_13.enabled = this.oPgFrm.Page2.oPag.oROARTSER_2_13.mCond()
    this.oPgFrm.Page2.oPag.oROSGRART_2_18.enabled = this.oPgFrm.Page2.oPag.oROSGRART_2_18.mCond()
    this.oPgFrm.Page2.oPag.oRODANUMD_2_36.enabled = this.oPgFrm.Page2.oPag.oRODANUMD_2_36.mCond()
    this.oPgFrm.Page2.oPag.oRODAALFN_2_39.enabled = this.oPgFrm.Page2.oPag.oRODAALFN_2_39.mCond()
    this.oPgFrm.Page2.oPag.oROANUMED_2_40.enabled = this.oPgFrm.Page2.oPag.oROANUMED_2_40.mCond()
    this.oPgFrm.Page2.oPag.oROAALFNU_2_43.enabled = this.oPgFrm.Page2.oPag.oROAALFNU_2_43.mCond()
    this.oPgFrm.Page2.oPag.oRODANUPR_2_50.enabled = this.oPgFrm.Page2.oPag.oRODANUPR_2_50.mCond()
    this.oPgFrm.Page2.oPag.oROANUMPR_2_52.enabled = this.oPgFrm.Page2.oPag.oROANUMPR_2_52.mCond()
    this.oPgFrm.Page3.oPag.oROCODAT1_3_7.enabled = this.oPgFrm.Page3.oPag.oROCODAT1_3_7.mCond()
    this.oPgFrm.Page3.oPag.oROCODAT2_3_10.enabled = this.oPgFrm.Page3.oPag.oROCODAT2_3_10.mCond()
    this.oPgFrm.Page3.oPag.oROCODAT3_3_13.enabled = this.oPgFrm.Page3.oPag.oROCODAT3_3_13.mCond()
    this.oPgFrm.Page3.oPag.oROCODAT4_3_16.enabled = this.oPgFrm.Page3.oPag.oROCODAT4_3_16.mCond()
    this.oPgFrm.Page3.oPag.oROCODAT5_3_19.enabled = this.oPgFrm.Page3.oPag.oROCODAT5_3_19.mCond()
    this.oPgFrm.Page3.oPag.oROCODAT6_3_22.enabled = this.oPgFrm.Page3.oPag.oROCODAT6_3_22.mCond()
    this.oPgFrm.Page3.oPag.oROCODAT7_3_25.enabled = this.oPgFrm.Page3.oPag.oROCODAT7_3_25.mCond()
    this.oPgFrm.Page3.oPag.oROCODAT8_3_28.enabled = this.oPgFrm.Page3.oPag.oROCODAT8_3_28.mCond()
    this.oPgFrm.Page3.oPag.oROCODAT9_3_31.enabled = this.oPgFrm.Page3.oPag.oROCODAT9_3_31.mCond()
    this.oPgFrm.Page3.oPag.oROCODSO1_3_40.enabled = this.oPgFrm.Page3.oPag.oROCODSO1_3_40.mCond()
    this.oPgFrm.Page3.oPag.oROCODSO2_3_41.enabled = this.oPgFrm.Page3.oPag.oROCODSO2_3_41.mCond()
    this.oPgFrm.Page2.oPag.oRODANUMD_2_69.enabled = this.oPgFrm.Page2.oPag.oRODANUMD_2_69.mCond()
    this.oPgFrm.Page2.oPag.oRODAALFN_2_71.enabled = this.oPgFrm.Page2.oPag.oRODAALFN_2_71.mCond()
    this.oPgFrm.Page2.oPag.oROANUMED_2_72.enabled = this.oPgFrm.Page2.oPag.oROANUMED_2_72.mCond()
    this.oPgFrm.Page2.oPag.oROAALFNU_2_74.enabled = this.oPgFrm.Page2.oPag.oROAALFNU_2_74.mCond()
    this.oPgFrm.Page2.oPag.oROARTSER_2_75.enabled = this.oPgFrm.Page2.oPag.oROARTSER_2_75.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(Isalt())
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Attributi/sezioni"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page2.oPag.oROARTSER_2_13.visible=!this.oPgFrm.Page2.oPag.oROARTSER_2_13.mHide()
    this.oPgFrm.Page2.oPag.oRODANUMD_2_36.visible=!this.oPgFrm.Page2.oPag.oRODANUMD_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_38.visible=!this.oPgFrm.Page2.oPag.oStr_2_38.mHide()
    this.oPgFrm.Page2.oPag.oRODAALFN_2_39.visible=!this.oPgFrm.Page2.oPag.oRODAALFN_2_39.mHide()
    this.oPgFrm.Page2.oPag.oROANUMED_2_40.visible=!this.oPgFrm.Page2.oPag.oROANUMED_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_42.visible=!this.oPgFrm.Page2.oPag.oStr_2_42.mHide()
    this.oPgFrm.Page2.oPag.oROAALFNU_2_43.visible=!this.oPgFrm.Page2.oPag.oROAALFNU_2_43.mHide()
    this.oPgFrm.Page2.oPag.oRODANUMD_2_69.visible=!this.oPgFrm.Page2.oPag.oRODANUMD_2_69.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_70.visible=!this.oPgFrm.Page2.oPag.oStr_2_70.mHide()
    this.oPgFrm.Page2.oPag.oRODAALFN_2_71.visible=!this.oPgFrm.Page2.oPag.oRODAALFN_2_71.mHide()
    this.oPgFrm.Page2.oPag.oROANUMED_2_72.visible=!this.oPgFrm.Page2.oPag.oROANUMED_2_72.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_73.visible=!this.oPgFrm.Page2.oPag.oStr_2_73.mHide()
    this.oPgFrm.Page2.oPag.oROAALFNU_2_74.visible=!this.oPgFrm.Page2.oPag.oROAALFNU_2_74.mHide()
    this.oPgFrm.Page2.oPag.oROARTSER_2_75.visible=!this.oPgFrm.Page2.oPag.oROARTSER_2_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_SATNIHJYAT()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZoomOff.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_42.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_43.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_44.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_45.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_46.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_47.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_48.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_49.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_50.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_51.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_52.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_67.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ROCODNOM
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_ROCODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_ROCODNOM))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oROCODNOM_1_4'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF2QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_ROCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_ROCODNOM)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODNOM = space(20)
      endif
      this.w_DESNOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROORINOM
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROORINOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AON',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_ROORINOM)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_ROORINOM))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROORINOM)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROORINOM) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oROORINOM_1_12'),i_cWhere,'GSAR_AON',"Origine nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROORINOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_ROORINOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_ROORINOM)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROORINOM = NVL(_Link_.ONCODICE,space(5))
      this.w_DESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROORINOM = space(5)
      endif
      this.w_DESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROORINOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODUTE
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_ROCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_ROCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_ROCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oROCODUTE_1_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_ROCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_ROCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODUTE = NVL(_Link_.CODE,0)
      this.w_DESOPE = NVL(_Link_.NAME,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODUTE = 0
      endif
      this.w_DESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROPRIOFF
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROPRIOFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_ROPRIOFF)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_ROPRIOFF))
          select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROPRIOFF)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROPRIOFF) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oROPRIOFF_2_1'),i_cWhere,'',"Priorit� offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROPRIOFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_ROPRIOFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_ROPRIOFF)
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROPRIOFF = NVL(_Link_.GPCODICE,space(5))
      this.w_DESPRI = NVL(_Link_.GPDESCRI,space(35))
      this.w_RODANUME = NVL(_Link_.GPNUMINI,0)
      this.w_ROANUMER = NVL(_Link_.GPNUMFIN,0)
    else
      if i_cCtrl<>'Load'
        this.w_ROPRIOFF = space(5)
      endif
      this.w_DESPRI = space(35)
      this.w_RODANUME = 0
      this.w_ROANUMER = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROPRIOFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROARTSER
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROARTSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ROARTSER)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ROARTSER))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROARTSER)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROARTSER) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oROARTSER_2_13'),i_cWhere,'',"Codici articoli/servizi",'GSVE_MDV.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROARTSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ROARTSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ROARTSER)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROARTSER = NVL(_Link_.CACODICE,space(20))
      this.w_DESARSE = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ROARTSER = space(20)
      endif
      this.w_DESARSE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROARTSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROGRUART
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIT_SEZI_IDX,3]
    i_lTable = "TIT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2], .t., this.TIT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROGRUART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TSCODICE like "+cp_ToStrODBC(trim(this.w_ROGRUART)+"%");

          i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TSCODICE',trim(this.w_ROGRUART))
          select TSCODICE,TSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROGRUART)==trim(_Link_.TSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROGRUART) and !this.bDontReportError
            deferred_cp_zoom('TIT_SEZI','*','TSCODICE',cp_AbsName(oSource.parent,'oROGRUART_2_15'),i_cWhere,'',"Gruppo articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',oSource.xKey(1))
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROGRUART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(this.w_ROGRUART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',this.w_ROGRUART)
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROGRUART = NVL(_Link_.TSCODICE,space(5))
      this.w_DESGRU = NVL(_Link_.TSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROGRUART = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.TSCODICE,1)
      cp_ShowWarn(i_cKey,this.TIT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROGRUART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROSGRART
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_lTable = "SOT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2], .t., this.SOT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROSGRART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SOT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SGCODSOT like "+cp_ToStrODBC(trim(this.w_ROSGRART)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ROGRUART);

          i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SGCODGRU,SGCODSOT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SGCODGRU',this.w_ROGRUART;
                     ,'SGCODSOT',trim(this.w_ROSGRART))
          select SGCODGRU,SGCODSOT,SGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SGCODGRU,SGCODSOT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROSGRART)==trim(_Link_.SGCODSOT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROSGRART) and !this.bDontReportError
            deferred_cp_zoom('SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(oSource.parent,'oROSGRART_2_18'),i_cWhere,'',"Sottogruppo articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ROGRUART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SGCODGRU="+cp_ToStrODBC(this.w_ROGRUART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',oSource.xKey(1);
                       ,'SGCODSOT',oSource.xKey(2))
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROSGRART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(this.w_ROSGRART);
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ROGRUART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',this.w_ROGRUART;
                       ,'SGCODSOT',this.w_ROSGRART)
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROSGRART = NVL(_Link_.SGCODSOT,space(5))
      this.w_DESSGR = NVL(_Link_.SGDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROSGRART = space(5)
      endif
      this.w_DESSGR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.SGCODGRU,1)+'\'+cp_ToStr(_Link_.SGCODSOT,1)
      cp_ShowWarn(i_cKey,this.SOT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROSGRART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODMOD
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_lTable = "MOD_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2], .t., this.MOD_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MOD_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_ROCODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_ROCODMOD))
          select MOCODICE,MODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODMOD) and !this.bDontReportError
            deferred_cp_zoom('MOD_OFFE','*','MOCODICE',cp_AbsName(oSource.parent,'oROCODMOD_2_21'),i_cWhere,'',"Modelli di riferimento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_ROCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_ROCODMOD)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODMOD = NVL(_Link_.MOCODICE,space(5))
      this.w_DESMOD = NVL(_Link_.MODESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODMOD = space(5)
      endif
      this.w_DESMOD = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROPRINOM
  func Link_2_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROPRINOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_ROPRINOM)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_ROPRINOM))
          select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROPRINOM)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROPRINOM) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oROPRINOM_2_46'),i_cWhere,'',"Priorit� offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROPRINOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_ROPRINOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_ROPRINOM)
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROPRINOM = NVL(_Link_.GPCODICE,space(5))
      this.w_DESPRINOM = NVL(_Link_.GPDESCRI,space(35))
      this.w_RODANUPR = NVL(_Link_.GPNUMINI,0)
      this.w_ROANUMPR = NVL(_Link_.GPNUMFIN,0)
    else
      if i_cCtrl<>'Load'
        this.w_ROPRINOM = space(5)
      endif
      this.w_DESPRINOM = space(35)
      this.w_RODANUPR = 0
      this.w_ROANUMPR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROPRINOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAGE
  func Link_2_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_ROCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_ROCODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oROCODAGE_2_55'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_ROCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_ROCODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODGRN
  func Link_2_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODGRN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_ROCODGRN)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_ROCODGRN))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODGRN)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODGRN) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oROCODGRN_2_58'),i_cWhere,'',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODGRN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_ROCODGRN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_ROCODGRN)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODGRN = NVL(_Link_.GNCODICE,space(5))
      this.w_DESGRN = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODGRN = space(5)
      endif
      this.w_DESGRN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODGRN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODZON
  func Link_2_61(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_ROCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_ROCODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oROCODZON_2_61'),i_cWhere,'',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_ROCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_ROCODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODZON = NVL(_Link_.ZOCODZON,space(5))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODZON = space(5)
      endif
      this.w_DESZON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT1
  func Link_3_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT1))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT1)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT1) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT1_3_7'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT1)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT1 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT1 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT1 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT1 = space(10)
      endif
      this.w_DESATT1 = space(35)
      this.w_TIPCAT1 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT2
  func Link_3_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT2))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT2)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT2) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT2_3_10'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT2)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT2 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT2 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT2 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT2 = space(10)
      endif
      this.w_DESATT2 = space(35)
      this.w_TIPCAT2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT3
  func Link_3_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT3))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT3)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT3) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT3_3_13'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT3)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT3 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT3 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT3 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT3 = space(10)
      endif
      this.w_DESATT3 = space(35)
      this.w_TIPCAT3 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT4
  func Link_3_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT4)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT4))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT4)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT4) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT4_3_16'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT4)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT4 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT4 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT4 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT4 = space(10)
      endif
      this.w_DESATT4 = space(35)
      this.w_TIPCAT4 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT5
  func Link_3_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT5)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT5))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT5)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT5) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT5_3_19'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT5)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT5 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT5 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT5 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT5 = space(10)
      endif
      this.w_DESATT5 = space(35)
      this.w_TIPCAT5 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT6
  func Link_3_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT6)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT6))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT6)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT6) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT6_3_22'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT6)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT6 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT6 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT6 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT6 = space(10)
      endif
      this.w_DESATT6 = space(35)
      this.w_TIPCAT6 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT7
  func Link_3_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT7)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT7))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT7)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT7) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT7_3_25'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT7)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT7 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT7 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT7 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT7 = space(10)
      endif
      this.w_DESATT7 = space(35)
      this.w_TIPCAT7 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT8
  func Link_3_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT8)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT8))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT8)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT8) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT8_3_28'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT8)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT8 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT8 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT8 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT8 = space(10)
      endif
      this.w_DESATT8 = space(35)
      this.w_TIPCAT8 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT9
  func Link_3_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT9)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT9))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT9)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT9) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT9_3_31'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT9)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT9 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT9 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT9 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT9 = space(10)
      endif
      this.w_DESATT9 = space(35)
      this.w_TIPCAT9 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODSO0
  func Link_3_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_lTable = "SEZ_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2], .t., this.SEZ_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODSO0) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SEZ_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SOCODICE like "+cp_ToStrODBC(trim(this.w_ROCODSO0)+"%");

          i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SOCODICE',trim(this.w_ROCODSO0))
          select SOCODICE,SODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODSO0)==trim(_Link_.SOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODSO0) and !this.bDontReportError
            deferred_cp_zoom('SEZ_OFFE','*','SOCODICE',cp_AbsName(oSource.parent,'oROCODSO0_3_36'),i_cWhere,'',"Sezioni offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',oSource.xKey(1))
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODSO0)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(this.w_ROCODSO0);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',this.w_ROCODSO0)
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODSO0 = NVL(_Link_.SOCODICE,space(10))
      this.w_DESSEZ0 = NVL(_Link_.SODESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODSO0 = space(10)
      endif
      this.w_DESSEZ0 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.SOCODICE,1)
      cp_ShowWarn(i_cKey,this.SEZ_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODSO0 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODSO1
  func Link_3_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_lTable = "SEZ_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2], .t., this.SEZ_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODSO1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SEZ_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SOCODICE like "+cp_ToStrODBC(trim(this.w_ROCODSO1)+"%");

          i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SOCODICE',trim(this.w_ROCODSO1))
          select SOCODICE,SODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODSO1)==trim(_Link_.SOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODSO1) and !this.bDontReportError
            deferred_cp_zoom('SEZ_OFFE','*','SOCODICE',cp_AbsName(oSource.parent,'oROCODSO1_3_40'),i_cWhere,'',"Sezioni offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',oSource.xKey(1))
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODSO1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(this.w_ROCODSO1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',this.w_ROCODSO1)
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODSO1 = NVL(_Link_.SOCODICE,space(10))
      this.w_DESSEZ1 = NVL(_Link_.SODESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODSO1 = space(10)
      endif
      this.w_DESSEZ1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.SOCODICE,1)
      cp_ShowWarn(i_cKey,this.SEZ_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODSO1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODSO2
  func Link_3_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_lTable = "SEZ_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2], .t., this.SEZ_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODSO2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SEZ_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SOCODICE like "+cp_ToStrODBC(trim(this.w_ROCODSO2)+"%");

          i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SOCODICE',trim(this.w_ROCODSO2))
          select SOCODICE,SODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODSO2)==trim(_Link_.SOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODSO2) and !this.bDontReportError
            deferred_cp_zoom('SEZ_OFFE','*','SOCODICE',cp_AbsName(oSource.parent,'oROCODSO2_3_41'),i_cWhere,'',"Sezioni offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',oSource.xKey(1))
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODSO2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(this.w_ROCODSO2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',this.w_ROCODSO2)
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODSO2 = NVL(_Link_.SOCODICE,space(10))
      this.w_DESSEZ2 = NVL(_Link_.SODESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODSO2 = space(10)
      endif
      this.w_DESSEZ2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.SOCODICE,1)
      cp_ShowWarn(i_cKey,this.SEZ_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODSO2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROCODAT0
  func Link_3_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROCODAT0) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ROCODAT0)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ROCODAT0))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROCODAT0)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROCODAT0) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oROCODAT0_3_53'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROCODAT0)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ROCODAT0);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ROCODAT0)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROCODAT0 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT0 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT0 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ROCODAT0 = space(10)
      endif
      this.w_DESATT0 = space(35)
      this.w_TIPCAT0 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROCODAT0 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO3
  func Link_3_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO3)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO3 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO3 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO4
  func Link_3_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO4)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO4 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO4 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO8
  func Link_3_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO8)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO8 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO8 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO6
  func Link_3_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO6)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO6 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO6 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO7
  func Link_3_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO7)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO7 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO7 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO9
  func Link_3_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO9)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO9 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO9 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO5
  func Link_3_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO5)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO5 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO5 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO2
  func Link_3_61(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO2)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO2 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO2 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO1
  func Link_3_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO1)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO1 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO1 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATNO0
  func Link_3_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATNO0) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATNO0)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATNO0);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATNO0)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATNO0 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATNO0 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATNO0 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF3
  func Link_3_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF3)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF3 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF3 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF4
  func Link_3_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF4)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF4 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF4 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF8
  func Link_3_66(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF8)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF8 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF8 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF6
  func Link_3_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF6)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF6 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF6 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF7
  func Link_3_68(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF7)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF7 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF7 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF9
  func Link_3_69(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF9)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF9 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF9 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF5
  func Link_3_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF5)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF5 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF5 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF2
  func Link_3_71(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF2)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF2 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF2 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF1
  func Link_3_72(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF1)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF1 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF1 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATOF0
  func Link_3_73(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATOF0) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATOF0)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CODATOF0);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CODATOF0)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATOF0 = NVL(_Link_.CTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODATOF0 = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATOF0 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROARTSER
  func Link_2_75(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROARTSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ROARTSER)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ROARTSER))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROARTSER)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ROARTSER) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oROARTSER_2_75'),i_cWhere,'',"Codici articoli/servizi",'GSVE0MDV.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROARTSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ROARTSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ROARTSER)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROARTSER = NVL(_Link_.CACODICE,space(20))
      this.w_DESARSE = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ROARTSER = space(20)
      endif
      this.w_DESARSE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROARTSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFCODMOD
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_lTable = "MOD_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2], .t., this.MOD_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MOALMAIL";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_OFCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_OFCODMOD)
            select MOCODICE,MOALMAIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODMOD = NVL(_Link_.MOCODICE,space(5))
      this.w_TIPOMAIL = NVL(_Link_.MOALMAIL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODMOD = space(5)
      endif
      this.w_TIPOMAIL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRONOMINA_1_1.RadioValue()==this.w_RONOMINA)
      this.oPgFrm.Page1.oPag.oRONOMINA_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oROCODNOM_1_4.value==this.w_ROCODNOM)
      this.oPgFrm.Page1.oPag.oROCODNOM_1_4.value=this.w_ROCODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_5.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_5.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oRODADATA_1_6.value==this.w_RODADATA)
      this.oPgFrm.Page1.oPag.oRODADATA_1_6.value=this.w_RODADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oROADATA_1_7.value==this.w_ROADATA)
      this.oPgFrm.Page1.oPag.oROADATA_1_7.value=this.w_ROADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oROSTATO_1_8.RadioValue()==this.w_ROSTATO)
      this.oPgFrm.Page1.oPag.oROSTATO_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRORICDAL_1_10.value==this.w_RORICDAL)
      this.oPgFrm.Page1.oPag.oRORICDAL_1_10.value=this.w_RORICDAL
    endif
    if not(this.oPgFrm.Page1.oPag.oRORICAL_1_11.value==this.w_RORICAL)
      this.oPgFrm.Page1.oPag.oRORICAL_1_11.value=this.w_RORICAL
    endif
    if not(this.oPgFrm.Page1.oPag.oROORINOM_1_12.value==this.w_ROORINOM)
      this.oPgFrm.Page1.oPag.oROORINOM_1_12.value=this.w_ROORINOM
    endif
    if not(this.oPgFrm.Page1.oPag.oRORIFDES_1_13.value==this.w_RORIFDES)
      this.oPgFrm.Page1.oPag.oRORIFDES_1_13.value=this.w_RORIFDES
    endif
    if not(this.oPgFrm.Page1.oPag.oROCODUTE_1_14.value==this.w_ROCODUTE)
      this.oPgFrm.Page1.oPag.oROCODUTE_1_14.value=this.w_ROCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oOrderBy_1_21.RadioValue()==this.w_OrderBy)
      this.oPgFrm.Page1.oPag.oOrderBy_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oROPRIOFF_2_1.value==this.w_ROPRIOFF)
      this.oPgFrm.Page2.oPag.oROPRIOFF_2_1.value=this.w_ROPRIOFF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRI_2_2.value==this.w_DESPRI)
      this.oPgFrm.Page2.oPag.oDESPRI_2_2.value=this.w_DESPRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESORI_1_31.value==this.w_DESORI)
      this.oPgFrm.Page1.oPag.oDESORI_1_31.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOPE_1_34.value==this.w_DESOPE)
      this.oPgFrm.Page1.oPag.oDESOPE_1_34.value=this.w_DESOPE
    endif
    if not(this.oPgFrm.Page2.oPag.oRODANUME_2_5.value==this.w_RODANUME)
      this.oPgFrm.Page2.oPag.oRODANUME_2_5.value=this.w_RODANUME
    endif
    if not(this.oPgFrm.Page2.oPag.oROANUMER_2_7.value==this.w_ROANUMER)
      this.oPgFrm.Page2.oPag.oROANUMER_2_7.value=this.w_ROANUMER
    endif
    if not(this.oPgFrm.Page2.oPag.oRODAIMPO_2_9.value==this.w_RODAIMPO)
      this.oPgFrm.Page2.oPag.oRODAIMPO_2_9.value=this.w_RODAIMPO
    endif
    if not(this.oPgFrm.Page2.oPag.oROAIMPOR_2_11.value==this.w_ROAIMPOR)
      this.oPgFrm.Page2.oPag.oROAIMPOR_2_11.value=this.w_ROAIMPOR
    endif
    if not(this.oPgFrm.Page2.oPag.oROARTSER_2_13.value==this.w_ROARTSER)
      this.oPgFrm.Page2.oPag.oROARTSER_2_13.value=this.w_ROARTSER
    endif
    if not(this.oPgFrm.Page2.oPag.oDESARSE_2_14.value==this.w_DESARSE)
      this.oPgFrm.Page2.oPag.oDESARSE_2_14.value=this.w_DESARSE
    endif
    if not(this.oPgFrm.Page2.oPag.oROGRUART_2_15.value==this.w_ROGRUART)
      this.oPgFrm.Page2.oPag.oROGRUART_2_15.value=this.w_ROGRUART
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU_2_17.value==this.w_DESGRU)
      this.oPgFrm.Page2.oPag.oDESGRU_2_17.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oROSGRART_2_18.value==this.w_ROSGRART)
      this.oPgFrm.Page2.oPag.oROSGRART_2_18.value=this.w_ROSGRART
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSGR_2_20.value==this.w_DESSGR)
      this.oPgFrm.Page2.oPag.oDESSGR_2_20.value=this.w_DESSGR
    endif
    if not(this.oPgFrm.Page2.oPag.oROCODMOD_2_21.value==this.w_ROCODMOD)
      this.oPgFrm.Page2.oPag.oROCODMOD_2_21.value=this.w_ROCODMOD
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMOD_2_23.value==this.w_DESMOD)
      this.oPgFrm.Page2.oPag.oDESMOD_2_23.value=this.w_DESMOD
    endif
    if not(this.oPgFrm.Page2.oPag.oRODADTSC_2_24.value==this.w_RODADTSC)
      this.oPgFrm.Page2.oPag.oRODADTSC_2_24.value=this.w_RODADTSC
    endif
    if not(this.oPgFrm.Page2.oPag.oROADATSC_2_26.value==this.w_ROADATSC)
      this.oPgFrm.Page2.oPag.oROADATSC_2_26.value=this.w_ROADATSC
    endif
    if not(this.oPgFrm.Page2.oPag.oRODADTIV_2_28.value==this.w_RODADTIV)
      this.oPgFrm.Page2.oPag.oRODADTIV_2_28.value=this.w_RODADTIV
    endif
    if not(this.oPgFrm.Page2.oPag.oROADATIV_2_30.value==this.w_ROADATIV)
      this.oPgFrm.Page2.oPag.oROADATIV_2_30.value=this.w_ROADATIV
    endif
    if not(this.oPgFrm.Page2.oPag.oRODADTCH_2_32.value==this.w_RODADTCH)
      this.oPgFrm.Page2.oPag.oRODADTCH_2_32.value=this.w_RODADTCH
    endif
    if not(this.oPgFrm.Page2.oPag.oROADATCH_2_34.value==this.w_ROADATCH)
      this.oPgFrm.Page2.oPag.oROADATCH_2_34.value=this.w_ROADATCH
    endif
    if not(this.oPgFrm.Page2.oPag.oRODANUMD_2_36.value==this.w_RODANUMD)
      this.oPgFrm.Page2.oPag.oRODANUMD_2_36.value=this.w_RODANUMD
    endif
    if not(this.oPgFrm.Page2.oPag.oRODAALFN_2_39.value==this.w_RODAALFN)
      this.oPgFrm.Page2.oPag.oRODAALFN_2_39.value=this.w_RODAALFN
    endif
    if not(this.oPgFrm.Page2.oPag.oROANUMED_2_40.value==this.w_ROANUMED)
      this.oPgFrm.Page2.oPag.oROANUMED_2_40.value=this.w_ROANUMED
    endif
    if not(this.oPgFrm.Page2.oPag.oROAALFNU_2_43.value==this.w_ROAALFNU)
      this.oPgFrm.Page2.oPag.oROAALFNU_2_43.value=this.w_ROAALFNU
    endif
    if not(this.oPgFrm.Page2.oPag.oROPRINOM_2_46.value==this.w_ROPRINOM)
      this.oPgFrm.Page2.oPag.oROPRINOM_2_46.value=this.w_ROPRINOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRINOM_2_47.value==this.w_DESPRINOM)
      this.oPgFrm.Page2.oPag.oDESPRINOM_2_47.value=this.w_DESPRINOM
    endif
    if not(this.oPgFrm.Page2.oPag.oRODANUPR_2_50.value==this.w_RODANUPR)
      this.oPgFrm.Page2.oPag.oRODANUPR_2_50.value=this.w_RODANUPR
    endif
    if not(this.oPgFrm.Page2.oPag.oROANUMPR_2_52.value==this.w_ROANUMPR)
      this.oPgFrm.Page2.oPag.oROANUMPR_2_52.value=this.w_ROANUMPR
    endif
    if not(this.oPgFrm.Page2.oPag.oCONTATTO_2_53.value==this.w_CONTATTO)
      this.oPgFrm.Page2.oPag.oCONTATTO_2_53.value=this.w_CONTATTO
    endif
    if not(this.oPgFrm.Page2.oPag.oROCODAGE_2_55.value==this.w_ROCODAGE)
      this.oPgFrm.Page2.oPag.oROCODAGE_2_55.value=this.w_ROCODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_57.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_57.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oROCODGRN_2_58.value==this.w_ROCODGRN)
      this.oPgFrm.Page2.oPag.oROCODGRN_2_58.value=this.w_ROCODGRN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRN_2_60.value==this.w_DESGRN)
      this.oPgFrm.Page2.oPag.oDESGRN_2_60.value=this.w_DESGRN
    endif
    if not(this.oPgFrm.Page2.oPag.oROCODZON_2_61.value==this.w_ROCODZON)
      this.oPgFrm.Page2.oPag.oROCODZON_2_61.value=this.w_ROCODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZON_2_63.value==this.w_DESZON)
      this.oPgFrm.Page2.oPag.oDESZON_2_63.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page3.oPag.oROASSATT_3_3.RadioValue()==this.w_ROASSATT)
      this.oPgFrm.Page3.oPag.oROASSATT_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT0_3_5.value==this.w_TIPCAT0)
      this.oPgFrm.Page3.oPag.oTIPCAT0_3_5.value=this.w_TIPCAT0
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT0_3_6.value==this.w_DESATT0)
      this.oPgFrm.Page3.oPag.oDESATT0_3_6.value=this.w_DESATT0
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT1_3_7.value==this.w_ROCODAT1)
      this.oPgFrm.Page3.oPag.oROCODAT1_3_7.value=this.w_ROCODAT1
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT1_3_8.value==this.w_TIPCAT1)
      this.oPgFrm.Page3.oPag.oTIPCAT1_3_8.value=this.w_TIPCAT1
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT1_3_9.value==this.w_DESATT1)
      this.oPgFrm.Page3.oPag.oDESATT1_3_9.value=this.w_DESATT1
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT2_3_10.value==this.w_ROCODAT2)
      this.oPgFrm.Page3.oPag.oROCODAT2_3_10.value=this.w_ROCODAT2
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT2_3_11.value==this.w_TIPCAT2)
      this.oPgFrm.Page3.oPag.oTIPCAT2_3_11.value=this.w_TIPCAT2
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT2_3_12.value==this.w_DESATT2)
      this.oPgFrm.Page3.oPag.oDESATT2_3_12.value=this.w_DESATT2
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT3_3_13.value==this.w_ROCODAT3)
      this.oPgFrm.Page3.oPag.oROCODAT3_3_13.value=this.w_ROCODAT3
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT3_3_14.value==this.w_TIPCAT3)
      this.oPgFrm.Page3.oPag.oTIPCAT3_3_14.value=this.w_TIPCAT3
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT3_3_15.value==this.w_DESATT3)
      this.oPgFrm.Page3.oPag.oDESATT3_3_15.value=this.w_DESATT3
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT4_3_16.value==this.w_ROCODAT4)
      this.oPgFrm.Page3.oPag.oROCODAT4_3_16.value=this.w_ROCODAT4
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT4_3_17.value==this.w_TIPCAT4)
      this.oPgFrm.Page3.oPag.oTIPCAT4_3_17.value=this.w_TIPCAT4
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT4_3_18.value==this.w_DESATT4)
      this.oPgFrm.Page3.oPag.oDESATT4_3_18.value=this.w_DESATT4
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT5_3_19.value==this.w_ROCODAT5)
      this.oPgFrm.Page3.oPag.oROCODAT5_3_19.value=this.w_ROCODAT5
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT5_3_20.value==this.w_TIPCAT5)
      this.oPgFrm.Page3.oPag.oTIPCAT5_3_20.value=this.w_TIPCAT5
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT5_3_21.value==this.w_DESATT5)
      this.oPgFrm.Page3.oPag.oDESATT5_3_21.value=this.w_DESATT5
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT6_3_22.value==this.w_ROCODAT6)
      this.oPgFrm.Page3.oPag.oROCODAT6_3_22.value=this.w_ROCODAT6
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT6_3_23.value==this.w_TIPCAT6)
      this.oPgFrm.Page3.oPag.oTIPCAT6_3_23.value=this.w_TIPCAT6
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT6_3_24.value==this.w_DESATT6)
      this.oPgFrm.Page3.oPag.oDESATT6_3_24.value=this.w_DESATT6
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT7_3_25.value==this.w_ROCODAT7)
      this.oPgFrm.Page3.oPag.oROCODAT7_3_25.value=this.w_ROCODAT7
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT7_3_26.value==this.w_TIPCAT7)
      this.oPgFrm.Page3.oPag.oTIPCAT7_3_26.value=this.w_TIPCAT7
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT7_3_27.value==this.w_DESATT7)
      this.oPgFrm.Page3.oPag.oDESATT7_3_27.value=this.w_DESATT7
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT8_3_28.value==this.w_ROCODAT8)
      this.oPgFrm.Page3.oPag.oROCODAT8_3_28.value=this.w_ROCODAT8
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT8_3_29.value==this.w_TIPCAT8)
      this.oPgFrm.Page3.oPag.oTIPCAT8_3_29.value=this.w_TIPCAT8
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT8_3_30.value==this.w_DESATT8)
      this.oPgFrm.Page3.oPag.oDESATT8_3_30.value=this.w_DESATT8
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT9_3_31.value==this.w_ROCODAT9)
      this.oPgFrm.Page3.oPag.oROCODAT9_3_31.value=this.w_ROCODAT9
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPCAT9_3_32.value==this.w_TIPCAT9)
      this.oPgFrm.Page3.oPag.oTIPCAT9_3_32.value=this.w_TIPCAT9
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT9_3_33.value==this.w_DESATT9)
      this.oPgFrm.Page3.oPag.oDESATT9_3_33.value=this.w_DESATT9
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODSO0_3_36.value==this.w_ROCODSO0)
      this.oPgFrm.Page3.oPag.oROCODSO0_3_36.value=this.w_ROCODSO0
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSEZ0_3_37.value==this.w_DESSEZ0)
      this.oPgFrm.Page3.oPag.oDESSEZ0_3_37.value=this.w_DESSEZ0
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSEZ1_3_38.value==this.w_DESSEZ1)
      this.oPgFrm.Page3.oPag.oDESSEZ1_3_38.value=this.w_DESSEZ1
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSEZ2_3_39.value==this.w_DESSEZ2)
      this.oPgFrm.Page3.oPag.oDESSEZ2_3_39.value=this.w_DESSEZ2
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODSO1_3_40.value==this.w_ROCODSO1)
      this.oPgFrm.Page3.oPag.oROCODSO1_3_40.value=this.w_ROCODSO1
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODSO2_3_41.value==this.w_ROCODSO2)
      this.oPgFrm.Page3.oPag.oROCODSO2_3_41.value=this.w_ROCODSO2
    endif
    if not(this.oPgFrm.Page3.oPag.oROCODAT0_3_53.value==this.w_ROCODAT0)
      this.oPgFrm.Page3.oPag.oROCODAT0_3_53.value=this.w_ROCODAT0
    endif
    if not(this.oPgFrm.Page2.oPag.oRODANUMD_2_69.value==this.w_RODANUMD)
      this.oPgFrm.Page2.oPag.oRODANUMD_2_69.value=this.w_RODANUMD
    endif
    if not(this.oPgFrm.Page2.oPag.oRODAALFN_2_71.value==this.w_RODAALFN)
      this.oPgFrm.Page2.oPag.oRODAALFN_2_71.value=this.w_RODAALFN
    endif
    if not(this.oPgFrm.Page2.oPag.oROANUMED_2_72.value==this.w_ROANUMED)
      this.oPgFrm.Page2.oPag.oROANUMED_2_72.value=this.w_ROANUMED
    endif
    if not(this.oPgFrm.Page2.oPag.oROAALFNU_2_74.value==this.w_ROAALFNU)
      this.oPgFrm.Page2.oPag.oROAALFNU_2_74.value=this.w_ROAALFNU
    endif
    if not(this.oPgFrm.Page2.oPag.oROARTSER_2_75.value==this.w_ROARTSER)
      this.oPgFrm.Page2.oPag.oROARTSER_2_75.value=this.w_ROARTSER
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_ROADATA) OR  .w_RODADATA<= .w_ROADATA)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRODADATA_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quello finale")
          case   not(.w_RODADATA <=.w_ROADATA or empty(.w_ROADATA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oROADATA_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quello finale")
          case   not(empty(.w_RORICAL) OR  .w_RORICDAL<= .w_RORICDAL)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRORICDAL_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quello finale")
          case   not(.w_RORICDAL <=.w_RORICAL or empty(.w_RORICAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRORICAL_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quello finale")
          case   not(.w_RODANUME<= .w_ROANUMER)  and (empty(.w_ROPRIOFF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODANUME_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � maggiore di quello finale")
          case   not(.w_RODANUME<=.w_ROANUMER)  and (empty(.w_ROPRIOFF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROANUMER_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � maggiore di quello finale")
          case   not(empty(.w_ROAIMPOR) OR .w_RODAIMPO<= .w_ROAIMPOR)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODAIMPO_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'importo iniziale � maggiore di quello finale")
          case   not(.w_RODAIMPO<=.w_ROAIMPOR or empty(.w_ROAIMPOR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROAIMPOR_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'importo iniziale � maggiore di quello finale")
          case   not(empty(.w_ROADATSC) OR .w_RODADTSC<= .w_ROADATSC)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODADTSC_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(.w_RODADTSC<=.w_ROADATSC or empty(.w_ROADATSC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROADATSC_2_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(empty(.w_ROADATIV) OR .w_RODADTIV<= .w_ROADATIV)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODADTIV_2_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(.w_RODADTIV<=.w_ROADATIV or empty(.w_ROADATIV))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROADATIV_2_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(empty(.w_ROADATCH) OR .w_RODADTCH<= .w_ROADATCH)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODADTCH_2_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(.w_RODADTCH<=.w_ROADATCH or empty(.w_ROADATCH))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROADATCH_2_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(empty(.w_ROANUMED) OR .w_RODANUMD<= .w_ROANUMED)  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODANUMD_2_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � maggiore di quella finale")
          case   not(empty(.w_ROAALFNU) OR  (UPPER(.w_RODAALFN)<= UPPER(.w_ROAALFNU)))  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODAALFN_2_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore di quella finale")
          case   not(.w_RODANUMD<=.w_ROANUMED or empty(.w_ROANUMED))  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROANUMED_2_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � maggiore di quella finale")
          case   not((UPPER(.w_RODAALFN) <= UPPER(.w_ROAALFNU)) or empty(.w_ROAALFNU))  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROAALFNU_2_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore di quella finale")
          case   not(.w_RODANUPR<= .w_ROANUMPR)  and (empty(.w_ROPRINOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODANUPR_2_50.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � maggiore di quella finale")
          case   not(.w_RODANUPR<=.w_ROANUMPR)  and (empty(.w_ROPRINOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROANUMPR_2_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � maggiore di quella finale")
          case   not(empty(.w_ROANUMED) OR .w_RODANUMD<= .w_ROANUMED)  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and (upper(g_APPLICATION) = "ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODANUMD_2_69.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � maggiore di quella finale")
          case   not(empty(.w_ROAALFNU) OR  (UPPER(.w_RODAALFN)<= UPPER(.w_ROAALFNU)))  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and (upper(g_APPLICATION) = "ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODAALFN_2_71.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore di quella finale")
          case   not(.w_RODANUMD<=.w_ROANUMED or empty(.w_ROANUMED))  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and (upper(g_APPLICATION) = "ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROANUMED_2_72.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � maggiore di quella finale")
          case   not((UPPER(.w_RODAALFN) <= UPPER(.w_ROAALFNU)) or empty(.w_ROAALFNU))  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and (upper(g_APPLICATION) = "ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROAALFNU_2_74.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_kro
      * --- Disabilita tasto F10
         i_bRes=.f.
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RONOMINA = this.w_RONOMINA
    this.o_ROPRINOM = this.w_ROPRINOM
    return

enddefine

* --- Define pages as container
define class tgsof_kroPag1 as StdContainer
  Width  = 790
  height = 424
  stdWidth  = 790
  stdheight = 424
  resizeXpos=396
  resizeYpos=243
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oRONOMINA_1_1 as StdCombo with uid="GNJTDAXDDK",rtseq=1,rtrep=.f.,left=121,top=7,width=128,height=21;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 147190953;
    , cFormVar="w_RONOMINA",RowSource=""+"Tutti,"+"Da valutare,"+"Potenziale,"+"Lead,"+"Prospect,"+"Cliente,"+"Congelato,"+"Non interessante", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRONOMINA_1_1.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'L',;
    iif(this.value =5,'P',;
    iif(this.value =6,'C',;
    iif(this.value =7,'G',;
    iif(this.value =8,'N',;
    space(1))))))))))
  endfunc
  func oRONOMINA_1_1.GetRadio()
    this.Parent.oContained.w_RONOMINA = this.RadioValue()
    return .t.
  endfunc

  func oRONOMINA_1_1.SetRadio()
    this.Parent.oContained.w_RONOMINA=trim(this.Parent.oContained.w_RONOMINA)
    this.value = ;
      iif(this.Parent.oContained.w_RONOMINA=='A',1,;
      iif(this.Parent.oContained.w_RONOMINA=='T',2,;
      iif(this.Parent.oContained.w_RONOMINA=='Z',3,;
      iif(this.Parent.oContained.w_RONOMINA=='L',4,;
      iif(this.Parent.oContained.w_RONOMINA=='P',5,;
      iif(this.Parent.oContained.w_RONOMINA=='C',6,;
      iif(this.Parent.oContained.w_RONOMINA=='G',7,;
      iif(this.Parent.oContained.w_RONOMINA=='N',8,;
      0))))))))
  endfunc

  add object oROCODNOM_1_4 as StdField with uid="SOMWIURCTL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ROCODNOM", cQueryName = "ROCODNOM",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo (spazio = no selezione)",;
    HelpContextID = 72787101,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=258, Top=7, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_ROCODNOM"

  func oROCODNOM_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODNOM_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODNOM_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oROCODNOM_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF2QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oROCODNOM_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_ROCODNOM
     i_obj.ecpSave()
  endproc

  add object oDESNOM_1_5 as StdField with uid="OADXZRBATB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nominativo",;
    HelpContextID = 78032842,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=431, Top=7, InputMask=replicate('X',40)

  add object oRODADATA_1_6 as StdField with uid="MGYUCPGMQK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_RODADATA", cQueryName = "RODADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quello finale",;
    ToolTipText = "Data inizio",;
    HelpContextID = 23368873,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=121, Top=33

  func oRODADATA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROADATA) OR  .w_RODADATA<= .w_ROADATA)
    endwith
    return bRes
  endfunc

  add object oROADATA_1_7 as StdField with uid="JRTLSAMPBU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ROADATA", cQueryName = "ROADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quello finale",;
    ToolTipText = "Data fine",;
    HelpContextID = 244434154,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=258, Top=33

  func oROADATA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODADATA <=.w_ROADATA or empty(.w_ROADATA))
    endwith
    return bRes
  endfunc


  add object oROSTATO_1_8 as StdCombo with uid="RDVBWRLGQX",value=8,rtseq=8,rtrep=.f.,left=491,top=33,width=171,height=21;
    , ToolTipText = "Stato offerta";
    , HelpContextID = 243311850;
    , cFormVar="w_ROSTATO",RowSource=""+"In corso,"+"Inviate,"+"In corso+inviate,"+"Confermate,"+"Versione chiusa,"+"Sospese,"+"Rifiutate,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oROSTATO_1_8.RadioValue()
    return(iif(this.value =1,'I ',;
    iif(this.value =2,'A ',;
    iif(this.value =3,'IA',;
    iif(this.value =4,'C ',;
    iif(this.value =5,'V ',;
    iif(this.value =6,'S ',;
    iif(this.value =7,'R ',;
    iif(this.value =8,'  ',;
    space(2))))))))))
  endfunc
  func oROSTATO_1_8.GetRadio()
    this.Parent.oContained.w_ROSTATO = this.RadioValue()
    return .t.
  endfunc

  func oROSTATO_1_8.SetRadio()
    this.Parent.oContained.w_ROSTATO=trim(this.Parent.oContained.w_ROSTATO)
    this.value = ;
      iif(this.Parent.oContained.w_ROSTATO=='I',1,;
      iif(this.Parent.oContained.w_ROSTATO=='A',2,;
      iif(this.Parent.oContained.w_ROSTATO=='IA',3,;
      iif(this.Parent.oContained.w_ROSTATO=='C',4,;
      iif(this.Parent.oContained.w_ROSTATO=='V',5,;
      iif(this.Parent.oContained.w_ROSTATO=='S',6,;
      iif(this.Parent.oContained.w_ROSTATO=='R',7,;
      iif(this.Parent.oContained.w_ROSTATO=='',8,;
      0))))))))
  endfunc

  add object oRORICDAL_1_10 as StdField with uid="JMVUEQLIUN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_RORICDAL", cQueryName = "RORICDAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quello finale",;
    ToolTipText = "Ricontattare dal",;
    HelpContextID = 241939614,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=121, Top=59

  func oRORICDAL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_RORICAL) OR  .w_RORICDAL<= .w_RORICDAL)
    endwith
    return bRes
  endfunc

  add object oRORICAL_1_11 as StdField with uid="CTYQJDFETU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_RORICAL", cQueryName = "RORICAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quello finale",;
    ToolTipText = "al",;
    HelpContextID = 244599574,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=258, Top=59

  func oRORICAL_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RORICDAL <=.w_RORICAL or empty(.w_RORICAL))
    endwith
    return bRes
  endfunc

  add object oROORINOM_1_12 as StdField with uid="ANHAVSTIDO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ROORINOM", cQueryName = "ROORINOM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine reperimento nominitavo",;
    HelpContextID = 67298461,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=491, Top=59, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", cZoomOnZoom="GSAR_AON", oKey_1_1="ONCODICE", oKey_1_2="this.w_ROORINOM"

  func oROORINOM_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oROORINOM_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROORINOM_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oROORINOM_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AON',"Origine nominativi",'',this.parent.oContained
  endproc
  proc oROORINOM_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AON()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ONCODICE=this.parent.oContained.w_ROORINOM
     i_obj.ecpSave()
  endproc

  add object oRORIFDES_1_13 as StdField with uid="KANYTXGEET",rtseq=13,rtrep=.f.,;
    cFormVar = "w_RORIFDES", cQueryName = "RORIFDES",;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento descrittivo",;
    HelpContextID = 29641577,;
   bGlobalFont=.t.,;
    Height=21, Width=290, Left=121, Top=85, InputMask=replicate('X',45)

  add object oROCODUTE_1_14 as StdField with uid="LBPCMNFSTR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ROCODUTE", cQueryName = "ROCODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 44653403,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=491, Top=85, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_ROCODUTE"

  func oROCODUTE_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODUTE_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODUTE_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oROCODUTE_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oBtn_1_15 as StdButton with uid="KVDHUDVOZX",left=9, top=379, width=48,height=45,;
    CpPicture="bmp\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare l'offerta associata alla riga selezionata";
    , HelpContextID = 157186161;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        OpenGest('A',iif(Isalt(),'GSPR_AOF','GSOF_AOF'),'OFSERIAL',.w_SEROFF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SEROFF))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="ZWPGIMCCMD",left=60, top=379, width=48,height=45,;
    CpPicture="bmp\stampa.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per stampare il report offerta";
    , HelpContextID = 190037738;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        do GSOF_BOF with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SEROFF))
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="NKKEGPZYLY",left=111, top=379, width=48,height=45,;
    CpPicture="bmp\word.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il file W.P. associato";
    , HelpContextID = 226359418;
    , Caption='\<W. P.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSOF_BWP(this.Parent.oContained,"W")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SEROFF))
      endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="ARZMUCFPLW",left=162, top=379, width=48,height=45,;
    CpPicture="bmp\pdf.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il file PDF associato";
    , HelpContextID = 75365220;
    , Caption='\<PDF';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSOF_BWP(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SEROFF))
      endwith
    endif
  endfunc


  add object ZoomOff as cp_zoombox with uid="TIHUYNVHMX",left=5, top=115, width=776,height=262,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ERTE",cZoomFile="GSOF_KRO",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bNoZoomGridShape=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 206447130


  add object oOrderBy_1_21 as StdCombo with uid="ABZPFJBGDG",rtseq=15,rtrep=.f.,left=587,top=387,width=194,height=21;
    , ToolTipText = "Ordinamento";
    , HelpContextID = 44142054;
    , cFormVar="w_OrderBy",RowSource=""+"Data registrazione,"+"Data invio,"+"Data scadenza,"+"Ricontattare il,"+"Nominativo,"+"Priorit� offerta,"+"Priorit� nominativo,"+"Importo totale,"+"Operatore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOrderBy_1_21.RadioValue()
    return(iif(this.value =1,"4,11",;
    iif(this.value =2,"17,11,4",;
    iif(this.value =3,"16,11,4",;
    iif(this.value =4,"5,11,4",;
    iif(this.value =5,"2,11,4",;
    iif(this.value =6,"11 desc,4",;
    iif(this.value =7,"21 desc,11,4",;
    iif(this.value =8,"29 desc,11,4",;
    iif(this.value =9,"9,11,4",;
    space(7)))))))))))
  endfunc
  func oOrderBy_1_21.GetRadio()
    this.Parent.oContained.w_OrderBy = this.RadioValue()
    return .t.
  endfunc

  func oOrderBy_1_21.SetRadio()
    this.Parent.oContained.w_OrderBy=trim(this.Parent.oContained.w_OrderBy)
    this.value = ;
      iif(this.Parent.oContained.w_OrderBy=="4,11",1,;
      iif(this.Parent.oContained.w_OrderBy=="17,11,4",2,;
      iif(this.Parent.oContained.w_OrderBy=="16,11,4",3,;
      iif(this.Parent.oContained.w_OrderBy=="5,11,4",4,;
      iif(this.Parent.oContained.w_OrderBy=="2,11,4",5,;
      iif(this.Parent.oContained.w_OrderBy=="11 desc,4",6,;
      iif(this.Parent.oContained.w_OrderBy=="21 desc,11,4",7,;
      iif(this.Parent.oContained.w_OrderBy=="29 desc,11,4",8,;
      iif(this.Parent.oContained.w_OrderBy=="9,11,4",9,;
      0)))))))))
  endfunc


  add object oBtn_1_22 as StdButton with uid="NKKUOZFFSZ",left=733, top=7, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 207522538;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESORI_1_31 as StdField with uid="MBSYGLUBVD",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione origine nominativo",;
    HelpContextID = 141930442,;
   bGlobalFont=.t.,;
    Height=21, Width=217, Left=564, Top=59, InputMask=replicate('X',35)

  add object oDESOPE_1_34 as StdField with uid="ZAHPRRGBQA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESOPE", cQueryName = "DESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione operatore",;
    HelpContextID = 211136458,;
   bGlobalFont=.t.,;
    Height=21, Width=217, Left=564, Top=85, InputMask=replicate('X',35)

  add object oStr_1_23 as StdString with uid="AXVCKHHLKY",Visible=.t., Left=4, Top=33,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ITDTCOTAKS",Visible=.t., Left=3, Top=63,;
    Alignment=1, Width=116, Height=18,;
    Caption="Ricontattare dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="APHYIAFQKO",Visible=.t., Left=4, Top=11,;
    Alignment=1, Width=115, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="IHIBKVKHGG",Visible=.t., Left=211, Top=33,;
    Alignment=1, Width=44, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="JSIRZVRYQK",Visible=.t., Left=447, Top=33,;
    Alignment=1, Width=42, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="VTGVPSLHTP",Visible=.t., Left=423, Top=387,;
    Alignment=1, Width=152, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KYKAHQEXZP",Visible=.t., Left=236, Top=63,;
    Alignment=1, Width=19, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="JVXMMYHELG",Visible=.t., Left=370, Top=63,;
    Alignment=1, Width=119, Height=18,;
    Caption="Origine nom.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="OWTOCZDMXE",Visible=.t., Left=4, Top=89,;
    Alignment=1, Width=115, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="OSZFVPOGPW",Visible=.t., Left=421, Top=89,;
    Alignment=1, Width=68, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_kroPag2 as StdContainer
  Width  = 790
  height = 424
  stdWidth  = 790
  stdheight = 424
  resizeXpos=342
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oROPRIOFF_2_1 as StdField with uid="YWNLSAVWWQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ROPRIOFF", cQueryName = "ROPRIOFF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice priorit�",;
    HelpContextID = 217918300,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=28, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", oKey_1_1="GPCODICE", oKey_1_2="this.w_ROPRIOFF"

  func oROPRIOFF_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oROPRIOFF_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROPRIOFF_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oROPRIOFF_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Priorit� offerta",'',this.parent.oContained
  endproc

  add object oDESPRI_2_2 as StdField with uid="EPVIIPVNLO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESPRI", cQueryName = "DESPRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione priorit� offerta",;
    HelpContextID = 141864906,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=217, Top=28, InputMask=replicate('X',35)

  add object oRODANUME_2_5 as StdField with uid="EHZUUPMGUE",rtseq=20,rtrep=.f.,;
    cFormVar = "w_RODANUME", cQueryName = "RODANUME",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � maggiore di quello finale",;
    ToolTipText = "Numero iniziale priorit�",;
    HelpContextID = 214209701,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=563, Top=28, cSayPict='"999999"', cGetPict='"999999"'

  func oRODANUME_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_ROPRIOFF))
    endwith
   endif
  endfunc

  func oRODANUME_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODANUME<= .w_ROANUMER)
    endwith
    return bRes
  endfunc

  add object oROANUMER_2_7 as StdField with uid="DQHUQLNEGT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ROANUMER", cQueryName = "ROANUMER",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � maggiore di quello finale",;
    ToolTipText = "Numero finale priorit�",;
    HelpContextID = 196623208,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=704, Top=28, cSayPict='"999999"', cGetPict='"999999"'

  func oROANUMER_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_ROPRIOFF))
    endwith
   endif
  endfunc

  func oROANUMER_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODANUME<=.w_ROANUMER)
    endwith
    return bRes
  endfunc

  add object oRODAIMPO_2_9 as StdField with uid="LEKQWOWOYW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_RODAIMPO", cQueryName = "RODAIMPO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'importo iniziale � maggiore di quello finale",;
    ToolTipText = "Importo totale iniziale",;
    HelpContextID = 85234843,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=153, Top=51, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oRODAIMPO_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROAIMPOR) OR .w_RODAIMPO<= .w_ROAIMPOR)
    endwith
    return bRes
  endfunc

  add object oROAIMPOR_2_11 as StdField with uid="GAXAHLDOSC",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ROAIMPOR", cQueryName = "ROAIMPOR",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'importo iniziale � maggiore di quello finale",;
    ToolTipText = "Importo totale finale",;
    HelpContextID = 30196888,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=477, Top=51, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oROAIMPOR_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODAIMPO<=.w_ROAIMPOR or empty(.w_ROAIMPOR))
    endwith
    return bRes
  endfunc

  add object oROARTSER_2_13 as StdField with uid="GQPWUBVJTX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ROARTSER", cQueryName = "ROARTSER",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente o inesistente o obsoleto",;
    ToolTipText = "Codice di ricerca articolo/servizio",;
    HelpContextID = 28064616,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=153, Top=74, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_ROARTSER"

  func oROARTSER_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!isAhe())
    endwith
   endif
  endfunc

  func oROARTSER_2_13.mHide()
    with this.Parent.oContained
      return (isAhe())
    endwith
  endfunc

  func oROARTSER_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oROARTSER_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROARTSER_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oROARTSER_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici articoli/servizi",'GSVE_MDV.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESARSE_2_14 as StdField with uid="RMOBKJTKTU",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESARSE", cQueryName = "DESARSE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice di ricerca articolo/servizio",;
    HelpContextID = 24924214,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=323, Top=74, InputMask=replicate('X',40)

  add object oROGRUART_2_15 as StdField with uid="TMSTLNFTHE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ROGRUART", cQueryName = "ROGRUART",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 4416662,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=97, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIT_SEZI", oKey_1_1="TSCODICE", oKey_1_2="this.w_ROGRUART"

  func oROGRUART_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
      if .not. empty(.w_ROSGRART)
        bRes2=.link_2_18('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oROGRUART_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROGRUART_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIT_SEZI','*','TSCODICE',cp_AbsName(this.parent,'oROGRUART_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppo articoli",'',this.parent.oContained
  endproc

  add object oDESGRU_2_17 as StdField with uid="IOZQIOHUHK",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 209563594,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=218, Top=97, InputMask=replicate('X',35)

  add object oROSGRART_2_18 as StdField with uid="EQBWQVMMXR",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ROSGRART", cQueryName = "ROSGRART",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sottogruppo",;
    HelpContextID = 8234134,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=120, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SOT_SEZI", oKey_1_1="SGCODGRU", oKey_1_2="this.w_ROGRUART", oKey_2_1="SGCODSOT", oKey_2_2="this.w_ROSGRART"

  func oROSGRART_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ROGRUART))
    endwith
   endif
  endfunc

  func oROSGRART_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oROSGRART_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROSGRART_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SOT_SEZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStrODBC(this.Parent.oContained.w_ROGRUART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStr(this.Parent.oContained.w_ROGRUART)
    endif
    do cp_zoom with 'SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(this.parent,'oROSGRART_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sottogruppo articoli",'',this.parent.oContained
  endproc

  add object oDESSGR_2_20 as StdField with uid="KEZTITIYEM",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESSGR", cQueryName = "DESSGR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione sottogruppo",;
    HelpContextID = 2207690,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=218, Top=120, InputMask=replicate('X',35)

  add object oROCODMOD_2_21 as StdField with uid="WQZYQYIRNC",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ROCODMOD", cQueryName = "ROCODMOD",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Modello di riferimento",;
    HelpContextID = 89564326,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=143, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MOD_OFFE", oKey_1_1="MOCODICE", oKey_1_2="this.w_ROCODMOD"

  func oROCODMOD_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODMOD_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODMOD_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_OFFE','*','MOCODICE',cp_AbsName(this.parent,'oROCODMOD_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Modelli di riferimento",'',this.parent.oContained
  endproc

  add object oDESMOD_2_23 as StdField with uid="UJWNCDNSHC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESMOD", cQueryName = "DESMOD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione modello",;
    HelpContextID = 229093322,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=218, Top=143, InputMask=replicate('X',35)

  add object oRODADTSC_2_24 as StdField with uid="KVXVIMEBEA",rtseq=32,rtrep=.f.,;
    cFormVar = "w_RODADTSC", cQueryName = "RODADTSC",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Inizio data scadenza",;
    HelpContextID = 241472679,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=153, Top=166

  func oRODADTSC_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROADATSC) OR .w_RODADTSC<= .w_ROADATSC)
    endwith
    return bRes
  endfunc

  add object oROADATSC_2_26 as StdField with uid="ZZMLIYWUIV",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ROADATSC", cQueryName = "ROADATSC",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Fine data scadenza",;
    HelpContextID = 244434087,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=540, Top=166

  func oROADATSC_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODADTSC<=.w_ROADATSC or empty(.w_ROADATSC))
    endwith
    return bRes
  endfunc

  add object oRODADTIV_2_28 as StdField with uid="HVWMXRUMXM",rtseq=34,rtrep=.f.,;
    cFormVar = "w_RODADTIV", cQueryName = "RODADTIV",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Inizio data invio",;
    HelpContextID = 26962796,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=153, Top=189

  func oRODADTIV_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROADATIV) OR .w_RODADTIV<= .w_ROADATIV)
    endwith
    return bRes
  endfunc

  add object oROADATIV_2_30 as StdField with uid="IZJEQUPKZB",rtseq=35,rtrep=.f.,;
    cFormVar = "w_ROADATIV", cQueryName = "ROADATIV",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Fine data invio",;
    HelpContextID = 24001388,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=540, Top=189

  func oROADATIV_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODADTIV<=.w_ROADATIV or empty(.w_ROADATIV))
    endwith
    return bRes
  endfunc

  add object oRODADTCH_2_32 as StdField with uid="JETNTTGAAW",rtseq=36,rtrep=.f.,;
    cFormVar = "w_RODADTCH", cQueryName = "RODADTCH",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Inizio data chiusura",;
    HelpContextID = 241472674,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=153, Top=212

  func oRODADTCH_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROADATCH) OR .w_RODADTCH<= .w_ROADATCH)
    endwith
    return bRes
  endfunc

  add object oROADATCH_2_34 as StdField with uid="XTTTZUOQFH",rtseq=37,rtrep=.f.,;
    cFormVar = "w_ROADATCH", cQueryName = "ROADATCH",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Fine data chiusura",;
    HelpContextID = 244434082,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=540, Top=212

  func oROADATCH_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODADTCH<=.w_ROADATCH or empty(.w_ROADATCH))
    endwith
    return bRes
  endfunc

  add object oRODANUMD_2_36 as StdField with uid="AOPEDDZOLF",rtseq=38,rtrep=.f.,;
    cFormVar = "w_RODANUMD", cQueryName = "RODANUMD",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � maggiore di quella finale",;
    ToolTipText = "Inizio numero documento",;
    HelpContextID = 214209702,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=153, Top=235, cSayPict='"999999"', cGetPict='"999999"'

  func oRODANUMD_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oRODANUMD_2_36.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oRODANUMD_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROANUMED) OR .w_RODANUMD<= .w_ROANUMED)
    endwith
    return bRes
  endfunc

  add object oRODAALFN_2_39 as StdField with uid="VMEJSOIBZJ",rtseq=39,rtrep=.f.,;
    cFormVar = "w_RODAALFN", cQueryName = "RODAALFN",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore di quella finale",;
    ToolTipText = "Inizio serie del documento",;
    HelpContextID = 158034788,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=224, Top=235, InputMask=replicate('X',2)

  func oRODAALFN_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oRODAALFN_2_39.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oRODAALFN_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROAALFNU) OR  (UPPER(.w_RODAALFN)<= UPPER(.w_ROAALFNU)))
    endwith
    return bRes
  endfunc

  add object oROANUMED_2_40 as StdField with uid="EMBRGHCBGP",rtseq=40,rtrep=.f.,;
    cFormVar = "w_ROANUMED", cQueryName = "ROANUMED",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � maggiore di quella finale",;
    ToolTipText = "Fine numero documento",;
    HelpContextID = 196623194,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=540, Top=235, cSayPict='"999999"', cGetPict='"999999"'

  func oROANUMED_2_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oROANUMED_2_40.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oROANUMED_2_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODANUMD<=.w_ROANUMED or empty(.w_ROANUMED))
    endwith
    return bRes
  endfunc

  add object oROAALFNU_2_43 as StdField with uid="NYGLEPSZWW",rtseq=41,rtrep=.f.,;
    cFormVar = "w_ROAALFNU", cQueryName = "ROAALFNU",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore di quella finale",;
    ToolTipText = "Fine serie del documento",;
    HelpContextID = 199541909,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=609, Top=235, InputMask=replicate('X',2)

  func oROAALFNU_2_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oROAALFNU_2_43.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oROAALFNU_2_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((UPPER(.w_RODAALFN) <= UPPER(.w_ROAALFNU)) or empty(.w_ROAALFNU))
    endwith
    return bRes
  endfunc

  add object oROPRINOM_2_46 as StdField with uid="VZEOJBCNWL",rtseq=42,rtrep=.f.,;
    cFormVar = "w_ROPRINOM", cQueryName = "ROPRINOM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice priorit� nominativo",;
    HelpContextID = 67294365,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=283, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", oKey_1_1="GPCODICE", oKey_1_2="this.w_ROPRINOM"

  func oROPRINOM_2_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oROPRINOM_2_46.ecpDrop(oSource)
    this.Parent.oContained.link_2_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROPRINOM_2_46.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oROPRINOM_2_46'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Priorit� offerta",'',this.parent.oContained
  endproc

  add object oDESPRINOM_2_47 as StdField with uid="ZCNAVCFEOM",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESPRINOM", cQueryName = "DESPRINOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione priorit� nominativo",;
    HelpContextID = 141863595,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=216, Top=283, InputMask=replicate('X',35)

  add object oRODANUPR_2_50 as StdField with uid="LLIJMNESOR",rtseq=44,rtrep=.f.,;
    cFormVar = "w_RODANUPR", cQueryName = "RODANUPR",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � maggiore di quella finale",;
    ToolTipText = "Numero iniziale priorit�",;
    HelpContextID = 214209688,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=563, Top=283, cSayPict='"999999"', cGetPict='"999999"'

  func oRODANUPR_2_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_ROPRINOM))
    endwith
   endif
  endfunc

  func oRODANUPR_2_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODANUPR<= .w_ROANUMPR)
    endwith
    return bRes
  endfunc

  add object oROANUMPR_2_52 as StdField with uid="RXLNYRKQBB",rtseq=45,rtrep=.f.,;
    cFormVar = "w_ROANUMPR", cQueryName = "ROANUMPR",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � maggiore di quella finale",;
    ToolTipText = "Numero finale priorit�",;
    HelpContextID = 71812248,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=704, Top=283, cSayPict='"999999"', cGetPict='"999999"'

  func oROANUMPR_2_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_ROPRINOM))
    endwith
   endif
  endfunc

  func oROANUMPR_2_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODANUPR<=.w_ROANUMPR)
    endwith
    return bRes
  endfunc

  add object oCONTATTO_2_53 as StdField with uid="LMRWBNSIXK",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CONTATTO", cQueryName = "CONTATTO",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contatto",;
    HelpContextID = 25102965,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=153, Top=307, InputMask=replicate('X',40)

  add object oROCODAGE_2_55 as StdField with uid="LQVOAJZYZP",rtseq=47,rtrep=.f.,;
    cFormVar = "w_ROCODAGE", cQueryName = "ROCODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente che cura i rapporti",;
    HelpContextID = 245979995,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=331, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_ROCODAGE"

  func oROCODAGE_2_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_55('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAGE_2_55.ecpDrop(oSource)
    this.Parent.oContained.link_2_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAGE_2_55.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oROCODAGE_2_55'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oDESAGE_2_57 as StdField with uid="WHJZLELBYA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 221491146,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=218, Top=331, InputMask=replicate('X',35)

  add object oROCODGRN_2_58 as StdField with uid="KWVYNTKKHT",rtseq=49,rtrep=.f.,;
    cFormVar = "w_ROCODGRN", cQueryName = "ROCODGRN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo di appartenenza",;
    HelpContextID = 190227612,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=355, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", oKey_1_1="GNCODICE", oKey_1_2="this.w_ROCODGRN"

  func oROCODGRN_2_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_58('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODGRN_2_58.ecpDrop(oSource)
    this.Parent.oContained.link_2_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODGRN_2_58.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oROCODGRN_2_58'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi nominativi",'',this.parent.oContained
  endproc

  add object oDESGRN_2_60 as StdField with uid="GSFYMKILXH",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESGRN", cQueryName = "DESGRN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo nominativo",;
    HelpContextID = 58568650,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=218, Top=355, InputMask=replicate('X',35)

  add object oROCODZON_2_61 as StdField with uid="BVGNYBPIKV",rtseq=51,rtrep=.f.,;
    cFormVar = "w_ROCODZON", cQueryName = "ROCODZON",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della zona di appartenenza",;
    HelpContextID = 139895964,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=153, Top=380, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_ROCODZON"

  func oROCODZON_2_61.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_61('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODZON_2_61.ecpDrop(oSource)
    this.Parent.oContained.link_2_61('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODZON_2_61.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oROCODZON_2_61'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Zone",'',this.parent.oContained
  endproc

  add object oDESZON_2_63 as StdField with uid="XCNJCAGMME",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione zona di appartenenza",;
    HelpContextID = 60469194,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=218, Top=380, InputMask=replicate('X',35)


  add object oObj_2_67 as cp_runprogram with uid="UDJJOCDODD",left=9, top=442, width=340,height=19,;
    caption='GSOF_BR3(O) Priorit� default',;
   bGlobalFont=.t.,;
    prg="GSOF_BR3('O')",;
    cEvent = "w_ROPRIOFF Changed",;
    nPag=2;
    , HelpContextID = 2692066

  add object oRODANUMD_2_69 as StdField with uid="CVWSXLREGR",rtseq=121,rtrep=.f.,;
    cFormVar = "w_RODANUMD", cQueryName = "RODANUMD",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � maggiore di quella finale",;
    ToolTipText = "Inizio numero documento",;
    HelpContextID = 214209702,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=153, Top=235, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oRODANUMD_2_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oRODANUMD_2_69.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oRODANUMD_2_69.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROANUMED) OR .w_RODANUMD<= .w_ROANUMED)
    endwith
    return bRes
  endfunc

  add object oRODAALFN_2_71 as StdField with uid="MAHINUPKXM",rtseq=122,rtrep=.f.,;
    cFormVar = "w_RODAALFN", cQueryName = "RODAALFN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore di quella finale",;
    ToolTipText = "Inizio serie del documento",;
    HelpContextID = 158034788,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=281, Top=235, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oRODAALFN_2_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oRODAALFN_2_71.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oRODAALFN_2_71.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROAALFNU) OR  (UPPER(.w_RODAALFN)<= UPPER(.w_ROAALFNU)))
    endwith
    return bRes
  endfunc

  add object oROANUMED_2_72 as StdField with uid="GMFDYAAVUF",rtseq=123,rtrep=.f.,;
    cFormVar = "w_ROANUMED", cQueryName = "ROANUMED",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � maggiore di quella finale",;
    ToolTipText = "Fine numero documento",;
    HelpContextID = 196623194,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=540, Top=235, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oROANUMED_2_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oROANUMED_2_72.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oROANUMED_2_72.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODANUMD<=.w_ROANUMED or empty(.w_ROANUMED))
    endwith
    return bRes
  endfunc

  add object oROAALFNU_2_74 as StdField with uid="FDZXBLBOED",rtseq=124,rtrep=.f.,;
    cFormVar = "w_ROAALFNU", cQueryName = "ROAALFNU",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore di quella finale",;
    ToolTipText = "Fine serie del documento",;
    HelpContextID = 199541909,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=669, Top=235, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oROAALFNU_2_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oROAALFNU_2_74.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oROAALFNU_2_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((UPPER(.w_RODAALFN) <= UPPER(.w_ROAALFNU)) or empty(.w_ROAALFNU))
    endwith
    return bRes
  endfunc

  add object oROARTSER_2_75 as StdField with uid="XJWJAJUTWQ",rtseq=125,rtrep=.f.,;
    cFormVar = "w_ROARTSER", cQueryName = "ROARTSER",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente o inesistente o obsoleto",;
    ToolTipText = "Codice di ricerca articolo/servizio",;
    HelpContextID = 28064616,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=153, Top=74, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_ROARTSER"

  func oROARTSER_2_75.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (isAhe())
    endwith
   endif
  endfunc

  func oROARTSER_2_75.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  func oROARTSER_2_75.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_75('Part',this)
    endwith
    return bRes
  endfunc

  proc oROARTSER_2_75.ecpDrop(oSource)
    this.Parent.oContained.link_2_75('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROARTSER_2_75.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oROARTSER_2_75'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici articoli/servizi",'GSVE0MDV.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oStr_2_3 as StdString with uid="EDJOGPKJEM",Visible=.t., Left=24, Top=31,;
    Alignment=1, Width=127, Height=18,;
    Caption="Priorit� offerta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="JUYFEEYIUQ",Visible=.t., Left=478, Top=30,;
    Alignment=1, Width=82, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="APQZEGHCXQ",Visible=.t., Left=626, Top=31,;
    Alignment=1, Width=75, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="HDJIJXZFYC",Visible=.t., Left=24, Top=54,;
    Alignment=1, Width=127, Height=18,;
    Caption="Da importo totale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="ZHPYLEZKJO",Visible=.t., Left=321, Top=55,;
    Alignment=1, Width=153, Height=18,;
    Caption="A importo totale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="JEHHWZJIBI",Visible=.t., Left=24, Top=77,;
    Alignment=1, Width=127, Height=18,;
    Caption="Articolo/servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="TUZBJPMECW",Visible=.t., Left=24, Top=100,;
    Alignment=1, Width=127, Height=18,;
    Caption="Gruppo articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="XTZVVUWCSZ",Visible=.t., Left=24, Top=123,;
    Alignment=1, Width=127, Height=18,;
    Caption="Sottogruppo articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="NVNQZICSUW",Visible=.t., Left=24, Top=146,;
    Alignment=1, Width=127, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="BKYDNUAUDZ",Visible=.t., Left=24, Top=169,;
    Alignment=1, Width=127, Height=18,;
    Caption="Da data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="JHFPCKDBWI",Visible=.t., Left=394, Top=170,;
    Alignment=1, Width=143, Height=18,;
    Caption="A data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="OZWCZVROLM",Visible=.t., Left=24, Top=192,;
    Alignment=1, Width=127, Height=18,;
    Caption="Da data invio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="EYLDODRJXT",Visible=.t., Left=394, Top=193,;
    Alignment=1, Width=143, Height=18,;
    Caption="A data invio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="RFLTHTPFXR",Visible=.t., Left=24, Top=215,;
    Alignment=1, Width=127, Height=18,;
    Caption="Da data chiusura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="HHGMDDYCZR",Visible=.t., Left=394, Top=216,;
    Alignment=1, Width=143, Height=18,;
    Caption="A data chiusura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="QBAOJQIYEP",Visible=.t., Left=24, Top=238,;
    Alignment=1, Width=127, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="VNBAYQXYEN",Visible=.t., Left=211, Top=239,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_38.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oStr_2_41 as StdString with uid="DPHXUCLEUO",Visible=.t., Left=394, Top=239,;
    Alignment=1, Width=143, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="RCKKRPLSRT",Visible=.t., Left=604, Top=239,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_42.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="AAZSYJHRGL",Visible=.t., Left=19, Top=8,;
    Alignment=0, Width=124, Height=18,;
    Caption="Offerta"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_48 as StdString with uid="JLJHFNHXJX",Visible=.t., Left=24, Top=286,;
    Alignment=1, Width=127, Height=18,;
    Caption="Priorit� nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="YMIUHSFKYU",Visible=.t., Left=479, Top=285,;
    Alignment=1, Width=81, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="EYDKCRMFEQ",Visible=.t., Left=626, Top=286,;
    Alignment=1, Width=75, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="OGHPSGGIOC",Visible=.t., Left=24, Top=310,;
    Alignment=1, Width=127, Height=18,;
    Caption="Contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="DVEJNSSOSS",Visible=.t., Left=24, Top=334,;
    Alignment=1, Width=127, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="LLWFYOYTYM",Visible=.t., Left=24, Top=358,;
    Alignment=1, Width=127, Height=18,;
    Caption="Gruppo nom.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_62 as StdString with uid="WBDNNHMHPX",Visible=.t., Left=24, Top=383,;
    Alignment=1, Width=127, Height=18,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="TDKCESPJQT",Visible=.t., Left=19, Top=263,;
    Alignment=0, Width=185, Height=18,;
    Caption="Nominativo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_70 as StdString with uid="PCPMVIDJPW",Visible=.t., Left=272, Top=239,;
    Alignment=0, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_70.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_2_73 as StdString with uid="RNJBSCVGVI",Visible=.t., Left=660, Top=239,;
    Alignment=0, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_73.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oBox_2_44 as StdBox with uid="BDXIUJZIRY",left=18, top=22, width=750,height=241

  add object oBox_2_64 as StdBox with uid="CHTRPIZUJH",left=18, top=276, width=750,height=132
enddefine
define class tgsof_kroPag3 as StdContainer
  Width  = 790
  height = 424
  stdWidth  = 790
  stdheight = 424
  resizeXpos=466
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oROASSATT_3_3 as StdCombo with uid="VRQGJZOLQU",rtseq=54,rtrep=.f.,left=195,top=8,width=87,height=21;
    , ToolTipText = "Specificare se gli attributi si riferiscono alle offerte o a i nominativi";
    , HelpContextID = 6472854;
    , cFormVar="w_ROASSATT",RowSource=""+"Offerte,"+"Nominativi", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oROASSATT_3_3.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oROASSATT_3_3.GetRadio()
    this.Parent.oContained.w_ROASSATT = this.RadioValue()
    return .t.
  endfunc

  func oROASSATT_3_3.SetRadio()
    this.Parent.oContained.w_ROASSATT=trim(this.Parent.oContained.w_ROASSATT)
    this.value = ;
      iif(this.Parent.oContained.w_ROASSATT=='O',1,;
      iif(this.Parent.oContained.w_ROASSATT=='N',2,;
      0))
  endfunc

  add object oTIPCAT0_3_5 as StdField with uid="DIJYTPPTAV",rtseq=55,rtrep=.f.,;
    cFormVar = "w_TIPCAT0", cQueryName = "TIPCAT0",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 1",;
    HelpContextID = 244439754,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=59, InputMask=replicate('X',5)

  add object oDESATT0_3_6 as StdField with uid="HMQDUTQTVT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESATT0", cQueryName = "DESATT0",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 1",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=59, InputMask=replicate('X',35)

  add object oROCODAT1_3_7 as StdField with uid="DLOEXUGDHN",rtseq=57,rtrep=.f.,;
    cFormVar = "w_ROCODAT1", cQueryName = "ROCODAT1",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 2",;
    HelpContextID = 22455481,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=83, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT1"

  func oROCODAT1_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODAT0)))
    endwith
   endif
  endfunc

  func oROCODAT1_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT1_3_7.ecpDrop(oSource)
    this.Parent.oContained.link_3_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT1_3_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT1_3_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oTIPCAT1_3_8 as StdField with uid="IHAWWFIEOT",rtseq=58,rtrep=.f.,;
    cFormVar = "w_TIPCAT1", cQueryName = "TIPCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 2",;
    HelpContextID = 244439754,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=83, InputMask=replicate('X',5)

  add object oDESATT1_3_9 as StdField with uid="WDFDSCLFND",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESATT1", cQueryName = "DESATT1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 2",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=83, InputMask=replicate('X',35)

  add object oROCODAT2_3_10 as StdField with uid="HNBJFMRUWX",rtseq=60,rtrep=.f.,;
    cFormVar = "w_ROCODAT2", cQueryName = "ROCODAT2",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 3",;
    HelpContextID = 22455480,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=107, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT2"

  func oROCODAT2_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODAT1)))
    endwith
   endif
  endfunc

  func oROCODAT2_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT2_3_10.ecpDrop(oSource)
    this.Parent.oContained.link_3_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT2_3_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT2_3_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oTIPCAT2_3_11 as StdField with uid="DNYNCUUKHI",rtseq=61,rtrep=.f.,;
    cFormVar = "w_TIPCAT2", cQueryName = "TIPCAT2",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 3",;
    HelpContextID = 244439754,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=107, InputMask=replicate('X',5)

  add object oDESATT2_3_12 as StdField with uid="MAJTQMWKHR",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESATT2", cQueryName = "DESATT2",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 3",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=107, InputMask=replicate('X',35)

  add object oROCODAT3_3_13 as StdField with uid="BNQEYHQYDN",rtseq=63,rtrep=.f.,;
    cFormVar = "w_ROCODAT3", cQueryName = "ROCODAT3",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 4",;
    HelpContextID = 22455479,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=131, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT3"

  func oROCODAT3_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODAT2)))
    endwith
   endif
  endfunc

  func oROCODAT3_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT3_3_13.ecpDrop(oSource)
    this.Parent.oContained.link_3_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT3_3_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT3_3_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oTIPCAT3_3_14 as StdField with uid="TJHGYLKYBE",rtseq=64,rtrep=.f.,;
    cFormVar = "w_TIPCAT3", cQueryName = "TIPCAT3",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 4",;
    HelpContextID = 244439754,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=131, InputMask=replicate('X',5)

  add object oDESATT3_3_15 as StdField with uid="IELFGCXART",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESATT3", cQueryName = "DESATT3",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 4",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=131, InputMask=replicate('X',35)

  add object oROCODAT4_3_16 as StdField with uid="STFSYPYSZH",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ROCODAT4", cQueryName = "ROCODAT4",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 5",;
    HelpContextID = 22455478,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=155, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT4"

  func oROCODAT4_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODAT3)))
    endwith
   endif
  endfunc

  func oROCODAT4_3_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT4_3_16.ecpDrop(oSource)
    this.Parent.oContained.link_3_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT4_3_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT4_3_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oTIPCAT4_3_17 as StdField with uid="GRIEXBOQRJ",rtseq=67,rtrep=.f.,;
    cFormVar = "w_TIPCAT4", cQueryName = "TIPCAT4",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 5",;
    HelpContextID = 23995702,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=155, InputMask=replicate('X',5)

  add object oDESATT4_3_18 as StdField with uid="TTVSRHBSED",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DESATT4", cQueryName = "DESATT4",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 5",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=155, InputMask=replicate('X',35)

  add object oROCODAT5_3_19 as StdField with uid="BHBXERTFYU",rtseq=69,rtrep=.f.,;
    cFormVar = "w_ROCODAT5", cQueryName = "ROCODAT5",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 6",;
    HelpContextID = 22455477,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=179, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT5"

  func oROCODAT5_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODAT4)))
    endwith
   endif
  endfunc

  func oROCODAT5_3_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT5_3_19.ecpDrop(oSource)
    this.Parent.oContained.link_3_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT5_3_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT5_3_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oTIPCAT5_3_20 as StdField with uid="OIFMUSXBHE",rtseq=70,rtrep=.f.,;
    cFormVar = "w_TIPCAT5", cQueryName = "TIPCAT5",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 6",;
    HelpContextID = 23995702,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=179, InputMask=replicate('X',5)

  add object oDESATT5_3_21 as StdField with uid="VHYSDADBOD",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DESATT5", cQueryName = "DESATT5",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 6",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=179, InputMask=replicate('X',35)

  add object oROCODAT6_3_22 as StdField with uid="AIDJNARPHX",rtseq=72,rtrep=.f.,;
    cFormVar = "w_ROCODAT6", cQueryName = "ROCODAT6",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 7",;
    HelpContextID = 22455476,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=203, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT6"

  func oROCODAT6_3_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODAT5)))
    endwith
   endif
  endfunc

  func oROCODAT6_3_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT6_3_22.ecpDrop(oSource)
    this.Parent.oContained.link_3_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT6_3_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT6_3_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oTIPCAT6_3_23 as StdField with uid="RSFDEPETDJ",rtseq=73,rtrep=.f.,;
    cFormVar = "w_TIPCAT6", cQueryName = "TIPCAT6",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 7",;
    HelpContextID = 23995702,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=203, InputMask=replicate('X',5)

  add object oDESATT6_3_24 as StdField with uid="FOTTNHBWMW",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DESATT6", cQueryName = "DESATT6",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 7",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=203, InputMask=replicate('X',35)

  add object oROCODAT7_3_25 as StdField with uid="MWEBONAXRD",rtseq=75,rtrep=.f.,;
    cFormVar = "w_ROCODAT7", cQueryName = "ROCODAT7",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 8",;
    HelpContextID = 22455475,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=227, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT7"

  func oROCODAT7_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODAT6)))
    endwith
   endif
  endfunc

  func oROCODAT7_3_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT7_3_25.ecpDrop(oSource)
    this.Parent.oContained.link_3_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT7_3_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT7_3_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oTIPCAT7_3_26 as StdField with uid="LSADKCGIOL",rtseq=76,rtrep=.f.,;
    cFormVar = "w_TIPCAT7", cQueryName = "TIPCAT7",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 8",;
    HelpContextID = 23995702,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=227, InputMask=replicate('X',5)

  add object oDESATT7_3_27 as StdField with uid="KBCELBEALV",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DESATT7", cQueryName = "DESATT7",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 8",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=227, InputMask=replicate('X',35)

  add object oROCODAT8_3_28 as StdField with uid="DTWRSHIQBR",rtseq=78,rtrep=.f.,;
    cFormVar = "w_ROCODAT8", cQueryName = "ROCODAT8",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 9",;
    HelpContextID = 22455474,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=251, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT8"

  func oROCODAT8_3_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODAT7)))
    endwith
   endif
  endfunc

  func oROCODAT8_3_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT8_3_28.ecpDrop(oSource)
    this.Parent.oContained.link_3_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT8_3_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT8_3_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oTIPCAT8_3_29 as StdField with uid="FGXUEWYPRB",rtseq=79,rtrep=.f.,;
    cFormVar = "w_TIPCAT8", cQueryName = "TIPCAT8",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 9",;
    HelpContextID = 23995702,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=251, InputMask=replicate('X',5)

  add object oDESATT8_3_30 as StdField with uid="UHXRCKAJZN",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DESATT8", cQueryName = "DESATT8",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 9",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=251, InputMask=replicate('X',35)

  add object oROCODAT9_3_31 as StdField with uid="UQSEZFBHPS",rtseq=81,rtrep=.f.,;
    cFormVar = "w_ROCODAT9", cQueryName = "ROCODAT9",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 10",;
    HelpContextID = 22455473,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=276, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT9"

  func oROCODAT9_3_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODAT8)))
    endwith
   endif
  endfunc

  func oROCODAT9_3_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT9_3_31.ecpDrop(oSource)
    this.Parent.oContained.link_3_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT9_3_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT9_3_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oTIPCAT9_3_32 as StdField with uid="DGHCHJFBCX",rtseq=82,rtrep=.f.,;
    cFormVar = "w_TIPCAT9", cQueryName = "TIPCAT9",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo categoria attributo 10",;
    HelpContextID = 23995702,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=276, InputMask=replicate('X',5)

  add object oDESATT9_3_33 as StdField with uid="CBRIXWXQMU",rtseq=83,rtrep=.f.,;
    cFormVar = "w_DESATT9", cQueryName = "DESATT9",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo 10",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=349, Top=275, InputMask=replicate('X',35)

  add object oROCODSO0_3_36 as StdField with uid="SHKZVYIWHM",rtseq=84,rtrep=.f.,;
    cFormVar = "w_ROCODSO0", cQueryName = "ROCODSO0",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice sezioni offerta 1",;
    HelpContextID = 257336506,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=339, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SEZ_OFFE", oKey_1_1="SOCODICE", oKey_1_2="this.w_ROCODSO0"

  func oROCODSO0_3_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODSO0_3_36.ecpDrop(oSource)
    this.Parent.oContained.link_3_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODSO0_3_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SEZ_OFFE','*','SOCODICE',cp_AbsName(this.parent,'oROCODSO0_3_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sezioni offerta",'',this.parent.oContained
  endproc

  add object oDESSEZ0_3_37 as StdField with uid="TWJFPXIKPM",rtseq=85,rtrep=.f.,;
    cFormVar = "w_DESSEZ0", cQueryName = "DESSEZ0",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione sezione offerta 1",;
    HelpContextID = 138522570,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=298, Top=339, InputMask=replicate('X',35)

  add object oDESSEZ1_3_38 as StdField with uid="PHQQURSGQH",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DESSEZ1", cQueryName = "DESSEZ1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione sezione offerta 2",;
    HelpContextID = 138522570,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=298, Top=363, InputMask=replicate('X',35)

  add object oDESSEZ2_3_39 as StdField with uid="YXEMLVSOAG",rtseq=87,rtrep=.f.,;
    cFormVar = "w_DESSEZ2", cQueryName = "DESSEZ2",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione sezione offerta 3",;
    HelpContextID = 138522570,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=298, Top=387, InputMask=replicate('X',35)

  add object oROCODSO1_3_40 as StdField with uid="VFWGVWSECO",rtseq=88,rtrep=.f.,;
    cFormVar = "w_ROCODSO1", cQueryName = "ROCODSO1",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice sezioni offerta 2",;
    HelpContextID = 257336505,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=363, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SEZ_OFFE", oKey_1_1="SOCODICE", oKey_1_2="this.w_ROCODSO1"

  func oROCODSO1_3_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODSO0)))
    endwith
   endif
  endfunc

  func oROCODSO1_3_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODSO1_3_40.ecpDrop(oSource)
    this.Parent.oContained.link_3_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODSO1_3_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SEZ_OFFE','*','SOCODICE',cp_AbsName(this.parent,'oROCODSO1_3_40'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sezioni offerta",'',this.parent.oContained
  endproc

  add object oROCODSO2_3_41 as StdField with uid="ZPKAUBXCSP",rtseq=89,rtrep=.f.,;
    cFormVar = "w_ROCODSO2", cQueryName = "ROCODSO2",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice sezioni offerta 3",;
    HelpContextID = 257336504,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=387, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SEZ_OFFE", oKey_1_1="SOCODICE", oKey_1_2="this.w_ROCODSO2"

  func oROCODSO2_3_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_ROCODSO1)))
    endwith
   endif
  endfunc

  func oROCODSO2_3_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODSO2_3_41.ecpDrop(oSource)
    this.Parent.oContained.link_3_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODSO2_3_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SEZ_OFFE','*','SOCODICE',cp_AbsName(this.parent,'oROCODSO2_3_41'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sezioni offerta",'',this.parent.oContained
  endproc


  add object oObj_3_42 as cp_runprogram with uid="QZVAIZWTMN",left=6, top=486, width=252,height=19,;
    caption='GSOF_BR1(1)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('1')",;
    cEvent = "w_ROCODAT1 Changed",;
    nPag=3;
    , HelpContextID = 246160873


  add object oObj_3_43 as cp_runprogram with uid="URHTLXDAIM",left=6, top=509, width=252,height=19,;
    caption='GSOF_BR1(2)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('2')",;
    cEvent = "w_ROCODAT2 Changed",;
    nPag=3;
    , HelpContextID = 246160617


  add object oObj_3_44 as cp_runprogram with uid="JCQTFSQSTQ",left=267, top=440, width=252,height=19,;
    caption='GSOF_BR1(3)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('3')",;
    cEvent = "w_ROCODAT3 Changed",;
    nPag=3;
    , HelpContextID = 246160361


  add object oObj_3_45 as cp_runprogram with uid="IBZBWLHCTN",left=267, top=463, width=252,height=19,;
    caption='GSOF_BR1(4)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('4')",;
    cEvent = "w_ROCODAT4 Changed",;
    nPag=3;
    , HelpContextID = 246160105


  add object oObj_3_46 as cp_runprogram with uid="SDUBYIXGLH",left=267, top=486, width=252,height=19,;
    caption='GSOF_BR1(5)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('5')",;
    cEvent = "w_ROCODAT5 Changed",;
    nPag=3;
    , HelpContextID = 246159849


  add object oObj_3_47 as cp_runprogram with uid="EWFTGGTQQC",left=267, top=509, width=252,height=19,;
    caption='GSOF_BR1(6)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('6')",;
    cEvent = "w_ROCODAT6 Changed",;
    nPag=3;
    , HelpContextID = 246159593


  add object oObj_3_48 as cp_runprogram with uid="CIFIBTHWXX",left=527, top=440, width=252,height=19,;
    caption='GSOF_BR1(7)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('7')",;
    cEvent = "w_ROCODAT7 Changed",;
    nPag=3;
    , HelpContextID = 246159337


  add object oObj_3_49 as cp_runprogram with uid="HIGPQZOEIW",left=527, top=463, width=252,height=19,;
    caption='GSOF_BR1(8)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('8')",;
    cEvent = "w_ROCODAT8 Changed",;
    nPag=3;
    , HelpContextID = 246159081


  add object oObj_3_50 as cp_runprogram with uid="QAYJAWUWIS",left=527, top=486, width=252,height=19,;
    caption='GSOF_BR1(0)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('0')",;
    cEvent = "w_ROCODAT0 Changed",;
    nPag=3;
    , HelpContextID = 246161129


  add object oObj_3_51 as cp_runprogram with uid="COUVQRLLXY",left=6, top=440, width=252,height=19,;
    caption='GSOF_BR1(A)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('A')",;
    cEvent = "w_ROCODSO0 Changed",;
    nPag=3;
    , HelpContextID = 246156777


  add object oObj_3_52 as cp_runprogram with uid="UZGOHTXSHL",left=6, top=463, width=252,height=19,;
    caption='GSOF_BR1(B)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR1('B')",;
    cEvent = "w_ROCODSO1 Changed",;
    nPag=3;
    , HelpContextID = 246156521

  add object oROCODAT0_3_53 as StdField with uid="SWLVYAGIXY",rtseq=95,rtrep=.f.,;
    cFormVar = "w_ROCODAT0", cQueryName = "ROCODAT0",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 1",;
    HelpContextID = 22455482,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=195, Top=59, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_ROCODAT0"

  func oROCODAT0_3_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_53('Part',this)
    endwith
    return bRes
  endfunc

  proc oROCODAT0_3_53.ecpDrop(oSource)
    this.Parent.oContained.link_3_53('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROCODAT0_3_53.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oROCODAT0_3_53'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oStr_3_2 as StdString with uid="CSUZZTDXVM",Visible=.t., Left=187, Top=39,;
    Alignment=0, Width=157, Height=18,;
    Caption="Attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_4 as StdString with uid="SLPWBWDEHQ",Visible=.t., Left=4, Top=10,;
    Alignment=1, Width=187, Height=18,;
    Caption="Associazione attributi:"  ;
  , bGlobalFont=.t.

  add object oStr_3_35 as StdString with uid="UCOTYUCNBK",Visible=.t., Left=187, Top=319,;
    Alignment=0, Width=206, Height=18,;
    Caption="Sezioni offerta"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_1 as StdBox with uid="UDOGSTCFQB",left=186, top=53, width=429,height=252

  add object oBox_3_34 as StdBox with uid="BNTHQROOZO",left=186, top=333, width=375,height=82
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_kro','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
