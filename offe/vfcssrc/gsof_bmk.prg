* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bmk                                                        *
*              Elimina riferimento al documenti di offerta                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_220]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-24                                                      *
* Last revis.: 2003-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bmk",oParentObject,m.pTIPOPE,m.pSERIAL)
return(i_retval)

define class tgsof_bmk as StdBatch
  * --- Local variables
  pTIPOPE = space(1)
  pSERIAL = space(10)
  w_SERIAL = space(10)
  w_SEROFF = space(10)
  * --- WorkFile variables
  OFF_ERTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Riferimento al Documento di Offerta (da GSVE_BMK)
    * --- Definizione variabili
    this.w_SERIAL = this.pSERIAL
    if NOT EMPTY(this.w_SERIAL)
      do case
        case this.pTIPOPE="D"
          * --- Elimina Riferimento
          * --- Select from OFF_ERTE
          i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFF_ERTE ";
                +" where OFRIFDOC="+cp_ToStrODBC(this.w_SERIAL)+"";
                 ,"_Curs_OFF_ERTE")
          else
            select * from (i_cTable);
             where OFRIFDOC=this.w_SERIAL;
              into cursor _Curs_OFF_ERTE
          endif
          if used('_Curs_OFF_ERTE')
            select _Curs_OFF_ERTE
            locate for 1=1
            do while not(eof())
            this.w_SEROFF = NVL(_Curs_OFF_ERTE.OFSERIAL, "")
            if NOT EMPTY(this.w_SEROFF)
              * --- Write into OFF_ERTE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OFRIFDOC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'OFF_ERTE','OFRIFDOC');
                +",OFSTATUS ="+cp_NullLink(cp_ToStrODBC("I"),'OFF_ERTE','OFSTATUS');
                +",OFDATINV ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'OFF_ERTE','OFDATINV');
                +",OFDATCHI ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'OFF_ERTE','OFDATCHI');
                    +i_ccchkf ;
                +" where ";
                    +"OFSERIAL = "+cp_ToStrODBC(this.w_SEROFF);
                       )
              else
                update (i_cTable) set;
                    OFRIFDOC = SPACE(10);
                    ,OFSTATUS = "I";
                    ,OFDATINV = cp_CharToDate("  -  -  ");
                    ,OFDATCHI = cp_CharToDate("  -  -  ");
                    &i_ccchkf. ;
                 where;
                    OFSERIAL = this.w_SEROFF;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
              select _Curs_OFF_ERTE
              continue
            enddo
            use
          endif
      endcase
    endif
  endproc


  proc Init(oParentObject,pTIPOPE,pSERIAL)
    this.pTIPOPE=pTIPOPE
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_ERTE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_OFF_ERTE')
      use in _Curs_OFF_ERTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE,pSERIAL"
endproc
