* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_aal                                                        *
*              Allegati                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2012-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsof_aal
PARAMETERS pCodice, pOrigine, pDescri, pNumdoc, pSerdoc, pDatdoc, pNumver
* --- Fine Area Manuale
return(createobject("tgsof_aal"))

* --- Class definition
define class tgsof_aal as StdForm
  Top    = 8
  Left   = 12

  * --- Standard Properties
  Width  = 705
  Height = 388+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-01-02"
  HelpContextID=125163159
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  ALL_EGAT_IDX = 0
  AZIENDA_IDX = 0
  ALL_ATTR_IDX = 0
  OFF_NOMI_IDX = 0
  MOD_OFFE_IDX = 0
  PAR_OFFE_IDX = 0
  cFile = "ALL_EGAT"
  cKeySelect = "ALSERIAL"
  cKeyWhere  = "ALSERIAL=this.w_ALSERIAL"
  cKeyWhereODBC = '"ALSERIAL="+cp_ToStrODBC(this.w_ALSERIAL)';

  cKeyWhereODBCqualified = '"ALL_EGAT.ALSERIAL="+cp_ToStrODBC(this.w_ALSERIAL)';

  cPrg = "gsof_aal"
  cComment = "Allegati"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_ALSERIAL = space(10)
  o_ALSERIAL = space(10)
  w_ALDATREG = ctod('  /  /  ')
  w_ALTIPALL = space(1)
  o_ALTIPALL = space(1)
  w_ALTIPORI = space(1)
  w_ALRIFNOM = space(15)
  w_ALRIFOFF = space(10)
  w_FLCOLL = space(1)
  w_NOMFIL = space(254)
  o_NOMFIL = space(254)
  w_NFILONLY = space(254)
  w_ALRIFMOD = space(5)
  w_ALPATFIL = space(254)
  w_ALDESNOM = space(40)
  w_ALDESMOD = space(40)
  w_OFNUMDOC = 0
  w_OFSERDOC = space(2)
  w_OFDATDOC = ctod('  /  /  ')
  w_OFNUMVER = 0
  w_FILOFF = space(1)
  w_ALOGGALL = space(50)
  w_AL__NOTE = space(0)
  w_ALDATVAL = ctod('  /  /  ')
  w_ALDATOBS = ctod('  /  /  ')
  w_OFSERDOC = space(10)
  w_OFNUMDOC = 0
  w_ALFLATTM = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_ALSERIAL = this.W_ALSERIAL

  * --- Children pointers
  GSOF_MAA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsof_aal
  pSeriale =' '
  pTipAll=' '
  pTDescri=' '
  pTNumdoc= 0
  pTSerdoc=' '
  pTDatdoc=' '
  pTNumver= 0
  
  * --- Disattiva la Caption dell'elenco
  proc ecpLoad()
          * ----
          DoDefault()
          this.oPgFrm.Pages(2).caption=''
  endproc
  proc ecpEdit()
          * ----
          DoDefault()
          this.oPgFrm.Pages(2).caption=''
  endproc
  
  proc ecpQuit()
          * ----
          DoDefault()
          this.bUpdated=.f.
          if this.cFunction='Query'
             Keyboard "{ESC}"
          endif
  endproc
  
  * --- Salva
  proc ecpSave()
         * ----
         if this.cFunction<>'Filter'
          DoDefault()
          this.bUpdated=.f.
          if this.cFunction='Query'
             Keyboard "{ESC}"
          endif
         else
          Dodefault()
         Endif
  endproc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ALL_EGAT','gsof_aal')
    stdPageFrame::Init()
    *set procedure to GSOF_MAA additive
    with this
      .Pages(1).addobject("oPag","tgsof_aalPag1","gsof_aal",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Allegato")
      .Pages(1).HelpContextID = 11401355
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oALSERIAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSOF_MAA
    * --- Area Manuale = Init Page Frame
    * --- gsof_aal
    this.parent.pSeriale =pCodice
    this.parent.pTipAll=pOrigine
    this.parent.pTDescri=pDescri
    this.parent.pTNumdoc=pNumdoc
    this.parent.pTSerdoc=pSerdoc
    this.parent.pTDatdoc=pDatdoc
    this.parent.pTNumver=pNumver
    * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ALL_ATTR'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='MOD_OFFE'
    this.cWorkTables[5]='PAR_OFFE'
    this.cWorkTables[6]='ALL_EGAT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ALL_EGAT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ALL_EGAT_IDX,3]
  return

  function CreateChildren()
    this.GSOF_MAA = CREATEOBJECT('stdDynamicChild',this,'GSOF_MAA',this.oPgFrm.Page1.oPag.oLinkPC_1_39)
    this.GSOF_MAA.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSOF_MAA)
      this.GSOF_MAA.DestroyChildrenChain()
      this.GSOF_MAA=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_39')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSOF_MAA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSOF_MAA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSOF_MAA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSOF_MAA.SetKey(;
            .w_ALSERIAL,"AACODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSOF_MAA.ChangeRow(this.cRowID+'      1',1;
             ,.w_ALSERIAL,"AACODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSOF_MAA)
        i_f=.GSOF_MAA.BuildFilter()
        if !(i_f==.GSOF_MAA.cQueryFilter)
          i_fnidx=.GSOF_MAA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSOF_MAA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSOF_MAA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSOF_MAA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSOF_MAA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ALSERIAL = NVL(ALSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ALL_EGAT where ALSERIAL=KeySet.ALSERIAL
    *
    i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ALL_EGAT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ALL_EGAT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ALL_EGAT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ALL_EGAT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ALSERIAL',this.w_ALSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_NOMFIL = space(254)
        .w_ALDESNOM = IIF(.w_ALTIPORI='N', this.pTDescri, SPACE(40))
        .w_ALDESMOD = IIF(.w_ALTIPORI='M', this.pTDescri, SPACE(40))
        .w_OFNUMDOC = IIF(.w_ALTIPORI='O', this.pTNumdoc, 0)
        .w_OFSERDOC = IIF(.w_ALTIPORI='O', this.pTSerdoc, SPACE(2))
        .w_OFDATDOC = IIF(.w_ALTIPORI='O', this.pTDatdoc, i_datsys)
        .w_OFNUMVER = IIF(.w_ALTIPORI='O', this.pTNumver, 0)
        .w_FILOFF = g_FILOFF
        .w_OFSERDOC = IIF(.w_ALTIPORI='O', this.pTSerdoc, SPACE(10))
        .w_OFNUMDOC = IIF(.w_ALTIPORI='O', this.pTNumdoc, 0)
        .w_CODAZI = i_CODAZI
          .link_1_1('Load')
        .w_ALSERIAL = NVL(ALSERIAL,space(10))
        .op_ALSERIAL = .w_ALSERIAL
        .w_ALDATREG = NVL(cp_ToDate(ALDATREG),ctod("  /  /  "))
        .w_ALTIPALL = NVL(ALTIPALL,space(1))
        .w_ALTIPORI = NVL(ALTIPORI,space(1))
        .w_ALRIFNOM = NVL(ALRIFNOM,space(15))
        .w_ALRIFOFF = NVL(ALRIFOFF,space(10))
        .w_FLCOLL = iif(.w_ALTIPORI='M','N','S')
        .w_NFILONLY = JUSTFNAME(.w_NOMFIL)
        .w_ALRIFMOD = NVL(ALRIFMOD,space(5))
        .w_ALPATFIL = NVL(ALPATFIL,space(254))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_ALOGGALL = NVL(ALOGGALL,space(50))
        .w_AL__NOTE = NVL(AL__NOTE,space(0))
        .w_ALDATVAL = NVL(cp_ToDate(ALDATVAL),ctod("  /  /  "))
        .w_ALDATOBS = NVL(cp_ToDate(ALDATOBS),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_ALFLATTM = NVL(ALFLATTM,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ALL_EGAT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_ALSERIAL = space(10)
      .w_ALDATREG = ctod("  /  /  ")
      .w_ALTIPALL = space(1)
      .w_ALTIPORI = space(1)
      .w_ALRIFNOM = space(15)
      .w_ALRIFOFF = space(10)
      .w_FLCOLL = space(1)
      .w_NOMFIL = space(254)
      .w_NFILONLY = space(254)
      .w_ALRIFMOD = space(5)
      .w_ALPATFIL = space(254)
      .w_ALDESNOM = space(40)
      .w_ALDESMOD = space(40)
      .w_OFNUMDOC = 0
      .w_OFSERDOC = space(2)
      .w_OFDATDOC = ctod("  /  /  ")
      .w_OFNUMVER = 0
      .w_FILOFF = space(1)
      .w_ALOGGALL = space(50)
      .w_AL__NOTE = space(0)
      .w_ALDATVAL = ctod("  /  /  ")
      .w_ALDATOBS = ctod("  /  /  ")
      .w_OFSERDOC = space(10)
      .w_OFNUMDOC = 0
      .w_ALFLATTM = space(1)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_ALDATREG = i_DATSYS
        .w_ALTIPALL = 'L'
        .w_ALTIPORI = this.pTipAll
        .w_ALRIFNOM = IIF(.w_ALTIPORI='N', this.pSeriale, SPACE(15))
        .w_ALRIFOFF = IIF(.w_ALTIPORI='O', this.pSeriale, SPACE(10))
        .w_FLCOLL = iif(.w_ALTIPORI='M','N','S')
          .DoRTCalc(9,9,.f.)
        .w_NFILONLY = JUSTFNAME(.w_NOMFIL)
        .w_ALRIFMOD = IIF(.w_ALTIPORI='M', this.pSeriale, SPACE(5))
        .w_ALPATFIL = iif(.w_ALTIPALL='L',.w_NOMFIL,.w_NFILONLY)
        .w_ALDESNOM = IIF(.w_ALTIPORI='N', this.pTDescri, SPACE(40))
        .w_ALDESMOD = IIF(.w_ALTIPORI='M', this.pTDescri, SPACE(40))
        .w_OFNUMDOC = IIF(.w_ALTIPORI='O', this.pTNumdoc, 0)
        .w_OFSERDOC = IIF(.w_ALTIPORI='O', this.pTSerdoc, SPACE(2))
        .w_OFDATDOC = IIF(.w_ALTIPORI='O', this.pTDatdoc, i_datsys)
        .w_OFNUMVER = IIF(.w_ALTIPORI='O', this.pTNumver, 0)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_FILOFF = g_FILOFF
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
          .DoRTCalc(20,23,.f.)
        .w_OFSERDOC = IIF(.w_ALTIPORI='O', this.pTSerdoc, SPACE(10))
        .w_OFNUMDOC = IIF(.w_ALTIPORI='O', this.pTNumdoc, 0)
        .w_ALFLATTM = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ALL_EGAT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsof_aal
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ALL_EGAT_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEALLOF","I_codazi,w_ALSERIAL")
      .op_codazi = .w_codazi
      .op_ALSERIAL = .w_ALSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oALSERIAL_1_2.enabled = !i_bVal
      .Page1.oPag.oALDATREG_1_4.enabled = i_bVal
      .Page1.oPag.oALTIPALL_1_6.enabled = i_bVal
      .Page1.oPag.oALOGGALL_1_37.enabled = i_bVal
      .Page1.oPag.oAL__NOTE_1_38.enabled = i_bVal
      .Page1.oPag.oALDATVAL_1_40.enabled = i_bVal
      .Page1.oPag.oALDATOBS_1_41.enabled = i_bVal
      .Page1.oPag.oALFLATTM_1_47.enabled = i_bVal
      .Page1.oPag.oBtn_1_36.enabled = i_bVal
      .Page1.oPag.oBtn_1_42.enabled = .Page1.oPag.oBtn_1_42.mCond()
      .Page1.oPag.oBtn_1_43.enabled = .Page1.oPag.oBtn_1_43.mCond()
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oObj_1_44.enabled = i_bVal
    endwith
    this.GSOF_MAA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ALL_EGAT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSOF_MAA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALSERIAL,"ALSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALDATREG,"ALDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALTIPALL,"ALTIPALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALTIPORI,"ALTIPORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALRIFNOM,"ALRIFNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALRIFOFF,"ALRIFOFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALRIFMOD,"ALRIFMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALPATFIL,"ALPATFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALOGGALL,"ALOGGALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AL__NOTE,"AL__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALDATVAL,"ALDATVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALDATOBS,"ALDATOBS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ALFLATTM,"ALFLATTM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ALL_EGAT_IDX,2])
    i_lTable = "ALL_EGAT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ALL_EGAT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ALL_EGAT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ALL_EGAT_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEALLOF","I_codazi,w_ALSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ALL_EGAT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ALL_EGAT')
        i_extval=cp_InsertValODBCExtFlds(this,'ALL_EGAT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ALSERIAL,ALDATREG,ALTIPALL,ALTIPORI,ALRIFNOM"+;
                  ",ALRIFOFF,ALRIFMOD,ALPATFIL,ALOGGALL,AL__NOTE"+;
                  ",ALDATVAL,ALDATOBS,ALFLATTM "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ALSERIAL)+;
                  ","+cp_ToStrODBC(this.w_ALDATREG)+;
                  ","+cp_ToStrODBC(this.w_ALTIPALL)+;
                  ","+cp_ToStrODBC(this.w_ALTIPORI)+;
                  ","+cp_ToStrODBC(this.w_ALRIFNOM)+;
                  ","+cp_ToStrODBC(this.w_ALRIFOFF)+;
                  ","+cp_ToStrODBC(this.w_ALRIFMOD)+;
                  ","+cp_ToStrODBC(this.w_ALPATFIL)+;
                  ","+cp_ToStrODBC(this.w_ALOGGALL)+;
                  ","+cp_ToStrODBC(this.w_AL__NOTE)+;
                  ","+cp_ToStrODBC(this.w_ALDATVAL)+;
                  ","+cp_ToStrODBC(this.w_ALDATOBS)+;
                  ","+cp_ToStrODBC(this.w_ALFLATTM)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ALL_EGAT')
        i_extval=cp_InsertValVFPExtFlds(this,'ALL_EGAT')
        cp_CheckDeletedKey(i_cTable,0,'ALSERIAL',this.w_ALSERIAL)
        INSERT INTO (i_cTable);
              (ALSERIAL,ALDATREG,ALTIPALL,ALTIPORI,ALRIFNOM,ALRIFOFF,ALRIFMOD,ALPATFIL,ALOGGALL,AL__NOTE,ALDATVAL,ALDATOBS,ALFLATTM  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ALSERIAL;
                  ,this.w_ALDATREG;
                  ,this.w_ALTIPALL;
                  ,this.w_ALTIPORI;
                  ,this.w_ALRIFNOM;
                  ,this.w_ALRIFOFF;
                  ,this.w_ALRIFMOD;
                  ,this.w_ALPATFIL;
                  ,this.w_ALOGGALL;
                  ,this.w_AL__NOTE;
                  ,this.w_ALDATVAL;
                  ,this.w_ALDATOBS;
                  ,this.w_ALFLATTM;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ALL_EGAT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ALL_EGAT_IDX,i_nConn)
      *
      * update ALL_EGAT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ALL_EGAT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ALDATREG="+cp_ToStrODBC(this.w_ALDATREG)+;
             ",ALTIPALL="+cp_ToStrODBC(this.w_ALTIPALL)+;
             ",ALTIPORI="+cp_ToStrODBC(this.w_ALTIPORI)+;
             ",ALRIFNOM="+cp_ToStrODBC(this.w_ALRIFNOM)+;
             ",ALRIFOFF="+cp_ToStrODBC(this.w_ALRIFOFF)+;
             ",ALRIFMOD="+cp_ToStrODBC(this.w_ALRIFMOD)+;
             ",ALPATFIL="+cp_ToStrODBC(this.w_ALPATFIL)+;
             ",ALOGGALL="+cp_ToStrODBC(this.w_ALOGGALL)+;
             ",AL__NOTE="+cp_ToStrODBC(this.w_AL__NOTE)+;
             ",ALDATVAL="+cp_ToStrODBC(this.w_ALDATVAL)+;
             ",ALDATOBS="+cp_ToStrODBC(this.w_ALDATOBS)+;
             ",ALFLATTM="+cp_ToStrODBC(this.w_ALFLATTM)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ALL_EGAT')
        i_cWhere = cp_PKFox(i_cTable  ,'ALSERIAL',this.w_ALSERIAL  )
        UPDATE (i_cTable) SET;
              ALDATREG=this.w_ALDATREG;
             ,ALTIPALL=this.w_ALTIPALL;
             ,ALTIPORI=this.w_ALTIPORI;
             ,ALRIFNOM=this.w_ALRIFNOM;
             ,ALRIFOFF=this.w_ALRIFOFF;
             ,ALRIFMOD=this.w_ALRIFMOD;
             ,ALPATFIL=this.w_ALPATFIL;
             ,ALOGGALL=this.w_ALOGGALL;
             ,AL__NOTE=this.w_AL__NOTE;
             ,ALDATVAL=this.w_ALDATVAL;
             ,ALDATOBS=this.w_ALDATOBS;
             ,ALFLATTM=this.w_ALFLATTM;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSOF_MAA : Saving
      this.GSOF_MAA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ALSERIAL,"AACODICE";
             )
      this.GSOF_MAA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSOF_MAA : Deleting
    this.GSOF_MAA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ALSERIAL,"AACODICE";
           )
    this.GSOF_MAA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ALL_EGAT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ALL_EGAT_IDX,i_nConn)
      *
      * delete ALL_EGAT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ALSERIAL',this.w_ALSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ALL_EGAT_IDX,2])
    if i_bUpd
      with this
        if .o_ALSERIAL<>.w_ALSERIAL
            .w_CODAZI = i_CODAZI
          .link_1_1('Full')
        endif
        .DoRTCalc(2,5,.t.)
            .w_ALRIFNOM = IIF(.w_ALTIPORI='N', this.pSeriale, SPACE(15))
        .DoRTCalc(7,7,.t.)
            .w_FLCOLL = iif(.w_ALTIPORI='M','N','S')
        .DoRTCalc(9,9,.t.)
            .w_NFILONLY = JUSTFNAME(.w_NOMFIL)
        .DoRTCalc(11,11,.t.)
        if .o_ALTIPALL<>.w_ALTIPALL.or. .o_NOMFIL<>.w_NOMFIL
            .w_ALPATFIL = iif(.w_ALTIPALL='L',.w_NOMFIL,.w_NFILONLY)
        endif
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEALLOF","I_codazi,w_ALSERIAL")
          .op_ALSERIAL = .w_ALSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(13,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oALDATREG_1_4.enabled = this.oPgFrm.Page1.oPag.oALDATREG_1_4.mCond()
    this.oPgFrm.Page1.oPag.oALTIPALL_1_6.enabled = this.oPgFrm.Page1.oPag.oALTIPALL_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oALRIFNOM_1_12.visible=!this.oPgFrm.Page1.oPag.oALRIFNOM_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oALRIFMOD_1_20.visible=!this.oPgFrm.Page1.oPag.oALRIFMOD_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oALDESNOM_1_24.visible=!this.oPgFrm.Page1.oPag.oALDESNOM_1_24.mHide()
    this.oPgFrm.Page1.oPag.oALDESMOD_1_26.visible=!this.oPgFrm.Page1.oPag.oALDESMOD_1_26.mHide()
    this.oPgFrm.Page1.oPag.oOFNUMDOC_1_27.visible=!this.oPgFrm.Page1.oPag.oOFNUMDOC_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oOFSERDOC_1_29.visible=!this.oPgFrm.Page1.oPag.oOFSERDOC_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oOFDATDOC_1_31.visible=!this.oPgFrm.Page1.oPag.oOFDATDOC_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oOFNUMVER_1_33.visible=!this.oPgFrm.Page1.oPag.oOFNUMVER_1_33.mHide()
    this.oPgFrm.Page1.oPag.oOFSERDOC_1_45.visible=!this.oPgFrm.Page1.oPag.oOFSERDOC_1_45.mHide()
    this.oPgFrm.Page1.oPag.oOFNUMDOC_1_46.visible=!this.oPgFrm.Page1.oPag.oOFNUMDOC_1_46.mHide()
    this.oPgFrm.Page1.oPag.oALFLATTM_1_47.visible=!this.oPgFrm.Page1.oPag.oALFLATTM_1_47.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_lTable = "PAR_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2], .t., this.PAR_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODAZI,POFILOFF";
                   +" from "+i_cTable+" "+i_lTable+" where POCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODAZI',this.w_CODAZI)
            select POCODAZI,POFILOFF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.POCODAZI,space(5))
      this.w_FLCOLL = NVL(_Link_.POFILOFF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_FLCOLL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.POCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oALSERIAL_1_2.value==this.w_ALSERIAL)
      this.oPgFrm.Page1.oPag.oALSERIAL_1_2.value=this.w_ALSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oALDATREG_1_4.value==this.w_ALDATREG)
      this.oPgFrm.Page1.oPag.oALDATREG_1_4.value=this.w_ALDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oALTIPALL_1_6.RadioValue()==this.w_ALTIPALL)
      this.oPgFrm.Page1.oPag.oALTIPALL_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oALRIFNOM_1_12.value==this.w_ALRIFNOM)
      this.oPgFrm.Page1.oPag.oALRIFNOM_1_12.value=this.w_ALRIFNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oALRIFMOD_1_20.value==this.w_ALRIFMOD)
      this.oPgFrm.Page1.oPag.oALRIFMOD_1_20.value=this.w_ALRIFMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oALPATFIL_1_21.value==this.w_ALPATFIL)
      this.oPgFrm.Page1.oPag.oALPATFIL_1_21.value=this.w_ALPATFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oALDESNOM_1_24.value==this.w_ALDESNOM)
      this.oPgFrm.Page1.oPag.oALDESNOM_1_24.value=this.w_ALDESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oALDESMOD_1_26.value==this.w_ALDESMOD)
      this.oPgFrm.Page1.oPag.oALDESMOD_1_26.value=this.w_ALDESMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oOFNUMDOC_1_27.value==this.w_OFNUMDOC)
      this.oPgFrm.Page1.oPag.oOFNUMDOC_1_27.value=this.w_OFNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFSERDOC_1_29.value==this.w_OFSERDOC)
      this.oPgFrm.Page1.oPag.oOFSERDOC_1_29.value=this.w_OFSERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDATDOC_1_31.value==this.w_OFDATDOC)
      this.oPgFrm.Page1.oPag.oOFDATDOC_1_31.value=this.w_OFDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFNUMVER_1_33.value==this.w_OFNUMVER)
      this.oPgFrm.Page1.oPag.oOFNUMVER_1_33.value=this.w_OFNUMVER
    endif
    if not(this.oPgFrm.Page1.oPag.oALOGGALL_1_37.value==this.w_ALOGGALL)
      this.oPgFrm.Page1.oPag.oALOGGALL_1_37.value=this.w_ALOGGALL
    endif
    if not(this.oPgFrm.Page1.oPag.oAL__NOTE_1_38.value==this.w_AL__NOTE)
      this.oPgFrm.Page1.oPag.oAL__NOTE_1_38.value=this.w_AL__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oALDATVAL_1_40.value==this.w_ALDATVAL)
      this.oPgFrm.Page1.oPag.oALDATVAL_1_40.value=this.w_ALDATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oALDATOBS_1_41.value==this.w_ALDATOBS)
      this.oPgFrm.Page1.oPag.oALDATOBS_1_41.value=this.w_ALDATOBS
    endif
    if not(this.oPgFrm.Page1.oPag.oOFSERDOC_1_45.value==this.w_OFSERDOC)
      this.oPgFrm.Page1.oPag.oOFSERDOC_1_45.value=this.w_OFSERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oOFNUMDOC_1_46.value==this.w_OFNUMDOC)
      this.oPgFrm.Page1.oPag.oOFNUMDOC_1_46.value=this.w_OFNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFLATTM_1_47.RadioValue()==this.w_ALFLATTM)
      this.oPgFrm.Page1.oPag.oALFLATTM_1_47.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ALL_EGAT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ALDATREG))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALDATREG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_ALDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ALPATFIL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALPATFIL_1_21.SetFocus()
            i_bnoObbl = !empty(.w_ALPATFIL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSOF_MAA.CheckForm()
      if i_bres
        i_bres=  .GSOF_MAA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ALSERIAL = this.w_ALSERIAL
    this.o_ALTIPALL = this.w_ALTIPALL
    this.o_NOMFIL = this.w_NOMFIL
    * --- GSOF_MAA : Depends On
    this.GSOF_MAA.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsof_aalPag1 as StdContainer
  Width  = 701
  height = 388
  stdWidth  = 701
  stdheight = 388
  resizeXpos=531
  resizeYpos=230
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oALSERIAL_1_2 as StdField with uid="OLBZXEEEUW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ALSERIAL", cQueryName = "ALSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Progressivo allegato",;
    HelpContextID = 169846958,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=83, Left=114, Top=13, InputMask=replicate('X',10)

  add object oALDATREG_1_4 as StdField with uid="NCUXUJTDJF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ALDATREG", cQueryName = "ALDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 17078451,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=315, Top=13

  func oALDATREG_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc


  add object oALTIPALL_1_6 as StdCombo with uid="GSGZIYBJWZ",rtseq=4,rtrep=.f.,left=114,top=41,width=105,height=21;
    , ToolTipText = "Tipo allegato: (collegamento o copia file)";
    , HelpContextID = 230975314;
    , cFormVar="w_ALTIPALL",RowSource=""+"Collegamento,"+"Copia file", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oALTIPALL_1_6.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oALTIPALL_1_6.GetRadio()
    this.Parent.oContained.w_ALTIPALL = this.RadioValue()
    return .t.
  endfunc

  func oALTIPALL_1_6.SetRadio()
    this.Parent.oContained.w_ALTIPALL=trim(this.Parent.oContained.w_ALTIPALL)
    this.value = ;
      iif(this.Parent.oContained.w_ALTIPALL=='L',1,;
      iif(this.Parent.oContained.w_ALTIPALL=='F',2,;
      0))
  endfunc

  func oALTIPALL_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCOLL='S' and .w_FILOFF = 'S' and .cFunction='Load')
    endwith
   endif
  endfunc

  add object oALRIFNOM_1_12 as StdField with uid="XJQAPMUUWY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ALRIFNOM", cQueryName = "ALRIFNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo",;
    HelpContextID = 170149715,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=116, Top=344, InputMask=replicate('X',15)

  func oALRIFNOM_1_12.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'N')
    endwith
  endfunc

  add object oALRIFMOD_1_20 as StdField with uid="HEBQEIVQGT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ALRIFMOD", cQueryName = "ALRIFMOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Modello",;
    HelpContextID = 153372490,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=116, Top=344, InputMask=replicate('X',5)

  func oALRIFMOD_1_20.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'M')
    endwith
  endfunc

  add object oALPATFIL_1_21 as StdField with uid="VJEYVLBUQG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ALPATFIL", cQueryName = "ALPATFIL",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso file allegato o nome file",;
    HelpContextID = 50079570,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=315, Top=41, InputMask=replicate('X',254)

  add object oALDESNOM_1_24 as StdField with uid="KAMNHLSGJE",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ALDESNOM", cQueryName = "ALDESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nominativo",;
    HelpContextID = 183461715,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=273, Top=344, InputMask=replicate('X',40)

  func oALDESNOM_1_24.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI <>'N')
    endwith
  endfunc

  add object oALDESMOD_1_26 as StdField with uid="DZLUQQPGWR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ALDESMOD", cQueryName = "ALDESMOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione modello",;
    HelpContextID = 166684490,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=174, Top=344, InputMask=replicate('X',40)

  func oALDESMOD_1_26.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI <>'M')
    endwith
  endfunc

  add object oOFNUMDOC_1_27 as StdField with uid="YHALRJCILV",rtseq=15,rtrep=.f.,;
    cFormVar = "w_OFNUMDOC", cQueryName = "OFNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 10486313,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=116, Top=344, cSayPict='"999999"', cGetPict='"999999"'

  func oOFNUMDOC_1_27.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O' or UPPER(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oOFSERDOC_1_29 as StdField with uid="LJVYXABAPQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_OFSERDOC", cQueryName = "OFSERDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Seriale documento",;
    HelpContextID = 14701097,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=206, Top=344, InputMask=replicate('X',2)

  func oOFSERDOC_1_29.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O' or UPPER(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oOFDATDOC_1_31 as StdField with uid="KDJDKNQZAT",rtseq=17,rtrep=.f.,;
    cFormVar = "w_OFDATDOC", cQueryName = "OFDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 16474665,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=329, Top=344

  func oOFDATDOC_1_31.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O')
    endwith
  endfunc

  add object oOFNUMVER_1_33 as StdField with uid="JNTQUTKWAJ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_OFNUMVER", cQueryName = "OFNUMVER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero versione offerta",;
    HelpContextID = 224394696,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=484, Top=344, cSayPict='"9999"', cGetPict='"9999"'

  func oOFNUMVER_1_33.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O')
    endwith
  endfunc


  add object oObj_1_34 as cp_runprogram with uid="JLPIUIGYFH",left=1, top=406, width=201,height=19,;
    caption='GSOF_BCF(I)',;
   bGlobalFont=.t.,;
    prg="GSOF_BCF('I')",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 4982228


  add object oBtn_1_36 as StdButton with uid="GWKJEGKECT",left=674, top=41, width=24,height=22,;
    caption="...", nPag=1;
    , HelpContextID = 125364182;
  , bGlobalFont=.t.

    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSOF_BCF(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction <> "Edit")
      endwith
    endif
  endfunc

  add object oALOGGALL_1_37 as StdField with uid="KLXOAAGTYN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ALOGGALL", cQueryName = "ALOGGALL",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto del documento allegato",;
    HelpContextID = 221386578,;
   bGlobalFont=.t.,;
    Height=21, Width=547, Left=114, Top=70, InputMask=replicate('X',50)

  add object oAL__NOTE_1_38 as StdMemo with uid="KRMNLVGRMD",rtseq=21,rtrep=.f.,;
    cFormVar = "w_AL__NOTE", cQueryName = "AL__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note aggiuntive",;
    HelpContextID = 71624885,;
   bGlobalFont=.t.,;
    Height=68, Width=547, Left=114, Top=99


  add object oLinkPC_1_39 as stdDynamicChildContainer with uid="PTTOCLTZQE",left=98, top=172, width=600, height=127, bOnScreen=.t.;


  add object oALDATVAL_1_40 as StdField with uid="DYZXWSKOKR",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ALDATVAL", cQueryName = "ALDATVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 218405038,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=116, Top=317

  add object oALDATOBS_1_41 as StdField with uid="HPSAWIBBRE",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ALDATOBS", cQueryName = "ALDATOBS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data obsolescenza",;
    HelpContextID = 67410087,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=330, Top=317


  add object oBtn_1_42 as StdButton with uid="QOUYREPXDQ",left=599, top=343, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 125183718;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_43 as StdButton with uid="ZOPXDCHXAD",left=650, top=343, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 88987386;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_44 as cp_runprogram with uid="XUQTMQCUHR",left=231, top=406, width=201,height=19,;
    caption='GSOF_BCF(D)',;
   bGlobalFont=.t.,;
    prg="GSOF_BCF('D')",;
    cEvent = "Delete end",;
    nPag=1;
    , HelpContextID = 4983508

  add object oOFSERDOC_1_45 as StdField with uid="FVWATWSNCQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_OFSERDOC", cQueryName = "OFSERDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale documento",;
    HelpContextID = 14701097,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=206, Top=344, InputMask=replicate('X',10)

  func oOFSERDOC_1_45.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O' or UPPER(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oOFNUMDOC_1_46 as StdField with uid="PWLJBVDZVN",rtseq=25,rtrep=.f.,;
    cFormVar = "w_OFNUMDOC", cQueryName = "OFNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 10486313,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=116, Top=344

  func oOFNUMDOC_1_46.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O' or UPPER(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oALFLATTM_1_47 as StdCheck with uid="NNKUYXVGME",rtseq=26,rtrep=.f.,left=418, top=317, caption="Utilizza nell'invio email",;
    ToolTipText = "Se attivo l'allegato verr� inserito nell'email da inviare",;
    HelpContextID = 2717869,;
    cFormVar="w_ALFLATTM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oALFLATTM_1_47.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oALFLATTM_1_47.GetRadio()
    this.Parent.oContained.w_ALFLATTM = this.RadioValue()
    return .t.
  endfunc

  func oALFLATTM_1_47.SetRadio()
    this.Parent.oContained.w_ALFLATTM=trim(this.Parent.oContained.w_ALFLATTM)
    this.value = ;
      iif(this.Parent.oContained.w_ALFLATTM=='S',1,;
      0)
  endfunc

  func oALFLATTM_1_47.mHide()
    with this.Parent.oContained
      return (!EMPTY(.w_ALRIFNOM))
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="SQHTYJMWNT",Visible=.t., Left=0, Top=13,;
    Alignment=1, Width=109, Height=18,;
    Caption="Progressivo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="ETIHEMUSJI",Visible=.t., Left=236, Top=13,;
    Alignment=1, Width=76, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="EBZZHSOZGK",Visible=.t., Left=0, Top=42,;
    Alignment=1, Width=109, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="VXUZMZETDA",Visible=.t., Left=217, Top=43,;
    Alignment=1, Width=95, Height=18,;
    Caption="Percorso file:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_ALTIPALL<>'L')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="XLISBSYCZJ",Visible=.t., Left=0, Top=74,;
    Alignment=1, Width=109, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="SDCLHGYKFT",Visible=.t., Left=0, Top=102,;
    Alignment=1, Width=109, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="UWSCPJFDJV",Visible=.t., Left=2, Top=348,;
    Alignment=1, Width=109, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'N')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="OVWMBPKKGS",Visible=.t., Left=2, Top=348,;
    Alignment=1, Width=109, Height=18,;
    Caption="Num.offerta:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="FUWLVHRXUY",Visible=.t., Left=223, Top=43,;
    Alignment=1, Width=89, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_ALTIPALL='L')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="SEPTPDFETJ",Visible=.t., Left=2, Top=348,;
    Alignment=1, Width=109, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'M')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="ZMXBNNDUQI",Visible=.t., Left=196, Top=321,;
    Alignment=1, Width=131, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="WDVTMGHTMN",Visible=.t., Left=2, Top=321,;
    Alignment=1, Width=109, Height=18,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="IOPGBDIKMU",Visible=.t., Left=186, Top=348,;
    Alignment=0, Width=9, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="EVSQCRRSIP",Visible=.t., Left=292, Top=344,;
    Alignment=1, Width=34, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="VYLDSEMJXQ",Visible=.t., Left=412, Top=344,;
    Alignment=1, Width=68, Height=18,;
    Caption="Versione:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_ALTIPORI<>'O')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_aal','ALL_EGAT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ALSERIAL=ALL_EGAT.ALSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
