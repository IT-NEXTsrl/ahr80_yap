* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_skp                                                        *
*              Stampa kit promozionali                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-24                                                      *
* Last revis.: 2007-07-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_skp",oParentObject))

* --- Class definition
define class tgsof_skp as StdForm
  Top    = 14
  Left   = 36

  * --- Standard Properties
  Width  = 640
  Height = 276+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-20"
  HelpContextID=43374231
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  OFF_NOMI_IDX = 0
  KITMPROM_IDX = 0
  VALUTE_IDX = 0
  UTE_AZI_IDX = 0
  GRU_PRIO_IDX = 0
  GRU_NOMI_IDX = 0
  ORI_NOMI_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  TIT_SEZI_IDX = 0
  SOT_SEZI_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  cPrg = "gsof_skp"
  cComment = "Stampa kit promozionali"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATASTAM = ctod('  /  /  ')
  o_DATASTAM = ctod('  /  /  ')
  w_SKCODIC1 = space(15)
  w_SKDES1 = space(35)
  w_SKCODIC2 = space(15)
  w_SKDES2 = space(35)
  w_SKCODAS = space(20)
  w_SKDESART = space(35)
  w_SKCODARS = space(5)
  w_DESARSE = space(35)
  w_SKSGRART = space(5)
  w_DESSG = space(35)
  w_OBSOLETI = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_SKCODVAL = space(3)
  w_SKDESVAL = space(35)
  w_SKCODGRU = space(5)
  w_DESCGN = space(35)
  w_SKCODPRI = space(6)
  w_DESPRIO = space(35)
  w_SKCODORI = space(5)
  w_DESORI = space(35)
  w_SKCODZON = space(3)
  w_DESZONE = space(35)
  w_SKCODGPR = space(5)
  w_DESGPR = space(35)
  w_SKCODAGE = space(5)
  w_DESAGE = space(35)
  w_SKCODUTE = 0
  w_DESOPE = space(35)
  w_SKGRUOP = 0
  w_SKGRDES = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_skpPag1","gsof_skp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsof_skpPag2","gsof_skp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri aggiuntivi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATASTAM_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[15]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='KITMPROM'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='UTE_AZI'
    this.cWorkTables[5]='GRU_PRIO'
    this.cWorkTables[6]='GRU_NOMI'
    this.cWorkTables[7]='ORI_NOMI'
    this.cWorkTables[8]='ZONE'
    this.cWorkTables[9]='AGENTI'
    this.cWorkTables[10]='TIT_SEZI'
    this.cWorkTables[11]='SOT_SEZI'
    this.cWorkTables[12]='KEY_ARTI'
    this.cWorkTables[13]='ART_ICOL'
    this.cWorkTables[14]='cpusers'
    this.cWorkTables[15]='cpgroups'
    return(this.OpenAllTables(15))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATASTAM=ctod("  /  /  ")
      .w_SKCODIC1=space(15)
      .w_SKDES1=space(35)
      .w_SKCODIC2=space(15)
      .w_SKDES2=space(35)
      .w_SKCODAS=space(20)
      .w_SKDESART=space(35)
      .w_SKCODARS=space(5)
      .w_DESARSE=space(35)
      .w_SKSGRART=space(5)
      .w_DESSG=space(35)
      .w_OBSOLETI=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_SKCODVAL=space(3)
      .w_SKDESVAL=space(35)
      .w_SKCODGRU=space(5)
      .w_DESCGN=space(35)
      .w_SKCODPRI=space(6)
      .w_DESPRIO=space(35)
      .w_SKCODORI=space(5)
      .w_DESORI=space(35)
      .w_SKCODZON=space(3)
      .w_DESZONE=space(35)
      .w_SKCODGPR=space(5)
      .w_DESGPR=space(35)
      .w_SKCODAGE=space(5)
      .w_DESAGE=space(35)
      .w_SKCODUTE=0
      .w_DESOPE=space(35)
      .w_SKGRUOP=0
      .w_SKGRDES=space(35)
        .w_DATASTAM = i_datsys
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SKCODIC1))
          .link_1_3('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_SKCODIC2))
          .link_1_6('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_SKCODAS))
          .link_1_9('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_SKCODARS))
          .link_1_12('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_SKSGRART))
          .link_1_15('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_OBSOLETI = 'N'
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .w_OBTEST = .w_DATASTAM
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_SKCODVAL))
          .link_2_2('Full')
        endif
        .DoRTCalc(16,17,.f.)
        if not(empty(.w_SKCODGRU))
          .link_2_5('Full')
        endif
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_SKCODPRI))
          .link_2_8('Full')
        endif
        .DoRTCalc(20,21,.f.)
        if not(empty(.w_SKCODORI))
          .link_2_11('Full')
        endif
        .DoRTCalc(22,23,.f.)
        if not(empty(.w_SKCODZON))
          .link_2_14('Full')
        endif
        .DoRTCalc(24,25,.f.)
        if not(empty(.w_SKCODGPR))
          .link_2_17('Full')
        endif
        .DoRTCalc(26,27,.f.)
        if not(empty(.w_SKCODAGE))
          .link_2_20('Full')
        endif
        .DoRTCalc(28,29,.f.)
        if not(empty(.w_SKCODUTE))
          .link_2_23('Full')
        endif
        .DoRTCalc(30,31,.f.)
        if not(empty(.w_SKGRUOP))
          .link_2_26('Full')
        endif
    endwith
    this.DoRTCalc(32,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .DoRTCalc(1,12,.t.)
        if .o_DATASTAM<>.w_DATASTAM
            .w_OBTEST = .w_DATASTAM
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSKSGRART_1_15.enabled = this.oPgFrm.Page1.oPag.oSKSGRART_1_15.mCond()
    this.oPgFrm.Page2.oPag.oSKCODUTE_2_23.enabled = this.oPgFrm.Page2.oPag.oSKCODUTE_2_23.mCond()
    this.oPgFrm.Page2.oPag.oSKGRUOP_2_26.enabled = this.oPgFrm.Page2.oPag.oSKGRUOP_2_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SKCODIC1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KITMPROM_IDX,3]
    i_lTable = "KITMPROM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2], .t., this.KITMPROM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODIC1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsof_mkp',True,'KITMPROM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" KPCODKIT like "+cp_ToStrODBC(trim(this.w_SKCODIC1)+"%");

          i_ret=cp_SQL(i_nConn,"select KPCODKIT,KPDESKIT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by KPCODKIT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'KPCODKIT',trim(this.w_SKCODIC1))
          select KPCODKIT,KPDESKIT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by KPCODKIT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODIC1)==trim(_Link_.KPCODKIT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SKCODIC1) and !this.bDontReportError
            deferred_cp_zoom('KITMPROM','*','KPCODKIT',cp_AbsName(oSource.parent,'oSKCODIC1_1_3'),i_cWhere,'gsof_mkp',"Kit promozionali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select KPCODKIT,KPDESKIT";
                     +" from "+i_cTable+" "+i_lTable+" where KPCODKIT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'KPCODKIT',oSource.xKey(1))
            select KPCODKIT,KPDESKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODIC1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select KPCODKIT,KPDESKIT";
                   +" from "+i_cTable+" "+i_lTable+" where KPCODKIT="+cp_ToStrODBC(this.w_SKCODIC1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'KPCODKIT',this.w_SKCODIC1)
            select KPCODKIT,KPDESKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODIC1 = NVL(_Link_.KPCODKIT,space(15))
      this.w_SKDES1 = NVL(_Link_.KPDESKIT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODIC1 = space(15)
      endif
      this.w_SKDES1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_SKCODIC2) OR  (UPPER(.w_SKCODIC1))<= UPPER(.w_SKCODIC2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice kit iniziale � maggiore di quello finale")
        endif
        this.w_SKCODIC1 = space(15)
        this.w_SKDES1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2])+'\'+cp_ToStr(_Link_.KPCODKIT,1)
      cp_ShowWarn(i_cKey,this.KITMPROM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODIC1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODIC2
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KITMPROM_IDX,3]
    i_lTable = "KITMPROM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2], .t., this.KITMPROM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODIC2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsof_mkp',True,'KITMPROM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" KPCODKIT like "+cp_ToStrODBC(trim(this.w_SKCODIC2)+"%");

          i_ret=cp_SQL(i_nConn,"select KPCODKIT,KPDESKIT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by KPCODKIT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'KPCODKIT',trim(this.w_SKCODIC2))
          select KPCODKIT,KPDESKIT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by KPCODKIT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODIC2)==trim(_Link_.KPCODKIT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SKCODIC2) and !this.bDontReportError
            deferred_cp_zoom('KITMPROM','*','KPCODKIT',cp_AbsName(oSource.parent,'oSKCODIC2_1_6'),i_cWhere,'gsof_mkp',"Kit promozionali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select KPCODKIT,KPDESKIT";
                     +" from "+i_cTable+" "+i_lTable+" where KPCODKIT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'KPCODKIT',oSource.xKey(1))
            select KPCODKIT,KPDESKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODIC2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select KPCODKIT,KPDESKIT";
                   +" from "+i_cTable+" "+i_lTable+" where KPCODKIT="+cp_ToStrODBC(this.w_SKCODIC2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'KPCODKIT',this.w_SKCODIC2)
            select KPCODKIT,KPDESKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODIC2 = NVL(_Link_.KPCODKIT,space(15))
      this.w_SKDES2 = NVL(_Link_.KPDESKIT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODIC2 = space(15)
      endif
      this.w_SKDES2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(UPPER(.w_SKCODIC1) <= UPPER(.w_SKCODIC2)) or empty(.w_SKCODIC2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice kit iniziale � maggiore di quello finale")
        endif
        this.w_SKCODIC2 = space(15)
        this.w_SKDES2 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2])+'\'+cp_ToStr(_Link_.KPCODKIT,1)
      cp_ShowWarn(i_cKey,this.KITMPROM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODIC2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODAS
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_SKCODAS)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_SKCODAS))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODAS)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_SKCODAS)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_SKCODAS)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SKCODAS) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oSKCODAS_1_9'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSVE_MDV.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SKCODAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SKCODAS)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODAS = NVL(_Link_.ARCODART,space(20))
      this.w_SKDESART = NVL(_Link_.ARDESART,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODAS = space(20)
      endif
      this.w_SKDESART = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_SKCODAS = space(20)
        this.w_SKDESART = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODARS
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIT_SEZI_IDX,3]
    i_lTable = "TIT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2], .t., this.TIT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODARS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATS',True,'TIT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TSCODICE like "+cp_ToStrODBC(trim(this.w_SKCODARS)+"%");

          i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TSCODICE',trim(this.w_SKCODARS))
          select TSCODICE,TSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODARS)==trim(_Link_.TSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SKCODARS) and !this.bDontReportError
            deferred_cp_zoom('TIT_SEZI','*','TSCODICE',cp_AbsName(oSource.parent,'oSKCODARS_1_12'),i_cWhere,'GSOF_ATS',"Gruppo articolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',oSource.xKey(1))
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODARS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(this.w_SKCODARS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',this.w_SKCODARS)
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODARS = NVL(_Link_.TSCODICE,space(5))
      this.w_DESARSE = NVL(_Link_.TSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODARS = space(5)
      endif
      this.w_DESARSE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.TSCODICE,1)
      cp_ShowWarn(i_cKey,this.TIT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODARS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKSGRART
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_lTable = "SOT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2], .t., this.SOT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKSGRART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ASF',True,'SOT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SGCODSOT like "+cp_ToStrODBC(trim(this.w_SKSGRART)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_SKCODARS);

          i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SGCODGRU,SGCODSOT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SGCODGRU',this.w_SKCODARS;
                     ,'SGCODSOT',trim(this.w_SKSGRART))
          select SGCODGRU,SGCODSOT,SGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SGCODGRU,SGCODSOT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKSGRART)==trim(_Link_.SGCODSOT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SKSGRART) and !this.bDontReportError
            deferred_cp_zoom('SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(oSource.parent,'oSKSGRART_1_15'),i_cWhere,'GSOF_ASF',"Sottogruppo articolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SKCODARS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SGCODGRU="+cp_ToStrODBC(this.w_SKCODARS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',oSource.xKey(1);
                       ,'SGCODSOT',oSource.xKey(2))
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKSGRART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(this.w_SKSGRART);
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_SKCODARS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',this.w_SKCODARS;
                       ,'SGCODSOT',this.w_SKSGRART)
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKSGRART = NVL(_Link_.SGCODSOT,space(5))
      this.w_DESSG = NVL(_Link_.SGDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKSGRART = space(5)
      endif
      this.w_DESSG = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.SGCODGRU,1)+'\'+cp_ToStr(_Link_.SGCODSOT,1)
      cp_ShowWarn(i_cKey,this.SOT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKSGRART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODVAL
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_SKCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_SKCODVAL))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_SKCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_SKCODVAL)+"%");

            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SKCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oSKCODVAL_2_2'),i_cWhere,'',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_SKCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_SKCODVAL)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SKDESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODVAL = space(3)
      endif
      this.w_SKDESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODGRU
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_SKCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_SKCODGRU))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODGRU)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SKCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oSKCODGRU_2_5'),i_cWhere,'',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_SKCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_SKCODGRU)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODGRU = NVL(_Link_.GNCODICE,space(5))
      this.w_DESCGN = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODGRU = space(5)
      endif
      this.w_DESCGN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODPRI
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODPRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_SKCODPRI)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_SKCODPRI))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODPRI)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SKCODPRI) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oSKCODPRI_2_8'),i_cWhere,'',"Gruppi priorit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODPRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_SKCODPRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_SKCODPRI)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODPRI = NVL(_Link_.GPCODICE,space(6))
      this.w_DESPRIO = NVL(_Link_.GPDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODPRI = space(6)
      endif
      this.w_DESPRIO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODPRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODORI
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_SKCODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_SKCODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SKCODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oSKCODORI_2_11'),i_cWhere,'',"Codici origine nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_SKCODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_SKCODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_DESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODORI = space(5)
      endif
      this.w_DESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODZON
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_SKCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_SKCODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SKCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oSKCODZON_2_14'),i_cWhere,'',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_SKCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_SKCODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZONE = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODZON = space(3)
      endif
      this.w_DESZONE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODGPR
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODGPR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_SKCODGPR)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_SKCODGPR))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODGPR)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SKCODGPR) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oSKCODGPR_2_17'),i_cWhere,'',"Gruppi priorit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODGPR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_SKCODGPR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_SKCODGPR)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODGPR = NVL(_Link_.GPCODICE,space(5))
      this.W_DESGPR = NVL(_Link_.GPDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODGPR = space(5)
      endif
      this.W_DESGPR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODGPR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODAGE
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_SKCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_SKCODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SKCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_SKCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_SKCODAGE)+"%");

            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SKCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oSKCODAGE_2_20'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_SKCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_SKCODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKCODUTE
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_SKCODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_SKCODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SKCODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oSKCODUTE_2_23'),i_cWhere,'',"Operatori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_SKCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_SKCODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKCODUTE = NVL(_Link_.code,0)
      this.w_DESOPE = NVL(_Link_.name,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKCODUTE = 0
      endif
      this.w_DESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SKGRUOP
  func Link_2_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SKGRUOP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_SKGRUOP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_SKGRUOP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SKGRUOP) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.parent,'oSKGRUOP_2_26'),i_cWhere,'',"Gruppi di utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SKGRUOP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_SKGRUOP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_SKGRUOP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SKGRUOP = NVL(_Link_.code,0)
      this.w_SKGRDES = NVL(_Link_.name,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SKGRUOP = 0
      endif
      this.w_SKGRDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SKGRUOP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATASTAM_1_2.value==this.w_DATASTAM)
      this.oPgFrm.Page1.oPag.oDATASTAM_1_2.value=this.w_DATASTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oSKCODIC1_1_3.value==this.w_SKCODIC1)
      this.oPgFrm.Page1.oPag.oSKCODIC1_1_3.value=this.w_SKCODIC1
    endif
    if not(this.oPgFrm.Page1.oPag.oSKDES1_1_4.value==this.w_SKDES1)
      this.oPgFrm.Page1.oPag.oSKDES1_1_4.value=this.w_SKDES1
    endif
    if not(this.oPgFrm.Page1.oPag.oSKCODIC2_1_6.value==this.w_SKCODIC2)
      this.oPgFrm.Page1.oPag.oSKCODIC2_1_6.value=this.w_SKCODIC2
    endif
    if not(this.oPgFrm.Page1.oPag.oSKDES2_1_7.value==this.w_SKDES2)
      this.oPgFrm.Page1.oPag.oSKDES2_1_7.value=this.w_SKDES2
    endif
    if not(this.oPgFrm.Page1.oPag.oSKCODAS_1_9.value==this.w_SKCODAS)
      this.oPgFrm.Page1.oPag.oSKCODAS_1_9.value=this.w_SKCODAS
    endif
    if not(this.oPgFrm.Page1.oPag.oSKDESART_1_10.value==this.w_SKDESART)
      this.oPgFrm.Page1.oPag.oSKDESART_1_10.value=this.w_SKDESART
    endif
    if not(this.oPgFrm.Page1.oPag.oSKCODARS_1_12.value==this.w_SKCODARS)
      this.oPgFrm.Page1.oPag.oSKCODARS_1_12.value=this.w_SKCODARS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESARSE_1_13.value==this.w_DESARSE)
      this.oPgFrm.Page1.oPag.oDESARSE_1_13.value=this.w_DESARSE
    endif
    if not(this.oPgFrm.Page1.oPag.oSKSGRART_1_15.value==this.w_SKSGRART)
      this.oPgFrm.Page1.oPag.oSKSGRART_1_15.value=this.w_SKSGRART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSG_1_16.value==this.w_DESSG)
      this.oPgFrm.Page1.oPag.oDESSG_1_16.value=this.w_DESSG
    endif
    if not(this.oPgFrm.Page1.oPag.oOBSOLETI_1_17.RadioValue()==this.w_OBSOLETI)
      this.oPgFrm.Page1.oPag.oOBSOLETI_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSKCODVAL_2_2.value==this.w_SKCODVAL)
      this.oPgFrm.Page2.oPag.oSKCODVAL_2_2.value=this.w_SKCODVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oSKDESVAL_2_3.value==this.w_SKDESVAL)
      this.oPgFrm.Page2.oPag.oSKDESVAL_2_3.value=this.w_SKDESVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oSKCODGRU_2_5.value==this.w_SKCODGRU)
      this.oPgFrm.Page2.oPag.oSKCODGRU_2_5.value=this.w_SKCODGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCGN_2_6.value==this.w_DESCGN)
      this.oPgFrm.Page2.oPag.oDESCGN_2_6.value=this.w_DESCGN
    endif
    if not(this.oPgFrm.Page2.oPag.oSKCODPRI_2_8.value==this.w_SKCODPRI)
      this.oPgFrm.Page2.oPag.oSKCODPRI_2_8.value=this.w_SKCODPRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRIO_2_9.value==this.w_DESPRIO)
      this.oPgFrm.Page2.oPag.oDESPRIO_2_9.value=this.w_DESPRIO
    endif
    if not(this.oPgFrm.Page2.oPag.oSKCODORI_2_11.value==this.w_SKCODORI)
      this.oPgFrm.Page2.oPag.oSKCODORI_2_11.value=this.w_SKCODORI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESORI_2_12.value==this.w_DESORI)
      this.oPgFrm.Page2.oPag.oDESORI_2_12.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oSKCODZON_2_14.value==this.w_SKCODZON)
      this.oPgFrm.Page2.oPag.oSKCODZON_2_14.value=this.w_SKCODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZONE_2_15.value==this.w_DESZONE)
      this.oPgFrm.Page2.oPag.oDESZONE_2_15.value=this.w_DESZONE
    endif
    if not(this.oPgFrm.Page2.oPag.oSKCODGPR_2_17.value==this.w_SKCODGPR)
      this.oPgFrm.Page2.oPag.oSKCODGPR_2_17.value=this.w_SKCODGPR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGPR_2_18.value==this.w_DESGPR)
      this.oPgFrm.Page2.oPag.oDESGPR_2_18.value=this.w_DESGPR
    endif
    if not(this.oPgFrm.Page2.oPag.oSKCODAGE_2_20.value==this.w_SKCODAGE)
      this.oPgFrm.Page2.oPag.oSKCODAGE_2_20.value=this.w_SKCODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_21.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_21.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oSKCODUTE_2_23.value==this.w_SKCODUTE)
      this.oPgFrm.Page2.oPag.oSKCODUTE_2_23.value=this.w_SKCODUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESOPE_2_24.value==this.w_DESOPE)
      this.oPgFrm.Page2.oPag.oDESOPE_2_24.value=this.w_DESOPE
    endif
    if not(this.oPgFrm.Page2.oPag.oSKGRUOP_2_26.value==this.w_SKGRUOP)
      this.oPgFrm.Page2.oPag.oSKGRUOP_2_26.value=this.w_SKGRUOP
    endif
    if not(this.oPgFrm.Page2.oPag.oSKGRDES_2_27.value==this.w_SKGRDES)
      this.oPgFrm.Page2.oPag.oSKGRDES_2_27.value=this.w_SKGRDES
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATASTAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATASTAM_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DATASTAM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_SKCODIC2) OR  (UPPER(.w_SKCODIC1))<= UPPER(.w_SKCODIC2))  and not(empty(.w_SKCODIC1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSKCODIC1_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice kit iniziale � maggiore di quello finale")
          case   not((UPPER(.w_SKCODIC1) <= UPPER(.w_SKCODIC2)) or empty(.w_SKCODIC2))  and not(empty(.w_SKCODIC2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSKCODIC2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice kit iniziale � maggiore di quello finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_SKCODAS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSKCODAS_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATASTAM = this.w_DATASTAM
    return

enddefine

* --- Define pages as container
define class tgsof_skpPag1 as StdContainer
  Width  = 636
  height = 276
  stdWidth  = 636
  stdheight = 276
  resizeXpos=412
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATASTAM_1_2 as StdField with uid="LYHJKIKYQV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATASTAM", cQueryName = "DATASTAM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza e di inizio validit�",;
    HelpContextID = 66298749,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=165, Top=11

  add object oSKCODIC1_1_3 as StdField with uid="FLQRTXUTOU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SKCODIC1", cQueryName = "SKCODIC1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice kit iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione dal codice kit promozionale",;
    HelpContextID = 265726121,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=165, Top=38, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="KITMPROM", cZoomOnZoom="gsof_mkp", oKey_1_1="KPCODKIT", oKey_1_2="this.w_SKCODIC1"

  func oSKCODIC1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODIC1_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODIC1_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KITMPROM','*','KPCODKIT',cp_AbsName(this.parent,'oSKCODIC1_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'gsof_mkp',"Kit promozionali",'',this.parent.oContained
  endproc
  proc oSKCODIC1_1_3.mZoomOnZoom
    local i_obj
    i_obj=gsof_mkp()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_KPCODKIT=this.parent.oContained.w_SKCODIC1
     i_obj.ecpSave()
  endproc

  add object oSKDES1_1_4 as StdField with uid="RCFFKFXLNJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SKDES1", cQueryName = "SKDES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 116431066,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=335, Top=38, InputMask=replicate('X',35)

  add object oSKCODIC2_1_6 as StdField with uid="FTHVFQKLLI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SKCODIC2", cQueryName = "SKCODIC2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice kit iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione al codice kit promozionale",;
    HelpContextID = 265726120,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=165, Top=65, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="KITMPROM", cZoomOnZoom="gsof_mkp", oKey_1_1="KPCODKIT", oKey_1_2="this.w_SKCODIC2"

  func oSKCODIC2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODIC2_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODIC2_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KITMPROM','*','KPCODKIT',cp_AbsName(this.parent,'oSKCODIC2_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'gsof_mkp',"Kit promozionali",'',this.parent.oContained
  endproc
  proc oSKCODIC2_1_6.mZoomOnZoom
    local i_obj
    i_obj=gsof_mkp()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_KPCODKIT=this.parent.oContained.w_SKCODIC2
     i_obj.ecpSave()
  endproc

  add object oSKDES2_1_7 as StdField with uid="TKYSFKDMDA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SKDES2", cQueryName = "SKDES2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 99653850,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=335, Top=65, InputMask=replicate('X',35)

  add object oSKCODAS_1_9 as StdField with uid="LSXKUYHOCQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SKCODAS", cQueryName = "SKCODAS",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Codice articolo/servizio",;
    HelpContextID = 131508442,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=165, Top=92, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_SKCODAS"

  func oSKCODAS_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODAS_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODAS_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oSKCODAS_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSVE_MDV.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oSKCODAS_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_SKCODAS
     i_obj.ecpSave()
  endproc

  add object oSKDESART_1_10 as StdField with uid="GBQFBYCEVI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SKDESART", cQueryName = "SKDESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 116430982,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=335, Top=92, InputMask=replicate('X',35)

  add object oSKCODARS_1_12 as StdField with uid="ZUAGNZHVYK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SKCODARS", cQueryName = "SKCODARS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo articolo",;
    HelpContextID = 131508359,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=165, Top=119, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIT_SEZI", cZoomOnZoom="GSOF_ATS", oKey_1_1="TSCODICE", oKey_1_2="this.w_SKCODARS"

  func oSKCODARS_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
      if .not. empty(.w_SKSGRART)
        bRes2=.link_1_15('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSKCODARS_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODARS_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIT_SEZI','*','TSCODICE',cp_AbsName(this.parent,'oSKCODARS_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATS',"Gruppo articolo",'',this.parent.oContained
  endproc
  proc oSKCODARS_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TSCODICE=this.parent.oContained.w_SKCODARS
     i_obj.ecpSave()
  endproc

  add object oDESARSE_1_13 as StdField with uid="BJZFCWWHMN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESARSE", cQueryName = "DESARSE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 184307766,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=231, Top=119, InputMask=replicate('X',35)

  add object oSKSGRART_1_15 as StdField with uid="XNRDACNZDH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SKSGRART", cQueryName = "SKSGRART",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sottogruppo articolo",;
    HelpContextID = 117287046,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=165, Top=146, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SOT_SEZI", cZoomOnZoom="GSOF_ASF", oKey_1_1="SGCODGRU", oKey_1_2="this.w_SKCODARS", oKey_2_1="SGCODSOT", oKey_2_2="this.w_SKSGRART"

  func oSKSGRART_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_SKCODARS))
    endwith
   endif
  endfunc

  func oSKSGRART_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKSGRART_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKSGRART_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SOT_SEZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStrODBC(this.Parent.oContained.w_SKCODARS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStr(this.Parent.oContained.w_SKCODARS)
    endif
    do cp_zoom with 'SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(this.parent,'oSKSGRART_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ASF',"Sottogruppo articolo",'',this.parent.oContained
  endproc
  proc oSKSGRART_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ASF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SGCODGRU=w_SKCODARS
     i_obj.w_SGCODSOT=this.parent.oContained.w_SKSGRART
     i_obj.ecpSave()
  endproc

  add object oDESSG_1_16 as StdField with uid="TMSEEOBJVX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESSG", cQueryName = "DESSG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 123621430,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=232, Top=146, InputMask=replicate('X',35)

  add object oOBSOLETI_1_17 as StdCheck with uid="LLGAKQJVHH",rtseq=12,rtrep=.f.,left=165, top=172, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo gli obsoleti",;
    HelpContextID = 55947729,;
    cFormVar="w_OBSOLETI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOBSOLETI_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOBSOLETI_1_17.GetRadio()
    this.Parent.oContained.w_OBSOLETI = this.RadioValue()
    return .t.
  endfunc

  func oOBSOLETI_1_17.SetRadio()
    this.Parent.oContained.w_OBSOLETI=trim(this.Parent.oContained.w_OBSOLETI)
    this.value = ;
      iif(this.Parent.oContained.w_OBSOLETI=='S',1,;
      0)
  endfunc


  add object oBtn_1_20 as StdButton with uid="ITMTBCXVOX",left=529, top=227, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 43394790;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oObj_1_21 as cp_outputCombo with uid="ZLERXIVPWT",left=165, top=195, width=463,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 47063578


  add object oBtn_1_22 as StdButton with uid="PQLYMEEARO",left=580, top=227, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 170776314;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=12, Top=38,;
    Alignment=1, Width=152, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BRSOYYEROT",Visible=.t., Left=12, Top=65,;
    Alignment=1, Width=152, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="SVICDQDGGX",Visible=.t., Left=12, Top=92,;
    Alignment=1, Width=152, Height=18,;
    Caption="Articolo/servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="CABKFPXKCU",Visible=.t., Left=12, Top=119,;
    Alignment=1, Width=152, Height=18,;
    Caption="Gruppo articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="EBDPPYFBUW",Visible=.t., Left=12, Top=146,;
    Alignment=1, Width=152, Height=18,;
    Caption="Sottogruppo articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KHWVLZCVUG",Visible=.t., Left=12, Top=195,;
    Alignment=1, Width=152, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="KOTSVIFBOL",Visible=.t., Left=12, Top=11,;
    Alignment=1, Width=152, Height=18,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_skpPag2 as StdContainer
  Width  = 636
  height = 276
  stdWidth  = 636
  stdheight = 276
  resizeXpos=381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSKCODVAL_2_2 as StdField with uid="MRMYEZOMSF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SKCODVAL", cQueryName = "SKCODVAL",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 47622286,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=186, Top=12, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_SKCODVAL"

  func oSKCODVAL_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODVAL_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODVAL_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oSKCODVAL_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Valute",'',this.parent.oContained
  endproc

  add object oSKDESVAL_2_3 as StdField with uid="YIDSVCSAPP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SKDESVAL", cQueryName = "SKDESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 32544910,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=12, InputMask=replicate('X',35)

  add object oSKCODGRU_2_5 as StdField with uid="UNPJLMPEEC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_SKCODGRU", cQueryName = "SKCODGRU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo di appartenenza",;
    HelpContextID = 30845061,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=186, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", oKey_1_1="GNCODICE", oKey_1_2="this.w_SKCODGRU"

  func oSKCODGRU_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODGRU_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODGRU_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oSKCODGRU_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi nominativi",'',this.parent.oContained
  endproc

  add object oDESCGN_2_6 as StdField with uid="OXGVGSHZVB",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCGN", cQueryName = "DESCGN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 179417034,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=36, InputMask=replicate('X',35)

  add object oSKCODPRI_2_8 as StdField with uid="KUEIEJAHIM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SKCODPRI", cQueryName = "SKCODPRI",;
    bObbl = .f. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Priorit� da attribuire al nominativo",;
    HelpContextID = 148285585,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=186, Top=60, cSayPict='"XXXXXX"', cGetPict='"XXXXXX"', InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="GRU_PRIO", oKey_1_1="GPCODICE", oKey_1_2="this.w_SKCODPRI"

  func oSKCODPRI_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODPRI_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODPRI_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oSKCODPRI_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi priorit�",'',this.parent.oContained
  endproc

  add object oDESPRIO_2_9 as StdField with uid="XTKEVJMOAS",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESPRIO", cQueryName = "DESPRIO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 250916810,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=60, InputMask=replicate('X',35)

  add object oSKCODORI_2_11 as StdField with uid="PNBSTVXJHA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_SKCODORI", cQueryName = "SKCODORI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine reperimento nominativo",;
    HelpContextID = 165062801,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=186, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", oKey_1_1="ONCODICE", oKey_1_2="this.w_SKCODORI"

  func oSKCODORI_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODORI_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODORI_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oSKCODORI_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici origine nominativi",'',this.parent.oContained
  endproc

  add object oDESORI_2_12 as StdField with uid="FBXJICCBRH",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 250982346,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=84, InputMask=replicate('X',35)

  add object oSKCODZON_2_14 as StdField with uid="JEIWNKIDFS",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SKCODZON", cQueryName = "SKCODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice zona",;
    HelpContextID = 248948876,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=186, Top=108, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_SKCODZON"

  func oSKCODZON_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODZON_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODZON_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oSKCODZON_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Zone",'',this.parent.oContained
  endproc

  add object oDESZONE_2_15 as StdField with uid="PJLDMIGCKS",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESZONE", cQueryName = "DESZONE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 98914358,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=108, InputMask=replicate('X',35)

  add object oSKCODGPR_2_17 as StdField with uid="UEJLNEBHYZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SKCODGPR", cQueryName = "SKCODGPR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppi priorit� offerta",;
    HelpContextID = 30845064,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=186, Top=132, cSayPict='"XXXXXX"', cGetPict='"XXXXXX"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", oKey_1_1="GPCODICE", oKey_1_2="this.w_SKCODGPR"

  func oSKCODGPR_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODGPR_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODGPR_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oSKCODGPR_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi priorit�",'',this.parent.oContained
  endproc

  add object oDESGPR_2_18 as StdField with uid="LMDTBSVTVH",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESGPR", cQueryName = "DESGPR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 102608842,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=132, InputMask=replicate('X',35)

  add object oSKCODAGE_2_20 as StdField with uid="UDANMVNSCD",rtseq=27,rtrep=.f.,;
    cFormVar = "w_SKCODAGE", cQueryName = "SKCODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente",;
    HelpContextID = 136927083,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=186, Top=156, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_SKCODAGE"

  func oSKCODAGE_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODAGE_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODAGE_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oSKCODAGE_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oDESAGE_2_21 as StdField with uid="NZBPQJVISD",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione agente",;
    HelpContextID = 62107594,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=156, InputMask=replicate('X',35)

  add object oSKCODUTE_2_23 as StdField with uid="WUQFLGQQWQ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_SKCODUTE", cQueryName = "SKCODUTE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 204035947,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=186, Top=180, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_SKCODUTE"

  func oSKCODUTE_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SKGRUOP=0)
    endwith
   endif
  endfunc

  func oSKCODUTE_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKCODUTE_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKCODUTE_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oSKCODUTE_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Operatori",'',this.parent.oContained
  endproc

  add object oDESOPE_2_24 as StdField with uid="UODXIIMSHN",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESOPE", cQueryName = "DESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice operatore",;
    HelpContextID = 51752906,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=180, InputMask=replicate('X',35)

  add object oSKGRUOP_2_26 as StdField with uid="NYLEZDRWIX",rtseq=31,rtrep=.f.,;
    cFormVar = "w_SKGRUOP", cQueryName = "SKGRUOP",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Gruppo utenti",;
    HelpContextID = 147024090,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=186, Top=203, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_SKGRUOP"

  func oSKGRUOP_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SKCODUTE=0)
    endwith
   endif
  endfunc

  func oSKGRUOP_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oSKGRUOP_2_26.ecpDrop(oSource)
    this.Parent.oContained.link_2_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSKGRUOP_2_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','code',cp_AbsName(this.parent,'oSKGRUOP_2_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi di utenti",'',this.parent.oContained
  endproc

  add object oSKGRDES_2_27 as StdField with uid="ZETAHWOEIM",rtseq=32,rtrep=.f.,;
    cFormVar = "w_SKGRDES", cQueryName = "SKGRDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 64186586,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=250, Top=203, InputMask=replicate('X',35)

  add object oStr_2_1 as StdString with uid="BBBPIDGSFA",Visible=.t., Left=10, Top=12,;
    Alignment=1, Width=171, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="GYZPKGJJXH",Visible=.t., Left=10, Top=36,;
    Alignment=1, Width=171, Height=18,;
    Caption="Gruppo nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="EIVPTJURQF",Visible=.t., Left=10, Top=60,;
    Alignment=1, Width=171, Height=18,;
    Caption="Gr.priorit� nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="JINCGGAKVH",Visible=.t., Left=10, Top=84,;
    Alignment=1, Width=171, Height=18,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="GGMQEJEICX",Visible=.t., Left=10, Top=108,;
    Alignment=1, Width=171, Height=18,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="UDIDVXBQJE",Visible=.t., Left=10, Top=132,;
    Alignment=1, Width=171, Height=18,;
    Caption="Gr.priorit� offerta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="EDIGVBZFUW",Visible=.t., Left=10, Top=156,;
    Alignment=1, Width=171, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="FSDBXSLPSK",Visible=.t., Left=10, Top=180,;
    Alignment=1, Width=171, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="FNQTWQUCYN",Visible=.t., Left=10, Top=203,;
    Alignment=1, Width=171, Height=18,;
    Caption="Gruppo utenti:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_skp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
