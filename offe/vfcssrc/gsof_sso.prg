* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_sso                                                        *
*              Stampa sezioni offerta                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-26                                                      *
* Last revis.: 2007-07-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_sso",oParentObject))

* --- Class definition
define class tgsof_sso as StdForm
  Top    = 13
  Left   = 11

  * --- Standard Properties
  Width  = 586
  Height = 162
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-20"
  HelpContextID=177591959
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  OFF_NOMI_IDX = 0
  SEZ_OFFE_IDX = 0
  cPrg = "gsof_sso"
  cComment = "Stampa sezioni offerta"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SOCODIC1 = space(10)
  w_SODESNO1 = space(40)
  w_SOCODIC2 = space(10)
  w_SODESNO2 = space(40)
  w_SOTIPSEZ = space(1)
  w_SOSTNOTE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_ssoPag1","gsof_sso",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSOCODIC1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='SEZ_OFFE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SOCODIC1=space(10)
      .w_SODESNO1=space(40)
      .w_SOCODIC2=space(10)
      .w_SODESNO2=space(40)
      .w_SOTIPSEZ=space(1)
      .w_SOSTNOTE=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_SOCODIC1))
          .link_1_2('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_SOCODIC2))
          .link_1_5('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_SOTIPSEZ = 'U'
        .w_SOSTNOTE = 'S'
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SOCODIC1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_lTable = "SEZ_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2], .t., this.SEZ_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODIC1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsof_aso',True,'SEZ_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SOCODICE like "+cp_ToStrODBC(trim(this.w_SOCODIC1)+"%");

          i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SOCODICE',trim(this.w_SOCODIC1))
          select SOCODICE,SODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODIC1)==trim(_Link_.SOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODIC1) and !this.bDontReportError
            deferred_cp_zoom('SEZ_OFFE','*','SOCODICE',cp_AbsName(oSource.parent,'oSOCODIC1_1_2'),i_cWhere,'gsof_aso',"Sezioni offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',oSource.xKey(1))
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODIC1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(this.w_SOCODIC1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',this.w_SOCODIC1)
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODIC1 = NVL(_Link_.SOCODICE,space(10))
      this.w_SODESNO1 = NVL(_Link_.SODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODIC1 = space(10)
      endif
      this.w_SODESNO1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_SOCODIC2) OR  (UPPER(.w_SOCODIC1)<= UPPER(.w_SOCODIC2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice sezione iniziale � maggiore di quello finale")
        endif
        this.w_SOCODIC1 = space(10)
        this.w_SODESNO1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.SOCODICE,1)
      cp_ShowWarn(i_cKey,this.SEZ_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODIC1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODIC2
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEZ_OFFE_IDX,3]
    i_lTable = "SEZ_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2], .t., this.SEZ_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODIC2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsof_aso',True,'SEZ_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SOCODICE like "+cp_ToStrODBC(trim(this.w_SOCODIC2)+"%");

          i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SOCODICE',trim(this.w_SOCODIC2))
          select SOCODICE,SODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODIC2)==trim(_Link_.SOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODIC2) and !this.bDontReportError
            deferred_cp_zoom('SEZ_OFFE','*','SOCODICE',cp_AbsName(oSource.parent,'oSOCODIC2_1_5'),i_cWhere,'gsof_aso',"Sezioni offerta",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',oSource.xKey(1))
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODIC2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,SODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(this.w_SOCODIC2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',this.w_SOCODIC2)
            select SOCODICE,SODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODIC2 = NVL(_Link_.SOCODICE,space(10))
      this.w_SODESNO2 = NVL(_Link_.SODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODIC2 = space(10)
      endif
      this.w_SODESNO2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(UPPER(.w_SOCODIC1) <= UPPER(.w_SOCODIC2)) or empty(.w_SOCODIC2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice sezione iniziale � maggiore di quello finale")
        endif
        this.w_SOCODIC2 = space(10)
        this.w_SODESNO2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEZ_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.SOCODICE,1)
      cp_ShowWarn(i_cKey,this.SEZ_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODIC2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSOCODIC1_1_2.value==this.w_SOCODIC1)
      this.oPgFrm.Page1.oPag.oSOCODIC1_1_2.value=this.w_SOCODIC1
    endif
    if not(this.oPgFrm.Page1.oPag.oSODESNO1_1_3.value==this.w_SODESNO1)
      this.oPgFrm.Page1.oPag.oSODESNO1_1_3.value=this.w_SODESNO1
    endif
    if not(this.oPgFrm.Page1.oPag.oSOCODIC2_1_5.value==this.w_SOCODIC2)
      this.oPgFrm.Page1.oPag.oSOCODIC2_1_5.value=this.w_SOCODIC2
    endif
    if not(this.oPgFrm.Page1.oPag.oSODESNO2_1_6.value==this.w_SODESNO2)
      this.oPgFrm.Page1.oPag.oSODESNO2_1_6.value=this.w_SODESNO2
    endif
    if not(this.oPgFrm.Page1.oPag.oSOTIPSEZ_1_8.RadioValue()==this.w_SOTIPSEZ)
      this.oPgFrm.Page1.oPag.oSOTIPSEZ_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSOSTNOTE_1_9.RadioValue()==this.w_SOSTNOTE)
      this.oPgFrm.Page1.oPag.oSOSTNOTE_1_9.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_SOCODIC2) OR  (UPPER(.w_SOCODIC1)<= UPPER(.w_SOCODIC2)))  and not(empty(.w_SOCODIC1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSOCODIC1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice sezione iniziale � maggiore di quello finale")
          case   not((UPPER(.w_SOCODIC1) <= UPPER(.w_SOCODIC2)) or empty(.w_SOCODIC2))  and not(empty(.w_SOCODIC2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSOCODIC2_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice sezione iniziale � maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsof_ssoPag1 as StdContainer
  Width  = 582
  height = 162
  stdWidth  = 582
  stdheight = 162
  resizeXpos=350
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSOCODIC1_1_2 as StdField with uid="FLQRTXUTOU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SOCODIC1", cQueryName = "SOCODIC1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice sezione iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione dal codice sezione",;
    HelpContextID = 131507369,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=119, Top=6, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SEZ_OFFE", cZoomOnZoom="gsof_aso", oKey_1_1="SOCODICE", oKey_1_2="this.w_SOCODIC1"

  func oSOCODIC1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODIC1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODIC1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SEZ_OFFE','*','SOCODICE',cp_AbsName(this.parent,'oSOCODIC1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'gsof_aso',"Sezioni offerta",'',this.parent.oContained
  endproc
  proc oSOCODIC1_1_2.mZoomOnZoom
    local i_obj
    i_obj=gsof_aso()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SOCODICE=this.parent.oContained.w_SOCODIC1
     i_obj.ecpSave()
  endproc

  add object oSODESNO1_1_3 as StdField with uid="RCFFKFXLNJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SODESNO1", cQueryName = "SODESNO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32543913,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=235, Top=7, InputMask=replicate('X',40)

  add object oSOCODIC2_1_5 as StdField with uid="VIKAEKGHKM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SOCODIC2", cQueryName = "SOCODIC2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice sezione iniziale � maggiore di quello finale",;
    ToolTipText = "Selezione al codice sezione",;
    HelpContextID = 131507368,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=119, Top=31, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SEZ_OFFE", cZoomOnZoom="gsof_aso", oKey_1_1="SOCODICE", oKey_1_2="this.w_SOCODIC2"

  func oSOCODIC2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODIC2_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODIC2_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SEZ_OFFE','*','SOCODICE',cp_AbsName(this.parent,'oSOCODIC2_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'gsof_aso',"Sezioni offerta",'',this.parent.oContained
  endproc
  proc oSOCODIC2_1_5.mZoomOnZoom
    local i_obj
    i_obj=gsof_aso()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SOCODICE=this.parent.oContained.w_SOCODIC2
     i_obj.ecpSave()
  endproc

  add object oSODESNO2_1_6 as StdField with uid="TKYSFKDMDA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SODESNO2", cQueryName = "SODESNO2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32543912,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=235, Top=32, InputMask=replicate('X',40)


  add object oSOTIPSEZ_1_8 as StdCombo with uid="SCSYQNMLUC",rtseq=5,rtrep=.f.,left=120,top=58,width=111,height=21;
    , ToolTipText = "Scelta tipologia sezione offerta: descrittiva, file WP, articoli, gruppi, sottogruppi, dati fissi, tutti";
    , HelpContextID = 48524160;
    , cFormVar="w_SOTIPSEZ",RowSource=""+"Descrittiva,"+"File WP,"+"Articoli,"+"Gruppi,"+"Sottogruppi,"+"Dati fissi,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSOTIPSEZ_1_8.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'W',;
    iif(this.value =3,'A',;
    iif(this.value =4,'G',;
    iif(this.value =5,'S',;
    iif(this.value =6,'T',;
    iif(this.value =7,'U',;
    space(1)))))))))
  endfunc
  func oSOTIPSEZ_1_8.GetRadio()
    this.Parent.oContained.w_SOTIPSEZ = this.RadioValue()
    return .t.
  endfunc

  func oSOTIPSEZ_1_8.SetRadio()
    this.Parent.oContained.w_SOTIPSEZ=trim(this.Parent.oContained.w_SOTIPSEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SOTIPSEZ=='D',1,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='W',2,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='A',3,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='G',4,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='S',5,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='T',6,;
      iif(this.Parent.oContained.w_SOTIPSEZ=='U',7,;
      0)))))))
  endfunc

  add object oSOSTNOTE_1_9 as StdCheck with uid="KFBYLJJCQL",rtseq=6,rtrep=.f.,left=235, top=59, caption="Stampa note",;
    ToolTipText = "Se attivo: stampa le note",;
    HelpContextID = 19965077,;
    cFormVar="w_SOSTNOTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSOSTNOTE_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSOSTNOTE_1_9.GetRadio()
    this.Parent.oContained.w_SOSTNOTE = this.RadioValue()
    return .t.
  endfunc

  func oSOSTNOTE_1_9.SetRadio()
    this.Parent.oContained.w_SOSTNOTE=trim(this.Parent.oContained.w_SOSTNOTE)
    this.value = ;
      iif(this.Parent.oContained.w_SOSTNOTE=='S',1,;
      0)
  endfunc


  add object oObj_1_11 as cp_outputCombo with uid="ZLERXIVPWT",left=119, top=86, width=446,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181281306


  add object oBtn_1_12 as StdButton with uid="ABCQEZBIEV",left=466, top=112, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 177612518;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="ZWVWIQYTYY",left=517, top=112, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 36558586;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=11, Top=7,;
    Alignment=1, Width=106, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="BRSOYYEROT",Visible=.t., Left=11, Top=32,;
    Alignment=1, Width=106, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="CFWPGFRFCB",Visible=.t., Left=11, Top=58,;
    Alignment=1, Width=106, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="PXGRYNJQTA",Visible=.t., Left=11, Top=86,;
    Alignment=1, Width=106, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_sso','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
