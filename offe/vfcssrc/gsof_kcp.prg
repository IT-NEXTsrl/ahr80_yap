* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_kcp                                                        *
*              Sostituzione codifica                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_61]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-17                                                      *
* Last revis.: 2008-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_kcp",oParentObject))

* --- Class definition
define class tgsof_kcp as StdForm
  Top    = 34
  Left   = 92

  * --- Standard Properties
  Width  = 572
  Height = 126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-26"
  HelpContextID=169203351
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsof_kcp"
  cComment = "Sostituzione codifica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODICE = space(15)
  w_CODCLI = space(20)
  w_OK = .F.
  w_ANDESCRI = space(40)
  w_NEWCOD = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_kcpPag1","gsof_kcp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNEWCOD_1_9
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICE=space(15)
      .w_CODCLI=space(20)
      .w_OK=.f.
      .w_ANDESCRI=space(40)
      .w_NEWCOD=space(20)
      .w_CODICE=oParentObject.w_CODICE
      .w_CODCLI=oParentObject.w_CODCLI
      .w_OK=oParentObject.w_OK
      .w_ANDESCRI=oParentObject.w_ANDESCRI
      .w_NEWCOD=oParentObject.w_NEWCOD
          .DoRTCalc(1,2,.f.)
        .w_OK = .T.
    endwith
    this.DoRTCalc(4,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODICE=.w_CODICE
      .oParentObject.w_CODCLI=.w_CODCLI
      .oParentObject.w_OK=.w_OK
      .oParentObject.w_ANDESCRI=.w_ANDESCRI
      .oParentObject.w_NEWCOD=.w_NEWCOD
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_1.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_1.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLI_1_3.value==this.w_CODCLI)
      this.oPgFrm.Page1.oPag.oCODCLI_1_3.value=this.w_CODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_8.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_8.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWCOD_1_9.value==this.w_NEWCOD)
      this.oPgFrm.Page1.oPag.oNEWCOD_1_9.value=this.w_NEWCOD
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(OFCHKCLI(.w_NEWCOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNEWCOD_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codifica gi� presente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsof_kcpPag1 as StdContainer
  Width  = 568
  height = 126
  stdWidth  = 568
  stdheight = 126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE_1_1 as StdField with uid="QIZJIBZDJE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice presente nell'anagrafica clienti",;
    HelpContextID = 208442842,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=146, Top=15, InputMask=replicate('X',15)

  add object oCODCLI_1_3 as StdField with uid="ZDFXSLSOCW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCLI", cQueryName = "CODCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice del nominativo gi� caricato nel modulo offerte o codifica incongruente rispetto alla struttura",;
    HelpContextID = 132290010,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=146, Top=41, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20)


  add object oBtn_1_5 as StdButton with uid="PJPAIVLNBZ",left=454, top=74, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 169203446;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NEWCOD))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="KYWZICSLCS",left=505, top=74, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 169203446;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oANDESCRI_1_8 as StdField with uid="LPQTBMMGYU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione codice cliente",;
    HelpContextID = 225482417,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=15, InputMask=replicate('X',40)

  add object oNEWCOD_1_9 as StdField with uid="KDXWCZOXUQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NEWCOD", cQueryName = "NEWCOD",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codifica gi� presente",;
    ToolTipText = "Nuovo codice da impostare per i nominativi",;
    HelpContextID = 212954922,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=146, Top=67, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20)

  func oNEWCOD_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (OFCHKCLI(.w_NEWCOD))
    endwith
    return bRes
  endfunc

  add object oStr_1_2 as StdString with uid="QFMYIYLMXM",Visible=.t., Left=6, Top=16,;
    Alignment=1, Width=137, Height=18,;
    Caption="Codifica cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="MMBLYAXUIW",Visible=.t., Left=6, Top=42,;
    Alignment=1, Width=137, Height=18,;
    Caption="Codifica nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="GHCXJJMIPK",Visible=.t., Left=6, Top=68,;
    Alignment=1, Width=137, Height=18,;
    Caption="Nuova codifica:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_kcp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
