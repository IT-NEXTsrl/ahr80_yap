* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bkp                                                        *
*              Controllo obsolescenza articolo kit                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-06                                                      *
* Last revis.: 2005-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bkp",oParentObject)
return(i_retval)

define class tgsof_bkp as StdBatch
  * --- Local variables
  w_DTOBS1 = ctod("  /  /  ")
  * --- WorkFile variables
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo di Obsolescenza Articolo da Kit Promozionali Offerte GSOF_MKP
    if Not Empty(this.oParentObject.w_KPCODICE)
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CADTOBSO"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_KPCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CADTOBSO;
          from (i_cTable) where;
              CACODICE = this.oParentObject.w_KPCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.W_DTOBS1 = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se � stata modificata la data di obsolescenza di un articolo in un ordine allora non deve essere visualizzato
      if NOT EMPTY(this.W_DTOBS1) AND this.W_DTOBS1<=i_DATSYS AND NOT EMPTY(this.oParentObject.w_KPCODICE)
        this.oParentObject.w_KPCODICE = Space(20)
        this.oParentObject.w_KPDESART = Space(40)
        AH_ErrorMsg("Codice articolo obsoleto dal %1",48,"",dtoc(this.w_dtobs1))
        i_retcode = 'stop'
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='KEY_ARTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
