* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_sgp                                                        *
*              Stampa gruppi priorit�                                          *
*                                                                              *
*      Author: Merone Francesco                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_40]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-15                                                      *
* Last revis.: 2008-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_sgp",oParentObject))

* --- Class definition
define class tgsof_sgp as StdForm
  Top    = 106
  Left   = 85

  * --- Standard Properties
  Width  = 507
  Height = 222
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-29"
  HelpContextID=244700823
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  GRU_PRIO_IDX = 0
  cPrg = "gsof_sgp"
  cComment = "Stampa gruppi priorit�"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATASTAM = ctod('  /  /  ')
  o_DATASTAM = ctod('  /  /  ')
  w_SGPCOD1 = space(5)
  w_DESPRI1 = space(35)
  w_SGPCOD2 = space(5)
  w_DESPRI2 = space(35)
  w_NUMERO = 0
  w_NUMERO1 = 0
  w_SGPCKOB = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_sgpPag1","gsof_sgp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATASTAM_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='GRU_PRIO'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsof_sgp
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATASTAM=ctod("  /  /  ")
      .w_SGPCOD1=space(5)
      .w_DESPRI1=space(35)
      .w_SGPCOD2=space(5)
      .w_DESPRI2=space(35)
      .w_NUMERO=0
      .w_NUMERO1=0
      .w_SGPCKOB=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
        .w_DATASTAM = i_datsys
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SGPCOD1))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_SGPCOD2))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_NUMERO = 1
        .w_NUMERO1 = 999999
        .w_SGPCKOB = 'N'
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_OBTEST = .w_DATASTAM
    endwith
    this.DoRTCalc(10,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(1,8,.t.)
        if .o_DATASTAM<>.w_DATASTAM
            .w_OBTEST = .w_DATASTAM
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SGPCOD1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SGPCOD1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AGP',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_SGPCOD1)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_SGPCOD1))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SGPCOD1)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SGPCOD1) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oSGPCOD1_1_2'),i_cWhere,'GSOF_AGP',"Gruppi priorit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SGPCOD1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_SGPCOD1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_SGPCOD1)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SGPCOD1 = NVL(_Link_.GPCODICE,space(5))
      this.w_DESPRI1 = NVL(_Link_.GPDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SGPCOD1 = space(5)
      endif
      this.w_DESPRI1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_sgpcod2) OR  (UPPER(.w_sgpcod1)<= UPPER(.w_sgpcod2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
        endif
        this.w_SGPCOD1 = space(5)
        this.w_DESPRI1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SGPCOD1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SGPCOD2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SGPCOD2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AGP',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_SGPCOD2)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_SGPCOD2))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SGPCOD2)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SGPCOD2) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oSGPCOD2_1_4'),i_cWhere,'GSOF_AGP',"Gruppi priorit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SGPCOD2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_SGPCOD2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_SGPCOD2)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SGPCOD2 = NVL(_Link_.GPCODICE,space(5))
      this.w_DESPRI2 = NVL(_Link_.GPDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SGPCOD2 = space(5)
      endif
      this.w_DESPRI2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(UPPER(.w_sgpcod1) <= UPPER(.w_sgpcod2)) or empty(.w_sgpcod2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
        endif
        this.w_SGPCOD2 = space(5)
        this.w_DESPRI2 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SGPCOD2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATASTAM_1_1.value==this.w_DATASTAM)
      this.oPgFrm.Page1.oPag.oDATASTAM_1_1.value=this.w_DATASTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oSGPCOD1_1_2.value==this.w_SGPCOD1)
      this.oPgFrm.Page1.oPag.oSGPCOD1_1_2.value=this.w_SGPCOD1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRI1_1_3.value==this.w_DESPRI1)
      this.oPgFrm.Page1.oPag.oDESPRI1_1_3.value=this.w_DESPRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oSGPCOD2_1_4.value==this.w_SGPCOD2)
      this.oPgFrm.Page1.oPag.oSGPCOD2_1_4.value=this.w_SGPCOD2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRI2_1_5.value==this.w_DESPRI2)
      this.oPgFrm.Page1.oPag.oDESPRI2_1_5.value=this.w_DESPRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_6.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_6.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO1_1_7.value==this.w_NUMERO1)
      this.oPgFrm.Page1.oPag.oNUMERO1_1_7.value=this.w_NUMERO1
    endif
    if not(this.oPgFrm.Page1.oPag.oSGPCKOB_1_8.RadioValue()==this.w_SGPCKOB)
      this.oPgFrm.Page1.oPag.oSGPCKOB_1_8.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATASTAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATASTAM_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DATASTAM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_sgpcod2) OR  (UPPER(.w_sgpcod1)<= UPPER(.w_sgpcod2)))  and not(empty(.w_SGPCOD1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSGPCOD1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
          case   not((UPPER(.w_sgpcod1) <= UPPER(.w_sgpcod2)) or empty(.w_sgpcod2))  and not(empty(.w_SGPCOD2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSGPCOD2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
          case   not((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale priorit� � pi� grande del numero finale priorit�")
          case   not((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO1_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale priorit� � pi� grande del numero finale priorit�")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATASTAM = this.w_DATASTAM
    return

enddefine

* --- Define pages as container
define class tgsof_sgpPag1 as StdContainer
  Width  = 503
  height = 222
  stdWidth  = 503
  stdheight = 222
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATASTAM_1_1 as StdField with uid="LYHJKIKYQV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATASTAM", cQueryName = "DATASTAM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 133407613,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=110, Top=4

  add object oSGPCOD1_1_2 as StdField with uid="FLQRTXUTOU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SGPCOD1", cQueryName = "SGPCOD1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Dal codice priorit�",;
    HelpContextID = 137485530,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=110, Top=27, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", cZoomOnZoom="GSOF_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_SGPCOD1"

  func oSGPCOD1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSGPCOD1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSGPCOD1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oSGPCOD1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AGP',"Gruppi priorit�",'',this.parent.oContained
  endproc
  proc oSGPCOD1_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_SGPCOD1
     i_obj.ecpSave()
  endproc

  add object oDESPRI1_1_3 as StdField with uid="VGEXINUONK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESPRI1", cQueryName = "DESPRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 49590218,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=184, Top=27, InputMask=replicate('X',35)

  add object oSGPCOD2_1_4 as StdField with uid="NXKKIIUGXH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SGPCOD2", cQueryName = "SGPCOD2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Al codice priorit�",;
    HelpContextID = 137485530,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=110, Top=50, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", cZoomOnZoom="GSOF_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_SGPCOD2"

  func oSGPCOD2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSGPCOD2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSGPCOD2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oSGPCOD2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AGP',"Gruppi priorit�",'',this.parent.oContained
  endproc
  proc oSGPCOD2_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_SGPCOD2
     i_obj.ecpSave()
  endproc

  add object oDESPRI2_1_5 as StdField with uid="GILWJPUWAI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESPRI2", cQueryName = "DESPRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 49590218,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=184, Top=50, InputMask=replicate('X',35)

  add object oNUMERO_1_6 as StdField with uid="BXBQYPGVTU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale priorit� � pi� grande del numero finale priorit�",;
    ToolTipText = "Numero iniziale priorit�",;
    HelpContextID = 218103594,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=110, Top=73, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
    endwith
    return bRes
  endfunc

  add object oNUMERO1_1_7 as StdField with uid="PROBAWFVAK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMERO1", cQueryName = "NUMERO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale priorit� � pi� grande del numero finale priorit�",;
    ToolTipText = "Numero finale priorit�",;
    HelpContextID = 218103594,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=110, Top=96, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO1_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO)))
    endwith
    return bRes
  endfunc

  add object oSGPCKOB_1_8 as StdCheck with uid="NUZMWBAOBT",rtseq=8,rtrep=.f.,left=111, top=119, caption="Solo obsoleti",;
    ToolTipText = "Flag per stampare solo gli obsoleti",;
    HelpContextID = 225565914,;
    cFormVar="w_SGPCKOB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSGPCKOB_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSGPCKOB_1_8.GetRadio()
    this.Parent.oContained.w_SGPCKOB = this.RadioValue()
    return .t.
  endfunc

  func oSGPCKOB_1_8.SetRadio()
    this.Parent.oContained.w_SGPCKOB=trim(this.Parent.oContained.w_SGPCKOB)
    this.value = ;
      iif(this.Parent.oContained.w_SGPCKOB=='S',1,;
      0)
  endfunc


  add object oBtn_1_12 as StdButton with uid="ICVWYYNYJJ",left=395, top=170, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 244721382;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="PACQXMQKUG",left=446, top=170, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 237885178;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_14 as cp_outputCombo with uid="ZLERXIVPWT",left=110, top=143, width=381,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 114172442

  add object oStr_1_9 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=10, Top=27,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="BRSOYYEROT",Visible=.t., Left=10, Top=50,;
    Alignment=1, Width=97, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="KHWVLZCVUG",Visible=.t., Left=10, Top=143,;
    Alignment=1, Width=97, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="KOTSVIFBOL",Visible=.t., Left=10, Top=6,;
    Alignment=1, Width=97, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="FBDSGEBOUN",Visible=.t., Left=10, Top=73,;
    Alignment=1, Width=97, Height=18,;
    Caption="Da priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="MRRDEGGOZA",Visible=.t., Left=10, Top=96,;
    Alignment=1, Width=97, Height=18,;
    Caption="A priorit�:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_sgp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
