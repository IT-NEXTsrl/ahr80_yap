* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bsk                                                        *
*              Check variazione sezioni fisse                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-14                                                      *
* Last revis.: 2005-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bsk",oParentObject)
return(i_retval)

define class tgsof_bsk as StdBatch
  * --- Local variables
  * --- WorkFile variables
  OFF_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla Variazione Sezioni Fisse (da GSOF_MSO)
    if this.oParentObject.w_OSEZVAR="S" AND this.oParentObject.o_CPROWORD<>0 AND this.oParentObject.w_CPROWORD<>this.oParentObject.o_CPROWORD
      ah_errormsg("Offerta associata a modello con sezioni a struttura rigida%0Impossibile inserire/modificare")
      this.oParentObject.w_CPROWORD = this.oParentObject.o_CPROWORD
    endif
    this.bUpdateParentObject=.f.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
