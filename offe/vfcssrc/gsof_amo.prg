* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_amo                                                        *
*              Modelli di offerta                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_256]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2015-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_amo"))

* --- Class definition
define class tgsof_amo as StdForm
  Top    = 4
  Left   = 9

  * --- Standard Properties
  Width  = 687
  Height = 434+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-08"
  HelpContextID=58054295
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=103

  * --- Constant Properties
  MOD_OFFE_IDX = 0
  TIP_DOCU_IDX = 0
  GRU_PRIO_IDX = 0
  ORI_NOMI_IDX = 0
  GRU_NOMI_IDX = 0
  LISTINI_IDX = 0
  LINGUE_IDX = 0
  ZONE_IDX = 0
  PORTI_IDX = 0
  MODASPED_IDX = 0
  PAG_AMEN_IDX = 0
  OUT_PUTS_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  PAR_OFFE_IDX = 0
  cFile = "MOD_OFFE"
  cKeySelect = "MOCODICE"
  cKeyWhere  = "MOCODICE=this.w_MOCODICE"
  cKeyWhereODBC = '"MOCODICE="+cp_ToStrODBC(this.w_MOCODICE)';

  cKeyWhereODBCqualified = '"MOD_OFFE.MOCODICE="+cp_ToStrODBC(this.w_MOCODICE)';

  cPrg = "gsof_amo"
  cComment = "Modelli di offerta"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TESTAD = space(1)
  w_MOCODICE = space(5)
  w_MODESCRI = space(35)
  w_MOSEZVAR = space(1)
  w_MOSERDOC = space(10)
  w_MONUMSCO = 0
  w_MOINISCA = space(2)
  o_MOINISCA = space(2)
  w_MOINISCA = space(2)
  w_MOGIOSCA = 0
  w_MOGIOFIS = 0
  w_MOCODCAU = space(5)
  w_MOCODCAU = space(5)
  w_DESCAU = space(35)
  w_RICNOM = space(1)
  w_MOFLPROV = space(1)
  w_MOCODVAL = space(3)
  o_MOCODVAL = space(3)
  w_MVCODVAL = space(3)
  w_SIMBOL = space(5)
  w_DECTOT = 0
  w_DECUNI = 0
  w_CALCPICT = 0
  w_MOCODLIS = space(5)
  w_DESLIS = space(35)
  w_MODATVAL = ctod('  /  /  ')
  w_MODATOBS = ctod('  /  /  ')
  w_MOCODUTE = 0
  w_MOGRPUTE = 0
  w_MOCODPRI = space(5)
  w_MOCODGRN = space(5)
  w_MOCODORI = space(5)
  w_MOCODZON = space(3)
  w_MOCODLIN = space(3)
  w_MOCODPOR = space(1)
  w_MOCODSPE = space(3)
  w_MOCODPAG = space(5)
  o_MOCODPAG = space(5)
  w_VALINC = space(3)
  w_VALIN2 = space(3)
  w_SPEINC = 0
  w_SPEIN2 = 0
  w_MOSCOCL1 = 0
  o_MOSCOCL1 = 0
  w_MOSCOCL2 = 0
  o_MOSCOCL2 = 0
  w_MOSCOPAG = 0
  w_MOSPEINC = 0
  w_MOSPEIMB = 0
  w_MOSPETRA = 0
  w_DESLIN = space(30)
  w_DESZON = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MODESGRU = space(35)
  w_MODESORI = space(35)
  w_MODESPRI = space(35)
  w_MODESOPE = space(35)
  w_MODESGRN = space(35)
  w_MOTIPOWP = space(1)
  o_MOTIPOWP = space(1)
  w_MOPATMOD = space(254)
  w_MONUMREP = 0
  w_MOPATARC = space(254)
  w_MONUMPER = space(1)
  w_MONOMDOC = space(254)
  w_MOTIPFOR = space(1)
  o_MOTIPFOR = space(1)
  w_DESPOR = space(35)
  w_DESSPE = space(35)
  w_DESPAG = space(30)
  w_DTOBSO = ctod('  /  /  ')
  w_PRGSTA = space(30)
  w_DESOUT = space(50)
  w_SERALL = space(10)
  w_NOMFIL = space(30)
  w_OGGETT = space(30)
  w_NOTE = space(30)
  w_PATALL = space(254)
  w_COD = space(5)
  w_DES1 = space(35)
  w_CODAZI = space(5)
  w_NUMSCO = 0
  w_BRES = .F.
  w_MVCLADOC = space(2)
  w_VALLIS = space(3)
  w_IVALIS = space(1)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_FLSCOR = space(1)
  w_CALCPICU = 0
  w_COD = space(5)
  w_DES1 = space(35)
  w_COD = space(5)
  w_DES1 = space(35)
  w_COD = space(5)
  w_DES1 = space(35)
  w_FLVEAC = space(1)
  w_FLINTE = space(1)
  w_CATDOC = space(2)
  w_FLANAL = space(1)
  w_FLCOMM = space(1)
  w_MOPRZVAC = space(1)
  w_FLORDAPE = space(1)
  w_MOALMAIL = space(1)
  w_MOALMAIL = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Children pointers
  GSOF_MSM = .NULL.
  GSOF_MAM = .NULL.
  w_ZoomAll = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=6, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_OFFE','gsof_amo')
    stdPageFrame::Init()
    *set procedure to GSOF_MSM additive
    with this
      .Pages(1).addobject("oPag","tgsof_amoPag1","gsof_amo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Documento")
      .Pages(1).HelpContextID = 4095590
      .Pages(2).addobject("oPag","tgsof_amoPag2","gsof_amo",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Filtri")
      .Pages(2).HelpContextID = 200211370
      .Pages(3).addobject("oPag","tgsof_amoPag3","gsof_amo",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati generali")
      .Pages(3).HelpContextID = 65602804
      .Pages(4).addobject("oPag","tgsof_amoPag4","gsof_amo",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Output")
      .Pages(4).HelpContextID = 12742426
      .Pages(5).addobject("oPag","tgsof_amoPag5","gsof_amo",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Allegati")
      .Pages(5).HelpContextID = 189925231
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMOCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSOF_MSM
    * --- Area Manuale = Init Page Frame
    * --- gsof_amo
    * --- Imposta il Titolo della Finestra
    WITH THIS.PARENT
       If Isalt()
       .cComment = CP_TRANSLATE('Modelli preventivo')
       Endif
    ENDWIT
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZoomAll = this.oPgFrm.Pages(5).oPag.ZoomAll
      DoDefault()
    proc Destroy()
      this.w_ZoomAll = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[17]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='GRU_PRIO'
    this.cWorkTables[3]='ORI_NOMI'
    this.cWorkTables[4]='GRU_NOMI'
    this.cWorkTables[5]='LISTINI'
    this.cWorkTables[6]='LINGUE'
    this.cWorkTables[7]='ZONE'
    this.cWorkTables[8]='PORTI'
    this.cWorkTables[9]='MODASPED'
    this.cWorkTables[10]='PAG_AMEN'
    this.cWorkTables[11]='OUT_PUTS'
    this.cWorkTables[12]='AZIENDA'
    this.cWorkTables[13]='VALUTE'
    this.cWorkTables[14]='CPUSERS'
    this.cWorkTables[15]='CPGROUPS'
    this.cWorkTables[16]='PAR_OFFE'
    this.cWorkTables[17]='MOD_OFFE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(17))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_OFFE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_OFFE_IDX,3]
  return

  function CreateChildren()
    this.GSOF_MSM = CREATEOBJECT('stdDynamicChild',this,'GSOF_MSM',this.oPgFrm.Page1.oPag.oLinkPC_1_24)
    this.GSOF_MSM.createrealchild()
    this.GSOF_MAM = CREATEOBJECT('stdDynamicChild',this,'GSOF_MAM',this.oPgFrm.Page3.oPag.oLinkPC_3_22)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSOF_MSM)
      this.GSOF_MSM.DestroyChildrenChain()
      this.GSOF_MSM=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_24')
    if !ISNULL(this.GSOF_MAM)
      this.GSOF_MAM.DestroyChildrenChain()
      this.GSOF_MAM=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_22')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSOF_MSM.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSOF_MAM.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSOF_MSM.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSOF_MAM.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSOF_MSM.NewDocument()
    this.GSOF_MAM.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSOF_MSM.SetKey(;
            .w_MOCODICE,"MSCODICE";
            )
      this.GSOF_MAM.SetKey(;
            .w_MOCODICE,"MACODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSOF_MSM.ChangeRow(this.cRowID+'      1',1;
             ,.w_MOCODICE,"MSCODICE";
             )
      .GSOF_MAM.ChangeRow(this.cRowID+'      1',1;
             ,.w_MOCODICE,"MACODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSOF_MSM)
        i_f=.GSOF_MSM.BuildFilter()
        if !(i_f==.GSOF_MSM.cQueryFilter)
          i_fnidx=.GSOF_MSM.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSOF_MSM.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSOF_MSM.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSOF_MSM.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSOF_MSM.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSOF_MAM)
        i_f=.GSOF_MAM.BuildFilter()
        if !(i_f==.GSOF_MAM.cQueryFilter)
          i_fnidx=.GSOF_MAM.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSOF_MAM.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSOF_MAM.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSOF_MAM.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSOF_MAM.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MOCODICE = NVL(MOCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_3_1_joined
    link_3_1_joined=.f.
    local link_3_3_joined
    link_3_3_joined=.f.
    local link_3_6_joined
    link_3_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_OFFE where MOCODICE=KeySet.MOCODICE
    *
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_OFFE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_OFFE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_OFFE '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_1_joined=this.AddJoinedLink_3_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_3_joined=this.AddJoinedLink_3_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_6_joined=this.AddJoinedLink_3_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCAU = space(35)
        .w_RICNOM = space(1)
        .w_SIMBOL = space(5)
        .w_DECTOT = 0
        .w_DECUNI = 0
        .w_DESLIS = space(35)
        .w_VALINC = space(3)
        .w_VALIN2 = space(3)
        .w_SPEINC = 0
        .w_SPEIN2 = 0
        .w_DESLIN = space(30)
        .w_DESZON = space(35)
        .w_DATOBSO = ctod("  /  /  ")
        .w_MODESGRU = space(35)
        .w_MODESORI = space(35)
        .w_MODESPRI = space(35)
        .w_MODESOPE = space(35)
        .w_MODESGRN = space(35)
        .w_DESPOR = space(35)
        .w_DESSPE = space(35)
        .w_DESPAG = space(30)
        .w_DTOBSO = ctod("  /  /  ")
        .w_PRGSTA = LEFT('GSOF_AOF' + SPACE(30),30)
        .w_DESOUT = space(50)
        .w_NOMFIL = space(30)
        .w_OGGETT = space(30)
        .w_NOTE = space(30)
        .w_NUMSCO = 0
        .w_BRES = .T.
        .w_VALLIS = space(3)
        .w_IVALIS = space(1)
        .w_INILIS = ctod("  /  /  ")
        .w_FINLIS = ctod("  /  /  ")
        .w_FLSCOR = 'N'
        .w_FLVEAC = space(1)
        .w_FLINTE = space(1)
        .w_CATDOC = space(2)
        .w_FLANAL = space(1)
        .w_FLCOMM = space(1)
        .w_FLORDAPE = space(1)
        .w_TESTAD = IIF(upper(g_APPLICATION) <> "ADHOC ENTERPRISE",'S','N')
        .w_MOCODICE = NVL(MOCODICE,space(5))
        .w_MODESCRI = NVL(MODESCRI,space(35))
        .w_MOSEZVAR = NVL(MOSEZVAR,space(1))
        .w_MOSERDOC = NVL(MOSERDOC,space(10))
        .w_MONUMSCO = NVL(MONUMSCO,0)
        .w_MOINISCA = NVL(MOINISCA,space(2))
        .w_MOINISCA = NVL(MOINISCA,space(2))
        .w_MOGIOSCA = NVL(MOGIOSCA,0)
        .w_MOGIOFIS = NVL(MOGIOFIS,0)
        .w_MOCODCAU = NVL(MOCODCAU,space(5))
          if link_1_11_joined
            this.w_MOCODCAU = NVL(TDTIPDOC111,NVL(this.w_MOCODCAU,space(5)))
            this.w_DESCAU = NVL(TDDESDOC111,space(35))
            this.w_CATDOC = NVL(TDCATDOC111,space(2))
            this.w_FLINTE = NVL(TDFLINTE111,space(1))
            this.w_FLVEAC = NVL(TDFLVEAC111,space(1))
            this.w_FLANAL = NVL(TDFLANAL111,space(1))
            this.w_FLCOMM = NVL(TDFLCOMM111,space(1))
            this.w_RICNOM = NVL(TDRICNOM111,space(1))
          else
          .link_1_11('Load')
          endif
        .w_MOCODCAU = NVL(MOCODCAU,space(5))
          if link_1_12_joined
            this.w_MOCODCAU = NVL(TDTIPDOC112,NVL(this.w_MOCODCAU,space(5)))
            this.w_DESCAU = NVL(TDDESDOC112,space(35))
            this.w_CATDOC = NVL(TDCATDOC112,space(2))
            this.w_FLINTE = NVL(TDFLINTE112,space(1))
            this.w_FLVEAC = NVL(TDFLVEAC112,space(1))
            this.w_FLANAL = NVL(TDFLANAL112,space(1))
            this.w_FLCOMM = NVL(TDFLCOMM112,space(1))
            this.w_FLORDAPE = NVL(TDORDAPE112,space(1))
            this.w_RICNOM = NVL(TDRICNOM112,space(1))
          else
          .link_1_12('Load')
          endif
        .w_MOFLPROV = NVL(MOFLPROV,space(1))
        .w_MOCODVAL = NVL(MOCODVAL,space(3))
          if link_1_16_joined
            this.w_MOCODVAL = NVL(VACODVAL116,NVL(this.w_MOCODVAL,space(3)))
            this.w_DATOBSO = NVL(cp_ToDate(VADTOBSO116),ctod("  /  /  "))
            this.w_SIMBOL = NVL(VASIMVAL116,space(5))
            this.w_DECTOT = NVL(VADECTOT116,0)
            this.w_DECUNI = NVL(VADECUNI116,0)
          else
          .link_1_16('Load')
          endif
        .w_MVCODVAL = .w_MOCODVAL
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_MOCODLIS = NVL(MOCODLIS,space(5))
          if link_1_22_joined
            this.w_MOCODLIS = NVL(LSCODLIS122,NVL(this.w_MOCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS122,space(35))
            this.w_VALLIS = NVL(LSVALLIS122,space(3))
            this.w_INILIS = NVL(cp_ToDate(LSDTINVA122),ctod("  /  /  "))
            this.w_FINLIS = NVL(cp_ToDate(LSDTOBSO122),ctod("  /  /  "))
            this.w_IVALIS = NVL(LSIVALIS122,space(1))
          else
          .link_1_22('Load')
          endif
        .w_MODATVAL = NVL(cp_ToDate(MODATVAL),ctod("  /  /  "))
        .w_MODATOBS = NVL(cp_ToDate(MODATOBS),ctod("  /  /  "))
        .w_MOCODUTE = NVL(MOCODUTE,0)
          if link_2_1_joined
            this.w_MOCODUTE = NVL(CODE201,NVL(this.w_MOCODUTE,0))
            this.w_MODESOPE = NVL(NAME201,space(35))
          else
          .link_2_1('Load')
          endif
        .w_MOGRPUTE = NVL(MOGRPUTE,0)
          if link_2_2_joined
            this.w_MOGRPUTE = NVL(CODE202,NVL(this.w_MOGRPUTE,0))
            this.w_MODESGRN = NVL(NAME202,space(35))
          else
          .link_2_2('Load')
          endif
        .w_MOCODPRI = NVL(MOCODPRI,space(5))
          if link_2_3_joined
            this.w_MOCODPRI = NVL(GPCODICE203,NVL(this.w_MOCODPRI,space(5)))
            this.w_MODESPRI = NVL(GPDESCRI203,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(GPDTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
        .w_MOCODGRN = NVL(MOCODGRN,space(5))
          if link_2_4_joined
            this.w_MOCODGRN = NVL(GNCODICE204,NVL(this.w_MOCODGRN,space(5)))
            this.w_MODESGRU = NVL(GNDESCRI204,space(35))
          else
          .link_2_4('Load')
          endif
        .w_MOCODORI = NVL(MOCODORI,space(5))
          if link_2_5_joined
            this.w_MOCODORI = NVL(ONCODICE205,NVL(this.w_MOCODORI,space(5)))
            this.w_MODESORI = NVL(ONDESCRI205,space(35))
          else
          .link_2_5('Load')
          endif
        .w_MOCODZON = NVL(MOCODZON,space(3))
          if link_2_6_joined
            this.w_MOCODZON = NVL(ZOCODZON206,NVL(this.w_MOCODZON,space(3)))
            this.w_DESZON = NVL(ZODESZON206,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(ZODTOBSO206),ctod("  /  /  "))
          else
          .link_2_6('Load')
          endif
        .w_MOCODLIN = NVL(MOCODLIN,space(3))
          if link_2_7_joined
            this.w_MOCODLIN = NVL(LUCODICE207,NVL(this.w_MOCODLIN,space(3)))
            this.w_DESLIN = NVL(LUDESCRI207,space(30))
          else
          .link_2_7('Load')
          endif
        .w_MOCODPOR = NVL(MOCODPOR,space(1))
          if link_3_1_joined
            this.w_MOCODPOR = NVL(POCODPOR301,NVL(this.w_MOCODPOR,space(1)))
            this.w_DESPOR = NVL(PODESPOR301,space(35))
          else
          .link_3_1('Load')
          endif
        .w_MOCODSPE = NVL(MOCODSPE,space(3))
          if link_3_3_joined
            this.w_MOCODSPE = NVL(SPCODSPE303,NVL(this.w_MOCODSPE,space(3)))
            this.w_DESSPE = NVL(SPDESSPE303,space(35))
          else
          .link_3_3('Load')
          endif
        .w_MOCODPAG = NVL(MOCODPAG,space(5))
          if link_3_6_joined
            this.w_MOCODPAG = NVL(PACODICE306,NVL(this.w_MOCODPAG,space(5)))
            this.w_DTOBSO = NVL(cp_ToDate(PADTOBSO306),ctod("  /  /  "))
            this.w_DESPAG = NVL(PADESCRI306,space(30))
            this.w_MOSCOPAG = NVL(PASCONTO306,0)
            this.w_VALINC = NVL(PAVALINC306,space(3))
            this.w_SPEINC = NVL(PASPEINC306,0)
            this.w_VALIN2 = NVL(PAVALIN2306,space(3))
            this.w_SPEIN2 = NVL(PASPEIN2306,0)
          else
          .link_3_6('Load')
          endif
        .w_MOSCOCL1 = NVL(MOSCOCL1,0)
        .w_MOSCOCL2 = NVL(MOSCOCL2,0)
        .w_MOSCOPAG = NVL(MOSCOPAG,0)
        .w_MOSPEINC = NVL(MOSPEINC,0)
        .w_MOSPEIMB = NVL(MOSPEIMB,0)
        .w_MOSPETRA = NVL(MOSPETRA,0)
        .w_OBTEST = i_datsys
        .w_MOTIPOWP = NVL(MOTIPOWP,space(1))
        .w_MOPATMOD = NVL(MOPATMOD,space(254))
        .w_MONUMREP = NVL(MONUMREP,0)
          .link_4_6('Load')
        .w_MOPATARC = NVL(MOPATARC,space(254))
        .w_MONUMPER = NVL(MONUMPER,space(1))
        .w_MONOMDOC = NVL(MONOMDOC,space(254))
        .w_MOTIPFOR = NVL(MOTIPFOR,space(1))
        .oPgFrm.Page5.oPag.ZoomAll.Calculate()
        .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
        .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
        .w_COD = .w_MOCODICE
        .w_DES1 = .w_MODESCRI
        .w_CODAZI = i_codazi
          .link_1_39('Load')
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .w_MVCLADOC = '  '
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_COD = .w_MOCODICE
        .w_DES1 = .w_MODESCRI
        .w_COD = .w_MOCODICE
        .w_DES1 = .w_MODESCRI
        .w_COD = .w_MOCODICE
        .w_DES1 = .w_MODESCRI
        .w_MOPRZVAC = NVL(MOPRZVAC,space(1))
        .w_MOALMAIL = NVL(MOALMAIL,space(1))
        .w_MOALMAIL = NVL(MOALMAIL,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'MOD_OFFE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TESTAD = space(1)
      .w_MOCODICE = space(5)
      .w_MODESCRI = space(35)
      .w_MOSEZVAR = space(1)
      .w_MOSERDOC = space(10)
      .w_MONUMSCO = 0
      .w_MOINISCA = space(2)
      .w_MOINISCA = space(2)
      .w_MOGIOSCA = 0
      .w_MOGIOFIS = 0
      .w_MOCODCAU = space(5)
      .w_MOCODCAU = space(5)
      .w_DESCAU = space(35)
      .w_RICNOM = space(1)
      .w_MOFLPROV = space(1)
      .w_MOCODVAL = space(3)
      .w_MVCODVAL = space(3)
      .w_SIMBOL = space(5)
      .w_DECTOT = 0
      .w_DECUNI = 0
      .w_CALCPICT = 0
      .w_MOCODLIS = space(5)
      .w_DESLIS = space(35)
      .w_MODATVAL = ctod("  /  /  ")
      .w_MODATOBS = ctod("  /  /  ")
      .w_MOCODUTE = 0
      .w_MOGRPUTE = 0
      .w_MOCODPRI = space(5)
      .w_MOCODGRN = space(5)
      .w_MOCODORI = space(5)
      .w_MOCODZON = space(3)
      .w_MOCODLIN = space(3)
      .w_MOCODPOR = space(1)
      .w_MOCODSPE = space(3)
      .w_MOCODPAG = space(5)
      .w_VALINC = space(3)
      .w_VALIN2 = space(3)
      .w_SPEINC = 0
      .w_SPEIN2 = 0
      .w_MOSCOCL1 = 0
      .w_MOSCOCL2 = 0
      .w_MOSCOPAG = 0
      .w_MOSPEINC = 0
      .w_MOSPEIMB = 0
      .w_MOSPETRA = 0
      .w_DESLIN = space(30)
      .w_DESZON = space(35)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_MODESGRU = space(35)
      .w_MODESORI = space(35)
      .w_MODESPRI = space(35)
      .w_MODESOPE = space(35)
      .w_MODESGRN = space(35)
      .w_MOTIPOWP = space(1)
      .w_MOPATMOD = space(254)
      .w_MONUMREP = 0
      .w_MOPATARC = space(254)
      .w_MONUMPER = space(1)
      .w_MONOMDOC = space(254)
      .w_MOTIPFOR = space(1)
      .w_DESPOR = space(35)
      .w_DESSPE = space(35)
      .w_DESPAG = space(30)
      .w_DTOBSO = ctod("  /  /  ")
      .w_PRGSTA = space(30)
      .w_DESOUT = space(50)
      .w_SERALL = space(10)
      .w_NOMFIL = space(30)
      .w_OGGETT = space(30)
      .w_NOTE = space(30)
      .w_PATALL = space(254)
      .w_COD = space(5)
      .w_DES1 = space(35)
      .w_CODAZI = space(5)
      .w_NUMSCO = 0
      .w_BRES = .f.
      .w_MVCLADOC = space(2)
      .w_VALLIS = space(3)
      .w_IVALIS = space(1)
      .w_INILIS = ctod("  /  /  ")
      .w_FINLIS = ctod("  /  /  ")
      .w_FLSCOR = space(1)
      .w_CALCPICU = 0
      .w_COD = space(5)
      .w_DES1 = space(35)
      .w_COD = space(5)
      .w_DES1 = space(35)
      .w_COD = space(5)
      .w_DES1 = space(35)
      .w_FLVEAC = space(1)
      .w_FLINTE = space(1)
      .w_CATDOC = space(2)
      .w_FLANAL = space(1)
      .w_FLCOMM = space(1)
      .w_MOPRZVAC = space(1)
      .w_FLORDAPE = space(1)
      .w_MOALMAIL = space(1)
      .w_MOALMAIL = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_TESTAD = IIF(upper(g_APPLICATION) <> "ADHOC ENTERPRISE",'S','N')
          .DoRTCalc(2,3,.f.)
        .w_MOSEZVAR = 'N'
          .DoRTCalc(5,6,.f.)
        .w_MOINISCA = 'DF'
        .w_MOINISCA = 'DF'
          .DoRTCalc(9,9,.f.)
        .w_MOGIOFIS = 0
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_MOCODCAU))
          .link_1_11('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_MOCODCAU))
          .link_1_12('Full')
          endif
          .DoRTCalc(13,14,.f.)
        .w_MOFLPROV = 'S'
        .w_MOCODVAL = g_PERVAL
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_MOCODVAL))
          .link_1_16('Full')
          endif
        .w_MVCODVAL = .w_MOCODVAL
          .DoRTCalc(18,20,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_MOCODLIS))
          .link_1_22('Full')
          endif
        .DoRTCalc(23,26,.f.)
          if not(empty(.w_MOCODUTE))
          .link_2_1('Full')
          endif
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_MOGRPUTE))
          .link_2_2('Full')
          endif
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_MOCODPRI))
          .link_2_3('Full')
          endif
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_MOCODGRN))
          .link_2_4('Full')
          endif
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_MOCODORI))
          .link_2_5('Full')
          endif
        .DoRTCalc(31,31,.f.)
          if not(empty(.w_MOCODZON))
          .link_2_6('Full')
          endif
        .w_MOCODLIN = g_CODLIN
        .DoRTCalc(32,32,.f.)
          if not(empty(.w_MOCODLIN))
          .link_2_7('Full')
          endif
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_MOCODPOR))
          .link_3_1('Full')
          endif
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_MOCODSPE))
          .link_3_3('Full')
          endif
        .DoRTCalc(35,35,.f.)
          if not(empty(.w_MOCODPAG))
          .link_3_6('Full')
          endif
          .DoRTCalc(36,40,.f.)
        .w_MOSCOCL2 = IIF(.w_MOSCOCL1=0, 0, .w_MOSCOCL2)
          .DoRTCalc(42,42,.f.)
        .w_MOSPEINC = IIF(.w_MOCODVAL=.w_VALINC, .w_SPEINC, IIF(.w_MOCODVAL=.w_VALIN2, .w_SPEIN2, 0))
          .DoRTCalc(44,47,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(49,54,.f.)
        .w_MOTIPOWP = 'W'
        .DoRTCalc(56,57,.f.)
          if not(empty(.w_MONUMREP))
          .link_4_6('Full')
          endif
          .DoRTCalc(58,58,.f.)
        .w_MONUMPER = 'N'
          .DoRTCalc(60,60,.f.)
        .w_MOTIPFOR = 'W'
          .DoRTCalc(62,65,.f.)
        .w_PRGSTA = LEFT('GSOF_AOF' + SPACE(30),30)
        .oPgFrm.Page5.oPag.ZoomAll.Calculate()
          .DoRTCalc(67,67,.f.)
        .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
          .DoRTCalc(69,71,.f.)
        .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
        .w_COD = .w_MOCODICE
        .w_DES1 = .w_MODESCRI
        .w_CODAZI = i_codazi
        .DoRTCalc(75,75,.f.)
          if not(empty(.w_CODAZI))
          .link_1_39('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
          .DoRTCalc(76,76,.f.)
        .w_BRES = .T.
        .w_MVCLADOC = '  '
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
          .DoRTCalc(79,82,.f.)
        .w_FLSCOR = 'N'
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_COD = .w_MOCODICE
        .w_DES1 = .w_MODESCRI
        .w_COD = .w_MOCODICE
        .w_DES1 = .w_MODESCRI
        .w_COD = .w_MOCODICE
        .w_DES1 = .w_MODESCRI
          .DoRTCalc(91,95,.f.)
        .w_MOPRZVAC = ' '
          .DoRTCalc(97,97,.f.)
        .w_MOALMAIL = 'S'
        .w_MOALMAIL = 'S'
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_OFFE')
    this.DoRTCalc(100,103,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsof_amo
    * --- Refresh dello Zoom
    if this.cFunction='Load'
       this.NotifyEvent('Calcola')
       endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMOCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oMODESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oMOSEZVAR_1_4.enabled = i_bVal
      .Page1.oPag.oMOSERDOC_1_5.enabled = i_bVal
      .Page1.oPag.oMONUMSCO_1_6.enabled = i_bVal
      .Page1.oPag.oMOINISCA_1_7.enabled = i_bVal
      .Page1.oPag.oMOINISCA_1_8.enabled = i_bVal
      .Page1.oPag.oMOGIOSCA_1_9.enabled = i_bVal
      .Page1.oPag.oMOGIOFIS_1_10.enabled = i_bVal
      .Page1.oPag.oMOCODCAU_1_11.enabled = i_bVal
      .Page1.oPag.oMOCODCAU_1_12.enabled = i_bVal
      .Page1.oPag.oMOFLPROV_1_15.enabled = i_bVal
      .Page1.oPag.oMOCODVAL_1_16.enabled = i_bVal
      .Page1.oPag.oMOCODLIS_1_22.enabled = i_bVal
      .Page1.oPag.oMODATVAL_1_25.enabled = i_bVal
      .Page1.oPag.oMODATOBS_1_27.enabled = i_bVal
      .Page2.oPag.oMOCODUTE_2_1.enabled = i_bVal
      .Page2.oPag.oMOGRPUTE_2_2.enabled = i_bVal
      .Page2.oPag.oMOCODPRI_2_3.enabled = i_bVal
      .Page2.oPag.oMOCODGRN_2_4.enabled = i_bVal
      .Page2.oPag.oMOCODORI_2_5.enabled = i_bVal
      .Page2.oPag.oMOCODZON_2_6.enabled = i_bVal
      .Page2.oPag.oMOCODLIN_2_7.enabled = i_bVal
      .Page3.oPag.oMOCODPOR_3_1.enabled = i_bVal
      .Page3.oPag.oMOCODSPE_3_3.enabled = i_bVal
      .Page3.oPag.oMOCODPAG_3_6.enabled = i_bVal
      .Page3.oPag.oMOSCOCL1_3_12.enabled = i_bVal
      .Page3.oPag.oMOSCOCL2_3_14.enabled = i_bVal
      .Page3.oPag.oMOSCOPAG_3_15.enabled = i_bVal
      .Page3.oPag.oMOSPEIMB_3_19.enabled = i_bVal
      .Page3.oPag.oMOSPETRA_3_21.enabled = i_bVal
      .Page4.oPag.oMOTIPOWP_4_1.enabled = i_bVal
      .Page4.oPag.oMOPATMOD_4_3.enabled = i_bVal
      .Page4.oPag.oMONUMREP_4_6.enabled = i_bVal
      .Page4.oPag.oMOPATARC_4_8.enabled = i_bVal
      .Page4.oPag.oMONUMPER_4_10.enabled = i_bVal
      .Page4.oPag.oMONOMDOC_4_12.enabled = i_bVal
      .Page4.oPag.oMOTIPFOR_4_13.enabled = i_bVal
      .Page5.oPag.oNOMFIL_5_11.enabled = i_bVal
      .Page5.oPag.oOGGETT_5_12.enabled = i_bVal
      .Page5.oPag.oNOTE_5_13.enabled = i_bVal
      .Page1.oPag.oMOPRZVAC_1_57.enabled = i_bVal
      .Page4.oPag.oMOALMAIL_4_28.enabled = i_bVal
      .Page4.oPag.oBtn_4_4.enabled = i_bVal
      .Page4.oPag.oBtn_4_9.enabled = i_bVal
      .Page5.oPag.oBtn_5_1.enabled = i_bVal
      .Page5.oPag.oBtn_5_3.enabled = i_bVal
      .Page5.oPag.oBtn_5_4.enabled = i_bVal
      .Page5.oPag.oBtn_5_5.enabled = i_bVal
      .Page5.oPag.oBtn_5_6.enabled = i_bVal
      .Page1.oPag.oObj_1_42.enabled = i_bVal
      .Page1.oPag.oObj_1_46.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMOCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMOCODICE_1_2.enabled = .t.
        .Page1.oPag.oMODESCRI_1_3.enabled = .t.
      endif
    endwith
    this.GSOF_MSM.SetStatus(i_cOp)
    this.GSOF_MAM.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MOD_OFFE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSOF_MSM.SetChildrenStatus(i_cOp)
  *  this.GSOF_MAM.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODICE,"MOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODESCRI,"MODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOSEZVAR,"MOSEZVAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOSERDOC,"MOSERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MONUMSCO,"MONUMSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOINISCA,"MOINISCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOINISCA,"MOINISCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOGIOSCA,"MOGIOSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOGIOFIS,"MOGIOFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODCAU,"MOCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODCAU,"MOCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOFLPROV,"MOFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODVAL,"MOCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODLIS,"MOCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODATVAL,"MODATVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODATOBS,"MODATOBS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODUTE,"MOCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOGRPUTE,"MOGRPUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODPRI,"MOCODPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODGRN,"MOCODGRN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODORI,"MOCODORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODZON,"MOCODZON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODLIN,"MOCODLIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODPOR,"MOCODPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODSPE,"MOCODSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODPAG,"MOCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOSCOCL1,"MOSCOCL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOSCOCL2,"MOSCOCL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOSCOPAG,"MOSCOPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOSPEINC,"MOSPEINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOSPEIMB,"MOSPEIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOSPETRA,"MOSPETRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOTIPOWP,"MOTIPOWP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPATMOD,"MOPATMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MONUMREP,"MONUMREP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPATARC,"MOPATARC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MONUMPER,"MONUMPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MONOMDOC,"MONOMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOTIPFOR,"MOTIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPRZVAC,"MOPRZVAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOALMAIL,"MOALMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOALMAIL,"MOALMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    i_lTable = "MOD_OFFE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_OFFE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsof_smo with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_OFFE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_OFFE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_OFFE')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_OFFE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MOCODICE,MODESCRI,MOSEZVAR,MOSERDOC,MONUMSCO"+;
                  ",MOINISCA,MOGIOSCA,MOGIOFIS,MOCODCAU,MOFLPROV"+;
                  ",MOCODVAL,MOCODLIS,MODATVAL,MODATOBS,MOCODUTE"+;
                  ",MOGRPUTE,MOCODPRI,MOCODGRN,MOCODORI,MOCODZON"+;
                  ",MOCODLIN,MOCODPOR,MOCODSPE,MOCODPAG,MOSCOCL1"+;
                  ",MOSCOCL2,MOSCOPAG,MOSPEINC,MOSPEIMB,MOSPETRA"+;
                  ",MOTIPOWP,MOPATMOD,MONUMREP,MOPATARC,MONUMPER"+;
                  ",MONOMDOC,MOTIPFOR,MOPRZVAC,MOALMAIL,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MOCODICE)+;
                  ","+cp_ToStrODBC(this.w_MODESCRI)+;
                  ","+cp_ToStrODBC(this.w_MOSEZVAR)+;
                  ","+cp_ToStrODBC(this.w_MOSERDOC)+;
                  ","+cp_ToStrODBC(this.w_MONUMSCO)+;
                  ","+cp_ToStrODBC(this.w_MOINISCA)+;
                  ","+cp_ToStrODBC(this.w_MOGIOSCA)+;
                  ","+cp_ToStrODBC(this.w_MOGIOFIS)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODCAU)+;
                  ","+cp_ToStrODBC(this.w_MOFLPROV)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODVAL)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODLIS)+;
                  ","+cp_ToStrODBC(this.w_MODATVAL)+;
                  ","+cp_ToStrODBC(this.w_MODATOBS)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODUTE)+;
                  ","+cp_ToStrODBCNull(this.w_MOGRPUTE)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODPRI)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODGRN)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODORI)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODZON)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODLIN)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODPOR)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODSPE)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODPAG)+;
                  ","+cp_ToStrODBC(this.w_MOSCOCL1)+;
                  ","+cp_ToStrODBC(this.w_MOSCOCL2)+;
                  ","+cp_ToStrODBC(this.w_MOSCOPAG)+;
                  ","+cp_ToStrODBC(this.w_MOSPEINC)+;
                  ","+cp_ToStrODBC(this.w_MOSPEIMB)+;
                  ","+cp_ToStrODBC(this.w_MOSPETRA)+;
                  ","+cp_ToStrODBC(this.w_MOTIPOWP)+;
                  ","+cp_ToStrODBC(this.w_MOPATMOD)+;
                  ","+cp_ToStrODBCNull(this.w_MONUMREP)+;
                  ","+cp_ToStrODBC(this.w_MOPATARC)+;
                  ","+cp_ToStrODBC(this.w_MONUMPER)+;
                  ","+cp_ToStrODBC(this.w_MONOMDOC)+;
                  ","+cp_ToStrODBC(this.w_MOTIPFOR)+;
                  ","+cp_ToStrODBC(this.w_MOPRZVAC)+;
                  ","+cp_ToStrODBC(this.w_MOALMAIL)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_OFFE')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_OFFE')
        cp_CheckDeletedKey(i_cTable,0,'MOCODICE',this.w_MOCODICE)
        INSERT INTO (i_cTable);
              (MOCODICE,MODESCRI,MOSEZVAR,MOSERDOC,MONUMSCO,MOINISCA,MOGIOSCA,MOGIOFIS,MOCODCAU,MOFLPROV,MOCODVAL,MOCODLIS,MODATVAL,MODATOBS,MOCODUTE,MOGRPUTE,MOCODPRI,MOCODGRN,MOCODORI,MOCODZON,MOCODLIN,MOCODPOR,MOCODSPE,MOCODPAG,MOSCOCL1,MOSCOCL2,MOSCOPAG,MOSPEINC,MOSPEIMB,MOSPETRA,MOTIPOWP,MOPATMOD,MONUMREP,MOPATARC,MONUMPER,MONOMDOC,MOTIPFOR,MOPRZVAC,MOALMAIL,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MOCODICE;
                  ,this.w_MODESCRI;
                  ,this.w_MOSEZVAR;
                  ,this.w_MOSERDOC;
                  ,this.w_MONUMSCO;
                  ,this.w_MOINISCA;
                  ,this.w_MOGIOSCA;
                  ,this.w_MOGIOFIS;
                  ,this.w_MOCODCAU;
                  ,this.w_MOFLPROV;
                  ,this.w_MOCODVAL;
                  ,this.w_MOCODLIS;
                  ,this.w_MODATVAL;
                  ,this.w_MODATOBS;
                  ,this.w_MOCODUTE;
                  ,this.w_MOGRPUTE;
                  ,this.w_MOCODPRI;
                  ,this.w_MOCODGRN;
                  ,this.w_MOCODORI;
                  ,this.w_MOCODZON;
                  ,this.w_MOCODLIN;
                  ,this.w_MOCODPOR;
                  ,this.w_MOCODSPE;
                  ,this.w_MOCODPAG;
                  ,this.w_MOSCOCL1;
                  ,this.w_MOSCOCL2;
                  ,this.w_MOSCOPAG;
                  ,this.w_MOSPEINC;
                  ,this.w_MOSPEIMB;
                  ,this.w_MOSPETRA;
                  ,this.w_MOTIPOWP;
                  ,this.w_MOPATMOD;
                  ,this.w_MONUMREP;
                  ,this.w_MOPATARC;
                  ,this.w_MONUMPER;
                  ,this.w_MONOMDOC;
                  ,this.w_MOTIPFOR;
                  ,this.w_MOPRZVAC;
                  ,this.w_MOALMAIL;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_OFFE_IDX,i_nConn)
      *
      * update MOD_OFFE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_OFFE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MODESCRI="+cp_ToStrODBC(this.w_MODESCRI)+;
             ",MOSEZVAR="+cp_ToStrODBC(this.w_MOSEZVAR)+;
             ",MOSERDOC="+cp_ToStrODBC(this.w_MOSERDOC)+;
             ",MONUMSCO="+cp_ToStrODBC(this.w_MONUMSCO)+;
             ",MOINISCA="+cp_ToStrODBC(this.w_MOINISCA)+;
             ",MOGIOSCA="+cp_ToStrODBC(this.w_MOGIOSCA)+;
             ",MOGIOFIS="+cp_ToStrODBC(this.w_MOGIOFIS)+;
             ",MOCODCAU="+cp_ToStrODBCNull(this.w_MOCODCAU)+;
             ",MOFLPROV="+cp_ToStrODBC(this.w_MOFLPROV)+;
             ",MOCODVAL="+cp_ToStrODBCNull(this.w_MOCODVAL)+;
             ",MOCODLIS="+cp_ToStrODBCNull(this.w_MOCODLIS)+;
             ",MODATVAL="+cp_ToStrODBC(this.w_MODATVAL)+;
             ",MODATOBS="+cp_ToStrODBC(this.w_MODATOBS)+;
             ",MOCODUTE="+cp_ToStrODBCNull(this.w_MOCODUTE)+;
             ",MOGRPUTE="+cp_ToStrODBCNull(this.w_MOGRPUTE)+;
             ",MOCODPRI="+cp_ToStrODBCNull(this.w_MOCODPRI)+;
             ",MOCODGRN="+cp_ToStrODBCNull(this.w_MOCODGRN)+;
             ",MOCODORI="+cp_ToStrODBCNull(this.w_MOCODORI)+;
             ",MOCODZON="+cp_ToStrODBCNull(this.w_MOCODZON)+;
             ",MOCODLIN="+cp_ToStrODBCNull(this.w_MOCODLIN)+;
             ",MOCODPOR="+cp_ToStrODBCNull(this.w_MOCODPOR)+;
             ",MOCODSPE="+cp_ToStrODBCNull(this.w_MOCODSPE)+;
             ",MOCODPAG="+cp_ToStrODBCNull(this.w_MOCODPAG)+;
             ",MOSCOCL1="+cp_ToStrODBC(this.w_MOSCOCL1)+;
             ",MOSCOCL2="+cp_ToStrODBC(this.w_MOSCOCL2)+;
             ",MOSCOPAG="+cp_ToStrODBC(this.w_MOSCOPAG)+;
             ",MOSPEINC="+cp_ToStrODBC(this.w_MOSPEINC)+;
             ",MOSPEIMB="+cp_ToStrODBC(this.w_MOSPEIMB)+;
             ",MOSPETRA="+cp_ToStrODBC(this.w_MOSPETRA)+;
             ",MOTIPOWP="+cp_ToStrODBC(this.w_MOTIPOWP)+;
             ",MOPATMOD="+cp_ToStrODBC(this.w_MOPATMOD)+;
             ",MONUMREP="+cp_ToStrODBCNull(this.w_MONUMREP)+;
             ",MOPATARC="+cp_ToStrODBC(this.w_MOPATARC)+;
             ",MONUMPER="+cp_ToStrODBC(this.w_MONUMPER)+;
             ",MONOMDOC="+cp_ToStrODBC(this.w_MONOMDOC)+;
             ",MOTIPFOR="+cp_ToStrODBC(this.w_MOTIPFOR)+;
             ",MOPRZVAC="+cp_ToStrODBC(this.w_MOPRZVAC)+;
             ",MOALMAIL="+cp_ToStrODBC(this.w_MOALMAIL)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_OFFE')
        i_cWhere = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
        UPDATE (i_cTable) SET;
              MODESCRI=this.w_MODESCRI;
             ,MOSEZVAR=this.w_MOSEZVAR;
             ,MOSERDOC=this.w_MOSERDOC;
             ,MONUMSCO=this.w_MONUMSCO;
             ,MOINISCA=this.w_MOINISCA;
             ,MOGIOSCA=this.w_MOGIOSCA;
             ,MOGIOFIS=this.w_MOGIOFIS;
             ,MOCODCAU=this.w_MOCODCAU;
             ,MOFLPROV=this.w_MOFLPROV;
             ,MOCODVAL=this.w_MOCODVAL;
             ,MOCODLIS=this.w_MOCODLIS;
             ,MODATVAL=this.w_MODATVAL;
             ,MODATOBS=this.w_MODATOBS;
             ,MOCODUTE=this.w_MOCODUTE;
             ,MOGRPUTE=this.w_MOGRPUTE;
             ,MOCODPRI=this.w_MOCODPRI;
             ,MOCODGRN=this.w_MOCODGRN;
             ,MOCODORI=this.w_MOCODORI;
             ,MOCODZON=this.w_MOCODZON;
             ,MOCODLIN=this.w_MOCODLIN;
             ,MOCODPOR=this.w_MOCODPOR;
             ,MOCODSPE=this.w_MOCODSPE;
             ,MOCODPAG=this.w_MOCODPAG;
             ,MOSCOCL1=this.w_MOSCOCL1;
             ,MOSCOCL2=this.w_MOSCOCL2;
             ,MOSCOPAG=this.w_MOSCOPAG;
             ,MOSPEINC=this.w_MOSPEINC;
             ,MOSPEIMB=this.w_MOSPEIMB;
             ,MOSPETRA=this.w_MOSPETRA;
             ,MOTIPOWP=this.w_MOTIPOWP;
             ,MOPATMOD=this.w_MOPATMOD;
             ,MONUMREP=this.w_MONUMREP;
             ,MOPATARC=this.w_MOPATARC;
             ,MONUMPER=this.w_MONUMPER;
             ,MONOMDOC=this.w_MONOMDOC;
             ,MOTIPFOR=this.w_MOTIPFOR;
             ,MOPRZVAC=this.w_MOPRZVAC;
             ,MOALMAIL=this.w_MOALMAIL;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSOF_MSM : Saving
      this.GSOF_MSM.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MOCODICE,"MSCODICE";
             )
      this.GSOF_MSM.mReplace()
      * --- GSOF_MAM : Saving
      this.GSOF_MAM.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MOCODICE,"MACODICE";
             )
      this.GSOF_MAM.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSOF_MSM : Deleting
    this.GSOF_MSM.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MOCODICE,"MSCODICE";
           )
    this.GSOF_MSM.mDelete()
    * --- GSOF_MAM : Deleting
    this.GSOF_MAM.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MOCODICE,"MACODICE";
           )
    this.GSOF_MAM.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_OFFE_IDX,i_nConn)
      *
      * delete MOD_OFFE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    if i_bUpd
      with this
            .w_TESTAD = IIF(upper(g_APPLICATION) <> "ADHOC ENTERPRISE",'S','N')
        .DoRTCalc(2,9,.t.)
        if .o_MOINISCA<>.w_MOINISCA
            .w_MOGIOFIS = 0
        endif
        .DoRTCalc(11,16,.t.)
        if .o_MOCODVAL<>.w_MOCODVAL
            .w_MVCODVAL = .w_MOCODVAL
        endif
        .DoRTCalc(18,20,.t.)
        if .o_MOCODVAL<>.w_MOCODVAL
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_MOCODVAL<>.w_MOCODVAL
          .link_1_22('Full')
        endif
        .DoRTCalc(23,40,.t.)
        if .o_MOSCOCL1<>.w_MOSCOCL1
            .w_MOSCOCL2 = IIF(.w_MOSCOCL1=0, 0, .w_MOSCOCL2)
        endif
        .DoRTCalc(42,42,.t.)
        if .o_MOCODVAL<>.w_MOCODVAL.or. .o_MOCODPAG<>.w_MOCODPAG
            .w_MOSPEINC = IIF(.w_MOCODVAL=.w_VALINC, .w_SPEINC, IIF(.w_MOCODVAL=.w_VALIN2, .w_SPEIN2, 0))
        endif
        .DoRTCalc(44,47,.t.)
            .w_OBTEST = i_datsys
        .oPgFrm.Page5.oPag.ZoomAll.Calculate()
        .DoRTCalc(49,67,.t.)
            .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
        .DoRTCalc(69,71,.t.)
            .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
            .w_COD = .w_MOCODICE
            .w_DES1 = .w_MODESCRI
            .w_CODAZI = i_codazi
          .link_1_39('Full')
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .DoRTCalc(76,77,.t.)
            .w_MVCLADOC = '  '
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .DoRTCalc(79,83,.t.)
        if .o_MOCODVAL<>.w_MOCODVAL
            .w_CALCPICU = DEFPIU(.w_DECUNI)
        endif
            .w_COD = .w_MOCODICE
            .w_DES1 = .w_MODESCRI
            .w_COD = .w_MOCODICE
            .w_DES1 = .w_MODESCRI
            .w_COD = .w_MOCODICE
            .w_DES1 = .w_MODESCRI
        .DoRTCalc(91,97,.t.)
        if .o_MOTIPFOR<>.w_MOTIPFOR
            .w_MOALMAIL = 'S'
        endif
        if .o_MOTIPOWP<>.w_MOTIPOWP
          .Calculate_SUMVCJYYDK()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(99,103,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page5.oPag.ZoomAll.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
    endwith
  return

  proc Calculate_SUMVCJYYDK()
    with this
          * --- Controllo su MOTIPOWP
          .w_MOPATMOD = iif(.w_MOTIPOWP='N' OR (.w_MOTIPOWP='W' AND LOWER(JUSTEXT(.w_MOPATMOD)) $ 'stw-sxw') OR (.w_MOTIPOWP='O' AND LOWER(JUSTEXT(.w_MOPATMOD)) $ 'dot-doc'),'',.w_MOPATMOD)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMOGIOFIS_1_10.enabled = this.oPgFrm.Page1.oPag.oMOGIOFIS_1_10.mCond()
    this.oPgFrm.Page2.oPag.oMOCODUTE_2_1.enabled = this.oPgFrm.Page2.oPag.oMOCODUTE_2_1.mCond()
    this.oPgFrm.Page2.oPag.oMOGRPUTE_2_2.enabled = this.oPgFrm.Page2.oPag.oMOGRPUTE_2_2.mCond()
    this.oPgFrm.Page3.oPag.oMOSCOCL2_3_14.enabled = this.oPgFrm.Page3.oPag.oMOSCOCL2_3_14.mCond()
    this.oPgFrm.Page3.oPag.oMOSPEIMB_3_19.enabled = this.oPgFrm.Page3.oPag.oMOSPEIMB_3_19.mCond()
    this.oPgFrm.Page3.oPag.oMOSPETRA_3_21.enabled = this.oPgFrm.Page3.oPag.oMOSPETRA_3_21.mCond()
    this.oPgFrm.Page4.oPag.oMOPATMOD_4_3.enabled = this.oPgFrm.Page4.oPag.oMOPATMOD_4_3.mCond()
    this.oPgFrm.Page4.oPag.oMOPATARC_4_8.enabled = this.oPgFrm.Page4.oPag.oMOPATARC_4_8.mCond()
    this.oPgFrm.Page4.oPag.oMONUMPER_4_10.enabled = this.oPgFrm.Page4.oPag.oMONUMPER_4_10.mCond()
    this.oPgFrm.Page4.oPag.oMONOMDOC_4_12.enabled = this.oPgFrm.Page4.oPag.oMONOMDOC_4_12.mCond()
    this.oPgFrm.Page4.oPag.oMOTIPFOR_4_13.enabled = this.oPgFrm.Page4.oPag.oMOTIPFOR_4_13.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_4.enabled = this.oPgFrm.Page4.oPag.oBtn_4_4.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_9.enabled = this.oPgFrm.Page4.oPag.oBtn_4_9.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_4.enabled = this.oPgFrm.Page5.oPag.oBtn_5_4.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_5.enabled = this.oPgFrm.Page5.oPag.oBtn_5_5.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_6.enabled = this.oPgFrm.Page5.oPag.oBtn_5_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(Isalt())
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Filtri"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    local i_show2
    i_show2=not(Isalt())
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Dati generali"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    local i_show4
    i_show4=not(Isalt())
    this.oPgFrm.Pages(5).enabled=i_show4
    this.oPgFrm.Pages(5).caption=iif(i_show4,cp_translate("Allegati"),"")
    this.oPgFrm.Pages(5).oPag.visible=this.oPgFrm.Pages(5).enabled
    this.oPgFrm.Page1.oPag.oMONUMSCO_1_6.visible=!this.oPgFrm.Page1.oPag.oMONUMSCO_1_6.mHide()
    this.oPgFrm.Page1.oPag.oMOINISCA_1_7.visible=!this.oPgFrm.Page1.oPag.oMOINISCA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oMOINISCA_1_8.visible=!this.oPgFrm.Page1.oPag.oMOINISCA_1_8.mHide()
    this.oPgFrm.Page1.oPag.oMOGIOSCA_1_9.visible=!this.oPgFrm.Page1.oPag.oMOGIOSCA_1_9.mHide()
    this.oPgFrm.Page1.oPag.oMOGIOFIS_1_10.visible=!this.oPgFrm.Page1.oPag.oMOGIOFIS_1_10.mHide()
    this.oPgFrm.Page1.oPag.oMOCODCAU_1_11.visible=!this.oPgFrm.Page1.oPag.oMOCODCAU_1_11.mHide()
    this.oPgFrm.Page1.oPag.oMOFLPROV_1_15.visible=!this.oPgFrm.Page1.oPag.oMOFLPROV_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oMOPRZVAC_1_57.visible=!this.oPgFrm.Page1.oPag.oMOPRZVAC_1_57.mHide()
    this.oPgFrm.Page4.oPag.oMOALMAIL_4_26.visible=!this.oPgFrm.Page4.oPag.oMOALMAIL_4_26.mHide()
    this.oPgFrm.Page4.oPag.oMOALMAIL_4_28.visible=!this.oPgFrm.Page4.oPag.oMOALMAIL_4_28.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page5.oPag.ZoomAll.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MOCODCAU
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_BZA',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MOCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDRICNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MOCODCAU))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDRICNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODCAU)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODCAU) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMOCODCAU_1_11'),i_cWhere,'GSOF_BZA',"Causali documenti",'GSOF0ATD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDRICNOM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDRICNOM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MOCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MOCODCAU)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODCAU = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLCOMM = NVL(_Link_.TDFLCOMM,space(1))
      this.w_RICNOM = NVL(_Link_.TDRICNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_CATDOC = space(2)
      this.w_FLINTE = space(1)
      this.w_FLVEAC = space(1)
      this.w_FLANAL = space(1)
      this.w_FLCOMM = space(1)
      this.w_RICNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_FLVEAC='V' AND .w_FLINTE='C' AND (.w_CATDOC='OR' OR .w_CATDOC='OP' OR (.w_CATDOC='DI' AND .w_RICNOM<>'A' AND EMPTY(.w_FLCOMM))) AND .w_FLORDAPE<>"S") or Isalt()
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_MOCODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_CATDOC = space(2)
        this.w_FLINTE = space(1)
        this.w_FLVEAC = space(1)
        this.w_FLANAL = space(1)
        this.w_FLCOMM = space(1)
        this.w_RICNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.TDTIPDOC as TDTIPDOC111"+ ",link_1_11.TDDESDOC as TDDESDOC111"+ ",link_1_11.TDCATDOC as TDCATDOC111"+ ",link_1_11.TDFLINTE as TDFLINTE111"+ ",link_1_11.TDFLVEAC as TDFLVEAC111"+ ",link_1_11.TDFLANAL as TDFLANAL111"+ ",link_1_11.TDFLCOMM as TDFLCOMM111"+ ",link_1_11.TDRICNOM as TDRICNOM111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on MOD_OFFE.MOCODCAU=link_1_11.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODCAU=link_1_11.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODCAU
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_BZA',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MOCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDORDAPE,TDRICNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MOCODCAU))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDORDAPE,TDRICNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODCAU)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODCAU) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMOCODCAU_1_12'),i_cWhere,'GSOF_BZA',"Causali documenti",'GSOF0ATD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDORDAPE,TDRICNOM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDORDAPE,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDORDAPE,TDRICNOM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MOCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MOCODCAU)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM,TDORDAPE,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODCAU = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLCOMM = NVL(_Link_.TDFLCOMM,space(1))
      this.w_FLORDAPE = NVL(_Link_.TDORDAPE,space(1))
      this.w_RICNOM = NVL(_Link_.TDRICNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_CATDOC = space(2)
      this.w_FLINTE = space(1)
      this.w_FLVEAC = space(1)
      this.w_FLANAL = space(1)
      this.w_FLCOMM = space(1)
      this.w_FLORDAPE = space(1)
      this.w_RICNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_FLVEAC='V' AND .w_FLINTE='C' AND (.w_CATDOC='OR' OR .w_CATDOC='OP' OR (.w_CATDOC='DI'  AND EMPTY(.w_FLANAL) AND EMPTY(.w_FLCOMM))) and !Isalt()) or (Isalt() and sotcatdo(.w_MOCODCAU)='V')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_MOCODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_CATDOC = space(2)
        this.w_FLINTE = space(1)
        this.w_FLVEAC = space(1)
        this.w_FLANAL = space(1)
        this.w_FLCOMM = space(1)
        this.w_FLORDAPE = space(1)
        this.w_RICNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.TDTIPDOC as TDTIPDOC112"+ ",link_1_12.TDDESDOC as TDDESDOC112"+ ",link_1_12.TDCATDOC as TDCATDOC112"+ ",link_1_12.TDFLINTE as TDFLINTE112"+ ",link_1_12.TDFLVEAC as TDFLVEAC112"+ ",link_1_12.TDFLANAL as TDFLANAL112"+ ",link_1_12.TDFLCOMM as TDFLCOMM112"+ ",link_1_12.TDORDAPE as TDORDAPE112"+ ",link_1_12.TDRICNOM as TDRICNOM112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on MOD_OFFE.MOCODCAU=link_1_12.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODCAU=link_1_12.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODVAL
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_MOCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_MOCODVAL))
          select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADTOBSO like "+cp_ToStrODBC(trim(this.w_MOCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADTOBSO like "+cp_ToStr(trim(this.w_MOCODVAL)+"%");

            select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oMOCODVAL_1_16'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MOCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MOCODVAL)
            select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_SIMBOL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_MOCODVAL = space(3)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_SIMBOL = space(5)
      this.w_DECTOT = 0
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE",CHKDTOBS(this, .w_DATOBSO, .w_OBTEST,"Valuta obsoleta alla data Attuale!"), CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
        endif
        this.w_MOCODVAL = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_SIMBOL = space(5)
        this.w_DECTOT = 0
        this.w_DECUNI = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.VACODVAL as VACODVAL116"+ ",link_1_16.VADTOBSO as VADTOBSO116"+ ",link_1_16.VASIMVAL as VASIMVAL116"+ ",link_1_16.VADECTOT as VADECTOT116"+ ",link_1_16.VADECUNI as VADECUNI116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on MOD_OFFE.MOCODVAL=link_1_16.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODVAL=link_1_16.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODLIS
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_MOCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_MOCODLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_MOCODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_MOCODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oMOCODLIS_1_22'),i_cWhere,'GSAR_ALI',"Listini",'GSVE1MDV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_MOCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_MOCODLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(35))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODLIS = space(5)
      endif
      this.w_DESLIS = space(35)
      this.w_VALLIS = space(3)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_IVALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MOCODLIS) OR CHKLISD(.w_MOCODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_MOCODVAL, i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOCODLIS = space(5)
        this.w_DESLIS = space(35)
        this.w_VALLIS = space(3)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_IVALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.LSCODLIS as LSCODLIS122"+ ",link_1_22.LSDESLIS as LSDESLIS122"+ ",link_1_22.LSVALLIS as LSVALLIS122"+ ",link_1_22.LSDTINVA as LSDTINVA122"+ ",link_1_22.LSDTOBSO as LSDTOBSO122"+ ",link_1_22.LSIVALIS as LSIVALIS122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on MOD_OFFE.MOCODLIS=link_1_22.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODLIS=link_1_22.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODUTE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_MOCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_MOCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_MOCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oMOCODUTE_2_1'),i_cWhere,'',"Utenti azienda",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_MOCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_MOCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODUTE = NVL(_Link_.CODE,0)
      this.w_MODESOPE = NVL(_Link_.NAME,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODUTE = 0
      endif
      this.w_MODESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPUSERS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CODE as CODE201"+ ",link_2_1.NAME as NAME201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on MOD_OFFE.MOCODUTE=link_2_1.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODUTE=link_2_1.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOGRPUTE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOGRPUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_MOGRPUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_MOGRPUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_MOGRPUTE) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oMOGRPUTE_2_2'),i_cWhere,'',"Gruppi utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOGRPUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_MOGRPUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_MOGRPUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOGRPUTE = NVL(_Link_.CODE,0)
      this.w_MODESGRN = NVL(_Link_.NAME,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOGRPUTE = 0
      endif
      this.w_MODESGRN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOGRPUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPGROUPS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CODE as CODE202"+ ",link_2_2.NAME as NAME202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on MOD_OFFE.MOGRPUTE=link_2_2.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOGRPUTE=link_2_2.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODPRI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODPRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AGP',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_MOCODPRI)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_MOCODPRI))
          select GPCODICE,GPDESCRI,GPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODPRI)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODPRI) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oMOCODPRI_2_3'),i_cWhere,'GSOF_AGP',"Gruppi priorit�",'GSOF_ZGP.GRU_PRIO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODPRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_MOCODPRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_MOCODPRI)
            select GPCODICE,GPDESCRI,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODPRI = NVL(_Link_.GPCODICE,space(5))
      this.w_MODESPRI = NVL(_Link_.GPDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.GPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODPRI = space(5)
      endif
      this.w_MODESPRI = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MOCODPRI = space(5)
        this.w_MODESPRI = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODPRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRU_PRIO_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.GPCODICE as GPCODICE203"+ ",link_2_3.GPDESCRI as GPDESCRI203"+ ",link_2_3.GPDTOBSO as GPDTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on MOD_OFFE.MOCODPRI=link_2_3.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODPRI=link_2_3.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODGRN
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODGRN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGN',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_MOCODGRN)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_MOCODGRN))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODGRN)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODGRN) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oMOCODGRN_2_4'),i_cWhere,'GSAR_AGN',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODGRN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_MOCODGRN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_MOCODGRN)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODGRN = NVL(_Link_.GNCODICE,space(5))
      this.w_MODESGRU = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODGRN = space(5)
      endif
      this.w_MODESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODGRN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRU_NOMI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.GNCODICE as GNCODICE204"+ ",link_2_4.GNDESCRI as GNDESCRI204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on MOD_OFFE.MOCODGRN=link_2_4.GNCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODGRN=link_2_4.GNCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODORI
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AON',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_MOCODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_MOCODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oMOCODORI_2_5'),i_cWhere,'GSAR_AON',"Origini nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_MOCODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_MOCODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_MODESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODORI = space(5)
      endif
      this.w_MODESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ORI_NOMI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.ONCODICE as ONCODICE205"+ ",link_2_5.ONDESCRI as ONDESCRI205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on MOD_OFFE.MOCODORI=link_2_5.ONCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODORI=link_2_5.ONCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODZON
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_MOCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_MOCODZON))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oMOCODZON_2_6'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_MOCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_MOCODZON)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODZON = space(3)
      endif
      this.w_DESZON = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_MOCODZON = space(3)
        this.w_DESZON = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZONE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.ZOCODZON as ZOCODZON206"+ ",link_2_6.ZODESZON as ZODESZON206"+ ",link_2_6.ZODTOBSO as ZODTOBSO206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on MOD_OFFE.MOCODZON=link_2_6.ZOCODZON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODZON=link_2_6.ZOCODZON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODLIN
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_MOCODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_MOCODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oMOCODLIN_2_7'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_MOCODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_MOCODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODLIN = space(3)
      endif
      this.w_DESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LINGUE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.LUCODICE as LUCODICE207"+ ",link_2_7.LUDESCRI as LUDESCRI207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on MOD_OFFE.MOCODLIN=link_2_7.LUCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODLIN=link_2_7.LUCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODPOR
  func Link_3_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_lTable = "PORTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2], .t., this.PORTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APO',True,'PORTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" POCODPOR like "+cp_ToStrODBC(trim(this.w_MOCODPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by POCODPOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'POCODPOR',trim(this.w_MOCODPOR))
          select POCODPOR,PODESPOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by POCODPOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODPOR)==trim(_Link_.POCODPOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODPOR) and !this.bDontReportError
            deferred_cp_zoom('PORTI','*','POCODPOR',cp_AbsName(oSource.parent,'oMOCODPOR_3_1'),i_cWhere,'GSAR_APO',"Porti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                     +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',oSource.xKey(1))
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                   +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(this.w_MOCODPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',this.w_MOCODPOR)
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODPOR = NVL(_Link_.POCODPOR,space(1))
      this.w_DESPOR = NVL(cp_TransLoadField('_Link_.PODESPOR'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODPOR = space(1)
      endif
      this.w_DESPOR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])+'\'+cp_ToStr(_Link_.POCODPOR,1)
      cp_ShowWarn(i_cKey,this.PORTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PORTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_1.POCODPOR as POCODPOR301"+ ","+cp_TransLinkFldName('link_3_1.PODESPOR')+" as PODESPOR301"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_1 on MOD_OFFE.MOCODPOR=link_3_1.POCODPOR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_1"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODPOR=link_3_1.POCODPOR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODSPE
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_lTable = "MODASPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2], .t., this.MODASPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASP',True,'MODASPED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SPCODSPE like "+cp_ToStrODBC(trim(this.w_MOCODSPE)+"%");

          i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SPCODSPE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SPCODSPE',trim(this.w_MOCODSPE))
          select SPCODSPE,SPDESSPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SPCODSPE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODSPE)==trim(_Link_.SPCODSPE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODSPE) and !this.bDontReportError
            deferred_cp_zoom('MODASPED','*','SPCODSPE',cp_AbsName(oSource.parent,'oMOCODSPE_3_3'),i_cWhere,'GSAR_ASP',"Spedizioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                     +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',oSource.xKey(1))
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(this.w_MOCODSPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',this.w_MOCODSPE)
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODSPE = NVL(_Link_.SPCODSPE,space(3))
      this.w_DESSPE = NVL(cp_TransLoadField('_Link_.SPDESSPE'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODSPE = space(3)
      endif
      this.w_DESSPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])+'\'+cp_ToStr(_Link_.SPCODSPE,1)
      cp_ShowWarn(i_cKey,this.MODASPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODASPED_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_3.SPCODSPE as SPCODSPE303"+ ","+cp_TransLinkFldName('link_3_3.SPDESSPE')+" as SPDESSPE303"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_3 on MOD_OFFE.MOCODSPE=link_3_3.SPCODSPE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_3"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODSPE=link_3_3.SPCODSPE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODPAG
  func Link_3_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_MOCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_MOCODPAG))
          select PACODICE,PADTOBSO,PADESCRI,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PADTOBSO like "+cp_ToStrODBC(trim(this.w_MOCODPAG)+"%");

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADTOBSO like "+cp_ToStr(trim(this.w_MOCODPAG)+"%");

            select PACODICE,PADTOBSO,PADESCRI,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oMOCODPAG_3_6'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADTOBSO,PADESCRI,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_MOCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_MOCODPAG)
            select PACODICE,PADTOBSO,PADESCRI,PASCONTO,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_MOSCOPAG = NVL(_Link_.PASCONTO,0)
      this.w_VALINC = NVL(_Link_.PAVALINC,space(3))
      this.w_SPEINC = NVL(_Link_.PASPEINC,0)
      this.w_VALIN2 = NVL(_Link_.PAVALIN2,space(3))
      this.w_SPEIN2 = NVL(_Link_.PASPEIN2,0)
    else
      if i_cCtrl<>'Load'
        this.w_MOCODPAG = space(5)
      endif
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_DESPAG = space(30)
      this.w_MOSCOPAG = 0
      this.w_VALINC = space(3)
      this.w_SPEINC = 0
      this.w_VALIN2 = space(3)
      this.w_SPEIN2 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento non definito oppure obsoleto")
        endif
        this.w_MOCODPAG = space(5)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_DESPAG = space(30)
        this.w_MOSCOPAG = 0
        this.w_VALINC = space(3)
        this.w_SPEINC = 0
        this.w_VALIN2 = space(3)
        this.w_SPEIN2 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_6.PACODICE as PACODICE306"+ ",link_3_6.PADTOBSO as PADTOBSO306"+ ","+cp_TransLinkFldName('link_3_6.PADESCRI')+" as PADESCRI306"+ ",link_3_6.PASCONTO as PASCONTO306"+ ",link_3_6.PAVALINC as PAVALINC306"+ ",link_3_6.PASPEINC as PASPEINC306"+ ",link_3_6.PAVALIN2 as PAVALIN2306"+ ",link_3_6.PASPEIN2 as PASPEIN2306"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_6 on MOD_OFFE.MOCODPAG=link_3_6.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_6"
          i_cKey=i_cKey+'+" and MOD_OFFE.MOCODPAG=link_3_6.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MONUMREP
  func Link_4_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_lTable = "OUT_PUTS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2], .t., this.OUT_PUTS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MONUMREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MOU',True,'OUT_PUTS')
        if i_nConn<>0
          i_cWhere = " OUROWNUM="+cp_ToStrODBC(this.w_MONUMREP);
                   +" and OUNOMPRG="+cp_ToStrODBC(this.w_PRGSTA);

          i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUROWNUM,OUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OUNOMPRG',this.w_PRGSTA;
                     ,'OUROWNUM',this.w_MONUMREP)
          select OUNOMPRG,OUROWNUM,OUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_MONUMREP) and !this.bDontReportError
            deferred_cp_zoom('OUT_PUTS','*','OUNOMPRG,OUROWNUM',cp_AbsName(oSource.parent,'oMONUMREP_4_6'),i_cWhere,'GSUT_MOU',"Report",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PRGSTA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUROWNUM,OUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select OUNOMPRG,OUROWNUM,OUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUROWNUM,OUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where OUROWNUM="+cp_ToStrODBC(oSource.xKey(2));
                     +" and OUNOMPRG="+cp_ToStrODBC(this.w_PRGSTA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',oSource.xKey(1);
                       ,'OUROWNUM',oSource.xKey(2))
            select OUNOMPRG,OUROWNUM,OUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MONUMREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUROWNUM,OUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where OUROWNUM="+cp_ToStrODBC(this.w_MONUMREP);
                   +" and OUNOMPRG="+cp_ToStrODBC(this.w_PRGSTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',this.w_PRGSTA;
                       ,'OUROWNUM',this.w_MONUMREP)
            select OUNOMPRG,OUROWNUM,OUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MONUMREP = NVL(_Link_.OUROWNUM,0)
      this.w_DESOUT = NVL(_Link_.OUDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_MONUMREP = 0
      endif
      this.w_DESOUT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])+'\'+cp_ToStr(_Link_.OUNOMPRG,1)+'\'+cp_ToStr(_Link_.OUROWNUM,1)
      cp_ShowWarn(i_cKey,this.OUT_PUTS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MONUMREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZNUMSCO";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZNUMSCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_NUMSCO = NVL(_Link_.AZNUMSCO,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_NUMSCO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMOCODICE_1_2.value==this.w_MOCODICE)
      this.oPgFrm.Page1.oPag.oMOCODICE_1_2.value=this.w_MOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_3.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_3.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMOSEZVAR_1_4.RadioValue()==this.w_MOSEZVAR)
      this.oPgFrm.Page1.oPag.oMOSEZVAR_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOSERDOC_1_5.value==this.w_MOSERDOC)
      this.oPgFrm.Page1.oPag.oMOSERDOC_1_5.value=this.w_MOSERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMONUMSCO_1_6.value==this.w_MONUMSCO)
      this.oPgFrm.Page1.oPag.oMONUMSCO_1_6.value=this.w_MONUMSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oMOINISCA_1_7.RadioValue()==this.w_MOINISCA)
      this.oPgFrm.Page1.oPag.oMOINISCA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOINISCA_1_8.RadioValue()==this.w_MOINISCA)
      this.oPgFrm.Page1.oPag.oMOINISCA_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOGIOSCA_1_9.value==this.w_MOGIOSCA)
      this.oPgFrm.Page1.oPag.oMOGIOSCA_1_9.value=this.w_MOGIOSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oMOGIOFIS_1_10.value==this.w_MOGIOFIS)
      this.oPgFrm.Page1.oPag.oMOGIOFIS_1_10.value=this.w_MOGIOFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODCAU_1_11.value==this.w_MOCODCAU)
      this.oPgFrm.Page1.oPag.oMOCODCAU_1_11.value=this.w_MOCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODCAU_1_12.value==this.w_MOCODCAU)
      this.oPgFrm.Page1.oPag.oMOCODCAU_1_12.value=this.w_MOCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_13.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_13.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oMOFLPROV_1_15.RadioValue()==this.w_MOFLPROV)
      this.oPgFrm.Page1.oPag.oMOFLPROV_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODVAL_1_16.value==this.w_MOCODVAL)
      this.oPgFrm.Page1.oPag.oMOCODVAL_1_16.value=this.w_MOCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMBOL_1_18.value==this.w_SIMBOL)
      this.oPgFrm.Page1.oPag.oSIMBOL_1_18.value=this.w_SIMBOL
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODLIS_1_22.value==this.w_MOCODLIS)
      this.oPgFrm.Page1.oPag.oMOCODLIS_1_22.value=this.w_MOCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_23.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_23.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMODATVAL_1_25.value==this.w_MODATVAL)
      this.oPgFrm.Page1.oPag.oMODATVAL_1_25.value=this.w_MODATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMODATOBS_1_27.value==this.w_MODATOBS)
      this.oPgFrm.Page1.oPag.oMODATOBS_1_27.value=this.w_MODATOBS
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODUTE_2_1.value==this.w_MOCODUTE)
      this.oPgFrm.Page2.oPag.oMOCODUTE_2_1.value=this.w_MOCODUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oMOGRPUTE_2_2.value==this.w_MOGRPUTE)
      this.oPgFrm.Page2.oPag.oMOGRPUTE_2_2.value=this.w_MOGRPUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODPRI_2_3.value==this.w_MOCODPRI)
      this.oPgFrm.Page2.oPag.oMOCODPRI_2_3.value=this.w_MOCODPRI
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODGRN_2_4.value==this.w_MOCODGRN)
      this.oPgFrm.Page2.oPag.oMOCODGRN_2_4.value=this.w_MOCODGRN
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODORI_2_5.value==this.w_MOCODORI)
      this.oPgFrm.Page2.oPag.oMOCODORI_2_5.value=this.w_MOCODORI
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODZON_2_6.value==this.w_MOCODZON)
      this.oPgFrm.Page2.oPag.oMOCODZON_2_6.value=this.w_MOCODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODLIN_2_7.value==this.w_MOCODLIN)
      this.oPgFrm.Page2.oPag.oMOCODLIN_2_7.value=this.w_MOCODLIN
    endif
    if not(this.oPgFrm.Page3.oPag.oMOCODPOR_3_1.value==this.w_MOCODPOR)
      this.oPgFrm.Page3.oPag.oMOCODPOR_3_1.value=this.w_MOCODPOR
    endif
    if not(this.oPgFrm.Page3.oPag.oMOCODSPE_3_3.value==this.w_MOCODSPE)
      this.oPgFrm.Page3.oPag.oMOCODSPE_3_3.value=this.w_MOCODSPE
    endif
    if not(this.oPgFrm.Page3.oPag.oMOCODPAG_3_6.value==this.w_MOCODPAG)
      this.oPgFrm.Page3.oPag.oMOCODPAG_3_6.value=this.w_MOCODPAG
    endif
    if not(this.oPgFrm.Page3.oPag.oMOSCOCL1_3_12.value==this.w_MOSCOCL1)
      this.oPgFrm.Page3.oPag.oMOSCOCL1_3_12.value=this.w_MOSCOCL1
    endif
    if not(this.oPgFrm.Page3.oPag.oMOSCOCL2_3_14.value==this.w_MOSCOCL2)
      this.oPgFrm.Page3.oPag.oMOSCOCL2_3_14.value=this.w_MOSCOCL2
    endif
    if not(this.oPgFrm.Page3.oPag.oMOSCOPAG_3_15.value==this.w_MOSCOPAG)
      this.oPgFrm.Page3.oPag.oMOSCOPAG_3_15.value=this.w_MOSCOPAG
    endif
    if not(this.oPgFrm.Page3.oPag.oMOSPEINC_3_17.value==this.w_MOSPEINC)
      this.oPgFrm.Page3.oPag.oMOSPEINC_3_17.value=this.w_MOSPEINC
    endif
    if not(this.oPgFrm.Page3.oPag.oMOSPEIMB_3_19.value==this.w_MOSPEIMB)
      this.oPgFrm.Page3.oPag.oMOSPEIMB_3_19.value=this.w_MOSPEIMB
    endif
    if not(this.oPgFrm.Page3.oPag.oMOSPETRA_3_21.value==this.w_MOSPETRA)
      this.oPgFrm.Page3.oPag.oMOSPETRA_3_21.value=this.w_MOSPETRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLIN_2_15.value==this.w_DESLIN)
      this.oPgFrm.Page2.oPag.oDESLIN_2_15.value=this.w_DESLIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZON_2_16.value==this.w_DESZON)
      this.oPgFrm.Page2.oPag.oDESZON_2_16.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page2.oPag.oMODESGRU_2_23.value==this.w_MODESGRU)
      this.oPgFrm.Page2.oPag.oMODESGRU_2_23.value=this.w_MODESGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oMODESORI_2_24.value==this.w_MODESORI)
      this.oPgFrm.Page2.oPag.oMODESORI_2_24.value=this.w_MODESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oMODESPRI_2_25.value==this.w_MODESPRI)
      this.oPgFrm.Page2.oPag.oMODESPRI_2_25.value=this.w_MODESPRI
    endif
    if not(this.oPgFrm.Page2.oPag.oMODESOPE_2_26.value==this.w_MODESOPE)
      this.oPgFrm.Page2.oPag.oMODESOPE_2_26.value=this.w_MODESOPE
    endif
    if not(this.oPgFrm.Page2.oPag.oMODESGRN_2_27.value==this.w_MODESGRN)
      this.oPgFrm.Page2.oPag.oMODESGRN_2_27.value=this.w_MODESGRN
    endif
    if not(this.oPgFrm.Page4.oPag.oMOTIPOWP_4_1.RadioValue()==this.w_MOTIPOWP)
      this.oPgFrm.Page4.oPag.oMOTIPOWP_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMOPATMOD_4_3.value==this.w_MOPATMOD)
      this.oPgFrm.Page4.oPag.oMOPATMOD_4_3.value=this.w_MOPATMOD
    endif
    if not(this.oPgFrm.Page4.oPag.oMONUMREP_4_6.value==this.w_MONUMREP)
      this.oPgFrm.Page4.oPag.oMONUMREP_4_6.value=this.w_MONUMREP
    endif
    if not(this.oPgFrm.Page4.oPag.oMOPATARC_4_8.value==this.w_MOPATARC)
      this.oPgFrm.Page4.oPag.oMOPATARC_4_8.value=this.w_MOPATARC
    endif
    if not(this.oPgFrm.Page4.oPag.oMONUMPER_4_10.RadioValue()==this.w_MONUMPER)
      this.oPgFrm.Page4.oPag.oMONUMPER_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMONOMDOC_4_12.value==this.w_MONOMDOC)
      this.oPgFrm.Page4.oPag.oMONOMDOC_4_12.value=this.w_MONOMDOC
    endif
    if not(this.oPgFrm.Page4.oPag.oMOTIPFOR_4_13.RadioValue()==this.w_MOTIPFOR)
      this.oPgFrm.Page4.oPag.oMOTIPFOR_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDESPOR_3_25.value==this.w_DESPOR)
      this.oPgFrm.Page3.oPag.oDESPOR_3_25.value=this.w_DESPOR
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSPE_3_26.value==this.w_DESSPE)
      this.oPgFrm.Page3.oPag.oDESSPE_3_26.value=this.w_DESSPE
    endif
    if not(this.oPgFrm.Page3.oPag.oDESPAG_3_27.value==this.w_DESPAG)
      this.oPgFrm.Page3.oPag.oDESPAG_3_27.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page4.oPag.oDESOUT_4_18.value==this.w_DESOUT)
      this.oPgFrm.Page4.oPag.oDESOUT_4_18.value=this.w_DESOUT
    endif
    if not(this.oPgFrm.Page5.oPag.oNOMFIL_5_11.value==this.w_NOMFIL)
      this.oPgFrm.Page5.oPag.oNOMFIL_5_11.value=this.w_NOMFIL
    endif
    if not(this.oPgFrm.Page5.oPag.oOGGETT_5_12.value==this.w_OGGETT)
      this.oPgFrm.Page5.oPag.oOGGETT_5_12.value=this.w_OGGETT
    endif
    if not(this.oPgFrm.Page5.oPag.oNOTE_5_13.value==this.w_NOTE)
      this.oPgFrm.Page5.oPag.oNOTE_5_13.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oCOD_2_28.value==this.w_COD)
      this.oPgFrm.Page2.oPag.oCOD_2_28.value=this.w_COD
    endif
    if not(this.oPgFrm.Page2.oPag.oDES1_2_29.value==this.w_DES1)
      this.oPgFrm.Page2.oPag.oDES1_2_29.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page3.oPag.oCOD_3_30.value==this.w_COD)
      this.oPgFrm.Page3.oPag.oCOD_3_30.value=this.w_COD
    endif
    if not(this.oPgFrm.Page3.oPag.oDES1_3_31.value==this.w_DES1)
      this.oPgFrm.Page3.oPag.oDES1_3_31.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page4.oPag.oCOD_4_24.value==this.w_COD)
      this.oPgFrm.Page4.oPag.oCOD_4_24.value=this.w_COD
    endif
    if not(this.oPgFrm.Page4.oPag.oDES1_4_25.value==this.w_DES1)
      this.oPgFrm.Page4.oPag.oDES1_4_25.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page5.oPag.oCOD_5_16.value==this.w_COD)
      this.oPgFrm.Page5.oPag.oCOD_5_16.value=this.w_COD
    endif
    if not(this.oPgFrm.Page5.oPag.oDES1_5_17.value==this.w_DES1)
      this.oPgFrm.Page5.oPag.oDES1_5_17.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPRZVAC_1_57.RadioValue()==this.w_MOPRZVAC)
      this.oPgFrm.Page1.oPag.oMOPRZVAC_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMOALMAIL_4_26.RadioValue()==this.w_MOALMAIL)
      this.oPgFrm.Page4.oPag.oMOALMAIL_4_26.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oMOALMAIL_4_28.RadioValue()==this.w_MOALMAIL)
      this.oPgFrm.Page4.oPag.oMOALMAIL_4_28.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'MOD_OFFE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MOCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_MOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MONUMSCO<=.w_NUMSCO)  and not(Isalt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMONUMSCO_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero sconti supera il massimo aziendale")
          case   not(.w_MOGIOFIS <= 31)  and not(Isalt())  and (.w_MOINISCA='GF' OR .w_MOINISCA='FM')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOGIOFIS_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MOCODCAU)) or not((.w_FLVEAC='V' AND .w_FLINTE='C' AND (.w_CATDOC='OR' OR .w_CATDOC='OP' OR (.w_CATDOC='DI' AND .w_RICNOM<>'A' AND EMPTY(.w_FLCOMM))) AND .w_FLORDAPE<>"S") or Isalt()))  and not(upper(g_APPLICATION) = "ADHOC ENTERPRISE")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODCAU_1_11.SetFocus()
            i_bnoObbl = !empty(.w_MOCODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   ((empty(.w_MOCODCAU)) or not((.w_FLVEAC='V' AND .w_FLINTE='C' AND (.w_CATDOC='OR' OR .w_CATDOC='OP' OR (.w_CATDOC='DI'  AND EMPTY(.w_FLANAL) AND EMPTY(.w_FLCOMM))) and !Isalt()) or (Isalt() and sotcatdo(.w_MOCODCAU)='V')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODCAU_1_12.SetFocus()
            i_bnoObbl = !empty(.w_MOCODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   ((empty(.w_MOCODVAL)) or not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE",CHKDTOBS(this, .w_DATOBSO, .w_OBTEST,"Valuta obsoleta alla data Attuale!"), CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODVAL_1_16.SetFocus()
            i_bnoObbl = !empty(.w_MOCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
          case   not(EMPTY(.w_MOCODLIS) OR CHKLISD(.w_MOCODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_MOCODVAL, i_datsys))  and not(empty(.w_MOCODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODLIS_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_MOCODPRI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOCODPRI_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_MOCODZON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOCODZON_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(empty(.w_MOCODPAG))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMOCODPAG_3_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento non definito oppure obsoleto")
          case   not(EMPTY(.w_MOPATMOD) OR FILE(FULLPATH(ALLTRIM(.w_MOPATMOD))))  and (.w_MOTIPOWP<>'N')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMOPATMOD_4_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il percorso per il modello Word Processor non � valido")
          case   not(EMPTY(.w_MOPATARC) OR DIRECTORY(FULLPATH(ALLTRIM(.w_MOPATARC))))  and (.w_MOTIPOWP<>'N')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMOPATARC_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il percorso di archiviazione non � valido")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSOF_MSM.CheckForm()
      if i_bres
        i_bres=  .GSOF_MSM.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSOF_MAM.CheckForm()
      if i_bres
        i_bres=  .GSOF_MAM.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsof_amo
      * --- Controlli ----
      if i_bRes=.t.
         .w_BRES=.t.
          Ah_Msg('Controlli finali',.T.)
           .NotifyEvent('ControlliFinali')
           WAIT CLEAR
           if .w_BRES=.f.
              i_bRes=.f.
           endif
      endif
      * --- Controllo campi obbligatori
      if i_bRes=.T.
         do case
         case .w_MOINISCA $ 'GF-FM'
            if EMPTY(.w_MOGIOFIS)
             i_bRes=.F.
             i_bnoChk=.F.
             i_cErrorMsg=Ah_MsgFormat("Giorno fisso obbligatorio")
            endif
         endcase
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MOINISCA = this.w_MOINISCA
    this.o_MOCODVAL = this.w_MOCODVAL
    this.o_MOCODPAG = this.w_MOCODPAG
    this.o_MOSCOCL1 = this.w_MOSCOCL1
    this.o_MOSCOCL2 = this.w_MOSCOCL2
    this.o_MOTIPOWP = this.w_MOTIPOWP
    this.o_MOTIPFOR = this.w_MOTIPFOR
    * --- GSOF_MSM : Depends On
    this.GSOF_MSM.SaveDependsOn()
    * --- GSOF_MAM : Depends On
    this.GSOF_MAM.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsof_amoPag1 as StdContainer
  Width  = 683
  height = 434
  stdWidth  = 683
  stdheight = 434
  resizeXpos=274
  resizeYpos=311
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOCODICE_1_2 as StdField with uid="ZJXCGHZULT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MOCODICE", cQueryName = "MOCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del modello",;
    HelpContextID = 251045109,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=60, Left=136, Top=12, InputMask=replicate('X',5)

  add object oMODESCRI_1_3 as StdField with uid="TFNVQXLGAL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 68195569,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=202, Top=12, InputMask=replicate('X',35)

  add object oMOSEZVAR_1_4 as StdCheck with uid="MSWXOLXGIT",rtseq=4,rtrep=.f.,left=494, top=12, caption="Sezioni a struttura rigida",;
    ToolTipText = "Se attivo: non consente di modificare la struttura nelle offerte",;
    HelpContextID = 10462440,;
    cFormVar="w_MOSEZVAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOSEZVAR_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMOSEZVAR_1_4.GetRadio()
    this.Parent.oContained.w_MOSEZVAR = this.RadioValue()
    return .t.
  endfunc

  func oMOSEZVAR_1_4.SetRadio()
    this.Parent.oContained.w_MOSEZVAR=trim(this.Parent.oContained.w_MOSEZVAR)
    this.value = ;
      iif(this.Parent.oContained.w_MOSEZVAR=='S',1,;
      0)
  endfunc

  add object oMOSERDOC_1_5 as StdField with uid="FXWZKXRTKY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MOSERDOC", cQueryName = "MOSERDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento",;
    HelpContextID = 52405495,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=136, Top=42, InputMask=replicate('X',10)

  add object oMONUMSCO_1_6 as StdField with uid="ECRUNLLSYY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MONUMSCO", cQueryName = "MONUMSCO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero sconti supera il massimo aziendale",;
    ToolTipText = "Numero sconti utilizzati",;
    HelpContextID = 73397483,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=433, Top=42, cSayPict='"9"', cGetPict='"9"'

  func oMONUMSCO_1_6.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oMONUMSCO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MONUMSCO<=.w_NUMSCO)
    endwith
    return bRes
  endfunc


  add object oMOINISCA_1_7 as StdCombo with uid="EIHVAGNQCO",rtseq=7,rtrep=.f.,left=136,top=72,width=144,height=21;
    , ToolTipText = "Inizio scadenza";
    , HelpContextID = 78071033;
    , cFormVar="w_MOINISCA",RowSource=""+"Data offerta,"+"Fine mese,"+"Giorno fisso,"+"Fine mese + g.fisso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOINISCA_1_7.RadioValue()
    return(iif(this.value =1,'DF',;
    iif(this.value =2,'FF',;
    iif(this.value =3,'GF',;
    iif(this.value =4,'FM',;
    space(2))))))
  endfunc
  func oMOINISCA_1_7.GetRadio()
    this.Parent.oContained.w_MOINISCA = this.RadioValue()
    return .t.
  endfunc

  func oMOINISCA_1_7.SetRadio()
    this.Parent.oContained.w_MOINISCA=trim(this.Parent.oContained.w_MOINISCA)
    this.value = ;
      iif(this.Parent.oContained.w_MOINISCA=='DF',1,;
      iif(this.Parent.oContained.w_MOINISCA=='FF',2,;
      iif(this.Parent.oContained.w_MOINISCA=='GF',3,;
      iif(this.Parent.oContained.w_MOINISCA=='FM',4,;
      0))))
  endfunc

  func oMOINISCA_1_7.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE" or Isalt())
    endwith
  endfunc


  add object oMOINISCA_1_8 as StdCombo with uid="WMOUPWAQHP",rtseq=8,rtrep=.f.,left=136,top=72,width=144,height=21;
    , ToolTipText = "Inizio scadenza";
    , HelpContextID = 78071033;
    , cFormVar="w_MOINISCA",RowSource=""+"Data offerta,"+"Fine mese,"+"Giorno fisso,"+"Fine mese + g.fisso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOINISCA_1_8.RadioValue()
    return(iif(this.value =1,'DF',;
    iif(this.value =2,'FF',;
    iif(this.value =3,'GF',;
    iif(this.value =4,'FM',;
    space(2))))))
  endfunc
  func oMOINISCA_1_8.GetRadio()
    this.Parent.oContained.w_MOINISCA = this.RadioValue()
    return .t.
  endfunc

  func oMOINISCA_1_8.SetRadio()
    this.Parent.oContained.w_MOINISCA=trim(this.Parent.oContained.w_MOINISCA)
    this.value = ;
      iif(this.Parent.oContained.w_MOINISCA=='DF',1,;
      iif(this.Parent.oContained.w_MOINISCA=='FF',2,;
      iif(this.Parent.oContained.w_MOINISCA=='GF',3,;
      iif(this.Parent.oContained.w_MOINISCA=='FM',4,;
      0))))
  endfunc

  func oMOINISCA_1_8.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE" or Isalt())
    endwith
  endfunc

  add object oMOGIOSCA_1_9 as StdField with uid="RKYRPXNLDI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MOGIOSCA", cQueryName = "MOGIOSCA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni scadenza",;
    HelpContextID = 72115449,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=433, Top=72, cSayPict='"999"', cGetPict='"999"'

  func oMOGIOSCA_1_9.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oMOGIOFIS_1_10 as StdField with uid="UGUQALBKAL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MOGIOFIS", cQueryName = "MOGIOFIS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorno fisso di scadenza",;
    HelpContextID = 246651673,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=571, Top=72, cSayPict='"99"', cGetPict='"99"'

  func oMOGIOFIS_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOINISCA='GF' OR .w_MOINISCA='FM')
    endwith
   endif
  endfunc

  func oMOGIOFIS_1_10.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oMOGIOFIS_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MOGIOFIS <= 31)
    endwith
    return bRes
  endfunc

  add object oMOCODCAU_1_11 as StdField with uid="GNPXWXFXKU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MOCODCAU", cQueryName = "MOCODCAU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Causale del documento",;
    HelpContextID = 83272933,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=101, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOF_BZA", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MOCODCAU"

  func oMOCODCAU_1_11.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC ENTERPRISE")
    endwith
  endfunc

  func oMOCODCAU_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODCAU_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODCAU_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMOCODCAU_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_BZA',"Causali documenti",'GSOF0ATD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMOCODCAU_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSOF_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MOCODCAU
     i_obj.ecpSave()
  endproc

  add object oMOCODCAU_1_12 as StdField with uid="TCXSPXPMZW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MOCODCAU", cQueryName = "MOCODCAU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Causale del documento",;
    HelpContextID = 83272933,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=136, Top=101, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOF_BZA", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MOCODCAU"

  func oMOCODCAU_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODCAU_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODCAU_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMOCODCAU_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_BZA',"Causali documenti",'GSOF0ATD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMOCODCAU_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSOF_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MOCODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_13 as StdField with uid="IILFTTOWZY",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale",;
    HelpContextID = 53587914,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=202, Top=101, InputMask=replicate('X',35)


  add object oMOFLPROV_1_15 as StdCombo with uid="VFADSTSSSY",rtseq=15,rtrep=.f.,left=571,top=102,width=103,height=21;
    , ToolTipText = "Stato del documento";
    , HelpContextID = 87651556;
    , cFormVar="w_MOFLPROV",RowSource=""+"Provvisorio,"+"Confermato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOFLPROV_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oMOFLPROV_1_15.GetRadio()
    this.Parent.oContained.w_MOFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oMOFLPROV_1_15.SetRadio()
    this.Parent.oContained.w_MOFLPROV=trim(this.Parent.oContained.w_MOFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_MOFLPROV=='S',1,;
      iif(this.Parent.oContained.w_MOFLPROV=='N',2,;
      0))
  endfunc

  func oMOFLPROV_1_15.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oMOCODVAL_1_16 as StdField with uid="UHCIMXFXUW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MOCODVAL", cQueryName = "MOCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o obsoleta o valuta esercizio non definita",;
    ToolTipText = "Codice valuta del modello",;
    HelpContextID = 32941294,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=136, Top=131, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_MOCODVAL"

  func oMOCODVAL_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODVAL_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODVAL_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oMOCODVAL_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oMOCODVAL_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_MOCODVAL
     i_obj.ecpSave()
  endproc

  add object oSIMBOL_1_18 as StdField with uid="JVDEUHNYEW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_SIMBOL", cQueryName = "SIMBOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo valuta",;
    HelpContextID = 189991642,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=187, Top=131, InputMask=replicate('X',5)

  add object oMOCODLIS_1_22 as StdField with uid="WGFYAWLQYP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MOCODLIS", cQueryName = "MOCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del listino",;
    HelpContextID = 67722009,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=337, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_MOCODLIS"

  func oMOCODLIS_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODLIS_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODLIS_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oMOCODLIS_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSVE1MDV.LISTINI_VZM',this.parent.oContained
  endproc
  proc oMOCODLIS_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_MOCODLIS
     i_obj.ecpSave()
  endproc

  add object oDESLIS_1_23 as StdField with uid="PCIDELKLKM",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione listino",;
    HelpContextID = 78163914,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=403, Top=131, InputMask=replicate('X',35)


  add object oLinkPC_1_24 as stdDynamicChildContainer with uid="GLXYAOPZFV",left=7, top=167, width=657, height=239, bOnScreen=.t.;


  add object oMODATVAL_1_25 as StdField with uid="VSAZNTHDNO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MODATVAL", cQueryName = "MODATVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 17077486,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=367, Top=411, tabstop=.f.

  add object oMODATOBS_1_27 as StdField with uid="ORXBLVEOAO",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MODATOBS", cQueryName = "MODATOBS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 134517991,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=590, Top=411, tabstop=.f.


  add object oObj_1_42 as cp_runprogram with uid="YUVAXTNYPJ",left=69, top=448, width=263,height=19,;
    caption='GSOF_BKM',;
   bGlobalFont=.t.,;
    prg="GSOF_BKM",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 196157107


  add object oObj_1_46 as cp_runprogram with uid="COFYYFOPAU",left=336, top=448, width=263,height=19,;
    caption='GSOF_BPC',;
   bGlobalFont=.t.,;
    prg="GSOF_BPC",;
    cEvent = "w_MOCODVAL Changed",;
    nPag=1;
    , HelpContextID = 72278359

  add object oMOPRZVAC_1_57 as StdCheck with uid="VECRYXCQMH",rtseq=96,rtrep=.f.,left=494, top=42, caption="Prezzo default U.P.V.",;
    HelpContextID = 9622775,;
    cFormVar="w_MOPRZVAC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOPRZVAC_1_57.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOPRZVAC_1_57.GetRadio()
    this.Parent.oContained.w_MOPRZVAC = this.RadioValue()
    return .t.
  endfunc

  func oMOPRZVAC_1_57.SetRadio()
    this.Parent.oContained.w_MOPRZVAC=trim(this.Parent.oContained.w_MOPRZVAC)
    this.value = ;
      iif(this.Parent.oContained.w_MOPRZVAC=='S',1,;
      0)
  endfunc

  func oMOPRZVAC_1_57.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="AD HOC ENTERPRISE" OR Isalt())
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="LJDLEFFYHC",Visible=.t., Left=46, Top=12,;
    Alignment=1, Width=88, Height=18,;
    Caption="Modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="GKIDZPQIXC",Visible=.t., Left=232, Top=42,;
    Alignment=1, Width=198, Height=18,;
    Caption="Sconti/maggiorazioni utilizzati:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="XPCYKDYIJE",Visible=.t., Left=18, Top=72,;
    Alignment=1, Width=116, Height=18,;
    Caption="Inizio scadenza:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="ADFAEWXEDL",Visible=.t., Left=21, Top=42,;
    Alignment=1, Width=113, Height=18,;
    Caption="Serie documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="DRZOBUFYCQ",Visible=.t., Left=494, Top=72,;
    Alignment=1, Width=74, Height=18,;
    Caption="G. fisso:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="EKUVXOMJJF",Visible=.t., Left=3, Top=101,;
    Alignment=1, Width=131, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="WHANVZIAJL",Visible=.t., Left=340, Top=72,;
    Alignment=1, Width=90, Height=18,;
    Caption="Gg.scadenza:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="XEULTUFOVP",Visible=.t., Left=277, Top=131,;
    Alignment=1, Width=58, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="NWZXMMFCVM",Visible=.t., Left=494, Top=101,;
    Alignment=1, Width=74, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="XLJJZWDQQZ",Visible=.t., Left=265, Top=415,;
    Alignment=1, Width=98, Height=18,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="ZMXBNNDUQI",Visible=.t., Left=448, Top=415,;
    Alignment=1, Width=139, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="QJNACXPCZP",Visible=.t., Left=70, Top=131,;
    Alignment=1, Width=64, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_amoPag2 as StdContainer
  Width  = 683
  height = 434
  stdWidth  = 683
  stdheight = 434
  resizeXpos=381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOCODUTE_2_1 as StdField with uid="TEMYSDFCLO",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MOCODUTE", cQueryName = "MOCODUTE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 49718517,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=127, Top=79, bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_MOCODUTE"

  func oMOCODUTE_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOGRPUTE = 0)
    endwith
   endif
  endfunc

  func oMOCODUTE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODUTE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODUTE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oMOCODUTE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti azienda",'',this.parent.oContained
  endproc

  add object oMOGRPUTE_2_2 as StdField with uid="YRCUSVECDH",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MOGRPUTE", cQueryName = "MOGRPUTE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Gruppo utente",;
    HelpContextID = 36922613,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=127, Top=107, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_MOGRPUTE"

  func oMOGRPUTE_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOCODUTE = 0)
    endwith
   endif
  endfunc

  func oMOGRPUTE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOGRPUTE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOGRPUTE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oMOGRPUTE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi utenti",'',this.parent.oContained
  endproc

  add object oMOCODPRI_2_3 as StdField with uid="LTZXEDFHNA",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MOCODPRI", cQueryName = "MOCODPRI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Gruppo priorit�",;
    HelpContextID = 133604593,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=177, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", cZoomOnZoom="GSOF_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_MOCODPRI"

  func oMOCODPRI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODPRI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODPRI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oMOCODPRI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AGP',"Gruppi priorit�",'GSOF_ZGP.GRU_PRIO_VZM',this.parent.oContained
  endproc
  proc oMOCODPRI_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_MOCODPRI
     i_obj.ecpSave()
  endproc

  add object oMOCODGRN_2_4 as StdField with uid="POUTKTHGQZ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MOCODGRN", cQueryName = "MOCODGRN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo di appartenenza",;
    HelpContextID = 16164076,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=206, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", cZoomOnZoom="GSAR_AGN", oKey_1_1="GNCODICE", oKey_1_2="this.w_MOCODGRN"

  func oMOCODGRN_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODGRN_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODGRN_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oMOCODGRN_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGN',"Gruppi nominativi",'',this.parent.oContained
  endproc
  proc oMOCODGRN_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GNCODICE=this.parent.oContained.w_MOCODGRN
     i_obj.ecpSave()
  endproc

  add object oMOCODORI_2_5 as StdField with uid="DRTMTMFUHF",rtseq=30,rtrep=.f.,;
    cFormVar = "w_MOCODORI", cQueryName = "MOCODORI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine reperimento nominitavo",;
    HelpContextID = 150381809,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=235, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", cZoomOnZoom="GSAR_AON", oKey_1_1="ONCODICE", oKey_1_2="this.w_MOCODORI"

  func oMOCODORI_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODORI_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODORI_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oMOCODORI_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AON',"Origini nominativi",'',this.parent.oContained
  endproc
  proc oMOCODORI_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AON()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ONCODICE=this.parent.oContained.w_MOCODORI
     i_obj.ecpSave()
  endproc

  add object oMOCODZON_2_6 as StdField with uid="HZRGRNUBEK",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MOCODZON", cQueryName = "MOCODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Codice della zona di appartenenza",;
    HelpContextID = 234267884,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=264, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_MOCODZON"

  func oMOCODZON_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODZON_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODZON_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oMOCODZON_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oMOCODZON_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_MOCODZON
     i_obj.ecpSave()
  endproc

  add object oMOCODLIN_2_7 as StdField with uid="AKKCWPUDAO",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MOCODLIN", cQueryName = "MOCODLIN",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Lingua collegata al nominativo",;
    HelpContextID = 67722004,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=293, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_MOCODLIN"

  func oMOCODLIN_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODLIN_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODLIN_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oMOCODLIN_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oMOCODLIN_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_MOCODLIN
     i_obj.ecpSave()
  endproc

  add object oDESLIN_2_15 as StdField with uid="CWTDJNOXMH",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESLIN", cQueryName = "DESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione lingua",;
    HelpContextID = 162049994,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=193, Top=293, InputMask=replicate('X',30)

  add object oDESZON_2_16 as StdField with uid="ENFHFGPEKI",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione zona",;
    HelpContextID = 154841034,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=193, Top=264, InputMask=replicate('X',35)

  add object oMODESGRU_2_23 as StdField with uid="RVWCTSCNEV",rtseq=50,rtrep=.f.,;
    cFormVar = "w_MODESGRU", cQueryName = "MODESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo nominativo",;
    HelpContextID = 1086693,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=193, Top=206, InputMask=replicate('X',35)

  add object oMODESORI_2_24 as StdField with uid="ODRVEDQFCX",rtseq=51,rtrep=.f.,;
    cFormVar = "w_MODESORI", cQueryName = "MODESORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione origine",;
    HelpContextID = 135304433,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=193, Top=235, InputMask=replicate('X',35)

  add object oMODESPRI_2_25 as StdField with uid="FLVUJUMBLP",rtseq=52,rtrep=.f.,;
    cFormVar = "w_MODESPRI", cQueryName = "MODESPRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione priorit�",;
    HelpContextID = 118527217,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=193, Top=177, InputMask=replicate('X',35)

  add object oMODESOPE_2_26 as StdField with uid="VFRULGHWXN",rtseq=53,rtrep=.f.,;
    cFormVar = "w_MODESOPE", cQueryName = "MODESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione operatore",;
    HelpContextID = 135304437,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=193, Top=79, InputMask=replicate('X',35)

  add object oMODESGRN_2_27 as StdField with uid="VVAQDFPMMO",rtseq=54,rtrep=.f.,;
    cFormVar = "w_MODESGRN", cQueryName = "MODESGRN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 1086700,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=193, Top=107, InputMask=replicate('X',35)

  add object oCOD_2_28 as StdField with uid="GXTHINYUCX",rtseq=73,rtrep=.f.,;
    cFormVar = "w_COD", cQueryName = "COD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del modello",;
    HelpContextID = 58354214,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=60, Left=127, Top=12, InputMask=replicate('X',5)

  add object oDES1_2_29 as StdField with uid="FIZIQBYMPF",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 61624374,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=193, Top=12, InputMask=replicate('X',35)

  add object oStr_2_8 as StdString with uid="ELVHONSCLO",Visible=.t., Left=8, Top=264,;
    Alignment=1, Width=117, Height=18,;
    Caption="Cod.zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="ZATVSYLLHQ",Visible=.t., Left=8, Top=293,;
    Alignment=1, Width=117, Height=18,;
    Caption="Cod. lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="WTNVOFUPVJ",Visible=.t., Left=8, Top=79,;
    Alignment=1, Width=117, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="KMXPVQFYOZ",Visible=.t., Left=8, Top=107,;
    Alignment=1, Width=117, Height=18,;
    Caption="Gruppo utenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="JGTPBYXOFG",Visible=.t., Left=8, Top=177,;
    Alignment=1, Width=117, Height=18,;
    Caption="Gruppo priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="YDCQWGNRGE",Visible=.t., Left=37, Top=12,;
    Alignment=1, Width=88, Height=18,;
    Caption="Modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_18 as StdString with uid="NZJVYCJCAQ",Visible=.t., Left=6, Top=50,;
    Alignment=0, Width=119, Height=18,;
    Caption="Privilegi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_20 as StdString with uid="AUQBPNAJEL",Visible=.t., Left=6, Top=149,;
    Alignment=0, Width=119, Height=18,;
    Caption="Nominativo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_21 as StdString with uid="HXHUGBYSBE",Visible=.t., Left=8, Top=206,;
    Alignment=1, Width=117, Height=18,;
    Caption="Gruppo nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="LYCTHQJGGO",Visible=.t., Left=8, Top=235,;
    Alignment=1, Width=117, Height=18,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oBox_2_14 as StdBox with uid="STMKYGSWTY",left=5, top=69, width=592,height=2

  add object oBox_2_19 as StdBox with uid="MQQPHUHNRP",left=5, top=168, width=592,height=2
enddefine
define class tgsof_amoPag3 as StdContainer
  Width  = 683
  height = 434
  stdWidth  = 683
  stdheight = 434
  resizeXpos=474
  resizeYpos=368
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOCODPOR_3_1 as StdField with uid="UPJXLSAJKS",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MOCODPOR", cQueryName = "MOCODPOR",;
    bObbl = .f. , nPag = 3, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Codice porto associato al modello",;
    HelpContextID = 133604584,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=127, Top=40, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="PORTI", cZoomOnZoom="GSAR_APO", oKey_1_1="POCODPOR", oKey_1_2="this.w_MOCODPOR"

  func oMOCODPOR_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODPOR_3_1.ecpDrop(oSource)
    this.Parent.oContained.link_3_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODPOR_3_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PORTI','*','POCODPOR',cp_AbsName(this.parent,'oMOCODPOR_3_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APO',"Porti",'',this.parent.oContained
  endproc
  proc oMOCODPOR_3_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_POCODPOR=this.parent.oContained.w_MOCODPOR
     i_obj.ecpSave()
  endproc

  add object oMOCODSPE_3_3 as StdField with uid="NNGJIAQQQO",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MOCODSPE", cQueryName = "MOCODSPE",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice spedizione associato al modello",;
    HelpContextID = 83272949,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=68, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODASPED", cZoomOnZoom="GSAR_ASP", oKey_1_1="SPCODSPE", oKey_1_2="this.w_MOCODSPE"

  func oMOCODSPE_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODSPE_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODSPE_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODASPED','*','SPCODSPE',cp_AbsName(this.parent,'oMOCODSPE_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASP',"Spedizioni",'',this.parent.oContained
  endproc
  proc oMOCODSPE_3_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SPCODSPE=this.parent.oContained.w_MOCODSPE
     i_obj.ecpSave()
  endproc

  add object oMOCODPAG_3_6 as StdField with uid="HBGMGBREOD",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MOCODPAG", cQueryName = "MOCODPAG",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento non definito oppure obsoleto",;
    ToolTipText = "Codice pagamento associato al modello",;
    HelpContextID = 133604595,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=96, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_MOCODPAG"

  func oMOCODPAG_3_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODPAG_3_6.ecpDrop(oSource)
    this.Parent.oContained.link_3_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODPAG_3_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oMOCODPAG_3_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oMOCODPAG_3_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_MOCODPAG
     i_obj.ecpSave()
  endproc

  add object oMOSCOCL1_3_12 as StdField with uid="OMNCFDBJHL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MOSCOCL1", cQueryName = "MOSCOCL1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^Maggiorazione (se positiva) o sconto (se negativa) applicata al modello",;
    HelpContextID = 195975927,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=124, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oMOSCOCL2_3_14 as StdField with uid="GEUYSNCBQN",rtseq=41,rtrep=.f.,;
    cFormVar = "w_MOSCOCL2", cQueryName = "MOSCOCL2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^ Maggiorazione (se positiva) o sconto (se negativa) applicata al modello",;
    HelpContextID = 195975928,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=211, Top=124, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMOSCOCL2_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOSCOCL1<>0)
    endwith
   endif
  endfunc

  add object oMOSCOPAG_3_15 as StdField with uid="CIFOZEJWPY",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MOSCOPAG", cQueryName = "MOSCOPAG",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% Maggiorazione (se positiva) o sconto (se negativa) applicata al pagamento",;
    HelpContextID = 122791155,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=396, Top=124, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oMOSPEINC_3_17 as StdField with uid="XDFPDSTBKR",rtseq=43,rtrep=.f.,;
    cFormVar = "w_MOSPEINC", cQueryName = "MOSPEINC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese incasso",;
    HelpContextID = 249865463,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=127, Top=152, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oMOSPEIMB_3_19 as StdField with uid="RILFFDCIDX",rtseq=44,rtrep=.f.,;
    cFormVar = "w_MOSPEIMB", cQueryName = "MOSPEIMB",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese imballo",;
    HelpContextID = 249865464,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=127, Top=180, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMOSPEIMB_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not empty(.w_MOCODVAL))
    endwith
   endif
  endfunc

  add object oMOSPETRA_3_21 as StdField with uid="NAEYRSFJIK",rtseq=45,rtrep=.f.,;
    cFormVar = "w_MOSPETRA", cQueryName = "MOSPETRA",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese trasporto",;
    HelpContextID = 65316089,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=127, Top=208, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oMOSPETRA_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not empty(.w_MOCODVAL))
    endwith
   endif
  endfunc


  add object oLinkPC_3_22 as stdDynamicChildContainer with uid="NFPEHGGDHD",left=50, top=264, width=589, height=138, bOnScreen=.t.;


  add object oDESPOR_3_25 as StdField with uid="YSIOMNMXDG",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESPOR", cQueryName = "DESPOR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione porto",;
    HelpContextID = 88387530,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=227, Top=40, InputMask=replicate('X',35)

  add object oDESSPE_3_26 as StdField with uid="MRKVVYXPSB",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESSPE", cQueryName = "DESSPE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione spedizione",;
    HelpContextID = 36810698,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=227, Top=68, InputMask=replicate('X',35)

  add object oDESPAG_3_27 as StdField with uid="OHUYBRXZDG",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione pagamento",;
    HelpContextID = 19181514,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=227, Top=96, InputMask=replicate('X',30)

  add object oCOD_3_30 as StdField with uid="CEJWQTQDJI",rtseq=85,rtrep=.f.,;
    cFormVar = "w_COD", cQueryName = "COD",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del modello",;
    HelpContextID = 58354214,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=60, Left=127, Top=12, InputMask=replicate('X',5)

  add object oDES1_3_31 as StdField with uid="KROPIZFGUF",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 61624374,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=193, Top=12, InputMask=replicate('X',35)

  add object oStr_3_2 as StdString with uid="QOSYEODVCY",Visible=.t., Left=8, Top=208,;
    Alignment=1, Width=117, Height=18,;
    Caption="Spese trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="ZRXJYCMKEU",Visible=.t., Left=8, Top=68,;
    Alignment=1, Width=117, Height=18,;
    Caption="Spedizione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="HCMHILMFWI",Visible=.t., Left=8, Top=40,;
    Alignment=1, Width=117, Height=18,;
    Caption="Porto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="VTZKHRLBGS",Visible=.t., Left=8, Top=96,;
    Alignment=1, Width=117, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="UTHNTZQURO",Visible=.t., Left=8, Top=124,;
    Alignment=1, Width=117, Height=18,;
    Caption="Magg./sconti:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="NSNREYYTZB",Visible=.t., Left=291, Top=124,;
    Alignment=1, Width=100, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="ELDYNVDBRF",Visible=.t., Left=8, Top=152,;
    Alignment=1, Width=117, Height=18,;
    Caption="Spese incasso:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="LMOXBMKJVK",Visible=.t., Left=8, Top=180,;
    Alignment=1, Width=117, Height=18,;
    Caption="Spese imballo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="MLMKFTYPHA",Visible=.t., Left=192, Top=124,;
    Alignment=2, Width=18, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_3_24 as StdString with uid="PYSXKLTAQF",Visible=.t., Left=52, Top=243,;
    Alignment=0, Width=100, Height=18,;
    Caption="Attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_29 as StdString with uid="QXMVYUKSYC",Visible=.t., Left=8, Top=12,;
    Alignment=1, Width=117, Height=18,;
    Caption="Modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsof_mam",lower(this.oContained.GSOF_MAM.class))=0
        this.oContained.GSOF_MAM.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsof_amoPag4 as StdContainer
  Width  = 683
  height = 434
  stdWidth  = 683
  stdheight = 434
  resizeXpos=431
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oMOTIPOWP_4_1 as StdCombo with uid="WRMISJFOSM",rtseq=55,rtrep=.f.,left=127,top=85,width=133,height=21;
    , ToolTipText = "Tipo Word Processor: nessuno; MS Word; Open Office Writer";
    , HelpContextID = 130312982;
    , cFormVar="w_MOTIPOWP",RowSource=""+"Nessuno,"+"MS Word,"+"Writer OpenOffice", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMOTIPOWP_4_1.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'W',;
    iif(this.value =3,'O',;
    space(1)))))
  endfunc
  func oMOTIPOWP_4_1.GetRadio()
    this.Parent.oContained.w_MOTIPOWP = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPOWP_4_1.SetRadio()
    this.Parent.oContained.w_MOTIPOWP=trim(this.Parent.oContained.w_MOTIPOWP)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPOWP=='N',1,;
      iif(this.Parent.oContained.w_MOTIPOWP=='W',2,;
      iif(this.Parent.oContained.w_MOTIPOWP=='O',3,;
      0)))
  endfunc

  add object oMOPATMOD_4_3 as StdField with uid="DBHDUZSZBE",rtseq=56,rtrep=.f.,;
    cFormVar = "w_MOPATMOD", cQueryName = "MOPATMOD",;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il percorso per il modello Word Processor non � valido",;
    ToolTipText = "Percorso modello Word Processor",;
    HelpContextID = 168023286,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=127, Top=117, InputMask=replicate('X',254)

  func oMOPATMOD_4_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPOWP<>'N')
    endwith
   endif
  endfunc

  func oMOPATMOD_4_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_MOPATMOD) OR FILE(FULLPATH(ALLTRIM(.w_MOPATMOD))))
    endwith
    return bRes
  endfunc


  add object oBtn_4_4 as StdButton with uid="GWKJEGKECT",left=488, top=117, width=24,height=22,;
    caption="...", nPag=4;
    , HelpContextID = 58255318;
  , bGlobalFont=.t.

    proc oBtn_4_4.Click()
      with this.Parent.oContained
        .w_MOPATMOD=left(iif(.w_MOTIPOWP='W',getfile('Doc,Dot','File modello '),getfile('Stw,Sxw','File modello '))+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MOTIPOWP<>'N')
      endwith
    endif
  endfunc

  add object oMONUMREP_4_6 as StdField with uid="NSEAUMCJFQ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_MONUMREP", cQueryName = "MONUMREP",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Programma di stampa associato al modello",;
    HelpContextID = 178260758,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=150, cSayPict='"999"', cGetPict='"999"', bHasZoom = .t. , cLinkFile="OUT_PUTS", cZoomOnZoom="GSUT_MOU", oKey_1_1="OUNOMPRG", oKey_1_2="this.w_PRGSTA", oKey_2_1="OUROWNUM", oKey_2_2="this.w_MONUMREP"

  func oMONUMREP_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMONUMREP_4_6.ecpDrop(oSource)
    this.Parent.oContained.link_4_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMONUMREP_4_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.OUT_PUTS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OUNOMPRG="+cp_ToStrODBC(this.Parent.oContained.w_PRGSTA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OUNOMPRG="+cp_ToStr(this.Parent.oContained.w_PRGSTA)
    endif
    do cp_zoom with 'OUT_PUTS','*','OUNOMPRG,OUROWNUM',cp_AbsName(this.parent,'oMONUMREP_4_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MOU',"Report",'',this.parent.oContained
  endproc
  proc oMONUMREP_4_6.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MOU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.OUNOMPRG=w_PRGSTA
     i_obj.w_OUROWNUM=this.parent.oContained.w_MONUMREP
     i_obj.ecpSave()
  endproc

  add object oMOPATARC_4_8 as StdField with uid="LQSWONRDNV",rtseq=58,rtrep=.f.,;
    cFormVar = "w_MOPATARC", cQueryName = "MOPATARC",;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il percorso di archiviazione non � valido",;
    ToolTipText = "Percorso archiviazione file WP/PDF",;
    HelpContextID = 100914423,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=127, Top=238, InputMask=replicate('X',254)

  func oMOPATARC_4_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPOWP<>'N')
    endwith
   endif
  endfunc

  func oMOPATARC_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_MOPATARC) OR DIRECTORY(FULLPATH(ALLTRIM(.w_MOPATARC))))
    endwith
    return bRes
  endfunc


  add object oBtn_4_9 as StdButton with uid="WPECWEHEVX",left=488, top=238, width=24,height=22,;
    caption="...", nPag=4;
    , HelpContextID = 58255318;
  , bGlobalFont=.t.

    proc oBtn_4_9.Click()
      with this.Parent.oContained
        .w_MOPATARC=left(cp_getdir(sys(5)+sys(2003),"Percorso di archiviazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MOTIPOWP<>'N')
      endwith
    endif
  endfunc


  add object oMONUMPER_4_10 as StdCombo with uid="CRKVJBJMVT",rtseq=59,rtrep=.f.,left=576,top=238,width=102,height=21;
    , ToolTipText = "Periodo archiviazione: nessuno; mese; trimestre; quadrimestre; semestre; anno";
    , HelpContextID = 144706328;
    , cFormVar="w_MONUMPER",RowSource=""+"Nessuno,"+"Mese,"+"Trimestre,"+"Quadrimestre,"+"Semestre,"+"Anno", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMONUMPER_4_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'S',;
    iif(this.value =6,'A',;
    space(1))))))))
  endfunc
  func oMONUMPER_4_10.GetRadio()
    this.Parent.oContained.w_MONUMPER = this.RadioValue()
    return .t.
  endfunc

  func oMONUMPER_4_10.SetRadio()
    this.Parent.oContained.w_MONUMPER=trim(this.Parent.oContained.w_MONUMPER)
    this.value = ;
      iif(this.Parent.oContained.w_MONUMPER=='N',1,;
      iif(this.Parent.oContained.w_MONUMPER=='M',2,;
      iif(this.Parent.oContained.w_MONUMPER=='T',3,;
      iif(this.Parent.oContained.w_MONUMPER=='Q',4,;
      iif(this.Parent.oContained.w_MONUMPER=='S',5,;
      iif(this.Parent.oContained.w_MONUMPER=='A',6,;
      0))))))
  endfunc

  func oMONUMPER_4_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPOWP<>'N')
    endwith
   endif
  endfunc

  add object oMONOMDOC_4_12 as StdField with uid="SQXISGYGQA",rtseq=60,rtrep=.f.,;
    cFormVar = "w_MONOMDOC", cQueryName = "MONOMDOC",;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Formula composizione nome documento",;
    HelpContextID = 57013495,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=127, Top=268, InputMask=replicate('X',254)

  func oMONOMDOC_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPOWP<>'N')
    endwith
   endif
  endfunc


  add object oMOTIPFOR_4_13 as StdCombo with uid="NCRZNFHAWE",rtseq=61,rtrep=.f.,left=576,top=268,width=102,height=21;
    , ToolTipText = "Formato di archiviazione: W.P.; PDF; W.P. + PDF";
    , HelpContextID = 20681960;
    , cFormVar="w_MOTIPFOR",RowSource=""+"W.P.,"+"PDF,"+"W.P. + PDF", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMOTIPFOR_4_13.RadioValue()
    return(iif(this.value =1,'W',;
    iif(this.value =2,'P',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oMOTIPFOR_4_13.GetRadio()
    this.Parent.oContained.w_MOTIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPFOR_4_13.SetRadio()
    this.Parent.oContained.w_MOTIPFOR=trim(this.Parent.oContained.w_MOTIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPFOR=='W',1,;
      iif(this.Parent.oContained.w_MOTIPFOR=='P',2,;
      iif(this.Parent.oContained.w_MOTIPFOR=='E',3,;
      0)))
  endfunc

  func oMOTIPFOR_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPOWP<>'N')
    endwith
   endif
  endfunc

  add object oDESOUT_4_18 as StdField with uid="XARHVNDHMN",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESOUT", cQueryName = "DESOUT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione report di stampa",;
    HelpContextID = 48607178,;
   bGlobalFont=.t.,;
    Height=21, Width=305, Left=177, Top=150, InputMask=replicate('X',50)

  add object oCOD_4_24 as StdField with uid="CKSMBVSHVK",rtseq=87,rtrep=.f.,;
    cFormVar = "w_COD", cQueryName = "COD",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del modello",;
    HelpContextID = 58354214,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=60, Left=127, Top=12, InputMask=replicate('X',5)

  add object oDES1_4_25 as StdField with uid="MEZIPMJMUQ",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 61624374,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=193, Top=12, InputMask=replicate('X',35)


  add object oMOALMAIL_4_26 as StdCombo with uid="SWLMOCGXDG",rtseq=98,rtrep=.f.,left=576,top=298,width=102,height=21, enabled=.f.;
    , HelpContextID = 160840466;
    , cFormVar="w_MOALMAIL",RowSource=""+"PDF standard", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMOALMAIL_4_26.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMOALMAIL_4_26.GetRadio()
    this.Parent.oContained.w_MOALMAIL = this.RadioValue()
    return .t.
  endfunc

  func oMOALMAIL_4_26.SetRadio()
    this.Parent.oContained.w_MOALMAIL=trim(this.Parent.oContained.w_MOALMAIL)
    this.value = ;
      iif(this.Parent.oContained.w_MOALMAIL=='S',1,;
      0)
  endfunc

  func oMOALMAIL_4_26.mHide()
    with this.Parent.oContained
      return (.w_MOTIPFOR<>'W')
    endwith
  endfunc


  add object oMOALMAIL_4_28 as StdCombo with uid="HXVJHJPKII",rtseq=99,rtrep=.f.,left=576,top=298,width=102,height=21;
    , ToolTipText = "File da allegare alla mail: PDF Standard=PDF del report; PDF da modello=PDF del modello Word; Entrambi=PDF del report+PDF del modello Word";
    , HelpContextID = 160840466;
    , cFormVar="w_MOALMAIL",RowSource=""+"PDF standard,"+"PDF da modello,"+"Entrambi", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oMOALMAIL_4_28.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oMOALMAIL_4_28.GetRadio()
    this.Parent.oContained.w_MOALMAIL = this.RadioValue()
    return .t.
  endfunc

  func oMOALMAIL_4_28.SetRadio()
    this.Parent.oContained.w_MOALMAIL=trim(this.Parent.oContained.w_MOALMAIL)
    this.value = ;
      iif(this.Parent.oContained.w_MOALMAIL=='S',1,;
      iif(this.Parent.oContained.w_MOALMAIL=='M',2,;
      iif(this.Parent.oContained.w_MOALMAIL=='E',3,;
      0)))
  endfunc

  func oMOALMAIL_4_28.mHide()
    with this.Parent.oContained
      return (.w_MOTIPFOR='W')
    endwith
  endfunc

  add object oStr_4_2 as StdString with uid="FBUHBHQQUV",Visible=.t., Left=5, Top=85,;
    Alignment=1, Width=120, Height=18,;
    Caption="Word Processor:"  ;
  , bGlobalFont=.t.

  add object oStr_4_5 as StdString with uid="TVVJHCXXVP",Visible=.t., Left=5, Top=117,;
    Alignment=1, Width=120, Height=18,;
    Caption="Modello W.P.:"  ;
  , bGlobalFont=.t.

  add object oStr_4_7 as StdString with uid="WRYJKOUTMJ",Visible=.t., Left=5, Top=150,;
    Alignment=1, Width=120, Height=18,;
    Caption="Report di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="MYKUOKLFQQ",Visible=.t., Left=5, Top=238,;
    Alignment=1, Width=120, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="SALAQZTTLU",Visible=.t., Left=5, Top=268,;
    Alignment=1, Width=120, Height=18,;
    Caption="Nome doc. W.P.:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="PTUNSKXHRI",Visible=.t., Left=6, Top=55,;
    Alignment=0, Width=205, Height=18,;
    Caption="Sistemi di output"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_20 as StdString with uid="NCYVIGLHVV",Visible=.t., Left=7, Top=206,;
    Alignment=0, Width=156, Height=18,;
    Caption="Archiviazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_21 as StdString with uid="GHPQUBMHKJ",Visible=.t., Left=518, Top=238,;
    Alignment=1, Width=55, Height=18,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_22 as StdString with uid="XUFOADKVTX",Visible=.t., Left=506, Top=268,;
    Alignment=1, Width=67, Height=18,;
    Caption="Formato:"  ;
  , bGlobalFont=.t.

  add object oStr_4_23 as StdString with uid="PPWLITUASK",Visible=.t., Left=37, Top=12,;
    Alignment=1, Width=88, Height=18,;
    Caption="Modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_27 as StdString with uid="JYMTORKDCM",Visible=.t., Left=503, Top=298,;
    Alignment=1, Width=70, Height=18,;
    Caption="Allegati mail:"  ;
  , bGlobalFont=.t.

  add object oBox_4_15 as StdBox with uid="NSWOBYKGUX",left=3, top=72, width=659,height=2

  add object oBox_4_19 as StdBox with uid="PZCEUDPKIN",left=4, top=223, width=659,height=2
enddefine
define class tgsof_amoPag5 as StdContainer
  Width  = 683
  height = 434
  stdWidth  = 683
  stdheight = 434
  resizeXpos=466
  resizeYpos=314
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_5_1 as StdButton with uid="WWPCQQOGTJ",left=612, top=24, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per ricercare i documenti allegati";
    , HelpContextID = 33458922;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_1.Click()
      with this.Parent.oContained
        .Notifyevent('Calcola')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomAll as cp_zoombox with uid="UVDGIFWGIJ",left=24, top=114, width=639,height=274,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSOF_AMO",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="ALL_EGAT",;
    cEvent = "Calcola,Load",;
    nPag=5;
    , HelpContextID = 32383514


  add object oBtn_5_3 as StdButton with uid="NFTYKEFVSA",left=612, top=391, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per associare un nuovo allegato";
    , HelpContextID = 182665430;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_3.Click()
      with this.Parent.oContained
        GSOF_BGM(this.Parent.oContained,"CARICA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_5_4 as StdButton with uid="OEXSBFSQXC",left=25, top=391, width=48,height=45,;
    CpPicture="bmp\VISUALI.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per aprire il documento allegato";
    , HelpContextID = 65432326;
    , Caption='\<Apri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_4.Click()
      with this.Parent.oContained
        GSOF_BGM(this.Parent.oContained,"APRI_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc


  add object oBtn_5_5 as StdButton with uid="SQPKZTQSOU",left=76, top=391, width=48,height=45,;
    CpPicture="BMP\MODIFICA.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per modificare i riferimenti del documento allegato";
    , HelpContextID = 54889255;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_5.Click()
      with this.Parent.oContained
        GSOF_BGM(this.Parent.oContained,"VARIA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc


  add object oBtn_5_6 as StdButton with uid="KMDXFIKYVW",left=127, top=391, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per cancellare i riferimenti del documento allegato";
    , HelpContextID = 126232762;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_6.Click()
      with this.Parent.oContained
        GSOF_BGM(this.Parent.oContained,"ELIMINA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc

  add object oNOMFIL_5_11 as StdField with uid="NJMFEJYBNS",rtseq=69,rtrep=.f.,;
    cFormVar = "w_NOMFIL", cQueryName = "NOMFIL",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca link del file",;
    HelpContextID = 196019498,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=128, Top=47, InputMask=replicate('X',30)

  add object oOGGETT_5_12 as StdField with uid="ZULEMOOYTM",rtseq=70,rtrep=.f.,;
    cFormVar = "w_OGGETT", cQueryName = "OGGETT",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca oggetto allegato",;
    HelpContextID = 50359578,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=128, Top=79, InputMask=replicate('X',30)

  add object oNOTE_5_13 as StdField with uid="PWLNYTXMPQ",rtseq=71,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca note allegato",;
    HelpContextID = 62941910,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=432, Top=79, InputMask=replicate('X',30)

  add object oCOD_5_16 as StdField with uid="WVPOYBKTPG",rtseq=89,rtrep=.f.,;
    cFormVar = "w_COD", cQueryName = "COD",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del modello",;
    HelpContextID = 58354214,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=60, Left=128, Top=12, InputMask=replicate('X',5)

  add object oDES1_5_17 as StdField with uid="FKAXGFXLTL",rtseq=90,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 61624374,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=194, Top=12, InputMask=replicate('X',35)

  add object oStr_5_7 as StdString with uid="FXVGAXWYFQ",Visible=.t., Left=38, Top=47,;
    Alignment=1, Width=88, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.

  add object oStr_5_8 as StdString with uid="XRKKMGDKHL",Visible=.t., Left=38, Top=79,;
    Alignment=1, Width=88, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_5_9 as StdString with uid="HNPBTANUPR",Visible=.t., Left=355, Top=79,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_5_15 as StdString with uid="ZGQWZBVQLC",Visible=.t., Left=38, Top=12,;
    Alignment=1, Width=88, Height=18,;
    Caption="Modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_amo','MOD_OFFE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MOCODICE=MOD_OFFE.MOCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
