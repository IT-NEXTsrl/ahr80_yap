* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bk2                                                        *
*              Conferma articoli provvisori                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-14                                                      *
* Last revis.: 2003-02-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bk2",oParentObject)
return(i_retval)

define class tgsof_bk2 as StdBatch
  * --- Local variables
  w_PTSA = .NULL.
  w_NURIGA = 0
  w_CODICE = space(20)
  w_CODART = space(20)
  w_DESART = space(40)
  w_TIPRIG = space(1)
  w_PADRE = .NULL.
  * --- WorkFile variables
  OFF_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conferma Articoli Provvisori (da GSOF_MDA)
    * --- Puntatore all'Oggetto Conferma Articoli (GSOF_MDC)
    this.w_PTSA = this.oParentObject.w_OGG
    * --- Puntatore all'oggetto dettaglio delle offerte
    this.w_PADRE = This.oParentObject
    SELECT ( this.w_PTSA.cTrsName )
    GO TOP
    SCAN FOR t_CPROWORD<>0 AND NOT EMPTY(t_ODCODICE) AND NOT EMPTY(t_ODUNIMIS) AND t_NURIGA<>0
    this.w_NURIGA = t_NURIGA
    this.w_CODICE = t_ODCODICE
    this.w_CODART = t_ODCODART
    this.w_DESART = t_ODDESART
    this.w_TIPRIG = t_ODTIPRIG
    * --- Ricerco la riga sul temporaneo di origine
    *     (l'utente pu� inserire nuove righe)
    SELECT ( this.w_PADRE.cTrsName)
    LOCATE FOR CPROWNUM=this.w_NURIGA
    if FOUND()
      * --- Write into OFF_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ODCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'OFF_DETT','ODCODICE');
        +",ODCODART ="+cp_NullLink(cp_ToStrODBC(this.w_CODART),'OFF_DETT','ODCODART');
        +",ODDESART ="+cp_NullLink(cp_ToStrODBC(this.w_DESART),'OFF_DETT','ODDESART');
        +",ODARNOCO ="+cp_NullLink(cp_ToStrODBC(" "),'OFF_DETT','ODARNOCO');
        +",ODTIPRIG ="+cp_NullLink(cp_ToStrODBC(this.w_TIPRIG),'OFF_DETT','ODTIPRIG');
            +i_ccchkf ;
        +" where ";
            +"ODSERIAL = "+cp_ToStrODBC(this.oParentObject.w_ODSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_NURIGA);
               )
      else
        update (i_cTable) set;
            ODCODICE = this.w_CODICE;
            ,ODCODART = this.w_CODART;
            ,ODDESART = this.w_DESART;
            ,ODARNOCO = " ";
            ,ODTIPRIG = this.w_TIPRIG;
            &i_ccchkf. ;
         where;
            ODSERIAL = this.oParentObject.w_ODSERIAL;
            and CPROWNUM = this.w_NURIGA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    SELECT ( this.w_PTSA.cTrsName )
    ENDSCAN
    this.w_PTSA.cFunction = "Query"
    this.w_PTSA.ecpQuit()     
    * --- Ricarico l'offerta x aggiornare i dati del dettaglio...
    this.w_PADRE.oParentObject.LoadRec()     
    * --- Lancio la generazione documento
    this.w_PADRE.oParentObject.NotifyEvent("Aggiorna2")     
    this.bUpdateParentObject=.f.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
