* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bkm                                                        *
*              Check modelli offerte                                           *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_180]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2013-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bkm",oParentObject)
return(i_retval)

define class tgsof_bkm as StdBatch
  * --- Local variables
  w_RECTRS1 = 0
  w_POFLCAAR = space(1)
  * --- WorkFile variables
  PAR_OFFE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli alla Conferma Modelli Offerte (da GSOF_AMO)
    this.oParentObject.w_BRES = .T.
    if this.oParentObject.w_MOSEZVAR = "S"
      SELECT (this.oParentObject.gsof_msm.cTrsName) 
      this.w_RECTRS1 = 0
      SCAN FOR NOT DELETED()
      this.w_RECTRS1 = this.w_RECTRS1 + 1
      endscan
      if this.w_RECTRS1=1
        GOTO TOP
        this.w_RECTRS1 = IIF(EMPTY(NVL(t_MSCODSEZ," ")), 0, 1)
      endif
      if this.w_RECTRS1 = 0
        ah_errormsg("Sezioni a struttura rigida senza inserimento sezioni")
        this.oParentObject.w_BRES = .F.
      endif
    endif
    * --- Se non c'� errore prima, verifico il percorso di archiviazione e la consistenza del modello W.P.
    if this.oParentObject.w_BRES
      * --- Read from PAR_OFFE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "POFLCAAR"+;
          " from "+i_cTable+" PAR_OFFE where ";
              +"POCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          POFLCAAR;
          from (i_cTable) where;
              POCODAZI = this.oParentObject.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_POFLCAAR = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if (this.w_POFLCAAR="S" AND EMPTY(this.oParentObject.w_MOPATARC) AND this.oParentObject.w_MOTIPOWP <> "N") OR NOT DIRECTORY(FULLPATH(ALLTRIM(this.oParentObject.w_MOPATARC)))
        ah_errormsg("Se nei parametri offerta � specificata l'archiviazione per nominativo � obbligatorio specificare un percorso di archiviazione valido")
        this.oParentObject.w_BRES = .F.
      endif
      if (this.oParentObject.w_MOTIPOWP="W" AND inlist(LOWER(JUSTEXT(this.oParentObject.w_MOPATMOD)),"stw","sxw")) OR (this.oParentObject.w_MOTIPOWP="O" AND inlist(LOWER(JUSTEXT(this.oParentObject.w_MOPATMOD)),"dot","doc")) OR (this.oParentObject.w_MOTIPOWP="N" AND not EMPTY(ALLTRIM(this.oParentObject.w_MOPATMOD)))
        ah_errormsg("Modello W.P. incongruente con Word Processor impostato. Verificare la congruenza del dato inserito")
        this.oParentObject.w_BRES = .F.
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_OFFE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
