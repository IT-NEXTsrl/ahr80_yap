* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_sbo                                                        *
*              Stampa offerte                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-11                                                      *
* Last revis.: 2014-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_sbo",oParentObject))

* --- Class definition
define class tgsof_sbo as StdForm
  Top    = 51
  Left   = 81

  * --- Standard Properties
  Width  = 568
  Height = 358+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-19"
  HelpContextID=160814743
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=79

  * --- Constant Properties
  _IDX = 0
  MOD_OFFE_IDX = 0
  CAT_ATTR_IDX = 0
  OFF_NOMI_IDX = 0
  GRU_NOMI_IDX = 0
  ZONE_IDX = 0
  ORI_NOMI_IDX = 0
  AGENTI_IDX = 0
  GRU_PRIO_IDX = 0
  cpusers_IDX = 0
  TIT_SEZI_IDX = 0
  cPrg = "gsof_sbo"
  cComment = "Stampa offerte"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_BDATE = ctod('  /  /  ')
  w_EDATE = ctod('  /  /  ')
  w_NUMERO = 0
  w_NUMERO1 = 0
  w_SERIE1 = space(2)
  w_SERIE2 = space(2)
  w_SOSTATUS = space(1)
  w_SOCODGPR = space(5)
  w_DATA11 = ctod('  /  /  ')
  w_DATA21 = ctod('  /  /  ')
  w_DATA13 = ctod('  /  /  ')
  w_DATA23 = ctod('  /  /  ')
  w_DATA12 = ctod('  /  /  ')
  w_DATA22 = ctod('  /  /  ')
  w_DATA14 = ctod('  /  /  ')
  w_DATA24 = ctod('  /  /  ')
  w_SOCODMOD = space(5)
  w_SOTIPNOM = space(1)
  o_SOTIPNOM = space(1)
  w_SOCODNOM = space(20)
  w_SOCODGRU = space(5)
  w_SOCODPRI = space(6)
  w_SOCODORI = space(5)
  w_SOCONTAT = space(40)
  w_SOCODUTE = 0
  w_SOCODAGE = space(5)
  w_SOCODZON = space(3)
  w_SOCODAT0 = space(10)
  o_SOCODAT0 = space(10)
  w_SOMODESC = space(35)
  w_DESATT0 = space(35)
  w_SOCODAT1 = space(10)
  o_SOCODAT1 = space(10)
  w_DESATT1 = space(35)
  w_SOCODAT2 = space(10)
  o_SOCODAT2 = space(10)
  w_DESATT2 = space(35)
  w_SOCODAT3 = space(10)
  o_SOCODAT3 = space(10)
  w_DESATT3 = space(35)
  w_SOCODAT4 = space(10)
  o_SOCODAT4 = space(10)
  w_DESATT4 = space(35)
  w_SOCODAT5 = space(10)
  o_SOCODAT5 = space(10)
  w_DESATT5 = space(35)
  w_SOCODAT6 = space(10)
  o_SOCODAT6 = space(10)
  w_DESATT6 = space(35)
  w_SOCODAT7 = space(10)
  o_SOCODAT7 = space(10)
  w_DESATT7 = space(35)
  w_SOCODAT8 = space(10)
  o_SOCODAT8 = space(10)
  w_DESATT8 = space(35)
  w_SOCODAT9 = space(10)
  w_DESATT9 = space(35)
  w_SODESNOM = space(40)
  w_DESCGN = space(35)
  w_DESZONE = space(35)
  w_DESORI = space(35)
  w_DESAGE = space(35)
  w_DESPRIO = space(35)
  w_DESOPE = space(35)
  w_DESGPR = space(35)
  w_DTOBAGE = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_SODANUME = 0
  w_SOANUMER = 0
  w_SODANUPR = 0
  w_SOANUMPR = 0
  w_SOSTATUS1 = space(1)
  w_TIPNOM = space(1)
  w_SOTIPNOM1 = space(1)
  w_NUMERO = 0
  w_NUMERO1 = 0
  w_SERIE1 = space(10)
  w_SERIE2 = space(10)
  w_SOSTATUS2 = space(1)
  w_SELORDBY = space(30)
  w_OFCODAGE = space(5)
  w_AGDESAGE = space(35)
  w_DTOBAGEO = ctod('  /  /  ')
  w_OFCODAG2 = space(5)
  w_AGDESAG2 = space(35)
  w_DTOBAGE2 = ctod('  /  /  ')
  w_AGTIPAGE = space(1)
  w_ORDERBY = space(30)
  w_BGARBAGE = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_sboPag1","gsof_sbo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsof_sboPag2","gsof_sbo",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri aggiuntivi")
      .Pages(3).addobject("oPag","tgsof_sboPag3","gsof_sbo",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Attributi")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oBDATE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='MOD_OFFE'
    this.cWorkTables[2]='CAT_ATTR'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='GRU_NOMI'
    this.cWorkTables[5]='ZONE'
    this.cWorkTables[6]='ORI_NOMI'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='GRU_PRIO'
    this.cWorkTables[9]='cpusers'
    this.cWorkTables[10]='TIT_SEZI'
    return(this.OpenAllTables(10))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsof_sbo
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_BDATE=ctod("  /  /  ")
      .w_EDATE=ctod("  /  /  ")
      .w_NUMERO=0
      .w_NUMERO1=0
      .w_SERIE1=space(2)
      .w_SERIE2=space(2)
      .w_SOSTATUS=space(1)
      .w_SOCODGPR=space(5)
      .w_DATA11=ctod("  /  /  ")
      .w_DATA21=ctod("  /  /  ")
      .w_DATA13=ctod("  /  /  ")
      .w_DATA23=ctod("  /  /  ")
      .w_DATA12=ctod("  /  /  ")
      .w_DATA22=ctod("  /  /  ")
      .w_DATA14=ctod("  /  /  ")
      .w_DATA24=ctod("  /  /  ")
      .w_SOCODMOD=space(5)
      .w_SOTIPNOM=space(1)
      .w_SOCODNOM=space(20)
      .w_SOCODGRU=space(5)
      .w_SOCODPRI=space(6)
      .w_SOCODORI=space(5)
      .w_SOCONTAT=space(40)
      .w_SOCODUTE=0
      .w_SOCODAGE=space(5)
      .w_SOCODZON=space(3)
      .w_SOCODAT0=space(10)
      .w_SOMODESC=space(35)
      .w_DESATT0=space(35)
      .w_SOCODAT1=space(10)
      .w_DESATT1=space(35)
      .w_SOCODAT2=space(10)
      .w_DESATT2=space(35)
      .w_SOCODAT3=space(10)
      .w_DESATT3=space(35)
      .w_SOCODAT4=space(10)
      .w_DESATT4=space(35)
      .w_SOCODAT5=space(10)
      .w_DESATT5=space(35)
      .w_SOCODAT6=space(10)
      .w_DESATT6=space(35)
      .w_SOCODAT7=space(10)
      .w_DESATT7=space(35)
      .w_SOCODAT8=space(10)
      .w_DESATT8=space(35)
      .w_SOCODAT9=space(10)
      .w_DESATT9=space(35)
      .w_SODESNOM=space(40)
      .w_DESCGN=space(35)
      .w_DESZONE=space(35)
      .w_DESORI=space(35)
      .w_DESAGE=space(35)
      .w_DESPRIO=space(35)
      .w_DESOPE=space(35)
      .w_DESGPR=space(35)
      .w_DTOBAGE=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_SODANUME=0
      .w_SOANUMER=0
      .w_SODANUPR=0
      .w_SOANUMPR=0
      .w_SOSTATUS1=space(1)
      .w_TIPNOM=space(1)
      .w_SOTIPNOM1=space(1)
      .w_NUMERO=0
      .w_NUMERO1=0
      .w_SERIE1=space(10)
      .w_SERIE2=space(10)
      .w_SOSTATUS2=space(1)
      .w_SELORDBY=space(30)
      .w_OFCODAGE=space(5)
      .w_AGDESAGE=space(35)
      .w_DTOBAGEO=ctod("  /  /  ")
      .w_OFCODAG2=space(5)
      .w_AGDESAG2=space(35)
      .w_DTOBAGE2=ctod("  /  /  ")
      .w_AGTIPAGE=space(1)
      .w_ORDERBY=space(30)
      .w_BGARBAGE=.f.
        .w_BDATE = G_INIESE
        .w_EDATE = G_FINESE
        .w_NUMERO = 1
        .w_NUMERO1 = 999999
        .w_SERIE1 = ''
        .w_SERIE2 = ''
        .w_SOSTATUS = 'T'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SOCODGPR))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,17,.f.)
        if not(empty(.w_SOCODMOD))
          .link_2_1('Full')
        endif
        .w_SOTIPNOM = 'A'
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_SOCODNOM))
          .link_2_3('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_SOCODGRU))
          .link_2_4('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_SOCODPRI))
          .link_2_5('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_SOCODORI))
          .link_2_6('Full')
        endif
          .DoRTCalc(23,23,.f.)
        .w_SOCODUTE = i_CODUTE
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_SOCODUTE))
          .link_2_8('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_SOCODAGE))
          .link_2_9('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_SOCODZON))
          .link_2_10('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_SOCODAT0))
          .link_3_1('Full')
        endif
          .DoRTCalc(28,29,.f.)
        .w_SOCODAT1 = SPACE(10)
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_SOCODAT1))
          .link_3_3('Full')
        endif
          .DoRTCalc(31,31,.f.)
        .w_SOCODAT2 = SPACE(10)
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_SOCODAT2))
          .link_3_5('Full')
        endif
          .DoRTCalc(33,33,.f.)
        .w_SOCODAT3 = SPACE(10)
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_SOCODAT3))
          .link_3_7('Full')
        endif
          .DoRTCalc(35,35,.f.)
        .w_SOCODAT4 = SPACE(10)
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_SOCODAT4))
          .link_3_9('Full')
        endif
          .DoRTCalc(37,37,.f.)
        .w_SOCODAT5 = SPACE(10)
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_SOCODAT5))
          .link_3_11('Full')
        endif
          .DoRTCalc(39,39,.f.)
        .w_SOCODAT6 = SPACE(10)
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_SOCODAT6))
          .link_3_13('Full')
        endif
          .DoRTCalc(41,41,.f.)
        .w_SOCODAT7 = SPACE(10)
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_SOCODAT7))
          .link_3_15('Full')
        endif
          .DoRTCalc(43,43,.f.)
        .w_SOCODAT8 = SPACE(10)
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_SOCODAT8))
          .link_3_17('Full')
        endif
          .DoRTCalc(45,45,.f.)
        .w_SOCODAT9 = SPACE(10)
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_SOCODAT9))
          .link_3_19('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
          .DoRTCalc(47,56,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(58,61,.f.)
        .w_SOSTATUS1 = IIF(.w_SOSTATUS='T', '', IIF(.w_SOSTATUS='X', 'I', .w_SOSTATUS))
        .w_TIPNOM = .w_SOTIPNOM
        .w_SOTIPNOM1 = IIF(.w_SOTIPNOM='A', ' ', .w_SOTIPNOM)
        .w_NUMERO = 1
        .w_NUMERO1 = IIF(upper(g_APPLICATION) = "AD HOC ENTERPRISE", 999999 ,999999999999999)
        .w_SERIE1 = ''
        .w_SERIE2 = ''
        .w_SOSTATUS2 = IIF(.w_SOSTATUS='T', '', IIF(.w_SOSTATUS='X', 'A', .w_SOSTATUS))
        .w_SELORDBY = ""
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_OFCODAGE))
          .link_2_37('Full')
        endif
        .DoRTCalc(72,74,.f.)
        if not(empty(.w_OFCODAG2))
          .link_2_41('Full')
        endif
    endwith
    this.DoRTCalc(75,79,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,29,.t.)
        if .o_SOCODAT0<>.w_SOCODAT0
            .w_SOCODAT1 = SPACE(10)
          .link_3_3('Full')
        endif
        .DoRTCalc(31,31,.t.)
        if .o_SOCODAT1<>.w_SOCODAT1
            .w_SOCODAT2 = SPACE(10)
          .link_3_5('Full')
        endif
        .DoRTCalc(33,33,.t.)
        if .o_SOCODAT2<>.w_SOCODAT2
            .w_SOCODAT3 = SPACE(10)
          .link_3_7('Full')
        endif
        .DoRTCalc(35,35,.t.)
        if .o_SOCODAT3<>.w_SOCODAT3
            .w_SOCODAT4 = SPACE(10)
          .link_3_9('Full')
        endif
        .DoRTCalc(37,37,.t.)
        if .o_SOCODAT4<>.w_SOCODAT4
            .w_SOCODAT5 = SPACE(10)
          .link_3_11('Full')
        endif
        .DoRTCalc(39,39,.t.)
        if .o_SOCODAT5<>.w_SOCODAT5
            .w_SOCODAT6 = SPACE(10)
          .link_3_13('Full')
        endif
        .DoRTCalc(41,41,.t.)
        if .o_SOCODAT6<>.w_SOCODAT6
            .w_SOCODAT7 = SPACE(10)
          .link_3_15('Full')
        endif
        .DoRTCalc(43,43,.t.)
        if .o_SOCODAT7<>.w_SOCODAT7
            .w_SOCODAT8 = SPACE(10)
          .link_3_17('Full')
        endif
        .DoRTCalc(45,45,.t.)
        if .o_SOCODAT8<>.w_SOCODAT8
            .w_SOCODAT9 = SPACE(10)
          .link_3_19('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .DoRTCalc(47,61,.t.)
            .w_SOSTATUS1 = IIF(.w_SOSTATUS='T', '', IIF(.w_SOSTATUS='X', 'I', .w_SOSTATUS))
            .w_TIPNOM = .w_SOTIPNOM
        if .o_SOTIPNOM<>.w_SOTIPNOM
            .w_SOTIPNOM1 = IIF(.w_SOTIPNOM='A', ' ', .w_SOTIPNOM)
        endif
        .DoRTCalc(65,68,.t.)
            .w_SOSTATUS2 = IIF(.w_SOSTATUS='T', '', IIF(.w_SOSTATUS='X', 'A', .w_SOSTATUS))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(70,79,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .cComment = IIF(Isalt(),  AH_MSGFORMAT("Stampa preventivi"),AH_MSGFORMAT("Stampa offerte"))
          .Caption = .cComment
    endwith
  endproc
  proc Calculate_YDESMCZPQN()
    with this
          * --- Lancio stampa
          .w_ORDERBY = .w_SELORDBY + IIF(EMPTY(.w_SELORDBY), "", ", ") + "OFF_ERTE.OFSERIAL"
          .w_BGARBAGE = vx_exec(alltrim(.w_OQRY)+","+alltrim(.w_orep),this)
          .w_ORDERBY = ""
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNUMERO_1_3.enabled = this.oPgFrm.Page1.oPag.oNUMERO_1_3.mCond()
    this.oPgFrm.Page1.oPag.oNUMERO1_1_4.enabled = this.oPgFrm.Page1.oPag.oNUMERO1_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSERIE1_1_5.enabled = this.oPgFrm.Page1.oPag.oSERIE1_1_5.mCond()
    this.oPgFrm.Page1.oPag.oSERIE2_1_6.enabled = this.oPgFrm.Page1.oPag.oSERIE2_1_6.mCond()
    this.oPgFrm.Page3.oPag.oSOCODAT1_3_3.enabled = this.oPgFrm.Page3.oPag.oSOCODAT1_3_3.mCond()
    this.oPgFrm.Page3.oPag.oSOCODAT2_3_5.enabled = this.oPgFrm.Page3.oPag.oSOCODAT2_3_5.mCond()
    this.oPgFrm.Page3.oPag.oSOCODAT3_3_7.enabled = this.oPgFrm.Page3.oPag.oSOCODAT3_3_7.mCond()
    this.oPgFrm.Page3.oPag.oSOCODAT4_3_9.enabled = this.oPgFrm.Page3.oPag.oSOCODAT4_3_9.mCond()
    this.oPgFrm.Page3.oPag.oSOCODAT5_3_11.enabled = this.oPgFrm.Page3.oPag.oSOCODAT5_3_11.mCond()
    this.oPgFrm.Page3.oPag.oSOCODAT6_3_13.enabled = this.oPgFrm.Page3.oPag.oSOCODAT6_3_13.mCond()
    this.oPgFrm.Page3.oPag.oSOCODAT7_3_15.enabled = this.oPgFrm.Page3.oPag.oSOCODAT7_3_15.mCond()
    this.oPgFrm.Page3.oPag.oSOCODAT8_3_17.enabled = this.oPgFrm.Page3.oPag.oSOCODAT8_3_17.mCond()
    this.oPgFrm.Page3.oPag.oSOCODAT9_3_19.enabled = this.oPgFrm.Page3.oPag.oSOCODAT9_3_19.mCond()
    this.oPgFrm.Page1.oPag.oNUMERO_1_41.enabled = this.oPgFrm.Page1.oPag.oNUMERO_1_41.mCond()
    this.oPgFrm.Page1.oPag.oNUMERO1_1_42.enabled = this.oPgFrm.Page1.oPag.oNUMERO1_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSERIE1_1_43.enabled = this.oPgFrm.Page1.oPag.oSERIE1_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSERIE2_1_44.enabled = this.oPgFrm.Page1.oPag.oSERIE2_1_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(Isalt())
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Attributi"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oNUMERO_1_3.visible=!this.oPgFrm.Page1.oPag.oNUMERO_1_3.mHide()
    this.oPgFrm.Page1.oPag.oNUMERO1_1_4.visible=!this.oPgFrm.Page1.oPag.oNUMERO1_1_4.mHide()
    this.oPgFrm.Page1.oPag.oSERIE1_1_5.visible=!this.oPgFrm.Page1.oPag.oSERIE1_1_5.mHide()
    this.oPgFrm.Page1.oPag.oSERIE2_1_6.visible=!this.oPgFrm.Page1.oPag.oSERIE2_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oNUMERO_1_41.visible=!this.oPgFrm.Page1.oPag.oNUMERO_1_41.mHide()
    this.oPgFrm.Page1.oPag.oNUMERO1_1_42.visible=!this.oPgFrm.Page1.oPag.oNUMERO1_1_42.mHide()
    this.oPgFrm.Page1.oPag.oSERIE1_1_43.visible=!this.oPgFrm.Page1.oPag.oSERIE1_1_43.mHide()
    this.oPgFrm.Page1.oPag.oSERIE2_1_44.visible=!this.oPgFrm.Page1.oPag.oSERIE2_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Stampa")
          .Calculate_YDESMCZPQN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SOCODGPR
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODGPR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AGP',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_SOCODGPR)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_SOCODGPR))
          select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODGPR)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODGPR) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oSOCODGPR_1_8'),i_cWhere,'GSOF_AGP',"Gruppi priorit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODGPR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_SOCODGPR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_SOCODGPR)
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODGPR = NVL(_Link_.GPCODICE,space(5))
      this.W_DESGPR = NVL(_Link_.GPDESCRI,space(35))
      this.w_SODANUME = NVL(_Link_.GPNUMINI,0)
      this.w_SOANUMER = NVL(_Link_.GPNUMFIN,0)
    else
      if i_cCtrl<>'Load'
        this.w_SOCODGPR = space(5)
      endif
      this.W_DESGPR = space(35)
      this.w_SODANUME = 0
      this.w_SOANUMER = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODGPR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODMOD
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_lTable = "MOD_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2], .t., this.MOD_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MOD_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_SOCODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_SOCODMOD))
          select MOCODICE,MODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODMOD) and !this.bDontReportError
            deferred_cp_zoom('MOD_OFFE','*','MOCODICE',cp_AbsName(oSource.parent,'oSOCODMOD_2_1'),i_cWhere,'',"Modelli offerte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_SOCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_SOCODMOD)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODMOD = NVL(_Link_.MOCODICE,space(5))
      this.w_SOMODESC = NVL(_Link_.MODESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODMOD = space(5)
      endif
      this.w_SOMODESC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODNOM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_SOCODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_SOCODNOM))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oSOCODNOM_2_3'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF2SNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_SOCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_SOCODNOM)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_SODESNOM = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODNOM = space(20)
      endif
      this.w_SODESNOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODGRU
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_SOCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_SOCODGRU))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODGRU)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oSOCODGRU_2_4'),i_cWhere,'',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_SOCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_SOCODGRU)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODGRU = NVL(_Link_.GNCODICE,space(5))
      this.w_DESCGN = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODGRU = space(5)
      endif
      this.w_DESCGN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODPRI
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODPRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_SOCODPRI)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_SOCODPRI))
          select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODPRI)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODPRI) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oSOCODPRI_2_5'),i_cWhere,'',"Gruppi priorit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODPRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_SOCODPRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_SOCODPRI)
            select GPCODICE,GPDESCRI,GPNUMINI,GPNUMFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODPRI = NVL(_Link_.GPCODICE,space(6))
      this.w_DESPRIO = NVL(_Link_.GPDESCRI,space(35))
      this.w_SODANUPR = NVL(_Link_.GPNUMINI,0)
      this.w_SOANUMPR = NVL(_Link_.GPNUMFIN,0)
    else
      if i_cCtrl<>'Load'
        this.w_SOCODPRI = space(6)
      endif
      this.w_DESPRIO = space(35)
      this.w_SODANUPR = 0
      this.w_SOANUMPR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODPRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODORI
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_SOCODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_SOCODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oSOCODORI_2_6'),i_cWhere,'',"Codici origine nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_SOCODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_SOCODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_DESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODORI = space(5)
      endif
      this.w_DESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODUTE
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_SOCODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_SOCODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_SOCODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oSOCODUTE_2_8'),i_cWhere,'',"Operatori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_SOCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_SOCODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODUTE = NVL(_Link_.code,0)
      this.w_DESOPE = NVL(_Link_.name,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODUTE = 0
      endif
      this.w_DESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAGE
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_SOCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_SOCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_SOCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_SOCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oSOCODAGE_2_9'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_SOCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_SOCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DTOBAGE = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DTOBAGE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!"), CHKDTOBS(.w_DTOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_SOCODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_DTOBAGE = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODZON
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_SOCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_SOCODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oSOCODZON_2_10'),i_cWhere,'',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_SOCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_SOCODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZONE = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODZON = space(3)
      endif
      this.w_DESZONE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT0
  func Link_3_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT0) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT0)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT0))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT0)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT0) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT0_3_1'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT0)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT0);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT0)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT0 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT0 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT0 = space(10)
      endif
      this.w_DESATT0 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT0 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT1
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT1))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT1)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT1) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT1_3_3'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT1)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT1 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT1 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT1 = space(10)
      endif
      this.w_DESATT1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT2
  func Link_3_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT2))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT2)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT2) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT2_3_5'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT2)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT2 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT2 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT2 = space(10)
      endif
      this.w_DESATT2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT3
  func Link_3_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT3))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT3)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT3) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT3_3_7'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT3)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT3 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT3 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT3 = space(10)
      endif
      this.w_DESATT3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT4
  func Link_3_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT4)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT4))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT4)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT4) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT4_3_9'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT4)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT4 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT4 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT4 = space(10)
      endif
      this.w_DESATT4 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT5
  func Link_3_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT5)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT5))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT5)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT5) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT5_3_11'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT5)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT5 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT5 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT5 = space(10)
      endif
      this.w_DESATT5 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT6
  func Link_3_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT6)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT6))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT6)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT6) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT6_3_13'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT6)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT6 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT6 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT6 = space(10)
      endif
      this.w_DESATT6 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT7
  func Link_3_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT7)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT7))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT7)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT7) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT7_3_15'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT7)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT7 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT7 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT7 = space(10)
      endif
      this.w_DESATT7 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT8
  func Link_3_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT8)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT8))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT8)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT8) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT8_3_17'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT8)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT8 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT8 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT8 = space(10)
      endif
      this.w_DESATT8 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SOCODAT9
  func Link_3_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SOCODAT9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_SOCODAT9)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_SOCODAT9))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SOCODAT9)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SOCODAT9) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oSOCODAT9_3_19'),i_cWhere,'',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SOCODAT9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_SOCODAT9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_SOCODAT9)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SOCODAT9 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT9 = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SOCODAT9 = space(10)
      endif
      this.w_DESATT9 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SOCODAT9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFCODAGE
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_OFCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_OFCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_OFCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_OFCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oOFCODAGE_2_37'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_OFCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_OFCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_AGDESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DTOBAGEO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODAGE = space(5)
      endif
      this.w_AGDESAGE = space(35)
      this.w_DTOBAGEO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBAGEO,.w_OBTEST,"Agente obsoleto alla data Attuale!"), CHKDTOBS(.w_DTOBAGEO,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OFCODAGE = space(5)
        this.w_AGDESAGE = space(35)
        this.w_DTOBAGEO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFCODAG2
  func Link_2_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODAG2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_OFCODAG2)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_OFCODAG2))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODAG2)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_OFCODAG2)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_OFCODAG2)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFCODAG2) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oOFCODAG2_2_41'),i_cWhere,'',"Agenti",'GSVE1KFT.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODAG2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_OFCODAG2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_OFCODAG2)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODAG2 = NVL(_Link_.AGCODAGE,space(5))
      this.w_AGDESAG2 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DTOBAGE2 = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_AGTIPAGE = NVL(_Link_.AGTIPAGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODAG2 = space(5)
      endif
      this.w_AGDESAG2 = space(35)
      this.w_DTOBAGE2 = ctod("  /  /  ")
      this.w_AGTIPAGE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_AGTIPAGE='C' AND IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBAGE2,.w_OBTEST,"Agente obsoleto alla data Attuale!"), CHKDTOBS(.w_DTOBAGE2,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'agente selezionato non di tipo capoarea")
        endif
        this.w_OFCODAG2 = space(5)
        this.w_AGDESAG2 = space(35)
        this.w_DTOBAGE2 = ctod("  /  /  ")
        this.w_AGTIPAGE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODAG2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oBDATE_1_1.value==this.w_BDATE)
      this.oPgFrm.Page1.oPag.oBDATE_1_1.value=this.w_BDATE
    endif
    if not(this.oPgFrm.Page1.oPag.oEDATE_1_2.value==this.w_EDATE)
      this.oPgFrm.Page1.oPag.oEDATE_1_2.value=this.w_EDATE
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_3.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_3.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO1_1_4.value==this.w_NUMERO1)
      this.oPgFrm.Page1.oPag.oNUMERO1_1_4.value=this.w_NUMERO1
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE1_1_5.value==this.w_SERIE1)
      this.oPgFrm.Page1.oPag.oSERIE1_1_5.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE2_1_6.value==this.w_SERIE2)
      this.oPgFrm.Page1.oPag.oSERIE2_1_6.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page1.oPag.oSOSTATUS_1_7.RadioValue()==this.w_SOSTATUS)
      this.oPgFrm.Page1.oPag.oSOSTATUS_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSOCODGPR_1_8.value==this.w_SOCODGPR)
      this.oPgFrm.Page1.oPag.oSOCODGPR_1_8.value=this.w_SOCODGPR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA11_1_15.value==this.w_DATA11)
      this.oPgFrm.Page1.oPag.oDATA11_1_15.value=this.w_DATA11
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA21_1_16.value==this.w_DATA21)
      this.oPgFrm.Page1.oPag.oDATA21_1_16.value=this.w_DATA21
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA13_1_19.value==this.w_DATA13)
      this.oPgFrm.Page1.oPag.oDATA13_1_19.value=this.w_DATA13
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA23_1_20.value==this.w_DATA23)
      this.oPgFrm.Page1.oPag.oDATA23_1_20.value=this.w_DATA23
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA12_1_23.value==this.w_DATA12)
      this.oPgFrm.Page1.oPag.oDATA12_1_23.value=this.w_DATA12
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA22_1_24.value==this.w_DATA22)
      this.oPgFrm.Page1.oPag.oDATA22_1_24.value=this.w_DATA22
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA14_1_27.value==this.w_DATA14)
      this.oPgFrm.Page1.oPag.oDATA14_1_27.value=this.w_DATA14
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA24_1_28.value==this.w_DATA24)
      this.oPgFrm.Page1.oPag.oDATA24_1_28.value=this.w_DATA24
    endif
    if not(this.oPgFrm.Page2.oPag.oSOCODMOD_2_1.value==this.w_SOCODMOD)
      this.oPgFrm.Page2.oPag.oSOCODMOD_2_1.value=this.w_SOCODMOD
    endif
    if not(this.oPgFrm.Page2.oPag.oSOTIPNOM_2_2.RadioValue()==this.w_SOTIPNOM)
      this.oPgFrm.Page2.oPag.oSOTIPNOM_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSOCODNOM_2_3.value==this.w_SOCODNOM)
      this.oPgFrm.Page2.oPag.oSOCODNOM_2_3.value=this.w_SOCODNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oSOCODGRU_2_4.value==this.w_SOCODGRU)
      this.oPgFrm.Page2.oPag.oSOCODGRU_2_4.value=this.w_SOCODGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oSOCODPRI_2_5.value==this.w_SOCODPRI)
      this.oPgFrm.Page2.oPag.oSOCODPRI_2_5.value=this.w_SOCODPRI
    endif
    if not(this.oPgFrm.Page2.oPag.oSOCODORI_2_6.value==this.w_SOCODORI)
      this.oPgFrm.Page2.oPag.oSOCODORI_2_6.value=this.w_SOCODORI
    endif
    if not(this.oPgFrm.Page2.oPag.oSOCONTAT_2_7.value==this.w_SOCONTAT)
      this.oPgFrm.Page2.oPag.oSOCONTAT_2_7.value=this.w_SOCONTAT
    endif
    if not(this.oPgFrm.Page2.oPag.oSOCODUTE_2_8.value==this.w_SOCODUTE)
      this.oPgFrm.Page2.oPag.oSOCODUTE_2_8.value=this.w_SOCODUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oSOCODAGE_2_9.value==this.w_SOCODAGE)
      this.oPgFrm.Page2.oPag.oSOCODAGE_2_9.value=this.w_SOCODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oSOCODZON_2_10.value==this.w_SOCODZON)
      this.oPgFrm.Page2.oPag.oSOCODZON_2_10.value=this.w_SOCODZON
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT0_3_1.value==this.w_SOCODAT0)
      this.oPgFrm.Page3.oPag.oSOCODAT0_3_1.value=this.w_SOCODAT0
    endif
    if not(this.oPgFrm.Page2.oPag.oSOMODESC_2_23.value==this.w_SOMODESC)
      this.oPgFrm.Page2.oPag.oSOMODESC_2_23.value=this.w_SOMODESC
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT0_3_2.value==this.w_DESATT0)
      this.oPgFrm.Page3.oPag.oDESATT0_3_2.value=this.w_DESATT0
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT1_3_3.value==this.w_SOCODAT1)
      this.oPgFrm.Page3.oPag.oSOCODAT1_3_3.value=this.w_SOCODAT1
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT1_3_4.value==this.w_DESATT1)
      this.oPgFrm.Page3.oPag.oDESATT1_3_4.value=this.w_DESATT1
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT2_3_5.value==this.w_SOCODAT2)
      this.oPgFrm.Page3.oPag.oSOCODAT2_3_5.value=this.w_SOCODAT2
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT2_3_6.value==this.w_DESATT2)
      this.oPgFrm.Page3.oPag.oDESATT2_3_6.value=this.w_DESATT2
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT3_3_7.value==this.w_SOCODAT3)
      this.oPgFrm.Page3.oPag.oSOCODAT3_3_7.value=this.w_SOCODAT3
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT3_3_8.value==this.w_DESATT3)
      this.oPgFrm.Page3.oPag.oDESATT3_3_8.value=this.w_DESATT3
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT4_3_9.value==this.w_SOCODAT4)
      this.oPgFrm.Page3.oPag.oSOCODAT4_3_9.value=this.w_SOCODAT4
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT4_3_10.value==this.w_DESATT4)
      this.oPgFrm.Page3.oPag.oDESATT4_3_10.value=this.w_DESATT4
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT5_3_11.value==this.w_SOCODAT5)
      this.oPgFrm.Page3.oPag.oSOCODAT5_3_11.value=this.w_SOCODAT5
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT5_3_12.value==this.w_DESATT5)
      this.oPgFrm.Page3.oPag.oDESATT5_3_12.value=this.w_DESATT5
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT6_3_13.value==this.w_SOCODAT6)
      this.oPgFrm.Page3.oPag.oSOCODAT6_3_13.value=this.w_SOCODAT6
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT6_3_14.value==this.w_DESATT6)
      this.oPgFrm.Page3.oPag.oDESATT6_3_14.value=this.w_DESATT6
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT7_3_15.value==this.w_SOCODAT7)
      this.oPgFrm.Page3.oPag.oSOCODAT7_3_15.value=this.w_SOCODAT7
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT7_3_16.value==this.w_DESATT7)
      this.oPgFrm.Page3.oPag.oDESATT7_3_16.value=this.w_DESATT7
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT8_3_17.value==this.w_SOCODAT8)
      this.oPgFrm.Page3.oPag.oSOCODAT8_3_17.value=this.w_SOCODAT8
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT8_3_18.value==this.w_DESATT8)
      this.oPgFrm.Page3.oPag.oDESATT8_3_18.value=this.w_DESATT8
    endif
    if not(this.oPgFrm.Page3.oPag.oSOCODAT9_3_19.value==this.w_SOCODAT9)
      this.oPgFrm.Page3.oPag.oSOCODAT9_3_19.value=this.w_SOCODAT9
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT9_3_20.value==this.w_DESATT9)
      this.oPgFrm.Page3.oPag.oDESATT9_3_20.value=this.w_DESATT9
    endif
    if not(this.oPgFrm.Page2.oPag.oSODESNOM_2_24.value==this.w_SODESNOM)
      this.oPgFrm.Page2.oPag.oSODESNOM_2_24.value=this.w_SODESNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCGN_2_25.value==this.w_DESCGN)
      this.oPgFrm.Page2.oPag.oDESCGN_2_25.value=this.w_DESCGN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZONE_2_26.value==this.w_DESZONE)
      this.oPgFrm.Page2.oPag.oDESZONE_2_26.value=this.w_DESZONE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESORI_2_27.value==this.w_DESORI)
      this.oPgFrm.Page2.oPag.oDESORI_2_27.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_28.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_28.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRIO_2_29.value==this.w_DESPRIO)
      this.oPgFrm.Page2.oPag.oDESPRIO_2_29.value=this.w_DESPRIO
    endif
    if not(this.oPgFrm.Page2.oPag.oDESOPE_2_30.value==this.w_DESOPE)
      this.oPgFrm.Page2.oPag.oDESOPE_2_30.value=this.w_DESOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGPR_1_35.value==this.w_DESGPR)
      this.oPgFrm.Page1.oPag.oDESGPR_1_35.value=this.w_DESGPR
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_41.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_41.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO1_1_42.value==this.w_NUMERO1)
      this.oPgFrm.Page1.oPag.oNUMERO1_1_42.value=this.w_NUMERO1
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE1_1_43.value==this.w_SERIE1)
      this.oPgFrm.Page1.oPag.oSERIE1_1_43.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE2_1_44.value==this.w_SERIE2)
      this.oPgFrm.Page1.oPag.oSERIE2_1_44.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page1.oPag.oSELORDBY_1_49.RadioValue()==this.w_SELORDBY)
      this.oPgFrm.Page1.oPag.oSELORDBY_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOFCODAGE_2_37.value==this.w_OFCODAGE)
      this.oPgFrm.Page2.oPag.oOFCODAGE_2_37.value=this.w_OFCODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oAGDESAGE_2_39.value==this.w_AGDESAGE)
      this.oPgFrm.Page2.oPag.oAGDESAGE_2_39.value=this.w_AGDESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oOFCODAG2_2_41.value==this.w_OFCODAG2)
      this.oPgFrm.Page2.oPag.oOFCODAG2_2_41.value=this.w_OFCODAG2
    endif
    if not(this.oPgFrm.Page2.oPag.oAGDESAG2_2_43.value==this.w_AGDESAG2)
      this.oPgFrm.Page2.oPag.oAGDESAG2_2_43.value=this.w_AGDESAG2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_edate)) OR (.w_edate>=.w_bdate))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBDATE_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not((.w_EDATE>=.w_BDATE) OR  ( empty(.w_bdate)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEDATE_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore della data iniziale")
          case   not((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO)))  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO1_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))  and not(upper(g_APPLICATION) <> "AD HOC ENTERPRISE")  and (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(empty(.w_data21) or .w_data11<=.w_data21)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA11_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_data11) or .w_data11<=.w_data21)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA21_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_data23) or .w_data13<=.w_data23)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA13_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_data13) or .w_data13<=.w_data23)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA23_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_data22) or .w_data12<=.w_data22)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA12_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_data12) or .w_data12<=.w_data22)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA22_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_data24) or .w_data14<=.w_data24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA14_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_data14) or .w_data14<=.w_data24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA24_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!"), CHKDTOBS(.w_DTOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.)))  and not(empty(.w_SOCODAGE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSOCODAGE_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and (upper(g_APPLICATION) = "ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO)))  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and (upper(g_APPLICATION) = "ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO1_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero documento iniziale � pi� grande del codice finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and (upper(g_APPLICATION) = "ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE1_1_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and (upper(g_APPLICATION) = "ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE2_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBAGEO,.w_OBTEST,"Agente obsoleto alla data Attuale!"), CHKDTOBS(.w_DTOBAGEO,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.)))  and not(empty(.w_OFCODAGE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOFCODAGE_2_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_AGTIPAGE='C' AND IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBAGE2,.w_OBTEST,"Agente obsoleto alla data Attuale!"), CHKDTOBS(.w_DTOBAGE2,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.)))  and not(empty(.w_OFCODAG2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOFCODAG2_2_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'agente selezionato non di tipo capoarea")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SOTIPNOM = this.w_SOTIPNOM
    this.o_SOCODAT0 = this.w_SOCODAT0
    this.o_SOCODAT1 = this.w_SOCODAT1
    this.o_SOCODAT2 = this.w_SOCODAT2
    this.o_SOCODAT3 = this.w_SOCODAT3
    this.o_SOCODAT4 = this.w_SOCODAT4
    this.o_SOCODAT5 = this.w_SOCODAT5
    this.o_SOCODAT6 = this.w_SOCODAT6
    this.o_SOCODAT7 = this.w_SOCODAT7
    this.o_SOCODAT8 = this.w_SOCODAT8
    return

enddefine

* --- Define pages as container
define class tgsof_sboPag1 as StdContainer
  Width  = 564
  height = 358
  stdWidth  = 564
  stdheight = 358
  resizeXpos=306
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oBDATE_1_1 as StdField with uid="JWHNCRKDRD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_BDATE", cQueryName = "BDATE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di inizio stampa",;
    HelpContextID = 238956310,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=19

  func oBDATE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_edate)) OR (.w_edate>=.w_bdate))
    endwith
    return bRes
  endfunc

  add object oEDATE_1_2 as StdField with uid="OWEPFQDEHL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_EDATE", cQueryName = "EDATE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore della data iniziale",;
    ToolTipText = "Data documento di fine stampa",;
    HelpContextID = 238956358,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=47

  func oEDATE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_EDATE>=.w_BDATE) OR  ( empty(.w_bdate)))
    endwith
    return bRes
  endfunc

  add object oNUMERO_1_3 as StdField with uid="BXBQYPGVTU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 33554218,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=340, Top=19, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oNUMERO_1_3.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oNUMERO_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
    endwith
    return bRes
  endfunc

  add object oNUMERO1_1_4 as StdField with uid="PROBAWFVAK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NUMERO1", cQueryName = "NUMERO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 33554218,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=340, Top=47, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMERO1_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oNUMERO1_1_4.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oNUMERO1_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO)))
    endwith
    return bRes
  endfunc

  add object oSERIE1_1_5 as StdField with uid="HVPVSEHHGU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Inizio serie del documento",;
    HelpContextID = 13352666,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=419, Top=19, InputMask=replicate('X',2)

  func oSERIE1_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oSERIE1_1_5.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oSERIE1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oSERIE2_1_6 as StdField with uid="LHPNDMBVHJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Fine serie del documento",;
    HelpContextID = 265010906,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=419, Top=48, InputMask=replicate('X',2)

  func oSERIE2_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oSERIE2_1_6.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  func oSERIE2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc


  add object oSOSTATUS_1_7 as StdCombo with uid="MWWDNPAJRA",rtseq=7,rtrep=.f.,left=134,top=80,width=142,height=21;
    , ToolTipText = "Stato: in corso; inviata; confermata; versione chiusa; sospesa; rifiutata";
    , HelpContextID = 33512313;
    , cFormVar="w_SOSTATUS",RowSource=""+"In corso,"+"Inviata,"+"In corso+inviate,"+"Confermata,"+"Versione chiusa,"+"Rifiutata,"+"Sospesa,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSOSTATUS_1_7.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'X',;
    iif(this.value =4,'C',;
    iif(this.value =5,'V',;
    iif(this.value =6,'R',;
    iif(this.value =7,'S',;
    iif(this.value =8,'T',;
    space(1))))))))))
  endfunc
  func oSOSTATUS_1_7.GetRadio()
    this.Parent.oContained.w_SOSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oSOSTATUS_1_7.SetRadio()
    this.Parent.oContained.w_SOSTATUS=trim(this.Parent.oContained.w_SOSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_SOSTATUS=='I',1,;
      iif(this.Parent.oContained.w_SOSTATUS=='A',2,;
      iif(this.Parent.oContained.w_SOSTATUS=='X',3,;
      iif(this.Parent.oContained.w_SOSTATUS=='C',4,;
      iif(this.Parent.oContained.w_SOSTATUS=='V',5,;
      iif(this.Parent.oContained.w_SOSTATUS=='R',6,;
      iif(this.Parent.oContained.w_SOSTATUS=='S',7,;
      iif(this.Parent.oContained.w_SOSTATUS=='T',8,;
      0))))))))
  endfunc

  add object oSOCODGPR_1_8 as StdField with uid="UEJLNEBHYZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SOCODGPR", cQueryName = "SOCODGPR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppi priorit� offerta",;
    HelpContextID = 181838984,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=134, Top=109, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", cZoomOnZoom="GSOF_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_SOCODGPR"

  func oSOCODGPR_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODGPR_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODGPR_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oSOCODGPR_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AGP',"Gruppi priorit�",'',this.parent.oContained
  endproc
  proc oSOCODGPR_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_SOCODGPR
     i_obj.ecpSave()
  endproc

  add object oDATA11_1_15 as StdField with uid="IMXCCGPCDR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATA11", cQueryName = "DATA11",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Ricontattare dal",;
    HelpContextID = 34841546,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=140

  func oDATA11_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data21) or .w_data11<=.w_data21)
    endwith
    return bRes
  endfunc

  add object oDATA21_1_16 as StdField with uid="XYJPELZXSB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATA21", cQueryName = "DATA21",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Al",;
    HelpContextID = 33792970,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=168

  func oDATA21_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data11) or .w_data11<=.w_data21)
    endwith
    return bRes
  endfunc

  add object oDATA13_1_19 as StdField with uid="CTNFAZFENS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATA13", cQueryName = "DATA13",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di scadenza offerta",;
    HelpContextID = 1287114,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=381, Top=140

  func oDATA13_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data23) or .w_data13<=.w_data23)
    endwith
    return bRes
  endfunc

  add object oDATA23_1_20 as StdField with uid="BRNDVQNGRC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATA23", cQueryName = "DATA23",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di scadenza offerta",;
    HelpContextID = 238538,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=381, Top=168

  func oDATA23_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data13) or .w_data13<=.w_data23)
    endwith
    return bRes
  endfunc

  add object oDATA12_1_23 as StdField with uid="NEHDYUIGTL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATA12", cQueryName = "DATA12",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di invio offerta",;
    HelpContextID = 18064330,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=199

  func oDATA12_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data22) or .w_data12<=.w_data22)
    endwith
    return bRes
  endfunc

  add object oDATA22_1_24 as StdField with uid="ZFYMCKCLTL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATA22", cQueryName = "DATA22",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di invio offerta",;
    HelpContextID = 17015754,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=227

  func oDATA22_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data12) or .w_data12<=.w_data22)
    endwith
    return bRes
  endfunc

  add object oDATA14_1_27 as StdField with uid="UOLDNZJIVX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATA14", cQueryName = "DATA14",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di chiusura offerta",;
    HelpContextID = 252945354,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=381, Top=199

  func oDATA14_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data24) or .w_data14<=.w_data24)
    endwith
    return bRes
  endfunc

  add object oDATA24_1_28 as StdField with uid="HTAIPVACCA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DATA24", cQueryName = "DATA24",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di chiusura offerta",;
    HelpContextID = 251896778,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=381, Top=227

  func oDATA24_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data14) or .w_data14<=.w_data24)
    endwith
    return bRes
  endfunc


  add object oBtn_1_31 as StdButton with uid="OEVOSMUXMQ",left=455, top=311, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 160835302;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      this.parent.oContained.NotifyEvent("Stampa")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_32 as cp_outputCombo with uid="ZLERXIVPWT",left=134, top=281, width=422,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 198058522


  add object oBtn_1_33 as StdButton with uid="RXAHGEFLBZ",left=508, top=311, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 53335802;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESGPR_1_35 as StdField with uid="LMDTBSVTVH",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESGPR", cQueryName = "DESGPR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 253603786,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=199, Top=109, InputMask=replicate('X',35)

  add object oNUMERO_1_41 as StdField with uid="RSYFGSIQYD",rtseq=65,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 33554218,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=340, Top=19, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMERO_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oNUMERO_1_41.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMERO_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_NUMERO1)) OR  (.w_NUMERO<=.w_NUMERO1))
    endwith
    return bRes
  endfunc

  add object oNUMERO1_1_42 as StdField with uid="WHOXPMOQOI",rtseq=66,rtrep=.f.,;
    cFormVar = "w_NUMERO1", cQueryName = "NUMERO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero documento iniziale � pi� grande del codice finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 33554218,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=340, Top=47, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMERO1_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oNUMERO1_1_42.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oNUMERO1_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMERO1>=.w_NUMERO) or (empty(.w_NUMERO)))
    endwith
    return bRes
  endfunc

  add object oSERIE1_1_43 as StdField with uid="ODZYYUBJWJ",rtseq=67,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Inizio serie del documento",;
    HelpContextID = 13352666,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=473, Top=19, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oSERIE1_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oSERIE1_1_43.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oSERIE1_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oSERIE2_1_44 as StdField with uid="OCSFYCEKAB",rtseq=68,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Fine serie del documento",;
    HelpContextID = 265010906,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=473, Top=47, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oSERIE2_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oSERIE2_1_44.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oSERIE2_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc


  add object oSELORDBY_1_49 as StdCombo with uid="WCXZJUWIMM",value=1,rtseq=70,rtrep=.f.,left=134,top=255,width=197,height=21;
    , HelpContextID = 217456257;
    , cFormVar="w_SELORDBY",RowSource=""+"Nessuno,"+"Data documento,"+"Operatore,"+"Nominativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELORDBY_1_49.RadioValue()
    return(iif(this.value =1,"",;
    iif(this.value =2,"OFF_ERTE.OFDATDOC",;
    iif(this.value =3,"OFF_ERTE.OFCODUTE",;
    iif(this.value =4,"OFF_ERTE.OFCODNOM",;
    space(30))))))
  endfunc
  func oSELORDBY_1_49.GetRadio()
    this.Parent.oContained.w_SELORDBY = this.RadioValue()
    return .t.
  endfunc

  func oSELORDBY_1_49.SetRadio()
    this.Parent.oContained.w_SELORDBY=trim(this.Parent.oContained.w_SELORDBY)
    this.value = ;
      iif(this.Parent.oContained.w_SELORDBY=="",1,;
      iif(this.Parent.oContained.w_SELORDBY=="OFF_ERTE.OFDATDOC",2,;
      iif(this.Parent.oContained.w_SELORDBY=="OFF_ERTE.OFCODUTE",3,;
      iif(this.Parent.oContained.w_SELORDBY=="OFF_ERTE.OFCODNOM",4,;
      0))))
  endfunc

  add object oStr_1_9 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=6, Top=19,;
    Alignment=1, Width=127, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="BRSOYYEROT",Visible=.t., Left=6, Top=47,;
    Alignment=1, Width=127, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="CYZLPTYBOV",Visible=.t., Left=220, Top=19,;
    Alignment=1, Width=119, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="CRPMILLJDX",Visible=.t., Left=220, Top=47,;
    Alignment=1, Width=119, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="FUOVNIOIRU",Visible=.t., Left=6, Top=80,;
    Alignment=1, Width=127, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="UDIDVXBQJE",Visible=.t., Left=6, Top=109,;
    Alignment=1, Width=127, Height=18,;
    Caption="Gruppo priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="IGIEGMJDQE",Visible=.t., Left=6, Top=140,;
    Alignment=1, Width=127, Height=18,;
    Caption="Ricontattare dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KWVDALQKTJ",Visible=.t., Left=6, Top=168,;
    Alignment=1, Width=127, Height=18,;
    Caption="Ricontattare al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="CTKQSMTNQC",Visible=.t., Left=242, Top=140,;
    Alignment=1, Width=136, Height=18,;
    Caption="Da data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="LDFBMAJEEV",Visible=.t., Left=242, Top=168,;
    Alignment=1, Width=136, Height=18,;
    Caption="A data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="JBSYOAXZGW",Visible=.t., Left=6, Top=199,;
    Alignment=1, Width=127, Height=18,;
    Caption="Da data invio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="DOQGNXKJJR",Visible=.t., Left=6, Top=227,;
    Alignment=1, Width=127, Height=18,;
    Caption="A data invio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="UICFEIBEON",Visible=.t., Left=242, Top=199,;
    Alignment=1, Width=136, Height=18,;
    Caption="Da data chiusura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ACEFNOYSMM",Visible=.t., Left=242, Top=227,;
    Alignment=1, Width=136, Height=18,;
    Caption="A data chiusura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="KHWVLZCVUG",Visible=.t., Left=6, Top=281,;
    Alignment=1, Width=127, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="FSQAAYBTWJ",Visible=.t., Left=410, Top=20,;
    Alignment=0, Width=6, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="QEWKKAZXGX",Visible=.t., Left=410, Top=50,;
    Alignment=0, Width=6, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="POFITEFGYU",Visible=.t., Left=461, Top=20,;
    Alignment=0, Width=13, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="ZJCLBEFJDF",Visible=.t., Left=461, Top=50,;
    Alignment=0, Width=13, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="AHBKKPDYVV",Visible=.t., Left=6, Top=255,;
    Alignment=1, Width=127, Height=18,;
    Caption="Stampa ordinata per:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_sboPag2 as StdContainer
  Width  = 564
  height = 358
  stdWidth  = 564
  stdheight = 358
  resizeXpos=367
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSOCODMOD_2_1 as StdField with uid="OVTGXUDDNM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_SOCODMOD", cQueryName = "SOCODMOD",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice modello",;
    HelpContextID = 81175702,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=25, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MOD_OFFE", oKey_1_1="MOCODICE", oKey_1_2="this.w_SOCODMOD"

  func oSOCODMOD_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODMOD_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODMOD_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_OFFE','*','MOCODICE',cp_AbsName(this.parent,'oSOCODMOD_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Modelli offerte",'',this.parent.oContained
  endproc


  add object oSOTIPNOM_2_2 as StdCombo with uid="SCSYQNMLUC",rtseq=18,rtrep=.f.,left=110,top=110,width=109,height=21;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 52139149;
    , cFormVar="w_SOTIPNOM",RowSource=""+"Tutti,"+"Da valutare,"+"Potenziale,"+"Lead,"+"Prospect,"+"Cliente,"+"Congelato,"+"Non interessante", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oSOTIPNOM_2_2.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'L',;
    iif(this.value =5,'P',;
    iif(this.value =6,'C',;
    iif(this.value =7,'G',;
    iif(this.value =8,'N',;
    space(1))))))))))
  endfunc
  func oSOTIPNOM_2_2.GetRadio()
    this.Parent.oContained.w_SOTIPNOM = this.RadioValue()
    return .t.
  endfunc

  func oSOTIPNOM_2_2.SetRadio()
    this.Parent.oContained.w_SOTIPNOM=trim(this.Parent.oContained.w_SOTIPNOM)
    this.value = ;
      iif(this.Parent.oContained.w_SOTIPNOM=='A',1,;
      iif(this.Parent.oContained.w_SOTIPNOM=='T',2,;
      iif(this.Parent.oContained.w_SOTIPNOM=='Z',3,;
      iif(this.Parent.oContained.w_SOTIPNOM=='L',4,;
      iif(this.Parent.oContained.w_SOTIPNOM=='P',5,;
      iif(this.Parent.oContained.w_SOTIPNOM=='C',6,;
      iif(this.Parent.oContained.w_SOTIPNOM=='G',7,;
      iif(this.Parent.oContained.w_SOTIPNOM=='N',8,;
      0))))))))
  endfunc

  add object oSOCODNOM_2_3 as StdField with uid="CKYDOSAPDM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SOCODNOM", cQueryName = "SOCODNOM",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 64398477,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=110, Top=136, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_SOCODNOM"

  func oSOCODNOM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODNOM_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODNOM_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oSOCODNOM_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF2SNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oSOCODNOM_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_SOCODNOM
     i_obj.ecpSave()
  endproc

  add object oSOCODGRU_2_4 as StdField with uid="UNPJLMPEEC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SOCODGRU", cQueryName = "SOCODGRU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo di appartenenza",;
    HelpContextID = 181838981,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=160, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", oKey_1_1="GNCODICE", oKey_1_2="this.w_SOCODGRU"

  func oSOCODGRU_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODGRU_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODGRU_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oSOCODGRU_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi nominativi",'',this.parent.oContained
  endproc

  add object oSOCODPRI_2_5 as StdField with uid="KUEIEJAHIM",rtseq=21,rtrep=.f.,;
    cFormVar = "w_SOCODPRI", cQueryName = "SOCODPRI",;
    bObbl = .f. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo priorit�",;
    HelpContextID = 30844049,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=110, Top=184, cSayPict='"XXXXXX"', cGetPict='"XXXXXX"', InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="GRU_PRIO", oKey_1_1="GPCODICE", oKey_1_2="this.w_SOCODPRI"

  func oSOCODPRI_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODPRI_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODPRI_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oSOCODPRI_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi priorit�",'',this.parent.oContained
  endproc

  add object oSOCODORI_2_6 as StdField with uid="PNBSTVXJHA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SOCODORI", cQueryName = "SOCODORI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine reperimento nominativo",;
    HelpContextID = 47621265,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=209, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", oKey_1_1="ONCODICE", oKey_1_2="this.w_SOCODORI"

  func oSOCODORI_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODORI_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODORI_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oSOCODORI_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici origine nominativi",'',this.parent.oContained
  endproc

  add object oSOCONTAT_2_7 as StdField with uid="LMRWBNSIXK",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SOCONTAT", cQueryName = "SOCONTAT",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contatto",;
    HelpContextID = 221684870,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=110, Top=254, InputMask=replicate('X',40)

  add object oSOCODUTE_2_8 as StdField with uid="EOVVDRAAXR",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SOCODUTE", cQueryName = "SOCODUTE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 215393429,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=110, Top=280, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_SOCODUTE"

  func oSOCODUTE_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODUTE_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODUTE_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oSOCODUTE_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Operatori",'',this.parent.oContained
  endproc

  add object oSOCODAGE_2_9 as StdField with uid="UDANMVNSCD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SOCODAGE", cQueryName = "SOCODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente associato al nominativo dell'offerta",;
    HelpContextID = 254368619,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=305, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_SOCODAGE"

  func oSOCODAGE_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAGE_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAGE_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oSOCODAGE_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oSOCODZON_2_10 as StdField with uid="JEIWNKIDFS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_SOCODZON", cQueryName = "SOCODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice zona",;
    HelpContextID = 131507340,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=110, Top=330, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_SOCODZON"

  func oSOCODZON_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODZON_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODZON_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oSOCODZON_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Zone",'',this.parent.oContained
  endproc

  add object oSOMODESC_2_23 as StdField with uid="RSCTAIVXZF",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SOMODESC", cQueryName = "SOMODESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 215352471,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=25, InputMask=replicate('X',35)

  add object oSODESNOM_2_24 as StdField with uid="KWJVRVYWXK",rtseq=48,rtrep=.f.,;
    cFormVar = "w_SODESNOM", cQueryName = "SODESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49321101,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=279, Top=136, InputMask=replicate('X',40)

  add object oDESCGN_2_25 as StdField with uid="OXGVGSHZVB",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESCGN", cQueryName = "DESCGN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 61976522,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=182, Top=160, InputMask=replicate('X',35)

  add object oDESZONE_2_26 as StdField with uid="PJLDMIGCKS",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESZONE", cQueryName = "DESZONE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52080586,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=330, InputMask=replicate('X',35)

  add object oDESORI_2_27 as StdField with uid="FBXJICCBRH",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 133541834,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=182, Top=209, InputMask=replicate('X',35)

  add object oDESAGE_2_28 as StdField with uid="NZBPQJVISD",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 213102538,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=305, InputMask=replicate('X',35)

  add object oDESPRIO_2_29 as StdField with uid="XTKEVJMOAS",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESPRIO", cQueryName = "DESPRIO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 133476298,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=182, Top=184, InputMask=replicate('X',35)

  add object oDESOPE_2_30 as StdField with uid="UODXIIMSHN",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DESOPE", cQueryName = "DESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202747850,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=280, InputMask=replicate('X',35)

  add object oOFCODAGE_2_37 as StdField with uid="XQPTRMNHVJ",rtseq=71,rtrep=.f.,;
    cFormVar = "w_OFCODAGE", cQueryName = "OFCODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente associato all'offerta",;
    HelpContextID = 254366251,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=51, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_OFCODAGE"

  func oOFCODAGE_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODAGE_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODAGE_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oOFCODAGE_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oAGDESAGE_2_39 as StdField with uid="YYFVJWTCNT",rtseq=72,rtrep=.f.,;
    cFormVar = "w_AGDESAGE", cQueryName = "AGDESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1008203,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=51, InputMask=replicate('X',35)

  add object oOFCODAG2_2_41 as StdField with uid="YASKXXYNUN",rtseq=74,rtrep=.f.,;
    cFormVar = "w_OFCODAG2", cQueryName = "OFCODAG2",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "L'agente selezionato non di tipo capoarea",;
    ToolTipText = "Codice capo area associato all'offerta",;
    HelpContextID = 254366232,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=77, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_OFCODAG2"

  func oOFCODAG2_2_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODAG2_2_41.ecpDrop(oSource)
    this.Parent.oContained.link_2_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODAG2_2_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oOFCODAG2_2_41'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'GSVE1KFT.AGENTI_VZM',this.parent.oContained
  endproc

  add object oAGDESAG2_2_43 as StdField with uid="YUONCYDLHB",rtseq=75,rtrep=.f.,;
    cFormVar = "w_AGDESAG2", cQueryName = "AGDESAG2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1008184,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=77, InputMask=replicate('X',35)

  add object oStr_2_11 as StdString with uid="QLCFKBAHHG",Visible=.t., Left=16, Top=136,;
    Alignment=1, Width=91, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="CFWPGFRFCB",Visible=.t., Left=16, Top=110,;
    Alignment=1, Width=91, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="RUIOOTHBLI",Visible=.t., Left=16, Top=254,;
    Alignment=1, Width=91, Height=18,;
    Caption="Contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="FSDBXSLPSK",Visible=.t., Left=16, Top=280,;
    Alignment=1, Width=91, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="EDIGVBZFUW",Visible=.t., Left=16, Top=305,;
    Alignment=1, Width=91, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="GGMQEJEICX",Visible=.t., Left=16, Top=330,;
    Alignment=1, Width=91, Height=18,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="OSXSKIGNCP",Visible=.t., Left=16, Top=25,;
    Alignment=1, Width=91, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="GYZPKGJJXH",Visible=.t., Left=16, Top=160,;
    Alignment=1, Width=91, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="JINCGGAKVH",Visible=.t., Left=4, Top=209,;
    Alignment=1, Width=103, Height=18,;
    Caption="Origine nom.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="EIVPTJURQF",Visible=.t., Left=16, Top=184,;
    Alignment=1, Width=91, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="GDSZAAMGKJ",Visible=.t., Left=16, Top=51,;
    Alignment=1, Width=91, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="TAYELDOZXD",Visible=.t., Left=16, Top=77,;
    Alignment=1, Width=91, Height=18,;
    Caption="Capoarea:"  ;
  , bGlobalFont=.t.

  add object oBox_2_21 as StdBox with uid="FMNLORWVGM",left=5, top=242, width=552,height=1

  add object oBox_2_22 as StdBox with uid="PGIRAMBKZF",left=6, top=102, width=553,height=1
enddefine
define class tgsof_sboPag3 as StdContainer
  Width  = 564
  height = 358
  stdWidth  = 564
  stdheight = 358
  resizeXpos=316
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSOCODAT0_3_1 as StdField with uid="FXJGYGRWAP",rtseq=27,rtrep=.f.,;
    cFormVar = "w_SOCODAT0", cQueryName = "SOCODAT0",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 1",;
    HelpContextID = 14066858,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=47, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT0"

  func oSOCODAT0_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT0_3_1.ecpDrop(oSource)
    this.Parent.oContained.link_3_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT0_3_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT0_3_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT0_3_2 as StdField with uid="HMQDUTQTVT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESATT0", cQueryName = "DESATT0",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216248266,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=47, InputMask=replicate('X',35)

  add object oSOCODAT1_3_3 as StdField with uid="DLOEXUGDHN",rtseq=30,rtrep=.f.,;
    cFormVar = "w_SOCODAT1", cQueryName = "SOCODAT1",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 2",;
    HelpContextID = 14066857,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=71, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT1"

  func oSOCODAT1_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_SOCODAT0)))
    endwith
   endif
  endfunc

  func oSOCODAT1_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT1_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT1_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT1_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT1_3_4 as StdField with uid="WDFDSCLFND",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESATT1", cQueryName = "DESATT1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216248266,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=71, InputMask=replicate('X',35)

  add object oSOCODAT2_3_5 as StdField with uid="HNBJFMRUWX",rtseq=32,rtrep=.f.,;
    cFormVar = "w_SOCODAT2", cQueryName = "SOCODAT2",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 3",;
    HelpContextID = 14066856,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=95, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT2"

  func oSOCODAT2_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_SOCODAT1)))
    endwith
   endif
  endfunc

  func oSOCODAT2_3_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT2_3_5.ecpDrop(oSource)
    this.Parent.oContained.link_3_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT2_3_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT2_3_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT2_3_6 as StdField with uid="MAJTQMWKHR",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESATT2", cQueryName = "DESATT2",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216248266,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=95, InputMask=replicate('X',35)

  add object oSOCODAT3_3_7 as StdField with uid="BNQEYHQYDN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SOCODAT3", cQueryName = "SOCODAT3",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 4",;
    HelpContextID = 14066855,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=119, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT3"

  func oSOCODAT3_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_SOCODAT2)))
    endwith
   endif
  endfunc

  func oSOCODAT3_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT3_3_7.ecpDrop(oSource)
    this.Parent.oContained.link_3_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT3_3_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT3_3_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT3_3_8 as StdField with uid="IELFGCXART",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESATT3", cQueryName = "DESATT3",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216248266,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=119, InputMask=replicate('X',35)

  add object oSOCODAT4_3_9 as StdField with uid="STFSYPYSZH",rtseq=36,rtrep=.f.,;
    cFormVar = "w_SOCODAT4", cQueryName = "SOCODAT4",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 5",;
    HelpContextID = 14066854,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=143, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT4"

  func oSOCODAT4_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_SOCODAT3)))
    endwith
   endif
  endfunc

  func oSOCODAT4_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT4_3_9.ecpDrop(oSource)
    this.Parent.oContained.link_3_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT4_3_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT4_3_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT4_3_10 as StdField with uid="TTVSRHBSED",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESATT4", cQueryName = "DESATT4",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216248266,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=143, InputMask=replicate('X',35)

  add object oSOCODAT5_3_11 as StdField with uid="BHBXERTFYU",rtseq=38,rtrep=.f.,;
    cFormVar = "w_SOCODAT5", cQueryName = "SOCODAT5",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 6",;
    HelpContextID = 14066853,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=167, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT5"

  func oSOCODAT5_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_SOCODAT4)))
    endwith
   endif
  endfunc

  func oSOCODAT5_3_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT5_3_11.ecpDrop(oSource)
    this.Parent.oContained.link_3_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT5_3_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT5_3_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT5_3_12 as StdField with uid="VHYSDADBOD",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESATT5", cQueryName = "DESATT5",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52187190,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=167, InputMask=replicate('X',35)

  add object oSOCODAT6_3_13 as StdField with uid="AIDJNARPHX",rtseq=40,rtrep=.f.,;
    cFormVar = "w_SOCODAT6", cQueryName = "SOCODAT6",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 7",;
    HelpContextID = 14066852,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=191, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT6"

  func oSOCODAT6_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_SOCODAT5)))
    endwith
   endif
  endfunc

  func oSOCODAT6_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT6_3_13.ecpDrop(oSource)
    this.Parent.oContained.link_3_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT6_3_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT6_3_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT6_3_14 as StdField with uid="FOTTNHBWMW",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESATT6", cQueryName = "DESATT6",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52187190,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=191, InputMask=replicate('X',35)

  add object oSOCODAT7_3_15 as StdField with uid="MWEBONAXRD",rtseq=42,rtrep=.f.,;
    cFormVar = "w_SOCODAT7", cQueryName = "SOCODAT7",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 8",;
    HelpContextID = 14066851,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=215, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT7"

  func oSOCODAT7_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_SOCODAT6)))
    endwith
   endif
  endfunc

  func oSOCODAT7_3_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT7_3_15.ecpDrop(oSource)
    this.Parent.oContained.link_3_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT7_3_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT7_3_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT7_3_16 as StdField with uid="KBCELBEALV",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESATT7", cQueryName = "DESATT7",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52187190,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=215, InputMask=replicate('X',35)

  add object oSOCODAT8_3_17 as StdField with uid="DTWRSHIQBR",rtseq=44,rtrep=.f.,;
    cFormVar = "w_SOCODAT8", cQueryName = "SOCODAT8",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 9",;
    HelpContextID = 14066850,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=239, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT8"

  func oSOCODAT8_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_SOCODAT7)))
    endwith
   endif
  endfunc

  func oSOCODAT8_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT8_3_17.ecpDrop(oSource)
    this.Parent.oContained.link_3_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT8_3_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT8_3_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT8_3_18 as StdField with uid="UHXRCKAJZN",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESATT8", cQueryName = "DESATT8",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52187190,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=239, InputMask=replicate('X',35)

  add object oSOCODAT9_3_19 as StdField with uid="UQSEZFBHPS",rtseq=46,rtrep=.f.,;
    cFormVar = "w_SOCODAT9", cQueryName = "SOCODAT9",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo 10",;
    HelpContextID = 14066849,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=72, Top=264, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", oKey_1_1="CTCODICE", oKey_1_2="this.w_SOCODAT9"

  func oSOCODAT9_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_SOCODAT8)))
    endwith
   endif
  endfunc

  func oSOCODAT9_3_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oSOCODAT9_3_19.ecpDrop(oSource)
    this.Parent.oContained.link_3_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSOCODAT9_3_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oSOCODAT9_3_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie attributi",'',this.parent.oContained
  endproc

  add object oDESATT9_3_20 as StdField with uid="CBRIXWXQMU",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESATT9", cQueryName = "DESATT9",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52187190,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=263, InputMask=replicate('X',35)

  add object oStr_3_21 as StdString with uid="WHHSQYGGIB",Visible=.t., Left=74, Top=25,;
    Alignment=0, Width=132, Height=18,;
    Caption="Attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_sbo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
