* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bgm                                                        *
*              Eventi da gestione modelli offerte                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_32]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-05                                                      *
* Last revis.: 2007-07-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bgm",oParentObject,m.Azione)
return(i_retval)

define class tgsof_bgm as StdBatch
  * --- Local variables
  Azione = space(10)
  w_OFNUMDOC = 0
  w_OFSERDOC = space(10)
  w_OFDATDOC = ctod("  /  /  ")
  w_OFNUMVER = 0
  GestALL = .NULL.
  TMPc = space(10)
  pop = space(100)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Gestione Modelli Offerte (da GSOF_AMO)
    * --- CARICA_ALLEGATO = Inserisce un Nuovo Allegato
    do case
      case this.Azione="CARICA_ALLEGATO"
        * --- Nuovo Allegato
        this.GestALL = GSOF_AAL(this.oParentObject.w_MOCODICE, "M",this.oParentObject.w_MODESCRI,this.w_OFNUMDOC,this.w_OFSERDOC,this.w_OFDATDOC,this.w_OFNUMVER)
        * --- Controllo se ha passato il test di accesso 
        if !(this.GestALL.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Lancia Caricamento ...
        this.GestALL.ECPLoad()     
        this.GestALL.w_ALTIPORI = "M"
        this.GestALL.w_ALRIFMOD = this.oParentObject.w_MOCODICE
        this.GestALL.mCalc(.T.)     
      case this.Azione $"VARIA_ALLEGATO|ELIMINA_ALLEGATO"
        this.GestALL = GSOF_AAL(this.oParentObject.w_MOCODICE, "M",this.oParentObject.w_MODESCRI,this.w_OFNUMDOC,this.w_OFSERDOC,this.w_OFDATDOC,this.w_OFNUMVER)
        * --- Controllo se ha passato il test di accesso 
        if !(this.GestALL.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carica record e lascia in interrograzione ...
        this.GestALL.w_ALSERIAL = this.oParentObject.w_SERALL
        this.TMPc = "ALSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERALL)
        this.GestALL.QueryKeySet(this.TmpC,"")     
        this.GestALL.LoadRecWarn()     
        do case
          case this.Azione = "VARIA_ALLEGATO"
            * --- Modifica record
            this.GestALL.ECPEdit()     
          case this.Azione = "ELIMINA_ALLEGATO"
            * --- Cancella record ...
            this.GestALL.ECPDelete()     
        endcase
      case this.Azione = "APRI_ALLEGATO"
        * --- Apri file ...
        this.pop = ofopenfile(ALLTRIM(this.oParentObject.w_PATALL),"","","")
    endcase
  endproc


  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
