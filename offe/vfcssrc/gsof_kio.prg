* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_kio                                                        *
*              Importa dettaglio offerte                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-13                                                      *
* Last revis.: 2008-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_kio",oParentObject))

* --- Class definition
define class tgsof_kio as StdForm
  Top    = 1
  Left   = 4

  * --- Standard Properties
  Width  = 792
  Height = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-26"
  HelpContextID=1431191
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsof_kio"
  cComment = "Importa dettaglio offerte"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FILNOM = space(20)
  w_DESNOM = space(40)
  w_FILINI = ctod('  /  /  ')
  w_FILFIN = ctod('  /  /  ')
  w_STAOFF = space(1)
  w_FILDES = space(45)
  w_SELEZI = space(1)
  w_CALSER = space(10)
  w_SERIAL = space(10)
  w_OFCODVAL = space(3)
  w_OFSERIAL = space(10)
  w_CODVAL = space(3)
  w_FILSER = space(10)
  w_DATOFF = ctod('  /  /  ')
  w_NUMOFF = 0
  w_SEROFF = space(2)
  w_FILSTI = space(1)
  w_FILSTA = space(1)
  w_FILSTC = space(1)
  w_FILSTR = space(1)
  w_FILSTS = space(1)
  w_NUMOFF = 0
  w_SEROFF = space(10)
  w_ZoomMast = .NULL.
  w_ZoomDett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_kioPag1","gsof_kio",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Documenti")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILNOM_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMast = this.oPgFrm.Pages(1).oPag.ZoomMast
    this.w_ZoomDett = this.oPgFrm.Pages(1).oPag.ZoomDett
    DoDefault()
    proc Destroy()
      this.w_ZoomMast = .NULL.
      this.w_ZoomDett = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='OFF_NOMI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FILNOM=space(20)
      .w_DESNOM=space(40)
      .w_FILINI=ctod("  /  /  ")
      .w_FILFIN=ctod("  /  /  ")
      .w_STAOFF=space(1)
      .w_FILDES=space(45)
      .w_SELEZI=space(1)
      .w_CALSER=space(10)
      .w_SERIAL=space(10)
      .w_OFCODVAL=space(3)
      .w_OFSERIAL=space(10)
      .w_CODVAL=space(3)
      .w_FILSER=space(10)
      .w_DATOFF=ctod("  /  /  ")
      .w_NUMOFF=0
      .w_SEROFF=space(2)
      .w_FILSTI=space(1)
      .w_FILSTA=space(1)
      .w_FILSTC=space(1)
      .w_FILSTR=space(1)
      .w_FILSTS=space(1)
      .w_NUMOFF=0
      .w_SEROFF=space(10)
      .w_OFCODVAL=oParentObject.w_OFCODVAL
      .w_OFSERIAL=oParentObject.w_OFSERIAL
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_FILNOM))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,4,.f.)
        .w_STAOFF = 'X'
      .oPgFrm.Page1.oPag.ZoomMast.Calculate()
          .DoRTCalc(6,6,.f.)
        .w_SELEZI = 'S'
        .w_CALSER = NVL(.w_ZoomMast.getVar('OFSERIAL'), SPACE(10))
        .w_SERIAL = IIF(EMPTY(NVL(.w_CALSER,' ')),'zzzzzzzzzz', .w_CALSER)
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
      .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
          .DoRTCalc(10,11,.f.)
        .w_CODVAL = .w_OFCODVAL
        .w_FILSER = .w_OFSERIAL
        .w_DATOFF = CP_TODATE(.w_ZoomMast.getVar('OFDATDOC'))
        .w_NUMOFF = NVL(.w_ZoomMast.getVar('OFNUMDOC'),0)
        .w_SEROFF = NVL(.w_ZoomMast.getVar('OFSERDOC'),'  ')
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_FILSTI = IIF(.w_STAOFF $ 'XIE', 'I', 'X')
        .w_FILSTA = IIF(.w_STAOFF $ 'XAE', 'A', 'X')
        .w_FILSTC = IIF(.w_STAOFF $ 'XC', 'C', 'X')
        .w_FILSTR = IIF(.w_STAOFF $ 'XR', 'R', 'X')
        .w_FILSTS = IIF(.w_STAOFF $ 'XS', 'S', 'X')
        .w_NUMOFF = NVL(.w_ZoomMast.getVar('OFNUMDOC'),0)
        .w_SEROFF = NVL(.w_ZoomMast.getVar('OFSERDOC'),'  ')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_OFCODVAL=.w_OFCODVAL
      .oParentObject.w_OFSERIAL=.w_OFSERIAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .DoRTCalc(1,7,.t.)
            .w_CALSER = NVL(.w_ZoomMast.getVar('OFSERIAL'), SPACE(10))
            .w_SERIAL = IIF(EMPTY(NVL(.w_CALSER,' ')),'zzzzzzzzzz', .w_CALSER)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .DoRTCalc(10,11,.t.)
            .w_CODVAL = .w_OFCODVAL
            .w_FILSER = .w_OFSERIAL
            .w_DATOFF = CP_TODATE(.w_ZoomMast.getVar('OFDATDOC'))
            .w_NUMOFF = NVL(.w_ZoomMast.getVar('OFNUMDOC'),0)
            .w_SEROFF = NVL(.w_ZoomMast.getVar('OFSERDOC'),'  ')
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
            .w_FILSTI = IIF(.w_STAOFF $ 'XIE', 'I', 'X')
            .w_FILSTA = IIF(.w_STAOFF $ 'XAE', 'A', 'X')
            .w_FILSTC = IIF(.w_STAOFF $ 'XC', 'C', 'X')
            .w_FILSTR = IIF(.w_STAOFF $ 'XR', 'R', 'X')
            .w_FILSTS = IIF(.w_STAOFF $ 'XS', 'S', 'X')
            .w_NUMOFF = NVL(.w_ZoomMast.getVar('OFNUMDOC'),0)
            .w_SEROFF = NVL(.w_ZoomMast.getVar('OFSERDOC'),'  ')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMOFF_1_26.visible=!this.oPgFrm.Page1.oPag.oNUMOFF_1_26.mHide()
    this.oPgFrm.Page1.oPag.oSEROFF_1_27.visible=!this.oPgFrm.Page1.oPag.oSEROFF_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oNUMOFF_1_44.visible=!this.oPgFrm.Page1.oPag.oNUMOFF_1_44.mHide()
    this.oPgFrm.Page1.oPag.oSEROFF_1_45.visible=!this.oPgFrm.Page1.oPag.oSEROFF_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomMast.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDett.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FILNOM
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_FILNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_FILNOM))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_FILNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_FILNOM)+"%");

            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FILNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oFILNOM_1_1'),i_cWhere,'GSAR_ANO',"Elenco nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_FILNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_FILNOM)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILNOM = space(20)
      endif
      this.w_DESNOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFILNOM_1_1.value==this.w_FILNOM)
      this.oPgFrm.Page1.oPag.oFILNOM_1_1.value=this.w_FILNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_2.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_2.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oFILINI_1_4.value==this.w_FILINI)
      this.oPgFrm.Page1.oPag.oFILINI_1_4.value=this.w_FILINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFILFIN_1_5.value==this.w_FILFIN)
      this.oPgFrm.Page1.oPag.oFILFIN_1_5.value=this.w_FILFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAOFF_1_6.RadioValue()==this.w_STAOFF)
      this.oPgFrm.Page1.oPag.oSTAOFF_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILDES_1_7.value==this.w_FILDES)
      this.oPgFrm.Page1.oPag.oFILDES_1_7.value=this.w_FILDES
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_13.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATOFF_1_24.value==this.w_DATOFF)
      this.oPgFrm.Page1.oPag.oDATOFF_1_24.value=this.w_DATOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMOFF_1_26.value==this.w_NUMOFF)
      this.oPgFrm.Page1.oPag.oNUMOFF_1_26.value=this.w_NUMOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oSEROFF_1_27.value==this.w_SEROFF)
      this.oPgFrm.Page1.oPag.oSEROFF_1_27.value=this.w_SEROFF
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMOFF_1_44.value==this.w_NUMOFF)
      this.oPgFrm.Page1.oPag.oNUMOFF_1_44.value=this.w_NUMOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oSEROFF_1_45.value==this.w_SEROFF)
      this.oPgFrm.Page1.oPag.oSEROFF_1_45.value=this.w_SEROFF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsof_kio
      * --- Controlli Finali
      this.NotifyEvent('ControlliFinali')
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsof_kioPag1 as StdContainer
  Width  = 788
  height = 511
  stdWidth  = 788
  stdheight = 511
  resizeXpos=388
  resizeYpos=292
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFILNOM_1_1 as StdField with uid="EZSUKHADER",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FILNOM", cQueryName = "FILNOM",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale filtro su nominativo",;
    HelpContextID = 229055402,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=92, Top=21, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_FILNOM"

  func oFILNOM_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILNOM_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILNOM_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oFILNOM_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Elenco nominativi",'',this.parent.oContained
  endproc
  proc oFILNOM_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_FILNOM
     i_obj.ecpSave()
  endproc

  add object oDESNOM_1_2 as StdField with uid="CLVXAMTBOW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229027786,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=261, Top=21, InputMask=replicate('X',40)


  add object oBtn_1_3 as StdButton with uid="PVMMCBLPZC",left=727, top=13, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 90082026;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        do GSOF_BIO with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFILINI_1_4 as StdField with uid="TMJMSHLNEY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FILINI", cQueryName = "FILINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale filtro su data offerta",;
    HelpContextID = 29105066,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=92, Top=48

  add object oFILFIN_1_5 as StdField with uid="DQIMZVCZXN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FILFIN", cQueryName = "FILFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale filtro finale su data offerta (vuota=no selezione)",;
    HelpContextID = 219093930,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=261, Top=48


  add object oSTAOFF_1_6 as StdCombo with uid="NQKCPNMUDJ",rtseq=5,rtrep=.f.,left=426,top=48,width=128,height=21;
    , ToolTipText = "Tipologia status offerte da ricercare";
    , HelpContextID = 87474138;
    , cFormVar="w_STAOFF",RowSource=""+"Tutte,"+"In corso,"+"Inviate,"+"In corso+inviate,"+"Confermate,"+"Rifiutate,"+"Sospese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAOFF_1_6.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'I',;
    iif(this.value =3,'A',;
    iif(this.value =4,'E',;
    iif(this.value =5,'C',;
    iif(this.value =6,'R',;
    iif(this.value =7,'S',;
    space(1)))))))))
  endfunc
  func oSTAOFF_1_6.GetRadio()
    this.Parent.oContained.w_STAOFF = this.RadioValue()
    return .t.
  endfunc

  func oSTAOFF_1_6.SetRadio()
    this.Parent.oContained.w_STAOFF=trim(this.Parent.oContained.w_STAOFF)
    this.value = ;
      iif(this.Parent.oContained.w_STAOFF=='X',1,;
      iif(this.Parent.oContained.w_STAOFF=='I',2,;
      iif(this.Parent.oContained.w_STAOFF=='A',3,;
      iif(this.Parent.oContained.w_STAOFF=='E',4,;
      iif(this.Parent.oContained.w_STAOFF=='C',5,;
      iif(this.Parent.oContained.w_STAOFF=='R',6,;
      iif(this.Parent.oContained.w_STAOFF=='S',7,;
      0)))))))
  endfunc

  add object oFILDES_1_7 as StdField with uid="TBMTUOAOFQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FILDES", cQueryName = "FILDES",;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale filtro su riferimento descrittivo",;
    HelpContextID = 139533226,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=92, Top=75, InputMask=replicate('X',45)


  add object ZoomMast as cp_zoombox with uid="VJRPDOWVIW",left=-2, top=121, width=283,height=335,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ERTE",cZoomFile="GSOFMOFF",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bRetriveAllRows=.f.,bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="GSOF_AOF",;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 89006618


  add object oBtn_1_11 as StdButton with uid="RAWCWTVUDP",left=678, top=461, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 1451750;
    , tabStop=.f.,Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="LRZKXPGNMB",left=729, top=461, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 212719354;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_13 as StdRadio with uid="VNICEXLLTR",rtseq=7,rtrep=.f.,left=334, top=462, width=143,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_13.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 16785114
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 16785114
      this.Buttons(2).Top=15
      this.SetAll("Width",141)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_13.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_13.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_16 as cp_runprogram with uid="PWUVEHENLH",left=3, top=552, width=238,height=18,;
    caption='GSOF_BIO',;
   bGlobalFont=.t.,;
    prg="GSOF_BIO",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 139534005


  add object ZoomDett as cp_szoombox with uid="CBTDKTEQAT",left=266, top=121, width=520,height=335,;
    caption='CalcRi',;
   bGlobalFont=.t.,;
    bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="OFF_ERTE",cZoomFile="GSOFDOFF",bQueryOnDblClick=.f.,;
    cEvent = "CalcRig",;
    nPag=1;
    , HelpContextID = 23069658


  add object oObj_1_18 as cp_runprogram with uid="BNLBWFKVTM",left=3, top=518, width=238,height=18,;
    caption='GSOF_BI5',;
   bGlobalFont=.t.,;
    prg="GSOF_BI5('SEL')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 139533979


  add object oObj_1_19 as cp_runprogram with uid="YHDJKTNFOV",left=3, top=569, width=238,height=18,;
    caption='GSOF_BI4',;
   bGlobalFont=.t.,;
    prg="GSOF_BI4",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 139533978

  add object oDATOFF_1_24 as StdField with uid="GWTJAQXOOC",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATOFF", cQueryName = "DATOFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 87401418,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=105, Top=484

  add object oNUMOFF_1_26 as StdField with uid="KKGUBRQPRU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_NUMOFF", cQueryName = "NUMOFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 87424810,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=105, Top=460

  func oNUMOFF_1_26.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oSEROFF_1_27 as StdField with uid="SYCTVSBJUX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SEROFF", cQueryName = "SEROFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 87408346,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=179, Top=460, InputMask=replicate('X',2)

  func oSEROFF_1_27.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc


  add object oObj_1_30 as cp_runprogram with uid="IBSQNIMYXZ",left=3, top=535, width=238,height=18,;
    caption='GSOF_BI5',;
   bGlobalFont=.t.,;
    prg="GSOF_BI5('BQD')",;
    cEvent = "ZOOMDETT after query",;
    nPag=1;
    , HelpContextID = 139533979

  add object oNUMOFF_1_44 as StdField with uid="OCPTHDMOPV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NUMOFF", cQueryName = "NUMOFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 87424810,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=105, Top=460

  func oNUMOFF_1_44.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oSEROFF_1_45 as StdField with uid="VUARFUGFZH",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SEROFF", cQueryName = "SEROFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 87408346,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=240, Top=460, InputMask=replicate('X',10)

  func oSEROFF_1_45.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="BNCNHVKGYN",Visible=.t., Left=5, Top=101,;
    Alignment=0, Width=196, Height=18,;
    Caption="Elenco offerte"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="DCXZNKWDRF",Visible=.t., Left=266, Top=101,;
    Alignment=0, Width=116, Height=15,;
    Caption="Dettaglio righe"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="UPFFWQYPNA",Visible=.t., Left=11, Top=460,;
    Alignment=1, Width=92, Height=18,;
    Caption="Offerta n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="CDXVEDAPEG",Visible=.t., Left=167, Top=460,;
    Alignment=2, Width=13, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="BPQTZHQZBV",Visible=.t., Left=11, Top=484,;
    Alignment=1, Width=92, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="JLPTGYJBLO",Visible=.t., Left=590, Top=101,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Giallo)"    , ForeColor=RGB(255,255,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="KPEGKTKNKF",Visible=.t., Left=653, Top=101,;
    Alignment=0, Width=126, Height=17,;
    Caption="Articolo da definire"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="QRIBEUFOJG",Visible=.t., Left=8, Top=1,;
    Alignment=0, Width=99, Height=18,;
    Caption="Filtri selezioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="UHLUXWMHMZ",Visible=.t., Left=9, Top=21,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="IKVZJNXMGI",Visible=.t., Left=344, Top=48,;
    Alignment=1, Width=78, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="SPEYXYLJDU",Visible=.t., Left=18, Top=48,;
    Alignment=1, Width=72, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="AOAHKDPBNT",Visible=.t., Left=180, Top=48,;
    Alignment=1, Width=80, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="KSTGWWWJNO",Visible=.t., Left=12, Top=75,;
    Alignment=1, Width=78, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="XGQLZPURNB",Visible=.t., Left=224, Top=460,;
    Alignment=2, Width=13, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_kio','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
