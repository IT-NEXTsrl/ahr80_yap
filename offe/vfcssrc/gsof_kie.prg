* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_kie                                                        *
*              Parametri importazione modelli offerte                          *
*                                                                              *
*      Author: Pollina Fabrizio                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-15                                                      *
* Last revis.: 2012-08-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_kie",oParentObject))

* --- Class definition
define class tgsof_kie as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 533
  Height = 111
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-08-28"
  HelpContextID=267004265
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsof_kie"
  cComment = "Parametri importazione modelli offerte"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CAUDOC = space(5)
  w_DESCAU = space(35)
  w_CATDOC = space(2)
  w_FLINTE = space(1)
  w_FLVEAC = space(1)
  w_FLANAL = space(1)
  w_FLCOMM = space(1)
  w_CODVAL = space(3)
  w_CODLIS = space(5)
  w_DESLIS = space(40)
  w_VALLIS = space(3)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_IVALIS = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_SIMBOL = space(5)
  w_DECTOT = 0
  w_DECUNI = 0
  w_OBTEST = ctod('  /  /  ')
  w_FLSCOR = space(1)
  w_TESTAD = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_kiePag1","gsof_kie",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCAUDOC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='LISTINI'
    this.cWorkTables[3]='VALUTE'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CAUDOC=space(5)
      .w_DESCAU=space(35)
      .w_CATDOC=space(2)
      .w_FLINTE=space(1)
      .w_FLVEAC=space(1)
      .w_FLANAL=space(1)
      .w_FLCOMM=space(1)
      .w_CODVAL=space(3)
      .w_CODLIS=space(5)
      .w_DESLIS=space(40)
      .w_VALLIS=space(3)
      .w_INILIS=ctod("  /  /  ")
      .w_FINLIS=ctod("  /  /  ")
      .w_IVALIS=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_SIMBOL=space(5)
      .w_DECTOT=0
      .w_DECUNI=0
      .w_OBTEST=ctod("  /  /  ")
      .w_FLSCOR=space(1)
      .w_TESTAD=space(1)
      .w_CAUDOC=oParentObject.w_CAUDOC
      .w_CODVAL=oParentObject.w_CODVAL
      .w_CODLIS=oParentObject.w_CODLIS
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CAUDOC))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,7,.f.)
        .w_CODVAL = g_PERVAL
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODVAL))
          .link_1_9('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODLIS))
          .link_1_10('Full')
        endif
          .DoRTCalc(10,18,.f.)
        .w_OBTEST = i_datsys
        .w_FLSCOR = 'N'
        .w_TESTAD = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CAUDOC=.w_CAUDOC
      .oParentObject.w_CODVAL=.w_CODVAL
      .oParentObject.w_CODLIS=.w_CODLIS
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
          .link_1_10('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CAUDOC
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CAUDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCAUDOC_1_1'),i_cWhere,'',"Causale documento",'GSOF0ATD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLINTE,TDFLVEAC,TDFLANAL,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLCOMM = NVL(_Link_.TDFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_CATDOC = space(2)
      this.w_FLINTE = space(1)
      this.w_FLVEAC = space(1)
      this.w_FLANAL = space(1)
      this.w_FLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLVEAC='V' AND .w_FLINTE='C' AND (.w_CATDOC='OR' OR .w_CATDOC='OP' OR (.w_CATDOC='DI' AND EMPTY(.w_FLANAL) AND EMPTY(.w_FLCOMM)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_CAUDOC = space(5)
        this.w_DESCAU = space(35)
        this.w_CATDOC = space(2)
        this.w_FLINTE = space(1)
        this.w_FLVEAC = space(1)
        this.w_FLANAL = space(1)
        this.w_FLCOMM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_9'),i_cWhere,'',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADTOBSO,VASIMVAL,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_SIMBOL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_SIMBOL = space(5)
      this.w_DECTOT = 0
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE",CHKDTOBS(this, .w_DATOBSO, .w_OBTEST,"Valuta obsoleta alla data Attuale!"), CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
        endif
        this.w_CODVAL = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_SIMBOL = space(5)
        this.w_DECTOT = 0
        this.w_DECUNI = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLIS
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CODLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_IVALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODLIS) OR CHKLISD(.w_CODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_CODVAL, i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_IVALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCAUDOC_1_1.value==this.w_CAUDOC)
      this.oPgFrm.Page1.oPag.oCAUDOC_1_1.value=this.w_CAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_3.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_3.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_9.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_9.value=this.w_CODVAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CAUDOC)) or not(.w_FLVEAC='V' AND .w_FLINTE='C' AND (.w_CATDOC='OR' OR .w_CATDOC='OP' OR (.w_CATDOC='DI' AND EMPTY(.w_FLANAL) AND EMPTY(.w_FLCOMM)))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUDOC_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CAUDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   ((empty(.w_CODVAL)) or not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE",CHKDTOBS(this, .w_DATOBSO, .w_OBTEST,"Valuta obsoleta alla data Attuale!"), CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODVAL_1_9.SetFocus()
            i_bnoObbl = !empty(.w_CODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
          case   not(EMPTY(.w_CODLIS) OR CHKLISD(.w_CODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_CODVAL, i_datsys))  and not(empty(.w_CODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODLIS_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsof_kiePag1 as StdContainer
  Width  = 529
  height = 111
  stdWidth  = 529
  stdheight = 111
  resizeXpos=317
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCAUDOC_1_1 as StdField with uid="BVCBJPYEWL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CAUDOC", cQueryName = "CAUDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Causale del documento",;
    HelpContextID = 139422758,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=161, Top=19, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CAUDOC"

  func oCAUDOC_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUDOC_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUDOC_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCAUDOC_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causale documento",'GSOF0ATD.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oDESCAU_1_3 as StdField with uid="LWFBITUPFE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 158224438,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=226, Top=19, InputMask=replicate('X',35)

  add object oCODVAL_1_9 as StdField with uid="FIRGMYUWLD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o obsoleta o valuta esercizio non definita",;
    ToolTipText = "Codice valuta del modello",;
    HelpContextID = 8415782,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=161, Top=42, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc


  add object oBtn_1_21 as StdButton with uid="EXCMYMDFSJ",left=418, top=60, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'importazione";
    , HelpContextID = 92377110;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="IIRYAWBJZR",left=471, top=60, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 92377110;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="TLFOQLPISK",Visible=.t., Left=12, Top=22,;
    Alignment=1, Width=147, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="RMQNRPUFEO",Visible=.t., Left=12, Top=45,;
    Alignment=1, Width=147, Height=18,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_kie','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
