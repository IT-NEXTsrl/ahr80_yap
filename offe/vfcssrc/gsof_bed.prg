* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bed                                                        *
*              Generazione documento direttamente alla conferma                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_229]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-04                                                      *
* Last revis.: 2014-09-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bed",oParentObject,m.pParam)
return(i_retval)

define class tgsof_bed as StdBatch
  * --- Local variables
  pParam = space(1)
  w_OLDSTA = space(1)
  w_CONTAC = 0
  w_TIPAGG = space(1)
  w_OBJMDO = .NULL.
  w_NUMPRA = space(15)
  w_DATAPRE = ctod("  /  /  ")
  w_OFCODRES = space(5)
  w_CUR_PREST = space(0)
  w_GSAG_KPR = .NULL.
  w_GSAG_MPR = .NULL.
  w_GENPRE = space(1)
  w_READAZI = space(5)
  w_PAFLDESP = space(1)
  w_CODLISPRE = space(5)
  w_FLDTRP = space(1)
  w_DPGRUPRE = space(5)
  w_SETLINKED = .f.
  w_MaxRiga = 0
  * --- WorkFile variables
  OFF_NOMI_idx=0
  PAR_ALTE_idx=0
  CAN_TIER_idx=0
  OFF_ERTE_idx=0
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Documento direttamente alla Conferma (da GSOF_AOF)
    * --- Operazione: 'A' =Record Inserted , 'B' =Bottone Conferma
    * --- Definizione variabili
    if NOT EMPTY(this.oParentObject.w_OFSERIAL) 
      do case
        case this.pPARAM="A"
          if this.oParentObject.w_OFSTATUS $ "IA" AND NOT EMPTY(this.oParentObject.w_OFCODNOM) AND this.oParentObject.w_TIPNOM<>"C"
            * --- Se Nominativo e' da Valutare setta Prospect
            this.w_TIPAGG = iif(Isalt(),"T","P")
            * --- Write into OFF_NOMI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC(this.w_TIPAGG),'OFF_NOMI','NOTIPNOM');
                  +i_ccchkf ;
              +" where ";
                  +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                     )
            else
              update (i_cTable) set;
                  NOTIPNOM = this.w_TIPAGG;
                  &i_ccchkf. ;
               where;
                  NOCODICE = this.oParentObject.w_OFCODNOM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if this.oParentObject.w_OFSTATUS="C" AND this.oParentObject.w_OLDSTAT<>"C"
            this.oParentObject.NotifyEvent("GeneraDocumento")
          endif
        case this.pPARAM="B"
          this.oParentObject.w_BRES = .T.
          this.w_OLDSTA = this.oParentObject.w_OFSTATUS
          * --- Controllo se sono state inserite le righe di dettaglio
          *     Nel caso di opportunit� posso confermare una offerta senza righe
          *     di dettaglio
          if Isalt()
            this.w_OBJMDO = this.oParentObject.GSPR_MDO
          else
            this.w_OBJMDO = this.oParentObject.GSOF_MDO
          endif
          if this.w_OBJMDO.NumRow()<1
            ah_ErrorMsg("Inserire almeno una riga di dettaglio",48)
            i_retcode = 'stop'
            return
          endif
          if this.oParentObject.w_OFSTATUS="C"
            AH_ErrorMsg("Documento gi� generato",48)
            i_retcode = 'stop'
            return
          endif
          this.w_CONTAC = 0
          * --- Verifica se esistono Articoli da Confermare
          if !Isalt()
            SELECT (this.w_OBJMDO.cTrsName)
            COUNT FOR t_CPROWORD<>0 AND NOT EMPTY(t_ODCODICE) AND t_ODARNOCO=1 TO this.w_CONTAC
          endif
          if this.w_CONTAC<>0
            AH_ErrorMsg("Esistono ancora articoli da confermare%0� necessario procedere prima alla loro conferma",48)
            this.w_OBJMDO.NotifyEvent("ConfermaArticoli")
          else
            this.oParentObject.w_OFSTATUS = "C"
            this.oParentObject.NotifyEvent("ControlliFinali")
            this.oParentObject.w_OFSTATUS = this.w_OLDSTA
            if this.oParentObject.w_BRES=.T.
              this.oParentObject.NotifyEvent("GeneraDocumento")
              this.oParentObject.Loadrecwarn()
            else
              this.oParentObject.mCalc(.T.)
            endif
          endif
          * --- Read from OFF_ERTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OFCODPRA,OFDATDOC,OFCODRES"+;
              " from "+i_cTable+" OFF_ERTE where ";
                  +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OFCODPRA,OFDATDOC,OFCODRES;
              from (i_cTable) where;
                  OFSERIAL = this.oParentObject.w_OFSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NUMPRA = NVL(cp_ToDate(_read_.OFCODPRA),cp_NullValue(_read_.OFCODPRA))
            this.w_DATAPRE = NVL(cp_ToDate(_read_.OFDATDOC),cp_NullValue(_read_.OFDATDOC))
            this.w_OFCODRES = NVL(cp_ToDate(_read_.OFCODRES),cp_NullValue(_read_.OFCODRES))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if IsAlt() AND !EMPTY(this.w_NUMPRA) 
            * --- Carica prestazioni nell'inserimento provvisorio
            this.w_CUR_PREST = "CUR_PREST"
            vq_exec("..\offe\exe\query\Gsof_bed.vqr",this,"CUR_PREST")
            if RECCOUNT()>0 AND Ah_YesNo("Vuoi inserire le prestazioni nella pratica? (Le prestazioni saranno inserite come non fatturabili)")
              this.w_READAZI = i_CODAZI
              this.w_PAFLDESP = "S"
              * --- Read from PAR_ALTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PAGENPRE"+;
                  " from "+i_cTable+" PAR_ALTE where ";
                      +"PACODAZI = "+cp_ToStrODBC(this.w_READAZI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PAGENPRE;
                  from (i_cTable) where;
                      PACODAZI = this.w_READAZI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_GENPRE = NVL(cp_ToDate(_read_.PAGENPRE),cp_NullValue(_read_.PAGENPRE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from CAN_TIER
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAN_TIER_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CNCODLIS"+;
                  " from "+i_cTable+" CAN_TIER where ";
                      +"CNCODCAN = "+cp_ToStrODBC(this.w_NUMPRA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CNCODLIS;
                  from (i_cTable) where;
                      CNCODCAN = this.w_NUMPRA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODLISPRE = NVL(cp_ToDate(_read_.CNCODLIS),cp_NullValue(_read_.CNCODLIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Assegna la data della prestazione precedente - nel nostro caso vale 'S' perch� tutte hanno la stessa data
              this.w_FLDTRP = "S"
              * --- Read from DIPENDEN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DIPENDEN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DPGRUPRE"+;
                  " from "+i_cTable+" DIPENDEN where ";
                      +"DPCODICE = "+cp_ToStrODBC(this.w_OFCODRES);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DPGRUPRE;
                  from (i_cTable) where;
                      DPCODICE = this.w_OFCODRES;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DPGRUPRE = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Lancia la maschera di Inserimento provvisorio delle prestazioni
              this.w_GSAG_KPR = GSAG_KPR(this.w_GENPRE)
              this.w_GSAG_KPR.w_CodPratica = this.w_NUMPRA
              this.w_GSAG_KPR.w_ATCODPRA = this.w_NUMPRA
              this.w_SETLINKED = SetValueLinked("M", this.w_GSAG_KPR, "w_CodPratica", this.w_NUMPRA)
              this.w_GSAG_KPR.w_EditCodPra = .F.
              * --- Gestiamo anche gli o_ in modo che non chieda di salvare
              this.w_GSAG_KPR.o_CodResp = this.w_OFCODRES
              this.w_GSAG_KPR.w_CodResp = this.w_OFCODRES
              this.w_SETLINKED = SetValueLinked("M", this.w_GSAG_KPR, "w_CODRESP",this.w_OFCODRES)
              this.w_GSAG_KPR.o_GRUPART = this.w_DPGRUPRE
              this.w_GSAG_KPR.w_GRUPART = this.w_DPGRUPRE
              this.w_SETLINKED = SetValueLinked("M", this.w_GSAG_KPR, "w_GRUPART",this.w_DPGRUPRE)
              AggioProvv(this.w_GSAG_KPR,"Z")
              * --- Non devono essere gestite le spese e le anticipazioni collegate alla prestazione
              this.w_GSAG_MPR = this.w_GSAG_KPR.GSAG_MPR
              * --- Memorizziamo il numero di righe gi� presenti, perch� dovranno essere deselezionate
              this.w_GSAG_MPR.Loadrec()     
              this.w_MaxRiga = this.w_GSAG_MPR.NumRow()
              this.w_GSAG_MPR.w_FL_BUP = "S"
              * --- Valorizzo la pratica in GSAG_KPR
              * --- Gestiamo anche gli o_ in modo che non chieda di salvare
              * --- Prima di tutto deselezioniamo le prestazioni gi� inserite
              do GSAG_BIP with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Ripristiniamo il normale funzionamento di associazione di spese e anticipazioni alla prestazione
              this.w_GSAG_MPR.w_FL_BUP = "N"
              * --- Visto che tutte le righe sono selezionate, deselezioniamo le prima che abbiamo gi� trovato come inserite
              this.w_GSAG_MPR.w_DESEL_BUP = this.w_MaxRiga
              this.w_GSAG_MPR.NotifyEvent("Deseleziona_BUP")     
            endif
            USE IN SELECT("CUR_PREST")
          endif
        case this.pPARAM="C"
          this.oParentObject.w_OFSTATUS = "C"
          this.oParentObject.NotifyEvent("ControlliFinali")
          this.oParentObject.w_OFSTATUS = this.w_OLDSTA
          if this.oParentObject.w_BRES=.T.
            this.oParentObject.NotifyEvent("GeneraDocumento")
          else
            this.oParentObject.mCalc(.T.)
          endif
      endcase
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='PAR_ALTE'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='OFF_ERTE'
    this.cWorkTables[5]='DIPENDEN'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
