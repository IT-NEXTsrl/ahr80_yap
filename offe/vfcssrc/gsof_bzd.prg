* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bzd                                                        *
*              Attiva versione precedente                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2012-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bzd",oParentObject)
return(i_retval)

define class tgsof_bzd as StdBatch
  * --- Local variables
  w_NUMVER = 0
  w_MESS = space(10)
  w_APPO = space(10)
  w_ALSERIAL = space(10)
  w_ALTIPALL = space(1)
  w_ALPATFIL = space(255)
  w_Periodo = ctod("  /  /  ")
  w_Pat = space(254)
  w_CritPeri = space(1)
  w_ArcNom = space(1)
  w_RagSoc = space(40)
  w_ERRORE = .f.
  w_PATHALL = space(254)
  w_RagSoc = space(100)
  w_ArcNom = space(1)
  * --- WorkFile variables
  NOM_CONT_idx=0
  RUO_CONT_idx=0
  OFF_ERTE_idx=0
  DOC_MAST_idx=0
  ALL_EGAT_idx=0
  ALL_ATTR_idx=0
  PAR_OFFE_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- In caso di delete versione offerta ,riabilita versione precedente (da GSOF_AOF)
    if NOT EMPTY(this.oParentObject.w_OFRIFDOC)
      this.w_APPO = SPACE(10)
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERIAL"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFRIFDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERIAL;
          from (i_cTable) where;
              MVSERIAL = this.oParentObject.w_OFRIFDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_APPO = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0 AND NOT EMPTY(this.w_APPO)
        this.w_MESS = ah_msgformat("Offerta associata ad un documento di conferma: impossibile eliminare")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    endif
    if this.oParentObject.w_OFSTATUS = "V"
      this.w_MESS = ah_Msgformat("Impossibile eliminare versione chiusa")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    else
      * --- Elimina eventuali allegati
      if NOT EMPTY(this.oParentObject.w_OFSERIAL)
        * --- Select from ALL_EGAT
        i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2],.t.,this.ALL_EGAT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ALL_EGAT ";
              +" where ALRIFOFF="+cp_ToStrODBC(this.oParentObject.w_OFSERIAL)+"";
               ,"_Curs_ALL_EGAT")
        else
          select * from (i_cTable);
           where ALRIFOFF=this.oParentObject.w_OFSERIAL;
            into cursor _Curs_ALL_EGAT
        endif
        if used('_Curs_ALL_EGAT')
          select _Curs_ALL_EGAT
          locate for 1=1
          do while not(eof())
          this.w_ALSERIAL = NVL(_Curs_ALL_EGAT.ALSERIAL, "")
          this.w_Periodo = CP_TODATE(_Curs_ALL_EGAT.ALDATREG)
          this.w_ALTIPALL = NVL(_Curs_ALL_EGAT.ALTIPALL, "L")
          this.w_ALPATFIL = NVL(_Curs_ALL_EGAT.ALPATFIL, "")
          if NOT EMPTY(this.w_ALSERIAL)
            * --- Delete from ALL_ATTR
            i_nConn=i_TableProp[this.ALL_ATTR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ALL_ATTR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"AACODICE = "+cp_ToStrODBC(this.w_ALSERIAL);
                     )
            else
              delete from (i_cTable) where;
                    AACODICE = this.w_ALSERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          if this.w_ALTIPALL="F" AND NOT EMPTY(this.w_ALPATFIL)
            * --- Elimina File Allegato
            this.w_Pat = ""
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PAT = ALLTRIM(this.w_PAT)
            if NOT EMPTY(this.w_PAT)
              this.w_PAT = this.w_PAT + IIF(RIGHT(this.w_PAT, 1)="\", "", "\") + ALLTRIM(this.w_ALPATFIL)
              DELFIL = ALLTRIM(this.w_PAT)
              if FILE(DELFIL)
                DELETE FILE (DELFIL)
              endif
            endif
          endif
            select _Curs_ALL_EGAT
            continue
          enddo
          use
        endif
        * --- Delete from ALL_EGAT
        i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"ALRIFOFF = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
                 )
        else
          delete from (i_cTable) where;
                ALRIFOFF = this.oParentObject.w_OFSERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      if this.oParentObject.w_OFNUMVER > 1
        * --- Write into OFF_ERTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OFSTATUS ="+cp_NullLink(cp_ToStrODBC("I"),'OFF_ERTE','OFSTATUS');
          +",OFDATINV ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'OFF_ERTE','OFDATINV');
          +",OFDATCHI ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'OFF_ERTE','OFDATCHI');
              +i_ccchkf ;
          +" where ";
              +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERPRE);
                 )
        else
          update (i_cTable) set;
              OFSTATUS = "I";
              ,OFDATINV = cp_CharToDate("  -  -  ");
              ,OFDATCHI = cp_CharToDate("  -  -  ");
              &i_ccchkf. ;
           where;
              OFSERIAL = this.oParentObject.w_OFSERPRE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from OFF_ERTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OFNUMVER"+;
            " from "+i_cTable+" OFF_ERTE where ";
                +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERPRE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OFNUMVER;
            from (i_cTable) where;
                OFSERIAL = this.oParentObject.w_OFSERPRE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NUMVER = NVL(cp_ToDate(_read_.OFNUMVER),cp_NullValue(_read_.OFNUMVER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        ah_ErrorMsg("Eliminata versione offerta n. %1%0Riaperta versione precedente offerta n. %2","i","", ALLTRIM(STR(this.oParentObject.w_OFNUMVER)), ALLTRIM(STR(this.w_NUMVER)) )
      endif
      * --- Read from PAR_OFFE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "POFLCAAR"+;
          " from "+i_cTable+" PAR_OFFE where ";
              +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          POFLCAAR;
          from (i_cTable) where;
              POCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ArcNom = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_ArcNom="S"
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NODESCRI"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_OFCODNOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NODESCRI;
            from (i_cTable) where;
                NOCODICE = this.oParentObject.w_OFCODNOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RagSoc = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PATHALL = AddBs(FULLPATH(ALLTRIM(this.oParentObject.w_PATHARC) +alltrim(this.w_RagSoc)))
      else
        this.w_PATHALL = AddBs(FULLPATH(ALLTRIM(this.oParentObject.w_PATHARC)))
      endif
      do case
        case this.oParentObject.w_PERIODO="M"
          this.w_PATHALL = this.w_PATHALL+ALLTRIM(CMONTH(this.oParentObject.w_OFDATDOC))+"\"
        case this.oParentObject.w_PERIODO="T"
          do case
            case MONTH(this.oParentObject.w_OFDATDOC)<=3
              this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Trimestre1\")
            case MONTH(this.oParentObject.w_OFDATDOC)>3 AND MONTH(this.oParentObject.w_OFDATDOC)<=6
              this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Trimestre2\")
            case MONTH(this.oParentObject.w_OFDATDOC)>6 AND MONTH(this.oParentObject.w_OFDATDOC)<=9
              this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Trimestre3\")
            case MONTH(this.oParentObject.w_OFDATDOC)>9 AND MONTH(this.oParentObject.w_OFDATDOC)<=12
              this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Trimestre4\")
          endcase
        case this.oParentObject.w_PERIODO="Q"
          do case
            case MONTH(this.oParentObject.w_OFDATDOC)<=4
              this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Quadrimestre1\")
            case MONTH(this.oParentObject.w_OFDATDOC)>4 AND MONTH(this.oParentObject.w_OFDATDOC)<=8
              this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Quadrimestre2\")
            case MONTH(this.oParentObject.w_OFDATDOC)>8 AND MONTH(this.oParentObject.w_OFDATDOC)<=12
              this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Quadrimestre3\")
          endcase
        case this.oParentObject.w_PERIODO="S"
          do case
            case MONTH(this.oParentObject.w_OFDATDOC)<=6
              this.w_PATHALL = this.w_PATHALL+ah_Msgformat("Semestre1\")
            case MONTH(this.oParentObject.w_OFDATDOC)>6 AND MONTH(this.oParentObject.w_OFDATDOC)<=12
              this.w_PATHALL = this.w_PATHALL+ ah_Msgformat("Semestre2\")
          endcase
        case this.oParentObject.w_PERIODO="A"
          this.w_PATHALL = this.w_PATHALL+ALLTRIM(STR(YEAR(this.oParentObject.w_OFDATDOC)))+"\"
      endcase
      if not empty(this.oParentObject.w_OFPATFWP) and Cp_fileexist(Alltrim(this.w_PATHALL) + Alltrim(this.oParentObject.w_OFPATFWP))
        if Ah_yesno("Si desidera procedere con l'eliminazione del documento Word?")
          TmpPat=Alltrim(this.w_PATHALL) + Alltrim(this.oParentObject.w_OFPATFWP)
          this.w_ERRORE = .F.
          w_ErrorHandler = on("ERROR")
          on error this.w_ERRORE = .T.
          delete file (TmpPat)
          on error &w_ErrorHandler
        endif
      endif
      if ! this.w_ERRORE and not empty(this.oParentObject.w_OFPATPDF) and Cp_fileexist(Alltrim(this.w_PATHALL) + Alltrim(this.oParentObject.w_OFPATPDF))
        if Ah_yesno("Si desidera procedere con l'eliminazione del documento PDF?")
          TmpPat=Alltrim(this.w_PATHALL) + Alltrim(this.oParentObject.w_OFPATPDF)
          this.w_ERRORE = .F.
          w_ErrorHandler = on("ERROR")
          on error this.w_ERRORE = .T.
          delete file (TmpPat)
          on error &w_ErrorHandler
        endif
      endif
      if this.w_ERRORE
        this.w_MESS = ah_Msgformat("Errore eliminazione file documento collegato")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Periodo
    * --- Calcolo nome directory memorizzazione allegati
    * --- Read from PAR_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "POPATOFF,POPEROFF,POFLCAAR"+;
        " from "+i_cTable+" PAR_OFFE where ";
            +"POCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        POPATOFF,POPEROFF,POFLCAAR;
        from (i_cTable) where;
            POCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_Pat = NVL(cp_ToDate(_read_.POPATOFF),cp_NullValue(_read_.POPATOFF))
      this.w_CritPeri = NVL(cp_ToDate(_read_.POPEROFF),cp_NullValue(_read_.POPEROFF))
      this.w_ArcNom = NVL(cp_ToDate(_read_.POFLCAAR),cp_NullValue(_read_.POFLCAAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if right(alltrim(this.w_Pat),1)="\"
      this.w_Pat = LEFT(ALLTRIM(this.w_Pat),LEN(ALLTRIM(this.w_Pat))-1)+"\" 
    endif
    if this.w_ArcNom="S"
      this.w_Pat = alltrim(this.w_Pat)+alltrim(this.w_RagSoc)+"\" 
    else
      this.w_Pat = alltrim(this.w_Pat)
    endif
    do case
      case this.w_CritPeri = "M"
        * --- Raggruppamento per mesi
        this.w_Pat = this.w_Pat + right("00"+alltrim(str(month(this.oParentObject.w_Periodo),2,0)), 2) + str(year(this.oParentObject.w_Periodo),4,0)
      case this.w_CritPeri = "T"
        * --- Raggruppamento per trimestri
        do case
          case month(this.oParentObject.w_Periodo) > 0 .and. month(this.oParentObject.w_Periodo) < 4
            this.w_Pat = this.w_Pat + "T1" + str(year(this.oParentObject.w_Periodo),4,0)
          case month(this.oParentObject.w_Periodo) > 3 .and. month(this.oParentObject.w_Periodo) < 7
            this.w_Pat = this.w_Pat + "T2" + str(year(this.oParentObject.w_Periodo),4,0)
          case month(this.oParentObject.w_Periodo) > 6 .and. month(this.oParentObject.w_Periodo) < 10
            this.w_Pat = this.w_Pat + "T3" + str(year(this.oParentObject.w_Periodo),4,0)
          case month(this.oParentObject.w_Periodo) > 9
            this.w_Pat = this.w_Pat + "T4" + str(year(this.oParentObject.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "Q"
        * --- Raggruppamento per quadrimestri
        do case
          case month(this.oParentObject.w_Periodo) > 0 .and. month(this.oParentObject.w_Periodo) < 5
            this.w_Pat = this.w_Pat + "Q1" + str(year(this.oParentObject.w_Periodo),4,0)
          case month(this.oParentObject.w_Periodo) > 4 .and. month(this.oParentObject.w_Periodo) < 9
            this.w_Pat = this.w_Pat + "Q2" + str(year(this.oParentObject.w_Periodo),4,0)
          case month(this.oParentObject.w_Periodo) > 8
            this.w_Pat = this.w_Pat + "Q3" + str(year(this.oParentObject.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "S"
        * --- Raggruppamento per semestri
        this.w_Pat = this.w_Pat + iif( month(this.oParentObject.w_Periodo) >6 , "S2", "S1") + str(year(this.oParentObject.w_Periodo),4,0)
      case this.w_CritPeri = "A"
        * --- Raggruppamento per anni
        this.w_Pat = this.w_Pat + str(year(this.oParentObject.w_Periodo),4,0)
    endcase
    this.w_Pat = this.w_Pat+"\" 
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='NOM_CONT'
    this.cWorkTables[2]='RUO_CONT'
    this.cWorkTables[3]='OFF_ERTE'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='ALL_EGAT'
    this.cWorkTables[6]='ALL_ATTR'
    this.cWorkTables[7]='PAR_OFFE'
    this.cWorkTables[8]='OFF_NOMI'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_ALL_EGAT')
      use in _Curs_ALL_EGAT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
