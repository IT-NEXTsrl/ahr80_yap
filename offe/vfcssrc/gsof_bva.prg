* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bva                                                        *
*              Visualizzazione allegati                                        *
*                                                                              *
*      Author: Nunzio Iorio                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_297]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2008-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bva",oParentObject)
return(i_retval)

define class tgsof_bva as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  w_NODESCRI = space(60)
  w_ALOGGALL = space(50)
  w_ALTIPALL = space(1)
  w_ALPATFIL = space(254)
  w_ALDATREG = ctod("  /  /  ")
  w_ALRIFNOM = space(20)
  w_OFNUMDOC = 0
  w_OFSERDOC = space(10)
  w_OFDATDOC = ctod("  /  /  ")
  w_OFNUMVER = 0
  w_ALRIFMOD = space(5)
  w_ALSERIAL = space(10)
  w_ALRIFOFF = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIATO DA RICERCA  (da GSOF_KRA)
    * --- Variabile che conterr� il nome dello Zoom
    * --- Variabile che conterr� l'ordinamento
    this.w_ZOOM = this.oParentObject.w_ZoomAll
    NC = this.w_ZOOM.cCursor
    * --- check = E (empty)  per query vuota
    this.oParentObject.w_CHECK = "E"
    This.oParentObject.NotifyEvent("Ricerca")
    * --- check = F (full)  per query piena
    this.oParentObject.w_CHECK = "F"
    vq_exec("..\offe\exe\query\Gsof_kra.vqr",this,"CUR1")
    if USED("CUR1")
      if this.oParentObject.w_FLGALL ="S"
        do case
          case this.oParentObject.w_ORDALL = "O"
            Select * from CUR1 GROUP BY CUR1.ALOGGALL,CUR1.ALTIPALL,CUR1.ALPATFIL into cursor CUR2 order by ALOGGALL, ALDATREG DESC
          case this.oParentObject.w_ORDALL = "N"
            Select * from CUR1 GROUP BY CUR1.ALOGGALL,CUR1.ALTIPALL,CUR1.ALPATFIL into cursor CUR2 order by ALPATFIL, ALDATREG DESC
          case this.oParentObject.w_ORDALL = "D"
            Select * from CUR1 GROUP BY CUR1.ALOGGALL,CUR1.ALTIPALL,CUR1.ALPATFIL into cursor CUR2 order by ALDATREG
          case this.oParentObject.w_ORDALL = "T"
            Select * from CUR1 GROUP BY CUR1.ALOGGALL,CUR1.ALTIPALL,CUR1.ALPATFIL into cursor CUR2 order by ALTIPALL, ALDATREG DESC
        endcase
      else
        do case
          case this.oParentObject.w_ORDALL = "O"
            Select * from CUR1 GROUP BY CUR1.ALSERIAL into cursor CUR2 ORDER BY ALOGGALL, ALDATREG DESC
          case this.oParentObject.w_ORDALL = "N"
            Select * from CUR1 GROUP BY CUR1.ALSERIAL into cursor CUR2 ORDER BY ALPATFIL, ALDATREG DESC
          case this.oParentObject.w_ORDALL = "D"
            Select * from CUR1 GROUP BY CUR1.ALSERIAL into cursor CUR2 ORDER BY ALDATREG
          case this.oParentObject.w_ORDALL = "T"
            Select * from CUR1 GROUP BY CUR1.ALSERIAL into cursor CUR2 ORDER BY ALTIPALL, ALDATREG DESC
        endcase
      endif
      do case
        case this.oParentObject.w_ASSOC = "N"
          Select * from CUR2 into cursor CUR3 where CUR2.ALRIFNOM <> " "
        case this.oParentObject.w_ASSOC = "O"
          Select * from CUR2 into cursor CUR3 where CUR2.ALRIFOFF <> " "
        case this.oParentObject.w_ASSOC = "M"
          Select * from CUR2 into cursor CUR3 where CUR2.ALRIFMOD <> " "
        case this.oParentObject.w_ASSOC = "T"
          Select * from CUR2 into cursor CUR3
      endcase
      do case
        case this.oParentObject.w_TIPO = "C"
          Select * from CUR3 into cursor CURN where CUR3.ALTIPALL like ah_msgformat("Collegamento")
        case this.oParentObject.w_TIPO = "F"
          Select * from CUR3 into cursor CURN where CUR3.ALTIPALL like ah_Msgformat("Copia file")
        case this.oParentObject.w_TIPO = "T"
          Select * from CUR3 into cursor CURN
      endcase
      SELECT CURN
      GO TOP
      SCAN 
      this.w_ALOGGALL = CURN.ALOGGALL
      this.w_ALTIPALL = CURN.ALTIPALL
      this.w_ALPATFIL = CURN.ALPATFIL
      this.w_ALDATREG = CURN.ALDATREG
      this.w_ALRIFNOM = CURN.ALRIFNOM
      this.w_NODESCRI = CURN.NODESCRI
      this.w_OFNUMDOC = CURN.OFNUMDOC
      this.w_OFSERDOC = CURN.OFSERDOC
      this.w_OFDATDOC = CURN.OFDATDOC
      this.w_OFNUMVER = CURN.OFNUMVER
      this.w_ALRIFMOD = CURN.ALRIFMOD
      this.w_ALSERIAL = CURN.ALSERIAL
      this.w_ALRIFOFF = CURN.ALRIFOFF
      INSERT INTO (NC) ;
      (ALOGGALL,ALTIPALL,ALPATFIL,ALDATREG,ALRIFNOM,NODESCRI,OFNUMDOC,OFSERDOC,OFDATDOC,OFNUMVER,ALRIFMOD,ALSERIAL,ALRIFOFF) ;
      VALUES (this.w_ALOGGALL,this.w_ALTIPALL,this.w_ALPATFIL,this.w_ALDATREG,this.w_ALRIFNOM,this.w_NODESCRI,this.w_OFNUMDOC,this.w_OFSERDOC,this.w_OFDATDOC,this.w_OFNUMVER,this.w_ALRIFMOD,this.w_ALSERIAL,this.w_ALRIFOFF) 
      ENDSCAN
    endif
    select (NC)
    go top
    * --- Refresh della griglia
    this.w_zoom.refresh()
    * --- chiude i cursori
    if USED("CUR1")
      SELECT CUR1
      USE
    endif
    if USED("CUR2")
      SELECT CUR2
      USE
    endif
    if USED("CUR3")
      SELECT CUR3
      USE
    endif
    if USED("CURN")
      SELECT CURN
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
