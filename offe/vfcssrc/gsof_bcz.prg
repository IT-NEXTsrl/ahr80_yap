* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bcz                                                        *
*              Check cancellazione zona                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-28                                                      *
* Last revis.: 2005-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bcz",oParentObject)
return(i_retval)

define class tgsof_bcz as StdBatch
  * --- Local variables
  * --- WorkFile variables
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo cancellazione zone
    this.oParentObject.w_OKOFFE = .F.
    * --- Select from OFF_NOMI
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFF_NOMI ";
          +" where NOCODZON="+cp_ToStrODBC(this.oParentObject.w_ZOCODZON)+"";
           ,"_Curs_OFF_NOMI")
    else
      select * from (i_cTable);
       where NOCODZON=this.oParentObject.w_ZOCODZON;
        into cursor _Curs_OFF_NOMI
    endif
    if used('_Curs_OFF_NOMI')
      select _Curs_OFF_NOMI
      locate for 1=1
      do while not(eof())
      * --- Esistono nominativi con il codice zona associato: impossibile cancellare
      this.oParentObject.w_OKOFFE = .T.
        select _Curs_OFF_NOMI
        continue
      enddo
      use
    endif
    if this.oParentObject.w_OKOFFE
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=AH_MSGFORMAT("Zona associata ad un nominativo offerte%0Impossibile cancellare")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_NOMI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_OFF_NOMI')
      use in _Curs_OFF_NOMI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
