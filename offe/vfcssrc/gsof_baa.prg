* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_baa                                                        *
*              Apre offerta su allegati da documento                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-05                                                      *
* Last revis.: 2010-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_baa",oParentObject,m.pOPER,m.pSERIAL)
return(i_retval)

define class tgsof_baa as StdBatch
  * --- Local variables
  pOPER = space(5)
  pSERIAL = space(10)
  w_OFSERIAL = space(10)
  w_GSOF_AOF = .NULL.
  * --- WorkFile variables
  OFF_ERTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OFSERIAL = ""
    * --- Read from OFF_ERTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OFSERIAL"+;
        " from "+i_cTable+" OFF_ERTE where ";
            +"OFRIFDOC = "+cp_ToStrODBC(this.pSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OFSERIAL;
        from (i_cTable) where;
            OFRIFDOC = this.pSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OFSERIAL = NVL(cp_ToDate(_read_.OFSERIAL),cp_NullValue(_read_.OFSERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_OFSERIAL)
      ah_ErrorMsg("Nessuna offerta collegata al documento.")
    else
      this.w_GSOF_AOF = GSOF_AOF()
      if this.w_GSOF_AOF.bSec1
        this.w_GSOF_AOF.ecpFilter()     
        this.w_GSOF_AOF.w_OFSERIAL = this.w_OFSERIAL
        this.w_GSOF_AOF.ecpSave()     
        do case
          case this.pOPER="AA"
            this.w_GSOF_AOF.oPgFrm.ActivePage = 5
        endcase
      endif
    endif
  endproc


  proc Init(oParentObject,pOPER,pSERIAL)
    this.pOPER=pOPER
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_ERTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pSERIAL"
endproc
