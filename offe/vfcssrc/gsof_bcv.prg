* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bcv                                                        *
*              Cambio stato offerta                                            *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-04                                                      *
* Last revis.: 2014-01-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bcv",oParentObject)
return(i_retval)

define class tgsof_bcv as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_STATO = space(1)
  w_PROG = .NULL.
  TMPc = space(10)
  * --- WorkFile variables
  OFF_ERTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cambia versione se l'offerta � in stato Inviata o In corso (da GSOF_AOF)
    * --- Definizione variabili
    if NOT EMPTY(this.oParentObject.w_OFRIFDOC)
      if Isalt()
        AH_ErrorMsg("Preventivo confermato. Impossibile proseguire",48)
      else
        AH_ErrorMsg("Offerta confermata. Impossibile proseguire",48)
      endif
      i_retcode = 'stop'
      return
    endif
    this.w_SERIAL = this.oParentObject.w_OFSERIAL
    * --- Controllo se la versione � gi� stata chiusa
    * --- Read from OFF_ERTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OFSTATUS"+;
        " from "+i_cTable+" OFF_ERTE where ";
            +"OFSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OFSTATUS;
        from (i_cTable) where;
            OFSERIAL = this.w_SERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_STATO = NVL(cp_ToDate(_read_.OFSTATUS),cp_NullValue(_read_.OFSTATUS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_STATO $ "CVRS"
      i_retcode = 'stop'
      return
    endif
    * --- chiusura versione
    * --- lascia in interrogazione ...
    this.oParentObject.ecpQuery()
    this.oParentObject.w_OFSERIAL = this.w_SERIAL
    this.TMPc = "OFSERIAL="+cp_ToStrODBC(this.w_SERIAL)
    this.oParentObject.QueryKeySet(this.TmpC,"")
    this.oParentObject.LoadRecWarn()
    this.oParentObject.w_OFSTATUS = "V"
    this.oParentObject.NotifyEvent("w_OFSTATUS Changed")
    * --- Nascondo bottoni
    This.OPARENTOBJECT.mHideControls()
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_ERTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
