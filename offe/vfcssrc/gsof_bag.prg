* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bag                                                        *
*              Cambio agente                                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-03                                                      *
* Last revis.: 2005-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bag",oParentObject)
return(i_retval)

define class tgsof_bag as StdBatch
  * --- Local variables
  * --- WorkFile variables
  AGENTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cambio Agente 
    if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
       CHKDTOBS(this, this.oParentObject.w_DTOBAGE, this.oParentObject.w_OBTEST, "Agente obsoleto alla data attuale")
    else
       CHKDTOBS(this.oParentObject.w_DTOBAGE,this.oParentObject.w_OBTEST,"Agente obsoleto alla data attuale", .T.)
    endif
    if NOT EMPTY(this.oParentObject.w_DTOBAGE) AND this.oParentObject.w_OBTEST > this.oParentObject.w_DTOBAGE
      this.oParentObject.w_OFCODAGE = SPACE(5)
      this.oParentObject.w_DESAGE = SPACE(35)
      this.oParentObject.w_OFCODAG2 = SPACE(5)
    endif
    if EMPTY(this.oParentObject.w_OFCODAGE)
      this.oParentObject.w_OFCODAG2 = SPACE(5)
      this.oParentObject.w_DESCAPO = SPACE(35)
    else
      if this.oParentObject.w_CODAG2<>this.oParentObject.w_OFCODAG2
        if AH_YesNo("Assegno il capoarea dell'agente selezionato?")
          this.oParentObject.w_OFCODAG2 = this.oParentObject.w_CODAG2
          * --- Read from AGENTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AGDESAGE"+;
              " from "+i_cTable+" AGENTI where ";
                  +"AGCODAGE = "+cp_ToStrODBC(this.oParentObject.w_OFCODAG2);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AGDESAGE;
              from (i_cTable) where;
                  AGCODAGE = this.oParentObject.w_OFCODAG2;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCAPO = NVL(cp_ToDate(_read_.AGDESAGE),cp_NullValue(_read_.AGDESAGE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AGENTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
