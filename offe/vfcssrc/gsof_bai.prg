* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bai                                                        *
*              Aggiorna partecipanti                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-12                                                      *
* Last revis.: 2010-05-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bai",oParentObject,m.pEXEC)
return(i_retval)

define class tgsof_bai as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_PADRE = .NULL.
  w_CODAZI = space(5)
  w_GRUPPO = space(10)
  w_PAFLVISI = space(1)
  w_OKPERS = .f.
  w_MESS = space(100)
  * --- WorkFile variables
  OFF_PART_idx=0
  PAR_AGEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da GSOF_AAT esegue aggiornamento e inserimento partecipanti
    *     se attivata attivit� e servizi
    * --- A  aggiorna partecipanti
    *     C  esegue controlli
    *     D  esegue delete dei partecipanti
    this.w_PADRE = This.oparentobject
    this.w_CODAZI = i_CODAZI
    if g_AGEN="S"
      * --- Read from PAR_AGEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAFLVISI"+;
          " from "+i_cTable+" PAR_AGEN where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAFLVISI;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PAFLVISI = NVL(cp_ToDate(_read_.PAFLVISI),cp_NullValue(_read_.PAFLVISI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do case
        case this.pEXEC="A"
          * --- Delete from OFF_PART
          i_nConn=i_TableProp[this.OFF_PART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PASERIAL = "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL);
                   )
          else
            delete from (i_cTable) where;
                  PASERIAL = this.oParentObject.w_ATSERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Select from gsof_bai
          do vq_exec with 'gsof_bai',this,'_Curs_gsof_bai','',.f.,.t.
          if used('_Curs_gsof_bai')
            select _Curs_gsof_bai
            locate for 1=1
            do while not(eof())
            this.w_GRUPPO = IIF(this.w_PAFLVISI<>"L",iif(Not Empty(Nvl(_Curs_gsof_bai.DPGRUPRE,"")),_Curs_gsof_bai.DPGRUPRE,_Curs_gsof_bai.SRCODICE) ,"")
            * --- Insert into OFF_PART
            i_nConn=i_TableProp[this.OFF_PART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_PART_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PASERIAL"+",CPROWNUM"+",CPROWORD"+",PATIPRIS"+",PACODRIS"+",PAGRURIS"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATSERIAL),'OFF_PART','PASERIAL');
              +","+cp_NullLink(cp_ToStrODBC(1),'OFF_PART','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(10),'OFF_PART','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC("P"),'OFF_PART','PATIPRIS');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_gsof_bai.DPCODICE),'OFF_PART','PACODRIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_GRUPPO),'OFF_PART','PAGRURIS');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PASERIAL',this.oParentObject.w_ATSERIAL,'CPROWNUM',1,'CPROWORD',10,'PATIPRIS',"P",'PACODRIS',_Curs_gsof_bai.DPCODICE,'PAGRURIS',this.w_GRUPPO)
              insert into (i_cTable) (PASERIAL,CPROWNUM,CPROWORD,PATIPRIS,PACODRIS,PAGRURIS &i_ccchkf. );
                 values (;
                   this.oParentObject.w_ATSERIAL;
                   ,1;
                   ,10;
                   ,"P";
                   ,_Curs_gsof_bai.DPCODICE;
                   ,this.w_GRUPPO;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
              select _Curs_gsof_bai
              continue
            enddo
            use
          endif
        case this.pEXEC="D"
          * --- Delete from OFF_PART
          i_nConn=i_TableProp[this.OFF_PART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PASERIAL = "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL);
                   )
          else
            delete from (i_cTable) where;
                  PASERIAL = this.oParentObject.w_ATSERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        otherwise
          * --- Select from gsof_bai
          do vq_exec with 'gsof_bai',this,'_Curs_gsof_bai','',.f.,.t.
          if used('_Curs_gsof_bai')
            select _Curs_gsof_bai
            locate for 1=1
            do while not(eof())
            this.w_OKPERS = .t.
            this.w_GRUPPO = Nvl(IIF(this.w_PAFLVISI<>"L",iif(Not Empty(_Curs_gsof_bai.DPGRUPRE),_Curs_gsof_bai.DPGRUPRE,_Curs_gsof_bai.SRCODICE) ,""),"")
              select _Curs_gsof_bai
              continue
            enddo
            use
          endif
          if ! this.w_OKPERS
            this.w_MESS = Ah_Msgformat("Attenzione l'operatore deve essere associato ad una persona")
          endif
          if Empty(this.w_GRUPPO) and this.w_PAFLVISI<>"L" and this.w_OKPERS
            this.w_MESS = Ah_Msgformat("Attenzione nessun gruppo collegato alla persona associata all'operatore")
            this.w_OKPERS = .f.
          endif
          if ! this.w_OKPERS
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
      endcase
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFF_PART'
    this.cWorkTables[2]='PAR_AGEN'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_gsof_bai')
      use in _Curs_gsof_bai
    endif
    if used('_Curs_gsof_bai')
      use in _Curs_gsof_bai
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
