* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bdm                                                        *
*              Duplica modello offerta                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-14                                                      *
* Last revis.: 2008-03-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bdm",oParentObject)
return(i_retval)

define class tgsof_bdm as StdBatch
  * --- Local variables
  * --- WorkFile variables
  MOD_OFFE_idx=0
  MOD_SEZI_idx=0
  TMPATTOF_idx=0
  TMPSEZOF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if EMPTY(NVL(this.oParentObject.w_CODDES, " "))
      AH_ErrorMsg("Codice nuovo modello obbligatorio",48)
      i_retcode = 'stop'
      return
    endif
    * --- Verifica esistenza codice di partenza
    * --- Read from MOD_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" MOD_OFFE where ";
            +"MOCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODORI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            MOCODICE = this.oParentObject.w_CODORI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows<1
      AH_ErrorMsg("Codice modello d'offerta da duplicare inesistente",48)
      i_retcode = 'stop'
      return
    endif
    * --- Verifica assenza codice di destinazione
    * --- Read from MOD_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" MOD_OFFE where ";
            +"MOCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODDES);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            MOCODICE = this.oParentObject.w_CODDES;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows>0
      AH_ErrorMsg("Codice nuovo modello d'offerta gi� esistente, impossibile proseguire",48)
      i_retcode = 'stop'
      return
    endif
    * --- Se La descrizione di destinazione � vuota inserisco quella di origine
    if EMPTY(this.oParentObject.w_DESDES)
      this.oParentObject.w_DESDES = LEFT(ALLTRIM(this.oParentObject.w_DESORI),27)+" (copia)"
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_03379680
    bErr_03379680=bTrsErr
    this.Try_03379680()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      AH_ErrorMsg("Duplicazione modello offerta fallita",48)
    endif
    bTrsErr=bTrsErr or bErr_03379680
    * --- End
    * --- Elimino tabelle temporanee
    * --- Drop temporary table TMPATTOF
    i_nIdx=cp_GetTableDefIdx('TMPATTOF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPATTOF')
    endif
    * --- Drop temporary table TMPSEZOF
    i_nIdx=cp_GetTableDefIdx('TMPSEZOF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSEZOF')
    endif
  endproc
  proc Try_03379680()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Utilizzo tabelle temporanee in modoc he in caso di aggiunta/rimozione
    *     campi non sia necessario modificare la routine
    * --- Trasferisco i dati del modello in tabelle temporanee
    * --- Create temporary table TMPATTOF
    i_nIdx=cp_AddTableDef('TMPATTOF') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MOD_OFFE_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where MOCODICE="+cp_ToStrODBC(this.oParentObject.w_CODORI)+"";
          )
    this.TMPATTOF_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPSEZOF
    i_nIdx=cp_AddTableDef('TMPSEZOF') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.MOD_SEZI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_SEZI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where MSCODICE="+cp_ToStrODBC(this.oParentObject.w_CODORI)+"";
          )
    this.TMPSEZOF_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiorno codice e descrizione della testata
    * --- Write into TMPATTOF
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPATTOF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPATTOF_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTOF_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MOCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODDES),'TMPATTOF','MOCODICE');
      +",MODESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESDES),'TMPATTOF','MODESCRI');
          +i_ccchkf ;
      +" where ";
          +"MOCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODORI);
             )
    else
      update (i_cTable) set;
          MOCODICE = this.oParentObject.w_CODDES;
          ,MODESCRI = this.oParentObject.w_DESDES;
          &i_ccchkf. ;
       where;
          MOCODICE = this.oParentObject.w_CODORI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorno codici dei dettagli
    * --- Write into TMPSEZOF
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPSEZOF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPSEZOF_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPSEZOF_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MSCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODDES),'TMPSEZOF','MSCODICE');
          +i_ccchkf ;
      +" where ";
          +"MSCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODORI);
             )
    else
      update (i_cTable) set;
          MSCODICE = this.oParentObject.w_CODDES;
          &i_ccchkf. ;
       where;
          MSCODICE = this.oParentObject.w_CODORI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Scrivo il nuovo modello
    * --- Insert into MOD_OFFE
    i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPATTOF_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where MOCODICE="+cp_ToStrODBC(this.oParentObject.w_CODDES)+"",this.MOD_OFFE_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into MOD_SEZI
    i_nConn=i_TableProp[this.MOD_SEZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_SEZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPSEZOF_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where MSCODICE="+cp_ToStrODBC(this.oParentObject.w_CODDES)+"",this.MOD_SEZI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    AH_ErrorMsg("Duplicazione modello offerta terminata con successo",64)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='MOD_OFFE'
    this.cWorkTables[2]='MOD_SEZI'
    this.cWorkTables[3]='*TMPATTOF'
    this.cWorkTables[4]='*TMPSEZOF'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
