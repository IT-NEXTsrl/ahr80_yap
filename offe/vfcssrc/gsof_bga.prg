* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bga                                                        *
*              Eventi da visualizza allegati                                   *
*                                                                              *
*      Author: Nunzio Iorio                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_56]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-05                                                      *
* Last revis.: 2010-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bga",oParentObject,m.Azione)
return(i_retval)

define class tgsof_bga as StdBatch
  * --- Local variables
  Azione = space(10)
  w_TIPALL = space(1)
  GestALL = .NULL.
  TMPc = space(10)
  pop = space(100)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Visualizza Allegati (da GSOF_KRA)
    do case
      case this.Azione $"VARIA_ALLEGATO|ELIMINA_ALLEGATO"
        if this.oParentObject.w_MOCODICE <> " "
          this.GestALL = GSOF_AAL(this.oParentObject.w_MOCODICE, "M",this.oParentObject.w_MODESCRI,this.oParentObject.w_OFNUMDOC,this.oParentObject.w_OFSERDOC,this.oParentObject.w_OFDATDOC,this.oParentObject.w_OFNUMVER)
        else
          if this.oParentObject.w_NOCODICE <> " "
            this.GestALL = GSOF_AAL(this.oParentObject.w_NOCODICE, "N",this.oParentObject.w_NODESCRI,this.oParentObject.w_OFNUMDOC,this.oParentObject.w_OFSERDOC,this.oParentObject.w_OFDATDOC,this.oParentObject.w_OFNUMVER)
          else
            if this.oParentObject.w_OFSERIAL <> " "
              this.GestALL = GSOF_AAL(this.oParentObject.w_OFSERIAL, "O"," ",this.oParentObject.w_OFNUMDOC,this.oParentObject.w_OFSERDOC,this.oParentObject.w_OFDATDOC,this.oParentObject.w_OFNUMVER)
            endif
          endif
        endif
        * --- Controllo se ha passato il test di accesso 
        if !(this.GestALL.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carica record e lascia in interrograzione ...
        this.GestALL.w_ALSERIAL = this.oParentObject.w_SERALL
        this.TMPc = "ALSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERALL)
        this.GestALL.QueryKeySet(this.TmpC,"")     
        this.GestALL.LoadRecWarn()     
        do case
          case this.Azione = "VARIA_ALLEGATO"
            * --- Modifica record
            this.GestALL.ECPEdit()     
          case this.Azione = "ELIMINA_ALLEGATO"
            * --- Cancella record ...
            this.GestALL.ECPDelete()     
        endcase
      case this.Azione = "APRI_ALLEGATO"
        * --- Apri file ...
        do case
          case this.oParentObject.w_MOCODICE <> " "
            this.pop = ofopenfile(ALLTRIM(this.oParentObject.w_PATALL),"","","",ALLTRIM(this.oParentObject.w_NOCODICE))
          case this.oParentObject.w_NOCODICE <> " "
            this.pop = ofopenfile(ALLTRIM(this.oParentObject.w_PATALL),ALLTRIM(this.oParentObject.w_SERALL),"N",ALLTRIM(this.oParentObject.w_NOCODICE))
          case this.oParentObject.w_OFSERIAL <> " "
            this.pop = ofopenfile(ALLTRIM(this.oParentObject.w_PATALL),ALLTRIM(this.oParentObject.w_SERALL),"O",ALLTRIM(this.oParentObject.w_NOCODICE))
        endcase
      case this.Azione = "ORIGINE_ALLEGATO"
        do case
          case this.oParentObject.w_MOCODICE <> " "
            this.GestALL = GSOF_AMO()
            * --- Controllo se ha passato il test di accesso
            if !(this.GestALL.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- Carica record e lascia in interrograzione ...
            this.GestALL.ecpFilter()     
            this.GestALL.w_MOCODICE = this.oParentObject.w_MOCODICE
            this.GestALL.ecpSave()     
            this.GestALL.ecpQuery()     
          case this.oParentObject.w_NOCODICE <> " "
            this.GestALL = GSAR_ANO()
            * --- Controllo se ha passato il test di accesso
            if !(this.GestALL.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- Carica record e lascia in interrograzione ...
            this.GestALL.ecpFilter()     
            this.GestALL.w_NOCODICE = this.oParentObject.w_NOCODICE
            this.GestALL.ecpSave()     
            this.GestALL.ecpQuery()     
          case this.oParentObject.w_OFSERIAL <> " "
            this.GestALL = GSOF_AOF()
            * --- Controllo se ha passato il test di accesso
            if !(this.GestALL.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- Carica record e lascia in interrograzione ...
            this.GestALL.ecpFilter()     
            this.GestALL.w_OFSERIAL = this.oParentObject.w_OFSERIAL
            this.GestALL.ecpSave()     
            this.GestALL.ecpQuery()     
        endcase
    endcase
  endproc


  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
