* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_mdc                                                        *
*              Articoli da confermare                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-14                                                      *
* Last revis.: 2012-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_mdc"))

* --- Class definition
define class tgsof_mdc as StdTrsForm
  Top    = 23
  Left   = 8

  * --- Standard Properties
  Width  = 790
  Height = 337+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-01-02"
  HelpContextID=80357737
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  OFF_DETT_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "OFF_DETT"
  cKeySelect = "ODSERIAL"
  cKeyWhere  = "ODSERIAL=this.w_ODSERIAL"
  cKeyDetail  = "ODSERIAL=this.w_ODSERIAL"
  cKeyWhereODBC = '"ODSERIAL="+cp_ToStrODBC(this.w_ODSERIAL)';

  cKeyDetailWhereODBC = '"ODSERIAL="+cp_ToStrODBC(this.w_ODSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"OFF_DETT.ODSERIAL="+cp_ToStrODBC(this.w_ODSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'OFF_DETT.CPROWORD '
  cPrg = "gsof_mdc"
  cComment = "Articoli da confermare"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 14
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PUNTAT = space(10)
  w_ODSERIAL = space(10)
  w_CPROWORD = 0
  w_ODCODICE = space(20)
  o_ODCODICE = space(20)
  w_DECTOT = 0
  o_DECTOT = 0
  w_DECUNI = 0
  o_DECUNI = 0
  w_ODCODICE = space(41)
  w_ODTIPRIG = space(1)
  w_ODCODART = space(20)
  w_UNMIS3 = space(3)
  w_UNMIS2 = space(3)
  w_ODDESART = space(40)
  w_ODUNIMIS = space(3)
  w_ODQTAMOV = 0
  w_ODPREZZO = 0
  w_ODCODSOT = space(5)
  w_ODCODGRU = space(5)
  w_UNMIS1 = space(3)
  w_NUMOFF = 0
  w_SEROFF = space(2)
  w_DATOFF = ctod('  /  /  ')
  w_CALCPICT = 0
  w_CALCPICU = 0
  w_OBTEST = ctod('  /  /  ')
  w_NURIGA = 0
  w_CODESC = space(10)
  w_NUMOFF = 0
  w_SEROFF = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsof_mdc
  * ---- Intercetta la Save standard e Conferma Chiusura Partite
  proc ecpSave()
    if this.CheckForm()
      this.w_PUNTAT.NotifyEvent('ConfermaRighe')
    endif
  endproc
  proc F6()
     * --- Funzione non Consentita
  endproc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'OFF_DETT','gsof_mdc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_mdcPag1","gsof_mdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Articoli da confermare")
      .Pages(1).HelpContextID = 222999515
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsof_mdc
    * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='OFF_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OFF_DETT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OFF_DETT_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_ODSERIAL = NVL(ODSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from OFF_DETT where ODSERIAL=KeySet.ODSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2],this.bLoadRecFilter,this.OFF_DETT_IDX,"gsof_mdc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OFF_DETT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OFF_DETT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OFF_DETT '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ODSERIAL',this.w_ODSERIAL  )
      select * from (i_cTable) OFF_DETT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PUNTAT = IIF(TYPE('THIS .w_PUNTAT')<>'O', p_PUNTAT, .w_PUNTAT)
        .w_OBTEST = i_DATSYS
        .w_ODSERIAL = NVL(ODSERIAL,space(10))
        .w_DECTOT = .w_PUNTAT.oParentObject .w_DECTOT
        .w_DECUNI = .w_PUNTAT.oParentObject .w_DECUNI
        .w_NUMOFF = .w_PUNTAT.oParentObject .w_OFNUMDOC
        .w_SEROFF = .w_PUNTAT.oParentObject .w_OFSERDOC
        .w_DATOFF = .w_PUNTAT.oParentObject .w_OFDATDOC
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_CODESC = Space(5)
        .w_NUMOFF = .w_PUNTAT.oParentObject .w_OFNUMDOC
        .w_SEROFF = .w_PUNTAT.oParentObject .w_OFSERDOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'OFF_DETT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_UNMIS3 = space(3)
          .w_UNMIS2 = space(3)
          .w_UNMIS1 = space(3)
          .w_NURIGA = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_ODCODICE = NVL(ODCODICE,space(20))
          if link_2_2_joined
            this.w_ODCODICE = NVL(CACODICE202,NVL(this.w_ODCODICE,space(20)))
            this.w_ODCODART = NVL(CACODART202,space(20))
            this.w_ODDESART = NVL(CADESART202,space(40))
            this.w_UNMIS3 = NVL(CAUNIMIS202,space(3))
            this.w_ODTIPRIG = NVL(CA__TIPO202,space(1))
          else
          .link_2_2('Load')
          endif
          .w_ODCODICE = NVL(ODCODICE,space(41))
          if link_2_3_joined
            this.w_ODCODICE = NVL(CACODICE203,NVL(this.w_ODCODICE,space(41)))
            this.w_ODCODART = NVL(CACODART203,space(20))
            this.w_ODDESART = NVL(CADESART203,space(40))
            this.w_UNMIS3 = NVL(CAUNIMIS203,space(3))
            this.w_ODTIPRIG = NVL(CA__TIPO203,space(1))
          else
          .link_2_3('Load')
          endif
          .w_ODTIPRIG = NVL(ODTIPRIG,space(1))
          .w_ODCODART = NVL(ODCODART,space(20))
          if link_2_5_joined
            this.w_ODCODART = NVL(ARCODART205,NVL(this.w_ODCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1205,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2205,space(3))
          else
          .link_2_5('Load')
          endif
          .w_ODDESART = NVL(ODDESART,space(40))
          .w_ODUNIMIS = NVL(ODUNIMIS,space(3))
          .w_ODQTAMOV = NVL(ODQTAMOV,0)
          .w_ODPREZZO = NVL(ODPREZZO,0)
          .w_ODCODSOT = NVL(ODCODSOT,space(5))
          .w_ODCODGRU = NVL(ODCODGRU,space(5))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_DECTOT = .w_PUNTAT.oParentObject .w_DECTOT
        .w_DECUNI = .w_PUNTAT.oParentObject .w_DECUNI
        .w_NUMOFF = .w_PUNTAT.oParentObject .w_OFNUMDOC
        .w_SEROFF = .w_PUNTAT.oParentObject .w_OFSERDOC
        .w_DATOFF = .w_PUNTAT.oParentObject .w_OFDATDOC
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_CODESC = Space(5)
        .w_NUMOFF = .w_PUNTAT.oParentObject .w_OFNUMDOC
        .w_SEROFF = .w_PUNTAT.oParentObject .w_OFSERDOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsof_mdc
    * --- 'Trucco' per evitare di entrare in 'Query' al termine della Load
    IF TYPE('p_puntat')<>'O'
           IF TYPE('this.w_PUNTAT') <> 'O'
                    * --- Deve fare 2 'giri' nella BlankRec prima di Uscire, (se esce subito da errore)
           	this.ecpQuit()
           ELSE
                    * --- Primo Giro
           	this.w_PUNTAT = SPACE(10)
           ENDIF
           RETURN
    ENDIF
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PUNTAT=space(10)
      .w_ODSERIAL=space(10)
      .w_CPROWORD=10
      .w_ODCODICE=space(20)
      .w_DECTOT=0
      .w_DECUNI=0
      .w_ODCODICE=space(41)
      .w_ODTIPRIG=space(1)
      .w_ODCODART=space(20)
      .w_UNMIS3=space(3)
      .w_UNMIS2=space(3)
      .w_ODDESART=space(40)
      .w_ODUNIMIS=space(3)
      .w_ODQTAMOV=0
      .w_ODPREZZO=0
      .w_ODCODSOT=space(5)
      .w_ODCODGRU=space(5)
      .w_UNMIS1=space(3)
      .w_NUMOFF=0
      .w_SEROFF=space(2)
      .w_DATOFF=ctod("  /  /  ")
      .w_CALCPICT=0
      .w_CALCPICU=0
      .w_OBTEST=ctod("  /  /  ")
      .w_NURIGA=0
      .w_CODESC=space(10)
      .w_NUMOFF=0
      .w_SEROFF=space(10)
      if .cFunction<>"Filter"
        .w_PUNTAT = IIF(TYPE('THIS .w_PUNTAT')<>'O', p_PUNTAT, .w_PUNTAT)
        .DoRTCalc(2,4,.f.)
        if not(empty(.w_ODCODICE))
         .link_2_2('Full')
        endif
        .w_DECTOT = .w_PUNTAT.oParentObject .w_DECTOT
        .w_DECUNI = .w_PUNTAT.oParentObject .w_DECUNI
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_ODCODICE))
         .link_2_3('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_ODCODART))
         .link_2_5('Full')
        endif
        .DoRTCalc(10,18,.f.)
        .w_NUMOFF = .w_PUNTAT.oParentObject .w_OFNUMDOC
        .w_SEROFF = .w_PUNTAT.oParentObject .w_OFSERDOC
        .w_DATOFF = .w_PUNTAT.oParentObject .w_OFDATDOC
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(25,25,.f.)
        .w_CODESC = Space(5)
        .w_NUMOFF = .w_PUNTAT.oParentObject .w_OFNUMDOC
        .w_SEROFF = .w_PUNTAT.oParentObject .w_OFSERDOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'OFF_DETT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsof_mdc
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'OFF_DETT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ODSERIAL,"ODSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
    i_lTable = "OFF_DETT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.OFF_DETT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_ODCODICE C(20);
      ,t_ODDESART C(40);
      ,t_ODUNIMIS C(3);
      ,t_ODQTAMOV N(12,3);
      ,t_ODPREZZO N(18,5);
      ,t_ODCODSOT C(5);
      ,t_ODCODGRU C(5);
      ,CPROWNUM N(10);
      ,t_ODTIPRIG C(1);
      ,t_ODCODART C(20);
      ,t_UNMIS3 C(3);
      ,t_UNMIS2 C(3);
      ,t_UNMIS1 C(3);
      ,t_NURIGA N(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsof_mdcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_2.controlsource=this.cTrsName+'.t_ODCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_3.controlsource=this.cTrsName+'.t_ODCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODDESART_2_8.controlsource=this.cTrsName+'.t_ODDESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODUNIMIS_2_9.controlsource=this.cTrsName+'.t_ODUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODQTAMOV_2_10.controlsource=this.cTrsName+'.t_ODQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODPREZZO_2_11.controlsource=this.cTrsName+'.t_ODPREZZO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODCODSOT_2_12.controlsource=this.cTrsName+'.t_ODCODSOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oODCODGRU_2_13.controlsource=this.cTrsName+'.t_ODCODGRU'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(46)
    this.AddVLine(218)
    this.AddVLine(267)
    this.AddVLine(320)
    this.AddVLine(528)
    this.AddVLine(560)
    this.AddVLine(640)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
      *
      * insert into OFF_DETT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OFF_DETT')
        i_extval=cp_InsertValODBCExtFlds(this,'OFF_DETT')
        i_cFldBody=" "+;
                  "(ODSERIAL,CPROWORD,ODCODICE,ODTIPRIG,ODCODART"+;
                  ",ODDESART,ODUNIMIS,ODQTAMOV,ODPREZZO,ODCODSOT"+;
                  ",ODCODGRU,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ODSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_ODCODICE)+","+cp_ToStrODBC(this.w_ODTIPRIG)+","+cp_ToStrODBCNull(this.w_ODCODART)+;
             ","+cp_ToStrODBC(this.w_ODDESART)+","+cp_ToStrODBC(this.w_ODUNIMIS)+","+cp_ToStrODBC(this.w_ODQTAMOV)+","+cp_ToStrODBC(this.w_ODPREZZO)+","+cp_ToStrODBC(this.w_ODCODSOT)+;
             ","+cp_ToStrODBC(this.w_ODCODGRU)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OFF_DETT')
        i_extval=cp_InsertValVFPExtFlds(this,'OFF_DETT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'ODSERIAL',this.w_ODSERIAL)
        INSERT INTO (i_cTable) (;
                   ODSERIAL;
                  ,CPROWORD;
                  ,ODCODICE;
                  ,ODTIPRIG;
                  ,ODCODART;
                  ,ODDESART;
                  ,ODUNIMIS;
                  ,ODQTAMOV;
                  ,ODPREZZO;
                  ,ODCODSOT;
                  ,ODCODGRU;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ODSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_ODCODICE;
                  ,this.w_ODTIPRIG;
                  ,this.w_ODCODART;
                  ,this.w_ODDESART;
                  ,this.w_ODUNIMIS;
                  ,this.w_ODQTAMOV;
                  ,this.w_ODPREZZO;
                  ,this.w_ODCODSOT;
                  ,this.w_ODCODGRU;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODUNIMIS) ) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'OFF_DETT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'OFF_DETT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODUNIMIS) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update OFF_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'OFF_DETT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",ODCODICE="+cp_ToStrODBCNull(this.w_ODCODICE)+;
                     ",ODTIPRIG="+cp_ToStrODBC(this.w_ODTIPRIG)+;
                     ",ODCODART="+cp_ToStrODBCNull(this.w_ODCODART)+;
                     ",ODDESART="+cp_ToStrODBC(this.w_ODDESART)+;
                     ",ODUNIMIS="+cp_ToStrODBC(this.w_ODUNIMIS)+;
                     ",ODQTAMOV="+cp_ToStrODBC(this.w_ODQTAMOV)+;
                     ",ODPREZZO="+cp_ToStrODBC(this.w_ODPREZZO)+;
                     ",ODCODSOT="+cp_ToStrODBC(this.w_ODCODSOT)+;
                     ",ODCODGRU="+cp_ToStrODBC(this.w_ODCODGRU)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'OFF_DETT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,ODCODICE=this.w_ODCODICE;
                     ,ODTIPRIG=this.w_ODTIPRIG;
                     ,ODCODART=this.w_ODCODART;
                     ,ODDESART=this.w_ODDESART;
                     ,ODUNIMIS=this.w_ODUNIMIS;
                     ,ODQTAMOV=this.w_ODQTAMOV;
                     ,ODPREZZO=this.w_ODPREZZO;
                     ,ODCODSOT=this.w_ODCODSOT;
                     ,ODCODGRU=this.w_ODCODGRU;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODUNIMIS) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete OFF_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODUNIMIS) ) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_DETT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .w_DECTOT = .w_PUNTAT.oParentObject .w_DECTOT
          .w_DECUNI = .w_PUNTAT.oParentObject .w_DECUNI
        .DoRTCalc(7,8,.t.)
        if .o_ODCODICE<>.w_ODCODICE
          .link_2_5('Full')
        endif
        .DoRTCalc(10,18,.t.)
          .w_NUMOFF = .w_PUNTAT.oParentObject .w_OFNUMDOC
          .w_SEROFF = .w_PUNTAT.oParentObject .w_OFSERDOC
          .w_DATOFF = .w_PUNTAT.oParentObject .w_OFDATDOC
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        if .o_DECTOT<>.w_DECTOT
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_DECUNI<>.w_DECUNI
          .w_CALCPICU = DEFPIU(.w_DECUNI)
        endif
        .DoRTCalc(24,25,.t.)
          .w_CODESC = Space(5)
          .w_NUMOFF = .w_PUNTAT.oParentObject .w_OFNUMDOC
          .w_SEROFF = .w_PUNTAT.oParentObject .w_OFSERDOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_ODTIPRIG with this.w_ODTIPRIG
      replace t_ODCODART with this.w_ODCODART
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_NURIGA with this.w_NURIGA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODCODICE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODCODICE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODCODICE_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oODCODICE_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMOFF_1_5.visible=!this.oPgFrm.Page1.oPag.oNUMOFF_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oSEROFF_1_8.visible=!this.oPgFrm.Page1.oPag.oSEROFF_1_8.mHide()
    this.oPgFrm.Page1.oPag.oNUMOFF_1_16.visible=!this.oPgFrm.Page1.oPag.oNUMOFF_1_16.mHide()
    this.oPgFrm.Page1.oPag.oSEROFF_1_17.visible=!this.oPgFrm.Page1.oPag.oSEROFF_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_2.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_3.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ODCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ODCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ODCODICE))
          select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStrODBC(trim(this.w_ODCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStr(trim(this.w_ODCODICE)+"%");

            select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ODCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oODCODICE_2_2'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSVE_MDV.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ODCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ODCODICE)
            select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_ODCODART = NVL(_Link_.CACODART,space(20))
      this.w_ODDESART = NVL(_Link_.CADESART,space(40))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_ODTIPRIG = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ODCODICE = space(20)
      endif
      this.w_ODCODART = space(20)
      this.w_ODDESART = space(40)
      this.w_UNMIS3 = space(3)
      this.w_ODTIPRIG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODTIPRIG<>'D'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto o U.M. incongruente o servizio descrittivo")
        endif
        this.w_ODCODICE = space(20)
        this.w_ODCODART = space(20)
        this.w_ODDESART = space(40)
        this.w_UNMIS3 = space(3)
        this.w_ODTIPRIG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CAUNIMIS as CAUNIMIS202"+ ",link_2_2.CA__TIPO as CA__TIPO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on OFF_DETT.ODCODICE=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and OFF_DETT.ODCODICE=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ODCODICE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ODCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ODCODICE))
          select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStrODBC(trim(this.w_ODCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStr(trim(this.w_ODCODICE)+"%");

            select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ODCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oODCODICE_2_3'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSVE0MDV.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ODCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ODCODICE)
            select CACODICE,CACODART,CADESART,CAUNIMIS,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODCODICE = NVL(_Link_.CACODICE,space(41))
      this.w_ODCODART = NVL(_Link_.CACODART,space(20))
      this.w_ODDESART = NVL(_Link_.CADESART,space(40))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_ODTIPRIG = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ODCODICE = space(41)
      endif
      this.w_ODCODART = space(20)
      this.w_ODDESART = space(40)
      this.w_UNMIS3 = space(3)
      this.w_ODTIPRIG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODTIPRIG<>'D'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto o U.M. incongruente o servizio descrittivo")
        endif
        this.w_ODCODICE = space(41)
        this.w_ODCODART = space(20)
        this.w_ODDESART = space(40)
        this.w_UNMIS3 = space(3)
        this.w_ODTIPRIG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CACODICE as CACODICE203"+ ",link_2_3.CACODART as CACODART203"+ ",link_2_3.CADESART as CADESART203"+ ",link_2_3.CAUNIMIS as CAUNIMIS203"+ ",link_2_3.CA__TIPO as CA__TIPO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on OFF_DETT.ODCODICE=link_2_3.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and OFF_DETT.ODCODICE=link_2_3.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ODCODART
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ODCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ODCODART)
            select ARCODART,ARUNMIS1,ARUNMIS2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ODCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.ARCODART as ARCODART205"+ ",link_2_5.ARUNMIS1 as ARUNMIS1205"+ ",link_2_5.ARUNMIS2 as ARUNMIS2205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on OFF_DETT.ODCODART=link_2_5.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and OFF_DETT.ODCODART=link_2_5.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oNUMOFF_1_5.value==this.w_NUMOFF)
      this.oPgFrm.Page1.oPag.oNUMOFF_1_5.value=this.w_NUMOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oSEROFF_1_8.value==this.w_SEROFF)
      this.oPgFrm.Page1.oPag.oSEROFF_1_8.value=this.w_SEROFF
    endif
    if not(this.oPgFrm.Page1.oPag.oDATOFF_1_10.value==this.w_DATOFF)
      this.oPgFrm.Page1.oPag.oDATOFF_1_10.value=this.w_DATOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMOFF_1_16.value==this.w_NUMOFF)
      this.oPgFrm.Page1.oPag.oNUMOFF_1_16.value=this.w_NUMOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oSEROFF_1_17.value==this.w_SEROFF)
      this.oPgFrm.Page1.oPag.oSEROFF_1_17.value=this.w_SEROFF
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_2.value==this.w_ODCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_2.value=this.w_ODCODICE
      replace t_ODCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_3.value==this.w_ODCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_3.value=this.w_ODCODICE
      replace t_ODCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODDESART_2_8.value==this.w_ODDESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODDESART_2_8.value=this.w_ODDESART
      replace t_ODDESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODDESART_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODUNIMIS_2_9.value==this.w_ODUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODUNIMIS_2_9.value=this.w_ODUNIMIS
      replace t_ODUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODUNIMIS_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODQTAMOV_2_10.value==this.w_ODQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODQTAMOV_2_10.value=this.w_ODQTAMOV
      replace t_ODQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODQTAMOV_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODPREZZO_2_11.value==this.w_ODPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODPREZZO_2_11.value=this.w_ODPREZZO
      replace t_ODPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODPREZZO_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODSOT_2_12.value==this.w_ODCODSOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODSOT_2_12.value=this.w_ODCODSOT
      replace t_ODCODSOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODSOT_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODGRU_2_13.value==this.w_ODCODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODGRU_2_13.value=this.w_ODCODGRU
      replace t_ODCODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODGRU_2_13.value
    endif
    cp_SetControlsValueExtFlds(this,'OFF_DETT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_mdc
      if i_bRes=.t.
        select (this.cTrsName)
        scan for not deleted() and t_CPROWORD<>0 and not empty(t_ODUNIMIS)
          if i_bRes=.t.
            if EMPTY(t_ODCODICE)
              i_cErrorMsg = ah_msgformat("Verificare riga: %1 %0Codice articolo non definito",STR(t_CPROWORD,5,0))
              i_bRes = .f.
              i_bnoChk = .f.
            else
              if t_ODUNIMIS<>t_UNMIS1 AND t_ODUNIMIS<>t_UNMIS2 AND t_ODUNIMIS<>t_UNMIS3
                i_cErrorMsg = ah_msgformat("Verificare riga: %1 %0Unit� di misura codice incongruente",STR(t_CPROWORD,5,0))
                i_bRes = .f.
                i_bnoChk = .f.
              endif
            endif
          endif
        endscan
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODUNIMIS) );
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_CPROWORD) and (NOT EMPTY(.w_CPROWORD) AND NOT EMPTY(.w_ODUNIMIS) )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(.w_ODTIPRIG<>'D') and (UPPER(g_APPLICATION)="ADHOC REVOLUTION") and not(empty(.w_ODCODICE)) and (NOT EMPTY(.w_CPROWORD) AND NOT EMPTY(.w_ODUNIMIS) )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto o U.M. incongruente o servizio descrittivo")
        case   not(.w_ODTIPRIG<>'D') and (UPPER(g_APPLICATION)="AD HOC ENTERPRISE") and not(empty(.w_ODCODICE)) and (NOT EMPTY(.w_CPROWORD) AND NOT EMPTY(.w_ODUNIMIS) )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oODCODICE_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto o U.M. incongruente o servizio descrittivo")
      endcase
      if NOT EMPTY(.w_CPROWORD) AND NOT EMPTY(.w_ODUNIMIS) 
        * --- Area Manuale = Check Row
        * --- gsof_mdc
        if i_bRes=.t. and EMPTY(.w_ODCODICE)
          i_cErrorMsg = Ah_MsgFormat("Codice articolo non definito")
          i_bRes = .f.
          i_bnoChk = .f.
        endif
        if i_bRes=.t. AND NOT(EMPTY(.w_ODCODICE))
          if .w_ODUNIMIS<>.w_UNMIS1 AND .w_ODUNIMIS<>.w_UNMIS2 AND .w_ODUNIMIS<>.w_UNMIS3
            i_cErrorMsg = Ah_MsgFormat("Unit� di misura codice incongruente")
            i_bRes = .f.
            i_bnoChk = .f.
          endif
        endif
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ODCODICE = this.w_ODCODICE
    this.o_DECTOT = this.w_DECTOT
    this.o_DECUNI = this.w_DECUNI
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_CPROWORD) AND NOT EMPTY(t_ODUNIMIS) )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_ODCODICE=space(20)
      .w_ODCODICE=space(41)
      .w_ODTIPRIG=space(1)
      .w_ODCODART=space(20)
      .w_UNMIS3=space(3)
      .w_UNMIS2=space(3)
      .w_ODDESART=space(40)
      .w_ODUNIMIS=space(3)
      .w_ODQTAMOV=0
      .w_ODPREZZO=0
      .w_ODCODSOT=space(5)
      .w_ODCODGRU=space(5)
      .w_UNMIS1=space(3)
      .w_NURIGA=0
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_ODCODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(5,7,.f.)
      if not(empty(.w_ODCODICE))
        .link_2_3('Full')
      endif
      .DoRTCalc(8,9,.f.)
      if not(empty(.w_ODCODART))
        .link_2_5('Full')
      endif
    endwith
    this.DoRTCalc(10,28,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_ODCODICE = t_ODCODICE
    this.w_ODCODICE = t_ODCODICE
    this.w_ODTIPRIG = t_ODTIPRIG
    this.w_ODCODART = t_ODCODART
    this.w_UNMIS3 = t_UNMIS3
    this.w_UNMIS2 = t_UNMIS2
    this.w_ODDESART = t_ODDESART
    this.w_ODUNIMIS = t_ODUNIMIS
    this.w_ODQTAMOV = t_ODQTAMOV
    this.w_ODPREZZO = t_ODPREZZO
    this.w_ODCODSOT = t_ODCODSOT
    this.w_ODCODGRU = t_ODCODGRU
    this.w_UNMIS1 = t_UNMIS1
    this.w_NURIGA = t_NURIGA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_ODCODICE with this.w_ODCODICE
    replace t_ODCODICE with this.w_ODCODICE
    replace t_ODTIPRIG with this.w_ODTIPRIG
    replace t_ODCODART with this.w_ODCODART
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_ODDESART with this.w_ODDESART
    replace t_ODUNIMIS with this.w_ODUNIMIS
    replace t_ODQTAMOV with this.w_ODQTAMOV
    replace t_ODPREZZO with this.w_ODPREZZO
    replace t_ODCODSOT with this.w_ODCODSOT
    replace t_ODCODGRU with this.w_ODCODGRU
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_NURIGA with this.w_NURIGA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsof_mdcPag1 as StdContainer
  Width  = 786
  height = 337
  stdWidth  = 786
  stdheight = 337
  resizeXpos=519
  resizeYpos=198
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMOFF_1_5 as StdField with uid="XVTZNZQOGY",rtseq=19,rtrep=.f.,;
    cFormVar = "w_NUMOFF", cQueryName = "NUMOFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 169213738,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=88, Top=16

  func oNUMOFF_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
    endif
  endfunc

  add object oSEROFF_1_8 as StdField with uid="LZLOOHIIRO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SEROFF", cQueryName = "SEROFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 169197274,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=167, Top=16, InputMask=replicate('X',2)

  func oSEROFF_1_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
    endif
  endfunc

  add object oDATOFF_1_10 as StdField with uid="RVGTWDHGRN",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DATOFF", cQueryName = "DATOFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 169190346,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=372, Top=16


  add object oObj_1_11 as cp_runprogram with uid="EGTHVLBQJQ",left=30, top=354, width=160,height=19,;
    caption='GSOF_BK1',;
   bGlobalFont=.t.,;
    prg="GSOF_BK1",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 210690409

  add object oNUMOFF_1_16 as StdField with uid="HJQQPVNWVK",rtseq=27,rtrep=.f.,;
    cFormVar = "w_NUMOFF", cQueryName = "NUMOFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 169213738,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=88, Top=16

  func oNUMOFF_1_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
    endif
  endfunc

  add object oSEROFF_1_17 as StdField with uid="HEJOABBFVV",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SEROFF", cQueryName = "SEROFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 169197274,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=220, Top=16, InputMask=replicate('X',10)

  func oSEROFF_1_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=49, width=780,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="CPROWORD",Label1="Riga",Field2="ODCODICE",Label2="",Field3="ODCODICE",Label3="Articolo",Field4="ODCODGRU",Label4="Gruppo",Field5="ODCODSOT",Label5="Sottogr.",Field6="ODDESART",Label6="Descrizione",Field7="ODUNIMIS",Label7="U.M.",Field8="ODQTAMOV",Label8="Quantit�",Field9="ODPREZZO",Label9="Prezzo unitario",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235733114

  add object oStr_1_6 as StdString with uid="BCZFUKNITJ",Visible=.t., Left=7, Top=16,;
    Alignment=1, Width=79, Height=18,;
    Caption="Offerta n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="WSOFBZFTIC",Visible=.t., Left=154, Top=16,;
    Alignment=2, Width=9, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION) <> "AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="ANWVLNWLRW",Visible=.t., Left=307, Top=16,;
    Alignment=1, Width=63, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="DITXEZWYVY",Visible=.t., Left=209, Top=16,;
    Alignment=2, Width=9, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=68,;
    width=776+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=69,width=775+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oODCODICE_2_2
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oODCODICE_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsof_mdcBodyRow as CPBodyRowCnt
  Width=766
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="AVIXBOFWAM",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 268061546,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oODCODICE_2_2 as StdTrsField with uid="WITWPKDCXL",rtseq=4,rtrep=.t.,;
    cFormVar="w_ODCODICE",value=space(20),;
    ToolTipText = "Codice di ricerca articolo",;
    HelpContextID = 147410987,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto o U.M. incongruente o servizio descrittivo",;
   bGlobalFont=.t.,;
    Height=17, Width=167, Left=45, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_ODCODICE"

  func oODCODICE_2_2.mCond()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oODCODICE_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
    endif
  endfunc

  func oODCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oODCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oODCODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oODCODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSVE_MDV.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oODCODICE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ODCODICE
    i_obj.ecpSave()
  endproc

  add object oODCODICE_2_3 as StdTrsField with uid="ZSDVABUHVC",rtseq=7,rtrep=.t.,;
    cFormVar="w_ODCODICE",value=space(41),;
    ToolTipText = "Codice di ricerca articolo",;
    HelpContextID = 147410987,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto o U.M. incongruente o servizio descrittivo",;
   bGlobalFont=.t.,;
    Height=17, Width=167, Left=41, Top=0, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_ODCODICE"

  func oODCODICE_2_3.mCond()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="AD HOC ENTERPRISE")
    endwith
  endfunc

  func oODCODICE_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE")
    endwith
    endif
  endfunc

  func oODCODICE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oODCODICE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oODCODICE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oODCODICE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSVE0MDV.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oODCODICE_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ODCODICE
    i_obj.ecpSave()
  endproc

  add object oODDESART_2_8 as StdTrsField with uid="NAMECXAXGG",rtseq=12,rtrep=.t.,;
    cFormVar="w_ODDESART",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 28270650,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=204, Left=317, Top=0, InputMask=replicate('X',40)

  add object oODUNIMIS_2_9 as StdTrsField with uid="XRVQJJTZCR",rtseq=13,rtrep=.t.,;
    cFormVar="w_ODUNIMIS",value=space(3),enabled=.f.,;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 219770937,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=29, Left=524, Top=0, InputMask=replicate('X',3)

  add object oODQTAMOV_2_10 as StdTrsField with uid="KDYMPBNDOS",rtseq=14,rtrep=.t.,;
    cFormVar="w_ODQTAMOV",value=0,enabled=.f.,;
    HelpContextID = 56676292,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=77, Left=556, Top=0, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)]

  add object oODPREZZO_2_11 as StdTrsField with uid="NTNGHEHTZO",rtseq=15,rtrep=.t.,;
    cFormVar="w_ODPREZZO",value=0,enabled=.f.,;
    ToolTipText = "Prezzo",;
    HelpContextID = 102948811,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=636, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)]

  add object oODCODSOT_2_12 as StdTrsField with uid="JSPHEKCKPA",rtseq=16,rtrep=.t.,;
    cFormVar="w_ODCODSOT",value=space(5),enabled=.f.,;
    HelpContextID = 221687750,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=264, Top=0, InputMask=replicate('X',5)

  add object oODCODGRU_2_13 as StdTrsField with uid="TBAIYOVTPH",rtseq=17,rtrep=.t.,;
    cFormVar="w_ODCODGRU",value=space(5),enabled=.f.,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 113856571,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=46, Left=212, Top=0, InputMask=replicate('X',5)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=13
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_mdc','OFF_DETT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ODSERIAL=OFF_DETT.ODSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
