* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_kat                                                        *
*              Agenda operatore                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_31]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-26                                                      *
* Last revis.: 2018-02-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_kat",oParentObject))

* --- Class definition
define class tgsof_kat as StdForm
  Top    = 12
  Left   = 15

  * --- Standard Properties
  Width  = 764
  Height = 506+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-02-26"
  HelpContextID=132786537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=48

  * --- Constant Properties
  _IDX = 0
  CPUSERS_IDX = 0
  OFF_NOMI_IDX = 0
  NOM_CONT_IDX = 0
  OFFTIPAT_IDX = 0
  OFF_ESIA_IDX = 0
  OFF_ERTE_IDX = 0
  cPrg = "gsof_kat"
  cComment = "Agenda operatore"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATAINI = ctod('  /  /  ')
  o_DATAINI = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATAINI1 = ctod('  /  /  ')
  o_DATAINI1 = ctod('  /  /  ')
  w_ORAINI1 = space(2)
  o_ORAINI1 = space(2)
  w_MININI1 = space(2)
  o_MININI1 = space(2)
  w_DATAFIN = ctod('  /  /  ')
  o_DATAFIN = ctod('  /  /  ')
  w_ORAFIN = space(2)
  o_ORAFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_DATAFIN1 = ctod('  /  /  ')
  o_DATAFIN1 = ctod('  /  /  ')
  w_ORAFIN1 = space(2)
  o_ORAFIN1 = space(2)
  w_MINFIN1 = space(2)
  o_MINFIN1 = space(2)
  w_NOMINI = space(20)
  o_NOMINI = space(20)
  w_NOMFIN = space(20)
  o_NOMFIN = space(20)
  w_CONTINI = space(5)
  w_PRIOINI = 0
  w_PRIOFIN = 0
  w_PRIOINI = 0
  w_ATDATINI = ctot('')
  w_ATDATFIN = ctot('')
  w_ATDATINI1 = ctot('')
  w_ATDATFIN1 = ctot('')
  w_OPERAT = 0
  w_NAME = space(20)
  w_STATO2 = space(1)
  w_STATO1 = space(1)
  w_TIPOATINI = space(5)
  w_TIPOATFIN = space(5)
  w_STATO = space(1)
  o_STATO = space(1)
  w_SUBJECT = space(254)
  o_SUBJECT = space(254)
  w_NODESCRI = space(40)
  w_NODESCRI1 = space(40)
  w_NCPERSON = space(40)
  w_TIPODES = space(50)
  w_TIPODES1 = space(50)
  w_ESITOATI = space(5)
  w_ESITOATF = space(5)
  w_OFCODNOM = space(20)
  w_ATSERIAL = space(20)
  w_OGGETTO = space(254)
  w_OFF_SER = space(10)
  w_OFSTATUS = space(1)
  w_OFNUMDOC = 0
  w_OFDATDOC = ctod('  /  /  ')
  w_OFSERDOC = space(10)
  w_OFNUMDOC = 0
  w_OFSERDOC = space(2)
  w_PRIOFIN = 0
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsof_kat
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_katPag1","gsof_kat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsof_katPag2","gsof_kat",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOPERAT_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='NOM_CONT'
    this.cWorkTables[4]='OFFTIPAT'
    this.cWorkTables[5]='OFF_ESIA'
    this.cWorkTables[6]='OFF_ERTE'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATAINI=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_DATAINI1=ctod("  /  /  ")
      .w_ORAINI1=space(2)
      .w_MININI1=space(2)
      .w_DATAFIN=ctod("  /  /  ")
      .w_ORAFIN=space(2)
      .w_MINFIN=space(2)
      .w_DATAFIN1=ctod("  /  /  ")
      .w_ORAFIN1=space(2)
      .w_MINFIN1=space(2)
      .w_NOMINI=space(20)
      .w_NOMFIN=space(20)
      .w_CONTINI=space(5)
      .w_PRIOINI=0
      .w_PRIOFIN=0
      .w_PRIOINI=0
      .w_ATDATINI=ctot("")
      .w_ATDATFIN=ctot("")
      .w_ATDATINI1=ctot("")
      .w_ATDATFIN1=ctot("")
      .w_OPERAT=0
      .w_NAME=space(20)
      .w_STATO2=space(1)
      .w_STATO1=space(1)
      .w_TIPOATINI=space(5)
      .w_TIPOATFIN=space(5)
      .w_STATO=space(1)
      .w_SUBJECT=space(254)
      .w_NODESCRI=space(40)
      .w_NODESCRI1=space(40)
      .w_NCPERSON=space(40)
      .w_TIPODES=space(50)
      .w_TIPODES1=space(50)
      .w_ESITOATI=space(5)
      .w_ESITOATF=space(5)
      .w_OFCODNOM=space(20)
      .w_ATSERIAL=space(20)
      .w_OGGETTO=space(254)
      .w_OFF_SER=space(10)
      .w_OFSTATUS=space(1)
      .w_OFNUMDOC=0
      .w_OFDATDOC=ctod("  /  /  ")
      .w_OFSERDOC=space(10)
      .w_OFNUMDOC=0
      .w_OFSERDOC=space(2)
      .w_PRIOFIN=0
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_ORAINI = '00'
        .w_MININI = '00'
        .w_DATAINI1 = .w_DATAINI
        .w_ORAINI1 = '00'
        .w_MININI1 = '00'
          .DoRTCalc(7,7,.f.)
        .w_ORAFIN = IIF(NOT EMPTY(.w_DATAFIN), '23', '00')
        .w_MINFIN = IIF(NOT EMPTY(.w_DATAFIN), '59', '00')
        .w_DATAFIN1 = .w_DATAFIN
        .w_ORAFIN1 = IIF(NOT EMPTY(.w_DATAFIN1), '23', '00')
        .w_MINFIN1 = IIF(NOT EMPTY(.w_DATAFIN1), '59', '00')
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_NOMINI))
          .link_2_14('Full')
        endif
        .w_NOMFIN = .w_NOMINI
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_NOMFIN))
          .link_2_15('Full')
        endif
        .w_CONTINI = IIF(.w_NOMINI = .w_NOMFIN, .w_CONTINI, SPACE(5))
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CONTINI))
          .link_2_16('Full')
        endif
          .DoRTCalc(16,22,.f.)
        .w_OPERAT = i_CODUTE
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_OPERAT))
          .link_1_2('Full')
        endif
          .DoRTCalc(24,24,.f.)
        .w_STATO2 = iif(g_AGEN='S','N','A')
        .w_STATO1 = iif(g_AGEN='S','D','A')
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_TIPOATINI))
          .link_1_6('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_TIPOATFIN))
          .link_1_7('Full')
        endif
        .w_STATO = IIF(g_AGEN='S',.w_STATO2,.w_STATO1)
          .DoRTCalc(30,35,.f.)
        .w_ESITOATI = IIF(.w_STATO = 'A', '', .w_ESITOATI)
        .w_ESITOATF = IIF(.w_STATO = 'A', '', .w_ESITOATF)
        .w_OFCODNOM = .w_NOMINI
        .w_ATSERIAL = .w_ZOOM.GetVar('ATSERIAL')
        .w_OGGETTO = '%'+ALLTRIM(.w_SUBJECT)
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_OFF_SER))
          .link_2_62('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_76.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
    endwith
    this.DoRTCalc(42,48,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .DoRTCalc(1,1,.t.)
        if .o_DATAINI<>.w_DATAINI
            .w_ORAINI = '00'
        endif
        if .o_DATAINI<>.w_DATAINI
            .w_MININI = '00'
        endif
        if .o_DATAINI<>.w_DATAINI
            .w_DATAINI1 = .w_DATAINI
        endif
        if .o_DATAINI1<>.w_DATAINI1
            .w_ORAINI1 = '00'
        endif
        if .o_DATAINI1<>.w_DATAINI1
            .w_MININI1 = '00'
        endif
        .DoRTCalc(7,7,.t.)
        if .o_DATAFIN<>.w_DATAFIN
            .w_ORAFIN = IIF(NOT EMPTY(.w_DATAFIN), '23', '00')
        endif
        if .o_DATAFIN<>.w_DATAFIN
            .w_MINFIN = IIF(NOT EMPTY(.w_DATAFIN), '59', '00')
        endif
        if .o_DATAFIN<>.w_DATAFIN
            .w_DATAFIN1 = .w_DATAFIN
        endif
        if .o_DATAFIN1<>.w_DATAFIN1
            .w_ORAFIN1 = IIF(NOT EMPTY(.w_DATAFIN1), '23', '00')
        endif
        if .o_DATAFIN1<>.w_DATAFIN1
            .w_MINFIN1 = IIF(NOT EMPTY(.w_DATAFIN1), '59', '00')
        endif
        .DoRTCalc(13,13,.t.)
        if .o_NOMINI<>.w_NOMINI
            .w_NOMFIN = .w_NOMINI
          .link_2_15('Full')
        endif
        if .o_NOMINI<>.w_NOMINI.or. .o_NOMFIN<>.w_NOMFIN
            .w_CONTINI = IIF(.w_NOMINI = .w_NOMFIN, .w_CONTINI, SPACE(5))
          .link_2_16('Full')
        endif
        if .o_ORAINI<>.w_ORAINI
          .Calculate_SLCYSAMDOK()
        endif
        if .o_MININI<>.w_MININI
          .Calculate_XLTWHZKGZW()
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_TTQFMXGTWH()
        endif
        if .o_ORAFIN<>.w_ORAFIN
          .Calculate_EMHXGAZDWG()
        endif
        if .o_DATAINI1<>.w_DATAINI1.or. .o_ORAINI1<>.w_ORAINI1.or. .o_MININI1<>.w_MININI1
          .Calculate_UGUEDMJCNH()
        endif
        if .o_DATAFIN1<>.w_DATAFIN1.or. .o_ORAFIN1<>.w_ORAFIN1.or. .o_MINFIN1<>.w_MINFIN1
          .Calculate_HODRFYTEIS()
        endif
        if .o_DATAINI<>.w_DATAINI.or. .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI
          .Calculate_HEZCDUNPFW()
        endif
        if .o_DATAFIN<>.w_DATAFIN.or. .o_ORAFIN<>.w_ORAFIN.or. .o_MINFIN<>.w_MINFIN
          .Calculate_ZQLIBXAFGH()
        endif
        if .o_ORAINI1<>.w_ORAINI1
          .Calculate_TQKNPEFJFF()
        endif
        if .o_MININI1<>.w_MININI1
          .Calculate_TLLYKMTAYV()
        endif
        if .o_MINFIN1<>.w_MINFIN1
          .Calculate_QCWVRVJIAY()
        endif
        if .o_ORAFIN1<>.w_ORAFIN1
          .Calculate_OSMGANIOHC()
        endif
        .DoRTCalc(16,28,.t.)
            .w_STATO = IIF(g_AGEN='S',.w_STATO2,.w_STATO1)
        .DoRTCalc(30,35,.t.)
        if .o_STATO<>.w_STATO
            .w_ESITOATI = IIF(.w_STATO = 'A', '', .w_ESITOATI)
        endif
        if .o_STATO<>.w_STATO
            .w_ESITOATF = IIF(.w_STATO = 'A', '', .w_ESITOATF)
        endif
            .w_OFCODNOM = .w_NOMINI
            .w_ATSERIAL = .w_ZOOM.GetVar('ATSERIAL')
        if .o_SUBJECT<>.w_SUBJECT
            .w_OGGETTO = '%'+ALLTRIM(.w_SUBJECT)
        endif
        .oPgFrm.Page2.oPag.oObj_2_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(41,48,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
    endwith
  return

  proc Calculate_SLCYSAMDOK()
    with this
          * --- Resetto w_oraini
          .w_ORAINI = .ZeroFill(.w_ORAINI)
    endwith
  endproc
  proc Calculate_XLTWHZKGZW()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_TTQFMXGTWH()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_EMHXGAZDWG()
    with this
          * --- Resetto w_orafin
          .w_ORAFIN = .ZeroFill(.w_ORAFIN)
    endwith
  endproc
  proc Calculate_UGUEDMJCNH()
    with this
          * --- Setta w_atdatini1
          .w_ATDATINI1 = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAINI1),  DTOC(.w_DATAINI1)+' '+.w_ORAINI1+':'+.w_MININI1+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_HODRFYTEIS()
    with this
          * --- Setta w_atdatfin1
          .w_ATDATFIN1 = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAFIN1),  DTOC(.w_DATAFIN1)+' '+.w_ORAFIN1+':'+.w_MINFIN1+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_HEZCDUNPFW()
    with this
          * --- Setta w_atdatini
          .w_ATDATINI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAINI),  DTOC(.w_DATAINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_ZQLIBXAFGH()
    with this
          * --- Setta w_atdatfin
          .w_ATDATFIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAFIN),  DTOC(.w_DATAFIN)+' '+.w_ORAFIN+':'+.w_MINFIN+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_TQKNPEFJFF()
    with this
          * --- Resetto w_oraini1
          .w_ORAINI1 = .ZeroFill(.w_ORAINI1)
    endwith
  endproc
  proc Calculate_TLLYKMTAYV()
    with this
          * --- Resetto w_minini1
          .w_MININI1 = .ZeroFill(.w_MININI1)
    endwith
  endproc
  proc Calculate_QCWVRVJIAY()
    with this
          * --- Resetto w_minfin1
          .w_MINFIN1 = .ZeroFill(.w_MINFIN1)
    endwith
  endproc
  proc Calculate_OSMGANIOHC()
    with this
          * --- Resetto w_orafin1
          .w_ORAFIN1 = .ZeroFill(.w_ORAFIN1)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCONTINI_2_16.enabled = this.oPgFrm.Page2.oPag.oCONTINI_2_16.mCond()
    this.oPgFrm.Page1.oPag.oOPERAT_1_2.enabled = this.oPgFrm.Page1.oPag.oOPERAT_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oPRIOINI_2_17.visible=!this.oPgFrm.Page2.oPag.oPRIOINI_2_17.mHide()
    this.oPgFrm.Page2.oPag.oPRIOFIN_2_18.visible=!this.oPgFrm.Page2.oPag.oPRIOFIN_2_18.mHide()
    this.oPgFrm.Page2.oPag.oPRIOINI_2_19.visible=!this.oPgFrm.Page2.oPag.oPRIOINI_2_19.mHide()
    this.oPgFrm.Page1.oPag.oSTATO2_1_4.visible=!this.oPgFrm.Page1.oPag.oSTATO2_1_4.mHide()
    this.oPgFrm.Page1.oPag.oSTATO1_1_5.visible=!this.oPgFrm.Page1.oPag.oSTATO1_1_5.mHide()
    this.oPgFrm.Page2.oPag.oOFF_SER_2_62.visible=!this.oPgFrm.Page2.oPag.oOFF_SER_2_62.mHide()
    this.oPgFrm.Page2.oPag.oOFNUMDOC_2_64.visible=!this.oPgFrm.Page2.oPag.oOFNUMDOC_2_64.mHide()
    this.oPgFrm.Page2.oPag.oOFSERDOC_2_66.visible=!this.oPgFrm.Page2.oPag.oOFSERDOC_2_66.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_67.visible=!this.oPgFrm.Page2.oPag.oStr_2_67.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_70.visible=!this.oPgFrm.Page2.oPag.oStr_2_70.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_71.visible=!this.oPgFrm.Page2.oPag.oStr_2_71.mHide()
    this.oPgFrm.Page2.oPag.oOFNUMDOC_2_72.visible=!this.oPgFrm.Page2.oPag.oOFNUMDOC_2_72.mHide()
    this.oPgFrm.Page2.oPag.oOFSERDOC_2_73.visible=!this.oPgFrm.Page2.oPag.oOFSERDOC_2_73.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_74.visible=!this.oPgFrm.Page2.oPag.oStr_2_74.mHide()
    this.oPgFrm.Page2.oPag.oPRIOFIN_2_75.visible=!this.oPgFrm.Page2.oPag.oPRIOFIN_2_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NOMINI
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_NOMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_NOMINI))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOMINI)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOMINI) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oNOMINI_2_14'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_NOMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_NOMINI)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMINI = NVL(_Link_.NOCODICE,space(20))
      this.w_NODESCRI = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_NOMINI = space(20)
      endif
      this.w_NODESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NOMINI <= .w_NOMFIN OR EMPTY(.w_NOMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NOMINI = space(20)
        this.w_NODESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOMFIN
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_NOMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_NOMFIN))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOMFIN)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOMFIN) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oNOMFIN_2_15'),i_cWhere,'GSAR_ANO',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_NOMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_NOMFIN)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMFIN = NVL(_Link_.NOCODICE,space(20))
      this.w_NODESCRI1 = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_NOMFIN = space(20)
      endif
      this.w_NODESCRI1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NOMINI <= .w_NOMFIN OR EMPTY(.w_NOMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NOMFIN = space(20)
        this.w_NODESCRI1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTINI
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_CONTINI)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_NOMINI);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_NOMINI;
                     ,'NCCODCON',trim(this.w_CONTINI))
          select NCCODICE,NCCODCON,NCPERSON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTINI)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONTINI) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oCONTINI_2_16'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_NOMINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_NOMINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_CONTINI);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_NOMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_NOMINI;
                       ,'NCCODCON',this.w_CONTINI)
            select NCCODICE,NCCODCON,NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTINI = NVL(_Link_.NCCODCON,space(5))
      this.w_NCPERSON = NVL(_Link_.NCPERSON,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CONTINI = space(5)
      endif
      this.w_NCPERSON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OPERAT
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OPERAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_OPERAT);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_OPERAT)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_OPERAT) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oOPERAT_1_2'),i_cWhere,'',"Elenco operatori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OPERAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_OPERAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_OPERAT)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OPERAT = NVL(_Link_.CODE,0)
      this.w_NAME = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OPERAT = 0
      endif
      this.w_NAME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OPERAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOATINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATA',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_TIPOATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_TIPOATINI))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOATINI)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOATINI) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oTIPOATINI_1_6'),i_cWhere,'GSOF_ATA',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_TIPOATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_TIPOATINI)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOATINI = NVL(_Link_.TACODTIP,space(5))
      this.w_TIPODES = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOATINI = space(5)
      endif
      this.w_TIPODES = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOATINI <= .w_TIPOATFIN OR EMPTY(.w_TIPOATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TIPOATINI = space(5)
        this.w_TIPODES = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOATFIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATA',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_TIPOATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_TIPOATFIN))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOATFIN)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOATFIN) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oTIPOATFIN_1_7'),i_cWhere,'GSOF_ATA',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_TIPOATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_TIPOATFIN)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOATFIN = NVL(_Link_.TACODTIP,space(5))
      this.w_TIPODES1 = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOATFIN = space(5)
      endif
      this.w_TIPODES1 = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOATINI <= .w_TIPOATFIN OR EMPTY(.w_TIPOATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TIPOATFIN = space(5)
        this.w_TIPODES1 = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFF_SER
  func Link_2_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    i_lTable = "OFF_ERTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2], .t., this.OFF_ERTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFF_SER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AOF',True,'OFF_ERTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OFSERIAL like "+cp_ToStrODBC(trim(this.w_OFF_SER)+"%");

          i_ret=cp_SQL(i_nConn,"select OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OFSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OFSERIAL',trim(this.w_OFF_SER))
          select OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OFSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFF_SER)==trim(_Link_.OFSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFF_SER) and !this.bDontReportError
            deferred_cp_zoom('OFF_ERTE','*','OFSERIAL',cp_AbsName(oSource.parent,'oOFF_SER_2_62'),i_cWhere,'GSOF_AOF',"Opportunita/offerte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                     +" from "+i_cTable+" "+i_lTable+" where OFSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OFSERIAL',oSource.xKey(1))
            select OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFF_SER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS";
                   +" from "+i_cTable+" "+i_lTable+" where OFSERIAL="+cp_ToStrODBC(this.w_OFF_SER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OFSERIAL',this.w_OFF_SER)
            select OFSERIAL,OFNUMDOC,OFDATDOC,OFSERDOC,OFSTATUS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFF_SER = NVL(_Link_.OFSERIAL,space(10))
      this.w_OFNUMDOC = NVL(_Link_.OFNUMDOC,0)
      this.w_OFDATDOC = NVL(cp_ToDate(_Link_.OFDATDOC),ctod("  /  /  "))
      this.w_OFSERDOC = NVL(_Link_.OFSERDOC,space(2))
      this.w_OFSTATUS = NVL(_Link_.OFSTATUS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OFF_SER = space(10)
      endif
      this.w_OFNUMDOC = 0
      this.w_OFDATDOC = ctod("  /  /  ")
      this.w_OFSERDOC = space(2)
      this.w_OFSTATUS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])+'\'+cp_ToStr(_Link_.OFSERIAL,1)
      cp_ShowWarn(i_cKey,this.OFF_ERTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFF_SER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oDATAINI_2_1.value==this.w_DATAINI)
      this.oPgFrm.Page2.oPag.oDATAINI_2_1.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oORAINI_2_2.value==this.w_ORAINI)
      this.oPgFrm.Page2.oPag.oORAINI_2_2.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oMININI_2_3.value==this.w_MININI)
      this.oPgFrm.Page2.oPag.oMININI_2_3.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATAINI1_2_4.value==this.w_DATAINI1)
      this.oPgFrm.Page2.oPag.oDATAINI1_2_4.value=this.w_DATAINI1
    endif
    if not(this.oPgFrm.Page2.oPag.oORAINI1_2_5.value==this.w_ORAINI1)
      this.oPgFrm.Page2.oPag.oORAINI1_2_5.value=this.w_ORAINI1
    endif
    if not(this.oPgFrm.Page2.oPag.oMININI1_2_6.value==this.w_MININI1)
      this.oPgFrm.Page2.oPag.oMININI1_2_6.value=this.w_MININI1
    endif
    if not(this.oPgFrm.Page2.oPag.oDATAFIN_2_7.value==this.w_DATAFIN)
      this.oPgFrm.Page2.oPag.oDATAFIN_2_7.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oORAFIN_2_8.value==this.w_ORAFIN)
      this.oPgFrm.Page2.oPag.oORAFIN_2_8.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMINFIN_2_10.value==this.w_MINFIN)
      this.oPgFrm.Page2.oPag.oMINFIN_2_10.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDATAFIN1_2_11.value==this.w_DATAFIN1)
      this.oPgFrm.Page2.oPag.oDATAFIN1_2_11.value=this.w_DATAFIN1
    endif
    if not(this.oPgFrm.Page2.oPag.oORAFIN1_2_12.value==this.w_ORAFIN1)
      this.oPgFrm.Page2.oPag.oORAFIN1_2_12.value=this.w_ORAFIN1
    endif
    if not(this.oPgFrm.Page2.oPag.oMINFIN1_2_13.value==this.w_MINFIN1)
      this.oPgFrm.Page2.oPag.oMINFIN1_2_13.value=this.w_MINFIN1
    endif
    if not(this.oPgFrm.Page2.oPag.oNOMINI_2_14.value==this.w_NOMINI)
      this.oPgFrm.Page2.oPag.oNOMINI_2_14.value=this.w_NOMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oNOMFIN_2_15.value==this.w_NOMFIN)
      this.oPgFrm.Page2.oPag.oNOMFIN_2_15.value=this.w_NOMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCONTINI_2_16.value==this.w_CONTINI)
      this.oPgFrm.Page2.oPag.oCONTINI_2_16.value=this.w_CONTINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIOINI_2_17.value==this.w_PRIOINI)
      this.oPgFrm.Page2.oPag.oPRIOINI_2_17.value=this.w_PRIOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIOFIN_2_18.value==this.w_PRIOFIN)
      this.oPgFrm.Page2.oPag.oPRIOFIN_2_18.value=this.w_PRIOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIOINI_2_19.RadioValue()==this.w_PRIOINI)
      this.oPgFrm.Page2.oPag.oPRIOINI_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOPERAT_1_2.value==this.w_OPERAT)
      this.oPgFrm.Page1.oPag.oOPERAT_1_2.value=this.w_OPERAT
    endif
    if not(this.oPgFrm.Page1.oPag.oNAME_1_3.value==this.w_NAME)
      this.oPgFrm.Page1.oPag.oNAME_1_3.value=this.w_NAME
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO2_1_4.RadioValue()==this.w_STATO2)
      this.oPgFrm.Page1.oPag.oSTATO2_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO1_1_5.RadioValue()==this.w_STATO1)
      this.oPgFrm.Page1.oPag.oSTATO1_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOATINI_1_6.value==this.w_TIPOATINI)
      this.oPgFrm.Page1.oPag.oTIPOATINI_1_6.value=this.w_TIPOATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOATFIN_1_7.value==this.w_TIPOATFIN)
      this.oPgFrm.Page1.oPag.oTIPOATFIN_1_7.value=this.w_TIPOATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSUBJECT_1_9.value==this.w_SUBJECT)
      this.oPgFrm.Page1.oPag.oSUBJECT_1_9.value=this.w_SUBJECT
    endif
    if not(this.oPgFrm.Page2.oPag.oNODESCRI_2_49.value==this.w_NODESCRI)
      this.oPgFrm.Page2.oPag.oNODESCRI_2_49.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oNODESCRI1_2_50.value==this.w_NODESCRI1)
      this.oPgFrm.Page2.oPag.oNODESCRI1_2_50.value=this.w_NODESCRI1
    endif
    if not(this.oPgFrm.Page2.oPag.oNCPERSON_2_51.value==this.w_NCPERSON)
      this.oPgFrm.Page2.oPag.oNCPERSON_2_51.value=this.w_NCPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODES_1_15.value==this.w_TIPODES)
      this.oPgFrm.Page1.oPag.oTIPODES_1_15.value=this.w_TIPODES
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODES1_1_17.value==this.w_TIPODES1)
      this.oPgFrm.Page1.oPag.oTIPODES1_1_17.value=this.w_TIPODES1
    endif
    if not(this.oPgFrm.Page2.oPag.oESITOATI_2_53.RadioValue()==this.w_ESITOATI)
      this.oPgFrm.Page2.oPag.oESITOATI_2_53.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oESITOATF_2_55.RadioValue()==this.w_ESITOATF)
      this.oPgFrm.Page2.oPag.oESITOATF_2_55.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOFF_SER_2_62.value==this.w_OFF_SER)
      this.oPgFrm.Page2.oPag.oOFF_SER_2_62.value=this.w_OFF_SER
    endif
    if not(this.oPgFrm.Page2.oPag.oOFSTATUS_2_63.RadioValue()==this.w_OFSTATUS)
      this.oPgFrm.Page2.oPag.oOFSTATUS_2_63.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOFNUMDOC_2_64.value==this.w_OFNUMDOC)
      this.oPgFrm.Page2.oPag.oOFNUMDOC_2_64.value=this.w_OFNUMDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oOFDATDOC_2_65.value==this.w_OFDATDOC)
      this.oPgFrm.Page2.oPag.oOFDATDOC_2_65.value=this.w_OFDATDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oOFSERDOC_2_66.value==this.w_OFSERDOC)
      this.oPgFrm.Page2.oPag.oOFSERDOC_2_66.value=this.w_OFSERDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oOFNUMDOC_2_72.value==this.w_OFNUMDOC)
      this.oPgFrm.Page2.oPag.oOFNUMDOC_2_72.value=this.w_OFNUMDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oOFSERDOC_2_73.value==this.w_OFSERDOC)
      this.oPgFrm.Page2.oPag.oOFSERDOC_2_73.value=this.w_OFSERDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIOFIN_2_75.RadioValue()==this.w_PRIOFIN)
      this.oPgFrm.Page2.oPag.oPRIOFIN_2_75.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_ORAINI) < 24)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oORAINI_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMININI_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_ORAINI) < 24)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oORAINI1_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMININI1_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_ORAFIN) < 24)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oORAFIN_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMINFIN_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_ORAFIN) < 24)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oORAFIN1_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMINFIN1_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_NOMINI <= .w_NOMFIN OR EMPTY(.w_NOMFIN))  and not(empty(.w_NOMINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOMINI_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NOMINI <= .w_NOMFIN OR EMPTY(.w_NOMFIN))  and not(empty(.w_NOMFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOMFIN_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PRIOINI <= .w_PRIOFIN OR EMPTY(.w_PRIOFIN))  and not(g_AGEN='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPRIOINI_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La priorit� iniziale deve essere minore della priorit� finale")
          case   not(.w_PRIOINI <= .w_PRIOFIN OR EMPTY(.w_PRIOINI))  and not(g_AGEN='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPRIOFIN_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La priorit� iniziale deve essere minore della priorit� finale")
          case   not(.w_PRIOINI <= .w_PRIOFIN OR EMPTY(.w_PRIOFIN))  and not(g_AGEN<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPRIOINI_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La priorit� iniziale deve essere minore della priorit� finale")
          case   not(.w_TIPOATINI <= .w_TIPOATFIN OR EMPTY(.w_TIPOATFIN))  and not(empty(.w_TIPOATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOATINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPOATINI <= .w_TIPOATFIN OR EMPTY(.w_TIPOATFIN))  and not(empty(.w_TIPOATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOATFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ESITOATI <= .w_ESITOATF OR EMPTY(.w_ESITOATF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oESITOATI_2_53.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'esito attivit� iniziale deve essere minore o uguale all'esito attivit� finale")
          case   not(.w_ESITOATI <= .w_ESITOATF OR EMPTY(.w_ESITOATI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oESITOATF_2_55.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'esito attivit� iniziale deve essere minore o uguale all'esito attivit� finale")
          case   not(.w_PRIOINI <= .w_PRIOFIN OR EMPTY(.w_PRIOFIN))  and not(g_AGEN<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPRIOFIN_2_75.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La priorit� iniziale deve essere minore della priorit� finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATAINI = this.w_DATAINI
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    this.o_DATAINI1 = this.w_DATAINI1
    this.o_ORAINI1 = this.w_ORAINI1
    this.o_MININI1 = this.w_MININI1
    this.o_DATAFIN = this.w_DATAFIN
    this.o_ORAFIN = this.w_ORAFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_DATAFIN1 = this.w_DATAFIN1
    this.o_ORAFIN1 = this.w_ORAFIN1
    this.o_MINFIN1 = this.w_MINFIN1
    this.o_NOMINI = this.w_NOMINI
    this.o_NOMFIN = this.w_NOMFIN
    this.o_STATO = this.w_STATO
    this.o_SUBJECT = this.w_SUBJECT
    return

enddefine

* --- Define pages as container
define class tgsof_katPag1 as StdContainer
  Width  = 862
  height = 506
  stdWidth  = 862
  stdheight = 506
  resizeXpos=335
  resizeYpos=364
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOM as cp_zoombox with uid="XBTEELQVTF",left=9, top=124, width=745,height=326,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",bQueryOnLoad=.f.,bReadOnly=.t.,cZoomFile="GSOF_KAT",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,bRetriveAllRows=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Init,Ricerca",;
    nPag=1;
    , HelpContextID = 45211110

  add object oOPERAT_1_2 as StdField with uid="TVADFWZWKK",rtseq=23,rtrep=.f.,;
    cFormVar = "w_OPERAT", cQueryName = "OPERAT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 8158182,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=15, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_OPERAT"

  func oOPERAT_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (CP_ISADMINISTRATOR())
    endwith
   endif
  endfunc

  func oOPERAT_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oOPERAT_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOPERAT_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oOPERAT_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco operatori",'',this.parent.oContained
  endproc

  add object oNAME_1_3 as StdField with uid="WASGPNEQLK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_NAME", cQueryName = "NAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione operatore",;
    HelpContextID = 127931178,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=216, Top=15, InputMask=replicate('X',20)


  add object oSTATO2_1_4 as StdCombo with uid="UAVRKIKCDT",rtseq=25,rtrep=.f.,left=632,top=14,width=122,height=21;
    , ToolTipText = "Stato attivit�";
    , HelpContextID = 10600410;
    , cFormVar="w_STATO2",RowSource=""+"Non evasa,"+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO2_1_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'T',;
    iif(this.value =3,'D',;
    iif(this.value =4,'I',;
    iif(this.value =5,'F',;
    iif(this.value =6,'K',;
    space(1))))))))
  endfunc
  func oSTATO2_1_4.GetRadio()
    this.Parent.oContained.w_STATO2 = this.RadioValue()
    return .t.
  endfunc

  func oSTATO2_1_4.SetRadio()
    this.Parent.oContained.w_STATO2=trim(this.Parent.oContained.w_STATO2)
    this.value = ;
      iif(this.Parent.oContained.w_STATO2=='N',1,;
      iif(this.Parent.oContained.w_STATO2=='T',2,;
      iif(this.Parent.oContained.w_STATO2=='D',3,;
      iif(this.Parent.oContained.w_STATO2=='I',4,;
      iif(this.Parent.oContained.w_STATO2=='F',5,;
      iif(this.Parent.oContained.w_STATO2=='K',6,;
      0))))))
  endfunc

  func oSTATO2_1_4.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc


  add object oSTATO1_1_5 as StdCombo with uid="IEURZMYTAN",rtseq=26,rtrep=.f.,left=632,top=14,width=122,height=21;
    , ToolTipText = "Stato attivit�";
    , HelpContextID = 27377626;
    , cFormVar="w_STATO1",RowSource=""+"Aperta,"+"Chiusa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO1_1_5.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oSTATO1_1_5.GetRadio()
    this.Parent.oContained.w_STATO1 = this.RadioValue()
    return .t.
  endfunc

  func oSTATO1_1_5.SetRadio()
    this.Parent.oContained.w_STATO1=trim(this.Parent.oContained.w_STATO1)
    this.value = ;
      iif(this.Parent.oContained.w_STATO1=='A',1,;
      iif(this.Parent.oContained.w_STATO1=='C',2,;
      0))
  endfunc

  func oSTATO1_1_5.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc

  add object oTIPOATINI_1_6 as StdField with uid="IGBZFKDZTV",rtseq=27,rtrep=.f.,;
    cFormVar = "w_TIPOATINI", cQueryName = "TIPOATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia attivit�",;
    HelpContextID = 260429292,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=46, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", cZoomOnZoom="GSOF_ATA", oKey_1_1="TACODTIP", oKey_1_2="this.w_TIPOATINI"

  func oTIPOATINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOATINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOATINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oTIPOATINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATA',"Tipologie attivit�",'',this.parent.oContained
  endproc
  proc oTIPOATINI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODTIP=this.parent.oContained.w_TIPOATINI
     i_obj.ecpSave()
  endproc

  add object oTIPOATFIN_1_7 as StdField with uid="ZXLFNIHWZK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_TIPOATFIN", cQueryName = "TIPOATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia attivit�",;
    HelpContextID = 8006239,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=518, Top=46, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", cZoomOnZoom="GSOF_ATA", oKey_1_1="TACODTIP", oKey_1_2="this.w_TIPOATFIN"

  func oTIPOATFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOATFIN_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOATFIN_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oTIPOATFIN_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATA',"Tipologie attivit�",'',this.parent.oContained
  endproc
  proc oTIPOATFIN_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODTIP=this.parent.oContained.w_TIPOATFIN
     i_obj.ecpSave()
  endproc

  add object oSUBJECT_1_9 as StdField with uid="MWYPXPNUGK",rtseq=30,rtrep=.f.,;
    cFormVar = "w_SUBJECT", cQueryName = "SUBJECT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto",;
    HelpContextID = 263475494,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=152, Top=77, InputMask=replicate('X',254)


  add object oBtn_1_10 as StdButton with uid="APIWEFNJEL",left=706, top=76, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare le attivit�";
    , HelpContextID = 132786442;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTIPODES_1_15 as StdField with uid="MHDPDKZRBO",rtseq=34,rtrep=.f.,;
    cFormVar = "w_TIPODES", cQueryName = "TIPODES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipologia attivit�",;
    HelpContextID = 27927862,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=216, Top=46, InputMask=replicate('X',50)

  add object oTIPODES1_1_17 as StdField with uid="MODTKPLMEO",rtseq=35,rtrep=.f.,;
    cFormVar = "w_TIPODES1", cQueryName = "TIPODES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipologia attivit�",;
    HelpContextID = 27927911,;
   bGlobalFont=.t.,;
    Height=21, Width=171, Left=583, Top=46, InputMask=replicate('X',50)


  add object oBtn_1_20 as StdButton with uid="GTVKTOIVKV",left=706, top=456, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 132786442;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="JLSDJFKBOF",left=109, top=456, width=48,height=45,;
    CpPicture="bmp\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per creare una nuova attivit�";
    , HelpContextID = 263609382;
    , caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="ILTPDBMKTC",left=59, top=456, width=48,height=45,;
    CpPicture="bmp\Modifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per modificare l'attivit�";
    , HelpContextID = 132483879;
    , caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="RTFYVLRCLJ",left=9, top=456, width=48,height=45,;
    CpPicture="bmp\VISUALI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare l'attivit�";
    , HelpContextID = 89989520;
    , caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc


  add object oObj_1_24 as cp_runprogram with uid="YZQGSJHCRK",left=44, top=549, width=188,height=20,;
    caption='GSOF_BAT(V)',;
   bGlobalFont=.t.,;
    prg="GSOF_BAT('V')",;
    cEvent = "w_ZOOM selected",;
    nPag=1;
    , HelpContextID = 5506874


  add object oBtn_1_25 as StdButton with uid="JUHQJOOOYG",left=159, top=456, width=48,height=45,;
    CpPicture="bmp\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare le attivit� selezionate";
    , HelpContextID = 9003046;
    , caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_11 as StdString with uid="TDPGPHZSYZ",Visible=.t., Left=13, Top=15,;
    Alignment=1, Width=138, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="VQVHVEIIGN",Visible=.t., Left=496, Top=14,;
    Alignment=1, Width=133, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="NPICABCYRW",Visible=.t., Left=13, Top=81,;
    Alignment=1, Width=138, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="TQNWSBHXFA",Visible=.t., Left=13, Top=46,;
    Alignment=1, Width=138, Height=18,;
    Caption="Da tipologia attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="LIERLIMINU",Visible=.t., Left=376, Top=46,;
    Alignment=1, Width=141, Height=18,;
    Caption="A tipologia attivit�:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_katPag2 as StdContainer
  Width  = 862
  height = 506
  stdWidth  = 862
  stdheight = 506
  resizeXpos=361
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATAINI_2_1 as StdField with uid="QXSPXYXHLS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit�",;
    HelpContextID = 85173194,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=146, Top=28

  add object oORAINI_2_2 as StdField with uid="IFZETBJSDN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 163365402,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=389, Top=28, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_2_3 as StdField with uid="BMAZHAYBOV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 163314490,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=423, Top=28, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATAINI1_2_4 as StdField with uid="ZPYELSAXLT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAINI1", cQueryName = "DATAINI1",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit�",;
    HelpContextID = 85173145,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=146, Top=54

  add object oORAINI1_2_5 as StdField with uid="ILHYLKGKZP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ORAINI1", cQueryName = "ORAINI1",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 105070054,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=389, Top=54, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI1_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI1_2_6 as StdField with uid="LJLDZENMNB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MININI1", cQueryName = "MININI1",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 105120966,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=423, Top=54, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI1_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATAFIN_2_7 as StdField with uid="IMMOZELIYO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit�",;
    HelpContextID = 172205002,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=146, Top=89

  add object oORAFIN_2_8 as StdField with uid="TWVCQEVEXZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora fine attivit�",;
    HelpContextID = 84918810,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=389, Top=93, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_2_10 as StdField with uid="VEYPJJGXRE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti fine attivit�",;
    HelpContextID = 84867898,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=423, Top=93, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oDATAFIN1_2_11 as StdField with uid="QEQVFVGKVE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATAFIN1", cQueryName = "DATAFIN1",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit�",;
    HelpContextID = 172204953,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=146, Top=115

  add object oORAFIN1_2_12 as StdField with uid="YOAYBAMOLC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ORAFIN1", cQueryName = "ORAFIN1",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora fine attivit�",;
    HelpContextID = 183516646,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=389, Top=119, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN1_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN1_2_13 as StdField with uid="XARMYFXLXJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MINFIN1", cQueryName = "MINFIN1",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti fine attivit�",;
    HelpContextID = 183567558,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=423, Top=119, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN1_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oNOMINI_2_14 as StdField with uid="ZEFEYIMGOO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NOMINI", cQueryName = "NOMINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo",;
    HelpContextID = 163317034,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=146, Top=163, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_NOMINI"

  func oNOMINI_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
      if .not. empty(.w_CONTINI)
        bRes2=.link_2_16('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oNOMINI_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOMINI_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oNOMINI_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oNOMINI_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_NOMINI
     i_obj.ecpSave()
  endproc

  add object oNOMFIN_2_15 as StdField with uid="NPWFHLHJJO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NOMFIN", cQueryName = "NOMFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo",;
    HelpContextID = 84870442,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=146, Top=191, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_NOMFIN"

  func oNOMFIN_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOMFIN_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOMFIN_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oNOMFIN_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'',this.parent.oContained
  endproc
  proc oNOMFIN_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_NOMFIN
     i_obj.ecpSave()
  endproc

  add object oCONTINI_2_16 as StdField with uid="TPGDOPMLNT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CONTINI", cQueryName = "CONTINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Contatto",;
    HelpContextID = 83949018,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=146, Top=227, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCCODICE", oKey_1_2="this.w_NOMINI", oKey_2_1="NCCODCON", oKey_2_2="this.w_CONTINI"

  func oCONTINI_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOMINI = .w_NOMFIN And NOT EMPTY(.w_NOMINI))
    endwith
   endif
  endfunc

  func oCONTINI_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTINI_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTINI_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.NOM_CONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_NOMINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStr(this.Parent.oContained.w_NOMINI)
    endif
    do cp_zoom with 'NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(this.parent,'oCONTINI_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oPRIOINI_2_17 as StdField with uid="JXKZIUYBKI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PRIOINI", cQueryName = "PRIOINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La priorit� iniziale deve essere minore della priorit� finale",;
    ToolTipText = "Priorit� attivit�",;
    HelpContextID = 84296202,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=146, Top=261, cSayPict='"999999"', cGetPict='"999999"'

  func oPRIOINI_2_17.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc

  func oPRIOINI_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRIOINI <= .w_PRIOFIN OR EMPTY(.w_PRIOFIN))
    endwith
    return bRes
  endfunc

  add object oPRIOFIN_2_18 as StdField with uid="ZKWPXFHOXZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PRIOFIN", cQueryName = "PRIOFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La priorit� iniziale deve essere minore della priorit� finale",;
    ToolTipText = "Priorit� attivit�",;
    HelpContextID = 171328010,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=415, Top=261, cSayPict='"999999"', cGetPict='"999999"'

  func oPRIOFIN_2_18.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc

  func oPRIOFIN_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRIOINI <= .w_PRIOFIN OR EMPTY(.w_PRIOINI))
    endwith
    return bRes
  endfunc


  add object oPRIOINI_2_19 as StdCombo with uid="TGDRVMLJAL",rtseq=18,rtrep=.f.,left=146,top=261,width=130,height=21;
    , ToolTipText = "Priorit� attivit�";
    , HelpContextID = 84296202;
    , cFormVar="w_PRIOINI",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine", bObbl = .f. , nPag = 2;
    , sErrorMsg = "La priorit� iniziale deve essere minore della priorit� finale";
  , bGlobalFont=.t.


  func oPRIOINI_2_19.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,3,;
    iif(this.value =3,4,;
    0))))
  endfunc
  func oPRIOINI_2_19.GetRadio()
    this.Parent.oContained.w_PRIOINI = this.RadioValue()
    return .t.
  endfunc

  func oPRIOINI_2_19.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PRIOINI==1,1,;
      iif(this.Parent.oContained.w_PRIOINI==3,2,;
      iif(this.Parent.oContained.w_PRIOINI==4,3,;
      0)))
  endfunc

  func oPRIOINI_2_19.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  func oPRIOINI_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRIOINI <= .w_PRIOFIN OR EMPTY(.w_PRIOFIN))
    endwith
    return bRes
  endfunc

  add object oNODESCRI_2_49 as StdField with uid="OHNGASKYDN",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nominativo",;
    HelpContextID = 9399071,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=304, Top=163, InputMask=replicate('X',40)

  add object oNODESCRI1_2_50 as StdField with uid="JFLYUEUMZN",rtseq=32,rtrep=.f.,;
    cFormVar = "w_NODESCRI1", cQueryName = "NODESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nominativo",;
    HelpContextID = 9399855,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=304, Top=190, InputMask=replicate('X',40)

  add object oNCPERSON_2_51 as StdField with uid="ZADRIKKLNS",rtseq=33,rtrep=.f.,;
    cFormVar = "w_NCPERSON", cQueryName = "NCPERSON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contatto",;
    HelpContextID = 260038876,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=198, Top=227, InputMask=replicate('X',40)


  add object oESITOATI_2_53 as StdTableCombo with uid="TTIVZNOMCH",rtseq=36,rtrep=.f.,left=146,top=296,width=179,height=21;
    , ToolTipText = "Esito attivit�";
    , HelpContextID = 241090191;
    , cFormVar="w_ESITOATI",tablefilter="", bObbl = .f. , nPag = 2;
    , sErrorMsg = "L'esito attivit� iniziale deve essere minore o uguale all'esito attivit� finale";
    , cTable='OFF_ESIA',cKey='ESCODESI',cValue='ESDESCRI',cOrderBy='ESCODESI',xDefault=space(5);
  , bGlobalFont=.t.


  func oESITOATI_2_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ESITOATI <= .w_ESITOATF OR EMPTY(.w_ESITOATF))
    endwith
    return bRes
  endfunc


  add object oESITOATF_2_55 as StdTableCombo with uid="WAVGOIPZBI",rtseq=37,rtrep=.f.,left=146,top=328,width=179,height=21;
    , ToolTipText = "Esito attivit�";
    , HelpContextID = 241090188;
    , cFormVar="w_ESITOATF",tablefilter="", bObbl = .f. , nPag = 2;
    , sErrorMsg = "L'esito attivit� iniziale deve essere minore o uguale all'esito attivit� finale";
    , cTable='OFF_ESIA',cKey='ESCODESI',cValue='ESDESCRI',cOrderBy='ESCODESI',xDefault=space(5);
  , bGlobalFont=.t.


  func oESITOATF_2_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ESITOATI <= .w_ESITOATF OR EMPTY(.w_ESITOATI))
    endwith
    return bRes
  endfunc


  add object oBtn_2_56 as StdButton with uid="CXRFQHUCZY",left=657, top=359, width=20,height=22,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per selezionare una opportunit�/offerta";
    , HelpContextID = 132585514;
  , bGlobalFont=.t.

    proc oBtn_2_56.Click()
      with this.Parent.oContained
        LinkOfferte(This.Parent.oContained)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oOFF_SER_2_62 as StdField with uid="VVLGJBEWXM",rtseq=41,rtrep=.f.,;
    cFormVar = "w_OFF_SER", cQueryName = "OFF_SER",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 44663270,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=779, Top=92, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="OFF_ERTE", cZoomOnZoom="GSOF_AOF", oKey_1_1="OFSERIAL", oKey_1_2="this.w_OFF_SER"

  func oOFF_SER_2_62.mHide()
    with this.Parent.oContained
      return (1 = 1)
    endwith
  endfunc

  func oOFF_SER_2_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFF_SER_2_62.ecpDrop(oSource)
    this.Parent.oContained.link_2_62('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFF_SER_2_62.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_ERTE','*','OFSERIAL',cp_AbsName(this.parent,'oOFF_SER_2_62'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AOF',"Opportunita/offerte",'',this.parent.oContained
  endproc
  proc oOFF_SER_2_62.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AOF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OFSERIAL=this.parent.oContained.w_OFF_SER
     i_obj.ecpSave()
  endproc


  add object oOFSTATUS_2_63 as StdCombo with uid="WJAVJWDSCL",rtseq=42,rtrep=.f.,left=537,top=360,width=115,height=21, enabled=.f.;
    , ToolTipText = "Stato opportunit�/offerta";
    , HelpContextID = 8344121;
    , cFormVar="w_OFSTATUS",RowSource=""+"In corso,"+"Inviata,"+"Confermata,"+"Versione chiusa,"+"Rifiutata,"+"Sospesa", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOFSTATUS_2_63.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'V',;
    iif(this.value =5,'R',;
    iif(this.value =6,'S',;
    space(1))))))))
  endfunc
  func oOFSTATUS_2_63.GetRadio()
    this.Parent.oContained.w_OFSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oOFSTATUS_2_63.SetRadio()
    this.Parent.oContained.w_OFSTATUS=trim(this.Parent.oContained.w_OFSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_OFSTATUS=='I',1,;
      iif(this.Parent.oContained.w_OFSTATUS=='A',2,;
      iif(this.Parent.oContained.w_OFSTATUS=='C',3,;
      iif(this.Parent.oContained.w_OFSTATUS=='V',4,;
      iif(this.Parent.oContained.w_OFSTATUS=='R',5,;
      iif(this.Parent.oContained.w_OFSTATUS=='S',6,;
      0))))))
  endfunc

  add object oOFNUMDOC_2_64 as StdField with uid="LQFYCLYDMU",rtseq=43,rtrep=.f.,;
    cFormVar = "w_OFNUMDOC", cQueryName = "OFNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 247463383,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=146, Top=360, cSayPict='"999999999999999"'

  func oOFNUMDOC_2_64.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oOFDATDOC_2_65 as StdField with uid="SCFATDDIMX",rtseq=44,rtrep=.f.,;
    cFormVar = "w_OFDATDOC", cQueryName = "OFDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 241475031,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=402, Top=360

  add object oOFSERDOC_2_66 as StdField with uid="LRTHJETKKI",rtseq=45,rtrep=.f.,;
    cFormVar = "w_OFSERDOC", cQueryName = "OFSERDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 243248599,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=272, Top=360, InputMask=replicate('X',10)

  func oOFSERDOC_2_66.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oOFNUMDOC_2_72 as StdField with uid="HOBSNZXZZF",rtseq=46,rtrep=.f.,;
    cFormVar = "w_OFNUMDOC", cQueryName = "OFNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 247463383,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=146, Top=360, cSayPict='"999999"'

  func oOFNUMDOC_2_72.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE")
    endwith
  endfunc

  add object oOFSERDOC_2_73 as StdField with uid="XEWELEQXUG",rtseq=47,rtrep=.f.,;
    cFormVar = "w_OFSERDOC", cQueryName = "OFSERDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 243248599,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=209, Top=360, InputMask=replicate('X',2)

  func oOFSERDOC_2_73.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE")
    endwith
  endfunc


  add object oPRIOFIN_2_75 as StdCombo with uid="AIMCJUHDAH",rtseq=48,rtrep=.f.,left=415,top=261,width=130,height=21;
    , ToolTipText = "Priorit� attivit�";
    , HelpContextID = 171328010;
    , cFormVar="w_PRIOFIN",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine", bObbl = .f. , nPag = 2;
    , sErrorMsg = "La priorit� iniziale deve essere minore della priorit� finale";
  , bGlobalFont=.t.


  func oPRIOFIN_2_75.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,3,;
    iif(this.value =3,4,;
    0))))
  endfunc
  func oPRIOFIN_2_75.GetRadio()
    this.Parent.oContained.w_PRIOFIN = this.RadioValue()
    return .t.
  endfunc

  func oPRIOFIN_2_75.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PRIOFIN==1,1,;
      iif(this.Parent.oContained.w_PRIOFIN==3,2,;
      iif(this.Parent.oContained.w_PRIOFIN==4,3,;
      0)))
  endfunc

  func oPRIOFIN_2_75.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  func oPRIOFIN_2_75.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRIOINI <= .w_PRIOFIN OR EMPTY(.w_PRIOFIN))
    endwith
    return bRes
  endfunc


  add object oObj_2_76 as cp_outputCombo with uid="ZLERXIVPWT",left=145, top=427, width=509,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=2;
    , HelpContextID = 45211110

  add object oStr_2_9 as StdString with uid="NORSJWWDPX",Visible=.t., Left=418, Top=28,;
    Alignment=1, Width=3, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="CXKRSVQCMU",Visible=.t., Left=225, Top=28,;
    Alignment=1, Width=160, Height=18,;
    Caption="Ora inizio attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="APZVWNKQUA",Visible=.t., Left=418, Top=93,;
    Alignment=1, Width=3, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="YYAKHNGDYF",Visible=.t., Left=225, Top=93,;
    Alignment=1, Width=160, Height=18,;
    Caption="Ora fine attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="PGFKKSQZAP",Visible=.t., Left=8, Top=28,;
    Alignment=1, Width=134, Height=18,;
    Caption="Da data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="UKKSUZUVPA",Visible=.t., Left=8, Top=89,;
    Alignment=1, Width=134, Height=18,;
    Caption="Da data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="ARBUVCCFDP",Visible=.t., Left=418, Top=54,;
    Alignment=1, Width=3, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="KHPZJAUXQX",Visible=.t., Left=225, Top=54,;
    Alignment=1, Width=160, Height=18,;
    Caption="Ora inizio attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="AECHOLBITB",Visible=.t., Left=8, Top=54,;
    Alignment=1, Width=134, Height=18,;
    Caption="A data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="WOQMDLAQFC",Visible=.t., Left=418, Top=119,;
    Alignment=1, Width=3, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="ZSGGHTRDKF",Visible=.t., Left=225, Top=119,;
    Alignment=1, Width=160, Height=18,;
    Caption="Ora fine attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="YAXOGZMPES",Visible=.t., Left=8, Top=115,;
    Alignment=1, Width=134, Height=18,;
    Caption="A data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="YIVXVDYBML",Visible=.t., Left=8, Top=167,;
    Alignment=1, Width=134, Height=18,;
    Caption="Da nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="XXNSFMGMNX",Visible=.t., Left=8, Top=195,;
    Alignment=1, Width=134, Height=18,;
    Caption="A nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="PIIQBSXYVV",Visible=.t., Left=8, Top=231,;
    Alignment=1, Width=134, Height=18,;
    Caption="Contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="DVCGDWMFWM",Visible=.t., Left=8, Top=296,;
    Alignment=1, Width=134, Height=18,;
    Caption="Da esito attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="CBNRPSJPBN",Visible=.t., Left=8, Top=328,;
    Alignment=1, Width=134, Height=18,;
    Caption="A esito attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_58 as StdString with uid="KHWVLZCVUG",Visible=.t., Left=8, Top=427,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="RTDLJUCFQF",Visible=.t., Left=8, Top=261,;
    Alignment=1, Width=134, Height=18,;
    Caption="Da priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="YWSPPZLKZT",Visible=.t., Left=286, Top=261,;
    Alignment=1, Width=126, Height=18,;
    Caption="A priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_67 as StdString with uid="PDAGFAJEHK",Visible=.t., Left=266, Top=360,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_67.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_2_68 as StdString with uid="MSWKBXMGNU",Visible=.t., Left=489, Top=360,;
    Alignment=1, Width=46, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_69 as StdString with uid="RHWKDBVNLE",Visible=.t., Left=361, Top=360,;
    Alignment=1, Width=38, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_70 as StdString with uid="RJNIIFDFWJ",Visible=.t., Left=8, Top=360,;
    Alignment=1, Width=134, Height=18,;
    Caption="Opportunit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_70.mHide()
    with this.Parent.oContained
      return (.w_OFSTATUS<>'I')
    endwith
  endfunc

  add object oStr_2_71 as StdString with uid="VINNSDQUWW",Visible=.t., Left=8, Top=360,;
    Alignment=1, Width=134, Height=18,;
    Caption="Offerta:"  ;
  , bGlobalFont=.t.

  func oStr_2_71.mHide()
    with this.Parent.oContained
      return (.w_OFSTATUS='I')
    endwith
  endfunc

  add object oStr_2_74 as StdString with uid="JYKZCIYBKZ",Visible=.t., Left=202, Top=360,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_74.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE")
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_kat','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsof_kat
*--- Effettua il link sulle offerte, lanciato dal bottone ...
Proc LinkOfferte(oParent)
    l_obj = oParent.GetCtrl('w_OFF_SER')
    oParent.bDontReportError=.t.
    public i_lastindirectaction
    i_lastindirectaction='ecpZoom'	
    l_obj.setFocus()
	  l_obj.mzoom()
EndFunc
* --- Fine Area Manuale
