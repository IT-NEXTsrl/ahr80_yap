* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_brn                                                        *
*              Eventi da gestione ricerca nominativi                           *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_158]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-28                                                      *
* Last revis.: 2007-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_brn",oParentObject,m.Azione)
return(i_retval)

define class tgsof_brn as StdBatch
  * --- Local variables
  Azione = space(10)
  w_prog = .NULL.
  DOPER = space(35)
  W_NOCODICE = 0
  * --- WorkFile variables
  ALL_EGAT_idx=0
  CPUSERS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Gestione Ricerca Nominativ' (da GSOF_KRN)
    do case
      case this.Azione="APRI_NOMINATIVO"
        this.w_PROG = GSAR_ANO()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_NOTIPCLI = "C"
        this.w_PROG.w_NOCODICE = this.oParentObject.w_SERCOD
        this.w_PROG.ecpSave()     
        this.w_PROG.ecpQuery()     
      case this.Azione="RICERCA_NOMINATIVO"
        * --- Alcuni controlli prima di eseguire la query
        if this.oParentObject.w_SNNUMINI>0 AND this.oParentObject.w_SNNUMFIN=0
          this.oParentObject.w_SNNUMFIN = this.oParentObject.w_SNNUMINI
        endif
        * --- Eseguo la  query 
        this.oParentObject.NotifyEvent("Ricerca")
        NM = this.oParentObject.w_ZoomAll.cCursor
        * --- Eseguo la stessa query ordinata x campi diversi in un cursore di appoggio
        do case
          case this.oParentObject.w_ORDNOM="D"
            SELECT * FROM (NM) GROUP BY NOCODICE INTO CURSOR __CURS__ order BY NODESCRI,NOCODPRI DESC
          case this.oParentObject.w_ORDNOM="P"
            SELECT * FROM (NM) GROUP BY NOCODICE INTO CURSOR __CURS__ order BY NOCODPRI DESC,NODESCRI
          case this.oParentObject.w_ORDNOM="T"
            SELECT * FROM (NM) GROUP BY NOCODICE INTO CURSOR __CURS__ order BY NOTIPNOM,NOCODPRI DESC,NODESCRI
          case this.oParentObject.w_ORDNOM="A"
            SELECT * FROM (NM) GROUP BY NOCODICE INTO CURSOR __CURS__ order BY NOASSOPE,NOCODPRI DESC,NODESCRI
          case this.oParentObject.w_ORDNOM="O"
            SELECT * FROM (NM) GROUP BY NOCODICE INTO CURSOR __CURS__ order BY NOCODOPE,NOCODPRI DESC,NODESCRI
        endcase
        * --- Mi preparo il cursore vuoto che andra' a riempire lo zoom
        this.oParentObject.w_COD = "zxzxzxzxzxzxzx"
        this.oParentObject.NotifyEvent("Ricerca")
        * --- Muovo i record Ordinati nel cursore vuoto.
        SELECT __CURS__ 
        SCAN
        scatter memvar
        INSERT INTO (NM) FROM MEMVAR
        ENDSCAN
        * --- Carico la descrizione operatore
        SELECT __CURS__ 
        GO TOP
        SCAN
        this.W_NOCODICE = NOCODOPE
        if NOT ISNULL(this.w_NOCODICE)
          * --- Read from CPUSERS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CPUSERS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPUSERS_idx,2],.t.,this.CPUSERS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NAME"+;
              " from "+i_cTable+" CPUSERS where ";
                  +"CODE = "+cp_ToStrODBC(this.W_NOCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NAME;
              from (i_cTable) where;
                  CODE = this.W_NOCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.DOPER = NVL(cp_ToDate(_read_.NAME),cp_NullValue(_read_.NAME))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        UPDATE (NM) SET DESOPE=this.DOPER WHERE this.W_NOCODICE=NOCODOPE
        ENDSCAN
        * --- Refresh della griglia
        SELECT (NM)
        GO TOP
        this.oParentObject.w_ZoomAll.refresh()
    endcase
    this.oParentObject.w_COD = " "
    if USED("__CURS__ ")
      SELECT __CURS__ 
      USE
    endif
    if USED("cCursor")
      SELECT cCursor 
      USE
    endif
  endproc


  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ALL_EGAT'
    this.cWorkTables[2]='CPUSERS'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
