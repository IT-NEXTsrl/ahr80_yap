* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_br2                                                        *
*              Cancella attributi                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_27]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-04                                                      *
* Last revis.: 2003-02-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_br2",oParentObject,m.pOPE)
return(i_retval)

define class tgsof_br2 as StdBatch
  * --- Local variables
  pOPE = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sbianca gli attributi successivi a quello corrente - da GSOF_KRN - GSOF_KRA
    * --- Codici attributo
    * --- Descrizione categoria
    * --- Descrizione attributi
    * --- Codice categoria
    * --- Scelta chiamata 1 - RACODAT1, 2 - RACODAT2 ....
    do case
      case this.pOPE="1"
        if EMPTY(this.oParentObject.w_RACODATT)
          * --- Cancellazione codici
          this.oParentObject.w_RACODAT1 = " "
          * --- Cancellazione descrizione categorie
          this.oParentObject.w_DESCAT1 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT1 = " "
          * --- Cancellazione codici categoria
          this.oParentObject.w_TIPCAT1 = " "
        endif
        if EMPTY(this.oParentObject.w_RACODAT1)
          * --- Cancellazione codici
          this.oParentObject.w_RACODAT2 = " "
          this.oParentObject.w_RACODAT3 = " "
          this.oParentObject.w_RACODAT4 = " "
          this.oParentObject.w_RACODAT5 = " "
          this.oParentObject.w_RACODAT6 = " "
          this.oParentObject.w_RACODAT7 = " "
          this.oParentObject.w_RACODAT8 = " "
          this.oParentObject.w_RACODAT9 = " "
          * --- Cancellazione descrizione categorie
          this.oParentObject.w_DESCAT2 = " "
          this.oParentObject.w_DESCAT3 = " "
          this.oParentObject.w_DESCAT4 = " "
          this.oParentObject.w_DESCAT5 = " "
          this.oParentObject.w_DESCAT6 = " "
          this.oParentObject.w_DESCAT7 = " "
          this.oParentObject.w_DESCAT8 = " "
          this.oParentObject.w_DESCAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT1 = " "
          this.oParentObject.w_DESATT2 = " "
          this.oParentObject.w_DESATT3 = " "
          this.oParentObject.w_DESATT4 = " "
          this.oParentObject.w_DESATT5 = " "
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codici categoria
          this.oParentObject.w_TIPCAT1 = " "
          this.oParentObject.w_TIPCAT2 = " "
          this.oParentObject.w_TIPCAT3 = " "
          this.oParentObject.w_TIPCAT4 = " "
          this.oParentObject.w_TIPCAT5 = " "
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="2"
        if EMPTY(this.oParentObject.w_RACODAT2)
          * --- Cancellazione codici
          this.oParentObject.w_RACODAT3 = " "
          this.oParentObject.w_RACODAT4 = " "
          this.oParentObject.w_RACODAT5 = " "
          this.oParentObject.w_RACODAT6 = " "
          this.oParentObject.w_RACODAT7 = " "
          this.oParentObject.w_RACODAT8 = " "
          this.oParentObject.w_RACODAT9 = " "
          * --- Cancellazione descrizione categorie
          this.oParentObject.w_DESCAT3 = " "
          this.oParentObject.w_DESCAT4 = " "
          this.oParentObject.w_DESCAT5 = " "
          this.oParentObject.w_DESCAT6 = " "
          this.oParentObject.w_DESCAT7 = " "
          this.oParentObject.w_DESCAT8 = " "
          this.oParentObject.w_DESCAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT3 = " "
          this.oParentObject.w_DESATT4 = " "
          this.oParentObject.w_DESATT5 = " "
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codici categoria
          this.oParentObject.w_TIPCAT3 = " "
          this.oParentObject.w_TIPCAT4 = " "
          this.oParentObject.w_TIPCAT5 = " "
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="3"
        if EMPTY(this.oParentObject.w_RACODAT3)
          * --- Cancellazione codici
          this.oParentObject.w_RACODAT4 = " "
          this.oParentObject.w_RACODAT5 = " "
          this.oParentObject.w_RACODAT6 = " "
          this.oParentObject.w_RACODAT7 = " "
          this.oParentObject.w_RACODAT8 = " "
          this.oParentObject.w_RACODAT9 = " "
          * --- Cancellazione descrizione categorie
          this.oParentObject.w_DESCAT4 = " "
          this.oParentObject.w_DESCAT5 = " "
          this.oParentObject.w_DESCAT6 = " "
          this.oParentObject.w_DESCAT7 = " "
          this.oParentObject.w_DESCAT8 = " "
          this.oParentObject.w_DESCAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT4 = " "
          this.oParentObject.w_DESATT5 = " "
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codici categoria
          this.oParentObject.w_TIPCAT4 = " "
          this.oParentObject.w_TIPCAT5 = " "
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="4"
        if EMPTY(this.oParentObject.w_RACODAT4)
          * --- Cancellazione codici
          this.oParentObject.w_RACODAT5 = " "
          this.oParentObject.w_RACODAT6 = " "
          this.oParentObject.w_RACODAT7 = " "
          this.oParentObject.w_RACODAT8 = " "
          this.oParentObject.w_RACODAT9 = " "
          * --- Cancellazione descrizione categorie
          this.oParentObject.w_DESCAT5 = " "
          this.oParentObject.w_DESCAT6 = " "
          this.oParentObject.w_DESCAT7 = " "
          this.oParentObject.w_DESCAT8 = " "
          this.oParentObject.w_DESCAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT5 = " "
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codici categoria
          this.oParentObject.w_TIPCAT5 = " "
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="5" 
 
        if EMPTY(this.oParentObject.w_RACODAT5)
          * --- Cancellazione codici
          this.oParentObject.w_RACODAT6 = " "
          this.oParentObject.w_RACODAT7 = " "
          this.oParentObject.w_RACODAT8 = " "
          this.oParentObject.w_RACODAT9 = " "
          * --- Cancellazione descrizione categorie
          this.oParentObject.w_DESCAT6 = " "
          this.oParentObject.w_DESCAT7 = " "
          this.oParentObject.w_DESCAT8 = " "
          this.oParentObject.w_DESCAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codici categoria
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="6"
        if EMPTY(this.oParentObject.w_RACODAT6)
          * --- Cancellazione codici
          this.oParentObject.w_RACODAT7 = " "
          this.oParentObject.w_RACODAT8 = " "
          this.oParentObject.w_RACODAT9 = " "
          * --- Cancellazione descrizione categorie
          this.oParentObject.w_DESCAT7 = " "
          this.oParentObject.w_DESCAT8 = " "
          this.oParentObject.w_DESCAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codici categoria
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="7"
        if EMPTY(this.oParentObject.w_RACODAT7)
          * --- Cancellazione codici
          this.oParentObject.w_RACODAT8 = " "
          this.oParentObject.w_RACODAT9 = " "
          * --- Cancellazione descrizione categorie
          this.oParentObject.w_DESCAT8 = " "
          this.oParentObject.w_DESCAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codici categoria
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="8"
        if EMPTY(this.oParentObject.w_RACODAT8)
          * --- Cancellazione codice
          this.oParentObject.w_RACODAT9 = " "
          * --- Cancellazione descrizione categoria
          this.oParentObject.w_DESCAT9 = " "
          * --- Cancellazione descrizione attributo
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codici categoria
          this.oParentObject.w_TIPCAT9 = " "
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPE)
    this.pOPE=pOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPE"
endproc
