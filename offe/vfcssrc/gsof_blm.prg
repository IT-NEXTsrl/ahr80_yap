* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_blm                                                        *
*              Legge modello offerte                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_199]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-09                                                      *
* Last revis.: 2014-01-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_blm",oParentObject)
return(i_retval)

define class tgsof_blm as StdBatch
  * --- Local variables
  w_ALSERIAL = space(10)
  w_ALDATOBS = ctod("  /  /  ")
  w_ALDATREG = ctod("  /  /  ")
  w_ALTIPALL = space(1)
  w_ALPATFIL = space(254)
  w_ALOGGALL = space(50)
  w_AL__NOTE = space(0)
  w_ALTIPORI = space(1)
  w_ALRIFNOM = space(20)
  w_ALRIFOFF = space(10)
  w_ALRIFMOD = space(5)
  w_ALDATVAL = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_AACODCAT = space(10)
  w_GSOF_MAS = .NULL.
  w_ALFLATTM = space(1)
  w_OLDVALUE = space(1)
  w_PADRE = .NULL.
  w_CTRL = .NULL.
  w_MESISCA = 0
  w_GIOR = 0
  w_MESE = 0
  w_ANNO = 0
  w_DATRIF = ctod("  /  /  ")
  w_OK = .f.
  w_CGIOR = space(2)
  w_CMESE = space(2)
  w_CANNO = space(4)
  w_APPO = 0
  * --- WorkFile variables
  ALL_EGAT_idx=0
  ALL_ATTR_idx=0
  MOD_ATTR_idx=0
  CAT_ATTR_idx=0
  TIP_CATT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge il Modello Offerte e carica le sezioni Offerta e gli allegati (da GSOF_AOF)
    * --- Solo Caricamento
    this.w_PADRE = This.oParentObject
    if this.w_PADRE.cFunction="Load" AND NOT EMPTY(this.oParentObject.w_OFCODMOD) AND Lower(this.w_PADRE.class)="tgsof_aof" AND Lower(this.w_PADRE.currentevent)<>"new record"
      * --- Forza il Refresh dei Dettagli
      this.w_GSOF_MAS = this.w_PADRE.GSOF_MAS
      if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
        this.oParentObject.w_OFCODLIS = IIF(EMPTY(this.oParentObject.w_CODCLI) AND NOT EMPTY(this.oParentObject.w_LISMOD), this.oParentObject.w_LISMOD, this.oParentObject.w_OFCODLIS)
        this.oParentObject.w_OFSCOLIS = IIF(EMPTY(this.oParentObject.w_CODCLI) AND NOT EMPTY(this.oParentObject.w_SCOMOD), this.oParentObject.w_SCOMOD, this.oParentObject.w_OFSCOLIS)
        this.oParentObject.w_OFPROLIS = IIF(EMPTY(this.oParentObject.w_CODCLI) AND NOT EMPTY(this.oParentObject.w_PROMOD), this.oParentObject.w_PROMOD, this.oParentObject.w_OFPROLIS)
        this.oParentObject.w_OFPROSCO = IIF(EMPTY(this.oParentObject.w_CODCLI) AND NOT EMPTY(this.oParentObject.w_PRSMOD), this.oParentObject.w_PRSMOD, this.oParentObject.w_OFPROSCO)
        this.oParentObject.w_OFCODVAL = IIF(EMPTY(this.oParentObject.w_CODCLI) AND NOT EMPTY(this.oParentObject.w_MOCODVAL), this.oParentObject.w_MOCODVAL, this.oParentObject.w_OFCODVAL)
      else
        if !EMPTY(this.oParentObject.w_CODCLI)
          this.oParentObject.w_OFCODLIS = IIF(EMPTY(this.oParentObject.w_LISCLI) AND NOT EMPTY(this.oParentObject.w_LISMOD), this.oParentObject.w_LISMOD, this.oParentObject.w_OFCODLIS)
        else
          this.oParentObject.w_OFCODLIS = IIF(EMPTY(this.oParentObject.w_LISNOM) AND NOT EMPTY(this.oParentObject.w_LISMOD), this.oParentObject.w_LISMOD, this.oParentObject.w_OFCODLIS)
        endif
      endif
      if !EMPTY(this.oParentObject.w_CODCLI)
        this.oParentObject.w_OFCODPAG = IIF(EMPTY(this.oParentObject.w_PAGCLI) AND NOT EMPTY(this.oParentObject.w_PAGMOD), this.oParentObject.w_PAGMOD, this.oParentObject.w_OFCODPAG)
      else
        this.oParentObject.w_OFCODPAG = IIF(EMPTY(this.oParentObject.w_NOCODPAG) AND NOT EMPTY(this.oParentObject.w_PAGMOD), this.oParentObject.w_PAGMOD, this.oParentObject.w_OFCODPAG)
      endif
      if Not Empty( this.oParentObject.w_OFCODPAG )
        * --- Valorizzo pagamento..
        this.w_CTRL = this.w_PADRE.GetCtrl("w_OFCODPAG")
        this.w_CTRL.Value = this.oParentObject.w_OFCODPAG
        this.w_CTRL.bUpd = .T.
        this.w_OLDVALUE = g_RICPERCONT
        g_RICPERCONT="N"
        this.w_CTRL.Valid()     
        g_RICPERCONT=this.w_OLDVALUE
      endif
      if EMPTY(this.oParentObject.w_CODCLI)
        * --- Se Nominativo no Cliente , Assegna Listino, Sconti Globali e Pagamento 
        this.oParentObject.w_OFSCOCL1 = this.oParentObject.w_SC1MOD
        this.oParentObject.w_OFSCOCL2 = this.oParentObject.w_SC2MOD
      endif
      this.w_PADRE.mCalc(.T.)     
      * --- Ricalcola le Picture 
      this.oParentObject.w_CALCPICT = DEFPIC(this.oParentObject.w_DECTOT)
      this.oParentObject.w_CALCPICU = DEFPIU(this.oParentObject.w_DECUNI)
      this.w_PADRE.GSOF_MDO.mCalc(.T.)     
      this.w_PADRE.GSOF_MSO.mCalc(.T.)     
      * --- Elimina Sezioni Allegati Eventualmente Inserite
      * --- Legge gli allegati dell' Offerta
      * --- Select from ALL_EGAT
      i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2],.t.,this.ALL_EGAT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ALL_EGAT ";
            +" where ALRIFOFF ="+cp_ToStrODBC(this.oParentObject.w_OFSERIAL)+"";
             ,"_Curs_ALL_EGAT")
      else
        select * from (i_cTable);
         where ALRIFOFF =this.oParentObject.w_OFSERIAL;
          into cursor _Curs_ALL_EGAT
      endif
      if used('_Curs_ALL_EGAT')
        select _Curs_ALL_EGAT
        locate for 1=1
        do while not(eof())
        this.w_ALSERIAL = NVL(_Curs_ALL_EGAT.ALSERIAL, "")
        if NOT EMPTY(this.w_ALSERIAL)
          * --- Try
          local bErr_033BE4F8
          bErr_033BE4F8=bTrsErr
          this.Try_033BE4F8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_Errormsg("Errore cancellazione ALL_ATTR%0%1",,"",MESSAGE() )
          endif
          bTrsErr=bTrsErr or bErr_033BE4F8
          * --- End
        endif
          select _Curs_ALL_EGAT
          continue
        enddo
        use
      endif
      * --- Try
      local bErr_0329C068
      bErr_0329C068=bTrsErr
      this.Try_0329C068()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_Errormsg("Errore cancellazione ALL_EGAT%0%1",,"",MESSAGE() )
      endif
      bTrsErr=bTrsErr or bErr_0329C068
      * --- End
      * --- Aggiorna Sezioni Offerta
      this.w_PADRE.GSOF_MSO.NotifyEvent("AggiornaSezioni")     
      * --- Select from ALL_EGAT
      i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2],.t.,this.ALL_EGAT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ALL_EGAT ";
            +" where ALRIFMOD = "+cp_ToStrODBC(this.oParentObject.w_OFCODMOD)+"";
             ,"_Curs_ALL_EGAT")
      else
        select * from (i_cTable);
         where ALRIFMOD = this.oParentObject.w_OFCODMOD;
          into cursor _Curs_ALL_EGAT
      endif
      if used('_Curs_ALL_EGAT')
        select _Curs_ALL_EGAT
        locate for 1=1
        do while not(eof())
        this.w_SERIAL = NVL(_Curs_ALL_EGAT.ALSERIAL, "")
        this.w_ALDATOBS = CP_TODATE(_Curs_ALL_EGAT.ALDATOBS)
        this.w_ALDATREG = CP_TODATE(_Curs_ALL_EGAT.ALDATREG)
        this.w_ALTIPALL = NVL(_Curs_ALL_EGAT.ALTIPALL, " ")
        this.w_ALPATFIL = NVL(_Curs_ALL_EGAT.ALPATFIL, "")
        this.w_ALOGGALL = NVL(_Curs_ALL_EGAT.ALOGGALL, "")
        this.w_AL__NOTE = NVL(_Curs_ALL_EGAT.AL__NOTE, "")
        this.w_ALTIPORI = "O"
        this.w_ALRIFNOM = SPACE(15)
        this.w_ALRIFOFF = this.oParentObject.w_OFSERIAL
        this.w_ALRIFMOD = SPACE(5)
        this.w_ALDATVAL = CP_TODATE(_Curs_ALL_EGAT.ALDATVAL)
        this.w_ALFLATTM = NVL(_Curs_ALL_EGAT.ALFLATTM, "N")
        * --- Scrive gli allegati del Modello Offerte nell'offerta in corso
        if EMPTY(this.w_ALDATOBS) OR this.w_ALDATOBS>this.oParentObject.w_OFDATDOC
          this.w_ALSERIAL = SPACE(10)
          i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
          cp_AskTableProg(this,i_nConn,"SEALLOF","I_codazi,w_ALSERIAL")
          cp_NextTableProg(this,i_nConn,"SEALLOF","I_codazi,w_ALSERIAL")
          * --- Try
          local bErr_033A9CC0
          bErr_033A9CC0=bTrsErr
          this.Try_033A9CC0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_Errormsg("Errore inserimento ALL_EGAT%0%1",,"",MESSAGE () )
          endif
          bTrsErr=bTrsErr or bErr_033A9CC0
          * --- End
          * --- Legge gli attributi dell'allegato del Modello 
          * --- Select from ALL_ATTR
          i_nConn=i_TableProp[this.ALL_ATTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ALL_ATTR_idx,2],.t.,this.ALL_ATTR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ALL_ATTR ";
                +" where AACODICE = "+cp_ToStrODBC(this.w_SERIAL)+"";
                 ,"_Curs_ALL_ATTR")
          else
            select * from (i_cTable);
             where AACODICE = this.w_SERIAL;
              into cursor _Curs_ALL_ATTR
          endif
          if used('_Curs_ALL_ATTR')
            select _Curs_ALL_ATTR
            locate for 1=1
            do while not(eof())
            this.w_AACODCAT = NVL(_Curs_ALL_ATTR.AACODCAT, "")
            if NOT EMPTY(this.w_AACODCAT)
              * --- Scrive gli attributi degli allegati
              * --- Try
              local bErr_0329E7F8
              bErr_0329E7F8=bTrsErr
              this.Try_0329E7F8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                ah_Errormsg("Errore inserimento ALL_ATTR%0%1",,"",MESSAGE () )
              endif
              bTrsErr=bTrsErr or bErr_0329E7F8
              * --- End
            endif
              select _Curs_ALL_ATTR
              continue
            enddo
            use
          endif
        endif
          select _Curs_ALL_EGAT
          continue
        enddo
        use
      endif
      * --- Select from MOD_ATTR
      i_nConn=i_TableProp[this.MOD_ATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_ATTR_idx,2],.t.,this.MOD_ATTR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MOD_ATTR ";
            +" where MACODICE = "+cp_ToStrODBC(this.oParentObject.w_OFCODMOD)+"";
             ,"_Curs_MOD_ATTR")
      else
        select * from (i_cTable);
         where MACODICE = this.oParentObject.w_OFCODMOD;
          into cursor _Curs_MOD_ATTR
      endif
      if used('_Curs_MOD_ATTR')
        select _Curs_MOD_ATTR
        locate for 1=1
        do while not(eof())
        this.w_GSOF_MAS.AddRow()     
        this.w_GSOF_MAS.w_OACODICE = this.oParentObject.w_OFSERIAL
        this.w_GSOF_MAS.w_OACODATT = _Curs_MOD_ATTR.MACODATT
        * --- Read from CAT_ATTR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAT_ATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAT_ATTR_idx,2],.t.,this.CAT_ATTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CTDESCRI,CTTIPCAT"+;
            " from "+i_cTable+" CAT_ATTR where ";
                +"CTCODICE = "+cp_ToStrODBC(this.w_GSOF_MAS.w_OACODATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CTDESCRI,CTTIPCAT;
            from (i_cTable) where;
                CTCODICE = this.w_GSOF_MAS.w_OACODATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GSOF_MAS.w_DESATT = NVL(cp_ToDate(_read_.CTDESCRI),cp_NullValue(_read_.CTDESCRI))
          this.w_GSOF_MAS.w_TIPCAT = NVL(cp_ToDate(_read_.CTTIPCAT),cp_NullValue(_read_.CTTIPCAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows>0
          * --- Read from TIP_CATT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_CATT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_CATT_idx,2],.t.,this.TIP_CATT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TCDESCRI"+;
              " from "+i_cTable+" TIP_CATT where ";
                  +"TCCODICE = "+cp_ToStrODBC(this.w_GSOF_MAS.w_TIPCAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TCDESCRI;
              from (i_cTable) where;
                  TCCODICE = this.w_GSOF_MAS.w_TIPCAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_GSOF_MAS.w_DESTIP = NVL(cp_ToDate(_read_.TCDESCRI),cp_NullValue(_read_.TCDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_GSOF_MAS.bUpdated = .T.
        this.w_GSOF_MAS.TrsFromWork()     
          select _Curs_MOD_ATTR
          continue
        enddo
        use
      endif
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_033BE4F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ALL_ATTR
    i_nConn=i_TableProp[this.ALL_ATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALL_ATTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"AACODICE = "+cp_ToStrODBC(this.w_ALSERIAL);
             )
    else
      delete from (i_cTable) where;
            AACODICE = this.w_ALSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0329C068()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ALL_EGAT
    i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ALRIFOFF = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
             )
    else
      delete from (i_cTable) where;
            ALRIFOFF = this.oParentObject.w_OFSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_033A9CC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ALL_EGAT
    i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ALL_EGAT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ALSERIAL"+",ALDATREG"+",ALTIPALL"+",ALPATFIL"+",ALOGGALL"+",AL__NOTE"+",ALTIPORI"+",ALRIFNOM"+",ALRIFOFF"+",ALRIFMOD"+",ALDATVAL"+",ALDATOBS"+",ALFLATTM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ALSERIAL),'ALL_EGAT','ALSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALDATREG),'ALL_EGAT','ALDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALTIPALL),'ALL_EGAT','ALTIPALL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALPATFIL),'ALL_EGAT','ALPATFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALOGGALL),'ALL_EGAT','ALOGGALL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AL__NOTE),'ALL_EGAT','AL__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALTIPORI),'ALL_EGAT','ALTIPORI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALRIFNOM),'ALL_EGAT','ALRIFNOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALRIFOFF),'ALL_EGAT','ALRIFOFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALRIFMOD),'ALL_EGAT','ALRIFMOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALDATVAL),'ALL_EGAT','ALDATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALDATOBS),'ALL_EGAT','ALDATOBS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALFLATTM),'ALL_EGAT','ALFLATTM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ALSERIAL',this.w_ALSERIAL,'ALDATREG',this.w_ALDATREG,'ALTIPALL',this.w_ALTIPALL,'ALPATFIL',this.w_ALPATFIL,'ALOGGALL',this.w_ALOGGALL,'AL__NOTE',this.w_AL__NOTE,'ALTIPORI',this.w_ALTIPORI,'ALRIFNOM',this.w_ALRIFNOM,'ALRIFOFF',this.w_ALRIFOFF,'ALRIFMOD',this.w_ALRIFMOD,'ALDATVAL',this.w_ALDATVAL,'ALDATOBS',this.w_ALDATOBS)
      insert into (i_cTable) (ALSERIAL,ALDATREG,ALTIPALL,ALPATFIL,ALOGGALL,AL__NOTE,ALTIPORI,ALRIFNOM,ALRIFOFF,ALRIFMOD,ALDATVAL,ALDATOBS,ALFLATTM &i_ccchkf. );
         values (;
           this.w_ALSERIAL;
           ,this.w_ALDATREG;
           ,this.w_ALTIPALL;
           ,this.w_ALPATFIL;
           ,this.w_ALOGGALL;
           ,this.w_AL__NOTE;
           ,this.w_ALTIPORI;
           ,this.w_ALRIFNOM;
           ,this.w_ALRIFOFF;
           ,this.w_ALRIFMOD;
           ,this.w_ALDATVAL;
           ,this.w_ALDATOBS;
           ,this.w_ALFLATTM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0329E7F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ALL_ATTR
    i_nConn=i_TableProp[this.ALL_ATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALL_ATTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ALL_ATTR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AACODICE"+",AACODCAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ALSERIAL),'ALL_ATTR','AACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AACODCAT),'ALL_ATTR','AACODCAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AACODICE',this.w_ALSERIAL,'AACODCAT',this.w_AACODCAT)
      insert into (i_cTable) (AACODICE,AACODCAT &i_ccchkf. );
         values (;
           this.w_ALSERIAL;
           ,this.w_AACODCAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola data Scadenza
    this.w_DATRIF = this.oParentObject.w_OFDATDOC
    this.w_MESISCA = this.oParentObject.w_OFGIOSCA / 30
    if this.w_MESISCA = INT(this.w_MESISCA)
      * --- Calcola data Scadenza a 30 giorni o multipli (Cambia Giorno se Tipo Pag.="DD")
      this.w_GIOR = DAY(this.w_DATRIF)
      this.w_MESE = MONTH(this.w_DATRIF) + this.w_MESISCA
      this.w_ANNO = YEAR(this.w_DATRIF)
    else
      * --- Calcola data Scadenza x rate diverse da 30 giorni o multipli
      this.w_GIOR = DAY(this.w_DATRIF + this.oParentObject.w_OFGIOSCA)
      this.w_MESE = MONTH(this.w_DATRIF + this.oParentObject.w_OFGIOSCA)
      this.w_ANNO = YEAR(this.w_DATRIF + this.oParentObject.w_OFGIOSCA)
    endif
    * --- Se > 12 scatta all'Anno successivo 
    do while this.w_MESE>12
      this.w_MESE = this.w_MESE - 12
      this.w_ANNO = this.w_ANNO + 1
    enddo
    * --- Calcola giorno Pagamento Fine Mese
    if this.oParentObject.w_OFINISCA = "FM" OR this.oParentObject.w_OFINISCA = "FF" 
      * --- Sposta tutto all primo giorno del mese successivo
      this.w_MESE = this.w_MESE + 1
      * --- Se > 12 scatta all'anno successivo 
      do while this.w_MESE>12
        this.w_MESE = this.w_MESE - 12
        this.w_ANNO = this.w_ANNO + 1
      enddo
      this.w_CMESE = RIGHT("00"+ALLTRIM(STR(this.w_MESE)),2)
      this.w_CANNO = RIGHT("0000"+ALLTRIM(STR(this.w_ANNO)),4)
      * --- Quindi toglie un giorno per tornare alla Fine del mese calcolato
      this.w_APPO = cp_CharToDate("01-" + this.w_CMESE + "-" + this.w_CANNO) - 1
      this.w_GIOR = DAY(this.w_APPO)
      this.w_MESE = MONTH(this.w_APPO)
      this.w_ANNO = YEAR(this.w_APPO)
    endif
    * --- Se Giorno Fisso cambia giorno (ev.te sposta avanti di un mese)
    if this.oParentObject.w_OFINISCA="GF" OR (this.oParentObject.w_OFINISCA="FM" AND this.oParentObject.w_OFGIOFIS<>0)
      this.w_MESE = iif(this.oParentObject.w_OFGIOFIS<this.w_GIOR, this.w_MESE+1, this.w_MESE)
      do while this.w_MESE>12
        this.w_MESE = this.w_MESE - 12
        this.w_ANNO = this.w_ANNO + 1
      enddo
      this.w_GIOR = this.oParentObject.w_OFGIOFIS
    endif
    * --- Verifica se il giorno e' congruente
    this.w_CMESE = RIGHT("00" + ALLTRIM(STR(this.w_MESE)), 2)
    this.w_CANNO = RIGHT("0000" + ALLTRIM(STR(this.w_ANNO)), 4)
    if this.w_CMESE $ "02-04-06-09-11" .and. this.w_GIOR > 28
      * --- Trova l'Ultimo giorno del mese
      this.w_CGIOR = DAY(cp_CharToDate("01-" + RIGHT("00"+ALLTRIM(STR(this.w_MESE+1)),2) + "-" + this.w_CANNO) - 1)
      * --- Se giorno > fine mese calcola a fine mese
      this.w_GIOR = IIF(this.w_GIOR > this.w_CGIOR, this.w_CGIOR, this.w_GIOR)
    endif
    this.w_CGIOR = RIGHT("00" + ALLTRIM(STR(this.w_GIOR)), 2)
    * --- Calcola data Scadenza
    this.oParentObject.w_OFDATSCA = cp_CharToDate(this.w_CGIOR + "-" + this.w_CMESE + "-" + this.w_CANNO)
    this.oParentObject.w_OFDATRIC = this.oParentObject.w_OFDATSCA
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ALL_EGAT'
    this.cWorkTables[2]='ALL_ATTR'
    this.cWorkTables[3]='MOD_ATTR'
    this.cWorkTables[4]='CAT_ATTR'
    this.cWorkTables[5]='TIP_CATT'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_ALL_EGAT')
      use in _Curs_ALL_EGAT
    endif
    if used('_Curs_ALL_EGAT')
      use in _Curs_ALL_EGAT
    endif
    if used('_Curs_ALL_ATTR')
      use in _Curs_ALL_ATTR
    endif
    if used('_Curs_MOD_ATTR')
      use in _Curs_MOD_ATTR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
