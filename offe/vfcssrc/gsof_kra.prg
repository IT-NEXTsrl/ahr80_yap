* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_kra                                                        *
*              Ricerca allegati                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2008-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_kra",oParentObject))

* --- Class definition
define class tgsof_kra as StdForm
  Top    = 0
  Left   = 3

  * --- Standard Properties
  Width  = 784
  Height = 424+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-26"
  HelpContextID=116009321
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=63

  * --- Constant Properties
  _IDX = 0
  CAT_ATTR_IDX = 0
  TIP_CATT_IDX = 0
  OFF_NOMI_IDX = 0
  MOD_OFFE_IDX = 0
  cPrg = "gsof_kra"
  cComment = "Ricerca allegati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_RACODATT = space(10)
  w_TIPCAT = space(5)
  w_DESCAT = space(35)
  w_DESATT = space(35)
  w_NOMFIL = space(30)
  w_OGGETT = space(30)
  w_NOTE = space(30)
  w_ORDALL = space(1)
  w_SERALL = space(10)
  w_PATALL = space(254)
  w_RODADATA = ctod('  /  /  ')
  w_ROADATA = ctod('  /  /  ')
  w_ASSOC = space(1)
  w_FLGALL = space(1)
  w_TIPO = space(1)
  w_OBSODAT1 = space(1)
  w_RACODAT1 = space(10)
  w_DESCAT1 = space(35)
  w_DESATT1 = space(35)
  w_RACODAT2 = space(10)
  w_DESCAT2 = space(35)
  w_DESATT2 = space(35)
  w_RACODAT3 = space(10)
  w_DESCAT3 = space(35)
  w_DESATT3 = space(35)
  w_RACODAT4 = space(10)
  w_DESCAT4 = space(35)
  w_DESATT4 = space(35)
  w_RACODAT5 = space(10)
  w_DESCAT5 = space(35)
  w_DESATT5 = space(35)
  w_RACODAT6 = space(10)
  w_DESCAT6 = space(35)
  w_DESATT6 = space(35)
  w_RACODAT7 = space(10)
  w_DESCAT7 = space(35)
  w_DESATT7 = space(35)
  w_RACODAT8 = space(10)
  w_DESCAT8 = space(35)
  w_DESATT8 = space(35)
  w_RACODAT9 = space(10)
  w_DESCAT9 = space(35)
  w_DESATT9 = space(35)
  w_TIPCAT1 = space(5)
  w_TIPCAT2 = space(5)
  w_TIPCAT3 = space(5)
  w_TIPCAT4 = space(5)
  w_TIPCAT5 = space(5)
  w_TIPCAT6 = space(5)
  w_TIPCAT7 = space(5)
  w_TIPCAT8 = space(5)
  w_TIPCAT9 = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_CHECK = space(10)
  w_OFNUMDOC = 0
  w_OFSERDOC = space(10)
  w_OFDATDOC = ctod('  /  /  ')
  w_OFNUMVER = 0
  w_OFSERIAL = space(10)
  w_NOCODICE = space(20)
  w_NODESCRI = space(40)
  w_MOCODICE = space(5)
  w_MODESCRI = space(35)
  w_ZoomAll = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_kraPag1","gsof_kra",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsof_kraPag2","gsof_kra",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRACODATT_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomAll = this.oPgFrm.Pages(1).oPag.ZoomAll
    DoDefault()
    proc Destroy()
      this.w_ZoomAll = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAT_ATTR'
    this.cWorkTables[2]='TIP_CATT'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='MOD_OFFE'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RACODATT=space(10)
      .w_TIPCAT=space(5)
      .w_DESCAT=space(35)
      .w_DESATT=space(35)
      .w_NOMFIL=space(30)
      .w_OGGETT=space(30)
      .w_NOTE=space(30)
      .w_ORDALL=space(1)
      .w_SERALL=space(10)
      .w_PATALL=space(254)
      .w_RODADATA=ctod("  /  /  ")
      .w_ROADATA=ctod("  /  /  ")
      .w_ASSOC=space(1)
      .w_FLGALL=space(1)
      .w_TIPO=space(1)
      .w_OBSODAT1=space(1)
      .w_RACODAT1=space(10)
      .w_DESCAT1=space(35)
      .w_DESATT1=space(35)
      .w_RACODAT2=space(10)
      .w_DESCAT2=space(35)
      .w_DESATT2=space(35)
      .w_RACODAT3=space(10)
      .w_DESCAT3=space(35)
      .w_DESATT3=space(35)
      .w_RACODAT4=space(10)
      .w_DESCAT4=space(35)
      .w_DESATT4=space(35)
      .w_RACODAT5=space(10)
      .w_DESCAT5=space(35)
      .w_DESATT5=space(35)
      .w_RACODAT6=space(10)
      .w_DESCAT6=space(35)
      .w_DESATT6=space(35)
      .w_RACODAT7=space(10)
      .w_DESCAT7=space(35)
      .w_DESATT7=space(35)
      .w_RACODAT8=space(10)
      .w_DESCAT8=space(35)
      .w_DESATT8=space(35)
      .w_RACODAT9=space(10)
      .w_DESCAT9=space(35)
      .w_DESATT9=space(35)
      .w_TIPCAT1=space(5)
      .w_TIPCAT2=space(5)
      .w_TIPCAT3=space(5)
      .w_TIPCAT4=space(5)
      .w_TIPCAT5=space(5)
      .w_TIPCAT6=space(5)
      .w_TIPCAT7=space(5)
      .w_TIPCAT8=space(5)
      .w_TIPCAT9=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_CHECK=space(10)
      .w_OFNUMDOC=0
      .w_OFSERDOC=space(10)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_OFNUMVER=0
      .w_OFSERIAL=space(10)
      .w_NOCODICE=space(20)
      .w_NODESCRI=space(40)
      .w_MOCODICE=space(5)
      .w_MODESCRI=space(35)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_RACODATT))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_TIPCAT))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,7,.f.)
        .w_ORDALL = 'D'
        .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
        .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
          .DoRTCalc(11,12,.f.)
        .w_ASSOC = 'T'
        .w_FLGALL = 'N'
        .w_TIPO = 'T'
        .w_OBSODAT1 = 'N'
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_RACODAT1))
          .link_2_13('Full')
        endif
        .DoRTCalc(18,20,.f.)
        if not(empty(.w_RACODAT2))
          .link_2_16('Full')
        endif
        .DoRTCalc(21,23,.f.)
        if not(empty(.w_RACODAT3))
          .link_2_19('Full')
        endif
        .DoRTCalc(24,26,.f.)
        if not(empty(.w_RACODAT4))
          .link_2_22('Full')
        endif
        .DoRTCalc(27,29,.f.)
        if not(empty(.w_RACODAT5))
          .link_2_25('Full')
        endif
        .DoRTCalc(30,32,.f.)
        if not(empty(.w_RACODAT6))
          .link_2_28('Full')
        endif
        .DoRTCalc(33,35,.f.)
        if not(empty(.w_RACODAT7))
          .link_2_31('Full')
        endif
        .DoRTCalc(36,38,.f.)
        if not(empty(.w_RACODAT8))
          .link_2_34('Full')
        endif
        .DoRTCalc(39,41,.f.)
        if not(empty(.w_RACODAT9))
          .link_2_37('Full')
        endif
        .DoRTCalc(42,44,.f.)
        if not(empty(.w_TIPCAT1))
          .link_2_40('Full')
        endif
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_TIPCAT2))
          .link_2_41('Full')
        endif
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_TIPCAT3))
          .link_2_42('Full')
        endif
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_TIPCAT4))
          .link_2_43('Full')
        endif
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_TIPCAT5))
          .link_2_44('Full')
        endif
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_TIPCAT6))
          .link_2_45('Full')
        endif
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_TIPCAT7))
          .link_2_46('Full')
        endif
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_TIPCAT8))
          .link_2_47('Full')
        endif
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_TIPCAT9))
          .link_2_48('Full')
        endif
        .w_OBTEST = i_datsys
          .DoRTCalc(54,54,.f.)
        .w_OFNUMDOC = NVL(.w_ZoomAll.getVar('OFNUMDOC'), 0)
        .w_OFSERDOC = NVL(.w_ZoomAll.getVar('OFSERDOC'), space(10))
        .w_OFDATDOC = NVL(.w_ZoomAll.getVar('OFDATDOC'), cp_CharToDate('  /  /  '))
        .w_OFNUMVER = NVL(.w_ZoomAll.getVar('OFNUMVER'), 0)
        .w_OFSERIAL = NVL(.w_ZoomAll.getVar('ALRIFOFF'), space(10))
        .w_NOCODICE = NVL(.w_ZoomAll.getVar('ALRIFNOM'), space(20))
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_NOCODICE))
          .link_1_28('Full')
        endif
          .DoRTCalc(61,61,.f.)
        .w_MOCODICE = NVL(.w_ZoomAll.getVar('ALRIFMOD'), space(5))
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_MOCODICE))
          .link_1_30('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_49.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_50.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_51.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_52.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_53.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_54.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_55.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_56.Calculate()
      .oPgFrm.Page1.oPag.ZoomAll.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
    endwith
    this.DoRTCalc(63,63,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,8,.t.)
            .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
            .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
        .DoRTCalc(11,43,.t.)
          .link_2_40('Full')
          .link_2_41('Full')
          .link_2_42('Full')
          .link_2_43('Full')
          .link_2_44('Full')
          .link_2_45('Full')
          .link_2_46('Full')
          .link_2_47('Full')
          .link_2_48('Full')
            .w_OBTEST = i_datsys
        .DoRTCalc(54,54,.t.)
            .w_OFNUMDOC = NVL(.w_ZoomAll.getVar('OFNUMDOC'), 0)
            .w_OFSERDOC = NVL(.w_ZoomAll.getVar('OFSERDOC'), space(10))
            .w_OFDATDOC = NVL(.w_ZoomAll.getVar('OFDATDOC'), cp_CharToDate('  /  /  '))
            .w_OFNUMVER = NVL(.w_ZoomAll.getVar('OFNUMVER'), 0)
            .w_OFSERIAL = NVL(.w_ZoomAll.getVar('ALRIFOFF'), space(10))
            .w_NOCODICE = NVL(.w_ZoomAll.getVar('ALRIFNOM'), space(20))
          .link_1_28('Full')
        .DoRTCalc(61,61,.t.)
            .w_MOCODICE = NVL(.w_ZoomAll.getVar('ALRIFMOD'), space(5))
          .link_1_30('Full')
        .oPgFrm.Page2.oPag.oObj_2_49.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_50.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_51.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_55.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_56.Calculate()
        .oPgFrm.Page1.oPag.ZoomAll.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(63,63,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_49.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_50.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_51.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_55.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_56.Calculate()
        .oPgFrm.Page1.oPag.ZoomAll.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oRACODAT1_2_13.enabled = this.oPgFrm.Page2.oPag.oRACODAT1_2_13.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT2_2_16.enabled = this.oPgFrm.Page2.oPag.oRACODAT2_2_16.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT3_2_19.enabled = this.oPgFrm.Page2.oPag.oRACODAT3_2_19.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT4_2_22.enabled = this.oPgFrm.Page2.oPag.oRACODAT4_2_22.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT5_2_25.enabled = this.oPgFrm.Page2.oPag.oRACODAT5_2_25.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT6_2_28.enabled = this.oPgFrm.Page2.oPag.oRACODAT6_2_28.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT7_2_31.enabled = this.oPgFrm.Page2.oPag.oRACODAT7_2_31.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT8_2_34.enabled = this.oPgFrm.Page2.oPag.oRACODAT8_2_34.mCond()
    this.oPgFrm.Page2.oPag.oRACODAT9_2_37.enabled = this.oPgFrm.Page2.oPag.oRACODAT9_2_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_18.visible=!this.oPgFrm.Page1.oPag.oBtn_1_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_19.visible=!this.oPgFrm.Page1.oPag.oBtn_1_19.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_49.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_50.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_51.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_52.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_53.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_54.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_55.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_56.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomAll.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RACODATT
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODATT))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODATT)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODATT) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODATT_1_1'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODATT)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODATT = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODATT = space(10)
      endif
      this.w_DESATT = space(35)
      this.w_TIPCAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT1
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT1))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT1)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT1) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT1_2_13'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT1)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT1 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT1 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT1 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT1 = space(10)
      endif
      this.w_DESATT1 = space(35)
      this.w_TIPCAT1 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT2
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT2))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT2)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT2) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT2_2_16'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT2)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT2 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT2 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT2 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT2 = space(10)
      endif
      this.w_DESATT2 = space(35)
      this.w_TIPCAT2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT3
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT3))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT3)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT3) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT3_2_19'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT3)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT3 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT3 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT3 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT3 = space(10)
      endif
      this.w_DESATT3 = space(35)
      this.w_TIPCAT3 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT4
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT4)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT4))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT4)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT4) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT4_2_22'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT4)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT4 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT4 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT4 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT4 = space(10)
      endif
      this.w_DESATT4 = space(35)
      this.w_TIPCAT4 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT5
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT5)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT5))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT5)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT5) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT5_2_25'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT5)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT5 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT5 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT5 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT5 = space(10)
      endif
      this.w_DESATT5 = space(35)
      this.w_TIPCAT5 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT6
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT6)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT6))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT6)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT6) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT6_2_28'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT6)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT6 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT6 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT6 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT6 = space(10)
      endif
      this.w_DESATT6 = space(35)
      this.w_TIPCAT6 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT7
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT7)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT7))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT7)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT7) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT7_2_31'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT7)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT7 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT7 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT7 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT7 = space(10)
      endif
      this.w_DESATT7 = space(35)
      this.w_TIPCAT7 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT8
  func Link_2_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT8)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT8))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT8)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT8) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT8_2_34'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT8)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT8 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT8 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT8 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT8 = space(10)
      endif
      this.w_DESATT8 = space(35)
      this.w_TIPCAT8 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT9
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT9)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT9))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT9)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT9) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT9_2_37'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT9)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT9 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT9 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT9 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT9 = space(10)
      endif
      this.w_DESATT9 = space(35)
      this.w_TIPCAT9 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT1
  func Link_2_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT1)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT1 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT1 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT1 = space(5)
      endif
      this.w_DESCAT1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT2
  func Link_2_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT2)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT2 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT2 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT2 = space(5)
      endif
      this.w_DESCAT2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT3
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT3)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT3 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT3 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT3 = space(5)
      endif
      this.w_DESCAT3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT4
  func Link_2_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT4)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT4 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT4 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT4 = space(5)
      endif
      this.w_DESCAT4 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT5
  func Link_2_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT5)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT5 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT5 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT5 = space(5)
      endif
      this.w_DESCAT5 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT6
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT6)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT6 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT6 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT6 = space(5)
      endif
      this.w_DESCAT6 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT7
  func Link_2_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT7)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT7 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT7 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT7 = space(5)
      endif
      this.w_DESCAT7 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT8
  func Link_2_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT8)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT8 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT8 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT8 = space(5)
      endif
      this.w_DESCAT8 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT9
  func Link_2_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT9)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT9 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT9 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT9 = space(5)
      endif
      this.w_DESCAT9 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCODICE
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_NOCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_NOCODICE)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODICE = NVL(_Link_.NOCODICE,space(20))
      this.w_NODESCRI = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODICE = space(20)
      endif
      this.w_NODESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCODICE
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_lTable = "MOD_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2], .t., this.MOD_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_MOCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_MOCODICE)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODICE = NVL(_Link_.MOCODICE,space(5))
      this.w_MODESCRI = NVL(_Link_.MODESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODICE = space(5)
      endif
      this.w_MODESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRACODATT_1_1.value==this.w_RACODATT)
      this.oPgFrm.Page1.oPag.oRACODATT_1_1.value=this.w_RACODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_3.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_3.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_4.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_4.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMFIL_1_5.value==this.w_NOMFIL)
      this.oPgFrm.Page1.oPag.oNOMFIL_1_5.value=this.w_NOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oOGGETT_1_6.value==this.w_OGGETT)
      this.oPgFrm.Page1.oPag.oOGGETT_1_6.value=this.w_OGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_7.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_7.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oORDALL_1_10.RadioValue()==this.w_ORDALL)
      this.oPgFrm.Page1.oPag.oORDALL_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRODADATA_2_2.value==this.w_RODADATA)
      this.oPgFrm.Page2.oPag.oRODADATA_2_2.value=this.w_RODADATA
    endif
    if not(this.oPgFrm.Page2.oPag.oROADATA_2_3.value==this.w_ROADATA)
      this.oPgFrm.Page2.oPag.oROADATA_2_3.value=this.w_ROADATA
    endif
    if not(this.oPgFrm.Page2.oPag.oASSOC_2_6.RadioValue()==this.w_ASSOC)
      this.oPgFrm.Page2.oPag.oASSOC_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLGALL_2_7.RadioValue()==this.w_FLGALL)
      this.oPgFrm.Page2.oPag.oFLGALL_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPO_2_9.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page2.oPag.oTIPO_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOBSODAT1_2_11.RadioValue()==this.w_OBSODAT1)
      this.oPgFrm.Page2.oPag.oOBSODAT1_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT1_2_13.value==this.w_RACODAT1)
      this.oPgFrm.Page2.oPag.oRACODAT1_2_13.value=this.w_RACODAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT1_2_14.value==this.w_DESCAT1)
      this.oPgFrm.Page2.oPag.oDESCAT1_2_14.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT1_2_15.value==this.w_DESATT1)
      this.oPgFrm.Page2.oPag.oDESATT1_2_15.value=this.w_DESATT1
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT2_2_16.value==this.w_RACODAT2)
      this.oPgFrm.Page2.oPag.oRACODAT2_2_16.value=this.w_RACODAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT2_2_17.value==this.w_DESCAT2)
      this.oPgFrm.Page2.oPag.oDESCAT2_2_17.value=this.w_DESCAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT2_2_18.value==this.w_DESATT2)
      this.oPgFrm.Page2.oPag.oDESATT2_2_18.value=this.w_DESATT2
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT3_2_19.value==this.w_RACODAT3)
      this.oPgFrm.Page2.oPag.oRACODAT3_2_19.value=this.w_RACODAT3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT3_2_20.value==this.w_DESCAT3)
      this.oPgFrm.Page2.oPag.oDESCAT3_2_20.value=this.w_DESCAT3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT3_2_21.value==this.w_DESATT3)
      this.oPgFrm.Page2.oPag.oDESATT3_2_21.value=this.w_DESATT3
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT4_2_22.value==this.w_RACODAT4)
      this.oPgFrm.Page2.oPag.oRACODAT4_2_22.value=this.w_RACODAT4
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT4_2_23.value==this.w_DESCAT4)
      this.oPgFrm.Page2.oPag.oDESCAT4_2_23.value=this.w_DESCAT4
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT4_2_24.value==this.w_DESATT4)
      this.oPgFrm.Page2.oPag.oDESATT4_2_24.value=this.w_DESATT4
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT5_2_25.value==this.w_RACODAT5)
      this.oPgFrm.Page2.oPag.oRACODAT5_2_25.value=this.w_RACODAT5
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT5_2_26.value==this.w_DESCAT5)
      this.oPgFrm.Page2.oPag.oDESCAT5_2_26.value=this.w_DESCAT5
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT5_2_27.value==this.w_DESATT5)
      this.oPgFrm.Page2.oPag.oDESATT5_2_27.value=this.w_DESATT5
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT6_2_28.value==this.w_RACODAT6)
      this.oPgFrm.Page2.oPag.oRACODAT6_2_28.value=this.w_RACODAT6
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT6_2_29.value==this.w_DESCAT6)
      this.oPgFrm.Page2.oPag.oDESCAT6_2_29.value=this.w_DESCAT6
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT6_2_30.value==this.w_DESATT6)
      this.oPgFrm.Page2.oPag.oDESATT6_2_30.value=this.w_DESATT6
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT7_2_31.value==this.w_RACODAT7)
      this.oPgFrm.Page2.oPag.oRACODAT7_2_31.value=this.w_RACODAT7
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT7_2_32.value==this.w_DESCAT7)
      this.oPgFrm.Page2.oPag.oDESCAT7_2_32.value=this.w_DESCAT7
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT7_2_33.value==this.w_DESATT7)
      this.oPgFrm.Page2.oPag.oDESATT7_2_33.value=this.w_DESATT7
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT8_2_34.value==this.w_RACODAT8)
      this.oPgFrm.Page2.oPag.oRACODAT8_2_34.value=this.w_RACODAT8
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT8_2_35.value==this.w_DESCAT8)
      this.oPgFrm.Page2.oPag.oDESCAT8_2_35.value=this.w_DESCAT8
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT8_2_36.value==this.w_DESATT8)
      this.oPgFrm.Page2.oPag.oDESATT8_2_36.value=this.w_DESATT8
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT9_2_37.value==this.w_RACODAT9)
      this.oPgFrm.Page2.oPag.oRACODAT9_2_37.value=this.w_RACODAT9
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT9_2_38.value==this.w_DESCAT9)
      this.oPgFrm.Page2.oPag.oDESCAT9_2_38.value=this.w_DESCAT9
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT9_2_39.value==this.w_DESATT9)
      this.oPgFrm.Page2.oPag.oDESATT9_2_39.value=this.w_DESATT9
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_ROADATA) OR  .w_RODADATA<= .w_ROADATA)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRODADATA_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quello finale")
          case   not(.w_RODADATA <=.w_ROADATA or empty(.w_ROADATA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oROADATA_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_kra
      * --- Disabilita tasto F10
         i_bRes=.f.
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsof_kraPag1 as StdContainer
  Width  = 780
  height = 425
  stdWidth  = 780
  stdheight = 425
  resizeXpos=479
  resizeYpos=253
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRACODATT_1_1 as StdField with uid="FXJGYGRWAP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RACODATT", cQueryName = "RACODATT",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976426,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=99, Top=13, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODATT"

  func oRACODATT_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODATT_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODATT_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODATT_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODATT_1_1.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODATT
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_3 as StdField with uid="WUEOCBDKUE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 244428746,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=200, Top=13, InputMask=replicate('X',35)

  add object oDESATT_1_4 as StdField with uid="HMQDUTQTVT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=390, Top=13, InputMask=replicate('X',35)

  add object oNOMFIL_1_5 as StdField with uid="IBOWGOOEQN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NOMFIL", cQueryName = "NOMFIL",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca nome o link del file",;
    HelpContextID = 101647658,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=99, Top=42, InputMask=replicate('X',30)

  add object oOGGETT_1_6 as StdField with uid="LBAOVJWUYE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_OGGETT", cQueryName = "OGGETT",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca oggetto allegato",;
    HelpContextID = 224423194,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=99, Top=71, InputMask=replicate('X',30)

  add object oNOTE_1_7 as StdField with uid="JGVLDCSZPO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca note allegato",;
    HelpContextID = 111121706,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=423, Top=71, InputMask=replicate('X',30)


  add object oBtn_1_8 as StdButton with uid="PVMMCBLPZC",left=724, top=48, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 60912918;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        do gsof_bva with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="JQKFUYTCMG",left=9, top=380, width=48,height=45,;
    CpPicture="bmp\VISUALI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il file dell'allegato selezionato";
    , HelpContextID = 108631290;
    , Caption='\<Apri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSOF_BGA(this.Parent.oContained,"APRI_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc


  add object oORDALL_1_10 as StdCombo with uid="ABZPFJBGDG",rtseq=8,rtrep=.f.,left=601,top=389,width=171,height=21;
    , ToolTipText = "Criteri d'ordinamento degli allegati visualizzati: oggetto, nome file, data registrazione o tipo";
    , HelpContextID = 98865690;
    , cFormVar="w_ORDALL",RowSource=""+"Oggetto,"+"Nome file,"+"Data registrazione,"+"Tipo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORDALL_1_10.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'N',;
    iif(this.value =3,'D',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oORDALL_1_10.GetRadio()
    this.Parent.oContained.w_ORDALL = this.RadioValue()
    return .t.
  endfunc

  func oORDALL_1_10.SetRadio()
    this.Parent.oContained.w_ORDALL=trim(this.Parent.oContained.w_ORDALL)
    this.value = ;
      iif(this.Parent.oContained.w_ORDALL=='O',1,;
      iif(this.Parent.oContained.w_ORDALL=='N',2,;
      iif(this.Parent.oContained.w_ORDALL=='D',3,;
      iif(this.Parent.oContained.w_ORDALL=='T',4,;
      0))))
  endfunc


  add object oBtn_1_17 as StdButton with uid="RPJIAVJGSY",left=60, top=380, width=48,height=45,;
    CpPicture="bmp\Modifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per modificare l'allegato selezionato";
    , HelpContextID = 149261095;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSOF_BGA(this.Parent.oContained,"VARIA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc

  func oBtn_1_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLGALL = 'S')
     endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="ODWRQODXDP",left=111, top=380, width=48,height=45,;
    CpPicture="bmp\Visualiz.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere all'origine dell'allegato";
    , HelpContextID = 236183014;
    , Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSOF_BGA(this.Parent.oContained,"ORIGINE_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc

  func oBtn_1_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLGALL = 'S')
     endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="XAJVXOZOMX",left=162, top=380, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per cancellare l'allegato selezionato";
    , HelpContextID = 236574534;
    , Caption='\<Cancella';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSOF_BGA(this.Parent.oContained,"ELIMINA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc

  func oBtn_1_19.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLGALL = 'S')
     endwith
    endif
  endfunc


  add object ZoomAll as cp_zoombox with uid="TIHUYNVHMX",left=7, top=115, width=770,height=262,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ALL_EGAT",cZoomFile="GSOF_KRA",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 61988326


  add object oObj_1_33 as cp_runprogram with uid="HZPGDYVMSL",left=2, top=436, width=248,height=19,;
    caption='GSOF_BR2(1)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('1')",;
    cEvent = "w_RACODATT Changed",;
    nPag=1;
    , HelpContextID = 246160872

  add object oStr_1_11 as StdString with uid="APHYIAFQKO",Visible=.t., Left=10, Top=17,;
    Alignment=1, Width=84, Height=18,;
    Caption="Attributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="VTGVPSLHTP",Visible=.t., Left=420, Top=391,;
    Alignment=1, Width=174, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="HPRURGYBNO",Visible=.t., Left=10, Top=46,;
    Alignment=1, Width=84, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="HRCTTQNPVO",Visible=.t., Left=10, Top=75,;
    Alignment=1, Width=84, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NNNBMINBCT",Visible=.t., Left=345, Top=75,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsof_kraPag2 as StdContainer
  Width  = 780
  height = 425
  stdWidth  = 780
  stdheight = 425
  resizeXpos=445
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRODADATA_2_2 as StdField with uid="MGYUCPGMQK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_RODADATA", cQueryName = "RODADATA",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quello finale",;
    ToolTipText = "Da data di registrazione allegato",;
    HelpContextID = 245066583,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=106, Top=21

  func oRODADATA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_ROADATA) OR  .w_RODADATA<= .w_ROADATA)
    endwith
    return bRes
  endfunc

  add object oROADATA_2_3 as StdField with uid="JRTLSAMPBU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ROADATA", cQueryName = "ROADATA",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quello finale",;
    ToolTipText = "A data di registrazione allegato",;
    HelpContextID = 244434154,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=276, Top=21

  func oROADATA_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RODADATA <=.w_ROADATA or empty(.w_ROADATA))
    endwith
    return bRes
  endfunc


  add object oASSOC_2_6 as StdCombo with uid="RDVBWRLGQX",rtseq=13,rtrep=.f.,left=106,top=52,width=108,height=21;
    , ToolTipText = "Selezione sull'origine da analizzare: archivio nominativi, offerte, modelli offerta o tutti";
    , HelpContextID = 40215034;
    , cFormVar="w_ASSOC",RowSource=""+"Tutti,"+"Nominativi,"+"Offerte,"+"Modelli offerta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oASSOC_2_6.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'O',;
    iif(this.value =4,'M',;
    space(1))))))
  endfunc
  func oASSOC_2_6.GetRadio()
    this.Parent.oContained.w_ASSOC = this.RadioValue()
    return .t.
  endfunc

  func oASSOC_2_6.SetRadio()
    this.Parent.oContained.w_ASSOC=trim(this.Parent.oContained.w_ASSOC)
    this.value = ;
      iif(this.Parent.oContained.w_ASSOC=='T',1,;
      iif(this.Parent.oContained.w_ASSOC=='N',2,;
      iif(this.Parent.oContained.w_ASSOC=='O',3,;
      iif(this.Parent.oContained.w_ASSOC=='M',4,;
      0))))
  endfunc

  add object oFLGALL_2_7 as StdCheck with uid="DOHJSNYXCL",rtseq=14,rtrep=.f.,left=276, top=49, caption="Allegati non ripetuti",;
    ToolTipText = "Visualizza i files ripetuti una sola volta",;
    HelpContextID = 98855082,;
    cFormVar="w_FLGALL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLGALL_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLGALL_2_7.GetRadio()
    this.Parent.oContained.w_FLGALL = this.RadioValue()
    return .t.
  endfunc

  func oFLGALL_2_7.SetRadio()
    this.Parent.oContained.w_FLGALL=trim(this.Parent.oContained.w_FLGALL)
    this.value = ;
      iif(this.Parent.oContained.w_FLGALL=='S',1,;
      0)
  endfunc


  add object oTIPO_2_9 as StdCombo with uid="OBHDQXLVFJ",rtseq=15,rtrep=.f.,left=106,top=81,width=108,height=21;
    , ToolTipText = "Selezione sulla tipologia di allegato: collegamento o copia file";
    , HelpContextID = 110484170;
    , cFormVar="w_TIPO",RowSource=""+"Tutti,"+"Collegamento,"+"Copia file", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPO_2_9.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oTIPO_2_9.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_2_9.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='T',1,;
      iif(this.Parent.oContained.w_TIPO=='C',2,;
      iif(this.Parent.oContained.w_TIPO=='F',3,;
      0)))
  endfunc

  add object oOBSODAT1_2_11 as StdCheck with uid="VHUTRZCWJY",rtseq=16,rtrep=.f.,left=276, top=78, caption="Solo obsoleti",;
    ToolTipText = "Flag solo obsoleti",;
    HelpContextID = 246042135,;
    cFormVar="w_OBSODAT1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oOBSODAT1_2_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oOBSODAT1_2_11.GetRadio()
    this.Parent.oContained.w_OBSODAT1 = this.RadioValue()
    return .t.
  endfunc

  func oOBSODAT1_2_11.SetRadio()
    this.Parent.oContained.w_OBSODAT1=trim(this.Parent.oContained.w_OBSODAT1)
    this.value = ;
      iif(this.Parent.oContained.w_OBSODAT1=='S',1,;
      0)
  endfunc

  add object oRACODAT1_2_13 as StdField with uid="QBQJLRJLTX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_RACODAT1", cQueryName = "RACODAT1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976391,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=62, Top=157, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT1"

  func oRACODAT1_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODATT)))
    endwith
   endif
  endfunc

  func oRACODAT1_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT1_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT1_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT1_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT1_2_13.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT1
     i_obj.ecpSave()
  endproc

  add object oDESCAT1_2_14 as StdField with uid="RQHPFELTYB",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 244428746,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=166, Top=157, InputMask=replicate('X',35)

  add object oDESATT1_2_15 as StdField with uid="BNZQCLINZM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESATT1", cQueryName = "DESATT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 224636874,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=358, Top=157, InputMask=replicate('X',35)

  add object oRACODAT2_2_16 as StdField with uid="ZGTNSJFFHG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_RACODAT2", cQueryName = "RACODAT2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976392,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=62, Top=182, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT2"

  func oRACODAT2_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT1)))
    endwith
   endif
  endfunc

  func oRACODAT2_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT2_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT2_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT2_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT2_2_16.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT2
     i_obj.ecpSave()
  endproc

  add object oDESCAT2_2_17 as StdField with uid="WJCRSTIJKM",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCAT2", cQueryName = "DESCAT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=166, Top=182, InputMask=replicate('X',35)

  add object oDESATT2_2_18 as StdField with uid="ZBFJSTINNA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESATT2", cQueryName = "DESATT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=358, Top=182, InputMask=replicate('X',35)

  add object oRACODAT3_2_19 as StdField with uid="YAADXVOHNX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_RACODAT3", cQueryName = "RACODAT3",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976393,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=62, Top=207, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT3"

  func oRACODAT3_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT2)))
    endwith
   endif
  endfunc

  func oRACODAT3_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT3_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT3_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT3_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT3_2_19.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT3
     i_obj.ecpSave()
  endproc

  add object oDESCAT3_2_20 as StdField with uid="NKTPRZLJCA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCAT3", cQueryName = "DESCAT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=166, Top=207, InputMask=replicate('X',35)

  add object oDESATT3_2_21 as StdField with uid="TBHEYGIQXL",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESATT3", cQueryName = "DESATT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=358, Top=207, InputMask=replicate('X',35)

  add object oRACODAT4_2_22 as StdField with uid="UKTASPMEPX",rtseq=26,rtrep=.f.,;
    cFormVar = "w_RACODAT4", cQueryName = "RACODAT4",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976394,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=62, Top=232, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT4"

  func oRACODAT4_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT3)))
    endwith
   endif
  endfunc

  func oRACODAT4_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT4_2_22.ecpDrop(oSource)
    this.Parent.oContained.link_2_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT4_2_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT4_2_22'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT4_2_22.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT4
     i_obj.ecpSave()
  endproc

  add object oDESCAT4_2_23 as StdField with uid="KQQPWWSABB",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCAT4", cQueryName = "DESCAT4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=166, Top=232, InputMask=replicate('X',35)

  add object oDESATT4_2_24 as StdField with uid="KWTSJDMBDL",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESATT4", cQueryName = "DESATT4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=358, Top=232, InputMask=replicate('X',35)

  add object oRACODAT5_2_25 as StdField with uid="BUPMCOLWZP",rtseq=29,rtrep=.f.,;
    cFormVar = "w_RACODAT5", cQueryName = "RACODAT5",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976395,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=62, Top=257, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT5"

  func oRACODAT5_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT4)))
    endwith
   endif
  endfunc

  func oRACODAT5_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT5_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT5_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT5_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT5_2_25.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT5
     i_obj.ecpSave()
  endproc

  add object oDESCAT5_2_26 as StdField with uid="LDOMVUPTHB",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCAT5", cQueryName = "DESCAT5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=166, Top=257, InputMask=replicate('X',35)

  add object oDESATT5_2_27 as StdField with uid="ELRCCPHXUB",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESATT5", cQueryName = "DESATT5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=358, Top=257, InputMask=replicate('X',35)

  add object oRACODAT6_2_28 as StdField with uid="UGZCRFAVOZ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_RACODAT6", cQueryName = "RACODAT6",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976396,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=62, Top=282, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT6"

  func oRACODAT6_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT5)))
    endwith
   endif
  endfunc

  func oRACODAT6_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT6_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT6_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT6_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT6_2_28.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT6
     i_obj.ecpSave()
  endproc

  add object oDESCAT6_2_29 as StdField with uid="IBXPDXZSZR",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCAT6", cQueryName = "DESCAT6",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=166, Top=282, InputMask=replicate('X',35)

  add object oDESATT6_2_30 as StdField with uid="IIBGTGBLKA",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESATT6", cQueryName = "DESATT6",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=358, Top=282, InputMask=replicate('X',35)

  add object oRACODAT7_2_31 as StdField with uid="NBCPGPORSO",rtseq=35,rtrep=.f.,;
    cFormVar = "w_RACODAT7", cQueryName = "RACODAT7",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976397,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=62, Top=307, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT7"

  func oRACODAT7_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT6)))
    endwith
   endif
  endfunc

  func oRACODAT7_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT7_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT7_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT7_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT7_2_31.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT7
     i_obj.ecpSave()
  endproc

  add object oDESCAT7_2_32 as StdField with uid="OKAELXSYCJ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESCAT7", cQueryName = "DESCAT7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=166, Top=307, InputMask=replicate('X',35)

  add object oDESATT7_2_33 as StdField with uid="PQZJXXURBV",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESATT7", cQueryName = "DESATT7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=358, Top=307, InputMask=replicate('X',35)

  add object oRACODAT8_2_34 as StdField with uid="KTMOQZCVRK",rtseq=38,rtrep=.f.,;
    cFormVar = "w_RACODAT8", cQueryName = "RACODAT8",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976398,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=62, Top=332, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT8"

  func oRACODAT8_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT7)))
    endwith
   endif
  endfunc

  func oRACODAT8_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT8_2_34.ecpDrop(oSource)
    this.Parent.oContained.link_2_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT8_2_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT8_2_34'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT8_2_34.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT8
     i_obj.ecpSave()
  endproc

  add object oDESCAT8_2_35 as StdField with uid="VPSVPPKLZC",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCAT8", cQueryName = "DESCAT8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=166, Top=332, InputMask=replicate('X',35)

  add object oDESATT8_2_36 as StdField with uid="GFJMETBLWA",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESATT8", cQueryName = "DESATT8",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=358, Top=332, InputMask=replicate('X',35)

  add object oRACODAT9_2_37 as StdField with uid="RRLXYHGTCR",rtseq=41,rtrep=.f.,;
    cFormVar = "w_RACODAT9", cQueryName = "RACODAT9",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo associato agli allegati",;
    HelpContextID = 245976399,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=62, Top=357, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT9"

  func oRACODAT9_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not (Empty(.w_RACODAT8)))
    endwith
   endif
  endfunc

  func oRACODAT9_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT9_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT9_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT9_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT9_2_37.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT9
     i_obj.ecpSave()
  endproc

  add object oDESCAT9_2_38 as StdField with uid="IBDWPEMNIP",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESCAT9", cQueryName = "DESCAT9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24006710,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=166, Top=357, InputMask=replicate('X',35)

  add object oDESATT9_2_39 as StdField with uid="ZPDGAGPBDR",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESATT9", cQueryName = "DESATT9",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43798582,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=358, Top=357, InputMask=replicate('X',35)


  add object oObj_2_49 as cp_runprogram with uid="QZVAIZWTMN",left=3, top=438, width=255,height=19,;
    caption='GSOF_BR2(1)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('1')",;
    cEvent = "w_RACODAT1 Changed",;
    nPag=2;
    , HelpContextID = 246160872


  add object oObj_2_50 as cp_runprogram with uid="KJXNRPYDTF",left=3, top=461, width=255,height=19,;
    caption='GSOF_BR2(2)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('2')",;
    cEvent = "w_RACODAT2 Changed",;
    nPag=2;
    , HelpContextID = 246160616


  add object oObj_2_51 as cp_runprogram with uid="SZAKSNTISM",left=3, top=484, width=255,height=19,;
    caption='GSOF_BR2(3)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('3')",;
    cEvent = "w_RACODAT3 Changed",;
    nPag=2;
    , HelpContextID = 246160360


  add object oObj_2_52 as cp_runprogram with uid="TFGWNFTNEG",left=265, top=438, width=255,height=19,;
    caption='GSOF_BR2(4)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('4')",;
    cEvent = "w_RACODAT4 Changed",;
    nPag=2;
    , HelpContextID = 246160104


  add object oObj_2_53 as cp_runprogram with uid="QUUSTHOGIA",left=265, top=461, width=255,height=19,;
    caption='GSOF_BR2(5)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('5')",;
    cEvent = "w_RACODAT5 Changed",;
    nPag=2;
    , HelpContextID = 246159848


  add object oObj_2_54 as cp_runprogram with uid="UTLRHCTLQO",left=265, top=484, width=255,height=19,;
    caption='GSOF_BR2(6)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('6')",;
    cEvent = "w_RACODAT6 Changed",;
    nPag=2;
    , HelpContextID = 246159592


  add object oObj_2_55 as cp_runprogram with uid="BWSHPVKQRX",left=525, top=438, width=255,height=19,;
    caption='GSOF_BR2(7)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('7')",;
    cEvent = "w_RACODAT7 Changed",;
    nPag=2;
    , HelpContextID = 246159336


  add object oObj_2_56 as cp_runprogram with uid="LRLLYTYNTI",left=525, top=461, width=255,height=19,;
    caption='GSOF_BR2(9)',;
   bGlobalFont=.t.,;
    prg="GSOF_BR2('8')",;
    cEvent = "w_RACODAT8 Changed",;
    nPag=2;
    , HelpContextID = 246158824

  add object oStr_2_4 as StdString with uid="AXVCKHHLKY",Visible=.t., Left=5, Top=25,;
    Alignment=1, Width=96, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="IHIBKVKHGG",Visible=.t., Left=199, Top=25,;
    Alignment=1, Width=73, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="JSIRZVRYQK",Visible=.t., Left=5, Top=54,;
    Alignment=1, Width=96, Height=18,;
    Caption="Associazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="XINUWMHVEE",Visible=.t., Left=5, Top=83,;
    Alignment=1, Width=96, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="XQIYOIBOJG",Visible=.t., Left=48, Top=116,;
    Alignment=0, Width=154, Height=18,;
    Caption="Altri attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_1 as StdBox with uid="CHTRPIZUJH",left=48, top=134, width=689,height=260
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_kra','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
