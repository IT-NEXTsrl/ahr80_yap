* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_kdo                                                        *
*              Generazione documento da offerta                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-22                                                      *
* Last revis.: 2013-07-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_kdo",oParentObject))

* --- Class definition
define class tgsof_kdo as StdForm
  Top    = 53
  Left   = 11

  * --- Standard Properties
  Width  = 501
  Height = 250
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-19"
  HelpContextID=185980567
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=60

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  cPrg = "gsof_kdo"
  cComment = "Generazione documento da offerta"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MVTIPCON = space(1)
  w_CODAZI = space(5)
  w_MVTIPDOR = space(5)
  o_MVTIPDOR = space(5)
  w_MVTIPDOE = space(5)
  o_MVTIPDOE = space(5)
  w_RICNOM = space(1)
  w_FLMGPR = space(1)
  w_MAGDOC = space(5)
  w_OMAG = space(5)
  w_MVFLVEAC = space(1)
  w_MVCLADOC = space(2)
  w_MVFLINTE = space(1)
  w_MVTCAMAG = space(5)
  o_MVTCAMAG = space(5)
  w_FLCASC = space(1)
  w_FLRISE = space(1)
  w_FLIMPE = space(1)
  w_FLORDI = space(1)
  w_MVCODESE = space(4)
  w_DESRIF = space(35)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(2)
  o_MVALFDOC = space(2)
  w_MVDATDOC = ctod('  /  /  ')
  o_MVDATDOC = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_MVCODMAG = space(5)
  w_DESMAG = space(30)
  w_MVFLPROV = space(1)
  o_MVFLPROV = space(1)
  w_OKDATDIV = .F.
  w_MVDATDIV = ctod('  /  /  ')
  w_MVDATEVA = ctod('  /  /  ')
  w_MVDATTRA = ctod('  /  /  ')
  w_MVORATRA = space(2)
  w_MVMINTRA = space(2)
  w_MVPRD = space(2)
  w_MVANNDOC = space(4)
  w_MVDATCIV = ctod('  /  /  ')
  w_MVFLACCO = space(1)
  w_MVTFRAGG = space(1)
  w_MVCAUCON = space(5)
  w_MVCODUTE = 0
  w_MVNUMREG = 0
  w_MVDATREG = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MVALFEST = space(2)
  w_MVPRP = space(2)
  w_MVNUMEST = 0
  w_FLPPRO = space(1)
  w_MVANNPRO = space(4)
  w_NUMSCO = 0
  w_OK = .F.
  w_FLANAL = space(1)
  w_MVASPEST = space(30)
  w_FLCOMM = space(1)
  w_MV_SEGNO = space(1)
  w_FLDTRC = space(1)
  w_MVDATRCO = ctod('  /  /  ')
  w_FLBLVE = space(1)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVTIPDOC = space(5)
  o_MVTIPDOC = space(5)
  w_TESTAD = space(1)
  w_CAUCOL = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_kdoPag1","gsof_kdo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMVTIPDOR_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='CAM_AGAZ'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsof_kdo
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVTIPCON=space(1)
      .w_CODAZI=space(5)
      .w_MVTIPDOR=space(5)
      .w_MVTIPDOE=space(5)
      .w_RICNOM=space(1)
      .w_FLMGPR=space(1)
      .w_MAGDOC=space(5)
      .w_OMAG=space(5)
      .w_MVFLVEAC=space(1)
      .w_MVCLADOC=space(2)
      .w_MVFLINTE=space(1)
      .w_MVTCAMAG=space(5)
      .w_FLCASC=space(1)
      .w_FLRISE=space(1)
      .w_FLIMPE=space(1)
      .w_FLORDI=space(1)
      .w_MVCODESE=space(4)
      .w_DESRIF=space(35)
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(2)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_MVCODMAG=space(5)
      .w_DESMAG=space(30)
      .w_MVFLPROV=space(1)
      .w_OKDATDIV=.f.
      .w_MVDATDIV=ctod("  /  /  ")
      .w_MVDATEVA=ctod("  /  /  ")
      .w_MVDATTRA=ctod("  /  /  ")
      .w_MVORATRA=space(2)
      .w_MVMINTRA=space(2)
      .w_MVPRD=space(2)
      .w_MVANNDOC=space(4)
      .w_MVDATCIV=ctod("  /  /  ")
      .w_MVFLACCO=space(1)
      .w_MVTFRAGG=space(1)
      .w_MVCAUCON=space(5)
      .w_MVCODUTE=0
      .w_MVNUMREG=0
      .w_MVDATREG=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_MVALFEST=space(2)
      .w_MVPRP=space(2)
      .w_MVNUMEST=0
      .w_FLPPRO=space(1)
      .w_MVANNPRO=space(4)
      .w_NUMSCO=0
      .w_OK=.f.
      .w_FLANAL=space(1)
      .w_MVASPEST=space(30)
      .w_FLCOMM=space(1)
      .w_MV_SEGNO=space(1)
      .w_FLDTRC=space(1)
      .w_MVDATRCO=ctod("  /  /  ")
      .w_FLBLVE=space(1)
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVTIPDOC=space(5)
      .w_TESTAD=space(1)
      .w_CAUCOL=space(5)
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_MVCLADOC=oParentObject.w_MVCLADOC
      .w_MVFLINTE=oParentObject.w_MVFLINTE
      .w_MVTCAMAG=oParentObject.w_MVTCAMAG
      .w_MVCODESE=oParentObject.w_MVCODESE
      .w_MVNUMDOC=oParentObject.w_MVNUMDOC
      .w_MVALFDOC=oParentObject.w_MVALFDOC
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVCODMAG=oParentObject.w_MVCODMAG
      .w_MVFLPROV=oParentObject.w_MVFLPROV
      .w_OKDATDIV=oParentObject.w_OKDATDIV
      .w_MVDATDIV=oParentObject.w_MVDATDIV
      .w_MVDATEVA=oParentObject.w_MVDATEVA
      .w_MVDATTRA=oParentObject.w_MVDATTRA
      .w_MVORATRA=oParentObject.w_MVORATRA
      .w_MVMINTRA=oParentObject.w_MVMINTRA
      .w_MVPRD=oParentObject.w_MVPRD
      .w_MVANNDOC=oParentObject.w_MVANNDOC
      .w_MVDATCIV=oParentObject.w_MVDATCIV
      .w_MVFLACCO=oParentObject.w_MVFLACCO
      .w_MVTFRAGG=oParentObject.w_MVTFRAGG
      .w_MVCAUCON=oParentObject.w_MVCAUCON
      .w_MVCODUTE=oParentObject.w_MVCODUTE
      .w_MVNUMREG=oParentObject.w_MVNUMREG
      .w_MVDATREG=oParentObject.w_MVDATREG
      .w_MVALFEST=oParentObject.w_MVALFEST
      .w_MVPRP=oParentObject.w_MVPRP
      .w_MVNUMEST=oParentObject.w_MVNUMEST
      .w_MVANNPRO=oParentObject.w_MVANNPRO
      .w_NUMSCO=oParentObject.w_NUMSCO
      .w_OK=oParentObject.w_OK
      .w_MVASPEST=oParentObject.w_MVASPEST
      .w_MV_SEGNO=oParentObject.w_MV_SEGNO
      .w_FLDTRC=oParentObject.w_FLDTRC
      .w_MVDATRCO=oParentObject.w_MVDATRCO
      .w_FLBLVE=oParentObject.w_FLBLVE
      .w_MVNUMDOC=oParentObject.w_MVNUMDOC
      .w_MVALFDOC=oParentObject.w_MVALFDOC
      .w_MVTIPDOC=oParentObject.w_MVTIPDOC
        .w_MVTIPCON = 'C'
        .w_CODAZI = i_codazi
        .w_MVTIPDOR = IIF(UPPER(g_APPLICATION)="ADHOC REVOLUTION", This.oParentObject .w_MVTIPDOC , '')
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MVTIPDOR))
          .link_1_3('Full')
        endif
        .w_MVTIPDOE = IIF(UPPER(g_APPLICATION)="AD HOC ENTERPRISE", This.oParentObject .w_MVTIPDOC , '')
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_MVTIPDOE))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,7,.f.)
        .w_OMAG = IIF(EMPTY(.w_MAGDOC), g_MAGAZI, .w_MAGDOC)
        .DoRTCalc(9,12,.f.)
        if not(empty(.w_MVTCAMAG))
          .link_1_12('Full')
        endif
          .DoRTCalc(13,16,.f.)
        .w_MVCODESE = g_CODESE
          .DoRTCalc(18,20,.f.)
        .w_MVDATDOC = i_DATSYS
        .w_OBTEST = .w_MVDATDOC
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_MVCODMAG))
          .link_1_23('Full')
        endif
          .DoRTCalc(24,29,.f.)
        .w_MVORATRA = left(time(),2)
        .w_MVMINTRA = substr(time(),4,2)
          .DoRTCalc(32,32,.f.)
        .w_MVANNDOC = STR(YEAR(.w_MVDATDOC), 4, 0)
        .w_MVDATCIV = .w_MVDATDOC
          .DoRTCalc(35,37,.f.)
        .w_MVCODUTE = IIF(g_MAGUTE='S', 0, i_CODUTE)
        .w_MVNUMREG = 0
        .w_MVDATREG = .w_MVDATDOC
          .DoRTCalc(41,41,.f.)
        .w_MVALFEST = '  '
        .w_MVPRP = 'NN'
        .w_MVNUMEST = 0
          .DoRTCalc(45,45,.f.)
        .w_MVANNPRO = CALPRO(.w_MVDATREG,.w_MVCODESE,.w_FLPPRO)
          .DoRTCalc(47,47,.f.)
        .w_OK = .T.
          .DoRTCalc(49,53,.f.)
        .w_MVDATRCO = i_DATSYS
          .DoRTCalc(55,57,.f.)
        .w_MVTIPDOC = IIF(UPPER(g_APPLICATION)="AD HOC ENTERPRISE", .w_MVTIPDOE , .w_MVTIPDOR)
        .w_TESTAD = 'S'
    endwith
    this.DoRTCalc(60,60,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_MVCLADOC=.w_MVCLADOC
      .oParentObject.w_MVFLINTE=.w_MVFLINTE
      .oParentObject.w_MVTCAMAG=.w_MVTCAMAG
      .oParentObject.w_MVCODESE=.w_MVCODESE
      .oParentObject.w_MVNUMDOC=.w_MVNUMDOC
      .oParentObject.w_MVALFDOC=.w_MVALFDOC
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVCODMAG=.w_MVCODMAG
      .oParentObject.w_MVFLPROV=.w_MVFLPROV
      .oParentObject.w_OKDATDIV=.w_OKDATDIV
      .oParentObject.w_MVDATDIV=.w_MVDATDIV
      .oParentObject.w_MVDATEVA=.w_MVDATEVA
      .oParentObject.w_MVDATTRA=.w_MVDATTRA
      .oParentObject.w_MVORATRA=.w_MVORATRA
      .oParentObject.w_MVMINTRA=.w_MVMINTRA
      .oParentObject.w_MVPRD=.w_MVPRD
      .oParentObject.w_MVANNDOC=.w_MVANNDOC
      .oParentObject.w_MVDATCIV=.w_MVDATCIV
      .oParentObject.w_MVFLACCO=.w_MVFLACCO
      .oParentObject.w_MVTFRAGG=.w_MVTFRAGG
      .oParentObject.w_MVCAUCON=.w_MVCAUCON
      .oParentObject.w_MVCODUTE=.w_MVCODUTE
      .oParentObject.w_MVNUMREG=.w_MVNUMREG
      .oParentObject.w_MVDATREG=.w_MVDATREG
      .oParentObject.w_MVALFEST=.w_MVALFEST
      .oParentObject.w_MVPRP=.w_MVPRP
      .oParentObject.w_MVNUMEST=.w_MVNUMEST
      .oParentObject.w_MVANNPRO=.w_MVANNPRO
      .oParentObject.w_NUMSCO=.w_NUMSCO
      .oParentObject.w_OK=.w_OK
      .oParentObject.w_MVASPEST=.w_MVASPEST
      .oParentObject.w_MV_SEGNO=.w_MV_SEGNO
      .oParentObject.w_FLDTRC=.w_FLDTRC
      .oParentObject.w_MVDATRCO=.w_MVDATRCO
      .oParentObject.w_FLBLVE=.w_FLBLVE
      .oParentObject.w_MVNUMDOC=.w_MVNUMDOC
      .oParentObject.w_MVALFDOC=.w_MVALFDOC
      .oParentObject.w_MVTIPDOC=.w_MVTIPDOC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_MVTIPCON = 'C'
            .w_CODAZI = i_codazi
        .DoRTCalc(3,7,.t.)
            .w_OMAG = IIF(EMPTY(.w_MAGDOC), g_MAGAZI, .w_MAGDOC)
        .DoRTCalc(9,11,.t.)
          .link_1_12('Full')
        .DoRTCalc(13,16,.t.)
            .w_MVCODESE = g_CODESE
        .DoRTCalc(18,21,.t.)
            .w_OBTEST = .w_MVDATDOC
        if .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_MVTCAMAG<>.w_MVTCAMAG.or. .o_MVTIPDOE<>.w_MVTIPDOE.or. .o_MVTIPDOR<>.w_MVTIPDOR
            .w_MVCODMAG = IIF (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION" , IIF(Empty(.w_FLCASC+.w_FLRISE+.w_FLORDI+.w_FLIMPE), SPACE(5),IIF(EMPTY(.w_MAGDOC), g_MAGAZI, IIF(EMPTY(.w_MAGDOC),.w_MVCODMAG,.w_MAGDOC)))     ,CALCMAG(1, .w_FLMGPR, '     ', .w_MAGDOC, .w_OMAG, '', ''))
          .link_1_23('Full')
        endif
        .DoRTCalc(24,24,.t.)
        if .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_MVFLPROV<>.w_MVFLPROV
            .w_MVFLPROV = IIF(NVL(.w_FLBLVE,' ')='S' AND g_PERFID='S' AND !EMPTY(.w_FLIMPE+.w_FLCASC), 'S', IIF(EMPTY(NVL(.w_MVFLPROV,'N')),'N', NVL(.w_MVFLPROV,'N')) )
        endif
        .DoRTCalc(26,26,.t.)
        if .o_MVDATDOC<>.w_MVDATDOC
            .w_MVDATDIV = IIF(.w_OKDATDIV,.w_MVDATDOC,cp_CharToDate('  /  /    '))
        endif
        if .o_MVDATDOC<>.w_MVDATDOC
            .w_MVDATEVA = .w_MVDATDOC
        endif
        if .o_MVDATDOC<>.w_MVDATDOC
            .w_MVDATTRA = .w_MVDATDOC
        endif
        .DoRTCalc(30,32,.t.)
            .w_MVANNDOC = STR(YEAR(.w_MVDATDOC), 4, 0)
        if .o_MVDATDOC<>.w_MVDATDOC
            .w_MVDATCIV = .w_MVDATDOC
        endif
        .DoRTCalc(35,39,.t.)
            .w_MVDATREG = .w_MVDATDOC
        .DoRTCalc(41,41,.t.)
            .w_MVALFEST = '  '
            .w_MVPRP = 'NN'
        .DoRTCalc(44,45,.t.)
            .w_MVANNPRO = CALPRO(.w_MVDATREG,.w_MVCODESE,.w_FLPPRO)
        .DoRTCalc(47,47,.t.)
            .w_OK = .T.
        .DoRTCalc(49,57,.t.)
            .w_MVTIPDOC = IIF(UPPER(g_APPLICATION)="AD HOC ENTERPRISE", .w_MVTIPDOE , .w_MVTIPDOR)
        if .o_MVALFDOC<>.w_MVALFDOC.or. .o_MVTIPDOC<>.w_MVTIPDOC.or. .o_MVDATDOC<>.w_MVDATDOC
          .Calculate_ZEURXQXCYG()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(59,60,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_ZEURXQXCYG()
    with this
          * --- Calcola progressivo documento
          gsof_bd1(this;
             )
    endwith
  endproc
  proc Calculate_DLNVSRPCBB()
    with this
          * --- Inizializza codice magazzino
          .w_MVCODMAG = IIF (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION" , IIF(Empty(.w_FLCASC+.w_FLRISE+.w_FLORDI+.w_FLIMPE), SPACE(5),IIF(EMPTY(.w_MAGDOC), g_MAGAZI, IIF(EMPTY(.w_MVCODMAG),.w_MAGDOC,.w_MVCODMAG)))     ,CALCMAG(1, .w_FLMGPR, '     ', .w_MAGDOC, .w_OMAG, '', ''))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMVTIPDOR_1_3.enabled = this.oPgFrm.Page1.oPag.oMVTIPDOR_1_3.mCond()
    this.oPgFrm.Page1.oPag.oMVTIPDOE_1_4.enabled = this.oPgFrm.Page1.oPag.oMVTIPDOE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oMVNUMDOC_1_19.enabled = this.oPgFrm.Page1.oPag.oMVNUMDOC_1_19.mCond()
    this.oPgFrm.Page1.oPag.oMVALFDOC_1_20.enabled = this.oPgFrm.Page1.oPag.oMVALFDOC_1_20.mCond()
    this.oPgFrm.Page1.oPag.oMVCODMAG_1_23.enabled = this.oPgFrm.Page1.oPag.oMVCODMAG_1_23.mCond()
    this.oPgFrm.Page1.oPag.oMVFLPROV_1_25.enabled = this.oPgFrm.Page1.oPag.oMVFLPROV_1_25.mCond()
    this.oPgFrm.Page1.oPag.oMVDATDIV_1_27.enabled = this.oPgFrm.Page1.oPag.oMVDATDIV_1_27.mCond()
    this.oPgFrm.Page1.oPag.oMVDATEVA_1_28.enabled = this.oPgFrm.Page1.oPag.oMVDATEVA_1_28.mCond()
    this.oPgFrm.Page1.oPag.oMVDATTRA_1_29.enabled = this.oPgFrm.Page1.oPag.oMVDATTRA_1_29.mCond()
    this.oPgFrm.Page1.oPag.oMVORATRA_1_30.enabled = this.oPgFrm.Page1.oPag.oMVORATRA_1_30.mCond()
    this.oPgFrm.Page1.oPag.oMVMINTRA_1_31.enabled = this.oPgFrm.Page1.oPag.oMVMINTRA_1_31.mCond()
    this.oPgFrm.Page1.oPag.oMVALFDOC_1_72.enabled = this.oPgFrm.Page1.oPag.oMVALFDOC_1_72.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMVTIPDOR_1_3.visible=!this.oPgFrm.Page1.oPag.oMVTIPDOR_1_3.mHide()
    this.oPgFrm.Page1.oPag.oMVTIPDOE_1_4.visible=!this.oPgFrm.Page1.oPag.oMVTIPDOE_1_4.mHide()
    this.oPgFrm.Page1.oPag.oMVNUMDOC_1_19.visible=!this.oPgFrm.Page1.oPag.oMVNUMDOC_1_19.mHide()
    this.oPgFrm.Page1.oPag.oMVALFDOC_1_20.visible=!this.oPgFrm.Page1.oPag.oMVALFDOC_1_20.mHide()
    this.oPgFrm.Page1.oPag.oMVDATDIV_1_27.visible=!this.oPgFrm.Page1.oPag.oMVDATDIV_1_27.mHide()
    this.oPgFrm.Page1.oPag.oMVDATEVA_1_28.visible=!this.oPgFrm.Page1.oPag.oMVDATEVA_1_28.mHide()
    this.oPgFrm.Page1.oPag.oMVDATTRA_1_29.visible=!this.oPgFrm.Page1.oPag.oMVDATTRA_1_29.mHide()
    this.oPgFrm.Page1.oPag.oMVORATRA_1_30.visible=!this.oPgFrm.Page1.oPag.oMVORATRA_1_30.mHide()
    this.oPgFrm.Page1.oPag.oMVMINTRA_1_31.visible=!this.oPgFrm.Page1.oPag.oMVMINTRA_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oMVDATRCO_1_67.visible=!this.oPgFrm.Page1.oPag.oMVDATRCO_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oMVNUMDOC_1_71.visible=!this.oPgFrm.Page1.oPag.oMVNUMDOC_1_71.mHide()
    this.oPgFrm.Page1.oPag.oMVALFDOC_1_72.visible=!this.oPgFrm.Page1.oPag.oMVALFDOC_1_72.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_ZEURXQXCYG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_DLNVSRPCBB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVTIPDOR
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_BZA',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MVTIPDOR)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TD_SEGNO,TDFLMGPR,TDRICNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MVTIPDOR))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TD_SEGNO,TDFLMGPR,TDRICNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTIPDOR)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVTIPDOR) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMVTIPDOR_1_3'),i_cWhere,'GSOF_BZA',"Causali documento",'GSOF0ATD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TD_SEGNO,TDFLMGPR,TDRICNOM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TD_SEGNO,TDFLMGPR,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TD_SEGNO,TDFLMGPR,TDRICNOM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOR)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TD_SEGNO,TDFLMGPR,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOR = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESRIF = NVL(_Link_.TDDESDOC,space(35))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_MVALFDOC = NVL(_Link_.TDALFDOC,space(10))
      this.w_MVPRD = NVL(_Link_.TDPRODOC,space(2))
      this.w_MVCLADOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_MVFLACCO = NVL(_Link_.TDFLACCO,space(1))
      this.w_MVFLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVTCAMAG = NVL(_Link_.TDCAUMAG,space(5))
      this.w_MVTFRAGG = NVL(_Link_.TFFLRAGG,space(1))
      this.w_MVCAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_FLPPRO = NVL(_Link_.TDFLPPRO,space(1))
      this.w_NUMSCO = NVL(_Link_.TDNUMSCO,0)
      this.w_MAGDOC = NVL(_Link_.TDCODMAG,space(5))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_MVASPEST = NVL(_Link_.TDASPETT,space(30))
      this.w_FLCOMM = NVL(_Link_.TDFLCOMM,space(1))
      this.w_MV_SEGNO = NVL(_Link_.TD_SEGNO,space(1))
      this.w_FLMGPR = NVL(_Link_.TDFLMGPR,space(1))
      this.w_RICNOM = NVL(_Link_.TDRICNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOR = space(5)
      endif
      this.w_DESRIF = space(35)
      this.w_MVFLVEAC = space(1)
      this.w_MVALFDOC = space(10)
      this.w_MVPRD = space(2)
      this.w_MVCLADOC = space(2)
      this.w_MVFLACCO = space(1)
      this.w_MVFLINTE = space(1)
      this.w_MVTCAMAG = space(5)
      this.w_MVTFRAGG = space(1)
      this.w_MVCAUCON = space(5)
      this.w_FLPPRO = space(1)
      this.w_NUMSCO = 0
      this.w_MAGDOC = space(5)
      this.w_FLANAL = space(1)
      this.w_MVASPEST = space(30)
      this.w_FLCOMM = space(1)
      this.w_MV_SEGNO = space(1)
      this.w_FLMGPR = space(1)
      this.w_RICNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVFLVEAC='V' AND .w_MVFLINTE='C' AND (.w_MVCLADOC='OR' OR (.w_MVCLADOC='DI' AND .w_RICNOM<>'A' AND EMPTY(.w_FLCOMM)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_MVTIPDOR = space(5)
        this.w_DESRIF = space(35)
        this.w_MVFLVEAC = space(1)
        this.w_MVALFDOC = space(10)
        this.w_MVPRD = space(2)
        this.w_MVCLADOC = space(2)
        this.w_MVFLACCO = space(1)
        this.w_MVFLINTE = space(1)
        this.w_MVTCAMAG = space(5)
        this.w_MVTFRAGG = space(1)
        this.w_MVCAUCON = space(5)
        this.w_FLPPRO = space(1)
        this.w_NUMSCO = 0
        this.w_MAGDOC = space(5)
        this.w_FLANAL = space(1)
        this.w_MVASPEST = space(30)
        this.w_FLCOMM = space(1)
        this.w_MV_SEGNO = space(1)
        this.w_FLMGPR = space(1)
        this.w_RICNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTIPDOE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_BZA',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MVTIPDOE)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TDFLDTRC,TDRICNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MVTIPDOE))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TDFLDTRC,TDRICNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTIPDOE)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVTIPDOE) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMVTIPDOE_1_4'),i_cWhere,'GSOF_BZA',"Causali documento",'GSOF0ATD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TDFLDTRC,TDRICNOM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TDFLDTRC,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TDFLDTRC,TDRICNOM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOE)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDALFDOC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDNUMSCO,TDCODMAG,TDFLANAL,TDASPETT,TDFLCOMM,TDFLDTRC,TDRICNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOE = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESRIF = NVL(_Link_.TDDESDOC,space(35))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_MVALFDOC = NVL(_Link_.TDALFDOC,space(10))
      this.w_MVPRD = NVL(_Link_.TDPRODOC,space(2))
      this.w_MVCLADOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_MVFLACCO = NVL(_Link_.TDFLACCO,space(1))
      this.w_MVFLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVTCAMAG = NVL(_Link_.TDCAUMAG,space(5))
      this.w_MVTFRAGG = NVL(_Link_.TFFLRAGG,space(1))
      this.w_MVCAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_FLPPRO = NVL(_Link_.TDFLPPRO,space(1))
      this.w_NUMSCO = NVL(_Link_.TDNUMSCO,0)
      this.w_MAGDOC = NVL(_Link_.TDCODMAG,space(5))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_MVASPEST = NVL(_Link_.TDASPETT,space(30))
      this.w_FLCOMM = NVL(_Link_.TDFLCOMM,space(1))
      this.w_FLDTRC = NVL(_Link_.TDFLDTRC,space(1))
      this.w_RICNOM = NVL(_Link_.TDRICNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOE = space(5)
      endif
      this.w_DESRIF = space(35)
      this.w_MVFLVEAC = space(1)
      this.w_MVALFDOC = space(10)
      this.w_MVPRD = space(2)
      this.w_MVCLADOC = space(2)
      this.w_MVFLACCO = space(1)
      this.w_MVFLINTE = space(1)
      this.w_MVTCAMAG = space(5)
      this.w_MVTFRAGG = space(1)
      this.w_MVCAUCON = space(5)
      this.w_FLPPRO = space(1)
      this.w_NUMSCO = 0
      this.w_MAGDOC = space(5)
      this.w_FLANAL = space(1)
      this.w_MVASPEST = space(30)
      this.w_FLCOMM = space(1)
      this.w_FLDTRC = space(1)
      this.w_RICNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVFLVEAC='V' AND .w_MVFLINTE='C' AND ((.w_MVCLADOC='OP' OR .w_MVCLADOC='OR') OR (.w_MVCLADOC='DI' AND .w_RICNOM<>'A' AND EMPTY(.w_FLANAL) AND EMPTY(.w_FLCOMM)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_MVTIPDOE = space(5)
        this.w_DESRIF = space(35)
        this.w_MVFLVEAC = space(1)
        this.w_MVALFDOC = space(10)
        this.w_MVPRD = space(2)
        this.w_MVCLADOC = space(2)
        this.w_MVFLACCO = space(1)
        this.w_MVFLINTE = space(1)
        this.w_MVTCAMAG = space(5)
        this.w_MVTFRAGG = space(1)
        this.w_MVCAUCON = space(5)
        this.w_FLPPRO = space(1)
        this.w_NUMSCO = 0
        this.w_MAGDOC = space(5)
        this.w_FLANAL = space(1)
        this.w_MVASPEST = space(30)
        this.w_FLCOMM = space(1)
        this.w_FLDTRC = space(1)
        this.w_RICNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTCAMAG
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTCAMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTCAMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVTCAMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVTCAMAG)
            select CMCODICE,CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTCAMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_FLCASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_FLRISE = NVL(_Link_.CMFLRISE,space(1))
      this.w_FLORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_FLIMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_CAUCOL = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MVTCAMAG = space(5)
      endif
      this.w_FLCASC = space(1)
      this.w_FLRISE = space(1)
      this.w_FLORDI = space(1)
      this.w_FLIMPE = space(1)
      this.w_CAUCOL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTCAMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODMAG
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MVCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MVCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMVCODMAG_1_23'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MVCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MVCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino inesistente o obsoleto")
        endif
        this.w_MVCODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVTIPDOR_1_3.value==this.w_MVTIPDOR)
      this.oPgFrm.Page1.oPag.oMVTIPDOR_1_3.value=this.w_MVTIPDOR
    endif
    if not(this.oPgFrm.Page1.oPag.oMVTIPDOE_1_4.value==this.w_MVTIPDOE)
      this.oPgFrm.Page1.oPag.oMVTIPDOE_1_4.value=this.w_MVTIPDOE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIF_1_18.value==this.w_DESRIF)
      this.oPgFrm.Page1.oPag.oDESRIF_1_18.value=this.w_DESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMVNUMDOC_1_19.value==this.w_MVNUMDOC)
      this.oPgFrm.Page1.oPag.oMVNUMDOC_1_19.value=this.w_MVNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFDOC_1_20.value==this.w_MVALFDOC)
      this.oPgFrm.Page1.oPag.oMVALFDOC_1_20.value=this.w_MVALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATDOC_1_21.value==this.w_MVDATDOC)
      this.oPgFrm.Page1.oPag.oMVDATDOC_1_21.value=this.w_MVDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODMAG_1_23.value==this.w_MVCODMAG)
      this.oPgFrm.Page1.oPag.oMVCODMAG_1_23.value=this.w_MVCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_24.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_24.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFLPROV_1_25.RadioValue()==this.w_MVFLPROV)
      this.oPgFrm.Page1.oPag.oMVFLPROV_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATDIV_1_27.value==this.w_MVDATDIV)
      this.oPgFrm.Page1.oPag.oMVDATDIV_1_27.value=this.w_MVDATDIV
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATEVA_1_28.value==this.w_MVDATEVA)
      this.oPgFrm.Page1.oPag.oMVDATEVA_1_28.value=this.w_MVDATEVA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATTRA_1_29.value==this.w_MVDATTRA)
      this.oPgFrm.Page1.oPag.oMVDATTRA_1_29.value=this.w_MVDATTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVORATRA_1_30.value==this.w_MVORATRA)
      this.oPgFrm.Page1.oPag.oMVORATRA_1_30.value=this.w_MVORATRA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVMINTRA_1_31.value==this.w_MVMINTRA)
      this.oPgFrm.Page1.oPag.oMVMINTRA_1_31.value=this.w_MVMINTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATRCO_1_67.value==this.w_MVDATRCO)
      this.oPgFrm.Page1.oPag.oMVDATRCO_1_67.value=this.w_MVDATRCO
    endif
    if not(this.oPgFrm.Page1.oPag.oMVNUMDOC_1_71.value==this.w_MVNUMDOC)
      this.oPgFrm.Page1.oPag.oMVNUMDOC_1_71.value=this.w_MVNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFDOC_1_72.value==this.w_MVALFDOC)
      this.oPgFrm.Page1.oPag.oMVALFDOC_1_72.value=this.w_MVALFDOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_MVTIPDOR)) or not(.w_MVFLVEAC='V' AND .w_MVFLINTE='C' AND (.w_MVCLADOC='OR' OR (.w_MVCLADOC='DI' AND .w_RICNOM<>'A' AND EMPTY(.w_FLCOMM)))))  and not(UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")  and (UPPER(g_APPLICATION)="ADHOC REVOLUTION")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVTIPDOR_1_3.SetFocus()
            i_bnoObbl = !empty(.w_MVTIPDOR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   ((empty(.w_MVTIPDOE)) or not(.w_MVFLVEAC='V' AND .w_MVFLINTE='C' AND ((.w_MVCLADOC='OP' OR .w_MVCLADOC='OR') OR (.w_MVCLADOC='DI' AND .w_RICNOM<>'A' AND EMPTY(.w_FLANAL) AND EMPTY(.w_FLCOMM)))))  and not(UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE")  and (UPPER(g_APPLICATION)="AD HOC ENTERPRISE")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVTIPDOE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_MVTIPDOE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   (empty(.w_MVNUMDOC))  and not(UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')  and (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVNUMDOC_1_19.SetFocus()
            i_bnoObbl = !empty(.w_MVNUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MVDATDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVDATDOC_1_21.SetFocus()
            i_bnoObbl = !empty(.w_MVDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MVCODMAG)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_FLCASC $'+-' OR .w_FLRISE $'+-' OR .w_FLORDI $'+-' OR .w_FLIMPE $'+-')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVCODMAG_1_23.SetFocus()
            i_bnoObbl = !empty(.w_MVCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino inesistente o obsoleto")
          case   (empty(.w_MVDATDIV))  and not(NOT(.w_OKDATDIV))  and (.w_OKDATDIV)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVDATDIV_1_27.SetFocus()
            i_bnoObbl = !empty(.w_MVDATDIV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MVDATEVA)) or not(.w_MVDATEVA>=.w_MVDATDOC))  and not(.w_MVCLADOC<>'OR')  and (.w_MVCLADOC='OR')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVDATEVA_1_28.SetFocus()
            i_bnoObbl = !empty(.w_MVDATEVA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data prevista evasione riga deve essere maggiore o uguale alla data ordine")
          case   not((.w_MVDATTRA>=.w_MVDATDOC) OR EMPTY(.w_MVDATTRA))  and not(.w_MVFLACCO<>'S')  and (.w_MVFLACCO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVDATTRA_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di trasporto non pu� precedere la data del documento")
          case   not(VAL(.w_MVORATRA)<24)  and not(.w_MVFLACCO<>'S')  and (.w_MVFLACCO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVORATRA_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora trasporto non corretta")
          case   not(VAL(.w_MVMINTRA)<60)  and not(.w_MVFLACCO<>'S')  and (.w_MVFLACCO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVMINTRA_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuto trasporto non corretto")
          case   (empty(.w_MVNUMDOC))  and not(UPPER(g_APPLICATION)<>'ADHOC REVOLUTION')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMVNUMDOC_1_71.SetFocus()
            i_bnoObbl = !empty(.w_MVNUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_kdo
      If UPPER(g_APPLICATION)="ADHOC REVOLUTION"
         IF Not Empty(CHKCONS('V'+IIF(Not Empty(.w_MVCODMAG),'M',''),.w_MVDATDOC,'B','N'))
           i_bnoChk=.F.
           i_bRes=.F.
           i_cErrorMsg=CHKCONS('V'+IIF(Not Empty(.w_MVCODMAG),'M',''),.w_MVDATDOC,'B','N')
         ENDIF
         If i_bRes and Not Empty(.w_CAUCOL)
           i_bnoChk=.F.
           i_bRes=.F.
           i_cErrorMsg=Ah_msgformat("Attenzione causale documento incongruente!")
         Endif
      EndIf
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVTIPDOR = this.w_MVTIPDOR
    this.o_MVTIPDOE = this.w_MVTIPDOE
    this.o_MVTCAMAG = this.w_MVTCAMAG
    this.o_MVALFDOC = this.w_MVALFDOC
    this.o_MVDATDOC = this.w_MVDATDOC
    this.o_MVFLPROV = this.w_MVFLPROV
    this.o_MVTIPDOC = this.w_MVTIPDOC
    return

enddefine

* --- Define pages as container
define class tgsof_kdoPag1 as StdContainer
  Width  = 497
  height = 250
  stdWidth  = 497
  stdheight = 250
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVTIPDOR_1_3 as StdField with uid="HJNMKCAPBA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MVTIPDOR", cQueryName = "MVTIPDOR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Causale documento da generare",;
    HelpContextID = 194743784,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=14, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOF_BZA", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOR"

  func oMVTIPDOR_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
   endif
  endfunc

  func oMVTIPDOR_1_3.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oMVTIPDOR_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVTIPDOR_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTIPDOR_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMVTIPDOR_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_BZA',"Causali documento",'GSOF0ATD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMVTIPDOR_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSOF_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MVTIPDOR
     i_obj.ecpSave()
  endproc

  add object oMVTIPDOE_1_4 as StdField with uid="MKKNTXIWQI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MVTIPDOE", cQueryName = "MVTIPDOE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Causale documento da generare",;
    HelpContextID = 194743797,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=14, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOF_BZA", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOE"

  func oMVTIPDOE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="AD HOC ENTERPRISE")
    endwith
   endif
  endfunc

  func oMVTIPDOE_1_4.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE")
    endwith
  endfunc

  func oMVTIPDOE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVTIPDOE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTIPDOE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMVTIPDOE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_BZA',"Causali documento",'GSOF0ATD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMVTIPDOE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSOF_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MVTIPDOE
     i_obj.ecpSave()
  endproc

  add object oDESRIF_1_18 as StdField with uid="NZLUHJPELA",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 167948234,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=215, Top=14, InputMask=replicate('X',35)

  add object oMVNUMDOC_1_19 as StdField with uid="ZXXUTHULLA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MVNUMDOC", cQueryName = "MVNUMDOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo numero documento da generare disponibile",;
    HelpContextID = 197127671,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=148, Top=44, cSayPict='"999999"', cGetPict='"999999"'

  func oMVNUMDOC_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
   endif
  endfunc

  func oMVNUMDOC_1_19.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc

  add object oMVALFDOC_1_20 as StdField with uid="LGHDPDHADI",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MVALFDOC", cQueryName = "MVALFDOC",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie documenti da generare",;
    HelpContextID = 205110775,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=227, Top=44, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2)

  func oMVALFDOC_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE')
    endwith
   endif
  endfunc

  func oMVALFDOC_1_20.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc

  add object oMVDATDOC_1_21 as StdField with uid="SRXRGALYSN",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MVDATDOC", cQueryName = "MVDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 191139319,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=148, Top=68

  add object oMVCODMAG_1_23 as StdField with uid="TPGOFEGYRB",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MVCODMAG", cQueryName = "MVCODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino inesistente o obsoleto",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 56008179,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=148, Top=94, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MVCODMAG"

  func oMVCODMAG_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCASC $'+-' OR .w_FLRISE $'+-' OR .w_FLORDI $'+-' OR .w_FLIMPE $'+-')
    endwith
   endif
  endfunc

  func oMVCODMAG_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODMAG_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODMAG_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMVCODMAG_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oMVCODMAG_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MVCODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_24 as StdField with uid="MDIGFDOPLR",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 159887306,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=215, Top=94, InputMask=replicate('X',30)


  add object oMVFLPROV_1_25 as StdCombo with uid="AQKGHLCJVS",rtseq=25,rtrep=.f.,left=148,top=124,width=98,height=21;
    , HelpContextID = 228158948;
    , cFormVar="w_MVFLPROV",RowSource=""+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMVFLPROV_1_25.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oMVFLPROV_1_25.GetRadio()
    this.Parent.oContained.w_MVFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oMVFLPROV_1_25.SetRadio()
    this.Parent.oContained.w_MVFLPROV=trim(this.Parent.oContained.w_MVFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_MVFLPROV=='N',1,;
      iif(this.Parent.oContained.w_MVFLPROV=='S',2,;
      0))
  endfunc

  func oMVFLPROV_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(NVL(.w_FLBLVE,' ')='S' AND g_PERFID='S' AND !EMPTY(.w_FLIMPE+.w_FLCASC)))
    endwith
   endif
  endfunc

  add object oMVDATDIV_1_27 as StdField with uid="WNWCHHHDLF",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MVDATDIV", cQueryName = "MVDATDIV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale data di inizio scadenze per pagamenti in data diversa",;
    HelpContextID = 77296156,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=412, Top=124

  func oMVDATDIV_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OKDATDIV)
    endwith
   endif
  endfunc

  func oMVDATDIV_1_27.mHide()
    with this.Parent.oContained
      return (NOT(.w_OKDATDIV))
    endwith
  endfunc

  add object oMVDATEVA_1_28 as StdField with uid="FZYFWFANWE",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MVDATEVA", cQueryName = "MVDATEVA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data prevista evasione riga deve essere maggiore o uguale alla data ordine",;
    ToolTipText = "Data prevista evasione se non valorizzata la data prevista consegna nelle righe dell'offerta",;
    HelpContextID = 94073351,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=148, Top=154

  func oMVDATEVA_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVCLADOC='OR')
    endwith
   endif
  endfunc

  func oMVDATEVA_1_28.mHide()
    with this.Parent.oContained
      return (.w_MVCLADOC<>'OR')
    endwith
  endfunc

  func oMVDATEVA_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MVDATEVA>=.w_MVDATDOC)
    endwith
    return bRes
  endfunc

  add object oMVDATTRA_1_29 as StdField with uid="IJCTLEUHIR",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MVDATTRA", cQueryName = "MVDATTRA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di trasporto non pu� precedere la data del documento",;
    ToolTipText = "Data del trasporto",;
    HelpContextID = 191139321,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=148, Top=185

  func oMVDATTRA_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLACCO='S')
    endwith
   endif
  endfunc

  func oMVDATTRA_1_29.mHide()
    with this.Parent.oContained
      return (.w_MVFLACCO<>'S')
    endwith
  endfunc

  func oMVDATTRA_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_MVDATTRA>=.w_MVDATDOC) OR EMPTY(.w_MVDATTRA))
    endwith
    return bRes
  endfunc

  add object oMVORATRA_1_30 as StdField with uid="XSVIQFABPV",rtseq=30,rtrep=.f.,;
    cFormVar = "w_MVORATRA", cQueryName = "MVORATRA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora trasporto non corretta",;
    ToolTipText = "Ora del trasporto",;
    HelpContextID = 209903097,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=283, Top=185, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMVORATRA_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLACCO='S')
    endwith
   endif
  endfunc

  func oMVORATRA_1_30.mHide()
    with this.Parent.oContained
      return (.w_MVFLACCO<>'S')
    endwith
  endfunc

  func oMVORATRA_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MVORATRA)<24)
    endwith
    return bRes
  endfunc

  add object oMVMINTRA_1_31 as StdField with uid="JXMLZTQLRV",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MVMINTRA", cQueryName = "MVMINTRA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuto trasporto non corretto",;
    ToolTipText = "Ora del trasporto",;
    HelpContextID = 196869625,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=322, Top=185, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMVMINTRA_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLACCO='S')
    endwith
   endif
  endfunc

  func oMVMINTRA_1_31.mHide()
    with this.Parent.oContained
      return (.w_MVFLACCO<>'S')
    endwith
  endfunc

  func oMVMINTRA_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MVMINTRA)<60)
    endwith
    return bRes
  endfunc


  add object oBtn_1_53 as StdButton with uid="AOMIVSQTCE",left=391, top=200, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 186001126;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_54 as StdButton with uid="XRAPIYCAZC",left=442, top=200, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare la generazione";
    , HelpContextID = 28169978;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMVDATRCO_1_67 as StdField with uid="GADOSQRLDB",rtseq=54,rtrep=.f.,;
    cFormVar = "w_MVDATRCO", cQueryName = "MVDATRCO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data richiesta consegna",;
    HelpContextID = 224693739,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=148, Top=216

  func oMVDATRCO_1_67.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE" Or .w_FLDTRC<>'S')
    endwith
  endfunc

  add object oMVNUMDOC_1_71 as StdField with uid="ZBZVXNLRWT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_MVNUMDOC", cQueryName = "MVNUMDOC",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo numero di ordine da generare disponibile",;
    HelpContextID = 197127671,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=148, Top=44, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oMVNUMDOC_1_71.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'ADHOC REVOLUTION')
    endwith
  endfunc

  add object oMVALFDOC_1_72 as StdField with uid="SMYAVACMUC",rtseq=57,rtrep=.f.,;
    cFormVar = "w_MVALFDOC", cQueryName = "MVALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documenti da generare",;
    HelpContextID = 205110775,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=281, Top=44, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  func oMVALFDOC_1_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='ADHOC REVOLUTION')
    endwith
   endif
  endfunc

  func oMVALFDOC_1_72.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'ADHOC REVOLUTION')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="HOVDNAQJWB",Visible=.t., Left=213, Top=44,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="LTTIBSJSQS",Visible=.t., Left=250, Top=124,;
    Alignment=1, Width=160, Height=15,;
    Caption="Scadenze in data diversa:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (NOT(.w_OKDATDIV))
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="XFEDUGSZQH",Visible=.t., Left=6, Top=14,;
    Alignment=1, Width=140, Height=15,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="JNOBILKAHU",Visible=.t., Left=6, Top=44,;
    Alignment=1, Width=140, Height=15,;
    Caption="Numero documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="FQFFGXFEAF",Visible=.t., Left=6, Top=68,;
    Alignment=1, Width=140, Height=18,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE')
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="KXLMVYGVLS",Visible=.t., Left=6, Top=124,;
    Alignment=1, Width=140, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="ELXDWQMCJK",Visible=.t., Left=6, Top=154,;
    Alignment=1, Width=140, Height=18,;
    Caption="Data evasione:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_MVCLADOC<>'OR')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="VPXMOLWSVP",Visible=.t., Left=6, Top=94,;
    Alignment=1, Width=140, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="MVAGZFWZOA",Visible=.t., Left=6, Top=185,;
    Alignment=1, Width=140, Height=18,;
    Caption="Data trasporto:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_MVFLACCO<>'S')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="RIBVYRBMAY",Visible=.t., Left=236, Top=185,;
    Alignment=1, Width=43, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.w_MVFLACCO<>'S')
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="OOHESEIEMC",Visible=.t., Left=311, Top=185,;
    Alignment=1, Width=13, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (.w_MVFLACCO<>'S')
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="XERAKZHIRI",Visible=.t., Left=6, Top=218,;
    Alignment=1, Width=140, Height=18,;
    Caption="Data rich. consegna:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE" Or .w_FLDTRC<>'S')
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="UHDIMGEYZN",Visible=.t., Left=6, Top=68,;
    Alignment=1, Width=140, Height=18,;
    Caption="Data documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'ADHOC REVOLUTION')
    endwith
  endfunc

  add object oStr_1_73 as StdString with uid="ITOHEGJIUL",Visible=.t., Left=267, Top=44,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'ADHOC REVOLUTION')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_kdo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
