* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bk1                                                        *
*              Carica articoli da confermare                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-14                                                      *
* Last revis.: 2005-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bk1",oParentObject)
return(i_retval)

define class tgsof_bk1 as StdBatch
  * --- Local variables
  w_CNT = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Articoli da Confermare (da GSOF_MDC)
    if this.oParentObject.cFunction<>"Load"
      i_retcode = 'stop'
      return
    endif
    if USED("APPART")
      SELECT (this.oParentObject.cTrsName)
      * --- Azzera il Transitorio
      if reccount()<>0
        ZAP
      endif
      * --- Cicla sul Risultato della Query
      SELECT APPART
      GO TOP
      this.w_CNT = 0
      SCAN FOR NOT EMPTY(NVL(t_ODCODICE, ""))
      SELECT (this.oParentObject.cTrsName)
      this.oParentObject.InitRow()
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      SELECT APPART
      this.oParentObject.w_CPROWORD = t_CPROWORD
      this.oParentObject.w_ODCODICE = t_ODCODICE
      this.oParentObject.w_ODTIPRIG = t_ODTIPRIG
      this.oParentObject.w_ODCODGRU = t_ODCODGRU
      this.oParentObject.w_ODCODSOT = t_ODCODSOT
      this.oParentObject.w_UNMIS1 = t_UNMIS1
      this.oParentObject.w_UNMIS2 = t_UNMIS2
      this.oParentObject.w_UNMIS3 = t_UNMIS3
      this.oParentObject.w_ODCODART = t_ODCODART
      this.oParentObject.w_ODDESART = t_ODDESART
      this.oParentObject.w_ODUNIMIS = t_ODUNIMIS
      this.oParentObject.w_ODQTAMOV = t_ODQTAMOV
      this.oParentObject.w_ODPREZZO = t_ODPREZZO
      this.oParentObject.w_NURIGA = CPROWNUM
      * --- Carica il Temporaneo
      this.w_CNT = this.w_CNT + 1
      * --- Carica il Temporaneo dei Dati
      SELECT (this.oParentObject.cTrsName)
      this.oParentObject.TrsFromWork
      SELECT APPART
      ENDSCAN
      * --- Questa Parte derivata dal Metodo LoadRec
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      With this.oParentObject
      .WorkFromTrs()
      .SaveDependsOn()
      .SetControlsValue()
      .ChildrenChangeRow()
      .oPgFrm.Page1.oPag.oBody.Refresh()
      .oPgFrm.Page1.oPag.oBody.nAbsRow=1
      .oPgFrm.Page1.oPag.oBody.nRelRow=1
      EndWith
    endif
    if used("APPART")
      SELECT APPART
      USE
    endif
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    if this.w_CNT=0
      if reccount()<>0
        ZAP
      endif
      ah_errormsg("Non ci sono dati da elaborare")
      this.oParentObject.InitRow()
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
