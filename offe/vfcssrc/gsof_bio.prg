* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bio                                                        *
*              Import da dettaglio offerte                                     *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_82]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-13                                                      *
* Last revis.: 2003-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bio",oParentObject)
return(i_retval)

define class tgsof_bio as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Ricerca da Import Documenti Collegati (da GSOF_KIO)
    this.w_ZOOM = this.oParentObject.w_ZoomMast
    * --- Lancia la Query
    this.oParentObject.NotifyEvent("Calcola")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
