* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bkk                                                        *
*              Check kit promozionali                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2005-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bkk",oParentObject)
return(i_retval)

define class tgsof_bkk as StdBatch
  * --- Local variables
  w_RECTRS1 = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Check kit promozionali
    this.oParentObject.w_BRES = .T.
    SELECT (this.oParentObject.cTrsName) 
    this.w_RECTRS1 = 0
    SCAN FOR NOT DELETED()
    this.w_RECTRS1 = this.w_RECTRS1 + 1
    endscan
    if this.w_RECTRS1=1
      GOTO TOP
      this.w_RECTRS1 = IIF(EMPTY(NVL(t_KPCODICE," ")), 0, 1)
    endif
    if this.w_RECTRS1 = 0
      ah_errormsg("Kit promozionale senza articoli/servizi")
      this.oParentObject.w_BRES = .F.
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
