* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_mkp                                                        *
*              Kit promozionali                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2014-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_mkp"))

* --- Class definition
define class tgsof_mkp as StdTrsForm
  Top    = 10
  Left   = 9

  * --- Standard Properties
  Width  = 796
  Height = 411+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-06"
  HelpContextID=37082775
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=65

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  KITMPROM_IDX = 0
  KIT_PROM_IDX = 0
  TIT_SEZI_IDX = 0
  SOT_SEZI_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  GRU_NOMI_IDX = 0
  GRU_PRIO_IDX = 0
  ORI_NOMI_IDX = 0
  VALUTE_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  UNIMIS_IDX = 0
  AZIENDA_IDX = 0
  TIP_DOCU_IDX = 0
  CPUSERS_IDX = 0
  CPGROUPS_IDX = 0
  cFile = "KITMPROM"
  cFileDetail = "KIT_PROM"
  cKeySelect = "KPCODKIT"
  cKeyWhere  = "KPCODKIT=this.w_KPCODKIT"
  cKeyDetail  = "KPCODKIT=this.w_KPCODKIT"
  cKeyWhereODBC = '"KPCODKIT="+cp_ToStrODBC(this.w_KPCODKIT)';

  cKeyDetailWhereODBC = '"KPCODKIT="+cp_ToStrODBC(this.w_KPCODKIT)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"KIT_PROM.KPCODKIT="+cp_ToStrODBC(this.w_KPCODKIT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'KIT_PROM.CPROWORD '
  cPrg = "gsof_mkp"
  cComment = "Kit promozionali"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_NUMSCO = 0
  w_KPCODKIT = space(15)
  w_KPDESKIT = space(35)
  w_KPCODVAL = space(3)
  o_KPCODVAL = space(3)
  w_DECTOT = 0
  w_DECUNI = 0
  w_CALCPICT = 0
  w_CALCPICU = 0
  w_KP__NOTE = space(0)
  w_KPDATOBS = ctod('  /  /  ')
  w_CPROWORD = 0
  w_KPCODICE = space(20)
  o_KPCODICE = space(20)
  w_KPCODICE = space(41)
  w_KPCODART = space(20)
  w_KPCODGRU = space(5)
  o_KPCODGRU = space(5)
  w_KPCODSOT = space(5)
  w_KPDESART = space(40)
  w_KPUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_KPQTAMOV = 0
  o_KPQTAMOV = 0
  w_KPCODUTE = 0
  w_KPGRPUTE = 0
  w_KPCODPRN = space(5)
  w_KPCODGRN = space(5)
  w_KPCODORI = space(5)
  w_KPCODZON = space(3)
  w_KPCODPRF = space(5)
  w_KPCODAGE = space(5)
  w_KPDATVAL = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_KPDESVAL = space(35)
  w_KPOBVAL = ctod('  /  /  ')
  w_KPPREZZO = 0
  w_KPDESGRN = space(35)
  w_KPDESORI = space(35)
  w_KPDESZON = space(35)
  w_KPOBZON = ctod('  /  /  ')
  w_KPDESAGE = space(35)
  w_KPOBAGE = ctod('  /  /  ')
  w_KPDESPRI = space(35)
  w_KPDESPRF = space(35)
  w_KPDESOPE = space(35)
  w_KPDESGRU = space(35)
  w_KPTIPRIG = space(1)
  w_KPSCONT1 = 0
  o_KPSCONT1 = 0
  w_KPSCONT2 = 0
  o_KPSCONT2 = 0
  w_KPSCONT3 = 0
  o_KPSCONT3 = 0
  w_KPSCONT4 = 0
  w_CODI = space(15)
  w_DESC = space(35)
  w_VALUNI = 0
  o_VALUNI = 0
  w_KPARNOCO = space(1)
  w_KPNETRIG = 0
  w_UNMIS3 = space(3)
  w_UNMIS1 = space(3)
  w_FLSERG = space(1)
  w_UNMIS2 = space(3)
  w_MOLTI3 = 0
  w_KPTOTRIG = 0
  w_KPNOTAGG = space(0)
  w_DTOBS1 = ctod('  /  /  ')
  w_KPFLOMAG = space(1)
  w_BRES = .F.
  w_CODESC = space(5)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'KITMPROM','gsof_mkp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_mkpPag1","gsof_mkp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dettaglio")
      .Pages(1).HelpContextID = 4094065
      .Pages(2).addobject("oPag","tgsof_mkpPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Filtri")
      .Pages(2).HelpContextID = 221182890
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oKPCODKIT_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[17]
    this.cWorkTables[1]='TIT_SEZI'
    this.cWorkTables[2]='SOT_SEZI'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='GRU_NOMI'
    this.cWorkTables[6]='GRU_PRIO'
    this.cWorkTables[7]='ORI_NOMI'
    this.cWorkTables[8]='VALUTE'
    this.cWorkTables[9]='ZONE'
    this.cWorkTables[10]='AGENTI'
    this.cWorkTables[11]='UNIMIS'
    this.cWorkTables[12]='AZIENDA'
    this.cWorkTables[13]='TIP_DOCU'
    this.cWorkTables[14]='CPUSERS'
    this.cWorkTables[15]='CPGROUPS'
    this.cWorkTables[16]='KITMPROM'
    this.cWorkTables[17]='KIT_PROM'
    * --- Area Manuale = Open Work Table
    * --- gsof_mkp
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.DynamicBackColor= ;
         "IIF(t_KPARNOCO=1,RGB(255,255,0), RGB(255,255,255))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(17))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.KITMPROM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.KITMPROM_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_KPCODKIT = NVL(KPCODKIT,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_4_1_joined
    link_4_1_joined=.f.
    local link_4_3_joined
    link_4_3_joined=.f.
    local link_4_5_joined
    link_4_5_joined=.f.
    local link_4_6_joined
    link_4_6_joined=.f.
    local link_4_9_joined
    link_4_9_joined=.f.
    local link_4_11_joined
    link_4_11_joined=.f.
    local link_4_13_joined
    link_4_13_joined=.f.
    local link_4_15_joined
    link_4_15_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from KITMPROM where KPCODKIT=KeySet.KPCODKIT
    *
    i_nConn = i_TableProp[this.KITMPROM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2],this.bLoadRecFilter,this.KITMPROM_IDX,"gsof_mkp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('KITMPROM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "KITMPROM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"KIT_PROM.","KITMPROM.")
      i_cTable = i_cTable+' KITMPROM '
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_1_joined=this.AddJoinedLink_4_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_3_joined=this.AddJoinedLink_4_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_5_joined=this.AddJoinedLink_4_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_6_joined=this.AddJoinedLink_4_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_9_joined=this.AddJoinedLink_4_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_11_joined=this.AddJoinedLink_4_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_13_joined=this.AddJoinedLink_4_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_15_joined=this.AddJoinedLink_4_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'KPCODKIT',this.w_KPCODKIT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_NUMSCO = 0
        .w_DECTOT = 0
        .w_DECUNI = 0
        .w_KPDESVAL = space(35)
        .w_KPOBVAL = ctod("  /  /  ")
        .w_KPDESGRN = space(35)
        .w_KPDESORI = space(35)
        .w_KPDESZON = space(35)
        .w_KPOBZON = ctod("  /  /  ")
        .w_KPDESAGE = space(35)
        .w_KPOBAGE = ctod("  /  /  ")
        .w_KPDESPRI = space(35)
        .w_KPDESPRF = space(35)
        .w_KPDESOPE = space(35)
        .w_KPDESGRU = space(35)
        .w_KPTOTRIG = 0
        .w_BRES = .T.
        .w_CODAZI = i_codazi
          .link_1_1('Load')
        .w_KPCODKIT = NVL(KPCODKIT,space(15))
        .w_KPDESKIT = NVL(KPDESKIT,space(35))
        .w_KPCODVAL = NVL(KPCODVAL,space(3))
          if link_1_6_joined
            this.w_KPCODVAL = NVL(VACODVAL106,NVL(this.w_KPCODVAL,space(3)))
            this.w_KPDESVAL = NVL(VADESVAL106,space(35))
            this.w_KPOBVAL = NVL(cp_ToDate(VADTOBSO106),ctod("  /  /  "))
            this.w_DECTOT = NVL(VADECTOT106,0)
            this.w_DECUNI = NVL(VADECUNI106,0)
          else
          .link_1_6('Load')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_KP__NOTE = NVL(KP__NOTE,space(0))
        .w_KPDATOBS = NVL(cp_ToDate(KPDATOBS),ctod("  /  /  "))
        .w_KPCODUTE = NVL(KPCODUTE,0)
          if link_4_1_joined
            this.w_KPCODUTE = NVL(CODE401,NVL(this.w_KPCODUTE,0))
            this.w_KPDESOPE = NVL(NAME401,space(35))
          else
          .link_4_1('Load')
          endif
        .w_KPGRPUTE = NVL(KPGRPUTE,0)
          if link_4_3_joined
            this.w_KPGRPUTE = NVL(CODE403,NVL(this.w_KPGRPUTE,0))
            this.w_KPDESGRU = NVL(NAME403,space(35))
          else
          .link_4_3('Load')
          endif
        .w_KPCODPRN = NVL(KPCODPRN,space(5))
          if link_4_5_joined
            this.w_KPCODPRN = NVL(GPCODICE405,NVL(this.w_KPCODPRN,space(5)))
            this.w_KPDESPRI = NVL(GPDESCRI405,space(35))
          else
          .link_4_5('Load')
          endif
        .w_KPCODGRN = NVL(KPCODGRN,space(5))
          if link_4_6_joined
            this.w_KPCODGRN = NVL(GNCODICE406,NVL(this.w_KPCODGRN,space(5)))
            this.w_KPDESGRN = NVL(GNDESCRI406,space(35))
          else
          .link_4_6('Load')
          endif
        .w_KPCODORI = NVL(KPCODORI,space(5))
          if link_4_9_joined
            this.w_KPCODORI = NVL(ONCODICE409,NVL(this.w_KPCODORI,space(5)))
            this.w_KPDESORI = NVL(ONDESCRI409,space(35))
          else
          .link_4_9('Load')
          endif
        .w_KPCODZON = NVL(KPCODZON,space(3))
          if link_4_11_joined
            this.w_KPCODZON = NVL(ZOCODZON411,NVL(this.w_KPCODZON,space(3)))
            this.w_KPDESZON = NVL(ZODESZON411,space(35))
            this.w_KPOBZON = NVL(cp_ToDate(ZODTOBSO411),ctod("  /  /  "))
          else
          .link_4_11('Load')
          endif
        .w_KPCODPRF = NVL(KPCODPRF,space(5))
          if link_4_13_joined
            this.w_KPCODPRF = NVL(GPCODICE413,NVL(this.w_KPCODPRF,space(5)))
            this.w_KPDESPRF = NVL(GPDESCRI413,space(35))
          else
          .link_4_13('Load')
          endif
        .w_KPCODAGE = NVL(KPCODAGE,space(5))
          if link_4_15_joined
            this.w_KPCODAGE = NVL(AGCODAGE415,NVL(this.w_KPCODAGE,space(5)))
            this.w_KPDESAGE = NVL(AGDESAGE415,space(35))
            this.w_KPOBAGE = NVL(cp_ToDate(AGDTOBSO415),ctod("  /  /  "))
          else
          .link_4_15('Load')
          endif
        .w_KPDATVAL = NVL(cp_ToDate(KPDATVAL),ctod("  /  /  "))
        .w_OBTEST = i_datsys
        .w_CODI = .w_KPCODKIT
        .w_DESC = .w_KPDESKIT
        .oPgFrm.Page1.oPag.oObj_3_9.Calculate()
        .w_CODESC = Space(5)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'KITMPROM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from KIT_PROM where KPCODKIT=KeySet.KPCODKIT
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.KIT_PROM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.KIT_PROM_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('KIT_PROM')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "KIT_PROM.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" KIT_PROM"
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'KPCODKIT',this.w_KPCODKIT  )
        select * from (i_cTable) KIT_PROM where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_KPTOTRIG = 0
      scan
        with this
          .w_FLFRAZ = space(1)
          .w_UNMIS3 = space(3)
          .w_UNMIS1 = space(3)
          .w_FLSERG = space(1)
          .w_UNMIS2 = space(3)
          .w_MOLTI3 = 0
          .w_DTOBS1 = ctod("  /  /  ")
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_KPCODICE = NVL(KPCODICE,space(20))
          if link_2_2_joined
            this.w_KPCODICE = NVL(CACODICE202,NVL(this.w_KPCODICE,space(20)))
            this.w_KPDESART = NVL(CADESART202,space(40))
            this.w_UNMIS3 = NVL(CAUNIMIS202,space(3))
            this.w_MOLTI3 = NVL(CAMOLTIP202,0)
            this.w_KPTIPRIG = NVL(CA__TIPO202,space(1))
            this.w_KPCODART = NVL(CACODART202,space(20))
            this.w_DTOBS1 = NVL(cp_ToDate(CADTOBSO202),ctod("  /  /  "))
            this.w_KPNOTAGG = NVL(CADESSUP202,space(0))
          else
          .link_2_2('Load')
          endif
          .w_KPCODICE = NVL(KPCODICE,space(41))
          if link_2_3_joined
            this.w_KPCODICE = NVL(CACODICE203,NVL(this.w_KPCODICE,space(41)))
            this.w_KPDESART = NVL(CADESART203,space(40))
            this.w_UNMIS3 = NVL(CAUNIMIS203,space(3))
            this.w_MOLTI3 = NVL(CAMOLTIP203,0)
            this.w_KPTIPRIG = NVL(CA__TIPO203,space(1))
            this.w_KPCODART = NVL(CACODART203,space(20))
            this.w_DTOBS1 = NVL(cp_ToDate(CADTOBSO203),ctod("  /  /  "))
            this.w_KPNOTAGG = NVL(CADESSUP203,space(0))
          else
          .link_2_3('Load')
          endif
          .w_KPCODART = NVL(KPCODART,space(20))
          if link_2_4_joined
            this.w_KPCODART = NVL(ARCODART204,NVL(this.w_KPCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1204,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2204,space(3))
            this.w_FLSERG = NVL(ARFLSERG204,space(1))
            this.w_KPCODGRU = NVL(ARCODGRU204,space(5))
            this.w_KPCODSOT = NVL(ARCODSOT204,space(5))
          else
          .link_2_4('Load')
          endif
          .w_KPCODGRU = NVL(KPCODGRU,space(5))
          * evitabile
          *.link_2_5('Load')
          .w_KPCODSOT = NVL(KPCODSOT,space(5))
          * evitabile
          *.link_2_6('Load')
          .w_KPDESART = NVL(KPDESART,space(40))
          .w_KPUNIMIS = NVL(KPUNIMIS,space(3))
          if link_2_8_joined
            this.w_KPUNIMIS = NVL(UMCODICE208,NVL(this.w_KPUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ208,space(1))
          else
          .link_2_8('Load')
          endif
          .w_KPQTAMOV = NVL(KPQTAMOV,0)
          .w_KPPREZZO = NVL(KPPREZZO,0)
          .w_KPTIPRIG = NVL(KPTIPRIG,space(1))
          .w_KPSCONT1 = NVL(KPSCONT1,0)
          .w_KPSCONT2 = NVL(KPSCONT2,0)
          .w_KPSCONT3 = NVL(KPSCONT3,0)
          .w_KPSCONT4 = NVL(KPSCONT4,0)
        .w_VALUNI = cp_ROUND(.w_KPPREZZO * (1+.w_KPSCONT1/100)*(1+.w_KPSCONT2/100)*(1+.w_KPSCONT3/100)*(1+.w_KPSCONT4/100),5)
          .w_KPARNOCO = NVL(KPARNOCO,space(1))
        .w_KPNETRIG = cp_ROUND(.w_KPQTAMOV*.w_VALUNI, .w_DECTOT)
          .w_KPNOTAGG = NVL(KPNOTAGG,space(0))
          .w_KPFLOMAG = NVL(KPFLOMAG,space(1))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_27.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_KPTOTRIG = .w_KPTOTRIG+.w_KPNETRIG
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CODAZI = i_codazi
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_OBTEST = i_datsys
        .w_CODI = .w_KPCODKIT
        .w_DESC = .w_KPDESKIT
        .oPgFrm.Page1.oPag.oObj_3_9.Calculate()
        .w_CODESC = Space(5)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CODAZI=space(5)
      .w_NUMSCO=0
      .w_KPCODKIT=space(15)
      .w_KPDESKIT=space(35)
      .w_KPCODVAL=space(3)
      .w_DECTOT=0
      .w_DECUNI=0
      .w_CALCPICT=0
      .w_CALCPICU=0
      .w_KP__NOTE=space(0)
      .w_KPDATOBS=ctod("  /  /  ")
      .w_CPROWORD=10
      .w_KPCODICE=space(20)
      .w_KPCODICE=space(41)
      .w_KPCODART=space(20)
      .w_KPCODGRU=space(5)
      .w_KPCODSOT=space(5)
      .w_KPDESART=space(40)
      .w_KPUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_KPQTAMOV=0
      .w_KPCODUTE=0
      .w_KPGRPUTE=0
      .w_KPCODPRN=space(5)
      .w_KPCODGRN=space(5)
      .w_KPCODORI=space(5)
      .w_KPCODZON=space(3)
      .w_KPCODPRF=space(5)
      .w_KPCODAGE=space(5)
      .w_KPDATVAL=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_KPDESVAL=space(35)
      .w_KPOBVAL=ctod("  /  /  ")
      .w_KPPREZZO=0
      .w_KPDESGRN=space(35)
      .w_KPDESORI=space(35)
      .w_KPDESZON=space(35)
      .w_KPOBZON=ctod("  /  /  ")
      .w_KPDESAGE=space(35)
      .w_KPOBAGE=ctod("  /  /  ")
      .w_KPDESPRI=space(35)
      .w_KPDESPRF=space(35)
      .w_KPDESOPE=space(35)
      .w_KPDESGRU=space(35)
      .w_KPTIPRIG=space(1)
      .w_KPSCONT1=0
      .w_KPSCONT2=0
      .w_KPSCONT3=0
      .w_KPSCONT4=0
      .w_CODI=space(15)
      .w_DESC=space(35)
      .w_VALUNI=0
      .w_KPARNOCO=space(1)
      .w_KPNETRIG=0
      .w_UNMIS3=space(3)
      .w_UNMIS1=space(3)
      .w_FLSERG=space(1)
      .w_UNMIS2=space(3)
      .w_MOLTI3=0
      .w_KPTOTRIG=0
      .w_KPNOTAGG=space(0)
      .w_DTOBS1=ctod("  /  /  ")
      .w_KPFLOMAG=space(1)
      .w_BRES=.f.
      .w_CODESC=space(5)
      if .cFunction<>"Filter"
        .w_CODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,4,.f.)
        .w_KPCODVAL = g_PERVAL
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_KPCODVAL))
         .link_1_6('Full')
        endif
        .DoRTCalc(6,7,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .DoRTCalc(10,13,.f.)
        if not(empty(.w_KPCODICE))
         .link_2_2('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_KPCODICE))
         .link_2_3('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_KPCODART))
         .link_2_4('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_KPCODGRU))
         .link_2_5('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_KPCODSOT))
         .link_2_6('Full')
        endif
        .DoRTCalc(18,18,.f.)
        .w_KPUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_KPUNIMIS))
         .link_2_8('Full')
        endif
        .DoRTCalc(20,20,.f.)
        .w_KPQTAMOV = IIF(.w_KPTIPRIG='F', 1, IIF(.w_KPTIPRIG='D', 0, .w_KPQTAMOV))
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_KPCODUTE))
         .link_4_1('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_KPGRPUTE))
         .link_4_3('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_KPCODPRN))
         .link_4_5('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_KPCODGRN))
         .link_4_6('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_KPCODORI))
         .link_4_9('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_KPCODZON))
         .link_4_11('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_KPCODPRF))
         .link_4_13('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_KPCODAGE))
         .link_4_15('Full')
        endif
        .DoRTCalc(30,30,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(32,44,.f.)
        .w_KPTIPRIG = 'R'
        .DoRTCalc(46,46,.f.)
        .w_KPSCONT2 = 0
        .w_KPSCONT3 = 0
        .w_KPSCONT4 = 0
        .w_CODI = .w_KPCODKIT
        .w_DESC = .w_KPDESKIT
        .w_VALUNI = cp_ROUND(.w_KPPREZZO * (1+.w_KPSCONT1/100)*(1+.w_KPSCONT2/100)*(1+.w_KPSCONT3/100)*(1+.w_KPSCONT4/100),5)
        .w_KPARNOCO = ' '
        .w_KPNETRIG = cp_ROUND(.w_KPQTAMOV*.w_VALUNI, .w_DECTOT)
        .DoRTCalc(55,62,.f.)
        .w_KPFLOMAG = 'X'
        .oPgFrm.Page1.oPag.oObj_3_9.Calculate()
        .w_BRES = .T.
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_27.Calculate()
        .w_CODESC = Space(5)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'KITMPROM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oKPCODKIT_1_3.enabled = i_bVal
      .Page1.oPag.oKPDESKIT_1_5.enabled = i_bVal
      .Page1.oPag.oKPCODVAL_1_6.enabled = i_bVal
      .Page1.oPag.oKP__NOTE_1_11.enabled = i_bVal
      .Page1.oPag.oKPDATOBS_3_1.enabled = i_bVal
      .Page2.oPag.oKPCODUTE_4_1.enabled = i_bVal
      .Page2.oPag.oKPGRPUTE_4_3.enabled = i_bVal
      .Page2.oPag.oKPCODPRN_4_5.enabled = i_bVal
      .Page2.oPag.oKPCODGRN_4_6.enabled = i_bVal
      .Page2.oPag.oKPCODORI_4_9.enabled = i_bVal
      .Page2.oPag.oKPCODZON_4_11.enabled = i_bVal
      .Page2.oPag.oKPCODPRF_4_13.enabled = i_bVal
      .Page2.oPag.oKPCODAGE_4_15.enabled = i_bVal
      .Page1.oPag.oKPDATVAL_3_3.enabled = i_bVal
      .Page1.oPag.oKPSCONT1_2_13.enabled = i_bVal
      .Page1.oPag.oKPSCONT2_2_14.enabled = i_bVal
      .Page1.oPag.oKPSCONT3_2_15.enabled = i_bVal
      .Page1.oPag.oKPSCONT4_2_16.enabled = i_bVal
      .Page1.oPag.oKPARNOCO_2_18.enabled = i_bVal
      .Page1.oPag.oKPFLOMAG_2_26.enabled = i_bVal
      .Page1.oPag.oObj_3_9.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_27.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oKPCODKIT_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oKPCODKIT_1_3.enabled = .t.
        .Page1.oPag.oKPDESKIT_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'KITMPROM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.KITMPROM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPCODKIT,"KPCODKIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPDESKIT,"KPDESKIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPCODVAL,"KPCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KP__NOTE,"KP__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPDATOBS,"KPDATOBS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPCODUTE,"KPCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPGRPUTE,"KPGRPUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPCODPRN,"KPCODPRN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPCODGRN,"KPCODGRN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPCODORI,"KPCODORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPCODZON,"KPCODZON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPCODPRF,"KPCODPRF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPCODAGE,"KPCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_KPDATVAL,"KPDATVAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.KITMPROM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2])
    i_lTable = "KITMPROM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.KITMPROM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsof_skp with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_KPCODICE C(20);
      ,t_KPCODGRU C(5);
      ,t_KPCODSOT C(5);
      ,t_KPDESART C(40);
      ,t_KPUNIMIS C(3);
      ,t_KPQTAMOV N(12,3);
      ,t_KPPREZZO N(18,5);
      ,t_KPSCONT1 N(6,2);
      ,t_KPSCONT2 N(6,2);
      ,t_KPSCONT3 N(6,2);
      ,t_KPSCONT4 N(6,2);
      ,t_KPARNOCO N(3);
      ,t_KPNETRIG N(10,2);
      ,t_KPFLOMAG N(3);
      ,CPROWNUM N(10);
      ,t_KPCODART C(20);
      ,t_FLFRAZ C(1);
      ,t_KPTIPRIG C(1);
      ,t_VALUNI N(18,5);
      ,t_UNMIS3 C(3);
      ,t_UNMIS1 C(3);
      ,t_FLSERG C(1);
      ,t_UNMIS2 C(3);
      ,t_MOLTI3 N(10,4);
      ,t_KPNOTAGG M(10);
      ,t_DTOBS1 D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsof_mkpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_2.controlsource=this.cTrsName+'.t_KPCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_3.controlsource=this.cTrsName+'.t_KPCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODGRU_2_5.controlsource=this.cTrsName+'.t_KPCODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODSOT_2_6.controlsource=this.cTrsName+'.t_KPCODSOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oKPDESART_2_7.controlsource=this.cTrsName+'.t_KPDESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oKPUNIMIS_2_8.controlsource=this.cTrsName+'.t_KPUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oKPQTAMOV_2_10.controlsource=this.cTrsName+'.t_KPQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oKPPREZZO_2_11.controlsource=this.cTrsName+'.t_KPPREZZO'
    this.oPgFRm.Page1.oPag.oKPSCONT1_2_13.controlsource=this.cTrsName+'.t_KPSCONT1'
    this.oPgFRm.Page1.oPag.oKPSCONT2_2_14.controlsource=this.cTrsName+'.t_KPSCONT2'
    this.oPgFRm.Page1.oPag.oKPSCONT3_2_15.controlsource=this.cTrsName+'.t_KPSCONT3'
    this.oPgFRm.Page1.oPag.oKPSCONT4_2_16.controlsource=this.cTrsName+'.t_KPSCONT4'
    this.oPgFRm.Page1.oPag.oKPARNOCO_2_18.controlsource=this.cTrsName+'.t_KPARNOCO'
    this.oPgFRm.Page1.oPag.oKPNETRIG_2_19.controlsource=this.cTrsName+'.t_KPNETRIG'
    this.oPgFRm.Page1.oPag.oKPFLOMAG_2_26.controlsource=this.cTrsName+'.t_KPFLOMAG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(48)
    this.AddVLine(172)
    this.AddVLine(237)
    this.AddVLine(302)
    this.AddVLine(519)
    this.AddVLine(569)
    this.AddVLine(647)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.KITMPROM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into KITMPROM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'KITMPROM')
        i_extval=cp_InsertValODBCExtFlds(this,'KITMPROM')
        local i_cFld
        i_cFld=" "+;
                  "(KPCODKIT,KPDESKIT,KPCODVAL,KP__NOTE,KPDATOBS"+;
                  ",KPCODUTE,KPGRPUTE,KPCODPRN,KPCODGRN,KPCODORI"+;
                  ",KPCODZON,KPCODPRF,KPCODAGE,KPDATVAL"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_KPCODKIT)+;
                    ","+cp_ToStrODBC(this.w_KPDESKIT)+;
                    ","+cp_ToStrODBCNull(this.w_KPCODVAL)+;
                    ","+cp_ToStrODBC(this.w_KP__NOTE)+;
                    ","+cp_ToStrODBC(this.w_KPDATOBS)+;
                    ","+cp_ToStrODBCNull(this.w_KPCODUTE)+;
                    ","+cp_ToStrODBCNull(this.w_KPGRPUTE)+;
                    ","+cp_ToStrODBCNull(this.w_KPCODPRN)+;
                    ","+cp_ToStrODBCNull(this.w_KPCODGRN)+;
                    ","+cp_ToStrODBCNull(this.w_KPCODORI)+;
                    ","+cp_ToStrODBCNull(this.w_KPCODZON)+;
                    ","+cp_ToStrODBCNull(this.w_KPCODPRF)+;
                    ","+cp_ToStrODBCNull(this.w_KPCODAGE)+;
                    ","+cp_ToStrODBC(this.w_KPDATVAL)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'KITMPROM')
        i_extval=cp_InsertValVFPExtFlds(this,'KITMPROM')
        cp_CheckDeletedKey(i_cTable,0,'KPCODKIT',this.w_KPCODKIT)
        INSERT INTO (i_cTable);
              (KPCODKIT,KPDESKIT,KPCODVAL,KP__NOTE,KPDATOBS,KPCODUTE,KPGRPUTE,KPCODPRN,KPCODGRN,KPCODORI,KPCODZON,KPCODPRF,KPCODAGE,KPDATVAL &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_KPCODKIT;
                  ,this.w_KPDESKIT;
                  ,this.w_KPCODVAL;
                  ,this.w_KP__NOTE;
                  ,this.w_KPDATOBS;
                  ,this.w_KPCODUTE;
                  ,this.w_KPGRPUTE;
                  ,this.w_KPCODPRN;
                  ,this.w_KPCODGRN;
                  ,this.w_KPCODORI;
                  ,this.w_KPCODZON;
                  ,this.w_KPCODPRF;
                  ,this.w_KPCODAGE;
                  ,this.w_KPDATVAL;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.KIT_PROM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.KIT_PROM_IDX,2])
      *
      * insert into KIT_PROM
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(KPCODKIT,CPROWORD,KPCODICE,KPCODART,KPCODGRU"+;
                  ",KPCODSOT,KPDESART,KPUNIMIS,KPQTAMOV,KPPREZZO"+;
                  ",KPTIPRIG,KPSCONT1,KPSCONT2,KPSCONT3,KPSCONT4"+;
                  ",KPARNOCO,KPNOTAGG,KPFLOMAG,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_KPCODKIT)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_KPCODICE)+","+cp_ToStrODBCNull(this.w_KPCODART)+","+cp_ToStrODBCNull(this.w_KPCODGRU)+;
             ","+cp_ToStrODBCNull(this.w_KPCODSOT)+","+cp_ToStrODBC(this.w_KPDESART)+","+cp_ToStrODBCNull(this.w_KPUNIMIS)+","+cp_ToStrODBC(this.w_KPQTAMOV)+","+cp_ToStrODBC(this.w_KPPREZZO)+;
             ","+cp_ToStrODBC(this.w_KPTIPRIG)+","+cp_ToStrODBC(this.w_KPSCONT1)+","+cp_ToStrODBC(this.w_KPSCONT2)+","+cp_ToStrODBC(this.w_KPSCONT3)+","+cp_ToStrODBC(this.w_KPSCONT4)+;
             ","+cp_ToStrODBC(this.w_KPARNOCO)+","+cp_ToStrODBC(this.w_KPNOTAGG)+","+cp_ToStrODBC(this.w_KPFLOMAG)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'KPCODKIT',this.w_KPCODKIT)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_KPCODKIT,this.w_CPROWORD,this.w_KPCODICE,this.w_KPCODART,this.w_KPCODGRU"+;
                ",this.w_KPCODSOT,this.w_KPDESART,this.w_KPUNIMIS,this.w_KPQTAMOV,this.w_KPPREZZO"+;
                ",this.w_KPTIPRIG,this.w_KPSCONT1,this.w_KPSCONT2,this.w_KPSCONT3,this.w_KPSCONT4"+;
                ",this.w_KPARNOCO,this.w_KPNOTAGG,this.w_KPFLOMAG,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.KITMPROM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update KITMPROM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'KITMPROM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " KPDESKIT="+cp_ToStrODBC(this.w_KPDESKIT)+;
             ",KPCODVAL="+cp_ToStrODBCNull(this.w_KPCODVAL)+;
             ",KP__NOTE="+cp_ToStrODBC(this.w_KP__NOTE)+;
             ",KPDATOBS="+cp_ToStrODBC(this.w_KPDATOBS)+;
             ",KPCODUTE="+cp_ToStrODBCNull(this.w_KPCODUTE)+;
             ",KPGRPUTE="+cp_ToStrODBCNull(this.w_KPGRPUTE)+;
             ",KPCODPRN="+cp_ToStrODBCNull(this.w_KPCODPRN)+;
             ",KPCODGRN="+cp_ToStrODBCNull(this.w_KPCODGRN)+;
             ",KPCODORI="+cp_ToStrODBCNull(this.w_KPCODORI)+;
             ",KPCODZON="+cp_ToStrODBCNull(this.w_KPCODZON)+;
             ",KPCODPRF="+cp_ToStrODBCNull(this.w_KPCODPRF)+;
             ",KPCODAGE="+cp_ToStrODBCNull(this.w_KPCODAGE)+;
             ",KPDATVAL="+cp_ToStrODBC(this.w_KPDATVAL)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'KITMPROM')
          i_cWhere = cp_PKFox(i_cTable  ,'KPCODKIT',this.w_KPCODKIT  )
          UPDATE (i_cTable) SET;
              KPDESKIT=this.w_KPDESKIT;
             ,KPCODVAL=this.w_KPCODVAL;
             ,KP__NOTE=this.w_KP__NOTE;
             ,KPDATOBS=this.w_KPDATOBS;
             ,KPCODUTE=this.w_KPCODUTE;
             ,KPGRPUTE=this.w_KPGRPUTE;
             ,KPCODPRN=this.w_KPCODPRN;
             ,KPCODGRN=this.w_KPCODGRN;
             ,KPCODORI=this.w_KPCODORI;
             ,KPCODZON=this.w_KPCODZON;
             ,KPCODPRF=this.w_KPCODPRF;
             ,KPCODAGE=this.w_KPCODAGE;
             ,KPDATVAL=this.w_KPDATVAL;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not Empty(t_CPROWORD)  AND NOT EMPTY(t_KPCODICE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.KIT_PROM_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.KIT_PROM_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from KIT_PROM
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update KIT_PROM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",KPCODICE="+cp_ToStrODBCNull(this.w_KPCODICE)+;
                     ",KPCODART="+cp_ToStrODBCNull(this.w_KPCODART)+;
                     ",KPCODGRU="+cp_ToStrODBCNull(this.w_KPCODGRU)+;
                     ",KPCODSOT="+cp_ToStrODBCNull(this.w_KPCODSOT)+;
                     ",KPDESART="+cp_ToStrODBC(this.w_KPDESART)+;
                     ",KPUNIMIS="+cp_ToStrODBCNull(this.w_KPUNIMIS)+;
                     ",KPQTAMOV="+cp_ToStrODBC(this.w_KPQTAMOV)+;
                     ",KPPREZZO="+cp_ToStrODBC(this.w_KPPREZZO)+;
                     ",KPTIPRIG="+cp_ToStrODBC(this.w_KPTIPRIG)+;
                     ",KPSCONT1="+cp_ToStrODBC(this.w_KPSCONT1)+;
                     ",KPSCONT2="+cp_ToStrODBC(this.w_KPSCONT2)+;
                     ",KPSCONT3="+cp_ToStrODBC(this.w_KPSCONT3)+;
                     ",KPSCONT4="+cp_ToStrODBC(this.w_KPSCONT4)+;
                     ",KPARNOCO="+cp_ToStrODBC(this.w_KPARNOCO)+;
                     ",KPNOTAGG="+cp_ToStrODBC(this.w_KPNOTAGG)+;
                     ",KPFLOMAG="+cp_ToStrODBC(this.w_KPFLOMAG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,KPCODICE=this.w_KPCODICE;
                     ,KPCODART=this.w_KPCODART;
                     ,KPCODGRU=this.w_KPCODGRU;
                     ,KPCODSOT=this.w_KPCODSOT;
                     ,KPDESART=this.w_KPDESART;
                     ,KPUNIMIS=this.w_KPUNIMIS;
                     ,KPQTAMOV=this.w_KPQTAMOV;
                     ,KPPREZZO=this.w_KPPREZZO;
                     ,KPTIPRIG=this.w_KPTIPRIG;
                     ,KPSCONT1=this.w_KPSCONT1;
                     ,KPSCONT2=this.w_KPSCONT2;
                     ,KPSCONT3=this.w_KPSCONT3;
                     ,KPSCONT4=this.w_KPSCONT4;
                     ,KPARNOCO=this.w_KPARNOCO;
                     ,KPNOTAGG=this.w_KPNOTAGG;
                     ,KPFLOMAG=this.w_KPFLOMAG;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not Empty(t_CPROWORD)  AND NOT EMPTY(t_KPCODICE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.KIT_PROM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.KIT_PROM_IDX,2])
        *
        * delete KIT_PROM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.KITMPROM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2])
        *
        * delete KITMPROM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not Empty(t_CPROWORD)  AND NOT EMPTY(t_KPCODICE)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.KITMPROM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.KITMPROM_IDX,2])
    if i_bUpd
      with this
          .w_CODAZI = i_codazi
          .link_1_1('Full')
        .DoRTCalc(2,7,.t.)
        if .o_KPCODVAL<>.w_KPCODVAL
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_KPCODVAL<>.w_KPCODVAL
          .w_CALCPICU = DEFPIU(.w_DECUNI)
        endif
        .DoRTCalc(10,14,.t.)
        if .o_KPCODICE<>.w_KPCODICE
          .link_2_4('Full')
        endif
        if .o_KPCODICE<>.w_KPCODICE
          .link_2_5('Full')
        endif
        if .o_KPCODICE<>.w_KPCODICE.or. .o_KPCODGRU<>.w_KPCODGRU
          .link_2_6('Full')
        endif
        .DoRTCalc(18,18,.t.)
        if .o_KPCODICE<>.w_KPCODICE
          .w_KPUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_2_8('Full')
        endif
        .DoRTCalc(20,20,.t.)
        if .o_KPCODICE<>.w_KPCODICE
          .w_KPQTAMOV = IIF(.w_KPTIPRIG='F', 1, IIF(.w_KPTIPRIG='D', 0, .w_KPQTAMOV))
        endif
        .DoRTCalc(22,30,.t.)
          .w_OBTEST = i_datsys
        .DoRTCalc(32,46,.t.)
        if .o_KPSCONT1<>.w_KPSCONT1
          .w_KPSCONT2 = 0
        endif
        if .o_KPSCONT2<>.w_KPSCONT2
          .w_KPSCONT3 = 0
        endif
        if .o_KPSCONT3<>.w_KPSCONT3
          .w_KPSCONT4 = 0
        endif
          .w_CODI = .w_KPCODKIT
          .w_DESC = .w_KPDESKIT
          .w_VALUNI = cp_ROUND(.w_KPPREZZO * (1+.w_KPSCONT1/100)*(1+.w_KPSCONT2/100)*(1+.w_KPSCONT3/100)*(1+.w_KPSCONT4/100),5)
        if .o_KPCODICE<>.w_KPCODICE
          .w_KPARNOCO = ' '
        endif
        if .o_KPQTAMOV<>.w_KPQTAMOV.or. .o_VALUNI<>.w_VALUNI
          .w_KPTOTRIG = .w_KPTOTRIG-.w_kpnetrig
          .w_KPNETRIG = cp_ROUND(.w_KPQTAMOV*.w_VALUNI, .w_DECTOT)
          .w_KPTOTRIG = .w_KPTOTRIG+.w_kpnetrig
        endif
        .oPgFrm.Page1.oPag.oObj_3_9.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_27.Calculate()
        .DoRTCalc(55,64,.t.)
          .w_CODESC = Space(5)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_KPCODART with this.w_KPCODART
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_KPTIPRIG with this.w_KPTIPRIG
      replace t_VALUNI with this.w_VALUNI
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_FLSERG with this.w_FLSERG
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_KPNOTAGG with this.w_KPNOTAGG
      replace t_DTOBS1 with this.w_DTOBS1
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_3_9.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_27.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_27.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oKPCODUTE_4_1.enabled = this.oPgFrm.Page2.oPag.oKPCODUTE_4_1.mCond()
    this.oPgFrm.Page2.oPag.oKPGRPUTE_4_3.enabled = this.oPgFrm.Page2.oPag.oKPGRPUTE_4_3.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPCODICE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPCODICE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPCODICE_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPCODICE_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPCODSOT_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPCODSOT_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPDESART_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPDESART_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPUNIMIS_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPUNIMIS_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPQTAMOV_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPQTAMOV_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPPREZZO_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oKPPREZZO_2_11.mCond()
    this.oPgFrm.Page1.oPag.oKPSCONT1_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oKPSCONT1_2_13.mCond()
    this.oPgFrm.Page1.oPag.oKPSCONT2_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oKPSCONT2_2_14.mCond()
    this.oPgFrm.Page1.oPag.oKPSCONT3_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oKPSCONT3_2_15.mCond()
    this.oPgFrm.Page1.oPag.oKPSCONT4_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oKPSCONT4_2_16.mCond()
    this.oPgFrm.Page1.oPag.oKPARNOCO_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oKPARNOCO_2_18.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_2.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_3.mHide()
    this.oPgFrm.Page1.oPag.oKPSCONT2_2_14.visible=!this.oPgFrm.Page1.oPag.oKPSCONT2_2_14.mHide()
    this.oPgFrm.Page1.oPag.oKPSCONT3_2_15.visible=!this.oPgFrm.Page1.oPag.oKPSCONT3_2_15.mHide()
    this.oPgFrm.Page1.oPag.oKPSCONT4_2_16.visible=!this.oPgFrm.Page1.oPag.oKPSCONT4_2_16.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_3_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZNUMSCO";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZNUMSCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_NUMSCO = NVL(_Link_.AZNUMSCO,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_NUMSCO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=KPCODVAL
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_KPCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO,VADECTOT,VADECUNI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_KPCODVAL))
          select VACODVAL,VADESVAL,VADTOBSO,VADECTOT,VADECUNI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_KPCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO,VADECTOT,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_KPCODVAL)+"%");

            select VACODVAL,VADESVAL,VADTOBSO,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_KPCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oKPCODVAL_1_6'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO,VADECTOT,VADECUNI";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADTOBSO,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO,VADECTOT,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_KPCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_KPCODVAL)
            select VACODVAL,VADESVAL,VADTOBSO,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_KPDESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_KPOBVAL = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_KPCODVAL = space(3)
      endif
      this.w_KPDESVAL = space(35)
      this.w_KPOBVAL = ctod("  /  /  ")
      this.w_DECTOT = 0
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE",  CHKDTOBS(this, .w_KPOBVAL,.w_OBTEST,"Valuta obsoleta alla data Attuale!"),CHKDTOBS(.w_KPOBVAL,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_KPCODVAL = space(3)
        this.w_KPDESVAL = space(35)
        this.w_KPOBVAL = ctod("  /  /  ")
        this.w_DECTOT = 0
        this.w_DECUNI = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.VACODVAL as VACODVAL106"+ ",link_1_6.VADESVAL as VADESVAL106"+ ",link_1_6.VADTOBSO as VADTOBSO106"+ ",link_1_6.VADECTOT as VADECTOT106"+ ",link_1_6.VADECUNI as VADECUNI106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on KITMPROM.KPCODVAL=link_1_6.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and KITMPROM.KPCODVAL=link_1_6.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_KPCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_KPCODICE))
          select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_KPCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_KPCODICE)+"%");

            select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_KPCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oKPCODICE_2_2'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSVE_MDV.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_KPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_KPCODICE)
            select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_KPDESART = NVL(_Link_.CADESART,space(40))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_KPTIPRIG = NVL(_Link_.CA__TIPO,space(1))
      this.w_KPCODART = NVL(_Link_.CACODART,space(20))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_KPNOTAGG = NVL(_Link_.CADESSUP,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODICE = space(20)
      endif
      this.w_KPDESART = space(40)
      this.w_UNMIS3 = space(3)
      this.w_MOLTI3 = 0
      this.w_KPTIPRIG = space(1)
      this.w_KPCODART = space(20)
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_KPNOTAGG = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CAUNIMIS as CAUNIMIS202"+ ",link_2_2.CAMOLTIP as CAMOLTIP202"+ ",link_2_2.CA__TIPO as CA__TIPO202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CADTOBSO as CADTOBSO202"+ ",link_2_2.CADESSUP as CADESSUP202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on KIT_PROM.KPCODICE=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and KIT_PROM.KPCODICE=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODICE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_KPCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_KPCODICE))
          select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_KPCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_KPCODICE)+"%");

            select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_KPCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oKPCODICE_2_3'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSVE0MDV.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_KPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_KPCODICE)
            select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CA__TIPO,CACODART,CADTOBSO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODICE = NVL(_Link_.CACODICE,space(41))
      this.w_KPDESART = NVL(_Link_.CADESART,space(40))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_KPTIPRIG = NVL(_Link_.CA__TIPO,space(1))
      this.w_KPCODART = NVL(_Link_.CACODART,space(20))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_KPNOTAGG = NVL(_Link_.CADESSUP,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODICE = space(41)
      endif
      this.w_KPDESART = space(40)
      this.w_UNMIS3 = space(3)
      this.w_MOLTI3 = 0
      this.w_KPTIPRIG = space(1)
      this.w_KPCODART = space(20)
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_KPNOTAGG = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CACODICE as CACODICE203"+ ",link_2_3.CADESART as CADESART203"+ ",link_2_3.CAUNIMIS as CAUNIMIS203"+ ",link_2_3.CAMOLTIP as CAMOLTIP203"+ ",link_2_3.CA__TIPO as CA__TIPO203"+ ",link_2_3.CACODART as CACODART203"+ ",link_2_3.CADTOBSO as CADTOBSO203"+ ",link_2_3.CADESSUP as CADESSUP203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on KIT_PROM.KPCODICE=link_2_3.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and KIT_PROM.KPCODICE=link_2_3.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODART
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARCODGRU,ARCODSOT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_KPCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_KPCODART)
            select ARCODART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARCODGRU,ARCODSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_KPCODGRU = NVL(_Link_.ARCODGRU,space(5))
      this.w_KPCODSOT = NVL(_Link_.ARCODSOT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_KPCODGRU = space(5)
      this.w_KPCODSOT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.ARCODART as ARCODART204"+ ",link_2_4.ARUNMIS1 as ARUNMIS1204"+ ",link_2_4.ARUNMIS2 as ARUNMIS2204"+ ",link_2_4.ARFLSERG as ARFLSERG204"+ ",link_2_4.ARCODGRU as ARCODGRU204"+ ",link_2_4.ARCODSOT as ARCODSOT204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on KIT_PROM.KPCODART=link_2_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and KIT_PROM.KPCODART=link_2_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODGRU
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIT_SEZI_IDX,3]
    i_lTable = "TIT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2], .t., this.TIT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATS',True,'TIT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TSCODICE like "+cp_ToStrODBC(trim(this.w_KPCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select TSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TSCODICE',trim(this.w_KPCODGRU))
          select TSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODGRU)==trim(_Link_.TSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_KPCODGRU) and !this.bDontReportError
            deferred_cp_zoom('TIT_SEZI','*','TSCODICE',cp_AbsName(oSource.parent,'oKPCODGRU_2_5'),i_cWhere,'GSOF_ATS',"Codici gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',oSource.xKey(1))
            select TSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(this.w_KPCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',this.w_KPCODGRU)
            select TSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODGRU = NVL(_Link_.TSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODGRU = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.TSCODICE,1)
      cp_ShowWarn(i_cKey,this.TIT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=KPCODSOT
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_lTable = "SOT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2], .t., this.SOT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODSOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ASF',True,'SOT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SGCODSOT like "+cp_ToStrODBC(trim(this.w_KPCODSOT)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_KPCODGRU);

          i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SGCODGRU,SGCODSOT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SGCODGRU',this.w_KPCODGRU;
                     ,'SGCODSOT',trim(this.w_KPCODSOT))
          select SGCODGRU,SGCODSOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SGCODGRU,SGCODSOT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODSOT)==trim(_Link_.SGCODSOT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_KPCODSOT) and !this.bDontReportError
            deferred_cp_zoom('SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(oSource.parent,'oKPCODSOT_2_6'),i_cWhere,'GSOF_ASF',"Codici sottogruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_KPCODGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SGCODGRU,SGCODSOT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                     +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SGCODGRU="+cp_ToStrODBC(this.w_KPCODGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',oSource.xKey(1);
                       ,'SGCODSOT',oSource.xKey(2))
            select SGCODGRU,SGCODSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODSOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT";
                   +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(this.w_KPCODSOT);
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_KPCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',this.w_KPCODGRU;
                       ,'SGCODSOT',this.w_KPCODSOT)
            select SGCODGRU,SGCODSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODSOT = NVL(_Link_.SGCODSOT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODSOT = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.SGCODGRU,1)+'\'+cp_ToStr(_Link_.SGCODSOT,1)
      cp_ShowWarn(i_cKey,this.SOT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODSOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=KPUNIMIS
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_KPUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_KPUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_KPUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oKPUNIMIS_2_8'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_KPUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_KPUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_KPUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_KPUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_KPUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_KPUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.UMCODICE as UMCODICE208"+ ",link_2_8.UMFLFRAZ as UMFLFRAZ208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on KIT_PROM.KPUNIMIS=link_2_8.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and KIT_PROM.KPUNIMIS=link_2_8.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODUTE
  func Link_4_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_KPCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_KPCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_KPCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oKPCODUTE_4_1'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_KPCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_KPCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODUTE = NVL(_Link_.CODE,0)
      this.w_KPDESOPE = NVL(_Link_.NAME,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODUTE = 0
      endif
      this.w_KPDESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPUSERS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_1.CODE as CODE401"+ ",link_4_1.NAME as NAME401"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_1 on KITMPROM.KPCODUTE=link_4_1.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_1"
          i_cKey=i_cKey+'+" and KITMPROM.KPCODUTE=link_4_1.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPGRPUTE
  func Link_4_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPGRPUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_KPGRPUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_KPGRPUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_KPGRPUTE) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oKPGRPUTE_4_3'),i_cWhere,'',"Gruppi utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPGRPUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_KPGRPUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_KPGRPUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPGRPUTE = NVL(_Link_.CODE,0)
      this.w_KPDESGRU = NVL(_Link_.NAME,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_KPGRPUTE = 0
      endif
      this.w_KPDESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPGRPUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CPGROUPS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_3.CODE as CODE403"+ ",link_4_3.NAME as NAME403"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_3 on KITMPROM.KPGRPUTE=link_4_3.CODE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_3"
          i_cKey=i_cKey+'+" and KITMPROM.KPGRPUTE=link_4_3.CODE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODPRN
  func Link_4_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODPRN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AGP',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_KPCODPRN)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_KPCODPRN))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODPRN)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_KPCODPRN) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oKPCODPRN_4_5'),i_cWhere,'GSOF_AGP',"Gruppi priorit� nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODPRN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_KPCODPRN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_KPCODPRN)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODPRN = NVL(_Link_.GPCODICE,space(5))
      this.w_KPDESPRI = NVL(_Link_.GPDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODPRN = space(5)
      endif
      this.w_KPDESPRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODPRN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRU_PRIO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_5.GPCODICE as GPCODICE405"+ ",link_4_5.GPDESCRI as GPDESCRI405"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_5 on KITMPROM.KPCODPRN=link_4_5.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_5"
          i_cKey=i_cKey+'+" and KITMPROM.KPCODPRN=link_4_5.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODGRN
  func Link_4_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODGRN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGN',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_KPCODGRN)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_KPCODGRN))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODGRN)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_KPCODGRN) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oKPCODGRN_4_6'),i_cWhere,'GSAR_AGN',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODGRN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_KPCODGRN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_KPCODGRN)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODGRN = NVL(_Link_.GNCODICE,space(5))
      this.w_KPDESGRN = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODGRN = space(5)
      endif
      this.w_KPDESGRN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODGRN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRU_NOMI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_6.GNCODICE as GNCODICE406"+ ",link_4_6.GNDESCRI as GNDESCRI406"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_6 on KITMPROM.KPCODGRN=link_4_6.GNCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_6"
          i_cKey=i_cKey+'+" and KITMPROM.KPCODGRN=link_4_6.GNCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODORI
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AON',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_KPCODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_KPCODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_KPCODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oKPCODORI_4_9'),i_cWhere,'GSAR_AON',"Origine nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_KPCODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_KPCODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_KPDESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODORI = space(5)
      endif
      this.w_KPDESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ORI_NOMI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_9.ONCODICE as ONCODICE409"+ ",link_4_9.ONDESCRI as ONDESCRI409"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_9 on KITMPROM.KPCODORI=link_4_9.ONCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_9"
          i_cKey=i_cKey+'+" and KITMPROM.KPCODORI=link_4_9.ONCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODZON
  func Link_4_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_KPCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_KPCODZON))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_KPCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oKPCODZON_4_11'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_KPCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_KPCODZON)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_KPDESZON = NVL(_Link_.ZODESZON,space(35))
      this.w_KPOBZON = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODZON = space(3)
      endif
      this.w_KPDESZON = space(35)
      this.w_KPOBZON = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_KPOBZON,.w_OBTEST,"Zona obsoleta alla data Attuale!"),CHKDTOBS(.w_KPOBZON,.w_OBTEST,"Zona obsoleta alla data Attuale!", .T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_KPCODZON = space(3)
        this.w_KPDESZON = space(35)
        this.w_KPOBZON = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZONE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_11.ZOCODZON as ZOCODZON411"+ ",link_4_11.ZODESZON as ZODESZON411"+ ",link_4_11.ZODTOBSO as ZODTOBSO411"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_11 on KITMPROM.KPCODZON=link_4_11.ZOCODZON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_11"
          i_cKey=i_cKey+'+" and KITMPROM.KPCODZON=link_4_11.ZOCODZON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODPRF
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODPRF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AGP',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_KPCODPRF)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_KPCODPRF))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODPRF)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_KPCODPRF) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oKPCODPRF_4_13'),i_cWhere,'GSOF_AGP',"Gruppi priorit� offerte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODPRF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_KPCODPRF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_KPCODPRF)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODPRF = NVL(_Link_.GPCODICE,space(5))
      this.w_KPDESPRF = NVL(_Link_.GPDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODPRF = space(5)
      endif
      this.w_KPDESPRF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODPRF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRU_PRIO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_13.GPCODICE as GPCODICE413"+ ",link_4_13.GPDESCRI as GPDESCRI413"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_13 on KITMPROM.KPCODPRF=link_4_13.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_13"
          i_cKey=i_cKey+'+" and KITMPROM.KPCODPRF=link_4_13.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=KPCODAGE
  func Link_4_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_KPCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_KPCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_KPCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_KPCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_KPCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_KPCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_KPCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oKPCODAGE_4_15'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_KPCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_KPCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_KPCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_KPCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_KPDESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_KPOBAGE = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_KPCODAGE = space(5)
      endif
      this.w_KPDESAGE = space(35)
      this.w_KPOBAGE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_KPOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!"),CHKDTOBS(.w_KPOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_KPCODAGE = space(5)
        this.w_KPDESAGE = space(35)
        this.w_KPOBAGE = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_KPCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_15.AGCODAGE as AGCODAGE415"+ ",link_4_15.AGDESAGE as AGDESAGE415"+ ",link_4_15.AGDTOBSO as AGDTOBSO415"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_15 on KITMPROM.KPCODAGE=link_4_15.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_15"
          i_cKey=i_cKey+'+" and KITMPROM.KPCODAGE=link_4_15.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oKPCODKIT_1_3.value==this.w_KPCODKIT)
      this.oPgFrm.Page1.oPag.oKPCODKIT_1_3.value=this.w_KPCODKIT
    endif
    if not(this.oPgFrm.Page1.oPag.oKPDESKIT_1_5.value==this.w_KPDESKIT)
      this.oPgFrm.Page1.oPag.oKPDESKIT_1_5.value=this.w_KPDESKIT
    endif
    if not(this.oPgFrm.Page1.oPag.oKPCODVAL_1_6.value==this.w_KPCODVAL)
      this.oPgFrm.Page1.oPag.oKPCODVAL_1_6.value=this.w_KPCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oKP__NOTE_1_11.value==this.w_KP__NOTE)
      this.oPgFrm.Page1.oPag.oKP__NOTE_1_11.value=this.w_KP__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oKPDATOBS_3_1.value==this.w_KPDATOBS)
      this.oPgFrm.Page1.oPag.oKPDATOBS_3_1.value=this.w_KPDATOBS
    endif
    if not(this.oPgFrm.Page2.oPag.oKPCODUTE_4_1.value==this.w_KPCODUTE)
      this.oPgFrm.Page2.oPag.oKPCODUTE_4_1.value=this.w_KPCODUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oKPGRPUTE_4_3.value==this.w_KPGRPUTE)
      this.oPgFrm.Page2.oPag.oKPGRPUTE_4_3.value=this.w_KPGRPUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oKPCODPRN_4_5.value==this.w_KPCODPRN)
      this.oPgFrm.Page2.oPag.oKPCODPRN_4_5.value=this.w_KPCODPRN
    endif
    if not(this.oPgFrm.Page2.oPag.oKPCODGRN_4_6.value==this.w_KPCODGRN)
      this.oPgFrm.Page2.oPag.oKPCODGRN_4_6.value=this.w_KPCODGRN
    endif
    if not(this.oPgFrm.Page2.oPag.oKPCODORI_4_9.value==this.w_KPCODORI)
      this.oPgFrm.Page2.oPag.oKPCODORI_4_9.value=this.w_KPCODORI
    endif
    if not(this.oPgFrm.Page2.oPag.oKPCODZON_4_11.value==this.w_KPCODZON)
      this.oPgFrm.Page2.oPag.oKPCODZON_4_11.value=this.w_KPCODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oKPCODPRF_4_13.value==this.w_KPCODPRF)
      this.oPgFrm.Page2.oPag.oKPCODPRF_4_13.value=this.w_KPCODPRF
    endif
    if not(this.oPgFrm.Page2.oPag.oKPCODAGE_4_15.value==this.w_KPCODAGE)
      this.oPgFrm.Page2.oPag.oKPCODAGE_4_15.value=this.w_KPCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oKPDATVAL_3_3.value==this.w_KPDATVAL)
      this.oPgFrm.Page1.oPag.oKPDATVAL_3_3.value=this.w_KPDATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oKPDESVAL_1_14.value==this.w_KPDESVAL)
      this.oPgFrm.Page1.oPag.oKPDESVAL_1_14.value=this.w_KPDESVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oKPDESGRN_4_23.value==this.w_KPDESGRN)
      this.oPgFrm.Page2.oPag.oKPDESGRN_4_23.value=this.w_KPDESGRN
    endif
    if not(this.oPgFrm.Page2.oPag.oKPDESORI_4_24.value==this.w_KPDESORI)
      this.oPgFrm.Page2.oPag.oKPDESORI_4_24.value=this.w_KPDESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oKPDESZON_4_25.value==this.w_KPDESZON)
      this.oPgFrm.Page2.oPag.oKPDESZON_4_25.value=this.w_KPDESZON
    endif
    if not(this.oPgFrm.Page2.oPag.oKPDESAGE_4_26.value==this.w_KPDESAGE)
      this.oPgFrm.Page2.oPag.oKPDESAGE_4_26.value=this.w_KPDESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oKPDESPRI_4_27.value==this.w_KPDESPRI)
      this.oPgFrm.Page2.oPag.oKPDESPRI_4_27.value=this.w_KPDESPRI
    endif
    if not(this.oPgFrm.Page2.oPag.oKPDESPRF_4_28.value==this.w_KPDESPRF)
      this.oPgFrm.Page2.oPag.oKPDESPRF_4_28.value=this.w_KPDESPRF
    endif
    if not(this.oPgFrm.Page2.oPag.oKPDESOPE_4_29.value==this.w_KPDESOPE)
      this.oPgFrm.Page2.oPag.oKPDESOPE_4_29.value=this.w_KPDESOPE
    endif
    if not(this.oPgFrm.Page2.oPag.oKPDESGRU_4_30.value==this.w_KPDESGRU)
      this.oPgFrm.Page2.oPag.oKPDESGRU_4_30.value=this.w_KPDESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oKPSCONT1_2_13.value==this.w_KPSCONT1)
      this.oPgFrm.Page1.oPag.oKPSCONT1_2_13.value=this.w_KPSCONT1
      replace t_KPSCONT1 with this.oPgFrm.Page1.oPag.oKPSCONT1_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oKPSCONT2_2_14.value==this.w_KPSCONT2)
      this.oPgFrm.Page1.oPag.oKPSCONT2_2_14.value=this.w_KPSCONT2
      replace t_KPSCONT2 with this.oPgFrm.Page1.oPag.oKPSCONT2_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oKPSCONT3_2_15.value==this.w_KPSCONT3)
      this.oPgFrm.Page1.oPag.oKPSCONT3_2_15.value=this.w_KPSCONT3
      replace t_KPSCONT3 with this.oPgFrm.Page1.oPag.oKPSCONT3_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oKPSCONT4_2_16.value==this.w_KPSCONT4)
      this.oPgFrm.Page1.oPag.oKPSCONT4_2_16.value=this.w_KPSCONT4
      replace t_KPSCONT4 with this.oPgFrm.Page1.oPag.oKPSCONT4_2_16.value
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_4_31.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_4_31.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_4_32.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_4_32.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page1.oPag.oKPARNOCO_2_18.RadioValue()==this.w_KPARNOCO)
      this.oPgFrm.Page1.oPag.oKPARNOCO_2_18.SetRadio()
      replace t_KPARNOCO with this.oPgFrm.Page1.oPag.oKPARNOCO_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oKPNETRIG_2_19.value==this.w_KPNETRIG)
      this.oPgFrm.Page1.oPag.oKPNETRIG_2_19.value=this.w_KPNETRIG
      replace t_KPNETRIG with this.oPgFrm.Page1.oPag.oKPNETRIG_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oKPTOTRIG_3_8.value==this.w_KPTOTRIG)
      this.oPgFrm.Page1.oPag.oKPTOTRIG_3_8.value=this.w_KPTOTRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oKPFLOMAG_2_26.RadioValue()==this.w_KPFLOMAG)
      this.oPgFrm.Page1.oPag.oKPFLOMAG_2_26.SetRadio()
      replace t_KPFLOMAG with this.oPgFrm.Page1.oPag.oKPFLOMAG_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_2.value==this.w_KPCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_2.value=this.w_KPCODICE
      replace t_KPCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_3.value==this.w_KPCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_3.value=this.w_KPCODICE
      replace t_KPCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODICE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODGRU_2_5.value==this.w_KPCODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODGRU_2_5.value=this.w_KPCODGRU
      replace t_KPCODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODGRU_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODSOT_2_6.value==this.w_KPCODSOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODSOT_2_6.value=this.w_KPCODSOT
      replace t_KPCODSOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPCODSOT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPDESART_2_7.value==this.w_KPDESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPDESART_2_7.value=this.w_KPDESART
      replace t_KPDESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPDESART_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPUNIMIS_2_8.value==this.w_KPUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPUNIMIS_2_8.value=this.w_KPUNIMIS
      replace t_KPUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPUNIMIS_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPQTAMOV_2_10.value==this.w_KPQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPQTAMOV_2_10.value=this.w_KPQTAMOV
      replace t_KPQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPQTAMOV_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPPREZZO_2_11.value==this.w_KPPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPPREZZO_2_11.value=this.w_KPPREZZO
      replace t_KPPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPPREZZO_2_11.value
    endif
    cp_SetControlsValueExtFlds(this,'KITMPROM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_KPCODKIT))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oKPCODKIT_1_3.SetFocus()
            i_bnoObbl = !empty(.w_KPCODKIT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_KPCODVAL) or not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE",  CHKDTOBS(this, .w_KPOBVAL,.w_OBTEST,"Valuta obsoleta alla data Attuale!"),CHKDTOBS(.w_KPOBVAL,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oKPCODVAL_1_6.SetFocus()
            i_bnoObbl = !empty(.w_KPCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_KPOBZON,.w_OBTEST,"Zona obsoleta alla data Attuale!"),CHKDTOBS(.w_KPOBZON,.w_OBTEST,"Zona obsoleta alla data Attuale!", .T.)))  and not(empty(.w_KPCODZON))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oKPCODZON_4_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_KPOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!"),CHKDTOBS(.w_KPOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.)))  and not(empty(.w_KPCODAGE))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oKPCODAGE_4_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_mkp
      * --- Controlli ----
      if i_bRes=.t.
         .w_BRES=.t.
          ah_msg('Controlli Finali')
           .NotifyEvent('ControlliFinali')
           WAIT CLEAR
           if .w_BRES=.f.
              i_bRes=.f.
           endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_CPROWORD) and (not Empty(.w_CPROWORD)  AND NOT EMPTY(.w_KPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_KPUNIMIS) or not(CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_KPUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_KPUNIMIS))) and (.w_KPTIPRIG $ 'RM' AND NOT EMPTY(.w_KPCODICE) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S')) and (not Empty(.w_CPROWORD)  AND NOT EMPTY(.w_KPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPUNIMIS_2_8
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   not((.w_KPQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_KPQTAMOV=INT(.w_KPQTAMOV))) OR NOT .w_KPTIPRIG $ 'RM') and (.w_KPTIPRIG $ 'RMA' AND NOT EMPTY(.w_KPCODICE)) and (not Empty(.w_CPROWORD)  AND NOT EMPTY(.w_KPCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oKPQTAMOV_2_10
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
      endcase
      if not Empty(.w_CPROWORD)  AND NOT EMPTY(.w_KPCODICE)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_KPCODVAL = this.w_KPCODVAL
    this.o_KPCODICE = this.w_KPCODICE
    this.o_KPCODGRU = this.w_KPCODGRU
    this.o_KPQTAMOV = this.w_KPQTAMOV
    this.o_KPSCONT1 = this.w_KPSCONT1
    this.o_KPSCONT2 = this.w_KPSCONT2
    this.o_KPSCONT3 = this.w_KPSCONT3
    this.o_VALUNI = this.w_VALUNI
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not Empty(t_CPROWORD)  AND NOT EMPTY(t_KPCODICE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_KPCODICE=space(20)
      .w_KPCODICE=space(41)
      .w_KPCODART=space(20)
      .w_KPCODGRU=space(5)
      .w_KPCODSOT=space(5)
      .w_KPDESART=space(40)
      .w_KPUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_KPQTAMOV=0
      .w_KPPREZZO=0
      .w_KPTIPRIG=space(1)
      .w_KPSCONT1=0
      .w_KPSCONT2=0
      .w_KPSCONT3=0
      .w_KPSCONT4=0
      .w_VALUNI=0
      .w_KPARNOCO=space(1)
      .w_KPNETRIG=0
      .w_UNMIS3=space(3)
      .w_UNMIS1=space(3)
      .w_FLSERG=space(1)
      .w_UNMIS2=space(3)
      .w_MOLTI3=0
      .w_KPNOTAGG=space(0)
      .w_DTOBS1=ctod("  /  /  ")
      .w_KPFLOMAG=space(1)
      .DoRTCalc(1,13,.f.)
      if not(empty(.w_KPCODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(14,14,.f.)
      if not(empty(.w_KPCODICE))
        .link_2_3('Full')
      endif
      .DoRTCalc(15,15,.f.)
      if not(empty(.w_KPCODART))
        .link_2_4('Full')
      endif
      .DoRTCalc(16,16,.f.)
      if not(empty(.w_KPCODGRU))
        .link_2_5('Full')
      endif
      .DoRTCalc(17,17,.f.)
      if not(empty(.w_KPCODSOT))
        .link_2_6('Full')
      endif
      .DoRTCalc(18,18,.f.)
        .w_KPUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
      .DoRTCalc(19,19,.f.)
      if not(empty(.w_KPUNIMIS))
        .link_2_8('Full')
      endif
      .DoRTCalc(20,20,.f.)
        .w_KPQTAMOV = IIF(.w_KPTIPRIG='F', 1, IIF(.w_KPTIPRIG='D', 0, .w_KPQTAMOV))
      .DoRTCalc(22,44,.f.)
        .w_KPTIPRIG = 'R'
      .DoRTCalc(46,46,.f.)
        .w_KPSCONT2 = 0
        .w_KPSCONT3 = 0
        .w_KPSCONT4 = 0
      .DoRTCalc(50,51,.f.)
        .w_VALUNI = cp_ROUND(.w_KPPREZZO * (1+.w_KPSCONT1/100)*(1+.w_KPSCONT2/100)*(1+.w_KPSCONT3/100)*(1+.w_KPSCONT4/100),5)
        .w_KPARNOCO = ' '
        .w_KPNETRIG = cp_ROUND(.w_KPQTAMOV*.w_VALUNI, .w_DECTOT)
      .DoRTCalc(55,62,.f.)
        .w_KPFLOMAG = 'X'
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_27.Calculate()
    endwith
    this.DoRTCalc(64,65,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_KPCODICE = t_KPCODICE
    this.w_KPCODICE = t_KPCODICE
    this.w_KPCODART = t_KPCODART
    this.w_KPCODGRU = t_KPCODGRU
    this.w_KPCODSOT = t_KPCODSOT
    this.w_KPDESART = t_KPDESART
    this.w_KPUNIMIS = t_KPUNIMIS
    this.w_FLFRAZ = t_FLFRAZ
    this.w_KPQTAMOV = t_KPQTAMOV
    this.w_KPPREZZO = t_KPPREZZO
    this.w_KPTIPRIG = t_KPTIPRIG
    this.w_KPSCONT1 = t_KPSCONT1
    this.w_KPSCONT2 = t_KPSCONT2
    this.w_KPSCONT3 = t_KPSCONT3
    this.w_KPSCONT4 = t_KPSCONT4
    this.w_VALUNI = t_VALUNI
    this.w_KPARNOCO = this.oPgFrm.Page1.oPag.oKPARNOCO_2_18.RadioValue(.t.)
    this.w_KPNETRIG = t_KPNETRIG
    this.w_UNMIS3 = t_UNMIS3
    this.w_UNMIS1 = t_UNMIS1
    this.w_FLSERG = t_FLSERG
    this.w_UNMIS2 = t_UNMIS2
    this.w_MOLTI3 = t_MOLTI3
    this.w_KPNOTAGG = t_KPNOTAGG
    this.w_DTOBS1 = t_DTOBS1
    this.w_KPFLOMAG = this.oPgFrm.Page1.oPag.oKPFLOMAG_2_26.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_KPCODICE with this.w_KPCODICE
    replace t_KPCODICE with this.w_KPCODICE
    replace t_KPCODART with this.w_KPCODART
    replace t_KPCODGRU with this.w_KPCODGRU
    replace t_KPCODSOT with this.w_KPCODSOT
    replace t_KPDESART with this.w_KPDESART
    replace t_KPUNIMIS with this.w_KPUNIMIS
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_KPQTAMOV with this.w_KPQTAMOV
    replace t_KPPREZZO with this.w_KPPREZZO
    replace t_KPTIPRIG with this.w_KPTIPRIG
    replace t_KPSCONT1 with this.w_KPSCONT1
    replace t_KPSCONT2 with this.w_KPSCONT2
    replace t_KPSCONT3 with this.w_KPSCONT3
    replace t_KPSCONT4 with this.w_KPSCONT4
    replace t_VALUNI with this.w_VALUNI
    replace t_KPARNOCO with this.oPgFrm.Page1.oPag.oKPARNOCO_2_18.ToRadio()
    replace t_KPNETRIG with this.w_KPNETRIG
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_FLSERG with this.w_FLSERG
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_KPNOTAGG with this.w_KPNOTAGG
    replace t_DTOBS1 with this.w_DTOBS1
    replace t_KPFLOMAG with this.oPgFrm.Page1.oPag.oKPFLOMAG_2_26.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_KPTOTRIG = .w_KPTOTRIG-.w_kpnetrig
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsof_mkpPag1 as StdContainer
  Width  = 792
  height = 411
  stdWidth  = 792
  stdheight = 411
  resizeXpos=441
  resizeYpos=200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oKPCODKIT_1_3 as StdField with uid="EAIYKIGQHI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_KPCODKIT", cQueryName = "KPCODKIT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice kit",;
    HelpContextID = 29973498,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=108, Left=76, Top=14, InputMask=replicate('X',15)

  add object oKPDESKIT_1_5 as StdField with uid="HFCBSPYEMG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_KPDESKIT", cQueryName = "KPDESKIT",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione kit promozionale",;
    HelpContextID = 45050874,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=187, Top=14, InputMask=replicate('X',35)

  add object oKPCODVAL_1_6 as StdField with uid="LESBIDQJVW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_KPCODVAL", cQueryName = "KPCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta del kit promozionale",;
    HelpContextID = 53912590,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=548, Top=14, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_KPCODVAL"

  func oKPCODVAL_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODVAL_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oKPCODVAL_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oKPCODVAL_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oKPCODVAL_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_KPCODVAL
    i_obj.ecpSave()
  endproc

  add object oKP__NOTE_1_11 as StdMemo with uid="ETKRECVAZC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_KP__NOTE", cQueryName = "KP__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 108731371,;
   bGlobalFont=.t.,;
    Height=61, Width=558, Left=187, Top=39

  add object oKPDESVAL_1_14 as StdField with uid="HPOQMPPODE",rtseq=32,rtrep=.f.,;
    cFormVar = "w_KPDESVAL", cQueryName = "KPDESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione valuta",;
    HelpContextID = 38835214,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=598, Top=14, InputMask=replicate('X',35)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=112, width=785,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="CPROWORD",Label1="Riga",Field2="KPCODICE",Label2="",Field3="KPCODICE",Label3="Articolo",Field4="KPCODGRU",Label4="Gruppo",Field5="KPCODSOT",Label5="Sottogr.",Field6="KPDESART",Label6="Descrizione",Field7="KPUNIMIS",Label7="U.M.",Field8="KPQTAMOV",Label8="Qt�",Field9="KPPREZZO",Label9="Prezzo unitario",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 84738170

  add object oStr_1_4 as StdString with uid="UEZJGMTQJY",Visible=.t., Left=4, Top=14,;
    Alignment=1, Width=70, Height=18,;
    Caption="Codice kit:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="YRUGJGMDJW",Visible=.t., Left=483, Top=14,;
    Alignment=1, Width=62, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ZQSCISKXKA",Visible=.t., Left=297, Top=333,;
    Alignment=2, Width=15, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<2)
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="UJNMEIWEEQ",Visible=.t., Left=369, Top=333,;
    Alignment=2, Width=15, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<3)
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="VKJNOUCJHL",Visible=.t., Left=444, Top=333,;
    Alignment=2, Width=12, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<4)
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="ESFJOMEFVE",Visible=.t., Left=107, Top=39,;
    Alignment=1, Width=76, Height=15,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=131,;
    width=781+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=132,width=780+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|TIT_SEZI|SOT_SEZI|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oKPSCONT1_2_13.Refresh()
      this.Parent.oKPSCONT2_2_14.Refresh()
      this.Parent.oKPSCONT3_2_15.Refresh()
      this.Parent.oKPSCONT4_2_16.Refresh()
      this.Parent.oKPARNOCO_2_18.Refresh()
      this.Parent.oKPNETRIG_2_19.Refresh()
      this.Parent.oKPFLOMAG_2_26.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oKPCODICE_2_2
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oKPCODICE_2_3
      case cFile='TIT_SEZI'
        oDropInto=this.oBodyCol.oRow.oKPCODGRU_2_5
      case cFile='SOT_SEZI'
        oDropInto=this.oBodyCol.oRow.oKPCODSOT_2_6
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oKPUNIMIS_2_8
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oKPSCONT1_2_13 as StdTrsField with uid="EERLJAUVAE",rtseq=46,rtrep=.t.,;
    cFormVar="w_KPSCONT1",value=0,;
    ToolTipText = "1^% Maggiorazione (se positiva) o sconto (se negativa) di riga",;
    HelpContextID = 91118551,;
    cTotal="", bFixedPos=.t., cQueryName = "KPSCONT1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=241, Top=330, cSayPict=["999.99"], cGetPict=["999.99"]

  func oKPSCONT1_2_13.mCond()
    with this.Parent.oContained
      return (.w_NUMSCO>0 AND .w_KPTIPRIG $ 'RFMA')
    endwith
  endfunc

  add object oKPSCONT2_2_14 as StdTrsField with uid="JBSKLKRGLF",rtseq=47,rtrep=.t.,;
    cFormVar="w_KPSCONT2",value=0,;
    ToolTipText = "2^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 91118552,;
    cTotal="", bFixedPos=.t., cQueryName = "KPSCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=313, Top=330, cSayPict=["999.99"], cGetPict=["999.99"]

  func oKPSCONT2_2_14.mCond()
    with this.Parent.oContained
      return (.w_KPSCONT1<>0 AND .w_KPTIPRIG $ 'RFMA')
    endwith
  endfunc

  func oKPSCONT2_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO<2)
    endwith
    endif
  endfunc

  add object oKPSCONT3_2_15 as StdTrsField with uid="YXDOTPKMRI",rtseq=48,rtrep=.t.,;
    cFormVar="w_KPSCONT3",value=0,;
    ToolTipText = "3^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 91118553,;
    cTotal="", bFixedPos=.t., cQueryName = "KPSCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=385, Top=330, cSayPict=["999.99"], cGetPict=["999.99"]

  func oKPSCONT3_2_15.mCond()
    with this.Parent.oContained
      return (.w_KPSCONT2<>0 AND .w_KPTIPRIG $ 'RFMA')
    endwith
  endfunc

  func oKPSCONT3_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO<3)
    endwith
    endif
  endfunc

  add object oKPSCONT4_2_16 as StdTrsField with uid="XJHXZQYWPU",rtseq=49,rtrep=.t.,;
    cFormVar="w_KPSCONT4",value=0,;
    ToolTipText = "4^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 91118554,;
    cTotal="", bFixedPos=.t., cQueryName = "KPSCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=458, Top=330, cSayPict=["999.99"], cGetPict=["999.99"]

  func oKPSCONT4_2_16.mCond()
    with this.Parent.oContained
      return (.w_KPSCONT3<>0 AND .w_KPTIPRIG $ 'RFMA')
    endwith
  endfunc

  func oKPSCONT4_2_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO<4)
    endwith
    endif
  endfunc

  add object oKPARNOCO_2_18 as StdTrsCheck with uid="FRIROHWFNZ",rtrep=.t.,;
    cFormVar="w_KPARNOCO",  caption="Articolo da definire",;
    ToolTipText = "Se attivo: identifica una codifica generica a fronte di un articolo/servizio da ridefinire prima della conferma",;
    HelpContextID = 160678923,;
    Left=296, Top=356,;
    cTotal="", cQueryName = "KPARNOCO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oKPARNOCO_2_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..KPARNOCO,&i_cF..t_KPARNOCO),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oKPARNOCO_2_18.GetRadio()
    this.Parent.oContained.w_KPARNOCO = this.RadioValue()
    return .t.
  endfunc

  func oKPARNOCO_2_18.ToRadio()
    this.Parent.oContained.w_KPARNOCO=trim(this.Parent.oContained.w_KPARNOCO)
    return(;
      iif(this.Parent.oContained.w_KPARNOCO=='S',1,;
      0))
  endfunc

  func oKPARNOCO_2_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oKPARNOCO_2_18.mCond()
    with this.Parent.oContained
      return (.w_KPTIPRIG<>'D')
    endwith
  endfunc

  add object oKPNETRIG_2_19 as StdTrsField with uid="EVYNRNETFC",rtseq=54,rtrep=.t.,;
    cFormVar="w_KPNETRIG",value=0,enabled=.f.,;
    ToolTipText = "Importo totale di riga",;
    HelpContextID = 163580909,;
    cTotal="this.Parent.oContained.w_kptotrig", bFixedPos=.t., cQueryName = "KPNETRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=665, Top=330, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oKPFLOMAG_2_26 as StdTrsCombo with uid="TDEHLPHNTT",rtrep=.t.,;
    cFormVar="w_KPFLOMAG", RowSource=""+"Normale,"+"Sconto merce,"+"Omaggio imponibile,"+"Omaggio imp. + IVA" , ;
    ToolTipText = "Test riga omaggio/sconto merce o normale",;
    HelpContextID = 193557523,;
    Height=25, Width=132, Left=12, Top=330,;
    cTotal="", cQueryName = "KPFLOMAG",;
    bObbl = .f. , nPag=2  , tabstop=.f.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oKPFLOMAG_2_26.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..KPFLOMAG,&i_cF..t_KPFLOMAG),this.value)
    return(iif(xVal =1,'X',;
    iif(xVal =2,'S',;
    iif(xVal =3,'I',;
    iif(xVal =4,'E',;
    space(1))))))
  endfunc
  func oKPFLOMAG_2_26.GetRadio()
    this.Parent.oContained.w_KPFLOMAG = this.RadioValue()
    return .t.
  endfunc

  func oKPFLOMAG_2_26.ToRadio()
    this.Parent.oContained.w_KPFLOMAG=trim(this.Parent.oContained.w_KPFLOMAG)
    return(;
      iif(this.Parent.oContained.w_KPFLOMAG=='X',1,;
      iif(this.Parent.oContained.w_KPFLOMAG=='S',2,;
      iif(this.Parent.oContained.w_KPFLOMAG=='I',3,;
      iif(this.Parent.oContained.w_KPFLOMAG=='E',4,;
      0)))))
  endfunc

  func oKPFLOMAG_2_26.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oKPDATOBS_3_1 as StdField with uid="VQAXUUJRYS",rtseq=11,rtrep=.f.,;
    cFormVar="w_KPDATOBS",value=ctod("  /  /  "),;
    HelpContextID = 155489287,;
    cQueryName = "KPDATOBS",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=710, Top=384

  add object oKPDATVAL_3_3 as StdField with uid="RZPAMRQOOY",rtseq=30,rtrep=.f.,;
    cFormVar="w_KPDATVAL",value=ctod("  /  /  "),;
    HelpContextID = 38048782,;
    cQueryName = "KPDATVAL",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=485, Top=384

  add object oKPTOTRIG_3_8 as StdField with uid="MDQOHOCHYU",rtseq=60,rtrep=.f.,;
    cFormVar="w_KPTOTRIG",value=0,enabled=.f.,;
    ToolTipText = "Totale importi di riga (gi� scontati)",;
    HelpContextID = 164260845,;
    cTotal="", bFixedPos=.t., cQueryName = "KPTOTRIG",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=665, Top=356, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oObj_3_9 as cp_runprogram with uid="WGVPEPEUEE",width=203,height=19,;
   left=32, top=428,;
    caption='GSOF_BKK',;
   bGlobalFont=.t.,;
    prg="GSOF_BKK",;
    cEvent = "ControlliFinali",;
    nPag=3;
    , HelpContextID = 175185585

  add object oStr_3_2 as StdString with uid="OQJFVYTVQM",Visible=.t., Left=569, Top=384,;
    Alignment=1, Width=138, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="ENAWOVSKKF",Visible=.t., Left=309, Top=384,;
    Alignment=1, Width=171, Height=18,;
    Caption="Data inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="OZUQSIILVP",Visible=.t., Left=145, Top=330,;
    Alignment=1, Width=93, Height=18,;
    Caption="Sconti/magg.ni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="JPHGYPZWOC",Visible=.t., Left=572, Top=330,;
    Alignment=1, Width=89, Height=18,;
    Caption="Netto riga:"  ;
  , bGlobalFont=.t.

  add object oStr_3_7 as StdString with uid="NNKKGTUFHT",Visible=.t., Left=572, Top=356,;
    Alignment=1, Width=89, Height=18,;
    Caption="Totale righe:"  ;
  , bGlobalFont=.t.
enddefine

  define class tgsof_mkpPag2 as StdContainer
    Width  = 792
    height = 411
    stdWidth  = 792
    stdheight = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oKPCODUTE_4_1 as StdField with uid="NCEIPHPRRC",rtseq=22,rtrep=.f.,;
    cFormVar = "w_KPCODUTE", cQueryName = "KPCODUTE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 197745643,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=126, Top=73, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_KPCODUTE"

  func oKPCODUTE_4_1.mCond()
    with this.Parent.oContained
      return (.w_KPGRPUTE = 0)
    endwith
  endfunc

  func oKPCODUTE_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODUTE_4_1.ecpDrop(oSource)
    this.Parent.oContained.link_4_1('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oKPCODUTE_4_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oKPCODUTE_4_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
  endproc

  add object oKPGRPUTE_4_3 as StdField with uid="FWILTGHXUS",rtseq=23,rtrep=.f.,;
    cFormVar = "w_KPGRPUTE", cQueryName = "KPGRPUTE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Gruppo utente",;
    HelpContextID = 210541547,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=126, Top=99, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_KPGRPUTE"

  func oKPGRPUTE_4_3.mCond()
    with this.Parent.oContained
      return (.w_KPCODUTE = 0)
    endwith
  endfunc

  func oKPGRPUTE_4_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPGRPUTE_4_3.ecpDrop(oSource)
    this.Parent.oContained.link_4_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oKPGRPUTE_4_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oKPGRPUTE_4_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi utenti",'',this.parent.oContained
  endproc

  add object oKPCODPRN_4_5 as StdField with uid="IJXMNWVXEL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_KPCODPRN", cQueryName = "KPCODPRN",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo priorit� nominativo",;
    HelpContextID = 154575884,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", cZoomOnZoom="GSOF_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_KPCODPRN"

  func oKPCODPRN_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODPRN_4_5.ecpDrop(oSource)
    this.Parent.oContained.link_4_5('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oKPCODPRN_4_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oKPCODPRN_4_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AGP',"Gruppi priorit� nominativi",'',this.parent.oContained
  endproc
  proc oKPCODPRN_4_5.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_KPCODPRN
    i_obj.ecpSave()
  endproc

  add object oKPCODGRN_4_6 as StdField with uid="FTQGFVPSCX",rtseq=25,rtrep=.f.,;
    cFormVar = "w_KPCODGRN", cQueryName = "KPCODGRN",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo nominativo",;
    HelpContextID = 37135372,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=183, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", cZoomOnZoom="GSAR_AGN", oKey_1_1="GNCODICE", oKey_1_2="this.w_KPCODGRN"

  func oKPCODGRN_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODGRN_4_6.ecpDrop(oSource)
    this.Parent.oContained.link_4_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oKPCODGRN_4_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oKPCODGRN_4_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGN',"Gruppi nominativi",'',this.parent.oContained
  endproc
  proc oKPCODGRN_4_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GNCODICE=this.parent.oContained.w_KPCODGRN
    i_obj.ecpSave()
  endproc

  add object oKPCODORI_4_9 as StdField with uid="FVCOZITZEC",rtseq=26,rtrep=.f.,;
    cFormVar = "w_KPCODORI", cQueryName = "KPCODORI",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine reperimento nominitavo",;
    HelpContextID = 171353105,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=209, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", cZoomOnZoom="GSAR_AON", oKey_1_1="ONCODICE", oKey_1_2="this.w_KPCODORI"

  func oKPCODORI_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODORI_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oKPCODORI_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oKPCODORI_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AON',"Origine nominativi",'',this.parent.oContained
  endproc
  proc oKPCODORI_4_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AON()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ONCODICE=this.parent.oContained.w_KPCODORI
    i_obj.ecpSave()
  endproc

  add object oKPCODZON_4_11 as StdField with uid="YRGAMSRPBV",rtseq=27,rtrep=.f.,;
    cFormVar = "w_KPCODZON", cQueryName = "KPCODZON",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della zona di appartenenza",;
    HelpContextID = 255239180,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=126, Top=235, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_KPCODZON"

  func oKPCODZON_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODZON_4_11.ecpDrop(oSource)
    this.Parent.oContained.link_4_11('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oKPCODZON_4_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oKPCODZON_4_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oKPCODZON_4_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_KPCODZON
    i_obj.ecpSave()
  endproc

  add object oKPCODPRF_4_13 as StdField with uid="YIWXBFCLFY",rtseq=28,rtrep=.f.,;
    cFormVar = "w_KPCODPRF", cQueryName = "KPCODPRF",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo priorit� offerta",;
    HelpContextID = 154575892,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=291, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", cZoomOnZoom="GSOF_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_KPCODPRF"

  func oKPCODPRF_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODPRF_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oKPCODPRF_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oKPCODPRF_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AGP',"Gruppi priorit� offerte",'',this.parent.oContained
  endproc
  proc oKPCODPRF_4_13.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_KPCODPRF
    i_obj.ecpSave()
  endproc

  add object oKPCODAGE_4_15 as StdField with uid="BTQOMIAUAE",rtseq=29,rtrep=.f.,;
    cFormVar = "w_KPCODAGE", cQueryName = "KPCODAGE",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente che cura i rapporti",;
    HelpContextID = 130636779,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=317, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_KPCODAGE"

  func oKPCODAGE_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODAGE_4_15.ecpDrop(oSource)
    this.Parent.oContained.link_4_15('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oKPCODAGE_4_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oKPCODAGE_4_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oKPCODAGE_4_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_KPCODAGE
    i_obj.ecpSave()
  endproc

  add object oKPDESGRN_4_23 as StdField with uid="ZDFIAPXNHI",rtseq=35,rtrep=.f.,;
    cFormVar = "w_KPDESGRN", cQueryName = "KPDESGRN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 22057996,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=183, InputMask=replicate('X',35)

  add object oKPDESORI_4_24 as StdField with uid="DBRXMDLKUV",rtseq=36,rtrep=.f.,;
    cFormVar = "w_KPDESORI", cQueryName = "KPDESORI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 156275729,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=209, InputMask=replicate('X',35)

  add object oKPDESZON_4_25 as StdField with uid="KHSKOXECMS",rtseq=37,rtrep=.f.,;
    cFormVar = "w_KPDESZON", cQueryName = "KPDESZON",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 240161804,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=235, InputMask=replicate('X',35)

  add object oKPDESAGE_4_26 as StdField with uid="PBRMQMWVLG",rtseq=39,rtrep=.f.,;
    cFormVar = "w_KPDESAGE", cQueryName = "KPDESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 145714155,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=317, InputMask=replicate('X',35)

  add object oKPDESPRI_4_27 as StdField with uid="XSBOJINEQG",rtseq=41,rtrep=.f.,;
    cFormVar = "w_KPDESPRI", cQueryName = "KPDESPRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 139498513,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=157, InputMask=replicate('X',35)

  add object oKPDESPRF_4_28 as StdField with uid="VWHJSPDQZB",rtseq=42,rtrep=.f.,;
    cFormVar = "w_KPDESPRF", cQueryName = "KPDESPRF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 139498516,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=291, InputMask=replicate('X',35)

  add object oKPDESOPE_4_29 as StdField with uid="XSGAGCXVPS",rtseq=43,rtrep=.f.,;
    cFormVar = "w_KPDESOPE", cQueryName = "KPDESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 156275733,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=73, InputMask=replicate('X',35)

  add object oKPDESGRU_4_30 as StdField with uid="KHHEGYVYVD",rtseq=44,rtrep=.f.,;
    cFormVar = "w_KPDESGRU", cQueryName = "KPDESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 22057989,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=99, InputMask=replicate('X',35)

  add object oCODI_4_31 as StdField with uid="CZBJFZWTOP",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 42166822,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=108, Left=77, Top=13, InputMask=replicate('X',15)

  add object oDESC_4_32 as StdField with uid="MEGHGNNFOM",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 41832502,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=188, Top=13, InputMask=replicate('X',35)

  add object oStr_4_2 as StdString with uid="BRKHLZUUFZ",Visible=.t., Left=4, Top=73,;
    Alignment=1, Width=120, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_4 as StdString with uid="FDATKATDSC",Visible=.t., Left=4, Top=99,;
    Alignment=1, Width=120, Height=18,;
    Caption="Gruppo utente:"  ;
  , bGlobalFont=.t.

  add object oStr_4_7 as StdString with uid="TMHLZTJCWQ",Visible=.t., Left=4, Top=183,;
    Alignment=1, Width=120, Height=18,;
    Caption="Gruppo nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="HPJYOKHGWH",Visible=.t., Left=4, Top=157,;
    Alignment=1, Width=120, Height=18,;
    Caption="Gruppo priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="ZCLAHVQKOY",Visible=.t., Left=4, Top=209,;
    Alignment=1, Width=120, Height=18,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="CRBZMPJOTB",Visible=.t., Left=4, Top=235,;
    Alignment=1, Width=120, Height=18,;
    Caption="Codice zona:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="JBOIWHOGTQ",Visible=.t., Left=4, Top=291,;
    Alignment=1, Width=120, Height=18,;
    Caption="Gruppo priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="WPLLDOYFGG",Visible=.t., Left=4, Top=317,;
    Alignment=1, Width=120, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.

  add object oStr_4_18 as StdString with uid="KQVHVGXDHA",Visible=.t., Left=10, Top=48,;
    Alignment=0, Width=115, Height=18,;
    Caption="Privilegi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_20 as StdString with uid="GMRGZLDPMX",Visible=.t., Left=10, Top=134,;
    Alignment=0, Width=157, Height=18,;
    Caption="Nominativo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_22 as StdString with uid="OWEJAQJREU",Visible=.t., Left=10, Top=268,;
    Alignment=0, Width=122, Height=18,;
    Caption="Offerta"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_33 as StdString with uid="YGAOFWYEYB",Visible=.t., Left=4, Top=13,;
    Alignment=1, Width=70, Height=18,;
    Caption="Codice kit:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_4_17 as StdBox with uid="JVTWWWDYJB",left=10, top=65, width=554,height=2

  add object oBox_4_19 as StdBox with uid="ZBACQGTTFB",left=10, top=150, width=554,height=2

  add object oBox_4_21 as StdBox with uid="TTHXEPHJLN",left=10, top=284, width=554,height=2
enddefine

* --- Defining Body row
define class tgsof_mkpBodyRow as CPBodyRowCnt
  Width=771
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="LUKMPRNIRM",rtseq=12,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 151368854,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], BackStyle=0

  add object oKPCODICE_2_2 as StdTrsField with uid="DRNAUIADUU",rtseq=13,rtrep=.t.,;
    cFormVar="w_KPCODICE",value=space(20),;
    ToolTipText = "Codice di ricerca articolo",;
    HelpContextID = 3580949,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente o inesistente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=122, Left=41, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , BackStyle=0, cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_KPCODICE"

  func oKPCODICE_2_2.mCond()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  func oKPCODICE_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
    endif
  endfunc

  func oKPCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oKPCODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oKPCODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSVE_MDV.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oKPCODICE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_KPCODICE
    i_obj.ecpSave()
  endproc

  add object oKPCODICE_2_3 as StdTrsField with uid="NHACEDALJU",rtseq=14,rtrep=.t.,;
    cFormVar="w_KPCODICE",value=space(41),;
    ToolTipText = "Codice di ricerca articolo",;
    HelpContextID = 3580949,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente o inesistente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=122, Left=41, Top=0, InputMask=replicate('X',41), bHasZoom = .t. , BackStyle=0, cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_KPCODICE"

  func oKPCODICE_2_3.mCond()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)="AD HOC ENTERPRISE")
    endwith
  endfunc

  func oKPCODICE_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>"AD HOC ENTERPRISE")
    endwith
    endif
  endfunc

  func oKPCODICE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODICE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oKPCODICE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oKPCODICE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSVE0MDV.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oKPCODICE_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_KPCODICE
    i_obj.ecpSave()
  endproc

  add object oKPCODGRU_2_5 as StdTrsField with uid="KKRNCNRQYC",rtseq=16,rtrep=.t.,;
    cFormVar="w_KPCODGRU",value=space(5),;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 37135365,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=166, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , BackStyle=0, cLinkFile="TIT_SEZI", cZoomOnZoom="GSOF_ATS", oKey_1_1="TSCODICE", oKey_1_2="this.w_KPCODGRU"

  func oKPCODGRU_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
      if .not. empty(.w_KPCODSOT)
        bRes2=.link_2_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oKPCODGRU_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oKPCODGRU_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIT_SEZI','*','TSCODICE',cp_AbsName(this.parent,'oKPCODGRU_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATS',"Codici gruppi",'',this.parent.oContained
  endproc
  proc oKPCODGRU_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TSCODICE=this.parent.oContained.w_KPCODGRU
    i_obj.ecpSave()
  endproc

  add object oKPCODSOT_2_6 as StdTrsField with uid="RWMOCYSJSP",rtseq=17,rtrep=.t.,;
    cFormVar="w_KPCODSOT",value=space(5),;
    ToolTipText = "Codice sottogruppo",;
    HelpContextID = 104244230,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=231, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , BackStyle=0, cLinkFile="SOT_SEZI", cZoomOnZoom="GSOF_ASF", oKey_1_1="SGCODGRU", oKey_1_2="this.w_KPCODGRU", oKey_2_1="SGCODSOT", oKey_2_2="this.w_KPCODSOT"

  func oKPCODSOT_2_6.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_KPCODGRU))
    endwith
  endfunc

  func oKPCODSOT_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPCODSOT_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oKPCODSOT_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SOT_SEZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStrODBC(this.Parent.oContained.w_KPCODGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStr(this.Parent.oContained.w_KPCODGRU)
    endif
    do cp_zoom with 'SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(this.parent,'oKPCODSOT_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ASF',"Codici sottogruppi",'',this.parent.oContained
  endproc
  proc oKPCODSOT_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ASF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SGCODGRU=w_KPCODGRU
     i_obj.w_SGCODSOT=this.parent.oContained.w_KPCODSOT
    i_obj.ecpSave()
  endproc

  add object oKPDESART_2_7 as StdTrsField with uid="HVKYNXTWKP",rtseq=18,rtrep=.t.,;
    cFormVar="w_KPDESART",value=space(40),;
    ToolTipText = "Descrizione articolo (F9 = accede alle descrizioni aggiuntive)",;
    HelpContextID = 122721286,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=214, Left=296, Top=-1, InputMask=replicate('X',40), bHasZoom = .t. , BackStyle=0

  func oKPDESART_2_7.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_KPCODICE))
    endwith
  endfunc

  proc oKPDESART_2_7.mZoom
    do GSOF_KDS with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oKPUNIMIS_2_8 as StdTrsField with uid="UWQLATYQEX",rtseq=19,rtrep=.t.,;
    cFormVar="w_KPUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 68779001,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=512, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , BackStyle=0, cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_KPUNIMIS"

  func oKPUNIMIS_2_8.mCond()
    with this.Parent.oContained
      return (.w_KPTIPRIG $ 'RM' AND NOT EMPTY(.w_KPCODICE) AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))
    endwith
  endfunc

  func oKPUNIMIS_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oKPUNIMIS_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oKPUNIMIS_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oKPUNIMIS_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oKPUNIMIS_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_KPUNIMIS
    i_obj.ecpSave()
  endproc

  add object oKPQTAMOV_2_10 as StdTrsField with uid="UQNINGSJPD",rtseq=21,rtrep=.t.,;
    cFormVar="w_KPQTAMOV",value=0,;
    ToolTipText = "Quantit� movimentata",;
    HelpContextID = 207668228,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=562, Top=0, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)], BackStyle=0

  func oKPQTAMOV_2_10.mCond()
    with this.Parent.oContained
      return (.w_KPTIPRIG $ 'RMA' AND NOT EMPTY(.w_KPCODICE))
    endwith
  endfunc

  func oKPQTAMOV_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_KPQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_KPQTAMOV=INT(.w_KPQTAMOV))) OR NOT .w_KPTIPRIG $ 'RM')
    endwith
    return bRes
  endfunc

  add object oKPPREZZO_2_11 as StdTrsField with uid="OJMCCQFYSV",rtseq=34,rtrep=.t.,;
    cFormVar="w_KPPREZZO",value=0,;
    ToolTipText = "Prezzo",;
    HelpContextID = 14494709,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=641, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)], BackStyle=0,width=117

  func oKPPREZZO_2_11.mCond()
    with this.Parent.oContained
      return (.w_KPTIPRIG $ 'RFMA' AND NOT EMPTY(.w_KPCODICE))
    endwith
  endfunc

  add object oObj_2_27 as cp_runprogram with uid="HEPRBPHAHP",width=220,height=19,;
   left=255, top=296,;
    caption='GSOF_BKP',;
   bGlobalFont=.t.,;
    prg="GSOF_BKP",;
    cEvent = "w_KPCODICE Changed",;
    nPag=2;
    , HelpContextID = 175185590
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_mkp','KITMPROM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".KPCODKIT=KITMPROM.KPCODKIT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
