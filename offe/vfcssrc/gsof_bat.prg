* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bat                                                        *
*              Agenda operatore                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-26                                                      *
* Last revis.: 2012-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bat",oParentObject,m.pOPER)
return(i_retval)

define class tgsof_bat as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_CODAZI = space(5)
  w_CAUATT = space(20)
  * --- WorkFile variables
  PAR_OFFE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Agenda Operatore (da GSOF_KAT)
    * --- Parametri pOPER
    *     V :  Visualizza
    *     M: Modifica
    *     N: Nuovo
    *     R: Ricerca
    if this.oParentObject.cfunction<>"Load" 
      if this.pOPER = "R"
        * --- Aggiorno zoom
        if this.oParentObject.cFunction="Query"
          this.oParentObject.w_OGGETTO = "%"+ALLTRIM(this.oParentObject.w_SUBJECT)
          this.oParentObject.w_STATO = IIF(UPPER(this.oParentObject.class)="TGSOF_AOF",IIF(g_AGEN<>"S", this.oParentObject.w_STAT1,this.oParentObject.w_STAT2),this.oParentObject.w_STATO)
          this.oParentObject.w_ATDATINI = cp_CharToDatetime( IIF(NOT EMPTY(this.oParentObject.w_DATAINI), DTOC(this.oParentObject.w_DATAINI)+" "+this.oParentObject.w_ORAINI+":"+this.oParentObject.w_MININI+":00", "  -  -       :  :  "))
          this.oParentObject.w_ATDATFIN1 = cp_CharToDatetime( IIF(NOT EMPTY(this.oParentObject.w_DATAFIN), DTOC(this.oParentObject.w_DATAFIN)+" "+this.oParentObject.w_ORAFIN+":"+this.oParentObject.w_MINFIN+":00", "  -  -       :  :  "))
          this.oParentObject.w_PRIOFIN = this.oParentObject.w_PRIOINI
        endif
        this.oParentObject.NotifyEvent("Ricerca")
      else
        * --- Instanzio la maschera
        if g_AGEN="S"
          * --- Gestione attivit� dal modulo attivit�
          this.w_OBJECT = GSAG_AAT()
        else
          * --- Gestione attivit� dal modulo offerte
          this.w_OBJECT = GSOF_AAT()
        endif
        * --- Controllo se ha passato il test di accesso
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
      endif
      do case
        case this.pOPER $ "VM"
          if !empty(this.oParentObject.w_ATSERIAL)
            * --- Carico il record selezionato
            this.w_OBJECT.ECPFILTER()     
            this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_ATSERIAL
            this.w_OBJECT.ECPSAVE()     
            if this.pOPER = "M"
              * --- Vado in modifica
              this.w_OBJECT.ECPEDIT()     
            endif
          endif
        case this.pOPER = "N"
          * --- Vado in caricamento
          this.w_OBJECT.ECPLOAD()     
          if g_AGEN="S"
            * --- Valorizzo tipo attivit� dai parametri offerte
            this.w_CODAZI = i_CODAZI
            * --- Read from PAR_OFFE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "POTIPATT"+;
                " from "+i_cTable+" PAR_OFFE where ";
                    +"POCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                POTIPATT;
                from (i_cTable) where;
                    POCODAZI = this.w_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CAUATT = NVL(cp_ToDate(_read_.POTIPATT),cp_NullValue(_read_.POTIPATT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_OBJECT.w_ATCAUATT = this.w_CAUATT
            this.w_OBJ = this.w_OBJECT.GetCtrl("w_ATCAUATT")
            this.w_OBJ.bUpd = .t.
            this.w_OBJECT.SetControlsValue()     
            this.w_OBJ.Valid()     
          endif
          * --- Se lanciato dai nominativi valorizzo subito il campo nominativo
          if Type("This.oParentObject.w_NOCODICE") = "C"
            this.w_OBJECT.w_ATCODNOM = this.oParentObject.w_NOCODICE
            this.w_OBJ = this.w_OBJECT.GetCtrl("w_ATCODNOM")
            this.w_OBJ.bUpd = .t.
            this.w_OBJECT.SetControlsValue()     
            this.w_OBJ.Valid()     
          endif
          * --- Se lanciato da offerte valorizzo i campi offerta e nominativo
          if Type("This.oParentObject.w_OFSERIAL") = "C"
            this.w_OBJECT.w_ATCODNOM = this.oParentObject.w_OFCODNOM
            this.w_OBJ = this.w_OBJECT.GetCtrl("w_ATCODNOM")
            this.w_OBJ.bUpd = .t.
            this.w_OBJECT.SetControlsValue()     
            this.w_OBJ.Valid()     
            this.w_OBJECT.w_ATOPPOFF = this.oParentObject.w_OFSERIAL
            this.w_OBJ = this.w_OBJECT.GetCtrl("w_ATOPPOFF")
            this.w_OBJ.bUpd = .t.
            this.w_OBJECT.SetControlsValue()     
            this.w_OBJ.Valid()     
            if Isalt()
              this.w_OBJECT.w_ATCODPRA = This.oParentObject.w_OFCODPRA
              this.w_OBJ = this.w_OBJECT.GetCtrl("w_ATCODPRA")
              this.w_OBJ.bUpd = .t.
              this.w_OBJECT.SetControlsValue()     
              this.w_OBJ.Valid()     
            endif
            this.w_OBJ = Null
          endif
      endcase
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_OFFE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
