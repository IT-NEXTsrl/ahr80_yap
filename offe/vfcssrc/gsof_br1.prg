* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_br1                                                        *
*              Cancella sez. offerta/attributi                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_40]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-04                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_br1",oParentObject,m.pOPE)
return(i_retval)

define class tgsof_br1 as StdBatch
  * --- Local variables
  pOPE = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sbianca gli attributi successivi a quello corrente - da GSOF_KRO
    * --- Codici attributo
    * --- Descrizione attributi
    * --- Codice categoria
    * --- Sezioni offerta
    * --- Scelta chiamata 0 - ROCODAT0, 1 - ROCODAT1 ....
    do case
      case this.pOPE="0"
        if EMPTY(this.oParentObject.w_ROCODAT0)
          * --- Cancellazione codici attributo
          this.oParentObject.w_ROCODAT1 = " "
          this.oParentObject.w_ROCODAT2 = " "
          this.oParentObject.w_ROCODAT3 = " "
          this.oParentObject.w_ROCODAT4 = " "
          this.oParentObject.w_ROCODAT5 = " "
          this.oParentObject.w_ROCODAT6 = " "
          this.oParentObject.w_ROCODAT7 = " "
          this.oParentObject.w_ROCODAT8 = " "
          this.oParentObject.w_ROCODAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT1 = " "
          this.oParentObject.w_DESATT2 = " "
          this.oParentObject.w_DESATT3 = " "
          this.oParentObject.w_DESATT4 = " "
          this.oParentObject.w_DESATT5 = " "
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codice categoria
          this.oParentObject.w_TIPCAT1 = " "
          this.oParentObject.w_TIPCAT2 = " "
          this.oParentObject.w_TIPCAT3 = " "
          this.oParentObject.w_TIPCAT4 = " "
          this.oParentObject.w_TIPCAT5 = " "
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="1"
        if EMPTY(this.oParentObject.w_ROCODAT1)
          * --- Cancellazione codici attributo
          this.oParentObject.w_ROCODAT2 = " "
          this.oParentObject.w_ROCODAT3 = " "
          this.oParentObject.w_ROCODAT4 = " "
          this.oParentObject.w_ROCODAT5 = " "
          this.oParentObject.w_ROCODAT6 = " "
          this.oParentObject.w_ROCODAT7 = " "
          this.oParentObject.w_ROCODAT8 = " "
          this.oParentObject.w_ROCODAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT2 = " "
          this.oParentObject.w_DESATT3 = " "
          this.oParentObject.w_DESATT4 = " "
          this.oParentObject.w_DESATT5 = " "
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codice categoria
          this.oParentObject.w_TIPCAT2 = " "
          this.oParentObject.w_TIPCAT3 = " "
          this.oParentObject.w_TIPCAT4 = " "
          this.oParentObject.w_TIPCAT5 = " "
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="2"
        if EMPTY(this.oParentObject.w_ROCODAT2)
          * --- Cancellazione codici attributo
          this.oParentObject.w_ROCODAT3 = " "
          this.oParentObject.w_ROCODAT4 = " "
          this.oParentObject.w_ROCODAT5 = " "
          this.oParentObject.w_ROCODAT6 = " "
          this.oParentObject.w_ROCODAT7 = " "
          this.oParentObject.w_ROCODAT8 = " "
          this.oParentObject.w_ROCODAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT3 = " "
          this.oParentObject.w_DESATT4 = " "
          this.oParentObject.w_DESATT5 = " "
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codice categoria
          this.oParentObject.w_TIPCAT3 = " "
          this.oParentObject.w_TIPCAT4 = " "
          this.oParentObject.w_TIPCAT5 = " "
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="3"
        if EMPTY(this.oParentObject.w_ROCODAT3)
          * --- Cancellazione codici attributo
          this.oParentObject.w_ROCODAT4 = " "
          this.oParentObject.w_ROCODAT5 = " "
          this.oParentObject.w_ROCODAT6 = " "
          this.oParentObject.w_ROCODAT7 = " "
          this.oParentObject.w_ROCODAT8 = " "
          this.oParentObject.w_ROCODAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT4 = " "
          this.oParentObject.w_DESATT5 = " "
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codice categoria
          this.oParentObject.w_TIPCAT4 = " "
          this.oParentObject.w_TIPCAT5 = " "
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="4"
        if EMPTY(this.oParentObject.w_ROCODAT4)
          * --- Cancellazione codici attributo
          this.oParentObject.w_ROCODAT5 = " "
          this.oParentObject.w_ROCODAT6 = " "
          this.oParentObject.w_ROCODAT7 = " "
          this.oParentObject.w_ROCODAT8 = " "
          this.oParentObject.w_ROCODAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT5 = " "
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codice categoria
          this.oParentObject.w_TIPCAT5 = " "
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="5" 
 
        if EMPTY(this.oParentObject.w_ROCODAT5)
          * --- Cancellazione codici attributo
          this.oParentObject.w_ROCODAT6 = " "
          this.oParentObject.w_ROCODAT7 = " "
          this.oParentObject.w_ROCODAT8 = " "
          this.oParentObject.w_ROCODAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT6 = " "
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codice categoria
          this.oParentObject.w_TIPCAT6 = " "
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="6"
        if EMPTY(this.oParentObject.w_ROCODAT6)
          * --- Cancellazione codici attributo
          this.oParentObject.w_ROCODAT7 = " "
          this.oParentObject.w_ROCODAT8 = " "
          this.oParentObject.w_ROCODAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT7 = " "
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codice categoria
          this.oParentObject.w_TIPCAT7 = " "
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="7"
        if EMPTY(this.oParentObject.w_ROCODAT7)
          * --- Cancellazione codici attributo
          this.oParentObject.w_ROCODAT8 = " "
          this.oParentObject.w_ROCODAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT8 = " "
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codice categoria
          this.oParentObject.w_TIPCAT8 = " "
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="8"
        if EMPTY(this.oParentObject.w_ROCODAT8)
          * --- Cancellazione codici attributo
          this.oParentObject.w_ROCODAT9 = " "
          * --- Cancellazione descrizione attributi
          this.oParentObject.w_DESATT9 = " "
          * --- Cancellazione codice categoria
          this.oParentObject.w_TIPCAT9 = " "
        endif
      case this.pOPE="A"
        if EMPTY(this.oParentObject.w_ROCODSO0)
          this.oParentObject.w_ROCODSO1 = " "
          this.oParentObject.w_DESSEZ1 = " "
          this.oParentObject.w_ROCODSO2 = " "
          this.oParentObject.w_DESSEZ2 = " "
        endif
      case this.pOPE="B"
        if EMPTY(this.oParentObject.w_ROCODSO1)
          this.oParentObject.w_ROCODSO2 = " "
          this.oParentObject.w_DESSEZ2 = " "
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPE)
    this.pOPE=pOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPE"
endproc
