* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bvn                                                        *
*              Nuova versione offerta                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-04                                                      *
* Last revis.: 2012-07-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bvn",oParentObject)
return(i_retval)

define class tgsof_bvn as StdBatch
  * --- Local variables
  w_OK = .f.
  w_OKGEN = .f.
  w_NUMVER = 0
  w_SERIAL = space(10)
  w_SERIALE = space(10)
  w_NUMDOC = 0
  w_CODESE = space(4)
  w_SERDOC = space(2)
  w_CODNOM = space(20)
  w_CODMOD = space(5)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_OSCODSEZ = space(10)
  w_OS__NOTE = space(0)
  w_PROG = .NULL.
  w_ODSERIAL = space(10)
  w_CPROWNUM1 = 0
  w_CPROWORD1 = 0
  w_ODCODGRU = space(5)
  w_ODCODSOT = space(5)
  w_ODCODICE = space(20)
  w_ODCODART = space(20)
  w_ODDESART = space(40)
  w_ODNOTAGG = space(0)
  w_ODUNIMIS = space(3)
  w_ODQTAMOV = 0
  w_ODQTAUM1 = 0
  w_ODPREZZO = 0
  w_ODFLOMAG = space(1)
  w_ODSCONT1 = 0
  w_ODSCONT2 = 0
  w_ODSCONT3 = 0
  w_ODSCONT4 = 0
  w_ODTIPRIG = space(1)
  w_ODARNOCO = space(1)
  w_ODVALRIG = 0
  w_CODCAT = space(10)
  w_CODATT = space(10)
  w_ALSERIAL = space(10)
  w_ALDATREG = ctod("  /  /  ")
  w_ALTIPALL = space(1)
  w_ALPATFIL = space(254)
  w_ALOGGALL = space(50)
  w_AL__NOTE = space(0)
  w_ALTIPORI = space(1)
  w_ALRIFNOM = space(15)
  w_ALRIFOFF = space(10)
  w_ALRIFMOD = space(5)
  w_ALDATVAL = ctod("  /  /  ")
  w_ALDATOBS = ctod("  /  /  ")
  * --- WorkFile variables
  OFF_ERTE_idx=0
  OFF_SEZI_idx=0
  OFF_DETT_idx=0
  ALL_EGAT_idx=0
  ALL_ATTR_idx=0
  OFF_ATTR_idx=0
  TMPSEZOF_idx=0
  TMPATTOF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia  maschera offerte per inserimento nuova versione (da GSOF_AOF)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_OK = .T.
    * --- In caso di versione chiusa chiedo conferma
    if this.oParentObject.w_OFSTATUS = "V"
      this.w_OKGEN = iif(Isalt(),AH_YesNo("Generazione nuova versione preventivo. Confermi?"),AH_YesNo("Generazione nuova versione offerta. Confermi?"))
      if Not(this.w_OKGEN)
        this.oParentObject.w_OFSTATUS = this.oParentObject.o_OFSTATUS
        this.w_OK = .F.
      endif
    endif
    do case
      case this.oParentObject.w_OFSTATUS = "A"
        * --- Se Offerta inviata imposto data invio = datsys
        this.oParentObject.w_OFDATINV = i_datsys
      case this.oParentObject.w_OFSTATUS = "I"
        * --- Se Offerta in Corso svuoto data invio 
        this.oParentObject.w_OFDATINV = cp_CharToDate("  -  -  ")
    endcase
    * --- In caso di offerta Confermata,Chiusa,Rifiutata,Sospesa imposto data chiusura = datsys
    this.oParentObject.w_OFDATCHI = cp_CharToDate("  -  -  ")
    if this.oParentObject.w_OFSTATUS $ "VCSR"
      this.oParentObject.w_OFDATCHI = i_datsys
    endif
    * --- In caso di offerta Chiusa creo nuova versione
    if this.w_OK=.T. AND this.oParentObject.w_OFSTATUS = "V"
      this.w_NUMVER = this.oParentObject.w_OFNUMVER + 1
      * --- Legge il Primo Progressivo Offerta (autonumber)
      this.w_SERIAL = "0000000000"
      this.w_NUMDOC = 0
      this.w_CODESE = this.oParentObject.w_OFCODESE
      this.w_SERDOC = this.oParentObject.w_OFSERDOC
      this.w_CODNOM = this.oParentObject.w_OFCODNOM
      this.w_CODMOD = this.oParentObject.w_OFCODMOD
      this.oParentObject.w_OFDATDOC = i_DATSYS
      this.oParentObject.w_OFDATSCA = i_DATSYS+this.oParentObject.w_OFGIOSCA
      this.oParentObject.w_OFPATPDF = SPACE(254)
      this.oParentObject.w_OFPATFWP = SPACE(254)
      i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
      this.w_SERIAL = SPACE (10)
      * --- Try
      local bErr_033796B0
      bErr_033796B0=bTrsErr
      this.Try_033796B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        AH_ErrorMsg("Errore inserimento nuova offerta%0%1",48,"",MESSAGE() )
        this.w_OK = .F.
      endif
      bTrsErr=bTrsErr or bErr_033796B0
      * --- End
      if this.w_OK=.T.
        this.w_PROG = iif(Isalt(),GSPR_AOF(),GSOF_AOF())
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_OFSERIAL = this.w_SERIAL
        this.w_PROG.ecpSave()     
        this.w_PROG.ecpEdit()     
      endif
    endif
  endproc
  proc Try_033796B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorna il progressivo offerta
    * --- begin transaction
    cp_BeginTrs()
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    cp_NextTableProg(this,i_nConn,"SEOFFERT","i_codazi,w_SERIAL")
    * --- Scrive testata nuova versione offerta
    * --- Insert into OFF_ERTE
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_ERTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OFSERIAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OFF_ERTE','OFSERIAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OFSERIAL',this.w_SERIAL)
      insert into (i_cTable) (OFSERIAL &i_ccchkf. );
         values (;
           this.w_SERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore Inserimento Nuova Offerta'
      return
    endif
    * --- Write into OFF_ERTE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OFNUMVER ="+cp_NullLink(cp_ToStrODBC(this.w_NUMVER),'OFF_ERTE','OFNUMVER');
      +",OFNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFNUMDOC),'OFF_ERTE','OFNUMDOC');
      +",OFDATDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFDATDOC),'OFF_ERTE','OFDATDOC');
      +",OFCODNOM ="+cp_NullLink(cp_ToStrODBC(this.w_CODNOM),'OFF_ERTE','OFCODNOM');
      +",OFSTATUS ="+cp_NullLink(cp_ToStrODBC("I"),'OFF_ERTE','OFSTATUS');
      +",OFCODMOD ="+cp_NullLink(cp_ToStrODBC(this.w_CODMOD),'OFF_ERTE','OFCODMOD');
      +",OFSERDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSERDOC),'OFF_ERTE','OFSERDOC');
      +",OFCODESE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODESE),'OFF_ERTE','OFCODESE');
      +",OFRIFDES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFRIFDES),'OFF_ERTE','OFRIFDES');
      +",OFDATSCA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFDATSCA),'OFF_ERTE','OFDATSCA');
      +",OFPATPDF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFPATPDF),'OFF_ERTE','OFPATPDF');
      +",OFCODAGE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODAGE),'OFF_ERTE','OFCODAGE');
      +",OFSPETRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSPETRA),'OFF_ERTE','OFSPETRA');
      +",OFCODAG2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODAG2),'OFF_ERTE','OFCODAG2');
      +",OFSCONTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSCONTI),'OFF_ERTE','OFSCONTI');
      +",OFIMPOFF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFIMPOFF),'OFF_ERTE','OFIMPOFF');
      +",OFSCOPAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSCOPAG),'OFF_ERTE','OFSCOPAG');
      +",OFSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSCOCL1),'OFF_ERTE','OFSCOCL1');
      +",OFSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSCOCL2),'OFF_ERTE','OFSCOCL2');
      +",OFCODPAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODPAG),'OFF_ERTE','OFCODPAG');
      +",OFFLFOSC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFFLFOSC),'OFF_ERTE','OFFLFOSC');
      +",OF__NOTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OF__NOTE),'OFF_ERTE','OF__NOTE');
      +",OFSPEINC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSPEINC),'OFF_ERTE','OFSPEINC');
      +",OFCODLIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODLIS),'OFF_ERTE','OFCODLIS');
      +",OFSPEIMB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSPEIMB),'OFF_ERTE','OFSPEIMB');
      +",OFCODUTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODUTE),'OFF_ERTE','OFCODUTE');
      +",OFCODSPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODSPE),'OFF_ERTE','OFCODSPE');
      +",OFCODCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODCON),'OFF_ERTE','OFCODCON');
      +",OFNUMPRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFNUMPRI),'OFF_ERTE','OFNUMPRI');
      +",OFPATFWP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFPATFWP),'OFF_ERTE','OFPATFWP');
      +",OFSERPRE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSERIAL),'OFF_ERTE','OFSERPRE');
      +",OFCODVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODVAL),'OFF_ERTE','OFCODVAL');
      +",OFCODPOR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODPOR),'OFF_ERTE','OFCODPOR');
      +",OFDATRIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFDATRIC),'OFF_ERTE','OFDATRIC');
      +",OFDATAPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFDATAPE),'OFF_ERTE','OFDATAPE');
          +i_ccchkf ;
      +" where ";
          +"OFSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          OFNUMVER = this.w_NUMVER;
          ,OFNUMDOC = this.oParentObject.w_OFNUMDOC;
          ,OFDATDOC = this.oParentObject.w_OFDATDOC;
          ,OFCODNOM = this.w_CODNOM;
          ,OFSTATUS = "I";
          ,OFCODMOD = this.w_CODMOD;
          ,OFSERDOC = this.oParentObject.w_OFSERDOC;
          ,OFCODESE = this.oParentObject.w_OFCODESE;
          ,OFRIFDES = this.oParentObject.w_OFRIFDES;
          ,OFDATSCA = this.oParentObject.w_OFDATSCA;
          ,OFPATPDF = this.oParentObject.w_OFPATPDF;
          ,OFCODAGE = this.oParentObject.w_OFCODAGE;
          ,OFSPETRA = this.oParentObject.w_OFSPETRA;
          ,OFCODAG2 = this.oParentObject.w_OFCODAG2;
          ,OFSCONTI = this.oParentObject.w_OFSCONTI;
          ,OFIMPOFF = this.oParentObject.w_OFIMPOFF;
          ,OFSCOPAG = this.oParentObject.w_OFSCOPAG;
          ,OFSCOCL1 = this.oParentObject.w_OFSCOCL1;
          ,OFSCOCL2 = this.oParentObject.w_OFSCOCL2;
          ,OFCODPAG = this.oParentObject.w_OFCODPAG;
          ,OFFLFOSC = this.oParentObject.w_OFFLFOSC;
          ,OF__NOTE = this.oParentObject.w_OF__NOTE;
          ,OFSPEINC = this.oParentObject.w_OFSPEINC;
          ,OFCODLIS = this.oParentObject.w_OFCODLIS;
          ,OFSPEIMB = this.oParentObject.w_OFSPEIMB;
          ,OFCODUTE = this.oParentObject.w_OFCODUTE;
          ,OFCODSPE = this.oParentObject.w_OFCODSPE;
          ,OFCODCON = this.oParentObject.w_OFCODCON;
          ,OFNUMPRI = this.oParentObject.w_OFNUMPRI;
          ,OFPATFWP = this.oParentObject.w_OFPATFWP;
          ,OFSERPRE = this.oParentObject.w_OFSERIAL;
          ,OFCODVAL = this.oParentObject.w_OFCODVAL;
          ,OFCODPOR = this.oParentObject.w_OFCODPOR;
          ,OFDATRIC = this.oParentObject.w_OFDATRIC;
          ,OFDATAPE = this.oParentObject.w_OFDATAPE;
          &i_ccchkf. ;
       where;
          OFSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Scrittura Nuova Offerta'
      return
    endif
    if Isahe()
      * --- Write into OFF_ERTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OFSCOLIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSCOLIS),'OFF_ERTE','OFSCOLIS');
        +",OFPROLIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFPROLIS),'OFF_ERTE','OFPROLIS');
        +",OFPROSCO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFPROSCO),'OFF_ERTE','OFPROSCO');
            +i_ccchkf ;
        +" where ";
            +"OFSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        update (i_cTable) set;
            OFSCOLIS = this.oParentObject.w_OFSCOLIS;
            ,OFPROLIS = this.oParentObject.w_OFPROLIS;
            ,OFPROSCO = this.oParentObject.w_OFPROSCO;
            &i_ccchkf. ;
         where;
            OFSERIAL = this.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore Scrittura Nuova Offerta'
        return
      endif
    endif
    if Isalt()
      * --- Write into OFF_ERTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OFUFFICI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFUFFICI),'OFF_ERTE','OFUFFICI');
        +",OFTIPPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFTIPPRA),'OFF_ERTE','OFTIPPRA');
        +",OFTARTEM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFTARTEM),'OFF_ERTE','OFTARTEM');
        +",OFTARCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFTARCON),'OFF_ERTE','OFTARCON');
        +",OFMATPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFMATPRA),'OFF_ERTE','OFMATPRA');
        +",OFIMPORT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFIMPORT),'OFF_ERTE','OFIMPORT');
        +",OFFLVALO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFFLVALO),'OFF_ERTE','OFFLVALO');
        +",OFFLDIND ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFFLDIND),'OFF_ERTE','OFFLDIND');
        +",OFFLAPON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFFLAPON),'OFF_ERTE','OFFLAPON');
        +",OFCOECAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCOECAL),'OFF_ERTE','OFCOECAL');
        +",OFCODRES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODRES),'OFF_ERTE','OFCODRES');
        +",OFCODPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCODPRA),'OFF_ERTE','OFCODPRA');
        +",OFCALDIR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFCALDIR),'OFF_ERTE','OFCALDIR');
        +",OF__ENTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OF__ENTE),'OFF_ERTE','OF__ENTE');
            +i_ccchkf ;
        +" where ";
            +"OFSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        update (i_cTable) set;
            OFUFFICI = this.oParentObject.w_OFUFFICI;
            ,OFTIPPRA = this.oParentObject.w_OFTIPPRA;
            ,OFTARTEM = this.oParentObject.w_OFTARTEM;
            ,OFTARCON = this.oParentObject.w_OFTARCON;
            ,OFMATPRA = this.oParentObject.w_OFMATPRA;
            ,OFIMPORT = this.oParentObject.w_OFIMPORT;
            ,OFFLVALO = this.oParentObject.w_OFFLVALO;
            ,OFFLDIND = this.oParentObject.w_OFFLDIND;
            ,OFFLAPON = this.oParentObject.w_OFFLAPON;
            ,OFCOECAL = this.oParentObject.w_OFCOECAL;
            ,OFCODRES = this.oParentObject.w_OFCODRES;
            ,OFCODPRA = this.oParentObject.w_OFCODPRA;
            ,OFCALDIR = this.oParentObject.w_OFCALDIR;
            ,OF__ENTE = this.oParentObject.w_OF__ENTE;
            &i_ccchkf. ;
         where;
            OFSERIAL = this.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore Scrittura Nuova Offerta'
        return
      endif
    endif
    * --- Legge gli attributi dell'offerta vecchia 
    * --- Select from OFF_ATTR
    i_nConn=i_TableProp[this.OFF_ATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTR_idx,2],.t.,this.OFF_ATTR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFF_ATTR ";
          +" where OACODICE = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL)+"";
           ,"_Curs_OFF_ATTR")
    else
      select * from (i_cTable);
       where OACODICE = this.oParentObject.w_OFSERIAL;
        into cursor _Curs_OFF_ATTR
    endif
    if used('_Curs_OFF_ATTR')
      select _Curs_OFF_ATTR
      locate for 1=1
      do while not(eof())
      this.w_CODATT = _Curs_OFF_ATTR.oacodatt
      * --- Insert into OFF_ATTR
      i_nConn=i_TableProp[this.OFF_ATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_ATTR_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"OACODICE"+",OACODATT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OFF_ATTR','OACODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'OFF_ATTR','OACODATT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'OACODICE',this.w_SERIAL,'OACODATT',this.w_CODATT)
        insert into (i_cTable) (OACODICE,OACODATT &i_ccchkf. );
           values (;
             this.w_SERIAL;
             ,this.w_CODATT;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore Inserimento Nuovi Attributi'
        return
      endif
        select _Curs_OFF_ATTR
        continue
      enddo
      use
    endif
    * --- Leggo dettaglio vecchia offerta
    * --- Create temporary table TMPSEZOF
    i_nIdx=cp_AddTableDef('TMPSEZOF') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.OFF_DETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_DETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where ODSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL)+" ";
          )
    this.TMPSEZOF_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPSEZOF
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPSEZOF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPSEZOF_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPSEZOF_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ODSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMPSEZOF','ODSERIAL');
          +i_ccchkf ;
      +" where ";
          +"ODSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
             )
    else
      update (i_cTable) set;
          ODSERIAL = this.w_SERIAL;
          &i_ccchkf. ;
       where;
          ODSERIAL = this.oParentObject.w_OFSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Scrive dettaglio nuova versione offerta
    * --- Insert into OFF_DETT
    i_nConn=i_TableProp[this.OFF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPSEZOF_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where ODSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"",this.OFF_DETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPSEZOF
    i_nIdx=cp_GetTableDefIdx('TMPSEZOF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSEZOF')
    endif
    * --- Leggo sezione offerta da chiudere
    * --- Leggo sezioni vecchia offerta
    * --- Create temporary table TMPSEZOF
    i_nIdx=cp_AddTableDef('TMPSEZOF') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.OFF_SEZI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_SEZI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where OSCODICE = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL)+" ";
          )
    this.TMPSEZOF_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPSEZOF
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPSEZOF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPSEZOF_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPSEZOF_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OSCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMPSEZOF','OSCODICE');
          +i_ccchkf ;
      +" where ";
          +"OSCODICE = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
             )
    else
      update (i_cTable) set;
          OSCODICE = this.w_SERIAL;
          &i_ccchkf. ;
       where;
          OSCODICE = this.oParentObject.w_OFSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Scrivo le sezioni nella nuova offerta
    * --- Insert into OFF_SEZI
    i_nConn=i_TableProp[this.OFF_SEZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_SEZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPSEZOF_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where OSCODICE="+cp_ToStrODBC(this.w_SERIAL)+"",this.OFF_SEZI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPSEZOF
    i_nIdx=cp_GetTableDefIdx('TMPSEZOF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSEZOF')
    endif
    * --- Legge gli allegati dell'offerta vecchia 
    * --- Leggo allegati vecchia offerta
    * --- Create temporary table TMPSEZOF
    i_nIdx=cp_AddTableDef('TMPSEZOF') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSOF_BVN',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPSEZOF_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Tabella per duplicazione attributi allegati creata vuota
    * --- Create temporary table TMPATTOF
    i_nIdx=cp_AddTableDef('TMPATTOF') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ALL_ATTR_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ALL_ATTR_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where 1=0";
          )
    this.TMPATTOF_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from TMPSEZOF
    i_nConn=i_TableProp[this.TMPSEZOF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPSEZOF_idx,2],.t.,this.TMPSEZOF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPSEZOF ";
           ,"_Curs_TMPSEZOF")
    else
      select * from (i_cTable);
        into cursor _Curs_TMPSEZOF
    endif
    if used('_Curs_TMPSEZOF')
      select _Curs_TMPSEZOF
      locate for 1=1
      do while not(eof())
      this.w_SERIALE = "0000000000"
      i_nConn = i_TableProp[this.ALL_EGAT_IDX,3]
      cp_AskTableProg(this,i_nConn,"SEALLOF","I_codazi,w_SERIALE")
      cp_NextTableProg(this,i_nConn,"SEALLOF","I_codazi,w_SERIALE")
      * --- Write into TMPSEZOF
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPSEZOF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSEZOF_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPSEZOF_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ALRIFOFF ="+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMPSEZOF','ALRIFOFF');
        +",ALSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'TMPSEZOF','ALSERIAL');
        +",ALTIPORI ="+cp_NullLink(cp_ToStrODBC("O"),'TMPSEZOF','ALTIPORI');
        +",ALRIFNOM ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPSEZOF','ALRIFNOM');
        +",ALRIFMOD ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'TMPSEZOF','ALRIFMOD');
            +i_ccchkf ;
        +" where ";
            +"ALSERIAL = "+cp_ToStrODBC(_Curs_TMPSEZOF.ALSERIAL);
               )
      else
        update (i_cTable) set;
            ALRIFOFF = this.w_SERIAL;
            ,ALSERIAL = this.w_SERIALE;
            ,ALTIPORI = "O";
            ,ALRIFNOM = SPACE(15);
            ,ALRIFMOD = SPACE(5);
            &i_ccchkf. ;
         where;
            ALSERIAL = _Curs_TMPSEZOF.ALSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Copio gli attributi dell'allegato corrente
      * --- Insert into TMPATTOF
      i_nConn=i_TableProp[this.TMPATTOF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPATTOF_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.ALL_ATTR_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where AACODICE = "+cp_ToStrODBC(_Curs_TMPSEZOF.ALSERIAL)+"",this.TMPATTOF_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Aggiorno seriale attributo allegato corrente
      * --- Write into TMPATTOF
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPATTOF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPATTOF_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTOF_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AACODICE ="+cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'TMPATTOF','AACODICE');
            +i_ccchkf ;
        +" where ";
            +"AACODICE = "+cp_ToStrODBC(_Curs_TMPSEZOF.ALSERIAL);
               )
      else
        update (i_cTable) set;
            AACODICE = this.w_SERIALE;
            &i_ccchkf. ;
         where;
            AACODICE = _Curs_TMPSEZOF.ALSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_TMPSEZOF
        continue
      enddo
      use
    endif
    * --- Scrivo gli allegati nella nuova offerta
    * --- Insert into ALL_EGAT
    i_nConn=i_TableProp[this.ALL_EGAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALL_EGAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPSEZOF_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ALL_EGAT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Scrivo gli attributi degli allegati
    * --- Insert into ALL_ATTR
    i_nConn=i_TableProp[this.ALL_ATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALL_ATTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPATTOF_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ALL_ATTR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPSEZOF
    i_nIdx=cp_GetTableDefIdx('TMPSEZOF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSEZOF')
    endif
    * --- Drop temporary table TMPATTOF
    i_nIdx=cp_GetTableDefIdx('TMPATTOF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPATTOF')
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Salvo l'offerta chiusa
    this.oParentObject.ecpSave()
    * --- Write into OFF_ERTE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ERTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OFSTATUS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OFSTATUS),'OFF_ERTE','OFSTATUS');
      +",OFDATCHI ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'OFF_ERTE','OFDATCHI');
          +i_ccchkf ;
      +" where ";
          +"OFSERIAL = "+cp_ToStrODBC(this.oParentObject.w_OFSERIAL);
             )
    else
      update (i_cTable) set;
          OFSTATUS = this.oParentObject.w_OFSTATUS;
          ,OFDATCHI = i_DATSYS;
          &i_ccchkf. ;
       where;
          OFSERIAL = this.oParentObject.w_OFSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione variabili
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='OFF_ERTE'
    this.cWorkTables[2]='OFF_SEZI'
    this.cWorkTables[3]='OFF_DETT'
    this.cWorkTables[4]='ALL_EGAT'
    this.cWorkTables[5]='ALL_ATTR'
    this.cWorkTables[6]='OFF_ATTR'
    this.cWorkTables[7]='*TMPSEZOF'
    this.cWorkTables[8]='*TMPATTOF'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_OFF_ATTR')
      use in _Curs_OFF_ATTR
    endif
    if used('_Curs_TMPSEZOF')
      use in _Curs_TMPSEZOF
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
