* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bgn                                                        *
*              Eventi da gestione nominativi                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_41]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-05                                                      *
* Last revis.: 2013-01-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bgn",oParentObject,m.Azione)
return(i_retval)

define class tgsof_bgn as StdBatch
  * --- Local variables
  Azione = space(10)
  w_OFNUMDOC = 0
  w_OFSERDOC = space(10)
  w_OFDATDOC = ctod("  /  /  ")
  w_OFNUMVER = 0
  w_CODCLI = space(15)
  GestALL = .NULL.
  TMPc = space(10)
  pop = space(100)
  w_OFSERIAL = space(10)
  w_PERIODO = space(1)
  w_OFPATFWP = space(254)
  w_OFPATPDF = space(254)
  w_CODMOD = space(5)
  w_DESCRI = space(50)
  * --- WorkFile variables
  GRU_PRIO_idx=0
  OFF_ERTE_idx=0
  MOD_OFFE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Gestione Nominativo (da GSAR_ANO)
    this.w_CODCLI = this.oParentObject.w_NOCODCLI
    * --- CARICA_ALLEGATO = Inserisce un Nuovo Allegato
    do case
      case this.Azione="CARICA_ALLEGATO"
        * --- Nuovo Allegato
        * --- Descrizione nominativo se persona fisica nome e cognome se ente denominazione 1+demoninazione 2
        this.w_DESCRI = iif( this.oParentObject.w_NOSOGGET="EN" , Alltrim( this.oParentObject.w_NODESCRI ) +" "+Alltrim( this.oParentObject.w_NODESCR2 ) , Alltrim(this.oParentObject.w_NO__NOME) +" "+Alltrim(this.oParentObject.w_NOCOGNOM))
        this.GestALL = GSOF_AAL(this.oParentObject.w_NOCODICE, "N",this.w_DESCRI,this.w_OFNUMDOC,this.w_OFSERDOC,this.w_OFDATDOC,this.w_OFNUMVER)
        * --- Controllo se ha passato il test di accesso 
        if !(this.GestALL.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        * --- Lancia Caricamento ...
        this.GestALL.ECPLoad()     
        this.GestALL.w_ALTIPORI = "N"
        this.GestALL.w_ALRIFNOM = this.oParentObject.w_NOCODICE
        this.GestALL.mCalc(.T.)     
      case this.Azione $"VARIA_ALLEGATO|ELIMINA_ALLEGATO"
        if Not Empty( this.oParentObject.w_SERALL )
          this.GestALL = GSOF_AAL(this.oParentObject.w_NOCODICE, "N",this.oParentObject.w_NODESCRI,this.w_OFNUMDOC,this.w_OFSERDOC,this.w_OFDATDOC,this.w_OFNUMVER)
          * --- Controllo se ha passato il test di accesso 
          if !(this.GestALL.bSec1)
            Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
            i_retcode = 'stop'
            return
          endif
          * --- Carica record e lascia in interrograzione ...
          this.GestALL.EcpFilter()     
          this.GestALL.w_ALSERIAL = this.oParentObject.w_SERALL
          this.GestALL.EcpSave()     
          * --- Carica record e lascia in interrograzione ...
          do case
            case this.Azione = "VARIA_ALLEGATO"
              * --- Modifica record
              this.GestALL.ECPEdit()     
            case this.Azione = "ELIMINA_ALLEGATO"
              * --- Cancella record ...
              this.GestALL.ECPDelete()     
          endcase
        endif
      case this.Azione="CARICA_OFFERTA"
        * --- Nuova Offerta
        this.GestALL = GSOF_AOF()
        * --- Controllo se ha passato il test di accesso 
        if !(this.GestALL.bSec1)
          Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
          i_retcode = 'stop'
          return
        endif
        * --- Lancia Caricamento ...
        this.GestALL.ECPLoad()     
        this.GestALL.w_OFCODNOM = this.oParentObject.w_NOCODICE
        this.GestALL.w_NOMDES = this.oParentObject.w_NODESCRI
        this.GestALL.w_NOMIND = this.oParentObject.w_NOINDIRI
        this.GestALL.w_NOMCAP = this.oParentObject.w_NO___CAP
        this.GestALL.w_NOMLOC = this.oParentObject.w_NOLOCALI
        this.GestALL.w_NOMPRO = this.oParentObject.w_NOPROVIN
        this.GestALL.w_VALCLF = this.oParentObject.w_NOCODVAL
        this.GestALL.w_OFCODAGE = this.oParentObject.w_NOCODAGE
        this.GestALL.w_NCODZON = this.oParentObject.w_NOCODZON
        this.GestALL.w_NCODORI = this.oParentObject.w_NOCODORI
        this.GestALL.w_NCODGRN = this.oParentObject.w_NOCODGRU
        this.GestALL.w_NCODPRI = this.oParentObject.w_NOCODPRI
        this.GestALL.w_CODCLI = this.oParentObject.w_NOCODCLI
        this.GestALL.w_TIPNOM = this.oParentObject.w_NOTIPNOM
        this.GestALL.w_DTOBS1 = this.oParentObject.w_NODTOBSO
        this.GestALL.w_OFCODUTE = this.oParentObject.w_NOCODOPE
        this.GestALL.w_NCODLIN = this.oParentObject.w_NOCODLIN
        this.GestALL.mCalc(.T.)     
        = setvaluelinked ( "M" , this.GestALL , "w_OFCODNOM" , this.oParentObject.w_NOCODICE)
      case this.Azione="VARIA_OFFERTA"
        if Not Empty( this.oParentObject.w_SEROFF )
          this.GestALL = GSOF_AOF()
          * --- Controllo se ha passato il test di accesso 
          if !(this.GestALL.bSec1)
            Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
            i_retcode = 'stop'
            return
          endif
          * --- Carica record e lascia in interrograzione ...
          this.GestALL.EcpFilter()     
          this.GestALL.w_OFSERIAL = this.oParentObject.w_SEROFF
          this.GestALL.EcpSave()     
          this.GestALL.ECPEdit()     
        endif
      case this.Azione = "APRI_ALLEGATO"
        * --- Apri file ...
        if Not Empty( this.oParentObject.w_SERALL ) and NOT EMPTY( this.oParentObject.w_PATALL )
          this.pop = ofopenfile(ALLTRIM(this.oParentObject.w_PATALL),ALLTRIM(this.oParentObject.w_SERALL),"N",ALLTRIM(this.oParentObject.w_NOCODICE))
        endif
      case this.Azione = "ESEGUE_REPORT"
        * --- Stampa Offerta selezionata
        this.w_OFSERIAL = this.oParentObject.w_SEROFF
        this.w_CODMOD = this.oParentObject.w_OFCODMOD
        * --- Read from OFF_ERTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_ERTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ERTE_idx,2],.t.,this.OFF_ERTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OFPATFWP,OFPATPDF"+;
            " from "+i_cTable+" OFF_ERTE where ";
                +"OFSERIAL = "+cp_ToStrODBC(this.w_OFSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OFPATFWP,OFPATPDF;
            from (i_cTable) where;
                OFSERIAL = this.w_OFSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OFPATFWP = NVL(cp_ToDate(_read_.OFPATFWP),cp_NullValue(_read_.OFPATFWP))
          this.w_OFPATPDF = NVL(cp_ToDate(_read_.OFPATPDF),cp_NullValue(_read_.OFPATPDF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from MOD_OFFE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOD_OFFE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOD_OFFE_idx,2],.t.,this.MOD_OFFE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MONUMPER"+;
            " from "+i_cTable+" MOD_OFFE where ";
                +"MOCODICE = "+cp_ToStrODBC(this.w_CODMOD);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MONUMPER;
            from (i_cTable) where;
                MOCODICE = this.w_CODMOD;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERIODO = NVL(cp_ToDate(_read_.MONUMPER),cp_NullValue(_read_.MONUMPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_OFSERIAL)
          do GSOF_BOF with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.Azione = "CALCOLA_OFFERTE"
        if this.oParentObject.cFunction="Query"
          * --- Read from GRU_PRIO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.GRU_PRIO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GRU_PRIO_idx,2],.t.,this.GRU_PRIO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "GPNUMINI,GPNUMFIN"+;
              " from "+i_cTable+" GRU_PRIO where ";
                  +"GPCODICE = "+cp_ToStrODBC(this.oParentObject.w_NFGRPRIO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              GPNUMINI,GPNUMFIN;
              from (i_cTable) where;
                  GPCODICE = this.oParentObject.w_NFGRPRIO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_NUMINI = NVL(cp_ToDate(_read_.GPNUMINI),cp_NullValue(_read_.GPNUMINI))
            this.oParentObject.w_NUMFIN = NVL(cp_ToDate(_read_.GPNUMFIN),cp_NullValue(_read_.GPNUMFIN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.oParentObject.NotifyEvent("CalcolaOfferte")
    endcase
  endproc


  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='GRU_PRIO'
    this.cWorkTables[2]='OFF_ERTE'
    this.cWorkTables[3]='MOD_OFFE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
