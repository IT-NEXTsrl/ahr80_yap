* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_apo                                                        *
*              Parametri offerte                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-10                                                      *
* Last revis.: 2015-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsof_apo")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsof_apo")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsof_apo")
  return

* --- Class definition
define class tgsof_apo as StdPCForm
  Width  = 599
  Height = 356
  Top    = 4
  Left   = 21
  cComment = "Parametri offerte"
  cPrg = "gsof_apo"
  HelpContextID=108385943
  add object cnt as tcgsof_apo
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsof_apo as PCContext
  w_POPRIDEF = 0
  w_POMODOFF = space(1)
  w_POMASOFF = space(15)
  w_POFLCOOF = space(1)
  w_POTIPATT = space(20)
  w_POCODMOD = space(5)
  w_POFLCAAR = space(1)
  w_POFILOFF = space(1)
  w_POCHKIVA = space(1)
  w_POPATALN = space(254)
  w_POPERALN = space(1)
  w_POPATOFF = space(254)
  w_POPEROFF = space(1)
  w_POCODAZI = space(5)
  w_POOUTEXP = space(40)
  w_POOUTRAG = space(1)
  w_POOUTIMP = space(40)
  w_CHKNOM = space(1)
  w_FLNSAP = space(1)
  w_CHKOBB = space(1)
  w_DESMOD = space(35)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = space(14)
  w_UTDV = space(14)
  proc Save(oFrom)
    this.w_POPRIDEF = oFrom.w_POPRIDEF
    this.w_POMODOFF = oFrom.w_POMODOFF
    this.w_POMASOFF = oFrom.w_POMASOFF
    this.w_POFLCOOF = oFrom.w_POFLCOOF
    this.w_POTIPATT = oFrom.w_POTIPATT
    this.w_POCODMOD = oFrom.w_POCODMOD
    this.w_POFLCAAR = oFrom.w_POFLCAAR
    this.w_POFILOFF = oFrom.w_POFILOFF
    this.w_POCHKIVA = oFrom.w_POCHKIVA
    this.w_POPATALN = oFrom.w_POPATALN
    this.w_POPERALN = oFrom.w_POPERALN
    this.w_POPATOFF = oFrom.w_POPATOFF
    this.w_POPEROFF = oFrom.w_POPEROFF
    this.w_POCODAZI = oFrom.w_POCODAZI
    this.w_POOUTEXP = oFrom.w_POOUTEXP
    this.w_POOUTRAG = oFrom.w_POOUTRAG
    this.w_POOUTIMP = oFrom.w_POOUTIMP
    this.w_CHKNOM = oFrom.w_CHKNOM
    this.w_FLNSAP = oFrom.w_FLNSAP
    this.w_CHKOBB = oFrom.w_CHKOBB
    this.w_DESMOD = oFrom.w_DESMOD
    this.w_UTCC = oFrom.w_UTCC
    this.w_UTCV = oFrom.w_UTCV
    this.w_UTDC = oFrom.w_UTDC
    this.w_UTDV = oFrom.w_UTDV
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_POPRIDEF = this.w_POPRIDEF
    oTo.w_POMODOFF = this.w_POMODOFF
    oTo.w_POMASOFF = this.w_POMASOFF
    oTo.w_POFLCOOF = this.w_POFLCOOF
    oTo.w_POTIPATT = this.w_POTIPATT
    oTo.w_POCODMOD = this.w_POCODMOD
    oTo.w_POFLCAAR = this.w_POFLCAAR
    oTo.w_POFILOFF = this.w_POFILOFF
    oTo.w_POCHKIVA = this.w_POCHKIVA
    oTo.w_POPATALN = this.w_POPATALN
    oTo.w_POPERALN = this.w_POPERALN
    oTo.w_POPATOFF = this.w_POPATOFF
    oTo.w_POPEROFF = this.w_POPEROFF
    oTo.w_POCODAZI = this.w_POCODAZI
    oTo.w_POOUTEXP = this.w_POOUTEXP
    oTo.w_POOUTRAG = this.w_POOUTRAG
    oTo.w_POOUTIMP = this.w_POOUTIMP
    oTo.w_CHKNOM = this.w_CHKNOM
    oTo.w_FLNSAP = this.w_FLNSAP
    oTo.w_CHKOBB = this.w_CHKOBB
    oTo.w_DESMOD = this.w_DESMOD
    oTo.w_UTCC = this.w_UTCC
    oTo.w_UTCV = this.w_UTCV
    oTo.w_UTDC = this.w_UTDC
    oTo.w_UTDV = this.w_UTDV
    PCContext::Load(oTo)
enddefine

define class tcgsof_apo as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 599
  Height = 356
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-08"
  HelpContextID=108385943
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Constant Properties
  PAR_OFFE_IDX = 0
  CAUMATTI_IDX = 0
  MOD_OFFE_IDX = 0
  cFile = "PAR_OFFE"
  cKeySelect = "POCODAZI"
  cKeyWhere  = "POCODAZI=this.w_POCODAZI"
  cKeyWhereODBC = '"POCODAZI="+cp_ToStrODBC(this.w_POCODAZI)';

  cKeyWhereODBCqualified = '"PAR_OFFE.POCODAZI="+cp_ToStrODBC(this.w_POCODAZI)';

  cPrg = "gsof_apo"
  cComment = "Parametri offerte"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_POPRIDEF = 0
  w_POMODOFF = space(1)
  w_POMASOFF = space(15)
  w_POFLCOOF = space(1)
  w_POTIPATT = space(20)
  w_POCODMOD = space(5)
  w_POFLCAAR = space(1)
  w_POFILOFF = space(1)
  w_POCHKIVA = space(1)
  w_POPATALN = space(254)
  w_POPERALN = space(1)
  w_POPATOFF = space(254)
  w_POPEROFF = space(1)
  w_POCODAZI = space(5)
  o_POCODAZI = space(5)
  w_POOUTEXP = space(40)
  w_POOUTRAG = space(1)
  w_POOUTIMP = space(40)
  w_CHKNOM = space(1)
  w_FLNSAP = space(1)
  w_CHKOBB = space(1)
  w_DESMOD = space(35)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_apoPag1","gsof_apo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 166833398
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPOPRIDEF_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAUMATTI'
    this.cWorkTables[2]='MOD_OFFE'
    this.cWorkTables[3]='PAR_OFFE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_OFFE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_OFFE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsof_apo'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_6_joined
    link_1_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_OFFE where POCODAZI=KeySet.POCODAZI
    *
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_OFFE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_OFFE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_OFFE '
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'POCODAZI',this.w_POCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CHKNOM = space(1)
        .w_FLNSAP = space(1)
        .w_CHKOBB = space(1)
        .w_DESMOD = space(35)
        .w_POPRIDEF = NVL(POPRIDEF,0)
        .w_POMODOFF = NVL(POMODOFF,space(1))
        .w_POMASOFF = NVL(POMASOFF,space(15))
        .w_POFLCOOF = NVL(POFLCOOF,space(1))
        .w_POTIPATT = NVL(POTIPATT,space(20))
          .link_1_5('Load')
        .w_POCODMOD = NVL(POCODMOD,space(5))
          if link_1_6_joined
            this.w_POCODMOD = NVL(MOCODICE106,NVL(this.w_POCODMOD,space(5)))
            this.w_DESMOD = NVL(MODESCRI106,space(35))
          else
          .link_1_6('Load')
          endif
        .w_POFLCAAR = NVL(POFLCAAR,space(1))
        .w_POFILOFF = NVL(POFILOFF,space(1))
        .w_POCHKIVA = NVL(POCHKIVA,space(1))
        .w_POPATALN = NVL(POPATALN,space(254))
        .w_POPERALN = NVL(POPERALN,space(1))
        .w_POPATOFF = NVL(POPATOFF,space(254))
        .w_POPEROFF = NVL(POPEROFF,space(1))
        .w_POCODAZI = NVL(POCODAZI,space(5))
        .w_POOUTEXP = NVL(POOUTEXP,space(40))
        .w_POOUTRAG = NVL(POOUTRAG,space(1))
        .w_POOUTIMP = NVL(POOUTIMP,space(40))
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'PAR_OFFE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_POPRIDEF = 0
      .w_POMODOFF = space(1)
      .w_POMASOFF = space(15)
      .w_POFLCOOF = space(1)
      .w_POTIPATT = space(20)
      .w_POCODMOD = space(5)
      .w_POFLCAAR = space(1)
      .w_POFILOFF = space(1)
      .w_POCHKIVA = space(1)
      .w_POPATALN = space(254)
      .w_POPERALN = space(1)
      .w_POPATOFF = space(254)
      .w_POPEROFF = space(1)
      .w_POCODAZI = space(5)
      .w_POOUTEXP = space(40)
      .w_POOUTRAG = space(1)
      .w_POOUTIMP = space(40)
      .w_CHKNOM = space(1)
      .w_FLNSAP = space(1)
      .w_CHKOBB = space(1)
      .w_DESMOD = space(35)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .DoRTCalc(1,5,.f.)
          if not(empty(.w_POTIPATT))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_POCODMOD))
          .link_1_6('Full')
          endif
          .DoRTCalc(7,13,.f.)
        .w_POCODAZI = i_codazi
          .DoRTCalc(15,15,.f.)
        .w_POOUTRAG = 'N'
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_OFFE')
    this.DoRTCalc(17,25,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPOPRIDEF_1_1.enabled = i_bVal
      .Page1.oPag.oPOMODOFF_1_2.enabled = i_bVal
      .Page1.oPag.oPOMASOFF_1_3.enabled = i_bVal
      .Page1.oPag.oPOFLCOOF_1_4.enabled = i_bVal
      .Page1.oPag.oPOTIPATT_1_5.enabled = i_bVal
      .Page1.oPag.oPOCODMOD_1_6.enabled = i_bVal
      .Page1.oPag.oPOFLCAAR_1_7.enabled = i_bVal
      .Page1.oPag.oPOFILOFF_1_8.enabled = i_bVal
      .Page1.oPag.oPOCHKIVA_1_9.enabled = i_bVal
      .Page1.oPag.oPOPATALN_1_10.enabled = i_bVal
      .Page1.oPag.oPOPERALN_1_12.enabled = i_bVal
      .Page1.oPag.oPOPATOFF_1_13.enabled = i_bVal
      .Page1.oPag.oPOPEROFF_1_15.enabled = i_bVal
      .Page1.oPag.oPOOUTEXP_1_17.enabled = i_bVal
      .Page1.oPag.oPOOUTRAG_1_18.enabled = i_bVal
      .Page1.oPag.oPOOUTIMP_1_19.enabled = i_bVal
      .Page1.oPag.oBtn_1_11.enabled = i_bVal
      .Page1.oPag.oBtn_1_14.enabled = i_bVal
      .Page1.oPag.oBtn_1_21.enabled = i_bVal
      .Page1.oPag.oBtn_1_23.enabled = .Page1.oPag.oBtn_1_23.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'PAR_OFFE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POPRIDEF,"POPRIDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POMODOFF,"POMODOFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POMASOFF,"POMASOFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POFLCOOF,"POFLCOOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POTIPATT,"POTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCODMOD,"POCODMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POFLCAAR,"POFLCAAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POFILOFF,"POFILOFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCHKIVA,"POCHKIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POPATALN,"POPATALN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POPERALN,"POPERALN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POPATOFF,"POPATOFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POPEROFF,"POPEROFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCODAZI,"POCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POOUTEXP,"POOUTEXP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POOUTRAG,"POOUTRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POOUTIMP,"POOUTIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_OFFE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_OFFE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_OFFE')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_OFFE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(POPRIDEF,POMODOFF,POMASOFF,POFLCOOF,POTIPATT"+;
                  ",POCODMOD,POFLCAAR,POFILOFF,POCHKIVA,POPATALN"+;
                  ",POPERALN,POPATOFF,POPEROFF,POCODAZI,POOUTEXP"+;
                  ",POOUTRAG,POOUTIMP,UTCC,UTCV,UTDC"+;
                  ",UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_POPRIDEF)+;
                  ","+cp_ToStrODBC(this.w_POMODOFF)+;
                  ","+cp_ToStrODBC(this.w_POMASOFF)+;
                  ","+cp_ToStrODBC(this.w_POFLCOOF)+;
                  ","+cp_ToStrODBCNull(this.w_POTIPATT)+;
                  ","+cp_ToStrODBCNull(this.w_POCODMOD)+;
                  ","+cp_ToStrODBC(this.w_POFLCAAR)+;
                  ","+cp_ToStrODBC(this.w_POFILOFF)+;
                  ","+cp_ToStrODBC(this.w_POCHKIVA)+;
                  ","+cp_ToStrODBC(this.w_POPATALN)+;
                  ","+cp_ToStrODBC(this.w_POPERALN)+;
                  ","+cp_ToStrODBC(this.w_POPATOFF)+;
                  ","+cp_ToStrODBC(this.w_POPEROFF)+;
                  ","+cp_ToStrODBC(this.w_POCODAZI)+;
                  ","+cp_ToStrODBC(this.w_POOUTEXP)+;
                  ","+cp_ToStrODBC(this.w_POOUTRAG)+;
                  ","+cp_ToStrODBC(this.w_POOUTIMP)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_OFFE')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_OFFE')
        cp_CheckDeletedKey(i_cTable,0,'POCODAZI',this.w_POCODAZI)
        INSERT INTO (i_cTable);
              (POPRIDEF,POMODOFF,POMASOFF,POFLCOOF,POTIPATT,POCODMOD,POFLCAAR,POFILOFF,POCHKIVA,POPATALN,POPERALN,POPATOFF,POPEROFF,POCODAZI,POOUTEXP,POOUTRAG,POOUTIMP,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_POPRIDEF;
                  ,this.w_POMODOFF;
                  ,this.w_POMASOFF;
                  ,this.w_POFLCOOF;
                  ,this.w_POTIPATT;
                  ,this.w_POCODMOD;
                  ,this.w_POFLCAAR;
                  ,this.w_POFILOFF;
                  ,this.w_POCHKIVA;
                  ,this.w_POPATALN;
                  ,this.w_POPERALN;
                  ,this.w_POPATOFF;
                  ,this.w_POPEROFF;
                  ,this.w_POCODAZI;
                  ,this.w_POOUTEXP;
                  ,this.w_POOUTRAG;
                  ,this.w_POOUTIMP;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_OFFE_IDX,i_nConn)
      *
      * update PAR_OFFE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_OFFE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " POPRIDEF="+cp_ToStrODBC(this.w_POPRIDEF)+;
             ",POMODOFF="+cp_ToStrODBC(this.w_POMODOFF)+;
             ",POMASOFF="+cp_ToStrODBC(this.w_POMASOFF)+;
             ",POFLCOOF="+cp_ToStrODBC(this.w_POFLCOOF)+;
             ",POTIPATT="+cp_ToStrODBCNull(this.w_POTIPATT)+;
             ",POCODMOD="+cp_ToStrODBCNull(this.w_POCODMOD)+;
             ",POFLCAAR="+cp_ToStrODBC(this.w_POFLCAAR)+;
             ",POFILOFF="+cp_ToStrODBC(this.w_POFILOFF)+;
             ",POCHKIVA="+cp_ToStrODBC(this.w_POCHKIVA)+;
             ",POPATALN="+cp_ToStrODBC(this.w_POPATALN)+;
             ",POPERALN="+cp_ToStrODBC(this.w_POPERALN)+;
             ",POPATOFF="+cp_ToStrODBC(this.w_POPATOFF)+;
             ",POPEROFF="+cp_ToStrODBC(this.w_POPEROFF)+;
             ",POOUTEXP="+cp_ToStrODBC(this.w_POOUTEXP)+;
             ",POOUTRAG="+cp_ToStrODBC(this.w_POOUTRAG)+;
             ",POOUTIMP="+cp_ToStrODBC(this.w_POOUTIMP)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_OFFE')
        i_cWhere = cp_PKFox(i_cTable  ,'POCODAZI',this.w_POCODAZI  )
        UPDATE (i_cTable) SET;
              POPRIDEF=this.w_POPRIDEF;
             ,POMODOFF=this.w_POMODOFF;
             ,POMASOFF=this.w_POMASOFF;
             ,POFLCOOF=this.w_POFLCOOF;
             ,POTIPATT=this.w_POTIPATT;
             ,POCODMOD=this.w_POCODMOD;
             ,POFLCAAR=this.w_POFLCAAR;
             ,POFILOFF=this.w_POFILOFF;
             ,POCHKIVA=this.w_POCHKIVA;
             ,POPATALN=this.w_POPATALN;
             ,POPERALN=this.w_POPERALN;
             ,POPATOFF=this.w_POPATOFF;
             ,POPEROFF=this.w_POPEROFF;
             ,POOUTEXP=this.w_POOUTEXP;
             ,POOUTRAG=this.w_POOUTRAG;
             ,POOUTIMP=this.w_POOUTIMP;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_OFFE_IDX,i_nConn)
      *
      * delete PAR_OFFE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'POCODAZI',this.w_POCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPOPATALN_1_10.enabled = this.oPgFrm.Page1.oPag.oPOPATALN_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPOPERALN_1_12.enabled = this.oPgFrm.Page1.oPag.oPOPERALN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oPOPATOFF_1_13.enabled = this.oPgFrm.Page1.oPag.oPOPATOFF_1_13.mCond()
    this.oPgFrm.Page1.oPag.oPOPEROFF_1_15.enabled = this.oPgFrm.Page1.oPag.oPOPEROFF_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPOPRIDEF_1_1.visible=!this.oPgFrm.Page1.oPag.oPOPRIDEF_1_1.mHide()
    this.oPgFrm.Page1.oPag.oPOMASOFF_1_3.visible=!this.oPgFrm.Page1.oPag.oPOMASOFF_1_3.mHide()
    this.oPgFrm.Page1.oPag.oPOFLCOOF_1_4.visible=!this.oPgFrm.Page1.oPag.oPOFLCOOF_1_4.mHide()
    this.oPgFrm.Page1.oPag.oPOTIPATT_1_5.visible=!this.oPgFrm.Page1.oPag.oPOTIPATT_1_5.mHide()
    this.oPgFrm.Page1.oPag.oPOFLCAAR_1_7.visible=!this.oPgFrm.Page1.oPag.oPOFLCAAR_1_7.mHide()
    this.oPgFrm.Page1.oPag.oPOFILOFF_1_8.visible=!this.oPgFrm.Page1.oPag.oPOFILOFF_1_8.mHide()
    this.oPgFrm.Page1.oPag.oPOCHKIVA_1_9.visible=!this.oPgFrm.Page1.oPag.oPOCHKIVA_1_9.mHide()
    this.oPgFrm.Page1.oPag.oPOPATALN_1_10.visible=!this.oPgFrm.Page1.oPag.oPOPATALN_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPOPERALN_1_12.visible=!this.oPgFrm.Page1.oPag.oPOPERALN_1_12.mHide()
    this.oPgFrm.Page1.oPag.oPOPATOFF_1_13.visible=!this.oPgFrm.Page1.oPag.oPOPATOFF_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oPOPEROFF_1_15.visible=!this.oPgFrm.Page1.oPag.oPOPEROFF_1_15.mHide()
    this.oPgFrm.Page1.oPag.oPOOUTEXP_1_17.visible=!this.oPgFrm.Page1.oPag.oPOOUTEXP_1_17.mHide()
    this.oPgFrm.Page1.oPag.oPOOUTRAG_1_18.visible=!this.oPgFrm.Page1.oPag.oPOOUTRAG_1_18.mHide()
    this.oPgFrm.Page1.oPag.oPOOUTIMP_1_19.visible=!this.oPgFrm.Page1.oPag.oPOOUTIMP_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=POTIPATT
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POTIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_POTIPATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACHKNOM,CACHKOBB,CAFLNSAP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_POTIPATT))
          select CACODICE,CACHKNOM,CACHKOBB,CAFLNSAP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_POTIPATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_POTIPATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPOTIPATT_1_5'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACHKNOM,CACHKOBB,CAFLNSAP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACHKNOM,CACHKOBB,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POTIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACHKNOM,CACHKOBB,CAFLNSAP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_POTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_POTIPATT)
            select CACODICE,CACHKNOM,CACHKOBB,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POTIPATT = NVL(_Link_.CACODICE,space(20))
      this.w_CHKNOM = NVL(_Link_.CACHKNOM,space(1))
      this.w_CHKOBB = NVL(_Link_.CACHKOBB,space(1))
      this.w_FLNSAP = NVL(_Link_.CAFLNSAP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_POTIPATT = space(20)
      endif
      this.w_CHKNOM = space(1)
      this.w_CHKOBB = space(1)
      this.w_FLNSAP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_FLNSAP='S' AND .w_CHKNOM <> 'P' AND .w_CHKOBB<>'P')  or Isalt()
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo attivit� soggetto a prestazioni o con nominativo/commessa obbligatori ")
        endif
        this.w_POTIPATT = space(20)
        this.w_CHKNOM = space(1)
        this.w_CHKOBB = space(1)
        this.w_FLNSAP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POTIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=POCODMOD
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_OFFE_IDX,3]
    i_lTable = "MOD_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2], .t., this.MOD_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AMO',True,'MOD_OFFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_POCODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_POCODMOD))
          select MOCODICE,MODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_POCODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_POCODMOD) and !this.bDontReportError
            deferred_cp_zoom('MOD_OFFE','*','MOCODICE',cp_AbsName(oSource.parent,'oPOCODMOD_1_6'),i_cWhere,'GSOF_AMO',"Modelli offerte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_POCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_POCODMOD)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POCODMOD = NVL(_Link_.MOCODICE,space(5))
      this.w_DESMOD = NVL(_Link_.MODESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_POCODMOD = space(5)
      endif
      this.w_DESMOD = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MOD_OFFE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MOD_OFFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.MOCODICE as MOCODICE106"+ ",link_1_6.MODESCRI as MODESCRI106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on PAR_OFFE.POCODMOD=link_1_6.MOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and PAR_OFFE.POCODMOD=link_1_6.MOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPOPRIDEF_1_1.value==this.w_POPRIDEF)
      this.oPgFrm.Page1.oPag.oPOPRIDEF_1_1.value=this.w_POPRIDEF
    endif
    if not(this.oPgFrm.Page1.oPag.oPOMODOFF_1_2.RadioValue()==this.w_POMODOFF)
      this.oPgFrm.Page1.oPag.oPOMODOFF_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOMASOFF_1_3.value==this.w_POMASOFF)
      this.oPgFrm.Page1.oPag.oPOMASOFF_1_3.value=this.w_POMASOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oPOFLCOOF_1_4.RadioValue()==this.w_POFLCOOF)
      this.oPgFrm.Page1.oPag.oPOFLCOOF_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOTIPATT_1_5.value==this.w_POTIPATT)
      this.oPgFrm.Page1.oPag.oPOTIPATT_1_5.value=this.w_POTIPATT
    endif
    if not(this.oPgFrm.Page1.oPag.oPOCODMOD_1_6.value==this.w_POCODMOD)
      this.oPgFrm.Page1.oPag.oPOCODMOD_1_6.value=this.w_POCODMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oPOFLCAAR_1_7.RadioValue()==this.w_POFLCAAR)
      this.oPgFrm.Page1.oPag.oPOFLCAAR_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOFILOFF_1_8.RadioValue()==this.w_POFILOFF)
      this.oPgFrm.Page1.oPag.oPOFILOFF_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOCHKIVA_1_9.RadioValue()==this.w_POCHKIVA)
      this.oPgFrm.Page1.oPag.oPOCHKIVA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOPATALN_1_10.value==this.w_POPATALN)
      this.oPgFrm.Page1.oPag.oPOPATALN_1_10.value=this.w_POPATALN
    endif
    if not(this.oPgFrm.Page1.oPag.oPOPERALN_1_12.RadioValue()==this.w_POPERALN)
      this.oPgFrm.Page1.oPag.oPOPERALN_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOPATOFF_1_13.value==this.w_POPATOFF)
      this.oPgFrm.Page1.oPag.oPOPATOFF_1_13.value=this.w_POPATOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oPOPEROFF_1_15.RadioValue()==this.w_POPEROFF)
      this.oPgFrm.Page1.oPag.oPOPEROFF_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOOUTEXP_1_17.value==this.w_POOUTEXP)
      this.oPgFrm.Page1.oPag.oPOOUTEXP_1_17.value=this.w_POOUTEXP
    endif
    if not(this.oPgFrm.Page1.oPag.oPOOUTRAG_1_18.RadioValue()==this.w_POOUTRAG)
      this.oPgFrm.Page1.oPag.oPOOUTRAG_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPOOUTIMP_1_19.value==this.w_POOUTIMP)
      this.oPgFrm.Page1.oPag.oPOOUTIMP_1_19.value=this.w_POOUTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOD_1_39.value==this.w_DESMOD)
      this.oPgFrm.Page1.oPag.oDESMOD_1_39.value=this.w_DESMOD
    endif
    cp_SetControlsValueExtFlds(this,'PAR_OFFE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_FLNSAP='S' AND .w_CHKNOM <> 'P' AND .w_CHKOBB<>'P')  or Isalt())  and not(g_AGEN<>'S')  and not(empty(.w_POTIPATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPOTIPATT_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo attivit� soggetto a prestazioni o con nominativo/commessa obbligatori ")
          case   not(EMPTY(.w_POPATOFF) Or  DIRECTORY(FULLPATH(ALLTRIM(.w_POPATOFF))))  and not(Isalt())  and (.w_POFILOFF='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPOPATOFF_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il percorso di archiviazione non � valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsof_apo
      * --- Controllo campi obbligatori
      if i_bRes=.T.
         if .w_POFILOFF='S'
            if EMPTY(.w_POPATALN)
               i_bRes=.F.
               i_bnoChk=.F.
               i_cErrorMsg=Ah_MsgFormat("Campo allegati nominativi obbligatorio")
            endif
            if EMPTY(.w_POPATOFF)
               i_bRes=.F.
               i_bnoChk=.F.
               i_cErrorMsg=Ah_MsgFormat("Campo allegati offerte obbligatorio")
            endif
            If Not(Right(Alltrim(.w_POPATALN),1)='\' And Right(Alltrim(.w_POPATOFF),1)='\')
               i_bRes=.F.
               i_bnoChk=.F.
               i_cErrorMsg=Ah_MsgFormat("I path relativi agli allegati devono terminare con \")
            Endif
         endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_POCODAZI = this.w_POCODAZI
    return

enddefine

* --- Define pages as container
define class tgsof_apoPag1 as StdContainer
  Width  = 595
  height = 356
  stdWidth  = 595
  stdheight = 356
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPOPRIDEF_1_1 as StdField with uid="LCNILDOBHY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_POPRIDEF", cQueryName = "POPRIDEF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit� default offerte",;
    HelpContextID = 10671300,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=168, Top=13, cSayPict='"999999"', cGetPict='"999999"'

  func oPOPRIDEF_1_1.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPOMODOFF_1_2 as StdCheck with uid="TSDHKTOZDB",rtseq=2,rtrep=.f.,left=364, top=9, caption="Blocco offerte inviate",;
    ToolTipText = "Se attivo: le offerte inviate non potranno essere modificate",;
    HelpContextID = 168426300,;
    cFormVar="w_POMODOFF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOMODOFF_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPOMODOFF_1_2.GetRadio()
    this.Parent.oContained.w_POMODOFF = this.RadioValue()
    return .t.
  endfunc

  func oPOMODOFF_1_2.SetRadio()
    this.Parent.oContained.w_POMODOFF=trim(this.Parent.oContained.w_POMODOFF)
    this.value = ;
      iif(this.Parent.oContained.w_POMODOFF=='S',1,;
      0)
  endfunc

  add object oPOMASOFF_1_3 as StdField with uid="HHCZQVIGJS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_POMASOFF", cQueryName = "POMASOFF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura input codici nominativi",;
    HelpContextID = 183237436,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=168, Top=41, InputMask=replicate('X',15)

  func oPOMASOFF_1_3.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPOFLCOOF_1_4 as StdCheck with uid="WOEWOKGIDF",rtseq=4,rtrep=.f.,left=364, top=37, caption="Codifica nominativi numerica",;
    ToolTipText = "Se attivo: i nominativi verranno codificati numericamente",;
    HelpContextID = 101283012,;
    cFormVar="w_POFLCOOF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOFLCOOF_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPOFLCOOF_1_4.GetRadio()
    this.Parent.oContained.w_POFLCOOF = this.RadioValue()
    return .t.
  endfunc

  func oPOFLCOOF_1_4.SetRadio()
    this.Parent.oContained.w_POFLCOOF=trim(this.Parent.oContained.w_POFLCOOF)
    this.value = ;
      iif(this.Parent.oContained.w_POFLCOOF=='S',1,;
      0)
  endfunc

  func oPOFLCOOF_1_4.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPOTIPATT_1_5 as StdField with uid="WCTJGSKBCS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_POTIPATT", cQueryName = "POTIPATT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo attivit� soggetto a prestazioni o con nominativo/commessa obbligatori ",;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 54236342,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=168, Top=69, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_POTIPATT"

  func oPOTIPATT_1_5.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  func oPOTIPATT_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPOTIPATT_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPOTIPATT_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPOTIPATT_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'',this.parent.oContained
  endproc
  proc oPOTIPATT_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_POTIPATT
     i_obj.ecpSave()
  endproc

  add object oPOCODMOD_1_6 as StdField with uid="UOKZWHISGP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_POCODMOD", cQueryName = "POCODMOD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Modello di riferimento",;
    HelpContextID = 133604550,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=168, Top=97, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MOD_OFFE", cZoomOnZoom="GSOF_AMO", oKey_1_1="MOCODICE", oKey_1_2="this.w_POCODMOD"

  func oPOCODMOD_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPOCODMOD_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPOCODMOD_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_OFFE','*','MOCODICE',cp_AbsName(this.parent,'oPOCODMOD_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AMO',"Modelli offerte",'',this.parent.oContained
  endproc
  proc oPOCODMOD_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AMO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_POCODMOD
     i_obj.ecpSave()
  endproc

  add object oPOFLCAAR_1_7 as StdCheck with uid="NEUOLXMEXC",rtseq=7,rtrep=.f.,left=364, top=65, caption="Cartella archiviazione per nominativo",;
    ToolTipText = "Se attivo: viene creata una sottocartella dedicata a ciascun nominativo con livello superiore rispetto all'eventuale periodo.",;
    HelpContextID = 67728568,;
    cFormVar="w_POFLCAAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOFLCAAR_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPOFLCAAR_1_7.GetRadio()
    this.Parent.oContained.w_POFLCAAR = this.RadioValue()
    return .t.
  endfunc

  func oPOFLCAAR_1_7.SetRadio()
    this.Parent.oContained.w_POFLCAAR=trim(this.Parent.oContained.w_POFLCAAR)
    this.value = ;
      iif(this.Parent.oContained.w_POFLCAAR=='S',1,;
      0)
  endfunc

  func oPOFLCAAR_1_7.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPOFILOFF_1_8 as StdCheck with uid="FEBTFMVTTP",rtseq=8,rtrep=.f.,left=8, top=128, caption="Gestione copia files allegati",;
    ToolTipText = "Se attivo � possibile caricare allegati nominativi e offerte di tipo copia file",;
    HelpContextID = 176393020,;
    cFormVar="w_POFILOFF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOFILOFF_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPOFILOFF_1_8.GetRadio()
    this.Parent.oContained.w_POFILOFF = this.RadioValue()
    return .t.
  endfunc

  func oPOFILOFF_1_8.SetRadio()
    this.Parent.oContained.w_POFILOFF=trim(this.Parent.oContained.w_POFILOFF)
    this.value = ;
      iif(this.Parent.oContained.w_POFILOFF=='S',1,;
      0)
  endfunc

  func oPOFILOFF_1_8.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPOCHKIVA_1_9 as StdCheck with uid="GBUGCJDAIU",rtseq=9,rtrep=.f.,left=364, top=128, caption="Controlla partita IVA del nominativo",;
    ToolTipText = "Se attivo, controlla la partita IVA del nominativo in fase di inserimento clienti",;
    HelpContextID = 74603319,;
    cFormVar="w_POCHKIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPOCHKIVA_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPOCHKIVA_1_9.GetRadio()
    this.Parent.oContained.w_POCHKIVA = this.RadioValue()
    return .t.
  endfunc

  func oPOCHKIVA_1_9.SetRadio()
    this.Parent.oContained.w_POCHKIVA=trim(this.Parent.oContained.w_POCHKIVA)
    this.value = ;
      iif(this.Parent.oContained.w_POCHKIVA=='S',1,;
      0)
  endfunc

  func oPOCHKIVA_1_9.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPOPATALN_1_10 as StdField with uid="OVHPBMAVOP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_POPATALN", cQueryName = "POPATALN",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il percorso di archiviazione non � valido",;
    ToolTipText = "Percorso di memorizzazione dei files nominativi di tipo copia file",;
    HelpContextID = 217852740,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=143, Top=160, InputMask=replicate('X',254)

  func oPOPATALN_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_POFILOFF='S')
    endwith
   endif
  endfunc

  func oPOPATALN_1_10.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oBtn_1_11 as StdButton with uid="WPECWEHEVX",left=404, top=160, width=22,height=22,;
    caption="...", nPag=1;
    , HelpContextID = 108586966;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .w_POPATALN=left(cp_getdir(sys(5)+sys(2003),ah_msgformat("Percorso allegati nominativi"))+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_POFILOFF='S')
      endwith
    endif
  endfunc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Isalt())
     endwith
    endif
  endfunc


  add object oPOPERALN_1_12 as StdCombo with uid="HCEKKUMJIW",rtseq=11,rtrep=.f.,left=489,top=160,width=99,height=21;
    , ToolTipText = "Periodo archiviazione allegati nominativi";
    , HelpContextID = 216017732;
    , cFormVar="w_POPERALN",RowSource=""+"Nessuno,"+"Mese,"+"Trimestre,"+"Quadrimestre,"+"Semestre,"+"Anno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPOPERALN_1_12.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'S',;
    iif(this.value =6,'A',;
    space(1))))))))
  endfunc
  func oPOPERALN_1_12.GetRadio()
    this.Parent.oContained.w_POPERALN = this.RadioValue()
    return .t.
  endfunc

  func oPOPERALN_1_12.SetRadio()
    this.Parent.oContained.w_POPERALN=trim(this.Parent.oContained.w_POPERALN)
    this.value = ;
      iif(this.Parent.oContained.w_POPERALN=='N',1,;
      iif(this.Parent.oContained.w_POPERALN=='M',2,;
      iif(this.Parent.oContained.w_POPERALN=='T',3,;
      iif(this.Parent.oContained.w_POPERALN=='Q',4,;
      iif(this.Parent.oContained.w_POPERALN=='S',5,;
      iif(this.Parent.oContained.w_POPERALN=='A',6,;
      0))))))
  endfunc

  func oPOPERALN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_POFILOFF='S')
    endwith
   endif
  endfunc

  func oPOPERALN_1_12.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPOPATOFF_1_13 as StdField with uid="HHYAGNLDGO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_POPATOFF", cQueryName = "POPATOFF",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il percorso di archiviazione non � valido",;
    ToolTipText = "Percorso di memorizzazione dei files offerte di tipo copia file",;
    HelpContextID = 184298300,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=143, Top=188, InputMask=replicate('X',254)

  func oPOPATOFF_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_POFILOFF='S')
    endwith
   endif
  endfunc

  func oPOPATOFF_1_13.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oPOPATOFF_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_POPATOFF) Or  DIRECTORY(FULLPATH(ALLTRIM(.w_POPATOFF))))
    endwith
    return bRes
  endfunc


  add object oBtn_1_14 as StdButton with uid="CQKVYWVZXT",left=404, top=188, width=22,height=22,;
    caption="...", nPag=1;
    , HelpContextID = 108586966;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        .w_POPATOFF=left(cp_getdir(sys(5)+sys(2003),ah_msgformat("Percorso allegati offerte"))+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_POFILOFF='S')
      endwith
    endif
  endfunc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Isalt())
     endwith
    endif
  endfunc


  add object oPOPEROFF_1_15 as StdCombo with uid="UNDHCFPUHW",rtseq=13,rtrep=.f.,left=489,top=188,width=99,height=21;
    , ToolTipText = "Periodo archiviazione allegati offerte";
    , HelpContextID = 182463292;
    , cFormVar="w_POPEROFF",RowSource=""+"Nessuno,"+"Mese,"+"Trimestre,"+"Quadrimestre,"+"Semestre,"+"Anno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPOPEROFF_1_15.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'S',;
    iif(this.value =6,'A',;
    space(1))))))))
  endfunc
  func oPOPEROFF_1_15.GetRadio()
    this.Parent.oContained.w_POPEROFF = this.RadioValue()
    return .t.
  endfunc

  func oPOPEROFF_1_15.SetRadio()
    this.Parent.oContained.w_POPEROFF=trim(this.Parent.oContained.w_POPEROFF)
    this.value = ;
      iif(this.Parent.oContained.w_POPEROFF=='N',1,;
      iif(this.Parent.oContained.w_POPEROFF=='M',2,;
      iif(this.Parent.oContained.w_POPEROFF=='T',3,;
      iif(this.Parent.oContained.w_POPEROFF=='Q',4,;
      iif(this.Parent.oContained.w_POPEROFF=='S',5,;
      iif(this.Parent.oContained.w_POPEROFF=='A',6,;
      0))))))
  endfunc

  func oPOPEROFF_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_POFILOFF='S')
    endwith
   endif
  endfunc

  func oPOPEROFF_1_15.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPOOUTEXP_1_17 as StdField with uid="NWIMOADGSU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_POOUTEXP", cQueryName = "POOUTEXP",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome della cartella contatti in cui esportare i nominativi",;
    HelpContextID = 17832774,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=143, Top=253, InputMask=replicate('X',40)

  func oPOOUTEXP_1_17.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oPOOUTRAG_1_18 as StdCombo with uid="KJAHFSEKZB",rtseq=16,rtrep=.f.,left=489,top=253,width=99,height=21;
    , ToolTipText = "Tipo di raggruppamento in esportazione contatti";
    , HelpContextID = 32498883;
    , cFormVar="w_POOUTRAG",RowSource=""+"Nessuno,"+"Tipo,"+"Localit�,"+"Provincia,"+"Gruppo nominativo,"+"Origine,"+"Operatore,"+"Zona,"+"Agente,"+"Ruolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPOOUTRAG_1_18.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'T',;
    iif(this.value =3,'L',;
    iif(this.value =4,'P',;
    iif(this.value =5,'G',;
    iif(this.value =6,'O',;
    iif(this.value =7,'U',;
    iif(this.value =8,'Z',;
    iif(this.value =9,'A',;
    iif(this.value =10,'R',;
    space(1))))))))))))
  endfunc
  func oPOOUTRAG_1_18.GetRadio()
    this.Parent.oContained.w_POOUTRAG = this.RadioValue()
    return .t.
  endfunc

  func oPOOUTRAG_1_18.SetRadio()
    this.Parent.oContained.w_POOUTRAG=trim(this.Parent.oContained.w_POOUTRAG)
    this.value = ;
      iif(this.Parent.oContained.w_POOUTRAG=='N',1,;
      iif(this.Parent.oContained.w_POOUTRAG=='T',2,;
      iif(this.Parent.oContained.w_POOUTRAG=='L',3,;
      iif(this.Parent.oContained.w_POOUTRAG=='P',4,;
      iif(this.Parent.oContained.w_POOUTRAG=='G',5,;
      iif(this.Parent.oContained.w_POOUTRAG=='O',6,;
      iif(this.Parent.oContained.w_POOUTRAG=='U',7,;
      iif(this.Parent.oContained.w_POOUTRAG=='Z',8,;
      iif(this.Parent.oContained.w_POOUTRAG=='A',9,;
      iif(this.Parent.oContained.w_POOUTRAG=='R',10,;
      0))))))))))
  endfunc

  func oPOOUTRAG_1_18.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPOOUTIMP_1_19 as StdField with uid="CSPCCUPTIL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_POOUTIMP", cQueryName = "POOUTIMP",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome della cartella contatti da cui importare i nominativi",;
    HelpContextID = 183493818,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=143, Top=281, InputMask=replicate('X',40)

  func oPOOUTIMP_1_19.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oObj_1_20 as cp_runprogram with uid="QNFMWJINQX",left=8, top=405, width=233,height=19,;
    caption='GSAR_BAM(modifica)',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("modifica")',;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 71840346


  add object oBtn_1_21 as StdButton with uid="SXVVCXGXSY",left=487, top=297, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 108406502;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="MTRWQEGJEF",left=538, top=297, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 105764602;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESMOD_1_39 as StdField with uid="SGCGWPCXUC",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESMOD", cQueryName = "DESMOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione modello offerta",;
    HelpContextID = 4698058,;
   bGlobalFont=.t.,;
    Height=21, Width=349, Left=238, Top=97, InputMask=replicate('X',35)

  add object oStr_1_22 as StdString with uid="BFLNHWFYPX",Visible=.t., Left=3, Top=41,;
    Alignment=1, Width=162, Height=18,;
    Caption="Struttura nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="LFDFXERCEE",Visible=.t., Left=27, Top=160,;
    Alignment=1, Width=114, Height=18,;
    Caption="Allegati nominativi:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="YHVONASLPX",Visible=.t., Left=32, Top=188,;
    Alignment=1, Width=109, Height=18,;
    Caption="Allegati offerte:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="QHPJGPSZDG",Visible=.t., Left=432, Top=188,;
    Alignment=1, Width=54, Height=18,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="TBGBUHGCVJ",Visible=.t., Left=432, Top=160,;
    Alignment=1, Width=54, Height=18,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="LBVTQHUBNF",Visible=.t., Left=34, Top=13,;
    Alignment=1, Width=131, Height=18,;
    Caption="Priorit� default:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="UVDJCGFASA",Visible=.t., Left=8, Top=225,;
    Alignment=0, Width=157, Height=18,;
    Caption="Import/export contatti"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="KKJPEOJOKX",Visible=.t., Left=3, Top=253,;
    Alignment=1, Width=138, Height=18,;
    Caption="Cartella export Outlook:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="BXWBEPAJST",Visible=.t., Left=6, Top=281,;
    Alignment=1, Width=135, Height=18,;
    Caption="Cartella import Outlook:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="KDTKMPRGMS",Visible=.t., Left=380, Top=253,;
    Alignment=1, Width=106, Height=18,;
    Caption="Raggruppamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="LVBLDQVWXV",Visible=.t., Left=34, Top=71,;
    Alignment=1, Width=131, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="HHOQGKDUCD",Visible=.t., Left=85, Top=100,;
    Alignment=1, Width=80, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oBox_1_29 as StdBox with uid="CXIUCUFRQK",left=4, top=151, width=586,height=2

  add object oBox_1_30 as StdBox with uid="TZPHGFNRLF",left=4, top=244, width=586,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_apo','PAR_OFFE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".POCODAZI=PAR_OFFE.POCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
