* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_bi1m                                                       *
*              Eventi da import kit promo                                      *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-27                                                      *
* Last revis.: 2005-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsof_bi1m",oParentObject,m.pEvent)
return(i_retval)

define class tgsof_bi1m as StdBatch
  * --- Local variables
  pEvent = space(3)
  w_ZOOM = space(10)
  RECSEL = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue eventi da Selezione/Spostamento Riga Master (da GSOF_KIM)
    * --- Evento: MC - Master Row Checked , MU - Master Row Checked
    * --- BQD - Before Query Detail , AQD - After Query Detail
    ND = this.oParentObject.w_ZoomDett.cCursor
    NM = this.oParentObject.w_ZoomMast.cCursor
    this.w_ZOOM = this.oParentObject.w_ZoomMast
    this.RECSEL = 0
    * --- Esegue il Check e l' Uncheck sul Master
    do case
      case this.pEvent = "MRC"
        * --- Controlla che non vi siano + kit selezionati
        SELECT &NM
        GO TOP
        SCAN FOR XCHK<>0
        if this.RECSEL = 0
          if XCHK=1
            this.RECSEL = 1
            this.oParentObject.NotifyEvent("CalcRig")
            * --- Inserisce il Flag Selezionato sulle righe del Detail 
            if USED(this.oParentObject.w_ZoomDett.cCursor)
              UPDATE (ND) SET XCHK=1
            endif
          endif
        else
          if XCHK=1
            ah_errormsg("� possibile selezionare solo un kit per volta")
            AA=ALLTRIM(STR(this.oParentObject.w_ZoomMast.grd.ColumnCount))
            this.oParentObject.w_ZoomMast.grd.Column&AA..chk.Value = 0
            i_retcode = 'stop'
            return
          endif
        endif
        ENDSCAN
      case this.pEvent = "MRU"
        * --- Eseguito l' Uncheck sul Master
        * --- Toglie il Flag Selezionato sulle righe del Detail 
        if USED(this.oParentObject.w_ZoomDett.cCursor)
          UPDATE (ND) SET XCHK=0
        endif
      case this.pEvent = "DRC"
        * --- Eseguito il Check sul Dettaglio
      case this.pEvent = "DRU"
        * --- Eseguito l' Uncheck sul Dettaglio
      case this.pEvent = "DBQ"
        * --- Evento Before Query sul Dettaglio
      case this.pEvent = "DAQ"
        * --- Evento After Query sul Dettaglio
      case this.pEvent = "SCH"
        * --- Evento w_SELEZI Changed
        * --- Seleziona/Deselezione la Righe Dettaglio 
        if USED(this.oParentObject.w_ZoomDett.cCursor)
          if this.oParentObject.w_ZoomMast.GetVar("XCHK") = 1
            * --- Selezionato il Documento Master
            if this.oParentObject.w_SELEZI = "S"
              * --- Seleziona Tutto
              UPDATE (ND) SET XCHK=1
            else
              * --- deseleziona Tutto
              UPDATE (ND) SET XCHK=0
            endif
          endif
        endif
    endcase
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,pEvent)
    this.pEvent=pEvent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvent"
endproc
