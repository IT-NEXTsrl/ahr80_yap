* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsof_asf                                                        *
*              Sottogruppi articoli/servizi                                    *
*                                                                              *
*      Author: Maurizio Rossetti                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2008-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsof_asf"))

* --- Class definition
define class tgsof_asf as StdForm
  Top    = 12
  Left   = 54

  * --- Standard Properties
  Width  = 442
  Height = 160+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-26"
  HelpContextID=109717865
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  SOT_SEZI_IDX = 0
  TIT_SEZI_IDX = 0
  cFile = "SOT_SEZI"
  cKeySelect = "SGCODGRU,SGCODSOT"
  cKeyWhere  = "SGCODGRU=this.w_SGCODGRU and SGCODSOT=this.w_SGCODSOT"
  cKeyWhereODBC = '"SGCODGRU="+cp_ToStrODBC(this.w_SGCODGRU)';
      +'+" and SGCODSOT="+cp_ToStrODBC(this.w_SGCODSOT)';

  cKeyWhereODBCqualified = '"SOT_SEZI.SGCODGRU="+cp_ToStrODBC(this.w_SGCODGRU)';
      +'+" and SOT_SEZI.SGCODSOT="+cp_ToStrODBC(this.w_SGCODSOT)';

  cPrg = "gsof_asf"
  cComment = "Sottogruppi articoli/servizi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SGCODGRU = space(5)
  w_SGDESGRU = space(35)
  w_SGCODSOT = space(5)
  w_SGDESCRI = space(35)
  w_SG__NOTE = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SOT_SEZI','gsof_asf')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsof_asfPag1","gsof_asf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Sottogruppo articolo/servizio")
      .Pages(1).HelpContextID = 35636986
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSGCODGRU_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIT_SEZI'
    this.cWorkTables[2]='SOT_SEZI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SOT_SEZI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SOT_SEZI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SGCODGRU = NVL(SGCODGRU,space(5))
      .w_SGCODSOT = NVL(SGCODSOT,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SOT_SEZI where SGCODGRU=KeySet.SGCODGRU
    *                            and SGCODSOT=KeySet.SGCODSOT
    *
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SOT_SEZI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SOT_SEZI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SOT_SEZI '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SGCODGRU',this.w_SGCODGRU  ,'SGCODSOT',this.w_SGCODSOT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SGDESGRU = space(35)
        .w_SGCODGRU = NVL(SGCODGRU,space(5))
          if link_1_1_joined
            this.w_SGCODGRU = NVL(TSCODICE101,NVL(this.w_SGCODGRU,space(5)))
            this.w_SGDESGRU = NVL(TSDESCRI101,space(35))
          else
          .link_1_1('Load')
          endif
        .w_SGCODSOT = NVL(SGCODSOT,space(5))
        .w_SGDESCRI = NVL(SGDESCRI,space(35))
        .w_SG__NOTE = NVL(SG__NOTE,space(0))
        cp_LoadRecExtFlds(this,'SOT_SEZI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SGCODGRU = space(5)
      .w_SGDESGRU = space(35)
      .w_SGCODSOT = space(5)
      .w_SGDESCRI = space(35)
      .w_SG__NOTE = space(0)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_SGCODGRU))
          .link_1_1('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'SOT_SEZI')
    this.DoRTCalc(2,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSGCODGRU_1_1.enabled = i_bVal
      .Page1.oPag.oSGCODSOT_1_4.enabled = i_bVal
      .Page1.oPag.oSGDESCRI_1_6.enabled = i_bVal
      .Page1.oPag.oSG__NOTE_1_7.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSGCODGRU_1_1.enabled = .f.
        .Page1.oPag.oSGCODSOT_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSGCODGRU_1_1.enabled = .t.
        .Page1.oPag.oSGCODSOT_1_4.enabled = .t.
        .Page1.oPag.oSGDESCRI_1_6.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SOT_SEZI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SGCODGRU,"SGCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SGCODSOT,"SGCODSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SGDESCRI,"SGDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SG__NOTE,"SG__NOTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    i_lTable = "SOT_SEZI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SOT_SEZI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SOT_SEZI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SOT_SEZI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SOT_SEZI')
        i_extval=cp_InsertValODBCExtFlds(this,'SOT_SEZI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SGCODGRU,SGCODSOT,SGDESCRI,SG__NOTE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_SGCODGRU)+;
                  ","+cp_ToStrODBC(this.w_SGCODSOT)+;
                  ","+cp_ToStrODBC(this.w_SGDESCRI)+;
                  ","+cp_ToStrODBC(this.w_SG__NOTE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SOT_SEZI')
        i_extval=cp_InsertValVFPExtFlds(this,'SOT_SEZI')
        cp_CheckDeletedKey(i_cTable,0,'SGCODGRU',this.w_SGCODGRU,'SGCODSOT',this.w_SGCODSOT)
        INSERT INTO (i_cTable);
              (SGCODGRU,SGCODSOT,SGDESCRI,SG__NOTE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SGCODGRU;
                  ,this.w_SGCODSOT;
                  ,this.w_SGDESCRI;
                  ,this.w_SG__NOTE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SOT_SEZI_IDX,i_nConn)
      *
      * update SOT_SEZI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SOT_SEZI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SGDESCRI="+cp_ToStrODBC(this.w_SGDESCRI)+;
             ",SG__NOTE="+cp_ToStrODBC(this.w_SG__NOTE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SOT_SEZI')
        i_cWhere = cp_PKFox(i_cTable  ,'SGCODGRU',this.w_SGCODGRU  ,'SGCODSOT',this.w_SGCODSOT  )
        UPDATE (i_cTable) SET;
              SGDESCRI=this.w_SGDESCRI;
             ,SG__NOTE=this.w_SG__NOTE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SOT_SEZI_IDX,i_nConn)
      *
      * delete SOT_SEZI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SGCODGRU',this.w_SGCODGRU  ,'SGCODSOT',this.w_SGCODSOT  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SGCODGRU
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIT_SEZI_IDX,3]
    i_lTable = "TIT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2], .t., this.TIT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SGCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATS',True,'TIT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TSCODICE like "+cp_ToStrODBC(trim(this.w_SGCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TSCODICE',trim(this.w_SGCODGRU))
          select TSCODICE,TSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SGCODGRU)==trim(_Link_.TSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SGCODGRU) and !this.bDontReportError
            deferred_cp_zoom('TIT_SEZI','*','TSCODICE',cp_AbsName(oSource.parent,'oSGCODGRU_1_1'),i_cWhere,'GSOF_ATS',"Gruppi articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',oSource.xKey(1))
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SGCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(this.w_SGCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',this.w_SGCODGRU)
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SGCODGRU = NVL(_Link_.TSCODICE,space(5))
      this.w_SGDESGRU = NVL(_Link_.TSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SGCODGRU = space(5)
      endif
      this.w_SGDESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.TSCODICE,1)
      cp_ShowWarn(i_cKey,this.TIT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SGCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIT_SEZI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.TSCODICE as TSCODICE101"+ ",link_1_1.TSDESCRI as TSDESCRI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on SOT_SEZI.SGCODGRU=link_1_1.TSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and SOT_SEZI.SGCODGRU=link_1_1.TSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSGCODGRU_1_1.value==this.w_SGCODGRU)
      this.oPgFrm.Page1.oPag.oSGCODGRU_1_1.value=this.w_SGCODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oSGDESGRU_1_2.value==this.w_SGDESGRU)
      this.oPgFrm.Page1.oPag.oSGDESGRU_1_2.value=this.w_SGDESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oSGCODSOT_1_4.value==this.w_SGCODSOT)
      this.oPgFrm.Page1.oPag.oSGCODSOT_1_4.value=this.w_SGCODSOT
    endif
    if not(this.oPgFrm.Page1.oPag.oSGDESCRI_1_6.value==this.w_SGDESCRI)
      this.oPgFrm.Page1.oPag.oSGDESCRI_1_6.value=this.w_SGDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSG__NOTE_1_7.value==this.w_SG__NOTE)
      this.oPgFrm.Page1.oPag.oSG__NOTE_1_7.value=this.w_SG__NOTE
    endif
    cp_SetControlsValueExtFlds(this,'SOT_SEZI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SGCODGRU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSGCODGRU_1_1.SetFocus()
            i_bnoObbl = !empty(.w_SGCODGRU)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsof_asfPag1 as StdContainer
  Width  = 438
  height = 160
  stdWidth  = 438
  stdheight = 160
  resizeXpos=299
  resizeYpos=96
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSGCODGRU_1_1 as StdField with uid="AKMLWZXKKE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SGCODGRU", cQueryName = "SGCODGRU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo di appartenenza",;
    HelpContextID = 84497275,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=89, Top=13, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIT_SEZI", cZoomOnZoom="GSOF_ATS", oKey_1_1="TSCODICE", oKey_1_2="this.w_SGCODGRU"

  func oSGCODGRU_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSGCODGRU_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSGCODGRU_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIT_SEZI','*','TSCODICE',cp_AbsName(this.parent,'oSGCODGRU_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATS',"Gruppi articoli",'',this.parent.oContained
  endproc
  proc oSGCODGRU_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TSCODICE=this.parent.oContained.w_SGCODGRU
     i_obj.ecpSave()
  endproc

  add object oSGDESGRU_1_2 as StdField with uid="LTEPORKYVH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SGDESGRU", cQueryName = "SGDESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 99574651,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=156, Top=13, InputMask=replicate('X',35)

  add object oSGCODSOT_1_4 as StdField with uid="STORWGYSUJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SGCODSOT", cQueryName = "SGCODGRU,SGCODSOT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sottogruppo",;
    HelpContextID = 17388410,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=89, Top=42, InputMask=replicate('X',5)

  add object oSGDESCRI_1_6 as StdField with uid="VBEJRTKPHZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SGDESCRI", cQueryName = "SGDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 32465775,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=156, Top=42, InputMask=replicate('X',35)

  add object oSG__NOTE_1_7 as StdMemo with uid="UKTANPQNFW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SG__NOTE", cQueryName = "SG__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 230364011,;
   bGlobalFont=.t.,;
    Height=81, Width=340, Left=89, Top=73

  add object oStr_1_3 as StdString with uid="EGRXQRIIKV",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=82, Height=18,;
    Caption="Gruppo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="TVYQSMSCDC",Visible=.t., Left=3, Top=42,;
    Alignment=1, Width=82, Height=18,;
    Caption="Sottogruppo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="EXKAZDZLHX",Visible=.t., Left=3, Top=73,;
    Alignment=1, Width=82, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsof_asf','SOT_SEZI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SGCODGRU=SOT_SEZI.SGCODGRU";
  +" and "+i_cAliasName2+".SGCODSOT=SOT_SEZI.SGCODSOT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
