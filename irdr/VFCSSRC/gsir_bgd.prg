* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bgd                                                        *
*              Generazione dati InfoPublisher                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_209]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-05                                                      *
* Last revis.: 2016-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pQRY_GRP,pFLQRYGRP,pMessage,pFILTRATA,pGIROWNUM,pZOOM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bgd",oParentObject,m.pQRY_GRP,m.pFLQRYGRP,m.pMessage,m.pFILTRATA,m.pGIROWNUM,m.pZOOM)
return(i_retval)

define class tgsir_bgd as StdBatch
  * --- Local variables
  pQRY_GRP = space(20)
  pFLQRYGRP = space(1)
  pMessage = .f.
  pFILTRATA = .f.
  pGIROWNUM = 0
  pZOOM = .f.
  w_IBPOCX = .NULL.
  w_CHIAVE = space(20)
  w_SendAdd = space(254)
  w_Server = space(254)
  w_Port = 0
  w_login = space(254)
  w_psw = space(254)
  w_AMMODCON = space(5)
  w_CPERR = .f.
  w_SYSPATH = space(254)
  w_CODGRP = space(20)
  w_COPATHCP = space(254)
  w_objFSO = .NULL.
  w_VERSION = space(15)
  w_NEED7ZIP = .f.
  w_ERROR = .f.
  w_ALLERROR = .f.
  w_NUM_RIGA = 0
  w_AQQUEFIL = space(1)
  w_AQDES = space(254)
  w_AQQRPATH = space(254)
  w_AQFORTYP = space(10)
  w_AQDTFILE = space(254)
  w_AQADOCS = space(254)
  w_AQIMPATH = space(254)
  w_AQLOG = space(254)
  w_AQFLGZCP = space(1)
  w_AQCONTYP = space(15)
  w_AQFLJOIN = space(1)
  w_AQCURLOC = space(1)
  w_AQCURTYP = space(20)
  w_AQENGTYP = space(1)
  w_AQTIMOUT = 0
  w_AQCMDTMO = 0
  w_AQCRIPTE = space(1)
  w_nConn = 0
  pd_KeyIstanza = space(254)
  pd_StrProcesso = space(254)
  w_IRDR_MR = .NULL.
  w_DIR = space(254)
  w_TMPFILE = space(254)
  w_DTFILE = space(254)
  w_DTPATH = space(254)
  w_FIFLNAME = space(30)
  w_FITIPFIL = space(8)
  w_FIFILTRO = space(254)
  L_FILTRO = space(254)
  w_GIPROGRA = space(254)
  w_EXT = space(5)
  L_FirstPos = 0
  L_LastPos = 0
  w_VARIABILE = space(254)
  w_OBJECT = .NULL.
  w_NUMERO = 0
  w_CURSORE = space(15)
  w_TMPFILE = space(254)
  w_TMPDIR7ZIP = space(254)
  w_FILENAME = space(254)
  w_UTSERVCP = space(1)
  w_DVPATHFI2 = space(100)
  w_DVCODELA = 0
  w_DVPATHFI = space(100)
  w_DEDESFIL = space(100)
  w_DVDESELA = space(50)
  w_DV__NOTE = space(0)
  w_DVDATCRE = ctod("  /  /  ")
  w_DV_PARAM = space(0)
  w_DERISERV = space(1)
  w_DEFOLDER = 0
  w_DETIPDES = space(1)
  w_DECODAGE = space(5)
  w_DETIPCON = space(1)
  w_DECODCON = space(15)
  w_FO__PATH = space(256)
  w_FO__TIPO = space(1)
  w_PFITYPE = space(5)
  w_PFIPKEY = space(50)
  w_FLERRFLD = .f.
  w_FILEXML = space(254)
  w_NHF = 0
  w_TMPC = space(250)
  w_TMPN = 0
  aFP_Name = 0
  aFP_Value = 0
  aFP_HasValue = 0
  aFL_Name = 0
  aFL_Type = 0
  aFL_Value = 0
  aFL_Remove = 0
  aFL_Azienda = 0
  w_NumParameter = 0
  w_NumFilter = 0
  w_VARNAME = space(20)
  w_DESVAR = space(50)
  w_TIPVAR = space(1)
  w_LENVAR = 0
  w_DECVAR = 0
  w_FIFILTROSUM = space(0)
  w_FIFLGREM = space(1)
  w_FIFLGAZI = space(1)
  w_Mask = .NULL.
  i = 0
  j = 0
  a_FP_Macro = .NULL.
  a_FParameter = .NULL.
  w_FIVISMSK = space(254)
  w_MASK = space(15)
  w_CPROWNUM = 0
  w_oMESS = .NULL.
  * --- WorkFile variables
  INF_AQM_idx=0
  INF_AQD_idx=0
  UTE_NTI_idx=0
  INF_PZCP_idx=0
  ZFOLDERS_idx=0
  ZDESTIN_idx=0
  INF_GRQD_idx=0
  INF_AFIM_idx=0
  INF_AGIM_idx=0
  INFPARFI_idx=0
  AUT_LPE_idx=0
  INF_FCPZ_idx=0
  CONTROPA_idx=0
  ACC_MAIL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pubblicazione Dati InfoPublisher
    *     Chiamata da GSIR_KGD / GSIR_BIR 
    * --- pQRY_GRP    : Contiene il codice query o il codice gruppo
    *     pFLQRYGRP  : Specifica se pQRY_GRP rappresenta una query o un gruppo
    *     pMessage       : Se true visualizza il messaggio di notifica, i messaggi di errore
    *                               vengono sempre mostrati
    *     pFILTRATA    : Tipo query .t. = Filtrata, .f. Non filtrata
    *     pGIROWNUM: CPROWNUM della gestione, utilizzato per recuperare i filtri
    *     pZOOM          : Se true la query � stata lanciata da uno zoom
    if g_IZCP$"SA"
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COPATHDO"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COPATHDO;
          from (i_cTable) where;
              COCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COPATHCP = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if ! Directory(this.w_COPATHCP, 1)
        MakeDir(this.w_COPATHCP)
      endif
    endif
    this.w_CPERR = .f.
    if Directory(GETENV("WINDIR")+ "\SysWOW64\", 1)
      * --- Ricavo il path della cartella SYSWOW64
      this.w_SYSPATH=GETENV("WINDIR")+ "\SysWOW64\"
    else
      * --- Ricavo il path della cartella SYSTEM32
      this.w_SYSPATH=GETENV("WINDIR")+ "\SYSTEM32\"
    endif
    * --- Verifico la presenza degli OCX nella cartella SYSTEM32
    if FILE(this.w_SYSPATH+"IBPublisher20.ocx")
      * --- OCX trovati - determino la versione
      this.w_objFSO=createobject("Scripting.FileSystemObject")
      this.w_VERSION = this.w_objFSO.GetFileVersion(this.w_SYSPATH+"IBPublisher20.ocx")
      this.w_objFSO = .NULL.
      * --- Se la versione dell' OCX � uguale o maggiore di 2.7.1.0 devo utilizzare 7Zip
      *     per gestire output multipli in caso di XLS, HTML, ecc...
      this.w_NEED7ZIP = this.w_VERSION >= "2.7.1.0"
      if g_INFO="S" AND NOT GSIN_BCB(this,"C")
        i_retcode = 'stop'
        return
      endif
      * --- Cursore per salvare le query che non possono essere pubblicate sul portale
      *     perch� l'utente non � abilitato
      CREATE CURSOR MsgErr (CODICE C(20), DES C(254))
      * --- Determino se devo pubblicare un gruppo o una query singola
      this.w_ERROR = .f.
      this.w_ALLERROR = .f.
      if this.pFLQRYGRP = "G"
        * --- Seleziono le query di InfoPublisher appartenenti al gruppo scelto
        *     Se non viene specificato nessun gruppo pubblica tutte le query
        *     di tutti i gruppi.
        *     Vengono pubblicate solo le query che hanno il check w_AQQUEFIL <> 'S'
        UPDATE (this.oParentObject.w_ZOOMQRY.ccursor) SET QEX=" " 
 Select (this.oParentObject.w_ZOOMQRY.ccursor) 
 Go Top 
 this.oParentObject.Refresh()
        this.w_CODGRP = this.pQRY_GRP
        * --- Select from GSIR2BGD
        do vq_exec with 'GSIR2BGD',this,'_Curs_GSIR2BGD','',.f.,.t.
        if used('_Curs_GSIR2BGD')
          select _Curs_GSIR2BGD
          locate for 1=1
          do while not(eof())
          this.w_CHIAVE = _Curs_GSIR2BGD.GQCODQUE
          this.w_NUM_RIGA = _Curs_GSIR2BGD.CPROWORD
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          UPDATE (this.oParentObject.w_ZOOMQRY.ccursor) SET QEX=iif(this.w_ERROR,"N","S") where this.w_NUM_RIGA=CPROWORD 
 this.oParentObject.Refresh()
          this.w_ERROR = .f.
            select _Curs_GSIR2BGD
            continue
          enddo
          use
        endif
      else
        * --- Pubblico la singola query
        this.w_CHIAVE = this.pQRY_GRP
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_ALLERROR
        ah_ErrorMsg("Operazione effettuata con errori. Verificare eventuali problemi sul file di log","!","")
      else
        * --- Se pMessage � true mostro il messaggio
        if this.pMessage
          ah_ErrorMsg("Operazione terminata","i","")
        endif
      endif
      * --- Messaggistica di errore Corporate Portal
      if this.w_CPERR and ah_YesNo("L'utente non � abilitato a pubblicare query su Zucchetti Corporate Portal.%0si desidera stampare l'elenco delle query non pubblicate sul portale?")
        SELECT * FROM MsgErr INTO CURSOR __TMP__
        CP_CHPRN("..\IRDR\EXE\QUERY\GSIR_BGD.FRX", " ", this)
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- OCX non presenti nella cartella SYSTEM32
      ah_ErrorMsg("Attenzione: componenti ocx non installati.%0utilizzare la voce di men� <installazione componenti>","!","")
    endif
    * --- Ripristino blocco
    if g_INFO="S"
      GSIN_BCB(this," ")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzera temporanei
    USE IN SELECT("__pdtmp__")
    USE IN SELECT("__tmp__")
    USE IN SELECT("doctmp")
    USE IN SELECT("MsgErr")
    USE IN SELECT("GENERA")
    this.w_IRDR_MR = .null.
    w_IRDR_RET = .null.
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pubblica query
    * --- Gestione errori
     
 L_errsav=on("ERROR") 
 Messaggio=" " 
 ON ERROR Messaggio="Errore OCX"
    * --- Creo l'oggetto InfoPublisher
    this.w_IBPOCX = createobject("IBPublisher20.IBPublisher")
    ON ERROR &L_errsav
    if Messaggio="Errore OCX"
      ah_ErrorMsg("Attenzione: impossibile creare oggetto ibpublisher20.ibpublisher%0utilizzare la voce di men� <installazione componenti>","!","")
      i_retcode = 'stop'
      return
    endif
    USE IN SELECT("__pdtmp__")
    USE IN SELECT("__tmp__")
    USE IN SELECT("doctmp")
    * --- Leggo le informazioni della query
    * --- Read from INF_AQM
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_AQM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AQM_idx,2],.t.,this.INF_AQM_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AQDES,AQQRPATH,AQDTFILE,AQIMPATH,AQLOG,AQADOCS,AQFLGZCP,AQFORTYP,AQCONTYP,AQCURLOC,AQENGTYP,AQTIMOUT,AQCMDTMO,AQQUEFIL,AQCRIPTE,AQCURTYP,AQFLJOIN"+;
        " from "+i_cTable+" INF_AQM where ";
            +"AQCODICE = "+cp_ToStrODBC(this.w_CHIAVE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AQDES,AQQRPATH,AQDTFILE,AQIMPATH,AQLOG,AQADOCS,AQFLGZCP,AQFORTYP,AQCONTYP,AQCURLOC,AQENGTYP,AQTIMOUT,AQCMDTMO,AQQUEFIL,AQCRIPTE,AQCURTYP,AQFLJOIN;
        from (i_cTable) where;
            AQCODICE = this.w_CHIAVE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AQDES = NVL(cp_ToDate(_read_.AQDES),cp_NullValue(_read_.AQDES))
      this.w_AQQRPATH = NVL(cp_ToDate(_read_.AQQRPATH),cp_NullValue(_read_.AQQRPATH))
      this.w_AQDTFILE = NVL(cp_ToDate(_read_.AQDTFILE),cp_NullValue(_read_.AQDTFILE))
      this.w_AQIMPATH = NVL(cp_ToDate(_read_.AQIMPATH),cp_NullValue(_read_.AQIMPATH))
      this.w_AQLOG = NVL(cp_ToDate(_read_.AQLOG),cp_NullValue(_read_.AQLOG))
      this.w_AQADOCS = NVL(cp_ToDate(_read_.AQADOCS),cp_NullValue(_read_.AQADOCS))
      this.w_AQFLGZCP = NVL(cp_ToDate(_read_.AQFLGZCP),cp_NullValue(_read_.AQFLGZCP))
      this.w_AQFORTYP = NVL(cp_ToDate(_read_.AQFORTYP),cp_NullValue(_read_.AQFORTYP))
      this.w_AQCONTYP = NVL(cp_ToDate(_read_.AQCONTYP),cp_NullValue(_read_.AQCONTYP))
      this.w_AQCURLOC = NVL(cp_ToDate(_read_.AQCURLOC),cp_NullValue(_read_.AQCURLOC))
      this.w_AQENGTYP = NVL(cp_ToDate(_read_.AQENGTYP),cp_NullValue(_read_.AQENGTYP))
      this.w_AQTIMOUT = NVL(cp_ToDate(_read_.AQTIMOUT),cp_NullValue(_read_.AQTIMOUT))
      this.w_AQCMDTMO = NVL(cp_ToDate(_read_.AQCMDTMO),cp_NullValue(_read_.AQCMDTMO))
      this.w_AQQUEFIL = NVL(cp_ToDate(_read_.AQQUEFIL),cp_NullValue(_read_.AQQUEFIL))
      this.w_AQCRIPTE = NVL(cp_ToDate(_read_.AQCRIPTE),cp_NullValue(_read_.AQCRIPTE))
      this.w_AQCURTYP = NVL(cp_ToDate(_read_.AQCURTYP),cp_NullValue(_read_.AQCURTYP))
      this.w_AQFLJOIN = NVL(cp_ToDate(_read_.AQFLJOIN),cp_NullValue(_read_.AQFLJOIN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_nConn = i_TableProp[this.INF_AQM_idx,3]
    this.w_AQADOCS = iif(empty(nvl(this.w_AQADOCS,"")), 'Provider=MSDASQL.1;Persist Security Info=False;Extended Properties="'+sqlgetprop(this.w_nConn,"ConnectString")+'"', this.w_AQADOCS)
    if type("this.oParentObject.w_NOMEFILE")="C" and not empty(this.oParentObject.w_NOMEFILE)
      do case
        case lower(RIGHT(ALLTRIM(this.oParentObject.w_NOMEFILE),4))=".csv"
          this.w_AQFORTYP = "efCSV"
        case lower(RIGHT(ALLTRIM(this.oParentObject.w_NOMEFILE),4))=".rtf"
          this.w_AQFORTYP = "efRTF"
        case lower(RIGHT(ALLTRIM(this.oParentObject.w_NOMEFILE),4))=".htm"
          this.w_AQFORTYP = "efHTML"
        case lower(RIGHT(ALLTRIM(this.oParentObject.w_NOMEFILE),5))=".xlsx"
          this.w_AQFORTYP = "efExcel"
        case lower(RIGHT(ALLTRIM(this.oParentObject.w_NOMEFILE),4))=".pdf"
          this.w_AQFORTYP = "efPDF"
        otherwise
          this.w_AQFORTYP = "efQuery"
      endcase
      this.oParentObject.w_FORTYP = this.w_AQFORTYP
    endif
    * --- ------------------------------------------------------------------------------------------------------
    *     Controllo se c'� un processo documentale
    this.pd_StrProcesso = " "
    if g_DOCM="S" and (UPPER(this.oParentObject.class)<>"TGSIR_BIR" or (UPPER(this.oParentObject.class)="TGSIR_BIR" and this.oParentObject.w_PROCESSO_DOC))
      VQ_EXEC("..\irdr\exe\query\GSIR_BGD_PRO.VQR",this,"ProcessoDoc")
      if USED("ProcessoDoc")
        Select ProcessoDoc 
 go top
        if ProcessoDoc.NUM_PRO>0
          this.pd_KeyIstanza = padl(alltrim(str(i_codute,5,0)),5,"_")+sys(2015)
          this.w_IRDR_MR=createobject("MultiReport")
          this.w_IRDR_MR.AddNewReport(this.w_CHIAVE, "","",.null.,.null.,,.t.)     
          w_IRDR_RET = .null.
          this.pd_StrProcesso = CHKPROCD(this.w_IRDR_MR,@w_IRDR_RET, this.pd_KeyIstanza,this.oParentObject, .t.)
        endif
        Select ProcessoDoc 
 Use
      else
        this.pd_StrProcesso = " "
      endif
    endif
    * --- ------------------------------------------------------------------------------------------------------
    * --- Gestione degli errori
     
 L_errsav=on("ERROR") 
 Messaggio=" " 
 ON ERROR Messaggio="Errore"
    * --- Controllo l'esistenza delle cartelle di log e di output se non ci sono le creo
    this.w_DIR = AddBs(JustPath(alltrim(this.w_AQDTFILE)))
    if NOT DIRECTORY(this.w_DIR, 1)
      MakeDir(this.w_DIR)
    endif
    this.w_DIR = AddBs(JustPath(alltrim(this.w_AQLOG)))
    if NOT DIRECTORY(this.w_DIR, 1)
      MakeDir(this.w_DIR)
    endif
    * --- Carico le propriet� dell'OCX.
    this.w_IBPOCX.Description = this.w_AQDES
    this.w_TMPFILE = ""
    if LEFT(ALLTRIM(this.w_AQQRPATH),3)="..\"
      this.w_TMPFILE = RIGHT(ALLTRIM(this.w_AQQRPATH),LEN(ALLTRIM(this.w_AQQRPATH))-3)
    endif
    if NOT EMPTY(this.w_TMPFILE)
      this.w_TMPFILE = cp_getStdFile(this.w_TMPFILE,"")
    endif
    this.w_IBPOCX.QueryPathname = IIF(EMPTY(this.w_TMPFILE), this.w_AQQRPATH, this.w_TMPFILE)
    this.w_IBPOCX.ExportFormat = IIF( EMPTY( this.w_AQFORTYP ) , "efQuery", this.w_AQFORTYP )
    this.w_TMPFILE = ""
    if LEFT(ALLTRIM(this.w_AQIMPATH),3)="..\"
      this.w_TMPFILE = RIGHT(ALLTRIM(this.w_AQIMPATH),LEN(ALLTRIM(this.w_AQIMPATH))-3)
    endif
    * --- Cerco infomart personalizzati
    if NOT EMPTY(this.w_TMPFILE)
      this.w_TMPFILE = cp_getStdFile(this.w_TMPFILE,"")
    endif
    this.w_IBPOCX.InfomartPathname = IIF(EMPTY(this.w_TMPFILE), this.w_AQIMPATH, this.w_TMPFILE)
    this.w_IBPOCX.LogPathname = this.w_AQLOG
    this.w_IBPOCX.ADOConnString = iif(NVL(this.w_AQCRIPTE,"N")="C",CifraCnf( ALLTRIM(this.w_AQADOCS) , "D" ),this.w_AQADOCS)
    this.w_IBPOCX.ConnType = IIF( EMPTY( this.w_AQCONTYP ), CP_DBTYPE, this.w_AQCONTYP)
    this.w_IBPOCX.ConnCursorLocation = IIF( this.w_AQCURLOC = "C", "clUseClient", "clUseServer")
    this.w_IBPOCX.ConnCursorType = IIF( EMPTY( this.w_AQCURTYP ), "ctOpenForwardOnly", this.w_AQCURTYP )
    this.w_IBPOCX.ConnTimeout = this.w_AQTIMOUT
    this.w_IBPOCX.ConnCustomProperties = IIF(this.w_AQFLJOIN="S","UseWhereForJoins=True","UseWhereForJoins=False")
    this.w_IBPOCX.AdvancedLog = iif(this.oParentObject.w_LOG="S", .t., .f.)
    this.w_IBPOCX.ShowErrors = .f.
    do CHKINFO with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Visualizzo wait window
    ah_Msg("Elaborazione query in corso...",.T.)
    * --- Discrimino tra Query Filtrate e no
    if this.pFILTRATA or not empty(this.pd_StrProcesso)
      * --- Query Filtrata
      * --- Determino un file di destinazione temporaneo
      * --- Ricavo l'estensione sulla base di w_AQFORTYP
      do case
        case this.w_AQFORTYP = "efQuery"
          this.w_EXT = ".IRP"
        case this.w_AQFORTYP = "efRTF"
          this.w_EXT = ".RTF"
        case this.w_AQFORTYP = "efExcel"
          this.w_EXT = ".XLSX"
        case this.w_AQFORTYP = "efHTML"
          this.w_EXT = ".HTM"
        case this.w_AQFORTYP = "efCSV"
          this.w_EXT = ".CSV"
        case this.w_AQFORTYP = "efPDF"
          this.w_EXT = ".PDF"
      endcase
      if empty(this.pd_StrProcesso)
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_CURSORE = w_IRDR_RET.GetCursorName(1)
        Select (this.w_CURSORE) 
 Go Top
        do while not EOF()
          this.w_NUMERO = RECNO()
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Rilascio l'oggetto
          this.w_IBPOCX = .NULL.
          * --- Gestione errori
           
 L_errsav=on("ERROR") 
 Messaggio=" " 
 ON ERROR Messaggio="Errore OCX"
          * --- Creo l'oggetto InfoPublisher
          this.w_IBPOCX = createobject("IBPublisher20.IBPublisher")
          * --- Carico le propriet� dell'OCX.
          this.w_IBPOCX.Description = this.w_AQDES
          this.w_TMPFILE = ""
          if LEFT(ALLTRIM(this.w_AQQRPATH),3)="..\"
            this.w_TMPFILE = RIGHT(ALLTRIM(this.w_AQQRPATH),LEN(ALLTRIM(this.w_AQQRPATH))-3)
          endif
          if NOT EMPTY(this.w_TMPFILE)
            this.w_TMPFILE = cp_getStdFile(this.w_TMPFILE,"")
          endif
          this.w_IBPOCX.QueryPathname = IIF(EMPTY(this.w_TMPFILE), this.w_AQQRPATH, this.w_TMPFILE)
          this.w_IBPOCX.ExportFormat = IIF( EMPTY( this.w_AQFORTYP ) , "efQuery", this.w_AQFORTYP )
          this.w_TMPFILE = ""
          if LEFT(ALLTRIM(this.w_AQIMPATH),3)="..\"
            this.w_TMPFILE = RIGHT(ALLTRIM(this.w_AQIMPATH),LEN(ALLTRIM(this.w_AQIMPATH))-3)
          endif
          * --- Cerco infomart personalizzati
          if NOT EMPTY(this.w_TMPFILE)
            this.w_TMPFILE = cp_getStdFile(this.w_TMPFILE,"")
          endif
          this.w_IBPOCX.InfomartPathname = IIF(EMPTY(this.w_TMPFILE), this.w_AQIMPATH, this.w_TMPFILE)
          this.w_IBPOCX.LogPathname = this.w_AQLOG
          this.w_IBPOCX.ADOConnString = iif(NVL(this.w_AQCRIPTE,"N")="C",CifraCnf( ALLTRIM(this.w_AQADOCS) , "D" ),this.w_AQADOCS)
          this.w_IBPOCX.ConnType = IIF( EMPTY( this.w_AQCONTYP ), CP_DBTYPE, this.w_AQCONTYP)
          this.w_IBPOCX.ConnCursorLocation = IIF( this.w_AQCURLOC = "C", "clUseClient", "clUseServer")
          this.w_IBPOCX.ConnCursorType = IIF( EMPTY( this.w_AQCURTYP ), "ctOpenForwardOnly", this.w_AQCURTYP )
          this.w_IBPOCX.ConnTimeout = this.w_AQTIMOUT
          this.w_IBPOCX.ConnCustomProperties = IIF(this.w_AQFLJOIN="S","UseWhereForJoins=True","UseWhereForJoins=False")
          this.w_IBPOCX.AdvancedLog = iif(this.oParentObject.w_LOG="S", .t., .f.)
          this.w_IBPOCX.ShowErrors = .f.
          do CHKINFO with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ON ERROR &L_errsav
          Select (this.w_CURSORE)
          Go this.w_NUMERO 
 skip
        enddo
        USE IN SELECT("__pdtmp__")
        USE IN SELECT("__tmp__")
        USE IN SELECT("doctmp")
      endif
    else
      * --- Query non Filtrata
      if this.w_AQFORTYP = "efQuery" OR this.w_AQFORTYP = "efRTF" OR this.w_AQFORTYP = "efPDF" OR this.w_AQFORTYP = "efExcel" OR !this.w_NEED7ZIP
        this.w_TMPDIR7ZIP = ""
        this.w_IBPOCX.DestinationPath = AddBs(JustPath(ALLTRIM(this.w_AQDTFILE)))
      else
        * --- Utilizzo una cartella temporanea che verr� poi zippata
        this.w_TMPDIR7ZIP = AddBs(TEMPADHOC() + "\IRDR\" + SYS(2015))
        * --- Creo la cartella temporanea
        MAKEDIR(this.w_TMPDIR7ZIP)
        this.w_IBPOCX.DestinationPath = this.w_TMPDIR7ZIP
      endif
      this.w_FILENAME = " "
      this.w_IBPOCX.DestinationFilename = alltrim(justfname(this.w_AQDTFILE))
      * --- PARAMETRI EMAIL
      this.w_IBPOCX.SMTPSenderDescription = ah_MsgFormat("Invio analisi %1",this.w_AQDES)
      * --- Leggo dati email dell'utente corrente
      this.w_SendAdd = g_MITTEN
      this.w_Server = g_SRVMAIL
      this.w_Port = g_SRVPORTA
      * --- Leggo login e password SMTP da ACC_MAIL (prima erano su AUT_LPE)
      * --- Read from ACC_MAIL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ACC_MAIL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ACC_MAIL_idx,2],.t.,this.ACC_MAIL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AM_LOGIN,AMPASSWD,AMMODCON"+;
          " from "+i_cTable+" ACC_MAIL where ";
              +"AMSERIAL = "+cp_ToStrODBC(g_AccountMail);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AM_LOGIN,AMPASSWD,AMMODCON;
          from (i_cTable) where;
              AMSERIAL = g_AccountMail;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_login = NVL(cp_ToDate(_read_.AM_LOGIN),cp_NullValue(_read_.AM_LOGIN))
        this.w_psw = NVL(cp_ToDate(_read_.AMPASSWD),cp_NullValue(_read_.AMPASSWD))
        this.w_AMMODCON = NVL(cp_ToDate(_read_.AMMODCON),cp_NullValue(_read_.AMMODCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IBPOCX.SMTPSenderAddress = this.w_SendAdd
      this.w_IBPOCX.SMTPServer = this.w_Server
      this.w_IBPOCX.SMTPPort = this.w_Port
      this.w_IBPOCX.SMTPLogin = IIF(EMPTY(NVL(this.w_login," ")),"",this.w_login)
      this.w_IBPOCX.SMTPPassword = IIF(EMPTY(NVL(CifraCnf( ALLTRIM(this.w_psw) , "D" )," ")),"",CifraCnf( ALLTRIM(this.w_psw) , "D" ))
      if this.w_VERSION >= "4.0.0" and g_INVIO="P"
        this.w_IBPOCX.SMTPUseSSL = True
        this.w_IBPOCX.SMTPUseSTARTTLS = this.w_AMMODCON="-starttls"
      endif
      * --- Aggiungo alla lista gli indirizzi email contenuti nel detail
      * --- Select from INF_AQD
      i_nConn=i_TableProp[this.INF_AQD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_AQD_idx,2],.t.,this.INF_AQD_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" INF_AQD ";
            +" where "+cp_ToStrODBC(this.w_CHIAVE)+"=AQCODICE";
             ,"_Curs_INF_AQD")
      else
        select * from (i_cTable);
         where this.w_CHIAVE=AQCODICE;
          into cursor _Curs_INF_AQD
      endif
      if used('_Curs_INF_AQD')
        select _Curs_INF_AQD
        locate for 1=1
        do while not(eof())
        this.w_IBPOCX.AddEmailRecipient(_Curs_INF_AQD.AQMAILR)     
          select _Curs_INF_AQD
          continue
        enddo
        use
      endif
      * --- Eseguo la pubblicazione
      this.w_IBPOCX.ExecuteQuery()     
      if !EMPTY(this.w_TMPDIR7ZIP)
        * --- Creo 7ZIP
        if !ZIPUNZIP("Z", FULLPATH(FORCEEXT(this.w_AQDTFILE, "7Z")), this.w_TMPDIR7ZIP)
           Messaggio="Errore"
        endif
        * --- Cancello i file
        if !DELETEFOLDER(this.w_TMPDIR7ZIP)
           Messaggio="Errore"
        endif
        this.w_AQDTFILE = FORCEEXT(this.w_AQDTFILE, "7Z")
      endif
      * --- Svuoto la lista dei destinatari email
      this.w_IBPOCX.ClearRecipientList()     
      if g_IZCP$"SA" and this.w_AQFLGZCP="S"
        * --- -- Se attivo il check CZP registra record per divulgazione dati ...
        * --- -- Controllo se l'utente � abilitato a pubblicare sul portale
        * --- Read from UTE_NTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UTE_NTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UTSERVCP"+;
            " from "+i_cTable+" UTE_NTI where ";
                +"UTCODICE = "+cp_ToStrODBC(i_CODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UTSERVCP;
            from (i_cTable) where;
                UTCODICE = i_CODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UTSERVCP = NVL(cp_ToDate(_read_.UTSERVCP),cp_NullValue(_read_.UTSERVCP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DVPATHFI2 = alltrim(this.w_COPATHCP) + "\" + alltrim(justfname(this.w_AQDTFILE))
        * --- Se divulgazione modifico il nome del file
        this.w_FILENAME = GENFILENAME(alltrim(justfname(this.w_AQDTFILE)),.T.)
        this.w_DVPATHFI = addbs(alltrim(this.w_COPATHCP))+ this.w_FILENAME
        COPY FILE (this.w_AQDTFILE) TO (this.w_DVPATHFI)
        * --- -- Se attivo il check CZP registra record per divulgazione dati ...
        if this.w_AQFLGZCP = "S" and File(this.w_DVPATHFI)
          if NVL(this.w_UTSERVCP,"N") = "S"
            * --- Variabili introdotte con integrazione Corporate Portal, valorizzate a space
            * --- Se occorre pubblicare in una folder-company ...
            * --- Read from INF_PZCP
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.INF_PZCP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_PZCP_idx,2],.t.,this.INF_PZCP_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DERISERV,DEFOLDER,DEDESFIL"+;
                " from "+i_cTable+" INF_PZCP where ";
                    +"DECODICE = "+cp_ToStrODBC(this.w_CHIAVE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DERISERV,DEFOLDER,DEDESFIL;
                from (i_cTable) where;
                    DECODICE = this.w_CHIAVE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DERISERV = NVL(cp_ToDate(_read_.DERISERV),cp_NullValue(_read_.DERISERV))
              this.w_DEFOLDER = NVL(cp_ToDate(_read_.DEFOLDER),cp_NullValue(_read_.DEFOLDER))
              this.w_DEDESFIL = NVL(cp_ToDate(_read_.DEDESFIL),cp_NullValue(_read_.DEDESFIL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from ZFOLDERS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ZFOLDERS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ZFOLDERS_idx,2],.t.,this.ZFOLDERS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "FO__TIPO,FO__PATH"+;
                " from "+i_cTable+" ZFOLDERS where ";
                    +"FOSERIAL = "+cp_ToStrODBC(this.w_DEFOLDER);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                FO__TIPO,FO__PATH;
                from (i_cTable) where;
                    FOSERIAL = this.w_DEFOLDER;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FO__TIPO = NVL(cp_ToDate(_read_.FO__TIPO),cp_NullValue(_read_.FO__TIPO))
              this.w_FO__PATH = NVL(cp_ToDate(_read_.FO__PATH),cp_NullValue(_read_.FO__PATH))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Leggo i parametri impostati
            * --- Select from INF_FCPZ
            i_nConn=i_TableProp[this.INF_FCPZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_FCPZ_idx,2],.t.,this.INF_FCPZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PFNOMVAR,PFVARVAL  from "+i_cTable+" INF_FCPZ ";
                  +" where PFCODICE="+cp_ToStrODBC(this.w_CHIAVE)+"";
                   ,"_Curs_INF_FCPZ")
            else
              select PFNOMVAR,PFVARVAL from (i_cTable);
               where PFCODICE=this.w_CHIAVE;
                into cursor _Curs_INF_FCPZ
            endif
            if used('_Curs_INF_FCPZ')
              select _Curs_INF_FCPZ
              locate for 1=1
              do while not(eof())
              L_MAC = ALLTRIM(_Curs_INF_FCPZ.PFNOMVAR)+" = "+ALLTRIM(_Curs_INF_FCPZ.PFVARVAL)
              &L_MAC
                select _Curs_INF_FCPZ
                continue
              enddo
              use
            endif
            * --- verifico la presenza di tutti i paramenti e effettuo la trascodifica
            do case
              case "%LOCATION%" $ UPPER(this.w_FO__PATH) AND EMPTY(g_ZCPLOCATION)
                ah_ErrorMsg("Il codice folder selezionato contiene il parametro %location%. impossibile utilizzarlo con questa tipo d'invio",,"")
                this.w_FLERRFLD = .T.
              case "%YEAR%" $ UPPER(this.w_FO__PATH) AND EMPTY(g_ZCPYEAR)
                ah_ErrorMsg("Il codice folder selezionato contiene il parametro %year%. impossibile utilizzarlo con questa tipo d'invio",,"")
                this.w_FLERRFLD = .T.
              case "%MONTH%" $ UPPER(this.w_FO__PATH) AND EMPTY(g_ZCPMONTH)
                ah_ErrorMsg("Il codice folder selezionato contiene il parametro %month%. impossibile utilizzarlo con questa tipo d'invio",,"")
                this.w_FLERRFLD = .T.
              case "%DOC_TYPE%" $ UPPER(this.w_FO__PATH) AND EMPTY(g_ZCPDOCTYPE)
                ah_ErrorMsg("Il codice folder selezionato contiene il parametro %doc_type%. Impossibile utilizzarlo con questa tipo d'invio",,"")
                this.w_FLERRFLD = .T.
              otherwise
                * --- Ricalcolo la stringa corretta
                this.w_FLERRFLD = .F.
                this.w_FO__PATH = STRTRAN(this.w_FO__PATH, "%LOCATION%", g_ZCPLOCATION )
                this.w_FO__PATH = STRTRAN(this.w_FO__PATH, "%YEAR%", g_ZCPYEAR )
                this.w_FO__PATH = STRTRAN(this.w_FO__PATH, "%MONTH%", g_ZCPMONTH )
                this.w_FO__PATH = STRTRAN(this.w_FO__PATH, "%DOC_TYPE%", g_ZCPDOCTYPE )
            endcase
            * --- Inserisce record testata ...
            this.w_DVDESELA = Left( this.w_DEDESFIL, 100 )
            this.w_DV__NOTE = alltrim( this.w_DEDESFIL )
            this.w_DVDATCRE = i_DATSYS
            if NOT this.w_FLERRFLD
              if g_ICPZSTANDALONE or g_IZCP="A"
                this.w_FILEXML = LRTrim(this.w_DVPATHFI)+".xml"
                this.w_NHF = FCreate(this.w_FILEXML)
                if this.w_NHF>=0
                  this.w_TmPC = '<?xml version="1.0" encoding="iso-8859-1"?>'
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = '<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = "  <Name>" + this.convertXML(JUSTFNAME(ALLTRIM(this.w_AQDTFILE))) + "</Name>"
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  if this.w_FO__TIPO="C"
                    * --- Legge destinatario principale ...
                    * --- Read from INF_PZCP
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.INF_PZCP_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.INF_PZCP_idx,2],.t.,this.INF_PZCP_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "DETIPDES,DETIPCON,DECODCON,DECODAGE"+;
                        " from "+i_cTable+" INF_PZCP where ";
                            +"DECODICE = "+cp_ToStrODBC(this.w_CHIAVE);
                            +" and DERIFCFO = "+cp_ToStrODBC("S");
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        DETIPDES,DETIPCON,DECODCON,DECODAGE;
                        from (i_cTable) where;
                            DECODICE = this.w_CHIAVE;
                            and DERIFCFO = "S";
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_DETIPDES = NVL(cp_ToDate(_read_.DETIPDES),cp_NullValue(_read_.DETIPDES))
                      this.w_DETIPCON = NVL(cp_ToDate(_read_.DETIPCON),cp_NullValue(_read_.DETIPCON))
                      this.w_DECODCON = NVL(cp_ToDate(_read_.DECODCON),cp_NullValue(_read_.DECODCON))
                      this.w_DECODAGE = NVL(cp_ToDate(_read_.DECODAGE),cp_NullValue(_read_.DECODAGE))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    this.w_TMPC = "  <FolderCompany>%" + this.w_DETIPDES+IIF(this.w_DETIPDES="A",alltrim(this.w_DECODAGE),alltrim(this.w_DECODCON))+"%"+alltrim(this.w_FO__PATH) + "</FolderCompany>"
                    this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  else
                    this.w_TmPC = "  <Folder>" + alltrim(this.w_FO__PATH) + "</Folder>"
                    this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  endif
                  this.w_TmPC = "  <Description>" + this.convertXML(LRTrim(this.w_DVDESELA)) + "</Description>"
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = "  <Notes>" + this.convertXML(LRTrim(this.w_DV__NOTE)) + "</Notes>"
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = "  <AttachmentType>" + this.w_PFITYPE + "</AttachmentType>"
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = "  <AttachmentOwnerCode>" + this.w_PFIPKEY + "</AttachmentOwnerCode>"
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = "  <RemoveFileAfterUpload>S</RemoveFileAfterUpload>"
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                  this.w_TmPC = "  <Reserved>" + this.w_DERISERV + "</Reserved>"
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                endif
              else
                this.w_DV_PARAM = "NAME="+JUSTFNAME(ALLTRIM(this.w_DVPATHFI2))+"; "
                if this.w_FO__TIPO="C"
                  * --- Legge destinatario principale ...
                  * --- Read from INF_PZCP
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.INF_PZCP_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.INF_PZCP_idx,2],.t.,this.INF_PZCP_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "DETIPDES,DETIPCON,DECODCON,DECODAGE"+;
                      " from "+i_cTable+" INF_PZCP where ";
                          +"DECODICE = "+cp_ToStrODBC(this.w_CHIAVE);
                          +" and DERIFCFO = "+cp_ToStrODBC("S");
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      DETIPDES,DETIPCON,DECODCON,DECODAGE;
                      from (i_cTable) where;
                          DECODICE = this.w_CHIAVE;
                          and DERIFCFO = "S";
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_DETIPDES = NVL(cp_ToDate(_read_.DETIPDES),cp_NullValue(_read_.DETIPDES))
                    this.w_DETIPCON = NVL(cp_ToDate(_read_.DETIPCON),cp_NullValue(_read_.DETIPCON))
                    this.w_DECODCON = NVL(cp_ToDate(_read_.DECODCON),cp_NullValue(_read_.DECODCON))
                    this.w_DECODAGE = NVL(cp_ToDate(_read_.DECODAGE),cp_NullValue(_read_.DECODAGE))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_DV_PARAM = this.w_DV_PARAM+"FOLDERCOMPANY=%"+this.w_DETIPDES+IIF(this.w_DETIPDES="A",alltrim(this.w_DECODAGE),alltrim(this.w_DECODCON))+"%"+alltrim(this.w_FO__PATH)+"; "
                else
                  this.w_DV_PARAM = this.w_DV_PARAM+"FOLDER="+alltrim(this.w_FO__PATH)+"; "
                endif
                this.w_DVCODELA = ZCPINDIV( "TRA", this.w_DVDESELA, this.w_DVDATCRE, "S", FULLPATH( this.w_DVPATHFI ), this.w_DV_PARAM, this.w_DV__NOTE, this.w_DERISERV, this.w_PFITYPE, this.w_PFIPKEY )
              endif
              * --- Dettaglio permessi ...
              if this.w_DERISERV="S"
                * --- Se stand-alone apre il tag Securities
                if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
                  this.w_TmPC = "  <Securities>"
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                endif
                * --- Select from INF_PZCP
                i_nConn=i_TableProp[this.INF_PZCP_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INF_PZCP_idx,2],.t.,this.INF_PZCP_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" INF_PZCP ";
                      +" where DECODICE="+cp_ToStrODBC(this.w_CHIAVE)+"";
                       ,"_Curs_INF_PZCP")
                else
                  select * from (i_cTable);
                   where DECODICE=this.w_CHIAVE;
                    into cursor _Curs_INF_PZCP
                endif
                if used('_Curs_INF_PZCP')
                  select _Curs_INF_PZCP
                  locate for 1=1
                  do while not(eof())
                  this.w_TmPC = IIF(_Curs_INF_PZCP.DETIPDES$"CF", _Curs_INF_PZCP.DECODCON, IIF(_Curs_INF_PZCP.DETIPDES="A", _Curs_INF_PZCP.DECODAGE, IIF(_Curs_INF_PZCP.DETIPDES="G", str(_Curs_INF_PZCP.DECODGRU,6,0), " ")))
                  if Not(Empty(this.w_TMPC))
                    if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
                      this.w_TmPC = "    <Security>"
                      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                      this.w_TmPC = IIF(_Curs_INF_PZCP.DETIPDES="C","CUSTOMER", IIF(_Curs_INF_PZCP.DETIPDES="F", "SUPPLIER", IIF(_Curs_INF_PZCP.DETIPDES="A","SALESAGENT",IIF(_Curs_INF_PZCP.DETIPDES="G","GROUP"," "))))
                      this.w_TmPC = space(6)+"<EntityType>" + this.w_Tmpc + "</EntityType>"
                      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                      this.w_TmPC = IIF(_Curs_INF_PZCP.DETIPDES$"CF", _Curs_INF_PZCP.DETIPDES+_Curs_INF_PZCP.DECODCON, IIF(_Curs_INF_PZCP.DETIPDES="A", "A"+_Curs_INF_PZCP.DECODAGE, IIF(_Curs_INF_PZCP.DETIPDES="G", str(_Curs_INF_PZCP.DECODGRU,6,0), " ")))
                      this.w_TmPC = space(6)+"<EntityCode>" + LRTrim(this.w_Tmpc) + "</EntityCode>"
                      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                      * --- Utente abilitato, lo inserisco ...
                      this.w_TmPC = space(6)+"<Read>" + _Curs_INF_PZCP.DE__READ + "</Read>"
                      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                      this.w_TmPC = space(6)+"<Write>" + _Curs_INF_PZCP.DE_WRITE + "</Write>"
                      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                      this.w_TmPC = space(6)+"<Delete>" + _Curs_INF_PZCP.DEDELETE + "</Delete>"
                      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                      this.w_TmPC = "    </Security>"
                      this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                    else
                      if this.w_DVCODELA>0 
                        * --- Insert into ZDESTIN
                        i_nConn=i_TableProp[this.ZDESTIN_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.ZDESTIN_idx,2])
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_ccchkf=''
                        i_ccchkv=''
                        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZDESTIN_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                      " ("+"DECODELA"+",CPROWNUM"+",DETIPDES"+",DECODGRU"+",DETIPCON"+",DECODCON"+",DECODAGE"+",DECODROL"+",DEVALDES"+",DE__READ"+",DE_WRITE"+",DEDELETE"+",DEFLGESE"+i_ccchkf+") values ("+;
                          cp_NullLink(cp_ToStrODBC(this.w_DVCODELA),'ZDESTIN','DECODELA');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.CPROWNUM),'ZDESTIN','CPROWNUM');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DETIPDES),'ZDESTIN','DETIPDES');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DECODGRU),'ZDESTIN','DECODGRU');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DETIPCON),'ZDESTIN','DETIPCON');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DECODCON),'ZDESTIN','DECODCON');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DECODAGE),'ZDESTIN','DECODAGE');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DECODROL),'ZDESTIN','DECODROL');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DEVALDES),'ZDESTIN','DEVALDES');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DE__READ),'ZDESTIN','DE__READ');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DE_WRITE),'ZDESTIN','DE_WRITE');
                          +","+cp_NullLink(cp_ToStrODBC(_Curs_INF_PZCP.DEDELETE),'ZDESTIN','DEDELETE');
                          +","+cp_NullLink(cp_ToStrODBC("N"),'ZDESTIN','DEFLGESE');
                               +i_ccchkv+")")
                        else
                          cp_CheckDeletedKey(i_cTable,0,'DECODELA',this.w_DVCODELA,'CPROWNUM',_Curs_INF_PZCP.CPROWNUM,'DETIPDES',_Curs_INF_PZCP.DETIPDES,'DECODGRU',_Curs_INF_PZCP.DECODGRU,'DETIPCON',_Curs_INF_PZCP.DETIPCON,'DECODCON',_Curs_INF_PZCP.DECODCON,'DECODAGE',_Curs_INF_PZCP.DECODAGE,'DECODROL',_Curs_INF_PZCP.DECODROL,'DEVALDES',_Curs_INF_PZCP.DEVALDES,'DE__READ',_Curs_INF_PZCP.DE__READ,'DE_WRITE',_Curs_INF_PZCP.DE_WRITE,'DEDELETE',_Curs_INF_PZCP.DEDELETE)
                          insert into (i_cTable) (DECODELA,CPROWNUM,DETIPDES,DECODGRU,DETIPCON,DECODCON,DECODAGE,DECODROL,DEVALDES,DE__READ,DE_WRITE,DEDELETE,DEFLGESE &i_ccchkf. );
                             values (;
                               this.w_DVCODELA;
                               ,_Curs_INF_PZCP.CPROWNUM;
                               ,_Curs_INF_PZCP.DETIPDES;
                               ,_Curs_INF_PZCP.DECODGRU;
                               ,_Curs_INF_PZCP.DETIPCON;
                               ,_Curs_INF_PZCP.DECODCON;
                               ,_Curs_INF_PZCP.DECODAGE;
                               ,_Curs_INF_PZCP.DECODROL;
                               ,_Curs_INF_PZCP.DEVALDES;
                               ,_Curs_INF_PZCP.DE__READ;
                               ,_Curs_INF_PZCP.DE_WRITE;
                               ,_Curs_INF_PZCP.DEDELETE;
                               ,"N";
                               &i_ccchkv. )
                          i_Rows=iif(bTrsErr,0,1)
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if i_Rows<0 or bTrsErr
                          * --- Error: insert not accepted
                          i_Error=MSG_INSERT_ERROR
                          return
                        endif
                      endif
                    endif
                  endif
                    select _Curs_INF_PZCP
                    continue
                  enddo
                  use
                endif
                * --- Se stand-alone chiude il tag Securities
                if (g_ICPZSTANDALONE or g_IZCP="A") and this.w_NHF>=0
                  this.w_TmPC = "  </Securities>"
                  this.w_TMPN = FPUTS(this.w_NHF, this.w_TMPC)
                endif
              endif
            endif
            * --- -- Pulizia variabili
            * --- Select from INF_FCPZ
            i_nConn=i_TableProp[this.INF_FCPZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_FCPZ_idx,2],.t.,this.INF_FCPZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PFNOMVAR,PFVARVAL  from "+i_cTable+" INF_FCPZ ";
                  +" where PFCODICE="+cp_ToStrODBC(this.w_DEFOLDER)+"";
                   ,"_Curs_INF_FCPZ")
            else
              select PFNOMVAR,PFVARVAL from (i_cTable);
               where PFCODICE=this.w_DEFOLDER;
                into cursor _Curs_INF_FCPZ
            endif
            if used('_Curs_INF_FCPZ')
              select _Curs_INF_FCPZ
              locate for 1=1
              do while not(eof())
              L_MAC = ALLTRIM(_Curs_PRO_FCPZ.PFNOMVAR)+' = " "'
              &L_MAC
                select _Curs_INF_FCPZ
                continue
              enddo
              use
            endif
          else
            INSERT INTO MsgErr (CODICE, DES) VALUES (this.w_CHIAVE, this.w_AQDES)
            this.w_CPERR = .t.
          endif
        endif
      endif
    endif
    * --- Rilascio l'oggetto
    this.w_IBPOCX = .NULL.
    * --- Attenzione! Qualunque problema ci sia stato, qui non lo vedo.
    *     Il riscontro � solo nel file di log
    ON ERROR &L_errsav
    if Messaggio="Errore"
      this.w_ERROR = .t.
      this.w_ALLERROR = .t.
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo gli array dei parametri e dei filtri
    * --- Oggetti di tipo Stack
    * --- Istanzio gli oggetti stack
    this.a_FP_Macro = createobject("Stack",1)
    this.a_FParameter = createobject("Stack",1)
    * --- Valorizzo gli indici degli array
    this.aFP_Name = 1
    this.aFP_Value = 2
    this.aFP_HasValue = 3
    this.aFL_Name = 1
    this.aFL_Type = 2
    this.aFL_Value = 3
    this.aFL_Remove = 4
    this.aFL_Azienda = 5
    * --- Costruisco l'array a_FP_Macro che conterr� le macro da utilizzare per
    *     creare i filtri a pagina 5
    if this.w_CPROWNUM=-1 and TYPE("this.oParentObject.oParentObject.parent.pdoParentObject")="O"
      this.w_MASK = UPPER(SUBSTR(this.oParentObject.oParentObject.parent.pdoParentObject.class,2,len(this.oParentObject.oParentObject.parent.pdoParentObject.class)-1))
      * --- Read from INF_AGIM
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INF_AGIM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INF_AGIM_idx,2],.t.,this.INF_AGIM_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CPROWNUM"+;
          " from "+i_cTable+" INF_AGIM where ";
              +"GICODQUE = "+cp_ToStrODBC(this.w_CHIAVE);
              +" and GIPROGRA = "+cp_ToStrODBC(this.w_MASK);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CPROWNUM;
          from (i_cTable) where;
              GICODQUE = this.w_CHIAVE;
              and GIPROGRA = this.w_MASK;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CPROWNUM = iif(NVL(this.w_CPROWNUM,0)=0,-1,this.w_CPROWNUM)
    endif
    this.i = 0
    * --- Select from INF_AFIM
    i_nConn=i_TableProp[this.INF_AFIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2],.t.,this.INF_AFIM_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select FIFLNAME,FITIPFIL,FIFILTRO,FIVISMSK,FIFLGREM,FIFLGAZI  from "+i_cTable+" INF_AFIM ";
          +" where FICODQUE="+cp_ToStrODBC(this.w_CHIAVE)+" AND   FIPRGRN="+cp_ToStrODBC(this.w_CPROWNUM)+"   ";
           ,"_Curs_INF_AFIM")
    else
      select FIFLNAME,FITIPFIL,FIFILTRO,FIVISMSK,FIFLGREM,FIFLGAZI from (i_cTable);
       where FICODQUE=this.w_CHIAVE AND   FIPRGRN=this.w_CPROWNUM   ;
        into cursor _Curs_INF_AFIM
    endif
    if used('_Curs_INF_AFIM')
      select _Curs_INF_AFIM
      locate for 1=1
      do while not(eof())
      this.i = this.i + 1
      this.w_FIVISMSK = FIVISMSK
      this.w_FIFLNAME = NVL( FIFLNAME, " " )
      this.w_FITIPFIL = NVL( FITIPFIL, " " )
      this.w_FIFILTRO = NVL( FIFILTRO, " " )
      this.w_FIFLGREM = NVL( FIFLGREM, " " )
      this.w_FIFLGAZI = NVL( FIFLGAZI, " " )
      this.w_FIFILTROSUM = this.w_FIFILTROSUM + this.w_FIFILTRO + CHR(13)
      * --- Ridimensiono gli array
      this.a_FP_Macro.Dim(this.i, 5)     
      * --- Riverso il contenuto dei filtri selezionati nell'array , dopo eseguir� una
      *     sostituzione per scambiare il nome del parametro con il contenuto del campo Value 
      *     dell'array a_FP_Parameter
      this.a_FP_Macro.Set(this.i, this.aFL_Name, this.w_FIFLNAME)     
      this.a_FP_Macro.Set(this.i, this.aFL_Type, this.w_FITIPFIL)     
      this.a_FP_Macro.Set(this.i, this.aFL_Value, this.w_FIFILTRO)     
      this.a_FP_Macro.Set(this.i, this.aFL_Remove, this.w_FIFLGREM)     
      this.a_FP_Macro.Set(this.i, this.aFL_Azienda, this.w_FIFLGAZI)     
        select _Curs_INF_AFIM
        continue
      enddo
      use
    endif
    * --- Salvo il numero totale dei filtri
    this.w_NumParameter = this.i
    * --- Scorrendo w_FIFILTROSUM, che contiene la parte destra di tutti i filtri, 
    *     determino quanti parametri effettivi ho utilizzato nei filtri
    this.i = 0
    do while 1 <= OCCURS( "?", this.w_FIFILTROSUM )
      * --- Estraggo la variabile cercando "?...?"
      this.i = this.i + 1
      this.a_FParameter.Dim(this.i, 3)     
      this.L_FirstPos = AT( "?", this.w_FIFILTROSUM ) + 1
      this.L_LastPos = AT( "?", this.w_FIFILTROSUM, 2 )
      this.w_VARIABILE = SUBSTR( this.w_FIFILTROSUM, this.L_FirstPos, this.L_LastPos - this.L_FirstPos )
      * --- Valorizzo l'array dei parametri, salvo tutte le variabili con w_
      this.a_FParameter.Set(this.i, this.aFP_Name, this.w_VARIABILE)     
      this.a_FParameter.Set(this.i, this.aFP_HasValue, .f.)     
      * --- Sostituisco nel filtro la variabile dell'oggetto
      this.w_FIFILTROSUM = STRTRAN( this.w_FIFILTROSUM, "?"+this.w_VARIABILE+"?", "" )
    enddo
    * --- Salvo il numero di parametri utilizzati
    this.w_NumFilter = this.i
    * --- Controllo se � stata specificata una visual mask per la richiesta di parametri
    if empty(this.pd_StrProcesso)
      if NOT EMPTY(this.w_FIVISMSK) AND NOT ISNULL(this.w_FIVISMSK)
        * --- Apro la visual mask associata
         
 L_errsav1=on("ERROR") 
 MessErr=" " 
 ON ERROR MessErr=Message()
        this.w_OBJECT = vm_exec(LOWER(ALLTRIM(this.w_FIVISMSK)), this.oParentObject.w_OBJECT)
        ON ERROR &L_errsav1
      else
        * --- Creo l'oggetto di tipo Object che servir� da interfaccia alla gestione dei parametri
        *     Object pu� gestire StdForm e Zoom
        this.w_OBJECT = createobject("ObjInt", This.oParentObject.w_OBJECT, this.pZOOM)
      endif
    endif
    * --- Valorizzo il campo a_FP_Value dell'array a_FP_Parameter
    this.i = 0
    this.j = 0
    do while this.i < this.w_NumFilter
      * --- Cerco le variabili per creare i filtri
      this.i = this.i + 1
      * --- Testo se la variabile � presente in w_OBJECT, cerco sempre le variabili con w_
      do case
        case TYPE(this.a_FParameter.Ret(this.i, this.aFP_Name))<>"U" or this.w_OBJECT.IsVar(this.a_FParameter.Ret(this.i, this.aFP_Name))
          if empty(this.pd_StrProcesso)
            this.a_FParameter.Set(this.i, this.aFP_Value, this.w_OBJECT.GetVar(this.a_FParameter.Ret(this.i, this.aFP_Name)))     
          else
            this.a_FParameter.Set(this.i, this.aFP_Value, (this.a_FParameter.Ret(this.i, this.aFP_Name)))     
          endif
          this.a_FParameter.Set(this.i, this.aFP_HasValue, .t.)     
        otherwise
          * --- Non � stato possibile recuperare il valore delle variabili specificate nel filtro
          *     verr� aperta la maschera dei parametri
          * --- Creo la maschera dei parametri, istanzio la maschera solo la prima volta
          if empty(this.pd_StrProcesso)
            if this.j = 0
              this.w_Mask = createobject("query_InfoPub_param_mask", this, .f.)
            endif
            * --- Aggiungo le variabili alla maschera, per farlo leggo le caratteristiche del parametro
            *     dalla tabella INFPARFI
            this.w_VARNAME = this.a_FParameter.Ret(this.i, this.aFP_Name)
            * --- Read from INFPARFI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.INFPARFI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INFPARFI_idx,2],.t.,this.INFPARFI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PFDESVAR,PFTIPVAR,PFLENVAR,PFDECVAR"+;
                " from "+i_cTable+" INFPARFI where ";
                    +"PFCODQUE = "+cp_ToStrODBC(this.w_CHIAVE);
                    +" and PFVARNAM = "+cp_ToStrODBC(this.w_VARNAME);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PFDESVAR,PFTIPVAR,PFLENVAR,PFDECVAR;
                from (i_cTable) where;
                    PFCODQUE = this.w_CHIAVE;
                    and PFVARNAM = this.w_VARNAME;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESVAR = NVL(cp_ToDate(_read_.PFDESVAR),cp_NullValue(_read_.PFDESVAR))
              this.w_TIPVAR = NVL(cp_ToDate(_read_.PFTIPVAR),cp_NullValue(_read_.PFTIPVAR))
              this.w_LENVAR = NVL(cp_ToDate(_read_.PFLENVAR),cp_NullValue(_read_.PFLENVAR))
              this.w_DECVAR = NVL(cp_ToDate(_read_.PFDECVAR),cp_NullValue(_read_.PFDECVAR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_Mask.Add(this.i, this.w_VARNAME, this.w_DESVAR, this.w_TIPVAR , this.w_LENVAR, this.w_DECVAR)     
            this.j = this.j + 1
          endif
      endcase
    enddo
    * --- Se necessario apro la maschera dei parametri
    if this.j > 0
      this.w_Mask.Show()     
      * --- Alla pressione del tasto Ok saranno copiati i valori della maschera dei rispettivi
      *     campi dell'array a_FP_Parameter
    endif
    * --- Cerco e sostituisco "?...?" con i campi dell'array a_FP_Parameter
    this.i = 0
    do while this.i < this.w_NumFilter
      this.i = this.i + 1
      this.w_VARNAME = this.a_FParameter.Ret(this.i, this.aFP_Name)
      * --- Costruisco la stringa da sostituire alla variabile
      if TYPE(this.w_VARNAME)="U" 
        this.L_FILTRO = "This.a_FParameter.Ret("+ALLTRIM(STR(this.i))+","+ALLTRIM(STR(this.aFP_Value))+")"
      else
        this.L_FILTRO = alltrim(this.w_VARNAME)
      endif
      this.j = 0
      do while this.j < this.w_NumParameter
        this.j = this.j + 1
        this.a_FP_Macro.Set(this.j, this.aFL_Value, STRTRAN( this.a_FP_Macro.Ret(this.j, this.aFL_Value), "?"+this.w_VARNAME+"?", this.L_FILTRO ))     
      enddo
    enddo
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Utilizzo la funzione SYS(2015) per ottenere un nome di file univoco
    this.w_DTFILE = SYS(2015)
    * --- Ricavo il path temporaneo
    this.w_DTPATH = TEMPADHOC() + "\IRDR\"
    * --- Utilizzo come path temporaneo una sottocartella  della TEMP, quindi se non 
    *     esiste devo crearla
    if !DIRECTORY(this.w_DTPATH, 1)
      MakeDir(this.w_DTPATH)
    endif
    * --- Valorizzo la variabile caller che conterr� la query risultato
    this.w_DTFILE = JUSTSTEM(ALLTRIM(this.w_AQDTFILE))+"_"+ ALLTRIM( this.w_DTFILE )
    if this.w_AQFORTYP = "efQuery" OR this.w_AQFORTYP = "efRTF" OR this.w_AQFORTYP = "efPDF" OR this.w_AQFORTYP = "efExcel" OR !this.w_NEED7ZIP
      this.w_TMPDIR7ZIP = ""
      this.w_IBPOCX.DestinationPath = this.w_DTPATH
    else
      * --- Utilizzo una cartella temporanea che verr� poi zippata
      this.w_TMPDIR7ZIP = AddBs(TEMPADHOC() + "\IRDR\" + SYS(2015))
      * --- Creo la cartella temporanea
      MAKEDIR(this.w_TMPDIR7ZIP)
      this.w_IBPOCX.DestinationPath = this.w_TMPDIR7ZIP
      this.w_EXT = ".7Z"
    endif
    if type("this.oParentObject.w_PATHFILE")="C"
      this.oParentObject.w_PATHFILE = ALLTRIM( this.w_DTPATH ) + this.w_DTFILE + this.w_EXT
    endif
    this.w_IBPOCX.DestinationFilename = this.w_DTFILE
    * --- Aggiungo i filtri runtime
    * --- Seleziono i filtri della gestione selezionata
    this.w_CPROWNUM = this.pGIROWNUM
    * --- ---------------------------------------------------------------------------------------------------------------------------
    *     Se non vuoto processo documentale devo costruire i filtri dal cusore di stampa
    if not empty(this.pd_StrProcesso) and used(this.w_CURSORE)
      Afields(STRUCTAB,this.w_CURSORE)
      for nFields = 1 to alen(STRUCTAB,1) 
      w_CAMPO_LOC=STRUCTAB(nFields,1) 
 w_VARIABILE_LOC=STRUCTAB(nFields,1) 
 Select(this.w_CURSORE) 
 GO this.w_NUMERO 
 &w_VARIABILE_LOC = &w_CAMPO_LOC
      endfor
      this.w_CPROWNUM = -1
    endif
    * --- ---------------------------------------------------------------------------------------------------------------------------
    * --- Al ritorno da pagina 4 l'array a_FP_Macro conterr� i filtri pronti per essere valutati
    *     con una macro
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.i = 0
    do while this.i < this.w_NumParameter
      * --- Aggiungo i filtri alla query
      this.i = this.i + 1
      this.w_FIFLNAME = alltrim(this.a_FP_Macro.Ret(this.i, this.aFL_Name))
      this.w_FITIPFIL = alltrim(this.a_FP_Macro.Ret(this.i, this.aFL_Type))
      this.w_FIFILTRO = alltrim(this.a_FP_Macro.Ret(this.i, this.aFL_Value))
      this.w_FIFLGREM = this.a_FP_Macro.Ret(this.i, this.aFL_Remove)
      this.w_FIFLGAZI = this.a_FP_Macro.Ret(this.i, this.aFL_Azienda)
      * --- Costruisco la macro per valutare il filtro
      L_COMMAND = "L_FILTER = " + this.w_FIFILTRO
       
 L_errsav1=on("ERROR") 
 MessErr=" " 
 ON ERROR MessErr=Message()
      * --- Valuto il filtro e controllo se si � verificato un errore
      &L_COMMAND
      ON ERROR &L_errsav1
      if NOT EMPTY(MessErr)
        this.w_oMESS=createobject("AH_MESSAGE")
        this.w_oMESS.AddMsgPartNL("Attenzione: si � verificato un errore durante la creazione dei filtri")     
        this.w_oMESS.AddMsgPart(MessErr)     
        this.w_oMESS.AH_ErrorMsg()     
      endif
      * --- Elimino i primi due caratteri di FIFLNAME se quest'ultimo inizia per "D_"
      this.w_FIFLNAME = ALLTRIM( this.w_FIFLNAME )
      this.w_FIFLNAME = IIF( LEFT( this.w_FIFLNAME, 2 ) = "D_", STRTRAN( this.w_FIFLNAME, "D_", "", 1, 1 ), this.w_FIFLNAME )
      * --- Verifico se devo rimuovere il filtro, se il flag FIFLGREM = 'S' e L_FILTER � vuoto
      *     non devo aggiungere il filtro
      if NOT (EMPTY ( L_FILTER) AND this.w_FIFLGREM = "S")
        * --- Aggiungo il filtro, utilizzo cp_ToStrODBC() per convertire L_FILTER in una stringa 
        *     adatta per effettuare il filtro
        if TYPE("L_FILTER") = "C"
          L_FILTER = L_FILTER + IIF(this.w_FIFLGAZI="S", i_CODAZI,"")
        endif
        this.w_IBPOCX.AddFilter("D", this.w_FIFLNAME, ALLTRIM(this.w_FITIPFIL) + " " + cp_ToStrODBC( L_FILTER ))     
      endif
    enddo
    * --- Eseguo la pubblicazione
    this.w_IBPOCX.ExecuteQuery()     
    if !EMPTY(this.w_TMPDIR7ZIP)
      * --- Creo 7ZIP
      if !ZIPUNZIP("Z", FULLPATH(FORCEEXT(ADDBS(this.w_DTPATH)+this.w_DTFILE, "7Z")), this.w_TMPDIR7ZIP)
         Messaggio="Errore"
      endif
      * --- Cancello i file
      if !DELETEFOLDER(this.w_TMPDIR7ZIP)
         Messaggio="Errore"
      endif
      this.w_DTFILE = FORCEEXT(this.w_DTFILE, "7Z")
    endif
    * --- Ripulisco la lista dei filtri
    this.w_IBPOCX.ClearFilterList()     
    if not empty(this.pd_StrProcesso)
      GSDM_BPD (this,"EP", this.pd_KeyIstanza, w_IRDR_RET)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if file(ALLTRIM( this.w_DTPATH ) + "\" + this.w_DTFILE + this.w_EXT)
        delete file ALLTRIM( this.w_DTPATH ) + "\" + this.w_DTFILE + this.w_EXT
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pQRY_GRP,pFLQRYGRP,pMessage,pFILTRATA,pGIROWNUM,pZOOM)
    this.pQRY_GRP=pQRY_GRP
    this.pFLQRYGRP=pFLQRYGRP
    this.pMessage=pMessage
    this.pFILTRATA=pFILTRATA
    this.pGIROWNUM=pGIROWNUM
    this.pZOOM=pZOOM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,14)]
    this.cWorkTables[1]='INF_AQM'
    this.cWorkTables[2]='INF_AQD'
    this.cWorkTables[3]='UTE_NTI'
    this.cWorkTables[4]='INF_PZCP'
    this.cWorkTables[5]='ZFOLDERS'
    this.cWorkTables[6]='ZDESTIN'
    this.cWorkTables[7]='INF_GRQD'
    this.cWorkTables[8]='INF_AFIM'
    this.cWorkTables[9]='INF_AGIM'
    this.cWorkTables[10]='INFPARFI'
    this.cWorkTables[11]='AUT_LPE'
    this.cWorkTables[12]='INF_FCPZ'
    this.cWorkTables[13]='CONTROPA'
    this.cWorkTables[14]='ACC_MAIL'
    return(this.OpenAllTables(14))

  proc CloseCursors()
    if used('_Curs_GSIR2BGD')
      use in _Curs_GSIR2BGD
    endif
    if used('_Curs_INF_AQD')
      use in _Curs_INF_AQD
    endif
    if used('_Curs_INF_FCPZ')
      use in _Curs_INF_FCPZ
    endif
    if used('_Curs_INF_PZCP')
      use in _Curs_INF_PZCP
    endif
    if used('_Curs_INF_FCPZ')
      use in _Curs_INF_FCPZ
    endif
    if used('_Curs_INF_AFIM')
      use in _Curs_INF_AFIM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsir_bgd
  * ===============================================================================
      * convertXML() - Funzione che controlla caratteri non graditi a XML
      *
      FUNCTION convertXML
      PARAMETERS sXml
      sXml = STRTRAN(sXml,"&","&amp;")
      sXml = STRTRAN(sXml,"'","&apos;")
      sXml = STRTRAN(sXml,">","&gt;")
      sXml = STRTRAN(sXml,"<","&lt;")
      sXml = STRTRAN(sXml,CHR(34),"&quot;")
      sXml = STRTRAN(sXml,CHR(13)+CHR(10),"{nbsp;}")
      sXml = STRTRAN(sXml,CHR(1),"")
      sXml = STRTRAN(sXml,CHR(2),"")
      sXml = STRTRAN(sXml,CHR(3),"")
      sXml = STRTRAN(sXml,CHR(4),"")
      sXml = STRTRAN(sXml,CHR(5),"")
      sXml = STRTRAN(sXml,CHR(6),"")
      sXml = STRTRAN(sXml,CHR(7),"")
      sXml = STRTRAN(sXml,CHR(8),"")
      sXml = STRTRAN(sXml,CHR(11),"")
      sXml = STRTRAN(sXml,CHR(12),"")
      sXml = STRTRAN(sXml,CHR(14),"")
      sXml = STRTRAN(sXml,CHR(15),"")
      sXml = STRTRAN(sXml,CHR(16),"")
      sXml = STRTRAN(sXml,CHR(17),"")
      sXml = STRTRAN(sXml,CHR(18),"")
      sXml = STRTRAN(sXml,CHR(19),"")
      sXml = STRTRAN(sXml,CHR(20),"")
      sXml = STRTRAN(sXml,CHR(21),"")
      sXml = STRTRAN(sXml,CHR(22),"")
      sXml = STRTRAN(sXml,CHR(23),"")
      sXml = STRTRAN(sXml,CHR(24),"")
      sXml = STRTRAN(sXml,CHR(25),"")
      sXml = STRTRAN(sXml,CHR(26),"")
      sXml = STRTRAN(sXml,CHR(27),"")
      sXml = STRTRAN(sXml,CHR(28),"")
      sXml = STRTRAN(sXml,CHR(29),"")
      sXml = STRTRAN(sXml,CHR(30),"")
      sXml = STRTRAN(sXml,CHR(31),"")
      sXml = STRTRAN(sXml,CHR(127),"")
      sXml = STRTRAN(sXml,CHR(129),"")
      sXml = STRTRAN(sXml,CHR(141),"")
      sXml = STRTRAN(sXml,CHR(143),"")
      sXml = STRTRAN(sXml,CHR(144),"")
      sXml = STRTRAN(sXml,CHR(157),"")
      RETURN sXml 
    Endfunc
  
  EndDefine
  
  Define class query_InfoPub_param_mask as query_param_mask
   proc ok.click()
      local i
      for i=2 to this.parent.nCtrls step 2
        if this.parent.Types[i]='C'
          this.parent.oQ.a_FParameter.Set(this.parent.nIdx[i], this.parent.oQ.aFP_Value, this.parent.Ctrls[i].value)
        else
          this.parent.oQ.a_FParameter.Set(this.parent.nIdx[i], this.parent.oQ.aFP_Value, this.parent.Ctrls[i].value)
        endif
      next
      i_curform=this.parent.oOldForm
      this.parent.oQ=.NULL.
      thisform.Hide()
      thisform.release()
    proc cancel.click()
      local i
      for i=2 to this.parent.nCtrls step 2
        this.parent.oQ.a_FParameter.Set(this.parent.nIdx[i], this.parent.oQ.aFP_Value, cp_NullValue(this.parent.Types[i]))
      next
      i_curform=this.parent.oOldForm
      this.parent.oQ=.NULL.
      thisform.Hide()
      thisform.release()
  EndDefine
  
  * --- Classe per la gestione degli array
  Define class Stack as custom
  declare a_Stack[1]
  Dim_X = 1
  Dim_Y = 0
  * --- Init : inizializza le dimensioni dell'array
  Proc Init(x, y)
    Do Case
      Case Parameters( ) = 1
        This.Dim(x)
      Case Parameters( ) = 2
        This.Dim(x, y)
    EndCase
  
  * --- Dim : Aggiunge/modifica le dimensioni di un array/matrice
  Proc Dim(x, y)
    Do Case
      Case Parameters( ) = 1
        Dimension This.a_Stack[x]
        This.Dim_X = x
      Case Parameters( ) = 2
        Dimension This.a_Stack[x, y]
        This.Dim_X = x
        This.Dim_Y = y
    EndCase
  
  * --- Ret : Ritorna un elemento dell'array
  Func Ret(x, y)
    Do Case
      Case Parameters( ) = 1
        If x > 0 And x <= This.Dim_X
          Return This.a_Stack[x]
        EndIf
      Case Parameters( ) = 2
        If x > 0 And x <= This.Dim_X And y > 0 And y <= This.Dim_Y
          Return This.a_Stack[x, y]
        EndIf
    EndCase
  
  * --- Set : Setta un elemento dell'array
  Proc Set(x, y, w)
    Do Case
      Case Parameters( ) = 2
        If x > 0 And x <= This.Dim_X
          This.a_Stack[x] = y
        EndIf
      Case Parameters( ) = 3
        If x > 0 And x <= This.Dim_X And y > 0 And y <= This.Dim_Y
          This.a_Stack[x, y] = w
        EndIf
    EndCase
  EndDefine
  
  * --- Questa classe permette di controllare e recuperare le variabili 
  * --- di una gestione o del suo oParentObject
  * --- NB: Avrei potuto utilizzare i metodi IsVar e GetVar del Painter
  * ---     ma nel caso in cui io utilizzi una struttura composta come
  * ---     w_ZOOM.GetVar('CAMPO') la type che viine eseguita da IsVar 
  * ---     non � indicata per determinare se � possibile recuperare il
  * ---     valore.
  Define class ObjInt as custom
  oObj = .null.  &&Oggetto da cui recuperare le variabili
  nStat = 0  && indica se il metodo GetVar debba ritornare oObj.w_ o oObj.oParentOnbject.w_
  lZoom = .f.  &&Flag zoom, se true specifica che l'oggetto oObj � uno zoom
  
  Proc Init(oObject, lFlg)
    this.oObj=oObject
    this.nStat = -1
    this.lZoom = lFlg
  
  * --- Determina se la variabile � presente o meno sulla gestione oObj
  Func IsVar(cVarName)
    Do Case
      Case this.lZoom
      * --- Zoom
        i = 0
        ret = .f.
        Do while i < This.oObj.nFields
          i = i +1
          If Upper(This.oObj.cFields[i]) == Upper(cVarName)
            ret = .t.
          Endif
        Enddo
        If Type('g_omenu.oKey')<>'U' And Not ret
        For k=1 To Alen(g_omenu.oKey,1)
           If Upper(g_omenu.oKey[k,1])==Upper(cVarName)
              this.nStat = 0
              ret=.t.
            Endif
        Endfor
        Endif
        Return ret
      Otherwise
      * --- StdForm
        L_errsav1=on('ERROR')
        MessErr=' '
        ON ERROR MessErr=Message()
          L_COMMAND = 'L_FILTER = this.oObj.w_' + cVarName
          &L_COMMAND
        ON ERROR &L_errsav1
        If Empty(MessErr)
          this.nStat = 1
          Return .t.
        Else
          L_errsav1=on('ERROR')
          MessErr=' '
          ON ERROR MessErr=Message()
            L_COMMAND = 'L_FILTER = this.oObj.oParentObject.w_' + cVarName
            &L_COMMAND
          ON ERROR &L_errsav1
          If Empty(MessErr)
            this.nStat = 2
            Return .t.
          Else
            this.nStat = 0
            If Type('g_omenu.oKey')<>'U'
              For k=1 To Alen(g_omenu.oKey,1)
                If Upper(g_omenu.oKey[k,1])==Upper(cVarName)
                  Return .t.
                Endif
              Endfor
            Endif
            Return .f.
          Endif
        Endif
    Endcase
  * --- Ritorna il valore della variabile selezionata
  * --- GetVar deve essere preceduta da IsVar affinch� quest'ultima valorizzi nStat
  Func GetVar(cVarName)
    Do Case
      Case This.nStat = 0
        Return g_omenu.getValue(cVarName)
      Case This.lZoom
      * --- Zoom
        Select (This.oObj.cCursor)
        Return &cVarName
      Otherwise
      * --- StdForm
        Do Case
          Case this.nStat = 1
            m = 'Return this.oObj.w_' + cVarName
            &m
          Case this.nStat = 2
            m = 'Return this.oObj.oParentObject.w_' + cVarName
            &m
        Endcase
    Endcase
  
    
  
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pQRY_GRP,pFLQRYGRP,pMessage,pFILTRATA,pGIROWNUM,pZOOM"
endproc
