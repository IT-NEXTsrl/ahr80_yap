* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bsr                                                        *
*              Selezione query InfoReader                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-24                                                      *
* Last revis.: 2004-02-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bsr",oParentObject)
return(i_retval)

define class tgsir_bsr as StdBatch
  * --- Local variables
  w_PUNPAD = .NULL.
  w_MESSAGGIO = space(254)
  w_PRGNAME = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza i filtri w_GIPROGRA e w_FILTRSI (da GSIR_KSR)
    this.w_PUNPAD = this.oParentObject
    this.oParentObject.w_UGCODUTE  = i_CODUTE
    * --- Valorizzo i filtri sulla gestione
    * --- Di default non mostro le query filtrate
    this.oParentObject.w_FILTRSI = "N"
    if Type( "this.oParentObject.w_OBJECT.cprg" )<>"C"
      this.oParentObject.w_GIPROGRA = space(254)
    else
      * --- Aggiungo trailing spaces per ottenere un stringa confrontabile con
      *     il nome programma memorizzato.
      this.w_PRGNAME = Upper ( this.oParentObject.w_OBJECT.getSecurityCode() )
      if  At( ",", this.w_PRGNAME ) <> 0
        this.oParentObject.w_GIPROGRA = Padr( Left( this.w_PRGNAME, At( ",", this.w_PRGNAME ) - 1 ) , 254 )
        this.oParentObject.w_GIPARAM = Padr( SubStr( this.w_PRGNAME, At( ",", this.w_PRGNAME ) + 1 ) , 15 )
      else
        this.oParentObject.w_GIPROGRA = Padr( this.w_PRGNAME , 254 )
      endif
      * --- Mostro anche le query filtrate
      this.oParentObject.w_FILTRSI = "S"
    endif
    this.w_PUNPAD.NotifyEvent("Aggiorna")     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
