* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bdq                                                        *
*              Duplica query                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-11                                                      *
* Last revis.: 2007-09-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOLDKEY
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bdq",oParentObject,m.pOLDKEY)
return(i_retval)

define class tgsir_bdq as StdBatch
  * --- Local variables
  pOLDKEY = space(20)
  w_GSIR_MGI = .NULL.
  w_GSIR_MFR = .NULL.
  w_GSIR_MPF = .NULL.
  w_GIROWNUM = 0
  w_GICODQUE = space(20)
  w_FUNCTION = space(20)
  * --- WorkFile variables
  INF_AFIM_idx=0
  INFPARFI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Duplico il contenuto della gestione GSIR_MFR (da GSIR_MAQ)
    this.w_GICODQUE = this.pOLDKEY
    * --- Valorizzo i filtri delle gestioni associate
    * --- Recupero gli oggetti
    this.w_GSIR_MGI = this.oParentObject.GSIR_MGI
    this.w_GSIR_MFR = this.oParentObject.GSIR_MGI.GSIR_MFR
    * --- Ciclo sul transitorio della gestione padre GSIR_MGI
     
 Select (this.w_GSIR_MGI.cTrsName) 
 Go top 
 Scan for not deleted()
    this.w_GSIR_MGI.ChildrenChangeRow()     
    this.w_GIROWNUM = CPROWNUM
    * --- Eseguendo la LinkPCClick istanzio il figlio GSIR_MFR
    this.w_GSIR_MFR.LinkPCClick(.T.)     
    * --- La LinkPCClick ha modificato il puntatore a GSIR_MFR della gestione GSIR_MGI 
    *     quindi devo recuperarlo di nuovo
    this.w_GSIR_MFR = this.oParentObject.GSIR_MGI.GSIR_MFR
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
     
 Select (this.w_GSIR_MGI.cTrsName) 
 EndScan
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riempie le righe della movimentazione
    this.w_GSIR_MFR.Hide()     
    * --- Select from INF_AFIM
    i_nConn=i_TableProp[this.INF_AFIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2],.t.,this.INF_AFIM_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CPROWNUM,CPROWORD,FIFLNAME,FITIPFIL,FIFILTRO,FIPRGRN,FIVISMSK,FIFLGREM,FIFLGAZI  from "+i_cTable+" INF_AFIM ";
          +" where FICODQUE = "+cp_ToStrODBC(this.w_GICODQUE)+" AND FIPRGRN = "+cp_ToStrODBC(this.w_GIROWNUM)+"";
           ,"_Curs_INF_AFIM")
    else
      select CPROWNUM,CPROWORD,FIFLNAME,FITIPFIL,FIFILTRO,FIPRGRN,FIVISMSK,FIFLGREM,FIFLGAZI from (i_cTable);
       where FICODQUE = this.w_GICODQUE AND FIPRGRN = this.w_GIROWNUM;
        into cursor _Curs_INF_AFIM
    endif
    if used('_Curs_INF_AFIM')
      select _Curs_INF_AFIM
      locate for 1=1
      do while not(eof())
      this.w_GSIR_MFR.cnt.AddRow()     
      this.w_GSIR_MFR.cnt.w_FIPRGRN = _Curs_INF_AFIM.FIPRGRN
      this.w_GSIR_MFR.cnt.w_CPROWORD = _Curs_INF_AFIM.CPROWORD
      this.w_GSIR_MFR.cnt.w_FIFLNAME = _Curs_INF_AFIM.FIFLNAME
      this.w_GSIR_MFR.cnt.w_FITIPFIL = _Curs_INF_AFIM.FITIPFIL
      this.w_GSIR_MFR.cnt.w_FIFILTRO = _Curs_INF_AFIM.FIFILTRO
      this.w_GSIR_MFR.cnt.w_FIVISMSK = _Curs_INF_AFIM.FIVISMSK
      this.w_GSIR_MFR.cnt.w_FIFLGREM = _Curs_INF_AFIM.FIFLGREM
      this.w_GSIR_MFR.cnt.w_FIFLGAZI = _Curs_INF_AFIM.FIFLGAZI
      this.w_GSIR_MFR.cnt.TrsFromWork()     
        select _Curs_INF_AFIM
        continue
      enddo
      use
    endif
    this.w_GSIR_MFR.cnt.bUpdated = .t.
    this.w_FUNCTION = this.w_GSIR_MFR.cFunction
    * --- Per evitare l'esecuzione della CheckForm
    this.w_GSIR_MFR.cFunction = "Query"
    this.w_GSIR_MFR.EcpSave()     
    this.w_GSIR_MFR.cFunction = this.w_FUNCTION
  endproc


  proc Init(oParentObject,pOLDKEY)
    this.pOLDKEY=pOLDKEY
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='INF_AFIM'
    this.cWorkTables[2]='INFPARFI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_INF_AFIM')
      use in _Curs_INF_AFIM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOLDKEY"
endproc
