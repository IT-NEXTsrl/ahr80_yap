* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bfr                                                        *
*              Zoom filtro                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2005-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bfr",oParentObject)
return(i_retval)

define class tgsir_bfr as StdBatch
  * --- Local variables
  w_PFVARNAM = space(10)
  w_FUNCTION = space(20)
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apre lo zoom per poter appendere i parametri ai filtri (da GSIR_MFR)
    * --- Determino se i filtri sono generici 
    if TYPE("This.oParentObject.oParentObject.oParentObject.w_DUPLICA") = "C"
      * --- Da gestioni
      this.w_OBJ = This.oParentObject.oParentObject.oParentObject
    else
      * --- Generico
      this.w_OBJ = This.oParentObject.oParentObject
    endif
    * --- Quando w_DUPLICA='S' non viene eseguita la show dei figli quindi posso
    *     istanziare il figlio senza visualizzarlo
    this.w_OBJ.w_DUPLICA = "S"
    this.w_OBJ.GSIR_MPF.LinkPCClick()     
    this.w_OBJ.w_DUPLICA = "N"
    * --- Una volta aperto devo chiuderlo
    this.w_FUNCTION = this.w_OBJ.GSIR_MPF.cFunction
    * --- Per evitare l'esecuzione della CheckForm
    this.w_OBJ.GSIR_MPF.cFunction = "Query"
    * --- Chiudo la maschera
    this.w_OBJ.GSIR_MPF.EcpSave()     
    this.w_OBJ.GSIR_MPF.cFunction = this.w_FUNCTION
    * --- A questo punto � possibile interrogare il cTrsName della gestione Parametri
    * --- Lancio la maschera con lo zoom
    do GSIR_KPR with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiungo al filtro la variabile
    if NOT EMPTY(this.w_PFVARNAM)
      this.oParentObject.w_FIFILTRO = ALLTRIM(this.oParentObject.w_FIFILTRO) + "?" + ALLTRIM(this.w_PFVARNAM) + "?" 
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
