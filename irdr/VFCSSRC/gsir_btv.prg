* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_btv                                                        *
*              Tabella vista query InfoPublisher                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-10                                                      *
* Last revis.: 2005-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_btv",oParentObject)
return(i_retval)

define class tgsir_btv as StdBatch
  * --- Local variables
  * --- WorkFile variables
  VISTAQUERY_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Popolo la tabella che conterra la vista delle query infopublisher
    * --- Per questioni di velocit� occorre inserire nella tabella VISTAQUERY tutte
    *     le query infopublisher con le informazioni sulle autorizzazioni e sulle gestioni,
    *     le query sono gi� 'esplose', cio� la stessa query se di tipo STD compare pi�
    *     volte (da pubblicare, gi� pubblicate)
    * --- Ripulisco la tabella
    ah_Msg("Cancellazione tabella vista query InfoPublisher",.T.)
    * --- Delete from VISTAQUERY
    i_nConn=i_TableProp[this.VISTAQUERY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VISTAQUERY_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    ah_Msg("Inserimento dati nella tabella vista query InfoPublisher",.T.)
    * --- Insert into VISTAQUERY
    i_nConn=i_TableProp[this.VISTAQUERY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VISTAQUERY_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\IRDR\EXE\QUERY\GSIR8BSR",this.VISTAQUERY_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into VISTAQUERY
    i_nConn=i_TableProp[this.VISTAQUERY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VISTAQUERY_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\IRDR\EXE\QUERY\GSIR9BSR",this.VISTAQUERY_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into VISTAQUERY
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VISTAQUERY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VISTAQUERY_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VISTAQUERY_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CODUTE ="+cp_NullLink(cp_ToStrODBC(-9999),'VISTAQUERY','CODUTE');
          +i_ccchkf ;
      +" where ";
          +"CODUTE is null ";
             )
    else
      update (i_cTable) set;
          CODUTE = -9999;
          &i_ccchkf. ;
       where;
          CODUTE is null;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from VISTAQUERY
    i_nConn=i_TableProp[this.VISTAQUERY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VISTAQUERY_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".AQCODICE  = "+i_cQueryTable+".AQCODICE ";
            +" and "+i_cTable+".AQDES     = "+i_cQueryTable+".AQDES    ";
            +" and "+i_cTable+".FORTYP   = "+i_cQueryTable+".FORTYP  ";
            +" and "+i_cTable+".TIPO = "+i_cQueryTable+".TIPO";
            +" and "+i_cTable+".AQFORTYP  = "+i_cQueryTable+".AQFORTYP ";
            +" and "+i_cTable+".AQQUEFIL    = "+i_cQueryTable+".AQQUEFIL   ";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
            +" and "+i_cTable+".CODUTE = "+i_cQueryTable+".CODUTE";
            +" and "+i_cTable+".PROGRAM = "+i_cQueryTable+".PROGRAM";
            +" and "+i_cTable+".PARAM = "+i_cQueryTable+".PARAM";
            +" and "+i_cTable+".MENU = "+i_cQueryTable+".MENU";
    
      do vq_exec with '..\IRDR\EXE\QUERY\GSIR9BSR1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Write into VISTAQUERY
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VISTAQUERY_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VISTAQUERY_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VISTAQUERY_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CODUTE ="+cp_NullLink(cp_ToStrODBC(NULL),'VISTAQUERY','CODUTE');
          +i_ccchkf ;
      +" where ";
          +"CODUTE = "+cp_ToStrODBC(-9999);
             )
    else
      update (i_cTable) set;
          CODUTE = NULL;
          &i_ccchkf. ;
       where;
          CODUTE = -9999;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.bUpdateParentObject=.f.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VISTAQUERY'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
