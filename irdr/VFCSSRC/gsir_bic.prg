* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bic                                                        *
*              Crea cursore per tasto dx                                       *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_37]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-01                                                      *
* Last revis.: 2006-12-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pGESTIONE,pOPER,pCODQUE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bic",oParentObject,m.pGESTIONE,m.pOPER,m.pCODQUE)
return(i_retval)

define class tgsir_bic as StdBatch
  * --- Local variables
  pGESTIONE = space(254)
  pOPER = space(0)
  pCODQUE = space(20)
  w_UGCODUTE = 0
  w_GIPROGRA = space(254)
  w_GIPARAM = space(15)
  w_VISQUERY = space(1)
  w_PRGNAME = space(50)
  w_TMPCURSOR = space(20)
  w_FIND = .f.
  * --- WorkFile variables
  VISTAQUERY_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore con l'elenco delle query associate alla gestione corrente
    *     Testa se l'utente pu� aprire al query
    this.w_UGCODUTE = i_CODUTE
    this.w_PRGNAME = Upper ( this.pGESTIONE )
    if  At( ",", this.w_PRGNAME ) <> 0
      this.w_GIPROGRA = Padr( Left( this.w_PRGNAME, At( ",", this.w_PRGNAME ) - 1 ) , 254 )
      this.w_GIPARAM = Padr( SubStr( this.w_PRGNAME, At( ",", this.w_PRGNAME ) + 1 ) , 15 )
    else
      this.w_GIPROGRA = Padr( this.w_PRGNAME , 254 )
    endif
    this.w_VISQUERY = "T"
    * --- Se w_GIPARAM � vuoto lo inizializzo a @
    if EMPTY(this.w_GIPARAM)
      this.w_GIPARAM = REPLICATE("@", 15)
    endif
    * --- Recupero le informazioni su connessione e tabella
     
 i_nConn=i_TableProp[this.VISTAQUERY_idx,3] 
 i_cTable=cp_SetAzi(i_TableProp[this.VISTAQUERY_idx,2])
    do case
      case this.pOPER
        this.w_TMPCURSOR = "TMPCURSOR"
        if i_nConn<>0
           
 sqlexec(i_nConn,"select *  from "+i_cTable; 
 +" where ((CODUTE = "+cp_ToStrODBC(this.w_UGCODUTE) + " OR CODUTE is null )  AND ("+cp_ToStrODBC(this.w_GIPARAM)+ ; 
 " = '@@@@@@@@@@@@@@@' OR PARAM = '@@@@@@@@@@@@@@@' OR PARAM = "; 
 +cp_ToStrODBC(this.w_GIPARAM) + ") AND (TIPO = "+cp_ToStrODBC(this.w_VISQUERY)+; 
 " OR "+cp_ToStrODBC(this.w_VISQUERY)+" = 'T')) order by MENU" ; 
 , this.w_TMPCURSOR)
        endif
      otherwise
        this.w_TMPCURSOR = This.oParentObject.cCurInfo
        if i_nConn<>0
           
 sqlexec(i_nConn,"select *  from "+i_cTable; 
 +" where ((CODUTE = "+cp_ToStrODBC(this.w_UGCODUTE) + " OR CODUTE is null ) AND PROGRAM = " ; 
 +cp_ToStrODBC(this.w_GIPROGRA)+" AND ("+cp_ToStrODBC(this.w_GIPARAM)+ ; 
 " = '@@@@@@@@@@@@@@@' OR PARAM = '@@@@@@@@@@@@@@@' OR PARAM = "; 
 +cp_ToStrODBC(this.w_GIPARAM) + ") AND (TIPO = "+cp_ToStrODBC(this.w_VISQUERY)+; 
 " OR "+cp_ToStrODBC(this.w_VISQUERY)+" = 'T')) order by MENU" ; 
 , this.w_TMPCURSOR)
        endif
    endcase
    * --- Eseguo una sqlexec per popolare il cursore
    if this.pOPER AND USED("TMPCURSOR")
      this.w_FIND = .F.
       
 SELECT TMPCURSOR 
 LOCATE FOR 1 = 1
      do while NOT EOF()
        if UPPER(ALLTRIM(this.pCODQUE)) == UPPER(ALLTRIM(AQCODICE))
          this.w_FIND = .T.
          EXIT
        endif
         
 SELECT TMPCURSOR 
 CONTINUE
      enddo
      USE IN SELECT("TMPCURSOR")
      if ! this.w_FIND
        ah_ErrorMsg("Accesso negato.%0L'utente non ha i diritti per accedere alla query: %1","!","",ALLTRIM(this.pCODQUE))
      endif
      i_retcode = 'stop'
      i_retval = this.w_FIND
      return
    endif
    * --- Non aggiorno le varibili dell'oParentObject, oParentObject � null
    this.bupdateparentobject=.f.
  endproc


  proc Init(oParentObject,pGESTIONE,pOPER,pCODQUE)
    this.pGESTIONE=pGESTIONE
    this.pOPER=pOPER
    this.pCODQUE=pCODQUE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VISTAQUERY'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pGESTIONE,pOPER,pCODQUE"
endproc
