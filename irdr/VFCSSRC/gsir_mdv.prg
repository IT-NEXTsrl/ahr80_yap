* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_mdv                                                        *
*              Corporate Portal - riservato a                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-22                                                      *
* Last revis.: 2012-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsir_mdv")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsir_mdv")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsir_mdv")
  return

* --- Class definition
define class tgsir_mdv as StdPCForm
  Width  = 807
  Height = 358
  Top    = 13
  Left   = 15
  cComment = "Corporate Portal - riservato a"
  cPrg = "gsir_mdv"
  HelpContextID=80310121
  add object cnt as tcgsir_mdv
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsir_mdv as PCContext
  w_DECODICE = space(20)
  w_DERISERV = space(1)
  w_CPROWORD = 0
  w_DETIPDES = space(1)
  w_OTIPGRU = space(1)
  w_DECODGRU = 0
  w_DETIPCON = space(1)
  w_DECODCON = space(15)
  w_DECODAGE = space(5)
  w_DERIFCFO = space(1)
  w_DEVALDES = space(25)
  w_DECODROL = 0
  w_DE__READ = space(1)
  w_DE_WRITE = space(1)
  w_DEDELETE = space(1)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_DESGRUPPO = space(50)
  w_DESCON = space(40)
  w_DESAGE = space(35)
  w_DESRIGA = space(40)
  w_DEFOLDER = 0
  w_DESFOLD = space(50)
  w_PATHFOLD = space(255)
  w_PERMESSO = space(1)
  w_ATTCORP = space(1)
  w_DEDESFIL = space(100)
  w_DE__NOTE = space(10)
  w_TIPORIGA = space(1)
  w_FO__TIPO = space(1)
  w_RISPUB = space(1)
  proc Save(i_oFrom)
    this.w_DECODICE = i_oFrom.w_DECODICE
    this.w_DERISERV = i_oFrom.w_DERISERV
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DETIPDES = i_oFrom.w_DETIPDES
    this.w_OTIPGRU = i_oFrom.w_OTIPGRU
    this.w_DECODGRU = i_oFrom.w_DECODGRU
    this.w_DETIPCON = i_oFrom.w_DETIPCON
    this.w_DECODCON = i_oFrom.w_DECODCON
    this.w_DECODAGE = i_oFrom.w_DECODAGE
    this.w_DERIFCFO = i_oFrom.w_DERIFCFO
    this.w_DEVALDES = i_oFrom.w_DEVALDES
    this.w_DECODROL = i_oFrom.w_DECODROL
    this.w_DE__READ = i_oFrom.w_DE__READ
    this.w_DE_WRITE = i_oFrom.w_DE_WRITE
    this.w_DEDELETE = i_oFrom.w_DEDELETE
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_DESGRUPPO = i_oFrom.w_DESGRUPPO
    this.w_DESCON = i_oFrom.w_DESCON
    this.w_DESAGE = i_oFrom.w_DESAGE
    this.w_DESRIGA = i_oFrom.w_DESRIGA
    this.w_DEFOLDER = i_oFrom.w_DEFOLDER
    this.w_DESFOLD = i_oFrom.w_DESFOLD
    this.w_PATHFOLD = i_oFrom.w_PATHFOLD
    this.w_PERMESSO = i_oFrom.w_PERMESSO
    this.w_ATTCORP = i_oFrom.w_ATTCORP
    this.w_DEDESFIL = i_oFrom.w_DEDESFIL
    this.w_DE__NOTE = i_oFrom.w_DE__NOTE
    this.w_TIPORIGA = i_oFrom.w_TIPORIGA
    this.w_FO__TIPO = i_oFrom.w_FO__TIPO
    this.w_RISPUB = i_oFrom.w_RISPUB
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DECODICE = this.w_DECODICE
    i_oTo.w_DERISERV = this.w_DERISERV
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DETIPDES = this.w_DETIPDES
    i_oTo.w_OTIPGRU = this.w_OTIPGRU
    i_oTo.w_DECODGRU = this.w_DECODGRU
    i_oTo.w_DETIPCON = this.w_DETIPCON
    i_oTo.w_DECODCON = this.w_DECODCON
    i_oTo.w_DECODAGE = this.w_DECODAGE
    i_oTo.w_DERIFCFO = this.w_DERIFCFO
    i_oTo.w_DEVALDES = this.w_DEVALDES
    i_oTo.w_DECODROL = this.w_DECODROL
    i_oTo.w_DE__READ = this.w_DE__READ
    i_oTo.w_DE_WRITE = this.w_DE_WRITE
    i_oTo.w_DEDELETE = this.w_DEDELETE
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_DESGRUPPO = this.w_DESGRUPPO
    i_oTo.w_DESCON = this.w_DESCON
    i_oTo.w_DESAGE = this.w_DESAGE
    i_oTo.w_DESRIGA = this.w_DESRIGA
    i_oTo.w_DEFOLDER = this.w_DEFOLDER
    i_oTo.w_DESFOLD = this.w_DESFOLD
    i_oTo.w_PATHFOLD = this.w_PATHFOLD
    i_oTo.w_PERMESSO = this.w_PERMESSO
    i_oTo.w_ATTCORP = this.w_ATTCORP
    i_oTo.w_DEDESFIL = this.w_DEDESFIL
    i_oTo.w_DE__NOTE = this.w_DE__NOTE
    i_oTo.w_TIPORIGA = this.w_TIPORIGA
    i_oTo.w_FO__TIPO = this.w_FO__TIPO
    i_oTo.w_RISPUB = this.w_RISPUB
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsir_mdv as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 807
  Height = 358
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-14"
  HelpContextID=80310121
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  INF_PZCP_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  ZRUOLI_IDX = 0
  ZFOLDERS_IDX = 0
  ZGRUPPI_IDX = 0
  INF_FCPZ_IDX = 0
  cFile = "INF_PZCP"
  cKeySelect = "DECODICE"
  cKeyWhere  = "DECODICE=this.w_DECODICE"
  cKeyDetail  = "DECODICE=this.w_DECODICE"
  cKeyWhereODBC = '"DECODICE="+cp_ToStrODBC(this.w_DECODICE)';

  cKeyDetailWhereODBC = '"DECODICE="+cp_ToStrODBC(this.w_DECODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"INF_PZCP.DECODICE="+cp_ToStrODBC(this.w_DECODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'INF_PZCP.CPROWORD '
  cPrg = "gsir_mdv"
  cComment = "Corporate Portal - riservato a"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DECODICE = space(20)
  w_DERISERV = space(1)
  o_DERISERV = space(1)
  w_CPROWORD = 0
  w_DETIPDES = space(1)
  o_DETIPDES = space(1)
  w_OTIPGRU = space(1)
  w_DECODGRU = 0
  o_DECODGRU = 0
  w_DETIPCON = space(1)
  w_DECODCON = space(15)
  o_DECODCON = space(15)
  w_DECODAGE = space(5)
  o_DECODAGE = space(5)
  w_DERIFCFO = space(1)
  o_DERIFCFO = space(1)
  w_DEVALDES = space(25)
  w_DECODROL = 0
  w_DE__READ = space(1)
  o_DE__READ = space(1)
  w_DE_WRITE = space(1)
  o_DE_WRITE = space(1)
  w_DEDELETE = space(1)
  o_DEDELETE = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESGRUPPO = space(50)
  w_DESCON = space(40)
  w_DESAGE = space(35)
  w_DESRIGA = space(40)
  o_DESRIGA = space(40)
  w_DEFOLDER = 0
  w_DESFOLD = space(50)
  w_PATHFOLD = space(255)
  w_PERMESSO = .F.
  w_ATTCORP = space(1)
  o_ATTCORP = space(1)
  w_DEDESFIL = space(100)
  w_DE__NOTE = space(0)
  w_TIPORIGA = space(1)
  w_FO__TIPO = space(1)
  w_RISPUB = space(1)

  * --- Children pointers
  GSIR_MPQ = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSIR_MPQ additive
    with this
      .Pages(1).addobject("oPag","tgsir_mdvPag1","gsir_mdv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDERISERV_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSIR_MPQ
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='ZRUOLI'
    this.cWorkTables[4]='ZFOLDERS'
    this.cWorkTables[5]='ZGRUPPI'
    this.cWorkTables[6]='INF_FCPZ'
    this.cWorkTables[7]='INF_PZCP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INF_PZCP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INF_PZCP_IDX,3]
  return

  function CreateChildren()
    this.GSIR_MPQ = CREATEOBJECT('stdLazyChild',this,'GSIR_MPQ')
    return

  procedure NewContext()
    return(createobject('tsgsir_mdv'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSIR_MPQ)
      this.GSIR_MPQ.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSIR_MPQ.HideChildrenChain()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSIR_MPQ)
      this.GSIR_MPQ.DestroyChildrenChain()
      this.GSIR_MPQ=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSIR_MPQ.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSIR_MPQ.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSIR_MPQ.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSIR_MPQ.ChangeRow(this.cRowID+'      1',1;
             ,.w_DECODICE,"PFCODICE";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from INF_PZCP where DECODICE=KeySet.DECODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.INF_PZCP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_PZCP_IDX,2],this.bLoadRecFilter,this.INF_PZCP_IDX,"gsir_mdv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INF_PZCP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INF_PZCP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INF_PZCP '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DECODICE',this.w_DECODICE  )
      select * from (i_cTable) INF_PZCP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESFOLD = space(50)
        .w_PATHFOLD = space(255)
        .w_PERMESSO = .F.
        .w_FO__TIPO = space(1)
        .w_DECODICE = NVL(DECODICE,space(20))
        .w_DERISERV = NVL(DERISERV,space(1))
        .w_DEFOLDER = NVL(DEFOLDER,0)
          if link_1_3_joined
            this.w_DEFOLDER = NVL(FOSERIAL103,NVL(this.w_DEFOLDER,0))
            this.w_DESFOLD = NVL(FODESCRI103,space(50))
            this.w_PATHFOLD = NVL(FO__PATH103,space(255))
            this.w_FO__TIPO = NVL(FO__TIPO103,space(1))
          else
          .link_1_3('Load')
          endif
        .w_ATTCORP = this.oParentObject .w_AQFLGZCP
        .w_DEDESFIL = NVL(DEDESFIL,space(100))
        .w_DE__NOTE = NVL(DE__NOTE,space(0))
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'INF_PZCP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESGRUPPO = space(50)
          .w_DESCON = space(40)
          .w_DESAGE = space(35)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DETIPDES = NVL(DETIPDES,space(1))
        .w_OTIPGRU = IIF(.w_DETIPDES='G','O','T')
          .w_DECODGRU = NVL(DECODGRU,0)
          if link_2_4_joined
            this.w_DECODGRU = NVL(code204,NVL(this.w_DECODGRU,0))
            this.w_DESGRUPPO = NVL(name204,space(50))
          else
          .link_2_4('Load')
          endif
          .w_DETIPCON = NVL(DETIPCON,space(1))
          .w_DECODCON = NVL(DECODCON,space(15))
          .link_2_6('Load')
          .w_DECODAGE = NVL(DECODAGE,space(5))
          if link_2_7_joined
            this.w_DECODAGE = NVL(AGCODAGE207,NVL(this.w_DECODAGE,space(5)))
            this.w_DESAGE = NVL(AGDESAGE207,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(AGDTOBSO207),ctod("  /  /  "))
          else
          .link_2_7('Load')
          endif
          .w_DERIFCFO = NVL(DERIFCFO,space(1))
          .w_DEVALDES = NVL(DEVALDES,space(25))
          .w_DECODROL = NVL(DECODROL,0)
          * evitabile
          *.link_2_10('Load')
          .w_DE__READ = NVL(DE__READ,space(1))
          .w_DE_WRITE = NVL(DE_WRITE,space(1))
          .w_DEDELETE = NVL(DEDELETE,space(1))
        .w_DESRIGA = IIF(.w_DETIPDES$'CF', .w_DESCON, IIF(.w_DETIPDES='A', .w_DESAGE, .w_DESGRUPPO))
        .w_TIPORIGA = .w_DETIPDES
        .w_RISPUB = .w_DERISERV
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_ATTCORP = this.oParentObject .w_AQFLGZCP
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DECODICE=space(20)
      .w_DERISERV=space(1)
      .w_CPROWORD=10
      .w_DETIPDES=space(1)
      .w_OTIPGRU=space(1)
      .w_DECODGRU=0
      .w_DETIPCON=space(1)
      .w_DECODCON=space(15)
      .w_DECODAGE=space(5)
      .w_DERIFCFO=space(1)
      .w_DEVALDES=space(25)
      .w_DECODROL=0
      .w_DE__READ=space(1)
      .w_DE_WRITE=space(1)
      .w_DEDELETE=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESGRUPPO=space(50)
      .w_DESCON=space(40)
      .w_DESAGE=space(35)
      .w_DESRIGA=space(40)
      .w_DEFOLDER=0
      .w_DESFOLD=space(50)
      .w_PATHFOLD=space(255)
      .w_PERMESSO=.f.
      .w_ATTCORP=space(1)
      .w_DEDESFIL=space(100)
      .w_DE__NOTE=space(0)
      .w_TIPORIGA=space(1)
      .w_FO__TIPO=space(1)
      .w_RISPUB=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_DERISERV = 'S'
        .DoRTCalc(3,3,.f.)
        .w_DETIPDES = 'C'
        .w_OTIPGRU = IIF(.w_DETIPDES='G','O','T')
        .w_DECODGRU = IIF(.w_DETIPDES='C',20, IIF(.w_DETIPDES='A',30, IIF(.w_DETIPDES='F',40, .w_DECODGRU)))
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_DECODGRU))
         .link_2_4('Full')
        endif
        .w_DETIPCON = IIF(.w_DETIPDES$'CF',.w_DETIPDES, ' ')
        .w_DECODCON = IIF(.w_DETIPDES$'CF',.w_DECODCON,' ')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_DECODCON))
         .link_2_6('Full')
        endif
        .w_DECODAGE = IIF(.w_DETIPDES='A',.w_DECODAGE,' ')
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DECODAGE))
         .link_2_7('Full')
        endif
        .w_DERIFCFO = 'N'
        .w_DEVALDES = IIF(.w_DETIPDES$'CF', .w_DECODCON, IIF(.w_DETIPDES='A', .w_DECODAGE, IIF(.w_DETIPDES='G', str(.w_DECODGRU,6,0), ' ')))
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_DECODROL))
         .link_2_10('Full')
        endif
        .w_DE__READ = "N"
        .w_DE_WRITE = 'N'
        .w_DEDELETE = 'N'
        .w_OBTEST = i_DATSYS
        .DoRTCalc(17,20,.f.)
        .w_DESRIGA = IIF(.w_DETIPDES$'CF', .w_DESCON, IIF(.w_DETIPDES='A', .w_DESAGE, .w_DESGRUPPO))
        .w_DEFOLDER = iif(.w_ATTCORP='S',.w_DEFOLDER,0)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_DEFOLDER))
         .link_1_3('Full')
        endif
        .DoRTCalc(23,24,.f.)
        .w_PERMESSO = .F.
        .w_ATTCORP = this.oParentObject .w_AQFLGZCP
        .w_DEDESFIL = iif(.w_ATTCORP='S' and empty(.w_DEDESFIL),LEFT(this.oParentObject .w_AQDES,100),'')
        .DoRTCalc(28,28,.f.)
        .w_TIPORIGA = .w_DETIPDES
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(30,30,.f.)
        .w_RISPUB = .w_DERISERV
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INF_PZCP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDERISERV_1_2.enabled_(i_bVal)
      .Page1.oPag.oDEFOLDER_1_3.enabled = i_bVal
      .Page1.oPag.oDEDESFIL_1_11.enabled = i_bVal
      .Page1.oPag.oDE__NOTE_1_14.enabled = i_bVal
      .Page1.oPag.oObj_1_16.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSIR_MPQ.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'INF_PZCP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSIR_MPQ.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INF_PZCP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DECODICE,"DECODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DERISERV,"DERISERV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEFOLDER,"DEFOLDER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEDESFIL,"DEDESFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DE__NOTE,"DE__NOTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_DETIPDES N(3);
      ,t_DECODGRU N(6);
      ,t_DECODCON C(15);
      ,t_DECODAGE C(5);
      ,t_DERIFCFO N(3);
      ,t_DE__READ N(3);
      ,t_DE_WRITE N(3);
      ,t_DEDELETE N(3);
      ,t_DESRIGA C(40);
      ,CPROWNUM N(10);
      ,t_OTIPGRU C(1);
      ,t_DETIPCON C(1);
      ,t_DEVALDES C(25);
      ,t_DECODROL N(6);
      ,t_DESGRUPPO C(50);
      ,t_DESCON C(40);
      ,t_DESAGE C(35);
      ,t_TIPORIGA C(1);
      ,t_RISPUB C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsir_mdvbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_2.controlsource=this.cTrsName+'.t_DETIPDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDECODGRU_2_4.controlsource=this.cTrsName+'.t_DECODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDECODCON_2_6.controlsource=this.cTrsName+'.t_DECODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDECODAGE_2_7.controlsource=this.cTrsName+'.t_DECODAGE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_8.controlsource=this.cTrsName+'.t_DERIFCFO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_11.controlsource=this.cTrsName+'.t_DE__READ'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_12.controlsource=this.cTrsName+'.t_DE_WRITE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_13.controlsource=this.cTrsName+'.t_DEDELETE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIGA_2_19.controlsource=this.cTrsName+'.t_DESRIGA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(47)
    this.AddVLine(125)
    this.AddVLine(197)
    this.AddVLine(348)
    this.AddVLine(413)
    this.AddVLine(690)
    this.AddVLine(713)
    this.AddVLine(736)
    this.AddVLine(759)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INF_PZCP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_PZCP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_PZCP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_PZCP_IDX,2])
      *
      * insert into INF_PZCP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INF_PZCP')
        i_extval=cp_InsertValODBCExtFlds(this,'INF_PZCP')
        i_cFldBody=" "+;
                  "(DECODICE,DERISERV,CPROWORD,DETIPDES,DECODGRU"+;
                  ",DETIPCON,DECODCON,DECODAGE,DERIFCFO,DEVALDES"+;
                  ",DECODROL,DE__READ,DE_WRITE,DEDELETE,DEFOLDER"+;
                  ",DEDESFIL,DE__NOTE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DECODICE)+","+cp_ToStrODBC(this.w_DERISERV)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_DETIPDES)+","+cp_ToStrODBCNull(this.w_DECODGRU)+;
             ","+cp_ToStrODBC(this.w_DETIPCON)+","+cp_ToStrODBCNull(this.w_DECODCON)+","+cp_ToStrODBCNull(this.w_DECODAGE)+","+cp_ToStrODBC(this.w_DERIFCFO)+","+cp_ToStrODBC(this.w_DEVALDES)+;
             ","+cp_ToStrODBCNull(this.w_DECODROL)+","+cp_ToStrODBC(this.w_DE__READ)+","+cp_ToStrODBC(this.w_DE_WRITE)+","+cp_ToStrODBC(this.w_DEDELETE)+","+cp_ToStrODBCNull(this.w_DEFOLDER)+;
             ","+cp_ToStrODBC(this.w_DEDESFIL)+","+cp_ToStrODBC(this.w_DE__NOTE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INF_PZCP')
        i_extval=cp_InsertValVFPExtFlds(this,'INF_PZCP')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DECODICE',this.w_DECODICE)
        INSERT INTO (i_cTable) (;
                   DECODICE;
                  ,DERISERV;
                  ,CPROWORD;
                  ,DETIPDES;
                  ,DECODGRU;
                  ,DETIPCON;
                  ,DECODCON;
                  ,DECODAGE;
                  ,DERIFCFO;
                  ,DEVALDES;
                  ,DECODROL;
                  ,DE__READ;
                  ,DE_WRITE;
                  ,DEDELETE;
                  ,DEFOLDER;
                  ,DEDESFIL;
                  ,DE__NOTE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DECODICE;
                  ,this.w_DERISERV;
                  ,this.w_CPROWORD;
                  ,this.w_DETIPDES;
                  ,this.w_DECODGRU;
                  ,this.w_DETIPCON;
                  ,this.w_DECODCON;
                  ,this.w_DECODAGE;
                  ,this.w_DERIFCFO;
                  ,this.w_DEVALDES;
                  ,this.w_DECODROL;
                  ,this.w_DE__READ;
                  ,this.w_DE_WRITE;
                  ,this.w_DEDELETE;
                  ,this.w_DEFOLDER;
                  ,this.w_DEDESFIL;
                  ,this.w_DE__NOTE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.INF_PZCP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_PZCP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not empty(t_CPROWORD) or t_RISPUB = 'N') and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'INF_PZCP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " DERISERV="+cp_ToStrODBC(this.w_DERISERV)+;
                 ",DEFOLDER="+cp_ToStrODBCNull(this.w_DEFOLDER)+;
                 ",DEDESFIL="+cp_ToStrODBC(this.w_DEDESFIL)+;
                 ",DE__NOTE="+cp_ToStrODBC(this.w_DE__NOTE)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'INF_PZCP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  DERISERV=this.w_DERISERV;
                 ,DEFOLDER=this.w_DEFOLDER;
                 ,DEDESFIL=this.w_DEDESFIL;
                 ,DE__NOTE=this.w_DE__NOTE;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not empty(t_CPROWORD) or t_RISPUB = 'N') and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INF_PZCP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'INF_PZCP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DERISERV="+cp_ToStrODBC(this.w_DERISERV)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DETIPDES="+cp_ToStrODBC(this.w_DETIPDES)+;
                     ",DECODGRU="+cp_ToStrODBCNull(this.w_DECODGRU)+;
                     ",DETIPCON="+cp_ToStrODBC(this.w_DETIPCON)+;
                     ",DECODCON="+cp_ToStrODBCNull(this.w_DECODCON)+;
                     ",DECODAGE="+cp_ToStrODBCNull(this.w_DECODAGE)+;
                     ",DERIFCFO="+cp_ToStrODBC(this.w_DERIFCFO)+;
                     ",DEVALDES="+cp_ToStrODBC(this.w_DEVALDES)+;
                     ",DECODROL="+cp_ToStrODBCNull(this.w_DECODROL)+;
                     ",DE__READ="+cp_ToStrODBC(this.w_DE__READ)+;
                     ",DE_WRITE="+cp_ToStrODBC(this.w_DE_WRITE)+;
                     ",DEDELETE="+cp_ToStrODBC(this.w_DEDELETE)+;
                     ",DEFOLDER="+cp_ToStrODBCNull(this.w_DEFOLDER)+;
                     ",DEDESFIL="+cp_ToStrODBC(this.w_DEDESFIL)+;
                     ",DE__NOTE="+cp_ToStrODBC(this.w_DE__NOTE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'INF_PZCP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DERISERV=this.w_DERISERV;
                     ,CPROWORD=this.w_CPROWORD;
                     ,DETIPDES=this.w_DETIPDES;
                     ,DECODGRU=this.w_DECODGRU;
                     ,DETIPCON=this.w_DETIPCON;
                     ,DECODCON=this.w_DECODCON;
                     ,DECODAGE=this.w_DECODAGE;
                     ,DERIFCFO=this.w_DERIFCFO;
                     ,DEVALDES=this.w_DEVALDES;
                     ,DECODROL=this.w_DECODROL;
                     ,DE__READ=this.w_DE__READ;
                     ,DE_WRITE=this.w_DE_WRITE;
                     ,DEDELETE=this.w_DEDELETE;
                     ,DEFOLDER=this.w_DEFOLDER;
                     ,DEDESFIL=this.w_DEDESFIL;
                     ,DE__NOTE=this.w_DE__NOTE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask footer and header children to save themselves
      * --- GSIR_MPQ : Saving
      this.GSIR_MPQ.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DECODICE,"PFCODICE";
             )
      this.GSIR_MPQ.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSIR_MPQ : Deleting
    this.GSIR_MPQ.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DECODICE,"PFCODICE";
           )
    this.GSIR_MPQ.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INF_PZCP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_PZCP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not empty(t_CPROWORD) or t_RISPUB = 'N') and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete INF_PZCP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not empty(t_CPROWORD) or t_RISPUB = 'N') and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INF_PZCP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_PZCP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .w_OTIPGRU = IIF(.w_DETIPDES='G','O','T')
        if .o_DETIPDES<>.w_DETIPDES
          .w_DECODGRU = IIF(.w_DETIPDES='C',20, IIF(.w_DETIPDES='A',30, IIF(.w_DETIPDES='F',40, .w_DECODGRU)))
          .link_2_4('Full')
        endif
          .w_DETIPCON = IIF(.w_DETIPDES$'CF',.w_DETIPDES, ' ')
        if .o_DETIPDES<>.w_DETIPDES
          .w_DECODCON = IIF(.w_DETIPDES$'CF',.w_DECODCON,' ')
          .link_2_6('Full')
        endif
        if .o_DETIPDES<>.w_DETIPDES
          .w_DECODAGE = IIF(.w_DETIPDES='A',.w_DECODAGE,' ')
          .link_2_7('Full')
        endif
        if .o_DETIPDES<>.w_DETIPDES
          .w_DERIFCFO = 'N'
        endif
          .w_DEVALDES = IIF(.w_DETIPDES$'CF', .w_DECODCON, IIF(.w_DETIPDES='A', .w_DECODAGE, IIF(.w_DETIPDES='G', str(.w_DECODGRU,6,0), ' ')))
          .link_2_10('Full')
        if .o_DETIPDES<>.w_DETIPDES
          .w_DE__READ = "N"
        endif
        if .o_DETIPDES<>.w_DETIPDES
          .w_DE_WRITE = 'N'
        endif
        if .o_DETIPDES<>.w_DETIPDES
          .w_DEDELETE = 'N'
        endif
        .DoRTCalc(16,20,.t.)
          .w_DESRIGA = IIF(.w_DETIPDES$'CF', .w_DESCON, IIF(.w_DETIPDES='A', .w_DESAGE, .w_DESGRUPPO))
        if .o_ATTCORP<>.w_ATTCORP.or. .o_DERISERV<>.w_DERISERV
          .w_DEFOLDER = iif(.w_ATTCORP='S',.w_DEFOLDER,0)
          .link_1_3('Full')
        endif
        .DoRTCalc(23,25,.t.)
          .w_ATTCORP = this.oParentObject .w_AQFLGZCP
        if .o_ATTCORP<>.w_ATTCORP
          .w_DEDESFIL = iif(.w_ATTCORP='S' and empty(.w_DEDESFIL),LEFT(this.oParentObject .w_AQDES,100),'')
        endif
        .DoRTCalc(28,28,.t.)
          .w_TIPORIGA = .w_DETIPDES
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(30,30,.t.)
          .w_RISPUB = .w_DERISERV
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        Local l_Dep1,l_Dep2
        l_Dep1= .o_DETIPDES<>.w_DETIPDES .or. .o_DECODGRU<>.w_DECODGRU .or. .o_DECODCON<>.w_DECODCON .or. .o_DECODAGE<>.w_DECODAGE .or. .o_DESRIGA<>.w_DESRIGA
        l_Dep2= .o_DERIFCFO<>.w_DERIFCFO .or. .o_DE__READ<>.w_DE__READ .or. .o_DE_WRITE<>.w_DE_WRITE .or. .o_DEDELETE<>.w_DEDELETE
        if m.l_Dep1 .or. m.l_Dep2
          .Calculate_HOKWRKSXLF()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OTIPGRU with this.w_OTIPGRU
      replace t_DETIPCON with this.w_DETIPCON
      replace t_DEVALDES with this.w_DEVALDES
      replace t_DECODROL with this.w_DECODROL
      replace t_DESGRUPPO with this.w_DESGRUPPO
      replace t_DESCON with this.w_DESCON
      replace t_DESAGE with this.w_DESAGE
      replace t_TIPORIGA with this.w_TIPORIGA
      replace t_RISPUB with this.w_RISPUB
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_HOKWRKSXLF()
    with this
          * --- Record modificato
          .bUpdated = .T.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLinkPC_1_6.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_6.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDECODGRU_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDECODGRU_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDECODCON_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDECODCON_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDECODAGE_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDECODAGE_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDERIFCFO_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDERIFCFO_2_8.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DECODGRU
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_lTable = "ZGRUPPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2], .t., this.ZGRUPPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZGRUPPI')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_DECODGRU);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_DECODGRU)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DECODGRU) and !this.bDontReportError
            deferred_cp_zoom('ZGRUPPI','*','code',cp_AbsName(oSource.parent,'oDECODGRU_2_4'),i_cWhere,'',"Gruppi organizzativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_DECODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_DECODGRU)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODGRU = NVL(_Link_.code,0)
      this.w_DESGRUPPO = NVL(_Link_.name,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DECODGRU = 0
      endif
      this.w_DESGRUPPO = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.ZGRUPPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZGRUPPI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.code as code204"+ ",link_2_4.name as name204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on INF_PZCP.DECODGRU=link_2_4.code"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and INF_PZCP.DECODGRU=link_2_4.code(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DECODCON
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DECODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DETIPCON;
                     ,'ANCODICE',trim(this.w_DECODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DECODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DECODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDECODCON_2_6'),i_cWhere,'',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DETIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente/fornitore non valido oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DECODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DETIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DETIPCON;
                       ,'ANCODICE',this.w_DECODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DECODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente/fornitore non valido oppure obsoleto")
        endif
        this.w_DECODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DECODAGE
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_DECODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_DECODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DECODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DECODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oDECODAGE_2_7'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_DECODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_DECODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DECODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente non valido oppure obsoleto")
        endif
        this.w_DECODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.AGCODAGE as AGCODAGE207"+ ",link_2_7.AGDESAGE as AGDESAGE207"+ ",link_2_7.AGDTOBSO as AGDTOBSO207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on INF_PZCP.DECODAGE=link_2_7.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and INF_PZCP.DECODAGE=link_2_7.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DECODROL
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZRUOLI_IDX,3]
    i_lTable = "ZRUOLI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2], .t., this.ZRUOLI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DECODROL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DECODROL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRGROUP,GRROLE";
                   +" from "+i_cTable+" "+i_lTable+" where GRROLE="+cp_ToStrODBC(this.w_DECODROL);
                   +" and GRGROUP="+cp_ToStrODBC(this.w_DECODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRGROUP',this.w_DECODGRU;
                       ,'GRROLE',this.w_DECODROL)
            select GRGROUP,GRROLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DECODROL = NVL(_Link_.GRROLE,0)
    else
      if i_cCtrl<>'Load'
        this.w_DECODROL = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZRUOLI_IDX,2])+'\'+cp_ToStr(_Link_.GRGROUP,1)+'\'+cp_ToStr(_Link_.GRROLE,1)
      cp_ShowWarn(i_cKey,this.ZRUOLI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DECODROL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DEFOLDER
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZFOLDERS_IDX,3]
    i_lTable = "ZFOLDERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2], .t., this.ZFOLDERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DEFOLDER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZFOLDERS')
        if i_nConn<>0
          i_cWhere = " FOSERIAL="+cp_ToStrODBC(this.w_DEFOLDER);

          i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOSERIAL',this.w_DEFOLDER)
          select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DEFOLDER) and !this.bDontReportError
            deferred_cp_zoom('ZFOLDERS','*','FOSERIAL',cp_AbsName(oSource.parent,'oDEFOLDER_1_3'),i_cWhere,'',"Elenco folder",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOSERIAL',oSource.xKey(1))
            select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DEFOLDER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where FOSERIAL="+cp_ToStrODBC(this.w_DEFOLDER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOSERIAL',this.w_DEFOLDER)
            select FOSERIAL,FODESCRI,FO__PATH,FO__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DEFOLDER = NVL(_Link_.FOSERIAL,0)
      this.w_DESFOLD = NVL(_Link_.FODESCRI,space(50))
      this.w_PATHFOLD = NVL(_Link_.FO__PATH,space(255))
      this.w_FO__TIPO = NVL(_Link_.FO__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DEFOLDER = 0
      endif
      this.w_DESFOLD = space(50)
      this.w_PATHFOLD = space(255)
      this.w_FO__TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DERISERV='N' and .w_FO__TIPO<>'C') or .w_DERISERV='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipologia folder non consentita per il permesso base selezionato")
        endif
        this.w_DEFOLDER = 0
        this.w_DESFOLD = space(50)
        this.w_PATHFOLD = space(255)
        this.w_FO__TIPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])+'\'+cp_ToStr(_Link_.FOSERIAL,1)
      cp_ShowWarn(i_cKey,this.ZFOLDERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DEFOLDER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZFOLDERS_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZFOLDERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.FOSERIAL as FOSERIAL103"+ ",link_1_3.FODESCRI as FODESCRI103"+ ",link_1_3.FO__PATH as FO__PATH103"+ ",link_1_3.FO__TIPO as FO__TIPO103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on INF_PZCP.DEFOLDER=link_1_3.FOSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and INF_PZCP.DEFOLDER=link_1_3.FOSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDERISERV_1_2.RadioValue()==this.w_DERISERV)
      this.oPgFrm.Page1.oPag.oDERISERV_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFOLDER_1_3.value==this.w_DEFOLDER)
      this.oPgFrm.Page1.oPag.oDEFOLDER_1_3.value=this.w_DEFOLDER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOLD_1_4.value==this.w_DESFOLD)
      this.oPgFrm.Page1.oPag.oDESFOLD_1_4.value=this.w_DESFOLD
    endif
    if not(this.oPgFrm.Page1.oPag.oPATHFOLD_1_5.value==this.w_PATHFOLD)
      this.oPgFrm.Page1.oPag.oPATHFOLD_1_5.value=this.w_PATHFOLD
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDESFIL_1_11.value==this.w_DEDESFIL)
      this.oPgFrm.Page1.oPag.oDEDESFIL_1_11.value=this.w_DEDESFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDE__NOTE_1_14.value==this.w_DE__NOTE)
      this.oPgFrm.Page1.oPag.oDE__NOTE_1_14.value=this.w_DE__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_2.RadioValue()==this.w_DETIPDES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_2.SetRadio()
      replace t_DETIPDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODGRU_2_4.value==this.w_DECODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODGRU_2_4.value=this.w_DECODGRU
      replace t_DECODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODGRU_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODCON_2_6.value==this.w_DECODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODCON_2_6.value=this.w_DECODCON
      replace t_DECODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODCON_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODAGE_2_7.value==this.w_DECODAGE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODAGE_2_7.value=this.w_DECODAGE
      replace t_DECODAGE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODAGE_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_8.RadioValue()==this.w_DERIFCFO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_8.SetRadio()
      replace t_DERIFCFO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_11.RadioValue()==this.w_DE__READ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_11.SetRadio()
      replace t_DE__READ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_12.RadioValue()==this.w_DE_WRITE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_12.SetRadio()
      replace t_DE_WRITE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_13.RadioValue()==this.w_DEDELETE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_13.SetRadio()
      replace t_DEDELETE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIGA_2_19.value==this.w_DESRIGA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIGA_2_19.value=this.w_DESRIGA
      replace t_DESRIGA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIGA_2_19.value
    endif
    cp_SetControlsValueExtFlds(this,'INF_PZCP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_DERISERV='N' and .w_FO__TIPO<>'C') or .w_DERISERV='S')  and not(empty(.w_DEFOLDER))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDEFOLDER_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipologia folder non consentita per il permesso base selezionato")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSIR_MPQ.CheckForm()
      if i_bres
        i_bres=  .GSIR_MPQ.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsir_mdv
      if g_IZCP$'SA'and empty(this.w_DEFOLDER) and this.oParentoBject.w_AQFLGZCP='S'
        i_bres=.f.
        ah_errormsg("Errore: Corporate Portal: occorre indicare un codice CPZ Folder","!")
      endif
      
      if g_IZCP$'SA'and empty(this.W_DEDESFIL) and this.oParentoBject.w_AQFLGZCP='S'
        i_bres=.f.
       ah_errormsg("Errore: Corporate Portal: occorre specificare un valore nel campo <descrizione file>","!")
      endif
      
      if g_IZCP$'SA'
        DO GSIR_BDV WITH THIS,'M'
        IF bTrsErr=.t.
        ah_errormsg(i_TrsMsg)
        bTrsErr=.F.
        i_bres=.f.
        ELSE
        DO GSIR_BDV WITH THIS,'S'
        if ! this.w_PERMESSO
           ah_errormsg("Errore: Corporate Portal: occorre specificare un codice cliente/fornitore/agente o gruppo","!")
           i_bres=.f.
        endif
        ENDIF
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and (.w_DETIPDES$'CF') and not(empty(.w_DECODCON)) and (not empty(.w_CPROWORD) or .w_RISPUB = 'N')
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODCON_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice cliente/fornitore non valido oppure obsoleto")
        case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and (.w_DETIPDES='A') and not(empty(.w_DECODAGE)) and (not empty(.w_CPROWORD) or .w_RISPUB = 'N')
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDECODAGE_2_7
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice agente non valido oppure obsoleto")
      endcase
      if not empty(.w_CPROWORD) or .w_RISPUB = 'N'
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DERISERV = this.w_DERISERV
    this.o_DETIPDES = this.w_DETIPDES
    this.o_DECODGRU = this.w_DECODGRU
    this.o_DECODCON = this.w_DECODCON
    this.o_DECODAGE = this.w_DECODAGE
    this.o_DERIFCFO = this.w_DERIFCFO
    this.o_DE__READ = this.w_DE__READ
    this.o_DE_WRITE = this.w_DE_WRITE
    this.o_DEDELETE = this.w_DEDELETE
    this.o_DESRIGA = this.w_DESRIGA
    this.o_ATTCORP = this.w_ATTCORP
    * --- GSIR_MPQ : Depends On
    this.GSIR_MPQ.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not empty(t_CPROWORD) or t_RISPUB = 'N')
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_DETIPDES=space(1)
      .w_OTIPGRU=space(1)
      .w_DECODGRU=0
      .w_DETIPCON=space(1)
      .w_DECODCON=space(15)
      .w_DECODAGE=space(5)
      .w_DERIFCFO=space(1)
      .w_DEVALDES=space(25)
      .w_DECODROL=0
      .w_DE__READ=space(1)
      .w_DE_WRITE=space(1)
      .w_DEDELETE=space(1)
      .w_DESGRUPPO=space(50)
      .w_DESCON=space(40)
      .w_DESAGE=space(35)
      .w_DESRIGA=space(40)
      .w_TIPORIGA=space(1)
      .w_RISPUB=space(1)
      .DoRTCalc(1,3,.f.)
        .w_DETIPDES = 'C'
        .w_OTIPGRU = IIF(.w_DETIPDES='G','O','T')
        .w_DECODGRU = IIF(.w_DETIPDES='C',20, IIF(.w_DETIPDES='A',30, IIF(.w_DETIPDES='F',40, .w_DECODGRU)))
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_DECODGRU))
        .link_2_4('Full')
      endif
        .w_DETIPCON = IIF(.w_DETIPDES$'CF',.w_DETIPDES, ' ')
        .w_DECODCON = IIF(.w_DETIPDES$'CF',.w_DECODCON,' ')
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_DECODCON))
        .link_2_6('Full')
      endif
        .w_DECODAGE = IIF(.w_DETIPDES='A',.w_DECODAGE,' ')
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_DECODAGE))
        .link_2_7('Full')
      endif
        .w_DERIFCFO = 'N'
        .w_DEVALDES = IIF(.w_DETIPDES$'CF', .w_DECODCON, IIF(.w_DETIPDES='A', .w_DECODAGE, IIF(.w_DETIPDES='G', str(.w_DECODGRU,6,0), ' ')))
      .DoRTCalc(12,12,.f.)
      if not(empty(.w_DECODROL))
        .link_2_10('Full')
      endif
        .w_DE__READ = "N"
        .w_DE_WRITE = 'N'
        .w_DEDELETE = 'N'
      .DoRTCalc(16,20,.f.)
        .w_DESRIGA = IIF(.w_DETIPDES$'CF', .w_DESCON, IIF(.w_DETIPDES='A', .w_DESAGE, .w_DESGRUPPO))
      .DoRTCalc(22,28,.f.)
        .w_TIPORIGA = .w_DETIPDES
      .DoRTCalc(30,30,.f.)
        .w_RISPUB = .w_DERISERV
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DETIPDES = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_2.RadioValue(.t.)
    this.w_OTIPGRU = t_OTIPGRU
    this.w_DECODGRU = t_DECODGRU
    this.w_DETIPCON = t_DETIPCON
    this.w_DECODCON = t_DECODCON
    this.w_DECODAGE = t_DECODAGE
    this.w_DERIFCFO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_8.RadioValue(.t.)
    this.w_DEVALDES = t_DEVALDES
    this.w_DECODROL = t_DECODROL
    this.w_DE__READ = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_11.RadioValue(.t.)
    this.w_DE_WRITE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_12.RadioValue(.t.)
    this.w_DEDELETE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_13.RadioValue(.t.)
    this.w_DESGRUPPO = t_DESGRUPPO
    this.w_DESCON = t_DESCON
    this.w_DESAGE = t_DESAGE
    this.w_DESRIGA = t_DESRIGA
    this.w_TIPORIGA = t_TIPORIGA
    this.w_RISPUB = t_RISPUB
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DETIPDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDETIPDES_2_2.ToRadio()
    replace t_OTIPGRU with this.w_OTIPGRU
    replace t_DECODGRU with this.w_DECODGRU
    replace t_DETIPCON with this.w_DETIPCON
    replace t_DECODCON with this.w_DECODCON
    replace t_DECODAGE with this.w_DECODAGE
    replace t_DERIFCFO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDERIFCFO_2_8.ToRadio()
    replace t_DEVALDES with this.w_DEVALDES
    replace t_DECODROL with this.w_DECODROL
    replace t_DE__READ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE__READ_2_11.ToRadio()
    replace t_DE_WRITE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDE_WRITE_2_12.ToRadio()
    replace t_DEDELETE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDEDELETE_2_13.ToRadio()
    replace t_DESGRUPPO with this.w_DESGRUPPO
    replace t_DESCON with this.w_DESCON
    replace t_DESAGE with this.w_DESAGE
    replace t_DESRIGA with this.w_DESRIGA
    replace t_TIPORIGA with this.w_TIPORIGA
    replace t_RISPUB with this.w_RISPUB
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsir_mdvPag1 as StdContainer
  Width  = 803
  height = 358
  stdWidth  = 803
  stdheight = 358
  resizeXpos=491
  resizeYpos=265
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDERISERV_1_2 as StdRadio with uid="RYBOWKVNMY",rtseq=2,rtrep=.f.,left=129, top=5, width=94,height=39;
    , ToolTipText = "Flag riservato/pubblico";
    , cFormVar="w_DERISERV", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDERISERV_1_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Riservato"
      this.Buttons(1).HelpContextID = 95746700
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Pubblico"
      this.Buttons(2).HelpContextID = 95746700
      this.Buttons(2).Top=18
      this.SetAll("Width",92)
      this.SetAll("Height",20)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Flag riservato/pubblico")
      StdRadio::init()
    endproc

  func oDERISERV_1_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DERISERV,&i_cF..t_DERISERV),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oDERISERV_1_2.GetRadio()
    this.Parent.oContained.w_DERISERV = this.RadioValue()
    return .t.
  endfunc

  func oDERISERV_1_2.ToRadio()
    this.Parent.oContained.w_DERISERV=trim(this.Parent.oContained.w_DERISERV)
    return(;
      iif(this.Parent.oContained.w_DERISERV=='S',1,;
      iif(this.Parent.oContained.w_DERISERV=='N',2,;
      0)))
  endfunc

  func oDERISERV_1_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDEFOLDER_1_3 as StdField with uid="BTMXUBVQEY",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DEFOLDER", cQueryName = "DEFOLDER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Tipologia folder non consentita per il permesso base selezionato",;
    HelpContextID = 71973512,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=307, Top=5, cSayPict='"@Z 9999999999"', bHasZoom = .t. , cLinkFile="ZFOLDERS", oKey_1_1="FOSERIAL", oKey_1_2="this.w_DEFOLDER"

  func oDEFOLDER_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDEFOLDER_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDEFOLDER_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZFOLDERS','*','FOSERIAL',cp_AbsName(this.parent,'oDEFOLDER_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco folder",'',this.parent.oContained
  endproc

  add object oDESFOLD_1_4 as StdField with uid="IRSQHACCCX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESFOLD", cQueryName = "DESFOLD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208800310,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=404, Top=5, InputMask=replicate('X',50)

  add object oPATHFOLD_1_5 as StdField with uid="CJBIDMRUBP",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PATHFOLD", cQueryName = "PATHFOLD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(255), bMultilanguage =  .f.,;
    ToolTipText = "Path folder su DMS",;
    HelpContextID = 18606278,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=425, Left=307, Top=29, InputMask=replicate('X',255)


  add object oLinkPC_1_6 as StdButton with uid="NBSUPBIJDD",left=737, top=5, width=48,height=45,;
    CpPicture="BMP\PARAMETRI.BMP", caption="", nPag=1;
    , ToolTipText = "Accede alla maschera di definizione parametri";
    , HelpContextID = 198295126;
    , caption='\<Parametri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_6.Click()
      this.Parent.oContained.GSIR_MPQ.LinkPCClick()
    endproc

  func oLinkPC_1_6.mCond()
    with this.Parent.oContained
      return (.w_DEFOLDER<>0 AND '%' $ .w_PATHFOLD)
    endwith
  endfunc

  add object oDEDESFIL_1_11 as StdField with uid="THOMOSKUML",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DEDESFIL", cQueryName = "DEDESFIL",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione file per DMS Corporate Portal",;
    HelpContextID = 156231038,;
   bGlobalFont=.t.,;
    Height=21, Width=602, Left=129, Top=53, InputMask=replicate('X',100)

  add object oDE__NOTE_1_14 as StdMemo with uid="DFGUVRBVUR",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DE__NOTE", cQueryName = "DE__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 259771003,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=41, Width=602, Left=129, Top=77


  add object oObj_1_16 as cp_runprogram with uid="WSRRUWECSI",left=398, top=407, width=319,height=21,;
    caption='GSIR_BRP(P)',;
   bGlobalFont=.t.,;
    prg="GSIR_BRP('P')",;
    cEvent = "w_DERISERV Changed",;
    nPag=1;
    , HelpContextID = 58743606


  add object oObj_1_17 as cp_runprogram with uid="NAPUDWHAAT",left=398, top=384, width=319,height=21,;
    caption='GSIR_BDV(M)',;
   bGlobalFont=.t.,;
    prg="GSIR_BDV('M')",;
    cEvent = "Insert start,Update start",;
    nPag=1;
    , HelpContextID = 58742844


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=125, width=788,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=10,Field1="CPROWORD",Label1="Riga",Field2="DETIPDES",Label2="Tipo",Field3="DECODGRU",Label3="Gruppo",Field4="DECODCON",Label4="Cliente/Fornitore",Field5="DECODAGE",Label5="Agente",Field6="DESRIGA",Label6="Descrizione",Field7="DERIFCFO",Label7="PR",Field8="DE__READ",Label8="R",Field9="DE_WRITE",Label9="W",Field10="DEDELETE",Label10="D",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235780730

  add object oStr_1_7 as StdString with uid="ZMUPBXOVHC",Visible=.t., Left=7, Top=5,;
    Alignment=1, Width=117, Height=18,;
    Caption="Permessi base:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ATNZURRIRU",Visible=.t., Left=224, Top=5,;
    Alignment=1, Width=80, Height=18,;
    Caption="CPZ folder:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="KAVJGJDPOE",Visible=.t., Left=7, Top=53,;
    Alignment=1, Width=117, Height=18,;
    Caption="Descrizione file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="BXYQEPPXWL",Visible=.t., Left=50, Top=77,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=143,;
    width=788+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=144,width=787+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ZGRUPPI|CONTI|AGENTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ZGRUPPI'
        oDropInto=this.oBodyCol.oRow.oDECODGRU_2_4
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oDECODCON_2_6
      case cFile='AGENTI'
        oDropInto=this.oBodyCol.oRow.oDECODAGE_2_7
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsir_mdvBodyRow as CPBodyRowCnt
  Width=778
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="KUICTKTBBH",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 268109162,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oDETIPDES_2_2 as StdTrsCombo with uid="HXGOLSXAGQ",rtrep=.t.,;
    cFormVar="w_DETIPDES", RowSource=""+"Cliente,"+"Fornitore,"+"Agente,"+"Gruppo" , ;
    HelpContextID = 75831945,;
    Height=22, Width=75, Left=42, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDETIPDES_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DETIPDES,&i_cF..t_DETIPDES),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'F',;
    iif(xVal =3,'A',;
    iif(xVal =4,'G',;
    space(1))))))
  endfunc
  func oDETIPDES_2_2.GetRadio()
    this.Parent.oContained.w_DETIPDES = this.RadioValue()
    return .t.
  endfunc

  func oDETIPDES_2_2.ToRadio()
    this.Parent.oContained.w_DETIPDES=trim(this.Parent.oContained.w_DETIPDES)
    return(;
      iif(this.Parent.oContained.w_DETIPDES=='C',1,;
      iif(this.Parent.oContained.w_DETIPDES=='F',2,;
      iif(this.Parent.oContained.w_DETIPDES=='A',3,;
      iif(this.Parent.oContained.w_DETIPDES=='G',4,;
      0)))))
  endfunc

  func oDETIPDES_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDECODGRU_2_4 as StdTrsField with uid="WBLZRBUJID",rtseq=6,rtrep=.t.,;
    cFormVar="w_DECODGRU",value=0,;
    HelpContextID = 113904267,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=119, Top=0, cSayPict=["@Z 999999"], cGetPict=["@Z 999999"], bHasZoom = .t. , cLinkFile="ZGRUPPI", oKey_1_1="code", oKey_1_2="this.w_DECODGRU"

  func oDECODGRU_2_4.mCond()
    with this.Parent.oContained
      return (Not(.w_DETIPDES$'ACF'))
    endwith
  endfunc

  func oDECODGRU_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
      if .not. empty(.w_DECODROL)
        bRes2=.link_2_10('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDECODGRU_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDECODGRU_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZGRUPPI','*','code',cp_AbsName(this.parent,'oDECODGRU_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi organizzativi",'',this.parent.oContained
  endproc

  add object oDECODCON_2_6 as StdTrsField with uid="RHRACZGAQC",rtseq=8,rtrep=.t.,;
    cFormVar="w_DECODCON",value=space(15),;
    HelpContextID = 46795396,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente/fornitore non valido oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=148, Left=192, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DETIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_DECODCON"

  func oDECODCON_2_6.mCond()
    with this.Parent.oContained
      return (.w_DETIPDES$'CF')
    endwith
  endfunc

  func oDECODCON_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oDECODCON_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDECODCON_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DETIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DETIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDECODCON_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDECODAGE_2_7 as StdTrsField with uid="SWTDSHHBPP",rtseq=9,rtrep=.t.,;
    cFormVar="w_DECODAGE",value=space(5),;
    HelpContextID = 255194501,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente non valido oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=343, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_DECODAGE"

  func oDECODAGE_2_7.mCond()
    with this.Parent.oContained
      return (.w_DETIPDES='A')
    endwith
  endfunc

  func oDECODAGE_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oDECODAGE_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDECODAGE_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oDECODAGE_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oDERIFCFO_2_8 as StdTrsCheck with uid="MLOYHGWVOW",rtrep=.t.,;
    cFormVar="w_DERIFCFO",  caption="",;
    ToolTipText = "Check riferimento company folder",;
    HelpContextID = 48560773,;
    Left=685, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , TABSTOP=.F.,AutoSize=.F.;
   , bGlobalFont=.t.


  func oDERIFCFO_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DERIFCFO,&i_cF..t_DERIFCFO),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDERIFCFO_2_8.GetRadio()
    this.Parent.oContained.w_DERIFCFO = this.RadioValue()
    return .t.
  endfunc

  func oDERIFCFO_2_8.ToRadio()
    this.Parent.oContained.w_DERIFCFO=trim(this.Parent.oContained.w_DERIFCFO)
    return(;
      iif(this.Parent.oContained.w_DERIFCFO=='S',1,;
      0))
  endfunc

  func oDERIFCFO_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDERIFCFO_2_8.mCond()
    with this.Parent.oContained
      return (.w_FO__TIPO='C' and .w_DETIPDES<>'G')
    endwith
  endfunc

  add object oDE__READ_2_11 as StdTrsCheck with uid="SNXSRXPZEP",rtrep=.t.,;
    cFormVar="w_DE__READ",  caption="",;
    HelpContextID = 96193146,;
    Left=708, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDE__READ_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DE__READ,&i_cF..t_DE__READ),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDE__READ_2_11.GetRadio()
    this.Parent.oContained.w_DE__READ = this.RadioValue()
    return .t.
  endfunc

  func oDE__READ_2_11.ToRadio()
    this.Parent.oContained.w_DE__READ=trim(this.Parent.oContained.w_DE__READ)
    return(;
      iif(this.Parent.oContained.w_DE__READ=='S',1,;
      0))
  endfunc

  func oDE__READ_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDE_WRITE_2_12 as StdTrsCheck with uid="KRVMHUEBHD",rtrep=.t.,;
    cFormVar="w_DE_WRITE",  caption="",;
    HelpContextID = 162777723,;
    Left=731, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDE_WRITE_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DE_WRITE,&i_cF..t_DE_WRITE),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDE_WRITE_2_12.GetRadio()
    this.Parent.oContained.w_DE_WRITE = this.RadioValue()
    return .t.
  endfunc

  func oDE_WRITE_2_12.ToRadio()
    this.Parent.oContained.w_DE_WRITE=trim(this.Parent.oContained.w_DE_WRITE)
    return(;
      iif(this.Parent.oContained.w_DE_WRITE=='S',1,;
      0))
  endfunc

  func oDE_WRITE_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDEDELETE_2_13 as StdTrsCheck with uid="VUQLWTWUMH",rtrep=.t.,;
    cFormVar="w_DEDELETE",  caption="",;
    HelpContextID = 88087163,;
    Left=754, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDEDELETE_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DEDELETE,&i_cF..t_DEDELETE),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDEDELETE_2_13.GetRadio()
    this.Parent.oContained.w_DEDELETE = this.RadioValue()
    return .t.
  endfunc

  func oDEDELETE_2_13.ToRadio()
    this.Parent.oContained.w_DEDELETE=trim(this.Parent.oContained.w_DEDELETE)
    return(;
      iif(this.Parent.oContained.w_DEDELETE=='S',1,;
      0))
  endfunc

  func oDEDELETE_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDESRIGA_2_19 as StdTrsField with uid="ESCDMHYQUG",rtseq=21,rtrep=.t.,;
    cFormVar="w_DESRIGA",value=space(40),enabled=.f.,;
    HelpContextID = 119409206,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=274, Left=408, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_mdv','INF_PZCP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DECODICE=INF_PZCP.DECODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
