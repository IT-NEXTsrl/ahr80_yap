* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_mfr                                                        *
*              Filtri report InfoPublisher                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-20                                                      *
* Last revis.: 2011-03-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsir_mfr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsir_mfr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsir_mfr")
  return

* --- Class definition
define class tgsir_mfr as StdPCForm
  Width  = 715
  Height = 448
  Top    = 3
  Left   = 3
  cComment = "Filtri report InfoPublisher"
  cPrg = "gsir_mfr"
  HelpContextID=46755689
  add object cnt as tcgsir_mfr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsir_mfr as PCContext
  w_FICODQUE = space(20)
  w_CPROWORD = 0
  w_FIFLNAME = space(30)
  w_FIPRGRN = 0
  w_FITIPFIL = space(8)
  w_FIFILTRO = space(254)
  w_FIVISMSK = space(254)
  w_CODQUE = space(20)
  w_FIFLGAZI = space(1)
  w_FIFLGREM = space(1)
  proc Save(i_oFrom)
    this.w_FICODQUE = i_oFrom.w_FICODQUE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_FIFLNAME = i_oFrom.w_FIFLNAME
    this.w_FIPRGRN = i_oFrom.w_FIPRGRN
    this.w_FITIPFIL = i_oFrom.w_FITIPFIL
    this.w_FIFILTRO = i_oFrom.w_FIFILTRO
    this.w_FIVISMSK = i_oFrom.w_FIVISMSK
    this.w_CODQUE = i_oFrom.w_CODQUE
    this.w_FIFLGAZI = i_oFrom.w_FIFLGAZI
    this.w_FIFLGREM = i_oFrom.w_FIFLGREM
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_FICODQUE = this.w_FICODQUE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_FIFLNAME = this.w_FIFLNAME
    i_oTo.w_FIPRGRN = this.w_FIPRGRN
    i_oTo.w_FITIPFIL = this.w_FITIPFIL
    i_oTo.w_FIFILTRO = this.w_FIFILTRO
    i_oTo.w_FIVISMSK = this.w_FIVISMSK
    i_oTo.w_CODQUE = this.w_CODQUE
    i_oTo.w_FIFLGAZI = this.w_FIFLGAZI
    i_oTo.w_FIFLGREM = this.w_FIFLGREM
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsir_mfr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 715
  Height = 448
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-07"
  HelpContextID=46755689
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  INF_AFIM_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "INF_AFIM"
  cKeySelect = "FICODQUE,FIPRGRN"
  cKeyWhere  = "FICODQUE=this.w_FICODQUE and FIPRGRN=this.w_FIPRGRN"
  cKeyDetail  = "FICODQUE=this.w_FICODQUE and FIPRGRN=this.w_FIPRGRN"
  cKeyWhereODBC = '"FICODQUE="+cp_ToStrODBC(this.w_FICODQUE)';
      +'+" and FIPRGRN="+cp_ToStrODBC(this.w_FIPRGRN)';

  cKeyDetailWhereODBC = '"FICODQUE="+cp_ToStrODBC(this.w_FICODQUE)';
      +'+" and FIPRGRN="+cp_ToStrODBC(this.w_FIPRGRN)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"INF_AFIM.FICODQUE="+cp_ToStrODBC(this.w_FICODQUE)';
      +'+" and INF_AFIM.FIPRGRN="+cp_ToStrODBC(this.w_FIPRGRN)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'INF_AFIM.CPROWORD '
  cPrg = "gsir_mfr"
  cComment = "Filtri report InfoPublisher"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FICODQUE = space(20)
  w_CPROWORD = 0
  w_FIFLNAME = space(30)
  w_FIPRGRN = 0
  w_FITIPFIL = space(8)
  w_FIFILTRO = space(254)
  w_FIVISMSK = space(254)
  w_CODQUE = space(20)
  w_FIFLGAZI = space(1)
  w_FIFLGREM = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_mfrPag1","gsir_mfr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='XDC_FIELDS'
    this.cWorkTables[2]='INF_AFIM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INF_AFIM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INF_AFIM_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsir_mfr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from INF_AFIM where FICODQUE=KeySet.FICODQUE
    *                            and FIPRGRN=KeySet.FIPRGRN
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.INF_AFIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AFIM_IDX,2],this.bLoadRecFilter,this.INF_AFIM_IDX,"gsir_mfr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INF_AFIM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INF_AFIM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INF_AFIM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FICODQUE',this.w_FICODQUE  ,'FIPRGRN',this.w_FIPRGRN  )
      select * from (i_cTable) INF_AFIM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODQUE = .oParentObject .w_AQCODICE
        .w_FICODQUE = NVL(FICODQUE,space(20))
        .w_FIPRGRN = NVL(FIPRGRN,0)
        .w_FIVISMSK = NVL(FIVISMSK,space(254))
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'INF_AFIM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_FIFLNAME = NVL(FIFLNAME,space(30))
          .w_FITIPFIL = NVL(FITIPFIL,space(8))
          .w_FIFILTRO = NVL(FIFILTRO,space(254))
          .w_FIFLGAZI = NVL(FIFLGAZI,space(1))
          .w_FIFLGREM = NVL(FIFLGREM,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_FICODQUE=space(20)
      .w_CPROWORD=10
      .w_FIFLNAME=space(30)
      .w_FIPRGRN=0
      .w_FITIPFIL=space(8)
      .w_FIFILTRO=space(254)
      .w_FIVISMSK=space(254)
      .w_CODQUE=space(20)
      .w_FIFLGAZI=space(1)
      .w_FIFLGREM=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_FITIPFIL = '='
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .DoRTCalc(6,7,.f.)
        .w_CODQUE = .oParentObject .w_AQCODICE
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INF_AFIM')
    this.DoRTCalc(9,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oFIVISMSK_3_1.enabled = i_bVal
      .Page1.oPag.oObj_3_3.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'INF_AFIM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INF_AFIM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FICODQUE,"FICODQUE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIPRGRN,"FIPRGRN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FIVISMSK,"FIVISMSK",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_FIFLNAME C(30);
      ,t_FITIPFIL N(3);
      ,t_FIFILTRO C(254);
      ,t_FIFLGAZI N(3);
      ,t_FIFLGREM N(3);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsir_mfrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLNAME_2_2.controlsource=this.cTrsName+'.t_FIFLNAME'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFITIPFIL_2_3.controlsource=this.cTrsName+'.t_FITIPFIL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFIFILTRO_2_4.controlsource=this.cTrsName+'.t_FIFILTRO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGAZI_2_5.controlsource=this.cTrsName+'.t_FIFLGAZI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGREM_2_6.controlsource=this.cTrsName+'.t_FIFLGREM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(65)
    this.AddVLine(222)
    this.AddVLine(295)
    this.AddVLine(624)
    this.AddVLine(658)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INF_AFIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AFIM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_AFIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AFIM_IDX,2])
      *
      * insert into INF_AFIM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INF_AFIM')
        i_extval=cp_InsertValODBCExtFlds(this,'INF_AFIM')
        i_cFldBody=" "+;
                  "(FICODQUE,CPROWORD,FIFLNAME,FIPRGRN,FITIPFIL"+;
                  ",FIFILTRO,FIVISMSK,FIFLGAZI,FIFLGREM,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_FICODQUE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_FIFLNAME)+","+cp_ToStrODBC(this.w_FIPRGRN)+","+cp_ToStrODBC(this.w_FITIPFIL)+;
             ","+cp_ToStrODBC(this.w_FIFILTRO)+","+cp_ToStrODBC(this.w_FIVISMSK)+","+cp_ToStrODBC(this.w_FIFLGAZI)+","+cp_ToStrODBC(this.w_FIFLGREM)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INF_AFIM')
        i_extval=cp_InsertValVFPExtFlds(this,'INF_AFIM')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'FICODQUE',this.w_FICODQUE,'FIPRGRN',this.w_FIPRGRN)
        INSERT INTO (i_cTable) (;
                   FICODQUE;
                  ,CPROWORD;
                  ,FIFLNAME;
                  ,FIPRGRN;
                  ,FITIPFIL;
                  ,FIFILTRO;
                  ,FIVISMSK;
                  ,FIFLGAZI;
                  ,FIFLGREM;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_FICODQUE;
                  ,this.w_CPROWORD;
                  ,this.w_FIFLNAME;
                  ,this.w_FIPRGRN;
                  ,this.w_FITIPFIL;
                  ,this.w_FIFILTRO;
                  ,this.w_FIVISMSK;
                  ,this.w_FIFLGAZI;
                  ,this.w_FIFLGREM;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.INF_AFIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AFIM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_FIFLNAME)) and not(Empty(t_FIFILTRO)) ) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'INF_AFIM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " FIVISMSK="+cp_ToStrODBC(this.w_FIVISMSK)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'INF_AFIM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  FIVISMSK=this.w_FIVISMSK;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_FIFLNAME)) and not(Empty(t_FIFILTRO)) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INF_AFIM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'INF_AFIM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",FIFLNAME="+cp_ToStrODBC(this.w_FIFLNAME)+;
                     ",FITIPFIL="+cp_ToStrODBC(this.w_FITIPFIL)+;
                     ",FIFILTRO="+cp_ToStrODBC(this.w_FIFILTRO)+;
                     ",FIVISMSK="+cp_ToStrODBC(this.w_FIVISMSK)+;
                     ",FIFLGAZI="+cp_ToStrODBC(this.w_FIFLGAZI)+;
                     ",FIFLGREM="+cp_ToStrODBC(this.w_FIFLGREM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'INF_AFIM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,FIFLNAME=this.w_FIFLNAME;
                     ,FITIPFIL=this.w_FITIPFIL;
                     ,FIFILTRO=this.w_FIFILTRO;
                     ,FIVISMSK=this.w_FIVISMSK;
                     ,FIFLGAZI=this.w_FIFLGAZI;
                     ,FIFLGREM=this.w_FIFLGREM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INF_AFIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AFIM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_FIFLNAME)) and not(Empty(t_FIFILTRO)) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete INF_AFIM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_FIFLNAME)) and not(Empty(t_FIFILTRO)) ) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INF_AFIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AFIM_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_3_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oFIVISMSK_3_1.value==this.w_FIVISMSK)
      this.oPgFrm.Page1.oPag.oFIVISMSK_3_1.value=this.w_FIVISMSK
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLNAME_2_2.value==this.w_FIFLNAME)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLNAME_2_2.value=this.w_FIFLNAME
      replace t_FIFLNAME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLNAME_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFITIPFIL_2_3.RadioValue()==this.w_FITIPFIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFITIPFIL_2_3.SetRadio()
      replace t_FITIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFITIPFIL_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFILTRO_2_4.value==this.w_FIFILTRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFILTRO_2_4.value=this.w_FIFILTRO
      replace t_FIFILTRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFILTRO_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGAZI_2_5.RadioValue()==this.w_FIFLGAZI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGAZI_2_5.SetRadio()
      replace t_FIFLGAZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGAZI_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGREM_2_6.RadioValue()==this.w_FIFLGREM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGREM_2_6.SetRadio()
      replace t_FIFLGREM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGREM_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'INF_AFIM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_FIFLNAME)) and not(Empty(.w_FIFILTRO)) 
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_FIFLNAME)) and not(Empty(t_FIFILTRO)) )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_FIFLNAME=space(30)
      .w_FITIPFIL=space(8)
      .w_FIFILTRO=space(254)
      .w_FIFLGAZI=space(1)
      .w_FIFLGREM=space(1)
      .DoRTCalc(1,4,.f.)
        .w_FITIPFIL = '='
    endwith
    this.DoRTCalc(6,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_FIFLNAME = t_FIFLNAME
    this.w_FITIPFIL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFITIPFIL_2_3.RadioValue(.t.)
    this.w_FIFILTRO = t_FIFILTRO
    this.w_FIFLGAZI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGAZI_2_5.RadioValue(.t.)
    this.w_FIFLGREM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGREM_2_6.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_FIFLNAME with this.w_FIFLNAME
    replace t_FITIPFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFITIPFIL_2_3.ToRadio()
    replace t_FIFILTRO with this.w_FIFILTRO
    replace t_FIFLGAZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGAZI_2_5.ToRadio()
    replace t_FIFLGREM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIFLGREM_2_6.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsir_mfrPag1 as StdContainer
  Width  = 711
  height = 448
  stdWidth  = 711
  stdheight = 448
  resizeXpos=430
  resizeYpos=372
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=15, top=9, width=688,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Seq.",Field2="FIFLNAME",Label2="Dimensione",Field3="FITIPFIL",Label3="Tipo filtro",Field4="FIFILTRO",Label4="Filtro",Field5="FIFLGAZI",Label5="Azi.",Field6="FIFLGREM",Label6="Rem.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 899706

  add object oStr_1_3 as StdString with uid="DGQFUHPHCK",Visible=.t., Left=109, Top=484,;
    Alignment=1, Width=84, Height=19,;
    Caption="ATTENZIONE:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="NLREYXRPQG",Visible=.t., Left=195, Top=485,;
    Alignment=0, Width=508, Height=18,;
    Caption="Se si modifica la dimensione della finestra occorre modificare la ridefinizione della classe"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="ALGODWRBRT",Visible=.t., Left=195, Top=500,;
    Alignment=0, Width=304, Height=18,;
    Caption="all'interno della manual block function_procedure"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=5,top=28,;
    width=683+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=6,top=29,width=682+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oFIVISMSK_3_1 as StdField with uid="AHUJRXZGSQ",rtseq=7,rtrep=.f.,;
    cFormVar="w_FIVISMSK",value=space(254),;
    ToolTipText = "Percorso della visual mask per la richiesta parametri",;
    HelpContextID = 263536289,;
    cQueryName = "FIVISMSK",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=382, Left=95, Top=419, InputMask=replicate('X',254)

  add object oObj_3_3 as cp_askfile with uid="VNKBZZTRCI",width=21,height=20,;
   left=481, top=420,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_FIVISMSK",cExt="vfm",;
    nPag=3;
    , ToolTipText = "Premere per scegliere una visual mask";
    , HelpContextID = 46554666

  add object oStr_3_2 as StdString with uid="KFJGFOANTC",Visible=.t., Left=6, Top=419,;
    Alignment=1, Width=88, Height=18,;
    Caption="Dialog:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsir_mfrBodyRow as CPBodyRowCnt
  Width=673
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="CRBWCUMOCX",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 33228138,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oFIFLNAME_2_2 as StdTrsField with uid="SWYSGLOGIM",rtseq=3,rtrep=.t.,;
    cFormVar="w_FIFLNAME",value=space(30),;
    ToolTipText = "Filtro",;
    HelpContextID = 211337573,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=50, Top=0, cSayPict=[REPLICATE( '!', 20 )], cGetPict=[REPLICATE( '!', 20 )], InputMask=replicate('X',30)

  add object oFITIPFIL_2_3 as StdTrsCombo with uid="YJJUFWYOFQ",rtrep=.t.,;
    cFormVar="w_FITIPFIL", RowSource=""+"=,"+"Like,"+">,"+"<,"+">=,"+"<=,"+"<>,"+"in,"+"not in" , ;
    ToolTipText = "Tipo filtro",;
    HelpContextID = 142941858,;
    Height=21, Width=69, Left=207, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oFITIPFIL_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FITIPFIL,&i_cF..t_FITIPFIL),this.value)
    return(iif(xVal =1,'=',;
    iif(xVal =2,'Like',;
    iif(xVal =3,'>',;
    iif(xVal =4,'<',;
    iif(xVal =5,'>=',;
    iif(xVal =6,'<=',;
    iif(xVal =7,'<>',;
    iif(xVal =8,'in',;
    iif(xVal =9,'not in',;
    space(8)))))))))))
  endfunc
  func oFITIPFIL_2_3.GetRadio()
    this.Parent.oContained.w_FITIPFIL = this.RadioValue()
    return .t.
  endfunc

  func oFITIPFIL_2_3.ToRadio()
    this.Parent.oContained.w_FITIPFIL=trim(this.Parent.oContained.w_FITIPFIL)
    return(;
      iif(this.Parent.oContained.w_FITIPFIL=='=',1,;
      iif(this.Parent.oContained.w_FITIPFIL=='Like',2,;
      iif(this.Parent.oContained.w_FITIPFIL=='>',3,;
      iif(this.Parent.oContained.w_FITIPFIL=='<',4,;
      iif(this.Parent.oContained.w_FITIPFIL=='>=',5,;
      iif(this.Parent.oContained.w_FITIPFIL=='<=',6,;
      iif(this.Parent.oContained.w_FITIPFIL=='<>',7,;
      iif(this.Parent.oContained.w_FITIPFIL=='in',8,;
      iif(this.Parent.oContained.w_FITIPFIL=='not in',9,;
      0))))))))))
  endfunc

  func oFITIPFIL_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFIFILTRO_2_4 as StdTrsField with uid="HEXEECXGCJ",rtseq=6,rtrep=.t.,;
    cFormVar="w_FIFILTRO",value=space(254),;
    ToolTipText = "Filtro",;
    HelpContextID = 105135781,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=322, Left=280, Top=0, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oFIFILTRO_2_4.mZoom
      with this.Parent.oContained
        do GSIR_BFR with this.Parent.oContained
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFIFLGAZI_2_5 as StdTrsCheck with uid="VCTALRPQBL",rtrep=.t.,;
    cFormVar="w_FIFLGAZI",  caption="",;
    HelpContextID = 49757855,;
    Left=612, Top=0, Width=23,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oFIFLGAZI_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FIFLGAZI,&i_cF..t_FIFLGAZI),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oFIFLGAZI_2_5.GetRadio()
    this.Parent.oContained.w_FIFLGAZI = this.RadioValue()
    return .t.
  endfunc

  func oFIFLGAZI_2_5.ToRadio()
    this.Parent.oContained.w_FIFLGAZI=trim(this.Parent.oContained.w_FIFLGAZI)
    return(;
      iif(this.Parent.oContained.w_FIFLGAZI=='S',1,;
      0))
  endfunc

  func oFIFLGAZI_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFIFLGREM_2_6 as StdTrsCheck with uid="DZUCGOVCHX",rtrep=.t.,;
    cFormVar="w_FIFLGREM",  caption="",;
    ToolTipText = "Se attivo: rimuove il filtro se vuoto",;
    HelpContextID = 66535075,;
    Left=647, Top=0, Width=21,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oFIFLGREM_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FIFLGREM,&i_cF..t_FIFLGREM),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oFIFLGREM_2_6.GetRadio()
    this.Parent.oContained.w_FIFLGREM = this.RadioValue()
    return .t.
  endfunc

  func oFIFLGREM_2_6.ToRadio()
    this.Parent.oContained.w_FIFLGREM=trim(this.Parent.oContained.w_FIFLGREM)
    return(;
      iif(this.Parent.oContained.w_FIFLGREM=='S',1,;
      0))
  endfunc

  func oFIFLGREM_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_mfr','INF_AFIM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FICODQUE=INF_AFIM.FICODQUE";
  +" and "+i_cAliasName2+".FIPRGRN=INF_AFIM.FIPRGRN";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
