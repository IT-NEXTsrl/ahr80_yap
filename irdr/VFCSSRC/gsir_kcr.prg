* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_kcr                                                        *
*              Cifratura stringa di connessione                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-08                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsir_kcr",oParentObject))

* --- Class definition
define class tgsir_kcr as StdForm
  Top    = 9
  Left   = 10

  * --- Standard Properties
  Width  = 706
  Height = 176
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=99184489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsir_kcr"
  cComment = "Cifratura stringa di connessione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ATCRIPLO = space(10)
  o_ATCRIPLO = space(10)
  w_STRCIFRA = space(254)
  o_STRCIFRA = space(254)
  w_STRCONN = space(254)
  o_STRCONN = space(254)
  w_AQCRIPTE = space(1)
  o_AQCRIPTE = space(1)
  w_AQADOCS = space(254)
  o_AQADOCS = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_kcrPag1","gsir_kcr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTRCONN_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ATCRIPLO=space(10)
      .w_STRCIFRA=space(254)
      .w_STRCONN=space(254)
      .w_AQCRIPTE=space(1)
      .w_AQADOCS=space(254)
      .w_AQCRIPTE=oParentObject.w_AQCRIPTE
      .w_AQADOCS=oParentObject.w_AQADOCS
        .w_ATCRIPLO = .w_AQCRIPTE
        .w_STRCIFRA = iif(.w_AQCRIPTE='C',.w_AQADOCS,'')
        .w_STRCONN = iif(.w_ATCRIPLO<>'C',.w_AQADOCS,.w_STRCONN)
        .w_AQCRIPTE = .w_AQCRIPTE
    endwith
    this.DoRTCalc(5,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AQCRIPTE=.w_AQCRIPTE
      .oParentObject.w_AQADOCS=.w_AQADOCS
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_AQCRIPTE<>.w_AQCRIPTE
            .w_ATCRIPLO = .w_AQCRIPTE
        endif
        if .o_STRCONN<>.w_STRCONN.or. .o_STRCIFRA<>.w_STRCIFRA
          .Calculate_TIYHFQUTDV()
        endif
        if .o_STRCONN<>.w_STRCONN.or. .o_STRCIFRA<>.w_STRCIFRA
          .Calculate_DMOREJPOGN()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_TIYHFQUTDV()
    with this
          * --- Variazione aqcripte
          .w_AQCRIPTE = iif(.w_AQCRIPTE='C','N','C')
    endwith
  endproc
  proc Calculate_DMOREJPOGN()
    with this
          * --- Variazione aqadocs
          .w_AQADOCS = iif(.w_AQCRIPTE='C',.w_STRCIFRA,.w_STRCONN)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTRCIFRA_1_3.value==this.w_STRCIFRA)
      this.oPgFrm.Page1.oPag.oSTRCIFRA_1_3.value=this.w_STRCIFRA
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRCONN_1_4.value==this.w_STRCONN)
      this.oPgFrm.Page1.oPag.oSTRCONN_1_4.value=this.w_STRCONN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ATCRIPLO = this.w_ATCRIPLO
    this.o_STRCIFRA = this.w_STRCIFRA
    this.o_STRCONN = this.w_STRCONN
    this.o_AQCRIPTE = this.w_AQCRIPTE
    this.o_AQADOCS = this.w_AQADOCS
    return

enddefine

* --- Define pages as container
define class tgsir_kcrPag1 as StdContainer
  Width  = 702
  height = 176
  stdWidth  = 702
  stdheight = 176
  resizeXpos=466
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTRCIFRA_1_3 as StdField with uid="AGMHWPBTTQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_STRCIFRA", cQueryName = "STRCIFRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di connessione cifrata",;
    HelpContextID = 185660825,;
   bGlobalFont=.t.,;
    Height=21, Width=685, Left=9, Top=99, InputMask=replicate('X',254)

  add object oSTRCONN_1_4 as StdField with uid="WTJHRNISJD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_STRCONN", cQueryName = "STRCONN",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di connessione",;
    HelpContextID = 45151706,;
   bGlobalFont=.t.,;
    Height=21, Width=685, Left=9, Top=25, InputMask=replicate('X',254)


  add object oBtn_1_7 as StdButton with uid="ZMIZZNTUDJ",left=594, top=51, width=48,height=45,;
    CpPicture="BMP\CIFRA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per cifrare la stringa di connessione";
    , HelpContextID = 257961099;
    , Caption='\<Cifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        .w_STRCIFRA=CifraCnf( ALLTRIM(.w_STRCONN) , 'C' )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_AQCRIPTE='N')
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="QVIVCARPHQ",left=645, top=51, width=48,height=45,;
    CpPicture="BMP\DECIFRA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per decifrare la stringa di connessione";
    , HelpContextID = 56328070;
    , Caption='\<Decifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        .w_STRCONN=CifraCnf( ALLTRIM(.w_AQADOCS) , 'D' )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_AQCRIPTE='C')
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="GEQFZRLUOR",left=594, top=125, width=48,height=45,;
    CpPicture="BMP\Ok.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare";
    , HelpContextID = 257706202;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="JWIBHYOYEX",left=645, top=125, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91867066;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="ZPIBSERKIK",Visible=.t., Left=9, Top=6,;
    Alignment=0, Width=235, Height=17,;
    Caption="Stringa di connessione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="LNMDVNDNVN",Visible=.t., Left=9, Top=80,;
    Alignment=0, Width=249, Height=17,;
    Caption="Stringa di connessione (cifrata)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_kcr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
