* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir1bzg                                                        *
*              Zoom gestioni da menu                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-20                                                      *
* Last revis.: 2009-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir1bzg",oParentObject,m.pOPER)
return(i_retval)

define class tgsir1bzg as StdBatch
  * --- Local variables
  pOPER = space(1)
  L_I = 0
  * --- WorkFile variables
  TMPVEND1_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Zoom Gestioni, (da GSIR_MGI)
    * --- Non posso usare lo zoom standard a causa del link, mi darebbe valore non 
    *     ammesso quando inserisco uno zoom o il nome di una gestione che non �
    *     a menu
    do case
      case this.pOPER = "C"
        * --- Creo il cursore per lo zoom delle gestioni
        if !Used( i_CUR_MENU )
          cp_menu()
        endif
        SELECT PADR(STRTRAN(NAMEPROC,"#",","),250," ")AS NAMEPROC,PADR(STRTRAN(VOCEMENU,"&",""),60," ")AS VOCEMENU,ah_MsgFormat("Gestione")AS TIPO FROM(i_CUR_MENU)WHERE NOT EMPTY(NAMEPROC)AND NAMEPROC<>"NoNameProc" INTO CURSOR CURGEST
        * --- Creo cursore per lo zoom sulle tabelle
        CREATE CURSOR CURTBLS (NAMEPROC C(250), VOCEMENU C(60), TIPO C(20))
        cp_ReadXdc()
        this.L_I = 1
        do while this.L_I <= i_dcx.GetTablesCount()
          INSERT INTO CURTBLS(NAMEPROC,VOCEMENU,TIPO)VALUES(i_dcx.GetTable(this.L_I),LEFT(i_dcx.GetTableDescr(this.L_I),60),ah_MsgFormat("Tabella"))
          this.L_I = this.L_I + 1
        enddo
        * --- Metto in union i due cursori
        SELECT * FROM CURGEST UNION SELECT * FROM CURTBLS INTO CURSOR TMPCURAPPO 
        * --- Trasformo il cursore fox in una tabella temporanea
        * --- Create temporary table TMPVEND1
        i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPVEND1_proto';
              )
        this.TMPVEND1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        CURTOTAB("TMPCURAPPO", "TMPVEND1", this)
        * --- Chiudo i cursori
        USE IN SELECT("CURTBLS")
        USE IN SELECT("CURGEST")
        USE IN SELECT("TMPCURAPPO")
      case this.pOPER = "S"
        * --- Alla selezione di un record dello zoom valorizzo le variabili della maschera delle
        *     gestioni
        This.oParentObject.oParentObject.w_GIPROGRA = IIF( AT( ",", this.oParentObject.w_PROGRAM ) <> 0, SUBSTR( this.oParentObject.w_PROGRAM, 1, AT(",", this.oParentObject.w_PROGRAM ) -1 ), this.oParentObject.w_PROGRAM )
        This.oParentObject.oParentObject.o_GIPROGRA = IIF( AT( ",", this.oParentObject.w_PROGRAM ) <> 0, SUBSTR( this.oParentObject.w_PROGRAM, 1, AT(",", this.oParentObject.w_PROGRAM ) -1 ), this.oParentObject.w_PROGRAM )
        if g_APPLICATION="ADHOC REVOLUTION"
          This.oParentObject.oParentObject.w_GIDESC = this.oParentObject.w_DESCRI
        endif
        This.oParentObject.oParentObject.w_GIDESGES = this.oParentObject.w_DESCRI
        This.oParentObject.oParentObject.w_GIPARAM = IIF( AT( ",", this.oParentObject.w_PROGRAM ) <> 0, STRTRAN(SUBSTR( this.oParentObject.w_PROGRAM, AT(",", this.oParentObject.w_PROGRAM ) + 1 ),"'"), "" )
      case this.pOPER = "D"
        * --- All'evento Done chiudo la tabella temporanea
        * --- Drop temporary table TMPVEND1
        i_nIdx=cp_GetTableDefIdx('TMPVEND1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND1')
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMPVEND1'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
