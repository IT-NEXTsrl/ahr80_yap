* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_amg                                                        *
*              Menu contestuale per query                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_16]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-12-06                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsir_amg"))

* --- Class definition
define class tgsir_amg as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 653
  Height = 188+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=210333545
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  IRDRMENU_IDX = 0
  cFile = "IRDRMENU"
  cKeySelect = "MGSERIAL"
  cKeyWhere  = "MGSERIAL=this.w_MGSERIAL"
  cKeyWhereODBC = '"MGSERIAL="+cp_ToStrODBC(this.w_MGSERIAL)';

  cKeyWhereODBCqualified = '"IRDRMENU.MGSERIAL="+cp_ToStrODBC(this.w_MGSERIAL)';

  cPrg = "gsir_amg"
  cComment = "Menu contestuale per query"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MGSERIAL = space(10)
  w_MG__MENU = space(254)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MGSERIAL = this.W_MGSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'IRDRMENU','gsir_amg')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_amgPag1","gsir_amg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Men� contestuale per query")
      .Pages(1).HelpContextID = 96189022
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMGSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='IRDRMENU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.IRDRMENU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.IRDRMENU_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MGSERIAL = NVL(MGSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from IRDRMENU where MGSERIAL=KeySet.MGSERIAL
    *
    i_nConn = i_TableProp[this.IRDRMENU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('IRDRMENU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "IRDRMENU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' IRDRMENU '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MGSERIAL',this.w_MGSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MGSERIAL = NVL(MGSERIAL,space(10))
        .op_MGSERIAL = .w_MGSERIAL
        .w_MG__MENU = NVL(MG__MENU,space(254))
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(ah_msgformat("Indicare le voci di menu da definire nella contestualizzazione del tasto destro%0per le query di InfoPublisher"),'','')
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(ah_msgformat("Utilizzare come separatore delle voci di menu '\'%0Esempio: acquisti \ articolo -> crea due sotto menu uno di nome acquisti e uno articolo"),'','')
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'IRDRMENU')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MGSERIAL = space(10)
      .w_MG__MENU = space(254)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(ah_msgformat("Indicare le voci di menu da definire nella contestualizzazione del tasto destro%0per le query di InfoPublisher"),'','')
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(ah_msgformat("Utilizzare come separatore delle voci di menu '\'%0Esempio: acquisti \ articolo -> crea due sotto menu uno di nome acquisti e uno articolo"),'','')
      endif
    endwith
    cp_BlankRecExtFlds(this,'IRDRMENU')
    this.DoRTCalc(1,2,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IRDRMENU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEMENU","i_codazi,w_MGSERIAL")
      .op_codazi = .w_codazi
      .op_MGSERIAL = .w_MGSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMGSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oMG__MENU_1_3.enabled = i_bVal
      .Page1.oPag.oObj_1_6.enabled = i_bVal
      .Page1.oPag.oObj_1_7.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMGSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMGSERIAL_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'IRDRMENU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.IRDRMENU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MGSERIAL,"MGSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MG__MENU,"MG__MENU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.IRDRMENU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])
    i_lTable = "IRDRMENU"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.IRDRMENU_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.IRDRMENU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.IRDRMENU_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEMENU","i_codazi,w_MGSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into IRDRMENU
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'IRDRMENU')
        i_extval=cp_InsertValODBCExtFlds(this,'IRDRMENU')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MGSERIAL,MG__MENU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MGSERIAL)+;
                  ","+cp_ToStrODBC(this.w_MG__MENU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'IRDRMENU')
        i_extval=cp_InsertValVFPExtFlds(this,'IRDRMENU')
        cp_CheckDeletedKey(i_cTable,0,'MGSERIAL',this.w_MGSERIAL)
        INSERT INTO (i_cTable);
              (MGSERIAL,MG__MENU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MGSERIAL;
                  ,this.w_MG__MENU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.IRDRMENU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.IRDRMENU_IDX,i_nConn)
      *
      * update IRDRMENU
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'IRDRMENU')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MG__MENU="+cp_ToStrODBC(this.w_MG__MENU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'IRDRMENU')
        i_cWhere = cp_PKFox(i_cTable  ,'MGSERIAL',this.w_MGSERIAL  )
        UPDATE (i_cTable) SET;
              MG__MENU=this.w_MG__MENU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.IRDRMENU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.IRDRMENU_IDX,i_nConn)
      *
      * delete IRDRMENU
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MGSERIAL',this.w_MGSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IRDRMENU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(ah_msgformat("Indicare le voci di menu da definire nella contestualizzazione del tasto destro%0per le query di InfoPublisher"),'','')
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(ah_msgformat("Utilizzare come separatore delle voci di menu '\'%0Esempio: acquisti \ articolo -> crea due sotto menu uno di nome acquisti e uno articolo"),'','')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEMENU","i_codazi,w_MGSERIAL")
          .op_MGSERIAL = .w_MGSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate(ah_msgformat("Indicare le voci di menu da definire nella contestualizzazione del tasto destro%0per le query di InfoPublisher"),'','')
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(ah_msgformat("Utilizzare come separatore delle voci di menu '\'%0Esempio: acquisti \ articolo -> crea due sotto menu uno di nome acquisti e uno articolo"),'','')
    endwith
  return

  proc Calculate_JMCSEOEWRW()
    with this
          * --- Aggiorna tabella vista
          gsir_btv(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Record Deleted") or lower(cEvent)==lower("Record Updated")
          .Calculate_JMCSEOEWRW()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMGSERIAL_1_1.value==this.w_MGSERIAL)
      this.oPgFrm.Page1.oPag.oMGSERIAL_1_1.value=this.w_MGSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMG__MENU_1_3.value==this.w_MG__MENU)
      this.oPgFrm.Page1.oPag.oMG__MENU_1_3.value=this.w_MG__MENU
    endif
    cp_SetControlsValueExtFlds(this,'IRDRMENU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsir_amgPag1 as StdContainer
  Width  = 649
  height = 188
  stdWidth  = 649
  stdheight = 188
  resizeXpos=392
  resizeYpos=131
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMGSERIAL_1_1 as StdField with uid="MVNYSHMYZZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MGSERIAL", cQueryName = "MGSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale menu",;
    HelpContextID = 31526162,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=99, Top=13, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMG__MENU_1_3 as StdField with uid="QECUVZMRHQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MG__MENU", cQueryName = "MG__MENU",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indicare le voci di menu da creare separate dal separatore '\'",;
    HelpContextID = 229362971,;
   bGlobalFont=.t.,;
    Height=85, Width=545, Left=99, Top=99, InputMask=replicate('X',254)


  add object oObj_1_6 as cp_calclbl with uid="AMQVJQPUOM",left=99, top=36, width=545,height=31,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 236099558


  add object oObj_1_7 as cp_calclbl with uid="ZLDUHXCFON",left=99, top=66, width=545,height=31,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 236099558

  add object oStr_1_2 as StdString with uid="OJGCWFIIYG",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=95, Height=18,;
    Caption="Seriale menu:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="CUPCYVZHGT",Visible=.t., Left=38, Top=99,;
    Alignment=1, Width=60, Height=18,;
    Caption="Menu:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_amg','IRDRMENU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MGSERIAL=IRDRMENU.MGSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
