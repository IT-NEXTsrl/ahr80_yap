* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_kqa                                                        *
*              Autorizzazioni query InfoPublisher                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-24                                                      *
* Last revis.: 2013-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsir_kqa",oParentObject))

* --- Class definition
define class tgsir_kqa as StdForm
  Top    = 13
  Left   = 8

  * --- Standard Properties
  Width  = 633
  Height = 500+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-01-18"
  HelpContextID=132738921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  INF_AQM_IDX = 0
  CPGROUPS_IDX = 0
  CPUSERS_IDX = 0
  cPrg = "gsir_kqa"
  cComment = "Autorizzazioni query InfoPublisher"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_QACODQUE = space(20)
  o_QACODQUE = space(20)
  w_DESCQUE = space(254)
  w_QAFLUTGR = space(1)
  o_QAFLUTGR = space(1)
  w_QAUTEGRP = 0
  w_AQCODICE = space(20)
  o_AQCODICE = space(20)
  w_SELEALL = space(1)
  w_GESTROW2 = 0
  w_SELEZGEST = space(1)
  o_SELEZGEST = space(1)
  w_QAUTEGRP = 0
  w_DESGRP = space(20)
  w_DESUTE = space(20)
  w_SELEZUTE = space(1)
  o_SELEZUTE = space(1)
  w_SELEZGRP = space(1)
  o_SELEZGRP = space(1)
  w_GESTROW = 0
  o_GESTROW = 0
  w_AQCODICE = space(20)
  w_ZOOMGEST1 = .NULL.
  w_ZOOMUTE = .NULL.
  w_ZOOMGRP = .NULL.
  w_ZOOMQUE = .NULL.
  w_ZOOMGEST2 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_kqaPag1","gsir_kqa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Query")
      .Pages(2).addobject("oPag","tgsir_kqaPag2","gsir_kqa",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Utenti/Gruppi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oQACODQUE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMGEST1 = this.oPgFrm.Pages(1).oPag.ZOOMGEST1
    this.w_ZOOMUTE = this.oPgFrm.Pages(1).oPag.ZOOMUTE
    this.w_ZOOMGRP = this.oPgFrm.Pages(1).oPag.ZOOMGRP
    this.w_ZOOMQUE = this.oPgFrm.Pages(2).oPag.ZOOMQUE
    this.w_ZOOMGEST2 = this.oPgFrm.Pages(2).oPag.ZOOMGEST2
    DoDefault()
    proc Destroy()
      this.w_ZOOMGEST1 = .NULL.
      this.w_ZOOMUTE = .NULL.
      this.w_ZOOMGRP = .NULL.
      this.w_ZOOMQUE = .NULL.
      this.w_ZOOMGEST2 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='INF_AQM'
    this.cWorkTables[2]='CPGROUPS'
    this.cWorkTables[3]='CPUSERS'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_QACODQUE=space(20)
      .w_DESCQUE=space(254)
      .w_QAFLUTGR=space(1)
      .w_QAUTEGRP=0
      .w_AQCODICE=space(20)
      .w_SELEALL=space(1)
      .w_GESTROW2=0
      .w_SELEZGEST=space(1)
      .w_QAUTEGRP=0
      .w_DESGRP=space(20)
      .w_DESUTE=space(20)
      .w_SELEZUTE=space(1)
      .w_SELEZGRP=space(1)
      .w_GESTROW=0
      .w_AQCODICE=space(20)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_QACODQUE))
          .link_1_1('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOMGEST1.Calculate()
      .oPgFrm.Page1.oPag.ZOOMUTE.Calculate()
      .oPgFrm.Page1.oPag.ZOOMGRP.Calculate()
          .DoRTCalc(2,2,.f.)
        .w_QAFLUTGR = 'U'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_QAUTEGRP))
          .link_2_2('Full')
        endif
      .oPgFrm.Page2.oPag.ZOOMQUE.Calculate()
        .w_AQCODICE = .w_ZOOMQUE.getvar('AQCODICE')
      .oPgFrm.Page2.oPag.ZOOMGEST2.Calculate(.w_AQCODICE)
          .DoRTCalc(6,6,.f.)
        .w_GESTROW2 = .w_ZOOMGEST2.getvar('CPROWNUM')
        .w_SELEZGEST = 'N'
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_QAUTEGRP))
          .link_2_16('Full')
        endif
          .DoRTCalc(10,11,.f.)
        .w_SELEZUTE = 'N'
        .w_SELEZGRP = 'N'
        .w_GESTROW = .w_ZOOMGEST1.getvar('CPROWNUM')
        .w_AQCODICE = .w_QACODQUE
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_12.enabled = this.oPgFrm.Page2.oPag.oBtn_2_12.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_14.enabled = this.oPgFrm.Page2.oPag.oBtn_2_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMGEST1.Calculate()
        .oPgFrm.Page1.oPag.ZOOMUTE.Calculate()
        .oPgFrm.Page1.oPag.ZOOMGRP.Calculate()
        .oPgFrm.Page2.oPag.ZOOMQUE.Calculate()
        .DoRTCalc(1,4,.t.)
            .w_AQCODICE = .w_ZOOMQUE.getvar('AQCODICE')
        .oPgFrm.Page2.oPag.ZOOMGEST2.Calculate(.w_AQCODICE)
        .DoRTCalc(6,6,.t.)
            .w_GESTROW2 = .w_ZOOMGEST2.getvar('CPROWNUM')
        .DoRTCalc(8,13,.t.)
            .w_GESTROW = .w_ZOOMGEST1.getvar('CPROWNUM')
        if .o_GESTROW<>.w_GESTROW
          .Calculate_UGANUSDAOF()
        endif
        if .o_AQCODICE<>.w_AQCODICE
          .Calculate_JLPEKANCFF()
        endif
        if .o_SELEZUTE<>.w_SELEZUTE
          .Calculate_LUCIMIZTYP()
        endif
        if .o_SELEZGRP<>.w_SELEZGRP
          .Calculate_ITHATPFGMG()
        endif
        if .o_SELEZGEST<>.w_SELEZGEST
          .Calculate_KIDKWJEEZQ()
        endif
        if .o_QACODQUE<>.w_QACODQUE
            .w_AQCODICE = .w_QACODQUE
        endif
        if .o_QAFLUTGR<>.w_QAFLUTGR
          .Calculate_MTKEIEKWLN()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMGEST1.Calculate()
        .oPgFrm.Page1.oPag.ZOOMUTE.Calculate()
        .oPgFrm.Page1.oPag.ZOOMGRP.Calculate()
        .oPgFrm.Page2.oPag.ZOOMQUE.Calculate()
        .oPgFrm.Page2.oPag.ZOOMGEST2.Calculate(.w_AQCODICE)
    endwith
  return

  proc Calculate_UGANUSDAOF()
    with this
          * --- Aggiorna zoomute e zoomgrp
          CHIUDICURS(this;
             )
          GSIR_BQA(this;
              ,'A';
              ,'1';
             )
    endwith
  endproc
  proc Calculate_JLPEKANCFF()
    with this
          * --- Aggiorna zoomgest2
          CHIUDICURS(this;
             )
          GSIR_BQA(this;
              ,'A';
              ,'2';
             )
    endwith
  endproc
  proc Calculate_LUCIMIZTYP()
    with this
          * --- De/seleziona tutti zoomute
          SELZOOM(this;
              ,.w_ZOOMUTE;
              ,.w_SELEZUTE;
             )
    endwith
  endproc
  proc Calculate_ITHATPFGMG()
    with this
          * --- De/seleziona tutti zoomgrp
          SELZOOM(this;
              ,.w_ZOOMGRP;
              ,.w_SELEZGRP;
             )
    endwith
  endproc
  proc Calculate_KIDKWJEEZQ()
    with this
          * --- De/seleziona tutti zoomgest2
          SELZOOM(this;
              ,.w_ZOOMGEST2;
              ,.w_SELEZGEST;
             )
    endwith
  endproc
  proc Calculate_XXRQURPGRS()
    with this
          * --- Salva contenuto di zoomute e zoomgrp
          SALVACURS(this;
              ,'1';
             )
    endwith
  endproc
  proc Calculate_QRZNDGSTDS()
    with this
          * --- Ripristino contenuto di zoomute e zoomgrp
          CARICACURS(this;
              ,'1';
             )
    endwith
  endproc
  proc Calculate_ITQNVDXRNW()
    with this
          * --- Chiusura cursori di appoggio
          CHIUDICURS(this;
             )
    endwith
  endproc
  proc Calculate_SLPRXOKIVJ()
    with this
          * --- Salva contenuto di zoomgest2
          SALVACURS(this;
              ,'2';
             )
    endwith
  endproc
  proc Calculate_DJHRXHQBCC()
    with this
          * --- Ripristino contenuto di zoomgest2
          CARICACURS(this;
              ,'2';
             )
    endwith
  endproc
  proc Calculate_MTKEIEKWLN()
    with this
          * --- Sbianco il campo w_qautegrp
          .w_QAUTEGRP = 0
          .w_DESGRP = SPACE(20)
          .w_DESUTE = SPACE(20)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oQAUTEGRP_2_2.enabled = this.oPgFrm.Page2.oPag.oQAUTEGRP_2_2.mCond()
    this.oPgFrm.Page2.oPag.oQAUTEGRP_2_16.enabled = this.oPgFrm.Page2.oPag.oQAUTEGRP_2_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oQAUTEGRP_2_2.visible=!this.oPgFrm.Page2.oPag.oQAUTEGRP_2_2.mHide()
    this.oPgFrm.Page2.oPag.oQAUTEGRP_2_16.visible=!this.oPgFrm.Page2.oPag.oQAUTEGRP_2_16.mHide()
    this.oPgFrm.Page2.oPag.oDESGRP_2_17.visible=!this.oPgFrm.Page2.oPag.oDESGRP_2_17.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page2.oPag.oDESUTE_2_19.visible=!this.oPgFrm.Page2.oPag.oDESUTE_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_20.visible=!this.oPgFrm.Page2.oPag.oStr_2_20.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsir_kqa
    * --- Quando premo il bottone ricerca ribadisco il valore della variabile
    * --- w_AQCODICE che viene utilizzata nella query dello zoom
    IF cEvent = 'GRicerca'
       .w_AQCODICE = .w_QACODQUE
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMGEST1.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMUTE.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMGRP.Event(cEvent)
      .oPgFrm.Page2.oPag.ZOOMQUE.Event(cEvent)
      .oPgFrm.Page2.oPag.ZOOMGEST2.Event(cEvent)
        if lower(cEvent)==lower("GRicerca")
          .Calculate_UGANUSDAOF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("QRicerca")
          .Calculate_JLPEKANCFF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoomute before query") or lower(cEvent)==lower("w_zoomgrp before query")
          .Calculate_XXRQURPGRS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoomgrp after query") or lower(cEvent)==lower("w_zoomute after query")
          .Calculate_QRZNDGSTDS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_ITQNVDXRNW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoomgest2 before query")
          .Calculate_SLPRXOKIVJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoomgest2 after query")
          .Calculate_DJHRXHQBCC()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=QACODQUE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_AQM_IDX,3]
    i_lTable = "INF_AQM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2], .t., this.INF_AQM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_QACODQUE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsir_maq',True,'INF_AQM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AQCODICE like "+cp_ToStrODBC(trim(this.w_QACODQUE)+"%");

          i_ret=cp_SQL(i_nConn,"select AQCODICE,AQDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AQCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AQCODICE',trim(this.w_QACODQUE))
          select AQCODICE,AQDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AQCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_QACODQUE)==trim(_Link_.AQCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_QACODQUE) and !this.bDontReportError
            deferred_cp_zoom('INF_AQM','*','AQCODICE',cp_AbsName(oSource.parent,'oQACODQUE_1_1'),i_cWhere,'gsir_maq',"Query InfoPublisher",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AQCODICE,AQDES";
                     +" from "+i_cTable+" "+i_lTable+" where AQCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AQCODICE',oSource.xKey(1))
            select AQCODICE,AQDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_QACODQUE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AQCODICE,AQDES";
                   +" from "+i_cTable+" "+i_lTable+" where AQCODICE="+cp_ToStrODBC(this.w_QACODQUE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AQCODICE',this.w_QACODQUE)
            select AQCODICE,AQDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_QACODQUE = NVL(_Link_.AQCODICE,space(20))
      this.w_DESCQUE = NVL(_Link_.AQDES,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_QACODQUE = space(20)
      endif
      this.w_DESCQUE = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2])+'\'+cp_ToStr(_Link_.AQCODICE,1)
      cp_ShowWarn(i_cKey,this.INF_AQM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_QACODQUE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=QAUTEGRP
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_QAUTEGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_QAUTEGRP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_QAUTEGRP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_QAUTEGRP) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oQAUTEGRP_2_2'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_QAUTEGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_QAUTEGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_QAUTEGRP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_QAUTEGRP = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_QAUTEGRP = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_QAUTEGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=QAUTEGRP
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPGROUPS_IDX,3]
    i_lTable = "CPGROUPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2], .t., this.CPGROUPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_QAUTEGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPGROUPS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_QAUTEGRP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_QAUTEGRP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_QAUTEGRP) and !this.bDontReportError
            deferred_cp_zoom('CPGROUPS','*','CODE',cp_AbsName(oSource.parent,'oQAUTEGRP_2_16'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_QAUTEGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_QAUTEGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_QAUTEGRP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_QAUTEGRP = NVL(_Link_.CODE,0)
      this.w_DESGRP = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_QAUTEGRP = 0
      endif
      this.w_DESGRP = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPGROUPS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPGROUPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_QAUTEGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oQACODQUE_1_1.value==this.w_QACODQUE)
      this.oPgFrm.Page1.oPag.oQACODQUE_1_1.value=this.w_QACODQUE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCQUE_1_3.value==this.w_DESCQUE)
      this.oPgFrm.Page1.oPag.oDESCQUE_1_3.value=this.w_DESCQUE
    endif
    if not(this.oPgFrm.Page2.oPag.oQAFLUTGR_2_1.RadioValue()==this.w_QAFLUTGR)
      this.oPgFrm.Page2.oPag.oQAFLUTGR_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oQAUTEGRP_2_2.value==this.w_QAUTEGRP)
      this.oPgFrm.Page2.oPag.oQAUTEGRP_2_2.value=this.w_QAUTEGRP
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEALL_2_6.RadioValue()==this.w_SELEALL)
      this.oPgFrm.Page2.oPag.oSELEALL_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZGEST_2_11.RadioValue()==this.w_SELEZGEST)
      this.oPgFrm.Page2.oPag.oSELEZGEST_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oQAUTEGRP_2_16.value==this.w_QAUTEGRP)
      this.oPgFrm.Page2.oPag.oQAUTEGRP_2_16.value=this.w_QAUTEGRP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRP_2_17.value==this.w_DESGRP)
      this.oPgFrm.Page2.oPag.oDESGRP_2_17.value=this.w_DESGRP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESUTE_2_19.value==this.w_DESUTE)
      this.oPgFrm.Page2.oPag.oDESUTE_2_19.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZUTE_1_14.RadioValue()==this.w_SELEZUTE)
      this.oPgFrm.Page1.oPag.oSELEZUTE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZGRP_1_15.RadioValue()==this.w_SELEZGRP)
      this.oPgFrm.Page1.oPag.oSELEZGRP_1_15.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_QACODQUE = this.w_QACODQUE
    this.o_QAFLUTGR = this.w_QAFLUTGR
    this.o_AQCODICE = this.w_AQCODICE
    this.o_SELEZGEST = this.w_SELEZGEST
    this.o_SELEZUTE = this.w_SELEZUTE
    this.o_SELEZGRP = this.w_SELEZGRP
    this.o_GESTROW = this.w_GESTROW
    return

enddefine

* --- Define pages as container
define class tgsir_kqaPag1 as StdContainer
  Width  = 629
  height = 501
  stdWidth  = 629
  stdheight = 501
  resizeXpos=274
  resizeYpos=179
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oQACODQUE_1_1 as StdField with uid="EWLAMGVNOV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_QACODQUE", cQueryName = "QACODQUE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 229246795,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=64, Top=22, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="INF_AQM", cZoomOnZoom="gsir_maq", oKey_1_1="AQCODICE", oKey_1_2="this.w_QACODQUE"

  func oQACODQUE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oQACODQUE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oQACODQUE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_AQM','*','AQCODICE',cp_AbsName(this.parent,'oQACODQUE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gsir_maq',"Query InfoPublisher",'',this.parent.oContained
  endproc
  proc oQACODQUE_1_1.mZoomOnZoom
    local i_obj
    i_obj=gsir_maq()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AQCODICE=this.parent.oContained.w_QACODQUE
     i_obj.ecpSave()
  endproc

  add object oDESCQUE_1_3 as StdField with uid="KYFTRQXKGW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCQUE", cQueryName = "DESCQUE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 40831542,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=233, Top=22, InputMask=replicate('X',254)


  add object ZOOMGEST1 as cp_zoombox with uid="BJHYZKITNF",left=13, top=65, width=616,height=178,;
    caption='ZOOMGEST1',;
   bGlobalFont=.t.,;
    cTable="INF_AGIM",cZoomFile="GSIR_KQA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "GRicerca",;
    nPag=1;
    , HelpContextID = 30988538


  add object ZOOMUTE as cp_szoombox with uid="CITMCLGBRR",left=13, top=295, width=307,height=158,;
    caption='ZOOMUTE',;
   bGlobalFont=.t.,;
    cTable="CPUSERS",cZoomFile="GSIR_KQA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 28890518


  add object ZOOMGRP as cp_szoombox with uid="AZINZCFLDO",left=322, top=295, width=307,height=158,;
    caption='ZOOMGRP',;
   bGlobalFont=.t.,;
    cTable="CPGROUPS",cZoomFile="GSIR_KQA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 19343978


  add object oBtn_1_7 as StdButton with uid="KCGHJEXCAH",left=567, top=12, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare zoom gestioni";
    , HelpContextID = 44183318;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        .NotifyEvent('GRicerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_QACODQUE))
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="MUYEUMWNCW",left=515, top=456, width=48,height=45,;
    CpPicture="bmp\AGGIUNGI.BMP", caption="", nPag=1;
    , ToolTipText = "Attribuisce le autorizzazioni alla query";
    , HelpContextID = 217266182;
    , Caption='\<Assegna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSIR_BQA(this.Parent.oContained,"U", "1")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_QACODQUE) And Not Empty(.w_GESTROW))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="XTLJWWQLMR",left=567, top=456, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 189981446;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="FWNMFPCCFA",left=567, top=246, width=48,height=45,;
    CpPicture="bmp\filtra.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per indicare i filtri da impostare";
    , HelpContextID = 122569130;
    , Caption='\<Filtri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSIR_KFR(this)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZUTE_1_14 as StdRadio with uid="VDPYLLMZLT",rtseq=12,rtrep=.f.,left=13, top=455, width=149,height=34;
    , cFormVar="w_SELEZUTE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZUTE_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 50371435
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 50371435
      this.Buttons(2).Top=16
      this.SetAll("Width",147)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZUTE_1_14.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSELEZUTE_1_14.GetRadio()
    this.Parent.oContained.w_SELEZUTE = this.RadioValue()
    return .t.
  endfunc

  func oSELEZUTE_1_14.SetRadio()
    this.Parent.oContained.w_SELEZUTE=trim(this.Parent.oContained.w_SELEZUTE)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZUTE=='T',1,;
      iif(this.Parent.oContained.w_SELEZUTE=='N',2,;
      0))
  endfunc

  add object oSELEZGRP_1_15 as StdRadio with uid="CYCECTWASZ",rtseq=13,rtrep=.f.,left=322, top=455, width=143,height=34;
    , cFormVar="w_SELEZGRP", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZGRP_1_15.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 184509578
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 184509578
      this.Buttons(2).Top=16
      this.SetAll("Width",141)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZGRP_1_15.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSELEZGRP_1_15.GetRadio()
    this.Parent.oContained.w_SELEZGRP = this.RadioValue()
    return .t.
  endfunc

  func oSELEZGRP_1_15.SetRadio()
    this.Parent.oContained.w_SELEZGRP=trim(this.Parent.oContained.w_SELEZGRP)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZGRP=='T',1,;
      iif(this.Parent.oContained.w_SELEZGRP=='N',2,;
      0))
  endfunc

  add object oStr_1_2 as StdString with uid="VIUYJWPHCW",Visible=.t., Left=0, Top=22,;
    Alignment=1, Width=62, Height=18,;
    Caption="Query:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="LHWFACMNMX",Visible=.t., Left=20, Top=46,;
    Alignment=0, Width=91, Height=17,;
    Caption="Contesti"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="XDONUHPMMK",Visible=.t., Left=22, Top=276,;
    Alignment=0, Width=77, Height=17,;
    Caption="Utenti"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="OUMASCQEGE",Visible=.t., Left=330, Top=276,;
    Alignment=0, Width=67, Height=17,;
    Caption="Gruppi"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsir_kqaPag2 as StdContainer
  Width  = 629
  height = 501
  stdWidth  = 629
  stdheight = 501
  resizeXpos=397
  resizeYpos=215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oQAFLUTGR_2_1 as StdCombo with uid="CNVHNPBCAT",rtseq=3,rtrep=.f.,left=107,top=22,width=73,height=21;
    , HelpContextID = 28784472;
    , cFormVar="w_QAFLUTGR",RowSource=""+"Utenti,"+"Gruppi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oQAFLUTGR_2_1.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oQAFLUTGR_2_1.GetRadio()
    this.Parent.oContained.w_QAFLUTGR = this.RadioValue()
    return .t.
  endfunc

  func oQAFLUTGR_2_1.SetRadio()
    this.Parent.oContained.w_QAFLUTGR=trim(this.Parent.oContained.w_QAFLUTGR)
    this.value = ;
      iif(this.Parent.oContained.w_QAFLUTGR=='U',1,;
      iif(this.Parent.oContained.w_QAFLUTGR=='G',2,;
      0))
  endfunc

  add object oQAUTEGRP_2_2 as StdField with uid="NLCLEHEQME",rtseq=4,rtrep=.f.,;
    cFormVar = "w_QAUTEGRP", cQueryName = "QAUTEGRP",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente",;
    HelpContextID = 205510826,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=270, Top=22, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_QAUTEGRP"

  func oQAUTEGRP_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_QAFLUTGR <> 'G')
    endwith
   endif
  endfunc

  func oQAUTEGRP_2_2.mHide()
    with this.Parent.oContained
      return (.w_QAFLUTGR = 'G')
    endwith
  endfunc

  func oQAUTEGRP_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oQAUTEGRP_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oQAUTEGRP_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oQAUTEGRP_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc


  add object oBtn_2_3 as StdButton with uid="GAVVVTJWDE",left=568, top=12, width=47,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Ricerca";
    , HelpContextID = 44183318;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        .NotifyEvent('QRicerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_QAUTEGRP))
      endwith
    endif
  endfunc


  add object ZOOMQUE as cp_zoombox with uid="GFVEHTUHXX",left=13, top=63, width=616,height=178,;
    caption='ZOOMQUE',;
   bGlobalFont=.t.,;
    cTable="INF_AQM",cZoomFile="GSIR_KQA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "QRicerca",;
    nPag=2;
    , HelpContextID = 41473430

  add object oSELEALL_2_6 as StdCheck with uid="AJGPZGIXNF",rtseq=6,rtrep=.f.,left=15, top=247, caption="Seleziona tutti",;
    ToolTipText = "Se attivo: inserisce a tutte le query le autorizzazioni specificate come autorizzazioni base",;
    HelpContextID = 126837978,;
    cFormVar="w_SELEALL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSELEALL_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSELEALL_2_6.GetRadio()
    this.Parent.oContained.w_SELEALL = this.RadioValue()
    return .t.
  endfunc

  func oSELEALL_2_6.SetRadio()
    this.Parent.oContained.w_SELEALL=trim(this.Parent.oContained.w_SELEALL)
    this.value = ;
      iif(this.Parent.oContained.w_SELEALL=='S',1,;
      0)
  endfunc


  add object ZOOMGEST2 as cp_szoombox with uid="LOCGWAECMQ",left=13, top=293, width=616,height=158,;
    caption='ZOOMGEST2',;
   bGlobalFont=.t.,;
    cTable="INF_AGIM",cZoomFile="GSIR2KQA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    nPag=2;
    , HelpContextID = 30988554

  add object oSELEZGEST_2_11 as StdRadio with uid="BRZZFXZXJD",rtseq=8,rtrep=.f.,left=13, top=455, width=148,height=34;
    , cFormVar="w_SELEZGEST", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZGEST_2_11.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 83927225
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 83927225
      this.Buttons(2).Top=16
      this.SetAll("Width",146)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZGEST_2_11.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSELEZGEST_2_11.GetRadio()
    this.Parent.oContained.w_SELEZGEST = this.RadioValue()
    return .t.
  endfunc

  func oSELEZGEST_2_11.SetRadio()
    this.Parent.oContained.w_SELEZGEST=trim(this.Parent.oContained.w_SELEZGEST)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZGEST=='T',1,;
      iif(this.Parent.oContained.w_SELEZGEST=='N',2,;
      0))
  endfunc


  add object oBtn_2_12 as StdButton with uid="IIXLSUKGYO",left=167, top=456, width=48,height=45,;
    CpPicture="bmp\filtra.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per indicare i filtri da impostare ";
    , HelpContextID = 122569130;
    , Caption='\<Filtri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_12.Click()
      with this.Parent.oContained
        GSIR_KFR(this)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_13 as StdButton with uid="MRPMNCKPPH",left=515, top=456, width=48,height=45,;
    CpPicture="bmp\AGGIUNGI.BMP", caption="", nPag=2;
    , ToolTipText = "Attribuisce le autorizzazioni alla query";
    , HelpContextID = 217266182;
    , Caption='A\<ssegna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        GSIR_BQA(this.Parent.oContained,"U", "2")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_QAUTEGRP <> 0 And Not Empty(.w_GESTROW2))
      endwith
    endif
  endfunc


  add object oBtn_2_14 as StdButton with uid="SKZVGETMGA",left=567, top=456, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 189981446;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oQAUTEGRP_2_16 as StdField with uid="FSSOXNNGED",rtseq=9,rtrep=.f.,;
    cFormVar = "w_QAUTEGRP", cQueryName = "QAUTEGRP",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Gruppo",;
    HelpContextID = 205510826,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=270, Top=22, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPGROUPS", oKey_1_1="CODE", oKey_1_2="this.w_QAUTEGRP"

  func oQAUTEGRP_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_QAFLUTGR <> 'U')
    endwith
   endif
  endfunc

  func oQAUTEGRP_2_16.mHide()
    with this.Parent.oContained
      return (.w_QAFLUTGR = 'U')
    endwith
  endfunc

  func oQAUTEGRP_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oQAUTEGRP_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oQAUTEGRP_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPGROUPS','*','CODE',cp_AbsName(this.parent,'oQAUTEGRP_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oDESGRP_2_17 as StdField with uid="EUTWRLWKGV",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESGRP", cQueryName = "DESGRP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 41743818,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=350, Top=22, InputMask=replicate('X',20)

  func oDESGRP_2_17.mHide()
    with this.Parent.oContained
      return (.w_QAFLUTGR = 'U')
    endwith
  endfunc

  add object oDESUTE_2_19 as StdField with uid="UHNDSXBELV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 223278538,;
   bGlobalFont=.t.,;
    Height=21, Width=186, Left=350, Top=22, InputMask=replicate('X',20)

  func oDESUTE_2_19.mHide()
    with this.Parent.oContained
      return (.w_QAFLUTGR = 'G')
    endwith
  endfunc

  add object oStr_2_7 as StdString with uid="PTOHXQPHKG",Visible=.t., Left=22, Top=46,;
    Alignment=0, Width=74, Height=17,;
    Caption="Query"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_10 as StdString with uid="KCJJKOCPED",Visible=.t., Left=22, Top=276,;
    Alignment=0, Width=74, Height=17,;
    Caption="Contesti"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_15 as StdString with uid="VICHNVZOJM",Visible=.t., Left=5, Top=22,;
    Alignment=1, Width=97, Height=17,;
    Caption="Utente/gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="ZZLRHKBUPQ",Visible=.t., Left=192, Top=22,;
    Alignment=1, Width=76, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (.w_QAFLUTGR = 'U')
    endwith
  endfunc

  add object oStr_2_20 as StdString with uid="AFGOKOLWBL",Visible=.t., Left=192, Top=22,;
    Alignment=1, Width=76, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  func oStr_2_20.mHide()
    with this.Parent.oContained
      return (.w_QAFLUTGR = 'G')
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="NDOKZWQFEK",Visible=.t., Left=140, Top=248,;
    Alignment=0, Width=488, Height=17,;
    Caption="Le impostazioni correnti saranno riportate su tutte le query come autorizzazioni generiche"    , ForeColor = RGB(255, 0, 0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (.w_SELEALL <> 'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_kqa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsir_kqa
* --- Procedura per De/Selezionare le righe degli zoom, prende come
* --- primo parametro oPARENT perch� la funzione viene lanciata come un
* --- batch da un calculate
Proc SELZOOM(oPARENT, oZOOM, cOPER)
   Local NC
   NC = oZOOM.cCursor
   UPDATE (NC) SET XCHK=IIF(cOPER = 'T',1,0)
EndProc

* --- Procedura per chiudere cursori
Proc CHIUDICURS(oPARENT)
   IF USED('CURUTE')
     SELECT CURUTE
     USE
   ENDIF
   IF USED('CURGRP')
     SELECT CURGRP
     USE
   ENDIF
   IF USED('CURGEST')
     SELECT CURGEST
     USE
   ENDIF
EndProc

* --- Procedura per salvare il contenuto dei cursori degli zoom
Proc SALVACURS(oPARENT, cOPER)
   DO CASE
     CASE cOPER = '1'
       IF USED(oPARENT.w_ZOOMUTE.cCursor)
         SELECT CODE AS CODICE FROM (oPARENT.w_ZOOMUTE.cCursor) INTO CURSOR CURUTE WHERE XCHK = 1
       ENDIF
       IF USED(oPARENT.w_ZOOMGRP.cCursor)
         SELECT CODE AS CODICE FROM (oPARENT.w_ZOOMGRP.cCursor) INTO CURSOR CURGRP WHERE XCHK = 1
       ENDIF
     CASE cOPER = '2'
       IF USED(oPARENT.w_ZOOMGEST2.cCursor)
         SELECT CPROWNUM AS CODICE FROM (oPARENT.w_ZOOMGEST2.cCursor) INTO CURSOR CURGEST WHERE XCHK = 1
       ENDIF
   ENDCASE
EndProc

* --- Procedura per ripristinare i XCHK degli zoom
Proc CARICACURS(oPARENT, cOPER)
   DO CASE
     CASE cOPER = '1'
       IF USED('CURUTE')
         UPDATE (oPARENT.w_ZOOMUTE.cCursor) SET XCHK = 1 WHERE CODE IN (SELECT CODICE FROM CURUTE)
       ENDIF
       IF USED('CURGRP')
         UPDATE (oPARENT.w_ZOOMGRP.cCursor) SET XCHK = 1 WHERE CODE IN (SELECT CODICE FROM CURGRP)
       ENDIF
     CASE cOPER = '2'
       IF USED('CURGEST')
         UPDATE (oPARENT.w_ZOOMGEST2.cCursor) SET XCHK = 1 WHERE CPROWNUM IN (SELECT CODICE FROM CURGEST)
       ENDIF
   ENDCASE
EndProc
* --- Fine Area Manuale
