* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_mgi                                                        *
*              Gestioni query                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-19                                                      *
* Last revis.: 2012-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsir_mgi")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsir_mgi")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsir_mgi")
  return

* --- Class definition
define class tgsir_mgi as StdPCForm
  Width  = 373
  Height = 423
  Top    = 3
  Left   = 2
  cComment = "Gestioni query"
  cPrg = "gsir_mgi"
  HelpContextID=29978473
  add object cnt as tcgsir_mgi
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsir_mgi as PCContext
  w_GICODQUE = space(20)
  w_CPROWORD = 0
  w_GIPROGRA = space(254)
  w_GIPARAM = space(15)
  w_GIDESC = space(50)
  w_CODREL = space(10)
  w_AQCODICE = space(20)
  w_AQQUEFIL = space(1)
  w_GIDESGES = space(50)
  w_GI__MENU = space(10)
  w_MG__MENU = space(254)
  proc Save(i_oFrom)
    this.w_GICODQUE = i_oFrom.w_GICODQUE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_GIPROGRA = i_oFrom.w_GIPROGRA
    this.w_GIPARAM = i_oFrom.w_GIPARAM
    this.w_GIDESC = i_oFrom.w_GIDESC
    this.w_CODREL = i_oFrom.w_CODREL
    this.w_AQCODICE = i_oFrom.w_AQCODICE
    this.w_AQQUEFIL = i_oFrom.w_AQQUEFIL
    this.w_GIDESGES = i_oFrom.w_GIDESGES
    this.w_GI__MENU = i_oFrom.w_GI__MENU
    this.w_MG__MENU = i_oFrom.w_MG__MENU
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_GICODQUE = this.w_GICODQUE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_GIPROGRA = this.w_GIPROGRA
    i_oTo.w_GIPARAM = this.w_GIPARAM
    i_oTo.w_GIDESC = this.w_GIDESC
    i_oTo.w_CODREL = this.w_CODREL
    i_oTo.w_AQCODICE = this.w_AQCODICE
    i_oTo.w_AQQUEFIL = this.w_AQQUEFIL
    i_oTo.w_GIDESGES = this.w_GIDESGES
    i_oTo.w_GI__MENU = this.w_GI__MENU
    i_oTo.w_MG__MENU = this.w_MG__MENU
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsir_mgi as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 373
  Height = 423
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-14"
  HelpContextID=29978473
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  INF_AGIM_IDX = 0
  XDC_TABLE_IDX = 0
  IRDRMENU_IDX = 0
  cFile = "INF_AGIM"
  cKeySelect = "GICODQUE"
  cKeyWhere  = "GICODQUE=this.w_GICODQUE"
  cKeyDetail  = "GICODQUE=this.w_GICODQUE"
  cKeyWhereODBC = '"GICODQUE="+cp_ToStrODBC(this.w_GICODQUE)';

  cKeyDetailWhereODBC = '"GICODQUE="+cp_ToStrODBC(this.w_GICODQUE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"INF_AGIM.GICODQUE="+cp_ToStrODBC(this.w_GICODQUE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'INF_AGIM.CPROWORD '
  cPrg = "gsir_mgi"
  cComment = "Gestioni query"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GICODQUE = space(20)
  w_CPROWORD = 0
  w_GIPROGRA = space(254)
  o_GIPROGRA = space(254)
  w_GIPARAM = space(15)
  w_GIDESC = space(50)
  w_CODREL = space(10)
  w_AQCODICE = space(20)
  w_AQQUEFIL = space(1)
  w_GIDESGES = space(50)
  w_GI__MENU = space(10)
  w_MG__MENU = space(254)

  * --- Children pointers
  gsir_mfr = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to gsir_mfr additive
    with this
      .Pages(1).addobject("oPag","tgsir_mgiPag1","gsir_mgi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure gsir_mfr
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='IRDRMENU'
    this.cWorkTables[3]='INF_AGIM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INF_AGIM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INF_AGIM_IDX,3]
  return

  function CreateChildren()
    this.gsir_mfr = CREATEOBJECT('stdLazyChild',this,'gsir_mfr')
    return

  procedure NewContext()
    return(createobject('tsgsir_mgi'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.gsir_mfr)
      this.gsir_mfr.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.gsir_mfr.HideChildrenChain()
    return

  procedure DestroyChildren()
    if !ISNULL(this.gsir_mfr)
      this.gsir_mfr.DestroyChildrenChain()
      this.gsir_mfr=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsir_mfr.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsir_mfr.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsir_mfr.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .gsir_mfr.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_GICODQUE,"FICODQUE";
             ,.w_CPROWNUM,"FIPRGRN";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_7_joined
    link_2_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from INF_AGIM where GICODQUE=KeySet.GICODQUE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.INF_AGIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AGIM_IDX,2],this.bLoadRecFilter,this.INF_AGIM_IDX,"gsir_mgi")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INF_AGIM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INF_AGIM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INF_AGIM '
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GICODQUE',this.w_GICODQUE  )
      select * from (i_cTable) INF_AGIM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODREL = alltrim(strtran(upper(g_VERSION),'REL.'))
        .w_AQCODICE = .oParentObject .w_AQCODICE
        .w_GICODQUE = NVL(GICODQUE,space(20))
        .w_AQQUEFIL = .oParentObject .w_AQQUEFIL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'INF_AGIM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_MG__MENU = space(254)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_GIPROGRA = NVL(GIPROGRA,space(254))
          .w_GIPARAM = NVL(GIPARAM,space(15))
          .w_GIDESC = NVL(GIDESC,space(50))
          .w_GIDESGES = NVL(GIDESGES,space(50))
          .w_GI__MENU = NVL(GI__MENU,space(10))
          if link_2_7_joined
            this.w_GI__MENU = NVL(MGSERIAL207,NVL(this.w_GI__MENU,space(10)))
            this.w_MG__MENU = NVL(MG__MENU207,space(254))
          else
          .link_2_7('Load')
          endif
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_AQQUEFIL = .oParentObject .w_AQQUEFIL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_GICODQUE=space(20)
      .w_CPROWORD=10
      .w_GIPROGRA=space(254)
      .w_GIPARAM=space(15)
      .w_GIDESC=space(50)
      .w_CODREL=space(10)
      .w_AQCODICE=space(20)
      .w_AQQUEFIL=space(1)
      .w_GIDESGES=space(50)
      .w_GI__MENU=space(10)
      .w_MG__MENU=space(254)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_GIDESC = this.oParentObject .w_AQDES
        .w_CODREL = alltrim(strtran(upper(g_VERSION),'REL.'))
        .w_AQCODICE = .oParentObject .w_AQCODICE
        .w_AQQUEFIL = .oParentObject .w_AQQUEFIL
        .w_GIDESGES = ''
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_GI__MENU))
         .link_2_7('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INF_AGIM')
    this.DoRTCalc(11,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oGIDESC_2_4.enabled = i_bVal
      .Page1.oPag.oGI__MENU_2_7.enabled = i_bVal
      .Page1.oPag.oMG__MENU_2_9.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.gsir_mfr.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'INF_AGIM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsir_mfr.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INF_AGIM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GICODQUE,"GICODQUE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_GIPROGRA C(254);
      ,t_GIPARAM C(15);
      ,t_GIDESC C(50);
      ,t_GIDESGES C(50);
      ,t_GI__MENU C(10);
      ,t_MG__MENU C(254);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsir_mgibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGIPROGRA_2_2.controlsource=this.cTrsName+'.t_GIPROGRA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGIPARAM_2_3.controlsource=this.cTrsName+'.t_GIPARAM'
    this.oPgFRm.Page1.oPag.oGIDESC_2_4.controlsource=this.cTrsName+'.t_GIDESC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGIDESGES_2_6.controlsource=this.cTrsName+'.t_GIDESGES'
    this.oPgFRm.Page1.oPag.oGI__MENU_2_7.controlsource=this.cTrsName+'.t_GI__MENU'
    this.oPgFRm.Page1.oPag.oMG__MENU_2_9.controlsource=this.cTrsName+'.t_MG__MENU'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(41)
    this.AddVLine(235)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INF_AGIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AGIM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_AGIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AGIM_IDX,2])
      *
      * insert into INF_AGIM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INF_AGIM')
        i_extval=cp_InsertValODBCExtFlds(this,'INF_AGIM')
        i_cFldBody=" "+;
                  "(GICODQUE,CPROWORD,GIPROGRA,GIPARAM,GIDESC"+;
                  ",GIDESGES,GI__MENU,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_GICODQUE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_GIPROGRA)+","+cp_ToStrODBC(this.w_GIPARAM)+","+cp_ToStrODBC(this.w_GIDESC)+;
             ","+cp_ToStrODBC(this.w_GIDESGES)+","+cp_ToStrODBCNull(this.w_GI__MENU)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INF_AGIM')
        i_extval=cp_InsertValVFPExtFlds(this,'INF_AGIM')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'GICODQUE',this.w_GICODQUE)
        INSERT INTO (i_cTable) (;
                   GICODQUE;
                  ,CPROWORD;
                  ,GIPROGRA;
                  ,GIPARAM;
                  ,GIDESC;
                  ,GIDESGES;
                  ,GI__MENU;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_GICODQUE;
                  ,this.w_CPROWORD;
                  ,this.w_GIPROGRA;
                  ,this.w_GIPARAM;
                  ,this.w_GIDESC;
                  ,this.w_GIDESGES;
                  ,this.w_GI__MENU;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.INF_AGIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AGIM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_GIPROGRA))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'INF_AGIM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'INF_AGIM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_GIPROGRA))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.gsir_mfr.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_GICODQUE,"FICODQUE";
                     ,this.w_CPROWNUM,"FIPRGRN";
                     )
              this.gsir_mfr.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INF_AGIM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'INF_AGIM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",GIPROGRA="+cp_ToStrODBC(this.w_GIPROGRA)+;
                     ",GIPARAM="+cp_ToStrODBC(this.w_GIPARAM)+;
                     ",GIDESC="+cp_ToStrODBC(this.w_GIDESC)+;
                     ",GIDESGES="+cp_ToStrODBC(this.w_GIDESGES)+;
                     ",GI__MENU="+cp_ToStrODBCNull(this.w_GI__MENU)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'INF_AGIM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,GIPROGRA=this.w_GIPROGRA;
                     ,GIPARAM=this.w_GIPARAM;
                     ,GIDESC=this.w_GIDESC;
                     ,GIDESGES=this.w_GIDESGES;
                     ,GI__MENU=this.w_GI__MENU;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_GIPROGRA)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.gsir_mfr.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_GICODQUE,"FICODQUE";
               ,this.w_CPROWNUM,"FIPRGRN";
               )
          this.gsir_mfr.mReplace()
          this.gsir_mfr.bSaveContext=.f.
        endif
      endscan
     this.gsir_mfr.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INF_AGIM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AGIM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_GIPROGRA))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- gsir_mfr : Deleting
        this.gsir_mfr.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_GICODQUE,"FICODQUE";
               ,this.w_CPROWNUM,"FIPRGRN";
               )
        this.gsir_mfr.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete INF_AGIM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_GIPROGRA))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INF_AGIM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AGIM_IDX,2])
    if i_bUpd
      with this
        if .o_GIPROGRA<>.w_GIPROGRA
          .Calculate_JVMWYXBGDI()
        endif
        .DoRTCalc(1,7,.t.)
          .w_AQQUEFIL = .oParentObject .w_AQQUEFIL
        if .o_GIPROGRA<>.w_GIPROGRA
          .w_GIDESGES = ''
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_JVMWYXBGDI()
    with this
          * --- Elimina parametri dalla stringa program
          .w_GIPROGRA = STRTRAN(IIF( AT( ',', .w_GIPROGRA ) <> 0, SUBSTR( .w_GIPROGRA, 1, AT(',', .w_GIPROGRA ) -1 ), .w_GIPROGRA),'_VZM','')
    endwith
  endproc
  proc Calculate_NGJORIZXYP()
    with this
          * --- Cancello autorizzazione gestione
          gsir1bqa(this;
              ,'G';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oGI__MENU_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oGI__MENU_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLinkPC_2_5.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_5.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.gsir_mfr.visible")=='L' And this.gsir_mfr.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_5.visible
      this.gsir_mfr.HideChildrenChain()
    endif 
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Delete row end")
          .Calculate_NGJORIZXYP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GI__MENU
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IRDRMENU_IDX,3]
    i_lTable = "IRDRMENU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2], .t., this.IRDRMENU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GI__MENU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIR_AMG',True,'IRDRMENU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGSERIAL like "+cp_ToStrODBC(trim(this.w_GI__MENU)+"%");

          i_ret=cp_SQL(i_nConn,"select MGSERIAL,MG__MENU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGSERIAL',trim(this.w_GI__MENU))
          select MGSERIAL,MG__MENU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GI__MENU)==trim(_Link_.MGSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GI__MENU) and !this.bDontReportError
            deferred_cp_zoom('IRDRMENU','*','MGSERIAL',cp_AbsName(oSource.parent,'oGI__MENU_2_7'),i_cWhere,'GSIR_AMG',"Elenco menu",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGSERIAL,MG__MENU";
                     +" from "+i_cTable+" "+i_lTable+" where MGSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGSERIAL',oSource.xKey(1))
            select MGSERIAL,MG__MENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GI__MENU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGSERIAL,MG__MENU";
                   +" from "+i_cTable+" "+i_lTable+" where MGSERIAL="+cp_ToStrODBC(this.w_GI__MENU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGSERIAL',this.w_GI__MENU)
            select MGSERIAL,MG__MENU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GI__MENU = NVL(_Link_.MGSERIAL,space(10))
      this.w_MG__MENU = NVL(_Link_.MG__MENU,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_GI__MENU = space(10)
      endif
      this.w_MG__MENU = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])+'\'+cp_ToStr(_Link_.MGSERIAL,1)
      cp_ShowWarn(i_cKey,this.IRDRMENU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GI__MENU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IRDRMENU_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IRDRMENU_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.MGSERIAL as MGSERIAL207"+ ",link_2_7.MG__MENU as MG__MENU207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on INF_AGIM.GI__MENU=link_2_7.MGSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and INF_AGIM.GI__MENU=link_2_7.MGSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oGIDESC_2_4.value==this.w_GIDESC)
      this.oPgFrm.Page1.oPag.oGIDESC_2_4.value=this.w_GIDESC
      replace t_GIDESC with this.oPgFrm.Page1.oPag.oGIDESC_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oGI__MENU_2_7.value==this.w_GI__MENU)
      this.oPgFrm.Page1.oPag.oGI__MENU_2_7.value=this.w_GI__MENU
      replace t_GI__MENU with this.oPgFrm.Page1.oPag.oGI__MENU_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMG__MENU_2_9.value==this.w_MG__MENU)
      this.oPgFrm.Page1.oPag.oMG__MENU_2_9.value=this.w_MG__MENU
      replace t_MG__MENU with this.oPgFrm.Page1.oPag.oMG__MENU_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIPROGRA_2_2.value==this.w_GIPROGRA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIPROGRA_2_2.value=this.w_GIPROGRA
      replace t_GIPROGRA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIPROGRA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIPARAM_2_3.value==this.w_GIPARAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIPARAM_2_3.value=this.w_GIPARAM
      replace t_GIPARAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIPARAM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIDESGES_2_6.value==this.w_GIDESGES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIDESGES_2_6.value=this.w_GIDESGES
      replace t_GIDESGES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIDESGES_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'INF_AGIM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_CPROWORD > 0) and (not(Empty(.w_CPROWORD)) AND not(Empty(.w_GIPROGRA)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_GIPARAM <> REPLICATE('@', 15)) and (not(Empty(.w_CPROWORD)) AND not(Empty(.w_GIPROGRA)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGIPARAM_2_3
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .gsir_mfr.CheckForm()
      if not(Empty(.w_CPROWORD)) AND not(Empty(.w_GIPROGRA))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GIPROGRA = this.w_GIPROGRA
    * --- gsir_mfr : Depends On
    this.gsir_mfr.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND not(Empty(t_GIPROGRA)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_GIPROGRA=space(254)
      .w_GIPARAM=space(15)
      .w_GIDESC=space(50)
      .w_GIDESGES=space(50)
      .w_GI__MENU=space(10)
      .w_MG__MENU=space(254)
      .DoRTCalc(1,4,.f.)
        .w_GIDESC = this.oParentObject .w_AQDES
      .DoRTCalc(6,8,.f.)
        .w_GIDESGES = ''
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_GI__MENU))
        .link_2_7('Full')
      endif
    endwith
    this.DoRTCalc(11,11,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_GIPROGRA = t_GIPROGRA
    this.w_GIPARAM = t_GIPARAM
    this.w_GIDESC = t_GIDESC
    this.w_GIDESGES = t_GIDESGES
    this.w_GI__MENU = t_GI__MENU
    this.w_MG__MENU = t_MG__MENU
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_GIPROGRA with this.w_GIPROGRA
    replace t_GIPARAM with this.w_GIPARAM
    replace t_GIDESC with this.w_GIDESC
    replace t_GIDESGES with this.w_GIDESGES
    replace t_GI__MENU with this.w_GI__MENU
    replace t_MG__MENU with this.w_MG__MENU
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsir_mgiPag1 as StdContainer
  Width  = 369
  height = 423
  stdWidth  = 369
  stdheight = 423
  resizeXpos=211
  resizeYpos=192
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=3, width=328,height=40,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Seq.",Field2="GIPROGRA",Label2="Nome contesto",Field3="GIDESGES",Label3="Descrizione",Field4="GIPARAM",Label4="Parametro",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 250758534

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=43,;
    width=325+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=44,width=324+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oGIDESC_2_4.Refresh()
      this.Parent.oGI__MENU_2_7.Refresh()
      this.Parent.oMG__MENU_2_9.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oGIDESC_2_4 as StdTrsField with uid="EELKGSWTNN",rtseq=5,rtrep=.t.,;
    cFormVar="w_GIDESC",value=space(50),;
    ToolTipText = "Descrizione",;
    HelpContextID = 112205414,;
    cTotal="", bFixedPos=.t., cQueryName = "GIDESC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=6, Top=315, InputMask=replicate('X',50)

  add object oLinkPC_2_5 as StdButton with uid="RABORSDVMH",width=24,height=25,;
   left=339, top=45,;
    caption="...", nPag=2;
    , ToolTipText = "Elenco campi filtro";
    , HelpContextID = 29777450;
  , bGlobalFont=.t.

    proc oLinkPC_2_5.Click()
      this.Parent.oContained.gsir_mfr.LinkPCClick()
    endproc

  func oLinkPC_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AQQUEFIL = 'N')
    endwith
   endif
  endfunc

  add object oGI__MENU_2_7 as StdTrsField with uid="JOKUVHZOSC",rtseq=10,rtrep=.t.,;
    cFormVar="w_GI__MENU",value=space(10),nZero=10,;
    ToolTipText = "Seriale menu contestualizzato",;
    HelpContextID = 141283003,;
    cTotal="", bFixedPos=.t., cQueryName = "GI__MENU",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=42, Top=339, cSayPict=["9999999999"], cGetPict=["9999999999"], InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IRDRMENU", cZoomOnZoom="GSIR_AMG", oKey_1_1="MGSERIAL", oKey_1_2="this.w_GI__MENU"

  func oGI__MENU_2_7.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_GIPROGRA))
    endwith
  endfunc

  func oGI__MENU_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oGI__MENU_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oGI__MENU_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IRDRMENU','*','MGSERIAL',cp_AbsName(this.parent,'oGI__MENU_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIR_AMG',"Elenco menu",'',this.parent.oContained
  endproc
  proc oGI__MENU_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSIR_AMG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGSERIAL=this.parent.oContained.w_GI__MENU
    i_obj.ecpSave()
  endproc

  add object oMG__MENU_2_9 as StdTrsField with uid="YOTEMKTFRA",rtseq=11,rtrep=.t.,;
    cFormVar="w_MG__MENU",value=space(254),;
    HelpContextID = 141282587,;
    cTotal="", bFixedPos=.t., cQueryName = "MG__MENU",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=54, Width=319, Left=42, Top=363, InputMask=replicate('X',254), readonly=.t.

  add object oStr_2_8 as StdString with uid="AVUFFQCEFH",Visible=.t., Left=8, Top=340,;
    Alignment=1, Width=33, Height=18,;
    Caption="Menu:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsir_mgiBodyRow as CPBodyRowCnt
  Width=315
  Height=int(fontmetric(1,"Arial",9,"")*2*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="QEXBTUANLE",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 50005354,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=32, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  func oCPROWORD_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CPROWORD > 0)
    endwith
    return bRes
  endfunc

  add object oGIPROGRA_2_2 as StdTrsField with uid="CPLIYDXGPW",rtseq=3,rtrep=.t.,;
    cFormVar="w_GIPROGRA",value=space(254),;
    ToolTipText = "Contesto",;
    HelpContextID = 176021159,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=191, Left=34, Top=0, cSayPict=[REPLICATE( '!', 254)], cGetPict=[REPLICATE( '!', 254 )], InputMask=replicate('X',254), bHasZoom = .t. 

  proc oGIPROGRA_2_2.mZoom
    do GSIR1KZG with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oGIPARAM_2_3 as StdTrsField with uid="HBIELYVVWE",rtseq=4,rtrep=.t.,;
    cFormVar="w_GIPARAM",value=space(15),;
    ToolTipText = "Parametro",;
    HelpContextID = 77389414,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=227, Top=0, InputMask=replicate('X',15)

  func oGIPARAM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GIPARAM <> REPLICATE('@', 15))
    endwith
    return bRes
  endfunc

  add object oGIDESGES_2_6 as StdTrsField with uid="BIMLNXTFAI",rtseq=9,rtrep=.t.,;
    cFormVar="w_GIDESGES",value=space(50),enabled=.f.,;
    ToolTipText = "Descrizione gestione",;
    HelpContextID = 89121095,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=191, Left=34, Top=19, InputMask=replicate('X',50)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_mgi','INF_AGIM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GICODQUE=INF_AGIM.GICODQUE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
