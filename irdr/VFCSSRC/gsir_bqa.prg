* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bqa                                                        *
*              Gestione autorizzazioni                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-24                                                      *
* Last revis.: 2005-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pPAGE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bqa",oParentObject,m.pOPER,m.pPAGE)
return(i_retval)

define class tgsir_bqa as StdBatch
  * --- Local variables
  pOPER = space(1)
  pPAGE = space(1)
  w_FLUTGR = space(1)
  w_UTEGRP = 0
  w_CODICE = space(20)
  w_ROWNUM = 0
  w_CODE = 0
  * --- WorkFile variables
  INF_AUQU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna ZOOM (da GSIR_KQA) 
    *     Parametri:
    *     pOPER: 'A' Aggiorna zoom, 'U' salva autorizzazioni
    *     pPAGE: '1' Lavora sugli zoom di pagina 1,  '2'  Lavora sugli zoom di pagina 2 
    do case
      case this.pPAGE = "1"
        * --- Lavoro su Pagina 1
        * --- Valorizzo i filtri per le query
        this.w_CODICE = this.oParentObject.w_QACODQUE
        this.w_ROWNUM = this.oParentObject.w_GESTROW
        do case
          case this.pOPER = "A"
            * --- Contrassegno le righe dello zoom che sono presenti in INF_AUQU
            VQ_EXEC("..\IRDR\EXE\QUERY\GSIR_BQA", This, "CURAPPO", "", .F. , .T.)
            * --- Utenti
            NC = this.oParentObject.w_ZOOMUTE.cCursor
            SELECT (NC)
            UPDATE (NC) SET XCHK = 0 WHERE 1 = 1
            UPDATE (NC) SET XCHK = 1 WHERE CODE IN (SELECT QAUTEGRP FROM CURAPPO WHERE QAFLUTGR = "U")
            * --- Gruppi
            NC = this.oParentObject.w_ZOOMGRP.cCursor
            SELECT (NC)
            UPDATE (NC) SET XCHK = 0 WHERE 1 = 1
            UPDATE (NC) SET XCHK = 1 WHERE CODE IN (SELECT QAUTEGRP FROM CURAPPO WHERE QAFLUTGR = "G")
            * --- Chiudo il cursore
            if USED("CURAPPO")
              SELECT CURAPPO
              USE
            endif
          case this.pOPER = "U"
            * --- Cancello tutti i vecchi valori per poi inserire i nuovi
            * --- Delete from INF_AUQU
            i_nConn=i_TableProp[this.INF_AUQU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"QACODQUE = "+cp_ToStrODBC(this.w_CODICE);
                    +" and QAROWPRG = "+cp_ToStrODBC(this.w_ROWNUM);
                     )
            else
              delete from (i_cTable) where;
                    QACODQUE = this.w_CODICE;
                    and QAROWPRG = this.w_ROWNUM;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Utenti
            NC = this.oParentObject.w_ZOOMUTE.cCursor
            select (NC)
            locate for XCHK = 1
            do while not(eof())
              * --- Inserisco gli utenti selezionati
              this.w_CODE = CODE
              * --- Insert into INF_AUQU
              i_nConn=i_TableProp[this.INF_AUQU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AUQU_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"QACODQUE"+",QAROWPRG"+",QAFLUTGR"+",QAUTEGRP"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CODICE),'INF_AUQU','QACODQUE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'INF_AUQU','QAROWPRG');
                +","+cp_NullLink(cp_ToStrODBC("U"),'INF_AUQU','QAFLUTGR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODE),'INF_AUQU','QAUTEGRP');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'QACODQUE',this.w_CODICE,'QAROWPRG',this.w_ROWNUM,'QAFLUTGR',"U",'QAUTEGRP',this.w_CODE)
                insert into (i_cTable) (QACODQUE,QAROWPRG,QAFLUTGR,QAUTEGRP &i_ccchkf. );
                   values (;
                     this.w_CODICE;
                     ,this.w_ROWNUM;
                     ,"U";
                     ,this.w_CODE;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              select (NC)
              continue
            enddo
            * --- Gruppi
            NC = this.oParentObject.w_ZOOMGRP.cCursor
            select (NC)
            locate for XCHK = 1
            do while not(eof())
              * --- Inserisco i gruppi selezionati
              this.w_CODE = CODE
              * --- Insert into INF_AUQU
              i_nConn=i_TableProp[this.INF_AUQU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AUQU_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"QACODQUE"+",QAROWPRG"+",QAFLUTGR"+",QAUTEGRP"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CODICE),'INF_AUQU','QACODQUE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'INF_AUQU','QAROWPRG');
                +","+cp_NullLink(cp_ToStrODBC("G"),'INF_AUQU','QAFLUTGR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODE),'INF_AUQU','QAUTEGRP');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'QACODQUE',this.w_CODICE,'QAROWPRG',this.w_ROWNUM,'QAFLUTGR',"G",'QAUTEGRP',this.w_CODE)
                insert into (i_cTable) (QACODQUE,QAROWPRG,QAFLUTGR,QAUTEGRP &i_ccchkf. );
                   values (;
                     this.w_CODICE;
                     ,this.w_ROWNUM;
                     ,"G";
                     ,this.w_CODE;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              select (NC)
              continue
            enddo
            This.oParentObject.NotifyEvent("GRicerca")
            * --- Aggiorno Tabella Vista
            do GSIR_BTV with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
      case this.pPAGE = "2"
        * --- Lavoro su Pagina 2
        this.w_CODICE = this.oParentObject.w_ZOOMQUE.getvar("AQCODICE")
        this.w_FLUTGR = this.oParentObject.w_QAFLUTGR
        this.w_UTEGRP = this.oParentObject.w_QAUTEGRP
        do case
          case this.pOPER = "A"
            * --- Contrassegno le righe dello zoom che sono presenti in INF_AUQU
            VQ_EXEC("..\IRDR\EXE\QUERY\GSIR_BQA", This, "CURAPPO", "", .F. , .T.)
            * --- Utenti
            NC = this.oParentObject.w_ZOOMGEST2.cCursor
            SELECT (NC)
            UPDATE (NC) SET XCHK = 0 WHERE 1 = 1
            UPDATE (NC) SET XCHK = 1 WHERE CPROWNUM IN (SELECT QAROWPRG FROM CURAPPO)
            * --- Chiudo il cursore
            if USED("CURAPPO")
              SELECT CURAPPO
              USE
            endif
          case this.pOPER = "U"
            * --- Cancello tutti i vecchi valori per poi inserire i nuovi
            * --- Delete from INF_AUQU
            i_nConn=i_TableProp[this.INF_AUQU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"QACODQUE = "+cp_ToStrODBC(this.w_CODICE);
                    +" and QAFLUTGR = "+cp_ToStrODBC(this.w_FLUTGR);
                    +" and QAUTEGRP = "+cp_ToStrODBC(this.w_UTEGRP);
                     )
            else
              delete from (i_cTable) where;
                    QACODQUE = this.w_CODICE;
                    and QAFLUTGR = this.w_FLUTGR;
                    and QAUTEGRP = this.w_UTEGRP;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Utenti
            NC = this.oParentObject.w_ZOOMGEST2.cCursor
            select (NC)
            locate for XCHK = 1
            do while not(eof())
              * --- Inserisco le gestioni selezionate
              this.w_CODE = CPROWNUM
              * --- Insert into INF_AUQU
              i_nConn=i_TableProp[this.INF_AUQU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AUQU_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"QACODQUE"+",QAROWPRG"+",QAFLUTGR"+",QAUTEGRP"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CODICE),'INF_AUQU','QACODQUE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODE),'INF_AUQU','QAROWPRG');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAFLUTGR),'INF_AUQU','QAFLUTGR');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAUTEGRP),'INF_AUQU','QAUTEGRP');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'QACODQUE',this.w_CODICE,'QAROWPRG',this.w_CODE,'QAFLUTGR',this.oParentObject.w_QAFLUTGR,'QAUTEGRP',this.oParentObject.w_QAUTEGRP)
                insert into (i_cTable) (QACODQUE,QAROWPRG,QAFLUTGR,QAUTEGRP &i_ccchkf. );
                   values (;
                     this.w_CODICE;
                     ,this.w_CODE;
                     ,this.oParentObject.w_QAFLUTGR;
                     ,this.oParentObject.w_QAUTEGRP;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              select (NC)
              continue
            enddo
            * --- Se ho attivato il check w_SELEALL devo salvare l'utente/gruppo corrente
            *     su tutte le gestioni generiche di tutte le query
            if this.oParentObject.w_SELEALL = "S"
              * --- Select from GSIRQKQA
              do vq_exec with 'GSIRQKQA',this,'_Curs_GSIRQKQA','',.f.,.t.
              if used('_Curs_GSIRQKQA')
                select _Curs_GSIRQKQA
                locate for 1=1
                do while not(eof())
                this.w_CODICE = AQCODICE
                * --- Provo ad inserire l'autorizzazione generica a tutte le query, se fallisce l'insert
                *     non faccio nulla
                * --- Try
                local bErr_04A2D630
                bErr_04A2D630=bTrsErr
                this.Try_04A2D630()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_04A2D630
                * --- End
                  select _Curs_GSIRQKQA
                  continue
                enddo
                use
              endif
            endif
            This.oParentObject.NotifyEvent("QRicerca")
            * --- Aggiorno Tabella Vista
            do GSIR_BTV with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
    endcase
  endproc
  proc Try_04A2D630()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AUQU
    i_nConn=i_TableProp[this.INF_AUQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AUQU_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"QACODQUE"+",QAROWPRG"+",QAFLUTGR"+",QAUTEGRP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'INF_AUQU','QACODQUE');
      +","+cp_NullLink(cp_ToStrODBC(-1),'INF_AUQU','QAROWPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLUTGR),'INF_AUQU','QAFLUTGR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTEGRP),'INF_AUQU','QAUTEGRP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'QACODQUE',this.w_CODICE,'QAROWPRG',-1,'QAFLUTGR',this.w_FLUTGR,'QAUTEGRP',this.w_UTEGRP)
      insert into (i_cTable) (QACODQUE,QAROWPRG,QAFLUTGR,QAUTEGRP &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,-1;
           ,this.w_FLUTGR;
           ,this.w_UTEGRP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOPER,pPAGE)
    this.pOPER=pOPER
    this.pPAGE=pPAGE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INF_AUQU'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSIRQKQA')
      use in _Curs_GSIRQKQA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pPAGE"
endproc
