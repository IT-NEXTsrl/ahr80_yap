* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_mpf                                                        *
*              Parametri filtri                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-29                                                      *
* Last revis.: 2011-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsir_mpf")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsir_mpf")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsir_mpf")
  return

* --- Class definition
define class tgsir_mpf as StdPCForm
  Width  = 505
  Height = 271
  Top    = 10
  Left   = 10
  cComment = "Parametri filtri"
  cPrg = "gsir_mpf"
  HelpContextID=147418985
  add object cnt as tcgsir_mpf
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsir_mpf as PCContext
  w_PFCODQUE = space(20)
  w_CPROWORD = 0
  w_PFVARNAM = space(20)
  w_PFDESVAR = space(50)
  w_PFTIPVAR = space(1)
  w_TIPVAR = space(10)
  w_PFLENVAR = 0
  w_PFDECVAR = 0
  proc Save(i_oFrom)
    this.w_PFCODQUE = i_oFrom.w_PFCODQUE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_PFVARNAM = i_oFrom.w_PFVARNAM
    this.w_PFDESVAR = i_oFrom.w_PFDESVAR
    this.w_PFTIPVAR = i_oFrom.w_PFTIPVAR
    this.w_TIPVAR = i_oFrom.w_TIPVAR
    this.w_PFLENVAR = i_oFrom.w_PFLENVAR
    this.w_PFDECVAR = i_oFrom.w_PFDECVAR
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PFCODQUE = this.w_PFCODQUE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_PFVARNAM = this.w_PFVARNAM
    i_oTo.w_PFDESVAR = this.w_PFDESVAR
    i_oTo.w_PFTIPVAR = this.w_PFTIPVAR
    i_oTo.w_TIPVAR = this.w_TIPVAR
    i_oTo.w_PFLENVAR = this.w_PFLENVAR
    i_oTo.w_PFDECVAR = this.w_PFDECVAR
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsir_mpf as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 505
  Height = 271
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-03"
  HelpContextID=147418985
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  INFPARFI_IDX = 0
  cFile = "INFPARFI"
  cKeySelect = "PFCODQUE"
  cKeyWhere  = "PFCODQUE=this.w_PFCODQUE"
  cKeyDetail  = "PFCODQUE=this.w_PFCODQUE"
  cKeyWhereODBC = '"PFCODQUE="+cp_ToStrODBC(this.w_PFCODQUE)';

  cKeyDetailWhereODBC = '"PFCODQUE="+cp_ToStrODBC(this.w_PFCODQUE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"INFPARFI.PFCODQUE="+cp_ToStrODBC(this.w_PFCODQUE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'INFPARFI.CPROWORD '
  cPrg = "gsir_mpf"
  cComment = "Parametri filtri"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PFCODQUE = space(20)
  w_CPROWORD = 0
  w_PFVARNAM = space(20)
  w_PFDESVAR = space(50)
  w_PFTIPVAR = space(1)
  o_PFTIPVAR = space(1)
  w_TIPVAR = space(10)
  o_TIPVAR = space(10)
  w_PFLENVAR = 0
  w_PFDECVAR = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_mpfPag1","gsir_mpf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='INFPARFI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INFPARFI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INFPARFI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsir_mpf'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from INFPARFI where PFCODQUE=KeySet.PFCODQUE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.INFPARFI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INFPARFI_IDX,2],this.bLoadRecFilter,this.INFPARFI_IDX,"gsir_mpf")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INFPARFI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INFPARFI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INFPARFI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PFCODQUE',this.w_PFCODQUE  )
      select * from (i_cTable) INFPARFI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PFCODQUE = NVL(PFCODQUE,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'INFPARFI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PFVARNAM = NVL(PFVARNAM,space(20))
          .w_PFDESVAR = NVL(PFDESVAR,space(50))
          .w_PFTIPVAR = NVL(PFTIPVAR,space(1))
        .w_TIPVAR = .w_PFTIPVAR
          .w_PFLENVAR = NVL(PFLENVAR,0)
          .w_PFDECVAR = NVL(PFDECVAR,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PFCODQUE=space(20)
      .w_CPROWORD=10
      .w_PFVARNAM=space(20)
      .w_PFDESVAR=space(50)
      .w_PFTIPVAR=space(1)
      .w_TIPVAR=space(10)
      .w_PFLENVAR=0
      .w_PFDECVAR=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,5,.f.)
        .w_TIPVAR = .w_PFTIPVAR
        .DoRTCalc(7,7,.f.)
        .w_PFDECVAR = IIF(.w_PFTIPVAR <> 'N', 0, .w_PFDECVAR)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INFPARFI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_2.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'INFPARFI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INFPARFI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PFCODQUE,"PFCODQUE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_PFVARNAM C(20);
      ,t_PFDESVAR C(50);
      ,t_TIPVAR N(3);
      ,t_PFLENVAR N(3);
      ,t_PFDECVAR N(3);
      ,CPROWNUM N(10);
      ,t_PFTIPVAR C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsir_mpfbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPFVARNAM_2_2.controlsource=this.cTrsName+'.t_PFVARNAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPFDESVAR_2_3.controlsource=this.cTrsName+'.t_PFDESVAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTIPVAR_2_5.controlsource=this.cTrsName+'.t_TIPVAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPFLENVAR_2_6.controlsource=this.cTrsName+'.t_PFLENVAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPFDECVAR_2_7.controlsource=this.cTrsName+'.t_PFDECVAR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(49)
    this.AddVLine(136)
    this.AddVLine(321)
    this.AddVLine(402)
    this.AddVLine(442)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INFPARFI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INFPARFI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INFPARFI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INFPARFI_IDX,2])
      *
      * insert into INFPARFI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INFPARFI')
        i_extval=cp_InsertValODBCExtFlds(this,'INFPARFI')
        i_cFldBody=" "+;
                  "(PFCODQUE,CPROWORD,PFVARNAM,PFDESVAR,PFTIPVAR"+;
                  ",PFLENVAR,PFDECVAR,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PFCODQUE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_PFVARNAM)+","+cp_ToStrODBC(this.w_PFDESVAR)+","+cp_ToStrODBC(this.w_PFTIPVAR)+;
             ","+cp_ToStrODBC(this.w_PFLENVAR)+","+cp_ToStrODBC(this.w_PFDECVAR)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INFPARFI')
        i_extval=cp_InsertValVFPExtFlds(this,'INFPARFI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PFCODQUE',this.w_PFCODQUE)
        INSERT INTO (i_cTable) (;
                   PFCODQUE;
                  ,CPROWORD;
                  ,PFVARNAM;
                  ,PFDESVAR;
                  ,PFTIPVAR;
                  ,PFLENVAR;
                  ,PFDECVAR;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PFCODQUE;
                  ,this.w_CPROWORD;
                  ,this.w_PFVARNAM;
                  ,this.w_PFDESVAR;
                  ,this.w_PFTIPVAR;
                  ,this.w_PFLENVAR;
                  ,this.w_PFDECVAR;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.INFPARFI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INFPARFI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) AND not (Empty(t_PFVARNAM)) AND not Empty(t_PFTIPVAR) ) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'INFPARFI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'INFPARFI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND not (Empty(t_PFVARNAM)) AND not Empty(t_PFTIPVAR) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INFPARFI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'INFPARFI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PFVARNAM="+cp_ToStrODBC(this.w_PFVARNAM)+;
                     ",PFDESVAR="+cp_ToStrODBC(this.w_PFDESVAR)+;
                     ",PFTIPVAR="+cp_ToStrODBC(this.w_PFTIPVAR)+;
                     ",PFLENVAR="+cp_ToStrODBC(this.w_PFLENVAR)+;
                     ",PFDECVAR="+cp_ToStrODBC(this.w_PFDECVAR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'INFPARFI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,PFVARNAM=this.w_PFVARNAM;
                     ,PFDESVAR=this.w_PFDESVAR;
                     ,PFTIPVAR=this.w_PFTIPVAR;
                     ,PFLENVAR=this.w_PFLENVAR;
                     ,PFDECVAR=this.w_PFDECVAR;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INFPARFI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INFPARFI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND not (Empty(t_PFVARNAM)) AND not Empty(t_PFTIPVAR) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete INFPARFI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND not (Empty(t_PFVARNAM)) AND not Empty(t_PFTIPVAR) ) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INFPARFI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INFPARFI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_PFTIPVAR<>.w_PFTIPVAR
          .w_TIPVAR = .w_PFTIPVAR
        endif
        .DoRTCalc(7,7,.t.)
        if .o_PFTIPVAR<>.w_PFTIPVAR
          .w_PFDECVAR = IIF(.w_PFTIPVAR <> 'N', 0, .w_PFDECVAR)
        endif
        if .o_TIPVAR<>.w_TIPVAR
          .Calculate_EBPTATKYQS()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PFTIPVAR with this.w_PFTIPVAR
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_EBPTATKYQS()
    with this
          * --- Valorizzo pftipvar
          .w_PFTIPVAR = .w_TIPVAR
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPFDECVAR_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPFDECVAR_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFVARNAM_2_2.value==this.w_PFVARNAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFVARNAM_2_2.value=this.w_PFVARNAM
      replace t_PFVARNAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFVARNAM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFDESVAR_2_3.value==this.w_PFDESVAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFDESVAR_2_3.value=this.w_PFDESVAR
      replace t_PFDESVAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFDESVAR_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPVAR_2_5.RadioValue()==this.w_TIPVAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPVAR_2_5.SetRadio()
      replace t_TIPVAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPVAR_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFLENVAR_2_6.value==this.w_PFLENVAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFLENVAR_2_6.value=this.w_PFLENVAR
      replace t_PFLENVAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFLENVAR_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFDECVAR_2_7.value==this.w_PFDECVAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFDECVAR_2_7.value=this.w_PFDECVAR
      replace t_PFDECVAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPFDECVAR_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'INFPARFI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD)) AND not (Empty(.w_PFVARNAM)) AND not Empty(.w_PFTIPVAR) 
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PFTIPVAR = this.w_PFTIPVAR
    this.o_TIPVAR = this.w_TIPVAR
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND not (Empty(t_PFVARNAM)) AND not Empty(t_PFTIPVAR) )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_PFVARNAM=space(20)
      .w_PFDESVAR=space(50)
      .w_PFTIPVAR=space(1)
      .w_TIPVAR=space(10)
      .w_PFLENVAR=0
      .w_PFDECVAR=0
      .DoRTCalc(1,5,.f.)
        .w_TIPVAR = .w_PFTIPVAR
      .DoRTCalc(7,7,.f.)
        .w_PFDECVAR = IIF(.w_PFTIPVAR <> 'N', 0, .w_PFDECVAR)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_PFVARNAM = t_PFVARNAM
    this.w_PFDESVAR = t_PFDESVAR
    this.w_PFTIPVAR = t_PFTIPVAR
    this.w_TIPVAR = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPVAR_2_5.RadioValue(.t.)
    this.w_PFLENVAR = t_PFLENVAR
    this.w_PFDECVAR = t_PFDECVAR
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PFVARNAM with this.w_PFVARNAM
    replace t_PFDESVAR with this.w_PFDESVAR
    replace t_PFTIPVAR with this.w_PFTIPVAR
    replace t_TIPVAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPVAR_2_5.ToRadio()
    replace t_PFLENVAR with this.w_PFLENVAR
    replace t_PFDECVAR with this.w_PFDECVAR
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsir_mpfPag1 as StdContainer
  Width  = 501
  height = 271
  stdWidth  = 501
  stdheight = 271
  resizeXpos=248
  resizeYpos=196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_2 as StdButton with uid="RIHNVYPWOK",left=440, top=221, width=48,height=45,;
    CpPicture="bmp\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare le variabili delle gestioni aperte";
    , HelpContextID = 267641991;
    , tabstop=.f., caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      do GSIR_KZG with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_2.mCond()
    with this.Parent.oContained
      return (INLIST(.cFunction, 'Edit', 'Load'))
    endwith
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=14, top=4, width=482,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="PFVARNAM",Label2="Nome",Field3="PFDESVAR",Label3="Descrizione",Field4="TIPVAR",Label4="Tipo",Field5="PFLENVAR",Label5="Lung.",Field6="PFDECVAR",Label6="Dec.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168671866

  add object oStr_1_3 as StdString with uid="DGQFUHPHCK",Visible=.t., Left=8, Top=332,;
    Alignment=1, Width=84, Height=19,;
    Caption="ATTENZIONE:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="NLREYXRPQG",Visible=.t., Left=-4, Top=356,;
    Alignment=0, Width=763, Height=18,;
    Caption="Se si modifica la dimensione della finestra occorre modificare la ridefinizione della classe all'interno della manual block function_procedure"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=4,top=23,;
    width=478+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=5,top=24,width=477+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsir_mpfBodyRow as CPBodyRowCnt
  Width=468
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="BSOMRXZZBE",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 201000298,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=33, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oPFVARNAM_2_2 as StdTrsField with uid="UDSQHHBMFC",rtseq=3,rtrep=.t.,;
    cFormVar="w_PFVARNAM",value=space(20),;
    ToolTipText = "Nome parametro",;
    HelpContextID = 178076739,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=35, Top=0, InputMask=replicate('X',20)

  add object oPFDESVAR_2_3 as StdTrsField with uid="PPBEDOFSYK",rtseq=4,rtrep=.t.,;
    cFormVar="w_PFDESVAR",value=space(50),;
    ToolTipText = "Descrizione parametro",;
    HelpContextID = 45096008,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=181, Left=122, Top=0, InputMask=replicate('X',50)

  add object oTIPVAR_2_5 as StdTrsCombo with uid="IZLWDJCNKJ",rtrep=.t.,;
    cFormVar="w_TIPVAR", RowSource=""+"Char,"+"Numeric,"+"Date,"+"Logic,"+"Memo,"+"Datetime" , ;
    HelpContextID = 228712246,;
    Height=21, Width=75, Left=307, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTIPVAR_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPVAR,&i_cF..t_TIPVAR),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'N',;
    iif(xVal =3,'D',;
    iif(xVal =4,'L',;
    iif(xVal =5,'M',;
    iif(xVal =6,'T',;
    space(10))))))))
  endfunc
  func oTIPVAR_2_5.GetRadio()
    this.Parent.oContained.w_TIPVAR = this.RadioValue()
    return .t.
  endfunc

  func oTIPVAR_2_5.ToRadio()
    this.Parent.oContained.w_TIPVAR=trim(this.Parent.oContained.w_TIPVAR)
    return(;
      iif(this.Parent.oContained.w_TIPVAR=='C',1,;
      iif(this.Parent.oContained.w_TIPVAR=='N',2,;
      iif(this.Parent.oContained.w_TIPVAR=='D',3,;
      iif(this.Parent.oContained.w_TIPVAR=='L',4,;
      iif(this.Parent.oContained.w_TIPVAR=='M',5,;
      iif(this.Parent.oContained.w_TIPVAR=='T',6,;
      0)))))))
  endfunc

  func oTIPVAR_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPFLENVAR_2_6 as StdTrsField with uid="YWNQHEUKXP",rtseq=7,rtrep=.t.,;
    cFormVar="w_PFLENVAR",value=0,;
    ToolTipText = "Lunghezza parametro",;
    HelpContextID = 39885896,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=390, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oPFDECVAR_2_7 as StdTrsField with uid="FQHAOPJTVI",rtseq=8,rtrep=.t.,;
    cFormVar="w_PFDECVAR",value=0,;
    ToolTipText = "Decimali parametro",;
    HelpContextID = 28318792,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=429, Top=0, cSayPict=["999"], cGetPict=["999"]

  func oPFDECVAR_2_7.mCond()
    with this.Parent.oContained
      return (.w_PFTIPVAR = 'N')
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_mpf','INFPARFI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PFCODQUE=INFPARFI.PFCODQUE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
