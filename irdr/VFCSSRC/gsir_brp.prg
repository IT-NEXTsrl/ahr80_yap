* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_brp                                                        *
*              Corporate da gestione documentale                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-23                                                      *
* Last revis.: 2005-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPERAZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_brp",oParentObject,m.pOPERAZ)
return(i_retval)

define class tgsir_brp as StdBatch
  * --- Local variables
  pOPERAZ = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine invocata da GSDM_MDV
    do case
      case this.pOPERAZ="P" 
        if this.oParentObject.w_DERISERV="N"
          ah_errormsg("Attenzione: i permessi sotto specificati non saranno presi in considerazione",48)
        endif
      case this.pOPERAZ="A" 
        if this.oParentObject.w_AQFLGZCP = "N"
          this.oParentObject.oPgFrm.pages(4).enabled=.f.
          this.oParentObject.Refresh
        else
          this.oParentObject.oPgFrm.pages(4).enabled=.t.
          this.oParentObject.Refresh
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPERAZ)
    this.pOPERAZ=pOPERAZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPERAZ"
endproc
