* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bsc                                                        *
*              Carica InfoPublisher                                            *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-23                                                      *
* Last revis.: 2016-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bsc",oParentObject,m.w_PARAM)
return(i_retval)

define class tgsir_bsc as StdBatch
  * --- Local variables
  w_PARAM = space(1)
  * --- WorkFile variables
  INF_GRQM_idx=0
  INF_GRQD_idx=0
  INF_AQM_idx=0
  INF_AQD_idx=0
  INF_AUGM_idx=0
  INF_AGIM_idx=0
  INF_AFIM_idx=0
  INF_PZCP_idx=0
  INF_AUQU_idx=0
  INFPARFI_idx=0
  IRDRMENU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Salva dati: Query e Gruppi di query InfoPublisher (da GSUT_BCO)
    * --- Try
    local bErr_026BB3E8
    bErr_026BB3E8=bTrsErr
    this.Try_026BB3E8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_ERRORGRU = .t.
    endif
    bTrsErr=bTrsErr or bErr_026BB3E8
    * --- End
  endproc
  proc Try_026BB3E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_PARAM = "A"
      * --- Carica anagrafica query
      * --- --
      * --- --
      * --- --
      * --- --
      * --- --
      * --- --
      * --- --
      if this.oParentObject.w_FLMD="M"
        * --- Inserisco Master
        * --- Try
        local bErr_026BAD28
        bErr_026BAD28=bTrsErr
        this.Try_026BAD28()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          if this.oParentObject.w_AGG
            * --- Write into INF_AQM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.INF_AQM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_AQM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AQM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AQDES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQDES),'INF_AQM','AQDES');
              +",AQQRPATH ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQQRPATH),'INF_AQM','AQQRPATH');
              +",AQDTFILE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQDTFILE),'INF_AQM','AQDTFILE');
              +",AQIMPATH ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQIMPATH),'INF_AQM','AQIMPATH');
              +",AQLOG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQLOG),'INF_AQM','AQLOG');
              +",AQADOCS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQADOCS),'INF_AQM','AQADOCS');
              +",AQFLGZCP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFLGZCP),'INF_AQM','AQFLGZCP');
              +",AQFORTYP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFORTYP),'INF_AQM','AQFORTYP');
              +",AQCONTYP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCONTYP),'INF_AQM','AQCONTYP');
              +",AQCURLOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCURLOC),'INF_AQM','AQCURLOC');
              +",AQCURTYP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCURTYP),'INF_AQM','AQCURTYP');
              +",AQENGTYP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQENGTYP),'INF_AQM','AQENGTYP');
              +",AQTIMOUT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQTIMOUT),'INF_AQM','AQTIMOUT');
              +",AQCMDTMO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCMDTMO),'INF_AQM','AQCMDTMO');
              +",AQQUEFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQQUEFIL),'INF_AQM','AQQUEFIL');
              +",AQCRIPTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCRIPTE),'INF_AQM','AQCRIPTE');
              +",AQFLJOIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFLJOIN),'INF_AQM','AQFLJOIN');
                  +i_ccchkf ;
              +" where ";
                  +"AQCODICE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                     )
            else
              update (i_cTable) set;
                  AQDES = this.oParentObject.w_AQDES;
                  ,AQQRPATH = this.oParentObject.w_AQQRPATH;
                  ,AQDTFILE = this.oParentObject.w_AQDTFILE;
                  ,AQIMPATH = this.oParentObject.w_AQIMPATH;
                  ,AQLOG = this.oParentObject.w_AQLOG;
                  ,AQADOCS = this.oParentObject.w_AQADOCS;
                  ,AQFLGZCP = this.oParentObject.w_AQFLGZCP;
                  ,AQFORTYP = this.oParentObject.w_AQFORTYP;
                  ,AQCONTYP = this.oParentObject.w_AQCONTYP;
                  ,AQCURLOC = this.oParentObject.w_AQCURLOC;
                  ,AQCURTYP = this.oParentObject.w_AQCURTYP;
                  ,AQENGTYP = this.oParentObject.w_AQENGTYP;
                  ,AQTIMOUT = this.oParentObject.w_AQTIMOUT;
                  ,AQCMDTMO = this.oParentObject.w_AQCMDTMO;
                  ,AQQUEFIL = this.oParentObject.w_AQQUEFIL;
                  ,AQCRIPTE = this.oParentObject.w_AQCRIPTE;
                  ,AQFLJOIN = this.oParentObject.w_AQFLJOIN;
                  &i_ccchkf. ;
               where;
                  AQCODICE = this.oParentObject.w_AQCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore inserimento query'
              return
            endif
          endif
        endif
        bTrsErr=bTrsErr or bErr_026BAD28
        * --- End
        if not(Empty(this.oParentObject.w_AQMAILR))
          * --- Inserisco Detail
          * --- Try
          local bErr_03B24C10
          bErr_03B24C10=bTrsErr
          this.Try_03B24C10()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_AQD
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_AQD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AQD_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AQD_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQROWORD),'INF_AQD','CPROWORD');
                +",AQUTENTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQUTENTE),'INF_AQD','AQUTENTE');
                +",AQFLUE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFLUE),'INF_AQD','AQFLUE');
                    +i_ccchkf ;
                +" where ";
                    +"AQCODICE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and AQMAILR = "+cp_ToStrODBC(this.oParentObject.w_AQMAILR);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_AQROWORD;
                    ,AQUTENTE = this.oParentObject.w_AQUTENTE;
                    ,AQFLUE = this.oParentObject.w_AQFLUE;
                    &i_ccchkf. ;
                 where;
                    AQCODICE = this.oParentObject.w_AQCODICE;
                    and AQMAILR = this.oParentObject.w_AQMAILR;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B24C10
          * --- End
        endif
        * --- Inserisco - Autorizzazioni Utenti/Gruppi
        if not(Empty(this.oParentObject.w_QAROWPRG)) AND this.oParentObject.w_QAUTEGRP<>0
          * --- Try
          local bErr_03B24D00
          bErr_03B24D00=bTrsErr
          this.Try_03B24D00()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Delete from INF_AUQU
              i_nConn=i_TableProp[this.INF_AUQU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"QACODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                      +" and QAUTEGRP = "+cp_ToStrODBC(this.oParentObject.w_QAUTEGRP);
                      +" and QAROWPRG = "+cp_ToStrODBC(this.oParentObject.w_QAROWPRG);
                      +" and QAFLUTGR = "+cp_ToStrODBC(this.oParentObject.w_QAFLUTGR);
                       )
              else
                delete from (i_cTable) where;
                      QACODQUE = this.oParentObject.w_AQCODICE;
                      and QAUTEGRP = this.oParentObject.w_QAUTEGRP;
                      and QAROWPRG = this.oParentObject.w_QAROWPRG;
                      and QAFLUTGR = this.oParentObject.w_QAFLUTGR;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Insert into INF_AUQU
              i_nConn=i_TableProp[this.INF_AUQU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AUQU_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"QACODQUE"+",QAROWPRG"+",QAFLUTGR"+",QAUTEGRP"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AUQU','QACODQUE');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAROWPRG),'INF_AUQU','QAROWPRG');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAFLUTGR),'INF_AUQU','QAFLUTGR');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAUTEGRP),'INF_AUQU','QAUTEGRP');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'QACODQUE',this.oParentObject.w_AQCODICE,'QAROWPRG',this.oParentObject.w_QAROWPRG,'QAFLUTGR',this.oParentObject.w_QAFLUTGR,'QAUTEGRP',this.oParentObject.w_QAUTEGRP)
                insert into (i_cTable) (QACODQUE,QAROWPRG,QAFLUTGR,QAUTEGRP &i_ccchkf. );
                   values (;
                     this.oParentObject.w_AQCODICE;
                     ,this.oParentObject.w_QAROWPRG;
                     ,this.oParentObject.w_QAFLUTGR;
                     ,this.oParentObject.w_QAUTEGRP;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B24D00
          * --- End
        endif
        * --- Inserisco Menu
        if NOT EMPTY(NVL(this.oParentObject.w_GI__MENU,""))
          * --- Try
          local bErr_03B24370
          bErr_03B24370=bTrsErr
          this.Try_03B24370()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into IRDRMENU
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.IRDRMENU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.IRDRMENU_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.IRDRMENU_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MG__MENU ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MG__MENU),'IRDRMENU','MG__MENU');
                    +i_ccchkf ;
                +" where ";
                    +"MGSERIAL = "+cp_ToStrODBC(this.oParentObject.w_GI__MENU);
                       )
              else
                update (i_cTable) set;
                    MG__MENU = this.oParentObject.w_MG__MENU;
                    &i_ccchkf. ;
                 where;
                    MGSERIAL = this.oParentObject.w_GI__MENU;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B24370
          * --- End
        endif
        * --- Inserisco Detail - Gestioni
        if not(Empty(this.oParentObject.w_GIROWORD)) AND not(Empty(this.oParentObject.w_GIPROGRA))
          * --- Try
          local bErr_03B236B0
          bErr_03B236B0=bTrsErr
          this.Try_03B236B0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_AGIM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_AGIM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AGIM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AGIM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIROWORD),'INF_AGIM','CPROWORD');
                +",GIPROGRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIPROGRA),'INF_AGIM','GIPROGRA');
                +",GIPARAM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIPARAM),'INF_AGIM','GIPARAM');
                +",GITABNAM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GITABNAM),'INF_AGIM','GITABNAM');
                +",GIDESC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIDESC),'INF_AGIM','GIDESC');
                +",GIDESGES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIDESGES),'INF_AGIM','GIDESGES');
                +",GI__MENU ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GI__MENU),'INF_AGIM','GI__MENU');
                    +i_ccchkf ;
                +" where ";
                    +"GICODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_GIROWNUM);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_GIROWORD;
                    ,GIPROGRA = this.oParentObject.w_GIPROGRA;
                    ,GIPARAM = this.oParentObject.w_GIPARAM;
                    ,GITABNAM = this.oParentObject.w_GITABNAM;
                    ,GIDESC = this.oParentObject.w_GIDESC;
                    ,GIDESGES = this.oParentObject.w_GIDESGES;
                    ,GI__MENU = this.oParentObject.w_GI__MENU;
                    &i_ccchkf. ;
                 where;
                    GICODQUE = this.oParentObject.w_AQCODICE;
                    and CPROWNUM = this.oParentObject.w_GIROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B236B0
          * --- End
        endif
        * --- Inserisco Detail - Filtri
        if not(Empty(this.oParentObject.w_FIROWORD)) AND not(Empty(this.oParentObject.w_FIPRGRN))
          * --- Try
          local bErr_03B23CB0
          bErr_03B23CB0=bTrsErr
          this.Try_03B23CB0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_AFIM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_AFIM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AFIM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWORD),'INF_AFIM','CPROWORD');
                +",FIFLNAME ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLNAME),'INF_AFIM','FIFLNAME');
                +",FITIPFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FITIPFIL),'INF_AFIM','FITIPFIL');
                +",FIFILTRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFILTRO),'INF_AFIM','FIFILTRO');
                +",FIFLGREM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGREM),'INF_AFIM','FIFLGREM');
                +",FIFLGAZI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGAZI),'INF_AFIM','FIFLGAZI');
                +",FIVISMSK ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIVISMSK),'INF_AFIM','FIVISMSK');
                    +i_ccchkf ;
                +" where ";
                    +"FICODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and FIPRGRN = "+cp_ToStrODBC(this.oParentObject.w_FIPRGRN);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_FIROWNUM);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_FIROWORD;
                    ,FIFLNAME = this.oParentObject.w_FIFLNAME;
                    ,FITIPFIL = this.oParentObject.w_FITIPFIL;
                    ,FIFILTRO = this.oParentObject.w_FIFILTRO;
                    ,FIFLGREM = this.oParentObject.w_FIFLGREM;
                    ,FIFLGAZI = this.oParentObject.w_FIFLGAZI;
                    ,FIVISMSK = this.oParentObject.w_FIVISMSK;
                    &i_ccchkf. ;
                 where;
                    FICODQUE = this.oParentObject.w_AQCODICE;
                    and FIPRGRN = this.oParentObject.w_FIPRGRN;
                    and CPROWNUM = this.oParentObject.w_FIROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B23CB0
          * --- End
        endif
        * --- Inserisco Detail - Filtri Generici
        if not(Empty(this.oParentObject.w_FIROWORD1)) 
          * --- Try
          local bErr_026B9D08
          bErr_026B9D08=bTrsErr
          this.Try_026B9D08()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_AFIM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_AFIM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AFIM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWORD1),'INF_AFIM','CPROWORD');
                +",FIFLNAME ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLNAME1),'INF_AFIM','FIFLNAME');
                +",FITIPFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FITIPFIL1),'INF_AFIM','FITIPFIL');
                +",FIFILTRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFILTRO1),'INF_AFIM','FIFILTRO');
                +",FIFLGREM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGREM1),'INF_AFIM','FIFLGREM');
                +",FIFLGAZI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGAZI1),'INF_AFIM','FIFLGAZI');
                +",FIVISMSK ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIVISMSK1),'INF_AFIM','FIVISMSK');
                    +i_ccchkf ;
                +" where ";
                    +"FICODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and FIPRGRN = "+cp_ToStrODBC(-1);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_FIROWNUM1);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_FIROWORD1;
                    ,FIFLNAME = this.oParentObject.w_FIFLNAME1;
                    ,FITIPFIL = this.oParentObject.w_FITIPFIL1;
                    ,FIFILTRO = this.oParentObject.w_FIFILTRO1;
                    ,FIFLGREM = this.oParentObject.w_FIFLGREM1;
                    ,FIFLGAZI = this.oParentObject.w_FIFLGAZI1;
                    ,FIVISMSK = this.oParentObject.w_FIVISMSK1;
                    &i_ccchkf. ;
                 where;
                    FICODQUE = this.oParentObject.w_AQCODICE;
                    and FIPRGRN = -1;
                    and CPROWNUM = this.oParentObject.w_FIROWNUM1;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_026B9D08
          * --- End
        endif
        * --- Inserisco Detail - Parametri Filtri
        if nvl(this.oParentObject.w_PFROWNUM,0)<>0
          * --- Try
          local bErr_026BBCB8
          bErr_026BBCB8=bTrsErr
          this.Try_026BBCB8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INFPARFI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INFPARFI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INFPARFI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INFPARFI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFROWORD),'INFPARFI','CPROWORD');
                +",PFTIPVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFTIPVAR),'INFPARFI','PFTIPVAR');
                +",PFVARNAM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFVARNAM),'INFPARFI','PFVARNAM');
                +",PFDESVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFDESVAR),'INFPARFI','PFDESVAR');
                +",PFLENVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFLENVAR),'INFPARFI','PFLENVAR');
                +",PFDECVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFDECVAR),'INFPARFI','PFDECVAR');
                    +i_ccchkf ;
                +" where ";
                    +"PFCODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_PFROWNUM);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_PFROWORD;
                    ,PFTIPVAR = this.oParentObject.w_PFTIPVAR;
                    ,PFVARNAM = this.oParentObject.w_PFVARNAM;
                    ,PFDESVAR = this.oParentObject.w_PFDESVAR;
                    ,PFLENVAR = this.oParentObject.w_PFLENVAR;
                    ,PFDECVAR = this.oParentObject.w_PFDECVAR;
                    &i_ccchkf. ;
                 where;
                    PFCODQUE = this.oParentObject.w_AQCODICE;
                    and CPROWNUM = this.oParentObject.w_PFROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_026BBCB8
          * --- End
        endif
        * --- Inserisco Detail - CoporatePortal
        if not(Empty(this.oParentObject.w_DEROWORD)) AND not(Empty(this.oParentObject.w_DEDESFIL))
          * --- Try
          local bErr_03B22B10
          bErr_03B22B10=bTrsErr
          this.Try_03B22B10()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_PZCP
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_PZCP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_PZCP_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_PZCP_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEROWORD),'INF_PZCP','CPROWORD');
                +",DERISERV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DERISERV),'INF_PZCP','DERISERV');
                +",DEFOLDER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEFOLDER),'INF_PZCP','DEFOLDER');
                +",DEDESFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEDESFIL),'INF_PZCP','DEDESFIL');
                +",DE__NOTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE__NOTE),'INF_PZCP','DE__NOTE');
                +",DETIPDES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPDES),'INF_PZCP','DETIPDES');
                +",DECODGRU ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODGRU),'INF_PZCP','DECODGRU');
                +",DETIPCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPCON),'INF_PZCP','DETIPCON');
                +",DECODCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODCON),'INF_PZCP','DECODCON');
                +",DECODAGE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODAGE),'INF_PZCP','DECODAGE');
                +",DECODROL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODROL),'INF_PZCP','DECODROL');
                +",DEVALDES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEVALDES),'INF_PZCP','DEVALDES');
                +",DE__READ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE__READ),'INF_PZCP','DE__READ');
                +",DE_WRITE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE_WRITE),'INF_PZCP','DE_WRITE');
                +",DEDELETE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEDELETE),'INF_PZCP','DEDELETE');
                +",DERIFCFO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DERIFCFO),'INF_PZCP','DERIFCFO');
                    +i_ccchkf ;
                +" where ";
                    +"DECODICE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_DEROWNUM);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_DEROWORD;
                    ,DERISERV = this.oParentObject.w_DERISERV;
                    ,DEFOLDER = this.oParentObject.w_DEFOLDER;
                    ,DEDESFIL = this.oParentObject.w_DEDESFIL;
                    ,DE__NOTE = this.oParentObject.w_DE__NOTE;
                    ,DETIPDES = this.oParentObject.w_DETIPDES;
                    ,DECODGRU = this.oParentObject.w_DECODGRU;
                    ,DETIPCON = this.oParentObject.w_DETIPCON;
                    ,DECODCON = this.oParentObject.w_DECODCON;
                    ,DECODAGE = this.oParentObject.w_DECODAGE;
                    ,DECODROL = this.oParentObject.w_DECODROL;
                    ,DEVALDES = this.oParentObject.w_DEVALDES;
                    ,DE__READ = this.oParentObject.w_DE__READ;
                    ,DE_WRITE = this.oParentObject.w_DE_WRITE;
                    ,DEDELETE = this.oParentObject.w_DEDELETE;
                    ,DERIFCFO = this.oParentObject.w_DERIFCFO;
                    &i_ccchkf. ;
                 where;
                    DECODICE = this.oParentObject.w_AQCODICE;
                    and CPROWNUM = this.oParentObject.w_DEROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B22B10
          * --- End
        endif
        this.oParentObject.w_FLMD = "D"
      else
        * --- Inserisco Detail
        if not(Empty(this.oParentObject.w_AQMAILR))
          * --- Try
          local bErr_03B7E800
          bErr_03B7E800=bTrsErr
          this.Try_03B7E800()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_AQD
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_AQD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AQD_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AQD_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQROWORD),'INF_AQD','CPROWORD');
                +",AQUTENTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQUTENTE),'INF_AQD','AQUTENTE');
                +",AQFLUE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFLUE),'INF_AQD','AQFLUE');
                    +i_ccchkf ;
                +" where ";
                    +"AQCODICE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and AQMAILR = "+cp_ToStrODBC(this.oParentObject.w_AQMAILR);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_AQROWORD;
                    ,AQUTENTE = this.oParentObject.w_AQUTENTE;
                    ,AQFLUE = this.oParentObject.w_AQFLUE;
                    &i_ccchkf. ;
                 where;
                    AQCODICE = this.oParentObject.w_AQCODICE;
                    and AQMAILR = this.oParentObject.w_AQMAILR;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B7E800
          * --- End
        endif
        if not(Empty(this.oParentObject.w_QAROWPRG)) AND this.oParentObject.w_QAUTEGRP<>0
          * --- Try
          local bErr_03B7B470
          bErr_03B7B470=bTrsErr
          this.Try_03B7B470()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Delete from INF_AUQU
              i_nConn=i_TableProp[this.INF_AUQU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"QACODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                      +" and QAUTEGRP = "+cp_ToStrODBC(this.oParentObject.w_QAUTEGRP);
                      +" and QAROWPRG = "+cp_ToStrODBC(this.oParentObject.w_QAROWPRG);
                      +" and QAFLUTGR = "+cp_ToStrODBC(this.oParentObject.w_QAFLUTGR);
                       )
              else
                delete from (i_cTable) where;
                      QACODQUE = this.oParentObject.w_AQCODICE;
                      and QAUTEGRP = this.oParentObject.w_QAUTEGRP;
                      and QAROWPRG = this.oParentObject.w_QAROWPRG;
                      and QAFLUTGR = this.oParentObject.w_QAFLUTGR;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Insert into INF_AUQU
              i_nConn=i_TableProp[this.INF_AUQU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AUQU_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"QACODQUE"+",QAROWPRG"+",QAFLUTGR"+",QAUTEGRP"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AUQU','QACODQUE');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAROWPRG),'INF_AUQU','QAROWPRG');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAFLUTGR),'INF_AUQU','QAFLUTGR');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAUTEGRP),'INF_AUQU','QAUTEGRP');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'QACODQUE',this.oParentObject.w_AQCODICE,'QAROWPRG',this.oParentObject.w_QAROWPRG,'QAFLUTGR',this.oParentObject.w_QAFLUTGR,'QAUTEGRP',this.oParentObject.w_QAUTEGRP)
                insert into (i_cTable) (QACODQUE,QAROWPRG,QAFLUTGR,QAUTEGRP &i_ccchkf. );
                   values (;
                     this.oParentObject.w_AQCODICE;
                     ,this.oParentObject.w_QAROWPRG;
                     ,this.oParentObject.w_QAFLUTGR;
                     ,this.oParentObject.w_QAUTEGRP;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B7B470
          * --- End
        endif
        * --- Inserisco Detail - Gestioni
        if not(Empty(this.oParentObject.w_GIROWORD)) AND not(Empty(this.oParentObject.w_GIPROGRA))
          * --- Try
          local bErr_03B78920
          bErr_03B78920=bTrsErr
          this.Try_03B78920()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_AGIM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_AGIM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AGIM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AGIM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIROWORD),'INF_AGIM','CPROWORD');
                +",GIPROGRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIPROGRA),'INF_AGIM','GIPROGRA');
                +",GIPARAM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIPARAM),'INF_AGIM','GIPARAM');
                +",GITABNAM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GITABNAM),'INF_AGIM','GITABNAM');
                +",GIDESC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIDESC),'INF_AGIM','GIDESC');
                +",GI__MENU ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GI__MENU),'INF_AGIM','GI__MENU');
                    +i_ccchkf ;
                +" where ";
                    +"GICODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_GIROWNUM);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_GIROWORD;
                    ,GIPROGRA = this.oParentObject.w_GIPROGRA;
                    ,GIPARAM = this.oParentObject.w_GIPARAM;
                    ,GITABNAM = this.oParentObject.w_GITABNAM;
                    ,GIDESC = this.oParentObject.w_GIDESC;
                    ,GI__MENU = this.oParentObject.w_GI__MENU;
                    &i_ccchkf. ;
                 where;
                    GICODQUE = this.oParentObject.w_AQCODICE;
                    and CPROWNUM = this.oParentObject.w_GIROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B78920
          * --- End
        endif
        * --- Inserisco Detail - Filtri
        if not(Empty(this.oParentObject.w_FIROWORD)) AND not(Empty(this.oParentObject.w_FIPRGRN))
          * --- Try
          local bErr_03B6AD80
          bErr_03B6AD80=bTrsErr
          this.Try_03B6AD80()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_AFIM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_AFIM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AFIM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWORD),'INF_AFIM','CPROWORD');
                +",FIFLNAME ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLNAME),'INF_AFIM','FIFLNAME');
                +",FITIPFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FITIPFIL),'INF_AFIM','FITIPFIL');
                +",FIFILTRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFILTRO),'INF_AFIM','FIFILTRO');
                +",FIFLGREM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGREM),'INF_AFIM','FIFLGREM');
                +",FIFLGAZI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGAZI),'INF_AFIM','FIFLGAZI');
                +",FIVISMSK ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIVISMSK),'INF_AFIM','FIVISMSK');
                    +i_ccchkf ;
                +" where ";
                    +"FICODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and FIPRGRN = "+cp_ToStrODBC(this.oParentObject.w_FIPRGRN);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_FIROWNUM);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_FIROWORD;
                    ,FIFLNAME = this.oParentObject.w_FIFLNAME;
                    ,FITIPFIL = this.oParentObject.w_FITIPFIL;
                    ,FIFILTRO = this.oParentObject.w_FIFILTRO;
                    ,FIFLGREM = this.oParentObject.w_FIFLGREM;
                    ,FIFLGAZI = this.oParentObject.w_FIFLGAZI;
                    ,FIVISMSK = this.oParentObject.w_FIVISMSK;
                    &i_ccchkf. ;
                 where;
                    FICODQUE = this.oParentObject.w_AQCODICE;
                    and FIPRGRN = this.oParentObject.w_FIPRGRN;
                    and CPROWNUM = this.oParentObject.w_FIROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B6AD80
          * --- End
        endif
        * --- Inserisco Detail - Filtri Generici
        if not(Empty(this.oParentObject.w_FIROWORD1)) 
          * --- Try
          local bErr_03B68230
          bErr_03B68230=bTrsErr
          this.Try_03B68230()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_AFIM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_AFIM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AFIM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWORD1),'INF_AFIM','CPROWORD');
                +",FIFLNAME ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLNAME1),'INF_AFIM','FIFLNAME');
                +",FITIPFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FITIPFIL1),'INF_AFIM','FITIPFIL');
                +",FIFILTRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFILTRO1),'INF_AFIM','FIFILTRO');
                +",FIFLGREM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGREM1),'INF_AFIM','FIFLGREM');
                +",FIFLGAZI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGAZI1),'INF_AFIM','FIFLGAZI');
                +",FIVISMSK ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIVISMSK1),'INF_AFIM','FIVISMSK');
                    +i_ccchkf ;
                +" where ";
                    +"FICODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and FIPRGRN = "+cp_ToStrODBC(-1);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_FIROWNUM1);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_FIROWORD1;
                    ,FIFLNAME = this.oParentObject.w_FIFLNAME1;
                    ,FITIPFIL = this.oParentObject.w_FITIPFIL1;
                    ,FIFILTRO = this.oParentObject.w_FIFILTRO1;
                    ,FIFLGREM = this.oParentObject.w_FIFLGREM1;
                    ,FIFLGAZI = this.oParentObject.w_FIFLGAZI1;
                    ,FIVISMSK = this.oParentObject.w_FIVISMSK1;
                    &i_ccchkf. ;
                 where;
                    FICODQUE = this.oParentObject.w_AQCODICE;
                    and FIPRGRN = -1;
                    and CPROWNUM = this.oParentObject.w_FIROWNUM1;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B68230
          * --- End
        endif
        * --- Inserisco Detail - Parametri Filtri
        if nvl(this.oParentObject.w_PFROWNUM,0)<>0
          * --- Try
          local bErr_03B656E0
          bErr_03B656E0=bTrsErr
          this.Try_03B656E0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INFPARFI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INFPARFI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INFPARFI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INFPARFI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFROWORD),'INFPARFI','CPROWORD');
                +",PFTIPVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFTIPVAR),'INFPARFI','PFTIPVAR');
                +",PFVARNAM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFVARNAM),'INFPARFI','PFVARNAM');
                +",PFDESVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFDESVAR),'INFPARFI','PFDESVAR');
                +",PFLENVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFLENVAR),'INFPARFI','PFLENVAR');
                +",PFDECVAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFDECVAR),'INFPARFI','PFDECVAR');
                    +i_ccchkf ;
                +" where ";
                    +"PFCODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_PFROWNUM);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_PFROWORD;
                    ,PFTIPVAR = this.oParentObject.w_PFTIPVAR;
                    ,PFVARNAM = this.oParentObject.w_PFVARNAM;
                    ,PFDESVAR = this.oParentObject.w_PFDESVAR;
                    ,PFLENVAR = this.oParentObject.w_PFLENVAR;
                    ,PFDECVAR = this.oParentObject.w_PFDECVAR;
                    &i_ccchkf. ;
                 where;
                    PFCODQUE = this.oParentObject.w_AQCODICE;
                    and CPROWNUM = this.oParentObject.w_PFROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B656E0
          * --- End
        endif
        * --- Inserisco Detail - CoporatePortal
        if not(Empty(this.oParentObject.w_DEROWORD)) AND not(Empty(this.oParentObject.w_DEDESFIL))
          * --- Try
          local bErr_03B60B70
          bErr_03B60B70=bTrsErr
          this.Try_03B60B70()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_AGG
              * --- Write into INF_PZCP
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.INF_PZCP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INF_PZCP_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_PZCP_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEROWORD),'INF_PZCP','CPROWORD');
                +",DERISERV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DERISERV),'INF_PZCP','DERISERV');
                +",DEFOLDER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEFOLDER),'INF_PZCP','DEFOLDER');
                +",DEDESFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEDESFIL),'INF_PZCP','DEDESFIL');
                +",DE__NOTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE__NOTE),'INF_PZCP','DE__NOTE');
                +",DETIPDES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPDES),'INF_PZCP','DETIPDES');
                +",DECODGRU ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODGRU),'INF_PZCP','DECODGRU');
                +",DETIPCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPCON),'INF_PZCP','DETIPCON');
                +",DECODCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODCON),'INF_PZCP','DECODCON');
                +",DECODAGE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODAGE),'INF_PZCP','DECODAGE');
                +",DECODROL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODROL),'INF_PZCP','DECODROL');
                +",DEVALDES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEVALDES),'INF_PZCP','DEVALDES');
                +",DE__READ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE__READ),'INF_PZCP','DE__READ');
                +",DE_WRITE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE_WRITE),'INF_PZCP','DE_WRITE');
                +",DEDELETE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEDELETE),'INF_PZCP','DEDELETE');
                +",DERIFCFO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DERIFCFO),'INF_PZCP','DERIFCFO');
                    +i_ccchkf ;
                +" where ";
                    +"DECODICE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_DEROWNUM);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.oParentObject.w_DEROWORD;
                    ,DERISERV = this.oParentObject.w_DERISERV;
                    ,DEFOLDER = this.oParentObject.w_DEFOLDER;
                    ,DEDESFIL = this.oParentObject.w_DEDESFIL;
                    ,DE__NOTE = this.oParentObject.w_DE__NOTE;
                    ,DETIPDES = this.oParentObject.w_DETIPDES;
                    ,DECODGRU = this.oParentObject.w_DECODGRU;
                    ,DETIPCON = this.oParentObject.w_DETIPCON;
                    ,DECODCON = this.oParentObject.w_DECODCON;
                    ,DECODAGE = this.oParentObject.w_DECODAGE;
                    ,DECODROL = this.oParentObject.w_DECODROL;
                    ,DEVALDES = this.oParentObject.w_DEVALDES;
                    ,DE__READ = this.oParentObject.w_DE__READ;
                    ,DE_WRITE = this.oParentObject.w_DE_WRITE;
                    ,DEDELETE = this.oParentObject.w_DEDELETE;
                    ,DERIFCFO = this.oParentObject.w_DERIFCFO;
                    &i_ccchkf. ;
                 where;
                    DECODICE = this.oParentObject.w_AQCODICE;
                    and CPROWNUM = this.oParentObject.w_DEROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore inserimento query'
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_03B60B70
          * --- End
        endif
      endif
    else
      * --- Carica gruppi query
      if this.oParentObject.w_FLMD="M"
        * --- Inserisco Master
        * --- Try
        local bErr_03BC5590
        bErr_03BC5590=bTrsErr
        this.Try_03BC5590()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          if this.oParentObject.w_AGG
            * --- Write into INF_GRQM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.INF_GRQM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_GRQM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_GRQM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"GQDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQDESCRI),'INF_GRQM','GQDESCRI');
              +",GQDTINVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQDTINVA),'INF_GRQM','GQDTINVA');
              +",GQDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQDTOBSO),'INF_GRQM','GQDTOBSO');
                  +i_ccchkf ;
              +" where ";
                  +"GQCODICE = "+cp_ToStrODBC(this.oParentObject.w_GQCODICE);
                     )
            else
              update (i_cTable) set;
                  GQDESCRI = this.oParentObject.w_GQDESCRI;
                  ,GQDTINVA = this.oParentObject.w_GQDTINVA;
                  ,GQDTOBSO = this.oParentObject.w_GQDTOBSO;
                  &i_ccchkf. ;
               where;
                  GQCODICE = this.oParentObject.w_GQCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore inserimento gruppo'
              return
            endif
          endif
        endif
        bTrsErr=bTrsErr or bErr_03BC5590
        * --- End
        * --- Inserisco Detail
        * --- Try
        local bErr_03BC60A0
        bErr_03BC60A0=bTrsErr
        this.Try_03BC60A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          if this.oParentObject.w_AGG
            * --- Write into INF_GRQD
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.INF_GRQD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_GRQD_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_GRQD_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_GRQD','CPROWORD');
              +",GQCODQUE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQCODQUE),'INF_GRQD','GQCODQUE');
              +",GQNUMSEQ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQNUMSEQ),'INF_GRQD','GQNUMSEQ');
                  +i_ccchkf ;
              +" where ";
                  +"GQCODICE = "+cp_ToStrODBC(this.oParentObject.w_GQCODICE);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  CPROWORD = this.oParentObject.w_CPROWORD;
                  ,GQCODQUE = this.oParentObject.w_GQCODQUE;
                  ,GQNUMSEQ = this.oParentObject.w_GQNUMSEQ;
                  &i_ccchkf. ;
               where;
                  GQCODICE = this.oParentObject.w_GQCODICE;
                  and CPROWNUM = this.oParentObject.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore inserimento query'
              return
            endif
          endif
        endif
        bTrsErr=bTrsErr or bErr_03BC60A0
        * --- End
        this.oParentObject.w_FLMD = "D"
      else
        * --- Inserisco Detail
        * --- Try
        local bErr_03BCC6D0
        bErr_03BCC6D0=bTrsErr
        this.Try_03BCC6D0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          if this.oParentObject.w_AGG
            * --- Write into INF_GRQD
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.INF_GRQD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_GRQD_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_GRQD_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_GRQD','CPROWORD');
              +",GQCODQUE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQCODQUE),'INF_GRQD','GQCODQUE');
              +",GQNUMSEQ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQNUMSEQ),'INF_GRQD','GQNUMSEQ');
                  +i_ccchkf ;
              +" where ";
                  +"GQCODICE = "+cp_ToStrODBC(this.oParentObject.w_GQCODICE);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  CPROWORD = this.oParentObject.w_CPROWORD;
                  ,GQCODQUE = this.oParentObject.w_GQCODQUE;
                  ,GQNUMSEQ = this.oParentObject.w_GQNUMSEQ;
                  &i_ccchkf. ;
               where;
                  GQCODICE = this.oParentObject.w_GQCODICE;
                  and CPROWNUM = this.oParentObject.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore inserimento query'
              return
            endif
          endif
        endif
        bTrsErr=bTrsErr or bErr_03BCC6D0
        * --- End
      endif
    endif
    return
  proc Try_026BAD28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AQM
    i_nConn=i_TableProp[this.INF_AQM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AQM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AQM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AQCODICE"+",AQDES"+",AQQRPATH"+",AQDTFILE"+",AQIMPATH"+",AQLOG"+",AQADOCS"+",AQFLGZCP"+",AQFORTYP"+",AQCONTYP"+",AQCURLOC"+",AQCURTYP"+",AQENGTYP"+",AQTIMOUT"+",AQCMDTMO"+",AQQUEFIL"+",AQCRIPTE"+",AQFLJOIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AQM','AQCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQDES),'INF_AQM','AQDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQQRPATH),'INF_AQM','AQQRPATH');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQDTFILE),'INF_AQM','AQDTFILE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQIMPATH),'INF_AQM','AQIMPATH');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQLOG),'INF_AQM','AQLOG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQADOCS),'INF_AQM','AQADOCS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFLGZCP),'INF_AQM','AQFLGZCP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFORTYP),'INF_AQM','AQFORTYP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCONTYP),'INF_AQM','AQCONTYP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCURLOC),'INF_AQM','AQCURLOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCURTYP),'INF_AQM','AQCURTYP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQENGTYP),'INF_AQM','AQENGTYP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQTIMOUT),'INF_AQM','AQTIMOUT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCMDTMO),'INF_AQM','AQCMDTMO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQQUEFIL),'INF_AQM','AQQUEFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCRIPTE),'INF_AQM','AQCRIPTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFLJOIN),'INF_AQM','AQFLJOIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AQCODICE',this.oParentObject.w_AQCODICE,'AQDES',this.oParentObject.w_AQDES,'AQQRPATH',this.oParentObject.w_AQQRPATH,'AQDTFILE',this.oParentObject.w_AQDTFILE,'AQIMPATH',this.oParentObject.w_AQIMPATH,'AQLOG',this.oParentObject.w_AQLOG,'AQADOCS',this.oParentObject.w_AQADOCS,'AQFLGZCP',this.oParentObject.w_AQFLGZCP,'AQFORTYP',this.oParentObject.w_AQFORTYP,'AQCONTYP',this.oParentObject.w_AQCONTYP,'AQCURLOC',this.oParentObject.w_AQCURLOC,'AQCURTYP',this.oParentObject.w_AQCURTYP)
      insert into (i_cTable) (AQCODICE,AQDES,AQQRPATH,AQDTFILE,AQIMPATH,AQLOG,AQADOCS,AQFLGZCP,AQFORTYP,AQCONTYP,AQCURLOC,AQCURTYP,AQENGTYP,AQTIMOUT,AQCMDTMO,AQQUEFIL,AQCRIPTE,AQFLJOIN &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_AQDES;
           ,this.oParentObject.w_AQQRPATH;
           ,this.oParentObject.w_AQDTFILE;
           ,this.oParentObject.w_AQIMPATH;
           ,this.oParentObject.w_AQLOG;
           ,this.oParentObject.w_AQADOCS;
           ,this.oParentObject.w_AQFLGZCP;
           ,this.oParentObject.w_AQFORTYP;
           ,this.oParentObject.w_AQCONTYP;
           ,this.oParentObject.w_AQCURLOC;
           ,this.oParentObject.w_AQCURTYP;
           ,this.oParentObject.w_AQENGTYP;
           ,this.oParentObject.w_AQTIMOUT;
           ,this.oParentObject.w_AQCMDTMO;
           ,this.oParentObject.w_AQQUEFIL;
           ,this.oParentObject.w_AQCRIPTE;
           ,this.oParentObject.w_AQFLJOIN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B24C10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AQD
    i_nConn=i_TableProp[this.INF_AQD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AQD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AQD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AQCODICE"+",AQMAILR"+",CPROWORD"+",AQUTENTE"+",AQFLUE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AQD','AQCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQMAILR),'INF_AQD','AQMAILR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQROWORD),'INF_AQD','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQUTENTE),'INF_AQD','AQUTENTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFLUE),'INF_AQD','AQFLUE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AQCODICE',this.oParentObject.w_AQCODICE,'AQMAILR',this.oParentObject.w_AQMAILR,'CPROWORD',this.oParentObject.w_AQROWORD,'AQUTENTE',this.oParentObject.w_AQUTENTE,'AQFLUE',this.oParentObject.w_AQFLUE)
      insert into (i_cTable) (AQCODICE,AQMAILR,CPROWORD,AQUTENTE,AQFLUE &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_AQMAILR;
           ,this.oParentObject.w_AQROWORD;
           ,this.oParentObject.w_AQUTENTE;
           ,this.oParentObject.w_AQFLUE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B24D00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AUQU
    i_nConn=i_TableProp[this.INF_AUQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AUQU_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"QACODQUE"+",QAROWPRG"+",QAFLUTGR"+",QAUTEGRP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AUQU','QACODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAROWPRG),'INF_AUQU','QAROWPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAFLUTGR),'INF_AUQU','QAFLUTGR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAUTEGRP),'INF_AUQU','QAUTEGRP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'QACODQUE',this.oParentObject.w_AQCODICE,'QAROWPRG',this.oParentObject.w_QAROWPRG,'QAFLUTGR',this.oParentObject.w_QAFLUTGR,'QAUTEGRP',this.oParentObject.w_QAUTEGRP)
      insert into (i_cTable) (QACODQUE,QAROWPRG,QAFLUTGR,QAUTEGRP &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_QAROWPRG;
           ,this.oParentObject.w_QAFLUTGR;
           ,this.oParentObject.w_QAUTEGRP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B24370()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into IRDRMENU
    i_nConn=i_TableProp[this.IRDRMENU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IRDRMENU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.IRDRMENU_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MGSERIAL"+",MG__MENU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GI__MENU),'IRDRMENU','MGSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MG__MENU),'IRDRMENU','MG__MENU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MGSERIAL',this.oParentObject.w_GI__MENU,'MG__MENU',this.oParentObject.w_MG__MENU)
      insert into (i_cTable) (MGSERIAL,MG__MENU &i_ccchkf. );
         values (;
           this.oParentObject.w_GI__MENU;
           ,this.oParentObject.w_MG__MENU;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B236B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AGIM
    i_nConn=i_TableProp[this.INF_AGIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AGIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AGIM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GICODQUE"+",CPROWNUM"+",CPROWORD"+",GIPROGRA"+",GIPARAM"+",GITABNAM"+",GIDESC"+",GIDESGES"+",GI__MENU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AGIM','GICODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIROWNUM),'INF_AGIM','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIROWORD),'INF_AGIM','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIPROGRA),'INF_AGIM','GIPROGRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIPARAM),'INF_AGIM','GIPARAM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GITABNAM),'INF_AGIM','GITABNAM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIDESC),'INF_AGIM','GIDESC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIDESGES),'INF_AGIM','GIDESGES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GI__MENU),'INF_AGIM','GI__MENU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GICODQUE',this.oParentObject.w_AQCODICE,'CPROWNUM',this.oParentObject.w_GIROWNUM,'CPROWORD',this.oParentObject.w_GIROWORD,'GIPROGRA',this.oParentObject.w_GIPROGRA,'GIPARAM',this.oParentObject.w_GIPARAM,'GITABNAM',this.oParentObject.w_GITABNAM,'GIDESC',this.oParentObject.w_GIDESC,'GIDESGES',this.oParentObject.w_GIDESGES,'GI__MENU',this.oParentObject.w_GI__MENU)
      insert into (i_cTable) (GICODQUE,CPROWNUM,CPROWORD,GIPROGRA,GIPARAM,GITABNAM,GIDESC,GIDESGES,GI__MENU &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_GIROWNUM;
           ,this.oParentObject.w_GIROWORD;
           ,this.oParentObject.w_GIPROGRA;
           ,this.oParentObject.w_GIPARAM;
           ,this.oParentObject.w_GITABNAM;
           ,this.oParentObject.w_GIDESC;
           ,this.oParentObject.w_GIDESGES;
           ,this.oParentObject.w_GI__MENU;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B23CB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AFIM
    i_nConn=i_TableProp[this.INF_AFIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AFIM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FICODQUE"+",FIPRGRN"+",CPROWNUM"+",CPROWORD"+",FIFLNAME"+",FITIPFIL"+",FIFILTRO"+",FIFLGREM"+",FIFLGAZI"+",FIVISMSK"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AFIM','FICODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIPRGRN),'INF_AFIM','FIPRGRN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWNUM),'INF_AFIM','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWORD),'INF_AFIM','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLNAME),'INF_AFIM','FIFLNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FITIPFIL),'INF_AFIM','FITIPFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFILTRO),'INF_AFIM','FIFILTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGREM),'INF_AFIM','FIFLGREM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGAZI),'INF_AFIM','FIFLGAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIVISMSK),'INF_AFIM','FIVISMSK');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FICODQUE',this.oParentObject.w_AQCODICE,'FIPRGRN',this.oParentObject.w_FIPRGRN,'CPROWNUM',this.oParentObject.w_FIROWNUM,'CPROWORD',this.oParentObject.w_FIROWORD,'FIFLNAME',this.oParentObject.w_FIFLNAME,'FITIPFIL',this.oParentObject.w_FITIPFIL,'FIFILTRO',this.oParentObject.w_FIFILTRO,'FIFLGREM',this.oParentObject.w_FIFLGREM,'FIFLGAZI',this.oParentObject.w_FIFLGAZI,'FIVISMSK',this.oParentObject.w_FIVISMSK)
      insert into (i_cTable) (FICODQUE,FIPRGRN,CPROWNUM,CPROWORD,FIFLNAME,FITIPFIL,FIFILTRO,FIFLGREM,FIFLGAZI,FIVISMSK &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_FIPRGRN;
           ,this.oParentObject.w_FIROWNUM;
           ,this.oParentObject.w_FIROWORD;
           ,this.oParentObject.w_FIFLNAME;
           ,this.oParentObject.w_FITIPFIL;
           ,this.oParentObject.w_FIFILTRO;
           ,this.oParentObject.w_FIFLGREM;
           ,this.oParentObject.w_FIFLGAZI;
           ,this.oParentObject.w_FIVISMSK;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_026B9D08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AFIM
    i_nConn=i_TableProp[this.INF_AFIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AFIM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FICODQUE"+",FIPRGRN"+",CPROWNUM"+",CPROWORD"+",FIFLNAME"+",FITIPFIL"+",FIFILTRO"+",FIFLGREM"+",FIFLGAZI"+",FIVISMSK"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AFIM','FICODQUE');
      +","+cp_NullLink(cp_ToStrODBC(-1),'INF_AFIM','FIPRGRN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWNUM1),'INF_AFIM','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWORD1),'INF_AFIM','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLNAME1),'INF_AFIM','FIFLNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FITIPFIL1),'INF_AFIM','FITIPFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFILTRO1),'INF_AFIM','FIFILTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGREM1),'INF_AFIM','FIFLGREM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGAZI1),'INF_AFIM','FIFLGAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIVISMSK1),'INF_AFIM','FIVISMSK');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FICODQUE',this.oParentObject.w_AQCODICE,'FIPRGRN',-1,'CPROWNUM',this.oParentObject.w_FIROWNUM1,'CPROWORD',this.oParentObject.w_FIROWORD1,'FIFLNAME',this.oParentObject.w_FIFLNAME1,'FITIPFIL',this.oParentObject.w_FITIPFIL1,'FIFILTRO',this.oParentObject.w_FIFILTRO1,'FIFLGREM',this.oParentObject.w_FIFLGREM1,'FIFLGAZI',this.oParentObject.w_FIFLGAZI1,'FIVISMSK',this.oParentObject.w_FIVISMSK1)
      insert into (i_cTable) (FICODQUE,FIPRGRN,CPROWNUM,CPROWORD,FIFLNAME,FITIPFIL,FIFILTRO,FIFLGREM,FIFLGAZI,FIVISMSK &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,-1;
           ,this.oParentObject.w_FIROWNUM1;
           ,this.oParentObject.w_FIROWORD1;
           ,this.oParentObject.w_FIFLNAME1;
           ,this.oParentObject.w_FITIPFIL1;
           ,this.oParentObject.w_FIFILTRO1;
           ,this.oParentObject.w_FIFLGREM1;
           ,this.oParentObject.w_FIFLGAZI1;
           ,this.oParentObject.w_FIVISMSK1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_026BBCB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INFPARFI
    i_nConn=i_TableProp[this.INFPARFI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INFPARFI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INFPARFI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PFCODQUE"+",CPROWORD"+",CPROWNUM"+",PFTIPVAR"+",PFVARNAM"+",PFDESVAR"+",PFLENVAR"+",PFDECVAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INFPARFI','PFCODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFROWORD),'INFPARFI','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFROWNUM),'INFPARFI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFTIPVAR),'INFPARFI','PFTIPVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFVARNAM),'INFPARFI','PFVARNAM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFDESVAR),'INFPARFI','PFDESVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFLENVAR),'INFPARFI','PFLENVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFDECVAR),'INFPARFI','PFDECVAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PFCODQUE',this.oParentObject.w_AQCODICE,'CPROWORD',this.oParentObject.w_PFROWORD,'CPROWNUM',this.oParentObject.w_PFROWNUM,'PFTIPVAR',this.oParentObject.w_PFTIPVAR,'PFVARNAM',this.oParentObject.w_PFVARNAM,'PFDESVAR',this.oParentObject.w_PFDESVAR,'PFLENVAR',this.oParentObject.w_PFLENVAR,'PFDECVAR',this.oParentObject.w_PFDECVAR)
      insert into (i_cTable) (PFCODQUE,CPROWORD,CPROWNUM,PFTIPVAR,PFVARNAM,PFDESVAR,PFLENVAR,PFDECVAR &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_PFROWORD;
           ,this.oParentObject.w_PFROWNUM;
           ,this.oParentObject.w_PFTIPVAR;
           ,this.oParentObject.w_PFVARNAM;
           ,this.oParentObject.w_PFDESVAR;
           ,this.oParentObject.w_PFLENVAR;
           ,this.oParentObject.w_PFDECVAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B22B10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_PZCP
    i_nConn=i_TableProp[this.INF_PZCP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_PZCP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_PZCP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DECODICE"+",DERISERV"+",DEFOLDER"+",DEDESFIL"+",DE__NOTE"+",CPROWNUM"+",CPROWORD"+",DETIPDES"+",DECODGRU"+",DETIPCON"+",DECODCON"+",DECODAGE"+",DECODROL"+",DEVALDES"+",DE__READ"+",DE_WRITE"+",DEDELETE"+",DERIFCFO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_PZCP','DECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DERISERV),'INF_PZCP','DERISERV');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEFOLDER),'INF_PZCP','DEFOLDER');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEDESFIL),'INF_PZCP','DEDESFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE__NOTE),'INF_PZCP','DE__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEROWNUM),'INF_PZCP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEROWORD),'INF_PZCP','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPDES),'INF_PZCP','DETIPDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODGRU),'INF_PZCP','DECODGRU');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPCON),'INF_PZCP','DETIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODCON),'INF_PZCP','DECODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODAGE),'INF_PZCP','DECODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODROL),'INF_PZCP','DECODROL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEVALDES),'INF_PZCP','DEVALDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE__READ),'INF_PZCP','DE__READ');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE_WRITE),'INF_PZCP','DE_WRITE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEDELETE),'INF_PZCP','DEDELETE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DERIFCFO),'INF_PZCP','DERIFCFO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DECODICE',this.oParentObject.w_AQCODICE,'DERISERV',this.oParentObject.w_DERISERV,'DEFOLDER',this.oParentObject.w_DEFOLDER,'DEDESFIL',this.oParentObject.w_DEDESFIL,'DE__NOTE',this.oParentObject.w_DE__NOTE,'CPROWNUM',this.oParentObject.w_DEROWNUM,'CPROWORD',this.oParentObject.w_DEROWORD,'DETIPDES',this.oParentObject.w_DETIPDES,'DECODGRU',this.oParentObject.w_DECODGRU,'DETIPCON',this.oParentObject.w_DETIPCON,'DECODCON',this.oParentObject.w_DECODCON,'DECODAGE',this.oParentObject.w_DECODAGE)
      insert into (i_cTable) (DECODICE,DERISERV,DEFOLDER,DEDESFIL,DE__NOTE,CPROWNUM,CPROWORD,DETIPDES,DECODGRU,DETIPCON,DECODCON,DECODAGE,DECODROL,DEVALDES,DE__READ,DE_WRITE,DEDELETE,DERIFCFO &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_DERISERV;
           ,this.oParentObject.w_DEFOLDER;
           ,this.oParentObject.w_DEDESFIL;
           ,this.oParentObject.w_DE__NOTE;
           ,this.oParentObject.w_DEROWNUM;
           ,this.oParentObject.w_DEROWORD;
           ,this.oParentObject.w_DETIPDES;
           ,this.oParentObject.w_DECODGRU;
           ,this.oParentObject.w_DETIPCON;
           ,this.oParentObject.w_DECODCON;
           ,this.oParentObject.w_DECODAGE;
           ,this.oParentObject.w_DECODROL;
           ,this.oParentObject.w_DEVALDES;
           ,this.oParentObject.w_DE__READ;
           ,this.oParentObject.w_DE_WRITE;
           ,this.oParentObject.w_DEDELETE;
           ,this.oParentObject.w_DERIFCFO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B7E800()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AQD
    i_nConn=i_TableProp[this.INF_AQD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AQD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AQD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AQCODICE"+",AQMAILR"+",CPROWORD"+",AQUTENTE"+",AQFLUE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AQD','AQCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQMAILR),'INF_AQD','AQMAILR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQROWORD),'INF_AQD','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQUTENTE),'INF_AQD','AQUTENTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQFLUE),'INF_AQD','AQFLUE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AQCODICE',this.oParentObject.w_AQCODICE,'AQMAILR',this.oParentObject.w_AQMAILR,'CPROWORD',this.oParentObject.w_AQROWORD,'AQUTENTE',this.oParentObject.w_AQUTENTE,'AQFLUE',this.oParentObject.w_AQFLUE)
      insert into (i_cTable) (AQCODICE,AQMAILR,CPROWORD,AQUTENTE,AQFLUE &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_AQMAILR;
           ,this.oParentObject.w_AQROWORD;
           ,this.oParentObject.w_AQUTENTE;
           ,this.oParentObject.w_AQFLUE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B7B470()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AUQU
    i_nConn=i_TableProp[this.INF_AUQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AUQU_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"QACODQUE"+",QAROWPRG"+",QAFLUTGR"+",QAUTEGRP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AUQU','QACODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAROWPRG),'INF_AUQU','QAROWPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAFLUTGR),'INF_AUQU','QAFLUTGR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QAUTEGRP),'INF_AUQU','QAUTEGRP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'QACODQUE',this.oParentObject.w_AQCODICE,'QAROWPRG',this.oParentObject.w_QAROWPRG,'QAFLUTGR',this.oParentObject.w_QAFLUTGR,'QAUTEGRP',this.oParentObject.w_QAUTEGRP)
      insert into (i_cTable) (QACODQUE,QAROWPRG,QAFLUTGR,QAUTEGRP &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_QAROWPRG;
           ,this.oParentObject.w_QAFLUTGR;
           ,this.oParentObject.w_QAUTEGRP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B78920()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AGIM
    i_nConn=i_TableProp[this.INF_AGIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AGIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AGIM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GICODQUE"+",CPROWNUM"+",CPROWORD"+",GIPROGRA"+",GIPARAM"+",GITABNAM"+",GIDESC"+",GI__MENU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AGIM','GICODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIROWNUM),'INF_AGIM','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIROWORD),'INF_AGIM','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIPROGRA),'INF_AGIM','GIPROGRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIPARAM),'INF_AGIM','GIPARAM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GITABNAM),'INF_AGIM','GITABNAM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GIDESC),'INF_AGIM','GIDESC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GI__MENU),'INF_AGIM','GI__MENU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GICODQUE',this.oParentObject.w_AQCODICE,'CPROWNUM',this.oParentObject.w_GIROWNUM,'CPROWORD',this.oParentObject.w_GIROWORD,'GIPROGRA',this.oParentObject.w_GIPROGRA,'GIPARAM',this.oParentObject.w_GIPARAM,'GITABNAM',this.oParentObject.w_GITABNAM,'GIDESC',this.oParentObject.w_GIDESC,'GI__MENU',this.oParentObject.w_GI__MENU)
      insert into (i_cTable) (GICODQUE,CPROWNUM,CPROWORD,GIPROGRA,GIPARAM,GITABNAM,GIDESC,GI__MENU &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_GIROWNUM;
           ,this.oParentObject.w_GIROWORD;
           ,this.oParentObject.w_GIPROGRA;
           ,this.oParentObject.w_GIPARAM;
           ,this.oParentObject.w_GITABNAM;
           ,this.oParentObject.w_GIDESC;
           ,this.oParentObject.w_GI__MENU;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B6AD80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AFIM
    i_nConn=i_TableProp[this.INF_AFIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AFIM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FICODQUE"+",FIPRGRN"+",CPROWNUM"+",CPROWORD"+",FIFLNAME"+",FITIPFIL"+",FIFILTRO"+",FIFLGREM"+",FIFLGAZI"+",FIVISMSK"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AFIM','FICODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIPRGRN),'INF_AFIM','FIPRGRN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWNUM),'INF_AFIM','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWORD),'INF_AFIM','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLNAME),'INF_AFIM','FIFLNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FITIPFIL),'INF_AFIM','FITIPFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFILTRO),'INF_AFIM','FIFILTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGREM),'INF_AFIM','FIFLGREM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGAZI),'INF_AFIM','FIFLGAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIVISMSK),'INF_AFIM','FIVISMSK');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FICODQUE',this.oParentObject.w_AQCODICE,'FIPRGRN',this.oParentObject.w_FIPRGRN,'CPROWNUM',this.oParentObject.w_FIROWNUM,'CPROWORD',this.oParentObject.w_FIROWORD,'FIFLNAME',this.oParentObject.w_FIFLNAME,'FITIPFIL',this.oParentObject.w_FITIPFIL,'FIFILTRO',this.oParentObject.w_FIFILTRO,'FIFLGREM',this.oParentObject.w_FIFLGREM,'FIFLGAZI',this.oParentObject.w_FIFLGAZI,'FIVISMSK',this.oParentObject.w_FIVISMSK)
      insert into (i_cTable) (FICODQUE,FIPRGRN,CPROWNUM,CPROWORD,FIFLNAME,FITIPFIL,FIFILTRO,FIFLGREM,FIFLGAZI,FIVISMSK &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_FIPRGRN;
           ,this.oParentObject.w_FIROWNUM;
           ,this.oParentObject.w_FIROWORD;
           ,this.oParentObject.w_FIFLNAME;
           ,this.oParentObject.w_FITIPFIL;
           ,this.oParentObject.w_FIFILTRO;
           ,this.oParentObject.w_FIFLGREM;
           ,this.oParentObject.w_FIFLGAZI;
           ,this.oParentObject.w_FIVISMSK;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B68230()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_AFIM
    i_nConn=i_TableProp[this.INF_AFIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AFIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AFIM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FICODQUE"+",FIPRGRN"+",CPROWNUM"+",CPROWORD"+",FIFLNAME"+",FITIPFIL"+",FIFILTRO"+",FIFLGREM"+",FIFLGAZI"+",FIVISMSK"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_AFIM','FICODQUE');
      +","+cp_NullLink(cp_ToStrODBC(-1),'INF_AFIM','FIPRGRN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWNUM1),'INF_AFIM','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIROWORD1),'INF_AFIM','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLNAME1),'INF_AFIM','FIFLNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FITIPFIL1),'INF_AFIM','FITIPFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFILTRO1),'INF_AFIM','FIFILTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGREM1),'INF_AFIM','FIFLGREM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIFLGAZI1),'INF_AFIM','FIFLGAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FIVISMSK1),'INF_AFIM','FIVISMSK');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FICODQUE',this.oParentObject.w_AQCODICE,'FIPRGRN',-1,'CPROWNUM',this.oParentObject.w_FIROWNUM1,'CPROWORD',this.oParentObject.w_FIROWORD1,'FIFLNAME',this.oParentObject.w_FIFLNAME1,'FITIPFIL',this.oParentObject.w_FITIPFIL1,'FIFILTRO',this.oParentObject.w_FIFILTRO1,'FIFLGREM',this.oParentObject.w_FIFLGREM1,'FIFLGAZI',this.oParentObject.w_FIFLGAZI1,'FIVISMSK',this.oParentObject.w_FIVISMSK1)
      insert into (i_cTable) (FICODQUE,FIPRGRN,CPROWNUM,CPROWORD,FIFLNAME,FITIPFIL,FIFILTRO,FIFLGREM,FIFLGAZI,FIVISMSK &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,-1;
           ,this.oParentObject.w_FIROWNUM1;
           ,this.oParentObject.w_FIROWORD1;
           ,this.oParentObject.w_FIFLNAME1;
           ,this.oParentObject.w_FITIPFIL1;
           ,this.oParentObject.w_FIFILTRO1;
           ,this.oParentObject.w_FIFLGREM1;
           ,this.oParentObject.w_FIFLGAZI1;
           ,this.oParentObject.w_FIVISMSK1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B656E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INFPARFI
    i_nConn=i_TableProp[this.INFPARFI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INFPARFI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INFPARFI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PFCODQUE"+",CPROWORD"+",CPROWNUM"+",PFTIPVAR"+",PFVARNAM"+",PFDESVAR"+",PFLENVAR"+",PFDECVAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INFPARFI','PFCODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFROWORD),'INFPARFI','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFROWNUM),'INFPARFI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFTIPVAR),'INFPARFI','PFTIPVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFVARNAM),'INFPARFI','PFVARNAM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFDESVAR),'INFPARFI','PFDESVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFLENVAR),'INFPARFI','PFLENVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PFDECVAR),'INFPARFI','PFDECVAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PFCODQUE',this.oParentObject.w_AQCODICE,'CPROWORD',this.oParentObject.w_PFROWORD,'CPROWNUM',this.oParentObject.w_PFROWNUM,'PFTIPVAR',this.oParentObject.w_PFTIPVAR,'PFVARNAM',this.oParentObject.w_PFVARNAM,'PFDESVAR',this.oParentObject.w_PFDESVAR,'PFLENVAR',this.oParentObject.w_PFLENVAR,'PFDECVAR',this.oParentObject.w_PFDECVAR)
      insert into (i_cTable) (PFCODQUE,CPROWORD,CPROWNUM,PFTIPVAR,PFVARNAM,PFDESVAR,PFLENVAR,PFDECVAR &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_PFROWORD;
           ,this.oParentObject.w_PFROWNUM;
           ,this.oParentObject.w_PFTIPVAR;
           ,this.oParentObject.w_PFVARNAM;
           ,this.oParentObject.w_PFDESVAR;
           ,this.oParentObject.w_PFLENVAR;
           ,this.oParentObject.w_PFDECVAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03B60B70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_PZCP
    i_nConn=i_TableProp[this.INF_PZCP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_PZCP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_PZCP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DECODICE"+",DERISERV"+",DEFOLDER"+",DEDESFIL"+",DE__NOTE"+",CPROWNUM"+",CPROWORD"+",DETIPDES"+",DECODGRU"+",DETIPCON"+",DECODCON"+",DECODAGE"+",DECODROL"+",DEVALDES"+",DE__READ"+",DE_WRITE"+",DEDELETE"+",DERIFCFO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQCODICE),'INF_PZCP','DECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DERISERV),'INF_PZCP','DERISERV');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEFOLDER),'INF_PZCP','DEFOLDER');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEDESFIL),'INF_PZCP','DEDESFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE__NOTE),'INF_PZCP','DE__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEROWNUM),'INF_PZCP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEROWORD),'INF_PZCP','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPDES),'INF_PZCP','DETIPDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODGRU),'INF_PZCP','DECODGRU');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPCON),'INF_PZCP','DETIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODCON),'INF_PZCP','DECODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODAGE),'INF_PZCP','DECODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECODROL),'INF_PZCP','DECODROL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEVALDES),'INF_PZCP','DEVALDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE__READ),'INF_PZCP','DE__READ');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DE_WRITE),'INF_PZCP','DE_WRITE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEDELETE),'INF_PZCP','DEDELETE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DERIFCFO),'INF_PZCP','DERIFCFO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DECODICE',this.oParentObject.w_AQCODICE,'DERISERV',this.oParentObject.w_DERISERV,'DEFOLDER',this.oParentObject.w_DEFOLDER,'DEDESFIL',this.oParentObject.w_DEDESFIL,'DE__NOTE',this.oParentObject.w_DE__NOTE,'CPROWNUM',this.oParentObject.w_DEROWNUM,'CPROWORD',this.oParentObject.w_DEROWORD,'DETIPDES',this.oParentObject.w_DETIPDES,'DECODGRU',this.oParentObject.w_DECODGRU,'DETIPCON',this.oParentObject.w_DETIPCON,'DECODCON',this.oParentObject.w_DECODCON,'DECODAGE',this.oParentObject.w_DECODAGE)
      insert into (i_cTable) (DECODICE,DERISERV,DEFOLDER,DEDESFIL,DE__NOTE,CPROWNUM,CPROWORD,DETIPDES,DECODGRU,DETIPCON,DECODCON,DECODAGE,DECODROL,DEVALDES,DE__READ,DE_WRITE,DEDELETE,DERIFCFO &i_ccchkf. );
         values (;
           this.oParentObject.w_AQCODICE;
           ,this.oParentObject.w_DERISERV;
           ,this.oParentObject.w_DEFOLDER;
           ,this.oParentObject.w_DEDESFIL;
           ,this.oParentObject.w_DE__NOTE;
           ,this.oParentObject.w_DEROWNUM;
           ,this.oParentObject.w_DEROWORD;
           ,this.oParentObject.w_DETIPDES;
           ,this.oParentObject.w_DECODGRU;
           ,this.oParentObject.w_DETIPCON;
           ,this.oParentObject.w_DECODCON;
           ,this.oParentObject.w_DECODAGE;
           ,this.oParentObject.w_DECODROL;
           ,this.oParentObject.w_DEVALDES;
           ,this.oParentObject.w_DE__READ;
           ,this.oParentObject.w_DE_WRITE;
           ,this.oParentObject.w_DEDELETE;
           ,this.oParentObject.w_DERIFCFO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03BC5590()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_GRQM
    i_nConn=i_TableProp[this.INF_GRQM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_GRQM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_GRQM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GQCODICE"+",GQDESCRI"+",GQDTINVA"+",GQDTOBSO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQCODICE),'INF_GRQM','GQCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQDESCRI),'INF_GRQM','GQDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQDTINVA),'INF_GRQM','GQDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQDTOBSO),'INF_GRQM','GQDTOBSO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GQCODICE',this.oParentObject.w_GQCODICE,'GQDESCRI',this.oParentObject.w_GQDESCRI,'GQDTINVA',this.oParentObject.w_GQDTINVA,'GQDTOBSO',this.oParentObject.w_GQDTOBSO)
      insert into (i_cTable) (GQCODICE,GQDESCRI,GQDTINVA,GQDTOBSO &i_ccchkf. );
         values (;
           this.oParentObject.w_GQCODICE;
           ,this.oParentObject.w_GQDESCRI;
           ,this.oParentObject.w_GQDTINVA;
           ,this.oParentObject.w_GQDTOBSO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03BC60A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from INF_GRQD
    i_nConn=i_TableProp[this.INF_GRQD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_GRQD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"GQCODICE = "+cp_ToStrODBC(this.oParentObject.w_GQCODICE);
             )
    else
      delete from (i_cTable) where;
            GQCODICE = this.oParentObject.w_GQCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into INF_GRQD
    i_nConn=i_TableProp[this.INF_GRQD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_GRQD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_GRQD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GQCODICE"+",CPROWNUM"+",CPROWORD"+",GQCODQUE"+",GQNUMSEQ"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQCODICE),'INF_GRQD','GQCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'INF_GRQD','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_GRQD','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQCODQUE),'INF_GRQD','GQCODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQNUMSEQ),'INF_GRQD','GQNUMSEQ');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GQCODICE',this.oParentObject.w_GQCODICE,'CPROWNUM',this.oParentObject.w_CPROWNUM,'CPROWORD',this.oParentObject.w_CPROWORD,'GQCODQUE',this.oParentObject.w_GQCODQUE,'GQNUMSEQ',this.oParentObject.w_GQNUMSEQ)
      insert into (i_cTable) (GQCODICE,CPROWNUM,CPROWORD,GQCODQUE,GQNUMSEQ &i_ccchkf. );
         values (;
           this.oParentObject.w_GQCODICE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_CPROWORD;
           ,this.oParentObject.w_GQCODQUE;
           ,this.oParentObject.w_GQNUMSEQ;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03BCC6D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INF_GRQD
    i_nConn=i_TableProp[this.INF_GRQD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_GRQD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_GRQD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GQCODICE"+",CPROWNUM"+",CPROWORD"+",GQCODQUE"+",GQNUMSEQ"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQCODICE),'INF_GRQD','GQCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'INF_GRQD','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'INF_GRQD','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQCODQUE),'INF_GRQD','GQCODQUE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GQNUMSEQ),'INF_GRQD','GQNUMSEQ');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GQCODICE',this.oParentObject.w_GQCODICE,'CPROWNUM',this.oParentObject.w_CPROWNUM,'CPROWORD',this.oParentObject.w_CPROWORD,'GQCODQUE',this.oParentObject.w_GQCODQUE,'GQNUMSEQ',this.oParentObject.w_GQNUMSEQ)
      insert into (i_cTable) (GQCODICE,CPROWNUM,CPROWORD,GQCODQUE,GQNUMSEQ &i_ccchkf. );
         values (;
           this.oParentObject.w_GQCODICE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_CPROWORD;
           ,this.oParentObject.w_GQCODQUE;
           ,this.oParentObject.w_GQNUMSEQ;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='INF_GRQM'
    this.cWorkTables[2]='INF_GRQD'
    this.cWorkTables[3]='INF_AQM'
    this.cWorkTables[4]='INF_AQD'
    this.cWorkTables[5]='INF_AUGM'
    this.cWorkTables[6]='INF_AGIM'
    this.cWorkTables[7]='INF_AFIM'
    this.cWorkTables[8]='INF_PZCP'
    this.cWorkTables[9]='INF_AUQU'
    this.cWorkTables[10]='INFPARFI'
    this.cWorkTables[11]='IRDRMENU'
    return(this.OpenAllTables(11))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
