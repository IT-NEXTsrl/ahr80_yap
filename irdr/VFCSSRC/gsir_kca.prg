* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_kca                                                        *
*              Caricamento report infobusiness esterni                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-23                                                      *
* Last revis.: 2013-02-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsir_kca",oParentObject))

* --- Class definition
define class tgsir_kca as StdForm
  Top    = 5
  Left   = 6

  * --- Standard Properties
  Width  = 828
  Height = 493
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-02-08"
  HelpContextID=169250967
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  INF_AQM_IDX = 0
  cPrg = "gsir_kca"
  cComment = "Caricamento report infobusiness esterni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DIRECTORI = space(200)
  o_DIRECTORI = space(200)
  w_SELEZI = space(1)
  w_CODAZI = space(5)
  w_DIR = space(200)
  w_LPATH = .F.
  w_HDIR = space(200)
  w_AQADOCS = space(254)
  w_CODQUERY = space(20)
  o_CODQUERY = space(20)
  w_ZoomFile = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_kcaPag1","gsir_kca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDIRECTORI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomFile = this.oPgFrm.Pages(1).oPag.ZoomFile
    DoDefault()
    proc Destroy()
      this.w_ZoomFile = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='INF_AQM'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DIRECTORI=space(200)
      .w_SELEZI=space(1)
      .w_CODAZI=space(5)
      .w_DIR=space(200)
      .w_LPATH=.f.
      .w_HDIR=space(200)
      .w_AQADOCS=space(254)
      .w_CODQUERY=space(20)
      .oPgFrm.Page1.oPag.ZoomFile.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .w_CODAZI = i_CODAZI
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate(.w_DIRECTORI)
          .DoRTCalc(4,5,.f.)
        .w_HDIR = SYS(5)+SYS(2003)
          .DoRTCalc(7,7,.f.)
        .w_CODQUERY = .w_ZoomFile.getvar('NOME')
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomFile.Calculate()
        if .o_DIRECTORI<>.w_DIRECTORI
          .Calculate_LMZUISUYVI()
        endif
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(.w_DIRECTORI)
        .DoRTCalc(1,7,.t.)
            .w_CODQUERY = .w_ZoomFile.getvar('NOME')
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomFile.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate(.w_DIRECTORI)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return

  proc Calculate_LMZUISUYVI()
    with this
          * --- Controllo path
          .w_DIRECTORI = IIF(right(alltrim(.w_DIRECTORI),1)='\' or empty(.w_DIRECTORI),.w_DIRECTORI,alltrim(.w_DIRECTORI)+iif(len(alltrim(.w_DIRECTORI))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_DIRECTORI,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomFile.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsir_kca
    * --- Chiude il cursore di appoggio utilizzato per memorizare i file della directori scelta dall utente
    if (cevent=='Done')
    IF (USED ('CursFILE'))
    SELECT CursFILE
    USE
    ENDIF
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDIRECTORI_1_2.value==this.w_DIRECTORI)
      this.oPgFrm.Page1.oPag.oDIRECTORI_1_2.value=this.w_DIRECTORI
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_8.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAQADOCS_1_16.value==this.w_AQADOCS)
      this.oPgFrm.Page1.oPag.oAQADOCS_1_16.value=this.w_AQADOCS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DIRECTORI = this.w_DIRECTORI
    this.o_CODQUERY = this.w_CODQUERY
    return

enddefine

* --- Define pages as container
define class tgsir_kcaPag1 as StdContainer
  Width  = 824
  height = 493
  stdWidth  = 824
  stdheight = 493
  resizeXpos=399
  resizeYpos=243
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDIRECTORI_1_2 as StdField with uid="YJTBDAQENK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DIRECTORI", cQueryName = "DIRECTORI",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Directory corrente",;
    HelpContextID = 225377512,;
   bGlobalFont=.t.,;
    Height=21, Width=665, Left=122, Top=15, InputMask=replicate('X',200)


  add object oBtn_1_3 as StdButton with uid="BYWCYEOHKL",left=796, top=15, width=19,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la directory ";
    , HelpContextID = 169451990;
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        GSIR_BCA(this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_4 as StdButton with uid="FCTKAGOXAT",left=717, top=439, width=48,height=45,;
    CpPicture="BMP\carica.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare i report esterni di infobusiness";
    , HelpContextID = 239659482;
    , Caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        GSIR_BCA(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="ZSONRLTWLJ",left=768, top=439, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176568390;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomFile as cp_szoombox with uid="RBUQXJLIXE",left=4, top=69, width=821,height=366,;
    caption='ZoomFile',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cZoomFile="GSIR_KCA",cTable="INF_AQM",bOptions=.f.,bQueryOnDblClick=.f.,bNoZoomGridShape=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 135596549

  add object oSELEZI_1_8 as StdRadio with uid="BIDMIWUBAA",rtseq=2,rtrep=.f.,left=12, top=439, width=129,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_8.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 117400794
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 117400794
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_8.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_8.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_9 as cp_runprogram with uid="QNEANUFZMX",left=11, top=511, width=219,height=26,;
    caption='GSIR_BCA(S)',;
   bGlobalFont=.t.,;
    prg="GSIR_BCA('S')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 228565465


  add object oObj_1_13 as cp_runprogram with uid="BCNZNZYMJB",left=232, top=511, width=219,height=26,;
    caption='GSIR_BCA(O)',;
   bGlobalFont=.t.,;
    prg="GSIR_BCA('O')",;
    cEvent = "w_DIRECTORI Changed",;
    nPag=1;
    , HelpContextID = 228566489


  add object oObj_1_14 as cp_runprogram with uid="XKJKYIOSPW",left=453, top=511, width=219,height=26,;
    caption='GSIR_BCA(I)',;
   bGlobalFont=.t.,;
    prg="GSIR_BCA('I')",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 228568025

  add object oAQADOCS_1_16 as StdField with uid="YQWEJUOZPP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_AQADOCS", cQueryName = "AQADOCS",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di connessione ADO",;
    HelpContextID = 229706234,;
   bGlobalFont=.t.,;
    Height=21, Width=665, Left=122, Top=42, InputMask=replicate('X',254)


  add object oBtn_1_18 as StdButton with uid="ZTMQWGXNSN",left=796, top=42, width=19,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Costruzione stringa ADO";
    , HelpContextID = 169451990;
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      do GSIR_KAD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_20 as cp_runprogram with uid="GXPROLZVIZ",left=232, top=537, width=219,height=26,;
    caption='GSIR_BCA(A)',;
   bGlobalFont=.t.,;
    prg="GSIR_BCA('A')",;
    cEvent = "ZOOMFILE menucheck,ZOOMFILE row checked,ZOOMFILE row unchecked,w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 228570073

  add object oStr_1_1 as StdString with uid="SHCDKROQHP",Visible=.t., Left=65, Top=19,;
    Alignment=1, Width=51, Height=18,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="TIUODYBUEB",Visible=.t., Left=-25, Top=44,;
    Alignment=1, Width=141, Height=18,;
    Caption="Connessione ADO:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_kca','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
