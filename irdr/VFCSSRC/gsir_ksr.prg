* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_ksr                                                        *
*              Selezione query InfoReader                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-23                                                      *
* Last revis.: 2012-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsir_ksr",oParentObject))

* --- Class definition
define class tgsir_ksr as StdForm
  Top    = 2
  Left   = 2

  * --- Standard Properties
  Width  = 775
  Height = 380
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-28"
  HelpContextID=99184489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsir_ksr"
  cComment = "Selezione query InfoReader"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_UGCODUTE = 0
  w_GIPROGRA = space(20)
  w_CODQUERY = space(20)
  w_DESPRG = space(20)
  w_QUIT = space(1)
  w_FILTRSI = space(1)
  w_VISQUERY = space(1)
  w_TIPO = space(1)
  w_GIROWNUM = space(1)
  w_GIPARAM = space(15)
  w_ZOOMSEL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsir_ksr
  *--- Istanzio l'oggetto che conterr� i_curform cio� la gestione utilizzata
  *--- per filtrare le query
  w_OBJECT = .null.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_ksrPag1","gsir_ksr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVISQUERY_1_13
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMSEL = this.oPgFrm.Pages(1).oPag.ZOOMSEL
    DoDefault()
    proc Destroy()
      this.w_ZOOMSEL = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gsir_bir(this,.w_CODQUERY, .w_GIPROGRA)
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_UGCODUTE=0
      .w_GIPROGRA=space(20)
      .w_CODQUERY=space(20)
      .w_DESPRG=space(20)
      .w_QUIT=space(1)
      .w_FILTRSI=space(1)
      .w_VISQUERY=space(1)
      .w_TIPO=space(1)
      .w_GIROWNUM=space(1)
      .w_GIPARAM=space(15)
      .oPgFrm.Page1.oPag.ZOOMSEL.Calculate()
          .DoRTCalc(1,2,.f.)
        .w_CODQUERY = .w_ZOOMSEL.getvar('AQCODICE')
        .w_DESPRG = .w_ZOOMSEL.getvar('GIDESC')
        .w_QUIT = 'N'
          .DoRTCalc(6,6,.f.)
        .w_VISQUERY = 'Q'
        .w_TIPO = .w_ZOOMSEL.getvar('TIPO')
        .w_GIROWNUM = .w_ZOOMSEL.getvar('CPROWNUM')
    endwith
    this.DoRTCalc(10,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsir_ksr
    endproc
    Proc Activate()
       this.w_OBJECT = i_curform
       dodefault()
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMSEL.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_CODQUERY = .w_ZOOMSEL.getvar('AQCODICE')
            .w_DESPRG = .w_ZOOMSEL.getvar('GIDESC')
        .DoRTCalc(5,7,.t.)
            .w_TIPO = .w_ZOOMSEL.getvar('TIPO')
            .w_GIROWNUM = .w_ZOOMSEL.getvar('CPROWNUM')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMSEL.Calculate()
    endwith
  return

  proc Calculate_OCNNMBEDRH()
    with this
          * --- Visualizzo la query
          GSIR_BIR(this;
              ,.w_CODQUERY;
              ,.w_OBJECT;
              ,.w_TIPO;
              ,.w_GIROWNUM;
             )
    endwith
  endproc
  proc Calculate_BUDOYEICFX()
    with this
          * --- Valorizzo w_giprogra per effettuare il filtro
          GSIR_BSR(this;
             )
    endwith
  endproc
  proc Calculate_OPJJSFWUOP()
    with this
          * --- Rilascio w_object
          .w_OBJECT = .null.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMSEL.Event(cEvent)
        if lower(cEvent)==lower("w_zoomsel selected")
          .Calculate_OCNNMBEDRH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_BUDOYEICFX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_OPJJSFWUOP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVISQUERY_1_13.RadioValue()==this.w_VISQUERY)
      this.oPgFrm.Page1.oPag.oVISQUERY_1_13.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsir_ksrPag1 as StdContainer
  Width  = 771
  height = 380
  stdWidth  = 771
  stdheight = 380
  resizeXpos=555
  resizeYpos=289
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMSEL as cp_zoombox with uid="ORMEDKVZGV",left=1, top=2, width=773,height=325,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="INF_AQM",cZoomFile="GSIR_KSR",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,bQueryOnDblClick=.t.,bRetriveAllRows=.f.,cMenuFile="",cZoomOnZoom="GSIR_MAQ",;
    cEvent = "Aggiorna,w_VISQUERY Changed",;
    nPag=1;
    , ToolTipText = "Selezionare la query che sar� aperta da InfoReader";
    , HelpContextID = 78813158


  add object oBtn_1_2 as StdButton with uid="LUBJNUMKMP",left=663, top=330, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la query con InfoReader";
    , HelpContextID = 99155738;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      with this.Parent.oContained
        gsir_bir(this.Parent.oContained,.w_CODQUERY, .w_OBJECT, .w_TIPO, .w_GIROWNUM)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CODQUERY))
      endwith
    endif
  endfunc


  add object oBtn_1_3 as StdButton with uid="AXDGTFGFMJ",left=714, top=330, width=48,height=45,;
    CpPicture="bmp\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91867066;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oVISQUERY_1_13 as StdCombo with uid="HPKFCFJNWG",rtseq=7,rtrep=.f.,left=324,top=342,width=155,height=21;
    , HelpContextID = 79499183;
    , cFormVar="w_VISQUERY",RowSource=""+"Query non filtrate,"+"Query non pubblicate,"+"Tutte,"+"Query contestuali non filtrate,"+"Query contestuali filtrate,"+"Tutte per contesto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVISQUERY_1_13.RadioValue()
    return(iif(this.value =1,'Q',;
    iif(this.value =2,'P',;
    iif(this.value =3,'A',;
    iif(this.value =4,'N',;
    iif(this.value =5,'F',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oVISQUERY_1_13.GetRadio()
    this.Parent.oContained.w_VISQUERY = this.RadioValue()
    return .t.
  endfunc

  func oVISQUERY_1_13.SetRadio()
    this.Parent.oContained.w_VISQUERY=trim(this.Parent.oContained.w_VISQUERY)
    this.value = ;
      iif(this.Parent.oContained.w_VISQUERY=='Q',1,;
      iif(this.Parent.oContained.w_VISQUERY=='P',2,;
      iif(this.Parent.oContained.w_VISQUERY=='A',3,;
      iif(this.Parent.oContained.w_VISQUERY=='N',4,;
      iif(this.Parent.oContained.w_VISQUERY=='F',5,;
      iif(this.Parent.oContained.w_VISQUERY=='T',6,;
      0))))))
  endfunc

  add object oStr_1_14 as StdString with uid="YYJXVBUJDA",Visible=.t., Left=230, Top=342,;
    Alignment=1, Width=92, Height=18,;
    Caption="Visualizza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="AHRJEJBOBX",Visible=.t., Left=9, Top=332,;
    Alignment=0, Width=107, Height=17,;
    Caption="Query filtrate"    , ForeColor=RGB(128,0,128);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="PBMCUFIKYZ",Visible=.t., Left=9, Top=345,;
    Alignment=0, Width=122, Height=17,;
    Caption="Query non filtrate"    , ForeColor=RGB(0,128,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="PPETGYXZZE",Visible=.t., Left=9, Top=358,;
    Alignment=0, Width=124, Height=17,;
    Caption="Query non pubblicate"    , ForeColor=RGB(24,116,205);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_17 as StdBox with uid="FDCHXEGXOV",left=5, top=330, width=138,height=46
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_ksr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
