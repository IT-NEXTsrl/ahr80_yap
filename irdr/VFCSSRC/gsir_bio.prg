* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bio                                                        *
*              Installa ocx                                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_22]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-05                                                      *
* Last revis.: 2007-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bio",oParentObject)
return(i_retval)

define class tgsir_bio as StdBatch
  * --- Local variables
  w_SYSPATH = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Installa/Aggiorna componenti OCX InfoPublisher
    * --- Verifico se sono su un sistema a 64bit
    if Directory(GETENV("WINDIR")+ "\SysWOW64\")
      * --- Ricavo il path della cartella SYSWOW64
      this.w_SYSPATH=GETENV("WINDIR")+ "\SysWOW64\"
    else
      * --- Ricavo il path della cartella SYSTEM32
      this.w_SYSPATH=GETENV("WINDIR")+ "\SYSTEM32\"
    endif
    * --- Chiedo all'utente se desidera Installare/Aggiornare gli OCX
    if ah_YesNo("Attenzione: si desidera installare/aggiornare i componenti ocx?")
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_ErrorMsg("Installazione/aggiornamento componenti ocx effettuata","i","")
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Installa CCubeX30.ocx
    ah_Msg("Installo ccubex30.ocx")
    w_FILEOCX = this.w_SYSPATH+"CCubeX30.ocx" 
 COPY FILE ..\IRDR\OCX\CCubeX30.ocx TO &w_FILEOCX
    this.ShellEx(this.w_SYSPATH+"REGSVR32.exe","","/S "+this.w_SYSPATH+"CCubeX30.ocx")
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Installa IBPublisher20.OCX
    ah_Msg("Installo ibpublisher.ocx",.T.)
    w_FILEOCX = this.w_SYSPATH+"IBPublisher20.ocx" 
 COPY FILE ..\IRDR\OCX\IBPublisher20.ocx TO &w_FILEOCX
    this.ShellEx(this.w_SYSPATH+"REGSVR32.exe","","/S "+this.w_SYSPATH+"IBPublisher20.ocx")
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Installa InfoReader.OCX
    ah_Msg("Installo InfoReader.ocx",.T.)
    w_FILEOCX = this.w_SYSPATH+"InfoReader.ocx" 
 COPY FILE ..\IRDR\OCX\InfoReader.ocx TO &w_FILEOCX
    this.ShellEx(this.w_SYSPATH+"REGSVR32.exe","","/S "+this.w_SYSPATH+"InfoReader.ocx")
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Disinstalla/Installa componenti OCX
    * --- Se i file OCX sono gi� presenti nella cartella SYSTEM32 prima devo disinstallarli
    * --- Controllo esistenza IBPublisher20.ocx
    if FILE(this.w_SYSPATH+"IBPublisher20.ocx")
      ah_Msg("Disinstallo ibpublisher20.ocx")
      this.ShellEx(this.w_SYSPATH+"REGSVR32.exe","", "/U /S"+this.w_SYSPATH+"IBPublisher20.ocx")
      w_FILEOCX = this.w_SYSPATH+"IBPublisher20.ocx" 
 DELETE FILE &w_FILEOCX RECYCLE
    endif
    * --- Controllo esistenza InfoReader.ocx
    if FILE(this.w_SYSPATH+"InfoReader.ocx")
      ah_Msg("Disinstallo InfoReader.ocx")
      this.ShellEx(this.w_SYSPATH+"REGSVR32.exe","", "/U /S"+this.w_SYSPATH+"InfoReader.ocx")
      w_FILEOCX = this.w_SYSPATH+"InfoReader.ocx" 
 DELETE FILE &w_FILEOCX RECYCLE
    endif
    * --- Controllo esistenza CCubeX30.ocx
    if FILE(this.w_SYSPATH+"CCubeX30.ocx")
      ah_Msg("Disinstallo ccubex30.ocx")
      this.ShellEx(this.w_SYSPATH+"REGSVR32.exe","", "/U /S"+this.w_SYSPATH+"CCubeX30.ocx")
      w_FILEOCX = this.w_SYSPATH+"CCubeX30.ocx" 
 DELETE FILE &w_FILEOCX RECYCLE
    endif
    * --- Installo gli OCX
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- gsir_bio
  func ShellEx(cLink,cAction,cParms)
    declare integer ShellExecute in shell32.dll;
      integer nHwnd,;
      string cOperation,;
      string cFileName,;
      string cParms,;
      string cDir,;
      integer nShowCmd
  
    declare integer FindWindow in win32api;
      string cNull,;
      string cWinName
  
    cAction=iif(empty(cAction),"Open",cAction)
    cParms=iif(empty(cParms),"",cParms)
    return(ShellExecute(FindWindow(0,_screen.caption),cAction,cLink,cParms,tempadhoc(),1))
  endfunc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
