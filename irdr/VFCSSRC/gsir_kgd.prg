* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_kgd                                                        *
*              Pubblicazione dati InfoPublisher                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-05                                                      *
* Last revis.: 2012-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsir_kgd",oParentObject))

* --- Class definition
define class tgsir_kgd as StdForm
  Top    = 2
  Left   = 2

  * --- Standard Properties
  Width  = 594
  Height = 437
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-16"
  HelpContextID=32075625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  INF_GRQM_IDX = 0
  cPrg = "gsir_kgd"
  cComment = "Pubblicazione dati InfoPublisher"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODGRQ = space(15)
  w_DESCRI = space(45)
  w_LOG = space(1)
  w_PROCESSO = space(10)
  w_AQCODICE = space(10)
  w_AQLOG = space(10)
  w_ZOOMQRY = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_kgdPag1","gsir_kgd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODGRQ_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMQRY = this.oPgFrm.Pages(1).oPag.ZOOMQRY
    DoDefault()
    proc Destroy()
      this.w_ZOOMQRY = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='INF_GRQM'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSIR_BGD(this, .w_CODGRQ, "G", .T. )
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODGRQ=space(15)
      .w_DESCRI=space(45)
      .w_LOG=space(1)
      .w_PROCESSO=space(10)
      .w_AQCODICE=space(10)
      .w_AQLOG=space(10)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODGRQ))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_LOG = ' '
      .oPgFrm.Page1.oPag.ZOOMQRY.Calculate(.w_CODGRQ)
        .w_PROCESSO = .w_ZOOMQRY.getVar('PROCESSO')
        .w_AQCODICE = .w_ZOOMQRY.getVar('AQCODICE')
        .w_AQLOG = .w_ZOOMQRY.getVar('AQLOG')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMQRY.Calculate(.w_CODGRQ)
        .DoRTCalc(1,3,.t.)
            .w_PROCESSO = .w_ZOOMQRY.getVar('PROCESSO')
            .w_AQCODICE = .w_ZOOMQRY.getVar('AQCODICE')
            .w_AQLOG = .w_ZOOMQRY.getVar('AQLOG')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMQRY.Calculate(.w_CODGRQ)
    endwith
  return

  proc Calculate_RQNPVHOBUN()
    with this
          * --- Cambio query zoom
          ChangeZoom(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMQRY.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_RQNPVHOBUN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODGRQ
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_GRQM_IDX,3]
    i_lTable = "INF_GRQM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_GRQM_IDX,2], .t., this.INF_GRQM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_GRQM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'INF_GRQM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GQCODICE like "+cp_ToStrODBC(trim(this.w_CODGRQ)+"%");

          i_ret=cp_SQL(i_nConn,"select GQCODICE,GQDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GQCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GQCODICE',trim(this.w_CODGRQ))
          select GQCODICE,GQDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GQCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRQ)==trim(_Link_.GQCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODGRQ) and !this.bDontReportError
            deferred_cp_zoom('INF_GRQM','*','GQCODICE',cp_AbsName(oSource.parent,'oCODGRQ_1_1'),i_cWhere,'',"Elenco gruppi di query",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GQCODICE,GQDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GQCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GQCODICE',oSource.xKey(1))
            select GQCODICE,GQDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GQCODICE,GQDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GQCODICE="+cp_ToStrODBC(this.w_CODGRQ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GQCODICE',this.w_CODGRQ)
            select GQCODICE,GQDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRQ = NVL(_Link_.GQCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.GQDESCRI,space(45))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRQ = space(15)
      endif
      this.w_DESCRI = space(45)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_GRQM_IDX,2])+'\'+cp_ToStr(_Link_.GQCODICE,1)
      cp_ShowWarn(i_cKey,this.INF_GRQM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODGRQ_1_1.value==this.w_CODGRQ)
      this.oPgFrm.Page1.oPag.oCODGRQ_1_1.value=this.w_CODGRQ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_2.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_2.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oLOG_1_4.RadioValue()==this.w_LOG)
      this.oPgFrm.Page1.oPag.oLOG_1_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsir_kgdPag1 as StdContainer
  Width  = 590
  height = 437
  stdWidth  = 590
  stdheight = 437
  resizeXpos=347
  resizeYpos=335
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODGRQ_1_1 as StdField with uid="IEXPDLTEUD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODGRQ", cQueryName = "CODGRQ",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo delle query che si vogliono generare",;
    HelpContextID = 75637798,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=123, Top=11, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="INF_GRQM", oKey_1_1="GQCODICE", oKey_1_2="this.w_CODGRQ"

  func oCODGRQ_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRQ_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRQ_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_GRQM','*','GQCODICE',cp_AbsName(this.parent,'oCODGRQ_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco gruppi di query",'',this.parent.oContained
  endproc

  add object oDESCRI_1_2 as StdField with uid="FOTDBPNNRE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo di query",;
    HelpContextID = 58783178,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=260, Top=11, InputMask=replicate('X',45)

  add object oLOG_1_4 as StdCheck with uid="MMEGMTVKGI",rtseq=3,rtrep=.f.,left=105, top=43, caption="Attiva informazioni avanzate di log",;
    ToolTipText = "Attiva informazioni avanzate di log",;
    HelpContextID = 31763274,;
    cFormVar="w_LOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLOG_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oLOG_1_4.GetRadio()
    this.Parent.oContained.w_LOG = this.RadioValue()
    return .t.
  endfunc

  func oLOG_1_4.SetRadio()
    this.Parent.oContained.w_LOG=trim(this.Parent.oContained.w_LOG)
    this.value = ;
      iif(this.Parent.oContained.w_LOG=='S',1,;
      0)
  endfunc


  add object oBtn_1_5 as StdButton with uid="RIHNVYPWOK",left=453, top=37, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare la generazione";
    , HelpContextID = 153885561;
    , tabstop=.f.,Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSIR_BGD(this.Parent.oContained, .w_CODGRQ, "G", .T. )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CODGRQ))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="LKDJRNWUMO",left=507, top=37, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 24758202;
    , tabstop=.f.,Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMQRY as cp_zoombox with uid="OHHLZIYJSX",left=2, top=87, width=582,height=301,;
    caption='Object',;
   bGlobalFont=.t.,;
    bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cZoomFile="GSIR_KGD",cTable="INF_AQM",cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 145922022


  add object oBtn_1_12 as StdButton with uid="RUNVYXDVJX",left=64, top=389, width=48,height=45,;
    CpPicture="bmp\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il dettaglio del processo documentale";
    , HelpContextID = 32075530;
    , tabstop=.f.,Caption='\<Det.Proc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSDM_BPD(this.Parent.oContained,"VL", cp_NewCCChk(), .w_PROCESSO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PROCESSO))
      endwith
    endif
  endfunc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="FGHORWLUQU",left=9, top=389, width=48,height=45,;
    CpPicture="bmp\log.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il log di elaborazione";
    , HelpContextID = 32075530;
    , tabstop=.f.,Caption='\<Log';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        STAPDF(fullpath(alltrim(.w_AQLOG)))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_AQLOG))
      endwith
    endif
  endfunc

  add object oStr_1_3 as StdString with uid="SQDUAJWIER",Visible=.t., Left=2, Top=11,;
    Alignment=1, Width=115, Height=18,;
    Caption="Gruppo di query:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="WNLFGNCNTL",Visible=.t., Left=9, Top=70,;
    Alignment=0, Width=292, Height=18,;
    Caption="Processo documentale associato"    , BackStyle=1, BackColor=RGB(255,255,128);
  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_kgd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsir_kgd
procedure ChangeZoom(oObj)
   if g_DOCM='S'
     oObj.w_ZOOMQRY.cCpQueryName='..\IRDR\EXE\QUERY\GSIR1KGD'
   endif
endproc
* --- Fine Area Manuale
