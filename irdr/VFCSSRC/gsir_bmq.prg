* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bmq                                                        *
*              Modifica query                                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-27                                                      *
* Last revis.: 2016-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bmq",oParentObject,m.pParam)
return(i_retval)

define class tgsir_bmq as StdBatch
  * --- Local variables
  pParam = space(10)
  w_ZOOM = .NULL.
  w_QRPATH = space(254)
  w_DTFILE = space(254)
  w_IMPATH = space(254)
  w_LOG = space(254)
  w_ADOCS = space(254)
  w_CODICE = space(20)
  w_FORTYP = space(10)
  w_CONTYP = space(15)
  w_CURTYP = space(20)
  w_CURLOC = space(1)
  w_ENGTYP = space(1)
  w_TIMOUT = 0
  w_CMDTMO = 0
  w_EXT = space(5)
  w_CRIPTE = space(1)
  w_QUEFIL = space(1)
  w_FLGZCP = space(1)
  w_FLJOIN = space(1)
  w_CHG = space(1)
  * --- WorkFile variables
  INF_AQM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riporta modifiche  a tutte le query selezionate (GSIR_KMQ)
    this.w_ZOOM = this.oParentObject.w_ZOOMSEL
    do case
      case this.pParam="SELDES"
        * --- Seleziona / Deseleziona tutte le query presenti nell'oggettino Zoom della maschera
        UPDATE ( this.w_ZOOM.cCursor ) SET XCHK = IIF(this.oParentObject.w_SELEZI="S", 1, 0) 
 Select ( this.w_ZOOM.cCursor ) 
 Go Top
      otherwise
        this.w_CHG = "N"
        * --- Controllo quante righe ho selezionato e se ho selezionato almento un flag
         
 Select ( this.w_ZOOM.cCursor ) 
 Calculate CNT() For XCHK=1 To w_count
        if w_count=0 or (this.oParentObject.w_CHG1<>"S" and this.oParentObject.w_CHG2<>"S" and this.oParentObject.w_CHG3<>"S" and this.oParentObject.w_CHG4<>"S" and this.oParentObject.w_CHG5<>"S" and this.oParentObject.w_CHG6<>"S" and this.oParentObject.w_CHG7<>"S" and this.oParentObject.w_CHG8<>"S" and this.oParentObject.w_CHG9<>"S" and this.oParentObject.w_CHG10<>"S" and this.oParentObject.w_CHG11<>"S" and this.oParentObject.w_CHG12<>"S" and this.oParentObject.w_CHG14<>"S" and this.oParentObject.w_CHG15<>"S" and this.oParentObject.w_CHG16<>"S")
          ah_ErrorMsg("Nessuna query selezionata e/o nessun flag di modifica selezionato",,"")
        else
          if ah_YesNo("Vuoi applicare le modifiche a tutte le query selezionate?")
             
 Select ( this.w_ZOOM.cCursor ) 
 Go Top
            Scan For Xchk=1
            this.w_CODICE = AQCODICE
            * --- Leggo i vecchi valori
            * --- Read from INF_AQM
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.INF_AQM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_AQM_idx,2],.t.,this.INF_AQM_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AQQRPATH,AQDTFILE,AQIMPATH,AQLOG,AQADOCS,AQFORTYP,AQCONTYP,AQCURLOC,AQCURTYP,AQENGTYP,AQTIMOUT,AQCMDTMO,AQCRIPTE,AQQUEFIL,AQFLGZCP,AQFLJOIN"+;
                " from "+i_cTable+" INF_AQM where ";
                    +"AQCODICE = "+cp_ToStrODBC(this.w_CODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AQQRPATH,AQDTFILE,AQIMPATH,AQLOG,AQADOCS,AQFORTYP,AQCONTYP,AQCURLOC,AQCURTYP,AQENGTYP,AQTIMOUT,AQCMDTMO,AQCRIPTE,AQQUEFIL,AQFLGZCP,AQFLJOIN;
                from (i_cTable) where;
                    AQCODICE = this.w_CODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_QRPATH = NVL(cp_ToDate(_read_.AQQRPATH),cp_NullValue(_read_.AQQRPATH))
              this.w_DTFILE = NVL(cp_ToDate(_read_.AQDTFILE),cp_NullValue(_read_.AQDTFILE))
              this.w_IMPATH = NVL(cp_ToDate(_read_.AQIMPATH),cp_NullValue(_read_.AQIMPATH))
              this.w_LOG = NVL(cp_ToDate(_read_.AQLOG),cp_NullValue(_read_.AQLOG))
              this.w_ADOCS = NVL(cp_ToDate(_read_.AQADOCS),cp_NullValue(_read_.AQADOCS))
              this.w_FORTYP = NVL(cp_ToDate(_read_.AQFORTYP),cp_NullValue(_read_.AQFORTYP))
              this.w_CONTYP = NVL(cp_ToDate(_read_.AQCONTYP),cp_NullValue(_read_.AQCONTYP))
              this.w_CURLOC = NVL(cp_ToDate(_read_.AQCURLOC),cp_NullValue(_read_.AQCURLOC))
              this.w_CURTYP = NVL(cp_ToDate(_read_.AQCURTYP),cp_NullValue(_read_.AQCURTYP))
              this.w_ENGTYP = NVL(cp_ToDate(_read_.AQENGTYP),cp_NullValue(_read_.AQENGTYP))
              this.w_TIMOUT = NVL(cp_ToDate(_read_.AQTIMOUT),cp_NullValue(_read_.AQTIMOUT))
              this.w_CMDTMO = NVL(cp_ToDate(_read_.AQCMDTMO),cp_NullValue(_read_.AQCMDTMO))
              this.w_CRIPTE = NVL(cp_ToDate(_read_.AQCRIPTE),cp_NullValue(_read_.AQCRIPTE))
              this.w_QUEFIL = NVL(cp_ToDate(_read_.AQQUEFIL),cp_NullValue(_read_.AQQUEFIL))
              this.w_FLGZCP = NVL(cp_ToDate(_read_.AQFLGZCP),cp_NullValue(_read_.AQFLGZCP))
              this.w_FLJOIN = NVL(cp_ToDate(_read_.AQFLJOIN),cp_NullValue(_read_.AQFLJOIN))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Controllo se devo aggiornare i valori
            this.w_QRPATH = left(IIF(this.oParentObject.w_CHG1="S",IIF(this.oParentObject.w_CHG1F="F",this.oParentObject.w_AQQRPATH,SUBSTR(this.oParentObject.w_AQQRPATH,1,RAT("\",this.oParentObject.w_AQQRPATH))+SUBSTR(this.w_QRPATH,RAT("\",this.w_QRPATH)+1)),this.w_QRPATH),254)
            this.w_IMPATH = left(IIF(this.oParentObject.w_CHG3="S",IIF(this.oParentObject.w_CHG3F="F",this.oParentObject.w_AQIMPATH,SUBSTR(this.oParentObject.w_AQIMPATH,1,RAT("\",this.oParentObject.w_AQIMPATH))+SUBSTR(this.w_IMPATH,RAT("\",this.w_IMPATH)+1)),this.w_IMPATH),254)
            this.w_LOG = left(IIF(this.oParentObject.w_CHG4="S",IIF(this.oParentObject.w_CHG4F="F",this.oParentObject.w_AQLOG,SUBSTR(this.oParentObject.w_AQLOG,1,RAT("\",this.oParentObject.w_AQLOG))+SUBSTR(this.w_LOG,RAT("\",this.w_LOG)+1)),this.w_LOG),254)
            this.w_ADOCS = left(IIF(this.oParentObject.w_CHG5="S",this.oParentObject.w_AQADOCS,this.w_ADOCS),254)
            this.w_FORTYP = left(IIF(this.oParentObject.w_CHG6="S",this.oParentObject.w_AQFORTYP,this.w_FORTYP),10)
            this.w_EXT = IIF(this.w_FORTYP = "efQuery", "IRP", IIF(this.w_FORTYP = "efRTF", "RTF", IIF(this.w_FORTYP = "efExcel", "XLSX", IIF(this.w_FORTYP = "efHTML", "HTM", IIF(this.w_FORTYP = "efPDF", "PDF", "CSV")))))
            this.w_DTFILE = IIF( this.oParentObject.w_CHG6="S", MODIFEXT( this.w_DTFILE, this.w_EXT, .T. ), this.w_DTFILE )
            this.w_CONTYP = left(IIF(this.oParentObject.w_CHG8="S",this.oParentObject.w_AQCONTYP,this.w_CONTYP),15)
            this.w_CURTYP = left(IIF(this.oParentObject.w_CHG7="S",this.oParentObject.w_AQCURTYP,this.w_CURTYP),20)
            this.w_CURLOC = IIF(this.oParentObject.w_CHG10="S",this.oParentObject.w_AQCURLOC,this.w_CURLOC)
            this.w_ENGTYP = IIF(this.oParentObject.w_CHG9="S",this.oParentObject.w_AQENGTYP,this.w_ENGTYP)
            this.w_TIMOUT = IIF(this.oParentObject.w_CHG11="S",this.oParentObject.w_AQTIMOUT,this.w_TIMOUT)
            this.w_CMDTMO = IIF(this.oParentObject.w_CHG12="S",this.oParentObject.w_AQCMDTMO,this.w_CMDTMO)
            this.w_CRIPTE = IIF(this.oParentObject.w_CHG13="S",this.oParentObject.w_AQCRIPTE,this.w_CRIPTE)
            this.w_QUEFIL = IIF(this.oParentObject.w_CHG14="S",this.oParentObject.w_AQQUEFIL,this.w_QUEFIL)
            if this.w_QUEFIL<>"F"
              this.w_DTFILE = left(IIF(this.oParentObject.w_CHG2="S",IIF(this.oParentObject.w_CHG2F="F",this.oParentObject.w_AQDTFILE,SUBSTR(this.oParentObject.w_AQDTFILE,1,RAT("\",this.oParentObject.w_AQDTFILE))+SUBSTR(this.w_DTFILE,RAT("\",this.w_DTFILE)+1)),this.w_DTFILE),254)
              this.w_FLGZCP = IIF(this.oParentObject.w_CHG15="S",this.oParentObject.w_AQFLGZCP,this.w_FLGZCP)
            endif
            this.w_FLJOIN = IIF(this.oParentObject.w_CHG16="S",this.oParentObject.w_AQFLJOIN,this.w_FLJOIN)
            * --- Aggiorno i valori
            * --- Write into INF_AQM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.INF_AQM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INF_AQM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.INF_AQM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AQQRPATH ="+cp_NullLink(cp_ToStrODBC(this.w_QRPATH),'INF_AQM','AQQRPATH');
              +",AQDTFILE ="+cp_NullLink(cp_ToStrODBC(this.w_DTFILE),'INF_AQM','AQDTFILE');
              +",AQIMPATH ="+cp_NullLink(cp_ToStrODBC(this.w_IMPATH),'INF_AQM','AQIMPATH');
              +",AQLOG ="+cp_NullLink(cp_ToStrODBC(this.w_LOG),'INF_AQM','AQLOG');
              +",AQADOCS ="+cp_NullLink(cp_ToStrODBC(this.w_ADOCS),'INF_AQM','AQADOCS');
              +",AQFORTYP ="+cp_NullLink(cp_ToStrODBC(this.w_FORTYP),'INF_AQM','AQFORTYP');
              +",AQCONTYP ="+cp_NullLink(cp_ToStrODBC(this.w_CONTYP),'INF_AQM','AQCONTYP');
              +",AQCURLOC ="+cp_NullLink(cp_ToStrODBC(this.w_CURLOC),'INF_AQM','AQCURLOC');
              +",AQCURTYP ="+cp_NullLink(cp_ToStrODBC(this.w_CURTYP),'INF_AQM','AQCURTYP');
              +",AQENGTYP ="+cp_NullLink(cp_ToStrODBC(this.w_ENGTYP),'INF_AQM','AQENGTYP');
              +",AQTIMOUT ="+cp_NullLink(cp_ToStrODBC(this.w_TIMOUT),'INF_AQM','AQTIMOUT');
              +",AQCMDTMO ="+cp_NullLink(cp_ToStrODBC(this.w_CMDTMO),'INF_AQM','AQCMDTMO');
              +",AQCRIPTE ="+cp_NullLink(cp_ToStrODBC(this.w_CRIPTE),'INF_AQM','AQCRIPTE');
              +",AQQUEFIL ="+cp_NullLink(cp_ToStrODBC(this.w_QUEFIL),'INF_AQM','AQQUEFIL');
              +",AQFLGZCP ="+cp_NullLink(cp_ToStrODBC(this.w_FLGZCP),'INF_AQM','AQFLGZCP');
              +",AQFLJOIN ="+cp_NullLink(cp_ToStrODBC(this.w_FLJOIN),'INF_AQM','AQFLJOIN');
                  +i_ccchkf ;
              +" where ";
                  +"AQCODICE = "+cp_ToStrODBC(this.w_CODICE);
                     )
            else
              update (i_cTable) set;
                  AQQRPATH = this.w_QRPATH;
                  ,AQDTFILE = this.w_DTFILE;
                  ,AQIMPATH = this.w_IMPATH;
                  ,AQLOG = this.w_LOG;
                  ,AQADOCS = this.w_ADOCS;
                  ,AQFORTYP = this.w_FORTYP;
                  ,AQCONTYP = this.w_CONTYP;
                  ,AQCURLOC = this.w_CURLOC;
                  ,AQCURTYP = this.w_CURTYP;
                  ,AQENGTYP = this.w_ENGTYP;
                  ,AQTIMOUT = this.w_TIMOUT;
                  ,AQCMDTMO = this.w_CMDTMO;
                  ,AQCRIPTE = this.w_CRIPTE;
                  ,AQQUEFIL = this.w_QUEFIL;
                  ,AQFLGZCP = this.w_FLGZCP;
                  ,AQFLJOIN = this.w_FLJOIN;
                  &i_ccchkf. ;
               where;
                  AQCODICE = this.w_CODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            EndScan
          endif
        endif
        * --- Aggiorno la zoom
        This.oParentObject.notifyEvent("Aggiorna")
        ah_ErrorMsg("Aggiornamento terminato",,"")
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INF_AQM'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
