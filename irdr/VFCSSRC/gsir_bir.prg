* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bir                                                        *
*              Run InfoReader                                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_57]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-06                                                      *
* Last revis.: 2015-10-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODQUERY,w_OBJ,w_TIPO,w_GIROWNUM,w_ZOOM,w_NOMEFILE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bir",oParentObject,m.w_CODQUERY,m.w_OBJ,m.w_TIPO,m.w_GIROWNUM,m.w_ZOOM,m.w_NOMEFILE)
return(i_retval)

define class tgsir_bir as StdBatch
  * --- Local variables
  w_CODQUERY = space(20)
  w_OBJ = .NULL.
  w_TIPO = space(1)
  w_GIROWNUM = 0
  w_ZOOM = .f.
  w_NOMEFILE = space(254)
  w_PATH = space(254)
  w_PATHFILE = space(254)
  w_FILE = space(254)
  w_SYSPATH = space(254)
  w_FORTYP = space(10)
  w_QUEFIL = space(1)
  w_OBJECT = .NULL.
  w_CAPTION = space(254)
  w_PROCESSO_DOC = .f.
  w_IRDRDELAY = 0
  w_LOG = space(1)
  w_objFSO = .NULL.
  w_VERSION = space(15)
  * --- WorkFile variables
  INF_AQM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizzo il risultato della publicazione infopublisher
    *     Sulla base del formato di destinazione apro il programma associato:
    *     *.IRP -> InfoReader
    *     *.RTF,*.XLS, *.HTML, *.CSV -> Applicazione associata al tipo
    * --- w_CODQUERY : Codice query
    *     w_OBJ              : Oggetto che contiene gli eventuali parametri
    *     w_TIPO             : Tipo query filtrata/non filtrata
    *     w_GIROWNUM: Numero riga della gestione selezionata
    *     w_ZOOM          : Se true la query � stata lanciata da uno zoom
    * --- Paramtero se deve essere salvato il file invece che aperto
    this.w_NOMEFILE = iif(TYPE("w_NOMEFILE")<>"C","",this.w_NOMEFILE)
    * --- Valorizzo w_OBJECT
    this.w_OBJECT = this.w_OBJ
    * --- Leggo le informazioni della query
    * --- Read from INF_AQM
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INF_AQM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AQM_idx,2],.t.,this.INF_AQM_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AQDTFILE,AQFORTYP,AQQUEFIL,AQDES"+;
        " from "+i_cTable+" INF_AQM where ";
            +"AQCODICE = "+cp_ToStrODBC(this.w_CODQUERY);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AQDTFILE,AQFORTYP,AQQUEFIL,AQDES;
        from (i_cTable) where;
            AQCODICE = this.w_CODQUERY;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PATHFILE = NVL(cp_ToDate(_read_.AQDTFILE),cp_NullValue(_read_.AQDTFILE))
      this.w_FORTYP = NVL(cp_ToDate(_read_.AQFORTYP),cp_NullValue(_read_.AQFORTYP))
      this.w_QUEFIL = NVL(cp_ToDate(_read_.AQQUEFIL),cp_NullValue(_read_.AQQUEFIL))
      this.w_CAPTION = NVL(cp_ToDate(_read_.AQDES),cp_NullValue(_read_.AQDES))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_TIPO = "P" 
      this.w_PROCESSO_DOC = .t.
      this.w_TIPO = "F"
      USE IN SELECT("__pdtmp__")
    endif
    if this.w_TIPO = "F" or this.w_TIPO = "V"
      * --- Query Filtrata, lancio la pubblicazione per eseguire i filtri
      *     GSIR_BGD modifica w_PATHFILE affinch� contenga la query temporanea
      *     contenente il risultato
      _vfp.AutoYield = .t.
      GSIR_BGD(this, this.w_CODQUERY, "Q", .F. , .T., this.w_GIROWNUM , this.w_ZOOM)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if type("g_irdrdelay")="N"
        this.w_IRDRDELAY = g_IRDRDELAY
      else
        this.w_IRDRDELAY = 0.1
      endif
      SYS(2339 ,1) 
 SYS(1104) 
 inkey(this.w_irdrdelay, "H") 
 _vfp.AutoYield = .f. 
    endif
    if Directory(GETENV("WINDIR")+ "\SysWOW64\")
      * --- Ricavo il path della cartella SYSWOW64
      this.w_SYSPATH=GETENV("WINDIR")+ "\SysWOW64\"
    else
      * --- Ricavo il path della cartella SYSTEM32
      this.w_SYSPATH=GETENV("WINDIR")+ "\SYSTEM32\"
    endif
    this.w_objFSO=createobject("Scripting.FileSystemObject")
    this.w_VERSION = this.w_objFSO.GetFileVersion(this.w_SYSPATH+"InfoReader.ocx")
    this.w_objFSO = .NULL.
    if this.w_VERSION >= "2.7.1.0" And (Alltrim(this.w_FORTYP)<>"efQuery" And Alltrim(this.w_FORTYP)<>"efRTF" And Alltrim(this.w_FORTYP)<>"efPDF" And Alltrim(this.w_FORTYP)<>"efExcel")
      this.w_PATHFILE = FORCEEXT(this.w_PATHFILE, "7Z")
    endif
    this.w_FILE = iif(UPPER(RIGHT(ALLTRIM(this.w_PATHFILE),4))=".HTM",ALLTRIM(this.w_PATHFILE)+"L",this.w_PATHFILE)
    if FILE( this.w_FILE )
      if NOT EMPTY(this.w_NOMEFILE)
        if JustExt(this.w_FILE) = "7Z"
          * --- Se nuova versione cambio estensione nomefile
          this.w_NOMEFILE = FORCEEXT(this.w_NOMEFILE, "7Z")
          Ah_ErrorMsg("Creato file %1", "i", "", Alltrim(this.w_NOMEFILE))
        endif
        w_EXECUTECMD = 'COPY FILE "'+ this.w_FILE + '" TO "' + this.w_NOMEFILE + '"'
        &w_EXECUTECMD
        if UPPER(RIGHT(ALLTRIM(this.w_PATHFILE),4))=".HTM"
          this.w_FILE = LEFT(ALLTRIM(this.w_FILE),LEN(ALLTRIM(this.w_FILE))-5)+"_data.html"
          if FILE( this.w_FILE )
            w_EXECUTECMD = 'COPY FILE "'+ this.w_FILE + '" TO "' + substr(this.w_FILE,rat("\",this.w_FILE)+1) + '"'
            &w_EXECUTECMD
          endif
        endif
      else
        * --- Visualizzo la query
        do case
          case this.w_FORTYP = "efQuery" OR EMPTY(this.w_FORTYP)
            * --- Visualizzo la query con InfoReader
            if FILE(this.w_SYSPATH+"InfoReader.ocx")
              this.w_PATH = sys(5)+sys(2003)
              this.w_PATHFILE = cifrainfo(fullpath(this.w_PATHFILE),"C")
              do gsir_kir with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              cd (this.w_PATH)
            else
              ah_ErrorMsg("Attenzione: componenti ocx non installati.%0utilizzare la voce di men� <installazione componenti>","stop","")
            endif
          otherwise
            * --- Visualizzo la query con l'applicazione associata
            this.w_PATHFILE = iif(UPPER(RIGHT(ALLTRIM(this.w_PATHFILE),4))=".HTM",ALLTRIM(this.w_PATHFILE)+"L",this.w_PATHFILE)
            STAPDF(FULLPATH( this.w_PATHFILE ), "OPEN", " ", .T.)
        endcase
      endif
      if this.w_TIPO = "F" AND this.w_FORTYP = "efQuery" OR EMPTY(this.w_FORTYP)
        * --- Se query filtrata elimino il file generato solo nel caso si formato IRP, 
        *     negli altri poich� lancio una applicazione esterna non posso cancellare
        *     il file perch� viene visto come in uso.
        *     La maschera di InfoReader � Modale quindi la delete file viene eseguita 
        *     solo alla chiusura e quindi al rilascio del file
        if FILE( this.w_FILE )
          w_EXECUTECMD = 'DELETE FILE "'+ this.w_FILE+'"'
          &w_EXECUTECMD
        endif
      endif
    else
      * --- Mostro msg File non trovato se la query non � filtrata
      if this.w_TIPO <> "F"
        ah_ErrorMsg("File non trovato: %0 ( %1 )","stop","",ALLTRIM(FULLPATH(this.w_FILE)))
      endif
    endif
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,w_CODQUERY,w_OBJ,w_TIPO,w_GIROWNUM,w_ZOOM,w_NOMEFILE)
    this.w_CODQUERY=w_CODQUERY
    this.w_OBJ=w_OBJ
    this.w_TIPO=w_TIPO
    this.w_GIROWNUM=w_GIROWNUM
    this.w_ZOOM=w_ZOOM
    this.w_NOMEFILE=w_NOMEFILE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INF_AQM'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODQUERY,w_OBJ,w_TIPO,w_GIROWNUM,w_ZOOM,w_NOMEFILE"
endproc
