* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bca                                                        *
*              Caricamento report infobusiness esterni                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-23                                                      *
* Last revis.: 2013-08-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bca",oParentObject,m.pParam)
return(i_retval)

define class tgsir_bca as StdBatch
  * --- Local variables
  pParam = space(1)
  w_HOMEDIR = space(200)
  w_FILENUMBER = 0
  w_ZOOM = space(10)
  w_POSI = 0
  w_CAMPOFOCUS = .NULL.
  w_PATH = space(200)
  w_DATIEL = space(200)
  w_OPER = space(2)
  w_PATH = space(200)
  w_DATIEL = space(200)
  w_OPER = space(2)
  w_ERRORE = .f.
  w_AQCODICE = space(20)
  w_AQDTFILE = space(200)
  w_AQDES = space(200)
  w_AQADOCS = space(200)
  w_TIPCON = space(200)
  w_MART = space(200)
  w_APPDIR = space(200)
  w_APPMART = space(10)
  w_RIGA = 0
  w_CONTA = 0
  w_POSIZIONE = space(0)
  * --- WorkFile variables
  INF_AQM_idx=0
  FLU_RITO_idx=0
  LOG_REBA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_ZOOM = this.oParentObject.w_ZoomFile
    NC = this.w_ZOOM.cCursor
    do case
      case this.pParam="S"
        SELECT (NC)
        this.w_POSI = RECNO()
        if this.oParentObject.w_SELEZI="S"
          UPDATE &NC SET XCHK = 1
        else
          UPDATE &NC SET XCHK = 0
        endif
        if this.w_POSI>0 AND i_rows>0
          go this.w_POSI
        endif
        GO TOP
        this.w_zoom.grd.refresh()
      case this.pParam="O"
         
 #DEFINE HKEY_LOCAL_MACHINE 2147483650 
 #DEFINE SECURITY_ACCESS_MASK 983103 
 * dichirazione/assegnamento variabili 
 lpType=0 
 lpcbData = 250 
 lpData = SPACE(lpcbData) 
 nKeyHandle = 0 
 nKeyPos = 0
        if isahe()
          if RegOpenKey(HKEY_LOCAL_MACHINE,"SOFTWARE\InfoBusiness\AHE", @nKeyHandle) = 0
            ret = RegQueryValueEx(nKeyHandle,"Install_Dir",0,0,@lpdata,@lpcbData) 
 RegCloseKey(nKeyHandle)
            this.w_HOMEDIR = iif(Len(alltrim(lpdata))=0," ",substr(lpdata,1,lpcbData))
            this.w_HOMEDIR = ADDBS(this.w_HOMEDIR)+"REPORT\"
          else
            this.w_HOMEDIR = this.oParentObject.w_HDIR
          endif
        else
          if RegOpenKey(HKEY_LOCAL_MACHINE,"SOFTWARE\InfoBusiness\AHR", @nKeyHandle) = 0
            ret = RegQueryValueEx(nKeyHandle,"Install_Dir",0,0,@lpdata,@lpcbData) 
 RegCloseKey(nKeyHandle)
            this.w_HOMEDIR = iif(Len(alltrim(lpdata))=0," ",substr(lpdata,1,lpcbData))
            this.w_HOMEDIR = ADDBS(this.w_HOMEDIR)+"REPORT\"
          else
            this.w_HOMEDIR = this.oParentObject.w_HDIR
          endif
        endif
        Messaggio = "" 
 ON ERROR Messaggio = "Path errato"
        ON ERROR
        if NOT EMPTY(Messaggio)
          Ah_ErrorMSG(Messaggio,48)
          Messaggio = "" 
 ON ERROR Messaggio = "Path errato"
        endif
        if UPPER(THIS.OPARENTOBJECT.CURRENTEVENT)<>"W_DIRECTORI CHANGED"
          this.oParentObject.w_DIR = cp_GetDIR(this.w_HOMEDIR)
          this.w_CAMPOFOCUS = THIS.OPARENTOBJECT.GETCTRL("w_DIRECTORI")
          this.w_CAMPOFOCUS.SetFocus()     
        else
          if right(alltrim(this.oParentObject.w_DIRECTORI),1)="\"
            this.oParentObject.w_DIR = alltrim(this.oParentObject.w_DIRECTORI)
          else
            this.oParentObject.w_DIR = alltrim(this.oParentObject.w_DIRECTORI)+"\"
          endif
        endif
        if EMPTY(this.oParentObject.w_DIR)
          cd (this.oParentObject.w_HDIR)
          ZAP IN ( this.w_ZOOM.cCursor )
          delete from (NC) where 1=1
          this.oParentObject.w_DIRECTORI = ""
          i_retcode = 'stop'
          return
        else
          * --- Pulisce il cursore della maschera GSIR_KCA prima di inserirvi nuovi dati  
          ZAP IN ( this.w_ZOOM.cCursor )
          delete from (NC) where 1=1
        endif
        * --- Visualizza la directori selezionata all interno della maschera GSIR_KCA
        this.oParentObject.w_DIRECTORI = this.oParentObject.w_DIR
        * --- Si posiziona sulla directory selezionata (per leggere i file tramite l istruzione ADIR (...)
        *     Legge i nomi dei file  
        *     Rispristina la directory di lavoro 
        this.w_FILENUMBER = ADIR(NOMIFILE, ADDBS(this.oParentObject.w_DIR)+"*.IRP") && Create array
        if this.w_FILENUMBER=0
          i_retcode = 'stop'
          return
        endif
        * --- Crea un cursore e vi inserisce tutti i file della directory selezionata 
        CD(this.oParentObject.w_HDIR)
        CREATE CURSOR CursFILE (DATIEL C(200), DIR C(200),XCHK N(1),AQADOCS C(200))
        APPEND FROM ARRAY NOMIFILE
        UPDATE CURSFILE SET AQADOCS=SPACE(200)
        update CursFILE set DIR=this.oparentobject.w_DIR where 1=1
        ZAP IN ( this.w_ZOOM.cCursor )
        * --- Pulisce il cursore della maschera GSIR_KCA prima di inserirvi nuovi dati  
        delete from (NC) where 1=1
        * --- I file vengono ricopiati all interno della maschera GSIR_KCA
        SELECT CursFILE 
 GO TOP 
 SCAN
        INSERT INTO (NC) Values (CursFILE.DATIEL, CursFILE.DATIEL,CursFILE.DIR,CursFILE.AQADOCS,CursFILE.XCHK)
        ENDSCAN
        if USED ("CursCARICO")
          SELECT CursCARICO 
 USE
        endif
        SELECT (NC)
        if reccount()>0
          go 1
        endif
        this.w_zoom.grd.refresh()
        i_retcode = 'stop'
        return
      case this.pParam="I"
         
 #DEFINE HKEY_LOCAL_MACHINE 2147483650 
 #DEFINE SECURITY_ACCESS_MASK 983103 
 * dichirazione/assegnamento variabili 
 lpType=0 
 lpcbData = 250 
 lpData = SPACE(lpcbData) 
 nKeyHandle = 0 
 nKeyPos = 0
        if isahe()
          if RegOpenKey(HKEY_LOCAL_MACHINE,"SOFTWARE\InfoBusiness\AHE", @nKeyHandle) = 0
             
 ret = RegQueryValueEx(nKeyHandle,"Install_Dir",0,0,@lpdata,@lpcbData) 
 RegCloseKey(nKeyHandle)
            this.w_HOMEDIR = iif(Len(alltrim(lpdata))=0," ",substr(lpdata,1,lpcbData))
            this.w_HOMEDIR = ADDBS(this.w_HOMEDIR)+"REPORT\"
          else
            this.w_HOMEDIR = this.oParentObject.w_HDIR
          endif
        else
          if RegOpenKey(HKEY_LOCAL_MACHINE,"SOFTWARE\InfoBusiness\AHR", @nKeyHandle) = 0
             
 ret = RegQueryValueEx(nKeyHandle,"Install_Dir",0,0,@lpdata,@lpcbData) 
 RegCloseKey(nKeyHandle)
            this.w_HOMEDIR = iif(Len(alltrim(lpdata))=0," ",substr(lpdata,1,lpcbData))
            this.w_HOMEDIR = ADDBS(this.w_HOMEDIR)+"REPORT\"
          else
            this.w_HOMEDIR = this.oParentObject.w_HDIR
          endif
        endif
        this.oParentObject.w_DIRECTORI = this.w_HOMEDIR
        this.oParentObject.w_DIR = this.w_HOMEDIR
        * --- Si posiziona sulla directory selezionata (per leggere i file tramite l istruzione ADIR (...)
        *     Legge i nomi dei file  
        *     Rispristina la directory di lavoro 
        this.w_FILENUMBER = ADIR(NOMIFILE, ADDBS(this.oParentObject.w_DIR)+"*.IRP") && Create array
        if this.w_FILENUMBER=0
          i_retcode = 'stop'
          return
        endif
        * --- Crea un cursore e vi inserisce tutti i file della directory selezionata 
        CD(this.oParentObject.w_HDIR)
        CREATE CURSOR CursFILE (DATIEL C(200), DIR C(200),XCHK N(1),AQADOCS C(200))
        APPEND FROM ARRAY NOMIFILE
        UPDATE CURSFILE SET AQADOCS=SPACE(200)
        update CursFILE set DIR=this.oparentobject.w_DIR where 1=1
        ZAP IN ( this.w_ZOOM.cCursor )
        * --- Pulisce il cursore della maschera GSIR_KCA prima di inserirvi nuovi dati  
        delete from (NC) where 1=1
        * --- I file vengono ricopiati all interno della maschera GSIR_KCA
        SELECT CursFILE 
 GO TOP 
 SCAN
        INSERT INTO (NC) Values (CursFILE.DATIEL, CursFILE.DATIEL,CursFILE.DIR,CursFILE.AQADOCS,CursFILE.XCHK)
        ENDSCAN
        if USED ("CursCARICO")
          SELECT CursCARICO 
 USE
        endif
        SELECT (NC)
        if reccount()>0
          go 1
        endif
        this.w_zoom.grd.refresh()
        i_retcode = 'stop'
        return
      case this.pParam="C"
        this.w_APPDIR = STREXTRACT(THIS.OPARENTOBJECT.w_DIRECTORI, "","REPORT",0,1)
        this.w_TIPCON = CP_DBTYPE
        this.w_ERRORE = .T.
        if EMPTY (alltrim(NVL(this.oParentObject.w_DIRECTORI," ")))
          i_retcode = 'stop'
          return
        endif
         
 #DEFINE HKEY_LOCAL_MACHINE 2147483650 
 #DEFINE SECURITY_ACCESS_MASK 983103 
 * dichirazione/assegnamento variabili 
 lpType=0 
 lpcbData = 250 
 lpData = SPACE(lpcbData) 
 nKeyHandle = 0 
 nKeyPos = 0
        if isahe()
          if RegOpenKey(HKEY_LOCAL_MACHINE,"SOFTWARE\InfoBusiness\AHE", @nKeyHandle) = 0
             
 ret = RegQueryValueEx(nKeyHandle,"Install_Dir",0,0,@lpdata,@lpcbData) 
 RegCloseKey(nKeyHandle)
            this.w_MART = iif(Len(alltrim(lpdata))=0," ",substr(lpdata,1,lpcbData))
            this.w_APPMART = ADDBS(this.w_MART)+" "
            if ALLTRIM(upper(this.w_APPMART))==alltrim(upper(this.w_APPDIR))
              this.w_MART = ADDBS(this.w_MART)+"MART\AHEETL.mrt"
            else
              this.w_MART = ""
            endif
          else
            this.w_MART = ""
          endif
        else
          if RegOpenKey(HKEY_LOCAL_MACHINE,"SOFTWARE\InfoBusiness\AHR", @nKeyHandle) = 0
             
 ret = RegQueryValueEx(nKeyHandle,"Install_Dir",0,0,@lpdata,@lpcbData) 
 RegCloseKey(nKeyHandle)
            this.w_MART = iif(Len(alltrim(lpdata))=0," ",substr(lpdata,1,lpcbData))
            this.w_APPMART = ADDBS(this.w_MART)+" "
            if ALLTRIM(upper(this.w_APPMART))==alltrim(upper(this.w_APPDIR))
              this.w_MART = ADDBS(this.w_MART)+"MART\AHRETL.mrt"
            else
              this.w_MART = ""
            endif
          else
            this.w_MART = ""
          endif
        endif
        * --- Try
        local bErr_04A68AC8
        bErr_04A68AC8=bTrsErr
        this.Try_04A68AC8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          Ah_ErrorMSG(i_errmsg,24)
          this.w_ERRORE = .F.
        endif
        bTrsErr=bTrsErr or bErr_04A68AC8
        * --- End
        if !this.w_ERRORE
          Ah_ErrorMSG("Progressivo gi� presente.%0Aggiornare manualmente nella gestione progressivi",24)
        endif
        if this.w_ERRORE AND RECCOUNT("CursCarica")<>0
          this.w_CONTA = RECCOUNT("CursCarica")
          if Ah_Yesno("Operazione completata: Generate n. %1 anagrafiche query.%0Si desidera visualizzare la stampa riassuntiva delle anagrafiche generate?",64,this.w_CONTA)
            SELECT * FROM __stampa__ INTO CURSOR __tmp__
            CP_CHPRN ("..\IRDR\EXE\QUERY\GSIR_KCA.FRX","",this)
          endif
          Select (NC) 
          UPDATE (NC) SET Xchk=0
        endif
        if USED ("CursCARICA")
          SELECT CursCARICA 
 USE
        endif
        if USED ("__stampa__")
          SELECT __stampa__ 
 USE
        endif
      case this.pParam="A"
        Select (NC) 
        this.w_POSIZIONE = Recno()
        UPDATE (NC) SET AQADOCS=this.oparentobject.w_AQADOCS where Xchk=1
        UPDATE (NC) SET AQADOCS=" " where Xchk=0
        GO this.w_POSIZIONE
    endcase
  endproc
  proc Try_04A68AC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT * FROM (NC) WHERE XCHK=1 INTO CURSOR CursCARICA 
    SELECT CursCARICA 
 GO TOP
    if RECCOUNT("CursCarica")=0
      Messaggio = "Non � stato selezionato alcun file"
      Ah_ErrorMSG(Messaggio,48)
      i_retcode = 'stop'
      return
    endif
    SELECT *,SPACE(20) as CHIAVE FROM CursCarica INTO CURSOR __stampa__
    =wrcursor ( "__stampa__" )
    SELECT CursCARICA
    SCAN 
    this.w_AQDTFILE = ALLTRIM(CursCARICA.DIR)+ALLTRIM(CursCARICA.NOME)
    this.w_AQDES = CursCARICA.DESCRI
    this.w_AQDES = CursCARICA.DESCRI
    this.oParentObject.w_AQADOCS = CursCARICA.AQADOCS
    this.w_AQCODICE = space(20)
    this.w_RIGA = RECNO("CursCARICA")
    i_Conn=i_TableProp[this.INF_AQM_IDX, 3]
    cp_NextTableProg(THIS, i_Conn, "CARREP", "i_CODAZI,w_AQCODICE")
    SELECT __stampa__
    GO this.w_RIGA
    REPLACE CHIAVE WITH this.w_AQCODICE 
    * --- Insert into INF_AQM
    i_nConn=i_TableProp[this.INF_AQM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INF_AQM_idx,2])
    i_commit = .f.
    local bErr_04A8B728
    bErr_04A8B728=bTrsErr
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INF_AQM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AQCODICE"+",AQDTFILE"+",AQDES"+",AQGENAUT"+",AQCONTYP"+",AQIMPATH"+",AQQRPATH"+",AQADOCS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_AQCODICE),'INF_AQM','AQCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AQDTFILE),'INF_AQM','AQDTFILE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AQDES),'INF_AQM','AQDES');
      +","+cp_NullLink(cp_ToStrODBC("S"),'INF_AQM','AQGENAUT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'INF_AQM','AQCONTYP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MART),'INF_AQM','AQIMPATH');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AQDTFILE),'INF_AQM','AQQRPATH');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AQADOCS),'INF_AQM','AQADOCS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AQCODICE',this.w_AQCODICE,'AQDTFILE',this.w_AQDTFILE,'AQDES',this.w_AQDES,'AQGENAUT',"S",'AQCONTYP',this.w_TIPCON,'AQIMPATH',this.w_MART,'AQQRPATH',this.w_AQDTFILE,'AQADOCS',this.oParentObject.w_AQADOCS)
      insert into (i_cTable) (AQCODICE,AQDTFILE,AQDES,AQGENAUT,AQCONTYP,AQIMPATH,AQQRPATH,AQADOCS &i_ccchkf. );
         values (;
           this.w_AQCODICE;
           ,this.w_AQDTFILE;
           ,this.w_AQDES;
           ,"S";
           ,this.w_TIPCON;
           ,this.w_MART;
           ,this.w_AQDTFILE;
           ,this.oParentObject.w_AQADOCS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      bTrsErr=bErr_04A8B728
      this.w_ERRORE = .F.
    endif
    do GSIR_BTV with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    SELECT CursCARICA
    GO this.w_RIGA
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='INF_AQM'
    this.cWorkTables[2]='FLU_RITO'
    this.cWorkTables[3]='LOG_REBA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
