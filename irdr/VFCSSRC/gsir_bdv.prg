* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bdv                                                        *
*              Check form gsir_mdv - riservato a                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_38]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-06                                                      *
* Last revis.: 2005-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pMOD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bdv",oParentObject,m.pMOD)
return(i_retval)

define class tgsir_bdv as StdBatch
  * --- Local variables
  pMOD = space(1)
  w_PUNPAD = .NULL.
  w_oPOS = 0
  w_CURSOR = space(10)
  w_TMPN = 0
  w_MSG = space(254)
  w_PUNPAD = .NULL.
  w_CURSOR = space(10)
  w_TMPN = space(15)
  w_TMPN1 = space(5)
  w_TMPN2 = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CheckForm
    if this.oParentObject.oParentObject.w_AQFLGZCP = "S" and g_IZCP$"SA"
      do case
        case this.pMOD = "M"
          * --- Inizializzazioni ...
          this.w_PUNPAD = this.oParentObject
          this.w_CURSOR = this.w_PUNPAD.cTrsname
          * --- Memorizza posizione ...
          Select (this.w_CURSOR)
          this.w_oPOS = RECNO()
          * --- Verifica 1 - E' stato indicato un folder 
          if this.oParentObject.w_DEFOLDER=0
            this.w_MSG = ah_MsgFormat("Corporate Portal: occorre indicare un codice CPZ folder")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MSG
            i_retcode = 'stop'
            return
          endif
          * --- Verifica 2 - Se � stato indicato un folder di tipo company, occorre verificare che sia presente una sola riga con il flag principale attivato
          this.w_TMPN = 0
          if this.oParentObject.w_FO__TIPO="C"
            Select (this.w_CURSOR)
            SUM t_DERIFCFO TO this.w_TMPN 
            if this.w_TMPN=0 and this.oParentObject.w_DERISERV="S"
              this.w_MSG = ah_MsgFormat("Corporate Portal: occorre indicare il rif.<principale> necessario per identificare il company folder%0Tale riferimento pu� essere specificato solo su righe destinatari tipo cliente/fornitore o agente")
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MSG
              i_retcode = 'stop'
              return
            else
              if this.w_TMPN>1 and this.oParentObject.w_DERISERV="S"
                this.w_MSG = ah_MsgFormat("Corporate Portal: esistono due o pi� righe con il flag <principale> attivato")
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=this.w_MSG
                i_retcode = 'stop'
                return
              endif
            endif
          endif
          * --- Ripristina ...
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.pMOD = "S"
          * --- Controllo che sia stato inserito almeno una riga di permessi
          this.w_PUNPAD = this.oParentObject
          this.w_CURSOR = this.w_PUNPAD.cTrsname
          Select (this.w_CURSOR)
          this.w_oPOS = RECNO()
          CLIFOR=.f.
          Select (this.w_CURSOR)
          Go top 
 scan 
          if (empty(t_DECODGRU) and t_DETIPDES=4) or (empty(t_DECODCON) AND between(t_DETIPDES,1,2)) or (empty(t_DECODAGE) AND t_DETIPDES=3)
            CLIFOR=.t.
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            exit
          endif
          endscan
          if CLIFOR and this.oParentObject.w_DERISERV = "S" 
            this.oParentObject.w_PERMESSO = .F.
          else
            this.oParentObject.w_PERMESSO = .T.
          endif
          * --- Ripristina ...
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
    else
      this.oParentObject.w_PERMESSO = .T.
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- riposiziona cursore
    Select (this.w_CURSOR)
    if this.w_oPOS>0 AND this.w_oPOS<=RECCOUNT()
      Select (this.w_CURSOR) 
 GOTO this.w_oPOS
    endif
  endproc


  proc Init(oParentObject,pMOD)
    this.pMOD=pMOD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pMOD"
endproc
