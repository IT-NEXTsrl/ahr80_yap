* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_kmq                                                        *
*              Modifica query                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-27                                                      *
* Last revis.: 2016-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsir_kmq",oParentObject))

* --- Class definition
define class tgsir_kmq as StdForm
  Top    = -2
  Left   = 2

  * --- Standard Properties
  Width  = 796
  Height = 369+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-12"
  HelpContextID=199847785
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=38

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsir_kmq"
  cComment = "Modifica query"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODQUERY = space(20)
  o_CODQUERY = space(20)
  w_SELEZI = space(1)
  w_AQQRPATH = space(254)
  w_CHG1 = space(1)
  o_CHG1 = space(1)
  w_CHG1F = space(1)
  w_AQDTFILE = space(254)
  w_CHG2 = space(1)
  o_CHG2 = space(1)
  w_CHG2F = space(1)
  w_AQIMPATH = space(254)
  w_CHG3 = space(1)
  o_CHG3 = space(1)
  w_CHG3F = space(1)
  w_AQLOG = space(254)
  w_CHG4 = space(1)
  o_CHG4 = space(1)
  w_CHG4F = space(1)
  w_AQCRIPTE = space(1)
  w_CHG13 = space(1)
  o_CHG13 = space(1)
  w_AQADOCS = space(254)
  w_CHG5 = space(1)
  o_CHG5 = space(1)
  w_AQFORTYP = space(10)
  w_CHG6 = space(1)
  w_AQCURTYP = space(20)
  w_CHG7 = space(1)
  w_AQCONTYP = space(15)
  w_CHG8 = space(1)
  w_AQENGTYP = space(1)
  w_CHG9 = space(1)
  w_AQCURLOC = space(1)
  w_CHG10 = space(1)
  w_AQTIMOUT = 0
  w_CHG11 = space(1)
  w_AQCMDTMO = 0
  w_CHG12 = space(1)
  w_AQQUEFIL = space(1)
  o_AQQUEFIL = space(1)
  w_CHG14 = space(1)
  w_AQFLJOIN = space(1)
  w_CHG16 = space(1)
  w_AQFLGZCP = space(1)
  w_CHG15 = space(1)
  w_ZOOMSEL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_kmqPag1","gsir_kmq",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Query")
      .Pages(2).addobject("oPag","tgsir_kmqPag2","gsir_kmq",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMSEL = this.oPgFrm.Pages(1).oPag.ZOOMSEL
    DoDefault()
    proc Destroy()
      this.w_ZOOMSEL = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSIR_BMQ(this,"OK")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODQUERY=space(20)
      .w_SELEZI=space(1)
      .w_AQQRPATH=space(254)
      .w_CHG1=space(1)
      .w_CHG1F=space(1)
      .w_AQDTFILE=space(254)
      .w_CHG2=space(1)
      .w_CHG2F=space(1)
      .w_AQIMPATH=space(254)
      .w_CHG3=space(1)
      .w_CHG3F=space(1)
      .w_AQLOG=space(254)
      .w_CHG4=space(1)
      .w_CHG4F=space(1)
      .w_AQCRIPTE=space(1)
      .w_CHG13=space(1)
      .w_AQADOCS=space(254)
      .w_CHG5=space(1)
      .w_AQFORTYP=space(10)
      .w_CHG6=space(1)
      .w_AQCURTYP=space(20)
      .w_CHG7=space(1)
      .w_AQCONTYP=space(15)
      .w_CHG8=space(1)
      .w_AQENGTYP=space(1)
      .w_CHG9=space(1)
      .w_AQCURLOC=space(1)
      .w_CHG10=space(1)
      .w_AQTIMOUT=0
      .w_CHG11=space(1)
      .w_AQCMDTMO=0
      .w_CHG12=space(1)
      .w_AQQUEFIL=space(1)
      .w_CHG14=space(1)
      .w_AQFLJOIN=space(1)
      .w_CHG16=space(1)
      .w_AQFLGZCP=space(1)
      .w_CHG15=space(1)
      .oPgFrm.Page1.oPag.ZOOMSEL.Calculate()
        .w_CODQUERY = .w_ZOOMSEL.getvar('AQCODICE')
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .w_AQQRPATH = .w_ZOOMSEL.getvar('AQQRPATH')
      .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .w_CHG1 = 'N'
        .w_CHG1F = 'P'
        .w_AQDTFILE = .w_ZOOMSEL.getvar('AQDTFILE')
      .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .w_CHG2 = 'N'
        .w_CHG2F = 'P'
        .w_AQIMPATH = .w_ZOOMSEL.getvar('AQIMPATH')
      .oPgFrm.Page2.oPag.oObj_2_10.Calculate()
        .w_CHG3 = 'N'
        .w_CHG3F = 'P'
        .w_AQLOG = .w_ZOOMSEL.getvar('AQLOG')
      .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .w_CHG4 = 'N'
        .w_CHG4F = 'P'
        .w_AQCRIPTE = .w_ZOOMSEL.getvar('AQCRIPTE')
        .w_CHG13 = .w_CHG5
        .w_AQADOCS = .w_ZOOMSEL.getvar('AQADOCS')
        .w_CHG5 = .w_CHG13
        .w_AQFORTYP = NVL( .w_ZOOMSEL.getvar('AQFORTYP') ,'efQuery' )
        .w_CHG6 = 'N'
        .w_AQCURTYP = NVL( .w_ZOOMSEL.getvar('AQCURTYP'), 'ctStatic' )
        .w_CHG7 = 'N'
        .w_AQCONTYP = NVL( .w_ZOOMSEL.getvar('AQCONTYP'), CP_DBTYPE)
        .w_CHG8 = 'N'
        .w_AQENGTYP = NVL( .w_ZOOMSEL.getvar('AQENGTYP'), 'R' )
        .w_CHG9 = 'N'
        .w_AQCURLOC = NVL( .w_ZOOMSEL.getvar('AQCURLOC'), 'C' )
        .w_CHG10 = 'N'
        .w_AQTIMOUT = NVL( .w_ZOOMSEL.getvar('AQTIMOUT'), 30 )
        .w_CHG11 = 'N'
        .w_AQCMDTMO = NVL( .w_ZOOMSEL.getvar('AQTIMOUT'), 30 )
        .w_CHG12 = 'N'
        .w_AQQUEFIL = NVL( .w_ZOOMSEL.getvar('AQQUEFIL'), 'S' )
        .w_CHG14 = 'N'
        .w_AQFLJOIN = .w_ZOOMSEL.getvar('AQFLJOIN')
        .w_CHG16 = 'N'
        .w_AQFLGZCP = NVL( .w_ZOOMSEL.getvar('AQQUEFIL'), 'N' )
        .w_CHG15 = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_54.enabled = this.oPgFrm.Page2.oPag.oBtn_2_54.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_55.enabled = this.oPgFrm.Page2.oPag.oBtn_2_55.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_57.enabled = this.oPgFrm.Page2.oPag.oBtn_2_57.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_58.enabled = this.oPgFrm.Page2.oPag.oBtn_2_58.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMSEL.Calculate()
            .w_CODQUERY = .w_ZOOMSEL.getvar('AQCODICE')
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .DoRTCalc(2,2,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQQRPATH = .w_ZOOMSEL.getvar('AQQRPATH')
        endif
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .DoRTCalc(4,4,.t.)
        if .o_CHG1<>.w_CHG1
            .w_CHG1F = 'P'
        endif
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQDTFILE = .w_ZOOMSEL.getvar('AQDTFILE')
        endif
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        if .o_AQQUEFIL<>.w_AQQUEFIL
            .w_CHG2 = 'N'
        endif
        if .o_CHG2<>.w_CHG2.or. .o_AQQUEFIL<>.w_AQQUEFIL
            .w_CHG2F = 'P'
        endif
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQIMPATH = .w_ZOOMSEL.getvar('AQIMPATH')
        endif
        .oPgFrm.Page2.oPag.oObj_2_10.Calculate()
        .DoRTCalc(10,10,.t.)
        if .o_CHG3<>.w_CHG3
            .w_CHG3F = 'P'
        endif
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQLOG = .w_ZOOMSEL.getvar('AQLOG')
        endif
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .DoRTCalc(13,13,.t.)
        if .o_CHG4<>.w_CHG4
            .w_CHG4F = 'P'
        endif
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQCRIPTE = .w_ZOOMSEL.getvar('AQCRIPTE')
        endif
        if .o_CHG5<>.w_CHG5
            .w_CHG13 = .w_CHG5
        endif
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQADOCS = .w_ZOOMSEL.getvar('AQADOCS')
        endif
        if .o_CHG13<>.w_CHG13
            .w_CHG5 = .w_CHG13
        endif
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQFORTYP = NVL( .w_ZOOMSEL.getvar('AQFORTYP') ,'efQuery' )
        endif
        .DoRTCalc(20,20,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQCURTYP = NVL( .w_ZOOMSEL.getvar('AQCURTYP'), 'ctStatic' )
        endif
        .DoRTCalc(22,22,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQCONTYP = NVL( .w_ZOOMSEL.getvar('AQCONTYP'), CP_DBTYPE)
        endif
        .DoRTCalc(24,24,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQENGTYP = NVL( .w_ZOOMSEL.getvar('AQENGTYP'), 'R' )
        endif
        .DoRTCalc(26,26,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQCURLOC = NVL( .w_ZOOMSEL.getvar('AQCURLOC'), 'C' )
        endif
        .DoRTCalc(28,28,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQTIMOUT = NVL( .w_ZOOMSEL.getvar('AQTIMOUT'), 30 )
        endif
        .DoRTCalc(30,30,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQCMDTMO = NVL( .w_ZOOMSEL.getvar('AQTIMOUT'), 30 )
        endif
        .DoRTCalc(32,32,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQQUEFIL = NVL( .w_ZOOMSEL.getvar('AQQUEFIL'), 'S' )
        endif
        .DoRTCalc(34,34,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQFLJOIN = .w_ZOOMSEL.getvar('AQFLJOIN')
        endif
        .DoRTCalc(36,36,.t.)
        if .o_CODQUERY<>.w_CODQUERY
            .w_AQFLGZCP = NVL( .w_ZOOMSEL.getvar('AQQUEFIL'), 'N' )
        endif
        if .o_AQQUEFIL<>.w_AQQUEFIL
            .w_CHG15 = 'N'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMSEL.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_10.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oAQDTFILE_2_5.enabled = this.oPgFrm.Page2.oPag.oAQDTFILE_2_5.mCond()
    this.oPgFrm.Page2.oPag.oCHG2_2_7.enabled = this.oPgFrm.Page2.oPag.oCHG2_2_7.mCond()
    this.oPgFrm.Page2.oPag.oAQFLGZCP_2_40.enabled = this.oPgFrm.Page2.oPag.oAQFLGZCP_2_40.mCond()
    this.oPgFrm.Page2.oPag.oCHG15_2_41.enabled = this.oPgFrm.Page2.oPag.oCHG15_2_41.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_57.enabled = this.oPgFrm.Page2.oPag.oBtn_2_57.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_58.enabled = this.oPgFrm.Page2.oPag.oBtn_2_58.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oCHG1F_2_4.visible=!this.oPgFrm.Page2.oPag.oCHG1F_2_4.mHide()
    this.oPgFrm.Page2.oPag.oCHG2F_2_8.visible=!this.oPgFrm.Page2.oPag.oCHG2F_2_8.mHide()
    this.oPgFrm.Page2.oPag.oCHG3F_2_12.visible=!this.oPgFrm.Page2.oPag.oCHG3F_2_12.mHide()
    this.oPgFrm.Page2.oPag.oCHG4F_2_16.visible=!this.oPgFrm.Page2.oPag.oCHG4F_2_16.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_57.visible=!this.oPgFrm.Page2.oPag.oBtn_2_57.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_58.visible=!this.oPgFrm.Page2.oPag.oBtn_2_58.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMSEL.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_2.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_6.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_10.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_14.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_6.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQQRPATH_2_1.value==this.w_AQQRPATH)
      this.oPgFrm.Page2.oPag.oAQQRPATH_2_1.value=this.w_AQQRPATH
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG1_2_3.RadioValue()==this.w_CHG1)
      this.oPgFrm.Page2.oPag.oCHG1_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG1F_2_4.RadioValue()==this.w_CHG1F)
      this.oPgFrm.Page2.oPag.oCHG1F_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQDTFILE_2_5.value==this.w_AQDTFILE)
      this.oPgFrm.Page2.oPag.oAQDTFILE_2_5.value=this.w_AQDTFILE
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG2_2_7.RadioValue()==this.w_CHG2)
      this.oPgFrm.Page2.oPag.oCHG2_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG2F_2_8.RadioValue()==this.w_CHG2F)
      this.oPgFrm.Page2.oPag.oCHG2F_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQIMPATH_2_9.value==this.w_AQIMPATH)
      this.oPgFrm.Page2.oPag.oAQIMPATH_2_9.value=this.w_AQIMPATH
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG3_2_11.RadioValue()==this.w_CHG3)
      this.oPgFrm.Page2.oPag.oCHG3_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG3F_2_12.RadioValue()==this.w_CHG3F)
      this.oPgFrm.Page2.oPag.oCHG3F_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQLOG_2_13.value==this.w_AQLOG)
      this.oPgFrm.Page2.oPag.oAQLOG_2_13.value=this.w_AQLOG
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG4_2_15.RadioValue()==this.w_CHG4)
      this.oPgFrm.Page2.oPag.oCHG4_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG4F_2_16.RadioValue()==this.w_CHG4F)
      this.oPgFrm.Page2.oPag.oCHG4F_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQCRIPTE_2_17.RadioValue()==this.w_AQCRIPTE)
      this.oPgFrm.Page2.oPag.oAQCRIPTE_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG13_2_18.RadioValue()==this.w_CHG13)
      this.oPgFrm.Page2.oPag.oCHG13_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQADOCS_2_19.value==this.w_AQADOCS)
      this.oPgFrm.Page2.oPag.oAQADOCS_2_19.value=this.w_AQADOCS
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG5_2_21.RadioValue()==this.w_CHG5)
      this.oPgFrm.Page2.oPag.oCHG5_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQFORTYP_2_22.RadioValue()==this.w_AQFORTYP)
      this.oPgFrm.Page2.oPag.oAQFORTYP_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG6_2_23.RadioValue()==this.w_CHG6)
      this.oPgFrm.Page2.oPag.oCHG6_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQCURTYP_2_24.RadioValue()==this.w_AQCURTYP)
      this.oPgFrm.Page2.oPag.oAQCURTYP_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG7_2_25.RadioValue()==this.w_CHG7)
      this.oPgFrm.Page2.oPag.oCHG7_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQCONTYP_2_26.RadioValue()==this.w_AQCONTYP)
      this.oPgFrm.Page2.oPag.oAQCONTYP_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG8_2_27.RadioValue()==this.w_CHG8)
      this.oPgFrm.Page2.oPag.oCHG8_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQENGTYP_2_28.RadioValue()==this.w_AQENGTYP)
      this.oPgFrm.Page2.oPag.oAQENGTYP_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG9_2_29.RadioValue()==this.w_CHG9)
      this.oPgFrm.Page2.oPag.oCHG9_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQCURLOC_2_30.RadioValue()==this.w_AQCURLOC)
      this.oPgFrm.Page2.oPag.oAQCURLOC_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG10_2_31.RadioValue()==this.w_CHG10)
      this.oPgFrm.Page2.oPag.oCHG10_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQTIMOUT_2_32.value==this.w_AQTIMOUT)
      this.oPgFrm.Page2.oPag.oAQTIMOUT_2_32.value=this.w_AQTIMOUT
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG11_2_33.RadioValue()==this.w_CHG11)
      this.oPgFrm.Page2.oPag.oCHG11_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQCMDTMO_2_34.value==this.w_AQCMDTMO)
      this.oPgFrm.Page2.oPag.oAQCMDTMO_2_34.value=this.w_AQCMDTMO
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG12_2_35.RadioValue()==this.w_CHG12)
      this.oPgFrm.Page2.oPag.oCHG12_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQQUEFIL_2_36.RadioValue()==this.w_AQQUEFIL)
      this.oPgFrm.Page2.oPag.oAQQUEFIL_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG14_2_37.RadioValue()==this.w_CHG14)
      this.oPgFrm.Page2.oPag.oCHG14_2_37.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQFLJOIN_2_38.RadioValue()==this.w_AQFLJOIN)
      this.oPgFrm.Page2.oPag.oAQFLJOIN_2_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG16_2_39.RadioValue()==this.w_CHG16)
      this.oPgFrm.Page2.oPag.oCHG16_2_39.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQFLGZCP_2_40.RadioValue()==this.w_AQFLGZCP)
      this.oPgFrm.Page2.oPag.oAQFLGZCP_2_40.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCHG15_2_41.RadioValue()==this.w_CHG15)
      this.oPgFrm.Page2.oPag.oCHG15_2_41.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODQUERY = this.w_CODQUERY
    this.o_CHG1 = this.w_CHG1
    this.o_CHG2 = this.w_CHG2
    this.o_CHG3 = this.w_CHG3
    this.o_CHG4 = this.w_CHG4
    this.o_CHG13 = this.w_CHG13
    this.o_CHG5 = this.w_CHG5
    this.o_AQQUEFIL = this.w_AQQUEFIL
    return

enddefine

* --- Define pages as container
define class tgsir_kmqPag1 as StdContainer
  Width  = 792
  height = 369
  stdWidth  = 792
  stdheight = 369
  resizeXpos=514
  resizeYpos=126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMSEL as cp_szoombox with uid="LPBPJMYPJV",left=1, top=23, width=790,height=292,;
    caption='',;
   bGlobalFont=.t.,;
    cTable="INF_AQM",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSIR_KMQ",cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Aggiorna,Init",;
    nPag=1;
    , HelpContextID = 199847690


  add object oBtn_1_4 as StdButton with uid="RIHNVYPWOK",left=685, top=322, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 53222265;
    , tabstop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        GSIR_BMQ(this.Parent.oContained,"OK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="LKDJRNWUMO",left=739, top=322, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 192530362;
    , tabstop=.f., caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_6 as StdRadio with uid="EVWZMNCSKG",rtseq=2,rtrep=.f.,left=10, top=333, width=179,height=36;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 218064090
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 218064090
      this.Buttons(2).Top=17
      this.SetAll("Width",177)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_6.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_6.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_7 as cp_runprogram with uid="BAYTHJPPRE",left=0, top=560, width=203,height=23,;
    caption='GSIR_BMQ',;
   bGlobalFont=.t.,;
    prg="GSIR_BMQ('SELDES')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 60983113

  add object oStr_1_1 as StdString with uid="JNVMZFPADL",Visible=.t., Left=19, Top=6,;
    Alignment=0, Width=327, Height=16,;
    Caption="Selezionare le query da modificare"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsir_kmqPag2 as StdContainer
  Width  = 792
  height = 369
  stdWidth  = 792
  stdheight = 369
  resizeXpos=417
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAQQRPATH_2_1 as StdField with uid="WMAHNUGPBI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_AQQRPATH", cQueryName = "AQQRPATH",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File completo di percorso che contiene la query da eseguire",;
    HelpContextID = 174978638,;
   bGlobalFont=.t.,;
    Height=21, Width=476, Left=145, Top=18, InputMask=replicate('X',254)


  add object oObj_2_2 as cp_askfile with uid="SGJESNBWWT",left=624, top=18, width=19,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_AQQRPATH",cExt="IRP",;
    nPag=2;
    , ToolTipText = "Premere per selezionare il file query di origine";
    , HelpContextID = 199646762

  add object oCHG1_2_3 as StdCheck with uid="OBESSGMMDK",rtseq=4,rtrep=.f.,left=652, top=18, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 196326106,;
    cFormVar="w_CHG1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG1_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG1_2_3.GetRadio()
    this.Parent.oContained.w_CHG1 = this.RadioValue()
    return .t.
  endfunc

  func oCHG1_2_3.SetRadio()
    this.Parent.oContained.w_CHG1=trim(this.Parent.oContained.w_CHG1)
    this.value = ;
      iif(this.Parent.oContained.w_CHG1=='S',1,;
      0)
  endfunc

  add object oCHG1F_2_4 as StdRadio with uid="GXFXOKXVGT",rtseq=5,rtrep=.f.,left=680, top=18, width=109,height=23;
    , ToolTipText = "Modifica intero nome file o solo path";
    , cFormVar="w_CHG1F", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oCHG1F_2_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="File"
      this.Buttons(1).HelpContextID = 122925786
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("File","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Path"
      this.Buttons(2).HelpContextID = 122925786
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Path","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Modifica intero nome file o solo path")
      StdRadio::init()
    endproc

  func oCHG1F_2_4.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'P',;
    'N')))
  endfunc
  func oCHG1F_2_4.GetRadio()
    this.Parent.oContained.w_CHG1F = this.RadioValue()
    return .t.
  endfunc

  func oCHG1F_2_4.SetRadio()
    this.Parent.oContained.w_CHG1F=trim(this.Parent.oContained.w_CHG1F)
    this.value = ;
      iif(this.Parent.oContained.w_CHG1F=='F',1,;
      iif(this.Parent.oContained.w_CHG1F=='P',2,;
      0))
  endfunc

  func oCHG1F_2_4.mHide()
    with this.Parent.oContained
      return (.w_CHG1<>'S')
    endwith
  endfunc

  add object oAQDTFILE_2_5 as StdField with uid="DJGHNHHZME",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AQDTFILE", cQueryName = "AQDTFILE",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File completo di percorso che conterr� i dati aggiornati che costituiscono il risultato della query",;
    HelpContextID = 238082485,;
   bGlobalFont=.t.,;
    Height=21, Width=476, Left=145, Top=45, InputMask=replicate('X',254)

  func oAQDTFILE_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AQQUEFIL <> 'F')
    endwith
   endif
  endfunc


  add object oObj_2_6 as cp_askfile with uid="TZTJPDPRUY",left=624, top=45, width=19,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_AQDTFILE",cExt="IRP",;
    nPag=2;
    , ToolTipText = "Premere per selezionare il file di destinazione";
    , HelpContextID = 199646762

  add object oCHG2_2_7 as StdCheck with uid="MFWRNMGKPG",rtseq=7,rtrep=.f.,left=652, top=45, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 196260570,;
    cFormVar="w_CHG2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG2_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG2_2_7.GetRadio()
    this.Parent.oContained.w_CHG2 = this.RadioValue()
    return .t.
  endfunc

  func oCHG2_2_7.SetRadio()
    this.Parent.oContained.w_CHG2=trim(this.Parent.oContained.w_CHG2)
    this.value = ;
      iif(this.Parent.oContained.w_CHG2=='S',1,;
      0)
  endfunc

  func oCHG2_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AQQUEFIL <> 'F')
    endwith
   endif
  endfunc

  add object oCHG2F_2_8 as StdRadio with uid="DBKTJCPJJE",rtseq=8,rtrep=.f.,left=680, top=45, width=109,height=23;
    , ToolTipText = "Modifica intero nome file o solo path";
    , cFormVar="w_CHG2F", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oCHG2F_2_8.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="File"
      this.Buttons(1).HelpContextID = 122860250
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("File","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Path"
      this.Buttons(2).HelpContextID = 122860250
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Path","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Modifica intero nome file o solo path")
      StdRadio::init()
    endproc

  func oCHG2F_2_8.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'P',;
    'N')))
  endfunc
  func oCHG2F_2_8.GetRadio()
    this.Parent.oContained.w_CHG2F = this.RadioValue()
    return .t.
  endfunc

  func oCHG2F_2_8.SetRadio()
    this.Parent.oContained.w_CHG2F=trim(this.Parent.oContained.w_CHG2F)
    this.value = ;
      iif(this.Parent.oContained.w_CHG2F=='F',1,;
      iif(this.Parent.oContained.w_CHG2F=='P',2,;
      0))
  endfunc

  func oCHG2F_2_8.mHide()
    with this.Parent.oContained
      return (.w_CHG2<>'S' AND .w_AQQUEFIL <> 'F')
    endwith
  endfunc

  add object oAQIMPATH_2_9 as StdField with uid="YLFYMNPYPT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_AQIMPATH", cQueryName = "AQIMPATH",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File completo di percorso dell'InfoMart a cui deve fare riferimento InfoPublisher",;
    HelpContextID = 174618190,;
   bGlobalFont=.t.,;
    Height=21, Width=476, Left=145, Top=72, InputMask=replicate('X',254)


  add object oObj_2_10 as cp_askfile with uid="VHSCVNZTBU",left=624, top=72, width=19,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_AQIMPATH",cExt="MRT",;
    nPag=2;
    , ToolTipText = "Premere per selezionare il file InfoMart";
    , HelpContextID = 199646762

  add object oCHG3_2_11 as StdCheck with uid="XPBLKYUALH",rtseq=10,rtrep=.f.,left=652, top=72, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 196195034,;
    cFormVar="w_CHG3", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG3_2_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG3_2_11.GetRadio()
    this.Parent.oContained.w_CHG3 = this.RadioValue()
    return .t.
  endfunc

  func oCHG3_2_11.SetRadio()
    this.Parent.oContained.w_CHG3=trim(this.Parent.oContained.w_CHG3)
    this.value = ;
      iif(this.Parent.oContained.w_CHG3=='S',1,;
      0)
  endfunc

  add object oCHG3F_2_12 as StdRadio with uid="NRXNYEUSQL",rtseq=11,rtrep=.f.,left=680, top=72, width=109,height=23;
    , ToolTipText = "Modifica intero nome file o solo path";
    , cFormVar="w_CHG3F", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oCHG3F_2_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="File"
      this.Buttons(1).HelpContextID = 122794714
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("File","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Path"
      this.Buttons(2).HelpContextID = 122794714
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Path","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Modifica intero nome file o solo path")
      StdRadio::init()
    endproc

  func oCHG3F_2_12.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'P',;
    'N')))
  endfunc
  func oCHG3F_2_12.GetRadio()
    this.Parent.oContained.w_CHG3F = this.RadioValue()
    return .t.
  endfunc

  func oCHG3F_2_12.SetRadio()
    this.Parent.oContained.w_CHG3F=trim(this.Parent.oContained.w_CHG3F)
    this.value = ;
      iif(this.Parent.oContained.w_CHG3F=='F',1,;
      iif(this.Parent.oContained.w_CHG3F=='P',2,;
      0))
  endfunc

  func oCHG3F_2_12.mHide()
    with this.Parent.oContained
      return (.w_CHG3<>'S')
    endwith
  endfunc

  add object oAQLOG_2_13 as StdField with uid="ORRGMJIXSH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_AQLOG", cQueryName = "AQLOG",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File completo di percorso del log",;
    HelpContextID = 119888378,;
   bGlobalFont=.t.,;
    Height=21, Width=476, Left=145, Top=99, InputMask=replicate('X',254)


  add object oObj_2_14 as cp_askfile with uid="XZWGAIEPIX",left=624, top=99, width=19,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_AQLOG",;
    nPag=2;
    , ToolTipText = "Premere per selezionare il file di log";
    , HelpContextID = 199646762

  add object oCHG4_2_15 as StdCheck with uid="FGTBDJZTPL",rtseq=13,rtrep=.f.,left=652, top=99, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 196129498,;
    cFormVar="w_CHG4", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG4_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG4_2_15.GetRadio()
    this.Parent.oContained.w_CHG4 = this.RadioValue()
    return .t.
  endfunc

  func oCHG4_2_15.SetRadio()
    this.Parent.oContained.w_CHG4=trim(this.Parent.oContained.w_CHG4)
    this.value = ;
      iif(this.Parent.oContained.w_CHG4=='S',1,;
      0)
  endfunc

  add object oCHG4F_2_16 as StdRadio with uid="ENURHLERXO",rtseq=14,rtrep=.f.,left=680, top=99, width=109,height=23;
    , ToolTipText = "Modifica intero nome file o solo path";
    , cFormVar="w_CHG4F", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oCHG4F_2_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="File"
      this.Buttons(1).HelpContextID = 122729178
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("File","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Path"
      this.Buttons(2).HelpContextID = 122729178
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Path","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Modifica intero nome file o solo path")
      StdRadio::init()
    endproc

  func oCHG4F_2_16.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'P',;
    'N')))
  endfunc
  func oCHG4F_2_16.GetRadio()
    this.Parent.oContained.w_CHG4F = this.RadioValue()
    return .t.
  endfunc

  func oCHG4F_2_16.SetRadio()
    this.Parent.oContained.w_CHG4F=trim(this.Parent.oContained.w_CHG4F)
    this.value = ;
      iif(this.Parent.oContained.w_CHG4F=='F',1,;
      iif(this.Parent.oContained.w_CHG4F=='P',2,;
      0))
  endfunc

  func oCHG4F_2_16.mHide()
    with this.Parent.oContained
      return (.w_CHG4<>'S')
    endwith
  endfunc


  add object oAQCRIPTE_2_17 as StdCombo with uid="UCAPDINIPS",rtseq=15,rtrep=.f.,left=145,top=126,width=124,height=21;
    , ToolTipText = "Tipo di protezione";
    , HelpContextID = 150804043;
    , cFormVar="w_AQCRIPTE",RowSource=""+"Stringa cifrata,"+"Nessuna cifratura", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAQCRIPTE_2_17.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oAQCRIPTE_2_17.GetRadio()
    this.Parent.oContained.w_AQCRIPTE = this.RadioValue()
    return .t.
  endfunc

  func oAQCRIPTE_2_17.SetRadio()
    this.Parent.oContained.w_AQCRIPTE=trim(this.Parent.oContained.w_AQCRIPTE)
    this.value = ;
      iif(this.Parent.oContained.w_AQCRIPTE=='C',1,;
      iif(this.Parent.oContained.w_AQCRIPTE=='N',2,;
      0))
  endfunc

  add object oCHG13_2_18 as StdCheck with uid="MJQOAFBKLW",rtseq=16,rtrep=.f.,left=272, top=126, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 142848730,;
    cFormVar="w_CHG13", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG13_2_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG13_2_18.GetRadio()
    this.Parent.oContained.w_CHG13 = this.RadioValue()
    return .t.
  endfunc

  func oCHG13_2_18.SetRadio()
    this.Parent.oContained.w_CHG13=trim(this.Parent.oContained.w_CHG13)
    this.value = ;
      iif(this.Parent.oContained.w_CHG13=='S',1,;
      0)
  endfunc

  add object oAQADOCS_2_19 as StdField with uid="YQWEJUOZPP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_AQADOCS", cQueryName = "AQADOCS",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di connessione ADO",;
    HelpContextID = 61934074,;
   bGlobalFont=.t.,;
    Height=21, Width=476, Left=145, Top=176, InputMask=replicate('X',254)


  add object oBtn_2_20 as StdButton with uid="ZTMQWGXNSN",left=624, top=176, width=20,height=22,;
    caption="...", nPag=2;
    , ToolTipText = "Costruzione stringa ADO";
    , HelpContextID = 199646762;
  , bGlobalFont=.t.

    proc oBtn_2_20.Click()
      do GSIR_KAD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCHG5_2_21 as StdCheck with uid="EFALPHQDVO",rtseq=18,rtrep=.f.,left=652, top=176, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 196063962,;
    cFormVar="w_CHG5", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG5_2_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG5_2_21.GetRadio()
    this.Parent.oContained.w_CHG5 = this.RadioValue()
    return .t.
  endfunc

  func oCHG5_2_21.SetRadio()
    this.Parent.oContained.w_CHG5=trim(this.Parent.oContained.w_CHG5)
    this.value = ;
      iif(this.Parent.oContained.w_CHG5=='S',1,;
      0)
  endfunc


  add object oAQFORTYP_2_22 as StdCombo with uid="WLCDXXBAGI",rtseq=19,rtrep=.f.,left=145,top=204,width=159,height=21;
    , ToolTipText = "Formato del file generato";
    , HelpContextID = 227165782;
    , cFormVar="w_AQFORTYP",RowSource=""+"Report infobusiness,"+"Rich Text Format,"+"Excel,"+"HTML statico,"+"Comma separated values,"+"PDF", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAQFORTYP_2_22.RadioValue()
    return(iif(this.value =1,'efQuery',;
    iif(this.value =2,'efRTF',;
    iif(this.value =3,'efExcel',;
    iif(this.value =4,'efHTML',;
    iif(this.value =5,'efCSV',;
    iif(this.value =6,'efPDF',;
    space(10))))))))
  endfunc
  func oAQFORTYP_2_22.GetRadio()
    this.Parent.oContained.w_AQFORTYP = this.RadioValue()
    return .t.
  endfunc

  func oAQFORTYP_2_22.SetRadio()
    this.Parent.oContained.w_AQFORTYP=trim(this.Parent.oContained.w_AQFORTYP)
    this.value = ;
      iif(this.Parent.oContained.w_AQFORTYP=='efQuery',1,;
      iif(this.Parent.oContained.w_AQFORTYP=='efRTF',2,;
      iif(this.Parent.oContained.w_AQFORTYP=='efExcel',3,;
      iif(this.Parent.oContained.w_AQFORTYP=='efHTML',4,;
      iif(this.Parent.oContained.w_AQFORTYP=='efCSV',5,;
      iif(this.Parent.oContained.w_AQFORTYP=='efPDF',6,;
      0))))))
  endfunc

  add object oCHG6_2_23 as StdCheck with uid="PFYGEDQIFY",rtseq=20,rtrep=.f.,left=311, top=207, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 195998426,;
    cFormVar="w_CHG6", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG6_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG6_2_23.GetRadio()
    this.Parent.oContained.w_CHG6 = this.RadioValue()
    return .t.
  endfunc

  func oCHG6_2_23.SetRadio()
    this.Parent.oContained.w_CHG6=trim(this.Parent.oContained.w_CHG6)
    this.value = ;
      iif(this.Parent.oContained.w_CHG6=='S',1,;
      0)
  endfunc


  add object oAQCURTYP_2_24 as StdCombo with uid="ZJHKJLHFXQ",rtseq=21,rtrep=.f.,left=440,top=204,width=159,height=21;
    , ToolTipText = "Indica il tipo di cursore ADO utilizzato";
    , HelpContextID = 227546710;
    , cFormVar="w_AQCURTYP",RowSource=""+"ctstatic,"+"ctunspecified,"+"ctopenforwardonly,"+"ctkeyset,"+"ctdynamic", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAQCURTYP_2_24.RadioValue()
    return(iif(this.value =1,'ctStatic',;
    iif(this.value =2,'ctUnspecified',;
    iif(this.value =3,'ctOpenForwardOnly',;
    iif(this.value =4,'ctKeyset',;
    iif(this.value =5,'ctDynamic',;
    space(20)))))))
  endfunc
  func oAQCURTYP_2_24.GetRadio()
    this.Parent.oContained.w_AQCURTYP = this.RadioValue()
    return .t.
  endfunc

  func oAQCURTYP_2_24.SetRadio()
    this.Parent.oContained.w_AQCURTYP=trim(this.Parent.oContained.w_AQCURTYP)
    this.value = ;
      iif(this.Parent.oContained.w_AQCURTYP=='ctStatic',1,;
      iif(this.Parent.oContained.w_AQCURTYP=='ctUnspecified',2,;
      iif(this.Parent.oContained.w_AQCURTYP=='ctOpenForwardOnly',3,;
      iif(this.Parent.oContained.w_AQCURTYP=='ctKeyset',4,;
      iif(this.Parent.oContained.w_AQCURTYP=='ctDynamic',5,;
      0)))))
  endfunc

  add object oCHG7_2_25 as StdCheck with uid="YHJCUBTQQU",rtseq=22,rtrep=.f.,left=606, top=207, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 195932890,;
    cFormVar="w_CHG7", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG7_2_25.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG7_2_25.GetRadio()
    this.Parent.oContained.w_CHG7 = this.RadioValue()
    return .t.
  endfunc

  func oCHG7_2_25.SetRadio()
    this.Parent.oContained.w_CHG7=trim(this.Parent.oContained.w_CHG7)
    this.value = ;
      iif(this.Parent.oContained.w_CHG7=='S',1,;
      0)
  endfunc


  add object oAQCONTYP_2_26 as StdCombo with uid="JVLKFAWFAO",rtseq=23,rtrep=.f.,left=145,top=232,width=159,height=21;
    , ToolTipText = "Motore di database utilizzato";
    , HelpContextID = 222959190;
    , cFormVar="w_AQCONTYP",RowSource=""+"Ad hoc,"+"Access,"+"SQL Server,"+"Oracle,"+"DB2,"+"AS400,"+"Informix,"+"Dbase,"+"Mysql,"+"Interbase,"+"DBMaker,"+"Visual foxpro,"+"Paradox,"+"Postgresql", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAQCONTYP_2_26.RadioValue()
    return(iif(this.value =1,CP_DBTYPE,;
    iif(this.value =2,'Access',;
    iif(this.value =3,'SQLServer',;
    iif(this.value =4,'Oracle',;
    iif(this.value =5,'DB2',;
    iif(this.value =6,'AS400',;
    iif(this.value =7,'Informix',;
    iif(this.value =8,'DBase',;
    iif(this.value =9,'Mysql',;
    iif(this.value =10,'Interbase',;
    iif(this.value =11,'DBMaker',;
    iif(this.value =12,'Visual FoxPro',;
    iif(this.value =13,'Paradox',;
    iif(this.value =14,'PostGreSQL',;
    space(15))))))))))))))))
  endfunc
  func oAQCONTYP_2_26.GetRadio()
    this.Parent.oContained.w_AQCONTYP = this.RadioValue()
    return .t.
  endfunc

  func oAQCONTYP_2_26.SetRadio()
    this.Parent.oContained.w_AQCONTYP=trim(this.Parent.oContained.w_AQCONTYP)
    this.value = ;
      iif(this.Parent.oContained.w_AQCONTYP==CP_DBTYPE,1,;
      iif(this.Parent.oContained.w_AQCONTYP=='Access',2,;
      iif(this.Parent.oContained.w_AQCONTYP=='SQLServer',3,;
      iif(this.Parent.oContained.w_AQCONTYP=='Oracle',4,;
      iif(this.Parent.oContained.w_AQCONTYP=='DB2',5,;
      iif(this.Parent.oContained.w_AQCONTYP=='AS400',6,;
      iif(this.Parent.oContained.w_AQCONTYP=='Informix',7,;
      iif(this.Parent.oContained.w_AQCONTYP=='DBase',8,;
      iif(this.Parent.oContained.w_AQCONTYP=='Mysql',9,;
      iif(this.Parent.oContained.w_AQCONTYP=='Interbase',10,;
      iif(this.Parent.oContained.w_AQCONTYP=='DBMaker',11,;
      iif(this.Parent.oContained.w_AQCONTYP=='Visual FoxPro',12,;
      iif(this.Parent.oContained.w_AQCONTYP=='Paradox',13,;
      iif(this.Parent.oContained.w_AQCONTYP=='PostGreSQL',14,;
      0))))))))))))))
  endfunc

  add object oCHG8_2_27 as StdCheck with uid="POWAYYBESQ",rtseq=24,rtrep=.f.,left=311, top=235, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 195867354,;
    cFormVar="w_CHG8", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG8_2_27.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG8_2_27.GetRadio()
    this.Parent.oContained.w_CHG8 = this.RadioValue()
    return .t.
  endfunc

  func oCHG8_2_27.SetRadio()
    this.Parent.oContained.w_CHG8=trim(this.Parent.oContained.w_CHG8)
    this.value = ;
      iif(this.Parent.oContained.w_CHG8=='S',1,;
      0)
  endfunc


  add object oAQENGTYP_2_28 as StdCombo with uid="QKIPUXRYEX",rtseq=25,rtrep=.f.,left=440,top=232,width=159,height=21;
    , ToolTipText = "Indica se memorizzare i dati risultanti dalla query direttamente nel cubo o in un cursore intermedio";
    , HelpContextID = 215561814;
    , cFormVar="w_AQENGTYP",RowSource=""+"ieuserecordset,"+"ieusedirect", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAQENGTYP_2_28.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oAQENGTYP_2_28.GetRadio()
    this.Parent.oContained.w_AQENGTYP = this.RadioValue()
    return .t.
  endfunc

  func oAQENGTYP_2_28.SetRadio()
    this.Parent.oContained.w_AQENGTYP=trim(this.Parent.oContained.w_AQENGTYP)
    this.value = ;
      iif(this.Parent.oContained.w_AQENGTYP=='R',1,;
      iif(this.Parent.oContained.w_AQENGTYP=='D',2,;
      0))
  endfunc

  add object oCHG9_2_29 as StdCheck with uid="XOEMHUGIXN",rtseq=26,rtrep=.f.,left=606, top=235, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 195801818,;
    cFormVar="w_CHG9", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG9_2_29.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG9_2_29.GetRadio()
    this.Parent.oContained.w_CHG9 = this.RadioValue()
    return .t.
  endfunc

  func oCHG9_2_29.SetRadio()
    this.Parent.oContained.w_CHG9=trim(this.Parent.oContained.w_CHG9)
    this.value = ;
      iif(this.Parent.oContained.w_CHG9=='S',1,;
      0)
  endfunc


  add object oAQCURLOC_2_30 as StdCombo with uid="IJXXVTRISF",rtseq=27,rtrep=.f.,left=145,top=260,width=159,height=21;
    , ToolTipText = "Indica se il cursore ADO che contiene i dati debba essere mantenuto lato server o lato client";
    , HelpContextID = 175106487;
    , cFormVar="w_AQCURLOC",RowSource=""+"Client,"+"Server", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAQCURLOC_2_30.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oAQCURLOC_2_30.GetRadio()
    this.Parent.oContained.w_AQCURLOC = this.RadioValue()
    return .t.
  endfunc

  func oAQCURLOC_2_30.SetRadio()
    this.Parent.oContained.w_AQCURLOC=trim(this.Parent.oContained.w_AQCURLOC)
    this.value = ;
      iif(this.Parent.oContained.w_AQCURLOC=='C',1,;
      iif(this.Parent.oContained.w_AQCURLOC=='S',2,;
      0))
  endfunc

  add object oCHG10_2_31 as StdCheck with uid="MMPBSPQQKV",rtseq=28,rtrep=.f.,left=311, top=263, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 145994458,;
    cFormVar="w_CHG10", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG10_2_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG10_2_31.GetRadio()
    this.Parent.oContained.w_CHG10 = this.RadioValue()
    return .t.
  endfunc

  func oCHG10_2_31.SetRadio()
    this.Parent.oContained.w_CHG10=trim(this.Parent.oContained.w_CHG10)
    this.value = ;
      iif(this.Parent.oContained.w_CHG10=='S',1,;
      0)
  endfunc

  add object oAQTIMOUT_2_32 as StdField with uid="TSAVABAGJP",rtseq=29,rtrep=.f.,;
    cFormVar = "w_AQTIMOUT", cQueryName = "AQTIMOUT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Timeout in secondi per la connessione (se zero timeout infinito)",;
    HelpContextID = 137700954,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=145, Top=288, cSayPict='"999"', cGetPict='"999"'

  add object oCHG11_2_33 as StdCheck with uid="RJWPMZIPYB",rtseq=30,rtrep=.f.,left=311, top=291, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 144945882,;
    cFormVar="w_CHG11", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG11_2_33.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG11_2_33.GetRadio()
    this.Parent.oContained.w_CHG11 = this.RadioValue()
    return .t.
  endfunc

  func oCHG11_2_33.SetRadio()
    this.Parent.oContained.w_CHG11=trim(this.Parent.oContained.w_CHG11)
    this.value = ;
      iif(this.Parent.oContained.w_CHG11=='S',1,;
      0)
  endfunc

  add object oAQCMDTMO_2_34 as StdField with uid="NDDPTUBXJJ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_AQCMDTMO", cQueryName = "AQCMDTMO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata massima in secondi del comando SQL (se zero timeout infinito)",;
    HelpContextID = 56093099,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=440, Top=288

  add object oCHG12_2_35 as StdCheck with uid="BGMSUXMQIU",rtseq=32,rtrep=.f.,left=606, top=291, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 143897306,;
    cFormVar="w_CHG12", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG12_2_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG12_2_35.GetRadio()
    this.Parent.oContained.w_CHG12 = this.RadioValue()
    return .t.
  endfunc

  func oCHG12_2_35.SetRadio()
    this.Parent.oContained.w_CHG12=trim(this.Parent.oContained.w_CHG12)
    this.value = ;
      iif(this.Parent.oContained.w_CHG12=='S',1,;
      0)
  endfunc


  add object oAQQUEFIL_2_36 as StdCombo with uid="KHGZODKJCJ",rtseq=33,rtrep=.f.,left=145,top=318,width=94,height=21;
    , ToolTipText = "Standard: con filtri e senza filtri, filtrata: solo con filtri, non filtrata: solo senza filtri";
    , HelpContextID = 247526994;
    , cFormVar="w_AQQUEFIL",RowSource=""+"Standard,"+"Filtrata,"+"Non filtrata", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAQQUEFIL_2_36.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oAQQUEFIL_2_36.GetRadio()
    this.Parent.oContained.w_AQQUEFIL = this.RadioValue()
    return .t.
  endfunc

  func oAQQUEFIL_2_36.SetRadio()
    this.Parent.oContained.w_AQQUEFIL=trim(this.Parent.oContained.w_AQQUEFIL)
    this.value = ;
      iif(this.Parent.oContained.w_AQQUEFIL=='S',1,;
      iif(this.Parent.oContained.w_AQQUEFIL=='F',2,;
      iif(this.Parent.oContained.w_AQQUEFIL=='N',3,;
      0)))
  endfunc

  add object oCHG14_2_37 as StdCheck with uid="WMJHOLJGZK",rtseq=34,rtrep=.f.,left=311, top=318, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 141800154,;
    cFormVar="w_CHG14", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG14_2_37.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG14_2_37.GetRadio()
    this.Parent.oContained.w_CHG14 = this.RadioValue()
    return .t.
  endfunc

  func oCHG14_2_37.SetRadio()
    this.Parent.oContained.w_CHG14=trim(this.Parent.oContained.w_CHG14)
    this.value = ;
      iif(this.Parent.oContained.w_CHG14=='S',1,;
      0)
  endfunc

  add object oAQFLJOIN_2_38 as StdCheck with uid="ITEUMLTKMF",rtseq=35,rtrep=.f.,left=440, top=318, caption="WhereForJoins",;
    ToolTipText = "Se attivo utilizza la sintassi where per costruire le clausole di join altrimenti la sintassi inner join",;
    HelpContextID = 134694484,;
    cFormVar="w_AQFLJOIN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAQFLJOIN_2_38.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAQFLJOIN_2_38.GetRadio()
    this.Parent.oContained.w_AQFLJOIN = this.RadioValue()
    return .t.
  endfunc

  func oAQFLJOIN_2_38.SetRadio()
    this.Parent.oContained.w_AQFLJOIN=trim(this.Parent.oContained.w_AQFLJOIN)
    this.value = ;
      iif(this.Parent.oContained.w_AQFLJOIN=='S',1,;
      0)
  endfunc

  add object oCHG16_2_39 as StdCheck with uid="GKNLELXZMB",rtseq=36,rtrep=.f.,left=606, top=318, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 139703002,;
    cFormVar="w_CHG16", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG16_2_39.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG16_2_39.GetRadio()
    this.Parent.oContained.w_CHG16 = this.RadioValue()
    return .t.
  endfunc

  func oCHG16_2_39.SetRadio()
    this.Parent.oContained.w_CHG16=trim(this.Parent.oContained.w_CHG16)
    this.value = ;
      iif(this.Parent.oContained.w_CHG16=='S',1,;
      0)
  endfunc

  add object oAQFLGZCP_2_40 as StdCheck with uid="LDLSVXZJFC",rtseq=37,rtrep=.f.,left=145, top=344, caption="Abilitazione pubblicazione query su Zucchetti Corporate Portal (DMS)",;
    ToolTipText = "Flag abilitazione invio query a Zucchetti Corporate Portal (DMS)",;
    HelpContextID = 47662678,;
    cFormVar="w_AQFLGZCP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAQFLGZCP_2_40.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAQFLGZCP_2_40.GetRadio()
    this.Parent.oContained.w_AQFLGZCP = this.RadioValue()
    return .t.
  endfunc

  func oAQFLGZCP_2_40.SetRadio()
    this.Parent.oContained.w_AQFLGZCP=trim(this.Parent.oContained.w_AQFLGZCP)
    this.value = ;
      iif(this.Parent.oContained.w_AQFLGZCP=='S',1,;
      0)
  endfunc

  func oAQFLGZCP_2_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP$'SA' AND .w_AQQUEFIL <> 'F')
    endwith
   endif
  endfunc

  add object oCHG15_2_41 as StdCheck with uid="EXFKBSDRDF",rtseq=38,rtrep=.f.,left=606, top=344, caption="",;
    ToolTipText = "Se selezionato riporta le modifiche alla conferma",;
    HelpContextID = 140751578,;
    cFormVar="w_CHG15", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCHG15_2_41.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCHG15_2_41.GetRadio()
    this.Parent.oContained.w_CHG15 = this.RadioValue()
    return .t.
  endfunc

  func oCHG15_2_41.SetRadio()
    this.Parent.oContained.w_CHG15=trim(this.Parent.oContained.w_CHG15)
    this.value = ;
      iif(this.Parent.oContained.w_CHG15=='S',1,;
      0)
  endfunc

  func oCHG15_2_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AQQUEFIL <> 'F')
    endwith
   endif
  endfunc


  add object oBtn_2_54 as StdButton with uid="WVAFWOQIPF",left=681, top=321, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 53222265;
    , tabstop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_54.Click()
      with this.Parent.oContained
        GSIR_BMQ(this.Parent.oContained,"OK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_55 as StdButton with uid="XPWKGFPJKO",left=735, top=321, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 192530362;
    , tabstop=.f., caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_55.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_57 as StdButton with uid="HZTLACVIML",left=297, top=126, width=48,height=45,;
    CpPicture="bmp\CIFRA.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per cifrare la stringa di connessione";
    , HelpContextID = 199847690;
    , Caption='\<Cifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_57.Click()
      do GSIR_KCR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_57.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_AQCRIPTE='N')
      endwith
    endif
  endfunc

  func oBtn_2_57.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_AQCRIPTE='C')
     endwith
    endif
  endfunc


  add object oBtn_2_58 as StdButton with uid="GASXYNBYBG",left=297, top=126, width=48,height=45,;
    CpPicture="BMP\DECIFRA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per decifrare la stringa di connessione";
    , HelpContextID = 199847690;
    , Caption='\<Decifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_58.Click()
      do GSIR_KCR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_58.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_AQCRIPTE='C')
      endwith
    endif
  endfunc

  func oBtn_2_58.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_AQCRIPTE='N')
     endwith
    endif
  endfunc

  add object oStr_2_42 as StdString with uid="HMGITJEYBF",Visible=.t., Left=1, Top=18,;
    Alignment=1, Width=141, Height=18,;
    Caption="File query di origine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="IAAXPJXGJD",Visible=.t., Left=1, Top=45,;
    Alignment=1, Width=141, Height=18,;
    Caption="File di destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="NRIWCAVODF",Visible=.t., Left=1, Top=72,;
    Alignment=1, Width=141, Height=18,;
    Caption="File InfoMart:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="DPXCPIYQSR",Visible=.t., Left=1, Top=99,;
    Alignment=1, Width=141, Height=18,;
    Caption="File del log:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="TIUODYBUEB",Visible=.t., Left=1, Top=176,;
    Alignment=1, Width=141, Height=18,;
    Caption="Connessione ADO:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="FTBIJHTUFE",Visible=.t., Left=1, Top=204,;
    Alignment=1, Width=141, Height=18,;
    Caption="Formato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="OOKCWVAUZQ",Visible=.t., Left=1, Top=232,;
    Alignment=1, Width=141, Height=17,;
    Caption="Tipo database:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="KHMYFUWEGB",Visible=.t., Left=1, Top=259,;
    Alignment=1, Width=141, Height=18,;
    Caption="Cursore lato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="QLSECUEBWJ",Visible=.t., Left=344, Top=204,;
    Alignment=1, Width=91, Height=17,;
    Caption="Tipo cursore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="KRHTMJJHUP",Visible=.t., Left=344, Top=233,;
    Alignment=1, Width=91, Height=17,;
    Caption="Enginetype:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="ORUQHWALOI",Visible=.t., Left=1, Top=288,;
    Alignment=1, Width=141, Height=18,;
    Caption="Timeout conn.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_53 as StdString with uid="XHZRKHTBVN",Visible=.t., Left=344, Top=288,;
    Alignment=1, Width=91, Height=18,;
    Caption="Cmd timeout:"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="KIGDBVVPMD",Visible=.t., Left=1, Top=126,;
    Alignment=1, Width=141, Height=18,;
    Caption="Tipo di protezione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="REGJWAMBNX",Visible=.t., Left=1, Top=346,;
    Alignment=1, Width=141, Height=18,;
    Caption="Corporate Portal:"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="FRSTFUUQXI",Visible=.t., Left=1, Top=318,;
    Alignment=1, Width=141, Height=18,;
    Caption="Tipo query:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_kmq','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
