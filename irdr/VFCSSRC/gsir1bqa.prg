* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir1bqa                                                        *
*              Cancella autorizzazioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-25                                                      *
* Last revis.: 2005-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir1bqa",oParentObject,m.pOPER)
return(i_retval)

define class tgsir1bqa as StdBatch
  * --- Local variables
  pOPER = space(1)
  * --- WorkFile variables
  INF_AUQU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancella le autorizzazioni nel caso in cui si cancelli una query, si cancelli una 
    *     gestione
    * --- Parametri:
    *     pOPER = 'Q' cancella query
    *     pOPER = 'G' cancella gestione
    do case
      case this.pOPER = "Q"
        * --- Delete from INF_AUQU
        i_nConn=i_TableProp[this.INF_AUQU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"QACODQUE = "+cp_ToStrODBC(this.oParentObject.w_AQCODICE);
                 )
        else
          delete from (i_cTable) where;
                QACODQUE = this.oParentObject.w_AQCODICE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Impossibile cancellare autorizzazione'
          return
        endif
      case this.pOPER = "G"
        * --- Delete from INF_AUQU
        i_nConn=i_TableProp[this.INF_AUQU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INF_AUQU_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"QACODQUE = "+cp_ToStrODBC(this.oParentObject.w_GICODQUE);
                +" and QAROWPRG = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                 )
        else
          delete from (i_cTable) where;
                QACODQUE = this.oParentObject.w_GICODQUE;
                and QAROWPRG = this.oParentObject.w_CPROWNUM;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Impossibile cancellare autorizzazione'
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INF_AUQU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
