* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_bzg                                                        *
*              Caricamento rapido parametri                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2005-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsir_bzg",oParentObject,m.pOPER)
return(i_retval)

define class tgsir_bzg as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_NUMFORMS = 0
  i = 0
  j = 0
  w_CAPTION = space(254)
  w_GESTNAME = space(30)
  w_MASK = .NULL.
  w_PAROBJ = .NULL.
  w_VARIABLE = space(20)
  w_DESC = space(254)
  w_TIPO = space(1)
  w_LUN = 0
  w_DEC = 0
  w_VALUE = space(254)
  w_PICT = space(254)
  w_COUNT = 0
  w_NumPage = 0
  w_NumCtrl = 0
  w_GSIR_MPF = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento rapido parametri da gestione (Da GSIR_KZG)
    * --- pOPER   : 'G'  Carica gestioni nello zoom GESTIONI
    *                      'P'  Carica parametri nello zoom PARAMETRI
    *                      'C'  Carica parametri selezionati nel figlio integrato
    do case
      case this.pOPER = "G"
        * --- Carico lo zoom delle gestioni aperte
        * --- Determino il numero di forms aperte a video
        this.w_NUMFORMS = _SCREEN.FormCount
        this.i = 0
        this.j = 0
        * --- Ciclo su tutte le forms
        do while this.i < this.w_NUMFORMS
          this.i = this.i + 1
          * --- Se l'oggetto sullo screen  � di tipo form , � un oggetto painter e non � un detail
          if LOWER( _SCREEN.FORMS[this.i].BaseClass ) = "form" AND TYPE(" _SCREEN.FORMS[this.i].cFile") <> "U" And TYPE("_SCREEN.FORMS[this.i].oPgFrm.PageCount") <> "U"
            this.w_CAPTION = _SCREEN.FORMS[this.i].Caption
            this.w_GESTNAME = ALLTRIM( UPPER( SUBSTR( _SCREEN.FORMS[this.i].Class, 2) ) )
            * --- Escludo le gestioni dell' anagrafica query
            if NOT INLIST(this.w_GESTNAME, "GSIR_MAQ", "GSIR_MPF", "GSIR_KZG")
              this.j = this.j + 1
              * --- Aggiorno lo zoom, nello zoom inserisco un progressivo che sar� utilizzato come chiave per 
              *     recuperare l'oggetto
              INSERT INTO (this.oParentObject.w_GESTIONI.cCursor) (ROWORD, GESTNAME, CAPTION) VALUES (this.j, this.w_GESTNAME, this.w_CAPTION)
              * --- Ogni oggetto che inserisco nello zoom viene inserito in un array cos� posso
              *     recuperare subito il controllo sull'oggetto, l'array � una propriet� della maschera
              *     padre (GSIR_KZG)
              Dimension This.oParentObject.w_AGEST[this.j]
              This.oParentObject.w_AGEST[this.j] = _SCREEN.FORMS[this.i]
            endif
          endif
        enddo
        * --- Mi posiziono all'inizio del cursore
        SELECT (this.oParentObject.w_GESTIONI.cCursor) 
 GO TOP
      case this.pOPER = "P"
        * --- Aggiorno lo zoom delle variabili
        * --- Svuoto lo zoom
        SELECT (this.oParentObject.w_PARAMETRI.cCursor)
        ZAP
        * --- Prendo il controllo della gestione selezionata
        this.w_MASK = this.oParentObject.w_AGEST[this.oParentObject.w_ROWORD]
        * --- Se � un master elimino dal conto delle pagine l'elenco
        if NOT EMPTY(this.w_MASK.cfile)
          this.w_COUNT = this.w_MASK.oPgFrm.PageCount - 1
        else
          this.w_COUNT = this.w_MASK.oPgFrm.PageCount
        endif
        this.w_NumPage = 0
        * --- Ciclo su tutte le pagine
        do while this.w_NumPage < this.w_COUNT
          this.w_NumPage = this.w_NumPage + 1
          * --- Ciclo sui controlli della pagina
          this.w_PAROBJ = this.w_MASK.oPgFrm.Pages(this.w_NumPage).oPag
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if Type("this.w_MASK.oPgFrm.Pages(this.w_NumPage).oPag.oBody.oBodycol.oRow") = "O"
            * --- Ciclo sui controlli del dettaglio
            this.w_PAROBJ = this.w_MASK.oPgFrm.Pages(this.w_NumPage).oPag.oBody.oBodycol.oRow
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        enddo
        * --- Ordino le variabili per nome
        SELECT * FROM (this.oParentObject.w_PARAMETRI.cCursor) INTO CURSOR TMPCURAPPO ORDER BY VARNAME
        * --- Svuoto lo zoom
        SELECT (this.oParentObject.w_PARAMETRI.cCursor)
        ZAP
        if USED("TMPCURAPPO")
          SELECT TMPCURAPPO
          GO TOP
          * --- Riempio lo zoom con TMPCURAPPO
          do while Not Eof( "TMPCURAPPO" )
            if Not Deleted()
               
 Scatter memVar 
 Select ( this.oParentObject.w_PARAMETRI.cCursor ) 
 Append Blank 
 Gather MemVar
            endif
            if Not Eof( "TMPCURAPPO" )
               
 Select TMPCURAPPO 
 Skip
            endif
          enddo
          SELECT TMPCURAPPO
          USE
        endif
        * --- Mi posiziono all'inizio del cursore e eseguo il refresh dello zoom
        SELECT (this.oParentObject.w_PARAMETRI.cCursor)
        GO TOP
        this.oParentObject.w_PARAMETRI.grd.Refresh()     
      case this.pOPER = "C"
        * --- Carico i dati sulla movimentazione
        * --- Recupero la movimentazione
        this.w_GSIR_MPF = This.oParentObject.oParentObject
        * --- Seleziono solo le variabili con il check
        SELECT * FROM (this.oParentObject.w_PARAMETRI.cCursor) WHERE XCHK = 1 INTO CURSOR APPO
        if USED("APPO")
          SELECT APPO
          LOCATE FOR 1=1
          do while NOT(EOF())
            * --- Aggiungo una riga
            this.w_GSIR_MPF.AddRow()     
            * --- Valorizzo le variabili
            this.w_GSIR_MPF.w_PFVARNAM = VARNAME
            this.w_GSIR_MPF.w_PFDESVAR = TOOLTIP
            this.w_GSIR_MPF.w_PFTIPVAR = TIPO
            this.w_GSIR_MPF.w_TIPVAR = TIPO
            this.w_GSIR_MPF.w_PFLENVAR = LEN
            this.w_GSIR_MPF.w_PFDECVAR = DEC
            * --- Aggiorno le t_ del transitorio
            this.w_GSIR_MPF.TrsFromWork()     
            * --- Valorizzo le propriet� value dei controlli visuali
            this.w_GSIR_MPF.SetControlsValue()     
            SELECT APPO
            CONTINUE
          enddo
          * --- Eseguo il refresh della maschera
          this.w_GSIR_MPF.Refresh()     
          USE
        endif
        
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scorre tutti i controls e recupera le informazioni sulle variabili
    this.w_NumCtrl = 0
    do while this.w_NumCtrl < this.w_PAROBJ.ControlCount
      this.w_NumCtrl = this.w_NumCtrl + 1
      * --- Seleziono solo i controlli di tipo 'STDFIELD-STDCHECK-STDCOMBO-STDRADIO'
      if UPPER(this.w_PAROBJ.Controls(this.w_NumCtrl).class) $ "STDFIELD-STDCHECK-STDCOMBO-STDRADIO-STDTRSFIELD-STDTRSCHECK-STDTRSCOMBO-STDTRSRADIO"
        * --- Recupero il nome della variabile, elimino l'eventuale w_
        this.w_VARIABLE = IIF(LOWER(LEFT(this.w_PAROBJ.Controls(this.w_NumCtrl).cFormVar,2)) = "w_", SUBSTR(this.w_PAROBJ.Controls(this.w_NumCtrl).cFormVar, 3), this.w_PAROBJ.Controls(this.w_NumCtrl).cFormVar)
        * --- Recupero il tooltip della variabile
        this.w_DESC = this.w_PAROBJ.Controls(this.w_NumCtrl).tooltiptext
        * --- Recupero il valore della variabile per determinare il tipo
        w_valore=this.w_PAROBJ.Controls(this.w_NumCtrl).value
        this.w_TIPO = type("w_valore")
        * --- Sulla base del tipo determino la lunghezza e i decimali del controllo
        do case
          case this.w_TIPO = "D"
            * --- Data
            this.w_LUN = 8
            this.w_DEC = 0
          case this.w_TIPO = "T"
            * --- DataTime
            this.w_LUN = 19
            this.w_DEC = 0
          case this.w_TIPO = "N"
            * --- Numeric
            * --- Se il tipo � numerico devo controllare se il controllo � 'STDCOMBO-STDCHECK-STDRADIO'
            *     In tal caso devo recuperare il vero tipo utilizzato nel controllo
            if UPPER(this.w_PAROBJ.Controls(this.w_NumCtrl).class) $ "STDCOMBO-STDCHECK-STDRADIO-STDTRSCOMBO-STDTRSCHECK-STDTRSRADIO"
              * --- In caso di checlbox, radio e combo metto sempre la lunghezza a 1 perch� 
              *     non riesco a scoprire la vera linghezza
              w_valore=this.w_PAROBJ.Controls(this.w_NumCtrl).RadioValue()
              this.w_TIPO = type("w_valore")
              this.w_LUN = 1
              this.w_DEC = 0
            else
              this.w_PICT = ALLTRIM( STRTRAN(this.w_PAROBJ.Controls(this.w_NumCtrl).cGetPict, '"', "") )
              this.w_TIPO = type("w_valore")
              this.w_LUN = LEN(IIF(AT(".", this.w_PICT) <> 0, RIGHT(this.w_PICT, AT(".", this.w_PICT) -1 ), this.w_PICT))
              this.w_DEC = LEN(IIF(AT(".", this.w_PICT) <> 0, SUBSTR(this.w_PICT, AT(".", this.w_PICT) + 1), "" ))
            endif
          case this.w_TIPO = "L"
            * --- Logic
            this.w_LUN = 1
            this.w_DEC = 0
          case this.w_TIPO = "C"
            * --- Char
            this.w_LUN = LEN(w_valore)
            this.w_DEC = 0
        endcase
        * --- Inserisco la variabile nello zoom
        INSERT INTO (this.oParentObject.w_PARAMETRI.cCursor) (VARNAME, TOOLTIP, TIPO, LEN, DEC) VALUES (this.w_VARIABLE, this.w_DESC, this.w_TIPO, this.w_LUN, this.w_DEC)
      endif
    enddo
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
