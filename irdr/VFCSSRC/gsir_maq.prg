* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_maq                                                        *
*              Anagrafica query                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-04                                                      *
* Last revis.: 2016-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsir_maq"))

* --- Class definition
define class tgsir_maq as StdTrsForm
  Top    = 1
  Left   = 10

  * --- Standard Properties
  Width  = 807
  Height = 453+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-12"
  HelpContextID=137793687
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  INF_AQM_IDX = 0
  INF_AQD_IDX = 0
  UTE_NTI_IDX = 0
  cFile = "INF_AQM"
  cFileDetail = "INF_AQD"
  cKeySelect = "AQCODICE"
  cKeyWhere  = "AQCODICE=this.w_AQCODICE"
  cKeyDetail  = "AQCODICE=this.w_AQCODICE and AQMAILR=this.w_AQMAILR"
  cKeyWhereODBC = '"AQCODICE="+cp_ToStrODBC(this.w_AQCODICE)';

  cKeyDetailWhereODBC = '"AQCODICE="+cp_ToStrODBC(this.w_AQCODICE)';
      +'+" and AQMAILR="+cp_ToStrODBC(this.w_AQMAILR)';

  cKeyWhereODBCqualified = '"INF_AQD.AQCODICE="+cp_ToStrODBC(this.w_AQCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'INF_AQD.CPROWORD'
  cPrg = "gsir_maq"
  cComment = "Anagrafica query"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AQCODICE = space(20)
  w_AQDES = space(254)
  w_AQQUEFIL = space(1)
  o_AQQUEFIL = space(1)
  w_AQQRPATH = space(254)
  w_DTFILE = space(254)
  o_DTFILE = space(254)
  w_LOG = space(254)
  o_LOG = space(254)
  w_AQFORTYP = space(10)
  o_AQFORTYP = space(10)
  w_AQDTFILE = space(254)
  o_AQDTFILE = space(254)
  w_AQCRIPTE = space(1)
  w_AQADOCS = space(254)
  w_CPROWORD = 0
  w_AQFLUE = space(1)
  w_AQUTENTE = 0
  w_DESTUTE = space(40)
  w_AQMAILR = space(100)
  w_QRPATH = space(254)
  o_QRPATH = space(254)
  w_AQIMPATH = space(254)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_AQLOG = space(254)
  o_AQLOG = space(254)
  w_AQFLGZCP = space(1)
  w_AQCONTYP = space(15)
  w_AQCURLOC = space(1)
  w_AQCURTYP = space(20)
  w_AQTIMOUT = 0
  w_AVANZATE = .F.
  w_EXT = space(5)
  w_IMPATH = space(254)
  o_IMPATH = space(254)
  w_DUPLICA = space(1)
  w_ROW = 0
  w_AQFLJOIN = space(1)

  * --- Children pointers
  GSIR_MGI = .NULL.
  GSIR_MDV = .NULL.
  gsir_mfr = .NULL.
  GSIR_MPF = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsir_maq
  *--- Duplica query
  Proc Duplica()
       * --- Eseguo la duplicazione dei figli integrati e dei campi del master/detail
       this.w_DUPLICA='S'
     * --- Apro i figli integrati affinch� sia possibile copiarli
     * --- semplicemente aggiornando il transitorio
     If this.w_AQQUEFIL <> 'N'
       this.GSIR_MPF.LinkPcClick(.t.)
       this.GSIR_MFR.LinkPcClick(.t.)
     Endif
     * --- Vado in caricamento
       this.ecpLoad()
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INF_AQM','gsir_maq')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_maqPag1","gsir_maq",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati")
      .Pages(1).HelpContextID = 145176118
      .Pages(2).addobject("oPag","tgsir_maqPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Datawarehouse")
      .Pages(2).HelpContextID = 131726363
      .Pages(3).addobject("oPag","tgsir_maqPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Gestioni e filtri")
      .Pages(3).HelpContextID = 247818031
      .Pages(4).addobject("oPag","tgsir_maqPag4")
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Permessi Corporate Portal")
      .Pages(4).HelpContextID = 28754977
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAQCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='UTE_NTI'
    this.cWorkTables[2]='INF_AQM'
    this.cWorkTables[3]='INF_AQD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INF_AQM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INF_AQM_IDX,3]
  return

  function CreateChildren()
    this.GSIR_MGI = CREATEOBJECT('stdDynamicChild',this,'GSIR_MGI',this.oPgFrm.Page3.oPag.oLinkPC_5_1)
    this.GSIR_MDV = CREATEOBJECT('stdDynamicChild',this,'GSIR_MDV',this.oPgFrm.Page4.oPag.oLinkPC_6_1)
    this.gsir_mfr = CREATEOBJECT('stdLazyChild',this,'gsir_mfr')
    this.GSIR_MPF = CREATEOBJECT('stdLazyChild',this,'GSIR_MPF')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSIR_MGI)
      this.GSIR_MGI.DestroyChildrenChain()
      this.GSIR_MGI=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_5_1')
    if !ISNULL(this.GSIR_MDV)
      this.GSIR_MDV.DestroyChildrenChain()
      this.GSIR_MDV=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_6_1')
    if !ISNULL(this.gsir_mfr)
      this.gsir_mfr.DestroyChildrenChain()
      this.gsir_mfr=.NULL.
    endif
    if !ISNULL(this.GSIR_MPF)
      this.GSIR_MPF.DestroyChildrenChain()
      this.GSIR_MPF=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSIR_MGI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSIR_MDV.IsAChildUpdated()
      i_bRes = i_bRes .or. this.gsir_mfr.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSIR_MPF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSIR_MGI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSIR_MDV.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.gsir_mfr.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSIR_MPF.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSIR_MGI.NewDocument()
    this.GSIR_MDV.NewDocument()
    this.gsir_mfr.NewDocument()
    this.GSIR_MPF.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSIR_MGI.ChangeRow(this.cRowID+'      1',1;
             ,.w_AQCODICE,"GICODQUE";
             )
      .GSIR_MDV.ChangeRow(this.cRowID+'      1',1;
             ,.w_AQCODICE,"DECODICE";
             )
      .gsir_mfr.ChangeRow(this.cRowID+'      1',1;
             ,.w_AQCODICE,"FICODQUE";
             ,.w_ROW,"FIPRGRN";
             )
      .GSIR_MPF.ChangeRow(this.cRowID+'      1',1;
             ,.w_AQCODICE,"PFCODQUE";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_AQCODICE = NVL(AQCODICE,space(20))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from INF_AQM where AQCODICE=KeySet.AQCODICE
    *
    i_nConn = i_TableProp[this.INF_AQM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2],this.bLoadRecFilter,this.INF_AQM_IDX,"gsir_maq")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INF_AQM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INF_AQM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"INF_AQD.","INF_AQM.")
      i_cTable = i_cTable+' INF_AQM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AQCODICE',this.w_AQCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DTFILE = space(254)
        .w_LOG = space(254)
        .w_QRPATH = space(254)
        .w_OBTEST = i_DATSYS
        .w_DATOBSO = ctod("  /  /  ")
        .w_AVANZATE = .F.
        .w_IMPATH = space(254)
        .w_DUPLICA = 'N'
        .w_ROW = -1
        .w_AQCODICE = NVL(AQCODICE,space(20))
        .w_AQDES = NVL(AQDES,space(254))
        .w_AQQUEFIL = NVL(AQQUEFIL,space(1))
        .w_AQQRPATH = NVL(AQQRPATH,space(254))
        .w_AQFORTYP = NVL(AQFORTYP,space(10))
        .w_AQDTFILE = NVL(AQDTFILE,space(254))
        .w_AQCRIPTE = NVL(AQCRIPTE,space(1))
        .w_AQADOCS = NVL(AQADOCS,space(254))
        .oPgFrm.Page1.oPag.oObj_3_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .w_AQIMPATH = NVL(AQIMPATH,space(254))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_AQLOG = NVL(AQLOG,space(254))
        .oPgFrm.Page1.oPag.oObj_3_8.Calculate()
        .w_AQFLGZCP = NVL(AQFLGZCP,space(1))
        .w_AQCONTYP = NVL(AQCONTYP,space(15))
        .w_AQCURLOC = NVL(AQCURLOC,space(1))
        .w_AQCURTYP = NVL(AQCURTYP,space(20))
        .w_AQTIMOUT = NVL(AQTIMOUT,0)
        .w_EXT = IIF(.w_AQFORTYP = 'efQuery', 'IRP', IIF(.w_AQFORTYP = 'efRTF', 'RTF', IIF(.w_AQFORTYP = 'efExcel', 'XLSX', IIF(.w_AQFORTYP = 'efHTML', 'HTM', IIF(.w_AQFORTYP = 'efPDF', 'PDF', 'CSV')))))
        .oPgFrm.Page3.oPag.oObj_5_4.Calculate(.w_AQCODICE)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_AQFLJOIN = NVL(AQFLJOIN,space(1))
        cp_LoadRecExtFlds(this,'INF_AQM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from INF_AQD where AQCODICE=KeySet.AQCODICE
      *                            and AQMAILR=KeySet.AQMAILR
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.INF_AQD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AQD_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('INF_AQD')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "INF_AQD.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" INF_AQD"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'AQCODICE',this.w_AQCODICE  )
        select * from (i_cTable) INF_AQD where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESTUTE = space(40)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_AQFLUE = NVL(AQFLUE,space(1))
          .w_AQUTENTE = NVL(AQUTENTE,0)
          .link_2_3('Load')
          .w_AQMAILR = NVL(AQMAILR,space(100))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace AQMAILR with .w_AQMAILR
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_3_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_8.Calculate()
        .w_EXT = IIF(.w_AQFORTYP = 'efQuery', 'IRP', IIF(.w_AQFORTYP = 'efRTF', 'RTF', IIF(.w_AQFORTYP = 'efExcel', 'XLSX', IIF(.w_AQFORTYP = 'efHTML', 'HTM', IIF(.w_AQFORTYP = 'efPDF', 'PDF', 'CSV')))))
        .oPgFrm.Page3.oPag.oObj_5_4.Calculate(.w_AQCODICE)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_3_16.enabled = .oPgFrm.Page1.oPag.oBtn_3_16.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsir_maq
    * --- Per Duplicazione: eseguo i metodi usati nella BlankRec senza per�
    * --- sbiancare le variabili
    LOCAL OLD_KEY
    IF this.w_DUPLICA='S' AND this.cFunction='Load' AND NOT EMPTY(this.w_AQCODICE)
     * --- Salvo la chiave e poi la sbianco
       OLD_KEY=this.w_AQCODICE
       this.w_AQCODICE=Space(20)
       * --- Inizio: assegnamenti per far si che i figli integrati vengano salvati
       * --- Aggiorno i_Srv di tutte i record anche di quelli marcati come deleted
       * --- affinch� vengano salvati i contenuti dei figli e del dettaglio
       * --- Esempio: Carico un record gi� esistente, entro in modifica (F3), elimino una riga, salvo F(10)
       * --- premo il bottone duplica e salvo il duplicato. Se non aggiorno i_Srv della riga cancellata,
       * --- la procedura tenta di cancellare dal DB la riga segnalando ovviamente un messaggio di errore
       * --- "Record modificato da un altro utente", aggiornando i_Srv la mReplace non fa nulla per i vecchi
       * --- record marcati come deleted
       this.MarkPos()
         set delete off
         update (this.cTrsName) set i_Srv='A' where 1=1
         set delete on
       this.Repos()
     * --- Duplico le gestioni
       this.GSIR_MGI.bLoaded=.f.
       this.GSIR_MGI.bUpdated=.t.
       this.GSIR_MGI.MarkPos()
         set delete off
         update (this.GSIR_MGI.cTrsName) set i_Srv='A' where 1=1
         set delete on
       this.GSIR_MGI.RePos()
     * --- Duplico la pagina del Corporate Portal
       IF this.w_AQFLGZCP='S' and g_IZCP='S' AND INLIST(this.w_AQQUEFIL,'S','N')
         this.GSIR_MDV.bLoaded=.f.
         this.GSIR_MDV.bUpdated=.t.
         this.GSIR_MDV.MarkPos()
           set delete off
           update (this.GSIR_MDV.cTrsName) set i_Srv='A' where 1=1
           set delete on
        this.GSIR_MDV.RePos()
       ELSE
         this.GSIR_MDV.bLoaded=.t.
         this.GSIR_MDV.bUpdated=.f.
       ENDIF
    
     * --- Se sto duplicando un query Std o filtrata duplico anche i parametri e i filtri
       IF this.w_AQQUEFIL <> 'N'
       * --- Duplica Parametri
         this.GSIR_MPF.cnt.bLoaded=.f.
         this.GSIR_MPF.cnt.bUpdated=.t.
         this.GSIR_MPF.cnt.MarkPos()
           set delete off
           update (this.GSIR_MPF.cnt.cTrsName) set i_Srv='A' where 1=1
           set delete on
         this.GSIR_MPF.cnt.RePos()
       * --- Duplica filtri generici
         this.GSIR_MFR.cnt.bLoaded=.f.
         this.GSIR_MFR.cnt.bUpdated=.t.
         this.GSIR_MFR.cnt.MarkPos()
         set delete off
           update (this.GSIR_MFR.cnt.cTrsName) set i_Srv='A' where 1=1
           set delete on
         this.GSIR_MFR.cnt.RePos()
       * --- Duplica filtri gestioni
       * --- Pulisco il transitorio
         this.GSIR_MGI.GSIR_MFR.NewDocument()
       * --- Eseguo il batch che legger� i dati direttamente dal DB e popoler�
       * --- le relative gestioni
         DO GSIR_BDQ with this, OLD_KEY
       ELSE
       * --- Svuoto le gestioni figlie
         this.GSIR_MPF.NewDocument()
         this.GSIR_MFR.NewDocument()
         this.GSIR_MGI.GSIR_MFR.NewDocument()
       ENDIF
       * --- Rimetto w_DUPLICA a 'N' per tornare nel ciclo normale
       this.w_DUPLICA='N'
     * ---
       this.SaveDependsOn()
       *Ripeto i link per problema sicurezza sul record E0000017549 1.0
       local L_MACRO, nAttIdx
       AMEMBERS(aMethods,this,1)
       FOR nAttIdx = 1 TO ALEN(aMethods, 1)
         If aMethods(nAttIdx, 2)=="Method" AND LEFT(aMethods(nAttIdx, 1), 5)=="LINK_" AND !aMethods(nAttIdx, 1)=="LINK_1_1"
           L_MACRO = "this." + ALLTRIM(aMethods(nAttIdx, 1)) + "('Full')"
           &L_MACRO
         EndIf
       NEXT
       Release aMethods, nAttIdx, L_MACRO
       this.SetControlsValue()
       this.mHideControls()
       this.ChildrenChangeRow()
       this.NotifyEvent('Blank')
       this.mCalc(.t.)
       return
    Endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_AQCODICE=space(20)
      .w_AQDES=space(254)
      .w_AQQUEFIL=space(1)
      .w_AQQRPATH=space(254)
      .w_DTFILE=space(254)
      .w_LOG=space(254)
      .w_AQFORTYP=space(10)
      .w_AQDTFILE=space(254)
      .w_AQCRIPTE=space(1)
      .w_AQADOCS=space(254)
      .w_CPROWORD=10
      .w_AQFLUE=space(1)
      .w_AQUTENTE=0
      .w_DESTUTE=space(40)
      .w_AQMAILR=space(100)
      .w_QRPATH=space(254)
      .w_AQIMPATH=space(254)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_AQLOG=space(254)
      .w_AQFLGZCP=space(1)
      .w_AQCONTYP=space(15)
      .w_AQCURLOC=space(1)
      .w_AQCURTYP=space(20)
      .w_AQTIMOUT=0
      .w_AVANZATE=.f.
      .w_EXT=space(5)
      .w_IMPATH=space(254)
      .w_DUPLICA=space(1)
      .w_ROW=0
      .w_AQFLJOIN=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_AQQUEFIL = 'S'
        .w_AQQRPATH = .w_QRPATH
        .DoRTCalc(5,6,.f.)
        .w_AQFORTYP = 'efQuery'
        .w_AQDTFILE = .w_DTFILE
        .w_AQCRIPTE = 'N'
        .DoRTCalc(10,11,.f.)
        .w_AQFLUE = 'U'
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_AQUTENTE))
         .link_2_3('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_3_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(14,16,.f.)
        .w_AQIMPATH = .w_IMPATH
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_OBTEST = i_DATSYS
        .DoRTCalc(19,19,.f.)
        .w_AQLOG = .w_LOG
        .oPgFrm.Page1.oPag.oObj_3_8.Calculate()
        .DoRTCalc(21,21,.f.)
        .w_AQCONTYP = CP_DBTYPE
        .w_AQCURLOC = 'S'
        .w_AQCURTYP = 'ctOpenForwardOnly'
        .w_AQTIMOUT = 30
        .w_AVANZATE = .F.
        .w_EXT = IIF(.w_AQFORTYP = 'efQuery', 'IRP', IIF(.w_AQFORTYP = 'efRTF', 'RTF', IIF(.w_AQFORTYP = 'efExcel', 'XLSX', IIF(.w_AQFORTYP = 'efHTML', 'HTM', IIF(.w_AQFORTYP = 'efPDF', 'PDF', 'CSV')))))
        .DoRTCalc(28,28,.f.)
        .w_DUPLICA = 'N'
        .w_ROW = -1
        .oPgFrm.Page3.oPag.oObj_5_4.Calculate(.w_AQCODICE)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_AQFLJOIN = 'S'
      endif
    endwith
    cp_BlankRecExtFlds(this,'INF_AQM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_3_16.enabled = this.oPgFrm.Page1.oPag.oBtn_3_16.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAQCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oAQDES_1_2.enabled = i_bVal
      .Page1.oPag.oAQQUEFIL_1_3.enabled = i_bVal
      .Page1.oPag.oAQQRPATH_1_5.enabled = i_bVal
      .Page1.oPag.oAQFORTYP_3_3.enabled = i_bVal
      .Page1.oPag.oAQDTFILE_3_4.enabled = i_bVal
      .Page2.oPag.oAQADOCS_4_2.enabled = i_bVal
      .Page1.oPag.oAQIMPATH_1_9.enabled = i_bVal
      .Page1.oPag.oAQLOG_3_7.enabled = i_bVal
      .Page1.oPag.oAQFLGZCP_3_9.enabled = i_bVal
      .Page2.oPag.oAQCONTYP_4_7.enabled = i_bVal
      .Page2.oPag.oAQCURLOC_4_8.enabled = i_bVal
      .Page2.oPag.oAQCURTYP_4_9.enabled = i_bVal
      .Page2.oPag.oAQTIMOUT_4_10.enabled = i_bVal
      .Page2.oPag.oAVANZATE_4_12.enabled = i_bVal
      .Page2.oPag.oAQFLJOIN_4_19.enabled = i_bVal
      .Page2.oPag.oBtn_4_3.enabled = i_bVal
      .Page2.oPag.oBtn_4_4.enabled = i_bVal
      .Page2.oPag.oBtn_4_5.enabled = i_bVal
      .Page1.oPag.oBtn_3_16.enabled = .Page1.oPag.oBtn_3_16.mCond()
      .Page1.oPag.oObj_3_6.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oObj_1_10.enabled = i_bVal
      .Page1.oPag.oObj_3_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oAQCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oAQCODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSIR_MGI.SetStatus(i_cOp)
    this.GSIR_MDV.SetStatus(i_cOp)
    this.gsir_mfr.SetStatus(i_cOp)
    this.GSIR_MPF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'INF_AQM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSIR_MGI.SetChildrenStatus(i_cOp)
  *  this.GSIR_MDV.SetChildrenStatus(i_cOp)
  *  this.gsir_mfr.SetChildrenStatus(i_cOp)
  *  this.GSIR_MPF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INF_AQM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQCODICE,"AQCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQDES,"AQDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQQUEFIL,"AQQUEFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQQRPATH,"AQQRPATH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQFORTYP,"AQFORTYP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQDTFILE,"AQDTFILE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQCRIPTE,"AQCRIPTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQADOCS,"AQADOCS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQIMPATH,"AQIMPATH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQLOG,"AQLOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQFLGZCP,"AQFLGZCP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQCONTYP,"AQCONTYP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQCURLOC,"AQCURLOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQCURTYP,"AQCURTYP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQTIMOUT,"AQTIMOUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AQFLJOIN,"AQFLJOIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INF_AQM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2])
    i_lTable = "INF_AQM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INF_AQM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsir_saq with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_AQFLUE N(3);
      ,t_AQUTENTE N(4);
      ,t_DESTUTE C(40);
      ,t_AQMAILR C(100);
      ,AQMAILR C(100);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsir_maqbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAQFLUE_2_2.controlsource=this.cTrsName+'.t_AQFLUE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAQUTENTE_2_3.controlsource=this.cTrsName+'.t_AQUTENTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESTUTE_2_4.controlsource=this.cTrsName+'.t_DESTUTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAQMAILR_2_5.controlsource=this.cTrsName+'.t_AQMAILR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(56)
    this.AddVLine(142)
    this.AddVLine(201)
    this.AddVLine(374)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_AQM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INF_AQM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INF_AQM')
        i_extval=cp_InsertValODBCExtFlds(this,'INF_AQM')
        local i_cFld
        i_cFld=" "+;
                  "(AQCODICE,AQDES,AQQUEFIL,AQQRPATH,AQFORTYP"+;
                  ",AQDTFILE,AQCRIPTE,AQADOCS,AQIMPATH,AQLOG"+;
                  ",AQFLGZCP,AQCONTYP,AQCURLOC,AQCURTYP,AQTIMOUT"+;
                  ",AQFLJOIN"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_AQCODICE)+;
                    ","+cp_ToStrODBC(this.w_AQDES)+;
                    ","+cp_ToStrODBC(this.w_AQQUEFIL)+;
                    ","+cp_ToStrODBC(this.w_AQQRPATH)+;
                    ","+cp_ToStrODBC(this.w_AQFORTYP)+;
                    ","+cp_ToStrODBC(this.w_AQDTFILE)+;
                    ","+cp_ToStrODBC(this.w_AQCRIPTE)+;
                    ","+cp_ToStrODBC(this.w_AQADOCS)+;
                    ","+cp_ToStrODBC(this.w_AQIMPATH)+;
                    ","+cp_ToStrODBC(this.w_AQLOG)+;
                    ","+cp_ToStrODBC(this.w_AQFLGZCP)+;
                    ","+cp_ToStrODBC(this.w_AQCONTYP)+;
                    ","+cp_ToStrODBC(this.w_AQCURLOC)+;
                    ","+cp_ToStrODBC(this.w_AQCURTYP)+;
                    ","+cp_ToStrODBC(this.w_AQTIMOUT)+;
                    ","+cp_ToStrODBC(this.w_AQFLJOIN)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INF_AQM')
        i_extval=cp_InsertValVFPExtFlds(this,'INF_AQM')
        cp_CheckDeletedKey(i_cTable,0,'AQCODICE',this.w_AQCODICE)
        INSERT INTO (i_cTable);
              (AQCODICE,AQDES,AQQUEFIL,AQQRPATH,AQFORTYP,AQDTFILE,AQCRIPTE,AQADOCS,AQIMPATH,AQLOG,AQFLGZCP,AQCONTYP,AQCURLOC,AQCURTYP,AQTIMOUT,AQFLJOIN &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_AQCODICE;
                  ,this.w_AQDES;
                  ,this.w_AQQUEFIL;
                  ,this.w_AQQRPATH;
                  ,this.w_AQFORTYP;
                  ,this.w_AQDTFILE;
                  ,this.w_AQCRIPTE;
                  ,this.w_AQADOCS;
                  ,this.w_AQIMPATH;
                  ,this.w_AQLOG;
                  ,this.w_AQFLGZCP;
                  ,this.w_AQCONTYP;
                  ,this.w_AQCURLOC;
                  ,this.w_AQCURTYP;
                  ,this.w_AQTIMOUT;
                  ,this.w_AQFLJOIN;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INF_AQD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AQD_IDX,2])
      *
      * insert into INF_AQD
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(AQCODICE,CPROWORD,AQFLUE,AQUTENTE,AQMAILR,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_AQCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_AQFLUE)+","+cp_ToStrODBCNull(this.w_AQUTENTE)+","+cp_ToStrODBC(this.w_AQMAILR)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'AQCODICE',this.w_AQCODICE,'AQMAILR',this.w_AQMAILR)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_AQCODICE,this.w_CPROWORD,this.w_AQFLUE,this.w_AQUTENTE,this.w_AQMAILR,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.INF_AQM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update INF_AQM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'INF_AQM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " AQDES="+cp_ToStrODBC(this.w_AQDES)+;
             ",AQQUEFIL="+cp_ToStrODBC(this.w_AQQUEFIL)+;
             ",AQQRPATH="+cp_ToStrODBC(this.w_AQQRPATH)+;
             ",AQFORTYP="+cp_ToStrODBC(this.w_AQFORTYP)+;
             ",AQDTFILE="+cp_ToStrODBC(this.w_AQDTFILE)+;
             ",AQCRIPTE="+cp_ToStrODBC(this.w_AQCRIPTE)+;
             ",AQADOCS="+cp_ToStrODBC(this.w_AQADOCS)+;
             ",AQIMPATH="+cp_ToStrODBC(this.w_AQIMPATH)+;
             ",AQLOG="+cp_ToStrODBC(this.w_AQLOG)+;
             ",AQFLGZCP="+cp_ToStrODBC(this.w_AQFLGZCP)+;
             ",AQCONTYP="+cp_ToStrODBC(this.w_AQCONTYP)+;
             ",AQCURLOC="+cp_ToStrODBC(this.w_AQCURLOC)+;
             ",AQCURTYP="+cp_ToStrODBC(this.w_AQCURTYP)+;
             ",AQTIMOUT="+cp_ToStrODBC(this.w_AQTIMOUT)+;
             ",AQFLJOIN="+cp_ToStrODBC(this.w_AQFLJOIN)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'INF_AQM')
          i_cWhere = cp_PKFox(i_cTable  ,'AQCODICE',this.w_AQCODICE  )
          UPDATE (i_cTable) SET;
              AQDES=this.w_AQDES;
             ,AQQUEFIL=this.w_AQQUEFIL;
             ,AQQRPATH=this.w_AQQRPATH;
             ,AQFORTYP=this.w_AQFORTYP;
             ,AQDTFILE=this.w_AQDTFILE;
             ,AQCRIPTE=this.w_AQCRIPTE;
             ,AQADOCS=this.w_AQADOCS;
             ,AQIMPATH=this.w_AQIMPATH;
             ,AQLOG=this.w_AQLOG;
             ,AQFLGZCP=this.w_AQFLGZCP;
             ,AQCONTYP=this.w_AQCONTYP;
             ,AQCURLOC=this.w_AQCURLOC;
             ,AQCURTYP=this.w_AQCURTYP;
             ,AQTIMOUT=this.w_AQTIMOUT;
             ,AQFLJOIN=this.w_AQFLJOIN;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_AQMAILR))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.INF_AQD_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.INF_AQD_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from INF_AQD
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and AQMAILR="+cp_ToStrODBC(&i_TN.->AQMAILR)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and AQMAILR=&i_TN.->AQMAILR;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace AQMAILR with this.w_AQMAILR
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INF_AQD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",AQFLUE="+cp_ToStrODBC(this.w_AQFLUE)+;
                     ",AQUTENTE="+cp_ToStrODBCNull(this.w_AQUTENTE)+;
                     " ,AQMAILR="+cp_ToStrODBC(this.w_AQMAILR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and AQMAILR="+cp_ToStrODBC(AQMAILR)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,AQFLUE=this.w_AQFLUE;
                     ,AQUTENTE=this.w_AQUTENTE;
                     ,AQMAILR=this.w_AQMAILR;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and AQMAILR=&i_TN.->AQMAILR;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSIR_MGI : Saving
      this.GSIR_MGI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_AQCODICE,"GICODQUE";
             )
      this.GSIR_MGI.mReplace()
      * --- GSIR_MDV : Saving
      this.GSIR_MDV.ChangeRow(this.cRowID+'      1',0;
             ,this.w_AQCODICE,"DECODICE";
             )
      this.GSIR_MDV.mReplace()
      * --- gsir_mfr : Saving
      this.gsir_mfr.ChangeRow(this.cRowID+'      1',0;
             ,this.w_AQCODICE,"FICODQUE";
             ,this.w_ROW,"FIPRGRN";
             )
      this.gsir_mfr.mReplace()
      * --- GSIR_MPF : Saving
      this.GSIR_MPF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_AQCODICE,"PFCODQUE";
             )
      this.GSIR_MPF.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSIR_MGI : Deleting
    this.GSIR_MGI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_AQCODICE,"GICODQUE";
           )
    this.GSIR_MGI.mDelete()
    * --- GSIR_MDV : Deleting
    this.GSIR_MDV.ChangeRow(this.cRowID+'      1',0;
           ,this.w_AQCODICE,"DECODICE";
           )
    this.GSIR_MDV.mDelete()
    * --- gsir_mfr : Deleting
    this.gsir_mfr.ChangeRow(this.cRowID+'      1',0;
           ,this.w_AQCODICE,"FICODQUE";
           ,this.w_ROW,"FIPRGRN";
           )
    this.gsir_mfr.mDelete()
    * --- GSIR_MPF : Deleting
    this.GSIR_MPF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_AQCODICE,"PFCODQUE";
           )
    this.GSIR_MPF.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_AQMAILR))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.INF_AQD_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.INF_AQD_IDX,2])
        *
        * delete INF_AQD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and AQMAILR="+cp_ToStrODBC(&i_TN.->AQMAILR)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and AQMAILR=&i_TN.->AQMAILR;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.INF_AQM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2])
        *
        * delete INF_AQM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_AQMAILR))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INF_AQM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_QRPATH<>.w_QRPATH
          .w_AQQRPATH = .w_QRPATH
        endif
        .DoRTCalc(5,7,.t.)
        if .o_DTFILE<>.w_DTFILE
          .w_AQDTFILE = .w_DTFILE
        endif
        .oPgFrm.Page1.oPag.oObj_3_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(9,16,.t.)
        if .o_IMPATH<>.w_IMPATH
          .w_AQIMPATH = .w_IMPATH
        endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(18,19,.t.)
        if .o_LOG<>.w_LOG
          .w_AQLOG = .w_LOG
        endif
        .oPgFrm.Page1.oPag.oObj_3_8.Calculate()
        .DoRTCalc(21,26,.t.)
        if .o_AQFORTYP<>.w_AQFORTYP
          .w_EXT = IIF(.w_AQFORTYP = 'efQuery', 'IRP', IIF(.w_AQFORTYP = 'efRTF', 'RTF', IIF(.w_AQFORTYP = 'efExcel', 'XLSX', IIF(.w_AQFORTYP = 'efHTML', 'HTM', IIF(.w_AQFORTYP = 'efPDF', 'PDF', 'CSV')))))
        endif
        if .o_AQFORTYP<>.w_AQFORTYP.or. .o_AQDTFILE<>.w_AQDTFILE
          .Calculate_HUNFDDTMKQ()
        endif
        if .o_AQLOG<>.w_AQLOG
          .Calculate_QYOKBBZQXN()
        endif
        .oPgFrm.Page3.oPag.oObj_5_4.Calculate(.w_AQCODICE)
        if .o_AQQUEFIL<>.w_AQQUEFIL
          .Calculate_RZABLJQGPN()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_3_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_8.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_4.Calculate(.w_AQCODICE)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_HUNFDDTMKQ()
    with this
          * --- Cambio estensione file di destinazione
          .w_AQDTFILE = MODIFEXT( .w_AQDTFILE, .w_EXT, .T. )
    endwith
  endproc
  proc Calculate_QYOKBBZQXN()
    with this
          * --- Cambio estensione file di log
          .w_AQLOG = MODIFEXT( .w_AQLOG, 'TXT', .T. )
    endwith
  endproc
  proc Calculate_DLLSFBDAXQ()
    with this
          * --- Cancello autorizzazione query
          gsir1bqa(this;
              ,'Q';
             )
    endwith
  endproc
  proc Calculate_RZABLJQGPN()
    with this
          * --- Nascondifiltri
          NascondiFiltri(this;
             )
    endwith
  endproc
  proc Calculate_JMCSEOEWRW()
    with this
          * --- Aggiorna tabella vista
          gsir_btv(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAQDTFILE_3_4.enabled = this.oPgFrm.Page1.oPag.oAQDTFILE_3_4.mCond()
    this.oPgFrm.Page1.oPag.oAQFLGZCP_3_9.enabled = this.oPgFrm.Page1.oPag.oAQFLGZCP_3_9.mCond()
    this.oPgFrm.Page2.oPag.oAQCURTYP_4_9.enabled = this.oPgFrm.Page2.oPag.oAQCURTYP_4_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_4.enabled = this.oPgFrm.Page2.oPag.oBtn_4_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_5.enabled = this.oPgFrm.Page2.oPag.oBtn_4_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_3_16.enabled = this.oPgFrm.Page1.oPag.oBtn_3_16.mCond()
    this.GSIR_MDV.enabled = this.oPgFrm.Page4.oPag.oLinkPC_6_1.mCond()
    this.oPgFrm.Page3.oPag.oLinkPC_5_3.enabled = this.oPgFrm.Page3.oPag.oLinkPC_5_3.mCond()
    this.oPgFrm.Page3.oPag.oLinkPC_5_7.enabled = this.oPgFrm.Page3.oPag.oLinkPC_5_7.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAQFLUE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAQFLUE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAQUTENTE_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAQUTENTE_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAQMAILR_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAQMAILR_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    local i_show3
    i_show3=not(NOT g_IZCP$'SA')
    this.oPgFrm.Pages(4).enabled=i_show3
    this.oPgFrm.Pages(4).caption=iif(i_show3,cp_translate("Permessi Corporate Portal"),"")
    this.oPgFrm.Pages(4).oPag.visible=this.oPgFrm.Pages(4).enabled
    this.oPgFrm.Page2.oPag.oBtn_4_4.visible=!this.oPgFrm.Page2.oPag.oBtn_4_4.mHide()
    this.oPgFrm.Page2.oPag.oBtn_4_5.visible=!this.oPgFrm.Page2.oPag.oBtn_4_5.mHide()
    this.oPgFrm.Page2.oPag.oAQCURLOC_4_8.visible=!this.oPgFrm.Page2.oPag.oAQCURLOC_4_8.mHide()
    this.oPgFrm.Page2.oPag.oAQCURTYP_4_9.visible=!this.oPgFrm.Page2.oPag.oAQCURTYP_4_9.mHide()
    this.oPgFrm.Page2.oPag.oAQTIMOUT_4_10.visible=!this.oPgFrm.Page2.oPag.oAQTIMOUT_4_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_13.visible=!this.oPgFrm.Page2.oPag.oStr_4_13.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_14.visible=!this.oPgFrm.Page2.oPag.oStr_4_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_15.visible=!this.oPgFrm.Page2.oPag.oStr_4_15.mHide()
    this.oPgFrm.Page1.oPag.oBtn_3_16.visible=!this.oPgFrm.Page1.oPag.oBtn_3_16.mHide()
    this.oPgFrm.Page3.oPag.oLinkPC_5_3.visible=!this.oPgFrm.Page3.oPag.oLinkPC_5_3.mHide()
    this.oPgFrm.Page3.oPag.oLinkPC_5_7.visible=!this.oPgFrm.Page3.oPag.oLinkPC_5_7.mHide()
    this.oPgFrm.Page2.oPag.oAQFLJOIN_4_19.visible=!this.oPgFrm.Page2.oPag.oAQFLJOIN_4_19.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsir_maq
            * istanzio immediatamente i figli della terza pagina
            IF Upper(CEVENT)='INIT'
             if Upper(this.GSIR_MGI.class)='STDDYNAMICCHILD'
               This.oPgFrm.Pages[3].opag.uienable(.T.)
             Endif
             if Upper(this.GSIR_MDV.class)='STDDYNAMICCHILD'
               This.oPgFrm.Pages[4].opag.uienable(.T.)
             Endif
             This.oPgFrm.ActivePage=1
            Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_3_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_3_8.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_5_4.Event(cEvent)
        if lower(cEvent)==lower("Delete end")
          .Calculate_DLLSFBDAXQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Deleted") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_JMCSEOEWRW()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AQUTENTE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
    i_lTable = "UTE_NTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2], .t., this.UTE_NTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AQUTENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UTE_NTI')
        if i_nConn<>0
          i_cWhere = " UTCODICE="+cp_ToStrODBC(this.w_AQUTENTE);

          i_ret=cp_SQL(i_nConn,"select UTCODICE,UTEMAIL,UTDESUTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UTCODICE',this.w_AQUTENTE)
          select UTCODICE,UTEMAIL,UTDESUTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_AQUTENTE) and !this.bDontReportError
            deferred_cp_zoom('UTE_NTI','*','UTCODICE',cp_AbsName(oSource.parent,'oAQUTENTE_2_3'),i_cWhere,'',"Elenco utenti dotati di E-mail",'gsir_maq.UTE_NTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UTCODICE,UTEMAIL,UTDESUTE";
                     +" from "+i_cTable+" "+i_lTable+" where UTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UTCODICE',oSource.xKey(1))
            select UTCODICE,UTEMAIL,UTDESUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AQUTENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UTCODICE,UTEMAIL,UTDESUTE";
                   +" from "+i_cTable+" "+i_lTable+" where UTCODICE="+cp_ToStrODBC(this.w_AQUTENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UTCODICE',this.w_AQUTENTE)
            select UTCODICE,UTEMAIL,UTDESUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AQUTENTE = NVL(_Link_.UTCODICE,0)
      this.w_AQMAILR = NVL(_Link_.UTEMAIL,space(100))
      this.w_DESTUTE = NVL(_Link_.UTDESUTE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_AQUTENTE = 0
      endif
      this.w_AQMAILR = space(100)
      this.w_DESTUTE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])+'\'+cp_ToStr(_Link_.UTCODICE,1)
      cp_ShowWarn(i_cKey,this.UTE_NTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AQUTENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oAQCODICE_1_1.value==this.w_AQCODICE)
      this.oPgFrm.Page1.oPag.oAQCODICE_1_1.value=this.w_AQCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oAQDES_1_2.value==this.w_AQDES)
      this.oPgFrm.Page1.oPag.oAQDES_1_2.value=this.w_AQDES
    endif
    if not(this.oPgFrm.Page1.oPag.oAQQUEFIL_1_3.RadioValue()==this.w_AQQUEFIL)
      this.oPgFrm.Page1.oPag.oAQQUEFIL_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAQQRPATH_1_5.value==this.w_AQQRPATH)
      this.oPgFrm.Page1.oPag.oAQQRPATH_1_5.value=this.w_AQQRPATH
    endif
    if not(this.oPgFrm.Page1.oPag.oAQFORTYP_3_3.RadioValue()==this.w_AQFORTYP)
      this.oPgFrm.Page1.oPag.oAQFORTYP_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAQDTFILE_3_4.value==this.w_AQDTFILE)
      this.oPgFrm.Page1.oPag.oAQDTFILE_3_4.value=this.w_AQDTFILE
    endif
    if not(this.oPgFrm.Page2.oPag.oAQCRIPTE_4_1.RadioValue()==this.w_AQCRIPTE)
      this.oPgFrm.Page2.oPag.oAQCRIPTE_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQADOCS_4_2.value==this.w_AQADOCS)
      this.oPgFrm.Page2.oPag.oAQADOCS_4_2.value=this.w_AQADOCS
    endif
    if not(this.oPgFrm.Page1.oPag.oAQIMPATH_1_9.value==this.w_AQIMPATH)
      this.oPgFrm.Page1.oPag.oAQIMPATH_1_9.value=this.w_AQIMPATH
    endif
    if not(this.oPgFrm.Page1.oPag.oAQLOG_3_7.value==this.w_AQLOG)
      this.oPgFrm.Page1.oPag.oAQLOG_3_7.value=this.w_AQLOG
    endif
    if not(this.oPgFrm.Page1.oPag.oAQFLGZCP_3_9.RadioValue()==this.w_AQFLGZCP)
      this.oPgFrm.Page1.oPag.oAQFLGZCP_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQCONTYP_4_7.RadioValue()==this.w_AQCONTYP)
      this.oPgFrm.Page2.oPag.oAQCONTYP_4_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQCURLOC_4_8.RadioValue()==this.w_AQCURLOC)
      this.oPgFrm.Page2.oPag.oAQCURLOC_4_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQCURTYP_4_9.RadioValue()==this.w_AQCURTYP)
      this.oPgFrm.Page2.oPag.oAQCURTYP_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQTIMOUT_4_10.value==this.w_AQTIMOUT)
      this.oPgFrm.Page2.oPag.oAQTIMOUT_4_10.value=this.w_AQTIMOUT
    endif
    if not(this.oPgFrm.Page2.oPag.oAVANZATE_4_12.RadioValue()==this.w_AVANZATE)
      this.oPgFrm.Page2.oPag.oAVANZATE_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAQFLJOIN_4_19.RadioValue()==this.w_AQFLJOIN)
      this.oPgFrm.Page2.oPag.oAQFLJOIN_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQFLUE_2_2.RadioValue()==this.w_AQFLUE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQFLUE_2_2.SetRadio()
      replace t_AQFLUE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQFLUE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQUTENTE_2_3.value==this.w_AQUTENTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQUTENTE_2_3.value=this.w_AQUTENTE
      replace t_AQUTENTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQUTENTE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESTUTE_2_4.value==this.w_DESTUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESTUTE_2_4.value=this.w_DESTUTE
      replace t_DESTUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESTUTE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQMAILR_2_5.value==this.w_AQMAILR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQMAILR_2_5.value=this.w_AQMAILR
      replace t_AQMAILR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQMAILR_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'INF_AQM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AQCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oAQCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_AQCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSIR_MGI.CheckForm()
      if i_bres
        i_bres=  .GSIR_MGI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSIR_MDV.CheckForm()
      if i_bres
        i_bres=  .GSIR_MDV.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .gsir_mfr.CheckForm()
      if i_bres
        i_bres=  .gsir_mfr.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSIR_MPF.CheckForm()
      if i_bres
        i_bres=  .GSIR_MPF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsir_maq
      if .w_AQFLGZCP='S'
         if VarType(.Gsir_mdv)='O'
           if empty(.Gsir_mdv.w_DEDESFIL)
               .Gsir_mdv.w_DEDESFIL = alltrim(.w_AQDES)
               .Gsir_mdv.refresh()
           endif
         else
           * Il figlio non � stato neppure istanziato
           ah_ErrorMsg("Indicare i permessi per DMS Corporate Portal nell'apposita sezione",48)
           i_bRes = .f.
         endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_AQMAILR))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AQQUEFIL = this.w_AQQUEFIL
    this.o_DTFILE = this.w_DTFILE
    this.o_LOG = this.w_LOG
    this.o_AQFORTYP = this.w_AQFORTYP
    this.o_AQDTFILE = this.w_AQDTFILE
    this.o_QRPATH = this.w_QRPATH
    this.o_AQLOG = this.w_AQLOG
    this.o_IMPATH = this.w_IMPATH
    * --- GSIR_MGI : Depends On
    this.GSIR_MGI.SaveDependsOn()
    * --- GSIR_MDV : Depends On
    this.GSIR_MDV.SaveDependsOn()
    * --- gsir_mfr : Depends On
    this.gsir_mfr.SaveDependsOn()
    * --- GSIR_MPF : Depends On
    this.GSIR_MPF.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_AQMAILR)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_AQFLUE=space(1)
      .w_AQUTENTE=0
      .w_DESTUTE=space(40)
      .w_AQMAILR=space(100)
      .DoRTCalc(1,11,.f.)
        .w_AQFLUE = 'U'
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_AQUTENTE))
        .link_2_3('Full')
      endif
    endwith
    this.DoRTCalc(14,31,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_AQFLUE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQFLUE_2_2.RadioValue(.t.)
    this.w_AQUTENTE = t_AQUTENTE
    this.w_DESTUTE = t_DESTUTE
    this.w_AQMAILR = t_AQMAILR
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_AQFLUE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAQFLUE_2_2.ToRadio()
    replace t_AQUTENTE with this.w_AQUTENTE
    replace t_DESTUTE with this.w_DESTUTE
    replace t_AQMAILR with this.w_AQMAILR
    if i_srv='A'
      replace AQMAILR with this.w_AQMAILR
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsir_maqPag1 as StdContainer
  Width  = 803
  height = 453
  stdWidth  = 803
  stdheight = 453
  resizeXpos=329
  resizeYpos=291
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAQCODICE_1_1 as StdField with uid="DZAQHBVATN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_AQCODICE", cQueryName = "AQCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice query",;
    HelpContextID = 171305397,;
   bGlobalFont=.t.,;
    Height=21, Width=145, Left=162, Top=14, InputMask=replicate('X',20)

  add object oAQDES_1_2 as StdField with uid="RMZYZIEHMF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AQDES", cQueryName = "AQDES",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 229647878,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=310, Top=14, InputMask=replicate('X',254)


  add object oAQQUEFIL_1_3 as StdCombo with uid="KHGZODKJCJ",rtseq=3,rtrep=.f.,left=694,top=14,width=94,height=21;
    , ToolTipText = "Standard: con filtri e senza filtri, filtrata: solo con filtri, non filtrata: solo senza filtri";
    , HelpContextID = 48297554;
    , cFormVar="w_AQQUEFIL",RowSource=""+"Standard,"+"Filtrata,"+"Non filtrata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAQQUEFIL_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AQQUEFIL,&i_cF..t_AQQUEFIL),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'F',;
    iif(xVal =3,'N',;
    space(1)))))
  endfunc
  func oAQQUEFIL_1_3.GetRadio()
    this.Parent.oContained.w_AQQUEFIL = this.RadioValue()
    return .t.
  endfunc

  func oAQQUEFIL_1_3.ToRadio()
    this.Parent.oContained.w_AQQUEFIL=trim(this.Parent.oContained.w_AQQUEFIL)
    return(;
      iif(this.Parent.oContained.w_AQQUEFIL=='S',1,;
      iif(this.Parent.oContained.w_AQQUEFIL=='F',2,;
      iif(this.Parent.oContained.w_AQQUEFIL=='N',3,;
      0))))
  endfunc

  func oAQQUEFIL_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oAQQRPATH_1_5 as StdField with uid="EUSKEMDULF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AQQRPATH", cQueryName = "AQQRPATH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File completo di percorso che contiene la query da eseguire",;
    HelpContextID = 24250802,;
   bGlobalFont=.t.,;
    Height=21, Width=600, Left=162, Top=42, InputMask=replicate('X',254)


  add object oObj_1_8 as cp_askfile with uid="IOSZOIAMBB",left=766, top=42, width=19,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_QRPATH",cExt="IRP",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il file query di origine";
    , HelpContextID = 137994710

  add object oAQIMPATH_1_9 as StdField with uid="ESHHYQSYOP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_AQIMPATH", cQueryName = "AQIMPATH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File completo di percorso dell'InfoMart a cui deve fare riferimento InfoPublisher",;
    HelpContextID = 24611250,;
   bGlobalFont=.t.,;
    Height=21, Width=600, Left=162, Top=67, InputMask=replicate('X',254)


  add object oObj_1_10 as cp_askfile with uid="GLARGBIZMK",left=766, top=67, width=19,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_IMPATH",cExt="MRT",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il file InfoMart";
    , HelpContextID = 137994710


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=100, width=794,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="N.riga",Field2="AQFLUE",Label2="Tipo",Field3="AQUTENTE",Label3="Utente",Field4="DESTUTE",Label4="Descrizione",Field5="AQMAILR",Label5="Destinatari E-mail",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185449082

  add object oStr_1_4 as StdString with uid="EJQXIWDSSI",Visible=.t., Left=12, Top=14,;
    Alignment=1, Width=147, Height=18,;
    Caption="Codice query:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="ZSLWVWQJDW",Visible=.t., Left=12, Top=42,;
    Alignment=1, Width=147, Height=18,;
    Caption="File query di origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="DAKBGHBMYK",Visible=.t., Left=12, Top=67,;
    Alignment=1, Width=147, Height=18,;
    Caption="File InfoMart:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="JLNEABYZZR",Visible=.t., Left=641, Top=14,;
    Alignment=1, Width=51, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=119,;
    width=788+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=120,width=787+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='UTE_NTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='UTE_NTI'
        oDropInto=this.oBodyCol.oRow.oAQUTENTE_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oAQFORTYP_3_3 as StdCombo with uid="SGVCIWNCBF",rtrep=.f.,;
    cFormVar="w_AQFORTYP", RowSource=""+"Report infobusiness,"+"Rich Text Format,"+"Excel,"+"HTML statico,"+"Comma separated values,"+"PDF" , ;
    ToolTipText = "Formato del file generato",;
    HelpContextID = 27936342,;
    Height=25, Width=199, Left=162, Top=321,;
    cQueryName = "AQFORTYP",;
    bObbl = .f. , nPag=3;
  , bGlobalFont=.t.



  func oAQFORTYP_3_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AQFORTYP,&i_cF..t_AQFORTYP),this.value)
    return(iif(xVal =1,'efQuery',;
    iif(xVal =2,'efRTF',;
    iif(xVal =3,'efExcel',;
    iif(xVal =4,'efHTML',;
    iif(xVal =5,'efCSV',;
    iif(xVal =6,'efPDF',;
    space(10))))))))
  endfunc
  func oAQFORTYP_3_3.GetRadio()
    this.Parent.oContained.w_AQFORTYP = this.RadioValue()
    return .t.
  endfunc

  func oAQFORTYP_3_3.ToRadio()
    this.Parent.oContained.w_AQFORTYP=trim(this.Parent.oContained.w_AQFORTYP)
    return(;
      iif(this.Parent.oContained.w_AQFORTYP=='efQuery',1,;
      iif(this.Parent.oContained.w_AQFORTYP=='efRTF',2,;
      iif(this.Parent.oContained.w_AQFORTYP=='efExcel',3,;
      iif(this.Parent.oContained.w_AQFORTYP=='efHTML',4,;
      iif(this.Parent.oContained.w_AQFORTYP=='efCSV',5,;
      iif(this.Parent.oContained.w_AQFORTYP=='efPDF',6,;
      0)))))))
  endfunc

  func oAQFORTYP_3_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oAQDTFILE_3_4 as StdField with uid="RKUBLURJEB",rtseq=8,rtrep=.f.,;
    cFormVar="w_AQDTFILE",value=space(254),;
    ToolTipText = "File completo di percorso che conterr� i dati aggiornati che costituiscono il risultato della query",;
    HelpContextID = 168876469,;
    cQueryName = "AQDTFILE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=600, Left=162, Top=365, InputMask=replicate('X',254)

  func oAQDTFILE_3_4.mCond()
    with this.Parent.oContained
      return (.w_AQQUEFIL <> 'F')
    endwith
  endfunc

  add object oObj_3_6 as cp_askfile with uid="TZTJPDPRUY",width=19,height=22,;
   left=766, top=365,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_DTFILE",cExt="IRP",;
    nPag=3;
    , ToolTipText = "Premere per selezionare il file di destinazione";
    , HelpContextID = 137994710

  add object oAQLOG_3_7 as StdField with uid="AZOLGOLVZB",rtseq=20,rtrep=.f.,;
    cFormVar="w_AQLOG",value=space(254),;
    ToolTipText = "File completo di percorso del log",;
    HelpContextID = 217753094,;
    cQueryName = "AQLOG",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=600, Left=162, Top=390, InputMask=replicate('X',254)

  add object oObj_3_8 as cp_askfile with uid="SBLSNIACJE",width=19,height=22,;
   left=766, top=390,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_LOG",;
    nPag=3;
    , ToolTipText = "Premere per selezionare il file di log";
    , HelpContextID = 137994710

  add object oAQFLGZCP_3_9 as StdCheck with uid="LDLSVXZJFC",rtrep=.f.,;
    cFormVar="w_AQFLGZCP",  caption="Abilitazione pubblicazione query su Zucchetti Corporate Portal (DMS)",;
    ToolTipText = "Flag abilitazione invio query a Zucchetti Corporate Portal (DMS)",;
    HelpContextID = 116868694,;
    Left=162, Top=420,;
    cQueryName = "AQFLGZCP",;
    bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAQFLGZCP_3_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AQFLGZCP,&i_cF..t_AQFLGZCP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oAQFLGZCP_3_9.GetRadio()
    this.Parent.oContained.w_AQFLGZCP = this.RadioValue()
    return .t.
  endfunc

  func oAQFLGZCP_3_9.ToRadio()
    this.Parent.oContained.w_AQFLGZCP=trim(this.Parent.oContained.w_AQFLGZCP)
    return(;
      iif(this.Parent.oContained.w_AQFLGZCP=='S',1,;
      0))
  endfunc

  func oAQFLGZCP_3_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAQFLGZCP_3_9.mCond()
    with this.Parent.oContained
      return (g_IZCP$'SA' AND .w_AQQUEFIL <> 'F')
    endwith
  endfunc

  add object oBtn_3_16 as StdButton with uid="HGSUFJPHJA",width=48,height=45,;
   left=683, top=312,;
    CpPicture="COPY.BMP", caption="", nPag=3;
    , ToolTipText = "Duplica la query corrente";
    , HelpContextID = 231077322;
    , Caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_16.Click()
      with this.Parent.oContained
        this.Parent.oContained.Duplica()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_16.mCond()
    with this.Parent.oContained
      return (.cFunction = 'Query')
    endwith
  endfunc

  func oBtn_3_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_AQCODICE) OR .cFunction <> 'Query')
    endwith
   endif
  endfunc

  add object oStr_3_5 as StdString with uid="TBTHCCVWWR",Visible=.t., Left=28, Top=365,;
    Alignment=1, Width=131, Height=18,;
    Caption="File di destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="REGJWAMBNX",Visible=.t., Left=28, Top=420,;
    Alignment=1, Width=131, Height=18,;
    Caption="Corporate Portal:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="NHBXBNSAVE",Visible=.t., Left=28, Top=390,;
    Alignment=1, Width=131, Height=18,;
    Caption="File del log:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="UPUNVVMOAE",Visible=.t., Left=28, Top=321,;
    Alignment=1, Width=131, Height=18,;
    Caption="Formato:"  ;
  , bGlobalFont=.t.
enddefine

  define class tgsir_maqPag2 as StdContainer
    Width  = 803
    height = 453
    stdWidth  = 803
    stdheight = 453
  resizeXpos=652
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oAQCRIPTE_4_1 as StdCombo with uid="UCAPDINIPS",rtseq=9,rtrep=.f.,left=310,top=43,width=124,height=21, enabled=.f.;
    , ToolTipText = "Tipo di protezione";
    , HelpContextID = 220010059;
    , cFormVar="w_AQCRIPTE",RowSource=""+"Stringa cifrata,"+"Nessuna cifratura", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAQCRIPTE_4_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AQCRIPTE,&i_cF..t_AQCRIPTE),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oAQCRIPTE_4_1.GetRadio()
    this.Parent.oContained.w_AQCRIPTE = this.RadioValue()
    return .t.
  endfunc

  func oAQCRIPTE_4_1.ToRadio()
    this.Parent.oContained.w_AQCRIPTE=trim(this.Parent.oContained.w_AQCRIPTE)
    return(;
      iif(this.Parent.oContained.w_AQCRIPTE=='C',1,;
      iif(this.Parent.oContained.w_AQCRIPTE=='N',2,;
      0)))
  endfunc

  func oAQCRIPTE_4_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oAQADOCS_4_2 as StdField with uid="QWKCRUORAH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AQADOCS", cQueryName = "AQADOCS",;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di connessione ADO",;
    HelpContextID = 261163514,;
   bGlobalFont=.t.,;
    Height=21, Width=678, Left=17, Top=73, InputMask=replicate('X',254)


  add object oBtn_4_3 as StdButton with uid="ZTMQWGXNSN",left=706, top=73, width=20,height=22,;
    caption="...", nPag=4;
    , ToolTipText = "Costruzione stringa ADO";
    , HelpContextID = 137994710;
  , bGlobalFont=.t.

    proc oBtn_4_3.Click()
      do GSIR_KAD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_4 as StdButton with uid="BLYJZKVELC",left=473, top=19, width=48,height=45,;
    CpPicture="bmp\CIFRA.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per cifrare la stringa di connessione";
    , HelpContextID = 137793782;
    , Caption='\<Cifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_4.Click()
      do GSIR_KCR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_4.mCond()
    with this.Parent.oContained
      return (.w_AQCRIPTE='N')
    endwith
  endfunc

  func oBtn_4_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AQCRIPTE='C')
    endwith
   endif
  endfunc


  add object oBtn_4_5 as StdButton with uid="GASXYNBYBG",left=473, top=19, width=48,height=45,;
    CpPicture="BMP\DECIFRA.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per decifrare la stringa di connessione";
    , HelpContextID = 137793782;
    , Caption='\<Decifra';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_5.Click()
      do GSIR_KCR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_5.mCond()
    with this.Parent.oContained
      return (.w_AQCRIPTE='C')
    endwith
  endfunc

  func oBtn_4_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AQCRIPTE='N')
    endwith
   endif
  endfunc


  add object oAQCONTYP_4_7 as StdCombo with uid="XAVSNDYKKI",rtseq=22,rtrep=.f.,left=163,top=182,width=118,height=21;
    , ToolTipText = "Motore di database utilizzato";
    , HelpContextID = 23729750;
    , cFormVar="w_AQCONTYP",RowSource=""+"Ad hoc (default),"+"Access,"+"SQL Server,"+"Oracle,"+"DB2,"+"AS400,"+"Informix,"+"Dbase,"+"Mysql,"+"Interbase,"+"DBMaker,"+"Visual foxpro,"+"Paradox,"+"Postgresql", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAQCONTYP_4_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AQCONTYP,&i_cF..t_AQCONTYP),this.value)
    return(iif(xVal =1,CP_DBTYPE,;
    iif(xVal =2,'Access',;
    iif(xVal =3,'SQLServer',;
    iif(xVal =4,'Oracle',;
    iif(xVal =5,'DB2',;
    iif(xVal =6,'AS400',;
    iif(xVal =7,'Informix',;
    iif(xVal =8,'DBase',;
    iif(xVal =9,'Mysql',;
    iif(xVal =10,'Interbase',;
    iif(xVal =11,'DBMaker',;
    iif(xVal =12,'Visual FoxPro',;
    iif(xVal =13,'Paradox',;
    iif(xVal =14,'PostGreSQL',;
    space(15))))))))))))))))
  endfunc
  func oAQCONTYP_4_7.GetRadio()
    this.Parent.oContained.w_AQCONTYP = this.RadioValue()
    return .t.
  endfunc

  func oAQCONTYP_4_7.ToRadio()
    this.Parent.oContained.w_AQCONTYP=trim(this.Parent.oContained.w_AQCONTYP)
    return(;
      iif(this.Parent.oContained.w_AQCONTYP==CP_DBTYPE,1,;
      iif(this.Parent.oContained.w_AQCONTYP=='Access',2,;
      iif(this.Parent.oContained.w_AQCONTYP=='SQLServer',3,;
      iif(this.Parent.oContained.w_AQCONTYP=='Oracle',4,;
      iif(this.Parent.oContained.w_AQCONTYP=='DB2',5,;
      iif(this.Parent.oContained.w_AQCONTYP=='AS400',6,;
      iif(this.Parent.oContained.w_AQCONTYP=='Informix',7,;
      iif(this.Parent.oContained.w_AQCONTYP=='DBase',8,;
      iif(this.Parent.oContained.w_AQCONTYP=='Mysql',9,;
      iif(this.Parent.oContained.w_AQCONTYP=='Interbase',10,;
      iif(this.Parent.oContained.w_AQCONTYP=='DBMaker',11,;
      iif(this.Parent.oContained.w_AQCONTYP=='Visual FoxPro',12,;
      iif(this.Parent.oContained.w_AQCONTYP=='Paradox',13,;
      iif(this.Parent.oContained.w_AQCONTYP=='PostGreSQL',14,;
      0)))))))))))))))
  endfunc

  func oAQCONTYP_4_7.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oAQCURLOC_4_8 as StdCombo with uid="RWATGPIRMO",rtseq=23,rtrep=.f.,left=163,top=227,width=118,height=21;
    , ToolTipText = "Indica se il cursore ADO che contiene i dati debba essere mantenuto lato server o lato client";
    , HelpContextID = 105900471;
    , cFormVar="w_AQCURLOC",RowSource=""+"Client,"+"Server (default)", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAQCURLOC_4_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AQCURLOC,&i_cF..t_AQCURLOC),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oAQCURLOC_4_8.GetRadio()
    this.Parent.oContained.w_AQCURLOC = this.RadioValue()
    return .t.
  endfunc

  func oAQCURLOC_4_8.ToRadio()
    this.Parent.oContained.w_AQCURLOC=trim(this.Parent.oContained.w_AQCURLOC)
    return(;
      iif(this.Parent.oContained.w_AQCURLOC=='C',1,;
      iif(this.Parent.oContained.w_AQCURLOC=='S',2,;
      0)))
  endfunc

  func oAQCURLOC_4_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAQCURLOC_4_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_AVANZATE)
    endwith
    endif
  endfunc


  add object oAQCURTYP_4_9 as StdCombo with uid="WPIGSSZONV",rtseq=24,rtrep=.f.,left=163,top=261,width=153,height=21;
    , ToolTipText = "Indica il tipo di cursore ADO utilizzato";
    , HelpContextID = 28317270;
    , cFormVar="w_AQCURTYP",RowSource=""+"ctstatic,"+"ctunspecified,"+"ctopenforwardonly (default),"+"ctkeyset,"+"ctdynamic", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAQCURTYP_4_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AQCURTYP,&i_cF..t_AQCURTYP),this.value)
    return(iif(xVal =1,'ctStatic',;
    iif(xVal =2,'ctUnspecified',;
    iif(xVal =3,'ctOpenForwardOnly',;
    iif(xVal =4,'ctKeyset',;
    iif(xVal =5,'ctDynamic',;
    space(20)))))))
  endfunc
  func oAQCURTYP_4_9.GetRadio()
    this.Parent.oContained.w_AQCURTYP = this.RadioValue()
    return .t.
  endfunc

  func oAQCURTYP_4_9.ToRadio()
    this.Parent.oContained.w_AQCURTYP=trim(this.Parent.oContained.w_AQCURTYP)
    return(;
      iif(this.Parent.oContained.w_AQCURTYP=='ctStatic',1,;
      iif(this.Parent.oContained.w_AQCURTYP=='ctUnspecified',2,;
      iif(this.Parent.oContained.w_AQCURTYP=='ctOpenForwardOnly',3,;
      iif(this.Parent.oContained.w_AQCURTYP=='ctKeyset',4,;
      iif(this.Parent.oContained.w_AQCURTYP=='ctDynamic',5,;
      0))))))
  endfunc

  func oAQCURTYP_4_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAQCURTYP_4_9.mCond()
    with this.Parent.oContained
      return (.w_AQCURLOC = 'S')
    endwith
  endfunc

  func oAQCURTYP_4_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_AVANZATE)
    endwith
    endif
  endfunc

  add object oAQTIMOUT_4_10 as StdField with uid="QHKIDTPBVF",rtseq=25,rtrep=.f.,;
    cFormVar = "w_AQTIMOUT", cQueryName = "AQTIMOUT",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Timeout in secondi per la connessione (se zero timeout infinito)",;
    HelpContextID = 206906970,;
   bGlobalFont=.t.,;
    Height=21, Width=130, Left=514, Top=261, cSayPict='"999"', cGetPict='"999"'

  func oAQTIMOUT_4_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_AVANZATE)
    endwith
    endif
  endfunc

  add object oAVANZATE_4_12 as StdCheck with uid="TPLVUKFQEU",rtseq=26,rtrep=.f.,left=514, top=185, caption="Avanzate",;
    ToolTipText = "Se attivo: visualizza le configurazioni avanzate",;
    HelpContextID = 14091445,;
    cFormVar="w_AVANZATE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAVANZATE_4_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AVANZATE,&i_cF..t_AVANZATE),this.value)
    return(iif(xVal =1,.T.,;
    .f.))
  endfunc
  func oAVANZATE_4_12.GetRadio()
    this.Parent.oContained.w_AVANZATE = this.RadioValue()
    return .t.
  endfunc

  func oAVANZATE_4_12.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_AVANZATE==.T.,1,;
      0))
  endfunc

  func oAVANZATE_4_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oAQFLJOIN_4_19 as StdCheck with uid="TRQIVTMYPW",rtseq=31,rtrep=.f.,left=514, top=229, caption="WhereForJoins",;
    ToolTipText = "Se attivo utilizza la sintassi where per costruire le clausole di join altrimenti la sintassi inner join",;
    HelpContextID = 203900500,;
    cFormVar="w_AQFLJOIN", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAQFLJOIN_4_19.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AQFLJOIN,&i_cF..t_AQFLJOIN),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oAQFLJOIN_4_19.GetRadio()
    this.Parent.oContained.w_AQFLJOIN = this.RadioValue()
    return .t.
  endfunc

  func oAQFLJOIN_4_19.ToRadio()
    this.Parent.oContained.w_AQFLJOIN=trim(this.Parent.oContained.w_AQFLJOIN)
    return(;
      iif(this.Parent.oContained.w_AQFLJOIN=='S',1,;
      0))
  endfunc

  func oAQFLJOIN_4_19.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAQFLJOIN_4_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_AVANZATE)
    endwith
    endif
  endfunc

  add object oStr_4_6 as StdString with uid="CGCNCSEBVX",Visible=.t., Left=17, Top=46,;
    Alignment=0, Width=173, Height=18,;
    Caption="Stringa di connessione ADO"  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="RGVZWSIYEH",Visible=.t., Left=29, Top=182,;
    Alignment=1, Width=131, Height=17,;
    Caption="Tipo database:"  ;
  , bGlobalFont=.t.

  add object oStr_4_13 as StdString with uid="HPZAYJZBIR",Visible=.t., Left=29, Top=227,;
    Alignment=1, Width=131, Height=18,;
    Caption="Cursore lato:"  ;
  , bGlobalFont=.t.

  func oStr_4_13.mHide()
    with this.Parent.oContained
      return (not .w_AVANZATE)
    endwith
  endfunc

  add object oStr_4_14 as StdString with uid="BDVVSUAAIO",Visible=.t., Left=29, Top=261,;
    Alignment=1, Width=131, Height=17,;
    Caption="Tipo cursore:"  ;
  , bGlobalFont=.t.

  func oStr_4_14.mHide()
    with this.Parent.oContained
      return (not .w_AVANZATE)
    endwith
  endfunc

  add object oStr_4_15 as StdString with uid="FBHMRPMZLD",Visible=.t., Left=371, Top=261,;
    Alignment=1, Width=141, Height=18,;
    Caption="Connection timeout:"  ;
  , bGlobalFont=.t.

  func oStr_4_15.mHide()
    with this.Parent.oContained
      return (not .w_AVANZATE)
    endwith
  endfunc

  add object oStr_4_17 as StdString with uid="ZGDKJNSOTD",Visible=.t., Left=12, Top=143,;
    Alignment=0, Width=91, Height=18,;
    Caption="Configurazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_18 as StdString with uid="KIGDBVVPMD",Visible=.t., Left=196, Top=46,;
    Alignment=1, Width=111, Height=18,;
    Caption="Tipo di protezione:"  ;
  , bGlobalFont=.t.

  add object oBox_4_16 as StdBox with uid="FAWGUJHWYU",left=7, top=161, width=699,height=180
enddefine

  define class tgsir_maqPag3 as StdContainer
    Width  = 803
    height = 453
    stdWidth  = 803
    stdheight = 453
  resizeXpos=543
  resizeYpos=253
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_5_1 as stdDynamicChildContainer with uid="ARIYWONOPI",left=409, top=27, width=367, height=421, bOnScreen=.t.;



  add object oLinkPC_5_3 as StdButton with uid="GGQMNXGAXU",left=66, top=405, width=48,height=45,;
    CpPicture="bmp\filtra.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per specificare i filtri non collegati a gestioni";
    , HelpContextID = 220560627;
    , Caption='\<Filtri Stan.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_5_3.Click()
      this.Parent.oContained.gsir_mfr.LinkPCClick()
    endproc

  func oLinkPC_5_3.mCond()
    with this.Parent.oContained
      return (.w_AQQUEFIL<>'N')
    endwith
  endfunc

  func oLinkPC_5_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AQQUEFIL='N')
    endwith
   endif
  endfunc


  add object oObj_5_4 as cp_zoombox with uid="CVUBPSXWKP",left=6, top=26, width=397,height=375,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSIR_MAQ",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.t.,cTable="INF_AUQU",;
    nPag=5;
    , HelpContextID = 221079578


  add object oLinkPC_5_7 as StdButton with uid="KKJYRAONOL",left=15, top=405, width=48,height=45,;
    CpPicture="bmp\param.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per visaulizzare i parametri";
    , HelpContextID = 74391032;
    , Caption='\<Parametri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_5_7.Click()
      this.Parent.oContained.GSIR_MPF.LinkPCClick()
    endproc

  func oLinkPC_5_7.mCond()
    with this.Parent.oContained
      return (.w_AQQUEFIL <> 'N')
    endwith
  endfunc

  func oLinkPC_5_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AQQUEFIL = 'N')
    endwith
   endif
  endfunc

  add object oStr_5_2 as StdString with uid="ZYGSYJLYUZ",Visible=.t., Left=427, Top=8,;
    Alignment=0, Width=96, Height=18,;
    Caption="Contesti"  ;
  , bGlobalFont=.t.

  add object oStr_5_5 as StdString with uid="KBQQTCRPMY",Visible=.t., Left=30, Top=6,;
    Alignment=0, Width=113, Height=18,;
    Caption="Autorizzazioni"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsir_mgi",lower(this.oContained.GSIR_MGI.class))=0
        this.oContained.GSIR_MGI.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

  define class tgsir_maqPag4 as StdContainer
    Width  = 803
    height = 453
    stdWidth  = 803
    stdheight = 453
  resizeXpos=479
  resizeYpos=345
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_6_1 as stdDynamicChildContainer with uid="HHYMUCQIEG",left=1, top=10, width=801, height=356, bOnScreen=.t.;


  func oLinkPC_6_1.mCond()
    with this.Parent.oContained
      return (.w_AQFLGZCP='S' and g_IZCP$'SA' AND .w_AQQUEFIL <> 'F')
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsir_mdv",lower(this.oContained.GSIR_MDV.class))=0
        this.oContained.GSIR_MDV.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

* --- Defining Body row
define class tgsir_maqBodyRow as CPBodyRowCnt
  Width=778
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="EYLWPEZOWV",rtseq=11,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 50657942,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0

  add object oAQFLUE_2_2 as StdTrsCombo with uid="YHZPZTMNXA",rtrep=.t.,;
    cFormVar="w_AQFLUE", RowSource=""+"Utente,"+"E-mail" , ;
    ToolTipText = "Tipo di destinatario (utente/E-mail)",;
    HelpContextID = 220772858,;
    Height=21, Width=83, Left=48, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAQFLUE_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AQFLUE,&i_cF..t_AQFLUE),this.value)
    return(iif(xVal =1,'U',;
    iif(xVal =2,'E',;
    space(1))))
  endfunc
  func oAQFLUE_2_2.GetRadio()
    this.Parent.oContained.w_AQFLUE = this.RadioValue()
    return .t.
  endfunc

  func oAQFLUE_2_2.ToRadio()
    this.Parent.oContained.w_AQFLUE=trim(this.Parent.oContained.w_AQFLUE)
    return(;
      iif(this.Parent.oContained.w_AQFLUE=='U',1,;
      iif(this.Parent.oContained.w_AQFLUE=='E',2,;
      0)))
  endfunc

  func oAQFLUE_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAQFLUE_2_2.mCond()
    with this.Parent.oContained
      return (.w_AQQUEFIL <> 'F')
    endwith
  endfunc

  add object oAQUTENTE_2_3 as StdTrsField with uid="QKNJCBBPCJ",rtseq=13,rtrep=.t.,;
    cFormVar="w_AQUTENTE",value=0,;
    ToolTipText = "Utente destinatario dell'E-mail",;
    HelpContextID = 182466123,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=136, Top=0, cSayPict=["9999"], cGetPict=["9999"], bHasZoom = .t. , cLinkFile="UTE_NTI", oKey_1_1="UTCODICE", oKey_1_2="this.w_AQUTENTE"

  func oAQUTENTE_2_3.mCond()
    with this.Parent.oContained
      return (.w_AQFLUE<>'E' AND .w_AQQUEFIL <> 'F')
    endwith
  endfunc

  func oAQUTENTE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oAQUTENTE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAQUTENTE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UTE_NTI','*','UTCODICE',cp_AbsName(this.parent,'oAQUTENTE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti dotati di E-mail",'gsir_maq.UTE_NTI_VZM',this.parent.oContained
  endproc

  add object oDESTUTE_2_4 as StdTrsField with uid="IOWKTVLYYP",rtseq=14,rtrep=.t.,;
    cFormVar="w_DESTUTE",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 31459894,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=169, Left=194, Top=0, InputMask=replicate('X',40)

  add object oAQMAILR_2_5 as StdTrsField with uid="ECDHSVEGRZ",rtseq=15,rtrep=.t.,;
    cFormVar="w_AQMAILR",value=space(100),isprimarykey=.t.,;
    HelpContextID = 116607482,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=406, Left=367, Top=0, InputMask=replicate('X',100)

  func oAQMAILR_2_5.mCond()
    with this.Parent.oContained
      return (.w_AQQUEFIL <> 'F')
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_maq','INF_AQM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AQCODICE=INF_AQM.AQCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsir_maq
Proc NascondiFiltri(oParent)
    oParent.GSIR_MGI.w_AQQUEFIL = oParent.w_AQQUEFIL
    oParent.GSIR_MGI.mHideControls()
EndProc
* --- Fine Area Manuale
