* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsir_kad                                                        *
*              Configurazione stringa ADO                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_4]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-10                                                      *
* Last revis.: 2009-08-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsir_kad",oParentObject))

* --- Class definition
define class tgsir_kad as StdForm
  Top    = 3
  Left   = 4

  * --- Standard Properties
  Width  = 359
  Height = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-08-05"
  HelpContextID=132738921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsir_kad"
  cComment = "Configurazione stringa ADO"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AQADOCS = space(254)
  w_ODBC = space(254)
  o_ODBC = space(254)
  w_NOMEUTE = space(254)
  o_NOMEUTE = space(254)
  w_PASSWORD = space(254)
  o_PASSWORD = space(254)
  w_DBNAME = space(254)
  o_DBNAME = space(254)
  w_MODACC = space(1)
  o_MODACC = space(1)
  w_SECINFO = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsir_kadPag1","gsir_kad",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oODBC_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AQADOCS=space(254)
      .w_ODBC=space(254)
      .w_NOMEUTE=space(254)
      .w_PASSWORD=space(254)
      .w_DBNAME=space(254)
      .w_MODACC=space(1)
      .w_SECINFO=space(1)
      .w_AQADOCS=oParentObject.w_AQADOCS
        .w_AQADOCS = 'Provider=MSDASQL.1;Persist Security Info=' + IIF(.w_SECINFO='S', 'True', 'False') + ';User ID=' + ALLTRIM(.w_NOMEUTE) + ';Password=' + ALLTRIM(.w_PASSWORD) + ';Data Source=' + ALLTRIM(.w_ODBC) + ';Initial Catalog=' + ALLTRIM(.w_DBNAME) + ';Mode='+IIF(.w_MODACC = 'E', 'ReadWrite', IIF(.w_MODACC = 'W', 'Write', 'Read'))
          .DoRTCalc(2,5,.f.)
        .w_MODACC = 'R'
        .w_SECINFO = ' '
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AQADOCS=.w_AQADOCS
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_DBNAME<>.w_DBNAME.or. .o_MODACC<>.w_MODACC.or. .o_NOMEUTE<>.w_NOMEUTE.or. .o_ODBC<>.w_ODBC.or. .o_PASSWORD<>.w_PASSWORD
            .w_AQADOCS = 'Provider=MSDASQL.1;Persist Security Info=' + IIF(.w_SECINFO='S', 'True', 'False') + ';User ID=' + ALLTRIM(.w_NOMEUTE) + ';Password=' + ALLTRIM(.w_PASSWORD) + ';Data Source=' + ALLTRIM(.w_ODBC) + ';Initial Catalog=' + ALLTRIM(.w_DBNAME) + ';Mode='+IIF(.w_MODACC = 'E', 'ReadWrite', IIF(.w_MODACC = 'W', 'Write', 'Read'))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oODBC_1_2.value==this.w_ODBC)
      this.oPgFrm.Page1.oPag.oODBC_1_2.value=this.w_ODBC
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEUTE_1_4.value==this.w_NOMEUTE)
      this.oPgFrm.Page1.oPag.oNOMEUTE_1_4.value=this.w_NOMEUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPASSWORD_1_6.value==this.w_PASSWORD)
      this.oPgFrm.Page1.oPag.oPASSWORD_1_6.value=this.w_PASSWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oDBNAME_1_8.value==this.w_DBNAME)
      this.oPgFrm.Page1.oPag.oDBNAME_1_8.value=this.w_DBNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oMODACC_1_10.RadioValue()==this.w_MODACC)
      this.oPgFrm.Page1.oPag.oMODACC_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSECINFO_1_12.RadioValue()==this.w_SECINFO)
      this.oPgFrm.Page1.oPag.oSECINFO_1_12.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ODBC = this.w_ODBC
    this.o_NOMEUTE = this.w_NOMEUTE
    this.o_PASSWORD = this.w_PASSWORD
    this.o_DBNAME = this.w_DBNAME
    this.o_MODACC = this.w_MODACC
    return

enddefine

* --- Define pages as container
define class tgsir_kadPag1 as StdContainer
  Width  = 355
  height = 239
  stdWidth  = 355
  stdheight = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oODBC_1_2 as StdField with uid="IJXYYBFVQY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ODBC", cQueryName = "ODBC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Specificare l'origine dati (ODBC)",;
    HelpContextID = 128058906,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=131, Top=15, InputMask=replicate('X',254)

  add object oNOMEUTE_1_4 as StdField with uid="WEXEXQWBRL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NOMEUTE", cQueryName = "NOMEUTE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 28357846,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=131, Top=47, InputMask=replicate('X',254)

  add object oPASSWORD_1_6 as StdField with uid="VQFOMROXNH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PASSWORD", cQueryName = "PASSWORD",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 215942970,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=131, Top=79, InputMask=replicate('X',254)

  add object oDBNAME_1_8 as StdField with uid="KHZVRKDTRY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DBNAME", cQueryName = "DBNAME",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 231950538,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=131, Top=111, InputMask=replicate('X',254)


  add object oMODACC_1_10 as StdCombo with uid="UJLFPTCESO",rtseq=6,rtrep=.f.,left=131,top=176,width=99,height=21;
    , ToolTipText = "Autorizzazioni di accesso";
    , HelpContextID = 7592762;
    , cFormVar="w_MODACC",RowSource=""+"Read,"+"Write,"+"Readwrite", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMODACC_1_10.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'W',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oMODACC_1_10.GetRadio()
    this.Parent.oContained.w_MODACC = this.RadioValue()
    return .t.
  endfunc

  func oMODACC_1_10.SetRadio()
    this.Parent.oContained.w_MODACC=trim(this.Parent.oContained.w_MODACC)
    this.value = ;
      iif(this.Parent.oContained.w_MODACC=='R',1,;
      iif(this.Parent.oContained.w_MODACC=='W',2,;
      iif(this.Parent.oContained.w_MODACC=='E',3,;
      0)))
  endfunc

  add object oSECINFO_1_12 as StdCheck with uid="XCGTMHQQJI",rtseq=7,rtrep=.f.,left=131, top=143, caption="Persist security info",;
    ToolTipText = "Persist security info",;
    HelpContextID = 213644506,;
    cFormVar="w_SECINFO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSECINFO_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSECINFO_1_12.GetRadio()
    this.Parent.oContained.w_SECINFO = this.RadioValue()
    return .t.
  endfunc

  func oSECINFO_1_12.SetRadio()
    this.Parent.oContained.w_SECINFO=trim(this.Parent.oContained.w_SECINFO)
    this.value = ;
      iif(this.Parent.oContained.w_SECINFO=='S',1,;
      0)
  endfunc


  add object oBtn_1_13 as StdButton with uid="RIHNVYPWOK",left=245, top=187, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 254548857;
    , tabstop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="LKDJRNWUMO",left=299, top=187, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 125421498;
    , tabstop=.f., caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="PZDJJASEXY",Visible=.t., Left=12, Top=15,;
    Alignment=1, Width=115, Height=18,;
    Caption="Origine dati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BZYZRTEQFS",Visible=.t., Left=12, Top=47,;
    Alignment=1, Width=115, Height=18,;
    Caption="Nome utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="HOVWAIQLHO",Visible=.t., Left=12, Top=79,;
    Alignment=1, Width=115, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="LTXHPSCSDI",Visible=.t., Left=6, Top=111,;
    Alignment=1, Width=121, Height=18,;
    Caption="Catalogo/nome DB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="XJJWZFEIHC",Visible=.t., Left=12, Top=176,;
    Alignment=1, Width=115, Height=18,;
    Caption="Accesso in:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsir_kad','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
